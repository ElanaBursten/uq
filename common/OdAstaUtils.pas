unit OdAstaUtils;

interface

function DoSql(sql: string; Params: array of const): Integer;
procedure SetCliSock(TheClisock: TAstaClientSocket);

implementation

uses
  AstaClientDataset;

var
  Clisock: TAstaClientSocket;

procedure SetCliSock(TheClisock: TAstaClientSocket);
begin
  Clisock := TheClisock;
end;

procedure SetParams(Query: TAstaClientDataset; Params: array of const);
var
  I: Integer;
begin
  for I := 0 to High(Params) do begin
    try
      with Params[I] do
        case VType of
          vtString:     Query.Params[i].AsString := VString^;
          vtInteger:    Query.Params[i].AsInteger := VInteger;
          vtPChar:      Query.Params[i].AsString := VPChar;
          vtAnsiString: Query.Params[i].AsString := string(VAnsiString);
          vtVariant:    Query.Params[i].AsString := VarToStr(VVariant^);
          vtBoolean:    Query.Params[i].AsBoolean := VBoolean;

          vtExtended:   begin
                           // DB seperates datetimes from floats; Delphi does not.
                          if Pos('DT', Query.Params[i].Name)>0 then  // named DT?
                            Query.Params[i].AsDateTime := VarToDateTime(VExtended^)
                          else
                            Query.Params[i].AsFloat := VExtended^;
                        end;

  {
          vtChar:       Result := Result + VChar;
          vtObject:     Result := Result + VObject.ClassName;
          vtClass:      Result := Result + VClass.ClassName;
          vtCurrency:   Result := Result + CurrToStr(VCurrency^);
          vtInt64:      Result := Result + IntToStr(VInt64^);
  }
          else
            raise Exception.Create('Unexpected VType: ' + IntToStr(VType));
        end;
    except
      on E: Exception do begin
        E.Message := E.Message + ' Param#=' + IntToStr(I);
        raise;
      end;
    end;
  end;
end;

function DoSql(sql: string; Params: array of const): Integer;
var
  Query: TAstaClientDataset;
begin
  Query := TAstaClientDataset.Create(nil);
  try
    Query.AstaClientSocket := CliSock;
    Query.SQL.Text := sql;
    SetParams(Query, Params);
    Query.ExecSQL;
    Result := Query.RowsAffected;
  finally
    FreeAndNil(Query);
  end;
end;

end.
