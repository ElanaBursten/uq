(*

Copyright (c) 1986-1997  Microsoft Corporation

Module Name:

    sti.h

Abstract:

    This module contains the user mode still image APIs in COM format

Revision History:

Delphi Conversion:

  (C) 2005 Milenko Mitrovic <dcoder@dsp-worx.de>

*)

unit STI;

{$DEFINE UNICODE}
{$DEFINE NOT_IMPLEMENTED}

interface

uses
  Windows;

const
  // Class ID
  CLSID_Sti: TGuid = '{B323F8E0-2E68-11D0-90EA-00AA0060F86C}';

  // Interface IID's
  IID_IStillImageW: TGuid = '{641BD880-2DC8-11D0-90EA-00AA0060F86C}';
  IID_IStillImageA: TGuid = '{A7B1F740-1D7F-11D1-ACA9-00A02438AD48}';
  IID_IStiDevice: TGuid = '{6CFA5A80-2DC8-11D0-90EA-00AA0060F86C}';

  // Standard event GUIDs
  GUID_DeviceArrivedLaunch: TGuid = '{740D9EE6-70F1-11d1-AD10-00A02438AD48}';
  GUID_ScanImage: TGuid = '{A6C5A715-8C6E-11d2-977A-0000F87A926F}';
  GUID_ScanPrintImage: TGuid = '{B441F425-8C6E-11d2-977A-0000F87A926F}';
  GUID_ScanFaxImage: TGuid = '{C00EB793-8C6E-11d2-977A-0000F87A926F}';
  GUID_STIUserDefined1: TGuid = '{C00EB795-8C6E-11d2-977A-0000F87A926F}';
  GUID_STIUserDefined2: TGuid = '{C77AE9C5-8C6E-11d2-977A-0000F87A926F}';
  GUID_STIUserDefined3: TGuid = '{C77AE9C6-8C6E-11d2-977A-0000F87A926F}';

  // Generic constants and definitions

  STI_VERSION_FLAG_MASK         = $FF000000;
  STI_VERSION_FLAG_UNICODE      = $01000000;

  function GET_STIVER_MAJOR(dwVersion: Cardinal): Cardinal;
  function GET_STIVER_MINOR(dwVersion: Cardinal): Cardinal;

const
  STI_VERSION_REAL              = $00000002;
  STI_VERSION_MIN_ALLOWED       = $00000002;

{$IFDEF UNICODE}
  STI_VERSION                   = STI_VERSION_REAL or STI_VERSION_FLAG_UNICODE;
{$ELSE}
  STI_VERSION                   = STI_VERSION_REAL;
{$ENDIF}

  // Maximum length of internal device name
  STI_MAX_INTERNAL_NAME_LENGTH  = 128;

  // begin sti_device_information

  //
  //  Device information definitions and prototypes
  // ----------------------------------------------
  //

  //
  // Following information is used for enumerating still image devices , currently configured
  // in the system. Presence of the device in the enumerated list does not mean availability
  // of the device, it only means that device was installed at least once and had not been removed since.
  //

  //
  // Type of device ( scanner, camera) is represented by DWORD value with
  // hi word containing generic device type , and lo word containing sub-type
  //
type
  _STI_DEVICE_MJ_TYPE = (
    StiDeviceTypeDefault          = 0,
    StiDeviceTypeScanner          = 1,
    StiDeviceTypeDigitalCamera    = 2,
    StiDeviceTypeStreamingVideo   = 3
  );
  STI_DEVICE_MJ_TYPE = _STI_DEVICE_MJ_TYPE;
  PSTI_DEVICE_MJ_TYPE = ^_STI_DEVICE_MJ_TYPE;

  STI_DEVICE_TYPE = DWORD;

  // Macros to extract device type/subtype from single type field
  function GET_STIDEVICE_TYPE(dwDevType: Cardinal): Cardinal;
  function GET_STIDEVICE_SUBTYPE(dwDevType: Cardinal): Cardinal;

  // Device capabilities bits.
  // Various capabilities are grouped into separate bitmasks
type
  _STI_DEV_CAPS = record
    dwGeneric: DWORD;
  end;
  STI_DEV_CAPS = _STI_DEV_CAPS;
  PSTI_DEV_CAPS = ^_STI_DEV_CAPS;

  // Generic capabilities mask contain 16 bits , common for all devices, maintained by MS
  // and 16 bits , which USD can use for proprietary capbailities reporting.
  function GET_STIDCOMMON_CAPS(dwGenericCaps: Cardinal): Cardinal;
  function GET_STIVENDOR_CAPS(dwGenericCaps: Cardinal): Cardinal;

const
  STI_GENCAP_COMMON_MASK: DWORD = $00FF;

  // Notifications are supported.
  // If this capability set , device can be subscribed to .
  STI_GENCAP_NOTIFICATIONS = $00000001;

  // Polling required .
  // This capability is used when previous is set to TRUE. Presence of it means
  // that device is not capable of issuing "truly" asyncronous notifications, but can
  // be polled to determine the moment when event happened
  STI_GENCAP_POLLING_NEEDED = $00000002;

  // Generate event on device arrival
  // If this capability is set, still image service will generate event when device
  // instance is successfully initialized ( typically in response to PnP arrival)

  // Note: on initial service enumeration events will nto be generated to avoid
  // end-user confusion.
  STI_GENCAP_GENERATE_ARRIVALEVENT = $00000004;

  // Auto port selection on non-PnP buses
  // This capability indicates that USD is able to detect non-PnP device on a
  // bus , device is supposed to be attached to.
  STI_GENCAP_AUTO_PORTSELECT = $00000008;

  // WIA capability bit.
  // This capability indicates that USD is WIA capable.
  STI_GENCAP_WIA = $00000010;

  // Subset driver bit.
  // This capability indicates that there is more featured driver exists. All
  // of inbox driver has this bit set. Fully featured (IHV) driver shouldn't have
  // this bit set.
  STI_GENCAP_SUBSET = $00000020;

  // Type of bus connection for those in need to know
  STI_HW_CONFIG_UNKNOWN   = $0001;
  STI_HW_CONFIG_SCSI      = $0002;
  STI_HW_CONFIG_USB       = $0004;
  STI_HW_CONFIG_SERIAL    = $0008;
  STI_HW_CONFIG_PARALLEL  = $0010;

  // Device information structure, this is not configurable. This data is returned from
  // device enumeration API and is used for populating UI or selecting which device
  // should be used in current session
type
  _STI_DEVICE_INFORMATIONW = record
    dwSize: DWORD;
    // Type of the hardware imaging device
    DeviceType: STI_DEVICE_TYPE;
    // Device identifier for reference when creating device object
    szDeviceInternalName: array[0..STI_MAX_INTERNAL_NAME_LENGTH -1]of WideChar;
    // Set of capabilities flags
    DeviceCapabilities: STI_DEV_CAPS;
    // This includes bus type
    dwHardwareConfiguration: DWORD;
    // Vendor description string
    pszVendorDescription: PWideChar;
    // Device description , provided by vendor
    pszDeviceDescription: PWideChar;
    // String , representing port on which device is accessible.
    pszPortName: PWideChar;
    // Control panel propery provider
    pszPropProvider: PWideChar;
    // Local specific ("friendly") name of the device, mainly used for showing in the UI
    pszLocalName: PWideChar;
  end;
  STI_DEVICE_INFORMATIONW = _STI_DEVICE_INFORMATIONW;
  PSTI_DEVICE_INFORMATIONW = ^_STI_DEVICE_INFORMATIONW;

  _STI_DEVICE_INFORMATIONA = record
    dwSize: DWORD;
    // Type of the hardware imaging device
    DeviceType: STI_DEVICE_TYPE;
    // Device identifier for reference when creating device object
    szDeviceInternalName: array[0..STI_MAX_INTERNAL_NAME_LENGTH -1] of Char;
    // Set of capabilities flags
    DeviceCapabilities: STI_DEV_CAPS;
    // This includes bus type
    dwHardwareConfiguration: DWORD;
    // Vendor description string
    pszVendorDescription: PChar;
    // Device description , provided by vendor
    pszDeviceDescription: PChar;
    // String , representing port on which device is accessible.
    pszPortName: PChar;
    // Control panel propery provider
    pszPropProvider: PChar;
    // Local specific ("friendly") name of the device, mainly used for showing in the UI
    pszLocalName: PChar;
  end;
  STI_DEVICE_INFORMATIONA = _STI_DEVICE_INFORMATIONA;
  PSTI_DEVICE_INFORMATIONA = ^_STI_DEVICE_INFORMATIONA;

{$IFDEF UNICODE}
  STI_DEVICE_INFORMATION = STI_DEVICE_INFORMATIONW;
  PSTI_DEVICE_INFORMATION = PSTI_DEVICE_INFORMATIONW;
{$ELSE}
  STI_DEVICE_INFORMATION = STI_DEVICE_INFORMATIONA;
  PSTI_DEVICE_INFORMATION = PSTI_DEVICE_INFORMATIONA;
{$ENDIF}

  // EXTENDED STI INFORMATION TO COVER WIA
  _STI_WIA_DEVICE_INFORMATIONW = record
    dwSize: DWORD;
    // Type of the hardware imaging device
    DeviceType: STI_DEVICE_TYPE;
    // Device identifier for reference when creating device object
    szDeviceInternalName: array[0..STI_MAX_INTERNAL_NAME_LENGTH -1] of WideChar;
    // Set of capabilities flags
    DeviceCapabilities: STI_DEV_CAPS;
    // This includes bus type
    dwHardwareConfiguration: DWORD;
    // Vendor description string
    pszVendorDescription: PWideChar;
    // Device description , provided by vendor
    pszDeviceDescription: PWideChar;
    // String , representing port on which device is accessible.
    pszPortName: PWideChar;
    // Control panel propery provider
    pszPropProvider: PWideChar;
    // Local specific ("friendly") name of the device, mainly used for showing in the UI
    pszLocalName: PWideChar;
    //
    // WIA values
    //
    pszUiDll: PWideChar;
    pszServer: PWideChar;
  end;
  STI_WIA_DEVICE_INFORMATIONW = _STI_WIA_DEVICE_INFORMATIONW;
  PSTI_WIA_DEVICE_INFORMATIONW = ^_STI_WIA_DEVICE_INFORMATIONW;


  _STI_WIA_DEVICE_INFORMATIONA = record
    dwSize: DWORD;
    // Type of the hardware imaging device
    DeviceType: STI_DEVICE_TYPE;
    // Device identifier for reference when creating device object
    szDeviceInternalName: array[0..STI_MAX_INTERNAL_NAME_LENGTH -1] of Char;
    // Set of capabilities flags
    DeviceCapabilities: STI_DEV_CAPS;
    // This includes bus type
    dwHardwareConfiguration: DWORD;
    // Vendor description string
    pszVendorDescription: PChar;
    // Device description , provided by vendor
    pszDeviceDescription: PChar;
    // String , representing port on which device is accessible.
    pszPortName: PChar;
    // Control panel propery provider
    pszPropProvider: PChar;
    // Local specific ("friendly") name of the device, mainly used for showing in the UI
    pszLocalName: PChar;
    //
    // WIA values
    //
    pszUiDll: PChar;
    pszServer: PChar;
  end;
  STI_WIA_DEVICE_INFORMATIONA = _STI_WIA_DEVICE_INFORMATIONA;
  PSTI_WIA_DEVICE_INFORMATIONA = ^_STI_WIA_DEVICE_INFORMATIONA;


{$IFDEF UNICODE}
  STI_WIA_DEVICE_INFORMATION = STI_WIA_DEVICE_INFORMATIONW;
  PSTI_WIA_DEVICE_INFORMATION = PSTI_WIA_DEVICE_INFORMATIONW;
{$ELSE}
  STI_WIA_DEVICE_INFORMATION = STI_WIA_DEVICE_INFORMATIONA;
  PSTI_WIA_DEVICE_INFORMATION = PSTI_WIA_DEVICE_INFORMATIONA;
{$ENDIF}
  // end sti_device_information

  //
  // Device state information.
  // ------------------------
  //
  // Following types  are used to inquire state characteristics of the device after
  // it had been opened.
  //
  // Device configuration structure contains configurable parameters reflecting
  // current state of the device
  //
  //
  // Device hardware status.
  //

  //
  // Individual bits for state acquiring  through StatusMask
  //

  // State of hardware as known to USD
const
  STI_DEVSTATUS_ONLINE_STATE = $0001;

  // State of pending events ( as known to USD)
  STI_DEVSTATUS_EVENTS_STATE = $0002;

  // Online state values
   STI_ONLINESTATE_OPERATIONAL = $00000001;
   STI_ONLINESTATE_PENDING = $00000002;
   STI_ONLINESTATE_ERROR = $00000004;
   STI_ONLINESTATE_PAUSED = $00000008;
   STI_ONLINESTATE_PAPER_JAM = $00000010;
   STI_ONLINESTATE_PAPER_PROBLEM = $00000020;
   STI_ONLINESTATE_OFFLINE = $00000040;
   STI_ONLINESTATE_IO_ACTIVE = $00000080;
   STI_ONLINESTATE_BUSY = $00000100;
   STI_ONLINESTATE_TRANSFERRING = $00000200;
   STI_ONLINESTATE_INITIALIZING = $00000400;
   STI_ONLINESTATE_WARMING_UP = $00000800;
   STI_ONLINESTATE_USER_INTERVENTION = $00001000;
   STI_ONLINESTATE_POWER_SAVE = $00002000;

  // Event processing parameters
  STI_EVENTHANDLING_ENABLED = $00000001;
  STI_EVENTHANDLING_POLLING = $00000002;
  STI_EVENTHANDLING_PENDING = $00000004;

type
  _STI_DEVICE_STATUS = record
    dwSize: DWORD;
    // Request field - bits of status to verify
    StatusMask: DWORD;
    //
    // Fields are set when status mask contains STI_DEVSTATUS_ONLINE_STATE bit set
    //
    // Bitmask describing  device state
    dwOnlineState: DWORD;
    // Device status code as defined by vendor
    dwHardwareStatusCode: DWORD;
    //
    // Fields are set when status mask contains STI_DEVSTATUS_EVENTS_STATE bit set
    //
    // State of device notification processing (enabled, pending)
    dwEventHandlingState: DWORD;
    // If device is polled, polling interval in ms
    dwPollingInterval: DWORD;
  end;
  STI_DEVICE_STATUS = _STI_DEVICE_STATUS;
  PSTI_DEVICE_STATUS = ^_STI_DEVICE_STATUS;

  //
  // Structure to describe diagnostic ( test ) request to be processed by USD
  //

  // Basic test for presence of associated hardware
const
  STI_DIAGCODE_HWPRESENCE = $00000001;

  //
  // Status bits for diagnostic
  //

  //
  // generic diagnostic errors
  //
type
  _ERROR_INFOW = record
    dwSize: DWORD;
    // Generic error , describing results of last operation
    dwGenericError: DWORD;
    // vendor specific error code
    dwVendorError: DWORD;
    // String, describing in more details results of last operation if it failed
    szExtendedErrorText: array[0..254] of WideChar;
  end;
  STI_ERROR_INFOW = _ERROR_INFOW;
  PSTI_ERROR_INFOW = ^_ERROR_INFOW;

  _ERROR_INFOA = record
    dwSize: DWORD;
    dwGenericError: DWORD;
    dwVendorError: DWORD;
    szExtendedErrorText: array[0..254] of Char;
  end;
  STI_ERROR_INFOA = _ERROR_INFOA;
  PSTI_ERROR_INFOA = ^_ERROR_INFOA;

{$IFDEF UNICODE}
  STI_ERROR_INFO = STI_ERROR_INFOW;
{$ELSE}
  STI_ERROR_INFO = STI_ERROR_INFOA;
{$ENDIF}
  PSTI_ERROR_INFO = ^STI_ERROR_INFO;

  _STI_DIAG = record
    dwSize: DWORD;
    // Diagnostic request fields. Are set on request by caller
    // One of the
    dwBasicDiagCode: DWORD;
    dwVendorDiagCode: DWORD;
    // Response fields
    dwStatusMask: DWORD;
    sErrorInfo: STI_ERROR_INFO;
  end;
  STI_DIAG = _STI_DIAG;
  PSTI_DIAG = ^_STI_DIAG;
  LPSTI_DIAG = ^_STI_DIAG;

  DIAG = STI_DIAG;
  PDIAG = ^STI_DIAG;
  LPDIAG = ^STI_DIAG;

  // end device state information.

  // Flags passed to WriteToErrorLog call in a first parameter, indicating type of the message
  // which needs to be logged
const
  STI_TRACE_INFORMATION = $00000001;
  STI_TRACE_WARNING = $00000002;
  STI_TRACE_ERROR = $00000004;

  //
  // Event notification mechansims.
  // ------------------------------
  //
  // Those are used to inform last subscribed caller of the changes in device state, initiated by
  // device.
  //
  // The only supported discipline of notification is stack. The last caller to subscribe will be notified
  // and will receive notification data. After caller unsubscribes , the previously subscribed caller will
  // become active.
  //

  // Notifications are sent to subscriber via window message. Window handle is passed as
  // parameter
  STI_SUBSCRIBE_FLAG_WINDOW = $0001;

  // Device notification is signalling Win32 event ( auto-set event). Event handle
  // is passed as a parameter
  STI_SUBSCRIBE_FLAG_EVENT = $0002;

type
  _STISUBSCRIBE = record
    dwSize: DWORD;
    dwFlags: DWORD;
    // Not used . Will be used for subscriber to set bit mask filtering different events
    dwFilter: DWORD;
    // When STI_SUBSCRIBE_FLAG_WINDOW bit is set, following fields should be set
    // Handle of the window which will receive notification message
    hWndNotify: HWND;
    // Handle of Win32 auto-reset event , which will be signalled whenever device has
    // notification pending
    hEvent: THandle;
    // Code of notification message, sent to window
    uiNotificationMessage: UINT;
  end;
  STISUBSCRIBE = _STISUBSCRIBE;
  PSTISUBSCRIBE = ^_STISUBSCRIBE;
  LPSTISUBSCRIBE = ^_STISUBSCRIBE;

const
  MAX_NOTIFICATION_DATA = 64;

  // Structure to describe notification information
type
  _STINOTIFY = record
    dwSize: DWORD;                 // Total size of the notification structure
    // GUID of the notification being retrieved
    guidNotificationCode: TGUID;
    // Vendor specific notification description
    abNotificationData: array[0..MAX_NOTIFICATION_DATA -1] of Byte;     // USD specific
  end;
  STINOTIFY = _STINOTIFY;
  PSTINOTIFY = ^_STINOTIFY;
  LPSTINOTIFY = ^_STINOTIFY;

  // end event_mechanisms

  //
  // STI device broadcasting
  //

  //
  // When STI Device is being added or removed, PnP broadacst is being sent , but it is not obvious
  // for application code to recognize if it is STI device and if so, what is the name of the
  // device. STI subsystem will analyze PnP broadcasts and rebroadcast another message via
  // BroadcastSystemMessage / WM_DEVICECHANGE / DBT_USERDEFINED .

  // String passed as user defined message contains STI prefix, action and device name
const
  STI_ADD_DEVICE_BROADCAST_ACTION = 'Arrival';
  STI_REMOVE_DEVICE_BROADCAST_ACTION = 'Removal';

  STI_ADD_DEVICE_BROADCAST_STRING = 'STI\\' +  STI_ADD_DEVICE_BROADCAST_ACTION + '\\%s';
  STI_REMOVE_DEVICE_BROADCAST_STRING = 'STI\\' + STI_REMOVE_DEVICE_BROADCAST_ACTION + '\\%s';

  // end STI broadcasting

  //
  // Device create modes
  //

  // Device is being opened only for status querying and notifications receiving
const
  STI_DEVICE_CREATE_STATUS = $00000001;

  // Device is being opened for data transfer ( supersedes status mode)
  STI_DEVICE_CREATE_DATA = $00000002;
  STI_DEVICE_CREATE_BOTH = $00000003;

  // Bit mask for legitimate mode bits, which can be used when calling CreateDevice
  STI_DEVICE_CREATE_MASK = $0000FFFF;

  // Flags controlling device enumeration
  STIEDFL_ALLDEVICES = $00000000;
  STIEDFL_ATTACHEDONLY = $00000001;

  // Control code , sent to the device through raw control interface
type
  STI_RAW_CONTROL_CODE = DWORD;

  // All raw codes below this one are reserved for future use.
const
  STI_RAW_RESERVED = $1000;

  STIDLL = 'sti.dll';

type
  // DCoder FIXME LPSUBSCRIBE seems to be a callback function
  LPSUBSCRIBE = Pointer;

  IStiDevice = interface(IUnknown)
  ['{6CFA5A80-2DC8-11D0-90EA-00AA0060F86C}']
    function Initialize(hinst: HINST; pwszDeviceName: PWideChar; dwVersion: DWORD; dwMode: DWORD): HRESULT; stdcall;
    function GetCapabilities(pDevCaps: PSTI_DEV_CAPS): HRESULT; stdcall;
    function GetStatus(pDevStatus: PSTI_DEVICE_STATUS): HRESULT; stdcall;
    function DeviceReset: HRESULT; stdcall;
    function Diagnostic(pBuffer: LPSTI_DIAG): HRESULT; stdcall;
    function Escape(EscapeFunction: STI_RAW_CONTROL_CODE; lpInData: Pointer; cbInDataSize: DWORD; pOutData: Pointer; dwOutDataSize: DWORD; out pdwActualData: DWORD): HRESULT; stdcall;
    function GetLastError(out pdwLastDeviceError: DWORD): HRESULT; stdcall;
    function LockDevice(dwTimeOut: DWORD): HRESULT; stdcall;
    function UnLockDevice: HRESULT; stdcall;
    function RawReadData(lpBuffer: Pointer; lpdwNumberOfBytes: PDWORD; lpOverlapped: PDWORD): HRESULT; stdcall;
    function RawWriteData(lpBuffer: Pointer; nNumberOfBytes: DWORD; lpOverlapped: PDWORD): HRESULT; stdcall;
    function RawReadCommand(lpBuffer: Pointer; lpdwNumberOfBytes: PDWORD; lpOverlapped: PDWORD): HRESULT; stdcall;
    function RawWriteCommand(lpBuffer: Pointer; nNumberOfBytes: DWORD; lpOverlapped: PDWORD): HRESULT; stdcall;
    // Subscription is used to enable "control center" style applications , where flow of
    // notifications should be redirected from monitor itself to another "launcher"
    function Subscribe(lpSubsribe: LPSTISUBSCRIBE): HRESULT; stdcall;
    function GetLastNotificationData(lpNotify: LPSTINOTIFY): HRESULT; stdcall;
    function UnSubscribe: HRESULT; stdcall;
    function GetLastErrorInfo(out pLastErrorInfo: STI_ERROR_INFO): HRESULT; stdcall;
  end;

  IStillImageW = interface(IUnknown)
  ['{641BD880-2DC8-11D0-90EA-00AA0060F86C}']
    function Initialize(hinst: HINST; dwVersion: DWORD): HRESULT; stdcall;
    function GetDeviceList(dwType: DWORD; dwFlags: DWORD; out dwItemsReturned: DWORD; out pBuffer: PByte): HRESULT; stdcall;
    function GetDeviceInfo(pwszDeviceName: PWideChar; out pBuffer: PByte): HRESULT; stdcall;
    function CreateDevice(pwszDeviceName: PWideChar; dwMode: DWORD; out pDevice: IStiDevice; punkOuter: IUnknown): HRESULT; stdcall;
    // Device instance values. Used to associate various data with device.
    function GetDeviceValue(pwszDeviceName: PWideChar; pValueName: PWideChar; out pType: DWORD; pData: PByte; out cbData: DWORD): HRESULT; stdcall;
    function SetDeviceValue(pwszDeviceName: PWideChar; pValueName: PWideChar; _Type: DWORD; pData: PByte; cbData: DWORD): HRESULT; stdcall;
    // For appllication started through push model launch, returns associated information
    function GetSTILaunchInformation(pwszDeviceName: PWideChar; out pdwEventCode: DWORD; pwszEventName: PWideChar): HRESULT; stdcall;
    function RegisterLaunchApplication(pwszAppName: PWideChar; pwszCommandLine: PWideChar): HRESULT; stdcall;
    function UnregisterLaunchApplication(pwszAppName: PWideChar): HRESULT; stdcall;
    // To control state of notification handling. For polled devices this means state of monitor
    // polling, for true notification devices means enabling/disabling notification flow
    // from monitor to registered applications
    function EnableHwNotifications(pwszDeviceName: PWideChar; bNewState: BOOL): HRESULT; stdcall;
    function GetHwNotificationState(pwszDeviceName: PWideChar; out pbCurrentState: BOOL): HRESULT; stdcall;
    // When device is installed but not accessible, application may request bus refresh
    // which in some cases will make device known. This is mainly used for nonPnP buses
    // like SCSI, when device was powered on after PnP enumeration
    function RefreshDeviceBus(pwszDeviceName: PWideChar): HRESULT; stdcall;
    // Launch application to emulate event on a device. Used by "control center" style components,
    // which intercept device event , analyze and later force launch based on certain criteria.
    function LaunchApplicationForDevice(pwszDeviceName: PWideChar; pwszAppName: PWideChar; pStiNotify: LPSTINOTIFY): HRESULT; stdcall;
    // For non-PnP devices with non-known bus type connection, setup extension, associated with the
    // device can set it's parameters
    function SetupDeviceParameters(device_information: PSTI_DEVICE_INFORMATIONW): HRESULT; stdcall;
    // Write message to STI error log
    function WriteToErrorLog(dwMessageType: DWORD; pszMessage: PWideChar): HRESULT; stdcall;
  {$IFDEF NOT_IMPLEMENTED}
    // TO register application for receiving various STI notifications
    function RegisterDeviceNotification(pwszAppName: PWideChar; lpSubscribe: LPSUBSCRIBE): HRESULT; stdcall;
    function UnregisterDeviceNotification: HRESULT; stdcall;
  {$ENDIF}
  end;

  IStillImageA = interface(IUnknown)
  ['{A7B1F740-1D7F-11D1-ACA9-00A02438AD48}']
    function Initialize(hinst: HINST; dwVersion: DWORD): HRESULT; stdcall;
    function GetDeviceList(dwType: DWORD; dwFlags: DWORD; out dwItemsReturned: DWORD; out pBuffer: PByte): HRESULT; stdcall;
    function GetDeviceInfo(pwszDeviceName: PChar; out pBuffer: PByte): HRESULT; stdcall;
    function CreateDevice(pwszDeviceName: PChar; dwMode: DWORD; out pDevice: IStiDevice; punkOuter: IUnknown): HRESULT; stdcall;
    // Device instance values. Used to associate various data with device.
    function GetDeviceValue(pwszDeviceName: PChar; pValueName: PChar; out pType: DWORD; pData: PByte; out cbData: DWORD): HRESULT; stdcall;
    function SetDeviceValue(pwszDeviceName: PChar; pValueName: PChar; _Type: DWORD; pData: PByte; cbData: DWORD): HRESULT; stdcall;
    // For appllication started through push model launch, returns associated information
    function GetSTILaunchInformation(pwszDeviceName: PChar; out pdwEventCode: DWORD; pwszEventName: PChar): HRESULT; stdcall;
    function RegisterLaunchApplication(pwszAppName: PChar; pwszCommandLine: PChar): HRESULT; stdcall;
    function UnregisterLaunchApplication(pwszAppName: PChar): HRESULT; stdcall;
    // To control state of notification handling. For polled devices this means state of monitor
    // polling, for true notification devices means enabling/disabling notification flow
    // from monitor to registered applications
    function EnableHwNotifications(pwszDeviceName: PChar; bNewState: BOOL): HRESULT; stdcall;
    function GetHwNotificationState(pwszDeviceName: PChar; out pbCurrentState: BOOL): HRESULT; stdcall;
    // When device is installed but not accessible, application may request bus refresh
    // which in some cases will make device known. This is mainly used for nonPnP buses
    // like SCSI, when device was powered on after PnP enumeration
    function RefreshDeviceBus(pwszDeviceName: PChar): HRESULT; stdcall;
    // Launch application to emulate event on a device. Used by "control center" style components,
    // which intercept device event , analyze and later force launch based on certain criteria.
    function LaunchApplicationForDevice(pwszDeviceName: PChar; pwszAppName: PChar; pStiNotify: LPSTINOTIFY): HRESULT; stdcall;
    // For non-PnP devices with non-known bus type connection, setup extension, associated with the
    // device can set it's parameters
    function SetupDeviceParameters(device_information: PSTI_DEVICE_INFORMATIONW): HRESULT; stdcall;
    // Write message to STI error log
    function WriteToErrorLog(dwMessageType: DWORD; pszMessage: PChar): HRESULT; stdcall;
  {$IFDEF NOT_IMPLEMENTED}
    // TO register application for receiving various STI notifications
    function RegisterDeviceNotification(pwszAppName: PChar; lpSubscribe: LPSUBSCRIBE): HRESULT; stdcall;
    function UnregisterDeviceNotification: HRESULT; stdcall;
  {$ENDIF}
  end;

  function StiCreateInstanceW(hinst: HINST; dwVer: DWORD; out sti: IStillImageW; punkOuter: IUnknown): HRESULT; stdcall; external STIDLL;
  function StiCreateInstanceA(hinst: HINST; dwVer: DWORD; out sti: IStillImageA; punkOuter: IUnknown): HRESULT; stdcall; external STIDLL;

const
{$IFDEF UNICODE}
  IID_IStillImage: TGuid = '{641BD880-2DC8-11D0-90EA-00AA0060F86C}';
  function StiCreateInstance(hinst: HINST; dwVer: DWORD; out sti: IStillImageW; punkOuter: IUnknown): HRESULT; stdcall; external STIDLL name 'StiCreateInstanceW';
type
  IStillImage = IStillImageW;
{$ELSE}
  IID_IStillImage: TGuid = '{A7B1F740-1D7F-11D1-ACA9-00A02438AD48}';
  function StiCreateInstance(hinst: HINST; dwVer: DWORD; out sti: IStillImageA; punkOuter: IUnknown): HRESULT; stdcall; external STIDLL name 'StiCreateInstanceA';
type
  IStillImage = IStillImageA;
{$ENDIF}

implementation

function GET_STIVER_MAJOR(dwVersion: Cardinal): Cardinal;
begin
  Result := HIWORD(dwVersion) and not STI_VERSION_FLAG_MASK;
end;

function GET_STIVER_MINOR(dwVersion: Cardinal): Cardinal;
begin
  Result := LOWORD(dwVersion);
end;

function GET_STIDEVICE_TYPE(dwDevType: Cardinal): Cardinal;
begin
  Result := HIWORD(dwDevType);
end;

function GET_STIDEVICE_SUBTYPE(dwDevType: Cardinal): Cardinal;
begin
  Result := LOWORD(dwDevType);
end;

function GET_STIDCOMMON_CAPS(dwGenericCaps: Cardinal): Cardinal;
begin
  Result := HIWORD(dwGenericCaps);
end;

function GET_STIVENDOR_CAPS(dwGenericCaps: Cardinal): Cardinal;
begin
  Result := LOWORD(dwGenericCaps);
end;

end.
