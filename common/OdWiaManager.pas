unit OdWiaManager;

interface

uses Forms, Windows, SysUtils, Classes, WIALib_TLB, Jpeg, Graphics;

Type
  TFileTransfer = procedure(FileName: TFileName) of object;

  TWiaManager = class(TComponent)
  private
    FWia: TWia;
    FOnDeviceConnected: TNotifyEvent;
    FOnDeviceDisconnected: TNotifyEvent;
    FAfterFileTransfer: TFileTransfer;
    FConnected: Boolean;
    FFileName: TFileName;
    procedure InternalDeviceConnected(Sender: TObject; var Id: OleVariant);
    procedure InternalDeviceDisconnected(Sender: TObject; var Id: OleVariant);
    procedure InternalTransferComplete(Sender: TObject; var Item, Path: OleVariant);
    function GetHasDeviceConnected: Boolean;
    procedure ConvertBMPToJPEG;
  public
    property HasDeviceConnected: Boolean read GetHasDeviceConnected;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure TransferFilesFromDevice(ToDirectory: string = '');
    procedure Connect;
    procedure TakePicture;
  published
    property OnDeviceConnected: TNotifyEvent read FOnDeviceConnected write FOnDeviceConnected;
    property OnDeviceDisconnected: TNotifyEvent read FOnDeviceDisconnected write FOnDeviceDisconnected;
    property AfterFileTransfer: TFileTransfer read FAfterFileTransfer write FAfterFileTransfer;
    property Connected: Boolean read FConnected write FConnected;
  end;

var
  WiaManager: TWiaManager = nil;

procedure InitializeWIA;
procedure FinalizeWIA;

implementation

uses OdMiscUtils, Dialogs;

{ TWIAManager }

constructor TWIAManager.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  FWia := TWIA.Create(nil);
  FWia.OnDeviceConnected := InternalDeviceConnected;
  FWia.OnDeviceDisconnected := InternalDeviceDisconnected;
  FWia.OnTransferComplete := InternalTransferComplete;
end;

destructor TWIAManager.Destroy;
begin
  FreeAndNil(FWia);
  inherited Destroy;
end;

function TWiaManager.GetHasDeviceConnected: Boolean;
begin
  if Connected then
    Result := FWia.Devices.Count > 0
  else
    Result := False;
end;

procedure TWIAManager.InternalDeviceConnected(Sender: TObject; var Id: OleVariant);
begin
  if Assigned(OnDeviceConnected) then
    OnDeviceConnected(Sender);
end;

procedure TWIAManager.InternalDeviceDisconnected(Sender: TObject; var Id: OleVariant);
begin
  if Assigned(OnDeviceDisconnected) then
    OnDeviceDisconnected(Sender);
end;

procedure TWiaManager.InternalTransferComplete(Sender: TObject; var Item,
  Path: OleVariant);
begin
  ConvertBMPToJPEG;
  DeleteFile(FFileName);
  FFileName := ChangeFileExt(FFileName, '.jpg');
  if Assigned(AfterFileTransfer) then
    AfterFileTransfer(FFileName);
end;

procedure TWiaManager.Connect;
begin
  FWia.Connect;
  Connected := True;
end;

procedure TWiaManager.TransferFilesFromDevice(ToDirectory: string);
var
  aDevice: DeviceInfo;
  aItem: Item;
  Image: Item;
  ImageList: Collection;
  i: Integer;
begin
  if not HasDeviceConnected then
    raise Exception.Create('There is no device ready for file transfer.');

  if IsEmpty(ToDirectory) then
    ToDirectory := GetWindowsTempPath;

  // Always use the first device on the list
  aDevice := DeviceInfo(FWia.Devices[0]);
  aItem := aDevice.Create;
  ImageList := Collection(aItem.GetItemsFromUI(UseCommonUI, BestPreview));

  if not (ImageList = nil) then
    for i := 0 to ImageList.Count - 1 do begin
      Image := Item(ImageList.Item[i]);
      FFileName := AddSlash(ToDirectory)+Image.Name+'.bmp';
      // Do a syncronous transfer
      Image.Transfer(FFileName, False);
      // Prevent a strange AV after a file transfer
      Application.ProcessMessages;
    end;
end;

procedure TWiaManager.TakePicture;
var
  aDevice: DeviceInfo;
  aItem: Item;
  Image: Item;
begin
  if not HasDeviceConnected then begin
    raise Exception.Create('There is no digital camera connected to this computer.');
  end;
  aDevice := DeviceInfo(FWia.Devices[0]);
  aItem := Item(aDevice.Create);
  Image := Item(aItem.TakePicture);
  FFileName := ExtractFilePath(Application.ExeName)+Image.Name+'.bmp';
  if FileExists(FFileName) then
    DeleteFile(FFileName);
  Application.ProcessMessages;
  Image.Transfer(FFileName, False);
  Application.ProcessMessages;
end;

procedure TWiaManager.ConvertBMPToJPEG;
var
   JpgImage: TJPEGImage;
   Bitmap: TBitmap;
   FJpgFileName: TFileName;
begin
  if (FileExists (FFileName)) then begin
    FJpgFileName := ChangeFileExt(FFileName, '.jpg');
    Bitmap := TBitmap.Create;
    JpgImage := TJPEGImage.Create;
    try
      BitMap.LoadFromFile(FFileName);
      JpgImage.Assign(Bitmap);
      JpgImage.Grayscale := False;
      JpgImage.Scale := jsFullSize;
      JpgImage.PixelFormat := jf24Bit;
      JpgImage.CompressionQuality := 90;
      JpgImage.DIBNeeded;
      JpgImage.Compress;
      JpgImage.SaveToFile(LowerCase(FJpgFileName));
    finally
      FreeAndNil(JpgImage);
      FreeAndNil(Bitmap);
    end;
  end;
end;

procedure InitializeWIA;
begin
  if IsWindowsVista then
    Exit; // Vista does not support this GUID
  WiaManager := TWiaManager.Create(nil);
  try
    WiaManager.Connect;
  except
    on E: Exception do begin
      E.Message := 'Windows Imaging Architecture Error in TWiaManager.Connect: ' + E.Message;
      ShowExceptionError(E);
    end;
  end;
end;

procedure FinalizeWIA;
begin
  FreeAndNil(WiaManager);
end;

end.
