unit WIALib_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// PASTLWTR : $Revision$
// File generated on 30/3/2007 09:50:49 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\WINDOWS\SYSTEM32\wiascr.dll (1)
// LIBID: {95CEDD63-2E34-4B84-9FB3-F86AF1D4BF7A}
// LCID: 0
// Helpfile:
// DepndLst:
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// Errors:
//   Hint: Member 'Type' of 'IWiaDeviceInfo' changed to 'Type_'
//   Error creating palette bitmap of (TWia) : Server C:\WINDOWS\system32\wiascr.dll contains no icons
//   Error creating palette bitmap of (TSafeWia) : Server C:\WINDOWS\system32\wiascr.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties
// which return objects that may need to be explicitly created via a function
// call prior to any access via the property. These items have been disabled
// in order to prevent accidental use from within the object inspector. You
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively
// removing them from the $IFDEF blocks. However, such items must still be
// programmatically created via a method of the appropriate CoClass before
// they can be used.
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, OleServer, StdVCL, Variants, Windows;



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  WIALibMajorVersion = 1;
  WIALibMinorVersion = 0;

  LIBID_WIALib: TGUID = '{95CEDD63-2E34-4B84-9FB3-F86AF1D4BF7A}';

  CLASS_WiaProtocol: TGUID = '{13F3EA8B-91D7-4F0A-AD76-D2853AC8BECE}';
  IID_ICollection: TGUID = '{C34C8CE7-B253-4F89-AA25-8A24AD71D0C0}';
  IID_IWiaDispatchItem: TGUID = '{D06D503F-4B71-40F3-94A7-66478F732BC9}';
  CLASS_Item: TGUID = '{E6C45109-442C-4585-BE44-F5D2884E544A}';
  IID_IWiaDeviceInfo: TGUID = '{5267FF5E-7CAF-4769-865D-17A25968525E}';
  CLASS_DeviceInfo: TGUID = '{34E1C006-99D2-4335-B0B1-CE7B9FE5396C}';
  CLASS_Collection: TGUID = '{6E27C0E7-1D45-4CA7-9BF7-BD6CDDAA1ADC}';
  IID_IWia: TGUID = '{B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}';
  DIID__IWiaEvents: TGUID = '{E5F04D72-6C16-42E2-BCCA-F8D0DB4ADE06}';
  CLASS_Wia: TGUID = '{4EC4272E-2E6F-4EEB-91D0-EBC4D58E8DEE}';
  CLASS_SafeWia: TGUID = '{0DAD5531-BF31-43AC-A513-1F8926BBF5EC}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library
// *********************************************************************//
// Constants for enum WiaIntent
type
  WiaIntent = TOleEnum;
const
  ImageTypeColor = $00000001;
  ImageTypeGrayscale = $00000002;
  ImageTypeText = $00000004;
  MinimizeSize = $00010000;
  MaximizeQuality = $00020000;
  BestPreview = $00040000;

// Constants for enum WiaFlag
type
  WiaFlag = TOleEnum;
const
  SingleImage = $00000002;
  UseCommonUI = $00000004;

// Constants for enum WiaDeviceInfoPropertyId
type
  WiaDeviceInfoPropertyId = TOleEnum;
const
  DeviceInfoDevId = $00000002;
  DeviceInfoVendDesc = $00000003;
  DeviceInfoDevDesc = $00000004;
  DeviceInfoDevType = $00000005;
  DeviceInfoPortName = $00000006;
  DeviceInfoDevName = $00000007;
  DeviceInfoServerName = $00000008;
  DeviceInfoRemoteDevId = $00000009;
  DeviceInfoUIClsid = $0000000A;
  DeviceInfoHwConfig = $0000000B;
  DeviceInfoBaudRate = $0000000C;
  DeviceInfoStiGenCapabilities = $0000000D;
  DeviceInfoWiaVersion = $0000000E;
  DeviceInfoDriverVersion = $0000000F;

// Constants for enum WiaItemPropertyId
type
  WiaItemPropertyId = TOleEnum;
const
  DeviceFirmwareVersion = $00000402;
  DeviceConnectStatus = $00000403;
  DeviceDeviceTime = $00000404;
  CameraDevicePicturesTaken = $00000802;
  CameraDevicePicturesRemaining = $00000803;
  CameraDeviceExposureMode = $00000804;
  CameraDeviceExposureComp = $00000805;
  CameraDeviceExposureTime = $00000806;
  CameraDeviceFNumber = $00000807;
  CameraDeviceFlashMode = $00000808;
  CameraDeviceFocusMode = $00000809;
  CameraDevicePanPosition = $0000080C;
  CameraDeviceTiltPosition = $0000080D;
  CameraDeviceTimerMode = $0000080E;
  CameraDeviceTimerValue = $0000080F;
  CameraDevicePowerMode = $00000810;
  CameraDeviceBatteryStatus = $00000811;
  CameraDeviceThumbWidth = $00000812;
  CameraDeviceThumbHeight = $00000813;
  CameraDevicePictWidth = $00000814;
  CameraDevicePictHeight = $00000815;
  CameraDeviceCompressionSetting = $00000817;
  CameraDeviceTimelapseInterval = $00000819;
  CameraDeviceTimelapseNumber = $0000081A;
  CameraDeviceBurstInterval = $0000081B;
  CameraDeviceBurstNumber = $0000081C;
  CameraDeviceEffectMode = $0000081D;
  CameraDeviceDigitalZoom = $0000081E;
  CameraDeviceSharpness = $0000081F;
  CameraDeviceContrast = $00000820;
  CameraDeviceCaptureMode = $00000821;
  CameraDeviceCaptureDelay = $00000822;
  CameraDeviceExposureIndex = $00000823;
  CameraDeviceExposureMeteringMode = $00000824;
  CameraDeviceFocusMeteringMode = $00000825;
  CameraDeviceFocusDistance = $00000826;
  CameraDeviceFocalLength = $00000827;
  CameraDeviceRGBGain = $00000828;
  CameraDeviceWhiteBalance = $00000829;
  CameraDeviceUploadURL = $0000082A;
  CameraDeviceArtist = $0000082B;
  CameraDeviceCopyrightInfo = $0000082C;
  ScannerDeviceHorizontalBedSize = $00000C02;
  ScannerDeviceVerticalBedSize = $00000C03;
  ScannerDeviceHorizontalSheetFeedSize = $00000C04;
  ScannerDeviceVerticalSheetFeedSize = $00000C05;
  ScannerDeviceSheetFeederRegistration = $00000C06;
  ScannerDeviceHorizontalBedRegistration = $00000C07;
  ScannerDeviceVerticalBedRegistration = $00000C08;
  ScannerDevicePlatenColor = $00000C09;
  ScannerDevicePadColor = $00000C0A;
  ScannerDeviceDocumentHandlingCapabilities = $00000C0E;
  ScannerDeviceDocumentHandlingStatus = $00000C0F;
  ScannerDeviceDocumentHandlingSelect = $00000C10;
  ScannerDeviceDocumentHandlingCapacity = $00000C11;
  ScannerDeviceOpticalXres = $00000C12;
  ScannerDeviceOpticalYres = $00000C13;
  ScannerDeviceEndorserCharacters = $00000C14;
  ScannerDeviceEndorserString = $00000C15;
  ScannerDeviceScanAheadPages = $00000C16;
  ScannerDeviceMaxScanTime = $00000C17;
  ScannerDevicePages = $00000C18;
  ScannerDevicePageSize = $00000C19;
  ScannerDevicePageWidth = $00000C1A;
  ScannerDevicePageHeight = $00000C1B;
  ScannerDevicePreview = $00000C1C;
  ScannerDeviceTransparency = $00000C1D;
  ScannerDeviceTransparencySelect = $00000C1E;
  ScannerDeviceShowPreviewControl = $00000C1F;
  ScannerDeviceMinHorizontalSheetFeedSize = $00000C20;
  ScannerDeviceMinVerticalSheetFeedSize = $00000C21;
  FileDeviceMountPoint = $00000D02;
  VideoDeviceLastPictureTaken = $00000E02;
  VideoDeviceImagesDirectory = $00000E03;
  VideoDeviceDShowDevicePath = $00000E04;
  PictureItemName = $00001002;
  PictureFullItemName = $00001003;
  PictureItemTime = $00001004;
  PictureItemFlags = $00001005;
  PictureAccessRights = $00001006;
  PictureDatatype = $00001007;
  PictureDepth = $00001008;
  PicturePreferredFormat = $00001009;
  PictureFormat = $0000100A;
  PictureCompression = $0000100B;
  PictureTymed = $0000100C;
  PictureChannelsPerPixel = $0000100D;
  PictureBitsPerChannel = $0000100E;
  PicturePlanar = $0000100F;
  PicturePixelsPerLine = $00001010;
  PictureBytesPerLine = $00001011;
  PictureNumberOfLines = $00001012;
  PictureGammaCurves = $00001013;
  PictureItemSize = $00001014;
  PictureColorProfile = $00001015;
  PictureMinBufferSize = $00001016;
  PictureBufferSize = $00001016;
  PictureRegionType = $00001017;
  PictureIcmProfileName = $00001018;
  PictureAppColorMapping = $00001019;
  PicturePropStreamCompatId = $0000101A;
  PictureFilenameExtension = $0000101B;
  PictureSuppressPropertyPage = $0000101C;
  CameraPictureThumbnail = $00001402;
  CameraPictureThumbWidth = $00001403;
  CameraPictureThumbHeight = $00001404;
  CameraPictureAudioAvailable = $00001405;
  CameraPictureAudioDataFormat = $00001406;
  CameraPictureAudioData = $00001407;
  CameraPictureNumPictPerRow = $00001408;
  CameraPictureSequence = $00001409;
  CameraPictureTimedelay = $0000140A;
  ScannerPictureCurIntent = $00001802;
  ScannerPictureXres = $00001803;
  ScannerPictureYres = $00001804;
  ScannerPictureXpos = $00001805;
  ScannerPictureYpos = $00001806;
  ScannerPictureXextent = $00001807;
  ScannerPictureYextent = $00001808;
  ScannerPicturePhotometricInterp = $00001809;
  ScannerPictureBrightness = $0000180A;
  ScannerPictureContrast = $0000180B;
  ScannerPictureOrientation = $0000180C;
  ScannerPictureRotation = $0000180D;
  ScannerPictureMirror = $0000180E;
  ScannerPictureThreshold = $0000180F;
  ScannerPictureInvert = $00001810;
  ScannerPictureWarmUpTime = $00001811;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ICollection = interface;
  ICollectionDisp = dispinterface;
  IWiaDispatchItem = interface;
  IWiaDispatchItemDisp = dispinterface;
  IWiaDeviceInfo = interface;
  IWiaDeviceInfoDisp = dispinterface;
  IWia = interface;
  IWiaDisp = dispinterface;
  _IWiaEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  Item = IWiaDispatchItem;
  DeviceInfo = IWiaDeviceInfo;
  Collection = ICollection;
  Wia = IWia;
  SafeWia = IWia;


// *********************************************************************//
// Declaration of structures, unions and aliases.
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}


// *********************************************************************//
// Interface: ICollection
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C34C8CE7-B253-4F89-AA25-8A24AD71D0C0}
// *********************************************************************//
  ICollection = interface(IDispatch)
    ['{C34C8CE7-B253-4F89-AA25-8A24AD71D0C0}']
    function  Get_Item(Index: Integer): IDispatch; safecall;
    function  Get_Count: Integer; safecall;
    function  Get_Length: LongWord; safecall;
    function  Get__NewEnum: IUnknown; safecall;
    property Item[Index: Integer]: IDispatch read Get_Item; default;
    property Count: Integer read Get_Count;
    property Length: LongWord read Get_Length;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  ICollectionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C34C8CE7-B253-4F89-AA25-8A24AD71D0C0}
// *********************************************************************//
  ICollectionDisp = dispinterface
    ['{C34C8CE7-B253-4F89-AA25-8A24AD71D0C0}']
    property Item[Index: Integer]: IDispatch readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    property Length: LongWord readonly dispid 2;
    property _NewEnum: IUnknown readonly dispid -4;
  end;

// *********************************************************************//
// Interface: IWiaDispatchItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D06D503F-4B71-40F3-94A7-66478F732BC9}
// *********************************************************************//
  IWiaDispatchItem = interface(IDispatch)
    ['{D06D503F-4B71-40F3-94A7-66478F732BC9}']
    function  Get_ConnectStatus: WideString; safecall;
    function  Get_Time: WideString; safecall;
    function  Get_FirmwareVersion: WideString; safecall;
    function  GetItemsFromUI(Flags: WiaFlag; Intent: WiaIntent): ICollection; safecall;
    procedure Transfer(const Filename: WideString; AsyncTransfer: WordBool); safecall;
    function  TakePicture: IWiaDispatchItem; safecall;
    function  Get_Name: WideString; safecall;
    function  Get_FullName: WideString; safecall;
    function  Get_ItemType: WideString; safecall;
    function  Get_Width: Integer; safecall;
    function  Get_Height: Integer; safecall;
    function  Get_ThumbWidth: Integer; safecall;
    function  Get_ThumbHeight: Integer; safecall;
    function  Get_Thumbnail: WideString; safecall;
    function  Get_PictureHeight: Integer; safecall;
    function  Get_PictureWidth: Integer; safecall;
    function  Get_Children: ICollection; safecall;
    function  GetPropById(Id: WiaItemPropertyId): OleVariant; safecall;
    property ConnectStatus: WideString read Get_ConnectStatus;
    property Time: WideString read Get_Time;
    property FirmwareVersion: WideString read Get_FirmwareVersion;
    property Name: WideString read Get_Name;
    property FullName: WideString read Get_FullName;
    property ItemType: WideString read Get_ItemType;
    property Width: Integer read Get_Width;
    property Height: Integer read Get_Height;
    property ThumbWidth: Integer read Get_ThumbWidth;
    property ThumbHeight: Integer read Get_ThumbHeight;
    property Thumbnail: WideString read Get_Thumbnail;
    property PictureHeight: Integer read Get_PictureHeight;
    property PictureWidth: Integer read Get_PictureWidth;
    property Children: ICollection read Get_Children;
  end;

// *********************************************************************//
// DispIntf:  IWiaDispatchItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D06D503F-4B71-40F3-94A7-66478F732BC9}
// *********************************************************************//
  IWiaDispatchItemDisp = dispinterface
    ['{D06D503F-4B71-40F3-94A7-66478F732BC9}']
    property ConnectStatus: WideString readonly dispid 1;
    property Time: WideString readonly dispid 2;
    property FirmwareVersion: WideString readonly dispid 3;
    function  GetItemsFromUI(Flags: WiaFlag; Intent: WiaIntent): ICollection; dispid 4;
    procedure Transfer(const Filename: WideString; AsyncTransfer: WordBool); dispid 5;
    function  TakePicture: IWiaDispatchItem; dispid 6;
    property Name: WideString readonly dispid 20;
    property FullName: WideString readonly dispid 21;
    property ItemType: WideString readonly dispid 22;
    property Width: Integer readonly dispid 23;
    property Height: Integer readonly dispid 24;
    property ThumbWidth: Integer readonly dispid 50;
    property ThumbHeight: Integer readonly dispid 51;
    property Thumbnail: WideString readonly dispid 52;
    property PictureHeight: Integer readonly dispid 53;
    property PictureWidth: Integer readonly dispid 54;
    property Children: ICollection readonly dispid 500;
    function  GetPropById(Id: WiaItemPropertyId): OleVariant; dispid 1000;
  end;

// *********************************************************************//
// Interface: IWiaDeviceInfo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5267FF5E-7CAF-4769-865D-17A25968525E}
// *********************************************************************//
  IWiaDeviceInfo = interface(IDispatch)
    ['{5267FF5E-7CAF-4769-865D-17A25968525E}']
    function  Create: IWiaDispatchItem; safecall;
    function  Get_Id: WideString; safecall;
    function  Get_Name: WideString; safecall;
    function  Get_Type_: WideString; safecall;
    function  Get_Port: WideString; safecall;
    function  Get_UIClsid: WideString; safecall;
    function  Get_Manufacturer: WideString; safecall;
    function  GetPropById(Id: WiaDeviceInfoPropertyId): OleVariant; safecall;
    property Id: WideString read Get_Id;
    property Name: WideString read Get_Name;
    property Type_: WideString read Get_Type_;
    property Port: WideString read Get_Port;
    property UIClsid: WideString read Get_UIClsid;
    property Manufacturer: WideString read Get_Manufacturer;
  end;

// *********************************************************************//
// DispIntf:  IWiaDeviceInfoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5267FF5E-7CAF-4769-865D-17A25968525E}
// *********************************************************************//
  IWiaDeviceInfoDisp = dispinterface
    ['{5267FF5E-7CAF-4769-865D-17A25968525E}']
    function  Create: IWiaDispatchItem; dispid 1;
    property Id: WideString readonly dispid 2;
    property Name: WideString readonly dispid 3;
    property Type_: WideString readonly dispid 4;
    property Port: WideString readonly dispid 5;
    property UIClsid: WideString readonly dispid 6;
    property Manufacturer: WideString readonly dispid 7;
    function  GetPropById(Id: WiaDeviceInfoPropertyId): OleVariant; dispid 1000;
  end;

// *********************************************************************//
// Interface: IWia
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}
// *********************************************************************//
  IWia = interface(IDispatch)
    ['{B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}']
    function  Get_Devices: ICollection; safecall;
    function  Create(var Device: OleVariant): IWiaDispatchItem; safecall;
    procedure _DebugDialog(fWait: Integer); safecall;
    property Devices: ICollection read Get_Devices;
  end;

// *********************************************************************//
// DispIntf:  IWiaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}
// *********************************************************************//
  IWiaDisp = dispinterface
    ['{B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}']
    property Devices: ICollection readonly dispid 1;
    function  Create(var Device: OleVariant): IWiaDispatchItem; dispid 2;
    procedure _DebugDialog(fWait: Integer); dispid 10010;
  end;

// *********************************************************************//
// DispIntf:  _IWiaEvents
// Flags:     (4096) Dispatchable
// GUID:      {E5F04D72-6C16-42E2-BCCA-F8D0DB4ADE06}
// *********************************************************************//
  _IWiaEvents = dispinterface
    ['{E5F04D72-6C16-42E2-BCCA-F8D0DB4ADE06}']
    procedure OnDeviceConnected(const Id: WideString); dispid 1;
    procedure OnDeviceDisconnected(const Id: WideString); dispid 2;
    procedure OnTransferComplete(const Item: IWiaDispatchItem; const Path: WideString); dispid 3;
  end;

// *********************************************************************//
// The Class CoItem provides a Create and CreateRemote method to
// create instances of the default interface IWiaDispatchItem exposed by
// the CoClass Item. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoItem = class
    class function Create: IWiaDispatchItem;
    class function CreateRemote(const MachineName: string): IWiaDispatchItem;
  end;

// *********************************************************************//
// The Class CoDeviceInfo provides a Create and CreateRemote method to
// create instances of the default interface IWiaDeviceInfo exposed by
// the CoClass DeviceInfo. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoDeviceInfo = class
    class function Create: IWiaDeviceInfo;
    class function CreateRemote(const MachineName: string): IWiaDeviceInfo;
  end;

// *********************************************************************//
// The Class CoCollection provides a Create and CreateRemote method to
// create instances of the default interface ICollection exposed by
// the CoClass Collection. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoCollection = class
    class function Create: ICollection;
    class function CreateRemote(const MachineName: string): ICollection;
  end;

// *********************************************************************//
// The Class CoWia provides a Create and CreateRemote method to
// create instances of the default interface IWia exposed by
// the CoClass Wia. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoWia = class
    class function Create: IWia;
    class function CreateRemote(const MachineName: string): IWia;
  end;

  TWiaOnDeviceConnected = procedure(Sender: TObject; var Id: OleVariant) of object;
  TWiaOnDeviceDisconnected = procedure(Sender: TObject; var Id: OleVariant) of object;
  TWiaOnTransferComplete = procedure(Sender: TObject; var Item: OleVariant;var Path: OleVariant) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TWia
// Help String      : WIA Class
// Default Interface: IWia
// Def. Intf. DISP? : No
// Event   Interface: _IWiaEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TWiaProperties= class;
{$ENDIF}
  TWia = class(TOleServer)
  private
    FOnDeviceConnected: TWiaOnDeviceConnected;
    FOnDeviceDisconnected: TWiaOnDeviceDisconnected;
    FOnTransferComplete: TWiaOnTransferComplete;
    FIntf:        IWia;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TWiaProperties;
    function      GetServerProperties: TWiaProperties;
{$ENDIF}
    function      GetDefaultInterface: IWia;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function  Get_Devices: ICollection;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IWia);
    procedure Disconnect; override;
    function  Create1(var Device: OleVariant): IWiaDispatchItem;
    procedure _DebugDialog(fWait: Integer);
    property  DefaultInterface: IWia read GetDefaultInterface;
    property Devices: ICollection read Get_Devices;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TWiaProperties read GetServerProperties;
{$ENDIF}
    property OnDeviceConnected: TWiaOnDeviceConnected read FOnDeviceConnected write FOnDeviceConnected;
    property OnDeviceDisconnected: TWiaOnDeviceDisconnected read FOnDeviceDisconnected write FOnDeviceDisconnected;
    property OnTransferComplete: TWiaOnTransferComplete read FOnTransferComplete write FOnTransferComplete;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TWia
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TWiaProperties = class(TPersistent)
  private
    FServer:    TWia;
    function    GetDefaultInterface: IWia;
    constructor Create(AServer: TWia);
  protected
    function  Get_Devices: ICollection;
  public
    property DefaultInterface: IWia read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSafeWia provides a Create and CreateRemote method to
// create instances of the default interface IWia exposed by
// the CoClass SafeWia. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoSafeWia = class
    class function Create: IWia;
    class function CreateRemote(const MachineName: string): IWia;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSafeWia
// Help String      : WIA Class (Safe for Scripting)
// Default Interface: IWia
// Def. Intf. DISP? : No
// Event   Interface:
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSafeWiaProperties= class;
{$ENDIF}
  TSafeWia = class(TOleServer)
  private
    FIntf:        IWia;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TSafeWiaProperties;
    function      GetServerProperties: TSafeWiaProperties;
{$ENDIF}
    function      GetDefaultInterface: IWia;
  protected
    procedure InitServerData; override;
    function  Get_Devices: ICollection;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IWia);
    procedure Disconnect; override;
    function  Create1(var Device: OleVariant): IWiaDispatchItem;
    procedure _DebugDialog(fWait: Integer);
    property  DefaultInterface: IWia read GetDefaultInterface;
    property Devices: ICollection read Get_Devices;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSafeWiaProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSafeWia
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSafeWiaProperties = class(TPersistent)
  private
    FServer:    TSafeWia;
    function    GetDefaultInterface: IWia;
    constructor Create(AServer: TSafeWia);
  protected
    function  Get_Devices: ICollection;
  public
    property DefaultInterface: IWia read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

implementation

uses ComObj;

class function CoItem.Create: IWiaDispatchItem;
begin
  Result := CreateComObject(CLASS_Item) as IWiaDispatchItem;
end;

class function CoItem.CreateRemote(const MachineName: string): IWiaDispatchItem;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Item) as IWiaDispatchItem;
end;

class function CoDeviceInfo.Create: IWiaDeviceInfo;
begin
  Result := CreateComObject(CLASS_DeviceInfo) as IWiaDeviceInfo;
end;

class function CoDeviceInfo.CreateRemote(const MachineName: string): IWiaDeviceInfo;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DeviceInfo) as IWiaDeviceInfo;
end;

class function CoCollection.Create: ICollection;
begin
  Result := CreateComObject(CLASS_Collection) as ICollection;
end;

class function CoCollection.CreateRemote(const MachineName: string): ICollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Collection) as ICollection;
end;

class function CoWia.Create: IWia;
begin
  Result := CreateComObject(CLASS_Wia) as IWia;
end;

class function CoWia.CreateRemote(const MachineName: string): IWia;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Wia) as IWia;
end;

procedure TWia.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{4EC4272E-2E6F-4EEB-91D0-EBC4D58E8DEE}';
    IntfIID:   '{B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}';
    EventIID:  '{E5F04D72-6C16-42E2-BCCA-F8D0DB4ADE06}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TWia.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as IWia;
  end;
end;

procedure TWia.ConnectTo(svrIntf: IWia);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TWia.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TWia.GetDefaultInterface: IWia;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TWia.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TWiaProperties.Create(Self);
{$ENDIF}
end;

destructor TWia.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TWia.GetServerProperties: TWiaProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TWia.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
   1: if Assigned(FOnDeviceConnected) then
            FOnDeviceConnected(Self, Params[0] {const WideString});
   2: if Assigned(FOnDeviceDisconnected) then
            FOnDeviceDisconnected(Self, Params[0] {const WideString});
   3: if Assigned(FOnTransferComplete) then
            FOnTransferComplete(Self, Params[1] {const WideString}, Params[0] {const IWiaDispatchItem});
  end; {case DispID}
end;

function  TWia.Get_Devices: ICollection;
begin
  Result := DefaultInterface.Devices;
end;

function  TWia.Create1(var Device: OleVariant): IWiaDispatchItem;
begin
  Result := DefaultInterface.Create(Device);
end;

procedure TWia._DebugDialog(fWait: Integer);
begin
  DefaultInterface._DebugDialog(fWait);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TWiaProperties.Create(AServer: TWia);
begin
  inherited Create;
  FServer := AServer;
end;

function TWiaProperties.GetDefaultInterface: IWia;
begin
  Result := FServer.DefaultInterface;
end;

function  TWiaProperties.Get_Devices: ICollection;
begin
  Result := DefaultInterface.Devices;
end;

{$ENDIF}

class function CoSafeWia.Create: IWia;
begin
  Result := CreateComObject(CLASS_SafeWia) as IWia;
end;

class function CoSafeWia.CreateRemote(const MachineName: string): IWia;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SafeWia) as IWia;
end;

procedure TSafeWia.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{0DAD5531-BF31-43AC-A513-1F8926BBF5EC}';
    IntfIID:   '{B10BA1BC-3713-4EC0-8EEA-690EBD2CED8A}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSafeWia.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IWia;
  end;
end;

procedure TSafeWia.ConnectTo(svrIntf: IWia);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSafeWia.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSafeWia.GetDefaultInterface: IWia;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TSafeWia.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSafeWiaProperties.Create(Self);
{$ENDIF}
end;

destructor TSafeWia.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSafeWia.GetServerProperties: TSafeWiaProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function  TSafeWia.Get_Devices: ICollection;
begin
  Result := DefaultInterface.Devices;
end;

function  TSafeWia.Create1(var Device: OleVariant): IWiaDispatchItem;
begin
  Result := DefaultInterface.Create(Device);
end;

procedure TSafeWia._DebugDialog(fWait: Integer);
begin
  DefaultInterface._DebugDialog(fWait);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSafeWiaProperties.Create(AServer: TSafeWia);
begin
  inherited Create;
  FServer := AServer;
end;

function TSafeWiaProperties.GetDefaultInterface: IWia;
begin
  Result := FServer.DefaultInterface;
end;

function  TSafeWiaProperties.Get_Devices: ICollection;
begin
  Result := DefaultInterface.Devices;
end;

{$ENDIF}

procedure Register;
begin
//  RegisterComponents('Standard', [TWia, TSafeWia]);
end;

end.
