unit HoursDataset;

interface

uses
  Classes, DB, DateUtils, ADODB;

function GetWorkHours(D: TDataSet): Double;
function GetCalloutHours(D: TDataSet): Double;
function TimeSlotIsEmpty(const StartTime, StopTime: Double): Boolean;
procedure CheckWorkPeriods(D: TDataSet; const FieldPrefix, DisplayName: string; FieldCount: Integer);
function CheckSequentialWorkTimes(D: TDataSet; const FieldPrefix: string; const FieldCount: integer): string;
procedure CheckHoursEntry(D: TDataSet; const FieldName, ErrorMsg: string);
procedure CheckOverlap(D: TDataSet; const Prefix1, Prefix2: string; FieldCount1, FieldCount2: Integer);
function TimesheetIsFinalApproved(D: TDataSet): Boolean;
procedure CheckWorkPeriodsEnd(D: TDataSet; const FieldPrefix: string; FieldCount: Integer);
function IsClockedOut(D: TDataSet; IsOutForLunch: Boolean): Boolean;
function IsClockedIn(D: TDataSet): Boolean;
procedure CheckPTOHours(TimeData, ConfigData: TDataSet; RefData: TADODataSet);

implementation

uses
  Variants, SysUtils, OdExceptions, OdMiscUtils, QMConst, OdIsoDates;

function DTValue(D: TDataSet; FieldName: string): Variant;
var
  Field: TField;
begin
  Result := 0;
  if D.Active then begin
    Field := D.FieldByName(FieldName);
    if Field.IsNull then
      Result := Null
    else
      Result := Field.AsDateTime;
  end;
end;

function GetHoursDifference(Start, Stop: Variant): Double;
begin
  if VarIsNull(Start) or VarIsNull(Stop) or (Start > Stop) then
    Result := 0
  else
    Result := FloatRound(SecondsBetween(Stop, Start) / (60 * 60), 2);
end;

function GetWorkHours(D: TDataSet): Double;
var
  I: Integer;
begin
  Result := 0.0;
  for I := 1 to TimesheetNumWorkPeriods do
    Result := Result + GetHoursDifference(
      DTValue(D, 'work_start' + IntToStr(I)),
      DTValue(D, 'work_stop' + IntToStr(I)));
end;

function GetCalloutHours(D: TDataSet): Double;
var
  I: Integer;
begin
  Result := 0.0;
  for I := 1 to TimesheetNumCalloutPeriods do
    Result := Result + GetHoursDifference(
      DTValue(D, 'callout_start' + IntToStr(I)),
      DTValue(D, 'callout_stop' + IntToStr(I)));
end;

function TimeSlotIsEmpty(const StartTime, StopTime: Double): Boolean;
begin
  Result := FloatEquals(StartTime, 0) and FloatEquals(StopTime, 0);
end;

procedure CheckWorkPeriods(D: TDataSet; const FieldPrefix, DisplayName: string; FieldCount: Integer);
var
  StartField: TField;
  StopField: TField;
  LastStop: TDateTime;
  ThisStart: TDateTime;
  ThisStop: TDateTime;
  i: Integer;
begin

  LastStop := 0;
  for i := 1 to FieldCount do begin
    StartField := D.FieldByName(FieldPrefix + 'start' + IntToStr(i));
    StopField := D.FieldByName(FieldPrefix + 'stop' + IntToStr(i));
    ThisStart := StartField.AsDateTime;
    ThisStop := StopField.AsDateTime;

    if TimeSlotIsEmpty(ThisStart, ThisStop) then
      Continue;

    if StartField.IsNull and not StopField.IsNull then
      raise EOdDataEntryError.CreateFmt('%s period %d must have a begin time', [DisplayName, i]);
//QM-212 reverted EB's change below.  Was causing exception due to setting start period to 1 rather than 0.   sr
    if (ThisStart >= ThisStop) and (ThisStop > 0) then  {QM-177 EB - failing when someone edited times with 00:00}
      raise EOdDataEntryError.CreateFmt('%s period %d must end after it starts', [DisplayName, i]);

    if (ThisStart < LastStop) or ((ThisStop < LastStop) and (ThisStop > 0)) then
      raise EOdDataEntryError.CreateFmt('All entered time periods must start before the previously entered time period ends.' +sLineBreak+ 'Please correct %s period %d.', [DisplayName, i]);
//QM-212 end change    sr
    LastStop := ThisStop;
  end;
end;

function CheckSequentialWorkTimes(D: TDataSet; const FieldPrefix: string; const FieldCount: integer): string;
var
  i: integer;
  StartField: TField;
  WorkStartName: string;
  PrevNullFlag: boolean;
  GapString: TStringList;
begin
  Result := '';
  PrevNullFlag := False;
  GapString := TStringList.Create;
  GapString.StrictDelimiter := True;
  GapString.Delimiter := ',';
  try
    for i := 1 to FieldCount do begin
      WorkStartName := FieldPrefix + 'start' + IntToStr(i);
      StartField := D.FieldByName(WorkStartName);
      if Not StartField.IsNull and PrevNullFlag then
        GapString.Add(WorkStartName);
      PrevNullFlag := StartField.IsNull;
    end;
   //DELETE
//    for i := 0 to GapString.Count - 1 do
//    begin
//              '
//      GapString[i]
//    end;
     Result := GapString.DelimitedText;
  finally
    FreeAndNil(GapString);
  end;
end;

procedure CheckWorkPeriodsEnd(D: TDataSet; const FieldPrefix: string; FieldCount: Integer);
var
  i: Integer;
  StartField: TField;
  StopField: TField;
begin
  Assert((FieldPrefix <> '') and (FieldCount > 0));
  for i := 1 to FieldCount do begin
    StartField := D.FindField(Format('%s%s%d', [FieldPrefix, '_start', i]));
    StopField :=  D.FindField(Format('%s%s%d', [FieldPrefix, '_stop' , i]));
    Assert(Assigned(StartField) and Assigned(StopField), 'Some timesheet Start/Stop fields are missing');

    if (not StartField.IsNull) and StopField.IsNull then begin
      raise EOdDataEntryError.CreateFmt('The %s period %d must have an end time', [FieldPrefix, i]);
    end;
  end;
end;

procedure CheckHoursEntry(D: TDataSet; const FieldName, ErrorMsg: string);
var
  Hours: Double;
begin
  Hours := D.FieldByName(FieldName).AsFloat;
  if (Hours > 24) or (Hours < 0) then
    raise EOdDataEntryError(ErrorMsg + ' hours must be between 0 and 24');
end;

procedure CheckOverlap(D: TDataSet; const Prefix1, Prefix2: string; FieldCount1, FieldCount2: Integer);
var
  i, j: Integer;
  Start1, Start2: TDateTime;
  Stop1, Stop2: TDateTime;
begin
  for i := 1 to FieldCount1 do begin
    for j := 1 to FieldCount2 do begin
      Start1 := D.FieldByName(Prefix1 + 'start' + IntToStr(i)).AsDateTime;
      Start2 := D.FieldByName(Prefix2 + 'start' + IntToStr(j)).AsDateTime;
      Stop1 := D.FieldByName(Prefix1 + 'stop' + IntToStr(i)).AsDateTime;
      Stop2 := D.FieldByName(Prefix2 + 'stop' + IntToStr(j)).AsDateTime;

      if TimeSlotIsEmpty(Start1, Stop1) then
        Continue;
      if TimeSlotIsEmpty(Start2, Stop2) then
        Continue;
      if FloatEquals(Stop1, 0) then
        Stop1 := IncSecond(Start1);
      if FloatEquals(Stop2, 0) then
        Stop2 := IncSecond(Start2);
      if RangesIntersect(Start1, Stop1, Start2, Stop2) then
        raise EOdDataEntryError.CreateFmt('Callout and work periods can not overlap.  Please correct callout period %d.', [j]);
    end;
  end;
end;

function TimesheetIsFinalApproved(D: TDataSet): Boolean;
begin
  if D.Eof then
    Result := False
  else
    Result := D.FieldByName('final_approve_by').AsInteger > 0;
end;

function IsClockedIn(D: TDataSet): Boolean;

  function ClockedIn(Start, Stop: string): Boolean;
  begin
    //start time, but no stop means clocked in
    Result := (not D.FieldByName(Start).IsNull) and D.FieldByName(Stop).IsNull;
  end;

var
  Working, CalledOut: Boolean;
  i: Integer;
begin
  if D.Eof then begin
    Result := False;
    Exit;
  end;

  Working := False;
  CalledOut := False;
  for i := 1 to TimesheetNumWorkPeriods do
    Working := Working or ClockedIn('work_start' + IntToStr(i), 'work_stop' + IntToStr(i));
  for i := 1 to TimesheetNumCalloutPeriods do
    CalledOut := CalledOut or ClockedIn('callout_start' + IntToStr(i), 'callout_stop' + IntToStr(i));
  Result := (Working or CalledOut);
end;

function IsClockedOutForLunch(D: TDataSet): Boolean;
var
  i: Integer;
begin
  if D.Eof or D.FieldByName('lunch_start').IsNull then begin
    Result := False;
    Exit;
  end else
    Result := True;

  // any work_start times after the lunch_start means lunch break is over
  for i := 1 to TimesheetNumWorkPeriods do begin
    if (D.FieldByName('work_start' + IntToStr(i)).AsDateTime > D.FieldByName('lunch_start').AsDateTime) then begin
      Result := False;
      Break;
    end;
  end;
end;

function IsClockedOut(D: TDataSet; IsOutForLunch: Boolean): Boolean;
begin
  if not IsOutForLunch then
    Result := not IsClockedIn(D)
  else
    Result := IsClockedOutForLunch(D);
end;

procedure CheckPTOHours(TimeData, ConfigData: TDataSet; RefData: TADODataSet);
const
  BadDate = 900000; //on invalid PTO start date, set the value far in the future
var
  PTO, Vac, Leave: Double;
  PTODateString: string;
  WorkDate, PTOStartDate: TDateTime;
begin
  PTO := TimeData.FieldByName('pto_hours').AsFloat;
  Vac := TimeData.FieldByName('vac_hours').AsFloat;
  Leave := TimeData.FieldByName('leave_hours').AsFloat;
  WorkDate := TimeData.FieldByName('work_date').AsDateTime;

  ConfigData.Open;
  try
    PTODateString := ConfigData.FieldByName('value').AsString;
  finally
    ConfigData.Close;
  end;
  PTOStartDate := IsoStrToDateDef(PTODateString, BadDate);

  if PTO > 0 then begin
    if PTOStartDate = BadDate then
      if Trim(PTODateString) <> '' then
        raise EOdDataEntryError.CreateFmt('PTO Hours entry is disabled. ' + CRLF +
          'The PTOHoursEntryStartDate configuration data of %s is not in the expected format of yyyy-mm-dd. ' +
          CRLF + 'After a system administrator fixes the entry with QManagerAdmin, try again',
          [PTODateString])
      else
        raise EOdDataEntryError.Create('PTO Hours have not been enabled');
    if WorkDate < PTOStartDate then
      raise EOdDataEntryError.Create('PTO Hours prior to PTOHoursEntryStartDate');
    if Vac + Leave > 0 then
      raise EOdDataEntryError.Create('A day cannot have both PTO and Vacation/Leave hours');
    RefData.Parameters.ParamByName('pto_hours').Value := FloatToStr(PTO);
    RefData.Open;
    try
      if RefData.IsEmpty then
        raise EOdDataEntryError.Create('Invalid value of [' + FloatToStr(PTO) + '] for PTO hours');
    finally
      RefData.Close;
    end;
  end;

  if (Vac + Leave > 0) and (WorkDate >= PTOStartDate) then
    raise EOdDataEntryError.Create('The PTO Hours entry start date has passed. Vacation/Leave ' +
      'hours may not be entered');
end;

end.
