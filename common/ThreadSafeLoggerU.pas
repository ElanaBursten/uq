// Oasis Digital Thread Safe Logging System
// If the LogEncoding property is not specified, then the log will be saved as ANSI.

unit ThreadSafeLoggerU;

interface

uses
  Classes, SyncObjs, SysUtils, Windows, OdMiscUtils;

type
  TBackgroundWriter = class;

  //the class has been switched to use a Ring buffer approach.
  TThreadSafeLogger = class
  protected
    WritePosition: Integer;
    ReadPosition: Integer;
    DataSize: Integer;
    LogStream: TMemoryStream;
    LogLock: TCriticalSection;
    Directory: string;
    FileName: string;
    InstanceId: Integer;
    FEnabled: Boolean;
    LogFile: TFileStream;
    BackgroundWriter: TBackgroundWriter;
    CharSize: Integer;
{$IFDEF UNICODE}
    FLogEncoding : TQMEncodingType;
{$ENDIF}
    ByteOrderMark: TBytes;
    function ReadNew: TBytes;
    procedure WriteLogOutIfAvailable;
    procedure SetEnabled(Enable: Boolean);
    function DailyLogName: string;
    procedure SetupLogFile;
    procedure SetupBackgroundTask;
  public
    constructor Create(const Dir: string; const Fn: string; const InstId: Integer = 0; Capacity: Integer = 8000000; Start: Boolean = True{$IFDEF UNICODE}; QMEncoding: TQMEncodingType = etAnsi{$ENDIF});
    destructor Destroy; override;
    procedure Log(s: String);
    procedure LogWithTime(const Msg: String);
    property Enabled: Boolean read FEnabled write SetEnabled;
  end;

  TPrefixedWriter = class
  private
    FLogger: TThreadSafeLogger;
    FPrefix: String;
  public
    constructor Create(Logger: TThreadSafeLogger; Prefix: String);
    procedure Log(s: String);
    property Prefix: String read FPrefix write FPrefix;
  end;

  TBackgroundWriter = class(TThread)
  private
    FTicks: Integer;
    FLogger: TThreadSafeLogger;
  protected
    procedure Execute; override;
  end;

implementation

var
  CrLf: String = #13#10;

function TThreadSafeLogger.DailyLogName: string;
begin
  Result := GetDailyFileName(Directory, FileName, InstanceID);
end;

procedure TThreadSafeLogger.SetupLogFile;
begin
  if not FileExists(DailyLogName()) then
    CreateEmptyFile(DailyLogName());

  LogFile := TFileStream.Create(DailyLogName(), fmOpenReadWrite or fmShareDenyWrite);
  LogFile.Seek(0, soFromEnd);

{$IFDEF UNICODE}
//http://docwiki.embarcadero.com/CodeExamples/XE6/en/TEncoding_(Delphi)
    if (LogFile.Size = 0) and (FLogEncoding = etUTF8)  then begin
      ByteOrderMark := TEncoding.UTF8.GetPreamble;
      LogFile.Write(ByteOrderMark[0], Length(ByteOrderMark));
    end;
{$ENDIF}
end;

procedure TThreadSafeLogger.SetupBackgroundTask;
begin
  if not Assigned(BackgroundWriter) then begin
    BackgroundWriter := TBackgroundWriter.Create(True);
    BackgroundWriter.FLogger := Self;
    BackgroundWriter.Priority := tpLower;
{$IF CompilerVersion >= 22}
    BackgroundWriter.Start;
{$ELSE}
    BackgroundWriter.Resume;
{$IFEND}
  end;
end;

procedure TThreadSafeLogger.Log(s: String);
var
  StringByteLength: Integer;
  S1, S2: Integer;
  Split1, Split2: string;
  CharSplitIndex: Integer;
begin
  if not FEnabled then
    Exit;

  LogLock.Enter;
  try
    LogStream.Position := WritePosition;
    s := s + CrLf;
    StringByteLength :=  Length(s) * CharSize;
    DataSize := DataSize + StringByteLength; //DataSize is in bytes; keep everything in bytes until just before writing to file.

    if (LogStream.Position + StringByteLength) > LogStream.Size then begin //wrapping //LogStream.Position is in bytes
      S1 := LogStream.Size - WritePosition;//bytes left to fill capacity; i.e. how many bytes are left available to fill from the string
      S2 := StringByteLength - S1;         //bytes of the string that will be left over and need another write

      //Split the string according to the bytes the stream can hold
      CharSplitIndex := S1 div CharSize;
      Split1 := copy(s,1,CharSplitIndex);
      Split2 := copy(s,CharSplitIndex+1,(S2 div CharSize)+1);

      //Write the bytes to the memory stream
      LogStream.WriteStringBytes(Split1{$IFDEF UNICODE},FLogEncoding{$ENDIF});
      LogStream.Position := 0;
      LogStream.WriteStringBytes(Split2{$IFDEF UNICODE},FLogEncoding{$ENDIF});

      WritePosition := LogStream.Position;
      if WritePosition > ReadPosition then begin
         ReadPosition := WritePosition;
      end;
    end
    else begin
      LogStream.WriteStringBytes(s{$IFDEF UNICODE},FLogEncoding{$ENDIF});
      WritePosition := LogStream.Position;
      if DataSize >= LogStream.Size then begin
        ReadPosition := WritePosition;
      end;
    end;
    if DataSize > LogStream.Size then
      DataSize := LogStream.Size;
  finally
    LogLock.Leave;
  end;
end;

procedure TThreadSafeLogger.LogWithTime(const Msg: String);
var
  FormatSettings : TFormatSettings;
begin
//This unit is used by QMLogic.dll(still D2007). Remain backwards compatible with D2007 until we get QMLogic converted to XE3.
// Note that SizeOf() was used vs StringElementSize() for this reason as well.
{$IF CompilerVersion >= 22} //XE
  FormatSettings := TFormatSettings.Create;//Call without locale parameter to avoid platform warning. Create calls GetThreadLocale without the parameter.
{$ELSE}
  GetLocaleFormatSettings(GetThreadLocale, FormatSettings);
{$IFEND}
  //Use the thread safe version of FormatDateTime:
  //http://docwiki.embarcadero.com/Libraries/XE3/en/System.SysUtils.FormatDateTime
  Log(FormatDateTime('yyyy-mm-dd hh:MM:ss ', Now, FormatSettings) + Msg);
end;

constructor TThreadSafeLogger.Create(const Dir: string; const Fn: string; const InstId: Integer = 0; Capacity: Integer = 8000000; Start: Boolean = True{$IFDEF UNICODE}; QMEncoding: TQMEncodingType = etAnsi{$ENDIF});
begin
{$IFDEF UNICODE}
  FLogEncoding := QMEncoding;//TODO: Change this to UTF-8 once QML has been replaced with QML2.
  if FLogEncoding = etAnsi then
    CharSize := SizeOf(AnsiChar)
  else
    CharSize := SizeOf(Char);
{$ELSE}
  CharSize := SizeOf(AnsiChar);
{$ENDIF}
  if Capacity = 0 then
    Capacity := 8000000
  else if Capacity < 10000 then
    Capacity := 10000;
  LogStream := TMemoryStream.Create;
  LogStream.SetSize(Capacity);
  LogLock := TCriticalSection.Create;
  WritePosition := 0;
  ReadPosition := 0;
  DataSize := 0;
  FEnabled := Start;
  if NotEmpty(Dir) then
    Directory := OdMiscUtils.AddSlash(Dir);
  FileName := Fn;
  InstanceId := InstId;

  if NotEmpty(FileName) then begin
    if Start then begin
      SetupLogFile;
      SetupBackgroundTask;
    end;
  end
  else
    raise Exception.Create('Log filename is required');
end;

function TThreadSafeLogger.ReadNew: TBytes;
var
  S1, S2: Integer;
begin
  LogLock.Enter;
  try
    if DataSize = 0 then begin
      SetLength(Result,0);
      Exit;
    end;

    SetLength(Result, DataSize);
    if DataSize < (LogStream.Size - ReadPosition) then begin
      LogStream.Position := ReadPosition;
      LogStream.Read(Result[0], DataSize);
      ReadPosition := ReadPosition + DataSize;
    end
    else begin
      LogStream.Position := ReadPosition;
      S1 := LogStream.Size - ReadPosition;
      S2 := DataSize - S1;
      LogStream.Read(Result[0],S1); //0-based
      LogStream.Position := 0;
      LogStream.Read(Result[S1], S2);
      ReadPosition := S2;
    end;
    DataSize := 0;

  finally
    LogLock.Leave;
  end;
end;

procedure TThreadSafeLogger.WriteLogOutIfAvailable;
var
  sBytes: TBytes;
begin
  if Assigned(LogFile) then
    if DataSize > 0 then begin
      sBytes := ReadNew;
      if LogFile.FileName <> DailyLogName() then begin
        LogFile.Free;
        SetupLogFile;
      end;
      //Deal only with bytes up to this point to avoid extra NUL terminators; each string ends with a NUL terminator so we only want to deal with one
      LogFile.WriteBytes(sBytes{$IFDEF UNICODE},FLogEncoding{$ENDIF});
    end;
end;

procedure TThreadSafeLogger.SetEnabled(Enable: Boolean);
begin
  if (not Assigned(LogFile)) and Enable then begin
    SetupLogFile;
    SetupBackgroundTask;
  end;

  FEnabled := Enable;
end;

destructor TThreadSafeLogger.Destroy;
begin
  if Assigned(BackgroundWriter) then begin
    BackgroundWriter.Terminate;  // does not actually end it
    BackgroundWriter.WaitFor;    // this waits for it to exit
    FreeAndNil(BackgroundWriter);
  end;

  WriteLogOutIfAvailable;      // Final Flush
  FreeAndNil(LogStream);
  FreeAndNil(LogFile);
  FreeAndNil(LogLock);
  inherited;
end;

constructor TPrefixedWriter.Create(Logger: TThreadSafeLogger; Prefix: String);
begin
  FLogger := Logger;
  FPrefix := Prefix;
end;

procedure TPrefixedWriter.Log(s: String);
begin
  FLogger.Log(FPrefix + s);
end;

{ TBackgroundWriter }
procedure TBackgroundWriter.Execute;
begin
  inherited;
  while not Terminated do begin
    Sleep(500);  // wake up often, so we can be terminated easily
                 // I should replace this with a WaitForSingleObject, really.
    Inc(FTicks);
    if (FTicks mod 40) = 0 then
      FLogger.WriteLogOutIfAvailable;
  end;
end;

end.

