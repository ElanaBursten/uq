{******************************************************************************}
{  Iso ISO Date Format Mapping                                                 }
{******************************************************************************}

unit OdIsoDates;

interface

uses
  Windows, SysUtils;

function IsoDateToStr(AValue: TDateTime): string;
function IsoDateToStrQuoted(AValue: TDateTime): string;
function IsoTimeToStr(AValue: TDateTime): string;
function IsoTimeToStrNoMillis(AValue: TDateTime): string;

function IsoDateTimeToStr(AValue: TDateTime; const ZeroValue: string): string; overload;
function IsoDateTimeToStr(AValue: TDateTime): string; overload;
function IsoDateTimeToStrQuoted(AValue: TDateTime): string;

function IsoStrToDate(const AValue: string): TDateTime;
function IsoStrToDateDef(const AValue: string; Default: TDateTime = 0.0): TDateTime;
function IsoStrToTime(const AValue: string): TDateTime;
function IsoStrToDateTime(const AValue: string): TDateTime;
function IsoStrToDateTimeDef(const AValue: string; Default: TDateTime = 0.0): TDateTime;
function IsoStrToSystemStr(const AValue: string): string;
function IsoIsValidDateTime(const AValue: string): Boolean;

implementation

uses
  OdMiscUtils;

resourcestring
  SInvalidSQLDate = '''%s'' is not a valid SQL date';
  SInvalidSQLTime = '''%s'' is not a valid SQL time';
  SInvalidSQLDateTime = '''%s'' is not a valid SQL date and time';
  SInvalidFloat = '''%s'' is not a valid floating point value';

const
  IsoDelphiDateFormat = 'YYYY"-"MM"-"DD';
  IsoDelphiTimeFormat = 'hh":"nn":"ss"."z';
  IsoDelphiTimeFormatNoMillis = 'hh":"nn":"ss';
  IsoDelphiDateTimeFormat = 'YYYY"-"MM"-"DD"T"hh":"nn":"ss"."zzz';

function IsoDateToStr(AValue: TDateTime): string;
begin
  Result := FormatDateTime(IsoDelphiDateFormat, AValue);
end;

function IsoDateToStrQuoted(AValue: TDateTime): string;
begin
  Result := '''' + IsoDateToStr(AValue) + '''';
end;

function IsoTimeToStr(AValue: TDateTime): string;
begin
  Result := FormatDateTime(IsoDelphiTimeFormat, AValue);
end;

function IsoTimeToStrNoMillis(AValue: TDateTime): string;
begin
  Result := FormatDateTime(IsoDelphiTimeFormatNoMillis, AValue);
end;

function IsoDateTimeToStr(AValue: TDateTime): string;
begin
  Result := FormatDateTime(IsoDelphiDateTimeFormat, AValue);
end;

function IsoDateTimeToStr(AValue: TDateTime; const ZeroValue: string): string;
begin
  if AValue = 0.0 then
    Result := ZeroValue
  else
    Result := IsoDateTimeToStr(AValue);
end;

function IsoDateTimeToStrQuoted(AValue: TDateTime): string;
begin
  Result := '''' + IsoDateTimeToStr(AValue) + '''';
end;

function CheckDateTimeFormat(const AValue, AFormat: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  if Length(AValue) <> Length(AFormat) then
    Exit;
  for i := 1 to Length(AValue) do begin
    if AFormat[i] = '9' then begin      { Digit }
      if not CharInSet(AValue[i], ['0'..'9']) then
        Exit;
    end
    else begin
      if AFormat[i] <> AValue[i] then
        Exit;
    end;
  end;
  Result := True;
end;

function ScanSQLDate(const AValue: string; var ADate: TDateTime): Boolean;
var
  Offset: Integer;
  Year, Month, Day: Word;
begin
  Result := False;
  if CheckDateTimeFormat(AValue, '9999-99-99') then
    Offset := 1
  else if CheckDateTimeFormat(AValue, '99999999') then
    Offset := 0
  else
    Exit;
  Year := StrToIntDef(Copy(AValue, 1, 4), 0);
  Month := StrToIntDef(Copy(AValue, 5 + 1 * Offset, 2), 0);
  Day := StrToIntDef(Copy(AValue, 7 + 2 * Offset, 2), 0);
  try
    ADate := EncodeDate(Year, Month, Day) + Frac(ADate);
    Result := True;
  except
    on EConvertError do
      ;
  end;
end;

function ScanSQLTime(const AValue: string; var ADate: TDateTime): Boolean;
var
  s, ms: string;
  i: Integer;
  Hours, Minutes, Seconds, Milliseconds: Word;
  Offset: Integer;
  WithSeconds: Boolean;

begin
  Result := False;
  i := Pos('.', AValue);
  if i = 0 then begin
    s := AValue;
    ms := '';
  end
  else begin
    s := Copy(AValue, 1, i - 1);
    ms := Copy(AValue, i + 1, MaxInt);
  end;
  if CheckDateTimeFormat(s, '99:99:99') then begin
    Offset := 1;
    WithSeconds := True;
  end
  else if CheckDateTimeFormat(s, '999999') then begin
    Offset := 0;
    WithSeconds := True;
  end
  else if CheckDateTimeFormat(s, '99:99') then begin
    Offset := 1;
    WithSeconds := False;
  end
  else if CheckDateTimeFormat(s, '9999') then begin
    Offset := 0;
    WithSeconds := False;
  end
  else begin
    Exit;
  end;

  Hours := StrToIntDef(Copy(AValue, 1, 2), 100);
  Minutes := StrToIntDef(Copy(AValue, 3 + 1 * Offset, 2), 100);

  if WithSeconds then begin
    Seconds := StrToIntDef(Copy(AValue, 5 + 2 * Offset, 2), 100);
  end
  else begin
    Seconds := 0;
    if Length(ms) > 0 then
      Exit; { Milliseconds without seconds -> error }
  end;

  if Length(ms) > 0 then begin
    Milliseconds := StrToIntDef(ms, 10000);
  end
  else begin
    Milliseconds := 0;
  end;

  try
    ADate := EncodeTime(Hours, Minutes, Seconds, Milliseconds) + Int(ADate);
    Result := True;
  except
    on EConvertError do
      ;
  end;
end;

function IsoStrToDate(const AValue: string): TDateTime;
begin
  Result := 0;
  if not ScanSQLDate(AValue, Result) then
    raise EConvertError.CreateFmt(SInvalidSQLDate, [AValue]);
end;

function IsoStrToDateDef(const AValue: string; Default: TDateTime = 0.0): TDateTime;
begin
  if (AValue = '-') or (Trim(AValue) = '') or (AValue = '0') or (AValue = '0.0') then
    Result := Default
  else try
    Result := IsoStrToDate(Avalue);
  except
    on EConvertError do
      Result := Default;
  end;
end;

function IsoStrToTime(const AValue: string): TDateTime;
begin
  Result := 0;
  if not ScanSQLTime(AValue, Result) then
    raise EConvertError.CreateFmt(SInvalidSQLTime, [AValue]);
end;

function IsoStrToDateTime(const AValue: string): TDateTime;
var
  ADate, ATime: string;
  i: Integer;
begin
  Result := 0;
  i := Pos('T', AValue);
  if i > 0 then begin
    ADate := Copy(AValue, 1, i - 1);
    ATime := Copy(AValue, i + 1, MaxInt);
    if ScanSQLDate(ADate, Result) and ScanSQLTime(ATime, Result) then
      Exit;
  end;
  raise EConvertError.CreateFmt(SInvalidSQLDateTime, [AValue]);
end;

function IsoStrToDateTimeDef(const AValue: string; Default: TDateTime = 0.0): TDateTime;
begin
  if (AValue = '-') or (AValue = '') or (AValue = '0') or (AValue = '0.0') then
    Result := Default
  else try
    Result := IsoStrToDateTime(Avalue);
  except
    on EConvertError do
      Result := Default;
  end;
end;

function IsoStrToSystemStr(const AValue: string): string;
var
  DT: TDateTime;
begin
  DT := IsoStrToDateTimeDef(AValue);
  if DT = 0 then
    Result := ''
  else
    Result := DateTimeToStr(DT);
end;

function IsoIsValidDateTime(const AValue: string): Boolean;
begin
  Result := False;
  if Trim(AValue) = '' then
    Exit;
  try
    IsoStrToDateTime(AValue);
    Result := True;
  except
    // Swallow exceptions and return false
  end;
end;

end.

