unit PriorityRulesBase;

interface

uses Classes, DB, Math;

type
  TEntityData = class(TObject);

type
  TPriorityRulesBase = class(TObject)
  protected
    FRules: TList;
    FRulesData: TDataset;
    FEntityData: TEntityData;
    procedure LoadRules; virtual; abstract;
  public
    constructor Create(AOwner: TComponent; const EntityData: TEntityData; const RulesData: TDataset);
    destructor Destroy; override;
    function ProcessRulesForEntity: string; virtual;
  end;

type
  TPriorityRule = class(TObject)
  protected
    FRulePriority: Integer;
  public
    function RuleMatched(EntityData: TEntityData): Boolean; virtual; abstract;
    function PerformRule: string; virtual; abstract;
  end;

implementation

function ComparePriorities(Item1, Item2: Pointer): Integer;
{ Make sure rules are sorted in ascending priority order; use the highest priority matching rule. }
begin
  Result := CompareValue(TPriorityRule(Item1).FRulePriority, TPriorityRule(Item2).FRulePriority);
end;

{ TPriorityRulesBase }

constructor TPriorityRulesBase.Create(AOwner: TComponent; const EntityData: TEntityData; const RulesData: TDataset);
begin
  FRules := TList.Create;
  FEntityData := EntityData;
  FRulesData := RulesData;
  LoadRules;
end;

destructor TPriorityRulesBase.Destroy;
begin
  FRules.Clear;
  FRules.Free;

  inherited;
end;

function TPriorityRulesBase.ProcessRulesForEntity: string;
var
  iRules : Integer;
begin
  FRules.Sort(ComparePriorities);

  Result := '';
  for iRules := 0 to FRules.Count - 1 do begin
    if TPriorityRule(FRules[iRules]).RuleMatched(FEntityData) then begin
      Result := TPriorityRule(FRules[iRules]).PerformRule;
      Break;
    end;
  end;
end;

end.

