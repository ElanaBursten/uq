unit CVUtils;

interface

uses Classes, DB, ADOdb;

function ReplaceCVs(const ForeignID, ForeignType: Integer; FileDataSet: TDataSet; FileList: TStringList;
  const RegEx, IniName: string): TDataSet;
function GetReportRegEx(Conn: TADOConnection): string;

implementation

uses Windows, SysUtils, ReportConfig, Variants, xSuperObject,
  WinHttp_TLB, PerlRegEx, OdMiscUtils, IniFiles, QMConst, OdDbUtils,
  BaseAttachmentDMu;

var
  CVServer: string;
  CertLocation: string;

function FileIsCV(const FileName: string): Boolean;
begin
  Result := SameText(ExtractFileExt(FileName), '.@CV');
end;

function GetCVFileList(const CVFile: string): ISuperObject;
var
  ListRequest: IWinHttpRequest;
begin
  ListRequest := CoWinHttpRequest.Create;

  //The following is split into multiple try/excepts to prevent potential error
  //message from becoming too bloated.
  //Establish connection to CV Web service
  try
    try
      ListRequest.open('POST', CVServer, False);
      ListRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');
    except
      on E: Exception do begin
        E.Message := 'Cannot connect to CV web service at: ' + CVServer + ': ' + E.Message;
        raise;
      end;
    end;

    //Establish Credentials
    try
      ListRequest.SetClientCertificate(CertLocation);
    except
      on E: Exception do begin
        E.Message := 'Invalid Cert or Cert Location for: ' + CertLocation + ': ' + E.Message;
        raise;
      end;
    end;

    //Post the file to dycom server
    ListRequest.Send(FileToOleVariant(CVFile));
    Assert(ListRequest.Status = HttpOK, Format('%s responded with code: %s (%s) '+
      'for @CV File: %s.', [CVServer, IntToStr(ListRequest.Status), ListRequest.StatusText, CVFile]));

    Result := SO(ListRequest.ResponseText);
  except
    on E: Exception do begin
      E.Message := 'Unable to retrieve CV content list from server: '+ E.Message;
      raise;
    end;
  end;
end;

procedure GetContentFileFromCV(const CVFile, FileUrl, FileName, OutFileName: string);
var
  FileRequest: IWinHttpRequest;
begin
  //Needs less error handling as the cv service will have already been contacted
  //once
  try
    FileRequest := CoWinHttpRequest.Create;
    FileRequest.Open('GET', FileUrl, False);
    FileRequest.SetRequestHeader('Content-Type', 'binary/octet-stream');
    FileRequest.SetClientCertificate(CertLocation);
    FileRequest.Send(EmptyParam);
    Assert(FileRequest.Status = HttpOK, 'Server responded with '+
      IntToStr(FileRequest.Status) + '(' + FileRequest.StatusText + ')');
    OleVariantToFile(FileRequest.ResponseBody, OutFileName);
  except
    on E: Exception do begin
      E.Message := 'Failed to retrieve: ' + FileName + ' from CV: '+
        ExtractFileName(CVFile) + ': ' + E.Message;
      raise;
    end;
  end;
end;

procedure GetFilesFromCV(FileList: TStringList; const CVFile, RegEx, Prefix: string);
var
  JsonData: ISuperObject;
  FileName: string;
  OutFileName: string;
  I: Integer;
  RX: TPerlRegEx;
begin
  Assert(Assigned(FileList), 'FileList is not assigned.');
  Assert(FileIsCV(CVFile), Format('Non-CV file %s cannot be sent to the CV web service', [CVFile]));
  Assert(CanWriteToDirectory(ExtractFilePath(CVFile)), 'Unable to write to download folder: ' + ExtractFilePath(CVFile));

  FileList.Clear;
  RX := TPerlRegEx.Create(nil);
  try
    JsonData := GetCVFileList(CVFile);
    for I := 0 to JsonData.AsArray.Length - 1 do begin
      //Extract the image file name from the url
      FileName := Copy(JsonData.AsArray.S[I], LastDelimiter('/', JsonData.AsArray.S[i])+1,
        Length(JsonData.AsArray.S[I])-1);
      {$IFDEF UNICODE}
      RX.Subject := UTF8String(FileName);
      RX.RegEx := UTF8String(RegEx);
      {$ELSE}
      RX.Subject := FileName;
      RX.RegEx := RegEx;
      {$ENDIF}
      if RX.Match then begin
        OutFileName := BuildFileName(ExtractFilePath(CVFile), Prefix + FileName);
        GetContentFileFromCV(CVFile, JsonData.AsArray.S[I], FileName, OutFileName);
        FileList.Add(OutFileName);
      end;
    end;
  finally
    FreeAndNil(RX);
  end;
end;

procedure GetIniValuesForCV(IniName: string);
const
  IniSection = 'CVService';
  CertFromIni = 'Cert';
  ServerFromIni = 'Server';
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(IniName);
  try
    CertLocation := Ini.ReadString(IniSection, CertFromIni, '');
    CVServer := Ini.ReadString(IniSection, ServerFromIni, '');
  finally
    FreeAndNil(Ini);
  end;
end;

function ReplaceCVs(const ForeignID, ForeignType: Integer; FileDataSet: TDataSet; FileList: TStringList;
  const RegEx, IniName: string): TDataSet;
var
  I: Integer;
  CVInternalsList: TStringList;
  CVFileName: string;
  D: TADODataset;
  DownloadPath: string;
  DownloadFilePrefix: string;
begin
  Result := FileDataSet;
  Assert(Assigned(FileList));
  if FileList.Count < 1 then // no downloaded attachments, so nothing to do
    Exit;

  DownloadPath := ExtractFilePath(FileList.Strings[0]);
  if not CanWriteToDirectory(DownloadPath) then
    raise Exception.Create('Cannot write to the specified download folder: ' + DownloadPath);

  D := TADODataset.Create(FileDataSet.Owner); // Delphi magic frees D when it frees FileDataSet.Owner.
  D.FieldDefs.Assign(FileDataSet.FieldDefs);
  D.CreateDataSet;
  D.Open;
  GetIniValuesForCV(IniName);
  DownloadFilePrefix := GetAttachmentFileNamePrefix(ForeignType, ForeignID);

  while not FileDataSet.EOF do begin
    //create a custom row(s) with the internal contents of the cv file as the orig_filename
    if FileIsCV(FileDataSet.FieldByName('orig_filename').AsString) then begin
      CVFileName := DownloadPath + FileDataSet.FieldByName('orig_filename').AsString;
      CVInternalsList := TStringList.Create;
      try
        GetFilesFromCV(CVInternalsList, CVFileName, RegEx, DownloadFilePrefix);
        FileList.AddStrings(CVInternalsList);
        for I := 0 to CVInternalsList.Count - 1 do begin
          CopyRecord(FileDataSet, D);
          D.Edit;
          D.FieldByName('orig_filename').Value := ExtractFileName(CVInternalsList[I]);
          D.FieldByName('display_name').Value  := StringReplace(ExtractFileName(
            CVInternalsList[I]), DownloadFilePrefix, '', []) +
            ' from CV file ' + FileDataSet.FieldByName('display_name').AsString;
          D.Post;
        end;
      finally
        FreeAndNil(CVInternalsList);
      end;
    end else // copy the row over as is
      CopyRecord(FileDataSet, D);

    FileDataSet.Next;
  end;
  Result := D;
  (Result as TADODataset).IndexFieldNames := 'attach_date;orig_filename';
end;

function GetReportRegEx(Conn: TADOConnection): string;
const
  SQL = 'select value from configuration_data where name = ''CVReportRegex''';
var
  Dataset: TADOQuery;
begin
  Dataset := TADOquery.Create(nil);
  Result := 'manifest'; // default to most likely useful value
  try
    Dataset.Connection := Conn;
    DataSet.SQL.Text := SQL;

    DataSet.Open;
    if not DataSet.Eof then
      Result := Dataset.FieldByName('value').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

end.
