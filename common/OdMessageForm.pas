unit OdMessageForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TMessageForm = class(TForm)
    MessageMemo: TMemo;
    Panel: TPanel;
    CloseButton: TButton;
    SaveToFileButton: TButton;
    SaveDialog: TSaveDialog;
    procedure SaveToFileButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
  private
    procedure SetMessageText(const Value: String);
    function GetMessageText: String;
    procedure SaveToFile;
  public
    property MessageText: String read GetMessageText write SetMessageText;
  end;


procedure ShowMessageForm(const Caption: String; TextMessage: TStrings; const Warning: Boolean); overload;

implementation

uses QMConst;

{$R *.dfm}

procedure ShowMessageForm(const Caption: String; TextMessage: TStrings; const Warning: Boolean); overload;
var
  MessageForm: TMessageForm;
begin
  MessageForm := TMessageForm.Create(Application);
  try
    MessageForm.Caption := Caption;
    MessageForm.MessageText := TextMessage.Text;
    if Warning then
      MessageForm.MessageMemo.Color := WarningColor;
    MessageForm.ShowModal;
  finally
    FreeAndNil(MessageForm);
  end;
end;

{ TMessageForm }

procedure TMessageForm.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

function TMessageForm.GetMessageText: String;
begin
  Result := MessageMemo.Lines.Text;
end;

procedure TMessageForm.SaveToFile;
begin
  if SaveDialog.Execute then
    MessageMemo.Lines.SaveToFile(SaveDialog.FileName);
end;

procedure TMessageForm.SaveToFileButtonClick(Sender: TObject);
begin
  SaveToFile;
end;

procedure TMessageForm.SetMessageText(const Value: String);
begin
  MessageMemo.Lines.Text := Value;
end;

end.
