unit OdInternetUtil;

interface

uses Classes, Windows;

procedure SendEmailWithIniSettings(IniFileName, IniSection, EmailTo, EmailAddrBcc, Subject, Body: string;
  FileList: TStrings = nil; SilentlySkipNoToAddress: Boolean = False; const VerpID: string = ''); overload; //qm-9 qm-137     Orig

procedure SendEmailWithIniSettings(IniFileName, IniSection, EmailFrom, EmailTo, EmailAddrBcc, Subject, Body: string;
  FileList: TStrings = nil; SilentlySkipNoToAddress: Boolean = False; const VerpID: string = '');  overload; //qm-9 qm-137  New for SMS

function GetVERPBounceEmail(const ReturnAddr: string): string;
function GetVERPBounceID(const ReturnAddr: string): string;
function IsVERPReturnEmailAddress(const Address: string): Boolean;
function IsVERPReturnIDAddress(const Address: string): Boolean;
function GetURLPath(const URL: string): string;
procedure ComposeEmailTo(const EmailAddress: string);

implementation

uses
  SysUtils, ShellAPI,
  IdAttachmentFile, // Indy 10 is required
  IdSMTP, IdMessage, IniFiles, OdMiscUtils, OdExceptions, IdEMailAddress, IdURI,
  IdSSLOpenSSL, IdExplicitTLSClientServerBase, IdText;

procedure ConfigureSecureSMTP(SMTP: TIdSMTP; const Username, Password: string);
var
  I: Integer;
begin
  Assert(Assigned(SMTP), 'SMTP connection is undefined.');
  Assert(SMTP.Connected, 'Connect SMTP before configuring security.');
  for I := 0 to SMTP.Capabilities.Count-1 do begin
    // TODO: other types of authentication may also be used if available
    if StrContainsText('PLAIN', SMTP.Capabilities[i]) then begin
      SMTP.AuthType := satDefault;
      Break;
    end;
  end;
  SMTP.Username := Username;
  SMTP.Password := Password;
  if not SMTP.Authenticate then
    raise Exception.Create('Unable to authenticate SMTP user.');
end;

function VerpFromID(const FromAddr, VerpID: string): string;
var
  FromUser, FromDomain: string;
  Parser: TIdEMailAddressItem;
begin
  Assert(IsValidEmailAddress(FromAddr));
  Parser := TIdEMailAddressItem.Create;
  try
    Parser.Text := FromAddr;
    FromUser := Parser.User;
    FromDomain := Parser.Domain;
  finally
    FreeAndNil(Parser);
  end;
  Result := FromUser + '+' + VerpID + '@' + FromDomain;
  Assert(IsValidEmailAddress(Result));
end;

function VERPFromEmail(const FromAddr, ToAddr: string): string;
var
  ToUser, ToDomain: string;
  Parser: TIdEMailAddressItem;
begin
  Assert(IsValidEmailAddress(FromAddr));
  Assert(IsValidEmailAddress(ToAddr));
  Parser := TIdEMailAddressItem.Create;
  try
    Parser.Text := ToAddr;
    ToUser := Parser.User;
    ToDomain := Parser.Domain;
  finally
    FreeAndNil(Parser);
  end;
  Result := VerpFromID(FromAddr, ToUser + '=' + ToDomain);
  Assert(IsValidEmailAddress(Result));
end;

function GetVERPBounceEmail(const ReturnAddr: string): string;
var
  PlusPos: Integer;
  AtPos: Integer;
  EqualPos: Integer;
  BounceUser: string;
  BounceDomain: string;
begin
  Result := '';
  Assert(IsValidEmailAddress(ReturnAddr));
  if not IsVERPReturnEmailAddress(ReturnAddr) then
    Exit;

  PlusPos := Pos('+', ReturnAddr);
  EqualPos := Pos('=', ReturnAddr);
  AtPos := Pos('@', ReturnAddr);
  BounceUser := Copy(ReturnAddr, PlusPos + 1, EqualPos - PlusPos - 1);
  BounceDomain := Copy(ReturnAddr, EqualPos + 1, AtPos - EqualPos - 1);
  Result := BounceUser + '@' + BounceDomain;
  Assert(IsValidEmailAddress(Result));
end;

function GetVERPBounceID(const ReturnAddr: string): string;
var
  PlusPos: Integer;
  AtPos: Integer;
  Chars: Integer;
begin
  Result := '';
  Assert(IsValidEmailAddress(ReturnAddr));
  if not IsVERPReturnIDAddress(ReturnAddr) then
    Exit;

  PlusPos := Pos('+', ReturnAddr);
  AtPos := Pos('@', ReturnAddr);
  Assert(PlusPos > 1);
  Assert(AtPos > PlusPos);
  Chars := AtPos - PlusPos - 1;
  Result := Copy(ReturnAddr, PlusPos + 1, Chars);
end;

function IsVERPReturnEmailAddress(const Address: string): Boolean;
var
  PlusPos: Integer;
  AtPos: Integer;
  EqualPos: Integer;
begin
  PlusPos := Pos('+', Address);
  AtPos := Pos('@', Address);
  EqualPos := Pos('=', Address);
  Result := (PlusPos > 0) and (EqualPos > 0) and (AtPos > 0) and (PlusPos < EqualPos) and (PlusPos < AtPos) and (EqualPos < AtPos);
end;

function IsVERPReturnIDAddress(const Address: string): Boolean;
var
  PlusPos: Integer;
  AtPos: Integer;
begin
  PlusPos := Pos('+', Address);
  AtPos := Pos('@', Address);
  Result := (PlusPos > 0) and (AtPos > PlusPos);
end;

function GetURLPath(const URL: string): string;
var
  URI: TIdURI;
begin
  URI := TIdURI.Create(URL);
  try
    Result := URI.Path;
  finally
    FreeAndNil(URI);
  end;
end;

procedure SendEmail(const ToAddr, FromAddr, BccList, Subject, Body, SMTPHost: string; //QM-137 sr added BccList
  FileList: TStrings; SMTPUser: string; SMTPPassword: string; const VerpID:
  string; SMTPPort: Integer; UseSSL: Boolean; SendHTML: Boolean; var ErrorStr: string);
var
  NotifyMsg: TIdMessage;
  SMTP: TIdSMTP;
  SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
  i: Integer;
  wsHTML, wsPlain: TIdText;
  marker : string;
begin
  SMTP := TIdSMTP.Create(nil);
  NotifyMsg := TIdMessage.Create(nil);
  ErrorStr := '';
  marker := '';
  try
    SMTP.Host := SMTPHost;
    SMTP.ReadTimeout := 10000;
    SMTP.ConnectTimeout := 15000;
    SMTP.Port := SMTPPort;

    if UseSSL then begin
      SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create;
      SMTP.AuthType := satDefault;
      SMTP.IOHandler := SSLHandler;
      SMTP.UseTLS := utUseRequireTLS;
    end;

    if SendHTML then begin
      NotifyMsg.ContentType := 'multipart/mixed';
      wsHTML := TIdText.Create(NotifyMsg.MessageParts);
      wsHTML.ContentType := 'text/html';
      wsHTML.Body.Text := '<pre>' + Body + '</pre>';

      wsPlain := TIdText.Create(NotifyMsg.MessageParts);
      wsPlain.ContentType := 'text/plain';
      wsPlain.Body.Text := Body;
    end else begin
      NotifyMsg.Body.Text := Body;
    end;

    NotifyMsg.Subject := Subject;
    NotifyMsg.From.Text := FromAddr;
    NotifyMsg.Recipients.EMailAddresses := ToAddr;
    NotifyMsg.BccList.EMailAddresses:= BccList; //QM-137 sr

    if FileList <> nil then
      for i := 0 to FileList.Count - 1 do
        if FileExists(FileList[i]) then
          TIdAttachmentFile.Create(NotifyMsg.MessageParts, FileList[i]);

    //VERP Settings
    //VERP draft is here:
    //http://tools.ietf.org/html/draft-varshavchik-verp-smtpext-01

    //For now- commenting out VERP keyword support. It fails to send with this option if not supported by the SMTP server
    // with the error "Unsupported option: XVERP". The tagged envelope support appears to work with a mail server that
    //accepts tags. i.e. like tuffmail but not Gmail/googlemail servers
    //Per Remy Lebeau (Indy Developer), we can check if VERP is supported on the SMTP server like this:
    //SMTP.UseVerp := (SMTP.Capabilities.IndexOf('VERP') <> -1); //this causes the VERP or XVERP keyword to be appended to the envelope sender, i.e. 'MAIL FROM: <oasisdigitaldeveloper@gmail.com> XVERP'

    //Tag the envelope sender; even if the VERP keyword is not supported by the SMTP server then VERP can be supported via the tagged envelope sender method.
    if NotEmpty(VerpID) then
      NotifyMsg.Sender.Text := VerpFromID(FromAddr, VerpID)
    else
      NotifyMsg.Sender.Text := FromAddr;
    try
      SMTP.Connect;
      if NotEmpty(SMTPUser) and NotEmpty(SMTPPassword) then
        ConfigureSecureSMTP(SMTP, SMTPUser, SMTPPassword);

      SMTP.Send(NotifyMsg);
      SMTP.Disconnect;
    except on E: Exception do
      ErrorStr := 'SMTP Error: ' + E.Message;
    end;
    finally
    FreeAndNil(SMTP);
    FreeAndNil(NotifyMsg);
    if UseSSL then
      FreeAndNil(SSLHandler);
  end;
end;

procedure SendEmailWithIniSettings(IniFileName, IniSection, EmailTo, EmailAddrBcc, Subject, Body: string;      //QM-9  Original
    FileList: TStrings; SilentlySkipNoToAddress: Boolean; const VerpID: string);
var
  Host, EmailFrom: string;
  IniEmailTo,sBccList: string;   //QM-137
  Ini: TIniFile;
  Ignore: Boolean;
  UseSSL: Boolean;
  SMTPUser: string;
  SMTPPassword: string;
  SMTPPort: Integer;
  SendHTML: Boolean;
  RetErrorStr: string;  //QM-1076 EB Hp Restriction
  ErrorLogPath: string;
  ErrorLog: TStringList;
begin
  Ini := TIniFile.Create(IniFileName);
  try
    IniEmailTo := Ini.ReadString(IniSection, 'To', '');

    // Semicolon seperate the addresses if both are present
    if (EmailTo <> '') and (IniEmailTo <> '') then
      EmailTo := EmailTo + ';';

    EmailTo := EmailTo + IniEmailTo;
    sBccList := EmailAddrBcc; //QM-137
    Host := Ini.ReadString(IniSection, 'SMTPServer', '');

    Host := Ini.ReadString(IniSection, 'SMTPServer', '');
    EmailFrom := Ini.ReadString(IniSection, 'From', '');
    SMTPUser := Ini.ReadString(IniSection, 'SMTPUser', '');
    SMTPPassword := Ini.ReadString(IniSection, 'SMTPPassword', '');
    SMTPPort := Ini.ReadInteger(IniSection, 'SMTPPort', 25);
    UseSSL := Ini.ReadBool(IniSection, 'UseSSL', False);
    SendHTML := Ini.ReadBool(IniSection, 'SendHTML', False);
    // Ignore is for development/test machines where we don't want to generate emails
    Ignore := Ini.ReadBool(IniSection, 'Ignore', False);
  finally
    FreeAndNil(Ini);
  end;

  if Ignore then
    Exit;

  if IsEmpty(EmailTo) and SilentlySkipNoToAddress then
    Exit;

  // If not configured, complain
  if (EmailTo = '') or (Host = '') or (EmailFrom = '') then
    raise EODConfigError.Create('Could not load To/From/SMTPServer email configuration data from ' + IniFileName + ' / [' + IniSection + ']');

  SendEmail(Emailto, EmailFrom, sBccList, Subject, Body, Host, FileList, SMTPUser, SMTPPassword, VerpID, SMTPPort, UseSSL, SendHTML, RetErrorStr); //QM-137
end;

procedure SendEmailWithIniSettings(IniFileName, IniSection, EmailFrom, EmailTo, EmailAddrBcc, Subject, Body: string;  //New for SMS   //QM-137
    FileList: TStrings; SilentlySkipNoToAddress: Boolean; const VerpID: string);
var
  Host, sEmailFrom: string; //QM-9  SR
  IniEmailTo,sBccList: string;  //QM-137
  Ini: TIniFile;
  Ignore: Boolean;
  UseSSL: Boolean;
  SMTPUser: string;
  SMTPPassword: string;
  SMTPPort: Integer;
  SendHTML: Boolean;
  RetErrorStr: string;  //QM-1076 EB Hp Restriction
  ErrorLogPath: string;
  ErrorLog: TStringList;
  DateStr: string;
begin
  Ini := TIniFile.Create(IniFileName);
  sEmailFrom :=  EmailFrom;
  RetErrorStr := '';
  DateStr :=  formatdatetime('YYMMDD-hhnnssz', Now);
  try
    IniEmailTo := Ini.ReadString(IniSection, 'To', '');
    ErrorLogPath := Ini.ReadString('LogFile', 'Path', '');

    // Semicolon seperate the addresses if both are present
    if (EmailTo <> '') and (IniEmailTo <> '') then
      EmailTo := EmailTo + ';';

    EmailTo := EmailTo + IniEmailTo;
    sBccList := EmailAddrBcc; //QM-137
    Host := Ini.ReadString(IniSection, 'SMTPServer', '');

    if sEmailFrom='' then  //QM-9 QM-47  SR
    sEmailFrom := Ini.ReadString(IniSection, 'From', '');  //QM-9 QM-47  SR

    SMTPUser := Ini.ReadString(IniSection, 'SMTPUser', '');
    SMTPPassword := Ini.ReadString(IniSection, 'SMTPPassword', '');
    SMTPPort := Ini.ReadInteger(IniSection, 'SMTPPort', 25);
    UseSSL := Ini.ReadBool(IniSection, 'UseSSL', False);
    SendHTML := Ini.ReadBool(IniSection, 'SendHTML', False);
    // Ignore is for development/test machines where we don't want to generate emails
    Ignore := Ini.ReadBool(IniSection, 'Ignore', False);
  finally
    FreeAndNil(Ini);
  end;

  if Ignore then
    Exit;

  if IsEmpty(EmailTo) and SilentlySkipNoToAddress then
    Exit;

  // If not configured, complain
  if (EmailTo = '') or (Host = '') or (sEmailFrom = '') then //QM-47  SR
    raise EODConfigError.Create('Could not load To/From/SMTPServer email configuration data from ' + IniFileName + ' / [' + IniSection + ']');

  SendEmail(Emailto, sEmailFrom, EmailAddrBcc, Subject, Body, Host, FileList, SMTPUser, SMTPPassword, VerpID, SMTPPort, UseSSL, SendHTML, RetErrorStr); //QM-47 QM-137 SR
  if RetErrorStr <> '' then begin
       ErrorLog := TStringList.Create;
    try
       ErrorLog.Add(RetErrorStr);
       ErrorLog.Add('---------------------------------');
       ErrorLog.Add('SMTPUser: ' + SMTPUser);
       ErrorLog.Add('SMTPPort: ' + IntToStr(SMTPPort));
       ErrorLog.Add('UseSSL: ' + BoolToStr(UseSSL));
       ErrorLog.Add('SendHTML: ' + BoolToStr(SendHTML));
       ErrorLog.Add('Host: ' + Host);
       ErrorLog.Add('EmailTo: ' + EmailTo);
       ErrorLog.Add('Subject: ' + Subject);
       ErrorLog.Add('Body: ');
       ErrorLog.Add(Body);
       ErrorLog.SaveToFile(ErrorLogPath + '/' + IniSection + 'ErrorLog_' + DateStr + '.txt');  //QM-1076 EB HP Restriction
    finally
      FreeAndNil(ErrorLog);
    end;
  end;
end;

procedure ComposeEmailTo(const EmailAddress: string);
begin
  if not (StrContains('@', EmailAddress) and StrContains('.', EmailAddress)) then
    EOdDataEntryError.Create('Invalid destination email address: ' + EmailAddress);
  ShellExecute(0, 'open', PChar('mailto:' + EmailAddress), nil, nil, SW_SHOWNORMAL);
end;

end.
