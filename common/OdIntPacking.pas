unit OdIntPacking;

interface

type
  IntArray = array of Integer;

function PackIntArray(Data: IntArray): string;
function UnPackIntArray(PackedData: string): IntArray;
procedure TestIt;

implementation

{$R-}

uses
  Math, SysUtils, JclSysUtils;

function PackIntArray(Data: IntArray): string;
var
  L: Integer;
begin
  L := (High(Data) - Low(Data) + 1) * SizeOf(Integer);
  SetLength(Result, L);
  System.Move(Data[Low(Data)], Result[1], L);
end;

function UnPackIntArray(PackedData: string): IntArray;
var
  L: Integer;
begin
  L := Length(PackedData) div SizeOf(Integer);
  SetLength(Result, L);
  System.Move(PackedData[1], Result[0], Length(PackedData));
end;

{
function PackIntArray(Data: IntArray): string;
var
  Accum: string;
  Str: TStringStream;
  L: Integer;
begin
  L := (High(Data) - Low(Data) + 1) * SizeOf(Integer);
  SetLength(Accum, L+100);

  Str := TStringStream.Create(Accum);
  try
    Str.Write(Data[Low(Data)], L);

    Result := Str.DataString;

  finally
    Str.Free;
  end;
end;
}

procedure TestIt;
var
  Iter, Len, I: Integer;
  Tix, Saved, NewTix: IntArray;
  StringData: string;
begin
  Randomize;

  for Iter := 1 to 1000 do begin
    Len := RandomRange(0, 4000);

    SetLength(Tix, Len);
    SetLength(Saved, Len);
    for I := 0 to Len-1 do begin
      Tix[I] := RandomRange(1000000, 1000000 + 50000);
    end;

    // We feed it sorted data.
    SortDynArray(Tix, SizeOf(Tix[0]), DynArrayCompareInteger);

    for I := 0 to Len-1 do begin
      Saved[I] := Tix[I];
    end;

    StringData := PackIntArray(Tix);

    NewTix := UnPackIntArray(StringData);

    for I := Low(Saved) to High(Saved) do begin
      if Saved[I] <> NewTix[I] then
        raise Exception.Create('Failed');
    end;
  end;
end;


end.
