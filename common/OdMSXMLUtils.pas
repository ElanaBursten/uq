unit OdMSXMLUtils;

interface

uses MSXML2_TLB;

const
  PrettyPrintingXslt = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> <xsl:output method="xml" encoding="UTF-8" %s/> ' +
    '<xsl:param name="indent-increment" select="''   ''" /> <xsl:template match="*"> <xsl:param name="indent" select="''&#xA;''"/> <xsl:value-of select="$indent"/> <xsl:copy> <xsl:copy-of select="@*" /> ' +
    '<xsl:apply-templates> <xsl:with-param name="indent" select="concat($indent, $indent-increment)"/> </xsl:apply-templates> <xsl:if test="*"> <xsl:value-of select="$indent"/> </xsl:if> </xsl:copy> </xsl:template> ' +
    '<xsl:template match="comment()|processing-instruction()"> <xsl:copy /> </xsl:template> ' +
    '<!-- WARNING: this is dangerous. Handle with care --> <xsl:template match="text()[normalize-space(.)='''']"/> </xsl:stylesheet>';

function GetAttributeTextFromNode(Node: IXMLDOMNode;
  const AttributeName: string; var AttributeText: string): Boolean;

function GetNodeText(RootNode: IXMLDOMNode; const QueryString: WideString;
  var NodeText: WideString): Boolean;

function IsValidXML(const XMLText: string): Boolean;
function ParseXml(const XmlText: string): IXMLDomDocument;
function LoadXMLFile(const FileName: string): IXMLDomDocument;

function PrettyPrintXml(const Doc: IXMLDomDocument; const CDATASectionElements: string=''): string;
function FormatXmlWithXslt(const SourceDoc, StyleDoc: IXMLDomDocument): string;
function XHTMLEncode( const sRawValue: string): string; //QMANTWO-691     -sr

implementation

uses SysUtils, Classes, OdMiscUtils, Variants, ActiveX;

function GetAttributeTextFromNode(Node: IXMLDOMNode;
  const AttributeName: string; var AttributeText: string): Boolean;
var
  Attribute: IXMLDOMAttribute;
begin
  Assert(Assigned(Node));
  Assert(AttributeName <> '');
  Result := False;
  AttributeText := '';
  if not Supports(Node, IXMLDOMNode) then
    Exit;

  Attribute := (Node as IXMLDOMElement).getAttributeNode(AttributeName);
  if not Assigned(Attribute) then
    Exit;
  AttributeText := Attribute.text;
  Result := True;
end;

function GetNodeText(RootNode: IXMLDOMNode; const QueryString: WideString;
  var NodeText: WideString): Boolean;
var
  Node: IXMLDOMNode;
begin
  Result := False;
  Assert(Assigned(RootNode));
  NodeText := '';
  Assert(Trim(QueryString) <> '');
  Node := RootNode.selectSingleNode(QueryString);
  if Assigned(Node) then begin
    NodeText := Node.text;
    Result := True;
  end;
end;

function IsValidXML(const XMLText: string): Boolean;
var
  Doc: IXMLDomDocument;
begin
  Result := False;
  if XMLText <> '' then begin
    Doc := CoDOMDocument.Create;
    Doc.Async := False;
    if Doc.loadXML(XmlText) then
      Result := True;
  end;
end;

function ParseXml(const XmlText: string): IXMLDomDocument;
begin
  Result := CoDOMDocument.Create;
  Result.Async := False;

  if XmlText = '' then
    raise Exception.Create('Empty XML document');
  if not Result.loadXML(XmlText) then begin
    SaveDebugFile('XMLDebug.xml', XmlText);
    raise Exception.Create('Invalid XML document: ' + Copy(XmlText, 1, 100));
  end;
end;

function LoadXMLFile(const FileName: string): IXMLDomDocument;
var
  Xml: TStringList;
begin
  if not FileExists(FileName) then
    raise Exception.Create(FileName + ' does not exist');
  Xml := TStringList.Create;
  try
    Xml.LoadFromFile(FileName);
    Result := ParseXml(Xml.Text);
  finally
    FreeAndNil(Xml);
  end;
end;

function PrettyPrintXml(const Doc: IXMLDomDocument; const CDATASectionElements: string=''): string;
var
  XSLDoc: IXMLDOMDocument;
  CDATASections: string;
begin
  CDATASections := '';
  if NotEmpty(CDATASectionElements) then
    CDATASections := 'cdata-section-elements="' + CDATASectionElements + '"';
  XSLDoc := CoFreeThreadedDOMDocument.Create;
  XSLDoc.async := False;
  XSLDoc.loadXML(Format(PrettyPrintingXslt, [CDataSections]));
  Result := FormatXmlWithXslt(Doc, XSLDoc);
end;

function FormatXmlWithXslt(const SourceDoc, StyleDoc: IXMLDomDocument): string;
var
  Str: TStringStream;
  StreamAdapter: IStream;
begin
  // Use of the stream and transformNodeToObject is needed to avoid an issue with
  // MSXML which restricts it to UTF-16 when used in a simpler calling method.
  Str := TStringStream.Create('');
  StreamAdapter := TStreamAdapter.Create(Str);
  try
    SourceDoc.transformNodeToObject(StyleDoc, StreamAdapter as IStream);
    Result := Str.DataString;
  finally
    FreeAndNil(Str);
  end;
end;

function XHTMLEncode( const sRawValue: string): string; //QMANTWO-691     -sr
var
  Sp, Rp: PChar;
begin
  SetLength( result, Length( sRawValue) * 10);
  Sp := PChar( sRawValue);
  Rp := PChar( result);
  while Sp^ <> #0 do
  begin
    case Sp^ of
      '&': begin
             FormatBuf( Rp^, 10, '&amp;', 10, []);
             Inc(Rp,4);
           end;
      '<',
      '>': begin
             if Sp^ = '<' then
               FormatBuf(Rp^, 8, '&lt;', 8, [])
             else
               FormatBuf(Rp^, 8, '&gt;', 8, []);
             Inc(Rp,3);
           end;
      '"': begin
             FormatBuf(Rp^, 12, '&quot;', 12, []);
             Inc(Rp,5);
           end;
      '''': begin
             FormatBuf(Rp^, 12, '&apos;', 12, []);
             Inc(Rp,5);
           end;
    else
      Rp^ := Sp^
    end;
    Inc(Rp);
    Inc(Sp);
  end;
  SetLength( result, Rp - PChar( result))
end;

end.
