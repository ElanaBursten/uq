unit OdTimerDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TTimerDialogForm = class(TForm)
    Timer: TTimer;
    MessageLabel: TLabel;
    ButtonYes: TButton;
    ButtonNo: TButton;
    procedure TimerTimer(Sender: TObject);
    procedure ButtonYesClick(Sender: TObject);
    procedure ButtonNoClick(Sender: TObject);
  private
    CountDown: Integer;
  public
    function Execute(Title, Message: string; TimerInterval: Integer = 5): Boolean;
  end;

implementation

uses OdMiscUtils;

{$R *.dfm}

function TTimerDialogForm.Execute(Title, Message: string; TimerInterval: Integer = 5): Boolean;
begin
  Caption := Title;
  MessageLabel.Caption := Message;
  CountDown := TimerInterval;
  ButtonYes.Caption := Format('&Yes (%d)', [TimerInterval]);
  Timer.Enabled := True;
  ShowModal;
  Timer.Enabled := False;
  Result := ModalResult = mrOk;
end;

procedure TTimerDialogForm.TimerTimer(Sender: TObject);
begin
  Dec(CountDown);
  ButtonYes.Caption := Format('&Yes (%d)', [CountDown]);
  Application.ProcessMessages;
  if CountDown <= 0 then
    ButtonYes.Click;
end;

procedure TTimerDialogForm.ButtonYesClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TTimerDialogForm.ButtonNoClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

end.
