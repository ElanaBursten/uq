unit OdWebUpdate;

interface

uses IniFiles;

type
  TFileDetails = record
    Name: string;
    Directory: string;
    URL: string;
    DateTime: TDateTime;
    Size: Integer;
    CRC32: Int64;
    MajorVersion: Integer;
    MinorVersion: Integer;
    ReleaseVersion: Integer;
    BuildVersion: Integer;
    LocalDirectory: string;
    NeedsUpdate: Boolean;
    CRCVerifyOnly: Boolean;
  end;

  TOdWebUpdate = class(TObject)
  private
    FURL: string;
    FIniFile: TMemIniFile;
    FUpdateFiles: array of TFileDetails;
    FServerUpdateDateTime: string;
    FLastUpdateDateTime: string;
    FDownloadedUpdate: Boolean;
    FApplicationMutex: string;
    FMapSysToApp: Boolean;
    procedure CheckReadyToUpdate;
    procedure RetrieveUpdateIniFile;
    procedure ValidateIniFile;
    procedure PerformUpdate;
    procedure ReadAndVerifyFileSections(NumberOfFiles: Cardinal);
    function GetServerUpdateDateTime: string;
    procedure SetLastUpdateDateTime(const Value: string);
    function IsUpdateNecessary: Boolean;
    function GetUpdateDirectory: string;
    function FileNeedsUpdate(var AFile: TFileDetails): Boolean;
    procedure DownloadUpdateFile(const AFile: TFileDetails);
    procedure SaveIniFile;
    procedure ApplyAllUpdates;
    procedure CreateUpdateDirectory;
    function TranslateDirectory(const Dir: string): string;
    function GetUpdateExeFileName: string;
    function GetUpdateIniFileName: string;
    function GetDownloadedUpdate: Boolean;
  protected
    FUpdateComplete: Boolean;
    procedure TerminateApplication; virtual;
  public
    property URL: string read FURL write FURL;
    property ServerUpdateDateTime: string read GetServerUpdateDateTime;
    property LastUpdateDateTime: string write SetLastUpdateDateTime;
    property ApplicationMutex: string read FApplicationMutex write FApplicationMutex;
    property DownloadedUpdate: Boolean read GetDownloadedUpdate;
    property MapSysToApp: Boolean read FMapSysToApp write FMapSysToApp;
    function Update: Boolean; virtual;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses SysUtils, Classes, Windows, OdWinInet, Forms, Dialogs, DateUtils, Math,
  OdIsoDates, OdMiscUtils, JclSysInfo, JclFileUtils, OdWebUpdateShared,
  ShellAPI, OdExceptions;

{ TOdWebUpdate }

procedure TOdWebUpdate.ApplyAllUpdates;
var
  ExecuteResult: THandle;
begin
  ExecuteResult := ShellExecute(0, 'open', PChar(GetUpdateExeFileName),
    PChar('"'+GetUpdateIniFileName+'"'), nil, SW_SHOW);
  if ExecuteResult <= 32 then
    raise Exception.Create('Unable to execute ' + GetUpdateExeFileName);
end;

procedure TOdWebUpdate.CheckReadyToUpdate;
var
  MutexHandle: Cardinal;
begin
  Assert(Assigned(FIniFile));
  if not SameText(Copy(FURL, 1, 8), 'https://') then
    raise Exception.Create('Invalid HTTP update URL specified: ' + FURL);
  if ApplicationMutex = '' then
    raise Exception.Create('An ApplicationMutex is required to perform an update');
  MutexHandle := CreateMutex(nil, False, PChar(ApplicationMutex));
  if not (GetLastError = ERROR_ALREADY_EXISTS) then
  begin
    ReleaseMutex(MutexHandle);
    raise Exception.Create('Required ApplicationMutex '+ApplicationMutex+' does not exist');
  end;
  if not FileExists(GetUpdateExeFileName) then
    raise Exception.Create('The update application does not exist: ' + GetUpdateExeFileName);
end;

constructor TOdWebUpdate.Create;
begin
  inherited;
  FIniFile := TMemIniFile.Create('');
end;

procedure TOdWebUpdate.CreateUpdateDirectory;
begin
  if not DirectoryExists(GetUpdateDirectory) then
    OdForceDirectories(GetUpdateDirectory);
end;

destructor TOdWebUpdate.Destroy;
begin
  FreeAndNil(FIniFile);
  inherited;
end;

procedure TOdWebUpdate.DownloadUpdateFile(const AFile: TFileDetails);
var
  DownloadFileName: string;
  FileStream: TFileStream;
  DownloadedFileCRC: Cardinal;
  DownloadedFileCRC64: Int64;
const
  BadCRC = 'The contents of the file %s are corrupt after downloading.  Please retry the download.  (Local CRC: %d, Server CRC: %d)';
begin
  CreateUpdateDirectory;
  DownloadFileName := IncludeTrailingPathDelimiter(GetUpdateDirectory) + AFile.Name;
  FileStream := TFileStream.Create(DownloadFileName, fmCreate or fmShareExclusive);
  try
    OdHttpGetStream(AFile.URL, FileStream);
  finally
    FreeAndNil(FileStream);
  end;
  Assert(FileExists(DownloadFileName));
  if AFile.DateTime <> 0 then
    FileSetDate(DownloadFileName, DateTimeToFileDate(AFile.DateTime));
  if AFile.CRC32 <> -1 then
    if GetFileCRC32(DownloadFileName, DownloadedFileCRC) then begin
      DownloadedFileCRC64 := DownloadedFileCRC;
      if DownloadedFileCRC64 <> AFile.CRC32 then
        raise EOdCorruptFile.Create(Format(BadCRC, [DownloadFileName, DownloadedFileCRC64, AFile.CRC32]));
    end
    else
      raise Exception.Create('Unable to get CRC32 value for file: ' + DownloadFileName);

  FDownloadedUpdate := True;
end;

function TOdWebUpdate.FileNeedsUpdate(var AFile: TFileDetails): Boolean;
var
  CheckSize: Boolean;
  CheckCRC32: Boolean;
  CheckVersion: Boolean;
  CheckDateTime: Boolean;
  LocalFile: string;
  LocalFileDateTime: TDateTime;
  LocalFileVersion: TVersionNumber;
  RemoteFileVersion: TVersionNumber;
  LocalFileCRC32: Cardinal;
begin
  Result := False;
  CheckSize := AFile.Size > -1;
  CheckCRC32 := (AFile.CRC32 <> -1) and (not AFile.CRCVerifyOnly);
  CheckDateTime := AFile.DateTime <> 0;
  CheckVersion := AFile.MajorVersion > -1;

  LocalFile := TranslateDirectory(AFile.Directory) + AFile.Name;
  LocalFile := ExpandFileName(LocalFile);
  AFile.LocalDirectory := ExtractFilePath(LocalFile);
  if not FileExists(LocalFile) then
  begin
    Result := True;
    Exit;
  end;

  if CheckSize then
  begin
    if JclFileUtils.GetSizeOfFile(LocalFile) <> AFile.Size then
    begin
      Result := True;
      Exit;
    end;
  end;

  if CheckDateTime then
  begin
    // FAT partition dates are only accurate within a 2 second tolerance
    LocalFileDateTime := GetFileDateTime(LocalFile);
    if SecondsBetween(LocalFileDateTime, AFile.DateTime) > 2 then
    begin
      Result := True;
      Exit;
    end;
  end;

  if CheckVersion then
  begin
    if not GetFileVersionNumber(LocalFile, LocalFileVersion) then
    begin
      Result := True;
      Exit;
    end;
    RemoteFileVersion.Major := Max(AFile.MajorVersion, 0);
    RemoteFileVersion.Minor := Max(AFile.MinorVersion, 0);
    RemoteFileVersion.Release := Max(AFile.ReleaseVersion, 0);
    RemoteFileVersion.Build := Max(AFile.BuildVersion, 0);

    if CompareVersionNumber(LocalFileVersion, RemoteFileVersion) < 0 then
    begin
      Result := True;
      Exit;
    end;
  end;

  if CheckCRC32 then
  begin
    if not GetFileCRC32(LocalFile, LocalFileCRC32) then
      raise Exception.Create('Unable to calculate CRC32 for ' + LocalFile);
    if LocalFileCRC32 <> AFile.CRC32 then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TOdWebUpdate.GetDownloadedUpdate: Boolean;
begin
  if FUpdateComplete then
    Result := FDownloadedUpdate
  else
    Result := False;
end;

function TOdWebUpdate.GetServerUpdateDateTime: string;
begin
  if FUpdateComplete then
    Result := FServerUpdateDateTime
  else
    raise Exception.Create('LastUpdate is not available until after the update process has completed');
end;

function TOdWebUpdate.GetUpdateDirectory: string;
begin
  Result := ExtractFilePath(Application.ExeName);
  Result := IncludeTrailingPathDelimiter(Result) + UpdateDownloadDirectory;
end;

function TOdWebUpdate.GetUpdateExeFileName: string;
begin
  Result := Application.ExeName;
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(Result));
  Result := Result + DoUpdateExeName;
end;

function TOdWebUpdate.GetUpdateIniFileName: string;
begin
  Result := IncludeTrailingPathDelimiter(GetUpdateDirectory) +
            UpdateIniFileName;
end;

function TOdWebUpdate.IsUpdateNecessary: Boolean;
var
  ServerDate: TDateTime;
  LastDate: TDateTime;
begin
  Result := True;
  if IsoIsValidDateTime(FServerUpdateDateTime) then
  begin
    if IsoIsValidDateTime(FLastUpdateDateTime) then
    begin
      ServerDate := IsoStrToDateTime(FServerUpdateDateTime);
      // TODO: Last update tracking does not work yet, since the app never sets this
      LastDate := IsoStrToDateTime(FLastUpdateDateTime);
      Result := ServerDate > LastDate;
    end;
  end;
end;

procedure TOdWebUpdate.PerformUpdate;
var
  i: Integer;
begin
  Assert(Assigned(FIniFile));
  if Length(FUpdateFiles) < 1 then
    Exit;
  for i := 0 to Length(FUpdateFiles) - 1 do
  begin
    FUpdateFiles[i].NeedsUpdate := FileNeedsUpdate(FUpdateFiles[i]);
    if FUpdateFiles[i].NeedsUpdate then
      DownloadUpdateFile(FUpdateFiles[i]);
  end;
  if FDownloadedUpdate then
  begin
    SaveIniFile;
    ApplyAllUpdates;
    TerminateApplication;
  end;
end;

procedure TOdWebUpdate.ReadAndVerifyFileSections(NumberOfFiles: Cardinal);

  procedure VerifyFileSection(Section: string);

    procedure VerifyOneOfValuesExist(const Values: array of string; const Msg: string);
    var
      i: Integer;
    begin
      for i := 0 to Length(Values) - 1 do
        if FIniFile.ValueExists(Section, Values[i]) then
          Exit;
      raise Exception.Create(Msg);
    end;

  begin
    if not FIniFile.SectionExists(Section) then
      raise Exception.Create('Unable to find file update section ' + Section);
    VerifyOneOfValuesExist([FileNameIdent], 'Name not specified for ' + Section);
    VerifyOneOfValuesExist([FileURLIdent], 'URL not specified for ' + Section);
    VerifyOneOfValuesExist([FileDateTimeIdent, FileSizeIdent, FileCRC32Ident, FileMajorVersionIdent], 'File update conditions not specified for ' + Section);
    if FIniFile.ValueExists(Section, FileDateTimeIdent) then
      if not IsoIsValidDateTime(FIniFile.ReadString(Section, FileDateTimeIdent, '')) then
        raise Exception.Create('Invalid ISO date/time for ' + Section)
  end;

var
  i: Integer;
  FileSection: string;
begin
  SetLength(FUpdateFiles, NumberOfFiles);
  for i := 0 to NumberOfFiles - 1 do
  begin
    FileSection := FileDetailsPrefix + IntToStr(i);
    VerifyFileSection(FileSection);
    FUpdateFiles[i].Name := FIniFile.ReadString(FileSection, FileNameIdent, '');
    FUpdateFiles[i].Directory := FIniFile.ReadString(FileSection, FileDirectoryIdent, '');
    FUpdateFiles[i].URL := FIniFile.ReadString(FileSection, FileURLIdent, '');
    if FIniFile.ValueExists(FileSection, FileDateTimeIdent) then
      FUpdateFiles[i].DateTime := IsoStrToDateTime(FIniFile.ReadString(FileSection, FileDateTimeIdent, ''))
    else
      FUpdateFiles[i].DateTime := 0;
    FUpdateFiles[i].Size := FIniFile.ReadInteger(FileSection, FileSizeIdent, -1);
    FUpdateFiles[i].CRC32 := StrToInt64(Trim(FIniFile.ReadString(FileSection, FileCRC32Ident, '-1')));
    FUpdateFiles[i].CRCVerifyOnly := FIniFile.ReadBool(FileSection, FileCRCVerifyOnlyIdent, False);
    FUpdateFiles[i].MajorVersion := FIniFile.ReadInteger(FileSection, FileMajorVersionIdent, -1);
    FUpdateFiles[i].MinorVersion := FIniFile.ReadInteger(FileSection, FileMinorVersionIdent, -1);
    FUpdateFiles[i].ReleaseVersion := FIniFile.ReadInteger(FileSection, FileReleaseVersionIdent, -1);
    FUpdateFiles[i].BuildVersion := FIniFile.ReadInteger(FileSection, FileBuildVersionIdent, -1);
  end;
end;

procedure TOdWebUpdate.RetrieveUpdateIniFile;
var
  Strings: TStringList;
begin
  Assert(Assigned(FIniFile));
  Strings := TStringList.Create;
  try
    Strings.Text := OdHttpGetString(FURL);
    FIniFile.SetStrings(Strings);
  finally
    FreeAndNil(Strings);
  end;
end;

procedure CopyIniSection(FromIni, ToIni: TCustomIniFile; const FromSec, ToSec: string);
var
  SL: TStringList;
  i: Integer;
begin
  Assert(Assigned(FromIni) and Assigned(ToIni));
  SL := TStringList.Create;
  try
    FromIni.ReadSectionValues(FromSec, SL);
    for i := 0 to SL.Count - 1 do
      ToIni.WriteString(ToSec, SL.Names[i], SL.Values[SL.Names[i]]);
  finally
    FreeAndNil(SL);
  end;
end;

procedure TOdWebUpdate.SaveIniFile;
var
  Strings: TStringList;
  i: Integer;
  FileSection: string;
  NumFiles: Integer;
  UpdateIni: TIniFile;
  SrcSection: string;
  DestSection: string;
begin
  Assert(Assigned(FIniFile));
  for i := 0 to Length(FUpdateFiles) - 1 do
  begin
    FileSection := FileDetailsPrefix + IntToStr(i);
    FIniFile.WriteString(FileSection, FileLocalDirectoryIdent, FUpdateFiles[i].LocalDirectory)
  end;
  FIniFile.WriteString(ConfigSection, MainApplicationIdent, Application.ExeName);
  FIniFile.WriteString(ConfigSection, MainApplicationMutexIdent, ApplicationMutex);

  CreateUpdateDirectory;
  if FileExists(GetUpdateIniFileName) then
    if not SysUtils.DeleteFile(GetUpdateIniFileName) then
      raise Exception.Create('Unable to delete update file: ' + GetUpdateIniFileName);
  Strings := nil;
  UpdateIni := TIniFile.Create(GetUpdateIniFileName);
  try
    CopyIniSection(FIniFile, UpdateIni, ConfigSection, ConfigSection);
    NumFiles := 0;
    for i := 0 to Length(FUpdateFiles) - 1 do
    begin
      if FUpdateFiles[i].NeedsUpdate then
      begin
        SrcSection  := FileDetailsPrefix + IntToStr(i);
        DestSection := FileDetailsPrefix + IntToStr(NumFiles);
        CopyIniSection(FIniFile, UpdateIni, SrcSection, DestSection);
        Inc(NumFiles);
      end;
      UpdateIni.WriteInteger(ConfigSection, NumberOfFilesIdent, NumFiles);
    end;
    UpdateIni.UpdateFile;
  finally
    FreeAndNil(UpdateIni);
    FreeAndNil(Strings);
  end;
end;

procedure TOdWebUpdate.SetLastUpdateDateTime(const Value: string);
begin
  if IsoIsValidDateTime(Value) then
    FLastUpdateDateTime := Value
  else
    raise Exception.Create('Invalid ISO date/time: ' + Value);
end;

procedure TOdWebUpdate.TerminateApplication;
begin
  Application.Terminate;
end;

function TOdWebUpdate.TranslateDirectory(const Dir: string): string;
var
  Directory: string;
begin
  Directory := Trim(Dir);
  if SameText('{app}', Directory) then
    Result := ExtractFilePath(Application.ExeName)
  else if SameText('{win}', Directory) then
    Result := GetWindowsFolder
  else if SameText('{sys}', Directory) then begin
    if MapSysToApp then
      Result := ExtractFilePath(Application.ExeName)
    else
      Result := GetWindowsSystemFolder;
  end
  else if PathIsAbsolute(Directory) then
    Result := Directory
  else
    Result := PathAppend(ExtractFilePath(Application.ExeName), Directory);
  Result := IncludeTrailingPathDelimiter(Result);
end;

function TOdWebUpdate.Update: Boolean;
begin
  CheckReadyToUpdate;
  RetrieveUpdateIniFile;
  ValidateIniFile;
  if IsUpdateNecessary then
    PerformUpdate;
  FUpdateComplete := True;
  Result := True;
end;

procedure TOdWebUpdate.ValidateIniFile;
var
  IniVersion: Double;
  NumberOfFiles: Integer;
begin
  Assert(Assigned(FIniFile));
  if not FIniFile.SectionExists(ConfigSection) then
    raise Exception.CreateFmt('%s is not a web update ini file.  It is missing the %s section.', [FURL, ConfigSection]);

  IniVersion := FIniFile.ReadFloat(ConfigSection, FileFormatVersionIdent, -1);
  if not Trunc(IniVersion) = Trunc(SupportedIniFileFormatVersion) then
    raise Exception.CreateFmt('Web update only supports version %f files, not %f', [SupportedIniFileFormatVersion, IniVersion]);

  if not FIniFile.ValueExists(ConfigSection, NumberOfFilesIdent) then
    raise Exception.Create('The number of updatable files was not specified using ' + NumberOfFilesIdent);

  FServerUpdateDateTime := Trim(FIniFile.ReadString(ConfigSection, LastUpdateIdent, ''));
  if FServerUpdateDateTime <> '' then
  begin
    if not IsoIsValidDateTime(FServerUpdateDateTime) then
      raise Exception.CreateFmt('Invalid ISO date/time in %s: %s', [LastUpdateIdent, FServerUpdateDateTime]);
  end;

  NumberOfFiles := FIniFile.ReadInteger(ConfigSection, NumberOfFilesIdent, 0);
  if NumberOfFiles > 0 then
    ReadAndVerifyFileSections(NumberOfFiles);
end;

end.
