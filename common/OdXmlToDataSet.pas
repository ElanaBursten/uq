unit OdXmlToDataSet;

interface

uses DB, DBISAMTb, MSXML2_TLB, OdDBISAMUtils, OdDataSetToXml;

procedure ConvertXMLToDataSet(XML: IXMLDOMDocument; const Def: TColumnList;
  Table: TDBISAMTable; CustomizeDataSet: TCustomizeDataSetProc = nil);

implementation

uses SysUtils, Variants, OdIsoDates;

function AttrValue(Element: IXMLDOMElement; const AttrName: string; const FieldType: TFieldType): Variant;
var
  AttrNode: IXMLDomNode;
  NodeText: string;
begin
  Result := Null;
  AttrNode := Element.attributes.getNamedItem(AttrName);
  if Assigned(AttrNode) then begin
    NodeText := AttrNode.text;

    if NodeText <> XmlNullValue then begin
      if FieldType = ftDateTime then begin
        if NodeText = '' then
          Result := Null
        else begin
          try
            Result := FormatDateTime('c', IsoStrToDateTime(NodeText));
          except
            // ignore, let it remain Null
          end;
        end;
      end
      else // Not a date type field
        Result := NodeText
    end;
  end;
end;

procedure ConvertXMLToDataSet(XML: IXMLDOMDocument; const Def: TColumnList;
  Table: TDBISAMTable; CustomizeDataSet: TCustomizeDataSetProc);
var
  Nodes: IXMLDOMNodeList;
  Elem: IXMLDOMElement;
  i, j: Integer;
  ColCount: Cardinal;
  ColInfo: TColumnInfo;
  FieldName: string;
begin
  Assert(Assigned(Def));
  Assert(Assigned(Table));

  Table.DisableControls;
  try
    Table.Close;
    if Table.Exists then
      Table.DeleteTable;

    CreateColumns(Def, Table, CustomizeDataSet);

    Assert(Table.Eof and Table.Bof);
    Table.Open;
    if Assigned(CustomizeDataSet) then
      CustomizeDataSet(Table, ceAfterFieldDefs);

    Nodes := XML.SelectNodes('//row');
    for i := 0 to Nodes.Length - 1 do begin
      Elem := Nodes[i] as IXMLDOMElement;
      ColCount := Def.ColumnCount;

      Table.Insert;
      for j := 0 to ColCount - 1 do
      begin
        Def.GetColumnInfo(ColInfo, j);
        FieldName := ColInfo.FieldName;
        Assert(not (Trim(FieldName) = ''));
        Table.FieldByName(FieldName).Value := AttrValue(Elem, FieldName, ColInfo.FieldType);
      end;
      if Assigned(CustomizeDataSet) then
        CustomizeDataSet(Table, ceDataRow);
      Table.Post;
    end;
  finally
    Table.EnableControls;
  end;
end;

end.
