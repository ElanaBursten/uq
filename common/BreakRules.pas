unit BreakRules;

interface

uses
  Controls;   // For unknown reasons, Delphi defines TTime in Controls.pas

type
  TBreakRulesChecker = class
  public
    procedure Clear; virtual; abstract;
    procedure Add(const AStartTime, AEndTime: TTime); virtual; abstract;
    function Check: Boolean; virtual; abstract;
  end;

// Instantiate a Break Rule checker given a rule name and params:
function CreateBreakRuleChecker(const RuleType: string;
  const BreakLength, TimeForBreak: Integer): TBreakRulesChecker;

implementation

// The implementation details, including the classes users,
// are all hidden from the module interface.

uses
  SysUtils, Classes;

type
  TBaseBreakRulesChecker = class(TBreakRulesChecker)
    StartTime, EndTime: array[0..50] of TTime;
    N: Integer;
    FBreakLength: Integer;
    FTimeForBreak: Integer;
    function Minutes(const StartTime, EndTime: TTime): Integer;
  public
    constructor Create(const BreakLength, TimeForBreak: Integer);
    procedure Add(const AStartTime, AEndTime: TTime); override;
    procedure Clear; override;
  end;

  TAfterRuleChecker = class(TBaseBreakRulesChecker)
  public
    function Check: Boolean; override;
  end;

  TEveryRuleChecker = class(TBaseBreakRulesChecker)
  public
    function Check: Boolean; override;
  end;

  TLunchRuleChecker = class(TBaseBreakRulesChecker)
  public
    function Check: Boolean; override;
  end;

  TFederalBreakRuleChecker = class(TBaseBreakRulesChecker)
  public
    function Check: Boolean; override;
  end;

{ CreateBreakRuleChecker }

function CreateBreakRuleChecker(const RuleType: string;
  const BreakLength, TimeForBreak: Integer): TBreakRulesChecker;
begin
  if RuleType = 'AFTER' then
    Result := TAfterRuleChecker.Create(BreakLength, TimeForBreak)
  else if RuleType = 'EVERY' then
    Result := TEveryRuleChecker.Create(BreakLength, TimeForBreak)
  else if UpperCase(RuleType) = 'LUNCH' then        //qmantwo-236 used internally
    Result := TLunchRuleChecker.Create(BreakLength, 0)
  else if RuleType = 'FED' then
    Result := TFederalBreakRuleChecker.Create(0,0)
  else
    raise Exception.Create('Unable to process break rules for ' + RuleType +
     ', please contact technical support or upgrade Q Manager.');
end;

function TBaseBreakRulesChecker.Minutes(const StartTime, EndTime: TTime): Integer;
var
  h1, m1, s1, ms1: Word;
  h2, m2, s2, ms2: Word;
begin
  Assert(EndTime > StartTime);
  DecodeTime(StartTime, h1, m1, s1, ms1);
  DecodeTime(EndTime, h2, m2, s2, ms2);
  Result := (h2*60 + m2) - (h1*60 + m1);
end;

{ TBaseBreakRulesChecker }

constructor TBaseBreakRulesChecker.Create(const BreakLength, TimeForBreak: Integer);
begin
  FBreakLength := BreakLength;
  FTimeForBreak := TimeForBreak;
end;

procedure TBaseBreakRulesChecker.Add(const AStartTime, AEndTime: TTime);
var
  I, Slot: Integer;
begin
  Assert(AEndTime > AStartTime, 'Work span must end after it begins');
  Assert(N<50, 'Maximum of 50 work spans');

  // Figure out where this one goes in the list
  Slot := N;
  for I := 0 to N-1 do begin
    if AStartTime < EndTime[I] then begin
      Slot := I;
      Break;
    end;
  end;

  // Copy existing elements "up" one to make room
  Inc(N);
  for I := N-1 downto Slot+1 do begin
    StartTime[I] := StartTime[I-1];
    EndTime[I] := EndTime[I-1];
  end;

  // Put this one in the open slot
  StartTime[Slot] := AStartTime;
  EndTime[Slot] := AEndTime;
end;

procedure TBaseBreakRulesChecker.Clear;
begin
  N := 0;
end;

{ TAfterRuleChecker }

function TAfterRuleChecker.Check: Boolean;
var
  TotalMinutesWorked: Integer;
  BreakCount: Integer;
  I: Integer;
begin
  Result := True;
  TotalMinutesWorked := 0;
  BreakCount := 0;

  for I := 0 to N-1 do begin
    TotalMinutesWorked := TotalMinutesWorked + Minutes(StartTime[I], EndTime[I]);
    if I>0 then begin
      if Minutes(EndTime[I-1], StartTime[I]) >= FBreakLength then
        Inc(BreakCount);
    end;
    if (TotalMinutesWorked >= FTimeForBreak) and (BreakCount=0) then
      Result := False;
  end;
end;

{ TEveryRuleChecker }

function TEveryRuleChecker.Check: Boolean;
var
  TotalMinutesWorked: Integer;
  BreakCount: Integer;
  I: Integer;
  BreaksNeeded: Integer;
begin
  Result := True;
  TotalMinutesWorked := 0;
  BreakCount := 0;

  for I := 0 to N-1 do begin
    TotalMinutesWorked := TotalMinutesWorked + Minutes(StartTime[I], EndTime[I]);
    if I>0 then begin
      if Minutes(EndTime[I-1], StartTime[I]) >= FBreakLength then
        Inc(BreakCount);
    end;
    BreaksNeeded := TotalMinutesWorked div FTimeForBreak;
    if BreaksNeeded > BreakCount then
      Result := False;
  end;
end;

{ TLunchRuleChecker }

function TLunchRuleChecker.Check: Boolean;
//if within Meal period, return False
//if outside Meal interval, return True -> passes
var
  I: Integer;
begin
  Result := True;

  for I := 0 to N-1 do begin
    if Minutes(StartTime[I], EndTime[I]) >= FBreakLength then
      Result := True  //outside required meal break QMANTWO-236
    else
      Result := False; //inside required meal break QMANTWO-236
  end;
end;

{ TFederalBreakRuleChecker }

function TFederalBreakRuleChecker.Check: Boolean;
begin
  Result := True;
end;

end.
