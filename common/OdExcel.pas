unit OdExcel;

interface

uses
  Windows, Variants, Excel2000;

function StartExcel: TExcelApplication;
procedure ConvertTSVFileToExcel(Excel: TExcelApplication; const InFileName, OutFileName: string);
procedure StopExcel(Excel: TExcelApplication);

implementation

function StartExcel: TExcelApplication;
var
  LCID: Cardinal;
begin
  LCID := GetUserDefaultLCID;
  Result := TExcelApplication.Create(nil);
  Result.Connect;
  Result.Visible[LCID] := False;
  Result.DisplayAlerts[LCID] := False;
end;

procedure StopExcel(Excel: TExcelApplication);
begin
  Excel.Quit;
  Excel.Disconnect;
end;

procedure ConvertTSVFileToExcel(Excel: TExcelApplication; const InFileName, OutFileName: string);
var
  Wbk: _Workbook;
  LCID: Cardinal;
begin
  LCID := GetUserDefaultLCID;
  WbK := Excel.Workbooks.Open(WideString(InFileName), EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, LCID);

  Excel.Cells.Select;
  Excel.Columns.AutoFit;
  Excel.ActiveCell.Select;

  Wbk.SaveAs(OleVariant(OutFileName), OleVariant(xlExcel4),
    EmptyParam, EmptyParam, OleVariant(False), OleVariant(False),
    xlNoChange, EmptyParam, EmptyParam, EmptyParam, EmptyParam, LCID);

  Excel.Workbooks.Close(LCID);
end;

end.
