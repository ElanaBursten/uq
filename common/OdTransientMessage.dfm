inherited TransientMessageForm: TTransientMessageForm
  AutoSize = True
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Message'
  ClientHeight = 191
  ClientWidth = 431
  Font.Name = 'Tahoma'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MsgLabel: TLabel
    Left = 12
    Top = 0
    Width = 408
    Height = 145
    Alignment = taCenter
    AutoSize = False
    Caption = 'Temporary message goes here'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -37
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 150
    Width = 431
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      431
      41)
    object CloseButton: TButton
      Left = 178
      Top = 8
      Width = 75
      Height = 25
      Anchors = []
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
    end
  end
  object HideTimer: TTimer
    Interval = 2000
    OnTimer = HideTimerTimer
    Left = 7
    Top = 155
  end
end
