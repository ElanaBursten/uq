unit BaseAttachmentDMu;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Classes, SysUtils, DB, IdFtp, OdUqInternet, IdComponent;

type
  TLogEvent = procedure(Sender: TObject; const LogMessage: string) of object;
  TProgressEvent = procedure(Sender: TObject; const ProgressMessage: string; const PctComplete: Integer) of object;

  TUploadFileItem = class
  public
    AttachmentID: Integer;
    OriginalFile: string;
    AttachFile: string;
    LocalAttachFilename: string;
    ForeignType: Integer;
    ForeignID: Integer;
    EmpID: integer;
    UploadDate: Variant;
  end;

  TBaseAttachment = class(TDataModule)
  private
    FOnLogMessage: TLogEvent;
    FOnDisplayTransientError: TLogEvent;
    FOnMinorProgress: TProgressEvent;
    FOnMajorProgress: TProgressEvent;
    FCanIAttachAnything: Boolean;
    FAttachedByEmpID: Integer;
    FApplicationName: string;
    FOnDownloadProgress: TProgressEvent;
    FOnDownloadDone: TNotifyEvent;
    FExpectedFileSize: Int64;
    FDownloadStartTime: TDateTime;
    FDownloadFileNumber: Integer;
    FNumberFilesToDownload: Integer;
    procedure VerifyAttachmentRestrictions(const FileName: string);
    function GenerateAttachmentFileName(const ForeignType, ForeignID: Integer; const OriginalFileName: string): string;
    procedure FTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
  protected
    procedure GetDirectoryNamesForAttachment(ForeignType,
      ForeignID: Integer; var ForeignTypeDir, YearDir, DateDir,
      IDDir: string);
    procedure DisplayTransientError(const ErrorMsg: String);
    procedure MajorProgress(const ProgressMessage: string; const PctComplete: Integer);
    procedure MinorProgress(const ProgressMessage: string; const PctComplete: Integer);
    procedure GetAttachmentDataFromAttachmentID(AttachmentID: Integer;
      var ForeignType, ForeignID, FileSize: Integer; var ServerFileName,
      OriginalFileName, FileHash: string); virtual;
    function IsManualTicket(TicketID: Integer): Boolean;
    function GetDownloadLocationSQL: string; virtual; abstract;
    function GetUploadFileTypesSQL: string; virtual; abstract;
    function GetAttachmentParentSQL: string; virtual; abstract;
    function GetDuplicateAttachmentCheckSQL: string; virtual; abstract;
    function OpenQuery(const SQL: string): TDataSet; virtual; abstract;
    procedure LockEnter; virtual;
    procedure LockExit; virtual;
    function GetFTPDownloaderData: TFTPDownloadConfig;
    procedure AssignAttachmentFields(AttachData: TDataSet; const Source,
      NewFileName, FileName: string; const ForeignType,
      ForeignID: Integer);
    function CheckAttachmentFileSizeOnFTPServer(FTP: TIdFTP; FileName: string; FileSize, ForeignType,
      ForeignID: Integer): TFileComparison;
    procedure ValidateAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string);
    function HasDuplicatedAttachmentFileName(const ForeignType, ForeignID: Integer; const FileName: string): Boolean;
  public
    procedure Log(const Msg: string);
    procedure RemoveAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string); virtual; abstract;
    property OnLogMessage: TLogEvent read FOnLogMessage write FOnLogMessage;
    property OnDisplayTransientError: TLogEvent read FOnDisplayTransientError write FOnDisplayTransientError;
    property OnMinorProgress: TProgressEvent read FOnMinorProgress write FOnMinorProgress;
    property OnMajorProgress: TProgressEvent read FOnMajorProgress write FOnMajorProgress;
    property OnDownloadProgress: TProgressEvent read FOnDownloadProgress
      write FOnDownloadProgress;
    property OnDownloadDone: TNotifyEvent read FOnDownloadDone write FOnDownloadDone;
    property DownloadFileNumber: Integer read FDownloadFileNumber write
      FDownloadFileNumber;
    property NumberFilesToDownload: Integer read FNumberFilesToDownload write
      FNumberFilesToDownload;

    property CanIAttachAnything: Boolean read FCanIAttachAnything write FCanIAttachAnything;
    property AttachedByEmpID: Integer read FAttachedByEmpID write FAttachedByEmpID;
    property ApplicationName: string read FApplicationName write FApplicationName;

    function PrepareFileForAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string): string;
    function AttachFileToRecord(const ForeignType, ForeignID: Integer; const FileName, Source: string; const CanAttachAnything: Boolean): string; virtual; abstract;
    function DownloadAttachmentToFile(const AttachmentID: Integer; const LocalFileName: string; FTP: TIdFTP = nil): string;
    function DownloadAttachmentToFileAWS(var pAttachmentData: TAttachmentData; const LocalFileName: string): string;
    function DownloadAttachment(const AttachmentID: Integer): string; virtual; abstract;
    function GetForeignTypeDirFromForeignType(const ForeignType: Integer): string;
    function GetAttachmentDownloadFolder: string; virtual; abstract;
    function GetAttachmentFolder: string; virtual; abstract;
    procedure PrepareFTPServer(FTP: TIDFtp; const LocationID: Integer; var RootDir: string);
    function CopyAttachmentToDownloadFolder(AttachmentID: Integer): string;
    procedure CleanAttachmentDownloads;
  end;

  function GetAttachmentFileNamePrefix(const ForeignType, ForeignID: Integer): string;

implementation

uses
  OdMiscUtils, QMConst, OdExceptions, OdSecureHash, JclFileUtils,
  JclStrings, JclSysInfo, DateUtils, OdFTP;

{$R *.dfm}

function GetAttachmentFileNamePrefix(const ForeignType, ForeignID: Integer): string;
begin
  case ForeignType of
    qmftTicket: Result := 'T';
    qmftDamage: Result := 'D';
    qmft3rdParty: Result := 'P';
    qmftLitigation: Result := 'L';
    qmftWorkOrder: Result := 'W';
  else
    Result := 'U';
  end;
  Result := Result + IntToStr(ForeignID) +'-';
end;

{ TBaseAttachment }
procedure TBaseAttachment.Log(const Msg: string);
begin
  if Assigned(FOnLogMessage) then
    FOnLogMessage(Self, Msg);
end;



procedure TBaseAttachment.DisplayTransientError(const ErrorMsg: String);
begin
  if Assigned(FOnDisplayTransientError) then
    FOnDisplayTransientError(Self, ErrorMsg);
end;

procedure TBaseAttachment.MajorProgress(const ProgressMessage: string; const PctComplete: Integer);
begin
  if Assigned(FOnMajorProgress) then
    FOnMajorProgress(Self, ProgressMessage, PctComplete);
end;

procedure TBaseAttachment.MinorProgress(const ProgressMessage: string; const PctComplete: Integer);
begin
  if Assigned(FOnMinorProgress) then
    FOnMinorProgress(Self, ProgressMessage, PctComplete);
end;

procedure TBaseAttachment.GetAttachmentDataFromAttachmentID(
  AttachmentID: Integer; var ForeignType, ForeignID, FileSize: Integer;
  var ServerFileName, OriginalFileName, FileHash: string);
const
  SQL = 'select * from attachment where attachment_id=';
var
  AttachData: TDataSet;
begin
  LockEnter;
  try
    AttachData := OpenQuery(SQL + IntToStr(AttachmentID));
    if AttachData.IsEmpty then
      raise Exception.CreateFmt('Attachment record %d not found in GetAttachmentDataFromAttachmentID', [AttachmentID]);
    try
      ForeignType := AttachData.FieldByName('foreign_type').AsInteger;
      ForeignID := AttachData.FieldByName('foreign_id').AsInteger;
      FileSize := AttachData.FieldByName('size').AsInteger;
      ServerFileName := AttachData.FieldByName('filename').AsString;
      OriginalFileName := AttachData.FieldByName('orig_filename').AsString;
      FileHash := AttachData.FieldByName('file_hash').AsString;
    finally
      AttachData.Close;
    end;
  finally
    LockExit;
  end;
  Assert(ForeignType > 0, 'Invalid ForeignType for attachment_id ' + IntToStr(AttachmentID));
  Assert(ForeignID > 0, 'Invalid ForeignID for attachment_id ' + IntToStr(AttachmentID));
  Assert(ServerFileName <> '', 'Blank ServerFileName for attachment_id ' + IntToStr(AttachmentID));
end;

// TODO: This is currently duplicated in the DMu & LogicDMu units.
function TBaseAttachment.IsManualTicket(TicketID: Integer): Boolean;
const
  SQL = 'select status from ticket where ticket_id = ';
var
  Ticket: TDataSet;
begin
  LockEnter;
  try
    Ticket := OpenQuery(SQL + IntToStr(TicketID));
    try
      Assert(not Ticket.Eof, 'Cannot find ticket with ticket_id=' + IntToStr(TicketID));
      Result := StrContains(TicketStatusManual, Ticket.FieldByName('status').AsString);
    finally
      Ticket.Close;
    end;
  finally
    LockExit;
  end;
end;

procedure TBaseAttachment.ValidateAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string);
begin
  if not FileExists(FileName) then
    raise Exception.Create('Unable to locate selected file to attach: ' + FileName);
  VerifyAttachmentRestrictions(FileName);
  if HasDuplicatedAttachmentFileName(ForeignType, ForeignID, ExtractFileName(FileName)) then begin
    if Source = AddinAttachmentSource then
      RemoveAttachment(ForeignType, ForeignID, ExtractFileName(FileName), Source)
    else
      raise EOdDataEntryError.Create('A file with the same name (' + ExtractFileName(FileName) +') is already attached. Please rename your file.');
  end;
end;

procedure TBaseAttachment.VerifyAttachmentRestrictions(const FileName: string);
const
  FileTooLarge = 'The maximum attachment size for %s files is %d KB.  %s is %d KB.';
var
  Extension: string;
  FileSizeKB: Integer;
  MaxSizeKB: Integer;
  SizeErrorMsg: string;
  FileTypes: TDataSet;
begin
  if CanIAttachAnything then
    Exit;

  LockEnter;
  try
    FileTypes := OpenQuery(GetUploadFileTypesSQL);
    try
      if FileTypes.IsEmpty then
        Exit;

      Extension := LowerCase(StrRemoveChars(ExtractFileExt(FileName), ['.']));
      if FileTypes.Locate('extension', Extension, []) then begin
        MaxSizeKB := FileTypes.FieldByName('max_kilobytes').AsInteger;
        FileSizeKB := RoundUp(GetFileSize(FileName) / 1024);
        SizeErrorMsg := Trim(FileTypes.FieldByName('size_error_message').AsString);
        if IsEmpty(SizeErrorMsg) then
          SizeErrorMsg := Format(FileTooLarge, [Extension, MaxSizeKB, ExtractFileName(FileName), FileSizeKB]);
        if (MaxSizeKB >= 0) and (FileSizeKB > MaxSizeKB) then
          raise EOdNotAllowed.Create(SizeErrorMsg);
      end
      else
        raise EOdNotAllowed.CreateFmt('Cannot attach file "%s": files of the type %s are not allowed as attachments.',
           [ExtractFileName(FileName), Extension]);
    finally
      FileTypes.Close;
    end;
  finally
    LockExit;
  end;
end;

function TBaseAttachment.GetFTPDownloaderData: TFTPDownloadConfig;
var
  Config: TFTPDownloadConfig;
  UploadLocation: TDataSet;
begin
  LockEnter;
  try
    UploadLocation := OpenQuery(GetDownloadLocationSQL);
    try
      if UploadLocation.IsEmpty then
        raise Exception.Create('There is no active ''Download'' location in the upload_location table');

      Config.LocationID := UploadLocation.FieldByName('location_id').AsInteger;
      Config.Host := UploadLocation.FieldByName('server').AsString;
      if UploadLocation.FieldByName('port').IsNull then
        { This shouldn't happen on the server, since upload_location.port
          doesn't allow nulls }
        raise Exception.Create('The ''Download'' location''s port number is ' +
          'blank in your upload_location table. Sync to get the port number.');
      Config.Port := UploadLocation.FieldByName('port').AsInteger;
      Config.User := UploadLocation.FieldByName('username').AsString;
      Config.Password := UploadLocation.FieldByName('password').AsString;
      Config.RootDir := UploadLocation.FieldByName('directory').AsString;
      Config.Timeout := 10000;
    finally
      UploadLocation.Close;
    end;
  finally
    LockExit;
  end;
  Result := Config;
end;

// Requires an update to DSSS.DLL
function TBaseAttachment.DownloadAttachmentToFile(const AttachmentID: Integer; const LocalFileName: string; FTP: TIdFTP): string;
var
  FileSize: Integer;
//  AttachmentData: TAttachmentData;
  Config: TFTPDownloadConfig;
  Phase: string;
  OrigFilename: string;
begin
  Assert(AttachmentID > 0);
  Assert(LocalFileName <> '');
  {EB - In process of fixing}
//  AttachmentData.AttachmentID := AttachmentID;
  MyAttachmentData.AttachmentID := AttachmentID;

  Result := LocalFileName;
  try

    Phase := 'GetFTPDownloaderData';
    Config := GetFTPDownloaderData;

    Phase := 'AWS: DownloadAttachmentToFile';
//    GetAWSConstants(AttachmentData.Constants);
//    GetAWSCredentials(AttachmentData.AWSKey, AttachmentData.AWSSecret);


    Phase := 'GetAttachmentDataFromAttachmentID';
    GetAttachmentDataFromAttachmentID(MyAttachmentData.AttachmentID, MyAttachmentData.ForeignTypeID, MyAttachmentData.ForeignID,
      FileSize, MyAttachmentData.ServerFileName, OrigFileName, MyAttachmentData.FileHash);

    Phase := Format('GetDirectoryNamesForAttachment %d %d %s %s %s %s', [MyAttachmentData.ForeignTypeID, MyAttachmentData.ForeignID, MyAttachmentData.ForeignTypeDir, MyAttachmentData.YearDir, MyAttachmentData.DateDir, MyAttachmentData.IDDir]);
    GetDirectoryNamesForAttachment(MyAttachmentData.ForeignTypeID, MyAttachmentData.ForeignID, MyAttachmentData.ForeignTypeDir, MyAttachmentData.YearDir, MyAttachmentData.DateDir, MyAttachmentData.IDDir);

    Phase := Format('DownloadAttachmentToFile %s from %s:%d', [OrigFilename,
      Config.Host, Config.Port]);

    if Assigned(FOnDownloadProgress) then
      FOnDownloadProgress(Self, 'Download starting', 0);
    FExpectedFileSize := FileSize;
    FDownloadStartTime := Now;
    try
      OdUqInternet.DownloadAttachmentToFile(LocalFilename, MyAttachmentData,
        Config, FTPWork, True);
    finally
      FExpectedFileSize := 0;
      FDownloadStartTime := 0;
      if Assigned(FOnDownloadDone) then
        FOnDownLoadDone(Self);
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;
end;

function TBaseAttachment.DownloadAttachmentToFileAWS(var pAttachmentData: TAttachmentData; const LocalFileName: string): string;
{QMANTWO-552 EB - Assumes that the AttachmentData has been loaded prior to the call}
var
  FileSize: Integer;
  Phase: string;
  Config: TFTPDownloadConfig;
begin
  Assert(pAttachmentData.AttachmentID > 0);
  Assert(LocalFileName <> '');

  Result := LocalFileName;
  try

    Phase := 'AWS: DownloadAttachmentToFileAWS';

    if Assigned(FOnDownloadProgress) then
      FOnDownloadProgress(Self, 'Download starting', 0);
    FExpectedFileSize := FileSize;
    FDownloadStartTime := Now;
    try
      OdUqInternet.DownloadAttachmentToFile(LocalFilename, pAttachmentData, Config);
    finally
      FExpectedFileSize := 0;
      FDownloadStartTime := 0;
      if Assigned(FOnDownloadDone) then
        FOnDownLoadDone(Self);
    end;
  except
    on E: Exception do begin
      E.Message := E.Message + ' in ' + Phase;
      raise;
    end;
  end;

end;

procedure TBaseAttachment.GetDirectoryNamesForAttachment(ForeignType, ForeignID: Integer;
  var ForeignTypeDir, YearDir, DateDir, IDDir: string);

  procedure DirNamesFromForeignTypeAndID(ForeignType, ForeignID: Integer; var YearDir, DateDir: string);
  var
    TableName: string;
    KeyName: string;
    CreateDateField: string;
    OrderByClause: string;
    AttachmentParent: TDataSet;
  begin
    case ForeignType of
      qmftTicket:
        begin
          if IsManualTicket(ForeignID) then begin
            TableName := 'ticket';
            KeyName := 'ticket_id';
            CreateDateField := 'transmit_date';
            OrderByClause := '';
          end else begin
            TableName := 'ticket_version';
            KeyName := 'ticket_id';
            CreateDateField := 'arrival_date';
            OrderByClause := 'order by ticket_version_id';
          end
        end;
      qmftDamage:
        begin
          TableName := 'damage';
          KeyName := 'damage_id';
          CreateDateField := 'uq_notified_date';
          OrderByClause := '';
        end;
      qmft3rdParty:
        begin
          TableName := 'damage_third_party';
          KeyName := 'third_party_id';
          CreateDateField := 'uq_notified_date';
          OrderByClause := '';
        end;
      qmftLitigation:
        begin
          TableName := 'damage_litigation';
          KeyName := 'litigation_id';
          CreateDateField := 'summons_date';
          OrderByClause := '';
        end;
      qmftWorkOrder:
        begin
          TableName := 'work_order_version';
          KeyName := 'wo_id';
          CreateDateField := 'arrival_date';
          OrderByClause := 'order by wo_version_id';
        end;
      else raise Exception.Create('Invalid foreign type');
    end;

    AttachmentParent := OpenQuery(Format(GetAttachmentParentSQL, [TableName, KeyName, ForeignID, OrderByClause]));
    try
      if AttachmentParent.Eof then
        raise Exception.CreateFmt('%s for record ID %d was not found to obtain the attachment''s FTP location.', [TableName, ForeignID]);
      YearDir := IntToStr(YearOf(AttachmentParent.FieldByName(CreateDateField).AsDateTime));
      Assert(StrToInt(YearDir) > 1900, 'Invalid attachment date year: ' + YearDir);
      DateDir := FormatDateTime(DirFormatForAttachments, AttachmentParent.FieldByName(CreateDateField).AsDateTime);
    finally
      AttachmentParent.Close;
    end;
  end;

begin
  LockEnter;
  try
    ForeignTypeDir := GetForeignTypeDirFromForeignType(ForeignType);
    DirNamesFromForeignTypeAndID(ForeignType, ForeignID, YearDir, DateDir);
    IDDir := IntToStr(ForeignID);
    Assert((ForeignTypeDir <> '') and (YearDir <> '') and (DateDir<> '') and (IDDir <> ''), 'Bad directories in GetDirectoryNamesForAttachment');
  finally
    LockExit;
  end;
end;

function TBaseAttachment.GetForeignTypeDirFromForeignType(const ForeignType: Integer): string;
begin
  case ForeignType of
    qmftTicket: Result := 'Tickets';
    qmftDamage: Result := 'Damages';
    qmft3rdParty: Result := 'ThirdParty';
    qmftLitigation: Result := 'Litigation';
    qmftWorkOrder: Result := 'WorkOrders';
    else raise Exception.Create('Invalid foreign type');
  end;
end;

function TBaseAttachment.GenerateAttachmentFileName(const ForeignType, ForeignID: Integer;
  const OriginalFileName: string): string;
begin
  Result := GetAttachmentFileNamePrefix(ForeignType, ForeignID) +
    RandomAlphaNumericString(16) + ExtractFileExt(OriginalFileName);
end;

function TBaseAttachment.HasDuplicatedAttachmentFileName(const ForeignType, ForeignID: Integer; const FileName: string): Boolean;
var
  SQL: string;
  CheckAttachment: TDataSet;
begin
  LockEnter;
  try
    SQL := GetDuplicateAttachmentCheckSQL;
    CheckAttachment := OpenQuery(Format(SQL, [ForeignType, ForeignID, QuotedStr(FileName)]));
    try
      Result := CheckAttachment.Fields[0].AsInteger > 0;
    finally
      CheckAttachment.Close;
    end;
  finally
    LockExit;
  end;
end;

procedure TBaseAttachment.PrepareFTPServer(FTP: TIDFtp; const LocationID: Integer; var RootDir: string);
const
  SQL = 'select * from upload_location where location_id=';
var
  Directory: string;
  UserName: string;
  Password: string;
  UploadLocation: TDataSet;
begin
  Assert(Assigned(FTP), 'FTP connection is not assigned in PrepareFTPServer');
  if FTP.Connected then
    Exit;

  try
    LockEnter;
    try
      UploadLocation := OpenQuery(SQL + IntToStr(LocationID));
      try
        if UploadLocation.IsEmpty then
          raise Exception.CreateFmt('Cannot find upload_location with location_id=%d', [LocationID]);

        FTP.Host := UploadLocation.FieldByName('server').AsString;
        if UploadLocation.FieldByName('port').IsNull then
          { This shouldn't happen on the server, since upload_location.port
            doesn't allow nulls }
          raise Exception.CreateFmt('The port number is blank for location_id' +
            '=%d in your upload_location table. Sync to get the port number.',
            [LocationID]);
        FTP.Port := FTPPortAsWord(UploadLocation.FieldByName('port').AsInteger);
        Password := UploadLocation.FieldByName('password').AsString;
        UserName := UploadLocation.FieldByName('username').AsString;
        Directory := UploadLocation.FieldByName('directory').AsString;
      finally
        UploadLocation.Close;
      end;
    finally
      LockExit;
    end;

    if Password <> '' then
      FTP.Password := Password
    else
      FTP.Password := 'qmanager@utiliquest.com';
    if UserName <> '' then
      FTP.Username := UserName
    else
      FTP.Username := 'anonymous';
    Log(Format('Connecting to FTP server: %s:%d...', [FTP.Host, FTP.Port]));
    FTP.ConnectTimeout := 10000; // Indy 10 is required
    FTP.AutoLogin := True;
    FTP.Passive := True;
    FTP.Connect;
    if Directory <> '' then begin
      Log('Changing to directory: ' + Directory);
      FTP.ChangeDir(Directory);
    end;
    RootDir := FTP.RetrieveCurrentDir;
  except
    on E: Exception do begin
      Log(E.Message);
      raise;
    end;
  end;
end;

procedure TBaseAttachment.AssignAttachmentFields(AttachData: TDataSet; const Source, NewFileName, FileName: string; const ForeignType, ForeignID: Integer);
var
  Extension : string;
begin
  Assert(Assigned(AttachData) and (AttachData.State in [dsEdit, dsInsert]), 'AttachData not in edit mode in AssignAttachmentFields');
  with AttachData do begin
    FieldByName('filename').AsString := ExtractFileName(NewFileName);
    FieldByName('orig_filename').AsString := ExtractFileName(FileName);
    Extension := StringReplace(ExtractFileExt(NewFileName), '.', '', []);
    FieldByName('extension').AsString := Extension;
    if SameText(Extension, 'sketch') then
      FieldByName('doc_type').AsString := DocumentTypeManifest;
    FieldByName('foreign_type').AsInteger := ForeignType;
    FieldByName('foreign_id').AsInteger := ForeignID;
    FieldByName('orig_file_mod_date').AsDateTime := GetFileDateTime(FileName);
    FieldByName('attach_date').AsDateTime := Now;
    FieldByName('modified_date').AsDateTime := Now;
    FieldByName('size').AsInteger := GetSizeOfFile(NewFileName);
    FieldByName('active').AsBoolean := True;
    FieldByName('attached_by').AsInteger := AttachedByEmpID;
    FieldByName('source').AsString := Source;
    FieldByName('file_hash').AsString := HashFile(FileName);
  end;
end;

function TBaseAttachment.PrepareFileForAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string): string;
var
  NewFileName: string;
begin
  ValidateAttachment(ForeignType, ForeignID, FileName, Source);
  NewFileName := GenerateAttachmentFileName(ForeignType, ForeignID, FileName);
  Result := IncludeTrailingPathDelimiter(GetAttachmentFolder) + NewFileName;
  if not FileCopy(FileName, Result, False) then
    raise Exception.CreateFmt('Failed to copy file %s to %s', [FileName, Result]);
end;

procedure TBaseAttachment.CleanAttachmentDownloads;
var
  DownloadDir: string;
begin
  DownloadDir := GetAttachmentDownloadFolder;
  OdDeleteFile(DownloadDir, '*.*', 32, True, False);
end;

function TBaseAttachment.CopyAttachmentToDownloadFolder(AttachmentID: Integer): string;
var
  DestDir: string;
  ForeignType: Integer;
  ForeignID: Integer;
  FileSize: Integer;
  ServerFileName: string;
  OriginalFileName: string;
  SourceFile: string;
  FileHash: string;
begin
  GetAttachmentDataFromAttachmentID(AttachmentID, ForeignType, ForeignID,
    FileSize, ServerFileName, OriginalFileName, FileHash);
  DestDir := GetAttachmentDownloadFolder;
  DestDir := DestDir + IncludeTrailingPathDelimiter(GetForeignTypeDirFromForeignType(ForeignType)) + IntToStr(ForeignID);
  if not DirectoryExists(DestDir) then
    OdForceDirectories(DestDir);

  Result := OriginalFileName;
  SourceFile := IncludeTrailingPathDelimiter(GetAttachmentFolder) + ServerFileName;
  if FileExists(SourceFile) then begin
    Result := IncludeTrailingPathDelimiter(DestDir) + Result;
    if not FileCopy(SourceFile, Result, True) then
      raise Exception.CreateFmt('Unable to copy file %s to attachment directory as %s', [SourceFile, Result]);
  end;
end;

function TBaseAttachment.CheckAttachmentFileSizeOnFTPServer(FTP: TIdFTP; FileName: string; FileSize, ForeignType, ForeignID: Integer): TFileComparison;
var
  AttachmentData: OdUqInternet.TAttachmentData;
  Phase: string;
begin
  Assert(FTP <> nil);
  Assert(FileName <> '');
  Assert(FileSize > 0, 'Empty (0 byte) files are not allowed');

  try
    Phase := 'AttachmentData.ServerFilename';
    AttachmentData.ServerFilename := FileName;
    Phase := Format('GetDirectoryNamesForAttachment %d %d %s %s %s %s', [ForeignType, ForeignID, AttachmentData.ForeignTypeDir, AttachmentData.YearDir, AttachmentData.DateDir, AttachmentData.IDDir]);
    GetDirectoryNamesForAttachment(ForeignType, ForeignID, AttachmentData.ForeignTypeDir, AttachmentData.YearDir, AttachmentData.DateDir, AttachmentData.IDDir);
    Phase := Format('OdUqInternet.CheckFileSizeOnFTPServer %s on %s:%d', [FileName, FTP.Host, FTP.Port]);
    Result := OdUqInternet.CheckFileSizeOnFTPServer(FTP, FileSize, AttachmentData);
  except
    on E: Exception do begin
      raise Exception.Create(E.Message + ' in ' + Phase);
    end;
  end;
end;

procedure TBaseAttachment.LockEnter;
begin
  // Override to enter a lock
end;

procedure TBaseAttachment.LockExit;
begin
  // Override to exit a lock
end;

procedure TBaseAttachment.FTPWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
var
  ProgressMessage: string;
  PctComplete, ElapsedSeconds, BytesPerSecond: Double;
  BytesRemaining: Int64;
begin
  if (AWorkMode <> wmRead) or not Assigned(FOnDownloadProgress) then
    Exit;

  try
    PctComplete := 0;
    BytesPerSecond := 0;
    ProgressMessage := '';

    if FExpectedFileSize <= 0 then
      ProgressMessage := IntToStr(AWorkCount) + ' bytes'
    else if AWorkCount < 0 then
      ProgressMessage := 'Download error'
    else if AWorkCount <= FExpectedFileSize then begin
      PctComplete := AWorkCount / FExpectedFileSize * 100;
      ElapsedSeconds := SecondsBetween(Now, FDownloadStartTime);
      if ElapsedSeconds > 0 then
        BytesPerSecond := AWorkCount / ElapsedSeconds;
      BytesRemaining := FExpectedFileSize - AWorkCount;
      if PctComplete > 0.001 then begin
        ProgressMessage := IntToStr(Trunc(PctComplete + 0.5)) + '%';
        if BytesPerSecond > 0 then
          ProgressMessage := ProgressMessage + ', ' +
            SecondsToHMMSS(Trunc(BytesRemaining / BytesPerSecond)) + ' remaining';
      end else
        ProgressMessage := '0%';
    end
    else begin
      PctComplete := 100;
      ProgressMessage := IntToStr(AWorkCount) + ' bytes'
    end;

    if FNumberFilesToDownload > 1 then
      ProgressMessage := IntToStr(FDownloadFileNumber) + ' of ' +
        IntToStr(FNumberFilesToDownload) + ', ' + ProgressMessage;
  except
    on E: Exception do begin
      PctComplete := 0;
      ProgressMessage := Format('Download error: %s', [E.Message]);
    end;
  end;

  FOnDownloadProgress(Self, ProgressMessage, Trunc(PctComplete));
end;



end.
