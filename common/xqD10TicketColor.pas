unit xqD10TicketColor;
interface
uses DB, Graphics, DBISAMTb;
type
  TTicketFlag = (tsPastDue, tsDueToday, tsDueHours, tsEmergency, tsUpdate,
    tsNotDue, tsDoNotMark, tsPastWorkloadDate, ts7NextBusDay);
  TTicketStatus = set of TTicketFlag;
type
  TxqTicketColor = class(TObject)
  private
    FZeroGeocodeMatchesColor: TColor;
    FDueHoursTicketColor: TColor;
    FHighestGeocodePrecisionColor: TColor;
    FCriticalTicketColor: TColor;
    FDueTicketColor: TColor;
    FDue7TomorrowColor: TColor;
    FDoNotMarkTicketColor: TColor;
    FAcceptableGeocodePrecisionColor: TColor;
    FShadeColor: TColor;
    procedure Initialize;

    function GetTicketStatusFromValues(const Kind, TicketType, TicketFormat: string;
      const DueDate, LegalGoodThru, DoNotMarkBefore: TDateTime; const
      UseWorkloadDate: Boolean=False; const WorkloadDate: TDateTime=0): TTicketStatus;
    function IsHoliday(ADate: TDateTime; CallCenter: string): boolean;
    function At7NextBusDay(const DueDate: TDateTime; CallCenter: String): boolean;

  public
  property ShadeColor: TColor read FShadeColor write FShadeColor;
  property DueTicketColor: TColor read FDueTicketColor write FDueTicketColor;
  property DueHoursTicketColor: TColor read FDueHoursTicketColor write FDueHoursTicketColor;
  property CriticalTicketColor: TColor read FCriticalTicketColor write FCriticalTicketColor;
  property DoNotMarkTicketColor: TColor read FDoNotMarkTicketColor write FDoNotMarkTicketColor;
  property Due7TomorrowColor: TColor read FDue7TomorrowColor write FDue7TomorrowColor;
  property HighestGeocodePrecisionColor: TColor read
    FHighestGeocodePrecisionColor write FHighestGeocodePrecisionColor;
  property AcceptableGeocodePrecisionColor: TColor read
    FAcceptableGeocodePrecisionColor write FAcceptableGeocodePrecisionColor;
  property ZeroGeocodeMatchesColor: TColor read FZeroGeocodeMatchesColor write
    FZeroGeocodeMatchesColor;
function GetLocatesStringForTicket(Locates: TDBISAMTable; TicketID: Integer;
  const Delimiter: string = ' '): string;
    function GetTicketColor(Status: TTicketStatus; Shaded: Boolean;
  DefaultColor: TColor=clWindow): TColor;
    function GetTicketStatusFromRecord(DataSet: TDataSet): TTicketStatus;
  constructor Create;
  end;
implementation
uses SysUtils, QMConst, Math, DateUtils,Types, streetMaptickets;
{ TxqTicketColor }

constructor TxqTicketColor.Create;
begin
  Initialize;
end;

function TxqTicketColor.GetTicketColor(Status: TTicketStatus; Shaded: Boolean; DefaultColor: TColor): TColor;
begin
  if (tsPastDue in Status) or (tsEmergency in Status) or (tsUpdate in Status) then
    Result := CriticalTicketColor
  else if tsDueHours in Status then
    Result := DueHoursTicketColor
  else if tsDueToday in Status then
    Result := DueTicketColor
  else if ts7NextBusDay in Status then
    Result := Due7TomorrowColor
  else if tsDoNotMark in Status then
    Result := DoNotMarkTicketColor
  else if Shaded then
    Result := ShadeColor
  else
    Result := DefaultColor;
end;

function TxqTicketColor.GetTicketStatusFromValues(const Kind, TicketType, TicketFormat: string;
  const DueDate, LegalGoodThru, DoNotMarkBefore: TDateTime;
  const UseWorkloadDate: Boolean=False; const WorkloadDate: TDateTime=0):
  TTicketStatus;
const
  HoursDue = 2;
var
  IsOngoing: Boolean;
  IsDueToday: Boolean;
  IsDueInHours: Boolean;
  IsPastDue: Boolean;
  IsDone: Boolean;
  EndOfToday: TDateTime;
  TwoHoursFromNow: TDateTime;
  NextBusDay7: Boolean;

  BeginningOfToday: TDateTime;
  EffectiveDueDate: TDateTime;
  IsEmergency: Boolean;
  IsUpdate: Boolean;
  IsNewJersey: Boolean;
  IsDoNotMark: Boolean;
begin
  IsOngoing := Kind = TicketKindOngoing;
  if IsOngoing then
    EffectiveDueDate := Max(DueDate, LegalGoodThru)
  else
    EffectiveDueDate := DueDate;
  TwoHoursFromNow := DateUtils.IncHour(Now, 2);
  EndOfToday := DateUtils.EndOfTheDay(Now);
  BeginningOfToday := DateUtils.StartOfTheDay(Now);
  NextBusDay7 := At7NextBusDay(EffectiveDueDate, TicketFormat);
  IsDone := Kind = TicketKindDone;
  IsPastDue := (not IsDone) and (EffectiveDueDate < Now);
  IsDueToday := (not IsDone) and (EffectiveDueDate >= BeginningOfToday)
                and (EffectiveDueDate < EndOfToday);
  IsDueInHours := (not IsDone) and (EffectiveDueDate >= Now)
                and (EffectiveDueDate <= TwoHoursFromNow);
  IsDoNotMark := (DoNotMarkBefore > 1) and (DoNotMarkBefore > Now);
  IsEmergency := Kind = TicketKindEmergency;
  IsUpdate    := TicketType = 'UPDATE';
  IsNewJersey := TicketFormat = CallCenterNewJersey;

  if IsPastDue then
    Result := [tsPastDue]
  else if IsDueInHours then
    Result := [tsDueHours]
  else if IsDueToday then
    Result := [tsDueToday]
  else
    Result := [tsNotDue];

  if IsNewJersey and IsEmergency then
    Include(Result, tsEmergency)
  else if IsNewJersey and IsUpdate then
    Include(Result, tsUpdate);

  if IsDoNotMark then
    Include(Result, tsDoNotMark);

  if UseWorkloadDate and (CompareDate(Now, WorkloadDate) = GreaterThanValue) then
    Include(Result, tsPastWorkloadDate);

  if NextBusDay7 then
    Include(Result, ts7NextBusDay);
end;

function TxqTicketColor.At7NextBusDay(const DueDate: TDateTime; CallCenter:String): boolean; //QMANTWO-503 EB
var
  NextBusDate: TDateTime;
  Exactly7AM: TDateTime;
begin
  Result := False;
  Exactly7AM := EncodeTime(7,0,0,0);

  {Establish that DueDate has not passed and it is a 7AM ticket}
  if (DueDate > Today) and (SameTime(DueDate, Exactly7AM))
      and ((CallCenter = 'OCC2') or (CallCenter = 'FDE2')) then begin  //QM-74 EB Added FDE2
      //EB: It would be better to move this to a table, but for rush-job...

    {Get the Next Business Day}
    case DayOfTheWeek(Today) of
      DayFriday :   NextBusDate := IncDay(Today, 3);
      DaySaturday : NextBusDate := IncDay(Today, 2);
      else          NextBusDate := Tomorrow;
    end;

    {Skip holidays}
    While IsHoliday(NextBusDate, CallCenter) do begin
      NextBusDate := IncDay(NextBusDate, 1);
    end;

    if DueDate <= (NextBusDate + Exactly7AM) then
      Result := True;
  end;
end;

function TxqTicketColor.IsHoliday(ADate: TDateTime; CallCenter: string): boolean;
var
  ShortDate: TDatetime;
  ccField: string;
begin
  Result := FALSE;
  if frmStreetMapTickets.Holidays.Active then
    frmStreetMapTickets.Holidays.Close;

  ShortDate := Trunc(ADate);
  frmStreetMapTickets.Holidays.ParamByName('inputdate').AsDate := ShortDate;

  frmStreetMapTickets.Holidays.Open;
  frmStreetMapTickets.Holidays.First;
  while not frmStreetMapTickets.Holidays.EOF do begin
    ccField := Trim(frmStreetMapTickets.Holidays.FieldByName('cc_code').AsString);
    if (CallCenter = ccField) OR (ccField = '*') then
      Result := True;
    frmStreetMapTickets.Holidays.Next;
  end;

  frmStreetMapTickets.Holidays.close;
end;

function TxqTicketColor.GetTicketStatusFromRecord(DataSet: TDataSet): TTicketStatus;
var
  DueDate: TDateTime;
  LegalGoodThru: TDateTime;
  Kind: string;
  TicketType: string;
  TicketFormat: string;
  DoNotMarkBefore: TDateTime;
  WorkloadDateField: TField;
begin
  Assert(Assigned(DataSet));
  Kind := Dataset.FieldByName('kind').AsString;
  TicketType := Dataset.FieldByName('ticket_type').AsString;
  TicketFormat := Dataset.FieldByName('ticket_format').AsString;
  DueDate := Dataset.FieldByName('due_date').AsDateTime;
  LegalGoodThru := Dataset.FieldByName('legal_good_thru').AsDateTime;
  DoNotMarkBefore := Dataset.FieldByName('do_not_mark_before').AsDateTime;

  WorkloadDateField := DataSet.FindField('workload_date');
  if (WorkloadDateField = nil) or WorkloadDateField.IsNull then
    Result := GetTicketStatusFromValues(Kind, TicketType, TicketFormat, DueDate,
      LegalGoodThru, DoNotMarkBefore)
  else
    Result := GetTicketStatusFromValues(Kind, TicketType, TicketFormat, DueDate,
      LegalGoodThru, DoNotMarkBefore, True, WorkloadDateField.AsDateTime);
end;

function TxqTicketColor.GetLocatesStringForTicket(Locates: TDBISAMTable; TicketID: Integer; const Delimiter: string): string;

  procedure AddLocateString(Loc: string);
  begin
    if Trim(Loc) = '' then
      Exit;
    if Result = '' then
      Result := Loc
    else
      Result := Result + Delimiter + Loc;
  end;

begin
  Assert(Assigned(Locates));

  if not Locates.Active then
    Locates.Open;

  Locates.IndexFieldNames := 'ticket_id';
  Locates.SetRange([TicketID], [TicketID]);
  Locates.First;
  while not Locates.Eof do
  begin
    AddLocateString(Locates.FieldByName('client_code').AsString);
    Locates.Next;
  end;
end;

procedure TxqTicketColor.Initialize;
begin
  ShadeColor := $00EBEBEB; // Light silver
  DueTicketColor := $0080FFFF; // Light yellow
  DueHoursTicketColor := $00FDCCE6; // Light purple
  CriticalTicketColor := $009B9DE3; // Light red
  DoNotMarkTicketColor := $00A6D2FF; // Light orange
  HighestGeocodePrecisionColor := $0096FF96; // green
  AcceptableGeocodePrecisionColor := $0000E8E8; // yellow
  ZeroGeocodeMatchesColor := $00FAFAFA; // white, same as background
  Due7TomorrowColor := $0080FFFF; //Light yellow

end;


end.
