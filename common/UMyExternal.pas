{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$WARN UNSAFE_TYPE OFF}


unit UMyExternal;

interface

uses
  Classes, ComObj, windows, messages, QManager_TLB;  // why the shell
//const
//   WM_ACK_MSG = WM_APP + 99;
type
  TMyExternal = class(TAutoIntfObject, IMyExternal, IDispatch)
  private

  protected
    { IMyExternal methods }

  public
    Procedure AckMsg(const Ack: WideString); safecall;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils, ActiveX, StdActns, StdCtrls, MessageAck;

{ TMyExternal }

constructor TMyExternal.Create;
var
  TypeLib: ITypeLib;    // type library information
  ExeName: WideString;  // name of our program's exe file
begin
  // Get name of application
  ExeName := ParamStr(0);
  // Load type library from application's resources
  OleCheck(LoadTypeLib(PWideChar(ExeName), TypeLib));
  // Call inherited constructor
  inherited Create(TypeLib, IMyExternal);
end;

destructor TMyExternal.Destroy;
begin

  inherited;
end;




procedure TMyExternal.AckMsg(const Ack: WideString);
begin
  PostMessage(MessageAck.msgAckHandle, WM_ACK_MSG,0,0);
end;

end.
