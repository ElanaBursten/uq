{
  Any TDataModule that uses an Interface should descend from the TInterfacedDM declared in here.
  Adding an Interface directly to a TDataModule descendent does not work because TDataModule does
  not fully implement reference counting.

  Add this unit to your project, and then descend any TDataModules that need an interface from TInterfacedDM.

  The code in this unit came from this article (with minor reformatting):
  http://wiert.me/2009/08/10/delphi-using-fastmm4-part-2-tdatamodule-descendants-exposing-interfaces-or-the-introduction-of-a-tinterfaceddatamodule/
}
unit InterfacedDataModuleU;

interface

uses
  System.SysUtils, System.Classes;

type
  TInterfacedDM = class(TDataModule, IInterface, IInterfaceComponentReference)
  strict protected
    FOwnerIsComponent: Boolean;
    FRefCount: Integer;
  protected
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    class function NewInstance: TObject; override;
    property OwnerIsComponent: Boolean read FOwnerIsComponent;
    property RefCount: Integer read FRefCount;
  end;

implementation

uses
  WinApi.Windows;

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

constructor TInterfacedDM.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TInterfacedDM.Destroy;
begin
  Destroying; // make everyone release references they have towards us
  if FOwnerIsComponent and (FRefCount > 1) then begin
    // perform cleanup of interface references that we refer to.
  end;

  inherited Destroy;
end;

procedure TInterfacedDM.AfterConstruction;
begin
  FOwnerIsComponent := Assigned(Owner) and (Owner is TComponent);
  // Release the NewInstance/constructor's implicit refcount
  InterlockedDecrement(FRefCount);
  inherited AfterConstruction;
end;

procedure TInterfacedDM.BeforeDestruction;
{$IFDEF DEBUG}
var
  WarningMessage: string;
{$ENDIF DEBUG}
begin
  if (RefCount <> 0) then begin
    if not OwnerIsComponent then
      System.Error(reInvalidPtr)
{$IFDEF DEBUG}
    else begin
      WarningMessage := Format('Trying to destroy an Owned TInterfacedDataModule of class %s ' +
        'named %s that still has %d interface references left',
        [ClassName, Name, RefCount]);
      OutputDebugString(PChar(WarningMessage));
    end;
{$ENDIF DEBUG}
  end;
  inherited BeforeDestruction;
end;

class function TInterfacedDM.NewInstance: TObject;
begin
  // Set an implicit refcount so that refcounting
  // during construction won't destroy the object.
  Result := inherited NewInstance;
  TInterfacedDM(Result).FRefCount := 1;
end;

{ IInterface }

function TInterfacedDM._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TInterfacedDM._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  { If we are not being used as a TComponent, then use refcount to manage our
    lifetime as with TInterfacedObject. }
  if (Result = 0) and not FOwnerIsComponent then
    Destroy;
end;

end.

