unit QMGraphics;

{
  Misc Image/Graphics Functions
  Copyright 2001-2014 Oasis Digital
}

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  System.Classes, System.SysUtils, SynGdiPlus;

function ConvertTIFStreamToPNGFile(TIFStream:TBytesStream; SaveToFileName: String): Boolean;

implementation

function ConvertTIFStreamToPNGFile(TIFStream:TBytesStream; SaveToFileName: String): Boolean;
var
  TIF: TTiffImage;
  PNGSaveStatus: TGdipStatus;
  PNGFileStream: TFileStream;
begin
  Assert(Assigned(TIFStream), 'The TIFStream must be assigned in order to convert the image.');

  GDip := TGDIPlusFull.Create('gdiplus.dll');
  TIF := TTiffImage.Create;
  PNGFileStream := TFileStream.Create(SaveToFileName, fmCreate or fmOpenWrite);
  try
    TIF.LoadFromStream(TIFStream);
    //Convert the TIF to PNG
    PNGSaveStatus := TIF.SaveAs(PNGFileStream, gptPNG, 100);
    Result := (PNGSaveStatus = stOk);
  finally
    FreeAndNil(TIF);
    FreeAndNil(PNGFileStream);
    FreeAndNil(GDip);
  end;
end;

end.
