unit AddressUtils;

interface

type
  TUSStreetAddress = class(TObject)
  strict private
    procedure ParseAddress;
    procedure CheckForValidIntegerRange(const NumRange: string);
  public
    FullAddress: string;
    StreetNumber: string;
    EndingStreetNumber: string;
    StreetName: string;
    constructor Create(const Address: string);
  end;

procedure SplitAddress(const Address: string; out Number, EndingNumber, Street: string);

implementation

uses System.SysUtils, System.Classes,
  OdMiscUtils;

procedure SplitAddress(const Address: string; out Number, EndingNumber, Street: string);
var
  Addy: TUSStreetAddress;
begin
  Addy := TUSStreetAddress.Create(Address);
  try
    Number := Addy.StreetNumber;
    EndingNumber := Addy.EndingStreetNumber;
    Street := Addy.StreetName;
  finally
    FreeAndNil(Addy);
  end;
end;

{ TUSStreetAddress }

constructor TUSStreetAddress.Create(const Address: string);
begin
  Assert(NotEmpty(Address), 'Address cannot be blank.');
  StreetNumber := '';
  EndingStreetNumber := '';
  StreetName := '';
  FullAddress := Address;
  ParseAddress;
end;


procedure TUSStreetAddress.CheckForValidIntegerRange(const NumRange: string);
var
  DashPos: Integer;
  Num: string;
begin
  DashPos := Pos('-', NumRange);
  if DashPos > 0 then begin
    Num := Copy(NumRange, 1, DashPos-1);
    if IsInteger(Num) then begin
      StreetNumber := Num;
      Num := Copy(NumRange, DashPos+1, Length(NumRange)-DashPos);
      if IsInteger(Num) then
        EndingStreetNumber := Num;
    end;
  end;
end;

procedure TUSStreetAddress.ParseAddress;
var
  SL: TStringList;
  s: string;
begin
  SL := TStringList.Create;
  try
    // Explode the address string parts into a string list
    ExtractStrings([' ', #09], [' ', #09], PWideChar(FullAddress), SL);
    if SL.Count < 1 then
      raise Exception.CreateFmt('No elements were found in the address string [%s]',
        [FullAddress]);

    if IsInteger(SL.Strings[0]) then
      StreetNumber := SL.Strings[0]
    else // First element is not a number, but if it is a range like 9991-9999, handle it.
      CheckForValidIntegerRange(SL.Strings[0]);

    // If no street # in first element, put the whole address in street name and bail.
    if IsEmpty(StreetNumber) then begin
      StreetName := FullAddress;
      Exit;
    end;

    // Already handled the first element, so discard it and put the rest back together in street name
    SL.Delete(0);
    for s in SL do
      StreetName := StreetName + ' ' + s;
    StreetName := Trim(StreetName);
  finally
    FreeAndNil(SL);
  end;
end;

end.
