unit OdDBTimeEdit;

interface

uses dxCntner, dxExEdtr, dxDBELib;

type
  TODDBTimeEdit = class(TdxDBTimeEdit)
  private
    FShowButton: Boolean;
  protected
    function CreateViewData(IsPaintCopy: Boolean): TdxEditViewData; override;
  published
    property ShowButton: Boolean read FShowButton write FShowButton;
  end;

procedure Register;

implementation

uses Classes, Graphics;

procedure Register;
begin
  RegisterComponents('OD', [TODDBTimeEdit]);
end;

function TODDBTimeEdit.CreateViewData(IsPaintCopy: Boolean): TdxEditViewData;
begin
  Result := inherited CreateViewData(IsPaintCopy);
  if not ShowButton then
    (Result as TdxSpinEditViewData).ShowButton := False;
  if not Enabled then begin
    //Brush := GetSysColorBrush(COLOR_BTNFACE);
    //BkColor := GetSysColor(COLOR_BTNFACE);
    //Result.TextColor := clBlack; //GetSysColor(COLOR_BTNSHADOW);
  end;
end;

end.

