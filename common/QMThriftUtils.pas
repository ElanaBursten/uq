unit QMThriftUtils;

interface

uses
  Classes, SysUtils, Data.DB, Rtti, Variants, Thrift, Thrift.Serializer,
  Thrift.Protocol, Thrift.Protocol.JSON, Generics.Collections, Thrift.Collections
{$IFDEF USE_FIREDAC}
  , uADCompClient, uADStanParam
{$ENDIF}
  ;

type
  // Unfortunately, TInterfacedObject is the parent class for Thrift structure objects.
  // This class helper lets us access the properties by name.
  TThriftObjectHelper = class helper for TInterfacedObject
  public
    function HasProperty(const PropName: string): Boolean;
    function GetPropValue(const PropName: string): Variant;
    procedure SetPropValue(const PropName: string; Value: Variant; Required: Boolean=False);
    function GetPropValueAsIBase(const PropName: string): IBase;
  end;

function IsValueProperty(const Prop: TRttiProperty; const ParentClass: TClass): Boolean;
function SerializeToJSON(Struct: IBase): string;
procedure DeserializeFromJSON(const JSON: string; Target: IBase);
procedure DataRowToThrift(Data: TDataSet; ThriftObj: IBase);
// TODO: In progress...  procedure DataSetToThrift(Data: TDataset; ThriftList: IThriftList<IBase>);
function ColumnNameToThriftName(const ColName: string): string;
{$IFDEF USE_FIREDAC}
function ExecuteSQLWithParams(Conn: TADConnection; const SQL: string; ThriftObj: IBase; const MapZeroToNull: Boolean=True): Integer;
procedure AssignParamsFromThriftObj(Parameters: TADParams; ThriftObj: IBase; MapZeroToNull: Boolean=True);
{$ENDIF}

const
  EVENT_IS_SET = '__isset_';

implementation

uses
  TypInfo, JclStrings, OdMiscUtils, OdDbUtils, OdIsoDates;

function ImplementsProperty(const Prop: TRttiProperty; const ParentClass: TClass): Boolean;
begin
  // True when Prop is implemented by ParentClass; False if Prop is inherited.
  Result := TRttiInstanceType(Prop.Parent).MetaclassType = ParentClass;
end;

function IsValueProperty(const Prop: TRttiProperty; const ParentClass: TClass): Boolean;
begin
  Result := ImplementsProperty(Prop, ParentClass) and
    (not StrBeginsWith(EVENT_IS_SET, Prop.Name));
end;

function CreateJSONProtocol: IProtocolFactory;
begin
  Result := TJSONProtocolImpl.TFactory.Create;
end;

function SerializeToJSON(Struct: IBase): string;
var
  Stream: TStringStream;
  S: TSerializer;
begin
  Stream := TStringStream.Create('',  TEncoding.UTF8);
  try
    S := TSerializer.Create(CreateJSONProtocol);
    // Serialize our struct to the memory stream
    Stream.Size := 0;
    S.Serialize(Struct, Stream);
    Result := Stream.DataString;
  finally
    FreeAndNil(Stream);
    FreeAndNil(S);
  end;
end;

procedure DeserializeFromJSON(const JSON: string; Target: IBase);
var
  Stream: TStringStream;
  D: TDeserializer;
begin
  Assert(Assigned(Target));
  Stream := TStringStream.Create('',  TEncoding.UTF8);
  try
    D := TDeserializer.Create(CreateJSONProtocol);
    // Deserialize our struct from the memory stream
    Stream.WriteString(JSON);
    D.Deserialize(Stream, Target);
  finally
    FreeAndNil(Stream);
    FreeAndNil(D);
  end;
end;

// convert a db column name to a property name (remove _ and make CamelCase)
function ColumnNameToThriftName(const ColName: string): string;
var
  Done: Boolean;
  Start: PChar;
  Name, Word: string;
begin
  Result := '';
  Name := StringReplace(ColName, '@', '', [rfReplaceAll]);
  Name := StringReplace(Name, '_', ' ', [rfReplaceAll]);
  Start := Pointer(Name);
  repeat
    Done := StrWord(Start, Word);
    if Word <> '' then begin
      Word := StrProper(Word);
      Result := Result + Word;
    end;
  until Done;
end;

procedure DataRowToThrift(Data: TDataSet; ThriftObj: IBase);
var
  F: TField;
begin
  Assert(Assigned(Data), 'Data is undefined');
  Assert(Assigned(ThriftObj), 'ThriftObj is undefined');
  Assert(ThriftObj is TInterfacedObject, 'ThriftObj is not a TInterfacedObject descendent');
  for F in Data.Fields do begin
    if not F.IsNull then begin
      if F.DataType = ftDateTime then
        (ThriftObj as TInterfacedObject).SetPropValue(ColumnNameToThriftName(F.FieldName),
          IsoDateTimeToStr(F.Value))
      else if F.DataType = ftDate then
        (ThriftObj as TInterfacedObject).SetPropValue(ColumnNameToThriftName(F.FieldName),
          IsoDateToStr(F.Value))
      else
        (ThriftObj as TInterfacedObject).SetPropValue(ColumnNameToThriftName(F.FieldName),
          F.Value);
    end;
  end;
end;

{

// TODO: This needs to be finished

procedure DataSetToThrift(Data: TDataset; ThriftList: IThriftList<IBase>);
begin
  Assert(Assigned(Data), 'Data is undefined');
  Assert(Assigned(ThriftList), 'ThriftList is undefined');
  Exit;

  OpenDataSet(Data);
  Data.First;
  while not Data.Eof do begin
//    DataRowToThrift(Data, ThriftList.Items);
    Data.Next;
  end;
end;
}

{$IFDEF USE_FIREDAC}
procedure AssignParamsFromThriftObj(Parameters: TADParams; ThriftObj: IBase;
  MapZeroToNull: Boolean);
var
  I: Integer;
  Param: TADParam;
  PropName: string;
  V: Variant;
begin
  for I := 0 to Parameters.Count - 1 do begin
    Param := Parameters[I];
    try
      PropName := ColumnNameToThriftName(Param.Name);
      if (ThriftObj as TInterfacedObject).HasProperty(PropName) then
        V := (ThriftObj as TInterfacedObject).GetPropValue(PropName)
      else
        V := Null;
      if VarIsStr(V) and IsEmpty(V) then
        Param.Value := Null
      else begin
        if MapZeroToNull and VarIsNumeric(V) and (V = 0) then
          Param.Value := Null
        else
          Param.Value := V;
      end;
    except
      on E: Exception do begin
        E.Message := E.Message + ' param ' + Param.Name;
        raise;
      end;
    end;
  end;
end;


function ExecuteSQLWithParams(Conn: TADConnection; const SQL: string;
  ThriftObj: IBase; const MapZeroToNull: Boolean): Integer;
var
  Command: TADCommand;
begin
  Assert(Assigned(Conn));
  Command := TADCommand.Create(nil);
  try
    Command.Connection := Conn;
    Command.CommandText.Text := SQL;
    AssignParamsFromThriftObj(Command.Params, ThriftObj, MapZeroToNull);
    Command.Execute;
    Result := Command.RowsAffected;
  finally
    FreeAndNil(Command);
  end;
end;
{$ENDIF}


// TThriftObjectHelper class helpers for TInterfacedObject

function TThriftObjectHelper.HasProperty(const PropName: string): Boolean;
var
  CTX: TRttiContext;
  RT: TRttiType;
begin
  RT := CTX.GetType(Self.ClassType);
  Result := (RT.GetProperty(PropName)) <> nil;
end;

function TThriftObjectHelper.GetPropValueAsIBase(const PropName: string): IBase;
var
  CTX: TRttiContext;
  RT: TRttiType;
  Val: TValue;
begin
  RT := CTX.GetType(Self.ClassType);
  Val := RT.GetProperty(PropName).GetValue(Self);
  Assert(Val.Kind = TTypeKind.tkInterface);
  Result := IBase(Val.AsInterface);
end;

function TThriftObjectHelper.GetPropValue(const PropName: string): Variant;

  function GetDateValue(const DateStr: string): Variant;
  begin
    if IsEmpty(DateStr) then
      Result := Null

    // Regex validation would be safer than checking length here
    else if Length(DateStr) = 10 then
      // date only
      Result := IsoStrToDate(DateStr)
    else if Length(DateStr) = 23 then
      // a datetime
      Result := IsoStrToDateTime(DateStr)
    else
      // just return the string for uexpected formats
      Result := DateStr;
  end;

var
  CTX: TRttiContext;
  RT: TRttiType;
  Prop: TRttiProperty;
  Val: TValue;
  BoolValue: Boolean;
begin
  Result := Null;
  if not HasProperty(PropName) then
    raise Exception.CreateFmt('No property named %s', [PropName]);
  RT := CTX.GetType(Self.ClassInfo);
  Prop := RT.GetProperty(PropName);
  if Prop.IsReadable then begin
    Val := Prop.GetValue(Self);
    if Val.Kind = TTypeKind.tkEnumeration then begin
      if not Val.TryAsType<boolean>(BoolValue) then
        raise Exception.Create('Unexpected Enumeration type for Property ' + PropName);
      Result := BoolValue;
    end

    // Ideally the check below would look for Prop.PropertyType='TDateTimeString'.
    // But RTTI says properties declared as TDateTimeString are string :(.
    // Assume any PropName containing 'date' is an ISO date/time string.
    else if (Val.Kind = TTypeKind.tkUString) and
      (Pos('date', Lowercase(PropName)) > 0) then begin
      Result := GetDateValue(Val.AsString);
    end else
      Result := Val.AsVariant;
  end;
end;

procedure TThriftObjectHelper.SetPropValue(const PropName: string;
  Value: Variant; Required: Boolean);
var
  CTX: TRttiContext;
  RT: TRttiType;
  Prop: TRttiProperty;
  Val: TValue;
begin
  if not HasProperty(PropName) then begin
    if Required then
      raise Exception.CreateFmt('No property named %s', [PropName])
    else
      Exit;
  end;

  Val := TValue.FromVariant(Value);
  RT := CTX.GetType(Self.ClassType);
  Prop := RT.GetProperty(PropName);
  if Prop.IsWritable then
    Prop.SetValue(Self, Val);
end;

end.
