unit TicketHighlighter;

interface

uses
  SysUtils, Classes, DB, FormatCriticalDisplayText;

type
  TTicketHighlighter = class(TObject)
  private
    HiLiteRules: TDataSet;
    Ticket: TDataSet;
    Decorator: TKeywordDecorator;
    procedure AddRules(const FieldName: string);
  public
    constructor Create(TicketData, RulesData: TDataSet);
    destructor Destroy; override;
  end;

function GetTicketWorkAsHTML(Ticket, Rules: TDataSet; FullScreen: Boolean): string;

implementation

uses OdMiscUtils;

function GetTicketWorkAsHTML(Ticket, Rules: TDataSet; FullScreen: Boolean): string;
const
  WorkDescripHeader = 'Work Description:';
  WorkAddressHeader = 'Work Address:';
  WorkRemarksHeader = 'Work Remarks:';
  NothingToShow = '(none)';
var
  WorkDescrip: string;
  WorkAddress: string;
  WorkRemarks: string;
  Styles: string;
  Body: string;
  HiLiter: TTicketHighlighter;
  HTML: TStringList;
  BodyFontSize: string;

  procedure BuildWorkAddress;
  var
    CityStateCountyLine: string;
  begin
    //ex: 500 - 600 Peachtree Street
    if NotEmpty(Ticket.FieldByName('work_address_number').AsString) then
      WorkAddress := Ticket.FieldByName('work_address_number').AsString;
    if NotEmpty(Ticket.FieldByName('work_address_number_2').AsString) then //work_address_number_2 is used for cases where the work spans a range of addresses
      WorkAddress := WorkAddress + ' - ' + Ticket.FieldByName('work_address_number_2').AsString;
    if NotEmpty(Ticket.FieldByName('work_address_street').AsString) then
      WorkAddress := WorkAddress + ' ' + Ticket.FieldByName('work_address_street').AsString;
    //new line if street info is populated
    if NotEmpty(WorkAddress) then
      WorkAddress := WorkAddress + '<br>';

    //new line after cross street
    if NotEmpty(Ticket.FieldByName('work_cross').AsString) then
      WorkAddress := WorkAddress + 'Nearest Cross Street: ' + Ticket.FieldByName('work_cross').AsString + '<br>';

    //city, state, county
    if NotEmpty(Ticket.FieldByName('work_city').AsString) then
      CityStateCountyLine := Ticket.FieldByName('work_city').AsString
    else
      CityStateCountyLine := '(Unspecified City)';
    if NotEmpty(Ticket.FieldByName('work_state').AsString) then
      CityStateCountyLine := CityStateCountyLine + ', ' + Ticket.FieldByName('work_state').AsString
    else
      CityStateCountyLine := CityStateCountyLine + ', ' + '(Unspecified State)';
    if NotEmpty(Ticket.FieldByName('work_county').AsString) then
      CityStateCountyLine := CityStateCountyLine + ', ' + Ticket.FieldByName('work_county').AsString
    else
      CityStateCountyLine := CityStateCountyLine + ', ' + '(Unspecified County)';
    //new line if city or county or state is populated
    if NotEmpty(CityStateCountyLine) then
      WorkAddress := WorkAddress + CityStateCountyLine + '<br>';

    //new line after subdivision if populated
    if NotEmpty(Ticket.FieldByName('work_subdivision').AsString) then
      WorkAddress := WorkAddress + 'Subdivision: ' + Ticket.FieldByName('work_subdivision').AsString + '<br>';

    if NotEmpty(Ticket.FieldByName('work_lat').AsString) then begin
      WorkAddress := WorkAddress + 'Coordinates: ' + Ticket.FieldByName('work_lat').AsString;
      if NotEmpty(Ticket.FieldByName('work_long').AsString) then
        WorkAddress := WorkAddress + ', ' + Ticket.FieldByName('work_long').AsString;
    end;

    if IsEmpty(WorkAddress) then
      WorkAddress := NothingToShow;
  end;

begin
  Result := '';
  HTML := nil;
  HiLiter := TTicketHighlighter.Create(Ticket, Rules);
  try
    HTML := TStringList.Create;

    if NotEmpty(Ticket.FieldByName('work_description').AsString) then
      WorkDescrip := Ticket.FieldByName('work_description').AsString
    else
      WorkDescrip := NothingToShow;
    BuildWorkAddress;
    if NotEmpty(Ticket.FieldByName('work_remarks').AsString) then
      WorkRemarks := Ticket.FieldByName('work_remarks').AsString
    else
      WorkRemarks := NothingToShow;
    if FullScreen then
      BodyFontSize := 'large'
    else
      BodyFontSize := 'small';

    HiLiter.Decorator.PlainText := WorkAddress;
    HiLiter.Decorator.Caption := WorkAddressHeader;
    HiLiter.AddRules('work_address');
    Styles := HiLiter.Decorator.StyleDefinitionBlock;
    Body := HiLiter.Decorator.AsHTMLParagraph;

    HiLiter.Decorator.PlainText := WorkDescrip;
    HiLiter.Decorator.Caption := WorkDescripHeader;
    HiLiter.AddRules('work_description');
    Styles := HiLiter.Decorator.StyleDefinitionBlock;
    Body := Body + HiLiter.Decorator.AsHTMLParagraph;

    HiLiter.Decorator.PlainText := WorkRemarks;
    HiLiter.Decorator.Caption := WorkRemarksHeader;
    HiLiter.Decorator.ClearHighlightRules;
    HiLiter.AddRules('work_remarks');
    Styles := Styles + HiLiter.Decorator.StyleDefinitionBlock;
    Body := Body + HiLiter.Decorator.AsHTMLParagraph;

    HTML.Add('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">');
    HTML.Add('<html><head>');
    HTML.Add('<title></title>');
    HTML.Add('<style type="text/css"><!--');
    HTML.Add('h1 {font-family: Tahoma, Geneva, sans-serif; font-size: smaller; font-weight: normal;}');
    HTML.Add('body {font-family: Tahoma, Geneva, sans-serif; font-size: ' + BodyFontSize + ';}');
    HTML.Add(Styles);
    HTML.Add('--></style>');
    HTML.Add('</head>');
    HTML.Add('<body>');
    HTML.Add(Body);
    HTML.Add('</body></html>');
    Result := HTML.Text;
  finally
    FreeAndNil(HTML);
    FreeAndNil(HiLiter);
  end;
end;

procedure TTicketHighlighter.AddRules(const FieldName: string);
begin
  HiLiteRules.Filter := 'field_name=' + QuotedStr(FieldName);
  HiLiteRules.Filtered := True;
  HiLiteRules.Open;
  try
    HiLiteRules.First;
    while not HiLiteRules.Eof do begin
      Decorator.AddHighlightRule(HiLiteRules.FieldByName('word_regex').AsString,
        HiLiteRules.FieldByName('color_name').AsString,
        HiLiteRules.FieldByName('color_code').AsString,
        HiLiteRules.FieldByName('highlight_bold').AsBoolean,
        HiLiteRules.FieldByName('regex_modifier').AsString,
        HiLiteRules.FieldByName('which_match').AsInteger);
      HiLiteRules.Next;
    end;
  finally
    HiLiteRules.Close;
    HiLiteRules.Filtered := False;
  end;
end;

constructor TTicketHighlighter.Create(TicketData, RulesData: TDataSet);
begin
  inherited Create;
  Assert(Assigned(RulesData), 'No rules data assigned');
  Assert(Assigned(TicketData), 'Ticket data not assigned');
  Assert(TicketData.Active and (not TicketData.IsEmpty), 'No active ticket selected');

  Ticket := TicketData;
  HiLiteRules := RulesData;
  Decorator := TKeywordDecorator.Create;
end;

destructor TTicketHighlighter.Destroy;
begin
  FreeAndNil(Decorator);
  inherited;
end;

end.
