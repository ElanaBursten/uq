unit OdAlignGrid;

// Oasis Digital Simple Alignable StringGrid
// Adapted from code posted by Peter Below on Borland newsgroups.

interface

uses
  Classes, Windows, Messages, SysUtils, Controls, Grids;

const
  GM_ACTIVATECELL = WM_USER + 1293;

type
  TGMActivateCell = record
    Msg: Cardinal;
    ACol, ARow: Integer;
    Result: Integer;
  end;

  TOdStringAlignGrid = class;

  TExitCellEvent = procedure(Sender: TOdStringAlignGrid; ACol, ARow: Integer;
    const EditText: string) of object;

  TGetCellAlignmentEvent = procedure(Sender: TOdStringAlignGrid; ACol, ARow: Integer;
    State: TGridDrawState; var CellAlignment: TAlignment) of object;

  TCaptionClickEvent = procedure(sender: TOdStringAlignGrid; ACol, ARow: Integer) of object;

  TOdStringAlignGrid = class(TStringGrid)
  private
    FExitCell: TExitCellEvent;
    FAfterEdit: TExitCellEvent;
    FAlignment: TAlignment;
    FSetCanvasProperties: TDrawCellEvent;
    FGetCellAlignment: TGetCellAlignmentEvent;
    FCaptionClick: TCaptionClickEvent;
    FCellOnMouseDown: TGridCoord;
    procedure GMActivateCell(var Msg: TGMActivateCell); message GM_ACTIVATECELL;
    procedure SetAlignment(const Value: TAlignment);
  protected
    function CreateEditor: TInplaceEdit; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure ExitCell(const EditText: string; ACol, ARow: Integer); virtual;
    procedure AfterEdit(const EditText: string; ACol, ARow: Integer); virtual;
    procedure SetCanvasProperties(ACol, ARow: Longint; Rect: TRect; State: TGridDrawState); virtual;
    procedure DrawCell(ACol, ARow: Longint; Rect: TRect; State: TGridDrawState); override;
    procedure CaptionClick(ACol, ARow: LongInt); virtual;
  public
    function GetCellAlignment(Acol, ARow: Longint; State: TGridDrawState): TAlignment; virtual;
    procedure DefaultDrawCell(ACol, ARow: Longint; Rect: TRect; State: TGridDrawState); virtual;
    procedure ActivateCell(ACol, ARow: Integer);
    property InplaceEditor;
  published
    property OnExitCell: TExitCellEvent read FExitCell write FExitCell;
    property OnAfterEdit: TExitCellEvent read FAfterEdit write FAfterEdit;
    property Alignment: TAlignment read FAlignment write SetAlignment;
    property OnSetCanvasProperties: TDrawCellEvent read FSetCanvasProperties write FSetCanvasProperties;
    property OnGetCellAlignment: TGetCellAlignmentEvent read FGetCellAlignment write FGetCellAlignment;
    property OnCaptionClick: TCaptionClickEvent read FCaptionClick write FCaptionClick;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Oasis Digital', [TOdStringAlignGrid]);
end;

type
  TExInplaceEdit = class(TInplaceEdit)
  private
    FLastCol, FLastRow: Integer;
    FStartingText: string;
    procedure WMKillFocus(var Msg: TMessage); message WM_KILLFOCUS;
    procedure WMSetFocus(var Msg: TMessage); message WM_SETFOCUS;
  public
    procedure CreateParams(var Params: TCreateParams); override;
  end;

{ TOdStringAlignGrid }

procedure TOdStringAlignGrid.ActivateCell(ACol, ARow: Integer);
begin
  PostMessage(handle, GM_ACTIVATECELL, ACol, ARow);
end;

procedure TOdStringAlignGrid.AfterEdit(const EditText: string; ACol, ARow: Integer);
begin
  if Assigned(FAfterEdit) then
    FAfterEdit(Self, ACol, ARow, EditText);
end;

procedure TOdStringAlignGrid.CaptionClick(ACol, ARow: LongInt);
begin
  if Assigned(FCaptionClick) then
    FCaptionClick(Self, ACol, ARow);
end;

function TOdStringAlignGrid.CreateEditor: TInplaceEdit;
begin
  Result := TExInplaceEdit.Create(Self);
end;

procedure TOdStringAlignGrid.DefaultDrawCell(ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
  Flags: array[TAlignment] of DWORD = (DT_LEFT, DT_RIGHT, DT_CENTER);
var
  S: string;
begin
  Canvas.FillRect(Rect);
  S := Cells[ACol, ARow];
  if Length(S) > 0 then begin
    InflateRect(Rect, -2, -2);
    DrawText(Canvas.Handle, PChar(S), Length(S), Rect,
      DT_SINGLELINE or DT_NOPREFIX or DT_VCENTER or
      Flags[GetCellAlignment(ACol, ARow, State)]);
  end;
end;

procedure TOdStringAlignGrid.DrawCell(ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  if Assigned(OnDrawCell) then
    inherited
  else begin
    SetCanvasProperties(ACol, ARow, Rect, State);
    DefaultDrawCell(ACol, ARow, Rect, State);
    Canvas.Font := Font;
    Canvas.Brush := Brush;
  end;
end;

procedure TOdStringAlignGrid.ExitCell(const EditText: string; ACol, ARow: Integer);
begin
  if Assigned(FExitCell) then
    FExitCell(Self, ACol, ARow, EditText);
end;

function TOdStringAlignGrid.GetCellAlignment(Acol, ARow: Integer;
  State: TGridDrawState): TAlignment;
begin
  Result := FAlignment;
  if Assigned(FGetCellAlignment) then
    FGetCellAlignment(Self, ACol, ARow, State, Result);
end;

procedure TOdStringAlignGrid.GMActivateCell(var Msg: TGMActivateCell);
begin
  Col := Msg.ACol;
  Row := Msg.ARow;
  EditorMode := True;
  InplaceEditor.SelectAll;
end;

procedure TOdStringAlignGrid.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if Button = mbLeft then
    MouseToCell(X, Y, FCellOnMouseDown.X, FCellOnMouseDown.Y)
  else
    FCellOnMouseDown := TGridCoord(Point(-1, -1));
end;

procedure TOdStringAlignGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Cell: TGridCoord;
begin
  if Button = mbLeft then
    MouseToCell(X, Y, Cell.X, Cell.Y);
  if CompareMem(@Cell, @FCellOnMouseDown, Sizeof(Cell))
     and ((Cell.X < FixedCols) or (Cell.Y < FixedRows)) then
    CaptionClick(Cell.X, Cell.Y);
  FCellOnMouseDown := TGridCoord(Point(-1, -1));
  inherited;
end;

procedure TOdStringAlignGrid.SetAlignment(const Value: TAlignment);
begin
  if FAlignment <> Value then begin
    FAlignment := Value;
    Invalidate;
    if Assigned(InplaceEditor) then
      TExInplaceEdit(InplaceEditor).RecreateWnd;
  end;
end;

procedure TOdStringAlignGrid.SetCanvasProperties(ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if Assigned(FSetCanvasProperties) then
    FSetCanvasProperties(Self, ACol, ARow, Rect, State);
end;

{ TExInplaceEdit }

procedure TExInplaceEdit.CreateParams(var Params: TCreateParams);
const
  Flags: array[TAlignment] of DWORD = (ES_LEFT, ES_RIGHT, ES_CENTER);
begin
  inherited;
  Params.Style := Params.Style or Flags[TOdStringAlignGrid(grid).Alignment];
end;

procedure TExInplaceEdit.WMKillFocus(var Msg: TMessage);
begin
  TOdStringAlignGrid(Grid).ExitCell(Text, FLastCol, FLastRow);
  if FStartingText <> Text then
    TOdStringAlignGrid(Grid).AfterEdit(Text, FLastCol, FLastRow);
  inherited;
end;

procedure TExInplaceEdit.WMSetFocus(var Msg: TMessage);
begin
  FLastCol := TOdStringAlignGrid(Grid).Col;
  FLastRow := TOdStringAlignGrid(Grid).Row;
  FStartingText := Text;
  inherited;
end;

end.

