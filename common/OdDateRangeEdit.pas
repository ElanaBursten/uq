unit OdDateRangeEdit;

interface

uses Classes, OdButtonEdit;

type
  TDateRangeEdit = class(TCustomButtonEdit)
  private
    FEndDate: TDateTime;
    FDisplayDate: TDateTime;
    FStartDate: TDateTime;
    FExtendEndDate: Boolean;
    FOnChange: TNotifyEvent;
    procedure SetDisplayDate(const Value: TDateTime);
    procedure SetEndDate(const Value: TDateTime);
    procedure SetStartDate(const Value: TDateTime);
    procedure OnButtonClick(Sender: TObject);
    procedure UpdateDisplay;
    procedure SetExtendEndDate(const Value: Boolean);
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    property StartDate: TDateTime read FStartDate write SetStartDate;
    property EndDate: TDateTime read FEndDate write SetEndDate;
    property DisplayDate: TDateTime read FDisplayDate write SetDisplayDate;
    procedure Clear; override;
  published
    property ExtendEndDate: Boolean read FExtendEndDate write SetExtendEndDate default False;
    property TabOrder;
    property TabStop default True;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

procedure Register;

implementation

uses
  OdDateRangeDlg, SysUtils, Controls, Graphics, Windows;

{ TDateRangeEdit }

procedure Register;
begin
  RegisterComponents('Samples', [TDateRangeEdit]);
end;

procedure TDateRangeEdit.Clear;
begin
  inherited;
  FEndDate := 0;
  FStartDate := 0;
  FDisplayDate := 0;
  UpdateDisplay;
end;

constructor TDateRangeEdit.Create(AOwner: TComponent);
begin
  inherited;
  FButton.OnClick := OnButtonClick;
  ReadOnly := True;
end;

procedure TDateRangeEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_DELETE: Clear;
    VK_BACK: Clear;
  end;
end;

procedure TDateRangeEdit.OnButtonClick(Sender: TObject);
begin
  if TDateRangeDialog.Execute(FStartDate, FEndDate, DisplayDate, ExtendEndDate) then begin
    UpdateDisplay;
    if Enabled and Visible and CanFocus then
    try
      SetFocus;
    except
    end;
    if Assigned(FOnChange) then
      FOnChange(Self);
  end;
end;

procedure TDateRangeEdit.SetDisplayDate(const Value: TDateTime);
begin
  FDisplayDate := Value;
end;

procedure TDateRangeEdit.SetEndDate(const Value: TDateTime);
begin
  FEndDate := Value;
  if StartDate > EndDate then
    StartDate := EndDate;
  UpdateDisplay;
end;

procedure TDateRangeEdit.SetExtendEndDate(const Value: Boolean);
begin
  FExtendEndDate := Value;
end;

procedure TDateRangeEdit.SetStartDate(const Value: TDateTime);
begin
  FStartDate := Value;
  if EndDate < StartDate then
    EndDate := StartDate;
  UpdateDisplay;
end;

procedure TDateRangeEdit.UpdateDisplay;
begin
  if (StartDate > 0) and (EndDate > 0) then
    Caption := FormatDateTime(ShortDateFormat, StartDate) + ' - ' + FormatDateTime(ShortDateFormat, EndDate)
  else
    Caption := '';
end;

end.
