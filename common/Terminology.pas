unit Terminology;

interface

uses
  SysUtils, Classes;

type
  TTerm = record
    Token: string;
    ReplaceText: string;
  end;

  TTerminology = class
  private
    FTerminology: string;
    FTerms: array of TTerm;
    function Term_Get(const Token: string): string;
  protected
    procedure Clear;
    procedure DefineTerm(const Token, Text: string);
  public
    procedure Initialize(const TermSet: string);
    function ReplaceTerms(const Str: string): string;
    procedure ReplaceVCL(const Root: TComponent);
    property Terminology: string read  FTerminology;
    property Term[const Token: string]: string read Term_Get;
  end;

var
  AppTerminology: TTerminology;

function TermSetFromState: string;

implementation

uses
  // These are only needed for the ReplaceVCL method - it may be good design to
  // move this functionality to a seperate unit eventually so that this unit
  // is not VCL or library dependant.
  Forms, StdCtrls, ExtCtrls, ComCtrls, Menus, Buttons, DBGrids;

function TermSetFromState: string;
begin
  Result := 'UtilityLocator';
end;

procedure TTerminology.Initialize(const TermSet: string);
begin
  Clear;

  if (TermSet = 'UtilityLocator') then
    FTerminology := TermSet
  else
    raise EAbort.Create('Undefined terminology set');
end;

function TTerminology.ReplaceTerms(const Str: string): string;
var
  wsIndex: Integer;
begin
  if (FTerminology = '') then
    Exit;  // Terminology set has not been specified - do nothing
  Result := Str;
  // Replace all Tokens found in the string with their defined text value
  for wsIndex := 0 to Length(FTerms) - 1 do
    Result := StringReplace(Result, FTerms[wsIndex].Token, FTerms[wsIndex].ReplaceText, [rfReplaceAll, rfIgnoreCase]);
end;

procedure TTerminology.ReplaceVCL(const Root: TComponent);

  procedure ReplaceComponent(const Component: TComponent);
  var
    wsIndex: Integer;
    wsCol: TColumn;
  begin
    // Consider using RTTI to look for and access common display property names
    // such as "caption" instead of looking for specific component classes in
    // the future.
    if (Component is TLabel) then
      with (Component as TLabel) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TCheckBox) then
      with (Component as TCheckBox) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TGroupBox) then
      with (Component as TGroupBox) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TRadioButton) then
      with (Component as TRadioButton) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TRadioGroup) then
      with (Component as TRadioGroup) do
        begin
          Items.Text := ReplaceTerms(Items.Text);
        end
    else if (Component is TButton) then
      with (Component as TButton) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TBitBtn) then
      with (Component as TBitBtn) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TSpeedButton) then
      with (Component as TSpeedButton) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TToolButton) then
      with (Component as TToolButton) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TMenuItem) then
      with (Component as TMenuItem) do
        begin
          Caption := ReplaceTerms(Caption);
        end
    else if (Component is TDBGrid) then
      with (Component as TDBGrid) do
        begin
          for wsIndex := 0 to Columns.Count - 1 do
            begin
              wsCol := Columns.Items[wsIndex];
              wsCol.Title.Caption := ReplaceTerms(wsCol.Title.Caption);
            end;
        end
    else if (Component is TTabSheet) then
      with (Component as TTabSheet) do
        begin
          Caption := ReplaceTerms(Caption);
        end
  end;

var
  wsIndex: Integer;
begin
  if (FTerminology <> '') and (Root <> nil) then begin
    // Process display text for this component
    ReplaceComponent(Root);
    // Process all child components
    for wsIndex := 0 to Root.ComponentCount - 1 do
      ReplaceVCL(Root.Components[wsIndex]);
  end;
end;

procedure TTerminology.Clear;
begin
  FTerminology := '';
  SetLength(FTerms, 0);
end;

procedure TTerminology.DefineTerm(const Token, Text: string);
var
  wsIndex: Integer;
begin
  Assert(FTerminology <> '');

  // Replace text if Token is already defined
  for wsIndex := 0 to Length(FTerms) - 1 do
    if (FTerms[wsIndex].Token = Token) then begin
      FTerms[wsIndex].ReplaceText := Text;
      Exit;
    end;

  // Define token
  SetLength(FTerms, Length(FTerms) + 1);
  FTerms[High(FTerms)].Token := Token;
  FTerms[High(FTerms)].ReplaceText := Text;
end;

function TTerminology.Term_Get(const Token: string): string;
var
  wsIndex: Integer;
begin
  if (FTerminology = '')
    then Exit;  // Terminology set has not been specified - do nothing
  Result := '';
  for wsIndex := 0 to Length(FTerms) - 1 do
    if (FTerms[wsIndex].Token = Token) then begin
      Result := FTerms[wsIndex].ReplaceText;
      Break;
    end;
end;

initialization
  AppTerminology := TTerminology.Create;

finalization
  FreeAndNil(AppTerminology);

end.
