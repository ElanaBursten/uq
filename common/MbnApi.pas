unit MbnApi;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 10/3/2014 6:22:34 AM from Type Library described below.

// ************************************************************************ //
// Type Lib: mbnapi.tlb (1)
// LIBID: {DCBBBAB6-0001-4BBB-AAEE-338E368AF6FA}
// LCID: 0
// Helpfile:
// HelpString: Definition: UCM Extension API for MBN Type Library
// DepndLst:
// (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
// Type Libraries : LIBID_xxxx
// CoClasses : CLASS_xxxx
// DISPInterfaces : DIID_xxxx
// Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MbnApiMajorVersion = 1;
  MbnApiMinorVersion = 0;

  LIBID_MbnApi: TGUID = '{DCBBBAB6-0001-4BBB-AAEE-338E368AF6FA}';

  IID_IMbnInterface: TGUID = '{DCBBBAB6-2001-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSubscriberInformation: TGUID = '{459ECC43-BCF5-11DC-A8A8-001321F1405F}';
  IID_IMbnConnection: TGUID = '{DCBBBAB6-200D-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnInterfaceEvents: TGUID = '{DCBBBAB6-2002-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSignal: TGUID = '{DCBBBAB6-2003-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSignalEvents: TGUID = '{DCBBBAB6-2004-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnPinManager: TGUID = '{DCBBBAB6-2005-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnPin: TGUID = '{DCBBBAB6-2007-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnPinManagerEvents: TGUID = '{DCBBBAB6-2006-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnPinEvents: TGUID = '{DCBBBAB6-2008-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnRegistration: TGUID = '{DCBBBAB6-2009-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnRegistrationEvents: TGUID = '{DCBBBAB6-200A-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionContext: TGUID = '{DCBBBAB6-200B-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionContextEvents: TGUID = '{DCBBBAB6-200C-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionEvents: TGUID = '{DCBBBAB6-200E-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionProfileManager: TGUID = '{DCBBBAB6-200F-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionProfile: TGUID = '{DCBBBAB6-2010-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionProfileEvents: TGUID = '{DCBBBAB6-2011-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSmsConfiguration: TGUID = '{DCBBBAB6-2012-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSmsReadMsgPdu: TGUID = '{DCBBBAB6-2013-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSmsReadMsgTextCdma: TGUID = '{DCBBBAB6-2014-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSms: TGUID = '{DCBBBAB6-2015-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnSmsEvents: TGUID = '{DCBBBAB6-2016-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnServiceActivation: TGUID = '{DCBBBAB6-2017-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnServiceActivationEvents: TGUID = '{DCBBBAB6-2018-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnVendorSpecificOperation: TGUID = '{DCBBBAB6-2019-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnVendorSpecificEvents: TGUID = '{DCBBBAB6-201A-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnInterfaceManager: TGUID = '{DCBBBAB6-201B-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnInterfaceManagerEvents: TGUID = '{DCBBBAB6-201C-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionManager: TGUID = '{DCBBBAB6-201D-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionManagerEvents: TGUID = '{DCBBBAB6-201E-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnConnectionProfileManagerEvents: TGUID = '{DCBBBAB6-201F-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnRadio: TGUID = '{DCCCCAB6-201F-4BBB-AAEE-338E368AF6FA}';
  IID_IMbnRadioEvents: TGUID = '{DCDDDAB6-201F-4BBB-AAEE-338E368AF6FA}';
  IID_IConnectionPointContainer: TGUID = '{B196B284-BAB4-101A-B69C-00AA00341D07}';
  CLASS_MbnConnectionProfileManager: TGUID = '{BDFEE05A-4418-11DD-90ED-001C257CCFF1}';
  IID_IEnumConnectionPoints: TGUID = '{B196B285-BAB4-101A-B69C-00AA00341D07}';
  IID_IConnectionPoint: TGUID = '{B196B286-BAB4-101A-B69C-00AA00341D07}';
  IID_IEnumConnections: TGUID = '{B196B287-BAB4-101A-B69C-00AA00341D07}';
  CLASS_MbnInterfaceManager: TGUID = '{BDFEE05B-4418-11DD-90ED-001C257CCFF1}';
  CLASS_MbnConnectionManager: TGUID = '{BDFEE05C-4418-11DD-90ED-001C257CCFF1}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library
// *********************************************************************//
// Constants for enum MBN_CELLULAR_CLASS
type
  MBN_CELLULAR_CLASS = TOleEnum;
const
  MBN_CELLULAR_CLASS_NONE = $00000000;
  MBN_CELLULAR_CLASS_GSM = $00000001;
  MBN_CELLULAR_CLASS_CDMA = $00000002;

// Constants for enum MBN_DATA_CLASS
type
  MBN_DATA_CLASS = TOleEnum;
const
  MBN_DATA_CLASS_NONE = $00000000;
  MBN_DATA_CLASS_GPRS = $00000001;
  MBN_DATA_CLASS_EDGE = $00000002;
  MBN_DATA_CLASS_UMTS = $00000004;
  MBN_DATA_CLASS_HSDPA = $00000008;
  MBN_DATA_CLASS_HSUPA = $00000010;
  MBN_DATA_CLASS_LTE = $00000020;
  MBN_DATA_CLASS_1XRTT = $00010000;
  MBN_DATA_CLASS_1XEVDO = $00020000;
  MBN_DATA_CLASS_1XEVDO_REVA = $00040000;
  MBN_DATA_CLASS_1XEVDV = $00080000;
  MBN_DATA_CLASS_3XRTT = $00100000;
  MBN_DATA_CLASS_1XEVDO_REVB = $00200000;
  MBN_DATA_CLASS_UMB = $00400000;
  MBN_DATA_CLASS_CUSTOM = $80000000;

// Constants for enum MBN_BAND_CLASS
type
  MBN_BAND_CLASS = TOleEnum;
const
  MBN_BAND_CLASS_NONE = $00000000;
  MBN_BAND_CLASS_0 = $00000001;
  MBN_BAND_CLASS_I = $00000002;
  MBN_BAND_CLASS_II = $00000004;
  MBN_BAND_CLASS_III = $00000008;
  MBN_BAND_CLASS_IV = $00000010;
  MBN_BAND_CLASS_V = $00000020;
  MBN_BAND_CLASS_VI = $00000040;
  MBN_BAND_CLASS_VII = $00000080;
  MBN_BAND_CLASS_VIII = $00000100;
  MBN_BAND_CLASS_IX = $00000200;
  MBN_BAND_CLASS_X = $00000400;
  MBN_BAND_CLASS_XI = $00000800;
  MBN_BAND_CLASS_XII = $00001000;
  MBN_BAND_CLASS_XIII = $00002000;
  MBN_BAND_CLASS_XIV = $00004000;
  MBN_BAND_CLASS_XV = $00008000;
  MBN_BAND_CLASS_XVI = $00010000;
  MBN_BAND_CLASS_XVII = $00020000;
  MBN_BAND_CLASS_CUSTOM = $80000000;

// Constants for enum MBN_VOICE_CLASS
type
  MBN_VOICE_CLASS = TOleEnum;
const
  MBN_VOICE_CLASS_NONE = $00000000;
  MBN_VOICE_CLASS_NO_VOICE = $00000001;
  MBN_VOICE_CLASS_SEPARATE_VOICE_DATA = $00000002;
  MBN_VOICE_CLASS_SIMULTANEOUS_VOICE_DATA = $00000003;

// Constants for enum MBN_CTRL_CAPS
type
  MBN_CTRL_CAPS = TOleEnum;
const
  MBN_CTRL_CAPS_NONE = $00000000;
  MBN_CTRL_CAPS_REG_MANUAL = $00000001;
  MBN_CTRL_CAPS_HW_RADIO_SWITCH = $00000002;
  MBN_CTRL_CAPS_CDMA_MOBILE_IP = $00000004;
  MBN_CTRL_CAPS_CDMA_SIMPLE_IP = $00000008;
  MBN_CTRL_CAPS_PROTECT_UNIQUEID = $00000010;

// Constants for enum MBN_SMS_CAPS
type
  MBN_SMS_CAPS = TOleEnum;
const
  MBN_SMS_CAPS_NONE = $00000000;
  MBN_SMS_CAPS_PDU_RECEIVE = $00000001;
  MBN_SMS_CAPS_PDU_SEND = $00000002;
  MBN_SMS_CAPS_TEXT_RECEIVE = $00000004;
  MBN_SMS_CAPS_TEXT_SEND = $00000008;

// Constants for enum MBN_INTERFACE_CAPS_CONSTANTS
type
  MBN_INTERFACE_CAPS_CONSTANTS = TOleEnum;
const
  MBN_DEVICEID_LEN = $00000012;
  MBN_MANUFACTURER_LEN = $00000020;
  MBN_MODEL_LEN = $00000020;
  MBN_FIRMWARE_LEN = $00000020;

// Constants for enum MBN_READY_STATE
type
  MBN_READY_STATE = TOleEnum;
const
  MBN_READY_STATE_OFF = $00000000;
  MBN_READY_STATE_INITIALIZED = $00000001;
  MBN_READY_STATE_SIM_NOT_INSERTED = $00000002;
  MBN_READY_STATE_BAD_SIM = $00000003;
  MBN_READY_STATE_FAILURE = $00000004;
  MBN_READY_STATE_NOT_ACTIVATED = $00000005;
  MBN_READY_STATE_DEVICE_LOCKED = $00000006;
  MBN_READY_STATE_DEVICE_BLOCKED = $00000007;

// Constants for enum MBN_PROVIDER_STATE
type
  MBN_PROVIDER_STATE = TOleEnum;
const
  MBN_PROVIDER_STATE_NONE = $00000000;
  MBN_PROVIDER_STATE_HOME = $00000001;
  MBN_PROVIDER_STATE_FORBIDDEN = $00000002;
  MBN_PROVIDER_STATE_PREFERRED = $00000004;
  MBN_PROVIDER_STATE_VISIBLE = $00000008;
  MBN_PROVIDER_STATE_REGISTERED = $00000010;

// Constants for enum MBN_PROVIDER_CONSTANTS
type
  MBN_PROVIDER_CONSTANTS = TOleEnum;
const
  MBN_PROVIDERNAME_LEN = $00000014;
  MBN_PROVIDERID_LEN = $00000006;

// Constants for enum MBN_SIGNAL_CONSTANTS
type
  MBN_SIGNAL_CONSTANTS = TOleEnum;
const
  MBN_RSSI_DEFAULT = $FFFFFFFF;
  MBN_RSSI_DISABLE = $00000000;
  MBN_RSSI_UNKNOWN = $00000063;
  MBN_ERROR_RATE_UNKNOWN = $00000063;

// Constants for enum MBN_PIN_TYPE
type
  MBN_PIN_TYPE = TOleEnum;
const
  MBN_PIN_TYPE_NONE = $00000000;
  MBN_PIN_TYPE_CUSTOM = $00000001;
  MBN_PIN_TYPE_PIN1 = $00000002;
  MBN_PIN_TYPE_PIN2 = $00000003;
  MBN_PIN_TYPE_DEVICE_SIM_PIN = $00000004;
  MBN_PIN_TYPE_DEVICE_FIRST_SIM_PIN = $00000005;
  MBN_PIN_TYPE_NETWORK_PIN = $00000006;
  MBN_PIN_TYPE_NETWORK_SUBSET_PIN = $00000007;
  MBN_PIN_TYPE_SVC_PROVIDER_PIN = $00000008;
  MBN_PIN_TYPE_CORPORATE_PIN = $00000009;
  MBN_PIN_TYPE_SUBSIDY_LOCK = $0000000A;

// Constants for enum MBN_PIN_MODE
type
  MBN_PIN_MODE = TOleEnum;
const
  MBN_PIN_MODE_ENABLED = $00000001;
  MBN_PIN_MODE_DISABLED = $00000002;

// Constants for enum MBN_PIN_FORMAT
type
  MBN_PIN_FORMAT = TOleEnum;
const
  MBN_PIN_FORMAT_NONE = $00000000;
  MBN_PIN_FORMAT_NUMERIC = $00000001;
  MBN_PIN_FORMAT_ALPHANUMERIC = $00000002;

// Constants for enum MBN_PIN_CONSTANTS
type
  MBN_PIN_CONSTANTS = TOleEnum;
const
  MBN_ATTEMPTS_REMAINING_UNKNOWN = $FFFFFFFF;
  MBN_PIN_LENGTH_UNKNOWN = $FFFFFFFF;

// Constants for enum MBN_PIN_STATE
type
  MBN_PIN_STATE = TOleEnum;
const
  MBN_PIN_STATE_NONE = $00000000;
  MBN_PIN_STATE_ENTER = $00000001;
  MBN_PIN_STATE_UNBLOCK = $00000002;

// Constants for enum MBN_REGISTRATION_CONSTANTS
type
  MBN_REGISTRATION_CONSTANTS = TOleEnum;
const
  MBN_ROAMTEXT_LEN = $00000040;
  MBN_CDMA_DEFAULT_PROVIDER_ID = $00000000;

// Constants for enum MBN_REGISTER_STATE
type
  MBN_REGISTER_STATE = TOleEnum;
const
  MBN_REGISTER_STATE_NONE = $00000000;
  MBN_REGISTER_STATE_DEREGISTERED = $00000001;
  MBN_REGISTER_STATE_SEARCHING = $00000002;
  MBN_REGISTER_STATE_HOME = $00000003;
  MBN_REGISTER_STATE_ROAMING = $00000004;
  MBN_REGISTER_STATE_PARTNER = $00000005;
  MBN_REGISTER_STATE_DENIED = $00000006;

// Constants for enum MBN_REGISTER_MODE
type
  MBN_REGISTER_MODE = TOleEnum;
const
  MBN_REGISTER_MODE_NONE = $00000000;
  MBN_REGISTER_MODE_AUTOMATIC = $00000001;
  MBN_REGISTER_MODE_MANUAL = $00000002;

// Constants for enum MBN_CONTEXT_CONSTANTS
type
  MBN_CONTEXT_CONSTANTS = TOleEnum;
const
  MBN_ACCESSSTRING_LEN = $00000064;
  MBN_USERNAME_LEN = $000000FF;
  MBN_PASSWORD_LEN = $000000FF;
  MBN_CONTEXT_ID_APPEND = $FFFFFFFF;

// Constants for enum MBN_AUTH_PROTOCOL
type
  MBN_AUTH_PROTOCOL = TOleEnum;
const
  MBN_AUTH_PROTOCOL_NONE = $00000000;
  MBN_AUTH_PROTOCOL_PAP = $00000001;
  MBN_AUTH_PROTOCOL_CHAP = $00000002;
  MBN_AUTH_PROTOCOL_MSCHAPV2 = $00000003;

// Constants for enum MBN_COMPRESSION
type
  MBN_COMPRESSION = TOleEnum;
const
  MBN_COMPRESSION_NONE = $00000000;
  MBN_COMPRESSION_ENABLE = $00000001;

// Constants for enum MBN_CONTEXT_TYPE
type
  MBN_CONTEXT_TYPE = TOleEnum;
const
  MBN_CONTEXT_TYPE_NONE = $00000000;
  MBN_CONTEXT_TYPE_INTERNET = $00000001;
  MBN_CONTEXT_TYPE_VPN = $00000002;
  MBN_CONTEXT_TYPE_VOICE = $00000003;
  MBN_CONTEXT_TYPE_VIDEO_SHARE = $00000004;
  MBN_CONTEXT_TYPE_CUSTOM = $00000005;

// Constants for enum MBN_ACTIVATION_STATE
type
  MBN_ACTIVATION_STATE = TOleEnum;
const
  MBN_ACTIVATION_STATE_NONE = $00000000;
  MBN_ACTIVATION_STATE_ACTIVATED = $00000001;
  MBN_ACTIVATION_STATE_ACTIVATING = $00000002;
  MBN_ACTIVATION_STATE_DEACTIVATED = $00000003;
  MBN_ACTIVATION_STATE_DEACTIVATING = $00000004;

// Constants for enum MBN_CONNECTION_MODE
type
  MBN_CONNECTION_MODE = TOleEnum;
const
  MBN_CONNECTION_MODE_PROFILE = $00000000;
  MBN_CONNECTION_MODE_TMP_PROFILE = $00000001;

// Constants for enum MBN_VOICE_CALL_STATE
type
  MBN_VOICE_CALL_STATE = TOleEnum;
const
  MBN_VOICE_CALL_STATE_NONE = $00000000;
  MBN_VOICE_CALL_STATE_IN_PROGRESS = $00000001;
  MBN_VOICE_CALL_STATE_HANGUP = $00000002;

// Constants for enum MBN_SMS_CONSTANTS
type
  MBN_SMS_CONSTANTS = TOleEnum;
const
  MBN_MESSAGE_INDEX_NONE = $00000000;
  MBN_CDMA_SHORT_MSG_SIZE_UNKNOWN = $00000000;
  MBN_CDMA_SHORT_MSG_SIZE_MAX = $000000A0;

// Constants for enum MBN_MSG_STATUS
type
  MBN_MSG_STATUS = TOleEnum;
const
  MBN_MSG_STATUS_NEW = $00000000;
  MBN_MSG_STATUS_OLD = $00000001;
  MBN_MSG_STATUS_DRAFT = $00000002;
  MBN_MSG_STATUS_SENT = $00000003;

// Constants for enum MBN_SMS_CDMA_LANG
type
  MBN_SMS_CDMA_LANG = TOleEnum;
const
  MBN_SMS_CDMA_LANG_NONE = $00000000;
  MBN_SMS_CDMA_LANG_ENGLISH = $00000001;
  MBN_SMS_CDMA_LANG_FRENCH = $00000002;
  MBN_SMS_CDMA_LANG_SPANISH = $00000003;
  MBN_SMS_CDMA_LANG_JAPANESE = $00000004;
  MBN_SMS_CDMA_LANG_KOREAN = $00000005;
  MBN_SMS_CDMA_LANG_CHINESE = $00000006;
  MBN_SMS_CDMA_LANG_HEBREW = $00000007;

// Constants for enum MBN_SMS_CDMA_ENCODING
type
  MBN_SMS_CDMA_ENCODING = TOleEnum;
const
  MBN_SMS_CDMA_ENCODING_OCTET = $00000000;
  MBN_SMS_CDMA_ENCODING_EPM = $00000001;
  MBN_SMS_CDMA_ENCODING_7BIT_ASCII = $00000002;
  MBN_SMS_CDMA_ENCODING_IA5 = $00000003;
  MBN_SMS_CDMA_ENCODING_UNICODE = $00000004;
  MBN_SMS_CDMA_ENCODING_SHIFT_JIS = $00000005;
  MBN_SMS_CDMA_ENCODING_KOREAN = $00000006;
  MBN_SMS_CDMA_ENCODING_LATIN_HEBREW = $00000007;
  MBN_SMS_CDMA_ENCODING_LATIN = $00000008;
  MBN_SMS_CDMA_ENCODING_GSM_7BIT = $00000009;

// Constants for enum MBN_SMS_FLAG
type
  MBN_SMS_FLAG = TOleEnum;
const
  MBN_SMS_FLAG_ALL = $00000000;
  MBN_SMS_FLAG_INDEX = $00000001;
  MBN_SMS_FLAG_NEW = $00000002;
  MBN_SMS_FLAG_OLD = $00000003;
  MBN_SMS_FLAG_SENT = $00000004;
  MBN_SMS_FLAG_DRAFT = $00000005;

// Constants for enum MBN_SMS_STATUS_FLAG
type
  MBN_SMS_STATUS_FLAG = TOleEnum;
const
  MBN_SMS_FLAG_NONE = $00000000;
  MBN_SMS_FLAG_MESSAGE_STORE_FULL = $00000001;
  MBN_SMS_FLAG_NEW_MESSAGE = $00000002;

// Constants for enum MBN_SMS_FORMAT
type
  MBN_SMS_FORMAT = TOleEnum;
const
  MBN_SMS_FORMAT_NONE = $00000000;
  MBN_SMS_FORMAT_PDU = $00000001;
  MBN_SMS_FORMAT_TEXT = $00000002;

// Constants for enum MBN_RADIO
type
  MBN_RADIO = TOleEnum;
const
  MBN_RADIO_OFF = $00000000;
  MBN_RADIO_ON = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IMbnInterface = interface;
  IMbnSubscriberInformation = interface;
  IMbnConnection = interface;
  IMbnInterfaceEvents = interface;
  IMbnSignal = interface;
  IMbnSignalEvents = interface;
  IMbnPinManager = interface;
  IMbnPin = interface;
  IMbnPinManagerEvents = interface;
  IMbnPinEvents = interface;
  IMbnRegistration = interface;
  IMbnRegistrationEvents = interface;
  IMbnConnectionContext = interface;
  IMbnConnectionContextEvents = interface;
  IMbnConnectionEvents = interface;
  IMbnConnectionProfileManager = interface;
  IMbnConnectionProfile = interface;
  IMbnConnectionProfileEvents = interface;
  IMbnSmsConfiguration = interface;
  IMbnSmsReadMsgPdu = interface;
  IMbnSmsReadMsgTextCdma = interface;
  IMbnSms = interface;
  IMbnSmsEvents = interface;
  IMbnServiceActivation = interface;
  IMbnServiceActivationEvents = interface;
  IMbnVendorSpecificOperation = interface;
  IMbnVendorSpecificEvents = interface;
  IMbnInterfaceManager = interface;
  IMbnInterfaceManagerEvents = interface;
  IMbnConnectionManager = interface;
  IMbnConnectionManagerEvents = interface;
  IMbnConnectionProfileManagerEvents = interface;
  IMbnRadio = interface;
  IMbnRadioEvents = interface;
  IConnectionPointContainer = interface;
  IEnumConnectionPoints = interface;
  IConnectionPoint = interface;
  IEnumConnections = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  MbnConnectionProfileManager = IConnectionPointContainer;
  MbnInterfaceManager = IConnectionPointContainer;
  MbnConnectionManager = IConnectionPointContainer;


// *********************************************************************//
// Declaration of structures, unions and aliases.
// *********************************************************************//
  PUserType1 = ^MBN_PIN_INFO; {*}
  PUserType2 = ^MBN_SMS_FILTER; {*}
  PUserType3 = ^TGUID; {*}

  MBN_INTERFACE_CAPS = packed record
    cellularClass: MBN_CELLULAR_CLASS;
    voiceClass: MBN_VOICE_CLASS;
    dataClass: LongWord;
    customDataClass: WideString;
    gsmBandClass: LongWord;
    cdmaBandClass: LongWord;
    customBandClass: WideString;
    smsCaps: LongWord;
    controlCaps: LongWord;
    deviceID: WideString;
    manufacturer: WideString;
    model: WideString;
    firmwareInfo: WideString;
  end;

  MBN_PROVIDER = packed record
    providerID: WideString;
    providerState: LongWord;
    providerName: WideString;
    dataClass: LongWord;
  end;

  MBN_PIN_INFO = packed record
    pinState: MBN_PIN_STATE;
    pinType: MBN_PIN_TYPE;
    attemptsRemaining: LongWord;
  end;

  MBN_CONTEXT = packed record
    contextID: LongWord;
    contextType: MBN_CONTEXT_TYPE;
    accessString: WideString;
    userName: WideString;
    password: WideString;
    compression: MBN_COMPRESSION;
    authType: MBN_AUTH_PROTOCOL;
  end;

  WWAEXT_SMS_CONSTANTS = MBN_SMS_CONSTANTS;

  MBN_SMS_FILTER = packed record
    flag: MBN_SMS_FLAG;
    messageIndex: LongWord;
  end;

  MBN_SMS_STATUS_INFO = packed record
    flag: LongWord;
    messageIndex: LongWord;
  end;

  tagCONNECTDATA = packed record
    pUnk: IUnknown;
    dwCookie: LongWord;
  end;


// *********************************************************************//
// Interface: IMbnInterface
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2001-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnInterface = interface(IUnknown)
    ['{DCBBBAB6-2001-4BBB-AAEE-338E368AF6FA}']
    function Get_InterfaceID(out InterfaceID: WideString): HResult; stdcall;
    function GetInterfaceCapability(out interfaceCaps: MBN_INTERFACE_CAPS): HResult; stdcall;
    function GetSubscriberInformation(out subscriberInformation: IMbnSubscriberInformation): HResult; stdcall;
    function GetReadyState(out readyState: MBN_READY_STATE): HResult; stdcall;
    function InEmergencyMode(out emergencyMode: WordBool): HResult; stdcall;
    function GetHomeProvider(out homeProvider: MBN_PROVIDER): HResult; stdcall;
    function GetPreferredProviders(out preferredProviders: PSafeArray): HResult; stdcall;
    function SetPreferredProviders(preferredProviders: PSafeArray; out requestID: LongWord): HResult; stdcall;
    function GetVisibleProviders(out age: LongWord; out visibleProviders: PSafeArray): HResult; stdcall;
    function ScanNetwork(out requestID: LongWord): HResult; stdcall;
    function GetConnection(out mbnConnection: IMbnConnection): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSubscriberInformation
// Flags: (384) NonExtensible OleAutomation
// GUID: {459ECC43-BCF5-11DC-A8A8-001321F1405F}
// *********************************************************************//
  IMbnSubscriberInformation = interface(IUnknown)
    ['{459ECC43-BCF5-11DC-A8A8-001321F1405F}']
    function Get_SubscriberID(out SubscriberID: WideString): HResult; stdcall;
    function Get_SimIccID(out SimIccID: WideString): HResult; stdcall;
    function Get_TelephoneNumbers(out TelephoneNumbers: PSafeArray): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnection
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200D-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnection = interface(IUnknown)
    ['{DCBBBAB6-200D-4BBB-AAEE-338E368AF6FA}']
    function Get_ConnectionID(out ConnectionID: WideString): HResult; stdcall;
    function Get_InterfaceID(out InterfaceID: WideString): HResult; stdcall;
    function Connect(connectionMode: MBN_CONNECTION_MODE; strProfile: PWideChar;
                     out requestID: LongWord): HResult; stdcall;
    function Disconnect(out requestID: LongWord): HResult; stdcall;
    function GetConnectionState(out ConnectionState: MBN_ACTIVATION_STATE;
                                out ProfileName: WideString): HResult; stdcall;
    function GetVoiceCallState(out voiceCallState: MBN_VOICE_CALL_STATE): HResult; stdcall;
    function GetActivationNetworkError(out networkError: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnInterfaceEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2002-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnInterfaceEvents = interface(IUnknown)
    ['{DCBBBAB6-2002-4BBB-AAEE-338E368AF6FA}']
    function OnInterfaceCapabilityAvailable(const newInterface: IMbnInterface): HResult; stdcall;
    function OnSubscriberInformationChange(const newInterface: IMbnInterface): HResult; stdcall;
    function OnReadyStateChange(const newInterface: IMbnInterface): HResult; stdcall;
    function OnEmergencyModeChange(const newInterface: IMbnInterface): HResult; stdcall;
    function OnHomeProviderAvailable(const newInterface: IMbnInterface): HResult; stdcall;
    function OnPreferredProvidersChange(const newInterface: IMbnInterface): HResult; stdcall;
    function OnSetPreferredProvidersComplete(const newInterface: IMbnInterface;
                                             requestID: LongWord; status: HResult): HResult; stdcall;
    function OnScanNetworkComplete(const newInterface: IMbnInterface; requestID: LongWord;
                                   status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSignal
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2003-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSignal = interface(IUnknown)
    ['{DCBBBAB6-2003-4BBB-AAEE-338E368AF6FA}']
    function GetSignalStrength(out signalStrength: LongWord): HResult; stdcall;
    function GetSignalError(out signalError: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSignalEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2004-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSignalEvents = interface(IUnknown)
    ['{DCBBBAB6-2004-4BBB-AAEE-338E368AF6FA}']
    function OnSignalStateChange(const newInterface: IMbnSignal): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnPinManager
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2005-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnPinManager = interface(IUnknown)
    ['{DCBBBAB6-2005-4BBB-AAEE-338E368AF6FA}']
    function GetPinList(out pinList: PSafeArray): HResult; stdcall;
    function GetPin(pinType: MBN_PIN_TYPE; out pin: IMbnPin): HResult; stdcall;
    function GetPinState(out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnPin
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2007-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnPin = interface(IUnknown)
    ['{DCBBBAB6-2007-4BBB-AAEE-338E368AF6FA}']
    function Get_pinType(out pinType: MBN_PIN_TYPE): HResult; stdcall;
    function Get_PinFormat(out PinFormat: MBN_PIN_FORMAT): HResult; stdcall;
    function Get_PinLengthMin(out PinLengthMin: LongWord): HResult; stdcall;
    function Get_PinLengthMax(out PinLengthMax: LongWord): HResult; stdcall;
    function Get_PinMode(out PinMode: MBN_PIN_MODE): HResult; stdcall;
    function Enable(pin: PWideChar; out requestID: LongWord): HResult; stdcall;
    function Disable(pin: PWideChar; out requestID: LongWord): HResult; stdcall;
    function Enter(pin: PWideChar; out requestID: LongWord): HResult; stdcall;
    function Change(pin: PWideChar; newPin: PWideChar; out requestID: LongWord): HResult; stdcall;
    function Unblock(puk: PWideChar; newPin: PWideChar; out requestID: LongWord): HResult; stdcall;
    function GetPinManager(out pinManager: IMbnPinManager): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnPinManagerEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2006-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnPinManagerEvents = interface(IUnknown)
    ['{DCBBBAB6-2006-4BBB-AAEE-338E368AF6FA}']
    function OnPinListAvailable(const pinManager: IMbnPinManager): HResult; stdcall;
    function OnGetPinStateComplete(const pinManager: IMbnPinManager; pinInfo: MBN_PIN_INFO;
                                   requestID: LongWord; status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnPinEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2008-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnPinEvents = interface(IUnknown)
    ['{DCBBBAB6-2008-4BBB-AAEE-338E368AF6FA}']
    function OnEnableComplete(const pin: IMbnPin; var pinInfo: MBN_PIN_INFO; requestID: LongWord;
                              status: HResult): HResult; stdcall;
    function OnDisableComplete(const pin: IMbnPin; var pinInfo: MBN_PIN_INFO; requestID: LongWord;
                               status: HResult): HResult; stdcall;
    function OnEnterComplete(const pin: IMbnPin; var pinInfo: MBN_PIN_INFO; requestID: LongWord;
                             status: HResult): HResult; stdcall;
    function OnChangeComplete(const pin: IMbnPin; var pinInfo: MBN_PIN_INFO; requestID: LongWord;
                              status: HResult): HResult; stdcall;
    function OnUnblockComplete(const pin: IMbnPin; var pinInfo: MBN_PIN_INFO; requestID: LongWord;
                               status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnRegistration
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2009-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnRegistration = interface(IUnknown)
    ['{DCBBBAB6-2009-4BBB-AAEE-338E368AF6FA}']
    function GetRegisterState(out registerState: MBN_REGISTER_STATE): HResult; stdcall;
    function GetRegisterMode(out registerMode: MBN_REGISTER_MODE): HResult; stdcall;
    function GetProviderID(out providerID: WideString): HResult; stdcall;
    function GetProviderName(out providerName: WideString): HResult; stdcall;
    function GetRoamingText(out roamingText: WideString): HResult; stdcall;
    function GetAvailableDataClasses(out availableDataClasses: LongWord): HResult; stdcall;
    function GetCurrentDataClass(out currentDataClass: LongWord): HResult; stdcall;
    function GetRegistrationNetworkError(out registrationNetworkError: LongWord): HResult; stdcall;
    function GetPacketAttachNetworkError(out packetAttachNetworkError: LongWord): HResult; stdcall;
    function SetRegisterMode(registerMode: MBN_REGISTER_MODE; providerID: PWideChar;
                             dataClass: LongWord; out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnRegistrationEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200A-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnRegistrationEvents = interface(IUnknown)
    ['{DCBBBAB6-200A-4BBB-AAEE-338E368AF6FA}']
    function OnRegisterModeAvailable(const newInterface: IMbnRegistration): HResult; stdcall;
    function OnRegisterStateChange(const newInterface: IMbnRegistration): HResult; stdcall;
    function OnPacketServiceStateChange(const newInterface: IMbnRegistration): HResult; stdcall;
    function OnSetRegisterModeComplete(const newInterface: IMbnRegistration; requestID: LongWord;
                                       status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionContext
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200B-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionContext = interface(IUnknown)
    ['{DCBBBAB6-200B-4BBB-AAEE-338E368AF6FA}']
    function GetProvisionedContexts(out provisionedContexts: PSafeArray): HResult; stdcall;
    function SetProvisionedContext(provisionedContexts: MBN_CONTEXT; providerID: PWideChar;
                                   out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionContextEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200C-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionContextEvents = interface(IUnknown)
    ['{DCBBBAB6-200C-4BBB-AAEE-338E368AF6FA}']
    function OnProvisionedContextListChange(const newInterface: IMbnConnectionContext): HResult; stdcall;
    function OnSetProvisionedContextComplete(const newInterface: IMbnConnectionContext;
                                             requestID: LongWord; status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200E-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionEvents = interface(IUnknown)
    ['{DCBBBAB6-200E-4BBB-AAEE-338E368AF6FA}']
    function OnConnectComplete(const newConnection: IMbnConnection; requestID: LongWord;
                               status: HResult): HResult; stdcall;
    function OnDisconnectComplete(const newConnection: IMbnConnection; reqeustID: LongWord;
                                  status: HResult): HResult; stdcall;
    function OnConnectStateChange(const newConnection: IMbnConnection): HResult; stdcall;
    function OnVoiceCallStateChange(const newConnection: IMbnConnection): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionProfileManager
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-200F-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionProfileManager = interface(IUnknown)
    ['{DCBBBAB6-200F-4BBB-AAEE-338E368AF6FA}']
    function GetConnectionProfiles(const mbnInterface: IMbnInterface;
                                   out connectionProfiles: PSafeArray): HResult; stdcall;
    function GetConnectionProfile(const mbnInterface: IMbnInterface; ProfileName: PWideChar;
                                  out connectionProfile: IMbnConnectionProfile): HResult; stdcall;
    function CreateConnectionProfile(xmlProfile: PWideChar): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionProfile
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2010-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionProfile = interface(IUnknown)
    ['{DCBBBAB6-2010-4BBB-AAEE-338E368AF6FA}']
    function GetProfileXmlData(out profileData: WideString): HResult; stdcall;
    function UpdateProfile(strProfile: PWideChar): HResult; stdcall;
    function Delete: HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionProfileEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2011-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionProfileEvents = interface(IUnknown)
    ['{DCBBBAB6-2011-4BBB-AAEE-338E368AF6FA}']
    function OnProfileUpdate(const newProfile: IMbnConnectionProfile): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSmsConfiguration
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2012-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSmsConfiguration = interface(IUnknown)
    ['{DCBBBAB6-2012-4BBB-AAEE-338E368AF6FA}']
    function Get_ServiceCenterAddress(out scAddress: WideString): HResult; stdcall;
    function Set_ServiceCenterAddress(scAddress: PWideChar): HResult; stdcall;
    function Get_MaxMessageIndex(out index: LongWord): HResult; stdcall;
    function Get_CdmaShortMsgSize(out shortMsgSize: LongWord): HResult; stdcall;
    function Get_SmsFormat(out SmsFormat: MBN_SMS_FORMAT): HResult; stdcall;
    function Set_SmsFormat(SmsFormat: MBN_SMS_FORMAT): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSmsReadMsgPdu
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2013-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSmsReadMsgPdu = interface(IUnknown)
    ['{DCBBBAB6-2013-4BBB-AAEE-338E368AF6FA}']
    function Get_index(out index: LongWord): HResult; stdcall;
    function Get_status(out status: MBN_MSG_STATUS): HResult; stdcall;
    function Get_PduData(out PduData: WideString): HResult; stdcall;
    function Get_Message(out Message: PSafeArray): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSmsReadMsgTextCdma
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2014-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSmsReadMsgTextCdma = interface(IUnknown)
    ['{DCBBBAB6-2014-4BBB-AAEE-338E368AF6FA}']
    function Get_index(out index: LongWord): HResult; stdcall;
    function Get_status(out status: MBN_MSG_STATUS): HResult; stdcall;
    function Get_Address(out Address: WideString): HResult; stdcall;
    function Get_Timestamp(out Timestamp: WideString): HResult; stdcall;
    function Get_EncodingID(out EncodingID: MBN_SMS_CDMA_ENCODING): HResult; stdcall;
    function Get_LanguageID(out LanguageID: MBN_SMS_CDMA_LANG): HResult; stdcall;
    function Get_SizeInCharacters(out SizeInCharacters: LongWord): HResult; stdcall;
    function Get_Message(out Message: PSafeArray): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSms
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2015-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSms = interface(IUnknown)
    ['{DCBBBAB6-2015-4BBB-AAEE-338E368AF6FA}']
    function GetSmsConfiguration(out smsConfiguration: IMbnSmsConfiguration): HResult; stdcall;
    function SetSmsConfiguration(const smsConfiguration: IMbnSmsConfiguration;
                                 out requestID: LongWord): HResult; stdcall;
    function SmsSendPdu(PduData: PWideChar; size: Byte; out requestID: LongWord): HResult; stdcall;
    function SmsSendCdma(Address: PWideChar; encoding: MBN_SMS_CDMA_ENCODING;
                         language: MBN_SMS_CDMA_LANG; SizeInCharacters: LongWord;
                         Message: PSafeArray; out requestID: LongWord): HResult; stdcall;
    function SmsSendCdmaPdu(Message: PSafeArray; out requestID: LongWord): HResult; stdcall;
    function SmsRead(var smsFilter: MBN_SMS_FILTER; SmsFormat: MBN_SMS_FORMAT;
                     out requestID: LongWord): HResult; stdcall;
    function SmsDelete(var smsFilter: MBN_SMS_FILTER; out requestID: LongWord): HResult; stdcall;
    function GetSmsStatus(out smsStatusInfo: MBN_SMS_STATUS_INFO): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnSmsEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2016-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnSmsEvents = interface(IUnknown)
    ['{DCBBBAB6-2016-4BBB-AAEE-338E368AF6FA}']
    function OnSmsConfigurationChange(const sms: IMbnSms): HResult; stdcall;
    function OnSetSmsConfigurationComplete(const sms: IMbnSms; requestID: LongWord; status: HResult): HResult; stdcall;
    function OnSmsSendComplete(const sms: IMbnSms; requestID: LongWord; status: HResult): HResult; stdcall;
    function OnSmsReadComplete(const sms: IMbnSms; SmsFormat: MBN_SMS_FORMAT; readMsgs: PSafeArray;
                               moreMsgs: WordBool; requestID: LongWord; status: HResult): HResult; stdcall;
    function OnSmsNewClass0Message(const sms: IMbnSms; SmsFormat: MBN_SMS_FORMAT;
                                   readMsgs: PSafeArray): HResult; stdcall;
    function OnSmsDeleteComplete(const sms: IMbnSms; requestID: LongWord; status: HResult): HResult; stdcall;
    function OnSmsStatusChange(const sms: IMbnSms): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnServiceActivation
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2017-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnServiceActivation = interface(IUnknown)
    ['{DCBBBAB6-2017-4BBB-AAEE-338E368AF6FA}']
    function Activate(vendorSpecificData: PSafeArray; out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnServiceActivationEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2018-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnServiceActivationEvents = interface(IUnknown)
    ['{DCBBBAB6-2018-4BBB-AAEE-338E368AF6FA}']
    function OnActivationComplete(const serviceActivation: IMbnServiceActivation;
                                  vendorSpecificData: PSafeArray; requestID: LongWord;
                                  status: HResult; networkError: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnVendorSpecificOperation
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-2019-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnVendorSpecificOperation = interface(IUnknown)
    ['{DCBBBAB6-2019-4BBB-AAEE-338E368AF6FA}']
    function SetVendorSpecific(vendorSpecificData: PSafeArray; out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnVendorSpecificEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201A-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnVendorSpecificEvents = interface(IUnknown)
    ['{DCBBBAB6-201A-4BBB-AAEE-338E368AF6FA}']
    function OnEventNotification(const vendorOperation: IMbnVendorSpecificOperation;
                                 vendorSpecificData: PSafeArray): HResult; stdcall;
    function OnSetVendorSpecificComplete(const vendorOperation: IMbnVendorSpecificOperation;
                                         vendorSpecificData: PSafeArray; requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnInterfaceManager
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201B-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnInterfaceManager = interface(IUnknown)
    ['{DCBBBAB6-201B-4BBB-AAEE-338E368AF6FA}']
    function GetInterface(InterfaceID: PWideChar; out mbnInterface: IMbnInterface): HResult; stdcall;
    function GetInterfaces(out mbnInterfaces: PSafeArray): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnInterfaceManagerEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201C-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnInterfaceManagerEvents = interface(IUnknown)
    ['{DCBBBAB6-201C-4BBB-AAEE-338E368AF6FA}']
    function OnInterfaceArrival(const newInterface: IMbnInterface): HResult; stdcall;
    function OnInterfaceRemoval(const oldInterface: IMbnInterface): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionManager
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201D-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionManager = interface(IUnknown)
    ['{DCBBBAB6-201D-4BBB-AAEE-338E368AF6FA}']
    function GetConnection(ConnectionID: PWideChar; out mbnConnection: IMbnConnection): HResult; stdcall;
    function GetConnections(out mbnConnections: PSafeArray): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionManagerEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201E-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionManagerEvents = interface(IUnknown)
    ['{DCBBBAB6-201E-4BBB-AAEE-338E368AF6FA}']
    function OnConnectionArrival(const newConnection: IMbnConnection): HResult; stdcall;
    function OnConnectionRemoval(const oldConnection: IMbnConnection): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnConnectionProfileManagerEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCBBBAB6-201F-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnConnectionProfileManagerEvents = interface(IUnknown)
    ['{DCBBBAB6-201F-4BBB-AAEE-338E368AF6FA}']
    function OnConnectionProfileArrival(const newConnectionProfile: IMbnConnectionProfile): HResult; stdcall;
    function OnConnectionProfileRemoval(const oldConnectionProfile: IMbnConnectionProfile): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnRadio
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCCCCAB6-201F-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnRadio = interface(IUnknown)
    ['{DCCCCAB6-201F-4BBB-AAEE-338E368AF6FA}']
    function Get_SoftwareRadioState(out SoftwareRadioState: MBN_RADIO): HResult; stdcall;
    function Get_HardwareRadioState(out HardwareRadioState: MBN_RADIO): HResult; stdcall;
    function SetSoftwareRadioState(radioState: MBN_RADIO; out requestID: LongWord): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IMbnRadioEvents
// Flags: (384) NonExtensible OleAutomation
// GUID: {DCDDDAB6-201F-4BBB-AAEE-338E368AF6FA}
// *********************************************************************//
  IMbnRadioEvents = interface(IUnknown)
    ['{DCDDDAB6-201F-4BBB-AAEE-338E368AF6FA}']
    function OnRadioStateChange(const newInterface: IMbnRadio): HResult; stdcall;
    function OnSetSoftwareRadioStateComplete(const newInterface: IMbnRadio; requestID: LongWord;
                                             status: HResult): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IConnectionPointContainer
// Flags: (0)
// GUID: {B196B284-BAB4-101A-B69C-00AA00341D07}
// *********************************************************************//
  IConnectionPointContainer = interface(IUnknown)
    ['{B196B284-BAB4-101A-B69C-00AA00341D07}']
    function EnumConnectionPoints(out ppEnum: IEnumConnectionPoints): HResult; stdcall;
    function FindConnectionPoint(var riid: TGUID; out ppCP: IConnectionPoint): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEnumConnectionPoints
// Flags: (0)
// GUID: {B196B285-BAB4-101A-B69C-00AA00341D07}
// *********************************************************************//
  IEnumConnectionPoints = interface(IUnknown)
    ['{B196B285-BAB4-101A-B69C-00AA00341D07}']
    function RemoteNext(cConnections: LongWord; out ppCP: IConnectionPoint; out pcFetched: LongWord): HResult; stdcall;
    function Skip(cConnections: LongWord): HResult; stdcall;
    function Reset: HResult; stdcall;
    function Clone(out ppEnum: IEnumConnectionPoints): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IConnectionPoint
// Flags: (0)
// GUID: {B196B286-BAB4-101A-B69C-00AA00341D07}
// *********************************************************************//
  IConnectionPoint = interface(IUnknown)
    ['{B196B286-BAB4-101A-B69C-00AA00341D07}']
    function GetConnectionInterface(out pIID: TGUID): HResult; stdcall;
    function GetConnectionPointContainer(out ppCPC: IConnectionPointContainer): HResult; stdcall;
    function Advise(const pUnkSink: IUnknown; out pdwCookie: LongWord): HResult; stdcall;
    function Unadvise(dwCookie: LongWord): HResult; stdcall;
    function EnumConnections(out ppEnum: IEnumConnections): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: IEnumConnections
// Flags: (0)
// GUID: {B196B287-BAB4-101A-B69C-00AA00341D07}
// *********************************************************************//
  IEnumConnections = interface(IUnknown)
    ['{B196B287-BAB4-101A-B69C-00AA00341D07}']
    function RemoteNext(cConnections: LongWord; out rgcd: tagCONNECTDATA; out pcFetched: LongWord): HResult; stdcall;
    function Skip(cConnections: LongWord): HResult; stdcall;
    function Reset: HResult; stdcall;
    function Clone(out ppEnum: IEnumConnections): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoMbnConnectionProfileManager provides a Create and CreateRemote method to
// create instances of the default interface IConnectionPointContainer exposed by
// the CoClass MbnConnectionProfileManager. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoMbnConnectionProfileManager = class
    class function Create: IConnectionPointContainer;
    class function CreateRemote(const MachineName: string): IConnectionPointContainer;
  end;

// *********************************************************************//
// The Class CoMbnInterfaceManager provides a Create and CreateRemote method to
// create instances of the default interface IConnectionPointContainer exposed by
// the CoClass MbnInterfaceManager. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoMbnInterfaceManager = class
    class function Create: IConnectionPointContainer;
    class function CreateRemote(const MachineName: string): IConnectionPointContainer;
  end;

// *********************************************************************//
// The Class CoMbnConnectionManager provides a Create and CreateRemote method to
// create instances of the default interface IConnectionPointContainer exposed by
// the CoClass MbnConnectionManager. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoMbnConnectionManager = class
    class function Create: IConnectionPointContainer;
    class function CreateRemote(const MachineName: string): IConnectionPointContainer;
  end;

implementation

uses ComObj;

class function CoMbnConnectionProfileManager.Create: IConnectionPointContainer;
begin
  Result := CreateComObject(CLASS_MbnConnectionProfileManager) as IConnectionPointContainer;
end;

class function CoMbnConnectionProfileManager.CreateRemote(const MachineName: string): IConnectionPointContainer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MbnConnectionProfileManager) as IConnectionPointContainer;
end;

class function CoMbnInterfaceManager.Create: IConnectionPointContainer;
begin
  Result := CreateComObject(CLASS_MbnInterfaceManager) as IConnectionPointContainer;
end;

class function CoMbnInterfaceManager.CreateRemote(const MachineName: string): IConnectionPointContainer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MbnInterfaceManager) as IConnectionPointContainer;
end;

class function CoMbnConnectionManager.Create: IConnectionPointContainer;
begin
  Result := CreateComObject(CLASS_MbnConnectionManager) as IConnectionPointContainer;
end;

class function CoMbnConnectionManager.CreateRemote(const MachineName: string): IConnectionPointContainer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MbnConnectionManager) as IConnectionPointContainer;
end;

end.

