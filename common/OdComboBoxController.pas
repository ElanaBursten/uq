unit OdComboBoxController;

// This controller allows a TComboBox (not a TDBComboBox) to have specific
// selections in the dropdown that are valid for old/existing database records,
// but are not available to select for current edits.  This allows deprecating
// and still displaying certain dropdown choices, but without allowing those
// choices to be selected for future edits or new records.  This operates mainly
// by setting up event handlers for the specified combobox (OnDrawItem, OnChange)
// and also by creating a TFieldDataLink notifier to listen for changes to the
// current record that require corresponding changes in the combo.
// This controller supports code/description mappings such that a shorter value
// stored in the database can map to a more detailed description displayed to
// the user.  Rather than being modified for each change in the controller
// dropdown configuration, the whole controller is often destroyed and recreated
// when codes, valid selections, or descriptions change.  The exception to this
// is changes such as the AllowAll and AllowSelectOf[Old|Raw]Invalid properties.

interface

uses Windows, Classes, Graphics, Controls, SysUtils, Dialogs, DB, DBCtrls, StdCtrls;

type
  TComboController = class(TObject)
  private
    FDataLink: TFieldDataLink;
    FComboBox: TComboBox;
    FOnChangeEvent: TNotifyEvent;

    FCodes: TStringList;
    FAllowedCodes: TStringList;
    FDisplayValues: TStringList;

    FRawAddedIndex: Integer;
    FRawDataText: string;
    FInvalidColor: TColor;
    FValidColor: TColor;
    FValidSelectedColor: TColor;
    FInvalidSelectedColor: TColor;
    FAllowRawSelect: Boolean;
    FAllowOldSelect: Boolean;
    FOriginalIndex: Integer;
    FAllowAll: Boolean;

    procedure DataChange(Sender: TObject);
    procedure UpdateData(Sender: TObject);
    procedure DrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure ComboChange(Sender: TObject);
    procedure SetComboItems;
    function IsValidChoice(ChoiceIndex: Integer): Boolean;
    function GetValidDisplayIndex(Index: Integer): Integer;
  public
    // Main entry point: Specify the TComboBox to modify, the datasource it reads
    // the current record from, the field it is representing, the codes stored
    // in the database codes representing each selection (short code representing
    // a longer GUI escription), the list of allowed codes (others are grayed out),
    // and finally, an optional list of Values the UI displays that map from the
    // supplied codes.  If no Values are specified, the codes are displayed as
    // the dropdown items.
    constructor Create(ComboBox: TComboBox; DataSource: TDataSource; LinkField: string; Codes, Allowed, Values: TStringList; OnChangeEvent: TNotifyEvent = nil);
    destructor Destroy; override;

    // How to color the valid dropdown selection items (Default clWindowText)
    property ValidColor: TColor read FValidColor write FValidColor;
    // How to color the invalid dropdown selection items (Default clGrayText)
    property InvalidColor: TColor read FInvalidColor write FInvalidColor;
    // How to color the valid selected dropdown selection items (Default clHighlightText)
    property ValidSelectedColor: TColor read FValidSelectedColor write FValidSelectedColor;
    // How to color the invalid selected dropdown selection items (Default clGrayText)
    property InvlaidSelectedColor: TColor read FInvalidSelectedColor write FInvalidSelectedColor;

    // Allow selection of the current value obtained from the current record
    property AllowSelectOfRawInvalid: Boolean read FAllowRawSelect write FAllowRawSelect;
    // Allow selection of the original value, even if it is now an invalid choice
    property AllowSelectOfOldInvalid: Boolean read FAllowOldSelect write FAllowOldSelect;
    // Allow selection of all valid and invalid items
    property AllowAll: Boolean read FAllowAll write FAllowAll;
  end;

implementation

{ TComboController }

procedure TComboController.DrawItem (Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Valid: Boolean;
begin
  Valid := IsValidChoice(Index);

  if (odSelected in State) then begin
    if Valid then
       FComboBox.Canvas.Font.Color := FValidSelectedColor
    else
       FComboBox.Canvas.Font.Color := FInvalidSelectedColor;
  end else begin
    if Valid then
       FComboBox.Canvas.Font.Color := FValidColor
    else
       FComboBox.Canvas.Font.Color := FInvalidColor;
  end;

  FComboBox.Canvas.TextRect(Rect, Rect.Left, Rect.Top, FComboBox.Items[Index])
end;

constructor TComboController.Create(ComboBox: TComboBox; DataSource: TDataSource; LinkField: string; Codes, Allowed, Values: TStringList;
  OnChangeEvent: TNotifyEvent = nil);
begin
  Assert(not Assigned(ComboBox.OnChange), 'Error OnChange Handler set on ' + ComboBox.Name + ' while ComboBox is maintained by TComboController' );

  FCodes := TStringList.Create;
  FAllowedCodes := TStringList.Create;
  if Assigned(Values) then
    FDisplayValues := TStringList.Create;

  FCodes.AddStrings(Codes);

  if Assigned(Allowed) then
    FAllowedCodes.AddStrings(Allowed);

  if Assigned(FDisplayValues) then
    FDisplayValues.AddStrings(Values);

  FComboBox := ComboBox;
  FComboBox.Style := csOwnerDrawFixed;
  FComboBox.OnDrawItem := DrawItem;
  FComboBox.OnChange := ComboChange;

  FRawAddedIndex := -1;
  FRawDataText := '';
  FAllowAll := False;

  FAllowOldSelect := True;
  FAllowRawSelect := True;

  SetComboItems;

  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := FComboBox;
  FDataLink.DataSource := DataSource;
  FDataLink.FieldName := LinkField;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnUpdateData := UpdateData;

  FInvalidColor := clGrayText;
  FValidColor := clWindowText;
  FInvalidSelectedColor := clGrayText;
  FValidSelectedColor := clHighlightText;

  FOnChangeEvent := OnChangeEvent;

  DataChange(nil);
end;

procedure TComboController.DataChange(Sender: TObject);
var
  Index: Integer;
begin
  if FRawAddedIndex >= 0 then
    SetComboItems;
  FRawAddedIndex := -1;

  if FDataLink.Field = nil then
    Exit;

  FRawDataText := FDataLink.Field.AsString;
  Index := FCodes.IndexOf(FRawDataText);
  if Index < 0 then begin
    FRawAddedIndex := FComboBox.Items.Add(FRawDataText); // Add the missing Text and keep the index
    FComboBox.ItemIndex := FRawAddedIndex;
  end else
    FComboBox.ItemIndex := Index;

  FOriginalIndex := Index;
end;

destructor TComboController.Destroy;
begin
  FOnChangeEvent := nil;

  FreeAndNil(FCodes);
  FreeAndNil(FAllowedCodes);
  FreeAndNil(FDisplayValues);
  FreeAndNil(FDataLink);

  FComboBox.Style := csDropDown;
  FComboBox.OnChange := nil;
  FComboBox.OnDrawItem := nil;

  inherited;
end;

procedure TComboController.UpdateData(Sender: TObject);
begin
  if IsValidChoice (FComboBox.ItemIndex) then
    FDataLink.Field.AsString := FCodes[FComboBox.ItemIndex];
end;

procedure TComboController.SetComboItems;
begin
  if csDestroying in FComboBox.ComponentState then
    Exit;

  FComboBox.Clear;

  if Assigned(FDisplayValues) then
    FComboBox.Items.AddStrings(FDisplayValues)
  else
    FComboBox.Items.AddStrings(FCodes);
end;

procedure TComboController.ComboChange(Sender: TObject);
var
  I: Integer;
begin
  if IsValidChoice (FComboBox.ItemIndex) then begin
    I := GetValidDisplayIndex(FComboBox.ItemIndex);

    FDataLink.OnDataChange := nil; // disable onchange event;
    try
      if FDataLink.Edit then begin
        if (I >= 0) and (I < FCodes.Count) then
          FDataLink.Field.AsString := FCodes[I]
        else
          FDataLink.Field.AsString := FComboBox.Items[FRawAddedIndex];
      end;
      if Assigned(FOnChangeEvent) then
        FOnChangeEvent(FComboBox);
    finally
      FDataLink.OnDataChange := DataChange;
    end;
  end else
    ShowMessage('You selected an invalid choice.');
end;

function TComboController.IsValidChoice(ChoiceIndex: Integer): Boolean;
var
  I: Integer;
begin
  Result := FAllowAll;
  if Result then
    Exit;

  I := GetValidDisplayIndex(ChoiceIndex);

  if (I >= 0) and (FAllowedCodes.IndexOf(FCodes[I]) >= 0) then // .. with a valid code?
    Result := True;

  if (ChoiceIndex = FRawAddedIndex) and  FAllowRawSelect then
    Result := True;

  if (ChoiceIndex = FOriginalIndex) and FAllowOldSelect then
    Result := True;
end;

function TComboController.GetValidDisplayIndex(Index: Integer): Integer;
begin
  if Assigned(FDisplayValues) then
    Result := FDisplayValues.IndexOf(FComboBox.Items[Index])
  else
    Result := FCodes.IndexOf(FComboBox.Items[Index]);
end;

end.
