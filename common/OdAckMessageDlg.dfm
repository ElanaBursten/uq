object AckMessageDialog: TAckMessageDialog
  Left = 245
  Top = 108
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Acknowledge Message'
  ClientHeight = 329
  ClientWidth = 392
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 398
  ParentFont = True
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    392
    329)
  PixelsPerInch = 96
  TextHeight = 13
  object MessageMemo: TMemo
    Left = 8
    Top = 8
    Width = 375
    Height = 278
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 288
    Width = 392
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Caption = '  '
    TabOrder = 1
  end
end
