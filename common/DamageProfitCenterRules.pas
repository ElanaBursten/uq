unit DamageProfitCenterRules;

interface

uses
  PriorityRulesBase, DB, SysUtils;

type
  TDamageLocationData = class(TEntityData)
  private
    State: string;
    County: string;
    City: string;
  end;

type
  TDamageProfitCenterRules = class(TPriorityRulesBase)
  protected
    procedure LoadRules; override;
  end;

type
  TDamagePCRule = class(TPriorityRule)
  private
    FRuleID: Integer;
    FPCCode: string;
    FState: string;
    FCounty: string;
    FCity: string;
  public
    constructor Create(const RuleID: Integer; const PCCode, State, County, City: string; const RulePriority: Integer);
    function PerformRule: string; override;
  end;

  TDamagePCFullMatchRule = class(TDamagePCRule)
  public
    function RuleMatched(EntityData: TEntityData): Boolean; override;
  end;

  TDamagePCCountyWildcardRule = class(TDamagePCRule)
  public
    function RuleMatched(EntityData: TEntityData): Boolean; override;
  end;

  TDamagePCCityWildcardRule = class(TDamagePCRule)
  public
    function RuleMatched(EntityData: TEntityData): Boolean; override;
  end;

  TDamagePCCountyCityWildcardRule = class(TDamagePCRule)
  public
    function RuleMatched(EntityData: TEntityData): Boolean; override;
  end;


  // Automatically determine the damage profit center given a damage location
function AutoDetermineDamageProfitCenter(const State, County, City: string;
  const DamagePCRulesData: TDataset): string;


implementation

function AutoDetermineDamageProfitCenter(const State, County, City: string;
  const DamagePCRulesData: TDataset): string;
var
  DamageProfitCenterRules: TDamageProfitCenterRules;
  DamageLocationData: TDamageLocationData;
begin
  Result := '';
  DamageLocationData := TDamageLocationData.Create;
  DamageLocationData.State := State;
  DamageLocationData.County := County;
  DamageLocationData.City := City;

  DamageProfitCenterRules := TDamageProfitCenterRules.Create(nil, DamageLocationData, DamagePCRulesData);
  try
    Result := DamageProfitCenterRules.ProcessRulesForEntity;
  finally
    DamageProfitCenterRules.Free;
    DamageLocationData.Free;
  end;
end;

{ TDamagePCRules }

procedure TDamageProfitCenterRules.LoadRules;
var
  iRules: Integer;
begin
  inherited;
  FRules.Clear;
  FRulesData.Close;
  FRulesData.Open;

  for iRules := 0 to FRulesData.recordcount -1 do begin
    with FRulesData do begin
      if ((FieldByName('County').AsString <> '*') and
          (FieldByName('City').AsString <> '*')) then
        FRules.Add(TDamagePCFullMatchRule.Create(
            FieldByName('rule_id').AsInteger,
            FieldByName('pc_code').AsString,
            FieldByName('state').AsString,
            FieldByName('county').AsString,
            FieldByName('city').AsString,
            FieldByName('rule_priority').AsInteger))
      else if ((FieldByName('County').AsString = '*') and
               (FieldByName('City').AsString <> '*')) then
        FRules.Add(TDamagePCCountyWildcardRule.Create(
            FieldByName('rule_id').AsInteger,
            FieldByName('pc_code').AsString,
            FieldByName('state').AsString,
            FieldByName('county').AsString,
            FieldByName('city').AsString,
            FieldByName('rule_priority').AsInteger))
      else if ((FieldByName('County').AsString <> '*') and
               (FieldByName('City').AsString = '*')) then
        FRules.Add(TDamagePCCityWildcardRule.Create(
            FieldByName('rule_id').AsInteger,
            FieldByName('pc_code').AsString,
            FieldByName('state').AsString,
            FieldByName('county').AsString,
            FieldByName('city').AsString,
            FieldByName('rule_priority').AsInteger))
      else if ((FieldByName('County').AsString = '*') and
               (FieldByName('City').AsString = '*')) then
        FRules.Add(TDamagePCCountyCityWildcardRule.Create(
            FieldByName('rule_id').AsInteger,
            FieldByName('pc_code').AsString,
            FieldByName('state').AsString,
            FieldByName('county').AsString,
            FieldByName('city').AsString,
            FieldByName('rule_priority').AsInteger));
      Next;
    end;
  end;
end;

{ TDamagePCRule }

constructor TDamagePCRule.Create(const RuleID: Integer; const PCCode, State,
  County, City: string; const RulePriority: Integer);
begin
  FRuleID := RuleID;
  FPCCode := PCCode;
  FState := State;
  FCounty := County;
  FCity := City;
  FRulePriority := RulePriority;
end;

function TDamagePCRule.PerformRule: string;
begin
  Result := FPCCode;
end;


{ TDamagePCFullMatchRule }

function TDamagePCFullMatchRule.RuleMatched(EntityData: TEntityData): Boolean;
begin
  Result := False;
  if (SameText(FState, TDamageLocationData(EntityData).State) and
      SameText(FCounty, TDamageLocationData(EntityData).County) and
      SameText(FCity, TDamageLocationData(EntityData).City)) then
    Result := True;
end;

{ TDamagePCCountyWildcardRule }

function TDamagePCCountyWildcardRule.RuleMatched(EntityData: TEntityData): Boolean;
begin
  Result := False;
  if (SameText(FState, TDamageLocationData(EntityData).State) and
      SameText(FCounty, '*') and
      SameText(FCity, TDamageLocationData(EntityData).City)) then
    Result := True;
end;

{ TDamagePCCityWildcardRule }

function TDamagePCCityWildcardRule.RuleMatched(EntityData: TEntityData): Boolean;
begin
  Result := False;
  if (SameText(FState, TDamageLocationData(EntityData).State) and
      SameText(FCounty, TDamageLocationData(EntityData).County) and
      SameText(FCity, '*')) then
    Result := True;
end;

{ TDamagePCCountyCityWildcardRule }

function TDamagePCCountyCityWildcardRule.RuleMatched(EntityData: TEntityData): Boolean;
begin
  Result := False;
  if (SameText(FState, TDamageLocationData(EntityData).State) and
      SameText(FCounty, '*') and
      SameText(FCity, '*')) then
    Result := True;
end;

end.
