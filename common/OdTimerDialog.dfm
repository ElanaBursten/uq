object TimerDialogForm: TTimerDialogForm
  Left = 480
  Top = 209
  AutoSize = True
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = 'Title'
  ClientHeight = 53
  ClientWidth = 212
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MessageLabel: TLabel
    Left = 0
    Top = 0
    Width = 67
    Height = 13
    Caption = 'MessageLabel'
  end
  object ButtonYes: TButton
    Left = 0
    Top = 28
    Width = 100
    Height = 25
    Caption = '&Yes'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = ButtonYesClick
  end
  object ButtonNo: TButton
    Left = 112
    Top = 28
    Width = 100
    Height = 25
    Cancel = True
    Caption = '&No'
    ModalResult = 2
    TabOrder = 1
    OnClick = ButtonNoClick
  end
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 144
    Top = 8
  end
end
