unit HoursCalc;

interface

uses
  SysUtils, Classes, DB, Math;

type
  // Define the float type used for all hours values
  HoursType = Single;

  TWeekData = array[0..6] of HoursType;

  // Abstract base class to calculate hours based on specific (named) rule sets
  TBaseHoursCalculator = class
    // Input data - all values must be provided by caller
    Worked: TWeekData;       // Number of hours worked per day by the employee
    Callout: TWeekData;      // Number of hours the employee work was working out of the office (on call-outs) each day

    // Output data - these are populated after calling the Calculate method
    Regular: TWeekData;      // Total number of Regular     hours employee is to be paid for per day - Pay rate is 1.0 (100%)
    Overtime: TWeekData;     // Total number of Over Time   hours employee is to be paid for per day - Pay rate is 1.5 (150%)
    DoubleTime: TWeekData;   // Total number of Double Time hours employee is to be paid for per day - Pay rate is 2.0 (200%)
    CalloutCalc: TWeekData;  // Total number of Callout     hours employee is to be paid for per day - Pay rate is 1.5 (150%)

    // Must be over-ridden by descendants to perform the required calculations
    procedure Calculate; virtual;

    // Clears (sets to 0) all of the values in the "output data" arrays
    procedure ClearResults;

    // Add up the daily hours and place the sums in Worked, capping each day at 24 hours
    // BEWARE that this does not accomodate Callout hours, which is OK because
    // it is only used by the old, non-callout-capable hours system
    procedure Tally;

    // Indicates if employees can be paid Double Time according to the specific hours calculator
    function DoubleTimePossible: Boolean; virtual;

    // Must be over-ridden by descendants to provide a descriptive name for the rule
    function Identity: string; virtual; abstract;
  private
    procedure Validate;
  public
    procedure WriteStateToFile(FileName: string);
  end;

// Factory to instantiate an Hours Calculator given a specific rule name:
function CreateHoursCalculator(Code: string): TBaseHoursCalculator;

// Utility methods for shuffling data:
procedure RowToWeekData(D: TDataSet; var Row: TWeekData);
procedure WeekDataToRow(var Row: TWeekData; D: TDataSet);
function TotalWeek(WeekData: TWeekData): HoursType;

implementation

uses
  OdMiscUtils;

type
  TStraightHoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
  end;

  TOver40HoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
  end;

  TEightFortyHoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
  end;

  TTwelveFortyHoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
  end;

  TCaliforniaHoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
    function DoubleTimePossible: Boolean; override;
  end;

  TSalaryNonExemptHoursCalculator = class(TBaseHoursCalculator)
    procedure Calculate; override;
    function Identity: string; override;
  end;

var
  DayFieldNames: array[0..6] of string;

{ Utilities }

procedure RowToWeekData(D: TDataSet; var Row: TWeekData);
var
  I: Integer;
begin
  for I := 0 to 6 do
    Row[I] := D.FieldByName(DayFieldNames[I]).AsFloat;
end;

procedure WeekDataToRow(var Row: TWeekData; D: TDataSet);
var
  I: Integer;
  Field: TField;
  NeedToPost: Boolean;
begin
  D.Edit;
  NeedToPost := False;
  for I := 0 to 6 do begin
    Field := D.FieldByName(DayFieldNames[I]);
    if Field.AsFloat <> Row[I] then begin
      Field.AsFloat := Row[I];
      NeedToPost := True;
    end;
  end;

  if NeedToPost then
    D.Post
  else
    D.Cancel;
end;

function TotalWeek(WeekData: TWeekData): HoursType;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to 6 do begin
    Result := Result + WeekData[I];
  end;
end;

{ TBaseHoursCalculator }

procedure TBaseHoursCalculator.Calculate;
begin
  // Base behaviour is to clear it all out
  ClearResults;
end;

procedure TBaseHoursCalculator.ClearResults;
var
  I: Integer;
begin
  for I := 0 to 6 do begin
    Regular[I] := 0;
    Overtime[I] := 0;
    DoubleTime[I] := 0;
    CalloutCalc[I] := 0;
  end;
end;

function TBaseHoursCalculator.DoubleTimePossible: Boolean;
begin
  Result := False;
end;

procedure TBaseHoursCalculator.Tally;
var
  I: Integer;
begin
  // Note: This ignores callout hours.  See above for details.
  for I := 0 to 6 do
    Worked[I] := Min(Regular[I] + Overtime[I] + DoubleTime[I], 24);
end;

procedure TBaseHoursCalculator.Validate;
// This method performs basic math cross-checking to ensure that the calculated
// total output values match the total input values for each day.
var
  I: Integer;
  RawTotal, CalcTotal: HoursType;
  ProblemFound: Boolean;
begin
  inherited;
  ProblemFound := False;
  for I := 0 to 6 do begin
    RawTotal := Worked[I] + Callout[I];
    CalcTotal := Regular[I] + Overtime[I] + DoubleTime[I] + CalloutCalc[I];
    if not FloatEquals(RawTotal, CalcTotal) then
      ProblemFound := True;
  end;

  if ProblemFound then begin
    WriteStateToFile(GetWindowsTempPath + 'hourscalc-error.log');
    raise Exception.Create('Validation problem in hours calculation, please contact technical support');
  end;
end;

procedure TBaseHoursCalculator.WriteStateToFile(FileName: string);
var
  SL: TStringList;
  I: Integer;
begin
  SL := TStringList.Create;
  try
    SL.Add('Hours Calc System State:');
    SL.Add(DateTimeToStr(Now));
    SL.Add(Self.Identity);
    SL.Add('Day     Work  CO    |  RT    OT    DT    CO    ');
    for I := 0 to 6 do begin
      SL.Add(Format('  %d   %6.2f%6.2f  |%6.2f%6.2f%6.2f%6.2f',
        [I, Worked[I], Callout[I],Regular[I],Overtime[I],DoubleTime[I],CalloutCalc[I]]));
    end;
    SL.Add('-----------');

    for I := 0 to 6 do
      SL.Add(Format('  Worked[%d]=%6.2f;    Callout[%d]=%6.2f;', [I, Worked[I], I, Callout[I]]));
    SL.Add('');

    for I := 0 to 6 do begin
      SL.Add(Format('  Check(FloatEquals(%6.2f, Regular[%d]));', [Regular[I], I]));
      SL.Add(Format('  Check(FloatEquals(%6.2f, Overtime[%d]));', [Overtime[I], I]));
      SL.Add(Format('  Check(FloatEquals(%6.2f, DoubleTime[%d]));', [DoubleTime[I], I]));
      SL.Add(Format('  Check(FloatEquals(%6.2f, CalloutCalc[%d]));', [CalloutCalc[I], I]));
    end;
    SL.Add('');

    SL.SaveToFile(FileName);
  finally
    FreeAndNil(SL);
  end;
end;

{ TStraightHoursCalculator }

procedure TStraightHoursCalculator.Calculate;
var
  I: Integer;
begin
  inherited;
  for I := 0 to 6 do begin
    Regular[I] := Worked[I];
    Overtime[I] := 0;
    DoubleTime[I] := 0;
    CalloutCalc[I] := Callout[I];
  end;
  Validate;
end;

function TStraightHoursCalculator.Identity: string;
begin
  Result := 'Straight Hours';
end;

{ TOver40HoursCalculator }

{ For the over-40 rule, callout always stays separate, and callout time
  does not make you eligible for OT, since callout is already paid at an
  OT rate.
}

procedure TOver40HoursCalculator.Calculate;
var
  I: Integer;
  RegHours: HoursType;
  WorkHours: HoursType;
begin
  inherited;
  RegHours := 0;
  for I := 0 to 6 do begin
    WorkHours := Worked[I];
    Regular[I] := Min(WorkHours, 40-RegHours);
    Overtime[I] := WorkHours - Regular[I];
    RegHours := RegHours + Regular[I];
    CalloutCalc[I] := Callout[I];
  end;
  Validate;
end;

function TOver40HoursCalculator.Identity: string;
begin
  Result := '40 Hour Rule';
end;

{ TEightFortyHoursCalculator }

{ For the 8/40, callout is handled on a daily basis, so we add it in
  (to effect the RT/OT) then subtract back out
}

procedure SubtractCOfromOTRTCO(COWorked: HoursType; var OT, RT, CO: HoursType);
// NOTE: This method is used by both the EightForty and TwilveForty calculators
var
  AmountToApportion: HoursType;
begin
  // This doesn't do anything useful, other than to make it easier to set a
  // breakpoint later in the method and only get there when there is real
  // work to do.
  if COWorked = 0 then
    Exit;

  // Apportion some to OT:
  AmountToApportion := Min(OT, COWorked);
  OT := OT - AmountToApportion;
  COWorked := COWorked - AmountToApportion;

  // Apportion the rest to RT:
  AmountToApportion := Min(RT, COWorked);
  RT := RT - AmountToApportion;
  COWorked := COWorked - AmountToApportion;

  // The wrinkle here is that we sometimes subtract CO worked, from the CO
  // paid, because some CO worked hours could be paid as DT
  AmountToApportion := Min(CO, COWorked);
  CO := CO - AmountToApportion;
  COWorked := COWorked - AmountToApportion;

  // There should be none left to apportion:
  Assert(COWorked < 0.01, 'Error apportioning CallOut to OT and RT, left over = ' + FloatToStr(COWorked));
end;

procedure TEightFortyHoursCalculator.Calculate;
var
  I: Integer;
  RegHours: HoursType;
  WorkHours: HoursType;
begin
  inherited;
  RegHours := 0;
  for I := 0 to 6 do begin
    WorkHours := Worked[I] + Callout[I];   // add it in
    Regular[I] := Min(Min(WorkHours, 40-RegHours), 8);
    Overtime[I] := WorkHours - Regular[I];
    RegHours := RegHours + Regular[I];

    CalloutCalc[I] := Callout[I];

    SubtractCOfromOTRTCO(CalloutCalc[I], Overtime[I], Regular[I], CalloutCalc[I]);
    Assert(FloatEquals(CalloutCalc[I],Callout[I]),
      'Error, should never realloc CO to other types in 8/40');
  end;
  Validate;
end;

function TEightFortyHoursCalculator.Identity: string;
begin
  Result := '40/Week 8/Day Rules';
end;

{ TTwelveFortyHoursCalculator }

{ For the 12/40, callout is handled on a daily basis, so we add it in
  (to effect the RT/OT) then subtract back out
}

procedure TTwelveFortyHoursCalculator.Calculate;
var
  I: Integer;
  RegHours: HoursType;
  WorkHours: HoursType;
begin
  inherited;
  RegHours := 0;
  for I := 0 to 6 do begin
    WorkHours := Worked[I] + Callout[I];   // add it in
    Regular[I] := Min(Min(WorkHours, 40-RegHours), 12);
    Overtime[I] := WorkHours - Regular[I];
    RegHours := RegHours + Regular[I];

    CalloutCalc[I] := Callout[I];

    SubtractCOfromOTRTCO(CalloutCalc[I], Overtime[I], Regular[I], CalloutCalc[I]);
    Assert(FloatEquals(CalloutCalc[I],Callout[I]),
      'Error, should never realloc CO to other types in 12/40');
  end;
  Validate;
end;

function TTwelveFortyHoursCalculator.Identity: string;
begin
  Result := '40/Week 12/Day Rules';
end;

{ TCaliforniaHoursCalculator }

procedure TCaliforniaHoursCalculator.Calculate;
var
  I: Integer;
  RegHours: HoursType;
  RegHoursStillPossible: HoursType;
  DoubleTimeThreshold: HoursType;
  OverTimeThreshold: HoursType;
  NumDaysWorked: Integer;
  WorkHours: HoursType;
begin
  inherited;
  RegHours := 0;
  NumDaysWorked := 0;
  for I := 0 to 6 do begin
    WorkHours := Worked[I] + Callout[I];   // add it in
    if WorkHours>0 then
      Inc(NumDaysWorked);

    RegHoursStillPossible := 40-RegHours;
    OverTimeThreshold := 8;
    DoubleTimeThreshold := 12;

    if (I = 6) and (NumDaysWorked = 7) then begin  // Special 7th day rule
      OverTimeThreshold := 0;
      DoubleTimeThreshold := 8;
    end;

    Regular[I] := Min(Min(WorkHours, RegHoursStillPossible), OverTimeThreshold);
    Overtime[I] := Min(WorkHours - Regular[I], DoubleTimeThreshold - Regular[I]);
    DoubleTime[I] := WorkHours - Regular[I] - Overtime[I];
    RegHours := RegHours + Regular[I];
    CalloutCalc[I] := Max(0, Callout[I] - DoubleTime[I]);

    SubtractCOfromOTRTCO(CalloutCalc[I], Overtime[I], Regular[I], CalloutCalc[I]);
  end;

  Validate;
end;

function TCaliforniaHoursCalculator.DoubleTimePossible: Boolean;
begin
  Result := True;
end;

function TCaliforniaHoursCalculator.Identity: string;
begin
  Result := 'California Rules';
end;

{ TSalaryNonExemptHoursCalculator }

{ The Salary Non-Exempt rule is very similar to the Over-40 rule above.  The
  difference (at present) is that it only outputs time as regular & overtime,
  it never outputs callout time.  For this rule callout is included into the
  over 40 calculation for overtime.
}
procedure TSalaryNonExemptHoursCalculator.Calculate;
var
  I: Integer;
  RegHours: HoursType;
  WorkHours: HoursType;
begin
  inherited;
  RegHours := 0;
  for I := 0 to 6 do begin
    WorkHours := Worked[I] + Callout[I];   // Include callout time
    Regular[I] := Min(WorkHours, 40-RegHours);
    Overtime[I] := WorkHours - Regular[I];
    RegHours := RegHours + Regular[I];
    CalloutCalc[I] := 0;  // Callout is included in regular / overtime
  end;
  Validate;
end;

function TSalaryNonExemptHoursCalculator.Identity: string;
begin
  Result := 'Salary Non-Exempt';
end;

function CreateHoursCalculator(Code: string): TBaseHoursCalculator;
begin
  Result := nil;

  if      Code = 'CA'      then Result := TCaliforniaHoursCalculator.Create
  else if Code = 'NORM'    then Result := TOver40HoursCalculator.Create
  else if Code = 'TWEL40'  then Result := TTwelveFortyHoursCalculator.Create
  else if Code = 'EIGHT40' then Result := TEightFortyHoursCalculator.Create
  else if Code = 'SAL'     then Result := TStraightHoursCalculator.Create
  else if Code = 'SALNEX'  then Result := TSalaryNonExemptHoursCalculator.Create
  else if Code = ''        then Result := TOver40HoursCalculator.Create;  // default for users with nothing specified

  if Result = nil then
    raise Exception.Create('Unable to process hours rules for ' + Code +
     ', please contact technical support or upgrade Q Manager.');
end;

initialization
  DayFieldNames[0] := 'sun_hours';
  DayFieldNames[1] := 'mon_hours';
  DayFieldNames[2] := 'tue_hours';
  DayFieldNames[3] := 'wed_hours';
  DayFieldNames[4] := 'thu_hours';
  DayFieldNames[5] := 'fri_hours';
  DayFieldNames[6] := 'sat_hours';
end.

