unit ACustomFormCONST;

interface
  uses Graphics;

  const
  {Group Background colors (alternating)}
  EvenColor = $00FCF1E4;
  OddColor = $00FFFDFB;

  //Questionnaire field types
  QuestionFieldType: array [1..9] of string = (
    'FL',
    'FF',
    'DATETIME',
    'DATE',
    'PHONE',
    'DROPDN',
    'CHKBOX',
    'BOOL',
    'GRPHEAD');

  DefFrameHeight = 40;

  {Some constants are from StatusFrame as a starting point}
  DEFWIDTH = 172;
  FNAMEWIDTH = 152;
  LNAMEWIDTH = 152;
  DEFHEIGHT = 15;
  SINGLEFRAMEHEIGHT = 25;
  EDITHEIGHT = 21;
  MAXSIZE = 50;

  {ScrollBox Settings}
  SBMARGIN = 3;

  {Font Defaults}
  DEFFONTSIZE = 9;   {Will change to be configurable}
  HEADERFONTSIZE = 10;
  DEFANSWERCOLOR = clNavy;
  RESTRICTEDCOLOR = clMaroon;

  PHONEMASKEXT = '!\(000\)000-0000 \ext. 999999';
  EXT = ' ext.';
  DATEFORMAT = 'mm/dd/yyy tt';
  DATEONLYFORMAT =  'mm/dd/yyy';
  EMPTY_ANSWER = '--';
  EMPTY_DATETIME = ' /  /       :  :';
  EMPTY_DATE = ' /  /    ';

type

  TcfControl = (cfFreeForm, cfPhone, cfDate, cfDateTime,
                cfCheckBox, cfLocDropDown, cfFirstLast,
                cfCustomDropDown, cfGroupHeading);

  TCFPlacement = (cfpDialog, cfpExistingForm, cfpGroup);

  TFormInfo = record     {Typically, we are only going to use this information to set up form}
    FormID: integer;
    Desc: string;             {Can be used on the top of a form for explaining or directions}
    Title: string;
    FormType: string;        {Code for the form for searching}
    ForeignType: integer;     {Ticket=1, Damage-2...etc}
    LinkRefId: integer;       {Link to Reference table (like AT&T damage group) - not required}
    LinkModifier: string;     {Secondary modifier ex: state or some other info}
    ViewStartDate: TDatetime; {Indefinite if not set - not required}
    ViewEndDate: TDatetime;   {not required}
    Modified_Date: TDatetime;
  end;

  TfieldInfo = record
    FormID: integer;
      FormData: TFormInfo;
    FieldID: integer;
    Group: integer;
    Order: integer;
    FormField: string;
    Question: string;
    InfoType: string;
    ControlType: TcfControl;
    Color: TColor;
    RestrictionInt: integer;
    Required: Boolean;
    Answer: string;
    AnsweredBy: integer;
    AnswerForeignId: integer;      {If there is a ticket_id, damage_id, etc. that the Answer is associated with}
    AnswerForeignType: integer;    {Goes along with the ID}
    ExistingAnswerDate: TDatetime; {Set for existing answer in DBISAM}
    ExistingAnswerID: integer;     {Set for existing answer in DBISAM}
    AnswerHasChanged: boolean;
  end;


implementation

end.
