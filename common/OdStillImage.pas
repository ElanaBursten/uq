unit OdStillImage;

interface

uses
  Messages, Sti, ActiveX, JclRegistry;

const
  WM_STI_LAUNCH = WM_USER + 4321;

function InitializeStillImage: HRESULT;
procedure UninitializeStillImage;
function RegisterLaunchApplication(AppName, CommandLine: string): HRESULT;
function UnregisterLaunchApplication(AppName: string): HRESULT;
function LaunchedBySTI: Boolean;
function CheckSTIRegistryEntries(AppName: string): Boolean;

implementation

var
  StillImage: IStillImage;

function InitializeStillImage: HRESULT;
begin
  Result := 0;
  if not Assigned(StillImage) then begin
    CoInitialize(nil);
    Result := CoCreateInstance(CLSID_Sti, nil, CLSCTX_INPROC_SERVER, IID_IStillImage, StillImage);
  end;
end;

procedure UninitializeStillImage;
begin
  if Assigned(StillImage) then begin
    StillImage := nil;
    CoUninitialize;
  end;
end;

function RegisterLaunchApplication(AppName, CommandLine: string): HRESULT;
var
  wsAppName: WideString;
  wsCommandLine: WideString;
  StillImageInitialized: Boolean;
begin
  StillImageInitialized := False;
  if not Assigned(StillImage) then begin
    InitializeStillImage;
    StillImageInitialized := True;
  end;
  wsAppName := AppName;
  wsCommandLine := CommandLine;
  Result := StillImage.RegisterLaunchApplication(PWideChar(wsAppName), PWideChar(wsCommandLine));
  if StillImageInitialized then
    UninitializeStillImage;
end;

function UnregisterLaunchApplication(AppName: string): HRESULT;
var
  wsAppName: WideString;
  StillImageInitialized: Boolean;
begin
  StillImageInitialized := False;
  if not Assigned(StillImage) then begin
    InitializeStillImage;
    StillImageInitialized := True;
  end;
  Assert(StillImage <> nil, 'You must call OdStillImage.InitializeStillImage before using this procedure');
  wsAppName := AppName;
  Result := StillImage.UnRegisterLaunchApplication(PWideChar(wsAppName));
  if StillImageInitialized then
    UninitializeStillImage;
end;

function LaunchedBySTI: Boolean;
begin
  //TODO: Is there a better way to do this?
  Result := False;
  if ParamCount>1 then
    Result := (Pos('/StiDevice:', ParamStr(1)) <> 0) and (Pos('/StiEvent:', ParamStr(2)) <> 0);
end;

function CheckSTIRegistryEntries(AppName: string): Boolean;
const
  STIKey = 'SOFTWARE\Microsoft\Windows\CurrentVersion\StillImage\Registered Applications';
begin
  Result := JclRegistry.RegReadAnsiStringDef(HKLM, STIKey, AppName, '') <> '';
end;

end.
