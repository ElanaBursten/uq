unit uSecure;

interface

const
  APepper:string = 'C2953B544A0F4A209166892E85BE89600AEB6E87F2BD48B8BA7FABEEAAB0508C7284825EFA674FC889357DA1DECD227D';

type
  hashstring = string;

function CheckHash(const input, hash:hashstring):boolean;

function CalcHash(const input:hashstring):hashstring; overload;
function CalcHash(ADate:TDateTime; const input:hashstring):hashstring; overload;

implementation

uses
  SysUtils
{$IFDEF  VER185}
  , HashES
  , StrUtils
  , DateUtils
  , Windows
  ;

function GetUtc:TDateTime;
var
  t:Windows.SYSTEMTIME;
begin
  GetSystemTime(t);
  Result := EncodeDate(t.wYear, t.wMonth, t.wDay) + EncodeTime(t.wHour, t.wMinute, t.wSecond, t.wMilliseconds)
//  result := TTimeZone.Local.ToUniversalTime(now)
end;

function GetHash(const input:hashstring):hashstring;
begin
  result := hashes.CalcHash2(input, haSHA1)
end;

{$ELSE}
  , System.Hash
  , System.StrUtils
  , System.DateUtils
  ;

function GetUtc:TDateTime;
begin
  result := TTimeZone.Local.ToUniversalTime(now)
end;

function GetHash(const input:hashstring):hashstring;
begin
  result := THashSHA1.GetHMAC(input, APepper);
end;

{$ENDIF}

function CheckHash(const input, hash:hashstring):boolean;
var
  n, n2, n3:TDateTime;
  s1, s2, s3:hashstring;
begin
  n :=  GetUtc;
  n2 := IncHour(n, -1);
  n3 := IncHour(n, 1);
  s1 := CalcHash(n, input);
  s2 := CalcHash(n2, input);
  s3 := CalcHash(n3, input);
  result :=
    (CompareText(s1, hash) = 0)
    or (CompareText(s2, hash) = 0)
    or (CompareText(s3, hash) = 0);
//  if not result then
//  begin
//    LogDebug('%d/%d/%d - %s',[YearOf(n), MonthOf(n), DayOf(n), input]);
//    LogDebug('  %s',[hash]);
//    LogDebug('  %d - %s',[HourOf(n), s1]);
//    LogDebug('  %d - %s',[HourOf(n2), s2]);
//  end;
end;

function CalcHash(const input:hashstring):hashstring; overload;
begin
  result := CalcHash(GetUtc, input)
end;

function CalcHash(ADate:TDateTime; const input:hashstring):hashstring;
var
  toHash:hashstring;
begin
  toHash := Format('%s/%s%d%d%s',[
    leftstr(APepper, 80)
    , input
    , HourOf(ADate)
    , Trunc(ADate)
    , rightstr(APepper, 80)
    ]);

  result := GetHash(toHash)
end;

initialization

finalization
//  Writeln('Finalizing uSecure');

end.
