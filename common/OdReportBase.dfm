inherited ReportBaseForm: TReportBaseForm
  Left = 207
  Top = 180
  Caption = 'ReportBaseForm'
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SaveTSVDialog: TSaveDialog
    DefaultExt = 'tsv'
    Filter = 
      'Tab Separated Values|*.tsv|Comma Separated Values|*.csv|All File' +
      's|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 8
    Top = 368
  end
end
