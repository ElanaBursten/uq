unit xqLengthConverter;

interface

uses SysUtils,Data.DB, Data.Win.ADODB;

const
  NoConversionID = 0;

type
  TLengthConversionCalc = class (TObject)
  private
    procedure SetupUnitConversionForLocate(const LocateID: Integer);
    procedure SetupUnitConversionForClient(const ClientID: Integer);
    procedure SetDefaults;
  public

    FirstUnitFactor: Integer;
    RestUnitsFactor: Integer;
    UnitConversionID: Integer;
    UnitType: string;
    qryLocateUnits: TADOQuery;
    constructor Create(ADOConn:TADOConnection; const LocateID: Integer); overload;
    constructor Create(ADOConn:TADOConnection; LocateData: TDataSet); overload;
    destructor Destroy; override;
    function LengthAsBillingUnits(const Length: Integer): Integer;
  end;

implementation

{ TLengthConversionCalc }

procedure TLengthConversionCalc.SetupUnitConversionForLocate(const LocateID: Integer);
const
  Select = 'select c.unit_conversion_id, uc.unit_type, ' +
    'uc.first_unit_factor, uc.rest_units_factor ' +
    'from client c join locate l on l.client_id = c.client_id ' +
    'join billing_unit_conversion uc on uc.unit_conversion_id = c.unit_conversion_id ' +
    'where l.locate_id = %d and Coalesce(uc.first_unit_factor, 0) > 0';
var
  UnitConversionData: TDataSet;
begin

  qryLocateUnits.sql.Text:= Format(Select, [LocateID]);
  qryLocateUnits.Open;
  UnitConversionData:=qryLocateUnits;



  try
    if not UnitConversionData.IsEmpty then begin
      UnitType := UnitConversionData.FieldByName('unit_type').AsString;
      FirstUnitFactor := UnitConversionData.FieldByName('first_unit_factor').AsInteger;
      RestUnitsFactor := UnitConversionData.FieldByName('rest_units_factor').AsInteger;
      UnitConversionID := UnitConversionData.FieldByName('unit_conversion_id').AsInteger;
    end;
  finally
    FreeAndNil(UnitConversionData);
    FreeAndNil(qryLocateUnits);
  end;
end;

procedure TLengthConversionCalc.SetupUnitConversionForClient(const ClientID: Integer);
var
  UnitConversionData: TDataSet;
const
  Select = 'select c.unit_conversion_id, uc.unit_type, ' +
    'uc.first_unit_factor, uc.rest_units_factor ' +
    'from client c ' +
    'join billing_unit_conversion uc on uc.unit_conversion_id = c.unit_conversion_id ' +
    'where c.client_id  = %d and Coalesce(uc.first_unit_factor, 0) > 0';
begin
  qryLocateUnits.sql.Text:= Format(Select, [ClientID]);
  qryLocateUnits.Open;
  UnitConversionData:=qryLocateUnits;

  try
    if not UnitConversionData.IsEmpty then begin
      UnitType := UnitConversionData.FieldByName('unit_type').AsString;
      FirstUnitFactor := UnitConversionData.FieldByName('first_unit_factor').AsInteger;
      RestUnitsFactor := UnitConversionData.FieldByName('rest_units_factor').AsInteger;
      UnitConversionID := UnitConversionData.FieldByName('unit_conversion_id').AsInteger;
    end;
  finally
    FreeAndNil(UnitConversionData);
    FreeAndNil(qryLocateUnits);
  end;
end;

procedure TLengthConversionCalc.SetDefaults;
begin
  FirstUnitFactor := 0;
  RestUnitsFactor := 0;
  UnitConversionID := NoConversionID;
  UnitType := 'Feet';
end;

constructor TLengthConversionCalc.Create(ADOConn:TADOConnection; const LocateID: Integer);
begin
  SetDefaults;
  SetupUnitConversionForLocate(LocateID);
  qryLocateUnits:=TADOquery.create(nil);
end;

constructor TLengthConversionCalc.Create(ADOConn:TADOConnection; LocateData: TDataSet);
begin
  SetDefaults;
  qryLocateUnits:=TADOquery.create(nil);
  Assert(Assigned(LocateData), 'LocateData is unassigned in TLengthConversionCalc.Create');
  Assert(LocateData.Active, 'LocateData is not active in TLengthConversionCalc.Create');
  Assert(Assigned(LocateData.FindField('client_id')), 'LocateData needs a client_id field in TLengthConversionCalc.Create');
  SetupUnitConversionForClient(LocateData.FieldByName('client_id').Value);
end;

destructor TLengthConversionCalc.Destroy;
begin
  inherited;
end;

function TLengthConversionCalc.LengthAsBillingUnits(const Length: Integer): Integer;
var
  RemainingUnits: Integer;
  BillingUnits: Integer;
begin
  Assert(FirstUnitFactor >= 0);
  Assert(RestUnitsFactor >= 0);
  if UnitConversionID = NoConversionID then begin
    Result := 1;
    Exit;
  end;

  if FirstUnitFactor = 0 then
    // There is no conversion to do
    BillingUnits := Length
  else if (RestUnitsFactor = 0) or (Length <= FirstUnitFactor) or (RestUnitsFactor = FirstUnitFactor) then begin
    // All units are converted using the same rate
    BillingUnits := Trunc(Length / FirstUnitFactor);
    if Length mod FirstUnitFactor > 0 then
      BillingUnits := BillingUnits + 1;
  end
  else begin
    // The first unit is at one rate, and the rest at a different
    RemainingUnits := Length - FirstUnitFactor;
    BillingUnits := Trunc(RemainingUnits / RestUnitsFactor) + 1;
    if RemainingUnits mod RestUnitsFactor > 0 then
      BillingUnits := BillingUnits + 1;
  end;
  Result := BillingUnits;
end;

end.
