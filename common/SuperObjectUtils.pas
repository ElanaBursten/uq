unit SuperObjectUtils;

interface

uses
  SuperObject;

function SuperObjectForKey(Source: ISuperObject; const Key: SOString):
  ISuperObject;

implementation

uses
  SysUtils;

function SuperObjectForKey(Source: ISuperObject; const Key: SOString):
  ISuperObject;
var
  Item: TSuperObjectIter;
begin
  Result := nil;
  if Source <> nil then
    try
      if ObjectFindFirst(Source, Item) then
        repeat
          if SameText(Item.Key, Key) then begin
            Result := Item.Val;
            Break;
          end;
        until not ObjectFindNext(Item);
    finally
      ObjectFindClose(Item);
    end;
end;

end.
