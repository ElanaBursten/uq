unit OdSecureHash;

{ OdSecureHash uses Hashes.pas which is a freeware hash implementation that supports
  Unicode.  Currently, we use SHA1.
  Copyright 2014 Oasis Digital

  There are more functions around various projects which need to be extracted
  and moved here.  -Kyle
}

interface

function HashToString16(S: string): string;
function HashFile(const FileName: string): string;

implementation

uses
  SysUtils, Hashes;


function HashToString16(S: string): string;
begin
  Result := UpperCase(Copy(CalcHash2(S, haSHA1), 1, 16));
end;

function HashFile(const FileName: string): string;
begin
  Result := UpperCase(CalcHash(FileName, haSHA1));

  if Result = 'DA39A3EE5E6B4B0D3255BFEF95601890AFD80709' then  //then the file is empty
    //This is the hash that the old SecHash calculated on empty files; this
    //was not to the SHA1 standard. So why are we hard-coding the old SecHash
    //value for empty files? Because there may be existing empty files
    //and this is the solution for the least impact (dealing with old data
    //and getting the clients in sync, or Tempo may need to have workarounds in place for it.)
    Result := '67452301EFCDAB8998BADCFE10325476C3D2E1F0';  //old SecHash value for empty file (non-standard SHA1 hash value)
end;

end.

