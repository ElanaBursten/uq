unit FDUtils;

interface

uses
  System.Classes, System.IniFiles, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UQDbConfig, FireDAC.Stan.Option, FireDAC.Stan.Intf, FireDAC.Stan.Param;

type
  TParamRec = record
    Name: string;
    ParamDataType: TFieldType;
    ParamType: TParamType;
  end;

procedure ConnecTFDConnection(Conn: TFDConnection; const ConnString: string);
procedure ConnecTFDConnectionWithIni(Conn: TFDConnection; Ini: TIniFile; Section: string='DB');
procedure ConnecTFDConnectionWithConfig(Conn: TFDConnection; DbConfig: TODBCDatabaseConfig);
procedure SetConnections(Connection: TFDCustomConnection; DM: TDataModule);

function ExecuteQuery(Connection: TFDCustomConnection; const SQL: string): Integer;
function ExecuteQueryWithParams(Connection: TFDCustomConnection; const SQL: string; Parameters: array of TParamRec; Values: array of Variant): Integer;

procedure LoadNextRecordsets(const SrcDS: TFDRdbmsDataSet; const DS: array of TFDDataSet);

procedure LoadNextRecordset(const SrcDS: TFDRdbmsDataSet; const DS: TFDDataSet);

function GetFieldValue(Connection: TFDConnection; TableName: String; FieldName: String; WhereCondition: String): Variant;

function ValidFireDAC(QueryTimeout: integer): integer;

implementation

uses SysUtils, StrUtils, OdMiscUtils;

procedure DoConnect(Conn: TFDConnection);

  function MakeConnectionName: string;
  begin
    // connection name is Conn_SERVER_DBNAME[_Pooled]
    Result := 'Conn_' + Conn.Params.Values['Server'] +
      '_' + Conn.Params.Values['Database'] +
      IfThen(Conn.Params.Values['Pooled'] = 'True', '_Pooled', '');
  end;

  procedure AddConnectionFormatOptions;
  begin
    Conn.FormatOptions.OwnMapRules := True;
  end;

  procedure AddRequiredConnectionParams;
  begin
    if Conn.Params.IndexOfName('LoginTimeout') = -1 then
      Conn.Params.Add('LoginTimeout=5'); // 5 seconds
    if Conn.Params.IndexOfName('MetaCurSchema') = -1 then
      Conn.Params.Add('MetaCurSchema=dbo'); // so every TFDStoredProc does not need a SchemaName
  end;
var
  ConnName: string;
begin
  Assert(Conn.Params.Count > 0, 'Connection Params must be set before calling DoConnect');
  AddConnectionFormatOptions;
  AddRequiredConnectionParams;

  { FireDAC requires an FDManager managed connection to do pooling. Convert the
  Conn.Params into an FDManager private named Connection and then reset the
  Conn to use our named connection instead. }
  ConnName := MakeConnectionName;
  if ConnName <> Conn.ConnectionDefName then begin
    if not FDManager.IsConnectionDef(ConnName) then
      FDManager.AddConnectionDef(ConnName, 'MSSQL', Conn.Params);
    Conn.Params.Clear;
    Conn.ConnectionDefName := ConnName;
  end;
  Conn.ResourceOptions.KeepConnection := True;
  Conn.Open;
end;

procedure ConnecTFDConnection(Conn: TFDConnection; const ConnString: string);
begin
  Conn.ConnectionString := ConnString;
  DoConnect(Conn);
end;

procedure ConnecTFDConnectionWithIni(Conn: TFDConnection; Ini: TIniFile; Section: string='DB');
var
  ConnString: string;
  QryTimeout: integer;
begin
  Assert(Assigned(Conn), 'Conn must be defined');
  Assert(Assigned(Ini), 'Ini must be defined');

  // See if there is a ConnectionString setting in the ini file
  ConnString := Ini.ReadString(Section, 'FDConnectionString', '');
  if IsEmpty(ConnString) then
    raise Exception.Create('No FDConnectionString configured in ' + Ini.FileName);

  {EB: FireDAC QueryTimeout setting coming in is in seconds, this will convert to ms}
  QryTimeout := Ini.ReadInteger(Section, 'QueryTimeout', 30); // about 30 seconds
  Conn.ResourceOptions.CmdExecTimeout := ValidFireDAC(QryTimeout);
  ConnectFDConnection(Conn, ConnString);
end;

procedure ConnecTFDConnectionWithConfig(Conn: TFDConnection; DbConfig: TODBCDatabaseConfig);
begin
  Assert(Assigned(DBConfig));
  if IsEmpty(DbConfig.ConnString) then
    raise Exception.Create('No ADConnectionString configured');
  ConnecTFDConnection(Conn, DbConfig.ConnString);
end;

procedure SetConnections(Connection: TFDCustomConnection; DM: TDataModule);
var
  i: Integer;
begin
  for i := 0 to DM.ComponentCount - 1 do begin
    if DM.Components[i] is TFDRdbmsDataSet then
      if TFDRdbmsDataSet(DM.Components[i]).Connection = nil then
        TFDRdbmsDataSet(DM.Components[i]).Connection := Connection;
    if DM.Components[i] is TFDCommand then
      if TFDCommand(DM.Components[i]).Connection = nil then
        TFDCommand(DM.Components[i]).Connection := Connection;
  end;
end;

function ExecuteQuery(Connection: TFDCustomConnection; const SQL: string): Integer;
var
  Cmd: TFDCommand;
begin
  Cmd := TFDCommand.Create(nil);
  try
    Cmd.Connection := Connection;
    Cmd.CommandText.Text := SQL;
    Cmd.Execute;
    Result := Cmd.RowsAffected;
  finally
    FreeAndNil(Cmd);
  end;
end;

function ExecuteQueryWithParams(Connection: TFDCustomConnection; const SQL: string;
  Parameters: array of TParamRec; Values: array of Variant): Integer;
var
  I: Integer;
  Cmd: TFDCommand;
begin
  Assert(Length(Values) = Length(Parameters), 'Values array must be the same length as Parameters array');
  Cmd := TFDCommand.Create(nil);
  try
    Cmd.Connection := Connection;
    Cmd.ResourceOptions.ParamCreate := False;
    Cmd.CommandText.Text := SQL;
    for I := Low(Parameters) to High(Parameters) do
      Cmd.Params.CreateParam(Parameters[I].ParamDataType, Parameters[I].Name,
        Parameters[I].ParamType).Value := Values[I];
    Cmd.Execute;
    Result := Cmd.RowsAffected;
  finally
    FreeAndNil(Cmd);
  end;
end;

procedure LoadNextRecordsets(const SrcDS: TFDRdbmsDataSet; const DS: array of TFDDataSet);
var
  cntr: integer;
begin
  Assert(Assigned(SrcDS), 'Source SP must be assigned');
  Assert(SrcDS.FetchOptions.AutoClose = False,
    'FetchOptions.AutoClose must be False for multiple result sets');

  SrcDS.FetchAll;
  DS[Low(DS)].Data := SrcDS.Data;

  for cntr := Low(DS)+1 to High(DS) do begin
    Assert(Assigned(DS[cntr]), 'Result DS must be assigned');
    SrcDS.NextRecordSet;
    if SrcDS.Active then begin
      Assert(Assigned(SrcDS.Data), 'SP.NextRecordset cannot return nil for ' + DS[cntr].Name);
      DS[cntr].Data := SrcDS.Data;
      Assert(DS[cntr].Active, 'Dataset should be active:  ' + DS[cntr].Name);
    end;
  end;
end;

procedure LoadNextRecordset(const SrcDS: TFDRdbmsDataSet; const DS: TFDDataSet);
begin
  Assert(Assigned(DS), 'Result DS must be assigned');
  Assert(Assigned(SrcDS), 'Source SP must be assigned');
  Assert(SrcDS.FetchOptions.AutoClose = False,
    'FetchOptions.AutoClose must be False for multiple result sets');

  SrcDS.NextRecordSet;
  if not SrcDS.Active then
    Exit;

  Assert(Assigned(SrcDS.Data), 'SP.NextRecordset cannot return nil for ' + DS.Name);
  DS.Data := SrcDS.Data;
  Assert(DS.Active, 'Dataset should be active:  ' + DS.Name);
end;

function CreateDatasetWithQuery(Connection: TFDConnection; const SelectSQL: string): TFDQuery;
begin
  // The caller must free the Result when done with it
  Result := TFDQuery.Create(nil);
  try
    Result.Connection := Connection;
    Result.SQL.Text := SelectSQL;
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function GetFieldValue(Connection: TFDConnection; TableName: String; FieldName: String; WhereCondition: String): Variant;
const
  SQL = 'select %s from %s where %s';
var
  Dataset: TFDQuery;
begin
  Dataset := CreateDatasetWithQuery(Connection, Format(SQL, [FieldName, TableName, IfThen(IsEmpty(WhereCondition), '1=1', WhereCondition)]));
  try
    Result := Dataset.FieldByName(FieldName).Value;
  finally
    FreeAndNil(Dataset);
  end;
end;

{Validate QueryTimeout for FireDAC connection - QMANTWO-344}
function ValidFireDAC(QueryTimeout: integer): integer;
begin
  {EB: If it looks like seconds, then convert to Milliseconds (up to 2 hours)}
  if (QueryTimeout > 0) and (QueryTimeout <= 7200)  then
    Result := QueryTimeout * 1000
  {if value appears to be milliseconds (between 1 minute and 2 hours), use as is}
  else If (QueryTimeout >= 60000) and (QueryTimeout <= 7200000) then
      Result := QueryTimeout
  {time unit is out of range...use default}
  else Result := 300000;  //5 minutes
end;

end.

