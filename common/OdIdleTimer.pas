unit OdIdleTimer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, AppEvnts, DateUtils;

type
  TOdIdleTimer = class(TObject)
    ApplicationEvents: TApplicationEvents;
    IdleTimer: TTimer;
    procedure IdleTimerTimer(Sender: TObject);
    procedure ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
    procedure ApplicationEventsMessage(var Msg: tagMSG; var Handled: Boolean);
  private
    FIdleStartTime: TDateTime;
    FLastActiveTime: TDateTime;
    FSecondsBetweenActivity: Integer;
    FIdleDelaySecondsMinimized: Integer;
    FIdleDelaySeconds: Integer;
    FSecondsSinceActive: Integer;
    FSecondsIdle: Integer;
    FEnabled: Boolean;
    FOnIdleExecute: TNotifyEvent;
    FOnWakeExecute: TNotifyEvent;
    procedure SetEnabled(const Value: Boolean);
    function MinimumIdleSeconds: Integer;
    procedure DoIdleExecute;
    procedure DoWakeExecute;
  public
    constructor Create(const Owner: TComponent);
    destructor Destroy; override;
    function IsApplicationIdle: Boolean;
    procedure NotIdle;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property IdleStartTime: TDateTime read FIdleStartTime;
    property LastActiveTime: TDateTime read FLastActiveTime write FLastActiveTime;
    property IdleDelaySeconds: Integer read FIdleDelaySeconds write FIdleDelaySeconds;
    property IdleDelaySecondsMinimized: Integer read FIdleDelaySecondsMinimized write FIdleDelaySecondsMinimized;
    property SecondsBetweenActivity: Integer read FSecondsBetweenActivity write FSecondsBetweenActivity;
    property SecondsSinceActive: Integer read FSecondsSinceActive;
    property SecondsIdle: Integer read FSecondsIdle;
    // events:
    property OnIdleExecute: TNotifyEvent read FOnIdleExecute write FOnIdleExecute;
    property OnWakeExecute: TNotifyEvent read FOnWakeExecute write FOnWakeExecute;
  end;

implementation

{ TOdIdleTimer }

procedure TOdIdleTimer.ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
begin
  // Careful, this is called A LOT!!! Keep it simple.
  // Everything in the message stack is processed, so we're idle.
  IdleTimer.Enabled := True;
end;

procedure TOdIdleTimer.ApplicationEventsMessage(var Msg: tagMSG; var Handled: Boolean);
begin
  // Careful, this is called A LOT!!! Keep it simple.
  // Keyboard, mouse click, or mouse wheel messages mean we're not idle.
  if IsApplicationIdle then
    case Msg.message of
      WM_KEYFIRST..WM_KEYLAST, WM_MOUSEFIRST..WM_MOUSELAST:
        if Msg.message <> WM_MOUSEMOVE then
          NotIdle;
    end;
end;

constructor TOdIdleTimer.Create(const Owner: TComponent);
begin
  IdleTimer := TTimer.Create(Owner);
  IdleTimer.Enabled := False;
  IdleTimer.Interval := 3000; // 3 seconds
  IdleTimer.OnTimer := IdleTimerTimer;
  ApplicationEvents := TApplicationEvents.Create(Owner);
  LastActiveTime := 0;
  FIdleStartTime := 0;
  IdleDelaySeconds := -1;
  IdleDelaySecondsMinimized := -1;
  SecondsBetweenActivity := 0;
  Enabled := False;
end;

destructor TOdIdleTimer.Destroy;
begin
  FreeAndNil(IdleTimer);
  FreeAndNil(ApplicationEvents);
  inherited;
end;

procedure TOdIdleTimer.IdleTimerTimer(Sender: TObject);
begin
  if IdleStartTime = 0 then
    FIdleStartTime := Now;

  FSecondsIdle := SecondsBetween(Now, IdleStartTime);
  if LastActiveTime > 0 then
    FSecondsSinceActive := SecondsBetween(Now, LastActiveTime)
  else
    FSecondsSinceActive := 0;

  if (SecondsIdle >= MinimumIdleSeconds) and ((LastActiveTime = 0) or
    (SecondsSinceActive >= SecondsBetweenActivity)) then begin
    ApplicationEvents.OnIdle := nil;
    try
      IdleTimer.Enabled := False;
      DoIdleExecute;
    finally
      ApplicationEvents.OnIdle := ApplicationEventsIdle;
    end;
  end;
end;

procedure TOdIdleTimer.DoIdleExecute;
begin
  if Assigned(FOnIdleExecute) then
    FOnIdleExecute(Self);

  // Unhandled exceptions in OnIdleExecute handler prevent LastActiveTime update.
  LastActiveTime := Now;
end;

procedure TOdIdleTimer.DoWakeExecute;
begin
  if Assigned(FOnWakeExecute) then
    FOnWakeExecute(Self);
end;

function TOdIdleTimer.MinimumIdleSeconds: Integer;
begin
  if Application.MainForm.WindowState = wsMinimized then
    Result := IdleDelaySecondsMinimized
  else
    Result := IdleDelaySeconds;
end;

procedure TOdIdleTimer.NotIdle;
begin
  if IdleTimer.Enabled then begin
    IdleTimer.Enabled := False;
    FIdleStartTime := 0;
    DoWakeExecute;
  end;
end;

procedure TOdIdleTimer.SetEnabled(const Value: Boolean);
begin
  if FEnabled <> Value then begin
    FIdleStartTime := 0;
    if Value then begin
      if IdleDelaySecondsMinimized < 1 then
        IdleDelaySecondsMinimized := IdleDelaySeconds;
      if (SecondsBetweenActivity < IdleDelaySeconds) then
        raise Exception.Create('SecondsBetweenActivity cannot be less than IdleDelaySeconds');
      if (SecondsBetweenActivity < IdleDelaySecondsMinimized) then
        raise Exception.Create('SecondsBetweenActivity cannot be less than IdleDelaySecondsMinimized');
      ApplicationEvents.OnIdle := ApplicationEventsIdle;
      ApplicationEvents.OnMessage := ApplicationEventsMessage;
    end else begin
      ApplicationEvents.OnIdle := nil;
      ApplicationEvents.OnMessage := nil;
    end;
    FEnabled := Value;
    IdleTimer.Enabled := FEnabled;
  end;
end;

function TOdIdleTimer.IsApplicationIdle: Boolean;
begin
  Result := IdleStartTime > 0;
end;

end.
