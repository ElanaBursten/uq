object ACustForm: TACustForm
  Left = 0
  Top = 0
  Caption = 'ACustForm'
  ClientHeight = 379
  ClientWidth = 720
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object MasterScrollBox: TScrollBox
    Left = 0
    Top = 26
    Width = 720
    Height = 317
    VertScrollBar.Style = ssFlat
    Align = alClient
    BevelEdges = []
    BevelInner = bvNone
    BevelOuter = bvNone
    TabOrder = 0
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 343
    Width = 720
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      720
      36)
    object btnCancel: TButton
      Left = 626
      Top = 6
      Width = 85
      Height = 25
      Anchors = [akTop, akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
      OnClick = btnSaveClick
    end
    object btnSave: TButton
      Left = 535
      Top = 6
      Width = 85
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 1
      OnClick = btnSaveClick
    end
    object btnCreateGroups: TButton
      Left = 13
      Top = 6
      Width = 117
      Height = 25
      Caption = 'Create Groups'
      TabOrder = 2
      OnClick = btnCreateGroupsClick
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 720
    Height = 26
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lblTitle: TLabel
      Left = 9
      Top = 4
      Width = 46
      Height = 18
      Caption = 'TITLE '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
end
