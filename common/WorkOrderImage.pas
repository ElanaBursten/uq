unit WorkOrderImage;

interface

uses SysUtils, MSXML2_Tlb;

function GetDisplayableWorkOrderImage(const RawImage, XsltFormat: string; var IsXml: Boolean): string;

implementation

uses OdMSXMLUtils, OdMiscUtils;

function IsXmlFormat(const Image: string): Boolean;
var
  Doc: IXMLDOMDocument;
begin
  // just check that Image is a valid xml doc
  Doc := CoDOMDocument.Create;
  Doc.async := False;
  Result := Doc.loadXML(Image);
end;

function GetXsltStyleDoc(const XsltFormat: string): IXMLDOMDocument;
var
  Xslt: string;
begin
  if IsEmpty(XsltFormat) then
    Xslt := Format(PrettyPrintingXslt, [''])
  else
    Xslt := XsltFormat;
  Result := ParseXml(Xslt);
end;

// use Xslt to format the Xml & return as text
function GetDisplayableWorkOrderImage(const RawImage, XsltFormat: string; var IsXml: Boolean): string;
var
  Doc: IXMLDOMDocument;
  StyleDoc: IXMLDOMDocument;
begin
  Result := RawImage;
  try
    IsXml := IsXmlFormat(RawImage);
    if IsXml then begin
      Doc := ParseXML(RawImage);
      StyleDoc := GetXsltStyleDoc(XsltFormat);
      Result := FormatXmlWithXslt(Doc, StyleDoc);
    end;
  except
    on E: Exception do
      Result := '*** Could not format XML work order; the unformatted work order is shown ***' +
        CRLF + E.Message + CRLF + RawImage;
  end;
end;

end.
