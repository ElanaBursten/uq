object MessageForm: TMessageForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Message'
  ClientHeight = 612
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MessageMemo: TMemo
    Left = 0
    Top = 0
    Width = 707
    Height = 571
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel: TPanel
    Left = 0
    Top = 571
    Width = 707
    Height = 41
    Align = alBottom
    TabOrder = 1
    object CloseButton: TButton
      Left = 627
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
      OnClick = CloseButtonClick
    end
    object SaveToFileButton: TButton
      Left = 546
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Save to File'
      TabOrder = 1
      OnClick = SaveToFileButtonClick
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'txt'
    Left = 592
    Top = 464
  end
end
