unit TicketImage;

interface

uses SysUtils, MSXML2_Tlb, classes;  //qm-253  sr

function GetDisplayableTicketImage(const RawImage, XsltFormat: string; var TicketIsXml: Boolean): string;
function GetGa811Image(const RawImage:string):string;  //qm-253  sr
implementation

uses OdMSXMLUtils, OdMiscUtils, strutils;

function IsXmlFormat(const RawImage: string): Boolean;
var
  Doc: IXMLDOMDocument;
begin
  // just check that Image is a valid xml doc
  Doc := CoDOMDocument.Create;
  Doc.async := False;
  Result := Doc.loadXML(RawImage);
end;

function GetXsltStyleDoc(const XsltFormat: string): IXMLDOMDocument;
var
  Xslt: string;
begin
  if IsEmpty(XsltFormat) then
    Xslt := PrettyPrintingXslt
  else
    Xslt := XsltFormat;
  Result := ParseXml(Xslt);
end;

function GetGa811Image(const RawImage:string):string;   //qm-253  sr
var
  text:string;
  tempString, tempReturn : TStringList;
  i:integer;
const
  spc='    ';
begin
try
  tempString := TStringList.create;
  tempReturn := TStringList.create;
  tempString.Text:=RawImage;

  for i := 0 to tempString.Count - 1 do
  begin
    text :=  tempString[i];
    if text ='' then continue;
    if LeftStr(text,1)='[' then
    begin
      if text<>'[TICKET]' then
      tempReturn.Add('');
      tempReturn.Add(text);
    end
    else
      tempReturn.Add(spc+text);
  end;

  result := tempReturn.Text;
finally
  tempString.Free;
  tempReturn.Free;
end;
end;

// use Xslt to format the Xml & return as text
function GetDisplayableTicketImage(const RawImage, XsltFormat: string; var TicketIsXml: Boolean): string;
var
  Doc: IXMLDOMDocument;
  StyleDoc: IXMLDOMDocument;
  sRawImage:string;//QMANTWO-691
begin
  Result := RawImage;
  try
    sRawImage := StringReplace(RawImage, '&', 'n',[rfReplaceAll]);   //QMANTWO-691
    TicketIsXml := IsXmlFormat(sRawImage);
    if TicketIsXml then begin
      Doc := ParseXML(sRawImage);
      StyleDoc := GetXsltStyleDoc(XsltFormat);
      Result := FormatXmlWithXslt(Doc, StyleDoc);
    end;
  except
    on E: Exception do
      Result := '*** Could not format XML ticket; the unformatted ticket is shown ***' +
        CRLF + E.Message + CRLF + RawImage;
  end;
end;

end.
