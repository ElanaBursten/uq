object DateRangeDialog: TDateRangeDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select a Date Range'
  ClientHeight = 302
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DateRangeLabel: TLabel
    Left = 95
    Top = 13
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Quick Select'
  end
  object Label2: TLabel
    Left = 265
    Top = 42
    Width = 225
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'End Date'
  end
  object Label3: TLabel
    Left = 12
    Top = 47
    Width = 225
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'Start Date'
  end
  object RangeLabel: TLabel
    Left = 12
    Top = 231
    Width = 473
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Caption = 'Selected Date Range: 10/10/2009 - 10/10/2009'
  end
  object StartCal: TMonthCalendar
    Left = 12
    Top = 65
    Width = 225
    Height = 160
    Date = 39988.559779212960000000
    FirstDayOfWeek = dowSunday
    TabOrder = 1
    OnClick = StartCalClick
    OnDblClick = StartCalDblClick
    OnGetMonthInfo = CalGetMonthInfo
  end
  object EndCal: TMonthCalendar
    Left = 261
    Top = 65
    Width = 225
    Height = 160
    Date = 39988.559779212960000000
    TabOrder = 2
    OnClick = EndCalClick
    OnDblClick = EndCalDblClick
    OnGetMonthInfo = CalGetMonthInfo
  end
  object DateRangeCombo: TComboBox
    Left = 169
    Top = 9
    Width = 233
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = DateRangeComboChange
  end
  object OKButton: TButton
    Left = 162
    Top = 265
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object CancelButton: TButton
    Left = 260
    Top = 265
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
