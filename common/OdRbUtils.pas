unit OdRbUtils;

interface

uses Classes, ppClass, ppCTMain, ppCtrls;

procedure ShowCrosstabColumn(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
procedure ShowCrosstabRow(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
procedure ShowCrosstabValue(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
procedure RemoveCrosstabValue(Crosstab: TppCrosstab; const FieldName: string);
function CreateLabel(BaseLabel: TppLabel): TppLabel;
procedure RemoveBold(ppLabel: TppLabel);
procedure AddBold(ppLabel: TppLabel);
procedure AdjustLabelPosition(aLabel: TppLabel; BaseLabel: TppLabel; Distance: Single); overload;
procedure AdjustLabelPosition(aLabel: TppLabel; Position: Single); overload;
procedure AlignRBComponentsByTag(Owner: TComponent);

implementation

uses
  SysUtils, Graphics;

procedure ShowCrosstabColumn(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
var
  i: Integer;
begin
  Assert(Assigned(Crosstab));
  for i := 0 to Crosstab.ColumnDefCount - 1 do begin
    if SameText(Crosstab.ColumnDefs[i].FieldName, FieldName) then
      Crosstab.ColumnDefs[i].Visible := Visible;
  end;
end;

procedure ShowCrosstabRow(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
var
  i: Integer;
begin
  Assert(Assigned(Crosstab));
  for i := 0 to Crosstab.RowDefCount - 1 do begin
    if SameText(Crosstab.RowDefs[i].FieldName, FieldName) then
      Crosstab.RowDefs[i].Visible := Visible;
  end;
end;

procedure ShowCrosstabValue(Crosstab: TppCrosstab; const FieldName: string; Visible: Boolean);
var
  i: Integer;
begin
  Assert(Assigned(Crosstab));
  for i := 0 to Crosstab.ValueDefCount - 1 do begin
    if SameText(Crosstab.ValueDefs[i].FieldName, FieldName) then
      Crosstab.ValueDefs[i].Visible := Visible;
  end;
end;

procedure RemoveCrosstabValue(Crosstab: TppCrosstab; const FieldName: string);
var
  i: Integer;
begin
  Assert(Assigned(Crosstab));
  for i := Crosstab.ValueDefCount - 1 downto 0 do begin
    if SameText(Crosstab.ValueDefs[i].FieldName, FieldName) then
      Crosstab.ValueDefs[i].Free;
  end;
end;

function CreateLabel(BaseLabel: TppLabel): TppLabel;
begin
  Result := TppLabel.Create(BaseLabel.Owner);
  Result.SetParentComponent(BaseLabel.Parent);
  Result.Top := BaseLabel.Top;
  Result.Left := BaseLabel.Left;
  Result.Band := BaseLabel.Band;
  Result.Transparent := BaseLabel.Transparent;
  Result.Font.Assign(BaseLabel.Font);
end;

procedure RemoveBold(ppLabel: TppLabel);
begin
  ppLabel.Font.Style := ppLabel.Font.Style - [fsBold];
end;

procedure AddBold(ppLabel: TppLabel);
begin
  ppLabel.Font.Style := ppLabel.Font.Style + [fsBold];
end;

procedure AdjustLabelPosition(aLabel: TppLabel; BaseLabel: TppLabel; Distance: Single);
begin
  aLabel.Left := BaseLabel.Left + BaseLabel.Width + Distance;
end;

procedure AdjustLabelPosition(aLabel: TppLabel; Position: Single);
begin
  aLabel.Left := Position;
end;

procedure AlignRBComponentsByTag(Owner: TComponent);
var
  I: Integer;
  C, Template: TppComponent;
  TemplateComps: array[1..99] of TppComponent;
begin
  for i := Low(TemplateComps) to High(TemplateComps) do
    TemplateComps[i] := nil;

  // gather templates
  for I := 0 to Owner.ComponentCount-1 do
    if Owner.Components[I] is TppComponent then begin
      C := Owner.Components[I] as TppComponent;
      if (C.Tag >= 1) and (C.Tag < 100) then
        TemplateComps[C.Tag] := C;
    end;

  // make others match
  for I := 0 to Owner.ComponentCount-1 do
    if Owner.Components[I] is TppComponent then begin
      C := Owner.Components[I] as TppComponent;
      if (C.Tag > 100) and (C.Tag < 200) then begin
        Template := TemplateComps[C.Tag - 100];
        Assert(Assigned(Template), C.Name + ' cannot be aligned because no template has a Tag=' +
          IntToStr(C.Tag-100) + '. Verify the report''s design.');
        C.Left := Template.Left;
        C.Width := Template.Width;
      end;
    end;
end;

end.
