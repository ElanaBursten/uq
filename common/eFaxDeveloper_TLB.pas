unit eFaxDeveloper_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 10/31/2017 3:35:59 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\windows\SysWow64\eFaxDeveloper.tlb (1)
// LIBID: {F1694401-50A1-40D4-84FB-CB71F6F703FF}
// LCID: 0
// Helpfile: 
// HelpString: eFax Developer API
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v2.4 mscorlib, (C:\windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.tlb)
// SYS_KIND: SYS_WIN32
// Errors:
//   Hint: Parameter 'array' of _Barcodes.CopyTo changed to 'array_'
//   Hint: Parameter 'array' of _Pages.CopyTo changed to 'array_'
//   Hint: Parameter 'array' of _UserFields.CopyTo changed to 'array_'
//   Hint: Parameter 'array' of _DispositionEmails.CopyTo changed to 'array_'
//   Hint: Parameter 'array' of _FaxFiles.CopyTo changed to 'array_'
//   Error creating palette bitmap of (TeFaxDeveloperException) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TAdditionalInfo) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TBarcodes) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TBarcode) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TCodeLocation) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TEndEdge) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TFaxControl) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TInboundClient) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TInboundPostRequest) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TPages) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TPage) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TPageCoordinates) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TStartEdge) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TUserFields) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TUserField) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TAccessControl) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TDispositionControl) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TDispositionEmail) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TDispositionEmails) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TFaxFiles) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TFaxFile) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundClient) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundDisposition) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundDispositionInternal) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundRequest) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundResponse) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TOutboundStatusResponse) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TRecipient) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TRequestTransmissionControl) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TStatusResponseRecipient) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TStatus) : Server mscoree.dll contains no icons
//   Error creating palette bitmap of (TXmlUtils) : Server mscoree.dll contains no icons
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, mscorlib_TLB, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  eFaxDeveloperMajorVersion = 2;
  eFaxDeveloperMinorVersion = 0;

  LIBID_eFaxDeveloper: TGUID = '{F1694401-50A1-40D4-84FB-CB71F6F703FF}';

  IID__eFaxDeveloperException: TGUID = '{0E90D19E-C74D-3F3D-B6C0-429461B37613}';
  IID__AdditionalInfo: TGUID = '{F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}';
  IID__Barcodes: TGUID = '{0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}';
  IID__Barcode: TGUID = '{26C22F4C-09C6-3E52-9DC9-F4D3AD297460}';
  IID__CodeLocation: TGUID = '{53C261F9-131F-3C83-816B-5BEA55C60D8D}';
  IID__EndEdge: TGUID = '{EB681918-BBE3-3186-8722-8F1680B092CE}';
  IID__FaxControl: TGUID = '{460B7977-990A-3396-AEC6-4BB77CA65888}';
  IID__InboundClient: TGUID = '{C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}';
  IID__InboundPostRequest: TGUID = '{85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}';
  IID__Pages: TGUID = '{478BFDC2-F758-37CE-B36D-3D860646B4F7}';
  IID__Page: TGUID = '{DF872539-0797-3D95-ACD6-71D129CD26B2}';
  IID__PageCoordinates: TGUID = '{62D36E29-A706-3A1B-8960-8CAA8E2600B7}';
  IID__StartEdge: TGUID = '{E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}';
  IID__UserFields: TGUID = '{C2C0489D-B095-3118-83B2-8AD371130E55}';
  IID__UserField: TGUID = '{FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}';
  IID__AccessControl: TGUID = '{DD20B019-DC7F-3F62-846B-07AE45730708}';
  IID__DispositionControl: TGUID = '{E9FB3360-AB9F-34CC-8353-F256A04D7500}';
  IID__DispositionEmail: TGUID = '{C31709AB-5C8B-3EF4-8C6E-562B34B39858}';
  IID__DispositionEmails: TGUID = '{4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}';
  IID__FaxFiles: TGUID = '{E5677D34-DBAE-3C6A-858E-ABE38B8658C5}';
  IID__FaxFile: TGUID = '{05100B08-5E11-3FA5-8C1B-D0E93D972DD3}';
  IID__OutboundClient: TGUID = '{451CAF73-04BD-3F45-BE38-5E92C77F5BB0}';
  IID__OutboundDisposition: TGUID = '{20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}';
  IID__OutboundDispositionInternal: TGUID = '{6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}';
  IID__OutboundRequest: TGUID = '{1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}';
  IID__OutboundResponse: TGUID = '{5765572A-9D6D-3F77-A0EA-8672CB6EF53E}';
  IID__OutboundStatusResponse: TGUID = '{F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}';
  IID__Recipient: TGUID = '{91AE3E60-EF43-3547-87D6-E6BC79245862}';
  IID__RequestTransmissionControl: TGUID = '{526EA2AA-D269-3E5C-96EF-C6C432FD41CB}';
  IID__StatusResponseRecipient: TGUID = '{70A838A9-9847-39B1-9FAC-10CADAC025D1}';
  IID__Status: TGUID = '{1ACD9FAB-E245-3C68-A298-CB721C4F9400}';
  IID__XmlUtils: TGUID = '{A903F01C-AC4C-3AA5-86F5-C70D386C7E50}';
  CLASS_eFaxDeveloperException: TGUID = '{CB799347-9BDB-31B4-94B8-97CA72DD00DA}';
  CLASS_AdditionalInfo: TGUID = '{AC496729-F924-38DA-A76F-BE9D8C46577F}';
  CLASS_Barcodes: TGUID = '{A8F4944D-75D2-33B1-881A-4746C6CA5DB8}';
  CLASS_Barcode: TGUID = '{C197E02D-D99D-32F7-AD8E-47B84674261C}';
  CLASS_CodeLocation: TGUID = '{C97CF81C-6584-3CBB-BBAD-402573E4597B}';
  CLASS_EndEdge: TGUID = '{7F4666B4-1837-3740-8073-FEBBBB29C998}';
  CLASS_FaxControl: TGUID = '{9FE71BBF-06DE-3A0E-B48A-E9E9F1959654}';
  CLASS_InboundClient: TGUID = '{8C5A7440-5459-339C-8BD3-DDACF93679E9}';
  CLASS_InboundPostRequest: TGUID = '{6814E5BF-A93C-316E-A6EA-E1051D2ED091}';
  CLASS_Pages: TGUID = '{E4F16925-A8CC-3F44-A9FE-9C84E7FF9E6E}';
  CLASS_Page: TGUID = '{399F4770-4F7B-3EA5-94D4-D2E1DF56BAC9}';
  CLASS_PageCoordinates: TGUID = '{AE9503EA-53DF-3781-87D0-6A7A6581F040}';
  CLASS_StartEdge: TGUID = '{BF1FBF7D-41F5-3390-AAFE-E3F454129BBA}';
  CLASS_UserFields: TGUID = '{10B1EA38-E94E-3565-A57E-08E8EF04C4AA}';
  CLASS_UserField: TGUID = '{9C7608DE-A1E5-3E2F-9B10-50D614DEA93C}';
  CLASS_AccessControl: TGUID = '{A98CBCEC-429D-3145-B20A-2CF705FF66D6}';
  CLASS_DispositionControl: TGUID = '{6C1FFD92-7D4C-32FF-B1F1-FA4EAE084EDF}';
  CLASS_DispositionEmail: TGUID = '{A6077296-FEC5-30B8-8E4C-E1576FA47AA0}';
  CLASS_DispositionEmails: TGUID = '{20A4BFC6-5488-3B22-8B40-20DD293BEADD}';
  CLASS_FaxFiles: TGUID = '{9CC81C16-D001-39A7-9A36-D88660B5E4FD}';
  CLASS_FaxFile: TGUID = '{F4EC8457-5481-3013-A806-5F8B509DFD58}';
  CLASS_OutboundClient: TGUID = '{04C63A38-C876-3F5B-BDBA-C31903E6BDF8}';
  CLASS_OutboundDisposition: TGUID = '{C2EA4DC0-5674-30A7-A141-712D3CB88DD6}';
  CLASS_OutboundDispositionInternal: TGUID = '{4874738B-7202-321A-BC72-CC79A89A6647}';
  CLASS_OutboundRequest: TGUID = '{A2FC4529-8B99-38B9-A01A-1001EB3ECB63}';
  CLASS_OutboundResponse: TGUID = '{18578943-BD82-33D4-AA9D-418E7D7079BF}';
  CLASS_OutboundStatusResponse: TGUID = '{5CD22B8F-A7AD-3BD5-AC1B-5651239DFD2E}';
  CLASS_Recipient: TGUID = '{826F4AF9-EC9F-37D1-81FB-59BEB909AA20}';
  CLASS_RequestTransmissionControl: TGUID = '{F25ED8FE-69EC-3B70-A1FA-76E190B7C056}';
  CLASS_StatusResponseRecipient: TGUID = '{876C2B0F-5DE3-3B9B-8BE0-60647B223F1D}';
  CLASS_Status: TGUID = '{74C7C8F3-B947-3A1E-B07C-1B97304DFB2E}';
  CLASS_XmlUtils: TGUID = '{39A72A5E-7607-3982-895D-F910E2E6A453}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum ReadDirection
type
  ReadDirection = TOleEnum;
const
  ReadDirection_TwoD = $00000000;
  ReadDirection_LeftToRight = $00000001;
  ReadDirection_TopToBottom = $00000002;
  ReadDirection_RightToLeft = $00000003;
  ReadDirection_BottomToTop = $00000004;

// Constants for enum RequestType
type
  RequestType = TOleEnum;
const
  RequestType_NewInbound = $00000000;
  RequestType_ManualRepost = $00000001;

// Constants for enum DispositionLevel
type
  DispositionLevel = TOleEnum;
const
  DispositionLevel_Error = $00000000;
  DispositionLevel_Success = $00000001;
  DispositionLevel_Both = $00000002;
  DispositionLevel_None = $00000003;

// Constants for enum DispositionMethod
type
  DispositionMethod = TOleEnum;
const
  DispositionMethod_Undefined = $FFFFFFFF;
  DispositionMethod_Post = $00000000;
  DispositionMethod_Email = $00000001;

// Constants for enum ErrorLevel
type
  ErrorLevel = TOleEnum;
const
  ErrorLevel_Undefined = $FFFFFFFF;
  ErrorLevel_User = $00000000;
  ErrorLevel_System = $00000001;

// Constants for enum FaxFileType
type
  FaxFileType = TOleEnum;
const
  FaxFileType_doc = $00000000;
  FaxFileType_docx = $00000001;
  FaxFileType_xls = $00000002;
  FaxFileType_xlsx = $00000003;
  FaxFileType_ppt = $00000004;
  FaxFileType_pptx = $00000005;
  FaxFileType_html = $00000006;
  FaxFileType_tif = $00000007;
  FaxFileType_jpg = $00000008;
  FaxFileType_txt = $00000009;
  FaxFileType_pdf = $0000000A;
  FaxFileType_rtf = $0000000B;
  FaxFileType_snp = $0000000C;
  FaxFileType_png = $0000000D;
  FaxFileType_gif = $0000000E;

// Constants for enum NoDuplicates
type
  NoDuplicates = TOleEnum;
const
  NoDuplicates_Undefined = $FFFFFFFF;
  NoDuplicates_Enable = $00000000;
  NoDuplicates_Disable = $00000001;

// Constants for enum Priority
type
  Priority = TOleEnum;
const
  Priority_Undefined = $FFFFFFFF;
  Priority_Normal = $00000000;
  Priority_High = $00000001;

// Constants for enum Resolution
type
  Resolution = TOleEnum;
const
  Resolution_Standard = $00000000;
  Resolution_Fine = $00000001;

// Constants for enum SelfBusy
type
  SelfBusy = TOleEnum;
const
  SelfBusy_Undefined = $FFFFFFFF;
  SelfBusy_Enable = $00000000;
  SelfBusy_Disable = $00000001;

// Constants for enum StatusCode
type
  StatusCode = TOleEnum;
const
  StatusCode_Success = $00000001;
  StatusCode_Failure = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _eFaxDeveloperException = interface;
  _eFaxDeveloperExceptionDisp = dispinterface;
  _AdditionalInfo = interface;
  _AdditionalInfoDisp = dispinterface;
  _Barcodes = interface;
  _BarcodesDisp = dispinterface;
  _Barcode = interface;
  _BarcodeDisp = dispinterface;
  _CodeLocation = interface;
  _CodeLocationDisp = dispinterface;
  _EndEdge = interface;
  _EndEdgeDisp = dispinterface;
  _FaxControl = interface;
  _FaxControlDisp = dispinterface;
  _InboundClient = interface;
  _InboundClientDisp = dispinterface;
  _InboundPostRequest = interface;
  _InboundPostRequestDisp = dispinterface;
  _Pages = interface;
  _PagesDisp = dispinterface;
  _Page = interface;
  _PageDisp = dispinterface;
  _PageCoordinates = interface;
  _PageCoordinatesDisp = dispinterface;
  _StartEdge = interface;
  _StartEdgeDisp = dispinterface;
  _UserFields = interface;
  _UserFieldsDisp = dispinterface;
  _UserField = interface;
  _UserFieldDisp = dispinterface;
  _AccessControl = interface;
  _AccessControlDisp = dispinterface;
  _DispositionControl = interface;
  _DispositionControlDisp = dispinterface;
  _DispositionEmail = interface;
  _DispositionEmailDisp = dispinterface;
  _DispositionEmails = interface;
  _DispositionEmailsDisp = dispinterface;
  _FaxFiles = interface;
  _FaxFilesDisp = dispinterface;
  _FaxFile = interface;
  _FaxFileDisp = dispinterface;
  _OutboundClient = interface;
  _OutboundClientDisp = dispinterface;
  _OutboundDisposition = interface;
  _OutboundDispositionDisp = dispinterface;
  _OutboundDispositionInternal = interface;
  _OutboundDispositionInternalDisp = dispinterface;
  _OutboundRequest = interface;
  _OutboundRequestDisp = dispinterface;
  _OutboundResponse = interface;
  _OutboundResponseDisp = dispinterface;
  _OutboundStatusResponse = interface;
  _OutboundStatusResponseDisp = dispinterface;
  _Recipient = interface;
  _RecipientDisp = dispinterface;
  _RequestTransmissionControl = interface;
  _RequestTransmissionControlDisp = dispinterface;
  _StatusResponseRecipient = interface;
  _StatusResponseRecipientDisp = dispinterface;
  _Status = interface;
  _StatusDisp = dispinterface;
  _XmlUtils = interface;
  _XmlUtilsDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  eFaxDeveloperException = _eFaxDeveloperException;
  AdditionalInfo = _AdditionalInfo;
  Barcodes = _Barcodes;
  Barcode = _Barcode;
  CodeLocation = _CodeLocation;
  EndEdge = _EndEdge;
  FaxControl = _FaxControl;
  InboundClient = _InboundClient;
  InboundPostRequest = _InboundPostRequest;
  Pages = _Pages;
  Page = _Page;
  PageCoordinates = _PageCoordinates;
  StartEdge = _StartEdge;
  UserFields = _UserFields;
  UserField = _UserField;
  AccessControl = _AccessControl;
  DispositionControl = _DispositionControl;
  DispositionEmail = _DispositionEmail;
  DispositionEmails = _DispositionEmails;
  FaxFiles = _FaxFiles;
  FaxFile = _FaxFile;
  OutboundClient = _OutboundClient;
  OutboundDisposition = _OutboundDisposition;
  OutboundDispositionInternal = _OutboundDispositionInternal;
  OutboundRequest = _OutboundRequest;
  OutboundResponse = _OutboundResponse;
  OutboundStatusResponse = _OutboundStatusResponse;
  Recipient = _Recipient;
  RequestTransmissionControl = _RequestTransmissionControl;
  StatusResponseRecipient = _StatusResponseRecipient;
  Status = _Status;
  XmlUtils = _XmlUtils;


// *********************************************************************//
// Interface: _eFaxDeveloperException
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {0E90D19E-C74D-3F3D-B6C0-429461B37613}
// *********************************************************************//
  _eFaxDeveloperException = interface(IDispatch)
    ['{0E90D19E-C74D-3F3D-B6C0-429461B37613}']
  end;

// *********************************************************************//
// DispIntf:  _eFaxDeveloperExceptionDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {0E90D19E-C74D-3F3D-B6C0-429461B37613}
// *********************************************************************//
  _eFaxDeveloperExceptionDisp = dispinterface
    ['{0E90D19E-C74D-3F3D-B6C0-429461B37613}']
  end;

// *********************************************************************//
// Interface: _AdditionalInfo
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}
// *********************************************************************//
  _AdditionalInfo = interface(IDispatch)
    ['{F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_ReadSequence: Integer; safecall;
    function Get_ReadDirection: ReadDirection; safecall;
    function Get_Symbology: WideString; safecall;
    function Get_CodeLocation: _CodeLocation; safecall;
    property ToString: WideString read Get_ToString;
    property ReadSequence: Integer read Get_ReadSequence;
    property ReadDirection: ReadDirection read Get_ReadDirection;
    property Symbology: WideString read Get_Symbology;
    property CodeLocation: _CodeLocation read Get_CodeLocation;
  end;

// *********************************************************************//
// DispIntf:  _AdditionalInfoDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}
// *********************************************************************//
  _AdditionalInfoDisp = dispinterface
    ['{F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property ReadSequence: Integer readonly dispid 1610743812;
    property ReadDirection: ReadDirection readonly dispid 1610743813;
    property Symbology: WideString readonly dispid 1610743814;
    property CodeLocation: _CodeLocation readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _Barcodes
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}
// *********************************************************************//
  _Barcodes = interface(IDispatch)
    ['{0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function IndexOf(const item: _Barcode): Integer; safecall;
    procedure Insert(index: Integer; const item: _Barcode); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_item(index: Integer): _Barcode; safecall;
    procedure _Set_item(index: Integer; const pRetVal: _Barcode); safecall;
    procedure Add(const item: _Barcode); safecall;
    procedure Clear; safecall;
    function Contains(const item: _Barcode): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get_IsReadOnly: WordBool; safecall;
    function Remove(const item: _Barcode): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _Barcode read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  end;

// *********************************************************************//
// DispIntf:  _BarcodesDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}
// *********************************************************************//
  _BarcodesDisp = dispinterface
    ['{0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}']
    property ToString: WideString readonly dispid 1610743808;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function IndexOf(const item: _Barcode): Integer; dispid 1610743812;
    procedure Insert(index: Integer; const item: _Barcode); dispid 1610743813;
    procedure RemoveAt(index: Integer); dispid 1610743814;
    property item[index: Integer]: _Barcode dispid 0; default;
    procedure Add(const item: _Barcode); dispid 1610743817;
    procedure Clear; dispid 1610743818;
    function Contains(const item: _Barcode): WordBool; dispid 1610743819;
    procedure CopyTo(array_: {NOT_OLEAUTO(PSafeArray)}OleVariant; arrayIndex: Integer); dispid 1610743820;
    property Count: Integer readonly dispid 1610743821;
    property IsReadOnly: WordBool readonly dispid 1610743822;
    function Remove(const item: _Barcode): WordBool; dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _Barcode
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {26C22F4C-09C6-3E52-9DC9-F4D3AD297460}
// *********************************************************************//
  _Barcode = interface(IDispatch)
    ['{26C22F4C-09C6-3E52-9DC9-F4D3AD297460}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_Key: WideString; safecall;
    function Get_AdditionalInfo: _AdditionalInfo; safecall;
    property ToString: WideString read Get_ToString;
    property Key: WideString read Get_Key;
    property AdditionalInfo: _AdditionalInfo read Get_AdditionalInfo;
  end;

// *********************************************************************//
// DispIntf:  _BarcodeDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {26C22F4C-09C6-3E52-9DC9-F4D3AD297460}
// *********************************************************************//
  _BarcodeDisp = dispinterface
    ['{26C22F4C-09C6-3E52-9DC9-F4D3AD297460}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property Key: WideString readonly dispid 1610743812;
    property AdditionalInfo: _AdditionalInfo readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _CodeLocation
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {53C261F9-131F-3C83-816B-5BEA55C60D8D}
// *********************************************************************//
  _CodeLocation = interface(IDispatch)
    ['{53C261F9-131F-3C83-816B-5BEA55C60D8D}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_PageNumber: Integer; safecall;
    function Get_PageCoordinates: _PageCoordinates; safecall;
    property ToString: WideString read Get_ToString;
    property PageNumber: Integer read Get_PageNumber;
    property PageCoordinates: _PageCoordinates read Get_PageCoordinates;
  end;

// *********************************************************************//
// DispIntf:  _CodeLocationDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {53C261F9-131F-3C83-816B-5BEA55C60D8D}
// *********************************************************************//
  _CodeLocationDisp = dispinterface
    ['{53C261F9-131F-3C83-816B-5BEA55C60D8D}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property PageNumber: Integer readonly dispid 1610743812;
    property PageCoordinates: _PageCoordinates readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _EndEdge
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {EB681918-BBE3-3186-8722-8F1680B092CE}
// *********************************************************************//
  _EndEdge = interface(IDispatch)
    ['{EB681918-BBE3-3186-8722-8F1680B092CE}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_XEndPointA: Single; safecall;
    function Get_YEndPointA: Single; safecall;
    function Get_XEndPointB: Single; safecall;
    function Get_YEndPointB: Single; safecall;
    property ToString: WideString read Get_ToString;
    property XEndPointA: Single read Get_XEndPointA;
    property YEndPointA: Single read Get_YEndPointA;
    property XEndPointB: Single read Get_XEndPointB;
    property YEndPointB: Single read Get_YEndPointB;
  end;

// *********************************************************************//
// DispIntf:  _EndEdgeDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {EB681918-BBE3-3186-8722-8F1680B092CE}
// *********************************************************************//
  _EndEdgeDisp = dispinterface
    ['{EB681918-BBE3-3186-8722-8F1680B092CE}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property XEndPointA: Single readonly dispid 1610743812;
    property YEndPointA: Single readonly dispid 1610743813;
    property XEndPointB: Single readonly dispid 1610743814;
    property YEndPointB: Single readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _FaxControl
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {460B7977-990A-3396-AEC6-4BB77CA65888}
// *********************************************************************//
  _FaxControl = interface(IDispatch)
    ['{460B7977-990A-3396-AEC6-4BB77CA65888}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_AccountID: WideString; safecall;
    function Get_NumberDialed: WideString; safecall;
    function Get_DateReceived: TDateTime; safecall;
    function Get_FaxName: WideString; safecall;
    function Get_FileType: FaxFileType; safecall;
    function Get_PageCount: Integer; safecall;
    function Get_CSID: WideString; safecall;
    function Get_ANI: WideString; safecall;
    function Get_Status: Integer; safecall;
    function Get_MCFID: Integer; safecall;
    function Get_UserFields: _UserFields; safecall;
    function Get_Barcodes: _Barcodes; safecall;
    function GetFileContentsCOM: OleVariant; safecall;
    function Get_Pages: _Pages; safecall;
    property ToString: WideString read Get_ToString;
    property AccountID: WideString read Get_AccountID;
    property NumberDialed: WideString read Get_NumberDialed;
    property DateReceived: TDateTime read Get_DateReceived;
    property FaxName: WideString read Get_FaxName;
    property FileType: FaxFileType read Get_FileType;
    property PageCount: Integer read Get_PageCount;
    property CSID: WideString read Get_CSID;
    property ANI: WideString read Get_ANI;
    property Status: Integer read Get_Status;
    property MCFID: Integer read Get_MCFID;
    property UserFields: _UserFields read Get_UserFields;
    property Barcodes: _Barcodes read Get_Barcodes;
    property Pages: _Pages read Get_Pages;
  end;

// *********************************************************************//
// DispIntf:  _FaxControlDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {460B7977-990A-3396-AEC6-4BB77CA65888}
// *********************************************************************//
  _FaxControlDisp = dispinterface
    ['{460B7977-990A-3396-AEC6-4BB77CA65888}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property AccountID: WideString readonly dispid 1610743812;
    property NumberDialed: WideString readonly dispid 1610743813;
    property DateReceived: TDateTime readonly dispid 1610743814;
    property FaxName: WideString readonly dispid 1610743815;
    property FileType: FaxFileType readonly dispid 1610743816;
    property PageCount: Integer readonly dispid 1610743817;
    property CSID: WideString readonly dispid 1610743818;
    property ANI: WideString readonly dispid 1610743819;
    property Status: Integer readonly dispid 1610743820;
    property MCFID: Integer readonly dispid 1610743821;
    property UserFields: _UserFields readonly dispid 1610743822;
    property Barcodes: _Barcodes readonly dispid 1610743823;
    function GetFileContentsCOM: OleVariant; dispid 1610743824;
    property Pages: _Pages readonly dispid 1610743825;
  end;

// *********************************************************************//
// Interface: _InboundClient
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}
// *********************************************************************//
  _InboundClient = interface(IDispatch)
    ['{C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function DeserializeInboundPostRequest(const xml: WideString): _InboundPostRequest; safecall;
    function DeserializeInboundPostRequest_2(const stream: _Stream): _InboundPostRequest; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _InboundClientDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}
// *********************************************************************//
  _InboundClientDisp = dispinterface
    ['{C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function DeserializeInboundPostRequest(const xml: WideString): _InboundPostRequest; dispid 1610743812;
    function DeserializeInboundPostRequest_2(const stream: _Stream): _InboundPostRequest; dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _InboundPostRequest
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}
// *********************************************************************//
  _InboundPostRequest = interface(IDispatch)
    ['{85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_FaxControl: _FaxControl; safecall;
    function Get_UserName: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_RequestDate: TDateTime; safecall;
    function Get_RequestType: RequestType; safecall;
    property ToString: WideString read Get_ToString;
    property FaxControl: _FaxControl read Get_FaxControl;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
    property RequestDate: TDateTime read Get_RequestDate;
    property RequestType: RequestType read Get_RequestType;
  end;

// *********************************************************************//
// DispIntf:  _InboundPostRequestDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}
// *********************************************************************//
  _InboundPostRequestDisp = dispinterface
    ['{85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property FaxControl: _FaxControl readonly dispid 1610743812;
    property UserName: WideString readonly dispid 1610743813;
    property Password: WideString readonly dispid 1610743814;
    property RequestDate: TDateTime readonly dispid 1610743815;
    property RequestType: RequestType readonly dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _Pages
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {478BFDC2-F758-37CE-B36D-3D860646B4F7}
// *********************************************************************//
  _Pages = interface(IDispatch)
    ['{478BFDC2-F758-37CE-B36D-3D860646B4F7}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function IndexOf(const item: _Page): Integer; safecall;
    procedure Insert(index: Integer; const item: _Page); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_item(index: Integer): _Page; safecall;
    procedure _Set_item(index: Integer; const pRetVal: _Page); safecall;
    procedure Add(const item: _Page); safecall;
    procedure Clear; safecall;
    function Contains(const item: _Page): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get_IsReadOnly: WordBool; safecall;
    function Remove(const item: _Page): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _Page read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  end;

// *********************************************************************//
// DispIntf:  _PagesDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {478BFDC2-F758-37CE-B36D-3D860646B4F7}
// *********************************************************************//
  _PagesDisp = dispinterface
    ['{478BFDC2-F758-37CE-B36D-3D860646B4F7}']
    property ToString: WideString readonly dispid 1610743808;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function IndexOf(const item: _Page): Integer; dispid 1610743812;
    procedure Insert(index: Integer; const item: _Page); dispid 1610743813;
    procedure RemoveAt(index: Integer); dispid 1610743814;
    property item[index: Integer]: _Page dispid 0; default;
    procedure Add(const item: _Page); dispid 1610743817;
    procedure Clear; dispid 1610743818;
    function Contains(const item: _Page): WordBool; dispid 1610743819;
    procedure CopyTo(array_: {NOT_OLEAUTO(PSafeArray)}OleVariant; arrayIndex: Integer); dispid 1610743820;
    property Count: Integer readonly dispid 1610743821;
    property IsReadOnly: WordBool readonly dispid 1610743822;
    function Remove(const item: _Page): WordBool; dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _Page
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DF872539-0797-3D95-ACD6-71D129CD26B2}
// *********************************************************************//
  _Page = interface(IDispatch)
    ['{DF872539-0797-3D95-ACD6-71D129CD26B2}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_PageNumber: Integer; safecall;
    function GetPageContentsCOM: OleVariant; safecall;
    property ToString: WideString read Get_ToString;
    property PageNumber: Integer read Get_PageNumber;
  end;

// *********************************************************************//
// DispIntf:  _PageDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DF872539-0797-3D95-ACD6-71D129CD26B2}
// *********************************************************************//
  _PageDisp = dispinterface
    ['{DF872539-0797-3D95-ACD6-71D129CD26B2}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property PageNumber: Integer readonly dispid 1610743812;
    function GetPageContentsCOM: OleVariant; dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _PageCoordinates
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {62D36E29-A706-3A1B-8960-8CAA8E2600B7}
// *********************************************************************//
  _PageCoordinates = interface(IDispatch)
    ['{62D36E29-A706-3A1B-8960-8CAA8E2600B7}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_StartEdge: _StartEdge; safecall;
    function Get_EndEdge: _EndEdge; safecall;
    property ToString: WideString read Get_ToString;
    property StartEdge: _StartEdge read Get_StartEdge;
    property EndEdge: _EndEdge read Get_EndEdge;
  end;

// *********************************************************************//
// DispIntf:  _PageCoordinatesDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {62D36E29-A706-3A1B-8960-8CAA8E2600B7}
// *********************************************************************//
  _PageCoordinatesDisp = dispinterface
    ['{62D36E29-A706-3A1B-8960-8CAA8E2600B7}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property StartEdge: _StartEdge readonly dispid 1610743812;
    property EndEdge: _EndEdge readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _StartEdge
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}
// *********************************************************************//
  _StartEdge = interface(IDispatch)
    ['{E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_XStartPointA: Single; safecall;
    function Get_YStartPointA: Single; safecall;
    function Get_XStartPointB: Single; safecall;
    function Get_YStartPointB: Single; safecall;
    property ToString: WideString read Get_ToString;
    property XStartPointA: Single read Get_XStartPointA;
    property YStartPointA: Single read Get_YStartPointA;
    property XStartPointB: Single read Get_XStartPointB;
    property YStartPointB: Single read Get_YStartPointB;
  end;

// *********************************************************************//
// DispIntf:  _StartEdgeDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}
// *********************************************************************//
  _StartEdgeDisp = dispinterface
    ['{E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property XStartPointA: Single readonly dispid 1610743812;
    property YStartPointA: Single readonly dispid 1610743813;
    property XStartPointB: Single readonly dispid 1610743814;
    property YStartPointB: Single readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _UserFields
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C2C0489D-B095-3118-83B2-8AD371130E55}
// *********************************************************************//
  _UserFields = interface(IDispatch)
    ['{C2C0489D-B095-3118-83B2-8AD371130E55}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function IndexOf(const item: _UserField): Integer; safecall;
    procedure Insert(index: Integer; const item: _UserField); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_item(index: Integer): _UserField; safecall;
    procedure _Set_item(index: Integer; const pRetVal: _UserField); safecall;
    procedure Add(const item: _UserField); safecall;
    procedure Clear; safecall;
    function Contains(const item: _UserField): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get_IsReadOnly: WordBool; safecall;
    function Remove(const item: _UserField): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _UserField read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  end;

// *********************************************************************//
// DispIntf:  _UserFieldsDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C2C0489D-B095-3118-83B2-8AD371130E55}
// *********************************************************************//
  _UserFieldsDisp = dispinterface
    ['{C2C0489D-B095-3118-83B2-8AD371130E55}']
    property ToString: WideString readonly dispid 1610743808;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function IndexOf(const item: _UserField): Integer; dispid 1610743812;
    procedure Insert(index: Integer; const item: _UserField); dispid 1610743813;
    procedure RemoveAt(index: Integer); dispid 1610743814;
    property item[index: Integer]: _UserField dispid 0; default;
    procedure Add(const item: _UserField); dispid 1610743817;
    procedure Clear; dispid 1610743818;
    function Contains(const item: _UserField): WordBool; dispid 1610743819;
    procedure CopyTo(array_: {NOT_OLEAUTO(PSafeArray)}OleVariant; arrayIndex: Integer); dispid 1610743820;
    property Count: Integer readonly dispid 1610743821;
    property IsReadOnly: WordBool readonly dispid 1610743822;
    function Remove(const item: _UserField): WordBool; dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _UserField
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}
// *********************************************************************//
  _UserField = interface(IDispatch)
    ['{FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_FieldName: WideString; safecall;
    procedure Set_FieldName(const pRetVal: WideString); safecall;
    function Get_FieldValue: WideString; safecall;
    procedure Set_FieldValue(const pRetVal: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property FieldName: WideString read Get_FieldName write Set_FieldName;
    property FieldValue: WideString read Get_FieldValue write Set_FieldValue;
  end;

// *********************************************************************//
// DispIntf:  _UserFieldDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}
// *********************************************************************//
  _UserFieldDisp = dispinterface
    ['{FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property FieldName: WideString dispid 1610743812;
    property FieldValue: WideString dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _AccessControl
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DD20B019-DC7F-3F62-846B-07AE45730708}
// *********************************************************************//
  _AccessControl = interface(IDispatch)
    ['{DD20B019-DC7F-3F62-846B-07AE45730708}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_UserName: WideString; safecall;
    function Get_Password: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
  end;

// *********************************************************************//
// DispIntf:  _AccessControlDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DD20B019-DC7F-3F62-846B-07AE45730708}
// *********************************************************************//
  _AccessControlDisp = dispinterface
    ['{DD20B019-DC7F-3F62-846B-07AE45730708}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property UserName: WideString readonly dispid 1610743812;
    property Password: WideString readonly dispid 1610743813;
  end;

// *********************************************************************//
// Interface: _DispositionControl
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E9FB3360-AB9F-34CC-8353-F256A04D7500}
// *********************************************************************//
  _DispositionControl = interface(IDispatch)
    ['{E9FB3360-AB9F-34CC-8353-F256A04D7500}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_DispositionURL: WideString; safecall;
    procedure Set_DispositionURL(const pRetVal: WideString); safecall;
    function Get_DispositionLevel: DispositionLevel; safecall;
    procedure Set_DispositionLevel(pRetVal: DispositionLevel); safecall;
    function Get_DispositionMethod: DispositionMethod; safecall;
    procedure Set_DispositionMethod(pRetVal: DispositionMethod); safecall;
    function Get_DispositionEmails: _DispositionEmails; safecall;
    property ToString: WideString read Get_ToString;
    property DispositionURL: WideString read Get_DispositionURL write Set_DispositionURL;
    property DispositionLevel: DispositionLevel read Get_DispositionLevel write Set_DispositionLevel;
    property DispositionMethod: DispositionMethod read Get_DispositionMethod write Set_DispositionMethod;
    property DispositionEmails: _DispositionEmails read Get_DispositionEmails;
  end;

// *********************************************************************//
// DispIntf:  _DispositionControlDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E9FB3360-AB9F-34CC-8353-F256A04D7500}
// *********************************************************************//
  _DispositionControlDisp = dispinterface
    ['{E9FB3360-AB9F-34CC-8353-F256A04D7500}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property DispositionURL: WideString dispid 1610743812;
    property DispositionLevel: DispositionLevel dispid 1610743814;
    property DispositionMethod: DispositionMethod dispid 1610743816;
    property DispositionEmails: _DispositionEmails readonly dispid 1610743818;
  end;

// *********************************************************************//
// Interface: _DispositionEmail
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C31709AB-5C8B-3EF4-8C6E-562B34B39858}
// *********************************************************************//
  _DispositionEmail = interface(IDispatch)
    ['{C31709AB-5C8B-3EF4-8C6E-562B34B39858}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_DispositionRecipient: WideString; safecall;
    procedure Set_DispositionRecipient(const pRetVal: WideString); safecall;
    function Get_DispositionAddress: WideString; safecall;
    procedure Set_DispositionAddress(const pRetVal: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property DispositionRecipient: WideString read Get_DispositionRecipient write Set_DispositionRecipient;
    property DispositionAddress: WideString read Get_DispositionAddress write Set_DispositionAddress;
  end;

// *********************************************************************//
// DispIntf:  _DispositionEmailDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C31709AB-5C8B-3EF4-8C6E-562B34B39858}
// *********************************************************************//
  _DispositionEmailDisp = dispinterface
    ['{C31709AB-5C8B-3EF4-8C6E-562B34B39858}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property DispositionRecipient: WideString dispid 1610743812;
    property DispositionAddress: WideString dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _DispositionEmails
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}
// *********************************************************************//
  _DispositionEmails = interface(IDispatch)
    ['{4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function IndexOf(const item: _DispositionEmail): Integer; safecall;
    procedure Insert(index: Integer; const item: _DispositionEmail); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_item(index: Integer): _DispositionEmail; safecall;
    procedure _Set_item(index: Integer; const pRetVal: _DispositionEmail); safecall;
    procedure Add(const item: _DispositionEmail); safecall;
    procedure Clear; safecall;
    function Contains(const item: _DispositionEmail): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get_IsReadOnly: WordBool; safecall;
    function Remove(const item: _DispositionEmail): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _DispositionEmail read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  end;

// *********************************************************************//
// DispIntf:  _DispositionEmailsDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}
// *********************************************************************//
  _DispositionEmailsDisp = dispinterface
    ['{4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}']
    property ToString: WideString readonly dispid 1610743808;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function IndexOf(const item: _DispositionEmail): Integer; dispid 1610743812;
    procedure Insert(index: Integer; const item: _DispositionEmail); dispid 1610743813;
    procedure RemoveAt(index: Integer); dispid 1610743814;
    property item[index: Integer]: _DispositionEmail dispid 0; default;
    procedure Add(const item: _DispositionEmail); dispid 1610743817;
    procedure Clear; dispid 1610743818;
    function Contains(const item: _DispositionEmail): WordBool; dispid 1610743819;
    procedure CopyTo(array_: {NOT_OLEAUTO(PSafeArray)}OleVariant; arrayIndex: Integer); dispid 1610743820;
    property Count: Integer readonly dispid 1610743821;
    property IsReadOnly: WordBool readonly dispid 1610743822;
    function Remove(const item: _DispositionEmail): WordBool; dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _FaxFiles
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E5677D34-DBAE-3C6A-858E-ABE38B8658C5}
// *********************************************************************//
  _FaxFiles = interface(IDispatch)
    ['{E5677D34-DBAE-3C6A-858E-ABE38B8658C5}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function IndexOf(const item: _FaxFile): Integer; safecall;
    procedure Insert(index: Integer; const item: _FaxFile); safecall;
    procedure RemoveAt(index: Integer); safecall;
    function Get_item(index: Integer): _FaxFile; safecall;
    procedure _Set_item(index: Integer; const pRetVal: _FaxFile); safecall;
    procedure Add(const item: _FaxFile); safecall;
    procedure Clear; safecall;
    function Contains(const item: _FaxFile): WordBool; safecall;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get_IsReadOnly: WordBool; safecall;
    function Remove(const item: _FaxFile): WordBool; safecall;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _FaxFile read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  end;

// *********************************************************************//
// DispIntf:  _FaxFilesDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {E5677D34-DBAE-3C6A-858E-ABE38B8658C5}
// *********************************************************************//
  _FaxFilesDisp = dispinterface
    ['{E5677D34-DBAE-3C6A-858E-ABE38B8658C5}']
    property ToString: WideString readonly dispid 1610743808;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function IndexOf(const item: _FaxFile): Integer; dispid 1610743812;
    procedure Insert(index: Integer; const item: _FaxFile); dispid 1610743813;
    procedure RemoveAt(index: Integer); dispid 1610743814;
    property item[index: Integer]: _FaxFile dispid 0; default;
    procedure Add(const item: _FaxFile); dispid 1610743817;
    procedure Clear; dispid 1610743818;
    function Contains(const item: _FaxFile): WordBool; dispid 1610743819;
    procedure CopyTo(array_: {NOT_OLEAUTO(PSafeArray)}OleVariant; arrayIndex: Integer); dispid 1610743820;
    property Count: Integer readonly dispid 1610743821;
    property IsReadOnly: WordBool readonly dispid 1610743822;
    function Remove(const item: _FaxFile): WordBool; dispid 1610743823;
  end;

// *********************************************************************//
// Interface: _FaxFile
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {05100B08-5E11-3FA5-8C1B-D0E93D972DD3}
// *********************************************************************//
  _FaxFile = interface(IDispatch)
    ['{05100B08-5E11-3FA5-8C1B-D0E93D972DD3}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function GetFileContentsCOM: OleVariant; safecall;
    procedure SetFileContentsCOM(data: OleVariant); safecall;
    function Get_FileType: FaxFileType; safecall;
    procedure Set_FileType(pRetVal: FaxFileType); safecall;
    property ToString: WideString read Get_ToString;
    property FileType: FaxFileType read Get_FileType write Set_FileType;
  end;

// *********************************************************************//
// DispIntf:  _FaxFileDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {05100B08-5E11-3FA5-8C1B-D0E93D972DD3}
// *********************************************************************//
  _FaxFileDisp = dispinterface
    ['{05100B08-5E11-3FA5-8C1B-D0E93D972DD3}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function GetFileContentsCOM: OleVariant; dispid 1610743812;
    procedure SetFileContentsCOM(data: OleVariant); dispid 1610743813;
    property FileType: FaxFileType dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _OutboundClient
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {451CAF73-04BD-3F45-BE38-5E92C77F5BB0}
// *********************************************************************//
  _OutboundClient = interface(IDispatch)
    ['{451CAF73-04BD-3F45-BE38-5E92C77F5BB0}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_UserName: WideString; safecall;
    procedure Set_UserName(const pRetVal: WideString); safecall;
    function Get_Password: WideString; safecall;
    procedure Set_Password(const pRetVal: WideString); safecall;
    function Get_AccountID: WideString; safecall;
    procedure Set_AccountID(const pRetVal: WideString); safecall;
    function Get_ServiceUrl: WideString; safecall;
    procedure Set_ServiceUrl(const pRetVal: WideString); safecall;
    procedure LoadWebConfiguration; safecall;
    procedure SaveWebConfiguration(protect: WordBool); safecall;
    procedure LoadConfiguration; safecall;
    procedure SaveConfiguration(protect: WordBool); safecall;
    function DeserializeOutboundDisposition(const stream: _Stream): _OutboundDisposition; safecall;
    function DeserializeOutboundDisposition_2(const xml: WideString): _OutboundDisposition; safecall;
    function SendOutboundRequest(const OutboundRequest: _OutboundRequest): _OutboundResponse; safecall;
    function SendOutboundStatusRequest(const transmissionId: WideString; const docId: WideString): _OutboundStatusResponse; safecall;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName write Set_UserName;
    property Password: WideString read Get_Password write Set_Password;
    property AccountID: WideString read Get_AccountID write Set_AccountID;
    property ServiceUrl: WideString read Get_ServiceUrl write Set_ServiceUrl;
  end;

// *********************************************************************//
// DispIntf:  _OutboundClientDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {451CAF73-04BD-3F45-BE38-5E92C77F5BB0}
// *********************************************************************//
  _OutboundClientDisp = dispinterface
    ['{451CAF73-04BD-3F45-BE38-5E92C77F5BB0}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property UserName: WideString dispid 1610743812;
    property Password: WideString dispid 1610743814;
    property AccountID: WideString dispid 1610743816;
    property ServiceUrl: WideString dispid 1610743818;
    procedure LoadWebConfiguration; dispid 1610743820;
    procedure SaveWebConfiguration(protect: WordBool); dispid 1610743821;
    procedure LoadConfiguration; dispid 1610743822;
    procedure SaveConfiguration(protect: WordBool); dispid 1610743823;
    function DeserializeOutboundDisposition(const stream: _Stream): _OutboundDisposition; dispid 1610743824;
    function DeserializeOutboundDisposition_2(const xml: WideString): _OutboundDisposition; dispid 1610743825;
    function SendOutboundRequest(const OutboundRequest: _OutboundRequest): _OutboundResponse; dispid 1610743826;
    function SendOutboundStatusRequest(const transmissionId: WideString; const docId: WideString): _OutboundStatusResponse; dispid 1610743827;
  end;

// *********************************************************************//
// Interface: _OutboundDisposition
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}
// *********************************************************************//
  _OutboundDisposition = interface(IDispatch)
    ['{20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_UserName: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_transmissionId: WideString; safecall;
    function Get_docId: WideString; safecall;
    function Get_FaxNumber: WideString; safecall;
    function Get_RecipientCSID: WideString; safecall;
    function Get_CompletionDate: TDateTime; safecall;
    function Get_FaxStatus: Integer; safecall;
    function Get_Duration: Single; safecall;
    function Get_PagesSent: Integer; safecall;
    function Get_NumberOfRetries: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
    property transmissionId: WideString read Get_transmissionId;
    property docId: WideString read Get_docId;
    property FaxNumber: WideString read Get_FaxNumber;
    property RecipientCSID: WideString read Get_RecipientCSID;
    property CompletionDate: TDateTime read Get_CompletionDate;
    property FaxStatus: Integer read Get_FaxStatus;
    property Duration: Single read Get_Duration;
    property PagesSent: Integer read Get_PagesSent;
    property NumberOfRetries: Integer read Get_NumberOfRetries;
  end;

// *********************************************************************//
// DispIntf:  _OutboundDispositionDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}
// *********************************************************************//
  _OutboundDispositionDisp = dispinterface
    ['{20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property UserName: WideString readonly dispid 1610743812;
    property Password: WideString readonly dispid 1610743813;
    property transmissionId: WideString readonly dispid 1610743814;
    property docId: WideString readonly dispid 1610743815;
    property FaxNumber: WideString readonly dispid 1610743816;
    property RecipientCSID: WideString readonly dispid 1610743817;
    property CompletionDate: TDateTime readonly dispid 1610743818;
    property FaxStatus: Integer readonly dispid 1610743819;
    property Duration: Single readonly dispid 1610743820;
    property PagesSent: Integer readonly dispid 1610743821;
    property NumberOfRetries: Integer readonly dispid 1610743822;
  end;

// *********************************************************************//
// Interface: _OutboundDispositionInternal
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}
// *********************************************************************//
  _OutboundDispositionInternal = interface(IDispatch)
    ['{6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}']
  end;

// *********************************************************************//
// DispIntf:  _OutboundDispositionInternalDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}
// *********************************************************************//
  _OutboundDispositionInternalDisp = dispinterface
    ['{6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}']
  end;

// *********************************************************************//
// Interface: _OutboundRequest
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}
// *********************************************************************//
  _OutboundRequest = interface(IDispatch)
    ['{1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_TransmissionControl: _RequestTransmissionControl; safecall;
    function Get_DispositionControl: _DispositionControl; safecall;
    function Get_Recipient: _Recipient; safecall;
    function Get_Files: _FaxFiles; safecall;
    property ToString: WideString read Get_ToString;
    property TransmissionControl: _RequestTransmissionControl read Get_TransmissionControl;
    property DispositionControl: _DispositionControl read Get_DispositionControl;
    property Recipient: _Recipient read Get_Recipient;
    property Files: _FaxFiles read Get_Files;
  end;

// *********************************************************************//
// DispIntf:  _OutboundRequestDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}
// *********************************************************************//
  _OutboundRequestDisp = dispinterface
    ['{1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property TransmissionControl: _RequestTransmissionControl readonly dispid 1610743812;
    property DispositionControl: _DispositionControl readonly dispid 1610743813;
    property Recipient: _Recipient readonly dispid 1610743814;
    property Files: _FaxFiles readonly dispid 1610743815;
  end;

// *********************************************************************//
// Interface: _OutboundResponse
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {5765572A-9D6D-3F77-A0EA-8672CB6EF53E}
// *********************************************************************//
  _OutboundResponse = interface(IDispatch)
    ['{5765572A-9D6D-3F77-A0EA-8672CB6EF53E}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_transmissionId: WideString; safecall;
    function Get_docId: WideString; safecall;
    function Get_StatusCode: StatusCode; safecall;
    function Get_StatusDescription: WideString; safecall;
    function Get_ErrorLevel: ErrorLevel; safecall;
    function Get_ErrorMessage: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId;
    property docId: WideString read Get_docId;
    property StatusCode: StatusCode read Get_StatusCode;
    property StatusDescription: WideString read Get_StatusDescription;
    property ErrorLevel: ErrorLevel read Get_ErrorLevel;
    property ErrorMessage: WideString read Get_ErrorMessage;
  end;

// *********************************************************************//
// DispIntf:  _OutboundResponseDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {5765572A-9D6D-3F77-A0EA-8672CB6EF53E}
// *********************************************************************//
  _OutboundResponseDisp = dispinterface
    ['{5765572A-9D6D-3F77-A0EA-8672CB6EF53E}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property transmissionId: WideString readonly dispid 1610743812;
    property docId: WideString readonly dispid 1610743813;
    property StatusCode: StatusCode readonly dispid 1610743814;
    property StatusDescription: WideString readonly dispid 1610743815;
    property ErrorLevel: ErrorLevel readonly dispid 1610743816;
    property ErrorMessage: WideString readonly dispid 1610743817;
  end;

// *********************************************************************//
// Interface: _OutboundStatusResponse
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}
// *********************************************************************//
  _OutboundStatusResponse = interface(IDispatch)
    ['{F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_transmissionId: WideString; safecall;
    function Get_CustomerID: WideString; safecall;
    function Get_Recipient: _StatusResponseRecipient; safecall;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId;
    property CustomerID: WideString read Get_CustomerID;
    property Recipient: _StatusResponseRecipient read Get_Recipient;
  end;

// *********************************************************************//
// DispIntf:  _OutboundStatusResponseDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}
// *********************************************************************//
  _OutboundStatusResponseDisp = dispinterface
    ['{F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property transmissionId: WideString readonly dispid 1610743812;
    property CustomerID: WideString readonly dispid 1610743813;
    property Recipient: _StatusResponseRecipient readonly dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _Recipient
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {91AE3E60-EF43-3547-87D6-E6BC79245862}
// *********************************************************************//
  _Recipient = interface(IDispatch)
    ['{91AE3E60-EF43-3547-87D6-E6BC79245862}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_RecipientName: WideString; safecall;
    procedure Set_RecipientName(const pRetVal: WideString); safecall;
    function Get_RecipientCompany: WideString; safecall;
    procedure Set_RecipientCompany(const pRetVal: WideString); safecall;
    function Get_RecipientFax: WideString; safecall;
    procedure Set_RecipientFax(const pRetVal: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property RecipientName: WideString read Get_RecipientName write Set_RecipientName;
    property RecipientCompany: WideString read Get_RecipientCompany write Set_RecipientCompany;
    property RecipientFax: WideString read Get_RecipientFax write Set_RecipientFax;
  end;

// *********************************************************************//
// DispIntf:  _RecipientDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {91AE3E60-EF43-3547-87D6-E6BC79245862}
// *********************************************************************//
  _RecipientDisp = dispinterface
    ['{91AE3E60-EF43-3547-87D6-E6BC79245862}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property RecipientName: WideString dispid 1610743812;
    property RecipientCompany: WideString dispid 1610743814;
    property RecipientFax: WideString dispid 1610743816;
  end;

// *********************************************************************//
// Interface: _RequestTransmissionControl
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {526EA2AA-D269-3E5C-96EF-C6C432FD41CB}
// *********************************************************************//
  _RequestTransmissionControl = interface(IDispatch)
    ['{526EA2AA-D269-3E5C-96EF-C6C432FD41CB}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_transmissionId: WideString; safecall;
    procedure Set_transmissionId(const pRetVal: WideString); safecall;
    function Get_CustomerID: WideString; safecall;
    procedure Set_CustomerID(const pRetVal: WideString); safecall;
    function Get_NoDuplicates: NoDuplicates; safecall;
    procedure Set_NoDuplicates(pRetVal: NoDuplicates); safecall;
    function Get_Resolution: Resolution; safecall;
    procedure Set_Resolution(pRetVal: Resolution); safecall;
    function Get_Priority: Priority; safecall;
    procedure Set_Priority(pRetVal: Priority); safecall;
    function Get_SelfBusy: SelfBusy; safecall;
    procedure Set_SelfBusy(pRetVal: SelfBusy); safecall;
    function Get_FaxHeader: WideString; safecall;
    procedure Set_FaxHeader(const pRetVal: WideString); safecall;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId write Set_transmissionId;
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property NoDuplicates: NoDuplicates read Get_NoDuplicates write Set_NoDuplicates;
    property Resolution: Resolution read Get_Resolution write Set_Resolution;
    property Priority: Priority read Get_Priority write Set_Priority;
    property SelfBusy: SelfBusy read Get_SelfBusy write Set_SelfBusy;
    property FaxHeader: WideString read Get_FaxHeader write Set_FaxHeader;
  end;

// *********************************************************************//
// DispIntf:  _RequestTransmissionControlDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {526EA2AA-D269-3E5C-96EF-C6C432FD41CB}
// *********************************************************************//
  _RequestTransmissionControlDisp = dispinterface
    ['{526EA2AA-D269-3E5C-96EF-C6C432FD41CB}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property transmissionId: WideString dispid 1610743812;
    property CustomerID: WideString dispid 1610743814;
    property NoDuplicates: NoDuplicates dispid 1610743816;
    property Resolution: Resolution dispid 1610743818;
    property Priority: Priority dispid 1610743820;
    property SelfBusy: SelfBusy dispid 1610743822;
    property FaxHeader: WideString dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _StatusResponseRecipient
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {70A838A9-9847-39B1-9FAC-10CADAC025D1}
// *********************************************************************//
  _StatusResponseRecipient = interface(IDispatch)
    ['{70A838A9-9847-39B1-9FAC-10CADAC025D1}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_docId: WideString; safecall;
    function Get_Name: WideString; safecall;
    function Get_Company: WideString; safecall;
    function Get_Fax: WideString; safecall;
    function Get_Status: _Status; safecall;
    function Get_RemoteCSID: WideString; safecall;
    function Get_LastAttempt: TDateTime; safecall;
    function Get_NextAttempt: TDateTime; safecall;
    function Get_Duration: Single; safecall;
    function Get_Retries: Integer; safecall;
    function Get_BaudRate: Integer; safecall;
    function Get_PagesScheduled: Integer; safecall;
    function Get_PagesSent: Integer; safecall;
    property ToString: WideString read Get_ToString;
    property docId: WideString read Get_docId;
    property Name: WideString read Get_Name;
    property Company: WideString read Get_Company;
    property Fax: WideString read Get_Fax;
    property Status: _Status read Get_Status;
    property RemoteCSID: WideString read Get_RemoteCSID;
    property LastAttempt: TDateTime read Get_LastAttempt;
    property NextAttempt: TDateTime read Get_NextAttempt;
    property Duration: Single read Get_Duration;
    property Retries: Integer read Get_Retries;
    property BaudRate: Integer read Get_BaudRate;
    property PagesScheduled: Integer read Get_PagesScheduled;
    property PagesSent: Integer read Get_PagesSent;
  end;

// *********************************************************************//
// DispIntf:  _StatusResponseRecipientDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {70A838A9-9847-39B1-9FAC-10CADAC025D1}
// *********************************************************************//
  _StatusResponseRecipientDisp = dispinterface
    ['{70A838A9-9847-39B1-9FAC-10CADAC025D1}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property docId: WideString readonly dispid 1610743812;
    property Name: WideString readonly dispid 1610743813;
    property Company: WideString readonly dispid 1610743814;
    property Fax: WideString readonly dispid 1610743815;
    property Status: _Status readonly dispid 1610743816;
    property RemoteCSID: WideString readonly dispid 1610743817;
    property LastAttempt: TDateTime readonly dispid 1610743818;
    property NextAttempt: TDateTime readonly dispid 1610743819;
    property Duration: Single readonly dispid 1610743820;
    property Retries: Integer readonly dispid 1610743821;
    property BaudRate: Integer readonly dispid 1610743822;
    property PagesScheduled: Integer readonly dispid 1610743823;
    property PagesSent: Integer readonly dispid 1610743824;
  end;

// *********************************************************************//
// Interface: _Status
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1ACD9FAB-E245-3C68-A298-CB721C4F9400}
// *********************************************************************//
  _Status = interface(IDispatch)
    ['{1ACD9FAB-E245-3C68-A298-CB721C4F9400}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function Get_Message: WideString; safecall;
    function Get_Classification: WideString; safecall;
    function Get_Outcome: WideString; safecall;
    property ToString: WideString read Get_ToString;
    property Message: WideString read Get_Message;
    property Classification: WideString read Get_Classification;
    property Outcome: WideString read Get_Outcome;
  end;

// *********************************************************************//
// DispIntf:  _StatusDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1ACD9FAB-E245-3C68-A298-CB721C4F9400}
// *********************************************************************//
  _StatusDisp = dispinterface
    ['{1ACD9FAB-E245-3C68-A298-CB721C4F9400}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    property Message: WideString readonly dispid 1610743812;
    property Classification: WideString readonly dispid 1610743813;
    property Outcome: WideString readonly dispid 1610743814;
  end;

// *********************************************************************//
// Interface: _XmlUtils
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {A903F01C-AC4C-3AA5-86F5-C70D386C7E50}
// *********************************************************************//
  _XmlUtils = interface(IDispatch)
    ['{A903F01C-AC4C-3AA5-86F5-C70D386C7E50}']
    function Get_ToString: WideString; safecall;
    function Equals(obj: OleVariant): WordBool; safecall;
    function GetHashCode: Integer; safecall;
    function GetType: _Type; safecall;
    function ExtractXMLFromURLEncodedData(const data: WideString): WideString; safecall;
    function ExtractXMLFromURLEncodedData_2(const stream: _Stream): WideString; safecall;
    property ToString: WideString read Get_ToString;
  end;

// *********************************************************************//
// DispIntf:  _XmlUtilsDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {A903F01C-AC4C-3AA5-86F5-C70D386C7E50}
// *********************************************************************//
  _XmlUtilsDisp = dispinterface
    ['{A903F01C-AC4C-3AA5-86F5-C70D386C7E50}']
    property ToString: WideString readonly dispid 0;
    function Equals(obj: OleVariant): WordBool; dispid 1610743809;
    function GetHashCode: Integer; dispid 1610743810;
    function GetType: _Type; dispid 1610743811;
    function ExtractXMLFromURLEncodedData(const data: WideString): WideString; dispid 1610743812;
    function ExtractXMLFromURLEncodedData_2(const stream: _Stream): WideString; dispid 1610743813;
  end;

// *********************************************************************//
// The Class CoeFaxDeveloperException provides a Create and CreateRemote method to          
// create instances of the default interface _eFaxDeveloperException exposed by              
// the CoClass eFaxDeveloperException. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoeFaxDeveloperException = class
    class function Create: _eFaxDeveloperException;
    class function CreateRemote(const MachineName: string): _eFaxDeveloperException;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TeFaxDeveloperException
// Help String      : 
// Default Interface: _eFaxDeveloperException
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (0)
// *********************************************************************//
  TeFaxDeveloperException = class(TOleServer)
  private
    FIntf: _eFaxDeveloperException;
    function GetDefaultInterface: _eFaxDeveloperException;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _eFaxDeveloperException);
    procedure Disconnect; override;
    property DefaultInterface: _eFaxDeveloperException read GetDefaultInterface;
  published
  end;

// *********************************************************************//
// The Class CoAdditionalInfo provides a Create and CreateRemote method to          
// create instances of the default interface _AdditionalInfo exposed by              
// the CoClass AdditionalInfo. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAdditionalInfo = class
    class function Create: _AdditionalInfo;
    class function CreateRemote(const MachineName: string): _AdditionalInfo;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TAdditionalInfo
// Help String      : 
// Default Interface: _AdditionalInfo
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TAdditionalInfo = class(TOleServer)
  private
    FIntf: _AdditionalInfo;
    function GetDefaultInterface: _AdditionalInfo;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_ReadSequence: Integer;
    function Get_ReadDirection: ReadDirection;
    function Get_Symbology: WideString;
    function Get_CodeLocation: _CodeLocation;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _AdditionalInfo);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _AdditionalInfo read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property ReadSequence: Integer read Get_ReadSequence;
    property ReadDirection: ReadDirection read Get_ReadDirection;
    property Symbology: WideString read Get_Symbology;
    property CodeLocation: _CodeLocation read Get_CodeLocation;
  published
  end;

// *********************************************************************//
// The Class CoBarcodes provides a Create and CreateRemote method to          
// create instances of the default interface _Barcodes exposed by              
// the CoClass Barcodes. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBarcodes = class
    class function Create: _Barcodes;
    class function CreateRemote(const MachineName: string): _Barcodes;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TBarcodes
// Help String      : 
// Default Interface: _Barcodes
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TBarcodes = class(TOleServer)
  private
    FIntf: _Barcodes;
    function GetDefaultInterface: _Barcodes;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_item(index: Integer): _Barcode;
    procedure _Set_item(index: Integer; const pRetVal: _Barcode);
    function Get_Count: Integer;
    function Get_IsReadOnly: WordBool;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Barcodes);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function IndexOf(const item: _Barcode): Integer;
    procedure Insert(index: Integer; const item: _Barcode);
    procedure RemoveAt(index: Integer);
    procedure Add(const item: _Barcode);
    procedure Clear;
    function Contains(const item: _Barcode): WordBool;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer);
    function Remove(const item: _Barcode): WordBool;
    property DefaultInterface: _Barcodes read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _Barcode read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  published
  end;

// *********************************************************************//
// The Class CoBarcode provides a Create and CreateRemote method to          
// create instances of the default interface _Barcode exposed by              
// the CoClass Barcode. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBarcode = class
    class function Create: _Barcode;
    class function CreateRemote(const MachineName: string): _Barcode;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TBarcode
// Help String      : 
// Default Interface: _Barcode
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TBarcode = class(TOleServer)
  private
    FIntf: _Barcode;
    function GetDefaultInterface: _Barcode;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_Key: WideString;
    function Get_AdditionalInfo: _AdditionalInfo;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Barcode);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _Barcode read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property Key: WideString read Get_Key;
    property AdditionalInfo: _AdditionalInfo read Get_AdditionalInfo;
  published
  end;

// *********************************************************************//
// The Class CoCodeLocation provides a Create and CreateRemote method to          
// create instances of the default interface _CodeLocation exposed by              
// the CoClass CodeLocation. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCodeLocation = class
    class function Create: _CodeLocation;
    class function CreateRemote(const MachineName: string): _CodeLocation;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCodeLocation
// Help String      : 
// Default Interface: _CodeLocation
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TCodeLocation = class(TOleServer)
  private
    FIntf: _CodeLocation;
    function GetDefaultInterface: _CodeLocation;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_PageNumber: Integer;
    function Get_PageCoordinates: _PageCoordinates;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _CodeLocation);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _CodeLocation read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property PageNumber: Integer read Get_PageNumber;
    property PageCoordinates: _PageCoordinates read Get_PageCoordinates;
  published
  end;

// *********************************************************************//
// The Class CoEndEdge provides a Create and CreateRemote method to          
// create instances of the default interface _EndEdge exposed by              
// the CoClass EndEdge. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoEndEdge = class
    class function Create: _EndEdge;
    class function CreateRemote(const MachineName: string): _EndEdge;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TEndEdge
// Help String      : 
// Default Interface: _EndEdge
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TEndEdge = class(TOleServer)
  private
    FIntf: _EndEdge;
    function GetDefaultInterface: _EndEdge;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_XEndPointA: Single;
    function Get_YEndPointA: Single;
    function Get_XEndPointB: Single;
    function Get_YEndPointB: Single;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _EndEdge);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _EndEdge read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property XEndPointA: Single read Get_XEndPointA;
    property YEndPointA: Single read Get_YEndPointA;
    property XEndPointB: Single read Get_XEndPointB;
    property YEndPointB: Single read Get_YEndPointB;
  published
  end;

// *********************************************************************//
// The Class CoFaxControl provides a Create and CreateRemote method to          
// create instances of the default interface _FaxControl exposed by              
// the CoClass FaxControl. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFaxControl = class
    class function Create: _FaxControl;
    class function CreateRemote(const MachineName: string): _FaxControl;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFaxControl
// Help String      : 
// Default Interface: _FaxControl
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TFaxControl = class(TOleServer)
  private
    FIntf: _FaxControl;
    function GetDefaultInterface: _FaxControl;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_AccountID: WideString;
    function Get_NumberDialed: WideString;
    function Get_DateReceived: TDateTime;
    function Get_FaxName: WideString;
    function Get_FileType: FaxFileType;
    function Get_PageCount: Integer;
    function Get_CSID: WideString;
    function Get_ANI: WideString;
    function Get_Status: Integer;
    function Get_MCFID: Integer;
    function Get_UserFields: _UserFields;
    function Get_Barcodes: _Barcodes;
    function Get_Pages: _Pages;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _FaxControl);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function GetFileContentsCOM: OleVariant;
    property DefaultInterface: _FaxControl read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property AccountID: WideString read Get_AccountID;
    property NumberDialed: WideString read Get_NumberDialed;
    property DateReceived: TDateTime read Get_DateReceived;
    property FaxName: WideString read Get_FaxName;
    property FileType: FaxFileType read Get_FileType;
    property PageCount: Integer read Get_PageCount;
    property CSID: WideString read Get_CSID;
    property ANI: WideString read Get_ANI;
    property Status: Integer read Get_Status;
    property MCFID: Integer read Get_MCFID;
    property UserFields: _UserFields read Get_UserFields;
    property Barcodes: _Barcodes read Get_Barcodes;
    property Pages: _Pages read Get_Pages;
  published
  end;

// *********************************************************************//
// The Class CoInboundClient provides a Create and CreateRemote method to          
// create instances of the default interface _InboundClient exposed by              
// the CoClass InboundClient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoInboundClient = class
    class function Create: _InboundClient;
    class function CreateRemote(const MachineName: string): _InboundClient;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TInboundClient
// Help String      : 
// Default Interface: _InboundClient
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TInboundClient = class(TOleServer)
  private
    FIntf: _InboundClient;
    function GetDefaultInterface: _InboundClient;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _InboundClient);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function DeserializeInboundPostRequest(const xml: WideString): _InboundPostRequest;
    function DeserializeInboundPostRequest_2(const stream: _Stream): _InboundPostRequest;
    property DefaultInterface: _InboundClient read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
  published
  end;

// *********************************************************************//
// The Class CoInboundPostRequest provides a Create and CreateRemote method to          
// create instances of the default interface _InboundPostRequest exposed by              
// the CoClass InboundPostRequest. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoInboundPostRequest = class
    class function Create: _InboundPostRequest;
    class function CreateRemote(const MachineName: string): _InboundPostRequest;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TInboundPostRequest
// Help String      : 
// Default Interface: _InboundPostRequest
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TInboundPostRequest = class(TOleServer)
  private
    FIntf: _InboundPostRequest;
    function GetDefaultInterface: _InboundPostRequest;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_FaxControl: _FaxControl;
    function Get_UserName: WideString;
    function Get_Password: WideString;
    function Get_RequestDate: TDateTime;
    function Get_RequestType: RequestType;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _InboundPostRequest);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _InboundPostRequest read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property FaxControl: _FaxControl read Get_FaxControl;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
    property RequestDate: TDateTime read Get_RequestDate;
    property RequestType: RequestType read Get_RequestType;
  published
  end;

// *********************************************************************//
// The Class CoPages provides a Create and CreateRemote method to          
// create instances of the default interface _Pages exposed by              
// the CoClass Pages. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPages = class
    class function Create: _Pages;
    class function CreateRemote(const MachineName: string): _Pages;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TPages
// Help String      : 
// Default Interface: _Pages
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TPages = class(TOleServer)
  private
    FIntf: _Pages;
    function GetDefaultInterface: _Pages;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_item(index: Integer): _Page;
    procedure _Set_item(index: Integer; const pRetVal: _Page);
    function Get_Count: Integer;
    function Get_IsReadOnly: WordBool;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Pages);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function IndexOf(const item: _Page): Integer;
    procedure Insert(index: Integer; const item: _Page);
    procedure RemoveAt(index: Integer);
    procedure Add(const item: _Page);
    procedure Clear;
    function Contains(const item: _Page): WordBool;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer);
    function Remove(const item: _Page): WordBool;
    property DefaultInterface: _Pages read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _Page read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  published
  end;

// *********************************************************************//
// The Class CoPage provides a Create and CreateRemote method to          
// create instances of the default interface _Page exposed by              
// the CoClass Page. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPage = class
    class function Create: _Page;
    class function CreateRemote(const MachineName: string): _Page;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TPage
// Help String      : 
// Default Interface: _Page
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TPage = class(TOleServer)
  private
    FIntf: _Page;
    function GetDefaultInterface: _Page;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_PageNumber: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Page);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function GetPageContentsCOM: OleVariant;
    property DefaultInterface: _Page read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property PageNumber: Integer read Get_PageNumber;
  published
  end;

// *********************************************************************//
// The Class CoPageCoordinates provides a Create and CreateRemote method to          
// create instances of the default interface _PageCoordinates exposed by              
// the CoClass PageCoordinates. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPageCoordinates = class
    class function Create: _PageCoordinates;
    class function CreateRemote(const MachineName: string): _PageCoordinates;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TPageCoordinates
// Help String      : 
// Default Interface: _PageCoordinates
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TPageCoordinates = class(TOleServer)
  private
    FIntf: _PageCoordinates;
    function GetDefaultInterface: _PageCoordinates;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_StartEdge: _StartEdge;
    function Get_EndEdge: _EndEdge;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _PageCoordinates);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _PageCoordinates read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property StartEdge: _StartEdge read Get_StartEdge;
    property EndEdge: _EndEdge read Get_EndEdge;
  published
  end;

// *********************************************************************//
// The Class CoStartEdge provides a Create and CreateRemote method to          
// create instances of the default interface _StartEdge exposed by              
// the CoClass StartEdge. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStartEdge = class
    class function Create: _StartEdge;
    class function CreateRemote(const MachineName: string): _StartEdge;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TStartEdge
// Help String      : 
// Default Interface: _StartEdge
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TStartEdge = class(TOleServer)
  private
    FIntf: _StartEdge;
    function GetDefaultInterface: _StartEdge;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_XStartPointA: Single;
    function Get_YStartPointA: Single;
    function Get_XStartPointB: Single;
    function Get_YStartPointB: Single;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _StartEdge);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _StartEdge read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property XStartPointA: Single read Get_XStartPointA;
    property YStartPointA: Single read Get_YStartPointA;
    property XStartPointB: Single read Get_XStartPointB;
    property YStartPointB: Single read Get_YStartPointB;
  published
  end;

// *********************************************************************//
// The Class CoUserFields provides a Create and CreateRemote method to          
// create instances of the default interface _UserFields exposed by              
// the CoClass UserFields. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUserFields = class
    class function Create: _UserFields;
    class function CreateRemote(const MachineName: string): _UserFields;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TUserFields
// Help String      : 
// Default Interface: _UserFields
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TUserFields = class(TOleServer)
  private
    FIntf: _UserFields;
    function GetDefaultInterface: _UserFields;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_item(index: Integer): _UserField;
    procedure _Set_item(index: Integer; const pRetVal: _UserField);
    function Get_Count: Integer;
    function Get_IsReadOnly: WordBool;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _UserFields);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function IndexOf(const item: _UserField): Integer;
    procedure Insert(index: Integer; const item: _UserField);
    procedure RemoveAt(index: Integer);
    procedure Add(const item: _UserField);
    procedure Clear;
    function Contains(const item: _UserField): WordBool;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer);
    function Remove(const item: _UserField): WordBool;
    property DefaultInterface: _UserFields read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _UserField read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  published
  end;

// *********************************************************************//
// The Class CoUserField provides a Create and CreateRemote method to          
// create instances of the default interface _UserField exposed by              
// the CoClass UserField. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUserField = class
    class function Create: _UserField;
    class function CreateRemote(const MachineName: string): _UserField;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TUserField
// Help String      : 
// Default Interface: _UserField
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TUserField = class(TOleServer)
  private
    FIntf: _UserField;
    function GetDefaultInterface: _UserField;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_FieldName: WideString;
    procedure Set_FieldName(const pRetVal: WideString);
    function Get_FieldValue: WideString;
    procedure Set_FieldValue(const pRetVal: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _UserField);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _UserField read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property FieldName: WideString read Get_FieldName write Set_FieldName;
    property FieldValue: WideString read Get_FieldValue write Set_FieldValue;
  published
  end;

// *********************************************************************//
// The Class CoAccessControl provides a Create and CreateRemote method to          
// create instances of the default interface _AccessControl exposed by              
// the CoClass AccessControl. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAccessControl = class
    class function Create: _AccessControl;
    class function CreateRemote(const MachineName: string): _AccessControl;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TAccessControl
// Help String      : 
// Default Interface: _AccessControl
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TAccessControl = class(TOleServer)
  private
    FIntf: _AccessControl;
    function GetDefaultInterface: _AccessControl;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_UserName: WideString;
    function Get_Password: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _AccessControl);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _AccessControl read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
  published
  end;

// *********************************************************************//
// The Class CoDispositionControl provides a Create and CreateRemote method to          
// create instances of the default interface _DispositionControl exposed by              
// the CoClass DispositionControl. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDispositionControl = class
    class function Create: _DispositionControl;
    class function CreateRemote(const MachineName: string): _DispositionControl;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TDispositionControl
// Help String      : 
// Default Interface: _DispositionControl
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TDispositionControl = class(TOleServer)
  private
    FIntf: _DispositionControl;
    function GetDefaultInterface: _DispositionControl;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_DispositionURL: WideString;
    procedure Set_DispositionURL(const pRetVal: WideString);
    function Get_DispositionLevel: DispositionLevel;
    procedure Set_DispositionLevel(pRetVal: DispositionLevel);
    function Get_DispositionMethod: DispositionMethod;
    procedure Set_DispositionMethod(pRetVal: DispositionMethod);
    function Get_DispositionEmails: _DispositionEmails;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _DispositionControl);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _DispositionControl read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property DispositionEmails: _DispositionEmails read Get_DispositionEmails;
    property DispositionURL: WideString read Get_DispositionURL write Set_DispositionURL;
    property DispositionLevel: DispositionLevel read Get_DispositionLevel write Set_DispositionLevel;
    property DispositionMethod: DispositionMethod read Get_DispositionMethod write Set_DispositionMethod;
  published
  end;

// *********************************************************************//
// The Class CoDispositionEmail provides a Create and CreateRemote method to          
// create instances of the default interface _DispositionEmail exposed by              
// the CoClass DispositionEmail. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDispositionEmail = class
    class function Create: _DispositionEmail;
    class function CreateRemote(const MachineName: string): _DispositionEmail;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TDispositionEmail
// Help String      : 
// Default Interface: _DispositionEmail
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TDispositionEmail = class(TOleServer)
  private
    FIntf: _DispositionEmail;
    function GetDefaultInterface: _DispositionEmail;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_DispositionRecipient: WideString;
    procedure Set_DispositionRecipient(const pRetVal: WideString);
    function Get_DispositionAddress: WideString;
    procedure Set_DispositionAddress(const pRetVal: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _DispositionEmail);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _DispositionEmail read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property DispositionRecipient: WideString read Get_DispositionRecipient write Set_DispositionRecipient;
    property DispositionAddress: WideString read Get_DispositionAddress write Set_DispositionAddress;
  published
  end;

// *********************************************************************//
// The Class CoDispositionEmails provides a Create and CreateRemote method to          
// create instances of the default interface _DispositionEmails exposed by              
// the CoClass DispositionEmails. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDispositionEmails = class
    class function Create: _DispositionEmails;
    class function CreateRemote(const MachineName: string): _DispositionEmails;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TDispositionEmails
// Help String      : 
// Default Interface: _DispositionEmails
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TDispositionEmails = class(TOleServer)
  private
    FIntf: _DispositionEmails;
    function GetDefaultInterface: _DispositionEmails;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_item(index: Integer): _DispositionEmail;
    procedure _Set_item(index: Integer; const pRetVal: _DispositionEmail);
    function Get_Count: Integer;
    function Get_IsReadOnly: WordBool;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _DispositionEmails);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function IndexOf(const item: _DispositionEmail): Integer;
    procedure Insert(index: Integer; const item: _DispositionEmail);
    procedure RemoveAt(index: Integer);
    procedure Add(const item: _DispositionEmail);
    procedure Clear;
    function Contains(const item: _DispositionEmail): WordBool;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer);
    function Remove(const item: _DispositionEmail): WordBool;
    property DefaultInterface: _DispositionEmails read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _DispositionEmail read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  published
  end;

// *********************************************************************//
// The Class CoFaxFiles provides a Create and CreateRemote method to          
// create instances of the default interface _FaxFiles exposed by              
// the CoClass FaxFiles. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFaxFiles = class
    class function Create: _FaxFiles;
    class function CreateRemote(const MachineName: string): _FaxFiles;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFaxFiles
// Help String      : 
// Default Interface: _FaxFiles
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TFaxFiles = class(TOleServer)
  private
    FIntf: _FaxFiles;
    function GetDefaultInterface: _FaxFiles;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_item(index: Integer): _FaxFile;
    procedure _Set_item(index: Integer; const pRetVal: _FaxFile);
    function Get_Count: Integer;
    function Get_IsReadOnly: WordBool;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _FaxFiles);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function IndexOf(const item: _FaxFile): Integer;
    procedure Insert(index: Integer; const item: _FaxFile);
    procedure RemoveAt(index: Integer);
    procedure Add(const item: _FaxFile);
    procedure Clear;
    function Contains(const item: _FaxFile): WordBool;
    procedure CopyTo(array_: PSafeArray; arrayIndex: Integer);
    function Remove(const item: _FaxFile): WordBool;
    property DefaultInterface: _FaxFiles read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property item[index: Integer]: _FaxFile read Get_item write _Set_item; default;
    property Count: Integer read Get_Count;
    property IsReadOnly: WordBool read Get_IsReadOnly;
  published
  end;

// *********************************************************************//
// The Class CoFaxFile provides a Create and CreateRemote method to          
// create instances of the default interface _FaxFile exposed by              
// the CoClass FaxFile. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFaxFile = class
    class function Create: _FaxFile;
    class function CreateRemote(const MachineName: string): _FaxFile;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TFaxFile
// Help String      : 
// Default Interface: _FaxFile
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TFaxFile = class(TOleServer)
  private
    FIntf: _FaxFile;
    function GetDefaultInterface: _FaxFile;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_FileType: FaxFileType;
    procedure Set_FileType(pRetVal: FaxFileType);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _FaxFile);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function GetFileContentsCOM: OleVariant;
    procedure SetFileContentsCOM(data: OleVariant);
    property DefaultInterface: _FaxFile read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property FileType: FaxFileType read Get_FileType write Set_FileType;
  published
  end;

// *********************************************************************//
// The Class CoOutboundClient provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundClient exposed by              
// the CoClass OutboundClient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundClient = class
    class function Create: _OutboundClient;
    class function CreateRemote(const MachineName: string): _OutboundClient;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundClient
// Help String      : 
// Default Interface: _OutboundClient
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOutboundClient = class(TOleServer)
  private
    FIntf: _OutboundClient;
    function GetDefaultInterface: _OutboundClient;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_UserName: WideString;
    procedure Set_UserName(const pRetVal: WideString);
    function Get_Password: WideString;
    procedure Set_Password(const pRetVal: WideString);
    function Get_AccountID: WideString;
    procedure Set_AccountID(const pRetVal: WideString);
    function Get_ServiceUrl: WideString;
    procedure Set_ServiceUrl(const pRetVal: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundClient);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    procedure LoadWebConfiguration;
    procedure SaveWebConfiguration(protect: WordBool);
    procedure LoadConfiguration;
    procedure SaveConfiguration(protect: WordBool);
    function DeserializeOutboundDisposition(const stream: _Stream): _OutboundDisposition;
    function DeserializeOutboundDisposition_2(const xml: WideString): _OutboundDisposition;
    function SendOutboundRequest(const OutboundRequest: _OutboundRequest): _OutboundResponse;
    function SendOutboundStatusRequest(const transmissionId: WideString; const docId: WideString): _OutboundStatusResponse;
    property DefaultInterface: _OutboundClient read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName write Set_UserName;
    property Password: WideString read Get_Password write Set_Password;
    property AccountID: WideString read Get_AccountID write Set_AccountID;
    property ServiceUrl: WideString read Get_ServiceUrl write Set_ServiceUrl;
  published
  end;

// *********************************************************************//
// The Class CoOutboundDisposition provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundDisposition exposed by              
// the CoClass OutboundDisposition. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundDisposition = class
    class function Create: _OutboundDisposition;
    class function CreateRemote(const MachineName: string): _OutboundDisposition;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundDisposition
// Help String      : 
// Default Interface: _OutboundDisposition
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (0)
// *********************************************************************//
  TOutboundDisposition = class(TOleServer)
  private
    FIntf: _OutboundDisposition;
    function GetDefaultInterface: _OutboundDisposition;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_UserName: WideString;
    function Get_Password: WideString;
    function Get_transmissionId: WideString;
    function Get_docId: WideString;
    function Get_FaxNumber: WideString;
    function Get_RecipientCSID: WideString;
    function Get_CompletionDate: TDateTime;
    function Get_FaxStatus: Integer;
    function Get_Duration: Single;
    function Get_PagesSent: Integer;
    function Get_NumberOfRetries: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundDisposition);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _OutboundDisposition read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property UserName: WideString read Get_UserName;
    property Password: WideString read Get_Password;
    property transmissionId: WideString read Get_transmissionId;
    property docId: WideString read Get_docId;
    property FaxNumber: WideString read Get_FaxNumber;
    property RecipientCSID: WideString read Get_RecipientCSID;
    property CompletionDate: TDateTime read Get_CompletionDate;
    property FaxStatus: Integer read Get_FaxStatus;
    property Duration: Single read Get_Duration;
    property PagesSent: Integer read Get_PagesSent;
    property NumberOfRetries: Integer read Get_NumberOfRetries;
  published
  end;

// *********************************************************************//
// The Class CoOutboundDispositionInternal provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundDispositionInternal exposed by              
// the CoClass OutboundDispositionInternal. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundDispositionInternal = class
    class function Create: _OutboundDispositionInternal;
    class function CreateRemote(const MachineName: string): _OutboundDispositionInternal;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundDispositionInternal
// Help String      : 
// Default Interface: _OutboundDispositionInternal
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOutboundDispositionInternal = class(TOleServer)
  private
    FIntf: _OutboundDispositionInternal;
    function GetDefaultInterface: _OutboundDispositionInternal;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundDispositionInternal);
    procedure Disconnect; override;
    property DefaultInterface: _OutboundDispositionInternal read GetDefaultInterface;
  published
  end;

// *********************************************************************//
// The Class CoOutboundRequest provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundRequest exposed by              
// the CoClass OutboundRequest. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundRequest = class
    class function Create: _OutboundRequest;
    class function CreateRemote(const MachineName: string): _OutboundRequest;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundRequest
// Help String      : 
// Default Interface: _OutboundRequest
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOutboundRequest = class(TOleServer)
  private
    FIntf: _OutboundRequest;
    function GetDefaultInterface: _OutboundRequest;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_TransmissionControl: _RequestTransmissionControl;
    function Get_DispositionControl: _DispositionControl;
    function Get_Recipient: _Recipient;
    function Get_Files: _FaxFiles;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundRequest);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _OutboundRequest read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property TransmissionControl: _RequestTransmissionControl read Get_TransmissionControl;
    property DispositionControl: _DispositionControl read Get_DispositionControl;
    property Recipient: _Recipient read Get_Recipient;
    property Files: _FaxFiles read Get_Files;
  published
  end;

// *********************************************************************//
// The Class CoOutboundResponse provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundResponse exposed by              
// the CoClass OutboundResponse. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundResponse = class
    class function Create: _OutboundResponse;
    class function CreateRemote(const MachineName: string): _OutboundResponse;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundResponse
// Help String      : 
// Default Interface: _OutboundResponse
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOutboundResponse = class(TOleServer)
  private
    FIntf: _OutboundResponse;
    function GetDefaultInterface: _OutboundResponse;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_transmissionId: WideString;
    function Get_docId: WideString;
    function Get_StatusCode: StatusCode;
    function Get_StatusDescription: WideString;
    function Get_ErrorLevel: ErrorLevel;
    function Get_ErrorMessage: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundResponse);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _OutboundResponse read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId;
    property docId: WideString read Get_docId;
    property StatusCode: StatusCode read Get_StatusCode;
    property StatusDescription: WideString read Get_StatusDescription;
    property ErrorLevel: ErrorLevel read Get_ErrorLevel;
    property ErrorMessage: WideString read Get_ErrorMessage;
  published
  end;

// *********************************************************************//
// The Class CoOutboundStatusResponse provides a Create and CreateRemote method to          
// create instances of the default interface _OutboundStatusResponse exposed by              
// the CoClass OutboundStatusResponse. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOutboundStatusResponse = class
    class function Create: _OutboundStatusResponse;
    class function CreateRemote(const MachineName: string): _OutboundStatusResponse;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOutboundStatusResponse
// Help String      : 
// Default Interface: _OutboundStatusResponse
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOutboundStatusResponse = class(TOleServer)
  private
    FIntf: _OutboundStatusResponse;
    function GetDefaultInterface: _OutboundStatusResponse;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_transmissionId: WideString;
    function Get_CustomerID: WideString;
    function Get_Recipient: _StatusResponseRecipient;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _OutboundStatusResponse);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _OutboundStatusResponse read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId;
    property CustomerID: WideString read Get_CustomerID;
    property Recipient: _StatusResponseRecipient read Get_Recipient;
  published
  end;

// *********************************************************************//
// The Class CoRecipient provides a Create and CreateRemote method to          
// create instances of the default interface _Recipient exposed by              
// the CoClass Recipient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRecipient = class
    class function Create: _Recipient;
    class function CreateRemote(const MachineName: string): _Recipient;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TRecipient
// Help String      : 
// Default Interface: _Recipient
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TRecipient = class(TOleServer)
  private
    FIntf: _Recipient;
    function GetDefaultInterface: _Recipient;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_RecipientName: WideString;
    procedure Set_RecipientName(const pRetVal: WideString);
    function Get_RecipientCompany: WideString;
    procedure Set_RecipientCompany(const pRetVal: WideString);
    function Get_RecipientFax: WideString;
    procedure Set_RecipientFax(const pRetVal: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Recipient);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _Recipient read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property RecipientName: WideString read Get_RecipientName write Set_RecipientName;
    property RecipientCompany: WideString read Get_RecipientCompany write Set_RecipientCompany;
    property RecipientFax: WideString read Get_RecipientFax write Set_RecipientFax;
  published
  end;

// *********************************************************************//
// The Class CoRequestTransmissionControl provides a Create and CreateRemote method to          
// create instances of the default interface _RequestTransmissionControl exposed by              
// the CoClass RequestTransmissionControl. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRequestTransmissionControl = class
    class function Create: _RequestTransmissionControl;
    class function CreateRemote(const MachineName: string): _RequestTransmissionControl;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TRequestTransmissionControl
// Help String      : 
// Default Interface: _RequestTransmissionControl
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TRequestTransmissionControl = class(TOleServer)
  private
    FIntf: _RequestTransmissionControl;
    function GetDefaultInterface: _RequestTransmissionControl;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_transmissionId: WideString;
    procedure Set_transmissionId(const pRetVal: WideString);
    function Get_CustomerID: WideString;
    procedure Set_CustomerID(const pRetVal: WideString);
    function Get_NoDuplicates: NoDuplicates;
    procedure Set_NoDuplicates(pRetVal: NoDuplicates);
    function Get_Resolution: Resolution;
    procedure Set_Resolution(pRetVal: Resolution);
    function Get_Priority: Priority;
    procedure Set_Priority(pRetVal: Priority);
    function Get_SelfBusy: SelfBusy;
    procedure Set_SelfBusy(pRetVal: SelfBusy);
    function Get_FaxHeader: WideString;
    procedure Set_FaxHeader(const pRetVal: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _RequestTransmissionControl);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _RequestTransmissionControl read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property transmissionId: WideString read Get_transmissionId write Set_transmissionId;
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property NoDuplicates: NoDuplicates read Get_NoDuplicates write Set_NoDuplicates;
    property Resolution: Resolution read Get_Resolution write Set_Resolution;
    property Priority: Priority read Get_Priority write Set_Priority;
    property SelfBusy: SelfBusy read Get_SelfBusy write Set_SelfBusy;
    property FaxHeader: WideString read Get_FaxHeader write Set_FaxHeader;
  published
  end;

// *********************************************************************//
// The Class CoStatusResponseRecipient provides a Create and CreateRemote method to          
// create instances of the default interface _StatusResponseRecipient exposed by              
// the CoClass StatusResponseRecipient. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStatusResponseRecipient = class
    class function Create: _StatusResponseRecipient;
    class function CreateRemote(const MachineName: string): _StatusResponseRecipient;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TStatusResponseRecipient
// Help String      : 
// Default Interface: _StatusResponseRecipient
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TStatusResponseRecipient = class(TOleServer)
  private
    FIntf: _StatusResponseRecipient;
    function GetDefaultInterface: _StatusResponseRecipient;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_docId: WideString;
    function Get_Name: WideString;
    function Get_Company: WideString;
    function Get_Fax: WideString;
    function Get_Status: _Status;
    function Get_RemoteCSID: WideString;
    function Get_LastAttempt: TDateTime;
    function Get_NextAttempt: TDateTime;
    function Get_Duration: Single;
    function Get_Retries: Integer;
    function Get_BaudRate: Integer;
    function Get_PagesScheduled: Integer;
    function Get_PagesSent: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _StatusResponseRecipient);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _StatusResponseRecipient read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property docId: WideString read Get_docId;
    property Name: WideString read Get_Name;
    property Company: WideString read Get_Company;
    property Fax: WideString read Get_Fax;
    property Status: _Status read Get_Status;
    property RemoteCSID: WideString read Get_RemoteCSID;
    property LastAttempt: TDateTime read Get_LastAttempt;
    property NextAttempt: TDateTime read Get_NextAttempt;
    property Duration: Single read Get_Duration;
    property Retries: Integer read Get_Retries;
    property BaudRate: Integer read Get_BaudRate;
    property PagesScheduled: Integer read Get_PagesScheduled;
    property PagesSent: Integer read Get_PagesSent;
  published
  end;

// *********************************************************************//
// The Class CoStatus provides a Create and CreateRemote method to          
// create instances of the default interface _Status exposed by              
// the CoClass Status. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStatus = class
    class function Create: _Status;
    class function CreateRemote(const MachineName: string): _Status;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TStatus
// Help String      : 
// Default Interface: _Status
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TStatus = class(TOleServer)
  private
    FIntf: _Status;
    function GetDefaultInterface: _Status;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
    function Get_Message: WideString;
    function Get_Classification: WideString;
    function Get_Outcome: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Status);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    property DefaultInterface: _Status read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
    property Message: WideString read Get_Message;
    property Classification: WideString read Get_Classification;
    property Outcome: WideString read Get_Outcome;
  published
  end;

// *********************************************************************//
// The Class CoXmlUtils provides a Create and CreateRemote method to          
// create instances of the default interface _XmlUtils exposed by              
// the CoClass XmlUtils. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoXmlUtils = class
    class function Create: _XmlUtils;
    class function CreateRemote(const MachineName: string): _XmlUtils;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TXmlUtils
// Help String      : 
// Default Interface: _XmlUtils
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TXmlUtils = class(TOleServer)
  private
    FIntf: _XmlUtils;
    function GetDefaultInterface: _XmlUtils;
  protected
    procedure InitServerData; override;
    function Get_ToString: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _XmlUtils);
    procedure Disconnect; override;
    function Equals(obj: OleVariant): WordBool;
    function GetHashCode: Integer;
    function GetType: _Type;
    function ExtractXMLFromURLEncodedData(const data: WideString): WideString;
    function ExtractXMLFromURLEncodedData_2(const stream: _Stream): WideString;
    property DefaultInterface: _XmlUtils read GetDefaultInterface;
    property ToString: WideString read Get_ToString;
  published
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses System.Win.ComObj;

class function CoeFaxDeveloperException.Create: _eFaxDeveloperException;
begin
  Result := CreateComObject(CLASS_eFaxDeveloperException) as _eFaxDeveloperException;
end;

class function CoeFaxDeveloperException.CreateRemote(const MachineName: string): _eFaxDeveloperException;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_eFaxDeveloperException) as _eFaxDeveloperException;
end;

procedure TeFaxDeveloperException.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{CB799347-9BDB-31B4-94B8-97CA72DD00DA}';
    IntfIID:   '{0E90D19E-C74D-3F3D-B6C0-429461B37613}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TeFaxDeveloperException.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _eFaxDeveloperException;
  end;
end;

procedure TeFaxDeveloperException.ConnectTo(svrIntf: _eFaxDeveloperException);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TeFaxDeveloperException.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TeFaxDeveloperException.GetDefaultInterface: _eFaxDeveloperException;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TeFaxDeveloperException.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TeFaxDeveloperException.Destroy;
begin
  inherited Destroy;
end;

class function CoAdditionalInfo.Create: _AdditionalInfo;
begin
  Result := CreateComObject(CLASS_AdditionalInfo) as _AdditionalInfo;
end;

class function CoAdditionalInfo.CreateRemote(const MachineName: string): _AdditionalInfo;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AdditionalInfo) as _AdditionalInfo;
end;

procedure TAdditionalInfo.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{AC496729-F924-38DA-A76F-BE9D8C46577F}';
    IntfIID:   '{F263B8D9-4FEB-3096-9FCA-7F021E5FAC2C}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TAdditionalInfo.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _AdditionalInfo;
  end;
end;

procedure TAdditionalInfo.ConnectTo(svrIntf: _AdditionalInfo);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TAdditionalInfo.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TAdditionalInfo.GetDefaultInterface: _AdditionalInfo;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TAdditionalInfo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TAdditionalInfo.Destroy;
begin
  inherited Destroy;
end;

function TAdditionalInfo.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TAdditionalInfo.Get_ReadSequence: Integer;
begin
  Result := DefaultInterface.ReadSequence;
end;

function TAdditionalInfo.Get_ReadDirection: ReadDirection;
begin
  Result := DefaultInterface.ReadDirection;
end;

function TAdditionalInfo.Get_Symbology: WideString;
begin
  Result := DefaultInterface.Symbology;
end;

function TAdditionalInfo.Get_CodeLocation: _CodeLocation;
begin
  Result := DefaultInterface.CodeLocation;
end;

function TAdditionalInfo.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TAdditionalInfo.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TAdditionalInfo.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoBarcodes.Create: _Barcodes;
begin
  Result := CreateComObject(CLASS_Barcodes) as _Barcodes;
end;

class function CoBarcodes.CreateRemote(const MachineName: string): _Barcodes;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Barcodes) as _Barcodes;
end;

procedure TBarcodes.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A8F4944D-75D2-33B1-881A-4746C6CA5DB8}';
    IntfIID:   '{0D77BC7A-0764-37CC-A1AC-A70F51A9FF92}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TBarcodes.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Barcodes;
  end;
end;

procedure TBarcodes.ConnectTo(svrIntf: _Barcodes);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TBarcodes.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TBarcodes.GetDefaultInterface: _Barcodes;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TBarcodes.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TBarcodes.Destroy;
begin
  inherited Destroy;
end;

function TBarcodes.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TBarcodes.Get_item(index: Integer): _Barcode;
begin
  Result := DefaultInterface.item[index];
end;

procedure TBarcodes._Set_item(index: Integer; const pRetVal: _Barcode);
begin
  DefaultInterface.item[index] := pRetVal;
end;

function TBarcodes.Get_Count: Integer;
begin
  Result := DefaultInterface.Count;
end;

function TBarcodes.Get_IsReadOnly: WordBool;
begin
  Result := DefaultInterface.IsReadOnly;
end;

function TBarcodes.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TBarcodes.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TBarcodes.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TBarcodes.IndexOf(const item: _Barcode): Integer;
begin
  Result := DefaultInterface.IndexOf(item);
end;

procedure TBarcodes.Insert(index: Integer; const item: _Barcode);
begin
  DefaultInterface.Insert(index, item);
end;

procedure TBarcodes.RemoveAt(index: Integer);
begin
  DefaultInterface.RemoveAt(index);
end;

procedure TBarcodes.Add(const item: _Barcode);
begin
  DefaultInterface.Add(item);
end;

procedure TBarcodes.Clear;
begin
  DefaultInterface.Clear;
end;

function TBarcodes.Contains(const item: _Barcode): WordBool;
begin
  Result := DefaultInterface.Contains(item);
end;

procedure TBarcodes.CopyTo(array_: PSafeArray; arrayIndex: Integer);
begin
  DefaultInterface.CopyTo(array_, arrayIndex);
end;

function TBarcodes.Remove(const item: _Barcode): WordBool;
begin
  Result := DefaultInterface.Remove(item);
end;

class function CoBarcode.Create: _Barcode;
begin
  Result := CreateComObject(CLASS_Barcode) as _Barcode;
end;

class function CoBarcode.CreateRemote(const MachineName: string): _Barcode;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Barcode) as _Barcode;
end;

procedure TBarcode.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{C197E02D-D99D-32F7-AD8E-47B84674261C}';
    IntfIID:   '{26C22F4C-09C6-3E52-9DC9-F4D3AD297460}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TBarcode.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Barcode;
  end;
end;

procedure TBarcode.ConnectTo(svrIntf: _Barcode);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TBarcode.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TBarcode.GetDefaultInterface: _Barcode;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TBarcode.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TBarcode.Destroy;
begin
  inherited Destroy;
end;

function TBarcode.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TBarcode.Get_Key: WideString;
begin
  Result := DefaultInterface.Key;
end;

function TBarcode.Get_AdditionalInfo: _AdditionalInfo;
begin
  Result := DefaultInterface.AdditionalInfo;
end;

function TBarcode.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TBarcode.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TBarcode.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoCodeLocation.Create: _CodeLocation;
begin
  Result := CreateComObject(CLASS_CodeLocation) as _CodeLocation;
end;

class function CoCodeLocation.CreateRemote(const MachineName: string): _CodeLocation;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CodeLocation) as _CodeLocation;
end;

procedure TCodeLocation.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{C97CF81C-6584-3CBB-BBAD-402573E4597B}';
    IntfIID:   '{53C261F9-131F-3C83-816B-5BEA55C60D8D}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCodeLocation.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _CodeLocation;
  end;
end;

procedure TCodeLocation.ConnectTo(svrIntf: _CodeLocation);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCodeLocation.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCodeLocation.GetDefaultInterface: _CodeLocation;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TCodeLocation.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TCodeLocation.Destroy;
begin
  inherited Destroy;
end;

function TCodeLocation.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TCodeLocation.Get_PageNumber: Integer;
begin
  Result := DefaultInterface.PageNumber;
end;

function TCodeLocation.Get_PageCoordinates: _PageCoordinates;
begin
  Result := DefaultInterface.PageCoordinates;
end;

function TCodeLocation.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TCodeLocation.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TCodeLocation.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoEndEdge.Create: _EndEdge;
begin
  Result := CreateComObject(CLASS_EndEdge) as _EndEdge;
end;

class function CoEndEdge.CreateRemote(const MachineName: string): _EndEdge;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_EndEdge) as _EndEdge;
end;

procedure TEndEdge.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{7F4666B4-1837-3740-8073-FEBBBB29C998}';
    IntfIID:   '{EB681918-BBE3-3186-8722-8F1680B092CE}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TEndEdge.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _EndEdge;
  end;
end;

procedure TEndEdge.ConnectTo(svrIntf: _EndEdge);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TEndEdge.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TEndEdge.GetDefaultInterface: _EndEdge;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TEndEdge.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TEndEdge.Destroy;
begin
  inherited Destroy;
end;

function TEndEdge.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TEndEdge.Get_XEndPointA: Single;
begin
  Result := DefaultInterface.XEndPointA;
end;

function TEndEdge.Get_YEndPointA: Single;
begin
  Result := DefaultInterface.YEndPointA;
end;

function TEndEdge.Get_XEndPointB: Single;
begin
  Result := DefaultInterface.XEndPointB;
end;

function TEndEdge.Get_YEndPointB: Single;
begin
  Result := DefaultInterface.YEndPointB;
end;

function TEndEdge.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TEndEdge.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TEndEdge.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoFaxControl.Create: _FaxControl;
begin
  Result := CreateComObject(CLASS_FaxControl) as _FaxControl;
end;

class function CoFaxControl.CreateRemote(const MachineName: string): _FaxControl;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FaxControl) as _FaxControl;
end;

procedure TFaxControl.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{9FE71BBF-06DE-3A0E-B48A-E9E9F1959654}';
    IntfIID:   '{460B7977-990A-3396-AEC6-4BB77CA65888}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFaxControl.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _FaxControl;
  end;
end;

procedure TFaxControl.ConnectTo(svrIntf: _FaxControl);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFaxControl.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFaxControl.GetDefaultInterface: _FaxControl;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TFaxControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFaxControl.Destroy;
begin
  inherited Destroy;
end;

function TFaxControl.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TFaxControl.Get_AccountID: WideString;
begin
  Result := DefaultInterface.AccountID;
end;

function TFaxControl.Get_NumberDialed: WideString;
begin
  Result := DefaultInterface.NumberDialed;
end;

function TFaxControl.Get_DateReceived: TDateTime;
begin
  Result := DefaultInterface.DateReceived;
end;

function TFaxControl.Get_FaxName: WideString;
begin
  Result := DefaultInterface.FaxName;
end;

function TFaxControl.Get_FileType: FaxFileType;
begin
  Result := DefaultInterface.FileType;
end;

function TFaxControl.Get_PageCount: Integer;
begin
  Result := DefaultInterface.PageCount;
end;

function TFaxControl.Get_CSID: WideString;
begin
  Result := DefaultInterface.CSID;
end;

function TFaxControl.Get_ANI: WideString;
begin
  Result := DefaultInterface.ANI;
end;

function TFaxControl.Get_Status: Integer;
begin
  Result := DefaultInterface.Status;
end;

function TFaxControl.Get_MCFID: Integer;
begin
  Result := DefaultInterface.MCFID;
end;

function TFaxControl.Get_UserFields: _UserFields;
begin
  Result := DefaultInterface.UserFields;
end;

function TFaxControl.Get_Barcodes: _Barcodes;
begin
  Result := DefaultInterface.Barcodes;
end;

function TFaxControl.Get_Pages: _Pages;
begin
  Result := DefaultInterface.Pages;
end;

function TFaxControl.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TFaxControl.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TFaxControl.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TFaxControl.GetFileContentsCOM: OleVariant;
begin
  Result := DefaultInterface.GetFileContentsCOM;
end;

class function CoInboundClient.Create: _InboundClient;
begin
  Result := CreateComObject(CLASS_InboundClient) as _InboundClient;
end;

class function CoInboundClient.CreateRemote(const MachineName: string): _InboundClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_InboundClient) as _InboundClient;
end;

procedure TInboundClient.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{8C5A7440-5459-339C-8BD3-DDACF93679E9}';
    IntfIID:   '{C6E353E6-F28E-3EAB-95C9-88C3C9CB0B4D}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TInboundClient.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _InboundClient;
  end;
end;

procedure TInboundClient.ConnectTo(svrIntf: _InboundClient);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TInboundClient.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TInboundClient.GetDefaultInterface: _InboundClient;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TInboundClient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TInboundClient.Destroy;
begin
  inherited Destroy;
end;

function TInboundClient.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TInboundClient.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TInboundClient.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TInboundClient.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TInboundClient.DeserializeInboundPostRequest(const xml: WideString): _InboundPostRequest;
begin
  Result := DefaultInterface.DeserializeInboundPostRequest(xml);
end;

function TInboundClient.DeserializeInboundPostRequest_2(const stream: _Stream): _InboundPostRequest;
begin
  Result := DefaultInterface.DeserializeInboundPostRequest_2(stream);
end;

class function CoInboundPostRequest.Create: _InboundPostRequest;
begin
  Result := CreateComObject(CLASS_InboundPostRequest) as _InboundPostRequest;
end;

class function CoInboundPostRequest.CreateRemote(const MachineName: string): _InboundPostRequest;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_InboundPostRequest) as _InboundPostRequest;
end;

procedure TInboundPostRequest.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{6814E5BF-A93C-316E-A6EA-E1051D2ED091}';
    IntfIID:   '{85A8DB5E-FC52-3CAB-8419-2BE73FC5C568}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TInboundPostRequest.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _InboundPostRequest;
  end;
end;

procedure TInboundPostRequest.ConnectTo(svrIntf: _InboundPostRequest);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TInboundPostRequest.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TInboundPostRequest.GetDefaultInterface: _InboundPostRequest;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TInboundPostRequest.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TInboundPostRequest.Destroy;
begin
  inherited Destroy;
end;

function TInboundPostRequest.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TInboundPostRequest.Get_FaxControl: _FaxControl;
begin
  Result := DefaultInterface.FaxControl;
end;

function TInboundPostRequest.Get_UserName: WideString;
begin
  Result := DefaultInterface.UserName;
end;

function TInboundPostRequest.Get_Password: WideString;
begin
  Result := DefaultInterface.Password;
end;

function TInboundPostRequest.Get_RequestDate: TDateTime;
begin
  Result := DefaultInterface.RequestDate;
end;

function TInboundPostRequest.Get_RequestType: RequestType;
begin
  Result := DefaultInterface.RequestType;
end;

function TInboundPostRequest.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TInboundPostRequest.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TInboundPostRequest.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoPages.Create: _Pages;
begin
  Result := CreateComObject(CLASS_Pages) as _Pages;
end;

class function CoPages.CreateRemote(const MachineName: string): _Pages;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Pages) as _Pages;
end;

procedure TPages.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E4F16925-A8CC-3F44-A9FE-9C84E7FF9E6E}';
    IntfIID:   '{478BFDC2-F758-37CE-B36D-3D860646B4F7}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TPages.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Pages;
  end;
end;

procedure TPages.ConnectTo(svrIntf: _Pages);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TPages.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TPages.GetDefaultInterface: _Pages;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TPages.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TPages.Destroy;
begin
  inherited Destroy;
end;

function TPages.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TPages.Get_item(index: Integer): _Page;
begin
  Result := DefaultInterface.item[index];
end;

procedure TPages._Set_item(index: Integer; const pRetVal: _Page);
begin
  DefaultInterface.item[index] := pRetVal;
end;

function TPages.Get_Count: Integer;
begin
  Result := DefaultInterface.Count;
end;

function TPages.Get_IsReadOnly: WordBool;
begin
  Result := DefaultInterface.IsReadOnly;
end;

function TPages.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TPages.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TPages.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TPages.IndexOf(const item: _Page): Integer;
begin
  Result := DefaultInterface.IndexOf(item);
end;

procedure TPages.Insert(index: Integer; const item: _Page);
begin
  DefaultInterface.Insert(index, item);
end;

procedure TPages.RemoveAt(index: Integer);
begin
  DefaultInterface.RemoveAt(index);
end;

procedure TPages.Add(const item: _Page);
begin
  DefaultInterface.Add(item);
end;

procedure TPages.Clear;
begin
  DefaultInterface.Clear;
end;

function TPages.Contains(const item: _Page): WordBool;
begin
  Result := DefaultInterface.Contains(item);
end;

procedure TPages.CopyTo(array_: PSafeArray; arrayIndex: Integer);
begin
  DefaultInterface.CopyTo(array_, arrayIndex);
end;

function TPages.Remove(const item: _Page): WordBool;
begin
  Result := DefaultInterface.Remove(item);
end;

class function CoPage.Create: _Page;
begin
  Result := CreateComObject(CLASS_Page) as _Page;
end;

class function CoPage.CreateRemote(const MachineName: string): _Page;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Page) as _Page;
end;

procedure TPage.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{399F4770-4F7B-3EA5-94D4-D2E1DF56BAC9}';
    IntfIID:   '{DF872539-0797-3D95-ACD6-71D129CD26B2}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TPage.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Page;
  end;
end;

procedure TPage.ConnectTo(svrIntf: _Page);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TPage.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TPage.GetDefaultInterface: _Page;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TPage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TPage.Destroy;
begin
  inherited Destroy;
end;

function TPage.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TPage.Get_PageNumber: Integer;
begin
  Result := DefaultInterface.PageNumber;
end;

function TPage.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TPage.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TPage.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TPage.GetPageContentsCOM: OleVariant;
begin
  Result := DefaultInterface.GetPageContentsCOM;
end;

class function CoPageCoordinates.Create: _PageCoordinates;
begin
  Result := CreateComObject(CLASS_PageCoordinates) as _PageCoordinates;
end;

class function CoPageCoordinates.CreateRemote(const MachineName: string): _PageCoordinates;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_PageCoordinates) as _PageCoordinates;
end;

procedure TPageCoordinates.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{AE9503EA-53DF-3781-87D0-6A7A6581F040}';
    IntfIID:   '{62D36E29-A706-3A1B-8960-8CAA8E2600B7}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TPageCoordinates.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _PageCoordinates;
  end;
end;

procedure TPageCoordinates.ConnectTo(svrIntf: _PageCoordinates);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TPageCoordinates.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TPageCoordinates.GetDefaultInterface: _PageCoordinates;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TPageCoordinates.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TPageCoordinates.Destroy;
begin
  inherited Destroy;
end;

function TPageCoordinates.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TPageCoordinates.Get_StartEdge: _StartEdge;
begin
  Result := DefaultInterface.StartEdge;
end;

function TPageCoordinates.Get_EndEdge: _EndEdge;
begin
  Result := DefaultInterface.EndEdge;
end;

function TPageCoordinates.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TPageCoordinates.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TPageCoordinates.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoStartEdge.Create: _StartEdge;
begin
  Result := CreateComObject(CLASS_StartEdge) as _StartEdge;
end;

class function CoStartEdge.CreateRemote(const MachineName: string): _StartEdge;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_StartEdge) as _StartEdge;
end;

procedure TStartEdge.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{BF1FBF7D-41F5-3390-AAFE-E3F454129BBA}';
    IntfIID:   '{E9F19E66-C82C-35EF-BCBB-9EF39EEC6899}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TStartEdge.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _StartEdge;
  end;
end;

procedure TStartEdge.ConnectTo(svrIntf: _StartEdge);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TStartEdge.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TStartEdge.GetDefaultInterface: _StartEdge;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TStartEdge.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TStartEdge.Destroy;
begin
  inherited Destroy;
end;

function TStartEdge.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TStartEdge.Get_XStartPointA: Single;
begin
  Result := DefaultInterface.XStartPointA;
end;

function TStartEdge.Get_YStartPointA: Single;
begin
  Result := DefaultInterface.YStartPointA;
end;

function TStartEdge.Get_XStartPointB: Single;
begin
  Result := DefaultInterface.XStartPointB;
end;

function TStartEdge.Get_YStartPointB: Single;
begin
  Result := DefaultInterface.YStartPointB;
end;

function TStartEdge.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TStartEdge.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TStartEdge.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoUserFields.Create: _UserFields;
begin
  Result := CreateComObject(CLASS_UserFields) as _UserFields;
end;

class function CoUserFields.CreateRemote(const MachineName: string): _UserFields;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UserFields) as _UserFields;
end;

procedure TUserFields.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{10B1EA38-E94E-3565-A57E-08E8EF04C4AA}';
    IntfIID:   '{C2C0489D-B095-3118-83B2-8AD371130E55}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TUserFields.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _UserFields;
  end;
end;

procedure TUserFields.ConnectTo(svrIntf: _UserFields);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TUserFields.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TUserFields.GetDefaultInterface: _UserFields;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TUserFields.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TUserFields.Destroy;
begin
  inherited Destroy;
end;

function TUserFields.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TUserFields.Get_item(index: Integer): _UserField;
begin
  Result := DefaultInterface.item[index];
end;

procedure TUserFields._Set_item(index: Integer; const pRetVal: _UserField);
begin
  DefaultInterface.item[index] := pRetVal;
end;

function TUserFields.Get_Count: Integer;
begin
  Result := DefaultInterface.Count;
end;

function TUserFields.Get_IsReadOnly: WordBool;
begin
  Result := DefaultInterface.IsReadOnly;
end;

function TUserFields.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TUserFields.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TUserFields.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TUserFields.IndexOf(const item: _UserField): Integer;
begin
  Result := DefaultInterface.IndexOf(item);
end;

procedure TUserFields.Insert(index: Integer; const item: _UserField);
begin
  DefaultInterface.Insert(index, item);
end;

procedure TUserFields.RemoveAt(index: Integer);
begin
  DefaultInterface.RemoveAt(index);
end;

procedure TUserFields.Add(const item: _UserField);
begin
  DefaultInterface.Add(item);
end;

procedure TUserFields.Clear;
begin
  DefaultInterface.Clear;
end;

function TUserFields.Contains(const item: _UserField): WordBool;
begin
  Result := DefaultInterface.Contains(item);
end;

procedure TUserFields.CopyTo(array_: PSafeArray; arrayIndex: Integer);
begin
  DefaultInterface.CopyTo(array_, arrayIndex);
end;

function TUserFields.Remove(const item: _UserField): WordBool;
begin
  Result := DefaultInterface.Remove(item);
end;

class function CoUserField.Create: _UserField;
begin
  Result := CreateComObject(CLASS_UserField) as _UserField;
end;

class function CoUserField.CreateRemote(const MachineName: string): _UserField;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UserField) as _UserField;
end;

procedure TUserField.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{9C7608DE-A1E5-3E2F-9B10-50D614DEA93C}';
    IntfIID:   '{FFF4AE8C-729E-3ED7-A9F0-7E07400D2C6D}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TUserField.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _UserField;
  end;
end;

procedure TUserField.ConnectTo(svrIntf: _UserField);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TUserField.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TUserField.GetDefaultInterface: _UserField;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TUserField.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TUserField.Destroy;
begin
  inherited Destroy;
end;

function TUserField.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TUserField.Get_FieldName: WideString;
begin
  Result := DefaultInterface.FieldName;
end;

procedure TUserField.Set_FieldName(const pRetVal: WideString);
begin
  DefaultInterface.FieldName := pRetVal;
end;

function TUserField.Get_FieldValue: WideString;
begin
  Result := DefaultInterface.FieldValue;
end;

procedure TUserField.Set_FieldValue(const pRetVal: WideString);
begin
  DefaultInterface.FieldValue := pRetVal;
end;

function TUserField.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TUserField.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TUserField.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoAccessControl.Create: _AccessControl;
begin
  Result := CreateComObject(CLASS_AccessControl) as _AccessControl;
end;

class function CoAccessControl.CreateRemote(const MachineName: string): _AccessControl;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AccessControl) as _AccessControl;
end;

procedure TAccessControl.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A98CBCEC-429D-3145-B20A-2CF705FF66D6}';
    IntfIID:   '{DD20B019-DC7F-3F62-846B-07AE45730708}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TAccessControl.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _AccessControl;
  end;
end;

procedure TAccessControl.ConnectTo(svrIntf: _AccessControl);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TAccessControl.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TAccessControl.GetDefaultInterface: _AccessControl;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TAccessControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TAccessControl.Destroy;
begin
  inherited Destroy;
end;

function TAccessControl.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TAccessControl.Get_UserName: WideString;
begin
  Result := DefaultInterface.UserName;
end;

function TAccessControl.Get_Password: WideString;
begin
  Result := DefaultInterface.Password;
end;

function TAccessControl.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TAccessControl.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TAccessControl.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoDispositionControl.Create: _DispositionControl;
begin
  Result := CreateComObject(CLASS_DispositionControl) as _DispositionControl;
end;

class function CoDispositionControl.CreateRemote(const MachineName: string): _DispositionControl;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DispositionControl) as _DispositionControl;
end;

procedure TDispositionControl.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{6C1FFD92-7D4C-32FF-B1F1-FA4EAE084EDF}';
    IntfIID:   '{E9FB3360-AB9F-34CC-8353-F256A04D7500}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TDispositionControl.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _DispositionControl;
  end;
end;

procedure TDispositionControl.ConnectTo(svrIntf: _DispositionControl);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TDispositionControl.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TDispositionControl.GetDefaultInterface: _DispositionControl;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TDispositionControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TDispositionControl.Destroy;
begin
  inherited Destroy;
end;

function TDispositionControl.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TDispositionControl.Get_DispositionURL: WideString;
begin
  Result := DefaultInterface.DispositionURL;
end;

procedure TDispositionControl.Set_DispositionURL(const pRetVal: WideString);
begin
  DefaultInterface.DispositionURL := pRetVal;
end;

function TDispositionControl.Get_DispositionLevel: DispositionLevel;
begin
  Result := DefaultInterface.DispositionLevel;
end;

procedure TDispositionControl.Set_DispositionLevel(pRetVal: DispositionLevel);
begin
  DefaultInterface.DispositionLevel := pRetVal;
end;

function TDispositionControl.Get_DispositionMethod: DispositionMethod;
begin
  Result := DefaultInterface.DispositionMethod;
end;

procedure TDispositionControl.Set_DispositionMethod(pRetVal: DispositionMethod);
begin
  DefaultInterface.DispositionMethod := pRetVal;
end;

function TDispositionControl.Get_DispositionEmails: _DispositionEmails;
begin
  Result := DefaultInterface.DispositionEmails;
end;

function TDispositionControl.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TDispositionControl.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TDispositionControl.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoDispositionEmail.Create: _DispositionEmail;
begin
  Result := CreateComObject(CLASS_DispositionEmail) as _DispositionEmail;
end;

class function CoDispositionEmail.CreateRemote(const MachineName: string): _DispositionEmail;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DispositionEmail) as _DispositionEmail;
end;

procedure TDispositionEmail.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A6077296-FEC5-30B8-8E4C-E1576FA47AA0}';
    IntfIID:   '{C31709AB-5C8B-3EF4-8C6E-562B34B39858}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TDispositionEmail.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _DispositionEmail;
  end;
end;

procedure TDispositionEmail.ConnectTo(svrIntf: _DispositionEmail);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TDispositionEmail.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TDispositionEmail.GetDefaultInterface: _DispositionEmail;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TDispositionEmail.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TDispositionEmail.Destroy;
begin
  inherited Destroy;
end;

function TDispositionEmail.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TDispositionEmail.Get_DispositionRecipient: WideString;
begin
  Result := DefaultInterface.DispositionRecipient;
end;

procedure TDispositionEmail.Set_DispositionRecipient(const pRetVal: WideString);
begin
  DefaultInterface.DispositionRecipient := pRetVal;
end;

function TDispositionEmail.Get_DispositionAddress: WideString;
begin
  Result := DefaultInterface.DispositionAddress;
end;

procedure TDispositionEmail.Set_DispositionAddress(const pRetVal: WideString);
begin
  DefaultInterface.DispositionAddress := pRetVal;
end;

function TDispositionEmail.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TDispositionEmail.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TDispositionEmail.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoDispositionEmails.Create: _DispositionEmails;
begin
  Result := CreateComObject(CLASS_DispositionEmails) as _DispositionEmails;
end;

class function CoDispositionEmails.CreateRemote(const MachineName: string): _DispositionEmails;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DispositionEmails) as _DispositionEmails;
end;

procedure TDispositionEmails.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{20A4BFC6-5488-3B22-8B40-20DD293BEADD}';
    IntfIID:   '{4804EF67-7FF8-3D18-BCD5-FCE7BA525B5A}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TDispositionEmails.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _DispositionEmails;
  end;
end;

procedure TDispositionEmails.ConnectTo(svrIntf: _DispositionEmails);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TDispositionEmails.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TDispositionEmails.GetDefaultInterface: _DispositionEmails;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TDispositionEmails.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TDispositionEmails.Destroy;
begin
  inherited Destroy;
end;

function TDispositionEmails.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TDispositionEmails.Get_item(index: Integer): _DispositionEmail;
begin
  Result := DefaultInterface.item[index];
end;

procedure TDispositionEmails._Set_item(index: Integer; const pRetVal: _DispositionEmail);
begin
  DefaultInterface.item[index] := pRetVal;
end;

function TDispositionEmails.Get_Count: Integer;
begin
  Result := DefaultInterface.Count;
end;

function TDispositionEmails.Get_IsReadOnly: WordBool;
begin
  Result := DefaultInterface.IsReadOnly;
end;

function TDispositionEmails.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TDispositionEmails.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TDispositionEmails.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TDispositionEmails.IndexOf(const item: _DispositionEmail): Integer;
begin
  Result := DefaultInterface.IndexOf(item);
end;

procedure TDispositionEmails.Insert(index: Integer; const item: _DispositionEmail);
begin
  DefaultInterface.Insert(index, item);
end;

procedure TDispositionEmails.RemoveAt(index: Integer);
begin
  DefaultInterface.RemoveAt(index);
end;

procedure TDispositionEmails.Add(const item: _DispositionEmail);
begin
  DefaultInterface.Add(item);
end;

procedure TDispositionEmails.Clear;
begin
  DefaultInterface.Clear;
end;

function TDispositionEmails.Contains(const item: _DispositionEmail): WordBool;
begin
  Result := DefaultInterface.Contains(item);
end;

procedure TDispositionEmails.CopyTo(array_: PSafeArray; arrayIndex: Integer);
begin
  DefaultInterface.CopyTo(array_, arrayIndex);
end;

function TDispositionEmails.Remove(const item: _DispositionEmail): WordBool;
begin
  Result := DefaultInterface.Remove(item);
end;

class function CoFaxFiles.Create: _FaxFiles;
begin
  Result := CreateComObject(CLASS_FaxFiles) as _FaxFiles;
end;

class function CoFaxFiles.CreateRemote(const MachineName: string): _FaxFiles;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FaxFiles) as _FaxFiles;
end;

procedure TFaxFiles.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{9CC81C16-D001-39A7-9A36-D88660B5E4FD}';
    IntfIID:   '{E5677D34-DBAE-3C6A-858E-ABE38B8658C5}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFaxFiles.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _FaxFiles;
  end;
end;

procedure TFaxFiles.ConnectTo(svrIntf: _FaxFiles);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFaxFiles.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFaxFiles.GetDefaultInterface: _FaxFiles;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TFaxFiles.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFaxFiles.Destroy;
begin
  inherited Destroy;
end;

function TFaxFiles.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TFaxFiles.Get_item(index: Integer): _FaxFile;
begin
  Result := DefaultInterface.item[index];
end;

procedure TFaxFiles._Set_item(index: Integer; const pRetVal: _FaxFile);
begin
  DefaultInterface.item[index] := pRetVal;
end;

function TFaxFiles.Get_Count: Integer;
begin
  Result := DefaultInterface.Count;
end;

function TFaxFiles.Get_IsReadOnly: WordBool;
begin
  Result := DefaultInterface.IsReadOnly;
end;

function TFaxFiles.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TFaxFiles.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TFaxFiles.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TFaxFiles.IndexOf(const item: _FaxFile): Integer;
begin
  Result := DefaultInterface.IndexOf(item);
end;

procedure TFaxFiles.Insert(index: Integer; const item: _FaxFile);
begin
  DefaultInterface.Insert(index, item);
end;

procedure TFaxFiles.RemoveAt(index: Integer);
begin
  DefaultInterface.RemoveAt(index);
end;

procedure TFaxFiles.Add(const item: _FaxFile);
begin
  DefaultInterface.Add(item);
end;

procedure TFaxFiles.Clear;
begin
  DefaultInterface.Clear;
end;

function TFaxFiles.Contains(const item: _FaxFile): WordBool;
begin
  Result := DefaultInterface.Contains(item);
end;

procedure TFaxFiles.CopyTo(array_: PSafeArray; arrayIndex: Integer);
begin
  DefaultInterface.CopyTo(array_, arrayIndex);
end;

function TFaxFiles.Remove(const item: _FaxFile): WordBool;
begin
  Result := DefaultInterface.Remove(item);
end;

class function CoFaxFile.Create: _FaxFile;
begin
  Result := CreateComObject(CLASS_FaxFile) as _FaxFile;
end;

class function CoFaxFile.CreateRemote(const MachineName: string): _FaxFile;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_FaxFile) as _FaxFile;
end;

procedure TFaxFile.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{F4EC8457-5481-3013-A806-5F8B509DFD58}';
    IntfIID:   '{05100B08-5E11-3FA5-8C1B-D0E93D972DD3}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TFaxFile.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _FaxFile;
  end;
end;

procedure TFaxFile.ConnectTo(svrIntf: _FaxFile);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TFaxFile.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TFaxFile.GetDefaultInterface: _FaxFile;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TFaxFile.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFaxFile.Destroy;
begin
  inherited Destroy;
end;

function TFaxFile.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TFaxFile.Get_FileType: FaxFileType;
begin
  Result := DefaultInterface.FileType;
end;

procedure TFaxFile.Set_FileType(pRetVal: FaxFileType);
begin
  DefaultInterface.FileType := pRetVal;
end;

function TFaxFile.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TFaxFile.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TFaxFile.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TFaxFile.GetFileContentsCOM: OleVariant;
begin
  Result := DefaultInterface.GetFileContentsCOM;
end;

procedure TFaxFile.SetFileContentsCOM(data: OleVariant);
begin
  DefaultInterface.SetFileContentsCOM(data);
end;

class function CoOutboundClient.Create: _OutboundClient;
begin
  Result := CreateComObject(CLASS_OutboundClient) as _OutboundClient;
end;

class function CoOutboundClient.CreateRemote(const MachineName: string): _OutboundClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundClient) as _OutboundClient;
end;

procedure TOutboundClient.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{04C63A38-C876-3F5B-BDBA-C31903E6BDF8}';
    IntfIID:   '{451CAF73-04BD-3F45-BE38-5E92C77F5BB0}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundClient.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundClient;
  end;
end;

procedure TOutboundClient.ConnectTo(svrIntf: _OutboundClient);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundClient.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundClient.GetDefaultInterface: _OutboundClient;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundClient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundClient.Destroy;
begin
  inherited Destroy;
end;

function TOutboundClient.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TOutboundClient.Get_UserName: WideString;
begin
  Result := DefaultInterface.UserName;
end;

procedure TOutboundClient.Set_UserName(const pRetVal: WideString);
begin
  DefaultInterface.UserName := pRetVal;
end;

function TOutboundClient.Get_Password: WideString;
begin
  Result := DefaultInterface.Password;
end;

procedure TOutboundClient.Set_Password(const pRetVal: WideString);
begin
  DefaultInterface.Password := pRetVal;
end;

function TOutboundClient.Get_AccountID: WideString;
begin
  Result := DefaultInterface.AccountID;
end;

procedure TOutboundClient.Set_AccountID(const pRetVal: WideString);
begin
  DefaultInterface.AccountID := pRetVal;
end;

function TOutboundClient.Get_ServiceUrl: WideString;
begin
  Result := DefaultInterface.ServiceUrl;
end;

procedure TOutboundClient.Set_ServiceUrl(const pRetVal: WideString);
begin
  DefaultInterface.ServiceUrl := pRetVal;
end;

function TOutboundClient.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TOutboundClient.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TOutboundClient.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

procedure TOutboundClient.LoadWebConfiguration;
begin
  DefaultInterface.LoadWebConfiguration;
end;

procedure TOutboundClient.SaveWebConfiguration(protect: WordBool);
begin
  DefaultInterface.SaveWebConfiguration(protect);
end;

procedure TOutboundClient.LoadConfiguration;
begin
  DefaultInterface.LoadConfiguration;
end;

procedure TOutboundClient.SaveConfiguration(protect: WordBool);
begin
  DefaultInterface.SaveConfiguration(protect);
end;

function TOutboundClient.DeserializeOutboundDisposition(const stream: _Stream): _OutboundDisposition;
begin
  Result := DefaultInterface.DeserializeOutboundDisposition(stream);
end;

function TOutboundClient.DeserializeOutboundDisposition_2(const xml: WideString): _OutboundDisposition;
begin
  Result := DefaultInterface.DeserializeOutboundDisposition_2(xml);
end;

function TOutboundClient.SendOutboundRequest(const OutboundRequest: _OutboundRequest): _OutboundResponse;
begin
  Result := DefaultInterface.SendOutboundRequest(OutboundRequest);
end;

function TOutboundClient.SendOutboundStatusRequest(const transmissionId: WideString; 
                                                   const docId: WideString): _OutboundStatusResponse;
begin
  Result := DefaultInterface.SendOutboundStatusRequest(transmissionId, docId);
end;

class function CoOutboundDisposition.Create: _OutboundDisposition;
begin
  Result := CreateComObject(CLASS_OutboundDisposition) as _OutboundDisposition;
end;

class function CoOutboundDisposition.CreateRemote(const MachineName: string): _OutboundDisposition;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundDisposition) as _OutboundDisposition;
end;

procedure TOutboundDisposition.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{C2EA4DC0-5674-30A7-A141-712D3CB88DD6}';
    IntfIID:   '{20FAC6F9-CC6A-38C3-A752-8346ED83DDA2}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundDisposition.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundDisposition;
  end;
end;

procedure TOutboundDisposition.ConnectTo(svrIntf: _OutboundDisposition);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundDisposition.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundDisposition.GetDefaultInterface: _OutboundDisposition;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundDisposition.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundDisposition.Destroy;
begin
  inherited Destroy;
end;

function TOutboundDisposition.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TOutboundDisposition.Get_UserName: WideString;
begin
  Result := DefaultInterface.UserName;
end;

function TOutboundDisposition.Get_Password: WideString;
begin
  Result := DefaultInterface.Password;
end;

function TOutboundDisposition.Get_transmissionId: WideString;
begin
  Result := DefaultInterface.transmissionId;
end;

function TOutboundDisposition.Get_docId: WideString;
begin
  Result := DefaultInterface.docId;
end;

function TOutboundDisposition.Get_FaxNumber: WideString;
begin
  Result := DefaultInterface.FaxNumber;
end;

function TOutboundDisposition.Get_RecipientCSID: WideString;
begin
  Result := DefaultInterface.RecipientCSID;
end;

function TOutboundDisposition.Get_CompletionDate: TDateTime;
begin
  Result := DefaultInterface.CompletionDate;
end;

function TOutboundDisposition.Get_FaxStatus: Integer;
begin
  Result := DefaultInterface.FaxStatus;
end;

function TOutboundDisposition.Get_Duration: Single;
begin
  Result := DefaultInterface.Duration;
end;

function TOutboundDisposition.Get_PagesSent: Integer;
begin
  Result := DefaultInterface.PagesSent;
end;

function TOutboundDisposition.Get_NumberOfRetries: Integer;
begin
  Result := DefaultInterface.NumberOfRetries;
end;

function TOutboundDisposition.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TOutboundDisposition.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TOutboundDisposition.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoOutboundDispositionInternal.Create: _OutboundDispositionInternal;
begin
  Result := CreateComObject(CLASS_OutboundDispositionInternal) as _OutboundDispositionInternal;
end;

class function CoOutboundDispositionInternal.CreateRemote(const MachineName: string): _OutboundDispositionInternal;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundDispositionInternal) as _OutboundDispositionInternal;
end;

procedure TOutboundDispositionInternal.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{4874738B-7202-321A-BC72-CC79A89A6647}';
    IntfIID:   '{6D8ECE03-22EB-3430-AEE3-FDE2CC379F7E}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundDispositionInternal.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundDispositionInternal;
  end;
end;

procedure TOutboundDispositionInternal.ConnectTo(svrIntf: _OutboundDispositionInternal);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundDispositionInternal.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundDispositionInternal.GetDefaultInterface: _OutboundDispositionInternal;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundDispositionInternal.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundDispositionInternal.Destroy;
begin
  inherited Destroy;
end;

class function CoOutboundRequest.Create: _OutboundRequest;
begin
  Result := CreateComObject(CLASS_OutboundRequest) as _OutboundRequest;
end;

class function CoOutboundRequest.CreateRemote(const MachineName: string): _OutboundRequest;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundRequest) as _OutboundRequest;
end;

procedure TOutboundRequest.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A2FC4529-8B99-38B9-A01A-1001EB3ECB63}';
    IntfIID:   '{1AC40DFF-B4FD-3E1B-84CA-F119F9FE8CFC}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundRequest.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundRequest;
  end;
end;

procedure TOutboundRequest.ConnectTo(svrIntf: _OutboundRequest);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundRequest.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundRequest.GetDefaultInterface: _OutboundRequest;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundRequest.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundRequest.Destroy;
begin
  inherited Destroy;
end;

function TOutboundRequest.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TOutboundRequest.Get_TransmissionControl: _RequestTransmissionControl;
begin
  Result := DefaultInterface.TransmissionControl;
end;

function TOutboundRequest.Get_DispositionControl: _DispositionControl;
begin
  Result := DefaultInterface.DispositionControl;
end;

function TOutboundRequest.Get_Recipient: _Recipient;
begin
  Result := DefaultInterface.Recipient;
end;

function TOutboundRequest.Get_Files: _FaxFiles;
begin
  Result := DefaultInterface.Files;
end;

function TOutboundRequest.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TOutboundRequest.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TOutboundRequest.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoOutboundResponse.Create: _OutboundResponse;
begin
  Result := CreateComObject(CLASS_OutboundResponse) as _OutboundResponse;
end;

class function CoOutboundResponse.CreateRemote(const MachineName: string): _OutboundResponse;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundResponse) as _OutboundResponse;
end;

procedure TOutboundResponse.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{18578943-BD82-33D4-AA9D-418E7D7079BF}';
    IntfIID:   '{5765572A-9D6D-3F77-A0EA-8672CB6EF53E}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundResponse.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundResponse;
  end;
end;

procedure TOutboundResponse.ConnectTo(svrIntf: _OutboundResponse);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundResponse.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundResponse.GetDefaultInterface: _OutboundResponse;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundResponse.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundResponse.Destroy;
begin
  inherited Destroy;
end;

function TOutboundResponse.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TOutboundResponse.Get_transmissionId: WideString;
begin
  Result := DefaultInterface.transmissionId;
end;

function TOutboundResponse.Get_docId: WideString;
begin
  Result := DefaultInterface.docId;
end;

function TOutboundResponse.Get_StatusCode: StatusCode;
begin
  Result := DefaultInterface.StatusCode;
end;

function TOutboundResponse.Get_StatusDescription: WideString;
begin
  Result := DefaultInterface.StatusDescription;
end;

function TOutboundResponse.Get_ErrorLevel: ErrorLevel;
begin
  Result := DefaultInterface.ErrorLevel;
end;

function TOutboundResponse.Get_ErrorMessage: WideString;
begin
  Result := DefaultInterface.ErrorMessage;
end;

function TOutboundResponse.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TOutboundResponse.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TOutboundResponse.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoOutboundStatusResponse.Create: _OutboundStatusResponse;
begin
  Result := CreateComObject(CLASS_OutboundStatusResponse) as _OutboundStatusResponse;
end;

class function CoOutboundStatusResponse.CreateRemote(const MachineName: string): _OutboundStatusResponse;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OutboundStatusResponse) as _OutboundStatusResponse;
end;

procedure TOutboundStatusResponse.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{5CD22B8F-A7AD-3BD5-AC1B-5651239DFD2E}';
    IntfIID:   '{F9E2D341-2115-3E42-ADF5-A7B58D2E8F07}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOutboundStatusResponse.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _OutboundStatusResponse;
  end;
end;

procedure TOutboundStatusResponse.ConnectTo(svrIntf: _OutboundStatusResponse);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOutboundStatusResponse.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOutboundStatusResponse.GetDefaultInterface: _OutboundStatusResponse;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TOutboundStatusResponse.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TOutboundStatusResponse.Destroy;
begin
  inherited Destroy;
end;

function TOutboundStatusResponse.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TOutboundStatusResponse.Get_transmissionId: WideString;
begin
  Result := DefaultInterface.transmissionId;
end;

function TOutboundStatusResponse.Get_CustomerID: WideString;
begin
  Result := DefaultInterface.CustomerID;
end;

function TOutboundStatusResponse.Get_Recipient: _StatusResponseRecipient;
begin
  Result := DefaultInterface.Recipient;
end;

function TOutboundStatusResponse.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TOutboundStatusResponse.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TOutboundStatusResponse.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoRecipient.Create: _Recipient;
begin
  Result := CreateComObject(CLASS_Recipient) as _Recipient;
end;

class function CoRecipient.CreateRemote(const MachineName: string): _Recipient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Recipient) as _Recipient;
end;

procedure TRecipient.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{826F4AF9-EC9F-37D1-81FB-59BEB909AA20}';
    IntfIID:   '{91AE3E60-EF43-3547-87D6-E6BC79245862}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TRecipient.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Recipient;
  end;
end;

procedure TRecipient.ConnectTo(svrIntf: _Recipient);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TRecipient.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TRecipient.GetDefaultInterface: _Recipient;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TRecipient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TRecipient.Destroy;
begin
  inherited Destroy;
end;

function TRecipient.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TRecipient.Get_RecipientName: WideString;
begin
  Result := DefaultInterface.RecipientName;
end;

procedure TRecipient.Set_RecipientName(const pRetVal: WideString);
begin
  DefaultInterface.RecipientName := pRetVal;
end;

function TRecipient.Get_RecipientCompany: WideString;
begin
  Result := DefaultInterface.RecipientCompany;
end;

procedure TRecipient.Set_RecipientCompany(const pRetVal: WideString);
begin
  DefaultInterface.RecipientCompany := pRetVal;
end;

function TRecipient.Get_RecipientFax: WideString;
begin
  Result := DefaultInterface.RecipientFax;
end;

procedure TRecipient.Set_RecipientFax(const pRetVal: WideString);
begin
  DefaultInterface.RecipientFax := pRetVal;
end;

function TRecipient.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TRecipient.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TRecipient.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoRequestTransmissionControl.Create: _RequestTransmissionControl;
begin
  Result := CreateComObject(CLASS_RequestTransmissionControl) as _RequestTransmissionControl;
end;

class function CoRequestTransmissionControl.CreateRemote(const MachineName: string): _RequestTransmissionControl;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RequestTransmissionControl) as _RequestTransmissionControl;
end;

procedure TRequestTransmissionControl.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{F25ED8FE-69EC-3B70-A1FA-76E190B7C056}';
    IntfIID:   '{526EA2AA-D269-3E5C-96EF-C6C432FD41CB}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TRequestTransmissionControl.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _RequestTransmissionControl;
  end;
end;

procedure TRequestTransmissionControl.ConnectTo(svrIntf: _RequestTransmissionControl);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TRequestTransmissionControl.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TRequestTransmissionControl.GetDefaultInterface: _RequestTransmissionControl;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TRequestTransmissionControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TRequestTransmissionControl.Destroy;
begin
  inherited Destroy;
end;

function TRequestTransmissionControl.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TRequestTransmissionControl.Get_transmissionId: WideString;
begin
  Result := DefaultInterface.transmissionId;
end;

procedure TRequestTransmissionControl.Set_transmissionId(const pRetVal: WideString);
begin
  DefaultInterface.transmissionId := pRetVal;
end;

function TRequestTransmissionControl.Get_CustomerID: WideString;
begin
  Result := DefaultInterface.CustomerID;
end;

procedure TRequestTransmissionControl.Set_CustomerID(const pRetVal: WideString);
begin
  DefaultInterface.CustomerID := pRetVal;
end;

function TRequestTransmissionControl.Get_NoDuplicates: NoDuplicates;
begin
  Result := DefaultInterface.NoDuplicates;
end;

procedure TRequestTransmissionControl.Set_NoDuplicates(pRetVal: NoDuplicates);
begin
  DefaultInterface.NoDuplicates := pRetVal;
end;

function TRequestTransmissionControl.Get_Resolution: Resolution;
begin
  Result := DefaultInterface.Resolution;
end;

procedure TRequestTransmissionControl.Set_Resolution(pRetVal: Resolution);
begin
  DefaultInterface.Resolution := pRetVal;
end;

function TRequestTransmissionControl.Get_Priority: Priority;
begin
  Result := DefaultInterface.Priority;
end;

procedure TRequestTransmissionControl.Set_Priority(pRetVal: Priority);
begin
  DefaultInterface.Priority := pRetVal;
end;

function TRequestTransmissionControl.Get_SelfBusy: SelfBusy;
begin
  Result := DefaultInterface.SelfBusy;
end;

procedure TRequestTransmissionControl.Set_SelfBusy(pRetVal: SelfBusy);
begin
  DefaultInterface.SelfBusy := pRetVal;
end;

function TRequestTransmissionControl.Get_FaxHeader: WideString;
begin
  Result := DefaultInterface.FaxHeader;
end;

procedure TRequestTransmissionControl.Set_FaxHeader(const pRetVal: WideString);
begin
  DefaultInterface.FaxHeader := pRetVal;
end;

function TRequestTransmissionControl.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TRequestTransmissionControl.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TRequestTransmissionControl.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoStatusResponseRecipient.Create: _StatusResponseRecipient;
begin
  Result := CreateComObject(CLASS_StatusResponseRecipient) as _StatusResponseRecipient;
end;

class function CoStatusResponseRecipient.CreateRemote(const MachineName: string): _StatusResponseRecipient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_StatusResponseRecipient) as _StatusResponseRecipient;
end;

procedure TStatusResponseRecipient.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{876C2B0F-5DE3-3B9B-8BE0-60647B223F1D}';
    IntfIID:   '{70A838A9-9847-39B1-9FAC-10CADAC025D1}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TStatusResponseRecipient.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _StatusResponseRecipient;
  end;
end;

procedure TStatusResponseRecipient.ConnectTo(svrIntf: _StatusResponseRecipient);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TStatusResponseRecipient.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TStatusResponseRecipient.GetDefaultInterface: _StatusResponseRecipient;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TStatusResponseRecipient.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TStatusResponseRecipient.Destroy;
begin
  inherited Destroy;
end;

function TStatusResponseRecipient.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TStatusResponseRecipient.Get_docId: WideString;
begin
  Result := DefaultInterface.docId;
end;

function TStatusResponseRecipient.Get_Name: WideString;
begin
  Result := DefaultInterface.Name;
end;

function TStatusResponseRecipient.Get_Company: WideString;
begin
  Result := DefaultInterface.Company;
end;

function TStatusResponseRecipient.Get_Fax: WideString;
begin
  Result := DefaultInterface.Fax;
end;

function TStatusResponseRecipient.Get_Status: _Status;
begin
  Result := DefaultInterface.Status;
end;

function TStatusResponseRecipient.Get_RemoteCSID: WideString;
begin
  Result := DefaultInterface.RemoteCSID;
end;

function TStatusResponseRecipient.Get_LastAttempt: TDateTime;
begin
  Result := DefaultInterface.LastAttempt;
end;

function TStatusResponseRecipient.Get_NextAttempt: TDateTime;
begin
  Result := DefaultInterface.NextAttempt;
end;

function TStatusResponseRecipient.Get_Duration: Single;
begin
  Result := DefaultInterface.Duration;
end;

function TStatusResponseRecipient.Get_Retries: Integer;
begin
  Result := DefaultInterface.Retries;
end;

function TStatusResponseRecipient.Get_BaudRate: Integer;
begin
  Result := DefaultInterface.BaudRate;
end;

function TStatusResponseRecipient.Get_PagesScheduled: Integer;
begin
  Result := DefaultInterface.PagesScheduled;
end;

function TStatusResponseRecipient.Get_PagesSent: Integer;
begin
  Result := DefaultInterface.PagesSent;
end;

function TStatusResponseRecipient.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TStatusResponseRecipient.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TStatusResponseRecipient.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoStatus.Create: _Status;
begin
  Result := CreateComObject(CLASS_Status) as _Status;
end;

class function CoStatus.CreateRemote(const MachineName: string): _Status;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Status) as _Status;
end;

procedure TStatus.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{74C7C8F3-B947-3A1E-B07C-1B97304DFB2E}';
    IntfIID:   '{1ACD9FAB-E245-3C68-A298-CB721C4F9400}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TStatus.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Status;
  end;
end;

procedure TStatus.ConnectTo(svrIntf: _Status);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TStatus.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TStatus.GetDefaultInterface: _Status;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TStatus.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TStatus.Destroy;
begin
  inherited Destroy;
end;

function TStatus.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TStatus.Get_Message: WideString;
begin
  Result := DefaultInterface.Message;
end;

function TStatus.Get_Classification: WideString;
begin
  Result := DefaultInterface.Classification;
end;

function TStatus.Get_Outcome: WideString;
begin
  Result := DefaultInterface.Outcome;
end;

function TStatus.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TStatus.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TStatus.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

class function CoXmlUtils.Create: _XmlUtils;
begin
  Result := CreateComObject(CLASS_XmlUtils) as _XmlUtils;
end;

class function CoXmlUtils.CreateRemote(const MachineName: string): _XmlUtils;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_XmlUtils) as _XmlUtils;
end;

procedure TXmlUtils.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{39A72A5E-7607-3982-895D-F910E2E6A453}';
    IntfIID:   '{A903F01C-AC4C-3AA5-86F5-C70D386C7E50}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TXmlUtils.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _XmlUtils;
  end;
end;

procedure TXmlUtils.ConnectTo(svrIntf: _XmlUtils);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TXmlUtils.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TXmlUtils.GetDefaultInterface: _XmlUtils;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TXmlUtils.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TXmlUtils.Destroy;
begin
  inherited Destroy;
end;

function TXmlUtils.Get_ToString: WideString;
begin
  Result := DefaultInterface.ToString;
end;

function TXmlUtils.Equals(obj: OleVariant): WordBool;
begin
  Result := DefaultInterface.Equals(obj);
end;

function TXmlUtils.GetHashCode: Integer;
begin
  Result := DefaultInterface.GetHashCode;
end;

function TXmlUtils.GetType: _Type;
begin
  Result := DefaultInterface.GetType;
end;

function TXmlUtils.ExtractXMLFromURLEncodedData(const data: WideString): WideString;
begin
  Result := DefaultInterface.ExtractXMLFromURLEncodedData(data);
end;

function TXmlUtils.ExtractXMLFromURLEncodedData_2(const stream: _Stream): WideString;
begin
  Result := DefaultInterface.ExtractXMLFromURLEncodedData_2(stream);
end;

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TeFaxDeveloperException, TAdditionalInfo, TBarcodes, TBarcode, 
    TCodeLocation, TEndEdge, TFaxControl, TInboundClient, TInboundPostRequest, 
    TPages, TPage, TPageCoordinates, TStartEdge, TUserFields, 
    TUserField, TAccessControl, TDispositionControl, TDispositionEmail, TDispositionEmails, 
    TFaxFiles, TFaxFile, TOutboundClient, TOutboundDisposition, TOutboundDispositionInternal, 
    TOutboundRequest, TOutboundResponse, TOutboundStatusResponse, TRecipient, TRequestTransmissionControl, 
    TStatusResponseRecipient, TStatus, TXmlUtils]);
end;

end.
