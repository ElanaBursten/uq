unit OdExceptions;

interface

uses SysUtils;

type
  EOdException = class(Exception)
  protected
    FLog: Boolean;
    class function LogByDefault: Boolean; virtual;
  public
    class function NewInstance: TObject; override;
    constructor CreateLogged(const AMessage: string; ALog: Boolean = True);
    property Log: Boolean read FLog write FLog;
  end;

  EOdDatabaseException = class(EOdException);

  EOdReferenceException = class(EOdDatabaseException);
  EOdBadGetReference = class(EOdReferenceException);
  EOdBadSetReference = class(EOdReferenceException);
  EOdReferenceMissing = class(EOdReferenceException);
  EODConfigError = class(EOdException);

  EOdNonLoggedException = class(EOdException)
  protected
    class function LogByDefault: Boolean; override;
  end;

  EOdEntryRequired = class(EOdNonLoggedException);
  EOdDataEntryError = class(EOdNonLoggedException);
  EOdNotAllowed = class(EOdNonLoggedException);
  ExqScalerOffline =  class(EOdNonLoggedException);

  EOdCriteriaRequiredException = class(EOdEntryRequired);

  EOdNetworkException = class(EOdException);
  EOdCorruptFile = class(EOdException);
  EOdSyncError = class(EOdException);
  EOdMutlipleDatabaseSetting = class(EOdException);
  EOdNoDatabaseSetting = class(EOdException);

implementation

{ EOdException }

constructor EOdException.CreateLogged(const AMessage: string; ALog: Boolean);
begin
  inherited Create(AMessage);
  FLog := ALog;
end;

class function EOdException.LogByDefault: Boolean;
begin
  Result := True;
end;

// There is probably a better way to ensure this defaults to True
// no matter what constructor was called, but this works for now
class function EOdException.NewInstance: TObject;
begin
  Result := inherited NewInstance;
  (Result as EOdException).FLog := LogByDefault;
end;

{ EOdNonLoggedException }

class function EOdNonLoggedException.LogByDefault: Boolean;
begin
  Result := False;
end;

end.
