program PALprj;
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}
uses
  Vcl.Forms,
  PAL in 'PAL.pas' {frmPal};

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Payroll Auto Output';
  Application.CreateForm(TfrmPal, frmPal);
  Application.Run;
end.
