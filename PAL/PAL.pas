﻿unit PAL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxCore, cxDateUtils, System.Actions, Vcl.ActnList, Data.Win.ADODB, Vcl.Buttons,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, Vcl.StdCtrls;

type
  TfrmPal = class(TForm)
    StatusBar1: TStatusBar;
    DBGrid1: TDBGrid;
    lblWeekEnding: TLabel;
    btnSave: TButton;
    btnOpen: TButton;
    cxEndDate: TcxDateEdit;
    BitBtn1: TBitBtn;
    ADOOpen: TADOQuery;
    FileADOConnection: TADOConnection;
    dsADOOpen: TDataSource;
    OpenDialog1: TOpenDialog;
    qryPeriod: TADOQuery;
    PeriodConn: TADOConnection;
    ckBxTimesheetsNotApproved: TCheckBox;
    qryTimesheetsNotApproved: TADOQuery;
    ADOConn: TADOConnection;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSaveClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure cxEndDatePropertiesChange(Sender: TObject);
  private
    EndDate : TDate;
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    fConnectedDB: string;
    fUserName: string;
    procedure SaveFile;
    function connectDB: boolean;
    { Private declarations }
  public
    { Public declarations }
    function OpenMyFile: String;
  end;

var
  frmPal: TfrmPal;

implementation
uses StrUtils;
{$R *.dfm}


procedure TfrmPal.SaveFile;
const
  HEADER='BATCH,,H,,,,,,,,,,,';
  DET = 'DETAIL';
  EXT='.csv';
  PAY_TYPE = 'REG';
  CAR_TYPE ='CARTF';
  CAR_ALL_TYPE = 'CARALL';
  HOL_TYPE = 'HOL';
  FHL_TYPE = 'FHL';
  BER_TYPE = 'BEV';
  PTO_TYPE = 'PTO';
  SEP1 = ',';
  SEP2 = ',,';
  SEP3 = ',,,';
  SEP4 = ',,,,';
  CARAL = '3';
  TEN0 ='0000000000';
  NO = 'No';
  DP = 'DycomPeriod\DycomPeriod.xlsx';
var
  Connstr: String;
  lineText, OutputFile: string;
  lineText2, OutputFile2: string;    //QM-48
  myFile: TextFile;
  myFile2: TextFile;   //QM-48
  Period: string;
  RefNbr: string;
  sCARAL: string;
  Center, SupNo, EmpNo, Role, PayGrp, RegHours, CarHours, PtoHours, HolHours, FhlHours, BerHours, TaskID: string;
  sEndDate: string;
  path: string;
  cnt, ii: integer;
  function ExtractNumberInString ( sChaine: String ): String ;
  var
      i: Integer ;
  begin
      Result := '' ;
      for i := 1 to length( sChaine ) do
      begin
          if sChaine[ i ] in ['0'..'9'] then
          Result := Result + sChaine[ i ] ;
      end ;
  end ;
begin
  Period := '';
  cnt := 0;
  if DayOfWeek(EndDate)<> 7 then
  begin
    showMessage('Week ending date must be a Saturday.');
    exit;
  end;
  path := IncludeTrailingbackSlash(GetCurrentDir);
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source='+path+DP;
  PeriodConn.ConnectionString := Connstr;
  qryPeriod.Connection := PeriodConn;
  qryPeriod.Connection.Open;
  qryPeriod.Parameters.ParamByName('enddate').Value := EndDate;
  qryPeriod.Parameters.ParamByName('enddate1').Value := EndDate;
  qryPeriod.Open;
  Period := ExtractNumberInString(qryPeriod.FieldByName('period').AsString);
  if Period = '' then
  begin
    showmessage('No Dycom period for the End Data');
    exit;
  end;

  btnSave.Enabled := false;
  RefNbr := Period;
  sEndDate :=  formatdatetime('mm/dd/yyyy',EndDate);
  OutputFile := IncludeTrailingbackSlash(GetCurrentDir)+formatdatetime('mm-dd-yyyy',EndDate)+'_PalExportFile'+EXT;
  AssignFile(myFile, OutputFile);
  ReWrite(myFile);
  if ckBxTimesheetsNotApproved.Checked then
  begin
    OutputFile2 := IncludeTrailingbackSlash(GetCurrentDir) + formatdatetime('mm-dd-yyyy', EndDate) + '_TimeSheetsNotApproved' + EXT;  //QM-48
    AssignFile(myFile2, OutputFile2);  //QM-48
    ReWrite(myFile2);  //QM-48

    qryTimesheetsNotApproved.close;  //QM-48
    qryTimesheetsNotApproved.Parameters.ParamByName('enddate').Value := EndDate;  //QM-48
    qryTimesheetsNotApproved.open;  //QM-48
    ii:=0;
    lineText2 :=  qryTimesheetsNotApproved.fields[0].DisplayName+SEP1+   //QM-48
                  qryTimesheetsNotApproved.fields[1].DisplayName+SEP1+
                  qryTimesheetsNotApproved.fields[2].DisplayName+SEP1+
                  qryTimesheetsNotApproved.fields[3].DisplayName+SEP1+
                  qryTimesheetsNotApproved.fields[4].DisplayName;
    WriteLn(myFile2, lineText2);
    while not qryTimesheetsNotApproved.eof do     //QM-48
    begin
      lineText2:=
                  qryTimesheetsNotApproved.fields.Fields[0].AsString+SEP1+
                  qryTimesheetsNotApproved.fields.Fields[1].AsString+SEP1+
                  qryTimesheetsNotApproved.fields.Fields[2].AsString+SEP1+
                  qryTimesheetsNotApproved.fields.Fields[3].AsString+SEP1+
                  qryTimesheetsNotApproved.fields.Fields[4].AsString;
      WriteLn(myFile2, lineText2);
      qryTimesheetsNotApproved.Next;
    end;
  End;

  try
      try
        WriteLn(myFile, HEADER);
        while not ADOOpen.EOF do
        begin
          with ADOOpen  do
          begin
            if trim(FieldByName('Center').AsString) = 'Center' then Next;
            if trim(FieldByName('Center').AsString) = '' then
              break;
            lineText := '';
            Center := FieldByName('Center').AsString;
            SupNo := FieldByName('Supervisor').AsString;
            EmpNo := FieldByName('Employee File Number').AsString;
            Role:= FieldByName('Role').AsString;
            PayGrp := FieldByName('Pay Group').AsString;
            RegHours := FieldByName('Reg').AsString;
            PtoHours := FieldByName('PTO').AsString;
            CarHours := FieldByName('CAR').AsString;
            sCARAL   := FieldByName('CARAL').AsString;
            HolHours := FieldByName('HOL').AsString;
            FhlHours := FieldByName('FHL').AsString;
            BerHours := FieldByName('Ber').AsString;
            TaskID := Center+SupNo+EmpNo;

            lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+PAY_TYPE+Role+SEP1+RegHours+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
            WriteLn(myFile, lineText);

            if trim(CarHours) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+CAR_TYPE+SEP1+CarHours+SEP1+CARAL+SEP3+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

            if trim(sCARAL) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+CAR_ALL_TYPE+SEP1+sCARAL+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

            if trim(PtoHours) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+PTO_TYPE+SEP1+PtoHours+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

            if trim(HolHours) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+HOL_TYPE+Role+SEP1+HolHours+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

            if trim(FhlHours) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+FHL_TYPE+Role+SEP1+FhlHours+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

            if trim(BerHours) <> '' then
            begin
              lineText := DET+SEP1+RefNbr+SEP1+sEndDate+SEP1+EmpNo+SEP1+Center+SEP1+BER_TYPE+Role+SEP1+BerHours+SEP4+TEN0+SEP1+TaskID+SEP2+NO;
              WriteLn(myFile, lineText);
            end;

          end;
          inc(cnt);
          ADOOpen.Next;
        end;

      except
        raise;
      end;
      ShowMessage(IntToStr(Cnt)+' records were exported to '+ OutputFile);
  finally
      CloseFile(myFile);
      CloseFile(myFile2);  //QM-48
      btnSave.Enabled := False;
      qryPeriod.Close;
      ADOOpen.Close;
      qryTimesheetsNotApproved.close; //QM-48
  end;

end;


procedure TfrmPal.btnOpenClick(Sender: TObject);
var
  Rslt: String;
begin
  Rslt := OpenMyFile;
  if Rslt <> '' then
    ShowMessage('connection to file error: ' + Rslt);
end;

procedure TfrmPal.btnSaveClick(Sender: TObject);
begin
  if ADOOpen.Active then SaveFile;
end;

procedure TfrmPal.cxEndDatePropertiesChange(Sender: TObject);
begin
  EndDate := cxEndDate.Date;
  if DayOfWeek(EndDate)<> 7  then  showMessage('Week ending date must be a Saturday.');
end;

procedure TfrmPal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ADOOpen.Close;
  FileADOConnection.Close;
  qryPeriod.Close;
  PeriodConn.Close;
end;

procedure TfrmPal.FormCreate(Sender: TObject);
  function GetAppVersionStr: string;
  var
    Exe: string;
    Size, Handle: DWORD;
    Buffer: TBytes;
    FixedPtr: PVSFixedFileInfo;
  begin
    Exe := ParamStr(0);
    Size := GetFileVersionInfoSize(PChar(Exe), Handle);
    if Size = 0 then
      RaiseLastOSError;
    SetLength(Buffer, Size);
    if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
      RaiseLastOSError;
    if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
      RaiseLastOSError;
    Result := Format('%d.%d.%d.%d',
      [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
       LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
       LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
       LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
  end;
Begin
  StatusBar1.Panels[1].Text :=  GetAppVersionStr;
  FileADOConnection.Close;
  connectDB;
end;

function TfrmPal.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;

  function EnDeCrypt(const Value: String): String;
  var
    CharIndex : integer;
  begin
    Result := Value;
    for CharIndex := 1 to Length(Value) do
      Result[CharIndex] := chr(not(ord(Value[CharIndex])));
  end;

begin
  ADOConn.Close;
  Result := false;

  aServer:= 'SSDS-UTQ-QM-02';
  apassword := 'DEC_ﾖﾗﾕﾖﾛﾐﾕﾐￌ';
  aTrusted := '1';
  aDatabase := 'QM';
  ausername := 'uqweb';
  try

    LogConnStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
  finally
     Result := ADOConn.Connected;
  end;
end;



function TfrmPal.OpenMyFile: String;
var
 Connstr: String;
begin
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  Result := '';
  openDialog1.Filter := 'Excel files|*.xlsx';
  if OpenDialog1.Execute then
  begin
     ADOOpen.Close;
     FileADOConnection.Close;
     Connstr := Connstr + OpenDialog1.FileName;
     try
       FileADOConnection.ConnectionString := Connstr;
       FileADOConnection.Open;
       ADOOpen.Open;
     except
       On E:Exception do
         Result := E.Message;
     end;
     BtnSave.Enabled := True;
  end;
end;

end.

