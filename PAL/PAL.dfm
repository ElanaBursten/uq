object frmPal: TfrmPal
  Left = 0
  Top = 0
  Caption = '      Payroll Auto Export'
  ClientHeight = 565
  ClientWidth = 1089
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblWeekEnding: TLabel
    Left = 408
    Top = 414
    Width = 62
    Height = 13
    Caption = 'Week Ending'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 546
    Width = 1089
    Height = 19
    Panels = <
      item
        Text = '     Version'
        Width = 70
      end
      item
        Width = 50
      end>
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 1089
    Height = 393
    Align = alTop
    DataSource = dsADOOpen
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Center'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Supervisor'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Employee File Number'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EMPL ID'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Role'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pay Group'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Last Name'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'First Name'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Reg'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PTO '
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HOL'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FHL'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ber'
        Width = 20
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CAR'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CARAL'
        Width = 40
        Visible = True
      end>
  end
  object btnSave: TButton
    Left = 528
    Top = 431
    Width = 64
    Height = 25
    Caption = 'Export'
    TabOrder = 2
    OnClick = btnSaveClick
  end
  object btnOpen: TButton
    Left = 289
    Top = 431
    Width = 64
    Height = 25
    Caption = 'Open File'
    TabOrder = 3
    OnClick = btnOpenClick
  end
  object cxEndDate: TcxDateEdit
    Left = 385
    Top = 433
    Properties.SaveTime = False
    Properties.ShowTime = False
    Properties.OnChange = cxEndDatePropertiesChange
    TabOrder = 4
    Width = 112
  end
  object BitBtn1: TBitBtn
    Left = 808
    Top = 431
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 5
  end
  object ckBxTimesheetsNotApproved: TCheckBox
    Left = 528
    Top = 472
    Width = 193
    Height = 17
    Caption = 'Include Timesheets Not Approved'
    Checked = True
    State = cbChecked
    TabOrder = 6
  end
  object ADOOpen: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM [Main$]')
    Left = 120
    Top = 169
  end
  object FileADOConnection: TADOConnection
    KeepConnection = False
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 184
    Top = 432
  end
  object dsADOOpen: TDataSource
    AutoEdit = False
    DataSet = ADOOpen
    Left = 32
    Top = 472
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'xlsx'
    FileName = 'SAL-CORP batch.xlsx'
    Left = 133
    Top = 394
  end
  object qryPeriod: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'EndDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'EndDate1'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT period '
      'FROM [DycomPeriod$]'
      'where  :EndDate > starting '
      'and :EndDate1 <= ending'
      '')
    Left = 664
    Top = 417
  end
  object PeriodConn: TADOConnection
    KeepConnection = False
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 736
    Top = 448
  end
  object qryTimesheetsNotApproved: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'EndDate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'use qm'
      ' Declare @StartDate  DateTime, @EndDate  DateTime;'
      ' Set @EndDate = :EndDate;'
      ' Set @StartDate = DateAdd(DD, -6, @EndDate)'
      ''
      
        ' select e.short_name,status,work_date,approve_date,final_approve' +
        '_date'
      ' from timesheet_entry te'
      ' join employee e on e.emp_id = te.work_emp_id'
      ' where work_date between @StartDate and @EndDate'
      ' and te.status = '#39'ACTIVE'#39
      ' and approve_date is null')
    Left = 424
    Top = 464
  end
  object ADOConn: TADOConnection
    CommandTimeout = 300
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 288
    Top = 472
  end
end
