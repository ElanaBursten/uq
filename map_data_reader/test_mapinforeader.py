# test_mapinforeader.py

import os
import string
import unittest
import mapinforeader

class TestMapInfoReader(unittest.TestCase):

    # This test case uses an existing AtlantaGrid.dat. If it's not there, it
    # will generate it. (Generating the .dat file takes a while, and it's
    # probably not a good idea to do that every time we run the test...)

    def _read_first_record(self):
        if not os.path.exists("AtlantaGrid.dat"):
            print "Generating AtlantaGrid.dat first."
            mir = mapinforeader.MapInfoReader("AtlantaGrid")
        f = open("AtlantaGrid.dat", "r")
        line = string.strip(f.readline())
        f.close()
        return line

    def test_firstrecord(self):
        x = self._read_first_record()
        parts = string.split(x, ",")
        self.assertEquals(len(parts), 8)
        self.assertEquals(parts[0], "GA1")
        self.assertEquals(parts[1], "1")
        self.assertEquals(parts[2], "A")
        self.assertEquals(parts[3], "1")
        self.assertEquals(parts[4], "34.564948")
        self.assertEquals(parts[5], "-85.112534")
        self.assertEquals(parts[6], "34.572202")
        self.assertEquals(parts[7], "-85.103792")


if __name__ == "__main__":

    unittest.main()

