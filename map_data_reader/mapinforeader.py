# mapinforeader.py
# Created: 24 Feb 2002, Hans Nowak

import re
import string
import sys

rMID = re.compile('(".*?"|[^",]+)(,|$)')
rLETNUM = re.compile("^([a-zA-Z]*)([0-9]*)$")
rNUMLET = re.compile("^([0-9]*)([a-zA-Z]*)$")

__usage__ = """\
mapinforeader.py basefilename
Collects data from MapInfo .MID and .MIF files, producing a .DAT file.

Expects: basefilename.mid
         basefilename.mif
Output:  basefilename.dat
"""

class MapInfoError(Exception): pass

class MIDFileReader:
    """ Process MapInfo .MID files.

        Processing instructions (field count starting at 1):
        - Ignore fields 1, 2 and 3.
        - Store field 4 in output file.
        - Store field 5 in output file.
        - Store field 6 in output file; letters and numbers need to be
          separated.
    """

    def __init__(self):
        self.data = []

    def read(self, filename):
        print "Reading %s..." % (filename),
        f = open(filename, "r")
        for line in f.xreadlines():
            line = string.strip(line)
            z = self._parse(line)
            self.data.append(z)
        f.close()
        print "OK"
        print len(self.data), "records read"

    def _parse(self, line):
        parts = rMID.findall(line)
        assert len(parts) == 6, "Invalid number of fields: %s" % (parts)
        parts = [t[0] for t in parts]
        result = []
        if parts[3].startswith('"'):
            parts[3] = parts[3][1:-1]
        if parts[4].startswith('"'):
            parts[4] = parts[4][1:-1]
        if parts[5].startswith('"'):
            parts[5] = parts[5][1:-1]
            m = rLETNUM.search(parts[5])
            if m:
                letters, numbers = m.group(1), m.group(2)
            else:
                m = rNUMLET.search(parts[5])
                if m:
                    # actually reversed, not a problem
                    letters, numbers = m.group(1), m.group(2)
                else:
                    raise MapInfoError, "Invalid field: %s" % (parts[5])
        result = [parts[3], parts[4], letters, numbers]
        return result


class MIFFileReader:
    """ Processes a .MIF file.

        Processing instructions:
        - Skip everything until the first "Region" is encountered. (A line
          starting with "Region").
        - For each region, only consider the lines containing two
          (longitude/latitude) coordinates. Store this information. Ignore
          everything else.
        - A new region starts whenever a line starts with "Region".
        - Upon completion of a region, compute the minimum and maximum
          longitude and latitude, resulting in four values. Store these
          values.
    """

    def __init__(self):
        self.data = []

    def read(self, filename):
        print "Reading %s..." % (filename),
        f = open(filename, "r")

        # skip lines until first "Region" occurrance
        line = f.readline()
        while not line.startswith("Region"):
            line = f.readline()
        line = f.readline() # read line after Region
        longitudes = []
        latitudes = []

        while line:
            if line.startswith("Region"):
                # Aha, a new region starts here
                # wrap up the region info we have so far and create a new one
                z = self.getminmax(longitudes, latitudes)
                self.data.append(z)
                longitudes = []
                latitudes = []
            else:
                # continue with current region
                if line.startswith(" "):    # ignore lines with indentation
                    line = f.readline()
                    continue
                if not string.strip(line):  # ignore empty lines
                    line = f.readline()
                    continue
                # assume that we have two floats
                try:
                    long, lat = map(float, string.split(line)[:2])
                    longitudes.append(long)
                    latitudes.append(lat)
                except:
                    print >> sys.stderr, "Invalid line:", line
                    raise

            # eventually...
            line = f.readline()

        # after an EOF, there might be an unprocessed region:
        if longitudes or latitudes:
            z = self.getminmax(longitudes, latitudes)
            self.data.append(z)

        f.close()
        print "OK"
        print len(self.data), "records read"

    def getminmax(self, longitudes, latitudes):
        minlong = min(longitudes)
        maxlong = max(longitudes)
        minlat = min(latitudes)
        maxlat = max(latitudes)
        return minlat, minlong, maxlat, maxlong

    def _verify(self, filename):
        """ Throw-away method that can be used to check if the correct
            number of regions has been read. It does so simply by counting the
            lines that start with "Region".
            If errors occur, this method may be useful for debugging.
        """
        count = 0
        f = open(filename, "r")
        for line in f.xreadlines():
            if line.startswith("Region"):
                count = count + 1
        f.close()
        return count


class MapInfoReader:
    """ Processes data from .MID and .MIF files, through the aforementioned
        classes. After processing, these objects should have lists of data
        with equal lengths. After this, as many records as possible are
        written to an output file (a "record" consisting of data collected
        by the MID- and MIFFileReaders).
    """

    def __init__(self, basefilename):
        self.basefilename = basefilename
        self.mif = MIFFileReader()
        self.mid = MIDFileReader()

    def read(self, midfile="", miffile=""):
        midfile = midfile or self.basefilename + ".MID"
        miffile = miffile or self.basefilename + ".MIF"
        self.mif.read(miffile)
        self.mid.read(midfile)

    def check(self, fatal=1):
        try:
            assert len(self.mif.data) == len(self.mid.data), \
             "MID data and MIF data have different lengths (%d vs %d)" % (
             len(self.mid.data), len(self.mif.data))
        except AssertionError, e:
            if fatal:
                raise
            else:
                print >> sys.stderr, e.args[0]

    def write(self, filename=""):
        filename = filename or self.basefilename + ".dat"
        print "Writing output file %s..." % (filename),
        f = open(filename, "w")
        # process as many records as possible
        for i in range(self._maxrecords()):
            middata, mifdata = self.mid.data[i], self.mif.data[i]
            s = "%s,%s,%s,%s,%.6f,%.6f,%.6f,%.6f\n" % (
             middata[0], middata[1], middata[2], middata[3],
             mifdata[0], mifdata[1], mifdata[2], mifdata[3])
            f.write(s)
        f.close()
        print "OK"
        # print message if the list lengths differed
        self.check(fatal=0)

    def _maxrecords(self):
        """ Determines the maximum number of records that can possibly be
            written. """
        return min(len(self.mif.data), len(self.mid.data))


if __name__ == "__main__":

    try:
        basefilename = sys.argv[1]
    except IndexError:
        sys.stderr.write(__usage__)
        sys.exit(1)

    mir = MapInfoReader(basefilename)
    mir.read()
    mir.write()

