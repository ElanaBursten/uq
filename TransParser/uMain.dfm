object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 324
  ClientWidth = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -6
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 7
  object Splitter1: TSplitter
    Left = 0
    Top = 111
    Width = 860
    Height = 6
    Cursor = crVSplit
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alTop
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 860
    Height = 111
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 270
      Top = 26
      Width = 1
      Height = 69
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      OnMoved = Splitter2Moved
    end
    object ListBox1: TListBox
      Left = 1
      Top = 26
      Width = 269
      Height = 69
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alLeft
      TabOrder = 0
      OnClick = ListBox1Click
    end
    object ListBox2: TListBox
      Left = 271
      Top = 26
      Width = 588
      Height = 69
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alClient
      TabOrder = 1
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 858
      Height = 25
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label7: TLabel
        Left = 7
        Top = 3
        Width = 20
        Height = 16
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Dir '
      end
      object Label8: TLabel
        Left = 678
        Top = 4
        Width = 53
        Height = 16
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'File Mask'
      end
      object Label9: TLabel
        Left = 393
        Top = 5
        Width = 84
        Height = 16
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Modified after:'
      end
      object edtFileMask: TEdit
        Left = 732
        Top = 1
        Width = 126
        Height = 24
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 0
        Text = '*-Q-trans-detail.txt'
      end
      object edtDirMask: TEdit
        Left = 309
        Top = 1
        Width = 73
        Height = 24
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 1
        Text = '*-California'
      end
      object dpFilterDate: TDateTimePicker
        Left = 482
        Top = 1
        Width = 100
        Height = 24
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Date = 41913.000000000000000000
        Time = 0.488646678240911600
        ShowCheckbox = True
        TabOrder = 2
      end
      object cbNotInDB: TCheckBox
        Left = 590
        Top = 5
        Width = 73
        Height = 14
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Not in DB'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
      object edtPath: TEdit
        Left = 32
        Top = 1
        Width = 273
        Height = 24
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        ReadOnly = True
        TabOrder = 4
        Text = 'edtPath'
      end
    end
    object StatusBar2: TStatusBar
      Left = 1
      Top = 95
      Width = 858
      Height = 15
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Panels = <
        item
          Text = 'Count'
          Width = 208
        end
        item
          Text = 'Count'
          Width = 40
        end>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 117
    Width = 860
    Height = 162
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label10: TLabel
      Left = 83
      Top = 64
      Width = 336
      Height = 15
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Alignment = taCenter
      AutoSize = False
    end
    object Label11: TLabel
      Left = 591
      Top = 14
      Width = 72
      Height = 16
      Caption = 'Delete Bill ID'
    end
    object btnListFiles: TButton
      Left = 310
      Top = 14
      Width = 102
      Height = 20
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Run Selected Billing'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -10
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = btnListFilesClick
    end
    object btnRun: TButton
      Left = 197
      Top = 14
      Width = 74
      Height = 20
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Run It on All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -10
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btnRunClick
    end
    object btnDirList: TButton
      Left = 14
      Top = 14
      Width = 53
      Height = 20
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'List Dir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -10
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnDirListClick
    end
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 151
      Width = 858
      Height = 10
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alBottom
      TabOrder = 3
    end
    object Memo1: TMemo
      Left = 1
      Top = 54
      Width = 858
      Height = 97
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alBottom
      ScrollBars = ssBoth
      TabOrder = 4
    end
    object cbDelBillid: TComboBox
      Left = 679
      Top = 11
      Width = 106
      Height = 24
      AutoDropDown = True
      TabOrder = 5
      OnSelect = cbDelBillidSelect
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 279
    Width = 860
    Height = 45
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label1: TLabel
      Left = 1
      Top = 10
      Width = 56
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'File name'
    end
    object Label2: TLabel
      Left = 326
      Top = 10
      Width = 25
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Rec '
    end
    object Label4: TLabel
      Left = 412
      Top = 10
      Width = 31
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Begin'
    end
    object Label5: TLabel
      Left = 482
      Top = 10
      Width = 54
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Complete'
    end
    object Label6: TLabel
      Left = 555
      Top = 10
      Width = 97
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Total Time (min)'
    end
    object Label3: TLabel
      Left = 362
      Top = 10
      Width = 33
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Count'
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 29
      Width = 858
      Height = 15
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Panels = <
        item
          Text = 'File Name'
          Width = 320
        end
        item
          Text = 'record'
          Width = 32
        end
        item
          Text = 'Count'
          Width = 40
        end
        item
          Text = 'Time Start'
          Width = 80
        end
        item
          Text = 'Time Stop'
          Width = 80
        end
        item
          Text = 'Total Time'
          Width = 112
        end
        item
          Text = ' Version'
          Width = 56
        end
        item
          Width = 40
        end>
      ParentFont = True
      UseSystemFont = False
    end
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 440
    Top = 184
  end
  object insData: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'bill_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'termcode'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'call_center'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticket_number'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ticket_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'con_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'work_city'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'transmit_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'work_county'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'work_address_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'work_address_number_2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'work_address_street'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'work_cross'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'map_page'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'rate'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'locate_closed'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'plat'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'revision'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ''
      'INSERT INTO [dbo].[billing_trans_detail]'
      '           ([bill_id]'
      '           ,[termcode]'
      '           ,[ticket_id]'
      '           ,[call_center]'
      '           ,[ticket_number]'
      '           ,[ticket_type]'
      '           ,[con_name]'
      '           ,[work_city]'
      '           ,[transmit_date]'
      '           ,[work_county]'
      '           ,[work_address_number]'
      '           ,[work_address_number_2]'
      '           ,[work_address_street]'
      '           ,[work_cross]'
      '           ,[map_page]'
      '           ,[rate]'
      '           ,[locate_closed]'
      '           ,[plat]'
      '           ,[revision])'
      '     VALUES'
      '           (:bill_id'
      '           ,:termcode'
      '           ,:ticket_id'
      '           ,:call_center'
      '           ,:ticket_number'
      '           ,:ticket_type'
      '           ,:con_name'
      '           ,:work_city'
      '           ,:transmit_date'
      '           ,:work_county'
      '           ,:work_address_number'
      '           ,:work_address_number_2'
      '           ,:work_address_street'
      '           ,:work_cross'
      '           ,:map_page'
      '           ,:rate'
      '           ,:locate_closed'
      '           ,:plat'
      '           ,:revision'
      ''
      #9#9'   )')
    Left = 320
    Top = 192
  end
  object qryConfigData: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'SELECT value'
      '  FROM [QM].[dbo].[configuration_data]'
      '  where name = '#39'BillingConfigRootDir'#39)
    Left = 384
    Top = 196
  end
  object qryTrannyBillIDs: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct top 10 '
      'bill_id'
      'from [dbo].[billing_trans_detail]'
      'order by bill_id desc')
    Left = 448
    Top = 125
  end
  object qryCountBillID: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'BillID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select count(bill_id) HowMany'
      'from [dbo].[billing_trans_detail]'
      'where bill_id = :BillID')
    Left = 552
    Top = 125
  end
  object delTrannyBillID: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'BillID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'delete'
      'from [dbo].[billing_trans_detail]'
      'where bill_id = :BillID')
    Left = 560
    Top = 197
  end
end
