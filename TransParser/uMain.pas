unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, System.RegularExpressions,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, Vcl.ComCtrls,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdSMTPBase, IdSMTP, IdBaseComponent, IdMessage, Vcl.Buttons, Vcl.DBCtrls;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[40];
    ExcepMsg: String[255];
    DataStream: String[255];
  end;

type
  TForm1 = class(TForm)
    btnListFiles: TButton;
    btnRun: TButton;
    ADOConn: TADOConnection;
    insData: TADOQuery;
    Panel1: TPanel;
    Panel2: TPanel;
    ListBox1: TListBox;
    Splitter1: TSplitter;
    btnDirList: TButton;
    ListBox2: TListBox;
    Panel3: TPanel;
    edtFileMask: TEdit;
    edtDirMask: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Splitter2: TSplitter;
    StatusBar2: TStatusBar;
    dpFilterDate: TDateTimePicker;
    Label9: TLabel;
    Panel4: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    cbNotInDB: TCheckBox;
    ProgressBar1: TProgressBar;
    qryConfigData: TADOQuery;
    Label10: TLabel;
    Memo1: TMemo;
    edtPath: TEdit;
    Label11: TLabel;
    qryTrannyBillIDs: TADOQuery;
    cbDelBillid: TComboBox;
    qryCountBillID: TADOQuery;
    delTrannyBillID: TADOQuery;   //qm-910
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDirListClick(Sender: TObject);
    procedure Splitter2Moved(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure btnListFilesClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure btnAbortClick(Sender: TObject);
    procedure cbDelBillidSelect(Sender: TObject);
  private
    LogResult: TLogResults;
    fconfigXML: tFilename;
    fadoDatabase: wideString;
    fadoPassword: wideString;
    fadoLogin: wideString;
    fadoHost: wideString;
    flogDir: wideString;
    fTotalizer: integer;
    fBillDir: String;
    BillingID: integer;

    procedure FileSearch(const PathName, FileName: String);
    procedure ProcessFile(SelectedFile: tFilename);
    procedure RunProcess(Selected: boolean = false);
    procedure ProcessFileList;
    function ReportFileTimes(const FileName: String): TDateTime;
    procedure clearLogRecord;
    function ConfigDB: boolean;

    function LoadParams: boolean;
    function ParseXML(xmlFile: tFilename): boolean;
    procedure GetConFigData;
    procedure FileSearch2(const PathName, FileName: String);
    function EnDeCrypt(const Value: String): String;
    function connectDB: boolean;
    procedure PopulateBillIDs;

    { Private declarations }
  public
    { Public declarations }
    StartTime, EndTime: TDateTime;
    fileData: TStrings;
    procedure GetSubDirectories(const directory: String; list: TStrings);
    procedure WriteLog(LogResult: TLogResults);

    property adoHost: wideString read fadoHost;
    property adoDatabase: wideString read fadoDatabase;
    property adoLogin: wideString read fadoLogin;
    property adoPassword: wideString read fadoPassword;

    property logDir: wideString read flogDir;

    property configXML: tFilename read fconfigXML;

  end;

function ExtractNumbers(const s: String): TArray<String>;

var
  Form1: TForm1;
  UseGUI : boolean;
  bAbort : boolean;
const
  ADOCONN_NOAUTH_STR =
  'Provider=SQLOLEDB.1; ' +
  'Password=%s; ' +
  'Persist Security Info=False; ' +
  'User ID=%s;  ' +
  'Initial Catalog=%s; ' +
  'Data Source=%s;  ' +
  'Use Procedure for Prepare=1;  ' +
  'Auto Translate=True; ' +
  'Packet Size=4096;';

implementation

uses dateutils, StrUtils, MSXML, System.IniFiles, odMiscUtils;
{$R *.dfm}

procedure TForm1.btnAbortClick(Sender: TObject);
begin
  bAbort := true;
end;

procedure TForm1.btnDirListClick(Sender: TObject);
const
  // DIR_ = '\\utl000fs001\departments$\billing\UTLBE';
  DIR_ = 'C:\UTLBE';
begin
  ListBox1.clear;
{$IFDEF Debug}
  GetSubDirectories(DIR_, ListBox1.Items);
{$ELSE}
  GetSubDirectories(fBillDir, ListBox1.Items);
{$ENDIF};

  StatusBar2.Panels[0].Text := IntToStr(ListBox1.Count);
  StatusBar2.Panels[1].Text := '';
end;

procedure TForm1.ProcessFileList;
var
  sFile: String;
  i: integer;
begin
  StartTime := Time;
  StatusBar1.Panels[3].Text := TimeToStr(TimeOf(StartTime));
  for i := 0 to ListBox2.Count - 1 do
  begin
    sFile := ListBox2.Items.Strings[i];
    ListBox2.ItemIndex := i;
    StatusBar1.Panels[0].Text := ExtractFileName(sFile);
    ProcessFile(sFile);

    LogResult.MethodName := 'ProcessFile';
    LogResult.DataStream := 'Processed Completed for ' + ExtractFileName(sFile);
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);

  end;
  EndTime := Time;
  StatusBar1.Panels[4].Text := TimeToStr(TimeOf(EndTime));
  StatusBar1.Panels[5].Text := IntToStr(MinutesBetween(StartTime, EndTime));
  ListBox2.clear;
end;

procedure TForm1.RunProcess(Selected: boolean);
var
  i: integer;
  transFileMask: String;
begin
  transFileMask := edtFileMask.Text;

  if not Selected then
  begin
    for i := 0 to ListBox1.Count - 1 do
    begin
      FileSearch2(ListBox1.Items.Strings[i], transFileMask);
      ListBox1.ItemIndex := i;
      if NOT useGUI  then
      ListBox1Click(nil);
      ProcessFileList;
    end;
    if i = 0 then
    begin
      LogResult.MethodName := 'ProcessFile';
      LogResult.Status := 'Nothing to process.  Exiting!!';
      Label10.Caption :=  'Nothing to process.  Exiting!!';
      LogResult.LogType := ltNotice;
      WriteLog(LogResult);
      if NOT useGUI  then
      application.Terminate;
      exit;
    end;
  end
  else
  begin
    ProcessFileList;
  end;

  LogResult.MethodName := 'Processed ' + IntToStr(fTotalizer) + ' Files';
  LogResult.Status := 'Process completed';
  Label10.Caption :=  'Process completed';
  ProgressBar1.Visible := false;
  LogResult.LogType := ltInfo;
  WriteLog(LogResult);
  if not useGui then
    application.Terminate;
end;

procedure TForm1.Splitter2Moved(Sender: TObject);
begin
  StatusBar2.Panels[0].Width := (Sender as TSplitter).Left;
end;

procedure TForm1.btnListFilesClick(Sender: TObject);
begin
  RunProcess(true);
  ProgressBar1.Visible := True;
end;

procedure TForm1.btnRunClick(Sender: TObject);
begin
  RunProcess;
end;

procedure TForm1.FileSearch(const PathName, FileName: String);
var
  Rec: TSearchRec;
  Path: String;
begin
  self.Cursor := crHourGlass;
  Path := IncludeTrailingBackslash(PathName);
  if FindFirst(Path + FileName, faAnyFile - faDirectory, Rec) = 0 then
    try
      repeat
        ListBox2.Items.Add(Path + Rec.Name);
      until FindNext(Rec) <> 0;
    finally
      FindClose(Rec);
    end;

  if FindFirst(Path + '*.*', faDirectory, Rec) = 0 then
    try
      repeat
        if ((Rec.Attr and faDirectory) <> 0) and (Rec.Name <> '.') and (Rec.Name <> '..') then
          FileSearch(Path + Rec.Name, FileName);
      until FindNext(Rec) <> 0;
    finally
      FindClose(Rec);
    end;
  self.Cursor := crDefault;
end;

procedure TForm1.FileSearch2(const PathName, FileName: String);
var
  Rec: TSearchRec;
  Path: String;
begin
  self.Cursor := crHourGlass;
  Path := IncludeTrailingBackslash(PathName);

  if FindFirst(Path + '*.*', faDirectory, Rec) = 0 then
    try
      repeat
        if ((Rec.Attr and faDirectory) <> 0) and (Rec.Name <> '.') and (Rec.Name <> '..') then
          FileSearch(Path + Rec.Name, FileName);
      until FindNext(Rec) <> 0;
    finally
      FindClose(Rec);
    end;
  self.Cursor := crDefault;
end;


procedure TForm1.GetSubDirectories(const directory: String; list: TStrings);
var
  sr: TSearchRec;
  getBillIDCount: TADOQuery;
  billDir, s, s1: String;
  iBillId: integer;

const

  GET_BILL_ID_COUNT = 'select NullIf(count(*),0)  ' +
                      'from [billing_trans_detail] ' +
                      'where bill_id = %d';

begin
  try
    iBillId := 0;
    StatusBar2.Panels[0].Text := '';
    getBillIDCount := TADOQuery.Create(nil);
    getBillIDCount.Connection := ADOConn;
    self.Cursor := crHourGlass;
    application.ProcessMessages;
    if FindFirst(IncludeTrailingPathDelimiter(directory) + edtDirMask.Text, faDirectory, sr) < 0 then
      exit
    else
      repeat
        if ((sr.Attr and faDirectory <> 0) and (sr.Name <> '*.*') and (sr.Name <> '..')) then
        begin
          billDir := IncludeTrailingPathDelimiter(directory) + sr.Name;

//{$IFNDEF Debug}
          if dpFilterDate.Checked then
          begin
            if (DateOf(dpFilterDate.Date) > ReportFileTimes(billDir)) then
              continue;
          end;

//{$ENDIF};

          if cbNotInDB.Checked then
          begin
            s := copy(billDir, Pos('-', billDir) + 1, maxChar);
            TryStrToInt(copy(s, 0, Pos('-', s) - 1), iBillId);
            getBillIDCount.Close;
            getBillIDCount.SQL.clear;
            getBillIDCount.SQL.Add(format(GET_BILL_ID_COUNT, [iBillId]));
            getBillIDCount.Open;
            if not(getBillIDCount.Fields[0].IsNull) then
              continue;
          end;
          list.Add(billDir);
        end;
      until FindNext(sr) <> 0;
  finally
    FindClose(sr);
    getBillIDCount.Close;
    FreeAndNil(getBillIDCount);
    self.Cursor := crDefault;
  end;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var
  transFileMask: String;
begin
  transFileMask := edtFileMask.Text;

  ListBox2.clear;
  if not UseGUI then
  ListBox1.ItemIndex:= ListBox1.Items.Count-1;

  FileSearch(ListBox1.Items.Strings[ListBox1.ItemIndex], transFileMask);
  StatusBar2.Panels[1].Text := IntToStr(ListBox2.Count);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  FreeAndNil(fileData);
  ADOConn.Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
  VersionString:string;
begin
  if GetFileVersionNumberString(Application.ExeName, VersionString) then
  StatusBar1.Panels[7].Text := VersionString;

  bAbort := false;
  fileData := TStringList.Create;
  dpFilterDate.Date := Date() - 4;

  fTotalizer := 0;
  BillingID := 0;
  flogDir := ExtractFilePath(application.ExeName);


  if connectDB then
  begin

    LogResult.MethodName := 'FormCreate';
    LogResult.DataStream := 'Run Billing for data after '+DateToStr(dpFilterDate.Date);
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
    if not UseGUI then
    begin
      btnDirListClick(nil);
      RunProcess(false);
    end;
  end
  else
  begin
    LogResult.MethodName := 'Tranny Parser Failed';
    LogResult.Status := 'Fatal failure';
    LogResult.ExcepMsg := 'Some startup process failed.  See logs above.  Process not run';
    LogResult.LogType := ltError;
    WriteLog(LogResult);
    if not useGui then
    application.terminate;
  end;
  PopulateBillIDs;   //qm-910
end;

procedure TForm1.PopulateBillIDs;  //qm-910
begin
  try
    cbDelBillid.items.Clear;
    qryTrannyBillIDs.Open;
    while not qryTrannyBillIDs.eof do
    begin
      cbDelBillid.items.Add(qryTrannyBillIDs.FieldByName('bill_id').AsString);
      qryTrannyBillIDs.Next;
    end;

  finally
    cbDelBillid.Refresh;
    qryTrannyBillIDs.Close;
  end;
end;

procedure TForm1.GetConFigData;
begin
  try
    qryConfigData.Close;
    qryConfigData.Open;
    fBillDir := qryConfigData.fieldByName('value').AsString;
    edtPath.Text:= fBillDir;
  finally
    LogResult.MethodName := 'GetConFigData';
    LogResult.DataStream := 'Billing Directory: '+fBillDir;
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
    qryConfigData.Close;
  end;
end;

procedure TForm1.ProcessFile(SelectedFile: tFilename);
var
  i, cnt: integer;
  tmpLine: TStringList;
begin
  try
    tmpLine := TStringList.Create;
    tmpLine.Delimiter := #9;
    tmpLine.StrictDelimiter := true;
    fileData.LoadFromFile(SelectedFile);
    BillingID := strtoint(ExtractNumbers(ExtractFileDir(SelectedFile))[0]);
    StatusBar1.Panels[2].Text := IntToStr(fileData.Count);
    ProgressBar1.Min := 0;
    cnt := fileData.Count;
    for i := 1 to cnt - 1 do
    begin
      ProgressBar1.StepIt;
      tmpLine.clear;
      tmpLine.DelimitedText := fileData[i];
      with insData, insData.Parameters do
      begin
        ParamByName('bill_id').Value := BillingID;
        ParamByName('termcode').Value := tmpLine[0];
        ParamByName('ticket_id').Value := strtoint(tmpLine[1]);
        ParamByName('call_center').Value := tmpLine[2];
        ParamByName('ticket_number').Value := tmpLine[3];
        ParamByName('ticket_type').Value := tmpLine[4];
        ParamByName('con_name').Value := tmpLine[5];
        ParamByName('work_city').Value := tmpLine[6];
        ParamByName('transmit_date').Value := StrToDateTime(tmpLine[7]);
        ParamByName('work_county').Value := tmpLine[8];
        ParamByName('work_address_number').Value := tmpLine[9];
        ParamByName('work_address_number_2').Value := tmpLine[10];
        ParamByName('work_address_street').Value := tmpLine[11];
        ParamByName('work_cross').Value := tmpLine[12];
        ParamByName('map_page').Value := tmpLine[13];
        ParamByName('rate').Value := tmpLine[14];
        ParamByName('locate_closed').Value := tmpLine[15];
        ParamByName('plat').Value := tmpLine[16];
        ParamByName('revision').Value := tmpLine[17];  //QMANTWO-366
        try
                  ExecSQL;
        except on E: Exception do
        begin
                  LogResult.MethodName := 'ProcessFile';
                  LogResult.Status := e.Message;
                  LogResult.LogType := ltError;
                  WriteLog(LogResult);
        end;
        end;
        if bAbort then
        begin
          LogResult.MethodName := 'ProcessFile';
          LogResult.Status := 'Process Aborted by User';
          LogResult.LogType := ltWarning;
          WriteLog(LogResult);
          break;
        end;
        StatusBar1.Panels[1].Text := IntToStr(i-1);
        application.ProcessMessages;
      end;
    end;

  finally

    LogResult.MethodName := 'ProcessFile';
    LogResult.Status := 'Processed ' + IntToStr(i-1) + ' Files for ' + ExtractFileName(SelectedFile);
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
    FreeAndNil(tmpLine);
    fTotalizer := fTotalizer + (i-1);
  end;

end;

function TForm1.ReportFileTimes(const FileName: String): TDateTime;
  procedure ReportTime(const FileTime: TFileTime);
  var
    SystemTime, LocalTime: TSystemTime;
  begin
    if not FileTimeToSystemTime(FileTime, SystemTime) then
      RaiseLastOSError;
    if not SystemTimeToTzSpecificLocalTime(nil, SystemTime, LocalTime) then
      RaiseLastOSError;
    result := SystemTimeToDateTime(LocalTime);
  end;

var
  fad: TWin32FileAttributeData;
begin
  if not GetFileAttributesEx(PChar(FileName), GetFileExInfoStandard, @fad) then
    RaiseLastOSError;
  ReportTime(fad.ftLastWriteTime);
end;

function ExtractNumbers(const s: String): TArray<String>;
var
  regex: TRegEx;
  match: TMatch;
  matches: TMatchCollection;
  i: integer;
begin // Output-48398-NewJersey
  result := nil;
  i := 0;

  regex := TRegEx.Create('(?<=-)\d{5}(?=-)');  // ref counted
  matches := regex.matches(s);
  if matches.Count > 0 then
  begin
    SetLength(result, matches.Count);
    for match in matches do
    begin
      result[i] := match.Value;
      Inc(i);
    end;
  end;
end;

function TForm1.ParseXML(xmlFile: tFilename): boolean;
var
  XMLDOMDocument: IXMLDOMDocument;
  XMLDOMNodeList: IXMLDOMNodeList;
  XMLDOMNode: IXMLDOMNode;
  aXMLDOMNode: IXMLDOMNode;
  mailNode: IXMLDOMNode;
  logNode: IXMLDOMNode;
  adoNode: IXMLDOMNode;
  xml: TStringList;
  i: integer;
  s: String;
begin // bodyTextDir  logDir

  result := false;

    try
      xml := TStringList.Create;
      xml.LoadFromFile(xmlFile);
      XMLDOMDocument := CoDOMDocument.Create;
      try
        XMLDOMDocument.loadXML(xml.Text);
        if (XMLDOMDocument.parseError.ErrorCode <> 0) then
          raise Exception.CreateFmt('Error in Xml Data %s', [XMLDOMDocument.parseError]);

        XMLDOMNode := XMLDOMDocument.selectSingleNode('//trannyParser');
        logNode := XMLDOMDocument.selectSingleNode('//trannyParser/logdir');

        flogDir := logNode.attributes.getNamedItem('name').Text;

        adoNode := XMLDOMDocument.selectSingleNode('//trannyParser/ado_database');
        fadoHost := adoNode.attributes.getNamedItem('host').Text;
        fadoDatabase := adoNode.attributes.getNamedItem('database').Text;
        fadoLogin := adoNode.attributes.getNamedItem('login').Text;
        fadoPassword := adoNode.attributes.getNamedItem('password').Text;

        XMLDOMNodeList := XMLDOMNode.selectNodes('//trannyParser/Recipient');

        LogResult.MethodName := 'ParseXML';
        LogResult.Status := 'XML successfully parsed';
        LogResult.LogType := ltInfo;
        WriteLog(LogResult);
      except
        on E: Exception do
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.ExcepMsg := E.Message;
          LogResult.DataStream := XMLDOMDocument.Text;
          LogResult.LogType := ltError;
          WriteLog(LogResult);
          exit;
        end;
      end;
      result := true;
    finally
      XMLDOMDocument := nil;
      if assigned(xml) then
      FreeAndNil(xml);
    end;
end;

function TForm1.connectDB: boolean;
var
  myPath:string;
  connStr,LogConnStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  BillingIni: TIniFile;
const
  DECRYPT = 'DEC_';
begin
  Result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');

    aServer := BillingIni.ReadString('Database', 'Server', '');
    aDatabase := BillingIni.ReadString('Database', 'DB', 'QM');
    ausername := BillingIni.ReadString('Database', 'UserName', '');
    apassword := BillingIni.ReadString('Database', 'Password', '');
    flogDir   := BillingIni.ReadString('Billing', 'LogDir', '');

    LogConnStr := 'Provider=SQLNCLI11.1;;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    LogResult.MethodName := 'ConfigDB';
    LogResult.DataStream := 'ConnectionString: ' + LogConnStr;
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);

    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
      LogResult.MethodName := 'ConnectDB';
      LogResult.DataStream := 'You are using a Clear Text Password.  Please contact IT for the correct Password';
      LogResult.LogType := ltWarning;
      WriteLog(LogResult);
    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
    ADOConn.ConnectionString := connStr;
    ADOConn.close;
    ADOConn.Open;
    Result := ADOConn.Connected;
    GetConFigData;
  finally
    freeandnil(BillingIni);
  end;
end;

function TForm1.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;


function TForm1.ConfigDB: boolean;
begin
  if ADOConn.Connected then
    ADOConn.Close;
  try
    with ADOConn do
    begin
      ConnectionString := format(ADOCONN_NOAUTH_STR, [adoPassword, adoLogin, adoDatabase, adoHost]);
      ADOConn.Open();
      result := ADOConn.Connected;
      LogResult.MethodName := 'ConfigDB';
      LogResult.DataStream := 'ConnectionString: ' + ConnectionString;
      LogResult.LogType := ltInfo;
      WriteLog(LogResult);
    end;
    assert(ADOConn.Connected, 'Failed to connect to database');
    GetConFigData;
  except
    on E: Exception do
    begin
      if E is EAssertionFailed then
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := E.Message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end
      else
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := E.Message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end;
    end;
  end;
end;

procedure TForm1.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: String;
  Leader: String;
  EntryType: String;
const
  TRANNY_PARSER = 'TRANNY_PARSER_%s';
  PRE_PEND = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_PEND, now);
  if LogResult.LogType <> ltMissing then

    LogName := logDir + '\' + format(TRANNY_PARSER, [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT;

  case LogResult.LogType of
    ltError:
      begin
        EntryType := '**************** ERROR ****************';
      end;
    ltInfo:
      begin
        EntryType := '****************  INFO  ****************';
      end;
    ltNotice:
      begin
        EntryType := '**************** NOTICE ****************';
      end;
    ltWarning:
      begin
        EntryType := '**************** WARNING ****************';
      end;
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;
  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
    if useGUI then
    memo1.Lines.Add(Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
    if useGUI then
    memo1.Lines.Add(Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
    if useGUI then
    memo1.Lines.Add(Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
    if useGUI then
    memo1.Lines.Add(Leader + 'Data : ' + LogResult.DataStream);
  end;

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TForm1.cbDelBillidSelect(Sender: TObject); // qm-910
var
  BillIDtoDelete, HowManytoDelete: integer;
begin
  try
    BillIDtoDelete := 0;
    HowManytoDelete := 0;
    BillIDtoDelete := strtoint(cbDelBillid.Text);
    qryCountBillID.Parameters.ParamByName('BillID').Value := BillIDtoDelete;
    qryCountBillID.Open;
    HowManytoDelete := qryCountBillID.FieldByName('HowMany').AsInteger;
    if (MessageDlg('You have optioned to delete Bill ID ' +
      IntToStr(BillIDtoDelete) + ' which has ' + IntToStr(HowManytoDelete) +
      ' records.' + #13 + #10 + 'Press Yes to delete.' + #13 + #10 +
      'Press Cancel if you do not want to continue.', mtInformation,
      [mbYes, mbCancel], 0) = mrYes) then
    begin
      delTrannyBillID.Parameters.ParamByName('BillID').Value := BillIDtoDelete;
      delTrannyBillID.ExecSQL;
      ShowMessage('All ' + IntToStr(HowManytoDelete) + ' of ' +
        IntToStr(BillIDtoDelete) + ' have been deleted');
    end
    else
      ShowMessage('Deletion of ' + IntToStr(BillIDtoDelete) + ' Cancelled');
  finally
    qryCountBillID.Close;
    PopulateBillIDs;
    cbDelBillid.ItemIndex:=-1;
  end;
end;

procedure TForm1.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TForm1.LoadParams: boolean;
begin
  try
    fconfigXML := '';

    fconfigXML := ParamStr(1);

    if fconfigXML = '' then
    begin
      LogResult.MethodName := 'LoadParams';
      LogResult.ExcepMsg := 'Fatal Error - Show Stopper';
      LogResult.DataStream := 'Could not find Config.XML file';
      LogResult.LogType := ltError;

    end;

  except
    on E: Exception do
    begin
      LogResult.MethodName := 'LoadParams';
      LogResult.ExcepMsg := E.Message;
      LogResult.Status := 'Exception thrown for '+fconfigXML ;
      LogResult.DataStream := 'Could not process command line parameters';
      LogResult.LogType := ltError;

      result := false;
      exit;
    end;
  end;

  LogResult.MethodName := 'LoadParams';
  LogResult.Status := 'process config.xml';
  LogResult.DataStream := 'Extracting data from ' +fconfigXML;
  LogResult.LogType := ltInfo;

  result := true;
end;

end.
