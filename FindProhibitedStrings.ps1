<# 
  Search a Delphi source tree for prohibited string patterns and exit with an error if found.
  
  There are separate arrays for DFM and PAS files. 
  Add additional regex expressions below as needed.
#> 

$dfmPatterns = @("Explicit*"

  <# Add additional ; delimited patterns here as needed #>
  )
$result = Get-ChildItem .\ -Filter *.dfm -Recurse | Select-String -Pattern $dfmPatterns -List


<# Uncomment this section if there is ever a need to add prohibited pas strings
$pasPatterns = @("") 
$result += Get-ChildItem .\ -Filter *.pas -Recurse | Select-String -Pattern $pasPatterns -List
#>

if ($result) {
  Write-Output $result
  Write-Error "Prohibited strings found!" -Category InvalidResult
  Exit 1
}

Write-Output "No prohibited strings found"