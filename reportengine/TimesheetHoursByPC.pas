unit TimesheetHoursByPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppVar, ppBands, ppStrtch, ppCTMain,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  DB, ppModule, daDataModule, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimesheetHoursByPCDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    FromDateLabel: TppLabel;
    ppLabel12: TppLabel;
    ToDateLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    Pipe: TppDBPipeline;
    HoursCrosstab: TppCrossTab;
    daDataModule1: TdaDataModule;
    ppLabel1: TppLabel;
    EmpTypesLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure HoursCrosstabGetCaptionText(Sender: TObject; aElement: TppElement; aColumn, aRow: Integer; const aDisplayFormat: string; aValue: Variant; var aText: string);
    procedure HoursCrosstabGetTotalCaptionText(Sender: TObject; aElement: TppElement; aColumn, aRow: Integer; var aText: string);
    procedure HoursCrosstabGetDimensionName(Sender: TObject; aDimension: TppDimension; var aName: string);
    procedure SPAfterOpen(DataSet: TDataSet);
  private
    FIncludeCallouts: Boolean;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses
  OdDbUtils, ReportEngineDMu, OdRbUtils;

{$R *.dfm}

{ TTimesheetHoursByPCDM }

procedure TTimesheetHoursByPCDM.Configure(QueryFields: TStrings);
begin
  inherited;

  FIncludeCallouts := SafeGetBoolean('IncludeCallouts', True);
  if FIncludeCallouts then
    RemoveCrosstabValue(HoursCrosstab, 'work_hours')
  else
    RemoveCrosstabValue(HoursCrosstab, 'total_hours');

  SP.Parameters.ParamByName('@StartDate').Value := GetDateTime('StartDate');
  SP.Parameters.ParamByName('@EndDate').Value := GetDateTime('EndDate');
  SP.Parameters.ParamByName('@ApproveOnly').Value := SafeGetInteger('ApproveOnly', 0);
  SP.Parameters.ParamByName('@FinalOnly').Value := SafeGetInteger('FinalOnly', 0);
  SP.Parameters.ParamByName('@EmpTypes').Value := SafeGetString('EmpTypes', '');
  SP.Open;

  ShowDateRange(FromDateLabel, ToDateLabel, True);
  EmpTypesLabel.Caption := SafeGetString('EmpTypes', 'All');
end;

procedure TTimesheetHoursByPCDM.HoursCrosstabGetCaptionText(
  Sender: TObject; aElement: TppElement; aColumn, aRow: Integer;
  const aDisplayFormat: string; aValue: Variant; var aText: string);
var
  Col: TppColumnDef;
begin
  if SameText(aText, '(blank)') then
    Exit;
  if (aElement is TppColumnDef) then begin
    Col := (aElement as TppColumnDef);
    if Col.FieldName = 'work_date' then
      Col.DisplayFormat := 'mm/dd'
  end
  else if aElement is TppRowDef then begin
    // No changes to these yet (sums/PCs, etc.)
  end
  else if aElement is TppValueCaptionDef then begin
    if SameText(aText, 'Sum of work_hours') then
      aText := 'Production Hours'
    else if SameText(aText, 'Sum of num_timesheets') then
      aText := 'Timesheets'
    else if SameText(aText, 'Sum of total_hours') then
      aText := 'Production Hours';
  end
  else if aText = 'work_date' then
    aText := 'Work Date'
end;

procedure TTimesheetHoursByPCDM.HoursCrosstabGetTotalCaptionText(
  Sender: TObject; aElement: TppElement; aColumn, aRow: Integer;
  var aText: string);
begin
  aText := 'Total';
end;

procedure TTimesheetHoursByPCDM.HoursCrosstabGetDimensionName(
  Sender: TObject; aDimension: TppDimension; var aName: string);
begin
  if Assigned(aDimension) then
    aName := SP.FieldByName(aDimension.FieldName).DisplayLabel;
end;

procedure TTimesheetHoursByPCDM.SPAfterOpen(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('work_hours').DisplayLabel := 'Work Hours';
  DataSet.FieldByName('total_hours').DisplayLabel := 'Total Hours';
  DataSet.FieldByName('emp_pc_code').DisplayLabel := 'Profit Center';
  DataSet.FieldByName('work_date').DisplayLabel := 'Work Date';
end;

initialization
  RegisterReport('ProductionHoursByPC', TTimesheetHoursByPCDM);

end.

