unit EmployeeActivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass,
  ppVar, ppReport, ppStrtch, ppSubRpt, ppCtrls, ppPrnabl, ppCache, ppProd,
  DB, ppComm, ppRelatv, ppDB, ppDBPipe, ExtCtrls, ppMemo, ppParameter, ppRegion,
  ppDesignLayer, ActivityGraph, ppRichTx, Data.Win.ADODB;

type
  TEmployeeActivityDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ReportHeader: TppShape;
    ppDetailBand: TppDetailBand;
    ShortNameText: TppDBText;
    EmpNumber: TppDBText;
    ppFooterBand1: TppFooterBand;
    ReportFooter: TppShape;
    ReportCopyright: TppLabel;
    ReportDateTime: TppCalc;
    ReportPageNumber: TppSystemVariable;
    CompanyLabel: TppLabel;
    EmpManagerLabel: TppLabel;
    EmpManagerName: TppDBText;
    EmployeeLabel: TppLabel;
    EmpNumberLabel: TppLabel;
    DetailsDS: TDataSource;
    DetailsPipe: TppDBPipeline;
    ActivityDataDS: TDataSource;
    ActivityDataPipe: TppDBPipeline;
    ReportHeaderLabel: TppLabel;
    BreakRulesDS: TDataSource;
    BreakRulesPipe: TppDBPipeline;
    EmpTypeDesc: TppDBText;
    DateRangeLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ActivitiesLabel: TppLabel;
    TopManagerLabel: TppLabel;
    AllEmployeesLabel: TppLabel;
    EmpTypeLabel: TppLabel;
    LSAAttachedLabel: TppLabel;
    ComputerUsageDS: TDataSource;
    ComputerUsagePipe: TppDBPipeline;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    DetailSubReport: TppSubReport;
    GraphSubReport: TppChildReport;
    ppGraphBand: TppDetailBand;
    ppVerticalLine: TppLine;
    WorkingHoursLabel: TppLabel;
    LoginLogoutLabel: TppLabel;
    ppTimeLine: TppLine;
    ActivityDate: TppDBText;
    ppDBTextRuleMsg: TppDBText;
    ppDBTextRuleBrokenMsg: TppDBText;
    ppLabelLoginLogoutLegend: TppLabel;
    LoginLogoutLine: TppLine;
    DetailFooterText: TppLabel;
    ppDesignLayer3: TppDesignLayer;
    ComputerUsageSubReport: TppSubReport;
    ComputerUsageReport: TppChildReport;
    ComputerUsageTitleBand: TppTitleBand;
    ComputerUsageLabel: TppLabel;
    ComputerSyncDateLabel: TppLabel;
    ComputerUsageDetailBand: TppDetailBand;
    ComputerName: TppDBText;
    ComputerSyncDate: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ListSubReport: TppSubReport;
    ppChildReportActivityData: TppChildReport;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ActivityTimeLabel: TppLabel;
    ActivityDescriptionLabel: TppLabel;
    ppDetailBandActivityData: TppDetailBand;
    DetailRegion: TppRegion;
    ppDBTextActivityDate: TppDBText;
    ppDBTextActivityType: TppDBText;
    ppDBMemoActivityDescription: TppDBMemo;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    ppRichMemoLegend: TppRichText;
    SP: TADOStoredProc;
    ComputerUsage: TADODataSet;
    SummaryGroup: TADODataSet;
    Summary: TADODataSet;
    BreakRules: TADODataSet;
    ActivityData: TADODataSet;
    Details: TADODataSet;
    Master: TADODataSet;
    procedure Configure(QueryFields: TStrings); override;
    procedure DataModuleDestroy(Sender: TObject);
    procedure ppDetailBandActivityDataBeforePrint(Sender: TObject);
    procedure ppDBTextActivityTypeGetText(Sender: TObject; var Text: string);
    procedure UpdateActivitieDataSet(Sender: TObject);
    procedure ppDetailBandBeforePrint(Sender: TObject);
  protected
    procedure MergePDFReportWithAttachments(const OutputDir, ReportFileName: string; var MergedPDFFileName: string); override;
    procedure HighLightLegendSymbols;
  private
    PDFFileList: TStringList;
    ShowGraph: Boolean;
    ShowList: Boolean;
    GraphDrawer: TGraphingDM;
    procedure SetContentTypes;
    procedure SetupReportControls;
    function GenerateLSAReport(QueryFields: TStrings): string;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, QMConst, LocateStatusActivity, OdPdf, StrUtils, odADOutils;

{$R *.dfm}

{ TEmployeeActivityDM }

procedure TEmployeeActivityDM.Configure(QueryFields: TStrings);
var
  StartDate, EndDate: TDateTime;
  HourSpanStart: TDateTime;
  HourSpanEnd: TDateTime;
  EmployeeStatus: Integer;
  OutOfRangeActivities, OutOfRangeEmployees: Integer;
  ActivitiesText: string;
  ManagerID: Integer;
  EmployeeID: Integer;
  LSAPDFFileName: string;
  UnusedVar: Single;
  RecsAffected : oleVariant;
begin
  inherited;
  PDFFileList := TStringList.Create;

  StartDate := GetDateTime('StartDate');
  EndDate := SafeGetDateTime('EndDate', StartDate + 1);

  HourSpanStart := SafeGetDateTime('HourSpanStart');
  HourSpanEnd := SafeGetDateTime('HourSpanEnd');

  EmployeeStatus := SafeGetInteger('EmployeeStatus', 2);
  OutOfRangeActivities := SafeGetInteger('OutOfRangeActivities', 0);
  OutOfRangeEmployees := SafeGetInteger('OutOfRangeEmployees', 0);
  ManagerID := SafeGetInteger('ManagerId', -1);
  EmployeeID := SafeGetInteger('EmployeeId',-1);

  with SP do begin
    Parameters.ParamByName('@EmployeeId').value := EmployeeID;
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@StartDate').value := StartDate;
    Parameters.ParamByName('@EndDate').value := EndDate;
    Parameters.ParamByName('@OutOfRangeActivities').value := OutOfRangeActivities;
    Parameters.ParamByName('@OutOfRangeEmployees').value := OutOfRangeEmployees;
    if HourSpanStart <> 0 then
      Parameters.ParamByName('@HourSpanStart').value := HourSpanStart;
    if HourSpanEnd <> 0 then
      Parameters.ParamByName('@HourSpanEnd').value := HourSpanEnd;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Open;
  end;

  // Discard the extra 2 record sets used in the summary rep: Summary and SummaryGroup

  Master.RecordSet              := SP.RecordSet;
  Details.RecordSet             := SP.RecordSet.NextRecordset(RecsAffected);
  ActivityData.RecordSet        := SP.RecordSet.NextRecordset(RecsAffected);
  BreakRules.RecordSet          := SP.RecordSet.NextRecordset(RecsAffected);
  Summary.RecordSet             := SP.RecordSet.NextRecordset(RecsAffected);
  SummaryGroup.RecordSet        := SP.RecordSet.NextRecordset(RecsAffected);
  ComputerUsage.RecordSet       := SP.RecordSet.NextRecordset(RecsAffected);

  SetupReportControls;

  //Create Dynamic Labels
  ShowDateRange(DateRangeLabel, 'StartDate' , 'EndDate');
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus, EmpManagerName.Left);
  ShowEmployeeName(TopManagerLabel, ManagerID, 'Manager:', EmpTypeDesc.Left);

  if IntToBoolean(OutOfRangeActivities) then
    ActivitiesText := 'Out of working hours'
  else if HourSpanStart = 0 then
    ActivitiesText := 'All'
  else
    ActivitiesText := Format(' Out of specific hours - %s to %s', [FormatDateTime('hh:mm', HourSpanStart),  FormatDateTime('hh:mm', HourSpanEnd)]);
  ShowText(ActivitiesLabel, 'Activities:', ActivitiesText, UnusedVar, EmpManagerName.Left);
  AllEmployeesLabel.Visible := EmployeeID <= 0;

  if SafeGetBoolean('IncludeLSA', False) then begin
    LSAPDFFileName := GenerateLSAReport(QueryFields);
    PDFFileList.Add(LSAPDFFileName);
    LSAAttachedLabel.Visible := True;
  end
  else
    LSAAttachedLabel.Visible := False;

end;

procedure TEmployeeActivityDM.SetContentTypes;
var
  ShowBoth: Boolean;
begin
  //Support old clients by looking for previous binary flag
  ShowBoth := SameText(SafeGetString('ReportContents', ''), 'Both') or (SafeGetInteger('Graph', 0) = 1);

  ShowList := ((SafeGetInteger('Graph', 0) = 0) and not SameText(SafeGetString('ReportContents', ''), 'Graph'))
    or SameText(SafeGetString('ReportContents', ''), 'List')
    or ShowBoth;

  ShowGraph := SameText(SafeGetString('ReportContents', ''), 'Graph') or ShowBoth;
end;


procedure TEmployeeActivityDM.HighLightLegendSymbols;
  procedure ReplaceColor(ReplaceStr: String; ReplaceColor: TColor);
  var
    CursorPos : integer;
  begin
    CursorPos := 0;
    CursorPos := ppRichMemoLegend.FindText(ReplaceStr, CursorPos, Length(PPRichMemoLegend.RichText),[]);
    if CursorPos <> -1 then begin
      ppRichMemoLegend.SelStart := CursorPos;
      ppRichMemoLegend.SelLength := Length(ReplaceStr);
      ppRichMemoLegend.SelAttributes.Color := ReplaceColor;
    end;
  end;
begin
   ReplaceColor(StatusedLateTxt, clMaroon);
   ReplaceColor(OutsideWorkSpanTxt, clRed);
end;


procedure TEmployeeActivityDM.SetupReportControls;
begin
  SetContentTypes;
  GraphDrawer := TGraphingDM.create(self);
  if ShowGraph then begin
    GraphDrawer.SetupGraphControls(ppGraphBand, ppTimeLine);
    ppRichMemoLegend.RichText := GraphDrawer.GraphLegend;
    HighlightLegendSymbols;
  end;

  if ShowList then begin
    ListSubReport.Visible := True;
    ComputerUsageSubReport.Visible := True;
    ppGraphBand.PrintCount := 1;
    //Remove the legend
    ppLabelLoginLogoutLegend.Visible := False;
    LoginLogoutLine.Visible := False;
    ppRichMemoLegend.Visible := False;
    if not ShowGraph then begin
      //Remove the graph components and shift the list components up
      ListSubReport.Top := 0.23;
      WorkingHoursLabel.Visible := False;
      ppVerticalLine.Visible := False;
      ppTimeLine.Visible := False;
    end;
  end;
end;

{ ******************* Code to configure and traverse datasets **************** }

//Called For Each Employee
procedure TEmployeeActivityDM.ppDetailBandBeforePrint(Sender: TObject);
begin
  inherited;
  Details.Filter := 'emp_id=' + IntToStr(Master.FieldByName('r_emp_id').AsInteger);
  Details.Filtered := True;
end;

//Called For Each Activity Day of Each Employee
procedure TEmployeeActivityDM.UpdateActivitieDataSet(Sender: TObject);
const
  ActivityDataFilter = 'emp_id=%s and activity_date>=%s and activity_date<%s';
  ComputerUsageFilter = 'emp_id=%s and sync_date>=%s and sync_date<%s';
var
  EmployeeID, StartDate, EndDate: string;
begin
  inherited;

  EmployeeID := Details.FieldByName('emp_id').AsString;
  StartDate := Details.FieldByName('activity_date').AsString;
  EndDate := DateTimeToStr(Details.FieldByName('activity_date').AsDateTime+1);

  BreakRules.Filter := 'work_emp_id=' + EmployeeID + ' and work_date = ''' +StartDate+'''';
  BreakRules.Filtered := True;

  ppDBTextRuleBrokenMsg.Left := ppDBTextRuleMsg.Left + ppDBTextRuleMsg.Width - ppDBTextRuleBrokenMsg.Width;

  ActivityData.Filter := Format(ActivityDataFilter, [EmployeeID, QuotedStr(StartDate), QuotedStr(EndDate)]);
  ActivityData.Filtered := True;
  ComputerUsage.Filter := Format(ComputerUsageFilter, [EmployeeID,
         QuotedStr(StartDate), QuotedStr(EndDate)]);
  ComputerUsage.Filtered := True;
  ComputerUsageSubReport.Visible := ComputerUsage.RecordCount > 1;
  if ShowGraph then begin
    GraphDrawer.DrawSpansAndEvents(ppGraphBand, WorkingHoursLabel, LoginLogoutLabel, ActivityData,
        EmployeeID, StartDate, EndDate);
  //Reset the activitydata
    ActivityData.Filter := Format(ActivityDataFilter,
        [EmployeeID, QuotedStr(StartDate), QuotedStr(EndDate)]);
    ActivityData.Filtered := True;
  end;
end;

{ ******************* Simple format changes based on datasets **************** }

procedure TEmployeeActivityDM.ppDetailBandActivityDataBeforePrint(Sender: TObject);
begin
  ppDBTextActivityDate.Font.Style := [];
  ppDBTextActivityDate.Color := clWhite;
  ppDBTextActivityDate.Font.Color := clBlack;

  // Out of range activities
  if ActivityData.FieldByName('out_of_range').AsBoolean then begin
    ppDBTextActivityDate.Font.Style := [fsBold];
    ppDBTextActivityDate.Color := $00E1E1E1;
  end;
  // Statused Late
  if (ActivityData.FieldByName('activity_type').AsString = 'LOCST') and
    AnsiContainsText(ActivityData.FieldByName('description').AsString, 'Status Locate Late') then begin
    ppDBTextActivityDate.Font.Color := clMaroon;
    ppDBTextActivityDate.Font.Style := [fsBold];
  end
end;

procedure TEmployeeActivityDM.ppDBTextActivityTypeGetText(Sender: TObject;
  var Text: string);
begin
  if ShowGraph then
    Text := GraphDrawer.GetLegendValue(ActivityData.FieldByName('activity_type').AsString)
  else
    Text := '';
end;

{ ******************* Code below here does not touch any datasets **************** }

procedure TEmployeeActivityDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(PDFFileList);
end;

{ ******************* Setup code, runs once at the start of generation **************** }

function TEmployeeActivityDM.GenerateLSAReport(QueryFields: TStrings): string;

  function CreateLSAQueryFields: TStringList;
  var
    I: Integer;
    SingleEmployee: Boolean;
  begin
    Result := TStringList.Create;
    Result.AddStrings(QueryFields);
    SingleEmployee := SafeGetInteger('EmployeeID', -1) > 0;
    for I := 0 to Result.Count - 1 do begin
      if SameText(Result.Names[I], 'StartDate') then
        Result.Strings[I] := 'DateFrom=' + QueryFields.Values['StartDate']
      else if SameText(Result.Names[I], 'EndDate') then
        Result.Strings[I] := 'DateTo=' + QueryFields.Values['EndDate']
      else if SameText(Result.Names[I], 'EmployeeID') and SingleEmployee then
        Result.Strings[I] := 'Manager=' + QueryFields.Values['EmployeeID']
      else if SameText(Result.Names[I], 'ManagerID') and (not SingleEmployee) then
        Result.Strings[I] := 'Manager=' + QueryFields.Values['ManagerID'];
    end;
    Result.Add('SingleEmployee=' + BooleanToString01(SingleEmployee));
    Result.Add('SortBy=0');
  end;

var
  LSAParams: TStringList;
  LSAReport: TBaseReportDM;
  FS: TFileStream;
  LSAOutput: string;
begin
  LSAParams := nil;
  LSAReport := CreateReport('LocateStatusActivity', IniName);
  try
    LSAParams := CreateLSAQueryFields;

    LSAReport.UseConnection(SP.Connection, SP.CommandTimeout);
    LSAReport.Configure(LSAParams);

    LSAOutput := ChangeFileExt(GetWindowsTempFileName(False, OutputFolder, 'QM'), '.PDF');
    FS := TFileStream.Create(LSAOutput, fmCreate);
    try
      LSAReport.CreatePDFStream(FS);
      Result := LSAOutput;
    finally
      FreeAndNil(FS);
    end;
  finally
    FreeAndNil(LSAParams);
    FreeAndNil(LSAReport);
  end;
end;



procedure TEmployeeActivityDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
begin
  MergedPDFFileName := MergePDFs(PDFFileList, OutputDir, ReportFileName, IniName);
end;

initialization
  RegisterReport('EmployeeActivity', TEmployeeActivityDM);

end.

