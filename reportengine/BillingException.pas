unit BillingException;
// issues
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppBands, ppClass, ppVar, ppCtrls, ppPrnabl, ppCache,
  ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB, ppStrtch,
  ppSubRpt, ppRegion, ppDesignLayer, ppParameter, ppMemo,   Data.Win.ADODB;

type
  TBillingExceptionDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel1: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel13: TppLabel;
    ppDBText12: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppDetailBand2: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppHeaderBand3: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppFooterBand3: TppFooterBand;
    ppHeaderBand4: TppHeaderBand;
    ppDetailBand4: TppDetailBand;
    ppFooterBand4: TppFooterBand;
    MultipleLocatesDS: TDataSource;
    MultipleLocatesPipe: TppDBPipeline;
    HourlyWorkDS: TDataSource;
    HourlyWorkPipe: TppDBPipeline;
    CalloutsDS: TDataSource;
    CalloutsPipe: TppDBPipeline;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    DateRangeLabel: TppLabel;
    CallCenterLabel: TppLabel;
    UnitsLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    CallCenterList: TppMemo;
    ppRegionColumnHeaders: TppRegion;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    Data: TADOStoredProc;
    MultipleLocates: TADODataSet;
    HourlyWork: TADODataSet;
    Callouts: TADODataSet;
    Master: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, odADOUtils;

procedure TBillingExceptionDM.Configure(QueryFields: TStrings);
var
  Units: Integer;
  CallCenters: String;
  RecsAffected : oleVariant;
begin
  inherited;
  Units := GetInteger('Units');
  CallCenters := GetString('CallCenters');

  with Data do begin
    Parameters.ParamByName('@BeginDate').value := GetDateTime('BeginDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Parameters.ParamByName('@CallCenters').value := CallCenters;
    Parameters.ParamByName('@Units').value := Units;
    Open;
  end;

  Master.RecordSet := Data.Recordset;
  MultipleLocates.RecordSet := Data.Recordset.NextRecordset(RecsAffected);
  HourlyWork.RecordSet := Data.Recordset.NextRecordset(RecsAffected );
  Callouts.RecordSet := Data.Recordset.NextRecordset(RecsAffected );

  ShowDateRange(DateRangeLabel, 'BeginDate', 'EndDate');
  ShowText(UnitsLabel, 'Units: ', IntToStr(Units));
  CallCenterList.Text := StringReplace(CallCenters, ',', ', ', [rfReplaceAll]);
end;

initialization
  RegisterReport('BillingException', TBillingExceptionDM);

end.
