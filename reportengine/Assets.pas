unit Assets;

interface

uses Forms, BaseReport, DB, ppDB, ppDBPipe, ppModule, daDataModule,
  ppCtrls, ppBands, ppClass, ppVar, ppPrnabl, ppCache, Classes, ppComm,
  ppRelatv, ppProd, ppReport, ppDesignLayer, ppParameter, 
  Data.Win.ADODB;

type
  TAssetReportDM = class(TBaseReportDM)
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    short_nameDBText: TppDBText;
    ppDBText3: TppDBText;
    ppFooterBand1: TppFooterBand;
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    ppDBText9: TppDBText;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    daDataModule1: TdaDataModule;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBCalc77: TppDBCalc;
    ppDBText2: TppDBText;
    ppLabel5: TppLabel;
    ManagerLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses OdRbHierarchy, ReportEngineDMu;

{$R *.dfm}

{ TLoadReportDM }

procedure TAssetReportDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
begin
  inherited;
  AddHierGroups(Report, short_nameDBText);
  ManagerID := GetInteger('ManagerID');
  with SP do begin
    Parameters.ParamByName('@ManagerID').value := ManagerID;
    Open;
  end;
  ShowEmployeeName(ManagerLabel, ManagerID);
end;

initialization
  RegisterReport('Assets', TAssetReportDM);

end.

