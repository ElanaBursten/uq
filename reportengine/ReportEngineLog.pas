unit ReportEngineLog;

interface

procedure StartLogging(Path : string);
procedure AddLogEntry(const Section, Message : string; IsError : Boolean = False);

//============================================================================//

implementation

uses
  Windows, SysUtils, OdSimpleLog, JclFileUtils, OdThreadTimer, QMConst;

type
  TReportEngineLog = class(TSimpleLog)
  protected
    FId : Int64;
    FStartDateTime : TDateTime;
    FFirstEntry : Boolean;
    FTimer: TOdThreadTimer;
    procedure OnDestroy; override;
  public
    constructor Create(Path: string);
    procedure AddLogEntry(const Section, Message: string; IsError : Boolean = False);
  end;

var
  FReportEngineLog : TReportEngineLog;

function GetReportEngineLog : TReportEngineLog;
begin
  Result := FReportEngineLog;
end;

procedure StartLogging(Path : string);
begin
  FReportEngineLog := TReportEngineLog.Create(Path);
end;

procedure AddLogEntry(const Section, Message: string; IsError : Boolean = False);
begin
  if Assigned(FReportEngineLog) then
    FReportEngineLog.AddLogEntry(Section, Message, IsError);
end;

//TReportEngineLog
constructor TReportEngineLog.Create(Path: string);
begin
  inherited Create(Path, 'Reports.log');
  FFirstEntry := True;

  if FileExists(FileName) then
    //use the file's current size as the instance ID for this run of the program
    FId := GetSizeOfFile(Filename)
  else begin
    FId := 0;
    Append('ID'^I'Type'^I'Timestamp'^I'Elapsed'^I'Report'^I'Message');
  end;
 Append('');

  AddLogEntry('---', 'Begin');
end;

procedure TReportEngineLog.AddLogEntry(const Section, Message: string; IsError : Boolean = False);
const
  IsErrorStr : array[Boolean] of string[3] = ('INF', 'ERR');
var
  DT : TDateTime;
begin
  DT := Now;
  if FFirstEntry then begin
    FStartDateTime := DT;
    FTimer := TOdThreadTimer.Create;
    FTImer.Reset;
    FFirstEntry := False;
  end;

  FTimer.Measure;

  Append(Format('%d'^I'%s'^I'%s'^I'%s'^I'%s'^I'%s'^I'%s',
    [FId, IsErrorStr[IsError],
     FormatDateTime('mm/dd/yyyy hh:nn:ss', Now),
     FormatDateTime('nn:ss.zzz', DT-FStartDateTime),
     FormatDateTime('nn:ss.zzz', FTimer.GetElapsedTime),
     Section, Message]));
end;

procedure TReportEngineLog.OnDestroy;
begin
  AddLogEntry('---', 'End');
end;

initialization

finalization
  FreeAndNil(FReportEngineLog);
end.

