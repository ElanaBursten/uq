select
  profit_center,
  invoice_code,
  description,
  ratio,
  ratio * 100 as ratio_percent,
  count(*) as N,
  sum(case when inv_total>0 then inv_total
      else coalesce(init_est_cost,0) end) as init_total_cost,

  sum(coalesce(init_est_cost,0)) * ratio as init_adj_total_cost,

  sum(case when inv_total>0 then inv_total
      else coalesce(curr_est_cost,0) end) as curr_total_cost,

  sum(coalesce(curr_est_cost,0)) * ratio as curr_adj_total_cost,
 
  sum(coalesce(init_est_cost,0) - coalesce(paid_amount,0)) as init_remaining_expo,
  sum(coalesce(curr_est_cost,0) - coalesce(paid_amount,0)) as curr_remaining_expo,

  sum((coalesce(init_est_cost,0) - coalesce(paid_amount,0)) * ratio) as init_total_accrual_amount,
  sum((coalesce(curr_est_cost,0) - coalesce(paid_amount,0)) * ratio) as curr_total_accrual_amount


from (
  select profit_center, invoice_code,
   r.description,
   convert(decimal(5,2), r.modifier) as ratio,
   (select top 1 amount from damage_estimate de2 where de2.modified_date =
    (select min(modified_date) from damage_estimate
         where damage_estimate.damage_id = d.damage_id
           and damage_estimate.amount>0)
     and de2.damage_id = d.damage_id) as init_est_cost,
   (select top 1 amount from damage_estimate de3 where de3.modified_date =
    (select max(modified_date) from damage_estimate
         where damage_estimate.damage_id = d.damage_id
           and damage_estimate.amount>0)
     and de3.damage_id = d.damage_id) as curr_est_cost,
   (select sum(i2.amount) from damage_invoice i2 where d.damage_id = i2.damage_id) as inv_total,
    coalesce((select SUM(i2.paid_amount) from damage_invoice i2 where d.damage_id = i2.damage_id),0) as paid_amount
  from damage d
    left join ticket t on d.ticket_id=t.ticket_id
    left join reference r on r.type='invcode' and r.code=d.invoice_code
  --WHERE
 ) data
 group by profit_center, invoice_code, description, ratio
 order by profit_center, description

