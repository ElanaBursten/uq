unit BaseReport;
//  Ado Converted
interface

uses
  Windows, SysUtils, Classes, ppReport, DB, ppCtrls, QMTempFolder, Data.Win.ADODB,
  reportconfig,
  System.ZLib, ppComm, ppRelatv, ppProd, ppClass;

type
  TBaseReportDM = class(TDataModule)
  private
    FParams: TStrings;
    FQueryFields: TStrings;
    FReportComponent: TppReport;
    FConnection: TADOConnection;
    FSaveSQL: Boolean;
    FIniName: string;
    FTempFolder: ITemporaryFolder;
    function HaveReportComponent: Boolean;
    function GetReport: TppReport;
    // The function below works, but nothing currently uses it.
    // function GetFirstDataSet: TDataSet;
    function GetDataSets: TList;
    function TranslateBrand(Before: string): string;
    procedure LookUpCompanyName;
  protected
    FCompanyName: string;
    FSQL: string;  // if a subclass has some SQL, store it here.
    function GetDateTime(const ParamName: string): TDateTime;
    function SafeGetDateTime(const ParamName: string; Default: TDateTime = 0): TDateTime;
    function GetString(const ParamName: string): string;
    function SafeGetString(const ParamName: string; const Default: string = ''): string;
    function GetInteger(const ParamName: string): Integer;
    function SafeGetInteger(const ParamName: string; Default: Integer = -1): Integer;
    function GetBoolean(const ParamName: string): Boolean;
    function SafeGetBoolean(const ParamName: string; Default: Boolean): Boolean;
    function HaveParam(const ParamName: string): Boolean;
    procedure PreviewFormCreate(Sender: TObject);
    procedure SetBrandingStrings;
    function GetLiabilityString(Liability: Integer): string;
    procedure DumpSQL; virtual;
    procedure SetParam(const Name: string; const Value: string);
    procedure SetParamInt(const Name: string; const Value: Integer);
    procedure SetParamDate(const Name: string; const Value: TDateTime; Required: Boolean = False);
    procedure SetParamBoolean(const Name: string; const Value: Boolean);
    procedure SetOutgoingParams; virtual;
    procedure ShowDateRange(const FromDateLabel, ToDateLabel: TppLabel; ToDateInclusive: Boolean = False); overload;
    procedure ShowDateRange(const DateRangeLabel: TppLabel; const StartDateParam: string = 'StartDate'; const EndDateParam: string = 'EndDate'; Caption: string = ''; ToDateInclusive: Boolean = False; IncludeHours: Boolean = False); overload;
    procedure MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string); virtual;
    procedure ShowEmployeeName(const EmployeeLabel: TppLabel; const EmployeeID: Integer; const Caption: string = 'Manager: '; const LeftPos: Single = -1);
    procedure ShowEmployeeStatus(const EmployeeStatusLabel: TppLabel; const EmployeeStatus: Integer; const LeftPos: Single = -1);
    procedure ShowText(const TitleLabel: TppLabel; const Caption, Text: string); overload;
    procedure ShowText(const TitleLabel: TppLabel; const Caption, Text: string; var LastPos: Single); overload;
    procedure ShowText(const TitleLabel: TppLabel; Text: string); overload;
    procedure ShowText(const TitleLabel: TppLabel; const Caption, Text: string; var LastPos: Single; LeftPosition: Single); overload;
    procedure AdjustLabelPosition(aLabel: TppLabel; BaseLabel: TppLabel; Distance: Single); overload;
    procedure AdjustLabelPosition(aLabel: TppLabel; Position: Single); overload;
    procedure ShowOfficeName(const OfficeLabel: TppLabel; const OfficeID: Integer; const Caption: string = 'Office: ');
    procedure ShowBooleanText(const TitleLabel: TppLabel; const Caption: string; const Value: Boolean);
    procedure ShowDateText(const TitleLabel: TppLabel; const Caption: string; const Value: TDateTime; const IncludeTime: Boolean = False);
    procedure ShowHierarchyOption(const HierarchyOptionLabel: TppLabel; const HierarchyOption: Integer);
    procedure ExportPDFToStream(Report: TppReport; Str: TStream);
  public

    constructor Create(AOwner: TComponent); override;
    procedure Configure(QueryFields: TStrings); virtual;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); virtual;
    procedure CreatePDFStream(FinalOutputStream: TStream); virtual;
    procedure AfterPrint; virtual;
    procedure PrintPreview; virtual;
    procedure UseConnection(Conn: TADOConnection; QueryTimeout: Integer);
    procedure AttachParamsToStream(Str: TStream); virtual;
    property SaveSQL: Boolean read FSaveSQL write FSaveSQL;
    property IniName: string read FIniName write FIniName;
    function OutputFolder: string;
  end;

  ReportClass = class of TBaseReportDM;

procedure RegisterReport(ReportName: string; ImplementingClass: ReportClass);

function CreateReport(ReportName: string; IniName: string): TBaseReportDM;

// This is exposed only so that the report-tester can get to the list
var
  RegisteredReportNames: TStringList;

implementation

uses ReportEngineDMu, OdIsoDates, OdDbUtils, Forms, ppViewr, ppTypes,
  QMConst, OdExceptions, GIFImg, Graphics, odADOUtils,
  ppPDFDevice, ppPDFSettings,
  OdMiscUtils, OdRbUtils, StrUtils, Variants;

const
  NeedDateMessage = 'You must specify a date / range: ';

{$R *.dfm}

var
  RegisteredClasses: TList;

{ TBaseReportDM }

procedure TBaseReportDM.Configure(QueryFields: TStrings);
begin
  FQueryFields := QueryFields;
  SetBrandingStrings;
end;

function TBaseReportDM.GetDateTime(const ParamName: string): TDateTime;
var
  S: string;
begin
  S := Trim(FQueryFields.Values[ParamName]);
  if S='' then
    raise Exception.Create('Missing Parameter: ' + ParamName);

  try
    if Length(S)=10 then
      Result := IsoStrToDate(S)
    else
      Result := IsoStrToDateTime(S);
  except
    on E: Exception do
      raise Exception.Create('Error with Parameter: ' + ParamName + ': ' + E.Message);
  end;
end;

function TBaseReportDM.GetInteger(const ParamName: string): Integer;
var
  S: string;
begin
  S := Trim(FQueryFields.Values[ParamName]);
  if S='' then
    raise Exception.Create('Missing Parameter: ' + ParamName);

  try
    Result := StrToInt(S);
  except
    on E: Exception do
      raise Exception.Create('Error with Parameter: ' + ParamName + ': ' + E.Message);
  end;
end;

function TBaseReportDM.GetString(const ParamName: string): string;
begin
  Result := Trim(FQueryFields.Values[ParamName]);
  if Result = '' then
    raise Exception.Create('Missing Parameter: ' + ParamName);
end;

procedure TBaseReportDM.AdjustLabelPosition(aLabel, BaseLabel: TppLabel; Distance: Single);
begin
  OdRbUtils.AdjustLabelPosition(aLabel, BaseLabel, Distance);
end;

procedure TBaseReportDM.AdjustLabelPosition(aLabel: TppLabel; Position: Single);
begin
  OdRbUtils.AdjustLabelPosition(aLabel, Position);
end;

procedure TBaseReportDM.AfterPrint;
begin
  //just a hook
end;

procedure RegisterReport(ReportName: string; ImplementingClass: ReportClass);
begin
  if RegisteredReportNames = nil then begin
    RegisteredReportNames := TStringList.Create;
    RegisteredClasses := TList.Create;
  end;

  if RegisteredReportNames.IndexOf(ReportName)>=0 then
    raise Exception.Create('Attempted to register name twice: ' + ReportName);

  RegisteredReportNames.Add(ReportName);
  RegisteredClasses.Add(Pointer(ImplementingClass));
end;

function CreateReport(ReportName: string; IniName: string): TBaseReportDM;
var
  i: Integer;
  RepClass: ReportClass;
begin
  i := RegisteredReportNames.IndexOf(ReportName);
  if i >= 0 then begin
    RepClass := ReportClass(RegisteredClasses.Items[i]);
    try
      Result := RepClass.Create(nil);
      Result.IniName := IniName;
    except
      on E: Exception do begin
        E.Message := 'Unable to create report ' + ReportName + E.Message;
        raise;
      end;
    end;
  end else
    raise Exception.Create('Report not found: ' + ReportName);
end;

constructor TBaseReportDM.Create(AOwner: TComponent);
begin
  inherited;
  FTempFolder := NewTemporaryFolder;
end;

function TBaseReportDM.SafeGetString(const ParamName: string; const Default: string): string;
begin
  if HaveParam(ParamName) then
    Result := GetString(ParamName)
  else
    Result := Default;
end;

function TBaseReportDM.SafeGetInteger(const ParamName: string; Default: Integer): Integer;
begin
  try
    if HaveParam(ParamName) then
      Result := GetInteger(ParamName)
    else
      Result := Default;
  except
    Result := Default;
  end;
end;

function TBaseReportDM.SafeGetDateTime(const ParamName: string; Default: TDateTime): TDateTime;
begin
  try
    if HaveParam(ParamName) then
      Result := GetDateTime(ParamName)
    else
      Result := Default;
  except
    Result := Default;
  end;
end;

procedure TBaseReportDM.UseConnection(Conn: TADOConnection; QueryTimeout: Integer);
var
  I : Integer;
  Comp: TComponent;
  DS: TCustomADODataSet;
begin
  Conn.ConnectionString;
  FConnection := Conn;
//  Conn.ConnectionTimeout := QueryTimeout;   sr
//  Conn.CommandTimeout := QueryTimeout;      sr

  for I := 0 to ComponentCount-1 do begin
    Comp := Components[I];

    if Comp is TCustomADODataSet then begin
       DS := Comp as TCustomADODataSet;
      if DS.ConnectionString <> '' then
        raise Exception.Create('ERROR: Connection string was not empty! ' + DS.Name);
      if DS.Active  then
        raise Exception.Create('ERROR: Dataset was open: ' + DS.Name);
      DS.Active := False;
      DS.Connection := Conn;
    end;

    if Comp is TADOStoredProc then
      (Comp as TADOStoredProc).CommandTimeout := QueryTimeout;

    if Comp is TADOQuery then
      (Comp as TADOQuery).CommandTimeout := QueryTimeout;

    if Comp is TADOCommand then begin
      (Comp as TADOCommand).Connection := Conn;
      (Comp as TADOCommand).CommandTimeout := QueryTimeout;
    end;
  end;
end;

function TBaseReportDM.GetReport: TppReport;
var
  i: Integer;
begin
  if Assigned(FReportComponent) then begin
    Result := FReportComponent;
    Exit;
  end;

  Result := nil;
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TppReport then begin
      Result := Components[i] as TppReport;
      Break;
    end;
  end;

  if not Assigned(Result) then
    raise Exception.Create('No TppReport found');
end;

function TBaseReportDM.HaveReportComponent: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TppReport then begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TBaseReportDM.CreatePDFStream(FinalOutputStream: TStream);
var
  OriginalPDFFileName: string;
  MergedPDFFileName: string;
  TempStr: TFileStream;
begin
  Assert(NotEmpty(OutputFolder), 'OutputFolder cannot be blank in CreatePDFStream');

  OriginalPDFFileName := AddSlash(OutputFolder) + 'QMRETempReportForMerging.pdf';
  TempStr := TFileStream.Create(OriginalPDFFileName, fmCreate);
  try
    // Export report to a PDF file, so it can be merged with our external tool to merge PDFs
    ExportPDFToStream(GetReport, TempStr);
  finally
    FreeAndNil(TempStr);
  end;

  MergePDFReportWithAttachments(OutputFolder, OriginalPDFFileName, MergedPDFFileName);
  if FinalOutputStream is TFileStream then begin
    TempStr := TFileStream.Create(MergedPDFFileName, fmOpenRead);
    try
      FinalOutputStream.CopyFrom(TempStr, TempStr.Size);
    finally
      FreeAndNil(TempStr);
    end;
  end
  else if FinalOutputStream is TMemoryStream then
    TMemoryStream(FinalOutputStream).LoadFromFile(MergedPDFFileName)
  else
    raise EOdException.CreateFmt('Unhandled stream class %s for FinalOutputStream in TBaseReportDM.CreatePDFStream.', [FinalOutputStream.ClassName]);
end;

procedure TBaseReportDM.ExportPDFToStream(Report: TppReport; Str: TStream);
var
  Device: TppPDFDevice;
begin
  Report.ShowPrintDialog := False;
  Report.BackgroundPrintSettings.Enabled := False;
  Report.BackgroundPrintSettings.Active := False;
  Report.PDFSettings.Author := FCompanyName;
  Report.PDFSettings.Creator := 'Q Manager ' + AppVersionShort;
  Report.PDFSettings.Title := '';
  Report.PDFSettings.OpenPDFFile := False;
  Report.PDFSettings.CompressionLevel := TppCompressionLevel(clMax);
  Report.PDFSettings.ImageCompressionLevel := 1; // best quality
  Report.PDFSettings.OptimizeImageExport := True;
  Report.PDFSettings.ScaleImages := False;
  Device := TppPDFDevice.Create(nil);
  try
    Device.PDFSettings := Report.PDFSettings;
    Device.OutputStream := Str;
    Device.Publisher := Report.Publisher;
    Report.PrintToDevices;
  finally
    FreeAndNil(Device);
  end;
end;

function TBaseReportDM.GetDataSets: TList;
var
  i: Integer;
  NumFound: Integer;
begin
  NumFound := 0;
  Result := TList.Create;
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TDataSet then begin
      Result.Add(Components[i]);
      NumFound := NumFound + 1;
    end;
  end;

  if (NumFound=0) then
    raise Exception.Create('No TDataSets found for export.');
end;

{The GetFirstDataSet function below works, but nothing currently uses it.
function TBaseReportDM.GetFirstDataSet: TDataSet;
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TDataSet then begin
      Result := Components[i] as TDataSet;
      Exit;
    end;
  end;
  raise Exception.Create('No TDataSets found for export.');
end;}

procedure TBaseReportDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
var
  DataSets: TList;
  i: Integer;
  CleanupFields: TStringList;
begin
  DataSets := GetDataSets;
  try
    CleanupFields := TStringList.Create;
    try
      CleanupFields.Add(CleanupAllStringFields);
      for i := 0 to DataSets.Count - 1 do begin
        if i > 0 then
          Str.WriteAnsiString(sLineBreak);  // Blank line between datasets
        TDataSet(DataSets[i]).First;
        SaveDelimToStream(TDataSet(DataSets[i]), Str, Delimiter, True, CleanupFields);
      end;
    finally
      FreeAndNil(CleanupFields);
    end;
  finally
    DataSets.Free;
  end;
end;

procedure TBaseReportDM.PrintPreview;
var
  ReportComponent: TppReport;
begin
  ReportComponent := GetReport;
  ReportComponent.ModalPreview := True;
  ReportComponent.OnPreviewFormCreate := PreviewFormCreate;
  ReportComponent.CachePages := True;
  ReportComponent.Print;
end;

procedure TBaseReportDM.PreviewFormCreate(Sender: TObject);
begin
  GetReport.PreviewForm.WindowState := wsMaximized;
  (GetReport.PreviewForm.Viewer as TppViewer).ZoomSetting := zsPageWidth;
end;

function TBaseReportDM.HaveParam(const ParamName: string): Boolean;
begin
  Result := FQueryFields.Values[ParamName] <> '';
end;

function TBaseReportDM.TranslateBrand(Before: string): string;
begin
  Result := StringReplace(Before, '%COMPANY%', FCompanyName, [ rfReplaceAll ]);
end;

procedure TBaseReportDM.SetBrandingStrings;
var
  i: Integer;
  Lab: TppLabel;
begin
  LookUpCompanyName;

  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TppLabel then begin
      Lab := Components[i] as TppLabel;
      Lab.Caption := TranslateBrand(Lab.Caption);
    end;
  end;

  if HaveReportComponent then
    GetReport.PrinterSetup.DocumentName :=
      TranslateBrand(GetReport.PrinterSetup.DocumentName);
end;

procedure TBaseReportDM.LookUpCompanyName;
var
  CompanyQuery: TADOQuery;
begin
  FCompanyName := 'CompanyName';

  Assert(Assigned(FConnection));
  CompanyQuery := TADOQuery.Create(nil);
  try
    CompanyQuery.Connection := FConnection;
    CompanyQuery.ConnectionString;
    CompanyQuery.SQL.Text := 'select description from reference where type=''global'' and code=''coname''';
    CompanyQuery.Open;
    if not CompanyQuery.EOF then
      FCompanyName := CompanyQuery.Fields[0].AsString;
    CompanyQuery.Close;
  finally
    CompanyQuery.Free;
  end;
end;

procedure TBaseReportDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
begin
  MergedPDFFileName := ReportFileName;
end;

function TBaseReportDM.GetLiabilityString(Liability: Integer): string;
var
  DamageLiabilityDescription: array[TDamageLiability] of string;
begin
  DamageLiabilityDescription[dlUQHas] := FCompanyName + ' has liability';
  DamageLiabilityDescription[dlUQNotHave] := FCompanyName + ' has no liability';
  DamageLiabilityDescription[dlOnlyUQ] := 'Only ' +FCompanyName + ' is liable';
  DamageLiabilityDescription[dlUQAndExcavator] := FCompanyName + ' and excavator are liable';
  DamageLiabilityDescription[dlNeither] := 'Neither ' + FCompanyName +  ' nor excavator are liable';
  DamageLiabilityDescription[dlAny] := 'ANY/ALL liability';

  if (Liability < Ord(Low(TDamageLiability))) or (Liability > Ord(High(TDamageLiability))) then
    Result := ''
  else begin
    Result := DamageLiabilityDescription[TDamageLiability(Liability)];
  end;
end;

procedure TBaseReportDM.DumpSQL;
var
  SqlStrings: TStringList;
begin
  if not FSaveSQL then
    Exit;

  if FSQL='' then
    Exit;

  SqlStrings := TStringList.Create;
  try
    SqlStrings.Text := FSQL;
    SqlStrings.SaveToFile('report_query.sql');
  finally
    SqlStrings.Free;
  end;
end;

procedure TBaseReportDM.AttachParamsToStream(Str: TStream);
var
  Text: string;
  TempStr: TMemoryStream;
begin
  SetOutgoingParams;
  Text := FParams.Text;// + END_OF_QM_REPORT_PARAMS;  QMANTWO-784  SR removed for test
  TempStr := TMemoryStream.Create;
  try
    TempStr.WriteString(Text);
    Str.Position := 0;
    TempStr.CopyFrom(Str, Str.Size);

    TMemoryStream(Str).SetSize(SizeOf(TempStr));
    TempStr.Seek(0, soFromBeginning);
    Str.Position := 0;
    TMemoryStream(Str).LoadFromStream(TempStr);
  finally
    TempStr.Free;
  end;
end;

procedure TBaseReportDM.SetParam(const Name, Value: string);
begin
  Assert(Assigned(FParams), 'inherited ValidateParams not called?');
  FParams.Values[Name] := Value;
end;

procedure TBaseReportDM.SetParamBoolean(const Name: string; const Value: Boolean);
begin
  if Value then
    SetParam(Name, '1')
  else
    SetParam(Name, '0')
end;

procedure TBaseReportDM.SetParamDate(const Name: string;
  const Value: TDateTime; Required: Boolean);
begin
  if Required then
    if Value = 0.0 then
      raise EOdEntryRequired.Create(NeedDateMessage + Name);
  SetParam(Name, IsoDateTimeToStr(Value));
end;

procedure TBaseReportDM.SetParamInt(const Name: string; const Value: Integer);
begin
  SetParam(Name, IntToStr(Value));
end;

procedure TBaseReportDM.SetOutgoingParams;
begin
  FreeAndNil(FParams);
  FParams := TStringList.Create;
  // Descendants should populate any outgoing params using SetParam*
end;

function TBaseReportDM.GetBoolean(const ParamName: string): Boolean;
var
  Value: Integer;
begin
  Value := GetInteger(ParamName);
  Result := Value <> 0;
end;

function TBaseReportDM.SafeGetBoolean(const ParamName: string; Default: Boolean): Boolean;
begin
  try
    if HaveParam(ParamName) then
      Result := GetBoolean(ParamName)
    else
      Result := Default;
  except
    Result := Default;
  end;
end;

// The procedures below are used to label start/stop ranges in a consistent way.
// These are made for the most common case (by far) in which the EndDate is
// a date-time of midnight at the end of the period; so an ending datetime of
// 2005-05-10 00:00:00, ends on the 9th as commonly described by end users.

procedure TBaseReportDM.ShowDateRange(const FromDateLabel, ToDateLabel: TppLabel; ToDateInclusive: Boolean);
begin
  FromDateLabel.Caption := FormatDateTime('m/d/yyyy', GetDateTime('StartDate'));
  if ToDateInclusive then
    ToDateLabel.Caption := FormatDateTime('m/d/yyyy', GetDateTime('EndDate'))
  else
    ToDateLabel.Caption := FormatDateTime('m/d/yyyy', GetDateTime('EndDate') - 1);
end;

procedure TBaseReportDM.ShowDateRange(const DateRangeLabel: TppLabel; const StartDateParam: string; const EndDateParam: string; Caption: string; ToDateInclusive: Boolean; IncludeHours: Boolean);

var
  BeginningOfTime, EndOfTime: TDateTime;

  function DateStrOrDash(DateParam: string; IncludeHours: Boolean; ToDateInclusive: Boolean = False): string;
  var
    D: TDateTime;
  begin
    D := SafeGetDateTime(DateParam);
    if (D = BeginningOfTime) or (D = EndOfTime) then
      Result := '-'
    else
      if ToDateInclusive then
        Result := FormatDateTime(IfThen(IncludeHours, 'mm/dd/yyyy hh:mm', 'mm/dd/yyyy'), D - 1)
      else
        Result := FormatDateTime(IfThen(IncludeHours, 'mm/dd/yyyy hh:mm', 'mm/dd/yyyy'), D)
  end;

var
  ToDateLabel: TppLabel;
  LastPos: Single;
  FromText: string;
begin
  BeginningOfTime := IsoStrToDate('1899-12-30');
  EndOfTime := IsoStrToDate('2500-01-01');

  FromText := IfThen(IsEmpty(Caption), 'From:', Caption);
  ToDateLabel := CreateLabel(DateRangeLabel);
  ShowText(DateRangeLabel, FromText, DateStrOrDash(StartDateParam, IncludeHours), LastPos);
  ToDateLabel.Left := LastPos + 0.075;
  ShowText(ToDateLabel, 'To:', DateStrOrDash(EndDateParam, IncludeHours, ToDateInclusive))
end;

procedure TBaseReportDM.ShowEmployeeName(const EmployeeLabel: TppLabel; const EmployeeID: Integer; const Caption: string; const LeftPos: Single);
const
  WhereCondition = 'emp_id = %d';
var
  EmployeeName: string;
  UnusedVar: single;
begin
  Assert(Assigned(FConnection));
  EmployeeName := VarToStr(GetFieldValue(FConnection, 'employee', 'short_name', Format(WhereCondition, [EmployeeID])));
  EmployeeName := IfThen(IsEmpty(EmployeeName) and (EmployeeID < 1) , 'All', EmployeeName);
  if LeftPos = -1 then
    ShowText(EmployeeLabel, Caption, EmployeeName)
  else
    ShowText(EmployeeLabel, Caption, EmployeeName, UnusedVar, LeftPos)
end;

procedure TBaseReportDM.ShowEmployeeStatus(const EmployeeStatusLabel: TppLabel; const EmployeeStatus: Integer; const LeftPos: Single = -1);
var
  StatusText: string;
  UnusedVar: single;
begin
  case EmployeeStatus of
    StatusInactive: StatusText := 'Inactive only';
    StatusActive: StatusText := 'Active only';
    StatusAll: StatusText := 'All Employees';
  else
    StatusText := 'Undefined employee status: ' + IntToStr(EmployeeStatus);
  end;
  if LeftPos = -1 then
    ShowText(EmployeeStatusLabel, 'Employees:', StatusText)
  else
    ShowText(EmployeeStatusLabel, 'Employees:', StatusText, UnusedVar, LeftPos)
end;

procedure TBaseReportDM.ShowOfficeName(const OfficeLabel: TppLabel;
  const OfficeID: Integer; const Caption: string);
const
  WhereCondition = 'office_id = %d';
var
  OfficeName: string;
begin
  Assert(Assigned(FConnection));
  OfficeName := VarToStr(GetFieldValue(FConnection, 'office', 'office_name', Format(WhereCondition, [OfficeID])));
  OfficeName := IfThen(IsEmpty(OfficeName) and (OfficeID < 1) , 'All', OfficeName);
  ShowText(OfficeLabel, Caption, OfficeName);
end;

procedure TBaseReportDM.ShowBooleanText(const TitleLabel: TppLabel; const Caption: string; const Value: Boolean);
begin
  ShowText(TitleLabel, Caption, IfThen(Value, 'Yes', 'No'));
end;

procedure TBaseReportDM.ShowDateText(const TitleLabel: TppLabel;
  const Caption: string; const Value: TDateTime; const IncludeTime: Boolean);
begin
  if IncludeTime then
    ShowText(TitleLabel, Caption, FormatDateTime('m/d/yyyy h:nn am/pm', Value))
  else
    ShowText(TitleLabel, Caption, FormatDateTime('m/d/yyyy', Value));
end;

procedure TBaseReportDM.ShowHierarchyOption(const HierarchyOptionLabel: TppLabel; const HierarchyOption: Integer);
var
  HierarchyText: string;
begin
  case HierarchyOption of
    HierarchyOptionLimitTo1..HierarchyOptionFullDepth-1:
      HierarchyText := Format('Hierarchy, Limit to %d %s',
        [HierarchyOption, AddSIfNot1(HierarchyOption, 'level')]);
    HierarchyOptionFullDepth:       HierarchyText := 'Hierarchy, Full Depth';
    HierarchyOptionFlatByPC:        HierarchyText := 'Flat, by Profit Center';
    HierarchyOptionFlatByPayrollPC: HierarchyText := 'Flat, by Payroll Profit Center';
  else
    HierarchyText := 'Unknown hierarchy option: ' + IntToStr(HierarchyOption);
  end;
  ShowText(HierarchyOptionLabel, 'Hierarchy Selection:', HierarchyText);
end;

procedure TBaseReportDM.ShowText(const TitleLabel: TppLabel; const Caption, Text: string; var LastPos: Single);
var
  TextLabel: TppLabel;
begin
  TextLabel := CreateLabel(TitleLabel);
  RemoveBold(TitleLabel);
  AddBold(TextLabel);

  TextLabel.Top := TextLabel.Top + 0.002;

  TitleLabel.Caption := Caption;
  TextLabel.Caption := Text;

  AdjustLabelPosition(TextLabel, TitleLabel, 0.025);
  LastPos := TextLabel.Left + TextLabel.Width;
end;

procedure TBaseReportDM.ShowText(const TitleLabel: TppLabel; const Caption, Text: string; var LastPos: Single; LeftPosition: Single);
var
  TextLabel: TppLabel;
begin
  TextLabel := CreateLabel(TitleLabel);
  RemoveBold(TitleLabel);
  AddBold(TextLabel);

  TextLabel.Top := TextLabel.Top + 0.002;

  TitleLabel.Caption := Caption;
  TextLabel.Caption := Text;

  AdjustLabelPosition(TextLabel, LeftPosition);
  LastPos := TextLabel.Left + TextLabel.Width;
end;

procedure TBaseReportDM.ShowText(const TitleLabel: TppLabel; const Caption, Text: string);
var
  UnusedVar: Single;
begin
  ShowText(TitleLabel, Caption, Text, UnusedVar);
end;

procedure TBaseReportDM.ShowText(const TitleLabel: TppLabel; Text: string);
var
  UnusedVar: Single;
begin
  ShowText(TitleLabel, TitleLabel.Caption, Text, UnusedVar);
end;

function TBaseReportDM.OutputFolder: string;
begin
  Assert(Assigned(FTempFolder));
  Result := FTempFolder.FullPathName;
end;

end.

