inherited NewTicketDetailDM: TNewTicketDetailDM
  OldCreateOrder = True
  Height = 274
  Width = 375
  object MasterDS: TDataSource
    DataSet = Master
    OnDataChange = MasterDSDataChange
    Left = 118
    Top = 27
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = MasterPipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Ticket Detail'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowAutoSearchDialog = True
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 264
    Top = 24
    Version = '18.03'
    mmColumnWidth = 0
    DataPipelineName = 'MasterPipe'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 13758
      mmPrintPosition = 0
      object ppShape2: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape2'
        Brush.Color = 14737632
        ParentWidth = True
        mmHeight = 13758
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        HyperlinkEnabled = False
        DataField = 'ticket_number'
        DataPipeline = MasterPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 20
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'MasterPipe'
        mmHeight = 8731
        mmLeft = 4498
        mmTop = 2381
        mmWidth = 71173
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        Caption = 'Received Date:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 135202
        mmTop = 794
        mmWidth = 19315
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText2'
        HyperlinkEnabled = False
        DataField = 'transmit_date'
        DataPipeline = MasterPipe
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'MasterPipe'
        mmHeight = 3704
        mmLeft = 156634
        mmTop = 794
        mmWidth = 33867
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label2'
        HyperlinkEnabled = False
        Caption = 'Due Date:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 135202
        mmTop = 5027
        mmWidth = 12435
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText3: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText3'
        HyperlinkEnabled = False
        DataField = 'due_date'
        DataPipeline = MasterPipe
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'MasterPipe'
        mmHeight = 3704
        mmLeft = 156634
        mmTop = 4763
        mmWidth = 33867
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label4'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Assigned To:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 135202
        mmTop = 8996
        mmWidth = 19579
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText4: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText4'
        HyperlinkEnabled = False
        DataField = 'short_name'
        DataPipeline = MasterPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'MasterPipe'
        mmHeight = 3704
        mmLeft = 156634
        mmTop = 8996
        mmWidth = 44450
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5027
      mmPrintPosition = 0
      object ppSubReport1: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'SubReport1'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'DetailPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = DetailPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Ticket Detail'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '18.03'
          mmColumnWidth = 0
          DataPipelineName = 'DetailPipe'
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 13229
            mmPrintPosition = 0
            object ppRegion2: TppRegion
              DesignLayer = ppDesignLayer2
              UserName = 'Region2'
              ParentWidth = True
              mmHeight = 5821
              mmLeft = 0
              mmTop = 0
              mmWidth = 203200
              BandType = 4
              LayerName = Foreground1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppShape4: TppShape
                DesignLayer = ppDesignLayer2
                UserName = 'Shape4'
                Brush.Color = 14737632
                Pen.Style = psClear
                mmHeight = 5292
                mmLeft = 123561
                mmTop = 265
                mmWidth = 79375
                BandType = 4
                LayerName = Foreground1
              end
              object ppDBText14: TppDBText
                DesignLayer = ppDesignLayer2
                UserName = 'DBText14'
                HyperlinkEnabled = False
                DataField = 'client_code'
                DataPipeline = DetailPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 3704
                mmLeft = 2646
                mmTop = 1058
                mmWidth = 35190
                BandType = 4
                LayerName = Foreground1
              end
              object ppDBText5: TppDBText
                DesignLayer = ppDesignLayer2
                UserName = 'DBText5'
                HyperlinkEnabled = False
                DataField = 'status'
                DataPipeline = DetailPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 3704
                mmLeft = 41540
                mmTop = 1058
                mmWidth = 27781
                BandType = 4
                LayerName = Foreground1
              end
              object myDBCheckBox1: TmyDBCheckBox
                DesignLayer = ppDesignLayer2
                UserName = 'DBCheckBox1'
                CheckBoxColor = clWindowText
                BooleanFalse = 'False'
                BooleanTrue = 'True'
                DataPipeline = DetailPipe
                DataField = 'high_profile'
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 4233
                mmLeft = 78846
                mmTop = 794
                mmWidth = 3704
                BandType = 4
                LayerName = Foreground1
              end
              object ppDBText16: TppDBText
                DesignLayer = ppDesignLayer2
                UserName = 'DBText16'
                HyperlinkEnabled = False
                DataField = 'qty_marked'
                DataPipeline = DetailPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 3704
                mmLeft = 108744
                mmTop = 1058
                mmWidth = 12171
                BandType = 4
                LayerName = Foreground1
              end
              object myDBCheckBox2: TmyDBCheckBox
                DesignLayer = ppDesignLayer2
                UserName = 'DBCheckBox2'
                CheckBoxColor = clWindowText
                BooleanFalse = 'False'
                BooleanTrue = 'True'
                DataPipeline = DetailPipe
                DataField = 'high_profile'
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 4233
                mmLeft = 131498
                mmTop = 794
                mmWidth = 3704
                BandType = 4
                LayerName = Foreground1
              end
              object ppDBText18: TppDBText
                DesignLayer = ppDesignLayer2
                UserName = 'DBText18'
                HyperlinkEnabled = False
                DataField = 'closed_date'
                DataPipeline = DetailPipe
                DisplayFormat = 'm/d/yyyy h:nn am/pm'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'DetailPipe'
                mmHeight = 3704
                mmLeft = 145257
                mmTop = 1058
                mmWidth = 33867
                BandType = 4
                LayerName = Foreground1
              end
              object ppLine8: TppLine
                DesignLayer = ppDesignLayer2
                UserName = 'Line8'
                Position = lpLeft
                Weight = 0.750000000000000000
                mmHeight = 5821
                mmLeft = 70908
                mmTop = 0
                mmWidth = 2117
                BandType = 4
                LayerName = Foreground1
              end
              object ppLine12: TppLine
                DesignLayer = ppDesignLayer2
                UserName = 'Line12'
                Position = lpLeft
                Weight = 0.750000000000000000
                mmHeight = 5821
                mmLeft = 123561
                mmTop = 0
                mmWidth = 794
                BandType = 4
                LayerName = Foreground1
              end
              object ppLine13: TppLine
                DesignLayer = ppDesignLayer2
                UserName = 'Line13'
                Position = lpLeft
                Weight = 0.750000000000000000
                mmHeight = 5821
                mmLeft = 142875
                mmTop = 0
                mmWidth = 794
                BandType = 4
                LayerName = Foreground1
              end
              object ppLine7: TppLine
                DesignLayer = ppDesignLayer2
                UserName = 'Line7'
                Position = lpLeft
                Weight = 0.750000000000000000
                mmHeight = 5821
                mmLeft = 39158
                mmTop = 0
                mmWidth = 1323
                BandType = 4
                LayerName = Foreground1
              end
              object ppLine11: TppLine
                DesignLayer = ppDesignLayer2
                UserName = 'Line11'
                Position = lpLeft
                Weight = 0.750000000000000000
                mmHeight = 5821
                mmLeft = 90223
                mmTop = 0
                mmWidth = 794
                BandType = 4
                LayerName = Foreground1
              end
            end
          end
          object ppDesignLayers2: TppDesignLayers
            object ppDesignLayer2: TppDesignLayer
              UserName = 'Foreground1'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 30163
      mmPrintPosition = 0
      object ppShape7: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape7'
        Brush.Color = 14737632
        Pen.Style = psClear
        mmHeight = 13229
        mmLeft = 158750
        mmTop = 6879
        mmWidth = 11113
        BandType = 8
        LayerName = Foreground
      end
      object ppShape6: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape6'
        Brush.Color = 14737632
        Pen.Style = psClear
        mmHeight = 7144
        mmLeft = 158750
        mmTop = 0
        mmWidth = 44450
        BandType = 8
        LayerName = Foreground
      end
      object ppShape5: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape5'
        Brush.Style = bsClear
        mmHeight = 20108
        mmLeft = 158750
        mmTop = 0
        mmWidth = 44450
        BandType = 8
        LayerName = Foreground
      end
      object ppShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape1'
        Brush.Color = 14737632
        ParentWidth = True
        Pen.Color = 14737632
        Pen.Style = psClear
        mmHeight = 5292
        mmLeft = 0
        mmTop = 24871
        mmWidth = 203200
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        AutoSize = False
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 158750
        mmTop = 25665
        mmWidth = 41804
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label13'
        HyperlinkEnabled = False
        Caption = '%COMPANY% Ticket Detail'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 1852
        mmTop = 25665
        mmWidth = 48948
        BandType = 8
        LayerName = Foreground
      end
      object ppShape3: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape3'
        mmHeight = 23548
        mmLeft = 0
        mmTop = 0
        mmWidth = 155575
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label5'
        HyperlinkEnabled = False
        Caption = 'Notes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 2646
        mmTop = 1588
        mmWidth = 7408
        BandType = 8
        LayerName = Foreground
      end
      object ppLine1: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line1'
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 12700
        mmTop = 6879
        mmWidth = 139700
        BandType = 8
        LayerName = Foreground
      end
      object ppLine2: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line2'
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 3175
        mmTop = 13229
        mmWidth = 149225
        BandType = 8
        LayerName = Foreground
      end
      object ppLine3: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line3'
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 3175
        mmTop = 19579
        mmWidth = 149225
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label6'
        HyperlinkEnabled = False
        Caption = 
          'M = Marked    A = Aerial    NP = No Plant    NC = No Charge    O' +
          ' = Ongoing'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 55033
        mmTop = 25665
        mmWidth = 93134
        BandType = 8
        LayerName = Foreground
      end
      object ppLine14: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line14'
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 20108
        mmLeft = 169598
        mmTop = 0
        mmWidth = 794
        BandType = 8
        LayerName = Foreground
      end
      object ppLine15: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line15'
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 20108
        mmLeft = 186267
        mmTop = 0
        mmWidth = 794
        BandType = 8
        LayerName = Foreground
      end
      object ppLine16: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line16'
        Weight = 0.750000000000000000
        mmHeight = 2117
        mmLeft = 158750
        mmTop = 6879
        mmWidth = 44450
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label7'
        HyperlinkEnabled = False
        Caption = 'Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 174096
        mmTop = 1588
        mmWidth = 5821
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel8: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label8'
        HyperlinkEnabled = False
        Caption = 'Time'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 192088
        mmTop = 1588
        mmWidth = 5821
        BandType = 8
        LayerName = Foreground
      end
      object ppLine17: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line17'
        Weight = 0.750000000000000000
        mmHeight = 2117
        mmLeft = 158750
        mmTop = 13229
        mmWidth = 44450
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel9: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label9'
        HyperlinkEnabled = False
        Caption = 'Start'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 160338
        mmTop = 8202
        mmWidth = 6085
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label10'
        HyperlinkEnabled = False
        Caption = 'Finish'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 160338
        mmTop = 14552
        mmWidth = 7408
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppGroup3: TppGroup
      BreakName = 'ticket_id'
      DataPipeline = MasterPipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      NewPage = True
      StartOnOddPage = False
      UserName = 'Group3'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'MasterPipe'
      NewFile = False
      object ppGroupHeaderBand3: TppGroupHeaderBand
        BeforeGenerate = ppGroupHeaderBand3BeforeGenerate
        Background.Brush.Style = bsClear
        PrintHeight = phDynamic
        mmBottomOffset = 0
        mmHeight = 183092
        mmPrintPosition = 0
        object ppRegion1: TppRegion
          DesignLayer = ppDesignLayer1
          UserName = 'Region1'
          Brush.Color = 14737632
          ParentWidth = True
          mmHeight = 5821
          mmLeft = 0
          mmTop = 175155
          mmWidth = 203200
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          object ppLabel17: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label17'
            HyperlinkEnabled = False
            Anchors = [atLeft, atBottom]
            AutoSize = False
            Caption = 'When Closed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 145257
            mmTop = 176477
            mmWidth = 23813
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel15: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label15'
            HyperlinkEnabled = False
            Anchors = [atLeft, atBottom]
            AutoSize = False
            Caption = 'Quantity Marked'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            TextAlignment = taRightJustified
            Transparent = True
            mmHeight = 3704
            mmLeft = 93134
            mmTop = 176477
            mmWidth = 27781
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel16: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label16'
            HyperlinkEnabled = False
            Anchors = [atLeft, atBottom]
            AutoSize = False
            Caption = 'High Profile'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            TextAlignment = taCentered
            Transparent = True
            mmHeight = 3704
            mmLeft = 73290
            mmTop = 176477
            mmWidth = 15081
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine6: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line6'
            Position = lpLeft
            Weight = 0.750000000000000000
            mmHeight = 5821
            mmLeft = 39158
            mmTop = 175155
            mmWidth = 794
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine5: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line5'
            Position = lpLeft
            Weight = 0.750000000000000000
            mmHeight = 5821
            mmLeft = 70908
            mmTop = 175155
            mmWidth = 1323
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine4: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line4'
            Position = lpLeft
            Weight = 0.750000000000000000
            mmHeight = 5821
            mmLeft = 90223
            mmTop = 175155
            mmWidth = 794
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine9: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line9'
            Position = lpLeft
            Weight = 0.750000000000000000
            mmHeight = 5821
            mmLeft = 123561
            mmTop = 175155
            mmWidth = 794
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine10: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line10'
            Position = lpLeft
            Weight = 0.750000000000000000
            mmHeight = 5821
            mmLeft = 142875
            mmTop = 175155
            mmWidth = 794
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
        end
        object ppLabel12: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label12'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = False
          Caption = 'Client'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 2646
          mmTop = 176477
          mmWidth = 35190
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel14: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label14'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = False
          Caption = 'Status'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 41540
          mmTop = 176477
          mmWidth = 9525
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel3: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label3'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = False
          Caption = 'Closed'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          TextAlignment = taCentered
          Transparent = True
          mmHeight = 3704
          mmLeft = 125942
          mmTop = 176477
          mmWidth = 15081
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ImageMemo: TppMemo
          OnPrint = ImageMemoPrint
          DesignLayer = ppDesignLayer1
          UserName = 'ImageMemo'
          Caption = 'ImageMemo'
          CharWrap = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Courier New'
          Font.Size = 10
          Font.Style = []
          RemoveEmptyLines = False
          Transparent = True
          mmHeight = 172509
          mmLeft = 2381
          mmTop = 1323
          mmWidth = 197644
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          mmLeading = 0
        end
      end
      object ppGroupFooterBand3: TppGroupFooterBand
        Background.Brush.Style = bsClear
        PrintHeight = phDynamic
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object MasterPipe: TppDBPipeline
    DataSource = MasterDS
    OpenDataSource = False
    UserName = 'MasterPipe'
    Left = 195
    Top = 27
  end
  object DetailDS: TDataSource
    DataSet = DetailData
    Left = 118
    Top = 81
  end
  object DetailPipe: TppDBPipeline
    DataSource = DetailDS
    OpenDataSource = False
    UserName = 'DetailPipe'
    Left = 195
    Top = 81
  end
  object MasterSP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_newticketdetail'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@ManagerId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@All'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@LocatorList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8000
      end
      item
        Name = '@PrintedForUserId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 42
    Top = 27
  end
  object BatchPrintedCommand: TADOCommand
    CommandText = 
      'update ticket_batch set'#13#10'  batch_printed = 1'#13#10'where ticket_batch' +
      '_id = :ticket_batch_id'
    Connection = ReportEngineDM.Conn
    Parameters = <
      item
        Name = 'ticket_batch_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
      end>
    Left = 40
    Top = 152
  end
  object DetailData: TADODataSet
    Parameters = <>
    Left = 40
    Top = 88
  end
  object Master: TADODataSet
    Parameters = <>
    Left = 192
    Top = 152
  end
end
