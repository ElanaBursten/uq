# UtiliQuest Q Manager
# Nightly Reports

from report_system import *
Setup(-1)   # run report for yesterday

# ######################################################
# THIS FILE IS THE REPORT CONFIGURATION
# UQ STAFF, PLEASE DO YOUR EDITS HERE, ADD REPORTS, ETC.
# ######################################################

# Lines that start with # are comments.

# Output Base Directory - this can be set to land in
# an intranet location, for example.

# Note that the backslash must be written as a double-backslash
# development location:
# SetBaseDir('\\uq\\reports\\')

# Production location:
SetBaseDir('\\\\utiliweb\\reports\\')

# Manager ID -> Directory and Name translation
# This section tells it how to translate manager IDs to
# top-level directories and file names
# This is here, not in the DB, because you might name
# these things different from the short_names.

#m[1687] = ('Chi', 'Chicago_Manager') -- inactive
#m[330] = ('PA', 'PA_Manager_2') -- inactive
m[211] = ('FAU', 'FAU_HJ_McGinness')
m[212] = ('FNJ', 'FNJ_Steve_Connelly')
m[295] = ('FWP', 'FWP_Manager')
m[340] = ('FCV', 'FCV_Frank_Smith')
m[674] = ('FOL', 'FOL_Manager')
m[692] = ('FJX', 'FJX_Manager')
m[694] = ('FCO', 'Colorado_Manager')
m[697] = ('FXL', 'FXL_Randy_Trimble')
m[836] = ('FBL', 'FBL_Dylan_Barbier')
m[1004] = ('FMW', 'FMW_Joe_Feronti')
m[1005] = ('FMB', 'FMB_Kelly_Hardy')
m[1248] = ('FNC', 'FNC_Clark_Whitcomb')
m[1249] = ('FND', 'FND_Tim_Richter')
m[1250] = ('FSD', 'FSD_Garth_Yarnall')
m[1251] = ('FCS', 'FCS_Joe_Mills')
m[1578] = ('FEF', 'FEF_Manager')
m[1673] = ('Terminated', 'Terminated_Manager')
m[1787] = ('FCL', 'FCL_Dee_Terry')
m[1865] = ('CA', 'c6._Fresno')
m[1866] = ('CA', 'c1._Riverside')
m[1867] = ('CA', 'c7._Richmond')
m[1868] = ('CA', 'c8._San_Jose')
m[1870] = ('CA', 'c2._Santa_Fe_Springs')
m[1871] = ('CA', 'c9._Modesto')
m[1872] = ('CA', 'c3._San_Diego')
m[1873] = ('CA', 'c5._Oxnard')
m[2103] = ('CA', 'zzc3._SanDiego-UQ')
m[2167] = ('FPL', 'FPL_Manager')
m[2233] = ('FNV', 'FNV_Gary_Ligon')
m[2302] = ('FTL', 'FTL_David_Golden')
m[2394] = ('FNL', 'FNL_Manager')
m[2410] = ('FDX', 'FDX_Ray_Williams')
m[2487] = ('FOK', 'FOK_Manager')
m[2540] = ('FAM', 'FAM_Manager')
m[2563] = ('FMS', 'FMS_Manager')
m[2597] = ('FJL', 'FJL_Fred_Jackson')
m[2657] = ('FPK', 'FPK_Andy_Woolard')
m[2729] = ('FDE', 'FDE_Manager')
m[2780] = ('FSA', 'FSA_Manager')
m[2782] = ('FTS', 'FTS_Manager')
m[4417] = ('FAQ', 'FAQ_Manager')
m[2781] = ('FHL', 'FHL_Manager')
m[4872] = ('FGV', 'FGV_Manager')
m[5336] = ('FWI', 'FWI_Jason_Brooks')
m[3849] = ('FCT', 'FCT_Manager')
m[3850] = ('FJT', 'FJT_Manager')
m[6061] = ('330', 'Jeffrey_Sharp')
m[6006] = ('350', '350_Manager')
m[6184] = ('370', 'Bruce_Bender')
m[5989] = ('620', 'Richard_Garza')
m[6010] = ('640', 'Jeremy_Comyford')
m[6024] = ('660', 'Nancy_Forsyth')
m[3876] = ('FCL', 'Randy_Smith')
m[663] = ('FCL', 'Scott_Paulk')
# add more managers here

# Call Center -> Directory Translation
# Use these to send call center reports to the appropriate
# top-level directory.  By making these coordinate with the
# manager settings above, you should be able to
# get all the related reports for both, in to the
# same directories.

c['Atlanta'] = 'FAU'
c['FAM1'] = 'FAM'
c['FAU2'] = 'FAU'
c['FBL1'] = 'FBL'
c['FCL1'] = 'FCL'
c['FCL2'] = 'FCL'
c['FCO1'] = 'FCO'
c['FCO2'] = 'FCO'
c['FCV1'] = 'FCV'
c['FCV2'] = 'FCV'
c['FCV3'] = 'FCV'
c['FDX1'] = 'FDX'
c['FDX2'] = 'FDX'
c['FDX3'] = 'FDX'
c['FEF1'] = 'FEF'
c['FHL1'] = 'FHL'
c['FHL2'] = 'FHL'
c['FJL1'] = 'FJL'
c['FJL2'] = 'FJL'
c['FMB1'] = 'FMB'
c['FDE1'] = 'FDE'
c['FDE2'] = 'FDE'
c['FMS1'] = 'FMS'
c['FMW1'] = 'FMW'
c['FMW2'] = 'FMW'
c['FNL1'] = 'FNL'
c['FNV1'] = 'FNV'
c['FNV2'] = 'FNV'
c['FOK1'] = 'FOK'
c['FOL1'] = 'FOL'
c['FPK1'] = 'FPK'
c['FPK2'] = 'FPK'
c['FPL1'] = 'FPL'
c['FSA1'] = 'FSA'
c['FTL1'] = 'FTL'
c['FTL2'] = 'FTL'
c['FTL3'] = 'FTL'
c['FTS1'] = 'FTS'
c['FWP1'] = 'FWP'
c['FWP3'] = 'FWP'
c['NCA1'] = 'NCA'
c['NewJersey'] = 'NJ'
c['OCC1'] = 'OCC'
c['SCA1'] = 'SCA'
c['SCA2'] = 'SCA'
c['FAQ1'] = 'FAQ'
c['FAQ2'] = 'FAQ'
c['FHL1'] = 'FHL'
c['FGV1'] = 'FGV'
c['FWI2'] = 'FWI'
c['FWI1'] = 'FWI'
c['FJT1'] = 'FJT'
c['FCT1'] = 'FCT'
c['FCT2'] = 'FCT'
c['FCT3'] = 'FCT'
c['300'] = '300'
c['6001'] = '6001'

# Profit Center -> Directory Translation
# Use these to send profit center reports to the appropriate
# top-level directory.  By making these coordinate with the
# manager settings, you should be able to get all the related
# reports for both, in to the same directories.

p['FAM'] = 'FAM'
p['FAQ'] = 'FAQ'
p['FAU'] = 'FAU'
p['FBL'] = 'FBL'
p['FCC'] = 'FCC'
p['FCH'] = 'FCH'
p['FCL'] = 'FCL'
p['FCO'] = 'FCO'
p['FCS'] = 'FCS'
p['FCT'] = 'FCT'
p['FCV'] = 'FCV'
p['FDE'] = 'FDE'
p['FDG'] = 'FDG'
p['FDX'] = 'FDX'
p['FEF'] = 'FEF'
p['FFR'] = 'FFR'
p['FGA'] = 'FGA'
p['FGV'] = 'FGV'
p['FHL'] = 'FHL'
p['FJL'] = 'FJL'
p['FJT'] = 'FJT'
p['FJX'] = 'FJX'
p['FMB'] = 'FMB'
p['FMO'] = 'FMO'
p['FMS'] = 'FMS'
p['FMW'] = 'FMW'
p['FNC'] = 'FNC'
p['FND'] = 'FND'
p['FNJ'] = 'FNJ'
p['FNL'] = 'FNL'
p['FNV'] = 'FNV'
p['FOK'] = 'FOK'
p['FOL'] = 'FOL'
p['FOX'] = 'FOX'
p['FPK'] = 'FPK'
p['FPL'] = 'FPL'
p['FRC'] = 'FRC'
p['FRI'] = 'FRI'
p['FRV'] = 'FRV'
p['FSA'] = 'FSA'
p['FSC'] = 'FSC'
p['FSD'] = 'FSD'
p['FSF'] = 'FSF'
p['FSJ'] = 'FSJ'
p['FTL'] = 'FTL'
p['FTS'] = 'FTS'
p['FTX'] = 'FTX'
p['FWI'] = 'FWI'
p['FWP'] = 'FWP'
p['FXL'] = 'FXL'
p['SLM'] = 'SLM'
p['ZCC'] = 'ZCC'
p['ZFR'] = 'ZFR'
p['ZMO'] = 'ZMO'
p['ZOX'] = 'ZOX'
p['ZRC'] = 'ZRC'
p['ZRI'] = 'ZRI'
p['ZSA'] = 'ZSA'
p['ZSF'] = 'ZSF'
p['ZSJ'] = 'ZSJ'
p['330'] = '300'
p['350'] = '300'
p['370'] = '300'
p['620'] = '6001'
p['640'] = '6001'
p['660'] = '6001'



# ######################################################
# Now, for specific reports:


# Daily Production Reports

# the D is the date this is executed.  So if it's run on Tuesday
# (which we might think of as Monday night), you need to say
# D to get Monday.

SetReportId('Productivity')
SetBaseFileName('Productivity Report (Daily)')

ClearParams()
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'
Params['ManagerID'] = '$'

# Per Bob's request, these are not in a loop, because you expect to customize them.
# But if you don't customize them, the following is much shorter and easier:
# GenerateForAllManagers()

TsvOutput()


GenerateForManager(211)
GenerateForManager(212, 'SpecialParam=Whatever')   # This is how you customize them
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)

# second, the weekly prod reports for each manager
# Note that existing values, like the name of the report, carry over

SetReportId('Productivity')
SetBaseFileName('Productivity Report (Weekly)')

ClearParams()
Params['DateFrom'] = '[StartOfWeek]'
Params['DateTo'] = '[D+1]'
Params['ManagerID'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)

# Daily Productivity Report without NC status

SetReportId('Productivity')
SetBaseFileName('Productivity Report (excl NC)')

ClearParams()
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'
Params['ManagerID'] = '$'
Params['ExcludeStatuses']= 'NC'

GenerateForManager(3876)
GenerateForManager(663)
GenerateForManager(4872)

# Sync Summary Report
# Previous Day

SetReportId('SyncSummary')
SetBaseFileName('Sync Summary Report')

ClearParams()
Params['DateStart'] = '[D]'
Params['DateEnd'] = '[D+1]'
Params['ManagerID'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)

# Current Locator Load
# Point in time

SetReportId('LocatorLoad')
SetBaseFileName('Locator Load Report')

ClearParams()
Params['manager_id'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)


# Tickets Due
# Current Day

SetReportId('TicketsDue')
SetBaseFileName('Tickets Due Report')

ClearParams()
Params['DueDate'] = '[D+1]'
Params['ManagerID'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)
GenerateForManager(3876)
GenerateForManager(663)

# High Profile Report
# Previous Day

SetReportId('HighProfile')
SetBaseFileName('High Profile Report')

ClearParams()
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'
Params['Kind'] = '0'
Params['OfficeId'] = '-1'
Params['ManagerId'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)


# Timesheet Exception Report
# Previous Day

SetReportId('TimesheetExceptionNew')
SetBaseFileName('Timesheet Exceptions')
ClearParams()
Params['StartDate'] = '[D]'
Params['ManagerID'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)

# Late Ticket Report
# Previous Day
SetReportId('LateTicket')
SetBaseFileName('Late Ticket Report')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenter'] = '$'
Params['ManagerID'] = 211
Params['SortBy'] = 'C'

# NOTE - the ManagerID is ignored by the report, its left over from
# an earlier version.

GenerateForCallCenter('Atlanta')
GenerateForCallCenter('FAM1')
GenerateForCallCenter('FAU2')
GenerateForCallCenter('FBL1')
GenerateForCallCenter('FCL1')
GenerateForCallCenter('FCL2')
GenerateForCallCenter('FCO1')
GenerateForCallCenter('FCO2')
GenerateForCallCenter('FCV1')
GenerateForCallCenter('FCV2')
GenerateForCallCenter('FCV3')
GenerateForCallCenter('FDX1')
GenerateForCallCenter('FDX2')
GenerateForCallCenter('FDX3')
GenerateForCallCenter('FEF1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FHL2')
GenerateForCallCenter('FJL1')
GenerateForCallCenter('FJL2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('FMS1')
GenerateForCallCenter('FMW1')
GenerateForCallCenter('FMW2')
GenerateForCallCenter('FNL1')
GenerateForCallCenter('FNV1')
GenerateForCallCenter('FNV2')
GenerateForCallCenter('FOK1')
GenerateForCallCenter('FOL1')
GenerateForCallCenter('FPK1')
GenerateForCallCenter('FPK2')
GenerateForCallCenter('FPL1')
GenerateForCallCenter('FSA1')
GenerateForCallCenter('FTL1')
GenerateForCallCenter('FTL2')
GenerateForCallCenter('FTL3')
GenerateForCallCenter('FTS1')
GenerateForCallCenter('FWP1')
GenerateForCallCenter('FWP3')
GenerateForCallCenter('NCA1')
GenerateForCallCenter('NewJersey')
GenerateForCallCenter('OCC1')
GenerateForCallCenter('SCA1')
GenerateForCallCenter('SCA2')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FGV1')
GenerateForCallCenter('FJT1')
GenerateForCallCenter('FCT1')
GenerateForCallCenter('FCT2')
GenerateForCallCenter('FCT3')
GenerateForCallCenter('FDE1')
GenerateForCallCenter('FDE2')
GenerateForCallCenter('300')
GenerateForCallCenter('6001')


# Tickets Received Report
# Previous Day
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Weekly by term)')

ClearParams()
Params['StartDate'] = '[StartOfWeek]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'

# NOTE - the ManagerID is ignored by the report, its left over from
# an earlier version.

GenerateForCallCenter('Atlanta')
GenerateForCallCenter('FAM1')
GenerateForCallCenter('FAU2')
GenerateForCallCenter('FBL1')
GenerateForCallCenter('FCL1')
GenerateForCallCenter('FCL2')
GenerateForCallCenter('FCO1')
GenerateForCallCenter('FCO2')
GenerateForCallCenter('FCV1')
GenerateForCallCenter('FCV2')
GenerateForCallCenter('FCV3')
GenerateForCallCenter('FDX1')
GenerateForCallCenter('FDX2')
GenerateForCallCenter('FDX3')
GenerateForCallCenter('FEF1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FHL2')
GenerateForCallCenter('FJL1')
GenerateForCallCenter('FJL2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('FMS1')
GenerateForCallCenter('FMW1')
GenerateForCallCenter('FMW2')
GenerateForCallCenter('FNL1')
GenerateForCallCenter('FNV1')
GenerateForCallCenter('FNV2')
GenerateForCallCenter('FOK1')
GenerateForCallCenter('FOL1')
GenerateForCallCenter('FPK1')
GenerateForCallCenter('FPK2')
GenerateForCallCenter('FPL1')
GenerateForCallCenter('FSA1')
GenerateForCallCenter('FTL1')
GenerateForCallCenter('FTL2')
GenerateForCallCenter('FTL3')
GenerateForCallCenter('FTS1')
GenerateForCallCenter('FWP1')
GenerateForCallCenter('FWP3')
GenerateForCallCenter('NCA1')
GenerateForCallCenter('NewJersey')
GenerateForCallCenter('OCC1')
GenerateForCallCenter('SCA1')
GenerateForCallCenter('SCA2')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FGV1')
GenerateForCallCenter('FJT1')
GenerateForCallCenter('FCT1')
GenerateForCallCenter('FCT2')
GenerateForCallCenter('FCT3')
GenerateForCallCenter('FDE1')
GenerateForCallCenter('FDE2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('300')
GenerateForCallCenter('6001')
GenerateForCallCenter('FAQ1')
GenerateForCallCenter('FAQ2')

# On Time Completion
# Previous Day
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '-1'

# NOTE - the ManagerID is ignored by the report, its left over from
# an earlier version.

GenerateForCallCenter('Atlanta')
GenerateForCallCenter('FAM1')
GenerateForCallCenter('FAU2')
GenerateForCallCenter('FBL1')
GenerateForCallCenter('FCL1')
GenerateForCallCenter('FCL2')
GenerateForCallCenter('FCO1')
GenerateForCallCenter('FCO2')
GenerateForCallCenter('FCV1')
GenerateForCallCenter('FCV2')
GenerateForCallCenter('FCV3')
GenerateForCallCenter('FDX1')
GenerateForCallCenter('FDX2')
GenerateForCallCenter('FDX3')
GenerateForCallCenter('FEF1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FHL2')
GenerateForCallCenter('FJL1')
GenerateForCallCenter('FJL2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('FMS1')
GenerateForCallCenter('FMW1')
GenerateForCallCenter('FMW2')
GenerateForCallCenter('FNL1')
GenerateForCallCenter('FNV1')
GenerateForCallCenter('FNV2')
GenerateForCallCenter('FOK1')
GenerateForCallCenter('FOL1')
GenerateForCallCenter('FPK1')
GenerateForCallCenter('FPK2')
GenerateForCallCenter('FPL1')
GenerateForCallCenter('FSA1')
GenerateForCallCenter('FTL1')
GenerateForCallCenter('FTL2')
GenerateForCallCenter('FTL3')
GenerateForCallCenter('FTS1')
GenerateForCallCenter('FWP1')
GenerateForCallCenter('FWP3')
GenerateForCallCenter('NCA1')
GenerateForCallCenter('NewJersey')
GenerateForCallCenter('OCC1')
GenerateForCallCenter('SCA1')
GenerateForCallCenter('SCA2')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FGV1')
GenerateForCallCenter('FJT1')
GenerateForCallCenter('FCT1')
GenerateForCallCenter('FCT2')
GenerateForCallCenter('FCT3')
GenerateForCallCenter('FDE1')
GenerateForCallCenter('FDE2')
GenerateForCallCenter('300')
GenerateForCallCenter('6001')


# Receive Audit Report
# Previous Day

# This is how you set something to run only past a certain time:
# Note that the indentation matters, it determines what is
# under the control of the IF statement.
# In this example, this only runs if the time is after 5 AM.

if HourOfTheDay()>5:
    SetReportId('Audit')
    SetBaseFileName('Receive Audit')
    ClearParams()
    Params['date'] = '[D]'
    Params['call_center'] = '$'

    GenerateForCallCenter('Atlanta')
    GenerateForCallCenter('FAM1')
    GenerateForCallCenter('FAU2')
    GenerateForCallCenter('FBL1')
    GenerateForCallCenter('FCL1')
    GenerateForCallCenter('FCL2')
    GenerateForCallCenter('FCO1')
    GenerateForCallCenter('FCO2')
    GenerateForCallCenter('FCV1')
    GenerateForCallCenter('FCV2')
    GenerateForCallCenter('FCV3')
    GenerateForCallCenter('FDX1')
    GenerateForCallCenter('FDX2')
    GenerateForCallCenter('FDX3')
    GenerateForCallCenter('FEF1')
    GenerateForCallCenter('FHL1')
    GenerateForCallCenter('FHL2')
    GenerateForCallCenter('FJL1')
    GenerateForCallCenter('FJL2')
    GenerateForCallCenter('FMB1')
    GenerateForCallCenter('FMS1')
    GenerateForCallCenter('FMW1')
    GenerateForCallCenter('FMW2')
    GenerateForCallCenter('FNL1')
    GenerateForCallCenter('FNV1')
    GenerateForCallCenter('FNV2')
    GenerateForCallCenter('FOK1')
    GenerateForCallCenter('FOL1')
    GenerateForCallCenter('FPK1')
    GenerateForCallCenter('FPK2')
    GenerateForCallCenter('FPL1')
    GenerateForCallCenter('FSA1')
    GenerateForCallCenter('FTL1')
    GenerateForCallCenter('FTL2')
    GenerateForCallCenter('FTL3')
    GenerateForCallCenter('FTS1')
    GenerateForCallCenter('FWP1')
    GenerateForCallCenter('FWP3')
    GenerateForCallCenter('NCA1')
    GenerateForCallCenter('NewJersey')
    GenerateForCallCenter('OCC1')
    GenerateForCallCenter('SCA1')
    GenerateForCallCenter('SCA2')
    GenerateForCallCenter('FAQ1')
    GenerateForCallCenter('FHL1')
    GenerateForCallCenter('FGV1')
    GenerateForCallCenter('FJT1')
    GenerateForCallCenter('FCT1')
    GenerateForCallCenter('FCT2')
    GenerateForCallCenter('FCT3')
    GenerateForCallCenter('FDE1')
    GenerateForCallCenter('FDE2')
    GenerateForCallCenter('300')
    GenerateForCallCenter('6001')

# Initial Status Report
# Previous Day
SetReportId('InitialStatus')
SetBaseFileName('Initial Status Report')
ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenter'] = '$'

GenerateForCallCenter('Atlanta')
GenerateForCallCenter('FAM1')
GenerateForCallCenter('FAU2')
GenerateForCallCenter('FBL1')
GenerateForCallCenter('FCL1')
GenerateForCallCenter('FCL2')
GenerateForCallCenter('FCO1')
GenerateForCallCenter('FCO2')
GenerateForCallCenter('FCV1')
GenerateForCallCenter('FCV2')
GenerateForCallCenter('FCV3')
GenerateForCallCenter('FDX1')
GenerateForCallCenter('FDX2')
GenerateForCallCenter('FDX3')
GenerateForCallCenter('FEF1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FHL2')
GenerateForCallCenter('FJL1')
GenerateForCallCenter('FJL2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('FMS1')
GenerateForCallCenter('FMW1')
GenerateForCallCenter('FMW2')
GenerateForCallCenter('FNL1')
GenerateForCallCenter('FNV1')
GenerateForCallCenter('FNV2')
GenerateForCallCenter('FOK1')
GenerateForCallCenter('FOL1')
GenerateForCallCenter('FPK1')
GenerateForCallCenter('FPK2')
GenerateForCallCenter('FPL1')
GenerateForCallCenter('FSA1')
GenerateForCallCenter('FTL1')
GenerateForCallCenter('FTL2')
GenerateForCallCenter('FTL3')
GenerateForCallCenter('FTS1')
GenerateForCallCenter('FWP1')
GenerateForCallCenter('FWP3')
GenerateForCallCenter('NCA1')
GenerateForCallCenter('NewJersey')
GenerateForCallCenter('OCC1')
GenerateForCallCenter('SCA1')
GenerateForCallCenter('SCA2')
GenerateForCallCenter('FAQ1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FGV1')
GenerateForCallCenter('FJT1')
GenerateForCallCenter('FCT1')
GenerateForCallCenter('FCT2')
GenerateForCallCenter('FCT3')
GenerateForCallCenter('FDE1')
GenerateForCallCenter('FDE2')
GenerateForCallCenter('300')


# Tickets Received Report
# Month To Current Day

SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received')
ClearParams()
Params['StartDate'] = '[StartOfMonth]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'

# To show per-client totals, turn this on:
# Params['ClientList'] = '*'

# Special example: generating a combined report for multiple centers
# The first parameter still needs to be a particular center;
# it will be used to determine the top-level directory
# It will also cause the output file to be named FCL1;
# tell us (Oasis Digital) if you want the flexibility to change that.

GenerateForCallCenter('FCL1', 'CallCenterList=FCL1,FCL2')

# normal ones:
GenerateForCallCenter('Atlanta', 'ClientList=ATL01,ATL02,BGAWA,BGAWC,BGAWM,BGAWR,BGAWS,CCAST1,CCAST2,CRT01,CTC04,GP420,PTC01')
GenerateForCallCenter('FAM1', 'ClientList=BEMC01')
GenerateForCallCenter('FAU2')
GenerateForCallCenter('FBL1')
GenerateForCallCenter('FCO1')
GenerateForCallCenter('FCO2')
GenerateForCallCenter('FCV1')
GenerateForCallCenter('FCV2')
GenerateForCallCenter('FCV3')
GenerateForCallCenter('FDX1')
GenerateForCallCenter('FDX2')
GenerateForCallCenter('FDX3')
GenerateForCallCenter('FDE1')
GenerateForCallCenter('FDE2')
GenerateForCallCenter('FEF1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FHL2')
GenerateForCallCenter('FJL1')
GenerateForCallCenter('FJL2')
GenerateForCallCenter('FMB1')
GenerateForCallCenter('FMS1')
GenerateForCallCenter('FMW1')
GenerateForCallCenter('FMW2')
GenerateForCallCenter('FNL1')
GenerateForCallCenter('FNV1')
GenerateForCallCenter('FNV2')
GenerateForCallCenter('FOK1')
GenerateForCallCenter('FOL1')
GenerateForCallCenter('FPK1')
GenerateForCallCenter('FPK2')
GenerateForCallCenter('FPL1')
GenerateForCallCenter('FSA1')
GenerateForCallCenter('FTL1')
GenerateForCallCenter('FTL2')
GenerateForCallCenter('FTL3')
GenerateForCallCenter('FTS1')
GenerateForCallCenter('FWP1')
GenerateForCallCenter('FWP3')
GenerateForCallCenter('NCA1')
GenerateForCallCenter('NewJersey')
GenerateForCallCenter('OCC1')
GenerateForCallCenter('SCA1')
GenerateForCallCenter('SCA2')
GenerateForCallCenter('FAQ1')
GenerateForCallCenter('FHL1')
GenerateForCallCenter('FGV1')
GenerateForCallCenter('FJT1')
GenerateForCallCenter('FCT1')
GenerateForCallCenter('FCT2')
GenerateForCallCenter('FCT3')
GenerateForCallCenter('300')


# EMailed HP Ticket Report
# Point in time
# just one center

SetReportId('EmailedHPTickets')
SetBaseFileName('Emailed HP Tickets')
ClearParams()
Params['CallCenter'] = '$'

GenerateForCallCenter('FCL1')

#Fax Status Report
SetReportId('FaxStatus')
SetBaseFileName('Fax Status Report (Daily)')

ClearParams()
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'
Params['CallCenter'] = '$'

GenerateForCallCenter('FCO1')

#No Response Ticket Report
SetReportId('NoResponseTickets')
SetBaseFileName('No Response Ticket Report')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'

GenerateForCallCenter('300')



## Damage Open Claims Report
SetReportId('DamageList')
SetBaseFileName('Open Claims')

ClearParams()
Params['ProfitCenter'] = '$'
Params['ClaimStatus'] = '1'
Params['InvCodes']='---,EXPECT,DISPUTED,DENIED,CLOSCORP'

GenerateForProfitCenter('FAM')
GenerateForProfitCenter('FAQ')
GenerateForProfitCenter('FAU')
GenerateForProfitCenter('FBL')
GenerateForProfitCenter('FCC')
GenerateForProfitCenter('FCH')
GenerateForProfitCenter('FCL')
GenerateForProfitCenter('FCO')
GenerateForProfitCenter('FCS')
GenerateForProfitCenter('FCT')
GenerateForProfitCenter('FCV')
GenerateForProfitCenter('FDE')
GenerateForProfitCenter('FDG')
GenerateForProfitCenter('FDX')
GenerateForProfitCenter('FEF')
GenerateForProfitCenter('FFR')
GenerateForProfitCenter('FGA')
GenerateForProfitCenter('FGV')
GenerateForProfitCenter('FHL')
GenerateForProfitCenter('FJL')
GenerateForProfitCenter('FJT')
GenerateForProfitCenter('FJX')
GenerateForProfitCenter('FMB')
GenerateForProfitCenter('FMO')
GenerateForProfitCenter('FMS')
GenerateForProfitCenter('FMW')
GenerateForProfitCenter('FNC')
GenerateForProfitCenter('FND')
GenerateForProfitCenter('FNJ')
GenerateForProfitCenter('FNL')
GenerateForProfitCenter('FNV')
GenerateForProfitCenter('FOK')
GenerateForProfitCenter('FOL')
GenerateForProfitCenter('FOX')
GenerateForProfitCenter('FPK')
GenerateForProfitCenter('FPL')
GenerateForProfitCenter('FRC')
GenerateForProfitCenter('FRI')
GenerateForProfitCenter('FRV')
GenerateForProfitCenter('FSA')
GenerateForProfitCenter('FSC')
GenerateForProfitCenter('FSD')
GenerateForProfitCenter('FSF')
GenerateForProfitCenter('FSJ')
GenerateForProfitCenter('FTL')
GenerateForProfitCenter('FTS')
GenerateForProfitCenter('FTX')
GenerateForProfitCenter('FWI')
GenerateForProfitCenter('FWP')
GenerateForProfitCenter('FXL')
GenerateForProfitCenter('ZCC')
GenerateForProfitCenter('ZFR')
GenerateForProfitCenter('ZMO')
GenerateForProfitCenter('ZOX')
GenerateForProfitCenter('ZRC')
GenerateForProfitCenter('ZRI')
GenerateForProfitCenter('ZSA')
GenerateForProfitCenter('ZSF')
GenerateForProfitCenter('ZSJ')
GenerateForProfitCenter('330')
GenerateForProfitCenter('350')
GenerateForProfitCenter('370')
GenerateForProfitCenter('620')
GenerateForProfitCenter('640')
GenerateForProfitCenter('660')

# ------------------------------------------------------
# Time Sheet Summary Reports
# Previous Day by manager

SetReportId('TimesheetSummaryNew')
SetBaseFileName('Timesheet Summary Report')

ClearParams()
Params['end_date'] = '[ThisSaturday]'
Params['HierarchyDepth'] = '99'
Params['manager_id'] = '$'

GenerateForManager(211)
GenerateForManager(212)
GenerateForManager(295)
GenerateForManager(340)
GenerateForManager(674)
GenerateForManager(692)
GenerateForManager(694)
GenerateForManager(697)
GenerateForManager(836)
GenerateForManager(1004)
GenerateForManager(1005)
GenerateForManager(1248)
GenerateForManager(1249)
GenerateForManager(1250)
GenerateForManager(1251)
GenerateForManager(1578)
GenerateForManager(1673)
GenerateForManager(1787)
GenerateForManager(1865)
GenerateForManager(1866)
GenerateForManager(1867)
GenerateForManager(1868)
GenerateForManager(1870)
GenerateForManager(1871)
GenerateForManager(1872)
GenerateForManager(1873)
GenerateForManager(2103)
GenerateForManager(2167)
GenerateForManager(2233)
GenerateForManager(2302)
GenerateForManager(2394)
GenerateForManager(2410)
GenerateForManager(2487)
GenerateForManager(2540)
GenerateForManager(2563)
GenerateForManager(2597)
GenerateForManager(2657)
GenerateForManager(2729)
GenerateForManager(2780)
GenerateForManager(2782)
GenerateForManager(4417)
GenerateForManager(2781)
GenerateForManager(4872)
GenerateForManager(3849)
GenerateForManager(3850)
GenerateForManager(6061)
GenerateForManager(6006)
GenerateForManager(6184)
GenerateForManager(5989)
GenerateForManager(6010)
GenerateForManager(6024)

# ------------------------------------------------------
# Damage Liability Changes Reports
# weekly results for profit center

SetReportId('DamageLiabilityChanges')
SetBaseFileName('Damage Liability Changes Report (Weekly)')

ClearParams()
Params['DateFrom'] = '[StartOfWeek]'
Params['DateTo'] = '[D+1]'
Params['ProfitCenter'] = '$'
Params['Mode'] = 'UQ Liability Change Reporting'
Params['LiabilityStart'] = '5'
Params['LiabilityEnd'] = '5'
Params['IncludeZeroUQLiab'] = '0'

GenerateForProfitCenter('FCV')
GenerateForProfitCenter('FMB')
GenerateForProfitCenter('FWP')
GenerateForProfitCenter('FXL')
GenerateForProfitCenter('FDE')
GenerateForProfitCenter('FMW')
GenerateForProfitCenter('FPK')
GenerateForProfitCenter('FXL')

# ------------------------------------------------------
# Damage Liability Changes Reports
# daily results for profit center

SetReportId('DamageLiabilityChanges')
SetBaseFileName('Damage Liability Changes Report (Daily)')

ClearParams()
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'
Params['ProfitCenter'] = '$'
Params['Mode'] = 'UQ Liability Change Reporting'
Params['LiabilityStart'] = '5'
Params['LiabilityEnd'] = '5'
Params['IncludeZeroUQLiab'] = '0'

GenerateForProfitCenter('FCV')
GenerateForProfitCenter('FMB')
GenerateForProfitCenter('FWP')
GenerateForProfitCenter('FXL')
GenerateForProfitCenter('FDE')
GenerateForProfitCenter('FMW')
GenerateForProfitCenter('FPK')
GenerateForProfitCenter('FXL')

# ------------------------------------------------------
# On Time Completion
# Previous Day by manager
# for specific term id's


# FCO - Northern Colorado
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report (Xcel)- FNC Manager')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '2105,2181,2106,2187,2062,2183'
Params['ManagerID'] = '1248'
GenerateForCallCenter('FCO1')


# FCO - North Denver
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report (Xcel)- FND Manager')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '2057,2182,2244,2216'
Params['ManagerID'] = '1249'
GenerateForCallCenter('FCO1')

# FCO - South Denver
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report (Comcast)- FSD Manager')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '2068,2069,2070,2071,2241'
Params['ManagerID'] = '1250'
GenerateForCallCenter('FCO1')

# FCO - Colorado Springs
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report (Xcel)- FCS Manager')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '2098,2184,2099,2185'
Params['ManagerID'] = '1251'
GenerateForCallCenter('FCO1')
# ------------------------------------------------------


# ------------------------------------------------------
# Tickets Received Report
# Previous Day by Manager office

# office 620
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Richard_Garza)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '5989'
GenerateForCallCenter('6001')

# office 640
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Jeremy_Comyford)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '6010'
GenerateForCallCenter('6001')

# office 660
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Nancy_Forsyth)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '6024'
GenerateForCallCenter('6001')

# office 330
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Jeffrey_Sharp)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '6061'
GenerateForCallCenter('300')

# office 350
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (David_Sheets)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '6006'
GenerateForCallCenter('300')

# office 370
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Bruce_Bender)')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '6184'
GenerateForCallCenter('300')

# office FCL
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Scott Paulk)')

ClearParams()
Params['StartDate'] = '[StartOfWeek]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '663'
GenerateForCallCenter('FCL1')

# office FCL
SetReportId('TicketsReceived')
SetBaseFileName('Tickets Received (Randy Smith)')

ClearParams()
Params['StartDate'] = '[StartOfWeek]'
Params['EndDate'] = '[D+1]'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = '3876'
GenerateForCallCenter('FCL1')
# ------------------------------------------------------


# ------------------------------------------------------
# On Time Completion
# Previous Day by manager

# Office 330
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 330 Jeffrey_Sharp')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '6061'
GenerateForCallCenter('300')

# Office 350
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 350 David_Sheets')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '6006'
GenerateForCallCenter('300')

# Office 370
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 370 Bruce_Bender')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '6184'
GenerateForCallCenter('300')

# Office 620
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 620 Richard_Garza')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '5989'
GenerateForCallCenter('6001')

# Office 640
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 640 Jeremy_Comyford')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '6010'
GenerateForCallCenter('6001')

# Office 660
SetReportId('OnTimeCompletion')
SetBaseFileName('On Time Completion Report - 660 Nancy_Forsyth')

ClearParams()
Params['DueDate'] = '[D]'
Params['CallCenters'] = '$'
Params['Clients'] = '*'
Params['ManagerID'] = '6024'
GenerateForCallCenter('6001')
# ------------------------------------------------------

# ------------------------------------------------------
# Late Ticket Report
# Previous Day by Manager/Office

# Office 620
SetReportId('LateTicket')
SetBaseFileName('Late Ticket Report - Richard_Garza')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenter'] = '$'
Params['ManagerID'] = '5989'
Params['SortBy'] = 'C'
GenerateForCallCenter('6001')

# Office 640
SetReportId('LateTicket')
SetBaseFileName('Late Ticket Report - Jeremy_Comyford')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenter'] = '$'
Params['ManagerID'] = '6010'
Params['SortBy'] = 'C'
GenerateForCallCenter('6001')

# Office 660
SetReportId('LateTicket')
SetBaseFileName('Late Ticket Report - Nancy_Forsyth')

ClearParams()
Params['StartDate'] = '[D]'
Params['EndDate'] = '[D+1]'
Params['CallCenter'] = '$'
Params['ManagerID'] = '6024'
Params['SortBy'] = 'C'
GenerateForCallCenter('6001')

# ------------------------------------------------------
# ######################################################
# Global reports, not per-center or per-manager
SetTopLevelDir('Systemwide')

SetReportId('OldOpenTickets')
SetBaseFileName('Old Open Tickets')
ClearParams()
Params['TransmitDateStart'] = '[D-360]'
Params['TransmitDateEnd'] = '[D-60]'
Generate()

# This actually runs all the report commands we have generated
Run(1)
# dump the resulting times also
#DumpResults()
