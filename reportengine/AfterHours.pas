unit AfterHours;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, jpeg, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TAfterHoursDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppShape1: TppShape;
    ppDBCalc1: TppDBCalc;
    EmployeeStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu;

procedure TAfterHoursDM.Configure(QueryFields: TStrings);
var
  EmployeeStatus: Integer;
  ManagerId: Integer;
begin
  inherited;

  EmployeeStatus := GetInteger('EmployeeStatus');
  ManagerId := SafeGetInteger('ManagerId', -1);

  with SP do begin
    Parameters.ParamByName('@DateFrom').value       := GetDateTime('DateFrom');
    Parameters.ParamByName('@DateTo').value         := GetDateTime('DateTo');
    Parameters.ParamByName('@ManagerId').value      := ManagerId;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Open;
  end;

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  ShowEmployeeName(ManagerLabel, ManagerId);
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
end;

initialization
  RegisterReport('AfterHours', TAfterHoursDM);

//http://localhost/TestDBapp/ReportEngineCGI.exe/PDF?Report=AfterHours&DateFrom=2002-04-23T00:00:00&DateTo=2002-04-24T00:00:00&ManagerId=547
end.

