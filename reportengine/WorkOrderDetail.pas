unit WorkOrderDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppBands, ppClass, ppCtrls, ppMemo, ppRegion,
  ppReport, ppSubRpt, ppVar, ppStrtch, myChkBox, ppPrnabl, ppCache, ppComm,
  ppRelatv, ppProd, ServerAttachmentDMu, ppDB, ppDBPipe, ppDesignLayer,
  ppParameter,  Data.Win.ADODB;

type
  TWorkOrderDetailDM = class(TBaseReportDM)
    MasterDS: TDataSource;
    MasterPipe: TppDBPipeline;
    NotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    AttachmentsDS: TDataSource;
    AttachmentsPipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppShape2: TppShape;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppDBText3: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppLabel13: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand2: TppSummaryBand;
    NotesSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    NoteMemo: TppDBMemo;
    ppDBText5: TppDBText;
    ppLabel5: TppLabel;
    ppNotesSummary: TppSummaryBand;
    ImagesSubReport: TppSubReport;
    ppChildReportWorkOrderAttachments: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailWorkOrderImages: TppDetailBand;
    ImageRegion: TppRegion;
    ppDBText6: TppDBText;
    ppMemoErrorMessage: TppMemo;
    ppImage: TppImage;
    ppSummaryBand3: TppSummaryBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    DetailHeaderRegion: TppRegion;
    ImageMemo: TppMemo;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppDBText4: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText18: TppDBText;
    myDBCheckBox2: TmyDBCheckBox;
    ppDBText7: TppDBText;
    ppLabel12: TppLabel;
    StatusLabel: TppLabel;
    ppLabel17: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    TicketsPipe: TppDBPipeline;
    TicketsDS: TDataSource;
    TicketsSubReport: TppSubReport;
    ppChildReportFacilities: TppChildReport;
    FacilitySubReportTitleBand: TppTitleBand;
    ppShape4: TppShape;
    TicketsSectionTitle: TppLabel;
    TicketsTicketNumberLabel: TppLabel;
    TicketsTicketFormatLabel: TppLabel;
    TicketsTransmitDatelLabel: TppLabel;
    FacilitySubReportHeaderBand: TppHeaderBand;
    FacilitySubReportDetailBand: TppDetailBand;
    TicketsTicketNumber: TppDBText;
    TicketsTicketFormat: TppDBText;
    TicketsTransmitDate: TppDBText;
    ppDBText8: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    WORemedyDS: TDataSource;
    WORemedyPipe: TppDBPipeline;
    WGLSubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand3: TppTitleBand;
    Inspectionlabel: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBRemedyDescription: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppDesignLayers5: TppDesignLayers;
    ppDesignLayer5: TppDesignLayer;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBFlagColorText: TppDBText;
    ComplianceDueDateLabel: TppLabel;
    WGLAccountNumberLabel: TppLabel;
    PremiseIDLabel: TppLabel;
    CustomerNameLabel: TppLabel;
    WorkAddressLabel: TppLabel;
    LatLongLabel: TppLabel;
    MapLabel: TppLabel;
    QuadMapLabel: TppLabel;
    ppLabel9: TppLabel;
    ppDBComplianceDueDate: TppDBText;
    ppDBCustomerName: TppDBText;
    ppDBWorkAddressStreet: TppDBText;
    ppDBWorkCity: TppDBText;
    ppDBWorkState: TppDBText;
    ppDBWorkZip: TppDBText;
    ppDBWorkLat: TppDBText;
    ppLabel6: TppLabel;
    ppDBWorkLong: TppDBText;
    ppDBMapPage: TppDBText;
    ppDBMapRef: TppDBText;
    ppLabel7: TppLabel;
    ppDBCallerPhone: TppDBText;
    ppDBPremiseID: TppDBText;
    ppDBAccountNumber: TppDBText;
    ppDBClientWOnumber: TppDBText;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDBStatusDate: TppDBText;
    ppDBCGAVisits: TppDBText;
    ppDBBuildingClassDesc: TppDBText;
    ppDBMapStatusDesc: TppDBText;
    ppDBActualMeterLocation: TppDBText;
    ppDBMeterNumber: TppDBText;
    ppDBActualMeterNumber: TppDBText;
    ppDBMercuryRegulator: TppDBText;
    VentClearanceLabel: TppLabel;
    ppDBText12: TppDBText;
    PotentialHazardAlertLabel: TppLabel;
    ppDBAlertFirstName: TppDBText;
    ppDBAlertLastName: TppDBText;
    ppDBAlertOrderNumber: TppDBText;
    ppLabel20: TppLabel;
    ppDBGasLightDesc: TppDBText;
    WorkOrder: TADOStoredProc;
    Tickets: TADODataSet;
    Notes: TADODataSet;
    Attachments: TADODataSet;
    WORemedy: TADODataSet;
    Master: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ppDetailWorkOrderImagesBeforeGenerate(Sender: TObject);
    procedure ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
    procedure ImageMemoPrint(Sender: TObject);
    procedure ppDBFlagColorTextGetText(Sender: TObject; var Text: string);
    procedure ppDBRemedyDescriptionGetText(Sender: TObject; var Text: string);
    procedure ppDetailBand3BeforePrint(Sender: TObject);
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
  protected
    procedure MergePDFReportWithAttachments(const OutputDir: string;
      const ReportFileName: string; var MergedPDFFileName: string); override;
  private
    FileList: TStringList;
    ErrorLog: TStringList;
    Attacher: TServerAttachment;
    ShowAlertContact: Boolean;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  WorkOrderDetailDM: TWorkOrderDetailDM;

implementation

{$R *.dfm}

uses
  OdRbMemoFit, OdMiscUtils, OdRbDownloadedImage, QMConst, OdPdf, WorkOrderImage,
  ppTypes, CVUtils, OdAdoUtils;
  //AdUtils;

{ TWorkOrderDetailDM }

procedure TWorkOrderDetailDM.Configure(QueryFields: TStrings);
var
  WOID: Integer;
  ImageList: string;
  RecsAffected : oleVariant;
begin
  inherited;
  ImageList := SafeGetString('ImageList', '-1');
  if not IsEmpty(ImageList) then
    if not ValidateIntegerCommaSeparatedList(ImageList) then
      raise Exception.Create('The parameter ImageList=' + ImageList +
        ' is not valid.');

  WOID := GetInteger('WOID');
  WorkOrder.ProcedureName := 'RPT_WorkOrderDetail';
  WorkOrder.Parameters.ParamByName('@WOID').Value := WOID;
  WorkOrder.Parameters.ParamByName('@AttachmentList').Value := ImageList;
  WorkOrder.Open;
  Assert(WorkOrder.RecordCount < 2, 'Multiple work orders selected for a ' +
    'single work order report');

  //QMANTWO-533 EB
  Master.Recordset      := WorkOrder.Recordset;
  Tickets.Recordset     := WorkOrder.Recordset.NextRecordset(RecsAffected);
  Notes.Recordset       := WorkOrder.Recordset.NextRecordset(RecsAffected);
  Attachments.Recordset := WorkOrder.Recordset.NextRecordset(RecsAffected);
  WORemedy.Recordset    := WorkOrder.Recordset.NextRecordset(RecsAffected);

  WGLSubReport.Visible := (Master.FieldByName('wo_source').AsString = WOSourceWGL);
  if (Master.FieldByName('wo_source').AsString = WOSourceWGL) then
    StatusLabel.Caption := 'Completion Code';

  NotesSubReport.Visible := SafeGetInteger('IncludeNotes', 1) = 1;

  Attacher := TServerAttachment.Create(WorkOrder.Connection);
  ImagesSubReport.Visible := Attacher.AttachmentsIncludeFileType(Attachments,
    Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));

  if (Attachments.RecordCount > 0) and (not Master.IsEmpty) then begin
    Attacher.DownloadAttachments(Master.FieldByName('wo_id').AsInteger,
      qmftWorkOrder, FileList, ErrorLog, ImageList, OutputFolder, True);
    FilterForNoPDFs(Attachments);
  end;

  AttachmentsDS.DataSet := ReplaceCVs(Master.FieldByName('wo_id').AsInteger,
    qmftWorkOrder, Attachments, FileList, GetReportRegEx(WorkOrder.Connection), IniName);

  ShowAlertContact := False;
end;

procedure TWorkOrderDetailDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  FileList := TStringList.Create;
  ErrorLog := TStringList.Create;
  ppImage.Units := utScreenPixels;
end;

procedure TWorkOrderDetailDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FileList);
  FreeAndNil(ErrorLog);
  FreeAndNil(Attacher);
end;

procedure TWorkOrderDetailDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

procedure TWorkOrderDetailDM.MergePDFReportWithAttachments(const OutputDir,
  ReportFileName: string; var MergedPDFFileName: string);
begin
  MergedPDFFileName := MergePDFs(FileList, OutputDir, ReportFileName, IniName);
end;

procedure TWorkOrderDetailDM.ppDBFlagColorTextGetText(Sender: TObject; var Text: string);
begin
  inherited;
  if IsEmpty(Text) then
    Text := 'AOC Inspection:'
  else if Text = 'ORANGE' then
    Text := Format('Customer Inspection (%s flag):',[Text])
  else if Text = 'RED' then
    Text := Format('Potential Hazard (%s flag):',[Text]);
end;

procedure TWorkOrderDetailDM.ppDBRemedyDescriptionGetText(Sender: TObject;
  var Text: string);
begin
  inherited;
  if WORemedy.FieldByName('work_type').AsString = 'VC' then
    VentClearanceLabel.Text := Master.FieldByName('VentClearanceDistDesc').AsString
  else if WORemedy.FieldByName('work_type').AsString = 'VI' then
    VentClearanceLabel.Text := Master.FieldByName('VentIgnitionDistDesc').AsString
  else
    VentClearanceLabel.Text := '';
end;

procedure TWorkOrderDetailDM.ppDetailBand3BeforePrint(Sender: TObject);
begin
  inherited;
  if WORemedy.FieldByName('alert').AsBoolean then
    ShowAlertContact := True;
end;

procedure TWorkOrderDetailDM.ppDetailWorkOrderImagesBeforeGenerate(
  Sender: TObject);
begin
  ShowDownloadedImage(FileList, ErrorLog, AttachmentsDS.DataSet, ppImage, ppMemoErrorMessage);
end;

procedure TWorkOrderDetailDM.ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
var
  IsXml: Boolean;
begin
  inherited;
  ImageMemo.Lines.Text := RemoveExcessWhitespace(GetDisplayableWorkOrderImage(
    Master.FieldByName('image').AsString, Master.FieldByName('xml_ticket_format').AsString, IsXml));
end;

procedure TWorkOrderDetailDM.ppSummaryBand1BeforePrint(Sender: TObject);
begin
  inherited;
   ppSummaryBand1.Visible := ShowAlertContact;
end;

initialization
  RegisterReport('WorkOrderDetail', TWorkOrderDetailDM);

end.
