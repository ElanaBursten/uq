# UtiliQuest Q Manager
# Run all the reports, and check their success or failure

from report_system import *
import os
import glob

Setup()

# Output goes in dir. Note that the backslash must be written as a double-backslash
dir = 'c:\\temp\\uqreports\\'

# Delphi's representation of day 0
zero_date = '1899-12-30T00:00:00.000'

# the second param means "flat output"
SetBaseDir(dir, True)

# delete all pdf files from dir
existing_files = glob.glob(dir + '*.pdf')
for fn in existing_files:
    os.remove(fn)

# Manager ID to use for employee selection
mgr = 2676

# Manager ID -> Directory and Name translation
m[811] = ('FXL', 'FXL_JuanVogel')
m[2676] = ('', 'Whole Company')

# Call Center -> Directory Translation
c['OCC2'] = 'FXL'

#Profit Center -> Directory Translation
p['FXL'] = 'FXL'

# D is the report date, which is "yesterday"

# Some of the report file names won't make sense, but that is ok for testing
SetReportId('ActiveComputerSync')
Params['ShowDetails'] = '1'
GenerateForManager(mgr)

SetReportId('AfterHours')
Params['DateFrom']='2012-12-01T00:00:00.000'
Params['DateTo']='2013-01-01T00:00:00.000'
Params['EmployeeStatus']='1'
Params['ManagerID']='$'
GenerateForManager(mgr)

SetReportId('Audit')
Params['date'] = '2010-06-14T00:00:00.000'
Params['call_center'] = '$'
GenerateForCallCenter('OCC2')

SetReportId('BillableDamages')
Params['BillingFromDate'] = '2012-12-01T00:00:00.000'
Params['BillingToDate'] = '2013-01-01T00:00:00.000'
Params['ProfitCenter'] = ''
GenerateForProfitCenter('FXL')

SetReportId('BillingException')
Params['BeginDate'] = '2012-12-23'
Params['EndDate'] = '2012-12-30'
Params['Units'] = '1'
Params['CallCenters'] = 'NewJersey'
GenerateForCallCenter('NewJersey')

SetReportId('BillingRate')
Params['call_center'] = '$'
GenerateForCallCenter('OCC2')

SetReportId('BlastingTickets')
Params['DateFrom'] = '2010-06-11T00:00:00.000'
Params['DateTo'] = '2010-06-19T00:00:00.000'
GenerateForCallCenter('3003')

SetReportId('DailyClientClosing')
Params['date'] = '[D]'
Params['sync_to_date'] = '[Tomorrow]'
Params['xmit_from_date'] = zero_date
Params['xmit_to_date'] = zero_date
Params['call_center'] = 'NewJersey'
Params['client_code'] = 'CAM'
Params['locate_closed'] = '0'
GenerateForCallCenter('NewJersey')

SetReportId('DailyClientClosingATMOS')
Params['sync_from_date'] = '[D]'
Params['sync_to_date'] = '[Tomorrow]'
Params['xmit_from_date'] = zero_date
Params['xmit_to_date'] = zero_date
Params['call_center'] = 'NewJersey'
Params['client_code_list'] = 'CAM,NJN'
Params['locate_closed'] = '0'
Params['show_status_history'] = '1'
GenerateForCallCenter('NewJersey')

SetReportId('DailyClientClosingCCT')
Params['date'] = '[D]'
Params['sync_to_date'] = '[Tomorrow]'
Params['xmit_from_date'] = zero_date
Params['xmit_to_date'] = zero_date
Params['call_center'] = 'NewJersey'
Params['client_code_list'] = 'CAM,NJN'
Params['locate_closed'] = '0'
Params['show_status_history'] = '1'
GenerateForCallCenter('NewJersey')

SetReportId('DailyClientClosingNNG')
Params['date'] = '[D]'
Params['sync_to_date'] = '[Tomorrow]'
Params['xmit_from_date'] = zero_date
Params['xmit_to_date'] = zero_date
Params['call_center'] = 'NewJersey'
Params['client_code_list'] = 'CAM,NJN'
Params['locate_closed'] = '0'
Params['show_status_history'] = '1'
GenerateForCallCenter('NewJersey')

# As of 12/21/2012 this report only contains a "This report is not yet complete." page.
#SetReportId('DailySales')
#Params['StartDate'] = '[D]'
#Params['EndDate'] = '[D+1]'
#Params['ManagerID'] = '$'
#GenerateForManager(mgr)

SetReportId('DamageApprovalDetails')
Params['DamageID'] = '20001'
GenerateForCallCenter('OCC2')

SetReportId('DamageDefaultEstimate')
Params['ProfitCenter'] = '$'
Params['FacilityType'] = ''
Params['UtilityCo'] = ''
GenerateForProfitCenter('FXL')

SetReportId('DamageDetails')
Params['DamageID'] = '20001'
Params['DamageImageList'] = '2000002,2000003'
Params['TicketImageList'] = '2000000,2000001'
GenerateForProfitCenter('FXL')

SetReportId('DamageIDException')
Params['StartDamageID'] = '20001'
Params['EndDamageID'] = '31000'
GenerateForProfitCenter('FXL')

SetReportId('DamageInvoice')
Params['ReceivedFromDate'] = '2012-12-01T00:00:00.000'
Params['ReceivedToDate'] = '2013-01-01T00:00:00.000'
Params['ProfitCenter'] = ''
Params['PaidFromDate'] = zero_date
Params['PaidToDate'] = zero_date
Params['ShowPaidAmount'] = '1'
Params['ShowEstimates'] = '1'
Params['InvoiceStatus'] = '0'
GenerateForProfitCenter('FXL')

SetReportId('DamageLiabilityChanges')
Params['DateFrom'] = '2012-12-01T00:00:00.000'
Params['DateTo'] = '[Tomorrow]'
Params['ProfitCenter'] = ''
Params['Mode'] = 'UQ/STS Liability Change Reporting'
Params['LiabilityStart'] = '5'
Params['LiabilityEnd'] = '5'
Params['IncludeZeroUQLiab'] = '1'
Params['MinChange'] = '0'
Params['ExcludeInitialEst'] = '0'
Params['IncludeThirdParty'] = '0'
Params['IncludeLitigation'] = '0'
GenerateForProfitCenter('FXL')

SetReportId('DamageList')
Params['StartDate'] = '2012-01-01'
Params['EndDate'] = '2013-01-01'
Params['StartDueDate'] = zero_date
Params['EndDueDate'] = zero_date
Params['StartNotifyDate'] = zero_date
Params['EndNotifyDate'] = zero_date
Params['ProfitCenter'] = ''
Params['IncludeUQResp'] = '0'
Params['IncludeExcvResp'] = '0'
Params['IncludeSpecialResp'] = '0'
Params['Attachments'] = '0'
Params['Invoices'] = '0'
Params['ClaimStatus'] = 'ALL'
Params['Estimate'] = '1'
Params['InvCodes'] = '---,EXPECT,DISPUTED,DENIED,NOPAY,MULTINV,WRITOFF,PAID,CLOSCORP,PENDAPP'
GenerateForProfitCenter('FXL')

SetReportId('DamageProductivity')
Params['DateFrom'] = '2012-01-01'
Params['DateTo'] = '2013-01-01'
Params['Manager'] = '$'
Params['InvestigatorId'] = '-1'
Params['DamageTypes'] = 'APPROVED,COMPLETED'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('DamagesByLocator')
Params['DateFrom'] = '2012-10-01T00:00:00.000'
Params['DateTo'] = '2013-01-01T00:00:00.000'
Params['ProfitCenter'] = ''
Params['Liability'] = '5'
Params['EmployeeID'] = '2676'
Params['SingleEmployee'] = '0'
Params['EmployeeStatus'] = '1'
GenerateForProfitCenter('FXL')

SetReportId('DamageShort')
Params['DamageID'] = '20001'
GenerateForProfitCenter('FXL')

SetReportId('DamagesPerLocates')
Params['DateFrom'] = '2012-10-01T00:00:00.000'
Params['DateTo'] = '2013-01-01T00:00:00.000'
Params['ProfitCenter'] = '$'
Params['ClientID'] = '-1'
Params['Liability'] = '5'
Params['LocateRatio'] = '1000'
Params['ShowLocatorDetail'] = '1'
GenerateForProfitCenter('FXL')

SetReportId('EmailedHPTickets')
Params['CallCenter'] = '$'
GenerateForCallCenter('3003')

SetReportId('EmailStatus')
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[Tomorrow]'
Params['CallCenter'] = 'NewJersey'
GenerateForCallCenter('NewJersey')

SetReportId('EmployeeActivity')
Params['StartDate'] = '2012-12-24'
Params['EndDate'] = '2012-12-31'
Params['ManagerID'] = '8532'
Params['EmployeeStatus'] = '1'
Params['EmployeeId'] = '-1'
Params['IncludeLSA'] = '1'
GenerateForManager(mgr)

SetReportId('EmployeeActivitySummary')
Params['StartDate'] = '2012-12-24'
Params['EndDate'] = '2012-12-31'
Params['ManagerID'] = '8532'
Params['EmployeeStatus'] = '1'
Params['EmployeeId'] = '-1'
Params['IncludeLSA'] = '0'
GenerateForManager(mgr)

SetReportId('EmployeeCovStatus')
Params['ManagerID'] = '$'
Params['ShowDetails'] = '1'
GenerateForManager(mgr)

SetReportId('EmployeeRights')
Params['StartDate'] = zero_date
Params['EndDate'] = zero_date
Params['OrigGrantStartDate'] = zero_date
Params['OrigGrantEndDate'] = zero_date
Params['ManagerID'] = '8532'
Params['EmployeeStatus'] = '1'
Params['EmployeeID'] = '-1'
Params['RightStatus'] = '1'
GenerateForManager(mgr)

SetReportId('Employees')
Params['ManagerID'] = '$'
Params['EmployeeStatus'] = '1'
Params['EmployeeTypeList'] = '8,9,10,11'
GenerateForManager(mgr)

SetReportId('EmployeesInTraining')
Params['StartDate'] = '2012-01-01T00:00:00.000'
Params['EndDate'] = '2013-01-01T00:00:00.000'
Params['ManagerID'] = '$'
Params['ShowDetails'] = '1'
GenerateForManager(mgr)

#As of 7/2012 ExcavatorCall fails. Find out if it is still used then fix it or remove it.
# ERROR  Cannot delete one or more files matching %Temp%\{DC1DD131-8F10-4494-98B5-1059D2A95691}\*.*.  The process cannot access the file because it is being used by another process
#SetReportId('ExcavatorCall')
#GenerateForManager(mgr)

SetReportId('FaxStatus')
Params['DateFrom'] = '2012-01-01'
Params['DateTo'] = '2013-01-01'
Params['CallCenter'] = 'NewJersey'
GenerateForCallCenter('NewJersey')

SetReportId('HighLevelProductivity')
Params['DateFrom'] = '2012-01-01T00:00:00.000'
Params['DateTo'] = '2013-01-01T00:00:00.000'
Params['ProfitCenters'] = ''
GenerateForProfitCenter('FXL')

SetReportId('HighProfile')
Params['DateFrom'] = '2010-01-01'
Params['DateTo'] = '2012-12-31'
Params['Kind'] = '0'
Params['OfficeId'] = '-1'
Params['ManagerId'] = '$'
GenerateForManager(mgr)

SetReportId('InitialStatus')
Params['StartDate'] = '2011-01-01'
Params['EndDate'] = '2011-07-01'
Params['CallCenter'] = '3003'
Params['Arrival'] = '0'
GenerateForCallCenter('3003')

SetReportId('InvoiceIDException')
Params['StartDate'] = '2012-10-01'
Params['EndDate'] = '[Tomorrow]'
GenerateForCallCenter('NewJersey')

SetReportId('LateTicket')
Params['StartDate'] = '2011-01-01'
Params['EndDate'] = '2011-07-01'
Params['CallCenter'] = '3003'
Params['ManagerID'] = mgr
Params['EmployeeStatus'] = '1'
Params['SortBy'] = 'C'
GenerateForCallCenter('3003')

SetReportId('LoadSheet')
Params['ManagerID']='$'
Params['Locators']='679,10432'
GenerateForManager(mgr)

SetReportId('LocatesCompletedSummary')
Params['StartDate'] = '2012-01-01'
Params['EndDate'] = '2013-01-01'
Params['ManagerID'] = '$'
Params['EmployeeTypes'] = '*'
GenerateForManager(mgr)

SetReportId('LocateStatusActivity')
Params['DateFrom'] = '2012-01-01'
Params['DateTo'] = '2013-01-01'
Params['SortBy'] = '0'
Params['EmployeeStatus'] = '2'
Params['Office'] = '-1'
Params['Manager'] = '$'
GenerateForManager(mgr)

SetReportId('LocatorLoad')
Params['manager_id'] = '$'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

# As of 7/2012 ManagerStatus fails. Find out if it is still used then fix it or remove it.
# ERROR  Invalid object name '#temp'
#SetReportId('ManagerStatus')
#Params['manager_id'] = '$'
#Params['start_date'] = '[D]'
#Params['end_date'] = '[D+1]'
#GenerateForManager(mgr)

SetReportId('NewTicketDetail')
Params['User'] = '837'
Params['ManagerID'] = '$'
Params['All'] = '1'
Params['Locators'] = '6007,10432'
GenerateForManager(mgr)

SetReportId('NoConflict')
Params['StartDate'] = '[D]'
Params['EndDate'] = '[Tomorrow]'
Params['CallCenter'] = '$'
Params['Statuses'] = 'A,C,FC,FN,FNP,N,NC,NG,NL,NP,S,SSM'
Params['State'] = 'VA'
GenerateForCallCenter('OCC2')

SetReportId('NoResponseTickets')
Params['StartDate'] = '2010-01-01'
Params['EndDate'] = '2011-01-01'
Params['CallCenterList'] = '1421'
GenerateForCallCenter('1421')

SetReportId('OnTimeCompletion')
Params['DueDate'] = '2010-01-01'
Params['EndDueDate'] = '2012-06-30'
Params['CallCenters'] = 'NewJersey'
Params['Clients'] = '3224,3225,3226,3227,3229,3230,3231,3232,3233,3234,3235,3260'
Params['ManagerID'] = mgr
GenerateForCallCenter('NewJersey')

SetReportId('PayrollExceptionAdHoc')
Params['ManagerID'] = '5457'
Params['Criteria'] = 'reg_hours=0'
Params['StartDate'] = '2012-11-01'
Params['EndDate'] = '2012-12-01'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('PayrollExceptionBreaks')
Params['ManagerID'] = '5457'
Params['EmployeeStatus'] = '1'
Params['StartDate'] = '2012-11-01'
Params['EndDate'] = '2012-12-01'
GenerateForManager(mgr)

SetReportId('PayrollExceptionStandard')
Params['manager_id'] = '$'
Params['end_date'] = '2012-12-01'
GenerateForManager(mgr)

SetReportId('PayrollSummary')
Params['pc'] = 'FCL'
Params['end_date'] = '2012-12-01'
GenerateForProfitCenter('FCL')

SetReportId('ProductionHoursByPC')
Params['StartDate'] = '2012-12-01'
Params['EndDate'] = '2013-01-01'
Params['ApproveOnly'] = '0'
Params['FinalOnly'] = '0'
Params['EmpTypes'] = ''
Params['IncludeCallouts'] = '1'
GenerateForProfitCenter('FXL')

SetReportId('Productivity')
Params['DateFrom'] = '2012-12-01'
Params['DateTo'] = '2013-01-01'
Params['ManagerID'] = '212'
Params['LimitCallCenters'] = 'NewJersey'
Params['LimitClients'] = ''
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('ProductivityClient')
Params['DateFrom'] = '[StartOfPrevWeek]'
Params['DateTo'] = '[StartOfWeek]'
Params['ManagerID'] = '$'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('ProfitCenterSetup')
GenerateForManager(mgr)

SetReportId('ResponseLog')
Params['StartDate'] = '[D]'
Params['EndDate'] = '[Tomorrow]'
Params['ManagerID'] = '$'
Params['CallCenterList'] = 'OCC2'
Params['IncludeSuccess'] = '0'
Params['IncludeFailure'] = '1'
GenerateForManager(mgr)

SetReportId('RestrictedUseExemption')
Params['GrantedStart'] = '2012-12-01T00:00:00.000'
Params['GrantedEnd'] = '2013-01-01T00:00:00.000'
Params['RevokedStart'] = zero_date
Params['RevokedEnd'] = zero_date
Params['ExemptionsFor'] = '0'
GenerateForManager(mgr)

SetReportId('StatusGroups')
GenerateForManager(mgr)

SetReportId('SyncSummary')
Params['DateStart'] = '2012-12-01'
Params['DateEnd'] = '2013-01-01'
Params['ManagerID'] = '$'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TicketActivity')
Params['StartDate'] = '2012-12-01'
Params['EndDate'] = '2013-01-01'
Params['ManagerID'] = '$'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TicketDetail')
Params['TicketID'] = '1390'
Params['IncludeNotes'] = '1'
Params['IncludeFacilities'] = '1'
Params['ImageList'] = '2000000,2000001'
GenerateForManager(mgr)

SetReportId('TicketsDue')
Params['DueDate'] = '[D+1]'
Params['ManagerID'] = '$'
Params['PrintTicketNotes'] = '1'
Params['PrintRemarks'] = '1'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TicketsReceived')
Params['StartDate'] = '2010-01-01'
Params['EndDate'] = '2013-01-01'
Params['CallCenterList'] = '$'
Params['ClientList'] = '*'
Params['ManagerID'] = mgr
Params['GroupByTicketType'] = '0'
GenerateForCallCenter('OCC2')

SetReportId('TicketsReceivedSummary')
Params['StartDate'] = '2010-01-01'
Params['EndDate'] = '2013-01-01'
Params['IncludeBreakdownByCC'] = '1'
Params['IncludeNoWorkTickets'] = '1'
GenerateForCallCenter('OCC2')

SetReportId('TimeRuleCheck')
Params['end_date'] = '2012-12-01'
Params['emp_id'] = '4513' # just the Corp office
Params['HierarchyDepth'] = '99'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TimesheetAudit')
Params['SheetsForManagerID'] = '5457' # just for the FCL manager
Params['WorkStart'] = '2012-11-25'
Params['WorkEnd'] = '2012-12-01'
Params['EntryStart'] = '2012-11-01'
Params['EntryEnd'] = '2012-12-31'
Params['ChangedBy'] = '0'
Params['SheetsFor'] = '1'
Params['ChangeType'] = '0'
Params['IncludeMgrAlterTimeResponse'] = '1'
GenerateForManager(mgr)

SetReportId('TimesheetDetailNew')
Params['end_date'] = '2012-12-01'
Params['one_emp'] = '1'
Params['emp_id'] = '2314'
Params['HierarchyDepth'] = '99'
Params['sort'] = 'short_name'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TimesheetEmpSummary')
Params['emp_id'] = '2314'
Params['start_date'] = '2012-11-25'
Params['end_date'] = '2012-12-01'
GenerateForManager(mgr)

SetReportId('TimesheetExceptionByEmp')
Params['EmployeeId'] = '2314'
Params['StartDate'] = '2012-11-01'
GenerateForManager(mgr)

SetReportId('TimesheetExceptionNew')
Params['StartDate'] = '2012-11-25'
Params['ManagerID'] = '5457'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TimesheetExport')
Params['WeekEnding'] = '2012-12-01'
Params['FinalOnly'] = '0'
GenerateForManager(mgr)

SetReportId('TimesheetSummaryNew')
Params['manager_id'] = '5457'
Params['end_date'] = '2012-12-01'
Params['HierarchyDepth'] = '99'
Params['Sort'] = 'Short Name'
Params['EmployeeStatus'] = '1'
GenerateForManager(mgr)

SetReportId('TimesheetTotals')
Params['manager_id'] = '5457'
Params['end_date'] = '2012-12-01'
Params['HierarchyDepth'] = '99'
Params['Sort'] = 'Short Name'
Params['EmployeeStatus'] = '1'
Params['FinalOnly'] = '0'
GenerateForManager(mgr)

SetReportId('TimeSubmitMessageResponse')
Params['DateStart'] = '2012-11-01'
Params['DateEnd'] = '2013-01-01'
Params['RuleType'] = 'SUBMIT'
Params['Response'] = 'Agree'
GenerateForManager(mgr)

SetReportId('WorkOrderDetail')
Params['WOID'] = '110066'
Params['ImageList'] = '2000004,2000005'
GenerateForManager(mgr)

# As of 12/2012 there is only 1 center (LAM01) and one client (4022) with work orders
SetReportId('WorkOrderStatusActivity')
Params['StatusDateFrom'] = '2012-12-20'
Params['StatusDateTo'] = '2012-12-31'
Params['CallCenterCodes'] = 'LAM01'
Params['WorkOrderClosed'] = '2'
Params['ClientIDList'] = '4022'
Params['ManagerID'] = '-1'
GenerateForManager(mgr)

# This actually runs all the report commands we have generated
Run(0)

# dumps the resulting times.
DumpResults()

# check for any report that completed with an error
CheckResults()
