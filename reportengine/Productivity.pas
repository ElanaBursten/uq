unit Productivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB, ppStrtch,
  ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppDBJIT,
  daDataModule, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  Tcv = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerNameLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ShortNameDBText: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText8: TppDBText;
    ppShape1: TppShape;
    ppSummaryBand1: TppSummaryBand;
    ppShape2: TppShape;
    ppLabel9: TppLabel;
    ppLine1: TppLine;
    TotalsSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    TotalStatusCode: TppDBText;
    TotalClosedStatus: TppDBText;
    TotalPartialStatus: TppDBText;
    TotalTotalStatus: TppDBText;
    StatusTotalsDS: TDataSource;
    StatusTotalsPipe: TppDBPipeline;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    StatusDetailsDS: TDataSource;
    StatusDetailsPipe: TppDBPipeline;
    DetailsSubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText3: TppDBText;
    ppDBText2: TppDBText;
    ppDBText11: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc9: TppDBCalc;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    LimitClientsDS: TDataSource;
    LimitClientsPipe: TppDBPipeline;
    LimitClientsSubReport: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppRegion1: TppRegion;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel7: TppLabel;
    ppLabel18: TppLabel;
    ppSummaryBand2: TppSummaryBand;
    ManagerNameDS: TDataSource;
    ManagerNamePipe: TppDBPipeline;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppLabel14: TppLabel;
    ppDBText1: TppDBText;
    ppLabel29: TppLabel;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBCalc18: TppDBCalc;
    ppDBCalc19: TppDBCalc;
    ppShape3: TppShape;
    ppDBCalc17: TppDBCalc;
    Detail_tph_working: TppDBText;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppDBText29: TppDBText;
    EmployeeStatusLabel: TppLabel;
    ppDBCalc20: TppDBCalc;
    ppDBCalc21: TppDBCalc;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    ppDBCalc24: TppDBCalc;
    ppDBCalc25: TppDBCalc;
    ppDBText24: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    ClientLabel: TppLabel;
    ppSummaryBand3: TppSummaryBand;
    ExcludedStatuses: TppLabel;
    ExcludedStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    StatusTotals: TADODataSet;
    LimitClients: TADODataSet;
    StatusDetails: TADODataSet;
    ManagerName: TADODataSet;
    procedure DetailsSubReportPrint(Sender: TObject);
    procedure ppHeaderBand1BeforePrint(Sender: TObject);
    function GetLimitation: Integer;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, odADOutils;

{ TProductivityDM }

procedure Tcv.Configure(QueryFields: TStrings);
var
  DateFrom, DateTo: TDateTime;
  Limitation: Integer;
  ManagerID: Integer;
  EmployeeStatus: Integer;
  ExcludeStatuses: string;
  HasClientLimit: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;

  DateFrom := GetDateTime('DateFrom');
  DateTo := GetDateTime('DateTo');

  Limitation := GetLimitation;

  if (DateTo-DateFrom) > Limitation then
    raise Exception.Create('Error - Productivity report is limited to '+IntToStr(Limitation)+' day(s).  Please contact the IT department for longer span Productivity reports.');

  ManagerID := GetInteger('ManagerId');
  EmployeeStatus := GetInteger('EmployeeStatus');
  ExcludeStatuses := SafeGetString('ExcludeStatuses');
  HasClientLimit := NotEmpty(SafeGetString('LimitClients'));
  with SP do begin
    Parameters.ParamByName('@DateFrom').value         := DateFrom;
    Parameters.ParamByName('@DateTo').value           := DateTo;
    Parameters.ParamByName('@ManagerId').value        := ManagerID;
    Parameters.ParamByName('@ExcludeStatuses').value  := ExcludeStatuses;
    Parameters.ParamByName('@LimitCallCenters').value := SafeGetString('LimitCallCenters');
    Parameters.ParamByName('@LimitClients').value     := SafeGetString('LimitClients');
    Parameters.ParamByName('@EmployeeStatus').value   := EmployeeStatus;
    Open;
  end;


  Master.Recordset := SP.Recordset;
  StatusDetails.Recordset   := SP.Recordset.NextRecordset(RecsAffected);
  StatusTotals.Recordset    := SP.Recordset.NextRecordset(RecsAffected);
  LimitClients.Recordset    := SP.Recordset.NextRecordset(RecsAffected);
  ManagerName.Recordset     := SP.Recordset.NextRecordset(RecsAffected);

  StatusDetails.IndexFieldNames := 'emp_id';

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo', '', False, True);
  ShowEmployeeName(ManagerNameLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);

  if HasClientLimit then
    ShowText(ClientLabel, 'Clients:', 'Limited')
  else
    ShowText(ClientLabel, 'Clients:', 'All');
  if IsEmpty(ExcludeStatuses) then
    ExcludedStatuses.Caption := 'None'
  else
    ExcludedStatuses.Caption := ExcludeStatuses;

  AlignRBComponentsByTag(Self);
  AddHierGroups(Report, ShortNameDBText, 0.06);
end;

procedure Tcv.DetailsSubReportPrint(Sender: TObject);
begin
  inherited;
  StatusDetails.Filtered := True;
  StatusDetails.Filter := 'emp_id=' + IntToStr(Master.FieldByName('emp_id').AsInteger);
end;

function Tcv.GetLimitation: Integer;
const
  SQL = 'select value from configuration_data where name = ''ProductivityReportLimit''';
var
  Dataset: TADOQuery;
begin
  Result := 32; // the default
  Dataset := TADOQuery.Create(nil);
  try
    Dataset.Connection := SP.Connection;
    DataSet.SQL.Text := SQL;
    DataSet.Open;
    if not DataSet.Eof then
      Result := Dataset.FieldByName('value').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure Tcv.ppHeaderBand1BeforePrint(Sender: TObject);
begin
  //LimitClientsSubReport.Visible := (LimitClients.RecordCount>0) and
  //                                      (Report.Page = 1);
end;

initialization
  RegisterReport('Productivity', Tcv);

end.

