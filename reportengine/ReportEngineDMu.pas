unit ReportEngineDMu;

interface

uses
  SysUtils, Classes, DB, odADOUtils, ReportConfig,  Data.Win.ADODB, OdUqInternet;

type
  TReportEngineDM = class(TDataModule)

    ReportRights: TADOQuery;
    LogStart: TADOCommand;
    LogFinish: TADOCommand;
    LogStartOldClient: TADOCommand;
    GetUserData: TADOQuery;
    GetConfigurationData: TADOQuery;
    RightDefinition: TADOTable;
    Conn: TADOConnection;
    AWSCredentialsQry: TADOQuery;
    AWSConstantsQry: TADOQuery;
    procedure ConnBeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ConnAfterConnect(Sender: TObject);
  private
    function GetUserPasswordIfActive(UserID: Integer): string;
    function GetLoginID(UserID: Integer): string;
    procedure OpenUserData(UserID: Integer);
    function ResolveDuplicatedReports(RepName: string): string;
    function GetAWSConstants: Boolean;
    function GetAWSCredentials: Boolean;
  public
      Report: TStrings;
      ConfigDMu: TReportConfig;
    procedure CheckReportSecurity(Params: TStrings; UserID: Integer; Timeout: Integer);
    procedure LogReportStart(const RequestID, RepName: string;
      UserID: Integer; ParamString: string; OldClient: Boolean);
    procedure LogReportResult(const RequestID: string; Success: Boolean; Details: string);
    function UpdateReportRightsDefinitions(RegisteredReportNames: TStrings; Report: TStrings; var AnyError: Boolean): Integer;
    function GetConfigurationDataValue(const SettingName, Default: string): string;
  end;

implementation

uses
  OdMiscUtils, ReportEngineLog, OdDbUtils, PasswordRules, QMConst, OdUtcDates;

{$R *.dfm}

{ TDM }

procedure TReportEngineDM.CheckReportSecurity(Params: TStrings; UserID: Integer; Timeout: Integer);
var
  RepName: string;
  PasswordHash: string;
  PasswordValid: Boolean;
  DBPassword: string;
  LoginID: string;
begin
  RepName := Params.Values['Report'];

  // To be replaced by a data-driven mechanism when permission control
  // features need to expand:
  if RepName = 'ClosedTicketDetail' then
    Exit;  // Anyone can run this report, but the rest need a permission set

  RepName := ResolveDuplicatedReports(RepName);

  PasswordValid := True; // Old clients do not send a hash
  PasswordHash := Params.Values['PasswordHash'];
  if NotEmpty(PasswordHash) then begin
    LoginID := GetLoginID(UserID);
    DBPassword := GetUserPasswordIfActive(UserID);
    if PasswordIsHashed(DBPassword) then
      PasswordValid := DBPassword = PasswordHash
    else
      PasswordValid := GeneratePasswordHash(LoginID, DBPassword) = PasswordHash;
  end;

  if not PasswordValid then
    raise Exception.Create('Invalid password for user ' + LoginID);

  Assert(not ReportRights.Active);
  ReportRights.Parameters.ParamByName('USERID').value := UserID;
  ReportRights.Parameters.ParamByName('ENTITYDATA').value := RepName;

  ReportRights.Open;
  try
    while not ReportRights.Eof do begin // Check both the report-specific and global report rights
      if ReportRights.FieldByName('Allowed').AsString = 'Y' then
        Exit;  // Successfully
      ReportRights.Next;
    end;
  finally
    ReportRights.Close;
  end;

  raise Exception.Create('Security error, user does not have permission to view this report: ' + RepName);
end;

function TReportEngineDM.GetUserPasswordIfActive(UserID: Integer): string;
begin
  OpenUserData(UserID);
  if (not GetUserData.FieldByName('active_ind').AsBoolean) or (not GetUserData.FieldByName('active').AsBoolean) then
    raise Exception.Create('User is no longer active: ' + IntToStr(UserID));
  Result := GetUserData.FieldByName('password').AsString;
  if IsEmpty(Result) then
    raise Exception.Create('A password is required to run reports');
end;

procedure TReportEngineDM.LogReportStart(const RequestID, RepName: string; UserID: Integer;
  ParamString: string; OldClient: Boolean);
var
  CommandToUse: TADOCommand;
begin
  if OldClient then
    CommandToUse := LogStartOldClient
  else
    CommandToUse := LogStart;

  CommandToUse.Parameters.ParamByName('REQUEST_ID_EXISTS').value := RequestID;
  CommandToUse.Parameters.ParamByName('REQUEST_ID').value := RequestID;
  CommandToUse.Parameters.ParamByName('REPORT_NAME').value := RepName;
  CommandToUse.Parameters.ParamByName('USER_ID').value := UserID;
  CommandToUse.Parameters.ParamByName('PARAMS').value := Copy(ParamString, 1, 500);
  CommandToUse.Execute;
end;

procedure TReportEngineDM.LogReportResult(const RequestID: string; Success: Boolean; Details: string);
begin
  LogFinish.Parameters.ParamByName('ENGINE_SUCCESS').value := Success;
  LogFinish.Parameters.ParamByName('ENGINE_DETAILS').value := Copy(Details, 1, 150);
  LogFinish.Parameters.ParamByName('REQUEST_ID').value := RequestID;
  LogFinish.Execute;
end;

function TReportEngineDM.GetLoginID(UserID: Integer): string;
begin
  OpenUserData(UserID);
  Result := GetUserData.FieldByName('login_id').AsString;
end;

procedure TReportEngineDM.OpenUserData(UserID: Integer);
begin
  GetUserData.Parameters.ParamByName('UID').value  := UserID;
  RefreshDataSet(GetUserData);
  if GetUserData.IsEmpty then
    raise Exception.Create('UserID does not exist: ' + IntToStr(UserID));
end;

function TReportEngineDM.ResolveDuplicatedReports(RepName: string): string;
begin
  // Some reports are triggered from the same screen, so there is only one right for all of them
  Result := RepName;
  if (RepName = 'TimesheetExceptionNew') then
    Result := 'TimesheetException'
  else if (RepName = 'TimeExportNew') then
    Result := 'TimeExport'
  else if (RepName = 'DamageShort') then
    Result := 'DamageDetails'
  else if (RepName = 'TimesheetSummaryNew') or (RepName = 'TimesheetTotals') then
    Result := 'TimesheetSummary'
  else if (RepName = 'TimesheetDetailNew') then
    Result := 'TimesheetDetail'
  else if (RepName = 'TicketDetail') then
    Result := 'ClosedTicketDetail'; // The TicketDetail report uses the old ClosedTicketDetail right
end;

function TReportEngineDM.UpdateReportRightsDefinitions(RegisteredReportNames: TStrings; Report: TStrings; var AnyError: Boolean): Integer;
var
  i: Integer;
  ReportID: string;
  ExistingReports: String;
begin
  Assert(Assigned(RegisteredReportNames));
  Assert(Assigned(Report));
  Result := 0;
  AnyError := False;

  if RightDefinition.Active then
    RightDefinition.Close;
  RightDefinition.Open;

  Report.Add('Existing Reports in the Rights table');
  Report.Add('------------------------------------');
  Report.Add('');
  RightDefinition.First;
  i := 0;
  while not RightDefinition.EOF do begin
    if SameText(RightDefinition.FieldByName('right_type').AsString, ReportRightType) then begin
      Inc(i);
      Report.Add(Format( '%d) %s', [i, RightDefinition.FieldByName('right_description').AsString]));
    end;
    RightDefinition.Next;
  end;

  ExistingReports := Report.Text;
  Report.Text := EmptyStr;

  try
    Report.Add('Reports added to the Rights table');
    Report.Add('---------------------------------');
    Report.Add('');
    for i := 0 to RegisteredReportNames.Count - 1  do begin
      try
        ReportID := ResolveDuplicatedReports(RegisteredReportNames[i]);
        if not RightDefinition.Locate('entity_data', ReportID, []) then begin
          RightDefinition.Insert;
          RightDefinition.FieldByName('right_description').AsString := 'Reports - '+ReportID;
          RightDefinition.FieldByName('entity_data').AsString := ReportID;
          RightDefinition.FieldByName('right_type').AsString := ReportRightType;
          RightDefinition.Post;
          Inc(Result);
          Report.Add(Format( '%d) %s', [Result, RightDefinition.FieldByName('right_description').AsString]));
        end;
      except
        on e:Exception do begin
          AnyError := True;
          Report.Add('<<<WARNING>>>');
          Report.Add(Format('An error occured trying to add "%s" report right to the rights table: ' + E.Message, [RegisteredReportNames[i]]));
          Report.Add('<<<WARNING>>>');
          Report.Add('');
        end;
      end;
    end;
    if Result = 0 then
      Report.Add('No new reports were added to the rights table.')
    else begin
      Report.Add('');
      Report.Add(Format('Total added reports: %d', [Result]));
    end;
  finally
    Report.Add('');
    Report.Add(ExistingReports);
    RightDefinition.Close;
  end
end;

procedure TReportEngineDM.ConnAfterConnect(Sender: TObject);
begin
  try
    if ConfigDMu.DevDebug then
       exit;
    assert(GetAWSCredentials, 'Invalid or missing Credentials');
    assert(GetAWSConstants, 'Missing constants');

  except
    on e: Exception do begin
      Report.Add('<<<WARNING>>>');
      Report.Add('An error occured trying to retrieve AWS credential or constants: '+ e.Message);
      Report.Add('<<<WARNING>>>'); Report.Add('');
    end;
  end;
end;

procedure TReportEngineDM.ConnBeforeConnect(Sender: TObject);
begin
//  Conn.CmdExecTimeout := 120000;   //QMANTWO-497
end;

procedure TReportEngineDM.DataModuleCreate(Sender: TObject);
begin
  ConfigDMu := LoadConfig(ExtractFilePath(ParamStr(0))+'ReportEngineCGI.ini');
end;

procedure TReportEngineDM.DataModuleDestroy(Sender: TObject);
begin
  FreeANdNil(ConfigDMu);
end;

function TReportEngineDM.GetConfigurationDataValue(const SettingName, Default: string): string;
const
  Select = 'select * from configuration_data where name = %s';
begin
  Result := Default;

  GetConfigurationData.Close;
  GetConfigurationData.SQL.Text := Format(Select, [QuotedStr(SettingName)]);
  GetConfigurationData.Open;
  try
    if GetConfigurationData.RecordCount = 1 then
      Result := GetConfigurationData.FieldByName('value').AsString;
  finally
    GetConfigurationData.Close;
  end;
end;


function TReportEngineDM.GetAWSConstants: Boolean;  //QMANTWO-553 sr
begin
   if AWSConstantsQry.Active then
      AWSConstantsQry.Close;
   AWSConstantsQry.Open;
   if AWSConstantsQry.RecordCount > 0 then begin
     myAttachmentData.Constants.Host := AWSConstantsQry.FieldByName('host').AsString;
     myAttachmentData.Constants.RestURL :=  AWSConstantsQry.FieldByName('rest_url').AsString;
     myAttachmentData.Constants.Service := AWSConstantsQry.FieldByName('service').AsString;
     myAttachmentData.Constants.StagingBucket := AWSConstantsQry.FieldByName('bucket').AsString;
     myAttachmentData.Constants.StagingFolder := AWSConstantsQry.FieldByName('bucket_folder').AsString;
     myAttachmentData.Constants.Region := AWSConstantsQry.FieldByName('region').AsString;
     myAttachmentData.Constants.Algorithm := AWSConstantsQry.FieldByName('algorithm').AsString;
     myAttachmentData.Constants.SignedHeaders := AWSConstantsQry.FieldByName('signed_headers').AsString;
     myAttachmentData.Constants.ContentType := AWSConstantsQry.FieldByName('content_type').AsString;
     myAttachmentData.Constants.AcceptedValues := AWSConstantsQry.FieldByName('accepted_values').AsString;
     if myAttachmentData.Constants.Host <> '' then
       Result := True
     else
       Result := False
   end
   else
     Result := False;
end;

function TReportEngineDM.GetAWSCredentials: Boolean;   //QMANTWO-553 sr
begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
  if AWSCredentialsQry.Active then
    AWSCredentialsQry.Close;
//  AWSCredentialsQry.FieldByName('UTCNow').AsDateTime := LocalDateTimeToUTCDateTime(Now);      //SystemTimeToTzSpecificLocalTime
  AWSCredentialsQry.Open;
  if AWSCredentialsQry.RecordCount > 0 then begin
    myAttachmentData.AWSKey := AWSCredentialsQry.FieldByName('enc_access_key').AsWideString;
    myAttachmentData.AWSSecret := AWSCredentialsQry.FieldByName('enc_secret_key').AsWideString;
    if (myAttachmentData.AWSKey <> '') and (myAttachmentData.AWSSecret <> '') then
      Result := True
    else
      Result := False;
  end
  else begin
    myAttachmentData.AWSKey := '';
    myAttachmentData.AWSSecret := '';
    Result := False;
  end;
end;



end.
