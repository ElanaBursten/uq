-- Add_Emp_Timesheet_Data:
-- Script will add Timesheet data for a single employee through a date range
-- Change the date ranges for @Start_entry_Date to  @End_entry_Date

-- Start Entry Date
DECLARE @Start_entry_Date datetime
  SET @Start_entry_Date = '2019-02-16 00:00:00.000'

-- End Entry Date
DECLARE @End_entry_Date datetime
  SET @End_entry_Date = '2019-02-28 00:00:00.000'

--10709 FMB.es Vincent Turner
--13722 FMB.es Erwin McCray
--13726 FMB.es Teresa Adkins  
--14575 FMB.es Joseph Connell Jr
--14989 FMB.es Robert Taylor
--15268 FMB.es Mark Magruder
--15544 FMB.es Ryan Gardner

-- Employee 
DECLARE @EmployeeID int
  SET @EmployeeID = 10709 --Emil Gasparian

-- Who is Entering Data
DECLARE @EntryBy int
  SET @EntryBy = 1636

-- This is the range for the random Qty of Hours
DECLARE @MinHourQty int
  SET @MinHourQty = 5
DECLARE @MaxHourQty int
  SET @MaxHourQty = 13

DECLARE @ThisDate datetime
DECLARE @CurHours int

DECLARE @PerDiem bit
  SET @PerDiem=0
DECLARE @RowCount int
  SET @RowCount=1


SET @ThisDate = @Start_entry_Date

WHILE @ThisDate <= @End_entry_Date BEGIN
  SET @CurHours = ROUND(((@MaxHourQty - @MinHourQty -1) * RAND() + @MinHourQty), 0)
  SET @RowCount = @RowCount + 1
  If @RowCount%3 = 0 SET @PerDiem = 1 ELSE SET @PerDiem = 0
  INSERT INTO timesheet_entry(work_emp_id,  work_date,   modified_date,  status,  entry_date,   entry_date_local, entry_by, approve_date, approve_date_local, approve_by, final_approve_date, final_approve_date_local, final_approve_by, work_start1, work_stop1, work_start2, work_stop2, work_start3, work_stop3, work_start4, work_stop4, work_start5, work_stop5, callout_start1, callout_stop1, callout_start2, callout_stop2, callout_start3, callout_stop3, callout_start4, callout_stop4, callout_start5, callout_stop5, callout_start6, callout_stop6, miles_start1, miles_stop1, vehicle_use, reg_hours, ot_hours, dt_hours, callout_hours, vac_hours, leave_hours, br_hours, hol_hours, jury_hours, floating_holiday, rule_ack_date, rule_id_ack, emp_type_id, work_pc_code, reason_changed, lunch_start, source,   work_start6, work_stop6, work_start7, work_stop7, work_start8, work_stop8, work_start9, work_stop9, work_start10, work_stop10, work_start11, work_stop11, work_start12, work_stop12, work_start13, work_stop13, work_start14, work_stop14, work_start15, work_stop15, work_start16, work_stop16, callout_start7, callout_stop7, callout_start8, callout_stop8, callout_start9, callout_stop9, callout_start10, callout_stop10, callout_start11, callout_stop11, callout_start12, callout_stop12, callout_start13, callout_stop13, callout_start14, callout_stop14, callout_start15, callout_stop15, callout_start16, callout_stop16,  pto_hours, per_diem)
                       VALUES(@EmployeeID,  @ThisDate,  @ThisDate,     'ACTIVE', @ThisDate,    @ThisDate,        @EntryBy, NULL,         NULL,               NULL,        NULL,              NULL,                     NULL,             NULL,        NULL,       NULL,         NULL,      NULL,         NULL,      NULL,        NULL,       NULL,         NULL,       NULL,          NULL,          NULL,           NULL,          NULL,           NULL,           NULL,          NULL,          NULL,           NULL,           NULL,           NULL,         3,            44,          'NONE',      @CurHours, 3.5,      0,         0,             0,         0,          0,        0,         0,           0,               NULL,          NULL,          1051,        NULL,        1106,           NULL,       'entry',  NULL,          NULL,      NULL,        NULL,       NULL,        NULL,        NULL,      NULL,       NULL,          NULL,         NULL,        NULL,        NULL,           NULL,      NULL,         NULL,        NULL,         NULL,        NULL,        NULL,        NULL,         NULL,         NULL,          NULL,          NULL,            NULL,          NULL,          NULL,           NULL,            NULL,           NULL,               NULL,         NULL,          NULL,           NULL,            NULL,           NULL,            NULL,           NULL,            NULL,           NULL,            NULL,            0,          @PerDiem)
     SET @ThisDate = DATEADD(DAY, 1, @ThisDate)
  IF @ThisDate = @End_Entry_Date
    BREAK;
  END
GO



    
