unit DamageShort;

interface

uses
  SysUtils, Windows, BaseReport, DB, ppDB, ppDBPipe, ppCtrls, ppBands,
  ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  Classes, ppStrtch, ppMemo, Graphics, ppModule, daDataModule, ppSubRpt,
  ppRegion, myChkBox, ppTypes, ServerAttachmentDMu, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageShortDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppDetailBand1: TppDetailBand;
    MasterDS: TDataSource;
    MasterPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    TicketNotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    ppSummaryBand1: TppSummaryBand;
    TicketSection: TppSubReport;
    ppChildReport5: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppShape9: TppShape;
    ppDBText26: TppDBText;
    ppLabel77: TppLabel;
    ppDBText27: TppDBText;
    ppLabel78: TppLabel;
    ppDBText28: TppDBText;
    ppDetailBand6: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppShape10: TppShape;
    ppReportDateTime: TppCalc;
    ppLabel79: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppSummaryBand2: TppSummaryBand;
    NotesSubReport: TppSubReport;
    ppChildReport6: TppChildReport;
    ppDetailBand7: TppDetailBand;
    NoteMemo: TppDBMemo;
    ppDBText75: TppDBText;
    ppLabel80: TppLabel;
    TicketIDGroup: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    DetailHeaderRegion: TppRegion;
    ImageMemo: TppMemo;
    NoTicketWarningLabel: TppLabel;
    ppGroupFooterBand3: TppGroupFooterBand;
    DamageAttachmentsDS: TDataSource;
    TicketAttachmentsDS: TDataSource;
    DamageAttachmentsPipe: TppDBPipeline;
    TicketAttachmentsPipe: TppDBPipeline;
    DamageImagesSubReport: TppSubReport;
    ppChildReportDamageAttachments: TppChildReport;
    ppDetailBand8: TppDetailBand;
    ppDamageImage: TppImage;
    ppDBText76: TppDBText;
    TicketImagesSubreport: TppSubReport;
    ppChildReportTicketAttachments: TppChildReport;
    ppHeaderBand3: TppHeaderBand;
    ppDetailBand9: TppDetailBand;
    ppDBText91: TppDBText;
    ppTicketImage: TppImage;
    ppFooterBand3: TppFooterBand;
    ppLabel82: TppLabel;
    ppLabel85: TppLabel;
    ppLabel88: TppLabel;
    ppLabel102: TppLabel;
    ppLabel86: TppLabel;
    ppLabel101: TppLabel;
    ppLabel81: TppLabel;
    ppLabel104: TppLabel;
    ppDBMemo1: TppDBMemo;
    ppDBText69: TppDBText;
    myDBCheckBox2: TmyDBCheckBox;
    myDBCheckBox1: TmyDBCheckBox;
    ppDBText73: TppDBText;
    ppDBText72: TppDBText;
    ppDBText71: TppDBText;
    ppDBText70: TppDBText;
    ppMemoErrorMessageTicket: TppMemo;
    ppMemoErrorMessageDamage: TppMemo;
    ThirdPartySection: TppSubReport;
    ppChildReport3: TppChildReport;
    ThirdPartyDetailBand: TppDetailBand;
    ppLabel126: TppLabel;
    ppDBText92: TppDBText;
    ppDBText93: TppDBText;
    ppDBText94: TppDBText;
    ppDBText95: TppDBText;
    ppDBText96: TppDBText;
    ThirdPartyClaimDescrMemo: TppDBMemo;
    ppDBText97: TppDBText;
    ppFooterBand4: TppFooterBand;
    ppSystemVariable4: TppSystemVariable;
    ppCalc1: TppCalc;
    ppLabel108: TppLabel;
    ppShape13: TppShape;
    LitigationSection: TppSubReport;
    LitigationChildReport: TppChildReport;
    LitigationDetailBand: TppDetailBand;
    NoLitigationWarningLabel: TppLabel;
    LitigationFooterBand: TppFooterBand;
    ppShape14: TppShape;
    ppSystemVariable5: TppSystemVariable;
    ppCalc2: TppCalc;
    ppLabel109: TppLabel;
    DamageSection: TppSubReport;
    ppChildReport9: TppChildReport;
    ppDetailBand10: TppDetailBand;
    DamageFooterBand: TppFooterBand;
    ppShape2: TppShape;
    ppSystemVariable6: TppSystemVariable;
    ppCalc3: TppCalc;
    ppLabel105: TppLabel;
    ConclusionsSection: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppShape8: TppShape;
    ppDBText3: TppDBText;
    ppLabel28: TppLabel;
    ppDBText4: TppDBText;
    ppLabel29: TppLabel;
    ppDBText77: TppDBText;
    ppLabel46: TppLabel;
    ppLabel100: TppLabel;
    ppDBText85: TppDBText;
    ppDBText86: TppDBText;
    ConclusionCompanyRegion: TppRegion;
    ppDBText81: TppDBText;
    ppLabel95: TppLabel;
    ppDBText82: TppDBText;
    ppLabel96: TppLabel;
    ppLine9: TppLine;
    ppDBText87: TppDBText;
    ppDBText88: TppDBText;
    flduq_resp_details: TppDBMemo;
    ppLabel103: TppLabel;
    ConclusionSpecialReason: TppRegion;
    ppDBText83: TppDBText;
    ppLabel97: TppLabel;
    ppDBText84: TppDBText;
    ppLabel98: TppLabel;
    ppDBMemo12: TppDBMemo;
    ppLabel99: TppLabel;
    ppLine10: TppLine;
    ppDBText89: TppDBText;
    ppDBText90: TppDBText;
    ConclusionExcavator1Region: TppRegion;
    ppLabel84: TppLabel;
    ppDBMemo9: TppDBMemo;
    ConclusionExcavator2Region: TppRegion;
    ppLabel94: TppLabel;
    ppDBMemo11: TppDBMemo;
    DiscussionSection: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand5: TppDetailBand;
    ppShape4: TppShape;
    ppLabel91: TppLabel;
    DiscAddPerson2Region: TppRegion;
    ppLabel73: TppLabel;
    ppDBText68: TppDBText;
    ppLabel74: TppLabel;
    ppDBMemo6: TppDBMemo;
    ppLabel75: TppLabel;
    ppLine7: TppLine;
    ppDBText67: TppDBText;
    DiscAddPerson1Region: TppRegion;
    ppDBText65: TppDBText;
    ppLabel70: TppLabel;
    ppDBText66: TppDBText;
    ppLabel71: TppLabel;
    ppDBMemo5: TppDBMemo;
    ppLabel72: TppLabel;
    ppLine6: TppLine;
    DiscExcRegion: TppRegion;
    ppDBText62: TppDBText;
    ppLabel66: TppLabel;
    ppDBText63: TppDBText;
    ppLabel67: TppLabel;
    ppDBText64: TppDBText;
    ppLabel68: TppLabel;
    ppDBMemo4: TppDBMemo;
    ppLabel69: TppLabel;
    ppLine5: TppLine;
    DiscRepairRegion: TppRegion;
    ppDBText58: TppDBText;
    ppLabel61: TppLabel;
    ppDBText59: TppDBText;
    ppLabel62: TppLabel;
    ppDBText60: TppDBText;
    ppLabel63: TppLabel;
    ppDBText61: TppDBText;
    ppLabel64: TppLabel;
    ppDBMemo3: TppDBMemo;
    ppLabel65: TppLabel;
    NarrativeSection: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBMemo2: TppDBMemo;
    ppShape6: TppShape;
    ppLabel87: TppLabel;
    ppLine1: TppLine;
    ppLabel1: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText15: TppDBText;
    ppDBText14: TppDBText;
    ppDBText13: TppDBText;
    ppDBText2: TppDBText;
    ppLabel83: TppLabel;
    ppDBText74: TppDBText;
    ppDBText46: TppDBText;
    ppDBText43: TppDBText;
    ppLabel92: TppLabel;
    ppDBText78: TppDBText;
    ppDBText79: TppDBText;
    ppLabel93: TppLabel;
    ppShape1: TppShape;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppLabel8: TppLabel;
    ppDBText8: TppDBText;
    ppLabel9: TppLabel;
    ppDBText9: TppDBText;
    ppLabel10: TppLabel;
    ppDBText12: TppDBText;
    ppLabel13: TppLabel;
    ppLabel17: TppLabel;
    ppDBText17: TppDBText;
    ppLabel18: TppLabel;
    ppDBText18: TppDBText;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppDBText20: TppDBText;
    ppLabel21: TppLabel;
    ppDBText21: TppDBText;
    ppLabel22: TppLabel;
    ppDBText22: TppDBText;
    ppLabel23: TppLabel;
    ppDBText23: TppDBText;
    ppLabel24: TppLabel;
    ppDBText24: TppDBText;
    ppLabel25: TppLabel;
    ppLabel3: TppLabel;
    ppDBText19: TppDBText;
    ppDBText16: TppDBText;
    ExcavatorRegion: TppRegion;
    ppShape7: TppShape;
    ppDBText29: TppDBText;
    ppLabel32: TppLabel;
    ppDBText30: TppDBText;
    ppLabel33: TppLabel;
    ppDBText31: TppDBText;
    ppLabel34: TppLabel;
    ppDBText33: TppDBText;
    ppLabel36: TppLabel;
    ppDBText34: TppDBText;
    ppLabel37: TppLabel;
    ppLabel5: TppLabel;
    ppShape3: TppShape;
    ppLabel4: TppLabel;
    ppLabel12: TppLabel;
    ppLabel26: TppLabel;
    ppDBText32: TppDBText;
    ppLabel27: TppLabel;
    ppLabel30: TppLabel;
    ppLabel35: TppLabel;
    ppDBText36: TppDBText;
    ppDBText37: TppDBText;
    ppDBText38: TppDBText;
    SiteInfoRegion: TppRegion;
    ppShape5: TppShape;
    ppDBText44: TppDBText;
    ppLabel47: TppLabel;
    ppDBText45: TppDBText;
    ppLabel48: TppLabel;
    ppDBText47: TppDBText;
    ppLabel50: TppLabel;
    ppDBText50: TppDBText;
    ppLabel53: TppLabel;
    ppLabel11: TppLabel;
    ppLabel31: TppLabel;
    ppDBText10: TppDBText;
    ppLabel49: TppLabel;
    ppDBText11: TppDBText;
    ppLabel76: TppLabel;
    ppDBText25: TppDBText;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    ppDBText35: TppDBText;
    ppDBText39: TppDBText;
    ppLabel40: TppLabel;
    ppDBText40: TppDBText;
    ppLabel41: TppLabel;
    ppDBText41: TppDBText;
    ppLabel42: TppLabel;
    ppDBText42: TppDBText;
    ppLabel43: TppLabel;
    ppDBText48: TppDBText;
    ThirdPartyDS: TDataSource;
    LitigationDS: TDataSource;
    ThirdPartyPipe: TppDBPipeline;
    LitigationPipe: TppDBPipeline;
    DamageHeaderBand: TppHeaderBand;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    ppLabel44: TppLabel;
    ppDBText49: TppDBText;
    ppShape11: TppShape;
    ppLabel52: TppLabel;
    ppLabel54: TppLabel;
    ppDBText52: TppDBText;
    ppLabel55: TppLabel;
    ppDBText53: TppDBText;
    ppLabel56: TppLabel;
    ppLabel57: TppLabel;
    ppLabel58: TppLabel;
    ppLabel59: TppLabel;
    ppDBText54: TppDBText;
    ppLabel116: TppLabel;
    ppDBText57: TppDBText;
    LitigationDetailRegion: TppRegion;
    ppDBText98: TppDBText;
    ppDBText99: TppDBText;
    ppDBText100: TppDBText;
    ppDBText101: TppDBText;
    ppDBText102: TppDBText;
    ppDBText103: TppDBText;
    ppDBText104: TppDBText;
    ppDBText105: TppDBText;
    ppLabel60: TppLabel;
    ppLabel90: TppLabel;
    ppLabel106: TppLabel;
    ppDBText55: TppDBText;
    ppLabel107: TppLabel;
    ppLabel110: TppLabel;
    ppLabel111: TppLabel;
    ppLabel112: TppLabel;
    ppLabel113: TppLabel;
    ppLabel114: TppLabel;
    ppDBText56: TppDBText;
    ppLabel115: TppLabel;
    ThirdPartyDetailRegion: TppRegion;
    NoThirdPartyWarningLabel: TppLabel;
    ppLine2: TppLine;
    ppLine3: TppLine;
    LitigationDamageIDGroup: TppGroup;
    LitigationDamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ThirdPartyIDGroup: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppShape15: TppShape;
    ppLabel117: TppLabel;
    ppCalc4: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppFooterBand1: TppFooterBand;
    ppShape16: TppShape;
    ppLabel118: TppLabel;
    ppCalc5: TppCalc;
    ppSystemVariable2: TppSystemVariable;
    ppTitleBand1: TppTitleBand;
    ppLabel89: TppLabel;
    ThirdPartyDamgeIDGroup: TppGroup;
    ThirdPartyDamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    LitigationIDGroup: TppGroup;
    ppGroupHeaderBand5: TppGroupHeaderBand;
    ppGroupFooterBand5: TppGroupFooterBand;
    ppDBText51: TppDBText;
    ppLabel45: TppLabel;
    ppLabel51: TppLabel;
    ppShape12: TppShape;
    ppFooterBand5: TppFooterBand;
    ppLabel119: TppLabel;
    ppCalc6: TppCalc;
    ppSystemVariable7: TppSystemVariable;
    ppShape17: TppShape;
    ppGroup1: TppGroup;
    DamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand6: TppGroupFooterBand;
    DamageNotesDS: TDataSource;
    DamageNotesPipe: TppDBPipeline;
    HistoryDS: TDataSource;
    HistoryPipe: TppDBPipeline;
    DamageNotesSection: TppSubReport;
    DamageNotesChildReport: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppLabel120: TppLabel;
    ppDBText106: TppDBText;
    ppDBMemo7: TppDBMemo;
    HistorySection: TppSubReport;
    HistoryChildReport: TppChildReport;
    ppDetailBand11: TppDetailBand;
    HistoryDamageIDGroup: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand7: TppGroupFooterBand;
    ppLabel121: TppLabel;
    ppDBText107: TppDBText;
    ppShape18: TppShape;
    ppLabel122: TppLabel;
    DamageNotesDamageIDGroup: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand8: TppGroupFooterBand;
    ppLabel124: TppLabel;
    ppDBText108: TppDBText;
    ppShape19: TppShape;
    ppLabel125: TppLabel;
    HistoryHeaderRegion: TppRegion;
    ppLabel127: TppLabel;
    ppLabel128: TppLabel;
    ppLabel129: TppLabel;
    ppLabel130: TppLabel;
    ppLabel131: TppLabel;
    ppLabel132: TppLabel;
    ppLabel133: TppLabel;
    ppLabel134: TppLabel;
    ppDBText109: TppDBText;
    ppDBText110: TppDBText;
    ppDBText111: TppDBText;
    ppDBText112: TppDBText;
    ppDBText113: TppDBText;
    ppDBText114: TppDBText;
    ppDBText115: TppDBText;
    ppDBText116: TppDBText;
    ppHeaderBand1: TppHeaderBand;
    ppLine13: TppLine;
    RegionResponse: TppRegion;
    ppLabel148: TppLabel;
    ExcRespResponse: TppDBMemo;
    UQRespEss: TppDBMemo;
    RegionDetails: TppRegion;
    ppLabel147: TppLabel;
    ExcRespDetails: TppDBMemo;
    UQRespDetails: TppDBMemo;
    SPCRespDetails: TppDBMemo;
    RegionOtherDesc: TppRegion;
    ppLabel146: TppLabel;
    ExcRespOtherDesc: TppDBMemo;
    UQRespOtherDesc: TppDBMemo;
    labelReasonChanged: TppLabel;
    dbReasonChanged: TppDBText;
    ppLabel149: TppLabel;
    ppLabel150: TppLabel;
    ppLabel151: TppLabel;
    ApprovalSection: TppSubReport;
    ppChildReport7: TppChildReport;
    ppDetailBand12: TppDetailBand;
    ppShape22: TppShape;
    ppLabel157: TppLabel;
    ppDBText119: TppDBText;
    ppLabel154: TppLabel;
    ppLabel155: TppLabel;
    ppLabel156: TppLabel;
    ppDBText120: TppDBText;
    ppDBText121: TppDBText;
    WarningSection: TppSubReport;
    ppChildReport8: TppChildReport;
    ppDetailBand13: TppDetailBand;
    WarningMemo: TppMemo;
    DamageAttachmentsRegion: TppRegion;
    TicketAttachmentsRegion: TppRegion;
    ppDBText117: TppDBText;
    ppDBText118: TppDBText;
    DuplicateRecordsDS: TDataSource;
    DuplicateRecordsPipe: TppDBPipeline;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    ppDesignLayers5: TppDesignLayers;
    ppDesignLayer5: TppDesignLayer;
    ppDesignLayers6: TppDesignLayers;
    ppDesignLayer6: TppDesignLayer;
    ppDesignLayers7: TppDesignLayers;
    ppDesignLayer7: TppDesignLayer;
    ppDesignLayers8: TppDesignLayers;
    ppDesignLayer8: TppDesignLayer;
    ppDesignLayers9: TppDesignLayers;
    ppDesignLayer9: TppDesignLayer;
    ppDesignLayers10: TppDesignLayers;
    ppDesignLayer10: TppDesignLayer;
    ppDesignLayers11: TppDesignLayers;
    ppDesignLayer11: TppDesignLayer;
    ppDesignLayers12: TppDesignLayers;
    ppDesignLayer12: TppDesignLayer;
    ppDesignLayers13: TppDesignLayers;
    ppDesignLayer13: TppDesignLayer;
    ppDesignLayers14: TppDesignLayers;
    ppDesignLayer14: TppDesignLayer;
    DuplicateRecordsSection: TppSubReport;
    ppChildReport10: TppChildReport;
    ppDesignLayers15: TppDesignLayers;
    ppDesignLayer15: TppDesignLayer;
    ppTitleBand2: TppTitleBand;
    ppDetailBand14: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppDBText122: TppDBText;
    ppDBText123: TppDBText;
    ppDBText124: TppDBText;
    ppDBText125: TppDBText;
    ppLabel123: TppLabel;
    ppLabel135: TppLabel;
    ppLabel136: TppLabel;
    ppLabel137: TppLabel;
    SP: TADOStoredProc;
    Reference: TADOQuery;
    DuplicateRecords: TADODataSet;
    History: TADODataSet;
    DamageNotes: TADODataSet;
    Litigation: TADODataSet;
    ThirdParty: TADODataSet;
    TicketAttachments: TADODataSet;
    DamageAttachments: TADODataSet;
    Ticket: TADODataSet;
    TicketNotes: TADODataSet;
    Locates: TADODataSet;
    Damage: TADOQuery;
    Carrier: TADOQuery;
    TicketFacilities: TADODataSet;
    Master: TADODataSet;
    procedure TicketAfterScroll(DataSet: TDataSet);
    procedure ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
    procedure ImageMemoPrint(Sender: TObject);
    function GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
    procedure ppTicketImagePrint(Sender: TObject);
    procedure ppDamageImagePrint(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure ThirdPartyDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
    procedure LitigationDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
  protected
    procedure MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string); override;
  private
    TicketFileList: TStringList;
    DamageFileList: TStringList;
    TicketErrorLog: TStringList;
    DamageErrorLog: TStringList;
    TicketImageList: string;
    DamageImageList: string;
    Attacher: TServerAttachment;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, ODDBUtils, OdRbMemoFit, OdExceptions, ODMiscUtils,
  OdRbDownloadedImage, QMConst, OdPdf, TicketImage, CVUtils, odADOutils;

procedure TDamageShortDM.Configure(QueryFields: TStrings);
var
  DamageID: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  TicketImageList := SafeGetString('TicketImageList', '-1');
  if not IsEmpty(TicketImageList) then
    if not ValidateIntegerCommaSeparatedList(TicketImageList) then
      raise Exception.Create('The parameter TicketImageList=' + TicketImageList + ' is not valid.');

  DamageImageList := SafeGetString('DamageImageList', '-1');
  if not IsEmpty(DamageImageList) then
    if not ValidateIntegerCommaSeparatedList(DamageImageList) then
      raise Exception.Create('The parameter DamageImageList=' + DamageImageList + ' is not valid.');
  Reference.Open;

  // Accept a damage_id preferably, or if not, we'll take a UQDamageID
  // Because the end user is allowed to enter the number, so it would be
  // an extra round-trip to have the client app convert.

  DamageID := SafeGetInteger('DamageID', 0);
  if DamageID = 0 then
    DamageID := GetDamageIDForUQDamageID(GetInteger('UQDamageID'));
  SP.Parameters.ParamByName('@damage_id').value := DamageID;
  SP.Parameters.ParamByName('@DamageAttachmentList').value := DamageImageList;
  SP.Parameters.ParamByName('@TicketAttachmentList').value := TicketImageList;
  SP.Open;
  if (SP.RecordCount <= 1) then begin
    WarningSection.Visible := False;
  end
  else begin
    WarningSection.Visible := True;
    WarningMemo.Lines.Clear;
    WarningMemo.Lines.Text := 'Warning: More damage information returned than expected. ' +
    'The system administrator should check for duplicate excavator, excavation, ' +
    'or facility reference codes.';
  end;


  Master.RecordSet               := SP.RecordSet;
  Ticket.RecordSet               := SP.RecordSet.NextRecordset(RecsAffected);
  Locates.RecordSet              := SP.RecordSet.NextRecordset(RecsAffected);
  TicketNotes.RecordSet          := SP.RecordSet.NextRecordset(RecsAffected);
  DamageAttachments.RecordSet    := SP.RecordSet.NextRecordset(RecsAffected);
  TicketAttachments.RecordSet    := SP.RecordSet.NextRecordset(RecsAffected);
  ThirdParty.RecordSet           := SP.RecordSet.NextRecordset(RecsAffected);
  Litigation.RecordSet           := SP.RecordSet.NextRecordset(RecsAffected);
  DamageNotes.RecordSet          := SP.RecordSet.NextRecordset(RecsAffected);
  History.RecordSet              := SP.RecordSet.NextRecordset(RecsAffected);
  TicketFacilities.RecordSet     := SP.RecordSet.NextRecordset(RecsAffected);
  DuplicateRecords.RecordSet     := SP.RecordSet.NextRecordset(RecsAffected);

  NarrativeSection.Visible   := SafeGetInteger('ShowNarrative'  , 1) = 1;
  DiscussionSection.Visible  := SafeGetInteger('ShowDiscussion' , 1) = 1;
  ConclusionsSection.Visible := SafeGetInteger('ShowConclusions', 1) = 1;
  TicketSection.Visible      := SafeGetInteger('ShowTicket'     , 1) = 1;
  DamageSection.Visible      := SafeGetInteger('ShowDamage'     , 1) = 1;
  ThirdPartySection.Visible  := SafeGetInteger('ShowThirdParty' , 0) = 1;
  LitigationSection.Visible  := SafeGetInteger('ShowLitigation' , 0) = 1;
  DamageNotesSection.Visible := SafeGetInteger('ShowNotes'      , 0) = 1;
  HistorySection.Visible     := SafeGetInteger('ShowHistory'    , 0) = 1;

  Attacher := TServerAttachment.Create(SP.Connection as TADOConnection);
  TicketImagesSubreport.Visible := Attacher.AttachmentsIncludeFileType(TicketAttachments, Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));
  DamageImagesSubReport.Visible := Attacher.AttachmentsIncludeFileType(DamageAttachments, Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));

  if DamageAttachments.RecordCount > 0 then begin
    Attacher.DownloadAttachments(Master.FieldByName('damage_id').AsInteger, qmftDamage,
      DamageFileList, DamageErrorLog, DamageImageList, OutputFolder, True);
    FilterForNoPDFs(DamageAttachments);
  end;

  if TicketAttachments.RecordCount > 0 then begin
    Attacher.DownloadAttachments(Ticket.FieldByName('ticket_id').AsInteger, qmftTicket,
      TicketFileList, TicketErrorLog, TicketImageList, OutputFolder, True);
    FilterForNoPDFs(TicketAttachments);
  end;

  DamageAttachmentsDS.DataSet := ReplaceCVs(Master.FieldByName('damage_id').AsInteger,
    qmftDamage, DamageAttachments, DamageFileList, GetReportRegEx(SP.Connection), IniName);

  TicketAttachmentsDS.DataSet := ReplaceCVs(Ticket.FieldByName('ticket_id').AsInteger,
    qmftTicket, TicketAttachments, TicketFileList, GetReportRegEx(SP.Connection), IniName);
end;

procedure TDamageShortDM.TicketAfterScroll(DataSet: TDataSet);
begin
  inherited;
  Locates.Filter := 'ticket_id=' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger);
  Locates.Filtered := True;
  TicketNotes.Filter := 'ticket_id=' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger);
  TicketNotes.Filtered := True;
  TicketAttachmentsDS.DataSet.Filter := 'foreign_id=' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger);
  TicketAttachmentsDS.DataSet.Filtered := True;

  TicketErrorLog.Clear;
end;

procedure TDamageShortDM.ppGroupHeaderBand3BeforeGenerate(
  Sender: TObject);
var
  Image: string;
  IsXml: Boolean;
begin
  inherited;
  NoTicketWarningLabel.Visible := Ticket.RecordCount = 0;
  Image := GetDisplayableTicketImage(Ticket.FieldByName('image').AsString,
    Ticket.FieldByName('image_format').AsString, IsXml);
  ImageMemo.Lines.Text := RemoveExcessWhitespace(Image);
end;

procedure TDamageShortDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

function TDamageShortDM.GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
begin
  Result := -1;
  Damage.Parameters.ParamByName('uq_damage_id').value := UQDamageID;
  Damage.Open;
  try
    if not Damage.IsEmpty then
      Result := Damage.Fields[0].Value;
  finally
    Damage.Close;
  end;
end;

procedure TDamageShortDM.ppTicketImagePrint(Sender: TObject);
begin
  ShowDownloadedImage(TicketFileList, TicketErrorLog, TicketAttachmentsDS.DataSet, ppTicketImage, ppMemoErrorMessageTicket);
end;

procedure TDamageShortDM.ppDamageImagePrint(Sender: TObject);
begin
  ShowDownloadedImage(DamageFileList, DamageErrorLog, DamageAttachmentsDS.DataSet, ppDamageImage, ppMemoErrorMessageDamage);
end;

procedure TDamageShortDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(TicketFileList);
  FreeAndNil(DamageFileList);
  FreeAndNil(TicketErrorLog);
  FreeAndNil(DamageErrorLog);
  FreeAndNil(Attacher);
end;

procedure TDamageShortDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TicketFileList := TStringList.Create;
  DamageFileList := TStringList.Create;
  TicketErrorLog := TStringList.Create;
  DamageErrorLog := TStringList.Create;
  ppChildReportTicketAttachments.Units := utScreenPixels;
  ppChildReportDamageAttachments.Units := utScreenPixels;
end;

procedure TDamageShortDM.ThirdPartyDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
begin
  inherited;
  ThirdPartyDetailRegion.Visible   := ThirdParty.RecordCount > 0;
  NoThirdPartyWarningLabel.Visible := ThirdParty.RecordCount = 0;
end;

procedure TDamageShortDM.LitigationDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
begin
  inherited;
  LitigationDetailRegion.Visible   := Litigation.RecordCount > 0;
  NoLitigationWarningLabel.Visible := Litigation.RecordCount = 0;
end;

procedure TDamageShortDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
var
  FileList: TStrings;
begin
  FileList := TStringList.Create;
  try
    FileList.AddStrings(DamageFileList);
    FileList.AddStrings(TicketFileList);
    MergedPDFFileName := MergePDFs(FileList, OutputDir, ReportFileName, IniName);
  finally
    FreeAndNil(FileList);
  end;
end;

initialization
  RegisterReport('DamageShort', TDamageShortDM);

end.
