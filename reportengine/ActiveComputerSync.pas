unit ActiveComputerSync;
//ADO converted
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppBands, ppCtrls, ppClass, ppVar,
  ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, DB,
  ppStrtch, ppSubRpt, ppModule, daDataModule, ppMemo, ppParameter, ppDesignLayer,Data.Win.ADODB;

type
  TActiveComputerSyncDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    Pipe: TppDBPipeline;
    ppSummaryBand1: TppSummaryBand;
    ActivitySum1Rpt: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText3: TppDBText;
    ppLabel14: TppLabel;
    ppReportCopyright: TppLabel;
    ppCalc1: TppCalc;
    ppSystemVariable2: TppSystemVariable;
    ppReportFooterShape1: TppShape;
    ppHeaderBand2: TppHeaderBand;
    ppDBText1: TppDBText;
    ppDBText4: TppDBText;
    ppDBText8: TppDBText;
    ppDBText10: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel23: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ActivitySum1DS: TDataSource;
    ActivitySum1Pipe: TppDBPipeline;
    ppLabel54: TppLabel;
    ppFooterBand2: TppFooterBand;
    ppDBText53: TppDBText;
    ppSummaryBand5: TppSummaryBand;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppShape3: TppShape;
    ppLabel17: TppLabel;
    ShowDetailsLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppDBText2: TppDBText;
    ppLabel11: TppLabel;
    ActivitySum1PipeppField1: TppField;
    ppLine1: TppLine;
    ppLabel4: TppLabel;
    ppDBCalc4: TppDBCalc;
    ppLine8: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText5: TppDBText;
    PipeppField1: TppField;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine3: TppLine;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLine6: TppLine;
    ppDBCalc3: TppDBCalc;
    ppLine7: TppLine;
    ppShape1: TppShape;
    ppLabel5: TppLabel;
    ppShape2: TppShape;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppCalc2: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLine2: TppLine;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLine9: TppLine;
    ppLine10: TppLine;
    ppDBText6: TppDBText;
    company_name: TppField;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppDBText11: TppDBText;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppLabel1: TppLabel;
    ppLabel9: TppLabel;
    ShowProfitCenter: TppLabel;
    RetireReasonsLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ActivitySum1: TADODataSet;
    Master: TADODataSet;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, StrUtils, odADOUtils;

{$R *.dfm}

{ TActiveComputerSyncDM }

procedure TActiveComputerSyncDM.Configure(QueryFields: TStrings);
var
  ProfitCenter: string;
  ShowDetail: Boolean;
  RetireReasons: string;
  rtn:integer;
begin
  inherited;
  ProfitCenter := SafeGetString('pc', '');
  ShowDetail := SafeGetInteger('ShowDetail', 1) = 1;
  RetireReasons := SafeGetString('RetireReasons', '00'); // default to show only non-retired

  ActivitySum1Rpt.Visible := ShowDetail;
  SP.Parameters.ParamByName('@pc').Value := ProfitCenter;
  SP.Parameters.ParamByName('@include_retire_reasons').Value := RetireReasons;
  SP.Open;

//  SP.NextRecordset(rtn);

  LoadNextRecordset(SP, Master);
  LoadNextRecordset(SP, ActivitySum1);

  ShowText(ShowDetailsLabel, 'Show Details: ', BooleanToStringYesNo(ShowDetail));
  ShowText(ShowProfitCenter, 'Profit Center: ', IfThen(not IsEmpty(ProfitCenter), ProfitCenter, 'All'));
  ShowText(RetireReasonsLabel, 'Retire Reasons: ', RetireReasons);
end;

initialization
  RegisterReport('ActiveComputerSync', TActiveComputerSyncDM);

end.
