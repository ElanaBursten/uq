{TODO:
* Replace ppReport with actual report format
* Use hierachy calls below
* Implement SP
}
unit DailySales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppVar, ppBands, ppCtrls,
  ppPrnabl, ppClass, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,

  Data.Win.ADODB;

type
  TDailySalesReportDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppLabel13: TppLabel;
    daDataModule1: TdaDataModule;
    ppLabel1: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  private
    { Private declarations }
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses
  ReportEngineDMu, OdRbHierarchy;

{$R *.dfm}

{ TDailySalesReportDM }
procedure TDailySalesReportDM.Configure(QueryFields: TStrings);
begin
  inherited;
//  AddHierGroups(Report, short_nameDBText);

  with SP do begin
    Parameters.ParamByName('@ManagerID').value := GetInteger('ManagerID');
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Open;
  end;
end;

initialization
  RegisterReport('DailySales', TDailySalesReportDM);
end.
