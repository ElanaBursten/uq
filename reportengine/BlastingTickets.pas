unit BlastingTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TBlastingTicketsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppLabel2: TppLabel;
    ClientCodeText: TppDBText;
    ppLabel7: TppLabel;
    ClosedText: TppDBText;
    ppLabel3: TppLabel;
    LocatorText: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    TicketNumberText: TppDBText;
    ppLabel4: TppLabel;
    ReceivedText: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    DescriptionMemo: TppDBMemo;
    WorkTypeText: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    WorkAddress: TppVariable;
    WorkCityState: TppVariable;
    ppLabel15: TppLabel;
    TicketCount: TppVariable;
    ppLabel17: TppLabel;
    ppDBText1: TppDBText;
    ppLabel19: TppLabel;
    ppDBText2: TppDBText;
    ppLabel20: TppLabel;
    ppDBText4: TppDBText;
    ppLine1: TppLine;
    MemoClients: TppMemo;
    ClientsLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ContactRegion: TppRegion;
    ppLabel8: TppLabel;
    ContactNameText: TppDBText;
    ContactAddressText: TppDBText;
    ContactCityStateZip: TppVariable;
    ContactNamePhone: TppVariable;
    SP: TADOStoredProc;
    ClientsDataSet: TADODataSet;
    Master: TADODataSet;
    procedure ContactCityStateZipCalc(Sender: TObject; var Value: Variant);
    procedure ContactNamePhoneCalc(Sender: TObject; var Value: Variant);
    procedure WorkAddressCalc(Sender: TObject; var Value: Variant);
    procedure WorkCityStateCalc(Sender: TObject; var Value: Variant);
    procedure ppGroupHeaderBand1BeforeGenerate(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdMiscUtils, QMConst, ReportEngineLog, StrUtils, odAdoUtils;

procedure TBlastingTicketsDM.Configure(QueryFields: TStrings);
var
  ClientIDs: String;
  ManagerID: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  ClientIDs := SafeGetString('ClientIds', '');
  ManagerID := SafeGetInteger('ManagerId', -1);

  SP.Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
  SP.Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
  SP.Parameters.ParamByName('@ClientIds').value := ClientIDs;
  SP.Parameters.ParamByName('@ManagerId').value := ManagerID;
  SP.Open;

  Master.RecordSet :=   SP.recordSet;
  ClientsDataSet.RecordSet := SP.recordSet.NextRecordset(RecsAffected);

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  ShowEmployeeName(ManagerLabel, ManagerId);

  // the dot and spaces in the string above is a little odd, but it is being used for layout purposes.
  // it makes the memo not overlap the ClientsLabel and use the space above this label in case there
  // are 2 lines in the memo.
  if SameText(ClientIDs, '') then
    MemoClients.Lines.Text := '.              ' + 'All'
  else
    MemoClients.Lines.Text := '.              ' + GetDataSetFieldPlainString(ClientsDataset, 'client_code', 40);
end;

procedure TBlastingTicketsDM.ContactCityStateZipCalc(Sender: TObject; var Value: Variant);
var
  City, State, Zip : string;
begin
  City  := Master.FieldByName('con_city').AsString;
  State := Master.FieldByName('con_state').AsString;
  Zip   := Master.FieldByName('con_zip').AsString;

  if (City = '') then
    Value := State
  else if (State <> '') then
    Value := City + ', ' + State
  else
    Value := City;

  if (Zip <> '') then
    if (Value = '') then
      Value := Zip
    else
      Value := Value + ' ' + Zip;
end;

procedure TBlastingTicketsDM.ContactNamePhoneCalc(Sender: TObject;
  var Value: Variant);
var
  Name, Phone : string;
begin
  Name  := Master.FieldByName('caller_contact').asstring;
  Phone := Master.FieldByName('caller_phone').asstring;
  if (Name <> '') and (Phone <> '') then
    Value := Name + ' (' + Phone + ')'
  else if Name <> '' then
    Value := Name
  else
    Value := Phone;
end;

procedure TBlastingTicketsDM.WorkAddressCalc(Sender: TObject;
  var Value: Variant);
var
  S, Number, Address, Cross : string;
begin
  Number  := Master.FieldByName('work_address_number').asstring;
  Address := Master.FieldByName('work_address_street').asstring;
  Cross   := Master.FieldByName('work_cross').asstring;
  if Number = '' then
    S := Address
  else if Address = '' then
    S := Number
  else
    S := Number + ' ' + Address;
  if S = '' then
    Value := Cross
  else if Cross = '' then
    Value := S
  else
    Value := S + ' & ' + Cross;
end;

procedure TBlastingTicketsDM.WorkCityStateCalc(Sender: TObject;
  var Value: Variant);
var
  City, State : string;
begin
  City  := Master.FieldByName('con_city').AsString;
  State := Master.FieldByName('con_state').AsString;

  if (City = '') then
    Value := State
  else if (State <> '') then
    Value := City + ', ' + State
  else
    Value := City;
end;

procedure TBlastingTicketsDM.ppGroupHeaderBand1BeforeGenerate(Sender: TObject);
begin
  TicketCount.Value := TicketCount.Value + 1;
end;

initialization
  RegisterReport('BlastingTickets', TBlastingTicketsDM);

end.

