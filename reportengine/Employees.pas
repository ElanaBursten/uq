unit Employees;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppCtrls, ppBands, ppClass,
  ppVar, ppPrnabl, ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB,
  ppDBPipe, DB, ppStrtch, ppMemo, ppRegion, ppDesignLayer, ppParameter,
  myChkBox, Data.Win.ADODB;

type
  TEmployeesDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    short_nameDBText: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppLabel11: TppLabel;
    EmployeeTypeLabel: TppMemo;
    ppRegion1: TppRegion;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    IncentivePayLabel: TppLabel;
    IncentivePayFlag: TppDBText;
    SP: TADOStoredProc;
    Reference: TADODataSet;
    Master: TADODataSet;
    procedure ppDBText8GetText(Sender: TObject; var Text: string);
    procedure ResizeToFit(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses OdRbHierarchy, OdMiscUtils, OdDbUtils, odADOutils, RbCustomTextFit;    //ReportEngineDMu,

procedure TEmployeesDM.Configure(QueryFields: TStrings);
var
  SortBy: Integer;
  EmployeeTypeList: string;
  ManagerID: Integer;
  EmployeeStatus: Integer;
  EmployeeTypes: TStringList;
  RecsAffected : oleVariant;
begin
  inherited;
  EmployeeTypeList := SafeGetString('EmployeeTypeList', '');
  if not IsEmpty(EmployeeTypeList) then
    if not ValidateIntegerCommaSeparatedList(EmployeeTypeList) then
      raise Exception.Create('The parameter EmployeeTypeList=' + EmployeeTypeList + ' is not valid.');

  ManagerID := GetInteger('ManagerID');
  EmployeeStatus := GetInteger('EmployeeStatus');
  SortBy := SafeGetInteger('SortBy',0);
  if SortBy = 0 then // sort by Employee Name
    AddHierGroups(Report, short_nameDBText, 0.15);
  with SP do begin
    Parameters.ParamByName('@ManagerID').Value := ManagerID;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Parameters.ParamByName('@EmployeeTypeList').value := EmployeeTypeList;
    Open;
  end;

  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);


  Master.Recordset      :=   SP.Recordset;
  Reference.Recordset   :=   SP.Recordset.NextRecordset(RecsAffected);

  case SortBy of
    0: Master.IndexFieldNames := 'h_namepath';
    1: Master.IndexFieldNames := 'h_emp_id';
    2: Master.IndexFieldNames := 'h_eff_payroll_pc';
  end;

  EmployeeTypes := TStringList.Create;
  try
    EmployeeTypes.Delimiter := ',';
    GetDataSetFieldValueList(EmployeeTypes, Reference, 'description');
    EmployeeTypeLabel.Text := StringReplace(EmployeeTypes.DelimitedText, ',', ', ', [rfReplaceAll]);
    EmployeeTypeLabel.Text := StringReplace(EmployeeTypeLabel.Text, '"', '', [rfReplaceAll]);
  finally
    FreeAndNil(EmployeeTypes);
  end;
end;

procedure TEmployeesDM.ResizeToFit(Sender: TObject);
begin
  inherited;
  ScaleRBTextToFit(Sender as TppDBText, 7);
end;

procedure TEmployeesDM.ppDBText8GetText(Sender: TObject; var Text: string);
begin
  if Text = 'False' then
    Text := 'No'
  else
    Text := '';
end;

initialization
  RegisterReport('Employees', TEmployeesDM);

// h_charge_cov is a ref_id. We need to get the 'code'
end.
