unit ReportWebModuleU;

interface

uses
  SysUtils, Classes, HTTPApp, DB, ReportEngineDMu, BaseReport, ReportConfig,
  ppPDFDevice;

type
  ReportFormatHandler = procedure(RepDM: TBaseReportDM;
                                  Response: TWebResponse;
                                  OutputStream: TStream) of object;

  TReportWebModule = class(TWebModule)
    procedure PDFActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure TSVActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure ReportWebModuleQMServerActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    LoggingAndSecurityDM: TReportEngineDM;
    RequestID: string;
    DM: TReportEngineDM;
    UserID: Integer;
    Config: TReportConfig;
    Phase: string;
    SwitchoverDate: TDateTime;
    Params: TStringList;
    RepName: string;
    UseMainDBForReport: Boolean;
    procedure Startup;
    procedure PerformReportAction(Handler: ReportFormatHandler; Request: TWebRequest;
      Response: TWebResponse);
    procedure GenerateTSV(RepDM: TBaseReportDM;
                                  Response: TWebResponse;
                                  OutputStream: TStream);
    procedure GeneratePDF(RepDM: TBaseReportDM;
                                  Response: TWebResponse;
                                  OutputStream: TStream);
    function ConfigDescription: string;
    function GetStackTrace: string;
    procedure LoadReportIniSettings;
    procedure StatusUpdate(Msg: string);
    procedure MungeRepName;
    procedure LogRequestStart;
    procedure LogRequestSuccess;
    procedure LogRequestFailure(ErrorMessage: string);
    function ParseDateTime(ParamName: string): TDateTime;
    function ClientSupportsParamsInResponse(ClientVersion: string): Boolean;
  end;

var
  ReportWebModule: TReportWebModule;
  IniName: string;

implementation

uses
  JclDebug, IniFiles, ReportEngineLog, OdIsoDates, OdMiscUtils;

const
  SupportParamsVersion = '2.0.96.26';
  DefaultSwitchoverDate = '2020-01-01';

{$R *.DFM}

procedure TReportWebModule.LoadReportIniSettings;
var
  Ini: TIniFile;
  S: string;
  DisableMessage: string;
  IsDebug: boolean;
begin
  Assert(NotEmpty(RepName), 'RepName must be known to get settings');
  Ini := TIniFile.Create(IniName);
  try
    isDebug := Ini.ReadBool('Dev', 'Debug', False );
    if isDebug then UseMainDBForREport := False
    else begin
      S := Ini.ReadString('ReportDatabaseUsage', RepName, '');
      UseMainDBForReport := (LowerCase(S) = 'main');

      SwitchoverDate := IsoStrToDate(Ini.ReadString('TimesheetReports', 'SwitchoverDate', DefaultSwitchoverDate));

      DisableMessage := Ini.ReadString('DisableReports', RepName, '');
      if NotEmpty(DisableMessage) then
        raise Exception.Create(DisableMessage);
    end;
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TReportWebModule.PerformReportAction(Handler: ReportFormatHandler;
  Request: TWebRequest; Response: TWebResponse);
var
  Str: TMemoryStream;
  RepDM: TBaseReportDM;
  ErrorMsg: string;
begin
  try
    if not Assigned(Params) then
      Params := TStringList.Create
    else
      Params.Clear;
    Startup;

    Phase := 'Getting Params from Request';
    Params.Text := Request.Content;

    // Get the params from query fields if they aren't in the POST data
    if Params.Count = 0 then begin
      Phase := 'Getting Params from Query String';
      Params.Assign(Request.QueryFields);
    end;

    DM := nil;
    RepDM := nil;
    try
      if Params.Count = 0 then
        raise Exception.Create('No parameters found in query string or POST data');

      Phase := 'Creating Report Object';
      RepName := Params.Values['Report'];
      if IsEmpty(RepName) then
        raise Exception.Create('No report ID parameter specified');
      UserID := StrToIntDef(Params.Values['User'], 0);

      Phase := 'Logging request start';
      LogRequestStart;

      if Params.Count = 0 then
        raise Exception.Create('No Report ID parameter found');

      LoadReportIniSettings;
      MungeRepName;

      RepDM := CreateReport(RepName, IniName);
      Assert(Assigned(RepDM));

      Phase := 'Connecting to Database';
      DM := TReportEngineDM.Create(nil);
      if UseMainDBForReport then
        Config.ConnectMainDb(DM.Conn, StatusUpdate)
      else
        Config.ConnectReportingDb(DM.Conn, StatusUpdate);

      Phase := 'Adding log entry for report';
      AddLogEntry(RepName, 'Connected: ' + Phase);

      Phase := 'Connecting Report Object to DB';
      RepDM.UseConnection(DM.Conn, Config.QueryTimeout);

      Params.Delimiter := '|';
      AddLogEntry(RepName, Params.DelimitedText);

      Phase := 'Checking Security';
      Assert(Assigned(LoggingAndSecurityDM), 'Security database is not connected');
      LoggingAndSecurityDM.CheckReportSecurity(Params, UserID, Config.QueryTimeout);

      AddLogEntry(RepName, 'Query start');
      Phase := 'Running Query(s)';
      RepDM.Configure(Params);
      Str := TMemoryStream.Create;

      Handler(RepDM, Response, Str);
      Str.Position := 0;
      Phase := 'Print/Export Done';

      DM.Conn.Connected := False;  // release our use of the database as soon as we are done

      if Str.Size = 0 then
        raise Exception.Create('Report is empty');

      if ClientSupportsParamsInResponse(Params.Values['Ver']) then begin
        Phase := 'Params Attachment';
        RepDM.AttachParamsToStream(Str);
      end;

      AddLogEntry(RepName, 'Done');
      Response.ContentStream := Str;
      Phase := 'Generation Complete';
    finally
      FreeAndNil(RepDM);
      FreeAndNil(DM);
    end;
    LogRequestSuccess;
  except
    on E: Exception do begin
      Response.ContentType := 'text/plain';
      ErrorMsg := E.Message + ConfigDescription;
      ErrorMsg := ErrorMsg + GetStackTrace;
      Response.Content := 'ERROR: ' + ErrorMsg;
      AddLogEntry(RepName, ErrorMsg, True);
      LogRequestFailure(ErrorMsg);
    end;
  end;
end;

procedure TReportWebModule.PDFActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  PerformReportAction(GeneratePDF, Request, Response);
end;

procedure TReportWebModule.GeneratePDF(RepDM: TBaseReportDM;
  Response: TWebResponse; OutputStream: TStream);
begin
  AddLogEntry(RepName, 'PDF start');
  Response.ContentType := 'application/pdf';
  Phase := 'Making PDF';
  RepDM.CreatePDFStream(OutputStream);
  Phase := 'After Print';
  RepDM.AfterPrint;   // We only do this after a Print, not after an export
end;

procedure TReportWebModule.TSVActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  PerformReportAction(GenerateTSV, Request, Response);
end;

procedure TReportWebModule.GenerateTSV( RepDM: TBaseReportDM;
  Response: TWebResponse; OutputStream: TStream);
begin
  AddLogEntry(RepName, 'TSV start');
  Response.ContentType := 'text/plain';
  Phase := 'Exporting as TSV';
  RepDM.CreateDelimitedStream(OutputStream, #9);
end;

procedure TReportWebModule.ReportWebModuleQMServerActionAction(
  Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
begin
  Handled := True;
  Response.ContentType := 'text/plain';
  Response.Content := 'ERROR: Please upgrade to the current version of Q Manager to save timesheet.';
end;

procedure TReportWebModule.Startup;
begin
  Phase := 'Starting exception tracking';
  JclStartExceptionTracking;
  Phase := 'Loading Config';
  Config := LoadConfig(IniName);

  Assert(Assigned(Config), 'Config could not be loaded');
  Phase := 'Checking Log File Name';
  if NotEmpty(Config.LogFileName) then begin
    Phase := 'Starting Log';
    StartLogging(Config.LogFileName);
  end;
end;

function TReportWebModule.GetStackTrace: string;
var
  Stack: TStrings;
begin
  Result := '';
  Stack := TStringList.Create;
  try
    JclLastExceptStackListToStrings(Stack, True, True, True);
    Result := sLineBreak + Stack.Text;
  finally
    Stack.Free;
  end;
end;

function TReportWebModule.ConfigDescription: string;
begin
  Result := '  Phase=' + Phase;
  if Assigned(Config) then
    Result := Result + Config.Description;
end;

procedure TReportWebModule.StatusUpdate(Msg: string);
begin
  Phase := Msg;
end;

function TReportWebModule.ParseDateTime(ParamName: string): TDateTime;
var
  S: string;
begin
  S := Trim(Params.Values[ParamName]);
  if Length(S) = 10 then
    Result := IsoStrToDate(S)
  else
    Result := IsoStrToDateTime(S);
end;

procedure TReportWebModule.MungeRepName;
var
  D: TDateTime;
begin
  // Change RepName based on the params, to load a different report
  // for a few special cases
  D := 0.0;  // by default, not a cutover date report

  if StringInArray(RepName, ['TimesheetDetail', 'TimesheetSummary']) then
    D := ParseDateTime('end_date');  // 2-week period, Saturday ending

  if (RepName='TimeExport') then
    D := ParseDateTime('period_ending');   // 2-week period, Saturday ending

  if (RepName='TimesheetException') then
    D := ParseDateTime('StartDate');   // just one day

  if D >= SwitchoverDate then
    RepName := RepName + 'New';
end;

procedure TReportWebModule.LogRequestStart;
var
  OldClient: Boolean;
  BareParams: TStringList;
begin
  Phase := 'Connection to main DB for security & request tracking';
  LoggingAndSecurityDM := TReportEngineDM.Create(nil);
  Config.ConnectMainDb(LoggingAndSecurityDM.Conn, StatusUpdate);
  RequestID := Params.Values['RequestID'];
  OldClient := False;

  // For old clients, make up our own req id
  if IsEmpty(RequestID) then begin
    RequestID := MakeGUIDString;
    OldClient := True;
  end;

  BareParams := TStringList.Create;
  try
    BareParams.Assign(Params);
    BareParams.Values['RequestID'] := '';
    BareParams.Values['Report'] := '';
    BareParams.Values['User'] := '';
    BareParams.Delimiter := '|';

    try
      LoggingAndSecurityDM.LogReportStart(RequestId, RepName, UserID, BareParams.DelimitedText, OldClient);
    except
      // logging the start failed, so dont log the stop
      RequestId := '';
      raise;
    end;
  finally
    BareParams.Free;
  end;
end;

procedure TReportWebModule.LogRequestSuccess;
begin
  if IsEmpty(RequestId) then
    Exit;

  LoggingAndSecurityDM.LogReportResult(RequestId, True, '');
  FreeAndNil(LoggingAndSecurityDM);
end;

procedure TReportWebModule.LogRequestFailure(ErrorMessage: string);
begin
  if IsEmpty(RequestId) then
    Exit;
  try
    LoggingAndSecurityDM.LogReportResult(RequestId, False, ErrorMessage);
  except
    // Nothing we can do about it
  end;
  FreeAndNil(LoggingAndSecurityDM);
end;

function TReportWebModule.ClientSupportsParamsInResponse(ClientVersion: string): Boolean;
var
  ClientVersionNumber: TVersionNumber;
  SupportParamsVersionNumber: TVersionNumber;
begin
  ClientVersionNumber := ParseVersionString(ClientVersion);
  SupportParamsVersionNumber := ParseVersionString(SupportParamsVersion);
  Result := not CompareVersionNumber(ClientVersionNumber,SupportParamsVersionNumber) < 0;
end;

end.

