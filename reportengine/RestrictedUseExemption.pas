unit RestrictedUseExemption;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd,
  ppClass, ppReport, ppVar, ppBands, ppCtrls, ppPrnabl, ppCache, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TRestrictedUseExemptionDM = class(TBaseReportDM)
    Pipeline: TppDBPipeline;
    DS: TDataSource;
    Report: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    GrantedToName: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    GrantedByName: TppDBText;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    EffectiveDate: TppDBText;
    ExpireDate: TppDBText;
    RevokedDate: TppDBText;
    RevokedByName: TppDBText;
    GrantedDateRangeLabel: TppLabel;
    RevokedDateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    EmployeesUnderLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  RestrictedUseExemptionDM: TRestrictedUseExemptionDM;

implementation

uses ReportEngineDMu, StrUtils;

{$R *.dfm}

{ TRestrictedUseExemptionDM }

procedure TRestrictedUseExemptionDM.Configure(QueryFields: TStrings);
var
  RevokedStart, RevokedEnd: TDateTime;
  ManagerID: Integer;
  EmpStatus: Integer;
  EmpsToInclude: Integer;
begin
  inherited;
  RevokedStart := SafeGetDateTime('RevokedStart');
  RevokedEnd := SafeGetDateTime('RevokedEnd');
  ManagerID := SafeGetInteger('ExemptionsForID', 0);
  EmpStatus := SafeGetInteger('EmployeeStatus', 2);
  EmpsToInclude := GetInteger('ExemptionsFor');
  with SP do begin
    Parameters.ParamByName('@ExemptionsFor').value := EmpsToInclude;
    Parameters.ParamByName('@ExemptionsForID').value := ManagerID;
    Parameters.ParamByName('@GrantedFromDate').value := GetDateTime('GrantedStart');
    Parameters.ParamByName('@GrantedToDate').value := GetDateTime('GrantedEnd');
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    if RevokedStart > 0 then
      Parameters.ParamByName('@RevokedFromDate').value := RevokedStart
    else
      Parameters.ParamByName('@RevokedFromDate').value := Null;
    if RevokedEnd > 0 then
      Parameters.ParamByName('@RevokedToDate').value := RevokedEnd
    else
      Parameters.ParamByName('@RevokedToDate').value := Null;
    Open;
  end;
  ShowDateRange(GrantedDateRangeLabel, 'GrantedStart', 'GrantedEnd', 'Granted:');
  ShowDateRange(RevokedDateRangeLabel, 'RevokedStart', 'RevokedEnd', 'Revoked:');
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  ManagerLabel.Visible := EmpsToInclude <> 0;
  if EmpsToInclude = 0 then
    ShowText(EmployeesUnderLabel, 'Employees Included:', 'Any Employee')
  else if EmpsToInclude = 1 then begin
    ShowText(EmployeesUnderLabel, 'Employees Included:', 'Single Employee');
    ShowEmployeeName(ManagerLabel, ManagerID, 'Employee:');
  end else if EmpsToInclude = 2 then begin
    ShowText(EmployeesUnderLabel, 'Employees Included:', 'Any Under Manager');
    ShowEmployeeName(ManagerLabel, ManagerID, 'Manager:');
  end;
end;

initialization
  RegisterReport('RestrictedUseExemption', TRestrictedUseExemptionDM);

end.
