unit DailyClientClosingNNG;

interface

uses
  SysUtils, Classes, ppCtrls, ppPrnabl, ppClass, ppDB, ppBands, ppCache,
  DB, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, ppVar,
  BaseReport, ppModule, daDataModule, ppStrtch, ppSubRpt, ppMemo, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TDailyClientClosingDMNNG = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel4: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppSummaryBand1: TppSummaryBand;
    daDataModule1: TdaDataModule;
    ppLabel5: TppLabel;
    ppLabel3: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText5: TppDBText;
    ppDBText3: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ClientCodeLabel: TppLabel;
    TicketsStatusedFrom: TppLabel;
    TicketsTransmittedFrom: TppLabel;
    CallCenterLabel: TppLabel;
    LocatesLabel: TppLabel;
    ShowStatusHistoryLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
  end;

implementation

uses ReportEngineDMu, QMConst, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TDailyClientClosingDM }

procedure TDailyClientClosingDMNNG.Configure(QueryFields: TStrings);
var
  FromDateParam: TDateTime;
  ToDateParam: TDateTime;
  ClientCodeList: String;
  CallCenter: String;
  LocateClosed: Integer;
  ShowStatusHistory: Boolean;
begin
  inherited;

  FromDateParam := GetDateTime('date'); // Legacy parameter name
  ToDateParam := SafeGetDateTime('sync_to_date');

  if (FromDateParam = 0) and (ToDateParam = 0) then begin
    FromDateParam := EncodeDate(1900, 1, 1);
    ToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsStatusedFrom, '');
  end
  else if (FromDateParam > 0) and (ToDateParam > 0) then begin
    ShowText(TicketsStatusedFrom, DateToStr(FromDateParam) + ' - ' + DateToStr(ToDateParam - 1))
  end
  else begin
    ToDateParam := FromDateParam + 1;
    ShowText(TicketsStatusedFrom, DateToStr(FromDateParam));
  end;
  SP.Parameters.ParamByName('@SyncDateFrom').value := FromDateParam;
  SP.Parameters.ParamByName('@SyncDateTo').value := ToDateParam;

  FromDateParam := SafeGetDateTime('xmit_from_date');
  ToDateParam := SafeGetDateTime('xmit_to_date');
  if (FromDateParam > 0) and (ToDateParam > 0) then
    ShowText(TicketsTransmittedFrom, DateToStr(FromDateParam) + ' - ' + DateToStr(ToDateParam - 1))
  else begin
    FromDateParam := EncodeDate(1900, 1, 1);
    ToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsTransmittedFrom, '');
  end;

  ClientCodeList := GetString('client_code_list');
  CallCenter := GetString('call_center');
  LocateClosed := SafeGetInteger('locate_closed', Ord(ocClosed)); // Old default = Open only
  ShowStatusHistory := SafeGetString('show_status_history', '0') = '1'; // 0 - False, 1 - True

  SP.Parameters.ParamByName('@XmitDateFrom').value := FromDateParam;
  SP.Parameters.ParamByName('@XmitDateTo').value := ToDateParam;
  SP.Parameters.ParamByName('@ClientCodeList').value := ClientCodeList;
  SP.Parameters.ParamByName('@CallCenter').value := CallCenter;
  SP.Parameters.ParamByName('@LocateClosed').value := LocateClosed;
  SP.Parameters.ParamByName('@ShowStatusHistory').value := ShowStatusHistory;
  SP.Open;

  ShowText(CallCenterLabel, CallCenter);
  ShowText(ClientCodeLabel, ClientCodeList);

  case LocateClosed of
    Ord(ocAll): ShowText(LocatesLabel, 'All');
    Ord(ocOpen): ShowText(LocatesLabel, 'Open');
    Ord(ocClosed): ShowText(LocatesLabel, 'Closed');
  end;

  ShowText(ShowStatusHistoryLabel, BooleanToStringYesNo(ShowStatusHistory));
{
  SP.Parameters.ParamByName('@XmitDateFrom'] := FromDateParam;
  SP.Parameters.ParamByName('@XmitDateTo'] := ToDateParam;
  SP.Parameters.ParamByName('@ClientCodeList'] := GetString('client_code_list');
  SP.Parameters.ParamByName('@CallCenter'] := GetString('call_center');
  SP.Parameters.ParamByName('@LocateClosed'] := SafeGetInteger('locate_closed', Ord(ocClosed)); // Old default = Open only
  SP.Parameters.ParamByName('@ShowStatusHistory'] := SafeGetInteger('show_status_history', 0); // 0 - False, 1 - True
  SP.Open;

  ShowText(CallCenterLabel, GetString('call_center'));
  ShowText(ClientCodeLabel, GetString('client_code_list'));

  case SafeGetInteger('locate_closed', Ord(ocClosed)) of
    Ord(ocAll): ShowText(LocatesLabel, 'All');
    Ord(ocOpen): ShowText(LocatesLabel, 'Open');
    Ord(ocClosed): ShowText(LocatesLabel, 'Closed');
  end;

  ShowText(ShowStatusHistoryLabel, BooleanToStringYesNo(SafeGetString('show_status_history', '0') = '1'));
}
end;

procedure TDailyClientClosingDMNNG.CreateDelimitedStream(Str: TStream; Delimiter: string);
begin
  SaveDelimToStream(SP, Str, Delimiter, True);
end;

initialization
  RegisterReport('DailyClientClosingNNG', TDailyClientClosingDMNNG);

end.

