unit NoConflict;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppStrtch, ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TNoConflictDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText8: TppDBText;
    ppLabel5: TppLabel;
    ppDBText3: TppDBText;
    ppLabel6: TppLabel;
    ppDBText4: TppDBText;
    ppLabel7: TppLabel;
    ppDBText5: TppDBText;
    ppLabel9: TppLabel;
    ppDBText7: TppDBText;
    ppLabel10: TppLabel;
    ppDBText9: TppDBText;
    ppLabel11: TppLabel;
    ppDBText10: TppDBText;
    ppLabel12: TppLabel;
    ppDBText11: TppDBText;
    ReportLabel: TppLabel;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    SyncDateLabel: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLabel13: TppLabel;
    ppDBText6: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppSystemVariable2: TppSystemVariable;
    ppLabel14: TppLabel;
    ppLabel19: TppLabel;
    OfficeAddressMemo: TppMemo;
    ppLabel4: TppLabel;
    CallCenterLabel: TppLabel;
    StatusLabel: TppLabel;
    LocationLabel: TppLabel;
    ppDBMemo1: TppDBMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure OfficeAddressMemoPrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, StrUtils, OdMiscUtils;

{$R *.dfm}

{ TNoConflictDM }

procedure TNoConflictDM.Configure(QueryFields: TStrings);
var
  StatusList: string;
  S: string;
  I: Integer;
  CallCenter: string;
  County: string;
  StateCode: string;
  Location: string;
begin
  inherited;
  CallCenter := GetString('CallCenter');
  StateCode := SafeGetString('State', '');
  County := SafeGetString('County', '');
  with SP do begin
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Parameters.ParamByName('@CallCenter').value := CallCenter;
    Parameters.ParamByName('@State').value := StateCode;
    Parameters.ParamByName('@County').value := County;

    // Statuses is a comma-delimited list (from new clients)
    StatusList := SafeGetString('Statuses', '');

    // Handle Status1..Status6 params (old clients)
    for I := 1 to 6 do begin
      S := SafeGetString('Status' + IntToStr(I), '');
      if (S <> '**') and (S <> '') then begin
        StatusList := StatusList + S + ',';
      end;
    end;

    Parameters.ParamByName('@Statuses').value := StatusList;
    Open;
  end;

  Location := County;
  if IsEmpty(Location) then begin
    if IsEmpty(StateCode) then
      Location := 'All'
    else
      Location := ' - , ' + StateCode;
  end
  else if IsEmpty(StateCode) then
    Location := Location + ', -'
  else
    Location := Location + ', ' + StateCode;

  ShowDateRange(SyncDateLabel);
  ShowText(CallCenterLabel, 'Call Center:', CallCenter);
  ShowText(StatusLabel, 'Statuses:', IfThen(IsEmpty(StatusList), 'All', 'Filtered'));
  ShowText(LocationLabel, 'County, State:', Location);
end;

procedure TNoConflictDM.OfficeAddressMemoPrint(Sender: TObject);
var
  S : string;

  procedure AddLine(const L : string);
  begin
    if L <> '' then
      if S = '' then
        S := L
      else
        S := S + ^M^J + L;
  end;

  function CityStateZip(const City, State, Zip : string) : string;
  begin
    if City = '' then
      Result := State
    else if State <> '' then
      Result := City + ', ' + State
    else
      Result := City;
    if Zip <> '' then
      if Result = '' then
        Result := Zip
      else
        Result := Result + ' ' + Zip;
  end;

begin
  S := '';
  try
    AddLine(SP.FieldValues['company_name']);
    AddLine(SP.FieldValues['address1']);
    AddLine(SP.FieldValues['address2']);
    AddLine(CityStateZip(
      SP.FieldValues['city'],
      Trim(SP.FieldValues['state']),
      SP.FieldValues['zipcode']));
    AddLine('Phone: '+SP.FieldValues['phone']);
    AddLine('Fax: '+SP.FieldValues['fax']);
  except
    S := '';
  end;
  OfficeAddressMemo.Text := S;
end;

initialization
  RegisterReport('NoConflict', TNoConflictDM);

//http://localhost/TestDBapp/ReportEngineCGI.exe/PDF?Report=NoConflict&StartDate=2001-06-13T00:00:00&EndDate=2002-06-14T00:00:00&CallCenter=FCL1&Status1=N&Status2=C&Status3=S

end.

