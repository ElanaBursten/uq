inherited StatusAutoEmailDM: TStatusAutoEmailDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Height = 367
  Width = 630
  object qryTicketsOnQueue: TADOQuery
    Connection = ReportEngineDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CallCenter'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'Declare @Callcenter varchar(20)'
      'set @Callcenter = :Callcenter'
      ''
      
        'select distinct t.ticket_id, t.ticket_number, t.caller_email, t.' +
        'ticket_format,'
      
        '  t.kind, t.map_page, t.transmit_date, t.due_date, t.work_descri' +
        'ption,'
      
        '  t.work_state, t.work_county, t.work_city, t.work_cross, t.work' +
        '_subdivision, t.work_type,'
      
        '  t.work_date, t.work_remarks, t.legal_date, t.legal_good_thru, ' +
        't.respond_date,'
      
        '  t.company, t.con_type, t.con_name, t.con_address, t.con_city, ' +
        't.con_state, t.con_zip,'
      
        '  t.call_date, t.caller, t.caller_contact, t.caller_phone, t.cal' +
        'ler_cellular,'
      '  max(eq.ls_id) as ls_id,'
      '    ltrim(isnull(t.work_address_number, '#39#39') + '#39' '#39' +'
      '          isnull(t.work_address_number_2, '#39#39') + '#39' '#39' +'
      '          isnull(t.work_address_street, '#39#39')) as street'
      'from email_queue eq'
      '  inner join locate_status ls on ls.ls_id = eq.ls_id'
      '  inner join locate l on l.locate_id = ls.locate_id'
      '  inner join ticket t on t.ticket_id = l.ticket_id'
      'where eq.call_center = @Callcenter or @Callcenter = '#39'*'#39
      '  and l.active = 1'
      
        'group by t.ticket_id, t.ticket_number, t.caller_email, t.ticket_' +
        'format,'
      
        '  t.kind, t.map_page, t.transmit_date, t.due_date, t.work_descri' +
        'ption,'
      
        '  t.work_state, t.work_county, t.work_city, t.work_cross, t.work' +
        '_subdivision, t.work_type,'
      
        '  t.work_date, t.work_remarks, t.legal_date, t.legal_good_thru, ' +
        't.respond_date,'
      
        '  t.company, t.con_type, t.con_name, t.con_address, t.con_city, ' +
        't.con_state, t.con_zip,'
      
        '  t.call_date, t.caller, t.caller_contact, t.caller_phone, t.cal' +
        'ler_cellular,'
      
        '  t.work_address_number, t.work_address_number_2, t.work_address' +
        '_street'
      'order by t.ticket_id')
    Left = 40
    Top = 24
  end
  object qryLocatesOnQueue: TADOQuery
    Connection = ReportEngineDM.Conn
    Parameters = <
      item
        Name = 'CallCenter'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Declare @Callcenter varchar(20)'
      'set @Callcenter = :Callcenter'
      ''
      'If object_id('#39'tempDB..#tix'#39') is not NULL'
      'drop table #tix'
      ''
      'select l.ticket_id, l.locate_id'
      'into #tix'
      'from locate l'
      '  inner join locate_status ls on ls.locate_id = l.locate_id'
      '  inner join email_queue eq on eq.ls_id = ls.ls_id'
      'where eq.call_center = @Callcenter'
      '  or @Callcenter = '#39'*'#39
      ''
      
        'select l.locate_id, l.client_code, l.client_id, l.status, l.high' +
        '_profile, l.qty_marked,'
      
        '  l.closed, l.closed_date, l.active, l.high_profile_reason, l.ma' +
        'rk_type, l.ticket_id,'
      
        '  sl.status_name, c.client_name, o.company_name, o.phone, eqr.st' +
        'atus_message as status_message, incl_bcc, bcc_address'
      'from locate l'
      '  left join statuslist sl on sl.status = l.status'
      '  inner join client c on c.client_id = l.client_id'
      '  left join office o on o.office_id = c.office_id'
      
        '  left join email_queue_rules eqr on eqr.status = l.status  and ' +
        '(eqr.client_code = l.client_code or eqr.client_code = '#39'*'#39') and (' +
        'eqr.state = o.state or eqr.state = '#39'*'#39')'
      
        '  inner join #tix on (#tix.locate_id = l.locate_id) and (l.activ' +
        'e = 1)'
      'order by l.ticket_id, l.locate_id'
      ''
      'If object_id('#39'tempDB..#tix'#39') is not NULL'
      'drop table #tix'
      '')
    Left = 40
    Top = 104
  end
  object insEmailQueueResult: TADOQuery
    Connection = ReportEngineDM.Conn
    CursorType = ctStatic
    Parameters = <>
    Left = 208
    Top = 24
  end
  object delQueueRecordsForTicket: TADOQuery
    Connection = ReportEngineDM.Conn
    CursorType = ctStatic
    Parameters = <>
    Left = 200
    Top = 104
  end
end
