unit Echo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppStrtch, ppMemo, ppPrnabl, ppClass, ppCtrls,
  ppCache, ppBands, ppComm, ppRelatv, ppProd, ppReport, ppDesignLayer,
  ppParameter;

type
  TEchoDM = class(TBaseReportDM)
    ppReport1: TppReport;
    ppDetailBand1: TppDetailBand;
    ppLabel1: TppLabel;
    ppMemo1: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

{ TEchoDM }

procedure TEchoDM.Configure(QueryFields: TStrings);
begin
  inherited;
  ppMemo1.Lines.Assign(QueryFields);
end;

initialization
  RegisterReport('Echo', TEchoDM);

end.
