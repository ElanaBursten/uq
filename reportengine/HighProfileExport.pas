unit HighProfileExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB,
  Data.Win.ADODB;

type
  THighProfileExportDM = class(TBaseReportDM)
    Data: TADOStoredProc;
    Notes: TADODataSet;
    Ticket: TADODataSet;
    Master: TADODataSet;
    Locate: TADODataSet;
  private
    FTicketSaveDir: string;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string);
      override;
    procedure ExportTickets;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdMiscUtils, QMConst, JclStrings, odADOutils;

{ THighProfileExportDM }

procedure THighProfileExportDM.Configure(QueryFields: TStrings);
var
  RecsAffected : oleVariant;
begin
  inherited;
  FTicketSaveDir := SafeGetString('TicketSaveDir', '');

  Data.Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
  Data.Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
  Data.Parameters.ParamByName('@ClientCodes').value := GetString('ClientCodes');
  Data.Open;

  Master.Recordset := Data.Recordset;
  Ticket.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  Locate.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  Notes.Recordset := Data.Recordset.NextRecordset(RecsAffected);

end;

procedure THighProfileExportDM.CreateDelimitedStream(Str: TStream;
  Delimiter: string);
begin
  SaveDelimToStream(Data, Str, Delimiter, False);
  if FTicketSaveDir <> '' then begin
    ExportTickets;
  end;
end;

procedure THighProfileExportDM.ExportTickets;
var
  Image: string;
  Content: TStringList;
  FileName: string;
begin
  while not Ticket.Eof do begin
    Locate.Filter := 'ticket_id=' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger);
    Locate.Filtered := True;

    Notes.Filter := 'ticket_id=' + IntToStr(Ticket.FieldByName('ticket_id').AsInteger);
    Notes.Filtered := True;

    Content := TStringList.Create;
    try
      Image := Trim(Ticket.FieldByName('image').AsString);
      StrReplace(Image, #13#10, #10, [rfReplaceAll]);
      StrReplace(Image, #10, #13#10, [rfReplaceAll]);
      Content.Append(Image);

      Content.Add('');
      Content.Add('==============================================================================');
      Content.Add('Ticket Number: ' + Ticket.FieldByName('ticket_number').AsString);
      Content.Add('------------------------------------------------------------------------------');

      Locate.First;
      while not Locate.EOF do begin
        Content.Add('Client Term:   ' + Locate['client_code']);
        Content.Add('Status:        ' + Locate['status_name']);
        Content.Add('When Statused: ' + Locate.FieldByName('closed_date').AsString);
        Content.Add('High Profile:  ' + BoolToStr(Locate.FieldByName('high_profile').AsBoolean, True));
        Content.Add('Closed:        ' + BoolToStr(Locate.FieldByName('closed').AsBoolean, True));
        Content.Add('Assigned To:   ' + Locate['short_name']);
        Content.Add('------------------------------------------------------------------------------');
        Locate.Next;
      end;

      Notes.First;
      while not Notes.EOF do begin
        Content.Add('--- Note entered ' + Notes.FieldByName('entry_date').AsString + ' ---');
        Content.Add(Notes.FieldByName('note').AsString);
        Content.Add('');
        Notes.Next;
      end;

      Content.Add('Generated: ' + DateTimeToStr(Now));
      Content.Add('==============================================================================');

      FileName := Ticket.FieldByName('ticket_number').AsString + '.txt';
      Content.SaveToFile(IncludeTrailingPathDelimiter(FTicketSaveDir) + FileName);
    finally
      Content.Free;
    end;

    Ticket.Next;
  end;

end;

initialization
  RegisterReport('HighProfileExport', THighProfileExportDM);

{



==============================================================================
Ticket No: PB3296851    For member: PECO Energy
-------------------------------------------------------------------------------
Member Remarks  :
==============================================================================
                            ELEC4 COMPLETION
------------------------------------------------------------------------------
Dept:KC                 | Plat #:32D5GH34        | Film Roll/Frm:
------------------------------------------------------------------------------
 [ ] Primary   Qty:   1 | [ ] Painted            | [X] Excavation Site Clear
 [ ] Secondary Qty:   1 | [ ] Flagged            | [ ] Out of Service Area
------------------------| [ ] Staked             | [ ] Not Completed
 [ ] Copper             | [ ] Staked Chasers     | Unmarked Reason:
 [ ] Street Light       | [ ] Offset      0.00   |
 [ ] Duct               | [ ] High Profile       |
 [ ] Instrument         | [ ] Private Facilities |
                        | [ ] Ongoing Job        |


------------------------------------------------------------------------------
Driver Remarks:
aerial electric marked gas service out per GFR tap measurements and per service
sketches marked main out per peco prints
------------------------------------------------------------------------------
Completion created on date: 11/30/2004   at time: 06:57 pm
Ticket completed on date: 11/30/2004   at time: 06:57 pm
Completed by: MOBILE     MobileId: PA045             OperatorId: 19791

                            GAS3 COMPLETION
------------------------------------------------------------------------------
Dept:KC                 | Plat #:32D5EH34        | Film Roll/Frm:1
------------------------------------------------------------------------------
 [X] Main      Qty:   1 | [X] Painted            | [ ] Excavation Site Clear
 [X] Services  Qty:   1 | [X] Flagged            | [ ] Out of Service Area
------------------------| [ ] Staked             | [ ] Not Completed
 [X] Cast Iron          | [ ] Staked Chasers     | Unmarked Reason:
 [X] Plastic            | [X] Offset      1.00   |
 [ ] Copper             | [X] High Profile       |
 [ ] Steel              | [ ] Private Facilities |
                        | [ ] Ongoing Job        |
------------------------------------------------------------------------------
Driver Remarks:
aerial electric marked gas service out per GFR tap measurements and per service
sketches marked main out per peco prints
------------------------------------------------------------------------------
Completion created on date: 11/30/2004   at time: 06:58 pm
Ticket completed on date: 11/30/2004   at time: 06:58 pm
Completed by: MOBILE     MobileId: PA045             OperatorId: 19791
}

end.
