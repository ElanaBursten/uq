unit OldOpenTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TOldOpenTicketsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppTitleBand1: TppTitleBand;
    ppGroup1: TppGroup;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel4: TppLabel;
    ppDBText5: TppDBText;
    ppLabel5: TppLabel;
    ppDBText6: TppDBText;
    ppLabel6: TppLabel;
    ppLine3: TppLine;
    ppDBText7: TppDBText;
    ppLabel7: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel8: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable3: TppSystemVariable;
    ppShape1: TppShape;
    DateRangeLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdDbUtils;

{$R *.dfm}

{ TOldOpenTicketsDM }

procedure TOldOpenTicketsDM.Configure(QueryFields: TStrings);
begin
  inherited;

  with SP do begin
    Parameters.ParamByName('@TransmitStartDate').value := GetDateTime('TransmitDateStart');
    Parameters.ParamByName('@TransmitEndDate').value := GetDateTime('TransmitDateEnd');
    Open;
  end;

  ShowDateRange(DateRangeLabel, 'TransmitDateStart', 'TransmitDateEnd', 'Transmit Date:');
end;

initialization
  RegisterReport('OldOpenTickets', TOldOpenTicketsDM);

end.
