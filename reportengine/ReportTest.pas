unit ReportTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BaseReport, ComCtrls, ReportEngineDMu, {$IF CompilerVersion >= 16} XPMan, {$IFEND}
  ppPDFDevice, ppPDFSettings;

type
  TReportHandler = procedure(RepName: string; RepDM: TBaseReportDM) of object;

  TReportTestForm = class(TForm)
    ReportListBox: TListBox;
    Label1: TLabel;
    ReportParamMemo: TMemo;
    Label2: TLabel;
    RunButton: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EditPassword: TEdit;
    Label6: TLabel;
    EditServer: TComboBox;
    EditDB: TComboBox;
    EditUsername: TComboBox;
    RunPDFButton: TButton;
    ExportButton: TButton;
    SaveTSVDialog: TSaveDialog;
    StatusBar: TStatusBar;
    SaveSqlCheckBox: TCheckBox;
    DelimCombo: TComboBox;
    UpdateRightDefs: TButton;
    TrustedBox: TCheckBox;
    Label7: TLabel;
    CommandLineEdit: TEdit;
    lblOverrideCmdLine: TLabel;
    OverrideCmdStr: TEdit;
    procedure FormShow(Sender: TObject);
    procedure RunButtonClick(Sender: TObject);
    procedure ReportParamMemoChange(Sender: TObject);
    procedure RunPDFButtonClick(Sender: TObject);
    procedure ExportButtonClick(Sender: TObject);
    procedure UpdateRightDefsClick(Sender: TObject);
    procedure TrustedBoxClick(Sender: TObject);
    procedure CommandLineEditClick(Sender: TObject);
  private
    FDelimiter: string;
    FFileName: string;
    IniName: string;
    QueryTimeout: integer;
    procedure SaveSettings;
    procedure LoadSettings;
    procedure CreateAllReportClasses;
    procedure RunReport(Handler: TReportHandler);
    procedure Preview(RepName: string; RepDM: TBaseReportDM);
    procedure SavePDF(RepName: string; RepDM: TBaseReportDM);
    procedure ExportDelimited(RepName: string; RepDM: TBaseReportDM);
    procedure AdjustUserPass;
    procedure Connect(const DM: TReportEngineDM);
    procedure ConnectWithStr(const DM: TReportEngineDM; AConnStr: string);
    procedure UpdateCommandLine;
  end;

var
  ReportTestForm: TReportTestForm;


implementation

uses OdHourglass, IniFiles, OdMiscUtils, OdMessageForm, StrUtils,
odADOUtils;

const
  DatabaseSection = 'Database';
  ReportTestSection = 'ReportTester';

{$R *.dfm}

procedure TReportTestForm.FormShow(Sender: TObject);
begin
  // Compile without the PSRB packages, or this may not compile correctly
  ReportListBox.Items.Assign(RegisteredReportNames);
  ReportListBox.Sorted := True;
  LoadSettings;
  AdjustUserPass;
  CreateAllReportClasses;
end;

procedure TReportTestForm.LoadSettings;
var
  IniFile: TIniFile;
  LastReport: string;
  Index: Integer;
  DevDebug: boolean;
begin
  IniName := ChangeFileExt(Application.ExeName, '.ini');
  IniFile := TIniFile.Create(IniName);
  try
    DevDebug := IniFile.ReadBool('Dev', 'Debug',False);
    OverrideCmdStr.Text := IniFile.ReadString(DatabaseSection,'ConnectionString', '');
    QueryTimeout := IniFile.ReadInteger(DatabaseSection, 'QueryTimeout', 122);  //EB - Odd number to flag it
    EditServer.Text := IniFile.ReadString(DatabaseSection, 'Server', 'localhost');
    EditDB.Text := IniFile.ReadString(DatabaseSection, 'DB', 'QM');
    TrustedBox.Checked := IniFile.ReadBool(DatabaseSection, 'Trusted', False);
    EditUsername.Text := IniFile.ReadString(DatabaseSection, 'UserName', 'sa');
    EditPassword.Text := IniFile.ReadString(DatabaseSection, 'Password', '');

    LastReport := IniFile.ReadString(ReportTestSection, 'LastReport', 'zzzzzz');
    Index := ReportListBox.Items.IndexOf(LastReport);
    if Index > -1 then begin
      ReportListBox.ItemIndex := Index;
    end;
    ReportParamMemo.Lines.Text := StringReplace(IniFile.ReadString(ReportTestSection, 'Params', ''), '\n', sLineBreak, [rfReplaceAll]);
  finally
    IniFile.Free;
  end;
end;

procedure TReportTestForm.RunButtonClick(Sender: TObject);
begin
  RunReport(Preview);
end;

procedure TReportTestForm.SaveSettings;
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    IniFile.WriteString(DatabaseSection, 'Server', EditServer.Text);
    IniFile.WriteString(DatabaseSection, 'DB', EditDB.Text);
    IniFile.WriteBool(DatabaseSection, 'Trusted', TrustedBox.Checked);
    IniFile.WriteString(DatabaseSection, 'UserName', EditUsername.Text);
    IniFile.WriteString(DatabaseSection, 'Password', EditPassword.Text);

    IniFile.WriteString(ReportTestSection, 'LastReport', ReportListBox.Items[ReportListBox.ItemIndex]);
    IniFile.WriteString(ReportTestSection, 'Params', StringReplace(ReportParamMemo.Lines.Text, sLineBreak, '\n', [rfReplaceAll]));
  finally
    IniFile.Free;
  end;
end;

procedure TReportTestForm.ReportParamMemoChange(Sender: TObject);
var
  Report: string;
  ReportIndex: Integer;
begin
  Report := ReportParamMemo.Lines.Values['Report'];
  if Report <> '' then begin
    ReportIndex := ReportListBox.Items.IndexOf(Report);
    if ReportIndex > -1 then
      ReportListBox.ItemIndex := ReportIndex;
  end;

  UpdateCommandLine;
end;

procedure TReportTestForm.CreateAllReportClasses;
var
  I: Integer;
  RepName: string;
  RepDM: TBaseReportDM;
begin
  // Create and free all the DMs - this will sometimes catch problems (by
  // throwing errors) if something was left open at design time.

  for I := 0 to ReportListBox.Items.Count - 1 do begin
    RepName := ReportListBox.Items[I];
    RepDM := CreateReport(RepName, IniName);
    RepDM.Free;
  end;
end;

procedure TReportTestForm.RunPDFButtonClick(Sender: TObject);
begin
  RunReport(SavePDF);
end;

procedure TReportTestForm.RunReport(Handler: TReportHandler);
var
  DM: TReportEngineDM;
  RepDM: TBaseReportDM;
  RepName: string;
  Cursor: IInterface;
  TimeStart, TimeEnd: Int64;
begin
  Cursor := ShowHourGlass;
  SaveSettings;   // do this before running, to make it easy to re-run if
                  // the user has to kill the app.

  RepDM := nil;
  DM := TReportEngineDM.Create(nil);
  try
    Connect(DM);
    RepName := ReportParamMemo.Lines.Values['Report'];

    RepDM := CreateReport(RepName, IniName);
    RepDM.UseConnection(DM.Conn, QueryTimeout);

    StatusBar.Panels[0].Text := '(Running)';
    StatusBar.Update;
    TimeStart := GetTickCount;

    RepDM.SaveSQL := SaveSqlCheckBox.Checked;
    try
      RepDM.Configure(ReportParamMemo.Lines);
    finally
      TimeEnd := GetTickCount;
      StatusBar.Panels[0].Text := Format('Query Time: %.1f seconds', [ (TimeEnd - TimeStart) / 1000.0]);
      StatusBar.Update;
    end;

    Handler(RepName, RepDM);
  finally
    FreeAndNil(DM);
    FreeAndNil(RepDM);
  end;
end;

procedure TReportTestForm.ExportDelimited(RepName: string; RepDM: TBaseReportDM);
var
  FS: TFileStream;
begin
  SaveTSVDialog.Filename := FFileName + '.' + 'tsv';
  SaveTSVDialog.DefaultExt := 'tsv';
  if not SaveTSVDialog.Execute then
    Exit;

  FS := TFileStream.Create(SaveTSVDialog.Filename, fmCreate);
  try
    RepDM.CreateDelimitedStream(FS, FDelimiter);
    ShowMessage('Saved to ' + SaveTSVDialog.FileName);
  finally
    FS.Free;
  end;
end;

procedure TReportTestForm.Preview(RepName: string; RepDM: TBaseReportDM);
begin
  RepDM.PrintPreview;
end;

procedure TReportTestForm.SavePDF(RepName: string; RepDM: TBaseReportDM);
var
  FS: TFileStream;
  FileName: string;
begin
  FileName := GetWindowsTempPath + Copy(FFileName, 1, 150) + '.pdf';
  FS := TFileStream.Create(FileName, fmCreate);
  try
    RepDM.CreatePDFStream(FS);
  finally
    FS.Free;
  end;

  // Launch a PDF viewer, assuming one is installed.
  OdShellExecute(FileName, '', True, 0);
end;

const
  Delimiters = ',;'#9;

procedure TReportTestForm.ExportButtonClick(Sender: TObject);
begin
  FDelimiter := Delimiters[DelimCombo.ItemIndex + 1];
  RunReport(ExportDelimited);
end;

procedure TReportTestForm.UpdateRightDefsClick(Sender: TObject);
var
  DM: TReportEngineDM;
  Updates: Integer;
  Report: TStringList;
  Cursor: IInterface;
  AnyError: Boolean;
begin
  Cursor := ShowHourGlass;
  DM := TReportEngineDM.Create(nil);
  Report := TStringList.Create;
  Updates := 0;
  try
    Connect(DM);
    try
      Updates := DM.UpdateReportRightsDefinitions(RegisteredReportNames, Report, AnyError);
    finally
      Report.Add('');
      Report.Add('Connection Information');
      Report.Add('----------------------');
      Report.Add('');
      Report.Add('Server: ' + EditServer.Text);
      Report.Add('Database: ' + EditDB.Text);
      Report.Add('Trusted Connection: ' + BooleanToStringYesNo(TrustedBox.Checked));
      Report.Add('User Name: ' + EditUsername.Text);
      Report.Add('Password: ' + EditPassword.Text);
      ShowMessageForm( IfThen(AnyError, 'WARNING: Some error has occured - ', '') + Format('Update report rights result - rights updated: %d', [Updates]) , Report, AnyError);
    end;
  finally
    FreeAndNil(DM);
    FreeAndNil(Report);
  end;
end;

procedure TReportTestForm.TrustedBoxClick(Sender: TObject);
begin
  AdjustUserPass;
end;

procedure TReportTestForm.AdjustUserPass;
begin
  EditUsername.Enabled := not TrustedBox.Checked;
  EditPassword.Enabled := not TrustedBox.Checked;
end;

procedure TReportTestForm.Connect(const DM: TReportEngineDM);
var  // corrected for ADO
  tmpStrConnection: string;
  bTrusted:boolean;
begin
  tmpStrConnection := 'Provider=SQLNCLI11;';  //qm-480 Can not use OleDB providers anymore 10/7/2021
  tmpStrConnection := tmpStrConnection + 'Data Source=' + EditServer.Text; //Data Source=DYATL-DQMGDB01
  tmpStrConnection := tmpStrConnection + ';Initial Catalog='+ EditDB.Text;   //  QM
  tmpStrConnection := tmpStrConnection + ';User ID=' + EditUserName.Text;  //uqweb
  tmpStrConnection := tmpStrConnection + ';Password=' + EditPassword.Text;  //password
  if TrustedBox.Checked then
  begin
    tmpStrConnection := tmpStrConnection + ';Persist Security Info=True';
    bTrusted := true;
  end
  else
  begin
    tmpStrConnection := tmpStrConnection + ';Persist Security Info=False';
    bTrusted := false;
  end;

  if OverrideCmdStr.Text <> '' then
    tmpStrConnection := OverrideCmdStr.Text;

ConnectAdoConnection(DM.Conn,  EditServer.Text, EditDB.Text,
  bTrusted, EditUserName.Text, EditPassword.Text, true,
  tmpStrConnection);


//  ConnectAdoConnection(DM.Conn, tmpStrConnection);
end;

procedure TReportTestForm.ConnectWithStr(const DM: TReportEngineDM;
  AConnStr: string);    //EB - Work with Override
begin
  ConnectADoConnection(DM.Conn, EditServer.Text, EditDB.Text, TrustedBox.Checked,
                       EditUserName.Text, EditPassword.Text,  True, AConnStr);
end;

procedure TReportTestForm.UpdateCommandLine;
var
  C: string;
  Params: TStringList;
  I: Integer;
  Param: string;

  procedure ClearParam(const ParamName: string);
  var
    Index: Integer;
  begin
    Index := Params.IndexOfName(ParamName);
    if Index>=0 then
      Params.Delete(Index);
  end;

begin
  Params := TStringList.Create;
  try
    Params.Assign(ReportParamMemo.Lines);
    FFileName := Trim(Params.Values['Report']);
    C := 'ReportEngineCGI ' + FFileName;

    ClearParam('Report');
    ClearParam('PasswordHash');
    ClearParam('Ver');
    ClearParam('User');

    for I := 0 to Params.Count - 1 do begin
      Param := Params.Strings[I];
      Param := StringReplace(Param, 'T00:00:00.000', '', [rfReplaceAll]);
      C := C + ' ' + Param;
      FFileName := FFileName + ',' + Param
    end;

    FFileName := StringReplace(FFileName, ':', '', [rfReplaceAll]);
    FFileName := StringReplace(FFileName, ' ', '', [rfReplaceAll]);
    FFileName := StringReplace(FFileName, '*', 'ALL', [rfReplaceAll]);

    C := C + ' /o "' + FFileName + '.pdf"';

    CommandLineEdit.Text := C;
  finally
    Params.Free;
  end;
end;

procedure TReportTestForm.CommandLineEditClick(Sender: TObject);
begin
  CommandLineEdit.SelectAll;
end;

end.

