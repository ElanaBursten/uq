if object_id('dbo.get_hier2') is not null
	drop function dbo.get_hier2
go

create function get_hier2(@id int, @managers_only bit, @level_limit int)
returns
 @results TABLE (
  h_emp_id integer NOT NULL PRIMARY KEY,
  h_short_name varchar(30) NULL,
  h_first_name varchar(30) NULL,
  h_last_name varchar(30) NULL,
  h_emp_number varchar(20) NULL,
  h_type_id integer,
  h_report_level integer,
  h_report_to integer
)
as
begin
  declare @adding_level int
  select @adding_level = 1

  -- Add the user himself
  INSERT INTO @results
    (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
     h_type_id, h_report_to, h_report_level)
  select e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
     e.type_id, e.report_to, @adding_level
    from employee e
    left join employee man on e.report_to = man.emp_id
   where e.emp_id = @id

  -- Add everyone below the user
  WHILE @adding_level < @level_limit
  BEGIN
    INSERT INTO @results
         (h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number,
          h_type_id, h_report_to, h_report_level)
      select
        e.emp_id, e.short_name, e.first_name, e.last_name, e.emp_number,
        e.type_id, e.report_to, r.h_report_level+1
      from employee e
       inner join @results r on e.report_to = r.h_emp_id
            AND r.h_report_level=@adding_level
            AND ((@managers_only=0) or (e.type_id IN (5,6,7,8,9,10)) )

    SELECT @adding_level = @adding_level + 1
  END
  return
end
GO

/*
select * from get_hier2(211, 1)
select * from get_hier2(211, 0)
select * from get_hier2(2676, 1)
select * from get_hier2(2676, 0)
*/
