unit ResponderStatusTranslations;

interface

uses
  System.SysUtils, System.Classes, BaseReport,
  ppParameter, ppDesignLayer, ppBands, ppClass,
  ppVar, ppCtrls, ppRegion, ppStrtch, ppMemo, ppPrnabl, ppCache, ppProd,
  ppReport, ppComm, ppRelatv, ppDB, ppDBPipe,
  Data.DB, myChkBox, Data.Win.ADODB;

type
  TResponderStatusTranslationsDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ActiveFlagLabel: TppLabel;
    CallCenterLabel: TppLabel;
    CallCenterList: TppMemo;
    ppRegionColumnHeaders: TppRegion;
    CCLabel: TppLabel;
    OutgoingStatusCodeLabel: TppLabel;
    ExplanationCodeLabel: TppLabel;
    ResponderTypeLabel: TppLabel;
    ResponderKindLabel: TppLabel;
    ActiveLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBTextCallCenter: TppDBText;
    ppDBTextQMStatusCode: TppDBText;
    ppDBTextOutgoingStatus: TppDBText;
    ppDBTextExplanationCode: TppDBText;
    ppDBTextResponderKind: TppDBText;
    ppDBTextResponderType: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel1: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppParameterList1: TppParameterList;
    SPDS: TDataSource;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ActiveYesNo: TppDBText;
    ppLine1: TppLine;
    ppLabel3: TppLabel;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  ResponderStatusTranslationsDM: TResponderStatusTranslationsDM;

implementation
{$R *.dfm}

uses ReportEngineDMu, odADOutils, OdDbUtils;

procedure TResponderStatusTranslationsDM.Configure(QueryFields: TStrings);
var
  ActiveFlag: Integer;
  ActiveFlagDesc: string;
  CallCenters: string;
begin
  inherited;
  ActiveFlag := GetInteger('ActiveFlag');
  CallCenters := GetString('CallCenters');

  with SP do begin
    Parameters.ParamByName('@CallCenters').value := CallCenters;
    Parameters.ParamByName('@ActiveFlag').value  := ActiveFlag;
    Open;
  end;

  if ActiveFlag = 0 then
    ActiveFlagDesc := 'Inactive Only'
  else if ActiveFlag = 1 then
    ActiveFlagDesc := 'Active Only'
  else
    ActiveFlagDesc := 'All';

  if CallCenters = '*' then
   CallCenters := 'All';

  ActiveFlagLabel.Caption := ActiveFlagDesc;
  CallCenterList.Text := StringReplace(CallCenters, ',', ', ', [rfReplaceAll]);
end;

initialization
  RegisterReport('ResponderStatusTranslations', TResponderStatusTranslationsDM);

end.
