unit DamagesPerLocates;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppVar, ppBands, ppCtrls,
  ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TDamagesPerLocatesDM = class(TBaseReportDM)
    DataDS: TDataSource;
    Report: TppReport;
    ppTitleBand1: TppTitleBand;
    ppShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ProfitCenterLabel: TppLabel;
    LiabilityLabel: TppLabel;
    ppHeaderBand1: TppHeaderBand;
    DetailBand: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    DataPipe: TppDBPipeline;
    ClientNameLabel: TppLabel;
    LocateCountLabel: TppLabel;
    DamageCountLabel: TppLabel;
    DamagesPerLabel: TppLabel;
    LocateRatioLabel: TppLabel;
    LocatorName: TppDBText;
    LocateCount: TppDBText;
    DamageCount: TppDBText;
    DamageRate: TppDBText;
    FacilityTitleLabel: TppLabel;
    ClientLabel: TppLabel;
    SummaryLocates: TppDBCalc;
    SummaryBand: TppSummaryBand;
    SummaryDamages: TppDBCalc;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    FacilityGroupFooter: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ClientGroupFooter: TppGroupFooterBand;
    FacilityLocates: TppDBCalc;
    FacilityDamages: TppDBCalc;
    ClientLocates: TppDBCalc;
    ClientDamages: TppDBCalc;
    ClientName: TppDBText;
    FacilityType: TppDBText;
    ppLabel4: TppLabel;
    ClientDamagePerLocate: TppVariable;
    FacilityDamagePerLocate: TppVariable;
    SummaryDamagePerLocate: TppVariable;
    ppLine1: TppLine;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Data: TADOStoredProc;
    Client: TADODataSet;
    Master: TADODataSet;
    procedure ClientGroupFooterBeforeGenerate(Sender: TObject);
    procedure FacilityGroupFooterBeforeGenerate(Sender: TObject);
    procedure SummaryBandBeforeGenerate(Sender: TObject);
  private
    FLocateRatio: Integer;
    function GetDamagePerLocateRate(LocateCount, DamageCount: Integer): Double;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdMiscUtils, QMConst, OdIsoDates, StrUtils, odADOutils;

{ TDamagesPerLocatesDM }

procedure TDamagesPerLocatesDM.Configure(QueryFields: TStrings);
var
  DateFrom, DateTo: TDateTime;
  ProfitCenter: string;
  FacilityType: string;
  LiabilityType: Integer;
  ClientID: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  DateFrom := GetDateTime('DateFrom');
  DateTo := GetDateTime('DateTo');
  ProfitCenter := GetString('ProfitCenter');
  LiabilityType := SafeGetInteger('Liability', 0);
  FLocateRatio := SafeGetInteger('LocateRatio', 1000);
  FacilityType := SafeGetString('FacilityType');
  ClientID := SafeGetInteger('ClientID', -1);

  Data.Parameters.ParamByName('@FromDate').value := DateFrom;
  Data.Parameters.ParamByName('@ToDate').value := DateTo;
  Data.Parameters.ParamByName('@ProfitCenter').value := ProfitCenter;
  Data.Parameters.ParamByName('@FacilityType').value := FacilityType;
  Data.Parameters.ParamByName('@ClientID').value := ClientID;
  Data.Parameters.ParamByName('@LiabilityType').value := LiabilityType;
  Data.Parameters.ParamByName('@LocateRatioBase').value := FLocateRatio;
  Data.Open;

  Master.RecordSet := Data.RecordSet;
  Client.RecordSet := Data.RecordSet.NextRecordset(RecsAffected);

  DetailBand.Visible := SafeGetInteger('ShowLocatorDetail', 0) = 1;
  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  ShowText(ProfitCenterLabel, ProfitCenter);
  ShowText(LiabilityLabel, GetLiabilityString(LiabilityType));
  LocateRatioLabel.Caption := IntToStr(FLocateRatio) + ' Locates';
  ShowText(FacilityTitleLabel, IfThen(IsEmpty(FacilityType), 'All', FacilityType));

  if ClientID < 0 then
    ShowText(ClientLabel, 'All')
  else begin
    Assert(Client.IsEmpty=False, 'missing expected client data');
    ShowText(ClientLabel, Client.FieldByName('client_name').AsString);
  end;
end;

function TDamagesPerLocatesDM.GetDamagePerLocateRate(LocateCount, DamageCount: Integer): Double;
begin
  if (LocateCount = 0) or (FLocateRatio = 0) then
    Result := 0
  else
    Result := DamageCount / (LocateCount / FLocateRatio);
end;

procedure TDamagesPerLocatesDM.ClientGroupFooterBeforeGenerate(Sender: TObject);
begin
  ClientDamagePerLocate.Value := GetDamagePerLocateRate(ClientLocates.Value,
    ClientDamages.Value);
end;

procedure TDamagesPerLocatesDM.FacilityGroupFooterBeforeGenerate(Sender: TObject);
begin
  FacilityDamagePerLocate.Value := GetDamagePerLocateRate(FacilityLocates.Value,
    FacilityDamages.Value);
end;

procedure TDamagesPerLocatesDM.SummaryBandBeforeGenerate(Sender: TObject);
begin
  SummaryDamagePerLocate.Value := GetDamagePerLocateRate(SummaryLocates.Value,
    SummaryDamages.Value);
end;

initialization
  RegisterReport('DamagesPerLocates', TDamagesPerLocatesDM);

end.
