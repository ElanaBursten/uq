unit DamageDetails;

interface

uses
  SysUtils, Windows, BaseReport, DB, ppDB, ppDBPipe, ppCtrls, ppBands,
  ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  Classes, ppStrtch, ppMemo, Graphics, ppModule, daDataModule, ppSubRpt,
  ppRegion, myChkBox, ppTypes, ServerAttachmentDMu, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageDetailsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppTitleBand1: TppTitleBand;
    ppLabel1: TppLabel;
    ppLabel89: TppLabel;
    ppHeaderBand: TppHeaderBand;
    ppDetailBand: TppDetailBand;
    ppShape7: TppShape;
    ppShape3: TppShape;
    ppShape1: TppShape;
    ppLine1: TppLine;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppLabel8: TppLabel;
    ppDBText8: TppDBText;
    ppLabel9: TppLabel;
    ppDBText9: TppDBText;
    ppLabel10: TppLabel;
    ppDBText12: TppDBText;
    ppLabel13: TppLabel;
    ppDBText13: TppDBText;
    ppLabel14: TppLabel;
    ppDBText14: TppDBText;
    ppLabel15: TppLabel;
    ppDBText15: TppDBText;
    ppLabel16: TppLabel;
    ppDBText16: TppDBText;
    ppLabel17: TppLabel;
    ppDBText17: TppDBText;
    ppLabel18: TppLabel;
    ppDBText18: TppDBText;
    ppLabel19: TppLabel;
    ppDBText19: TppDBText;
    ppLabel20: TppLabel;
    ppDBText20: TppDBText;
    ppLabel21: TppLabel;
    ppDBText21: TppDBText;
    ppLabel22: TppLabel;
    ppDBText22: TppDBText;
    ppLabel23: TppLabel;
    ppDBText23: TppDBText;
    ppLabel24: TppLabel;
    ppDBText24: TppDBText;
    ppLabel25: TppLabel;
    ppDBText29: TppDBText;
    ppLabel32: TppLabel;
    ppDBText30: TppDBText;
    ppLabel33: TppLabel;
    ppDBText31: TppDBText;
    ppLabel34: TppLabel;
    ppDBText32: TppDBText;
    ppLabel35: TppLabel;
    ppDBText33: TppDBText;
    ppLabel36: TppLabel;
    ppDBText34: TppDBText;
    ppLabel37: TppLabel;
    ppDBText48: TppDBText;
    ppLabel51: TppLabel;
    ppDBText49: TppDBText;
    ppLabel52: TppLabel;
    ppDBText52: TppDBText;
    ppLabel55: TppLabel;
    ppDBText53: TppDBText;
    ppLabel56: TppLabel;
    ppDBText54: TppDBText;
    ppLabel57: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppFooterBand1: TppFooterBand;
    ppLine2: TppLine;
    ppSystemVariable1: TppSystemVariable;
    ppSystemVariable2: TppSystemVariable;
    ppDBText43: TppDBText;
    ppDBText74: TppDBText;
    ppLabel83: TppLabel;
    ppDBText46: TppDBText;
    ppDBText78: TppDBText;
    ppLabel92: TppLabel;
    ppDBText79: TppDBText;
    ppLabel93: TppLabel;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppDBText2: TppDBText;
    ppLabel27: TppLabel;
    NarrativeSection: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBMemo2: TppDBMemo;
    ppLabel87: TppLabel;
    ConclusionsSection: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppShape8: TppShape;
    ppDBText3: TppDBText;
    ppLabel28: TppLabel;
    ppDBText4: TppDBText;
    ppLabel29: TppLabel;
    ppDBText77: TppDBText;
    ppLabel46: TppLabel;
    ppDBMemo9: TppDBMemo;
    ppLabel84: TppLabel;
    ppDBMemo11: TppDBMemo;
    ppLabel94: TppLabel;
    ppDBText81: TppDBText;
    ppLabel95: TppLabel;
    ppDBText82: TppDBText;
    ppLabel96: TppLabel;
    ppDBText83: TppDBText;
    ppLabel97: TppLabel;
    ppDBText84: TppDBText;
    ppLabel98: TppLabel;
    ppDBMemo12: TppDBMemo;
    ppLabel99: TppLabel;
    ppLabel100: TppLabel;
    ppLine9: TppLine;
    ppLine10: TppLine;
    ppDBText85: TppDBText;
    ppDBText86: TppDBText;
    ppDBText87: TppDBText;
    ppDBText88: TppDBText;
    ppDBText89: TppDBText;
    ppDBText90: TppDBText;
    SketchSection: TppSubReport;
    ppChildReport3: TppChildReport;
    DiscussionSection: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppImage1: TppImage;
    ppShape2: TppShape;
    ppLabel30: TppLabel;
    ppDetailBand5: TppDetailBand;
    ppShape4: TppShape;
    ppDBText58: TppDBText;
    ppLabel61: TppLabel;
    ppDBText59: TppDBText;
    ppLabel62: TppLabel;
    ppDBText60: TppDBText;
    ppLabel63: TppLabel;
    ppDBText61: TppDBText;
    ppLabel64: TppLabel;
    ppDBMemo3: TppDBMemo;
    ppLabel65: TppLabel;
    ppDBText62: TppDBText;
    ppLabel66: TppLabel;
    ppDBText63: TppDBText;
    ppLabel67: TppLabel;
    ppDBText64: TppDBText;
    ppLabel68: TppLabel;
    ppDBMemo4: TppDBMemo;
    ppLabel69: TppLabel;
    ppDBText65: TppDBText;
    ppLabel70: TppLabel;
    ppDBText66: TppDBText;
    ppLabel71: TppLabel;
    ppDBMemo5: TppDBMemo;
    ppLabel72: TppLabel;
    ppDBText67: TppDBText;
    ppLabel73: TppLabel;
    ppDBText68: TppDBText;
    ppLabel74: TppLabel;
    ppDBMemo6: TppDBMemo;
    ppLabel75: TppLabel;
    ppLabel91: TppLabel;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppLine8: TppLine;
    SiteInfoRegion: TppRegion;
    ppShape5: TppShape;
    ppDBText35: TppDBText;
    ppLabel38: TppLabel;
    ppDBText36: TppDBText;
    ppLabel39: TppLabel;
    ppDBText37: TppDBText;
    ppLabel40: TppLabel;
    ppDBText38: TppDBText;
    ppLabel41: TppLabel;
    ppDBText39: TppDBText;
    ppLabel42: TppLabel;
    ppDBText40: TppDBText;
    ppLabel43: TppLabel;
    ppDBText41: TppDBText;
    ppLabel44: TppLabel;
    ppDBText42: TppDBText;
    ppLabel45: TppLabel;
    ppDBText44: TppDBText;
    ppLabel47: TppLabel;
    ppDBText45: TppDBText;
    ppLabel48: TppLabel;
    ppDBText47: TppDBText;
    ppLabel50: TppLabel;
    ppDBText50: TppDBText;
    ppLabel53: TppLabel;
    ppDBText51: TppDBText;
    ppLabel54: TppLabel;
    ppDBText55: TppDBText;
    ppLabel58: TppLabel;
    ppDBText56: TppDBText;
    ppLabel59: TppLabel;
    ppDBText57: TppDBText;
    ppLabel60: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel90: TppLabel;
    ExcavatorRegion: TppRegion;
    ppShape6: TppShape;
    ppLine12: TppLine;
    DiscAddPerson2Region: TppRegion;
    DiscAddPerson1Region: TppRegion;
    DiscExcRegion: TppRegion;
    DiscRepairRegion: TppRegion;
    DiscFinalRegion: TppRegion;
    ppLabel31: TppLabel;
    ppDBText10: TppDBText;
    ppLabel49: TppLabel;
    ppDBText11: TppDBText;
    ppLabel76: TppLabel;
    ppDBText25: TppDBText;
    AddInfoRegion: TppRegion;
    ppLabel26: TppLabel;
    RemarksMemo: TppDBMemo;
    MasterDS: TDataSource;
    MasterPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    TicketNotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    ppSummaryBand: TppSummaryBand;
    flduq_resp_details: TppDBMemo;
    ppLabel103: TppLabel;
    DamageAttachmentsDS: TDataSource;
    DamageAttachmentsPipe: TppDBPipeline;
    TicketAttachmentsDS: TDataSource;
    TicketAttachmentsPipe: TppDBPipeline;
    TicketSection: TppSubReport;
    ppChildReport5: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppShape9: TppShape;
    ppDBText26: TppDBText;
    ppLabel77: TppLabel;
    ppDBText27: TppDBText;
    ppLabel78: TppLabel;
    ppDBText28: TppDBText;
    ppDetailBand6: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppShape10: TppShape;
    ppReportDateTime: TppCalc;
    ppLabel79: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppSummaryBand2: TppSummaryBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    DetailHeaderRegion: TppRegion;
    ImageMemo: TppMemo;
    NoTicketWarningLabel: TppLabel;
    ppGroupFooterBand3: TppGroupFooterBand;
    ThirdPartySection: TppSubReport;
    ppThirdPartyChildReport: TppChildReport;
    LitigationSection: TppSubReport;
    ppLitigationChildReport: TppChildReport;
    ThirdPartyDS: TDataSource;
    LitigationDS: TDataSource;
    ThirdPartyPipe: TppDBPipeline;
    LitigationPipe: TppDBPipeline;
    ThirdPartyDetailBand: TppDetailBand;
    ThirdPartyDetailRegion: TppRegion;
    ThirdPartyClaimDescrMemo: TppDBMemo;
    ppDBText92: TppDBText;
    ppDBText93: TppDBText;
    ppDBText94: TppDBText;
    ppDBText95: TppDBText;
    ppDBText96: TppDBText;
    ppDBText97: TppDBText;
    ppLabel105: TppLabel;
    ppLabel108: TppLabel;
    ppDBText108: TppDBText;
    ppLabel118: TppLabel;
    ppDBText109: TppDBText;
    ppLabel119: TppLabel;
    ppLabel120: TppLabel;
    ppLabel121: TppLabel;
    ppLabel122: TppLabel;
    ppDBText110: TppDBText;
    ppLabel123: TppLabel;
    ppDBText111: TppDBText;
    NoThirdPartyWarningLabel: TppLabel;
    ppFooterBand3: TppFooterBand;
    ppShape13: TppShape;
    ppSystemVariable4: TppSystemVariable;
    ppCalc1: TppCalc;
    ppLabel124: TppLabel;
    ThirdPartyDamgeIDGroup: TppGroup;
    ThirdPartyDamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppLabel125: TppLabel;
    ppDBText112: TppDBText;
    ppShape11: TppShape;
    ppLabel126: TppLabel;
    ppGroupFooterBand4: TppGroupFooterBand;
    ThirdPartyIDGroup: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine14: TppLine;
    LitigationDetailBand: TppDetailBand;
    NoLitigationWarningLabel: TppLabel;
    LitigationDetailRegion: TppRegion;
    ppDBText98: TppDBText;
    ppDBText99: TppDBText;
    ppDBText100: TppDBText;
    ppDBText101: TppDBText;
    ppDBText102: TppDBText;
    ppDBText103: TppDBText;
    ppDBText104: TppDBText;
    ppDBText105: TppDBText;
    ppLabel106: TppLabel;
    ppLabel107: TppLabel;
    ppLabel109: TppLabel;
    ppDBText106: TppDBText;
    ppLabel110: TppLabel;
    ppLabel111: TppLabel;
    ppLabel112: TppLabel;
    ppLabel113: TppLabel;
    ppLabel114: TppLabel;
    ppLabel115: TppLabel;
    ppDBText107: TppDBText;
    ppLabel116: TppLabel;
    LitigationFooterBand: TppFooterBand;
    ppShape14: TppShape;
    ppSystemVariable5: TppSystemVariable;
    ppCalc2: TppCalc;
    ppLabel117: TppLabel;
    LitigationDamageIDGroup: TppGroup;
    LitigationDamageIDGroupHeaderBand: TppGroupHeaderBand;
    ppShape12: TppShape;
    ppDBText113: TppDBText;
    ppLabel127: TppLabel;
    ppLabel128: TppLabel;
    ppGroupFooterBand1: TppGroupFooterBand;
    LitigationIDGroup: TppGroup;
    ppGroupHeaderBand5: TppGroupHeaderBand;
    ppGroupFooterBand5: TppGroupFooterBand;
    ppLine11: TppLine;
    DamageNotesDS: TDataSource;
    HistoryDS: TDataSource;
    DamageNotesPipe: TppDBPipeline;
    HistoryPipe: TppDBPipeline;
    DamageNotesSection: TppSubReport;
    ppChildReport8: TppChildReport;
    HistorySection: TppSubReport;
    ppChildReport10: TppChildReport;
    ppDetailBand9: TppDetailBand;
    ppLabel129: TppLabel;
    ppDBText114: TppDBText;
    ppDBMemo7: TppDBMemo;
    DamageNotesDamageIDGroup: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppLabel130: TppLabel;
    ppDBText115: TppDBText;
    ppShape19: TppShape;
    ppLabel131: TppLabel;
    ppGroupFooterBand8: TppGroupFooterBand;
    ppDetailBand11: TppDetailBand;
    ModifiedDate: TppDBText;
    ProfitCenter: TppDBText;
    ExcRespCode: TppDBText;
    ExcRespType: TppDBText;
    UQRespType: TppDBText;
    UQRespCode: TppDBText;
    SPCRespType: TppDBText;
    SPCRespCode: TppDBText;
    HistoryDamageIDGroup: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel132: TppLabel;
    ppDBText124: TppDBText;
    ppShape18: TppShape;
    ppLabel133: TppLabel;
    HistoryHeaderRegion: TppRegion;
    ppLabel134: TppLabel;
    ppLabel135: TppLabel;
    ppLabel136: TppLabel;
    ppLabel137: TppLabel;
    ppLabel138: TppLabel;
    ppLabel139: TppLabel;
    ppLabel140: TppLabel;
    ppLabel141: TppLabel;
    ppGroupFooterBand7: TppGroupFooterBand;
    ppFooterBand4: TppFooterBand;
    ppShape15: TppShape;
    ppLabel142: TppLabel;
    ppCalc3: TppCalc;
    ppSystemVariable6: TppSystemVariable;
    ppFooterBand5: TppFooterBand;
    ppShape16: TppShape;
    ppLabel143: TppLabel;
    ppCalc4: TppCalc;
    ppSystemVariable7: TppSystemVariable;
    NoNotesWarningLabel: TppLabel;
    ppLabel81: TppLabel;
    ppLabel82: TppLabel;
    ppLabel85: TppLabel;
    ppLabel86: TppLabel;
    ppLabel101: TppLabel;
    ppLabel102: TppLabel;
    ppLabel104: TppLabel;
    ppLabel144: TppLabel;
    ppDBText69: TppDBText;
    ppDBText70: TppDBText;
    ppDBText71: TppDBText;
    ppDBText72: TppDBText;
    myDBCheckBox1: TmyDBCheckBox;
    myDBCheckBox2: TmyDBCheckBox;
    ppDBText125: TppDBText;
    ppDBMemo1: TppDBMemo;
    TicketNotesSubReport: TppSubReport;
    ppChildReport6: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand7: TppDetailBand;
    NoteMemo: TppDBMemo;
    ppDBText75: TppDBText;
    ppLabel80: TppLabel;
    ppSummaryNotesTicket: TppSummaryBand;
    DamageImagesSubReport: TppSubReport;
    ppChildReportDamageAttachments: TppChildReport;
    ppHeaderBand4: TppHeaderBand;
    ppDetailBand8: TppDetailBand;
    ppDamageImage: TppImage;
    ppDBText126: TppDBText;
    ppMemoErrorMessageDamage: TppMemo;
    ppFooterBand7: TppFooterBand;
    ppShape17: TppShape;
    ppLabel145: TppLabel;
    ppCalc5: TppCalc;
    ppSystemVariable8: TppSystemVariable;
    TicketFacilitiesSubReport: TppSubReport;
    ppTicketFacilitiesChildReport: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppDetailBand10: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    TicketFacilitiesDS: TDataSource;
    TicketFacilitiesPipe: TppDBPipeline;
    ppShape20: TppShape;
    FacilitySectionTitle: TppLabel;
    FacilityClientLabel: TppLabel;
    FacilityTypeLabel: TppLabel;
    FacilityMaterialLabel: TppLabel;
    FacilitySizeLabel: TppLabel;
    FacilityPressureLabel: TppLabel;
    FacilityOffsetLabel: TppLabel;
    FacilityAddedByLabel: TppLabel;
    FacClient: TppDBText;
    FacType: TppDBText;
    FacMaterial: TppDBText;
    FacSize: TppDBText;
    FacOffsets: TppDBText;
    FacAddedBy: TppDBText;
    FacilityPressure: TppDBText;
    TicketImagesSubreport: TppSubReport;
    ppChildReportTicketAttachments: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppDetailBandTicketAttach: TppDetailBand;
    ppDBText91: TppDBText;
    ppTicketImage: TppImage;
    ppMemoErrorMessageTicket: TppMemo;
    ppSummaryBand5: TppSummaryBand;
    ppLabel88: TppLabel;
    ppDBText73: TppDBText;
    labelReasonChanged: TppLabel;
    dbReasonChanged: TppDBText;
    RegionOtherDesc: TppRegion;
    ppLabel146: TppLabel;
    RegionResponse: TppRegion;
    RegionDetails: TppRegion;
    ExcRespOtherDesc: TppDBMemo;
    UQRespOtherDesc: TppDBMemo;
    ppLabel148: TppLabel;
    ExcRespResponse: TppDBMemo;
    ppLabel147: TppLabel;
    ExcRespDetails: TppDBMemo;
    UQRespDetails: TppDBMemo;
    SPCRespDetails: TppDBMemo;
    ppLabel149: TppLabel;
    ppLabel150: TppLabel;
    ppLabel151: TppLabel;
    ppLine13: TppLine;
    UQRespEss: TppDBMemo;
    ApprovalSection: TppSubReport;
    ppChildReport7: TppChildReport;
    ppDetailBand12: TppDetailBand;
    ppShape22: TppShape;
    ppLabel157: TppLabel;
    ppDBText117: TppDBText;
    ppLabel154: TppLabel;
    ppLabel155: TppLabel;
    ppLabel156: TppLabel;
    ppDBText118: TppDBText;
    ppDBText119: TppDBText;
    DamageAttachmentsRegion: TppRegion;
    TicketAttachmentRegion: TppRegion;
    ppDBText76: TppDBText;
    ppDBText116: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    ppDesignLayers5: TppDesignLayers;
    ppDesignLayer5: TppDesignLayer;
    ppDesignLayers6: TppDesignLayers;
    ppDesignLayer6: TppDesignLayer;
    WarningSection: TppSubReport;
    ppChildReport9: TppChildReport;
    ppDesignLayers7: TppDesignLayers;
    ppDesignLayer7: TppDesignLayer;
    ppTitleBand5: TppTitleBand;
    ppDetailBand13: TppDetailBand;
    ppSummaryBand4: TppSummaryBand;
    WarningMemo: TppMemo;
    DuplicateRecordsDS: TDataSource;
    DuplicateRecordsPipe: TppDBPipeline;
    ppLabel152: TppLabel;
    ppLabel153: TppLabel;
    ppLabel158: TppLabel;
    ppLabel159: TppLabel;
    ppDBText122: TppDBText;
    ppDBText123: TppDBText;
    ppDBText80: TppDBText;
    ppDBText120: TppDBText;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Ticket: TADODataSet;
    Locates: TADODataSet;
    DamageAttachments: TADODataSet;
    TicketNotes: TADODataSet;
    TicketAttachments: TADODataSet;
    ThirdParty: TADODataSet;
    DamageNotes: TADODataSet;
    Litigation: TADODataSet;
    TicketFacilities: TADODataSet;
    History: TADODataSet;
    DuplicateRecords: TADODataSet;
    Damage: TADOQuery;
    procedure TicketAfterScroll(DataSet: TDataSet);
    procedure ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
    procedure ImageMemoPrint(Sender: TObject);
    function GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ppDamageImagePrint(Sender: TObject);
    procedure ppTicketImagePrint(Sender: TObject);
    procedure ThirdPartyDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
    procedure LitigationDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
    procedure ppGroupHeaderBand4BeforeGenerate(Sender: TObject);
  protected
    procedure MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string); override;
  private
    TicketImageList: String;
    DamageImageList: String;
    TicketFileList: TStringList;
    DamageFileList: TStringList;
    TicketErrorLog: TStringList;
    DamageErrorLog: TStringList;
    Attacher: TServerAttachment;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdRbMemoFit, OdExceptions, ODMiscUtils,
  OdRbDownloadedImage, QMConst, OdPdf, TicketImage, CVUtils, odAdoUtils;

procedure TDamageDetailsDM.Configure(QueryFields: TStrings);
var
  ShowTicket: Boolean;
  DamageID: Integer;
  ShowLitigation: Boolean;
  ShowThirdParty: Boolean;
  ShowDamageNotes: Boolean;
  ShowHistory: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;
  TicketImageList := SafeGetString('TicketImageList', '-1');
  if not IsEmpty(TicketImageList) then
    if not ValidateIntegerCommaSeparatedList(TicketImageList) then
      raise Exception.Create('The parameter TicketImageList=' + TicketImageList + ' is not valid.');

  DamageImageList := SafeGetString('DamageImageList', '-1');
  if not IsEmpty(DamageImageList) then
    if not ValidateIntegerCommaSeparatedList(DamageImageList) then
      raise Exception.Create('The parameter DamageImageList=' + DamageImageList + ' is not valid.');

  ShowTicket := SafeGetInteger('ShowTicket', 1) = 1;
  ShowLitigation := SafeGetInteger('ShowLitigation', 0) = 1;
  ShowThirdParty := SafeGetInteger('ShowThirdParty', 0) = 1;
  ShowDamageNotes := SafeGetInteger('ShowNotes', 0) = 1;
  ShowHistory := SafeGetInteger('ShowHistory', 0) = 1;

  // Accept a damage_id preferably, or if not, we'll take a UQDamageID
  // Because the end user is allowed to enter the number, so it would be
  // an extra round-trip to have the client app convert.

  DamageID := SafeGetInteger('DamageID', 0);
  if DamageID = 0 then
    DamageID := GetDamageIDForUQDamageID(GetInteger('UQDamageID'));
  SP.Parameters.ParamByName('@damage_id').value := DamageID;
  SP.Parameters.ParamByName('@DamageAttachmentList').value := DamageImageList;
  SP.Parameters.ParamByName('@TicketAttachmentList').value := TicketImageList;
  SP.Open;

  if (SP.RecordCount <= 1) then begin
    ppDetailBand.Visible := True;
    ppSummaryBand.Visible := True;
    WarningSection.Visible := False;
  end
  else begin
    ppDetailBand.Visible := False;
    ppSummaryBand.Visible := False;
    WarningSection.Visible := True;
    WarningMemo.Lines.Clear;
    WarningMemo.Lines.Text := 'Warning: More damage information returned than expected. ' +
    'The system administrator should check for duplicate excavator, excavation, ' +
    'or facility reference codes.';
  end;



  Master.Recordset             := SP.Recordset;
  Ticket.Recordset             := SP.Recordset.NextRecordset(RecsAffected);
  Locates.Recordset            := SP.Recordset.NextRecordset(RecsAffected);
  TicketNotes.Recordset        := SP.Recordset.NextRecordset(RecsAffected);
  DamageAttachments.Recordset  := SP.Recordset.NextRecordset(RecsAffected);
  TicketAttachments.Recordset  := SP.Recordset.NextRecordset(RecsAffected);
  ThirdParty.Recordset         := SP.Recordset.NextRecordset(RecsAffected);
  Litigation.Recordset         := SP.Recordset.NextRecordset(RecsAffected);
  DamageNotes.Recordset        := SP.Recordset.NextRecordset(RecsAffected);
  History.Recordset            := SP.Recordset.NextRecordset(RecsAffected);
  TicketFacilities.Recordset   := SP.Recordset.NextRecordset(RecsAffected);
  DuplicateRecords.Recordset   := SP.Recordset.NextRecordset(RecsAffected);

  TicketSection.Visible := ShowTicket;

  Attacher := TServerAttachment.Create(SP.Connection as TADOConnection);
  TicketImagesSubreport.Visible := Attacher.AttachmentsIncludeFileType(TicketAttachments, Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));
  DamageImagesSubReport.Visible := Attacher.AttachmentsIncludeFileType(DamageAttachments, Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));

  ThirdPartySection.Visible := ShowThirdParty;
  LitigationSection.Visible := ShowLitigation;
  DamageNotesSection.Visible := ShowDamageNotes;
  HistorySection.Visible := ShowHistory;

  if DamageAttachments.RecordCount > 0 then begin
    Attacher.DownloadAttachments(Master.FieldByName('damage_id').AsInteger, qmftDamage,
      DamageFileList, DamageErrorLog, DamageImageList, OutputFolder, True);
    FilterForNoPDFs(DamageAttachments);
  end;
  if TicketAttachments.RecordCount > 0 then begin
    Attacher.DownloadAttachments(Ticket.FieldByName('ticket_id').AsInteger, qmftTicket,
      TicketFileList, TicketErrorLog, TicketImageList, OutputFolder, True);
    FilterForNoPDFs(TicketAttachments);
  end;

  DamageAttachmentsDS.DataSet := ReplaceCVs(Master.FieldByName('damage_id').AsInteger,
    qmftDamage, DamageAttachments, DamageFileList, GetReportRegEx(SP.Connection), IniName);

  TicketAttachmentsDS.DataSet := ReplaceCVs(Ticket.FieldByName('ticket_id').AsInteger,
    qmftTicket, TicketAttachments, TicketFileList, GetReportRegEx(SP.Connection), IniName);
end;

procedure TDamageDetailsDM.TicketAfterScroll(DataSet: TDataSet);
begin
  inherited;
  Locates.Filter := 'ticket_id=' + Ticket.FieldByName('ticket_id').AsString;
  Locates.Filtered := True;
  TicketNotes.Filter := 'ticket_id=' + Ticket.FieldByName('ticket_id').AsString;
  TicketNotes.Filtered := True;
  TicketAttachments.Filter := 'foreign_id=' + Ticket.FieldByName('ticket_id').AsString;
  TicketAttachments.Filtered := True;
  TicketErrorLog.Clear;
end;

procedure TDamageDetailsDM.ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
var
  IsXml: Boolean;
begin
  inherited;
  NoTicketWarningLabel.Visible := Ticket.RecordCount = 0;
  ImageMemo.Lines.Text := RemoveExcessWhitespace(GetDisplayableTicketImage(
    Ticket.FieldByName('image').AsString,
    Ticket.FieldByName('image_format').AsString, IsXml));
end;

procedure TDamageDetailsDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

function TDamageDetailsDM.GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
begin
  Result := -1;
  Damage.Parameters.ParamByName('uq_damage_id').value := UQDamageID;
  Damage.Open;
  try
    if not Damage.IsEmpty then
      Result := Damage.Fields[0].Value;
  finally
    Damage.Close;
  end;
end;

procedure TDamageDetailsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TicketFileList := TStringList.Create;
  DamageFileList := TStringList.Create;
  TicketErrorLog := TStringList.Create;
  DamageErrorLog := TStringList.Create;
  ppChildReportTicketAttachments.Units := utScreenPixels;
  ppChildReportDamageAttachments.Units := utScreenPixels;
end;

procedure TDamageDetailsDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(TicketFileList);
  FreeAndNil(DamageFileList);
  FreeAndNil(TicketErrorLog);
  FreeAndNil(DamageErrorLog);
  FreeAndNil(Attacher);
end;

procedure TDamageDetailsDM.ppDamageImagePrint(Sender: TObject);
begin
  ShowDownloadedImage(DamageFileList, DamageErrorLog, DamageAttachmentsDS.DataSet, ppDamageImage, ppMemoErrorMessageDamage);
end;

procedure TDamageDetailsDM.ppTicketImagePrint(Sender: TObject);
begin
  ShowDownloadedImage(TicketFileList, TicketErrorLog, TicketAttachmentsDS.DataSet, ppTicketImage, ppMemoErrorMessageTicket);
end;

procedure TDamageDetailsDM.ThirdPartyDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
begin
  inherited;
  NoThirdPartyWarningLabel.Visible := ThirdParty.RecordCount = 0;
  ThirdPartyDetailRegion.Visible   := ThirdParty.RecordCount > 0;
end;

procedure TDamageDetailsDM.LitigationDamageIDGroupHeaderBandBeforeGenerate(Sender: TObject);
begin
  inherited;
  NoLitigationWarningLabel.Visible := Litigation.RecordCount = 0;
  LitigationDetailRegion.Visible   := Litigation.RecordCount > 0;
end;

procedure TDamageDetailsDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
var
  FileList: TStrings;
begin
  FileList := TStringList.Create;
  try
    FileList.AddStrings(DamageFileList);
    FileList.AddStrings(TicketFileList);
    MergedPDFFileName := MergePDFs(FileList, OutputDir, ReportFileName, IniName);
  finally
    FreeAndNil(FileList);
  end;
end;

procedure TDamageDetailsDM.ppGroupHeaderBand4BeforeGenerate(Sender: TObject);
begin
  inherited;
  NoNotesWarningLabel.Visible := DamageNotes.RecordCount = 0;
end;

initialization
  RegisterReport('DamageDetails', TDamageDetailsDM);

{
Report=DamageDetails
DamageID=49248

Some damage IDs that have a lot of comment text:

49248
32951
45026

select damage_id, datalength(remarks), datalength(disc_repair_comment), datalength(disc_exc_comment)
 from damage
where  datalength(remarks)>300
 or    datalength(disc_repair_comment)>300
 or    datalength(disc_exc_comment)>300
}

end.

