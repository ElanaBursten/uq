unit ActivityGraph;
//ADO converted
interface

uses
  System.SysUtils, System.Classes, ppClass, ppBands, ppCtrls, ppSubRpt, ppMemo,
  VCL.Graphics, Data.DB, Data.Win.ADODB;

type
  TGraphingDM = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
    Labels: Array of TppLabel;
    Spans: Array of TppShape;
    LabelsInUse, SpansInUse: Integer;
    TimeLinePositions: TStringList;
    MnemonicList: TStringList;
    Legend: string;
    CurrentEmployee, StartDate, EndDate: string;
    procedure CalcPosition(Time: TDateTime; var x, y: Single);
    procedure CreateTimeLine(var DetailBand: TppDetailBand; var TimeLine: TppLine);
    procedure DrawSpan(ppBand: TppBand; StartTime, EndTime: TDateTime; y: Single; BrushStyle: TBrushStyle; BrushColor: TColor; LineOnly: Boolean = False);
    procedure DrawActivityMark(ActivityType: string; DetailBand: TppDetailBand; ActivityTime: TDateTime; y: Single; AColor: TColor; MnemonicList: TStringList);
    procedure InitMnemonicList;
    procedure DrawActivitySpan(ActivityData: TADODataSet; DetailBand: TppDetailBand; ActivityTypeStart, ActivityTypeEnd: string;
        RefControl: TppLabel; AColor: TColor; LineOnly: Boolean = False);
    procedure DrawActivityEvents(ActivityData: TADODataSet; DetailBand: TppDetailBand; ActivityType: string; Y: Single; AColor: TColor);
  public
    procedure DrawSpansAndEvents(GraphBand: TppDetailBand; WorkingHoursLabel, LoginLogoutLabel: TppLabel; ActivityData: TADODataSet;
        EmployeeID, SDate, EDate: string);
    procedure SetupGraphControls(var GraphBand: TppDetailBand; var TimeLine: TppLine);
    procedure ClearDetailBand;
    property GraphLegend: string read Legend;
    function GetLegendValue(Key: string): String;
  end;

const
  StatusedLateTxt = 'L (Maroon)=Statused Late ';
  OutsideWorkSpanTxt = 'Symbol in Red=outside work span';

implementation

uses
  ODMiscUtils, QMConst, StrUtils;

{$R *.dfm}

type
  TRBPoint = class
    x: Single;
    y: Single;
  end;

const
  NumberOfHours = 16; // 16 hours
  StartingHour  = 5;  // 05:00 AM
  MaxLabels = 2000;
  MaxSpans = 200;

procedure TGraphingDM.SetupGraphControls(var GraphBand: TppDetailBand; var TimeLine: TppLine);
var
  I: Integer;
begin
  InitMnemonicList;
  TimeLinePositions := TStringList.Create;
  CreateTimeLine(GraphBand, TimeLine);

  SetLength(Spans, MaxSpans);
  for I := 0 to Length(Spans)-1 do begin
    Spans[I] := tppShape.Create(Self);
    Spans[I].Visible := False;
  end;

  SetLength(Labels, MaxLabels);
  for I := 0 to Length(Labels)-1 do begin
    Labels[I] := tppLabel.Create(Self);
    Labels[I].Visible := False;
  end;
end;

procedure TGraphingDM.CreateTimeLine(var DetailBand: TppDetailBand; var TimeLine: TppLine);

  //Used to draw the time intervals on the axis
  procedure DrawInterval(x, y: Single; Hour: string);
  begin
    with TppLabel.Create(DetailBand) do begin
      Left := x - 0.12;
      Top  := y + 0.05;
      Band := DetailBand;
      SetParentComponent(DetailBand);
      Font.Size := 8;
      if not StrContains('30', Hour) then
        Caption := Hour
      else
        Caption := ''
    end;
    with TppLabel.Create(DetailBand) do begin
      Left := x;
      Band := DetailBand;
      SetParentComponent(DetailBand);
      if StrContains('30', Hour) then begin
        Font.Size := 6;
        Top  := y - 0.12;
      end
      else begin
        Font.Size := 8;
        Top  := y - 0.10;
        Font.Style := [fsBold];
      end;
      Caption := '|';
      Transparent := True;
    end
  end;

var
  Distance: Single;
  i: Integer;
  Text: string;
  RBPoint: TRBPoint;
  Position: Integer;
begin
  Distance := TimeLine.Width / NumberOfHours;
  for i := 0 to NumberOfHours - 1 do begin
    Text := FormatFloat('00', i + StartingHour) + ':00';
    RBPoint := TRBPoint.Create;
    RBPoint.x := Distance * i + TimeLine.Left - 0.01;
    RBPoint.y := TimeLine.Top;
    DrawInterval(RBPoint.x, RBPoint.y, Text);
    TimeLinePositions.AddObject(Text, RBPoint);

    Text := FormatFloat('00', i + StartingHour) + ':30';
    RBPoint := TRBPoint.Create;
    RBPoint.x := Distance * i + TimeLine.Left + (Distance / 2) - 0.01;
    RBPoint.y := TimeLine.Top;
    DrawInterval(RBPoint.x, RBPoint.y, Text);
    TimeLinePositions.AddObject(Text, RBPoint);
    Position := i;
  end;
  TimeLine.Width := TimeLine.Width + (Distance / 2);
  Text := FormatFloat('00', Position + StartingHour) + ':00';
  RBPoint := TRBPoint.Create;
  RBPoint.x := Distance * Position + TimeLine.Left - 0.01;
  RBPoint.y := TimeLine.Top;
  DrawInterval(RBPoint.x, RBPoint.y, Text);
  TimeLinePositions.AddObject(Text, RBPoint);
end;

procedure TGraphingDM.CalcPosition(Time: TDateTime; var x, y: Single);
var
  Hour, Min, Sec, MSec: Word;
  RefTime: string;
  RBPoint1, RBPoint2: TRBPoint;
  Index: Integer;
begin
  DecodeTime(Time, Hour, Min, Sec, MSec);
  if Min >= 30 then
    RefTime := FormatFloat('00', Hour) + ':30'
  else
    RefTime := FormatFloat('00', Hour) + ':00';

  with TimeLinePositions do begin
    Index := IndexOf(RefTime);
    if Index = -1 then
      Index := Count-1;
    RBPoint1 := TRBPoint(Objects[Index]);
    if Index + 1 < Count then
      RBPoint2 := TRBPoint(Objects[Index+1])
    else
      RBPoint2 := TRBPoint(Objects[Index]);
  end;

  if Min >= 30 then
    Min := Min - 30;

  x := RBPoint1.x + (RBPoint2.x - RBPoint1.x)/100 * (Min/30*100);
  y := RBPoint1.y;
end;

procedure TGraphingDM.DrawActivitySpan(ActivityData: TADODataSet; DetailBand: TppDetailBand;
  ActivityTypeStart, ActivityTypeEnd: string; RefControl: TppLabel;
  AColor: TColor; LineOnly: Boolean = False);
const
  ActivityDataFilter = '(emp_id=%s AND activity_date>=%s AND activity_date<%s AND activity_type=%s) OR (emp_id=%s AND activity_date>=%s AND activity_date<%s AND activity_type=%s)';
var
  StartTime: TDateTime;
  EndTime: TDateTime;
begin
  with ActivityData do begin
    Filter := Format(ActivityDataFilter,
        [CurrentEmployee, QuotedStr(StartDate), QuotedStr(EndDate), QuotedStr(ActivityTypeStart),
         CurrentEmployee, QuotedStr(StartDate), QuotedStr(EndDate), QuotedStr(ActivityTypeEnd)]);
    Filtered := True;
    First;
    while not EOF do begin
      if FieldByName('activity_type').AsString = ActivityTypeStart then begin
        StartTime := FieldByName('activity_date').AsDateTime;
        // search for the matching
        Next;
        if FieldByName('activity_type').AsString = ActivityTypeEnd then begin
          EndTime := FieldByName('activity_date').AsDateTime;
          DrawSpan(DetailBand, StartTime, EndTime, RefControl.Top, bsSolid, AColor, LineOnly);
          Next;
        end;
      end else
        Next;
    end;
  end;
end;

procedure TGraphingDM.DrawActivityEvents(ActivityData: TADODataSet; DetailBand: TppDetailBand; ActivityType: string; Y: Single; AColor: TColor);
const
  ActivityDataFilter = 'emp_id=%s and activity_date>=%s and activity_date<%s and activity_type=%s';
begin
  with ActivityData do begin
    Filter := Format(ActivityDataFilter,
        [CurrentEmployee,
         QuotedStr(StartDate),
         QuotedStr(EndDate),
         QuotedStr(ActivityType)]);
    Filtered := True;
    First;
    while not EOF do begin
      if (FieldByName('activity_type').AsString = 'LOCST') and
      AnsiContainsText(ActivityData.FieldByName('description').AsString, 'Status Locate Late') then
        AColor := clMaroon;
      if (FieldByName('out_of_range').AsBoolean) then
        AColor := clRed;
      DrawActivityMark(ActivityType, DetailBand, FieldByName('activity_date').AsDateTime, Y + 0.05, AColor, MnemonicList);
      Next;
    end;
  end;
end;

procedure TGraphingDM.DrawSpansAndEvents(GraphBand: TppDetailBand; WorkingHoursLabel, LoginLogoutLabel: TppLabel;
    ActivityData: TADODataSet; EmployeeID, SDate, EDate: string);
begin
  ClearDetailBand;

  CurrentEmployee := EmployeeID;
  StartDate := SDate;
  EndDate := EDate;

  // Login - Logout
  DrawActivitySpan(ActivityData, GraphBand, ActivityTypeStart, ActivityTypeQuit, WorkingHoursLabel, clBlack, True);

  // Working Hours
  DrawActivitySpan(ActivityData, GraphBand, ActivityTypeWorkStart, ActivityTypeWorkStop, WorkingHoursLabel, clMoneyGreen);
  DrawActivitySpan(ActivityData, GraphBand, ActivityTypeCalloutStart, ActivityTypeCalloutStop, WorkingHoursLabel, clSkyBlue);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeSync, WorkingHoursLabel.Top - 0.025, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeSyncFailed, WorkingHoursLabel.Top - 0.025, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeNewTicket, WorkingHoursLabel.Top + 0.05, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeFindTicket, WorkingHoursLabel.Top + 0.15, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeViewTicket, WorkingHoursLabel.Top + 0.25, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeAddNote, WorkingHoursLabel.Top + 0.05, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeMoveTicket, WorkingHoursLabel.Top + 0.2, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeRouteTicket, WorkingHoursLabel.Top + 0.3, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeAttachmentUpload, WorkingHoursLabel.Top + 0.05, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeAttachmentAdd, WorkingHoursLabel.Top + 0.075, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeLocateStatus, WorkingHoursLabel.Top + 0.4, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeLocateHours, WorkingHoursLabel.Top + 0.4, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeRunReport, WorkingHoursLabel.Top + 0.5, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeTimeSheetEntry, WorkingHoursLabel.Top + 0.5, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeApproveTime, WorkingHoursLabel.Top + 0.6, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeViewMessage, WorkingHoursLabel.Top + 0.6, clBlack);

  //PC comes out of standby or hibernate
  //PC goes to Standby or Hibernate
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypePCSleep, WorkingHoursLabel.Top, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypePCWake, WorkingHoursLabel.Top, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypePCShutdown, WorkingHoursLabel.Top, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinLogOn, WorkingHoursLabel.Top + 0.65, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinLogOff, WorkingHoursLabel.Top + 0.65, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinUserChange, WorkingHoursLabel.Top, clBlack);

  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinLock, WorkingHoursLabel.Top - 0.05, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinUnLock, WorkingHoursLabel.Top - 0.05, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinConnect, WorkingHoursLabel.Top, clBlack);
  DrawActivityEvents(ActivityData, GraphBand, ActivityTypeWinDisconnect, WorkingHoursLabel.Top, clBlack);
end;

function TGraphingDM.GetLegendValue(Key: string): string;
begin
  Result := MnemonicList.Values[Key];
end;

procedure TGraphingDM.DrawSpan(ppBand: TppBand; StartTime, EndTime: TDateTime; y: Single; BrushStyle: TBrushStyle; BrushColor: TColor; LineOnly: Boolean = False);
var
  x1, y1, x2, y2: Single;
begin
  Assert(SpansInUse < Length(Spans), 'Exceeded maximum number of spans');
  CalcPosition(StartTime, x1, y1);
  CalcPosition(EndTime, x2, y2);
  y1 := y;
  if LineOnly then begin
    y1 := y1 - 0.025;
    y2 := y - 0.02;
  end
  else
    y2 := y + 0.70;

  with Spans[SpansInUse] do begin
    Top := y1;
    Left := x1 + 0.015;
    Height := y2 - y1;
    Width := x2 - x1 ;
    Brush.Style := BrushStyle;
    Brush.Color := BrushColor;
    Pen.Style := psClear;
    Visible := True;
    //For efficiency purposes, only assign the band if it is going to be used.
    //This provides significant gains in performance over pre-populating the
    //band for every span.
    Band := ppBand;
    SendToBack;
  end;
  SpansInUse := SpansInUse + 1;
end;

procedure TGraphingDM.DrawActivityMark(ActivityType: string; DetailBand: TppDetailBand;
  ActivityTime: TDateTime; y: Single; AColor: TColor; MnemonicList: TStringList);
var
  x1, y1: Single;
begin
  Assert(LabelsInUse < Length(Labels), 'Exceeded maximum number of labels');
  CalcPosition(ActivityTime, x1, y1);
  with Labels[LabelsInUse] do begin
    Left := x1;
    Top := y - 0.05;
    Font.Size := 6;
    Font.Name := 'Arial';
    Font.Color := AColor;
    Caption := MnemonicList.Values[ActivityType];
    Transparent := True;
    Visible := True;
    //For efficiency purposes, only assign the band if it is going to be used.
    //This provides significant gains in performance over pre-populating the
    //band for every label.
    Band := DetailBand;
    BringToFront;
  end;
  LabelsInUse := LabelsInUse + 1;
end;

procedure TGraphingDM.ClearDetailBand;
var
  I: Integer;
begin
  for I := 0 to LabelsInUse do begin
    Labels[i].Caption := '';
    Labels[i].Visible := False;
  end;

  for I := 0 to SpansInUse do begin
    Spans[i].Reset;
    Spans[i].Visible := False;
  end;

  LabelsInUse := 0;
  SpansInUse := 0;
end;

procedure TGraphingDM.InitMnemonicList;
  procedure AddToListAndLegend(ActivityType, Text, Symbol: string);
  begin
    MnemonicList.Add(ActivityType + '=' + Symbol);
    Legend := Legend + Symbol + '=' + Text + '   ';
  end;
begin
  MnemonicList := TStringList.Create;

  AddToListAndLegend(ActivityTypeViewTicket, 'View Ticket', 'V');
  AddToListAndLegend(ActivityTypeRouteTicket, 'Route Ticket', '>');
  AddToListAndLegend(ActivityTypePCSleep, 'PC Sleep', 'Z');
  AddToListAndLegend(ActivityTypePCWake, 'PC Wake', 'W');
  AddToListAndLegend(ActivityTypePCShutdown, 'PC Shutdown', 'D');
  AddToListAndLegend(ActivityTypeWinLogOn, 'Windows Logon', 'O');
  AddToListAndLegend(ActivityTypeWinLogOff, 'Windows Logoff', 'X');
  AddToListAndLegend(ActivityTypeLocateHours, 'Locate Hours', 'H');
  AddToListAndLegend(ActivityTypeWinUnLock, 'Windows Unlock', 'C');
  AddToListAndLegend(ActivityTypeWinConnect, 'Logged on', 'J');
  AddToListAndLegend(ActivityTypeWinDisconnect, 'Logged off', 'K');
  AddToListAndLegend(ActivityTypeLocateStatus, 'Locate Status', 'L');
  Legend := Legend + ' ' + StatusedLateTxt + '   ';
  AddToListAndLegend(ActivityTypeAddNote, 'Add Note', 'N');
  AddToListAndLegend(ActivityTypeViewMessage, 'View Message', 'M');
  AddToListAndLegend(ActivityTypeNewTicket, 'New Ticket', 'T');
  AddToListAndLegend(ActivityTypeMoveTicket, 'Move Ticket', 'P');
  AddToListAndLegend(ActivityTypeRunReport, 'Run Report', 'R');
  AddToListAndLegend(ActivityTypeSync, 'Sync', 'S');
  AddToListAndLegend(ActivityTypeAttachmentUpload, 'Attachment Upload', 'U');
  AddToListAndLegend(ActivityTypeTimeSheetEntry, 'Timesheet Entry', 'E');

  AddToListAndLegend(ActivityTypeApproveTime, 'Time Approve', 'A');
  AddToListAndLegend(ActivityTypeFindTicket, 'Find Ticket', 'F');
  AddToListAndLegend(ActivityTypeWinLock, 'Windows Lock','#');
  AddToListAndLegend(ActivityTypeSyncFailed, 'Sync Failed', '!');
  AddToListAndLegend(ActivityTypeAttachmentAdd, 'Attachment Add', '+');
  AddToListAndLegend(ActivityTypeWinUserChange, 'Windows User Change', '*');

  MnemonicList.Add(ActivityTypeWorkStart +'=1');
  MnemonicList.Add(ActivityTypeWorkStop +'=2');
  MnemonicList.Add(ActivityTypeStart +'=3');
  MnemonicList.Add(ActivityTypeQuit +'=4');
  MnemonicList.Add(ActivityTypeCalloutStart + '=5');
  MnemonicList.Add(ActivityTypeCalloutStop + '=6');

  Legend := Legend + OutsideWorkSpanTxt;
end;

procedure TGraphingDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(MnemonicList);
end;

end.
