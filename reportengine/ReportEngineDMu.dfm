object ReportEngineDM: TReportEngineDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 349
  Width = 401
  object ReportRights: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'UserID'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'EntityData'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'declare @EmpID  int'#13#10'set @EmpID = (select emp_id from users wher' +
        'e uid = :UserID)'#13#10#13#10'select er.right_id, er.allowed, er.limitatio' +
        'n'#13#10'from dbo.get_effective_rights(@EmpID) er'#13#10'  inner join right_' +
        'definition rd on rd.right_id = er.right_id'#13#10'where rd.entity_data' +
        ' = :EntityData'#13#10#13#10'union'#13#10#13#10'select er.right_id, er.allowed, er.li' +
        'mitation'#13#10'from dbo.get_effective_rights(@EmpID) er'#13#10'  inner join' +
        ' right_definition rd on rd.right_id = er.right_id'#13#10'where rd.enti' +
        'ty_data = '#39'ReportScreen'#39#13#10)
    Left = 144
    Top = 40
  end
  object LogStart: TADOCommand
    CommandText = 
      'if not exists(select request_id from report_log where request_id' +
      '=:request_id_exists)'#13#10'insert into report_log'#13#10'(request_id, repor' +
      't_name, user_id, params, start_date)'#13#10'values'#13#10'(:request_id, :rep' +
      'ort_name, :user_id, :params, getdate())'#13#10#13#10
    CommandTimeout = 300
    Connection = Conn
    Parameters = <
      item
        Name = 'request_id_exists'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'request_id'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'report_name'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'params'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 48
    Top = 144
  end
  object LogFinish: TADOCommand
    CommandText = 
      'update report_log set'#13#10' finish_date = getdate(),'#13#10' engine_succes' +
      's = :engine_success,'#13#10' engine_details = :engine_details'#13#10'where'#13#10 +
      ' request_id = :request_id'#13#10
    CommandTimeout = 300
    Connection = Conn
    Parameters = <
      item
        Name = 'engine_success'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = 'engine_details'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'request_id'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 144
    Top = 144
  end
  object LogStartOldClient: TADOCommand
    CommandText = 
      'if not exists(select request_id from report_log where request_id' +
      '=:request_id_exists)'#13#10'insert into report_log'#13#10'(request_id, repor' +
      't_name, user_id, params, start_date, client_success, client_deta' +
      'ils)'#13#10'values'#13#10'(:request_id, :report_name, :user_id, :params, get' +
      'date(), 1, '#39'(assumed)'#39')'#13#10
    CommandTimeout = 300
    Connection = Conn
    Parameters = <
      item
        Name = 'request_id_exists'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'request_id'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'report_name'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'params'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 48
    Top = 200
  end
  object GetUserData: TADOQuery
    Connection = Conn
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'uid'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select u.*, e.active'#13#10'from users u'#13#10'  left join employee e on e.' +
        'emp_id = u.emp_id'#13#10'where uid = :uid')
    Left = 144
    Top = 200
  end
  object GetConfigurationData: TADOQuery
    Connection = Conn
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end>
    SQL.Strings = (
      'select * from configuration_data where name = :name')
    Left = 256
    Top = 200
  end
  object RightDefinition: TADOTable
    Connection = Conn
    TableName = 'right_definition'
    Left = 248
    Top = 40
  end
  object Conn: TADOConnection
    CommandTimeout = 300
    ConnectionTimeout = 150
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    AfterConnect = ConnAfterConnect
    Left = 40
    Top = 40
  end
  object AWSCredentialsQry: TADOQuery
    Connection = Conn
    CommandTimeout = 300
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from api_storage_credentials'
      'where (api_storage_constants_id = 1)'
      ''
      '')
    Left = 41
    Top = 276
  end
  object AWSConstantsQry: TADOQuery
    Connection = Conn
    CommandTimeout = 300
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from api_storage_constants'
      'where (id = 1)')
    Left = 145
    Top = 276
  end
end
