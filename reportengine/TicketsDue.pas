unit TicketsDue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppStrtch, ppSubRpt, ppMemo, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TTicketsDueDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel4: TppLabel;
    DueDateLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText2: TppDBText;
    ppDBText1: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppShape1: TppShape;
    ppLabel11: TppLabel;
    ppDBText9: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppShape2: TppShape;
    ppDBText10: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppLabel13: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppDBText3: TppDBText;
    ppLabel9: TppLabel;
    ppDBText8: TppDBText;
    ppLabel2: TppLabel;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel10: TppLabel;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine1: TppLine;
    ppDBCalc2: TppDBCalc;
    ppDBText11: TppDBText;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLine2: TppLine;
    ppLabel14: TppLabel;
    ppLine3: TppLine;
    ppShape3: TppShape;
    GrandTotalLocates: TppDBCalc;
    ppLabel16: TppLabel;
    NotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    NotesSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText12: TppDBText;
    ppLabel17: TppLabel;
    ppDBMemo1: TppDBMemo;
    RemarksMemo: TppDBMemo;
    RemarksLabel: TppLabel;
    ppDBText13: TppDBText;
    ppLabel18: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    ppReportFooterShape1: TppShape;
    priority: TppField;
    ppLabel19: TppLabel;
    ppDBText14: TppDBText;
    ppDBCalc3: TppDBCalc;
    ticket_count: TppField;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppLabel15: TppLabel;
    ppShape4: TppShape;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    IncludeNotesLabel: TppLabel;
    IncludeRemarksLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc; //QM-563 set time out to 120  SR
    Notes: TADODataSet;
    Master: TADODataSet;
    procedure NotesSubReportPrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;     //QMANTWO-783
  end;

implementation

uses ReportEngineDMu, OdDbUtils, OdAdoUtils;

{$R *.dfm}

{ TTicketsDueDM }

procedure TTicketsDueDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
  DueDate: TDateTime;
  ShowRemarks: Boolean;
  ShowNotes: Boolean;
  RecsAffected: OleVariant;
begin
  inherited;

  ManagerID := GetInteger('ManagerId');
  EmpStatus := GetInteger('EmployeeStatus');
  DueDate := GetDateTime('DueDate');
  ShowRemarks := SafeGetInteger('PrintRemarks') = 1;
  ShowNotes := SafeGetInteger('PrintTicketNotes') = 1;

  with SP do begin
    Parameters.ParamByName('@ManagerId').Value      := ManagerID;
    Parameters.ParamByName('@DueDate').Value        := DueDate;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;

//QMANTWO-533 EB
  Master.Recordset := SP.Recordset;   //QMANTWO-783

//  Master.Open;
  Notes.Recordset := SP.Recordset.NextRecordset(RecsAffected);
//  Notes.Open;
//  sp.Close;       //QMANTWO-783
//  FreeandNil(sp);    //QMANTWO-783
  ShowDateText(DueDateLabel, 'Open tickets due before:', DueDate, True);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  ShowBooleanText(IncludeRemarksLabel, 'Include Remarks:', ShowRemarks);
  ShowBooleanText(IncludeNotesLabel, 'Include Notes:', ShowNotes);

  RemarksMemo.Visible := ShowRemarks;
  RemarksLabel.Visible := ShowRemarks;
  NotesSubReport.Visible := ShowNotes;
end;

procedure TTicketsDueDM.CreateDelimitedStream(Str: TStream; Delimiter: string);  //QMANTWO-783
//var
//  myStr : TStream;
begin
//  myStr := TStream.Create;
  Delimiter := ',';
  SaveDelimToStream(SP, Str, Delimiter, True);    //QMANTWO-783
//  myStr.Free;
end;

procedure TTicketsDueDM.NotesSubReportPrint(Sender: TObject);
begin
  inherited;
  Notes.Filtered := True;
  Notes.Filter := 'ticket_id=' + IntToStr(Master.FieldByName('ticket_id').AsInteger);
end;

initialization
  RegisterReport('TicketsDue', TTicketsDueDM);

end.
