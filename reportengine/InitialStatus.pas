unit InitialStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppBands, ppClass, ppVar, ppCtrls, ppPrnabl, ppCache,
  ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB, ppModule,
  daDataModule, ppDBJIT, ppMemo, ppStrtch, ppRegion, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TInitialStatusDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    DateRangeLabel: TppLabel;
    ppDBText9: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalc2: TppDBCalc;
    ppLabel14: TppLabel;
    ppLine2: TppLine;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText10: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppLabel2: TppLabel;
    ppDBText11: TppDBText;
    ppLabel17: TppLabel;
    ppDBText12: TppDBText;
    ppLabel18: TppLabel;
    ppDBText13: TppDBText;
    ppLabel19: TppLabel;
    ppDBText14: TppDBText;
    ppLabel20: TppLabel;
    ppLine1: TppLine;
    ppLine3: TppLine;
    ppDBText15: TppDBText;
    ppLabel22: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    ppReportFooterShape1: TppShape;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ArrivalLabel: TppLabel;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppRegion1: TppRegion;
    ppLabel4: TppLabel;
    ppLabel10: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel21: TppLabel;
    ppLabel3: TppLabel;
    StatusColumnLabel: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel12: TppLabel;
    CallCenterLabel: TppLabel;
    CallCentersMemo: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    SummaryData: TADODataSet;
    Master: TADODataSet;
    procedure ReportNoData(Sender, aDialog: TObject;
      var aShowDialog: Boolean; aDrawCommand: TObject;
      var aAddDrawCommand: Boolean);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu,  ppDrwCmd, ppTypes, odADOutils;

{$R *.dfm}

// TInitialStatusDM

procedure TInitialStatusDM.Configure(QueryFields: TStrings);
var
  ManagerId: Integer;
  ArrivalMode: Boolean;
  CallCenters: string;
  RecsAffected : oleVariant;
begin
  inherited;

  ManagerId := SafeGetInteger('ManagerId', 0);
  ArrivalMode := SafeGetBoolean('Arrival', False);
  CallCenters := GetString('CallCenter');

  SP.Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
  SP.Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
  SP.Parameters.ParamByName('@CallCenterCodes').value := CallCenters;
  SP.Parameters.ParamByName('@ManagerId').value := ManagerId;
  SP.Parameters.ParamByName('@Arrival').value := ArrivalMode;
  SP.Open;

  Master.Recordset := SP.Recordset;
  SummaryData.Recordset := SP.Recordset.NextRecordset(RecsAffected);

  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowDateRange(DateRangeLabel);
  CallCentersMemo.Text := StringReplace(CallCenters, ',', ', ', [rfReplaceAll]);

  if ArrivalMode then begin
    ArrivalLabel.Caption := 'Initial Status or Arrival';
    StatusColumnLabel.Caption := 'Initial Stat. / Arr. Date';
  end else begin
    ArrivalLabel.Caption := 'Initial Status Only';
    StatusColumnLabel.Caption := 'Initial Status Date';
  end;
end;

procedure TInitialStatusDM.ReportNoData(Sender, aDialog: TObject;
  var aShowDialog: Boolean; aDrawCommand: TObject;
  var aAddDrawCommand: Boolean);
var
  dt: TppDrawText;
begin
  inherited;
  dt := TppDrawText(aDrawCommand);
  dt.TextAlignment := taCentered;
  dt.Text := 'No Initial Status data for this date range.';
  dt.Left := 0;
  dt.Width := Report.PrinterSetup.PageDef.mmPrintableWidth;
end;

initialization
  RegisterReport('InitialStatus', TInitialStatusDM);

end.

