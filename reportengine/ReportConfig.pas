unit ReportConfig;

interface

uses
  SysUtils, IniFiles, Contnrs, Classes, OdMiscUtils, UQDbConfig
  ,AdoDB, OdAdoUtils;

type
  TStatusUpdateProc = procedure(Status: string) of object;

  TReportConfig = class
    QueryTimeout: Integer;
    LogFileName: string;
    MainDB: TADODatabaseConfig;
    ReportingDBs: TObjectList;
    RetryInterval: Integer;
    OverallTimeout: Integer;
    FaxQueueDir: string;
    FaxAccountID: string;
    FaxUserName: string;
    FaxPassword: string;
    FaxDispositionURL: string;
    RandomChoice: Boolean;
    HomeDomain: string;
    DevDebug: Boolean;
    DevOverrideConnectionStr: string;

    function Description: string;

    procedure ConnectReportingDb(Conn: TADOConnection; StatusProc: TStatusUpdateProc);
    procedure ConnectMainDb(Conn: TADOConnection; StatusProc: TStatusUpdateProc);

    destructor Destroy; override;
    function ReportingDBsExist: Boolean;
  end;

function LoadConfig(IniFileName: string): TReportConfig;

implementation

{ TReportConfig }

const
  DatabaseSection = 'Database';

function LoadConfig(IniFileName: string): TReportConfig;
const
  IniError = 'Could not load Database and ReportingDatabase information from INI file: %s.  Running in directory: %s';
var
  Ini: TIniFile;
  NumRepDBs, I: Integer;
begin
  Ini := TIniFile.Create(IniFileName);
  Result := TReportConfig.Create;
  try
    Result.DevDebug := Ini.ReadBool('Dev', 'Debug', False);    //EB - Had to add this so that we can test locally
    if Result.DevDebug then
      Result.DevOverrideConnectionStr := Ini.ReadString('Database', 'ConnectionString', '');

    Result.QueryTimeout := Ini.ReadInteger(DatabaseSection, 'QueryTimeout', 120);
    Result.RetryInterval := Ini.ReadInteger(DatabaseSection, 'RetryInterval', 10);
    Result.OverallTimeout := Ini.ReadInteger(DatabaseSection, 'OverallTimeout', 90);
    Result.RandomChoice := Ini.ReadString(DatabaseSection, 'RandomReportingDB', '0') = '1';

    Result.LogFileName := Ini.ReadString('LogFile', 'Path', '');
    Result.FaxQueueDir := Ini.ReadString('Fax', 'FaxQueueDir', '');
    Result.FaxAccountID := Ini.ReadString('Fax', 'AccountID', '');
    Result.FaxUserName := Ini.ReadString('Fax', 'UserName', '');
    Result.FaxPassword := Ini.ReadString('Fax', 'Password', '');
    Result.FaxDispositionURL := Ini.ReadString('Fax', 'DispositionURL', '');

    Result.MainDB := TADODatabaseConfig.CreateFromIni(Ini, DatabaseSection, 'MainDB');

    NumRepDBs := Ini.ReadInteger(DatabaseSection, 'NumberOfReportingDBs', 0);
    Result.ReportingDBs := TObjectList.Create;
    Result.HomeDomain := Ini.ReadString('Domain_Settings', 'HomeDomain', 'DYNUTIL');

    if Result.DevDebug then        //EB
        Result.ReportingDBs.Add(TADODatabaseConfig.CreateFromIni(Ini, DatabaseSection, 'MainDB'))
    else
      for I := 1 to NumRepDBs do begin
        Result.ReportingDBs.Add(
           TADODatabaseConfig.CreateFromIni(Ini, 'ReportingDatabase' + IntToStr(I),
                                            'RepDB ' + IntToStr(I)));
      end;


    if IsEmpty(Result.MainDB.Server) and IsEmpty(Result.MainDB.ConnString) then begin
      if not Result.DevDebug then
      raise Exception.CreateFmt(IniError, [IniFileName, GetCurrentDir]);
    end;
  finally
    FreeAndNil(Ini);
  end;
end;


function AddSeconds(Starting: TDateTime; Seconds: Integer): TDateTime;
begin
  Result := Starting + Seconds / 60 / 60 /24;
end;

{ TReportConfig }
destructor TReportConfig.Destroy;
begin
  FreeAndNil(ReportingDBs);
  FreeAndNil(MainDB);
  inherited;
end;

function TReportConfig.ReportingDBsExist: Boolean;
begin
  Result := ReportingDBs.Count > 0;
end;

function TReportConfig.Description: string;
begin
  Result := '  Configuration: ' +
            ' Timeout=' + IntToStr(QueryTimeout);

  if Assigned(MainDB) then
    Result := Result + ' Main DB: ' + MainDB.Description + '  ';

  Result := Result + ' Number of Reporting DBs: ' + IntToStr(ReportingDBs.Count);
end;

procedure TReportConfig.ConnectMainDb(Conn: TADOConnection;
  StatusProc: TStatusUpdateProc);
begin
  if Assigned(StatusProc) then
    StatusProc('Connecting to Main DB in TReportConfig');
  ConnectAdoConnectionWithConfig(Conn, MainDB);
end;

procedure TReportConfig.ConnectReportingDb(Conn: TADOConnection;
  StatusProc: TStatusUpdateProc);
var
  StartTime: TDateTime;
  GiveUpTime: TDateTime;
  DBC: TADODatabaseConfig;
  I, Offset: Integer;
  RepDbIndex: Integer;
  NumAttempts: Integer;
  StatusText: string;
begin
  StartTime := Now;
  GiveUpTime := AddSeconds(StartTime, OverallTimeout);
  NumAttempts := 0;

  while Now < GiveUpTime do begin
    // Try each of our reportings DBs

    if RandomChoice then
      Offset := Random(ReportingDBs.Count)
    else
      Offset := 0;

    if ReportingDBs.Count < 1 then
      StatusProc('No reporting DBs set, please define [Database] NumberOfReportingDBs=1 or more');
    for I := 0 to ReportingDBs.Count - 1 do begin
      RepDbIndex := (I + Offset) mod ReportingDBs.Count;
      DBC := ReportingDBs[RepDbIndex] as TADODatabaseConfig;
      try
        Inc(NumAttempts);
        StatusText := 'Attempt ' + IntToStr(NumAttempts) +
                      ': ' + DBC.Name;
        StatusProc(StatusText);
        ConnectAdoConnectionWithConfig(Conn, DBC);
        StatusProc(DBC.Name);  // short success indicator
        Exit;
      except
        // Failed to connect to this one, go on to the next.
      end;
    end;
    StatusProc('Waiting ' + IntToStr(RetryInterval) + ' seconds for retry');
    Sleep(RetryInterval * 1000); // Wait N seconds, then try them all again.
  end;
  StatusProc('Rep DB connection failed');
  raise Exception.Create('No reporting database was available after ' +
                         IntToStr(NumAttempts) + ' attempts');
end;


end.

