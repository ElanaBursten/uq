unit DamageInvoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppStrtch, ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageInvoiceDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    DetailBand: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ReceivedDate: TppDBText;
    ppLabel3: TppLabel;
    Company: TppDBText;
    ppLabel4: TppLabel;
    InvoiceCode: TppDBText;
    ppLabel5: TppLabel;
    InvoiceNumber: TppDBText;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    DamageDate: TppDBText;
    ppLabel8: TppLabel;
    InvoiceAmount: TppDBText;
    ppLabel2: TppLabel;
    UQDamageId: TppDBText;
    ReceivedFromLabel: TppLabel;
    ReceivedFromDate: TppLabel;
    ReceivedToDate: TppLabel;
    ReceivedToLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    PaidAmountLabel: TppLabel;
    PaidAmount: TppDBText;
    PaidDateLabel: TppLabel;
    PaidDate: TppDBText;
    FirstEstimateLabel: TppLabel;
    FirstEstimate: TppDBText;
    CurrentEstimateLabel: TppLabel;
    CurrentEstimate: TppDBText;
    ppGroup1: TppGroup;
    ProfitCenterGroupHeaderBand: TppGroupHeaderBand;
    ProfitCenterGroupFooterBand: TppGroupFooterBand;
    InvoiceAmountSum: TppDBCalc;
    FirstEstimateSum: TppDBCalc;
    CurrentEstimateSum: TppDBCalc;
    PaidAmountSum: TppDBCalc;
    ppLabel14: TppLabel;
    ppDBText13: TppDBText;
    PaidToDate: TppLabel;
    PaidFromDate: TppLabel;
    PaidFromLabel: TppLabel;
    PaidToLabel: TppLabel;
    ProfitCenterLabel: TppLabel;
    SelectedProfitCenter: TppLabel;
    CompanyLabel: TppLabel;
    SelectedCompany: TppLabel;
    ppGroup2: TppGroup;
    CompanyGroupHeaderBand: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ReasonDeniedLabel: TppLabel;
    ppLabel10: TppLabel;
    SelectedInvoices: TppLabel;
    ReasonDenied: TppDBMemo;
    ppSummaryBand1: TppSummaryBand;
    ppLabel9: TppLabel;
    InvoiceAmountGrandTotal: TppDBCalc;
    FirstEstimateGrandTotal: TppDBCalc;
    CurrentEstimateGrandTotal: TppDBCalc;
    PaidAmountGrandTotal: TppDBCalc;
    ShowPaidAmountLabel: TppLabel;
    ShowEstimatesLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TDamageInvoiceDM }

procedure TDamageInvoiceDM.Configure(QueryFields: TStrings);
var
  MaxDate: TDateTime;
  RecvFromDate, RecvToDate: TDateTime;
  PdFromDate, PdToDate: TDateTime;
  ProfitCenter: string;
  Company: string;
  ShowPaidAmount: Boolean;
  ShowEstimates: Boolean;
  InvoiceStatus: Integer;
begin
  inherited;
  MaxDate := EncodeDate(9999, 12, 31);
  RecvFromDate := GetDateTime('ReceivedFromDate');
  RecvToDate := GetDateTime('ReceivedToDate');
  ProfitCenter := SafeGetString('ProfitCenter');
  Company := SafeGetString('Company');
  ShowPaidAmount := (SafeGetInteger('ShowPaidAmount', 0) <> 0);
  ShowEstimates := (SafeGetInteger('ShowEstimates', 0) <> 0);
  InvoiceStatus := SafeGetInteger('InvoiceStatus', 0);

  // don't apply any paid date criteria if the paid amount is not shown
  if ShowPaidAmount then begin
    PdFromDate := SafeGetDateTime('PaidFromDate');
    PdToDate := SafeGetDateTime('PaidToDate');
  end
  else begin
    PdFromDate := 0;
    PdToDate := 0;
  end;

  // setup criteria labels on report header
  ReceivedFromDate.Caption := FormatDateTime('mm/dd/yyyy', RecvFromDate);
  if RecvToDate = MaxDate then
    ReceivedToDate.Caption := 'Any'
  else
    ReceivedToDate.Caption := FormatDateTime('mm/dd/yyyy', RecvToDate);
  if (PdFromDate = 0) then
    PaidFromDate.Caption := 'Any'
  else
    PaidFromDate.Caption := FormatDateTime('mm/dd/yyyy', PdFromDate);
  if PdToDate = 0 then
    PaidToDate.Caption := 'Any'
  else
    PaidToDate.Caption := FormatDateTime('mm/dd/yyyy', PdToDate);
  if ProfitCenter = '' then
    SelectedProfitCenter.Caption := 'All'
  else
    SelectedProfitCenter.Caption := ProfitCenter;
  if Company = '' then
    SelectedCompany.Caption := 'All'
  else
    SelectedCompany.Caption := Company;
  if InvoiceStatus = 1 then
    SelectedInvoices.Caption := 'Received and Payable'
  else if InvoiceStatus = 2 then
    SelectedInvoices.Caption := 'Received and Denied'
  else
     SelectedInvoices.Caption := 'All';

  // configure columns based on options
  PaidAmount.Visible := ShowPaidAmount;
  FirstEstimateLabel.Visible := ShowEstimates;
  FirstEstimate.Visible := ShowEstimates;
  FirstEstimateSum.Visible := ShowEstimates;
  FirstEstimateGrandTotal.Visible := ShowEstimates;
  CurrentEstimateLabel.Visible := ShowEstimates;
  CurrentEstimate.Visible := ShowEstimates;
  CurrentEstimateSum.Visible := ShowEstimates;
  CurrentEstimateGrandTotal.Visible := ShowEstimates;
  if InvoiceStatus = 1 then
    ReasonDeniedLabel.Visible := False;
  if InvoiceStatus = 2 then begin
    PaidAmount.Visible := False;
    ReasonDenied.Left := PaidAmountLabel.Left;
    ReasonDeniedLabel.Left := ReasonDenied.Left;
    ReasonDenied.Width := Report.PrinterSetup.PaperWidth - Report.PrinterSetup.MarginLeft -
      Report.PrinterSetup.MarginRight - ReasonDenied.Left;
  end;
  PaidAmountLabel.Visible := PaidAmount.Visible;
  PaidAmountSum.Visible := PaidAmount.Visible;
  PaidAmountGrandTotal.Visible := PaidAmount.Visible;
  PaidDateLabel.Visible := PaidAmount.Visible;
  PaidDate.Visible := PaidAmount.Visible;
  PaidFromLabel.Visible := PaidAmount.Visible;
  PaidToLabel.Visible := PaidAmount.Visible;
  PaidFromDate.Visible := PaidAmount.Visible;
  PaidToDate.Visible := PaidAmount.Visible;

  // get the data
  with SP do begin
    Parameters.ParamByName('@ReceivedFromDate').value := RecvFromDate;
    Parameters.ParamByName('@ReceivedToDate').value := RecvToDate;
    Parameters.ParamByName('@ProfitCenter').value := ProfitCenter;
    Parameters.ParamByName('@Company').value := Company;
    if PdFromDate > 0 then
      Parameters.ParamByName('@PaidFromDate').value := PdFromDate;
    if PdToDate > 0 then
      Parameters.ParamByName('@PaidToDate').value := PdToDate;

    Parameters.ParamByName('@InvoiceStatus').value := InvoiceStatus;
    Open;
  end;

  ShowText(ShowEstimatesLabel, BooleanToStringYesNo(ShowEstimates));
  ShowText(ShowPaidAmountLabel, BooleanToStringYesNo(ShowPaidAmount));
end;

initialization
  RegisterReport('DamageInvoice', TDamageInvoiceDM);

end.
