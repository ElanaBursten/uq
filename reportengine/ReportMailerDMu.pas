unit ReportMailerDMu;
//  This does NOT send any mail.  It posts the PDF file.
interface

uses
  SysUtils, Classes, ComObj, ReportConfig, Windows,
  ppPDFDevice, ppPDFSettings;

type
  TReportMailerDM = class(TDataModule)
  private
    Config: TReportConfig;
    procedure StatusUpdate(Msg: string);
  public
    procedure Run;
  end;

implementation

{$R *.dfm}

uses BaseReport, ReportEngineDMu, OdIsoDates, OdMiscUtils,
  ReportEngineLog;

{ TReportMailerDM }

type
  TOutputType = (omPDF, omTSV, omSSV, omCSV);

procedure TReportMailerDM.Run;
var
  DM: TReportEngineDM;
  RepDM: TBaseReportDM;
  RepName: string;
  TimeStart, TimeEnd: Int64;
  QueryStart, QueryEnd: Int64;
  OutStream: TFileStream;
  Params: TStrings;
  I: Integer;
  OutputFileName: string;
  TestMode: Boolean;
  SkipIfExists: Boolean;
  DirName: string;
  ResultData: TStringList;
  IniFileName: string;
  Param: string;
  Output: TOutputType;
  ResultFileName: string;
begin
  ResultData := TStringList.Create;
  IniFileName := ChangeFileExt(ParamStr(0), '.ini');
  ResultFileName := ChangeFileExt(ParamStr(0), '.out');
  QueryStart := 0;
  QueryEnd := 0;

  WriteLn('INI file: ' + IniFileName);

  TimeStart := GetTickCount;
  try
    try
      Config := LoadConfig(IniFileName);

      if NotEmpty(Config.LogFileName) then
        StartLogging(Config.LogFileName);

      DeleteFile(PChar(ResultFileName));

      DM := TReportEngineDM.Create(nil);
      try
        OutputFileName := 'output.pdf';

        Params := TStringList.Create;
        Params.Add('Report=' + ParamStr(1));

        TestMode := False;
        SkipIfExists := False;
        Output := omPDF;
        I := 2;
        while I <= ParamCount do begin
          Param := ParamStr(I);
          if (Param = '-t') or (Param = '/t') then begin
            TestMode := True;
          end else if (Param = '/skip') then begin
            SkipIfExists := True;
          end else if (Param = '-o') or (Param = '/o') then begin
            OutputFileName := ParamStr(I+1);
            Inc(I)
          end else if (Param = '/tsv') then begin
            Output := omTSV;
          end else if (Param = '/ssv') then begin
            Output := omSSV;
          end else if (Param = '/csv') then begin
            Output := omCSV;
          end else
            Params.Add(Param);

          Inc(I);
        end;

        // Write this status information to the stdout;
        // We'll log the essential stuff to a file at the end.
        WriteLn('Settings:');
        WriteLn('Output File=' + OutputFileName);
        WriteLn(Params.Text);

        if SkipIfExists then begin
          if FileExists(OutputFileName) then begin
            WriteLn('Output file exists, not generating report.');
            Exit;
          end;
        end;

        Config.ConnectReportingDb(DM.Conn, StatusUpdate);

        RepName := Params.Values['Report'];
        WriteLn('Loading Report: ' + RepName);
        RepDM := CreateReport(RepName, IniFileName);
        AddLogEntry(RepName, 'Connected: Adding log entry for report');
        if TestMode then begin
          WriteLn('Exiting without generating report.');
          ResultData.Add('Success');
          Exit;
        end;

        try
          RepDM.UseConnection(DM.Conn, Config.QueryTimeout);

          WriteLn('Running Report Query/SP');
          QueryStart := GetTickCount;
          try
            RepDM.Configure(Params);
          finally
            QueryEnd := GetTickCount;
          end;

          DirName := ExtractFileDir(OutputFileName);
          if DirName <> '' then begin
            WriteLn('Creating Directory if needed: ' + DirName);
            OdForceDirectories(DirName);
          end;

          WriteLn('Saving to ' + OutputFileName);
          OutStream := TFileStream.Create(OutputFileName, fmCreate);
          try
            if Output = omTSV then
              RepDM.CreateDelimitedStream(OutStream, #9);

            if Output = omCSV then
              RepDM.CreateDelimitedStream(OutStream, ',');

            if Output = omSSV then
              RepDM.CreateDelimitedStream(OutStream, ';');

            if Output = omPDF then
              RepDM.CreatePDFStream(OutStream);

          finally
            OutStream.Free;
          end;

          WriteLn('Done.');
          ResultData.Add('Success');
        finally
          RepDM.Free;
        end;
      finally
        DM.Free;
      end;
    except
      on E: Exception do begin
        ResultData.Add('ERROR');
        ResultData.Add(E.Message);
        WriteLn('********** ERROR **************');
        WriteLn(E.Message);
        AddLogEntry(RepName, E.Message + Config.Description, True);
      end;
    end;
  finally
    // always put the time there.
    TimeEnd := GetTickCount;
    WriteLn(Format('Elapsed Time: %.1f seconds', [ (TimeEnd - TimeStart) / 1000.0]));
    WriteLn('');
    ResultData.Insert(0, Format('%.1f', [ (TimeEnd - TimeStart) / 1000.0]));
    ResultData.Insert(1, Format('%.1f', [ (QueryEnd - QueryStart) / 1000.0]));
    ResultData.SaveToFile(ResultFileName);
  end;
end;

{
ReportEngineCGI /tsv ClosedTicketDetail TicketID=84421 IncludeNotes=1 /o test.tsv
}

procedure TReportMailerDM.StatusUpdate(Msg: string);
begin
  WriteLn('Reporting DB Connection: ' + Msg);
end;

end.

