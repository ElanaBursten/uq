unit PayrollSummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppModule, daDataModule, ppBands, ppClass,
  ppCtrls, ppReport, ppStrtch, ppSubRpt, ppVar, ppPrnabl, ppCache, ppProd,
  ppDB, ppComm, ppRelatv, ppDBPipe, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TPayrollSummaryDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel15: TppLabel;
    ppLabel23: TppLabel;
    ppLabel03: TppLabel;
    ppLabel04: TppLabel;
    ppLabel05: TppLabel;
    ppLabel06: TppLabel;
    ppLabel07: TppLabel;
    ppLabel08: TppLabel;
    ppLabel10: TppLabel;
    ppLabel09: TppLabel;
    ppLabel11: TppLabel;
    ppLabel31: TppLabel;
    ppLabel26: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText03: TppDBText;
    ppDBText04: TppDBText;
    ppDBText05: TppDBText;
    ppDBText06: TppDBText;
    ppDBText07: TppDBText;
    ppDBText08: TppDBText;
    ppDBText10: TppDBText;
    ppDBText09: TppDBText;
    ppDBText11: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppLabel4: TppLabel;
    ppLine1: TppLine;
    ppDBCalcT03: TppDBCalc;
    ppDBCalcT04: TppDBCalc;
    ppDBCalcT05: TppDBCalc;
    ppDBCalcT06: TppDBCalc;
    ppDBCalcT07: TppDBCalc;
    ppDBCalcT08: TppDBCalc;
    ppDBCalcT09: TppDBCalc;
    ppDBCalcT10: TppDBCalc;
    ppDBCalcT11: TppDBCalc;
    short_namePPText: TppDBText;
    EmpNumberText: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppLabel01: TppLabel;
    ppDBText01: TppDBText;
    ppLabel02: TppLabel;
    ppDBText02: TppDBText;
    COVLabel: TppLabel;
    COVDBText: TppDBText;
    ppLabel13: TppLabel;
    ppDBText13: TppDBText;
    ppDBCalcT01: TppDBCalc;
    ppDBCalcT02: TppDBCalc;
    ppDBCalcT12: TppDBCalc;
    ppDBCalcT13: TppDBCalc;
    ppLabel19: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalc03: TppDBCalc;
    ppDBCalc04: TppDBCalc;
    ppDBCalc05: TppDBCalc;
    ppDBCalc06: TppDBCalc;
    ppDBCalc07: TppDBCalc;
    ppDBCalc08: TppDBCalc;
    ppDBCalc09: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc01: TppDBCalc;
    ppDBCalc02: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppLabel20: TppLabel;
    ppDBText23: TppDBText;
    ppLabel21: TppLabel;
    ppDBText18: TppDBText;
    ppLine2: TppLine;
    ppLabel14: TppLabel;
    ppDBText14: TppDBText;
    ppDBCalc14: TppDBCalc;
    ppDBCalcT14: TppDBCalc;
    ppLabel22: TppLabel;
    ppDBText20: TppDBText;
    ppDBText15: TppDBText;
    PTOLabel: TppLabel;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    FHolidayDBText: TppDBText;
    FHolLabel: TppLabel;
    FHolidaySubTotalDBCalc: TppDBCalc;
    FHolTotalsDBCalc: TppDBCalc;
    Data: TADOStoredProc;
    procedure TotalsPrint(Sender: TObject);
  private
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, RbCustomTextFit;

{$R *.dfm}

{ TPayrollSummaryDM }

procedure TPayrollSummaryDM.Configure(QueryFields: TStrings);
var
  PC: string;
begin
  inherited;
  PC := GetString('pc');
  if PC = 'ALL' then begin
    Data.Parameters.ParamByName('@PayrollPC').value := Null;
    ppGroupFooterBand1.Visible := True;
  end else begin
    Data.Parameters.ParamByName('@PayrollPC').value := PC;
    ppGroupFooterBand1.Visible := False;
  end;
  Data.Parameters.ParamByName('@EndDate').value := GetDateTime('end_date');
  Data.Open;
  AlignRBComponentsByTag(Self);
end;

procedure TPayrollSummaryDM.TotalsPrint(Sender: TObject);
begin
  ScaleRBTextToFit(Sender as TppDBText, 8);
end;

initialization
  RegisterReport('PayrollSummary', TPayrollSummaryDM);

end.
