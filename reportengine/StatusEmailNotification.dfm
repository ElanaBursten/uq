inherited EmailStatusDM: TEmailStatusDM
  OldCreateOrder = True
  object EmailDS: TDataSource
    DataSet = Emails
    Left = 88
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = EmailPipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Fax Status Report'
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    ModalPreview = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowAutoSearchDialog = True
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 216
    Top = 24
    Version = '14.08'
    mmColumnWidth = 0
    DataPipelineName = 'EmailPipe'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 23283
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentWidth = True
        mmHeight = 15875
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 0
        LayerName = Foreground
      end
      object ppReportHeaderLabel: TppLabel
        UserName = 'ppReportHeaderLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Email Notification Status Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 3175
        mmTop = 2381
        mmWidth = 195527
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        UserName = 'Label10'
        HyperlinkEnabled = False
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3302
        mmLeft = 3175
        mmTop = 10054
        mmWidth = 6943
        BandType = 0
        LayerName = Foreground
      end
      object FromDateLabel: TppLabel
        UserName = 'FromDateLabel'
        HyperlinkEnabled = False
        Caption = 'FromDateLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3302
        mmLeft = 11906
        mmTop = 10054
        mmWidth = 18542
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel12: TppLabel
        UserName = 'Label12'
        HyperlinkEnabled = False
        Caption = 'To'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3302
        mmLeft = 43127
        mmTop = 10054
        mmWidth = 3471
        BandType = 0
        LayerName = Foreground
      end
      object ToDateLabel: TppLabel
        UserName = 'ToDateLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'ToDateLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3175
        mmLeft = 48683
        mmTop = 10054
        mmWidth = 27517
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel14: TppLabel
        UserName = 'Label14'
        HyperlinkEnabled = False
        Caption = 'Call Center:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 157692
        mmTop = 10054
        mmWidth = 16933
        BandType = 0
        LayerName = Foreground
      end
      object CallCenterLabel: TppLabel
        UserName = 'CallCenterLabel'
        HyperlinkEnabled = False
        Caption = '---'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 177007
        mmTop = 10054
        mmWidth = 3175
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        Transparent = True
        mmHeight = 3704
        mmLeft = 0
        mmTop = 18785
        mmWidth = 12965
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        UserName = 'Label2'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Email Address'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        Transparent = True
        mmHeight = 3704
        mmLeft = 13494
        mmTop = 18785
        mmWidth = 41275
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Ticket Number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        Transparent = True
        mmHeight = 3704
        mmLeft = 56092
        mmTop = 18785
        mmWidth = 32015
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        UserName = 'Label4'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Sent Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        Transparent = True
        mmHeight = 3704
        mmLeft = 89959
        mmTop = 18785
        mmWidth = 32279
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        UserName = 'Label5'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Error Details'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        Transparent = True
        mmHeight = 3704
        mmLeft = 124090
        mmTop = 18785
        mmWidth = 78317
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5027
      mmPrintPosition = 0
      object ppDBText1: TppDBText
        UserName = 'DBText1'
        HyperlinkEnabled = False
        DataField = 'status'
        DataPipeline = EmailPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'EmailPipe'
        mmHeight = 3704
        mmLeft = 0
        mmTop = 529
        mmWidth = 12965
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        UserName = 'DBText2'
        HyperlinkEnabled = False
        DataField = 'email_address'
        DataPipeline = EmailPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'EmailPipe'
        mmHeight = 3704
        mmLeft = 13494
        mmTop = 529
        mmWidth = 41275
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText3: TppDBText
        UserName = 'DBText3'
        HyperlinkEnabled = False
        DataField = 'ticket_number'
        DataPipeline = EmailPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'EmailPipe'
        mmHeight = 3704
        mmLeft = 56092
        mmTop = 529
        mmWidth = 32015
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText4: TppDBText
        UserName = 'DBText4'
        HyperlinkEnabled = False
        DataField = 'sent_date'
        DataPipeline = EmailPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'EmailPipe'
        mmHeight = 3704
        mmLeft = 89959
        mmTop = 529
        mmWidth = 32279
        BandType = 4
        LayerName = Foreground
      end
      object ppDBMemo1: TppDBMemo
        UserName = 'DBMemo1'
        CharWrap = False
        DataField = 'error_details'
        DataPipeline = EmailPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        Stretch = True
        Transparent = True
        DataPipelineName = 'EmailPipe'
        mmHeight = 3704
        mmLeft = 124090
        mmTop = 529
        mmWidth = 78317
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeading = 0
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 12700
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 12700
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 8
        LayerName = Foreground
      end
      object ppReportCopyright: TppLabel
        UserName = 'ppReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Email Notification Status Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 55827
        mmTop = 4233
        mmWidth = 81227
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 167746
        mmTop = 2117
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 178594
        mmTop = 6615
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        UserName = 'Label13'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 3175
        mmTop = 2117
        mmWidth = 48683
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppSummaryBand1: TppSummaryBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 4498
      mmPrintPosition = 0
      object SummarySubRep: TppSubReport
        UserName = 'SummarySubRep'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        Visible = False
        mmHeight = 3969
        mmLeft = 0
        mmTop = 529
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Fax Status Report'
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '14.08'
          mmColumnWidth = 0
          object ppTitleBand1: TppTitleBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 38629
            mmPrintPosition = 0
            object ppLabel6: TppLabel
              UserName = 'Label6'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsUnderline]
              Transparent = True
              mmHeight = 3704
              mmLeft = 16933
              mmTop = 32544
              mmWidth = 19579
              BandType = 1
            end
            object ppLabel7: TppLabel
              UserName = 'Label7'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Count'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsUnderline]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 46302
              mmTop = 32544
              mmWidth = 11377
              BandType = 1
            end
            object ppLabel8: TppLabel
              UserName = 'Label8'
              HyperlinkEnabled = False
              Caption = 'Totals by status:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 12
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 5292
              mmLeft = 16669
              mmTop = 16140
              mmWidth = 33602
              BandType = 1
            end
          end
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ppDBText5: TppDBText
              UserName = 'DBText5'
              HyperlinkEnabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              mmHeight = 3969
              mmLeft = 16669
              mmTop = 1058
              mmWidth = 26458
              BandType = 4
            end
            object ppDBText6: TppDBText
              UserName = 'DBText6'
              HyperlinkEnabled = False
              DataField = 'N'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 46831
              mmTop = 1058
              mmWidth = 11642
              BandType = 4
            end
          end
          object ppSummaryBand2: TppSummaryBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 13229
            mmPrintPosition = 0
          end
        end
      end
    end
    object daDataModule1: TdaDataModule
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object EmailPipe: TppDBPipeline
    DataSource = EmailDS
    OpenDataSource = False
    UserName = 'EmailPipe'
    Left = 152
    Top = 24
    object EmailPipeppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'eqr_id'
      FieldName = 'eqr_id'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 10
      Position = 0
    end
    object EmailPipeppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'ticket_id'
      FieldName = 'ticket_id'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 1
    end
    object EmailPipeppField3: TppField
      FieldAlias = 'email_address'
      FieldName = 'email_address'
      FieldLength = 60
      DisplayWidth = 60
      Position = 2
    end
    object EmailPipeppField4: TppField
      FieldAlias = 'status'
      FieldName = 'status'
      FieldLength = 20
      DisplayWidth = 20
      Position = 3
    end
    object EmailPipeppField5: TppField
      FieldAlias = 'sent_date'
      FieldName = 'sent_date'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 4
    end
    object EmailPipeppField6: TppField
      FieldAlias = 'error_details'
      FieldName = 'error_details'
      FieldLength = 0
      DataType = dtMemo
      DisplayWidth = 10
      Position = 5
      Searchable = False
      Sortable = False
    end
    object EmailPipeppField7: TppField
      FieldAlias = 'ticket_number'
      FieldName = 'ticket_number'
      FieldLength = 20
      DisplayWidth = 20
      Position = 6
    end
    object EmailPipeppField8: TppField
      FieldAlias = 'ticket_format'
      FieldName = 'ticket_format'
      FieldLength = 20
      DisplayWidth = 20
      Position = 7
    end
    object EmailPipeppField9: TppField
      FieldAlias = 'kind'
      FieldName = 'kind'
      FieldLength = 20
      DisplayWidth = 20
      Position = 8
    end
    object EmailPipeppField10: TppField
      FieldAlias = 'con_name'
      FieldName = 'con_name'
      FieldLength = 50
      DisplayWidth = 50
      Position = 9
    end
    object EmailPipeppField11: TppField
      FieldAlias = 'work_state'
      FieldName = 'work_state'
      FieldLength = 2
      DisplayWidth = 2
      Position = 10
    end
    object EmailPipeppField12: TppField
      FieldAlias = 'work_city'
      FieldName = 'work_city'
      FieldLength = 40
      DisplayWidth = 40
      Position = 11
    end
    object EmailPipeppField13: TppField
      FieldAlias = 'LastBounce'
      FieldName = 'LastBounce'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 12
    end
  end
  object Emails: TADOQuery
    Connection = ReportEngineDM.Conn3
    Parameters = <
      item
        Name = 'CallCenter'
        DataType = ftString
        Value = Null
      end
      item
        Name = 'DateFrom'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = 'DateTo'
        DataType = ftDateTime
        Value = Null
      end>
    SQL.Strings = (
      
        'declare @CallCenter varchar(30)'#13#10'set @CallCenter = :CallCenter'#13#10 +
        #13#10'select eqr.*, t.ticket_number, t.ticket_format, t.kind,'#13#10'  t.c' +
        'on_name, t.work_state, t.work_city'#13#10'from email_queue_result eqr'#13 +
        #10'  inner join ticket t on t.ticket_id = eqr.ticket_id'#13#10'where ((@' +
        'CallCenter = '#39'*'#39') or (t.ticket_format =@CallCenter))'#13#10'  and ((eq' +
        'r.sent_date >= :DateFrom) and (eqr.sent_date < :DateTo))'#13#10'order ' +
        'by eqr.status, eqr.sent_date')
    Left = 32
    Top = 24
  end
end
