unit RbCustomTextFit;

{
To use, add code like this to a TppCustomText descendant component's OnPrint event:

  ScaleRBTextToFit(Sender as TppCustomText, 12);
}

interface

uses
  Windows, SysUtils, Classes, ppCtrls;

procedure ScaleRBTextToFit(T: TppCustomText; DefaultPointSize: Integer);

implementation

uses
  ppUtils, ppClass, ppPrintr, ppTypes;

function TryToFit(T: TppCustomText; PointSize: Single): Boolean;
var
  TextWidth: Integer;
  ControlWidth: Integer;
  PixPerInchY: Integer;
  Height: Integer;
  lPrinter: TppPrinter;
begin
  if (T.Band <> nil) then
    lPrinter := T.Band.GetPrinter
  else
    lPrinter := ppPrinter;
  // Get the width in pixels of the TppCustomText control
  ControlWidth := ppUtils.ppToScreenPixels(T.mmWidth, utMMThousandths, pprtHorizontal, lPrinter);

  // Attempt fine precision font sizing
  T.Font.PixelsPerInch := 600;
  PixPerInchY := T.Font.PixelsPerInch;
  Height := Round(PointSize * PixPerInchY / 72);
  T.Font.Height := - Height;
  // Get the width in pixels of control T's Text drawn using its Font
  TextWidth := ppUtils.ppGetSpTextWidth(T.Font, T.Text);

  // Does the text fit the control
  Result := (TextWidth <= ControlWidth);
end;

procedure ScaleRBTextToFit(T: TppCustomText; DefaultPointSize: Integer);
var
  Size: Single;
begin
  Assert(not T.WordWrap, Format('ScaleRBTextToFit only works when %s.WordWrap=False', [T.Name]));
  Size := DefaultPointSize;
  // Reduce by 1/4 point until the text fits; bail at 4 points so it isn't too small
  repeat
    if TryToFit(T, Size) then
      Exit;

    Size := Size - 0.25;
  until Size <= 4.0;
end;

end.
