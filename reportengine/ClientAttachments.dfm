inherited ClientAttachmentsDM: TClientAttachmentsDM
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  object ClientAttachmentsSP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_ClientAttachments'
    Parameters = <
      item
        Name = '@CallCenters'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@ClientIDs'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@AttachUploadStartDate'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@AttachUploadEndDate'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ForeignTypes'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@IncludeFileTypes'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@ExcludeFileNames'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 80
    Top = 24
  end
end
