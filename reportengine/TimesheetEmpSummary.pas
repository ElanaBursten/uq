unit TimesheetEmpSummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppBands, ppClass, ppCtrls, ppReport,
  ppStrtch, ppSubRpt, ppVar, ppPrnabl, ppCache, ppProd, ppDB, ppComm,
  ppRelatv, ppDBPipe, myChkBox, SqlTimSt, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimesheetEmpSummaryDm = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLabel4: TppLabel;
    ppLine1: TppLine;
    SummarySubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ppTitleBand1: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel32: TppLabel;
    ppDBText22: TppDBText;
    ppLabel21: TppLabel;
    reg_hours_total: TppDBCalc;
    ppLabel20: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText7: TppDBText;
    ppLabel7: TppLabel;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppDBText9: TppDBText;
    ppLabel9: TppLabel;
    ppDBText10: TppDBText;
    ppLabel10: TppLabel;
    ppDBText11: TppDBText;
    ppLabel11: TppLabel;
    ppDBText12: TppDBText;
    ppLabel12: TppLabel;
    ppDBText14: TppDBText;
    ppLabel13: TppLabel;
    ppDBText15: TppDBText;
    ppLabel14: TppLabel;
    ppDBText16: TppDBText;
    ppDBText6: TppDBText;
    ppLine3: TppLine;
    ppDBCalc9: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBCalc17: TppDBCalc;
    ppDBCalc18: TppDBCalc;
    ppDBCalc19: TppDBCalc;
    ppDBCalc20: TppDBCalc;
    ppDBCalc21: TppDBCalc;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    ppDBCalc24: TppDBCalc;
    ppDBCalc25: TppDBCalc;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText23: TppDBText;
    ppLabel5: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc26: TppDBCalc;
    ppLine2: TppLine;
    EmpTotalsDS: TDataSource;
    EmpTotalsPipe: TppDBPipeline;
    ppDBText24: TppDBText;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppDBText25: TppDBText;
    ppDBCalc27: TppDBCalc;
    ppDBCalc28: TppDBCalc;
    ppTitleBand2: TppTitleBand;
    ppPageStyle1: TppPageStyle;
    ppLabel28: TppLabel;
    ppDBText26: TppDBText;
    ppDBCalc29: TppDBCalc;
    FloatingHolidayLabel: TppLabel;
    FloatingHolidayCheckBox: TmyDBCheckBox;
    ppDBCalc30: TppDBCalc;
    FloatingHolidayTotal: TppLabel;
    FloatingHolidayGrandTotal: TppLabel;
    PipeDS: TDataSource;
    ppDatesLabel: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel2: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    EmployeeNameLabel: TppLabel;
    EmployeeNumLabel: TppLabel;
    ppLabel3: TppLabel;
    ppDBText2: TppDBText;
    ppDBCalc31: TppDBCalc;
    ppDBCalc32: TppDBCalc;
    ppLabel15: TppLabel;
    ppDBText27: TppDBText;
    ppDBCalc33: TppDBCalc;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    Data: TADOStoredProc;
    Summary: TADODataSet;
    EmpTotals: TADODataSet;
    Master: TADODataSet;
    procedure ppGroupHeaderBand1BeforePrint(Sender: TObject);
    procedure ppGroupFooterBand1BeforePrint(Sender: TObject);
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
    procedure FloatingHolidayCheckBoxPrint(Sender: TObject);
    procedure TotalsPrint(Sender: TObject);
  private
    FCalcDone:  Boolean;
    FFloatingHolidays: Integer;
    FFloatingHolidaysTotal: Integer;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, StrUtils, RbCustomTextFit, OdAdoUtils;

{$R *.dfm}

{ TTimesheetSummaryDM }

procedure TTimesheetEmpSummaryDm.Configure(QueryFields: TStrings);
var
  EmpID: Integer;
  RecsAffected : oleVariant;
begin
  inherited;

  //TODO: Remove this block to enable Floating Holiday support
  FloatingHolidayLabel.Visible := False;
  FloatingHolidayCheckBox.Visible := False;
  FloatingHolidayTotal.Visible := False;
  FloatingHolidayGrandTotal.Visible := False;
  //-end floating holiday disable

  FCalcDone := False;
  FFloatingHolidays := 0;
  FFloatingHolidaysTotal := 0;

  EmpID := GetInteger('emp_id');
  with Data do begin    //QMANTWO-533 EB
    Parameters.ParamByName('@EmpId').Value := EmpID;
    Parameters.ParamByName('@StartDate').Value := GetDateTime('start_date');
    Parameters.ParamByName('@EndDate').Value := GetDateTime('end_date');
    Open;
  end;

   //QMANTWO-533 EB
  Master.Recordset := Data.Recordset;
  EmpTotals.Recordset := Data.Recordset.NextRecordset(RecsAffected );
  Summary.Recordset := Data.Recordset.NextRecordset(RecsAffected);

  ShowDateRange(ppDatesLabel, 'start_date', 'end_date');
  ShowEmployeeName(EmployeeNameLabel, EmpID, 'Employee:');
  ShowText(EmployeeNumLabel, 'Employee Number:',
    IfThen(Master.IsEmpty, 'n/a', Master.FieldByName('emp_number').AsString));
  AlignRBComponentsByTag(Self);
end;

procedure TTimesheetEmpSummaryDm.ppGroupHeaderBand1BeforePrint(Sender: TObject);
begin
  inherited;
  FFloatingHolidays := 0;
end;

procedure TTimesheetEmpSummaryDm.FloatingHolidayCheckBoxPrint(Sender: TObject);
begin
  inherited;
  if (Master.FieldByName('floating_holiday').AsBoolean = True) then begin
    Inc(FFloatingHolidays);
    if (not FCalcDone) then
      Inc(FFloatingHolidaysTotal);
  end;
end;

procedure TTimesheetEmpSummaryDm.TotalsPrint(Sender: TObject);
begin
  ScaleRBTextToFit(Sender as TppDBText, 8);
end;

procedure TTimesheetEmpSummaryDm.ppGroupFooterBand1BeforePrint(
  Sender: TObject);
begin
  inherited;
  if (FFloatingHolidays = 0) then
    FloatingHolidayTotal.Caption := '-'
  else begin
    FloatingHolidayTotal.Caption := IntToStr(FFloatingHolidays);
  end;
end;

procedure TTimesheetEmpSummaryDm.ppSummaryBand1BeforePrint(
  Sender: TObject);
begin
  inherited;
  if (FFloatingHolidaysTotal = 0) then
    FloatingHolidayGrandTotal.Caption := '-'
  else
    FloatingHolidayGrandTotal.Caption := IntToStr(FFloatingHolidaysTotal);
  FCalcDone := True;
end;

initialization
  RegisterReport('TimesheetEmpSummary', TTimesheetEmpSummaryDm);

end.

