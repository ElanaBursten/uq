unit StatusAutoEmail;

(*
To test:
Add an email notification rule using the admin or: insert into email_queue_rules (call_center, client_code, status, state) values ('Atlanta', '*', '*', '*')
Add a manual ticket using the QM client, and make it match the above rule
Be sure the RE is set to enable VERP checking and a POP server in the .ini
Set the caller email address: update ticket set caller_email  = 'erik.berry@oasisdigital.com' where ticket_id = 1000
Run QManagerAdmin.exe /emailqueue *
Be sure the email is sent
Create a similar ticket and set the email to an invalid address
Run QManagerAdmin.exe /emailqueue *
Wait a minute for the bounce to arrive
Run QManagerAdmin.exe /emailqueue *
Be sure the email marked as a bounce in the email notification report
*)
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppCtrls, ppBands, ppStrtch,
  ppMemo, ppClass, ppVar, ppPrnabl, ppCache, ppProd, ppReport, DB,
  ppDB, ppComm, ppRelatv, ppDBPipe, ReportConfig,

  OdSimpleLog, Data.Win.ADODB;

type
  TStatusAutoEmailDM = class(TBaseReportDM)
    qryTicketsOnQueue: TADOQuery;
    qryLocatesOnQueue: TADOQuery;
    insEmailQueueResult: TADOQuery;
    delQueueRecordsForTicket: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
  private
    EmailLog : TEnhancedLog;
    Config: TReportConfig;
    CallCenter: string;
    IniFileName: string;
    LogCaptureAll: Boolean;
    procedure SendEmailsForCenter(const CallCenter: string);
    procedure ProcessBounces;
    procedure StartEmailLogging(Path: string; CallCenter: string);
    procedure AddEmailLogEntry(const Message: string; IsError : Boolean = False; Console: Boolean = False );
  public
    procedure Run;
  end;

implementation

uses ReportEngineDMu, OdIsoDates, odADOutils, OdMiscUtils, OdInternetUtil,
  OdExceptions, IniFiles, IdPOP3, IdMessage, StrUtils, IdSSLOpenSSL,
  IdExplicitTLSClientServerBase, IdSMTP, QMConst;

{$R *.dfm}

{ TStatusAutoEmailDM }

procedure TStatusAutoEmailDM.StartEmailLogging(Path: string; CallCenter: string);
begin
  EmailLog := TEnhancedLog.Create(EmailResponses + CallCenter + '.log', Path);
end;

procedure TStatusAutoEmailDM.AddEmailLogEntry(const Message: string; IsError : Boolean = False; Console: Boolean = False );
begin
 if Console then
   WriteLn(Message)
 else begin
   Assert(Assigned(EmailLog), 'EmailLog is not assigned');
   EMailLog.AddLogEntry(Message, IsError);
 end;
end;

procedure TStatusAutoEmailDM.Run;
var
  DM: TReportEngineDM;
begin
  inherited;
  try
    if ParamCount < 2 then
      raise Exception.Create('Parameters: /emailqueue CallCenter /logalltofile  {CallCenter can be a single center or * for ALL}'
         + #10#13 + '{/logalltofile is optional - use to capture all console output to log file} ');

    CallCenter := ParamStr(2);

    if ParamCount = 3 then
     LogCaptureAll := LowerCase(ParamStr(3))='/logalltofile';

    IniFileName := ChangeFileExt(ParamStr(0), '.ini');
    Config := LoadConfig(IniFileName);

    if NotEmpty(Config.LogFileName) then begin
      StartEmailLogging(Config.LogFileName, CallCenter);
      WriteLn('Output logged to ' + GetDailyFileName(Config.LogFileName, EmailResponses + CallCenter + '.log'));
    end;

    AddEmailLogEntry('Starting at ' + DateTimeToStr(Now), False, not LogCaptureAll);

    DM := TReportEngineDM.Create(nil);
    try
      AddEmailLogEntry('Connecting to ' + Config.MainDB.Server + '/' + Config.MainDB.DB, False, not LogCaptureAll);
      ConnectAdOConnectionWithConfig(DM.Conn, Config.MainDB);

      UseConnection(DM.Conn, 800);

      SendEmailsForCenter(CallCenter);

      AddEmailLogEntry('Done', False, not LogCaptureAll);
    finally
      FreeAndNil(DM);
    end;
  except
    on E: Exception do begin
      AddEmailLogEntry('********** ERROR **************', True);
      AddEmailLogEntry(E.Message, True);
      Sleep(2000);
    end;
  end;
end;

procedure TStatusAutoEmailDM.SendEmailsForCenter(const CallCenter: string);
const
  DeleteQueue = 'delete from email_queue ' +
    'where ls_id in ' +
    '  (select ls_id from locate_status where locate_id in ' +
    '    (select locate_id from locate where ticket_id = %d))';
  InsertResult = 'insert into email_queue_result (ticket_id, email_address, status, sent_date, email_body, error_details, ls_id) values (%d, %s, %s, %s, %s, %s, %s)';    //QM-146 sr
  DUE_DATE_STATUS = 'OXJ';  //QM-224 sr this is a hard code but should be pulled in from a list.
  STATUS_DUE_DATE = '@@DUEDATE';
var
  EmailAddr: string;
  EmailAddrBcc: string;  //QM-137
  TicketNumber: string;
  TicketID: Integer;
  TicketKind: string;
  Subject: string;
  Body: TStringList;
  ClientName: string;
  ClientCode: string;
  StatusName: string;
  Status: string;
  StatusMessage: string;
  Closed: string;
  ClosedDate: string;
  Address: string;
  Location: string;
  City, County, State: string;
  DeletedItems: Integer;
  CompanyName: string;
  CompanyPhone: string;
  ContractorContact: string;
  ContractorName: string;
  LocateStatusID: string;
//  IsDueDateStatus: boolean; //qm-224 sr
  TicketDueDate: string;
  BodyText: string;  //QM-146 sr
  procedure AddResultRecord(Status,BodyText, ErrorMsg: string);    //QM-146 sr
  var
    InsertedRecords: Integer;   //QM-137   sr
  begin
    insEmailQueueResult.SQL.clear; //QM-137 sr
    InsertedRecords:= -1;    //QM-137 sr
    insEmailQueueResult.SQL.Add(Format(InsertResult, [TicketID, QuotedStr(EmailAddr), QuotedStr(Status), IsoDateTimeToStrQuoted(Now), QuotedStr(BodyText), QuotedStr(ErrorMsg), LocateStatusID]));//QM-137 sr
    InsertedRecords := insEmailQueueResult.ExecSQL;  //QM-137 sr
    if InsertedRecords < 1 then //QM-137 sr
      raise Exception.Create('No email result records added for ticket ' + IntToStr(TicketID));
  end;

  procedure DeleteQueueRecordsForTicket;
  var
    deletedRecords: Integer;   //QM-137   sr
  begin
    delQueueRecordsForTicket.SQL.clear; //QM-137 sr
    deletedRecords:= -1;    //QM-137 sr
    delQueueRecordsForTicket.SQL.Add(Format(DeleteQueue, [TicketID])); //QM-137 sr
    deletedRecords := delQueueRecordsForTicket.ExecSQL;  //QM-137 sr

    if deletedRecords < 1 then
      raise Exception.Create('No email queue items deleted for ticket: ' + IntToStr(TicketID));
  end;

  procedure FilterLocates;
  begin
    qryLocatesOnQueue.Filter := 'ticket_id=' + IntToStr(TicketID);
    qryLocatesOnQueue.Filtered := False;
    qryLocatesOnQueue.Filtered := True;
//    Assert(qryLocatesOnQueue.RecordCount >= 1, 'No locates on ticket ' + IntToStr(TicketID));   queries do not return record count  SR
  end;


begin
  AddEmailLogEntry('Sending emails for call center: ' + CallCenter+ '.  Gathering tickets and locates.');
  qryTicketsOnQueue.Parameters.ParamByName('CallCenter').Value := CallCenter;
  qryTicketsOnQueue.Open;

  qryLocatesOnQueue.Parameters.ParamByName('CallCenter').Value := CallCenter;
  qryLocatesOnQueue.Open;

  AddEmailLogEntry(Format('%d ticket(s) on the queue for email notification', [qryTicketsOnQueue.RecordCount]));
  Body := TStringList.Create;
  try
    while not qryTicketsOnQueue.Eof do begin
      Body.Clear;
      EmailAddr := qryTicketsOnQueue.FieldByName('caller_email').AsString;
      TicketNumber := qryTicketsOnQueue.FieldByName('ticket_number').AsString;
      TicketID := qryTicketsOnQueue.FieldByName('ticket_id').AsInteger;
      TicketKind := qryTicketsOnQueue.FieldByName('kind').AsString;
      TicketDueDate := qryTicketsOnQueue.FieldByName('due_date').AsString; //qm-224 sr
      Address := qryTicketsOnQueue.FieldByName('street').AsString; // Includes street numbers
      City := qryTicketsOnQueue.FieldByName('work_city').AsString;
      County := qryTicketsOnQueue.FieldByName('work_county').AsString;
      State := qryTicketsOnQueue.FieldByName('work_state').AsString;
      ContractorName := qryTicketsOnQueue.FieldByName('con_name').AsString;
      ContractorContact := qryTicketsOnQueue.FieldByName('caller').AsString;
      LocateStatusID := qryTicketsOnQueue.FieldByName('ls_id').AsString;
      Assert(NotEmpty(LocateStatusID));
      Location := Format('%s, %s', [City, State]);
      FilterLocates;
      CompanyName := qryLocatesOnQueue.FieldByName('company_name').AsString;
      CompanyPhone := qryLocatesOnQueue.FieldByName('phone').AsString;

      if IsValidEmailAddress(EmailAddr) then begin
        AddEmailLogEntry(Format('Sending email for ticket %s (%d) to: %s', [TicketNumber, TicketID, EmailAddr]));
        Subject := Format('Status update for ticket %s', [TicketNumber]);
        Body.Add(Subject);
        Body.Add('Ticket Kind: ' + TicketKind);
        Body.Add('Contractor: ' + ContractorName);
        Body.Add('Contact: ' + ContractorContact);
        Body.Add('');

        Body.Add('Address:');
        Body.Add(CompressWhiteSpace(Address));
        Body.Add(CompressWhiteSpace(Location));
        Body.Add('County: ' + County);
        Body.Add('');

        Body.Add('Locating Company:');
        Body.Add(CompanyName);
        Body.Add(CompanyPhone);

        Body.Add('');
        Body.Add('Locates:');
        FilterLocates;
        while not qryLocatesOnQueue.Eof do
        begin
//          IsDueDateStatus := false; //qm-224 sr init to false for ea. locate
          ClientCode := qryLocatesOnQueue.FieldByName('client_code').AsString;
          ClientName := qryLocatesOnQueue.FieldByName('client_name').AsString;
          StatusName := qryLocatesOnQueue.FieldByName('status_name').AsString;
          Status := qryLocatesOnQueue.FieldByName('status').AsString;
//          if Status = DUE_DATE_STATUS then IsDueDateStatus := true;   //QM-224 sr

          StatusMessage := qryLocatesOnQueue.FieldByName('status_message').AsString;

          if qryLocatesOnQueue.FieldByName('incl_bcc').AsBoolean then  //QM-137
            EmailAddrBcc := qryLocatesOnQueue.FieldByName('bcc_address').AsString;//QM-137

          if qryLocatesOnQueue.FieldByName('closed_date').AsDateTime > 1 then
            ClosedDate := DateTimeToStr(qryLocatesOnQueue.FieldByName('closed_date').AsDateTime)
          else
            ClosedDate := '';
          if qryLocatesOnQueue.FieldByName('closed').AsBoolean then
            Closed := 'Yes'
          else
            Closed := 'No';
          Body.Add('Utility: ' + ClientName);
          Body.AddFmt('Status: %s (%s)', [StatusName, Status]);
          if NotEmpty(StatusMessage) then
          begin
//            If IsDueDateStatus then   //QM-224 sr
            StatusMessage := StringReplace(StatusMessage, STATUS_DUE_DATE, TicketDueDate,[rfReplaceAll,rfIgnoreCase] ); //QM-224 sr

            Body.Add('Status Message: ' + StatusMessage);
          end;
          Body.Add('Completed: ' + Closed);
          Body.Add('Date: ' + ClosedDate);
          Body.Add('');
          qryLocatesOnQueue.Next;
        end;//while not qryLocatesOnQueue.Eof do
        BodyText := Body.Text; //qm-146
        try
          AddEmailLogEntry('Email: ' + BodyText);  //qm-220  sr
          SendEmailWithIniSettings(IniFileName, 'StatusEmail', EmailAddr, EmailAddrBcc, Subject, BodyText, nil, True, LocateStatusID);//QM-137  qm-146     TEST POINT
        except
          on E: Exception do begin
            if (E is EODConfigError) then
              raise
            else begin
              AddEmailLogEntry('Error sending email: ' + E.Message + '.  Recording failure.', True);
              AddResultRecord('Error','', E.Message);  //qm-146 sr
              qryTicketsOnQueue.Next; //!!
              Continue;
            end;
          end;
        end;
        AddEmailLogEntry('Clearing ticket from queue and recording sent email...');
        DeleteQueueRecordsForTicket;      //   TEST POINT
        AddResultRecord('Sent', BodyText, '');   //qm-220 sr    //   TEST POINT
      end
      else begin
        if IsEmpty(EmailAddr) then
          AddResultRecord('Error', '', 'No email address')    //qm-146 sr
        else
          AddResultRecord('Error', '', 'Invalid email: ' + EmailAddr);    //qm-146 sr
        AddEmailLogEntry(Format('Ticket %s (%d) does not have a valid caller_email: %s', [TicketNumber, TicketID, EmailAddr]));
        DeleteQueueRecordsForTicket;   //   TEST POINT
      end;
      qryTicketsOnQueue.Next;
    end; //while
  finally
    FreeAndNil(Body);
  end;
  qryTicketsOnQueue.Close;
  qryLocatesOnQueue.Close;
  AddEmailLogEntry('---------------------------------------------------------------------');
  ProcessBounces;
 end;

procedure TStatusAutoEmailDM.ProcessBounces;
const
  UpdateBounce = 'update email_queue_result set Status = ''Bounce'', error_details = %s where ls_id = %s';

var
  Ini: TIniFile;
  POP: TIdPOP3;
  POPServer, POPUser, POPPass: string;
  POPPort: Integer;
  UseVERP: Boolean;
  UseSSL: Boolean;
  Msg: TIdMessage;
  MsgCount: Integer;
  i, j: Integer;
  NumEmailsProcessed: Integer;
  MsgDescription: string;
  BounceID: string;
  BouncesSQL: string;
  SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
const
  MaxEmailsPerSession = 50;
  MaxEmailsPerExecution = 500;

  function CheckForVERPBounce(const Address: string): Boolean;
  begin
    Result := False;
    if IsVERPReturnIDAddress(Address) then begin
      BounceID := Trim(GetVERPBounceID(Address));
      if NotEmpty(BounceID) and IsInteger(BounceID) then begin
        BouncesSQL := BouncesSQL + sLineBreak + Format(UpdateBounce, [QuotedStr(MsgDescription), BounceID]);
        AddEmailLogEntry('Processed bounce for locate status ID: ' + BounceID);
        Result := True;
      end;
    end;
  end;

begin
  Ini := TIniFile.Create(IniFileName);
  try
    UseVERP := Ini.ReadBool('StatusEmailBounce', 'Enabled', False);
    POPServer := Ini.ReadString('StatusEmailBounce', 'POPServer', '');
    POPUser := Ini.ReadString('StatusEmailBounce', 'POPUser', '');
    POPPass := Ini.ReadString('StatusEmailBounce', 'POPPass', '');
    POPPort := Ini.ReadInteger('StatusEmailBounce', 'POPPort', 110);
    UseSSL := Ini.ReadBool('StatusEmailBounce', 'UseSSL', False);
  finally
    FreeAndNil(Ini);
  end;

  if UseVERP and NotEmpty(POPServer) and NotEmpty(POPUser) and NotEmpty(POPPass) then begin
    AddEmailLogEntry(Format('Checking POP server %s as %s for email bounces', [POPServer, POPUser]));
    Msg := nil;
    POP := TIdPOP3.Create(nil);
    try
      Msg := TIdMessage.Create(nil);
      POP.Host := POPServer;
      POP.Username := POPUser;
      POP.Password := POPPass;
      if UseSSL then begin
        SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create;
        POP.Port := POPPort;
        POP.IOHandler := SSLHandler;
        POP.UseTLS := utUseImplicitTLS;
      end;
      try
        NumEmailsProcessed := 0;
        while NumEmailsProcessed < MaxEmailsPerExecution do begin
          POP.Connect;
          MsgCount := POP.CheckMessages;
          if MsgCount = 0 then
            Break;

          //Limit the number of bounces processed per run to prevent timeout
          if MsgCount > MaxEmailsPerSession then
            MsgCount := MaxEmailsPerSession;

          for i := 1 to MsgCount do begin
            if not Pop.RetrieveHeader(i, Msg) then
              raise Exception.CreateFmt('Failed to retrieve POP message #%d from %s', [i, POPServer]);
            MsgDescription := Format('Bounce Date: %s,  Subject: %s', [DateTimeToStr(Msg.Date), LeftStr(Msg.Subject, 80)]);
            if not CheckForVERPBounce(Msg.Headers.Values['To']) then begin
              for j := 0 to Msg.Recipients.Count - 1 do begin
                if CheckForVERPBounce(Msg.Recipients[j].Address) then
                  Break;
                if j = Msg.Recipients.Count - 1 then
                  AddEmailLogEntry(Format('Removing non-bounce email "%s" to %s', [Msg.Subject, CompressWhiteSpace(Msg.Recipients.EMailAddresses)]));
              end;
            end;
            Inc(NumEmailsProcessed);
            POP.Delete(i);
          end;
          if NotEmpty(BouncesSQL) then
            qryLocatesOnQueue.Connection.Execute(BouncesSQL);
          POP.Disconnect; // Commit
          BouncesSQL := '';
        end;
      except
        on E: Exception do begin
          AddEmailLogEntry(Format('Error checking POP server for bounces: %s', [E.Message]), True);
        end;
      end;
    finally
      FreeAndNil(POP);
      FreeAndNil(Msg);
      FreeAndNil(SSLHandler);
    end;
  end
  else
    AddEmailLogEntry('Email bounce detection disabled.');
end;

procedure TStatusAutoEmailDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(EmailLog);
  inherited;
end;

end.

