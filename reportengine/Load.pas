unit Load;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, ppModule,
  daDataModule, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TLoadReportDM = class(TBaseReportDM)
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    short_nameDBText: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppFooterBand1: TppFooterBand;
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLine1: TppLine;
    ppDBText15: TppDBCalc;
    ppDBText16: TppDBCalc;
    ppDBText17: TppDBCalc;
    ppDBText18: TppDBCalc;
    ppDBText19: TppDBCalc;
    ppDBText20: TppDBCalc;
    ppDBText21: TppDBCalc;
    ppDBText22: TppDBCalc;
    ppDBText23: TppDBCalc;
    ppDBText24: TppDBCalc;
    ppDBText25: TppDBCalc;
    ppDBText26: TppDBCalc;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    daDataModule1: TdaDataModule;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppDBCalc77: TppDBCalc;
    ppLabel10: TppLabel;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses OdRbHierarchy, ReportEngineDMu;

{$R *.dfm}

{ TLoadReportDM }

procedure TLoadReportDM.Configure(QueryFields: TStrings);
var
  ReportDate: TDateTime;
  EmployeeStatus: Integer;
  ManagerID: Integer;
begin
  inherited;
  AddHierGroups(Report, short_nameDBText);
  ManagerID := GetInteger('manager_id');
  ReportDate := SafeGetDateTime('Today', Now);
  EmployeeStatus := GetInteger('EmployeeStatus');
  with SP do begin
    Parameters.ParamByName('@ManagerID').value := ManagerID;
    Parameters.ParamByName('@Today').value := ReportDate;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Open;
  end;

  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
  ShowEmployeeName(ManagerLabel, ManagerID);
end;

initialization
  RegisterReport('LocatorLoad', TLoadReportDM);

end.

