unit TimeExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, Data.Win.ADODB;

type
  TTimeExportDM = class(TBaseReportDM)
    FindProfitCenter: TADOQuery;
    SP: TADOStoredProc;
  private
    FEndingDate: TDateTime;
    FProfitCenter: string;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
    procedure CreatePDFStream(Str : TStream); override;
  end;

implementation

{$R *.dfm}

{ TTimeExportDM }

uses
  OdDbUtils, ReportEngineDMu;

procedure TTimeExportDM.Configure(QueryFields: TStrings);
var
  Weeks: Integer;
  ManagerID: Integer;
begin
  inherited;
  with SP do begin
    ManagerID := GetInteger('manager_id');
    Weeks := GetInteger('weeks');   // always 2, hardcoded in client
    FEndingDate := GetDateTime('period_ending');

    if GetString('Report') = 'TimeExportNew' then begin
      // switch to new SP
      // Ticket.ProcedureName := 'tse_export_adp;1';
      // Ticket.Params.Refresh;

      raise Exception.Create('Export of new timesheet data to ADP format is not available, please contact support.');
    end else begin
      //Ticket.ProcedureName := 'payroll_export_adp;1';
    end;

    Parameters.ParamByName('@manager').Value := ManagerID;
    Parameters.ParamByName('@datefrom').Value := FEndingDate - Weeks*7 + 1;
    Parameters.ParamByName('@dateto').Value := FEndingDate;
    Parameters.ParamByName('@LevelLimit').Value := SafeGetInteger('HierarchyDepth', 99);
    Open;
  end;

  with FindProfitCenter do begin
    Parameters.ParamByName('emp_id').Value := ManagerID;
    Open;
    FProfitCenter := Fields[0].AsString;
    Close;
  end;
end;

procedure TTimeExportDM.CreatePDFStream(Str: TStream);
begin
  raise Exception.Create('This report does not support PDF creation, only export.');
end;

procedure TTimeExportDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
var
  FileNameStr: string;
begin
  // First 6 chars are EPIV8T   (don't know what it means)
  // Then the last two digits of the profit center
  FileNameStr := 'EPIV8T' + Copy(FProfitCenter, 2, 2) + '.csv' + #13#10;

  Str.Write(FileNameStr[1], Length(FileNameStr));
  SaveDelimToStream(SP, Str, ',', True);
end;

initialization
  RegisterReport('TimeExport', TTimeExportDM);
  RegisterReport('TimeExportNew', TTimeExportDM);

end.

