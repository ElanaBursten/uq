unit HighLevelProductivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppBands, ppClass, ppCtrls, ppVar,
  ppStrtch, ppRegion, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, DB,
  ppDesignLayer, ppParameter,  Data.Win.ADODB;

type
  THighLevelProductivityDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ShortNameDBText: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText8: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppShape2: TppShape;
    ppLabel9: TppLabel;
    ppLine1: TppLine;
    ppDBText10: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc9: TppDBCalc;
    ppRegion1: TppRegion;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    Detail_tph_working: TppDBText;
    ppLabel5: TppLabel;
    ppDBText1: TppDBText;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLine: TppLine;
    ppLine2: TppLine;
    PCGroup: TppGroup;
    PCGroupHeaderBand: TppGroupHeaderBand;
    PCGroupFooterBand: TppGroupFooterBand;
    ppLabel18: TppLabel;
    ppLine4: TppLine;
    ppDBCalc5: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBText2: TppDBText;
    ProfitCenterLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdMiscUtils, OdRbHierarchy;

{ THighLevelProductivityDM }

procedure THighLevelProductivityDM.Configure(QueryFields: TStrings);
var
  DateFrom, DateTo: TDateTime;
  ProfitCenters: string;
begin
  inherited;

  DateFrom := GetDateTime('DateFrom');
  DateTo := GetDateTime('DateTo');
  ProfitCenters := SafeGetString('ProfitCenters');
  SP.Parameters.ParamByName('@DateFrom').value      := DateFrom;
  SP.Parameters.ParamByName('@DateTo').value        := DateTo;
  SP.Parameters.ParamByName('@ProfitCenters').value := ProfitCenters;
  SP.Open;

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  if IsEmpty(ProfitCenters) then
    ShowText(ProfitCenterLabel, 'Profit Centers:', 'All')
  else
    ShowText(ProfitCenterLabel, 'Profit Centers:', 'Limited');
  PCGroupFooterBand.Visible := DateTo - DateFrom > 1;
end;

initialization
  RegisterReport('HighLevelProductivity', THighLevelProductivityDM);

end.


