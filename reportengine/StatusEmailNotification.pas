unit StatusEmailNotification;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppReport, ppSubRpt, ppRegion,
  ppBands, ppVar, ppStrtch, ppMemo, ppClass, myChkBox, ppCtrls, ppPrnabl,
  ppCache, ppComm, ppRelatv, ppProd, DB, ppModule, daDataModule,
  ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TEmailStatusDM = class(TBaseReportDM)
    EmailDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    FromDateLabel: TppLabel;
    ppLabel12: TppLabel;
    ToDateLabel: TppLabel;
    ppLabel14: TppLabel;
    CallCenterLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    EmailPipe: TppDBPipeline;
    ppSummaryBand1: TppSummaryBand;
    SummarySubRep: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppLabel6: TppLabel;
    ppDBText5: TppDBText;
    ppLabel7: TppLabel;
    ppDBText6: TppDBText;
    ppLabel8: TppLabel;
    ppSummaryBand2: TppSummaryBand;
    daDataModule1: TdaDataModule;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppDBMemo1: TppDBMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Emails: TADOQuery;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu;

{$R *.dfm}

{ TEmailStatusDM }

procedure TEmailStatusDM.Configure(QueryFields: TStrings);
begin
  inherited;

  CallCenterLabel.Caption := GetString('CallCenter');
  FromDateLabel.Caption := DateTimeToStr(GetDateTime('DateFrom'));
  ToDateLabel.Caption := DateTimeToStr(GetDateTime('DateTo'));

  Emails.Parameters.ParamByName('CallCenter').Value := GetString('CallCenter');
  Emails.Parameters.ParamByName('DateFrom').Value := GetDateTime('DateFrom');
  Emails.Parameters.ParamByName('DateTo').Value := GetDateTime('DateTo');
  Emails.Open;
end;

initialization
  RegisterReport('EmailStatus', TEmailStatusDM);

{
Report=EmailStatus
CallCenter=*
DateFrom=2009-01-01
DateTo=2010-01-01
}

end.

