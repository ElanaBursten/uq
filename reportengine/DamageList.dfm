inherited DamageListDM: TDamageListDM
  OldCreateOrder = True
  Height = 268
  Width = 400
  object SPDS: TDataSource
    DataSet = Damages
    Left = 120
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Damage / Investigation List'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 10160
    PrinterSetup.mmMarginLeft = 12700
    PrinterSetup.mmMarginRight = 12700
    PrinterSetup.mmMarginTop = 10160
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowAutoSearchDialog = True
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 272
    Top = 24
    Version = '18.03'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppTitleBand3: TppTitleBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 32808
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 32808
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 1
        LayerName = Foreground
      end
      object TitleLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'TitleLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Damage / Investigation List'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 4763
        mmTop = 2117
        mmWidth = 247650
        BandType = 1
        LayerName = Foreground
      end
      object AttachmentsLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'AttachmentsLabel'
        HyperlinkEnabled = False
        Caption = 'AttachmentsLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 187590
        mmTop = 24871
        mmWidth = 26723
        BandType = 1
        LayerName = Foreground
      end
      object DamageDateRange: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DamageDateRange'
        HyperlinkEnabled = False
        Caption = 'Damaged:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 8202
        mmTop = 8731
        mmWidth = 12700
        BandType = 1
        LayerName = Foreground
      end
      object DamagedCompanyLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DamagedCompanyLabel'
        HyperlinkEnabled = False
        Caption = 'Utility Co. Damaged:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 92869
        mmTop = 28840
        mmWidth = 25135
        BandType = 1
        LayerName = Foreground
      end
      object DueDateRange: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DueDateRange'
        HyperlinkEnabled = False
        Caption = 'Due:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 15081
        mmTop = 12965
        mmWidth = 5821
        BandType = 1
        LayerName = Foreground
      end
      object ExcavatorLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ExcavatorLabel'
        HyperlinkEnabled = False
        Caption = 'Excavator:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 104247
        mmTop = 20902
        mmWidth = 13758
        BandType = 1
        LayerName = Foreground
      end
      object EstimateLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EstimateLabel'
        HyperlinkEnabled = False
        Caption = 'EstimateLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 187590
        mmTop = 8731
        mmWidth = 17463
        BandType = 1
        LayerName = Foreground
      end
      object InvestigationStatusLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'InvestigationStatusLabel'
        HyperlinkEnabled = False
        Caption = 'Invest. Status:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 99749
        mmTop = 16933
        mmWidth = 18256
        BandType = 1
        LayerName = Foreground
      end
      object InvLocLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'InvLocLabel'
        HyperlinkEnabled = False
        Caption = 'Investigator:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 102394
        mmTop = 12965
        mmWidth = 15610
        BandType = 1
        LayerName = Foreground
      end
      object InvoiceCodesLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'InvoiceCodesLabel'
        HyperlinkEnabled = False
        Caption = 'Inv. Codes:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 187590
        mmTop = 12700
        mmWidth = 14288
        BandType = 1
        LayerName = Foreground
      end
      object InvoicesLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'InvoicesLabel'
        HyperlinkEnabled = False
        Caption = 'InvoicesLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 187590
        mmTop = 28840
        mmWidth = 20108
        BandType = 1
        LayerName = Foreground
      end
      object LocationLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'LocationLabel'
        HyperlinkEnabled = False
        Caption = 'Location:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 9525
        mmTop = 28840
        mmWidth = 11642
        BandType = 1
        LayerName = Foreground
      end
      object ManagerLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ManagerLabel'
        HyperlinkEnabled = False
        Caption = 'Manager:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 106099
        mmTop = 8731
        mmWidth = 11906
        BandType = 1
        LayerName = Foreground
      end
      object NotifyDateRange: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'NotifyDateRange'
        HyperlinkEnabled = False
        Caption = 'Notified:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 10583
        mmTop = 16933
        mmWidth = 10319
        BandType = 1
        LayerName = Foreground
      end
      object ProfitCenterLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label16'
        HyperlinkEnabled = False
        Caption = 'Profit Center:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 4498
        mmTop = 20902
        mmWidth = 16669
        BandType = 1
        LayerName = Foreground
      end
      object ResponsabilitiesLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ResponsabilitiesLabel'
        HyperlinkEnabled = False
        Caption = 'Resp:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 13758
        mmTop = 24871
        mmWidth = 7408
        BandType = 1
        LayerName = Foreground
      end
      object WorkTobeDoneLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'WorkTobeDoneLabel'
        HyperlinkEnabled = False
        Caption = 'Work to be done:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 96309
        mmTop = 24871
        mmWidth = 21696
        BandType = 1
        LayerName = Foreground
      end
      object InvCodesMemo: TppMemo
        DesignLayer = ppDesignLayer1
        UserName = 'InvCodesMemo'
        CharWrap = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        RemoveEmptyLines = False
        Transparent = True
        mmHeight = 11642
        mmLeft = 187590
        mmTop = 12700
        mmWidth = 65617
        BandType = 1
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeading = 0
      end
    end
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 16669
      mmPrintPosition = 0
      object ppLine1: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line1'
        ParentHeight = True
        ParentWidth = True
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 16669
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Damage ID'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 0
        mmTop = 265
        mmWidth = 14817
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label2'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 89165
        mmTop = 3969
        mmWidth = 20638
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel3: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label3'
        HyperlinkEnabled = False
        Caption = 'Damage Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 89165
        mmTop = 265
        mmWidth = 18256
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel11: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label11'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Size'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 57415
        mmTop = 3969
        mmWidth = 22225
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel14: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label14'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Facility Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 57415
        mmTop = 265
        mmWidth = 22225
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel15: TppLabel
        Tag = 5
        DesignLayer = ppDesignLayer1
        UserName = 'Label15'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Profit Center'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        WordWrap = True
        mmHeight = 7673
        mmLeft = 173567
        mmTop = 265
        mmWidth = 11377
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label5'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Address'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 0
        mmTop = 3969
        mmWidth = 14817
        BandType = 0
        LayerName = Foreground
      end
      object EstCostLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EstCostLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Estimated Cost'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 7938
        mmLeft = 186002
        mmTop = 265
        mmWidth = 16669
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel17: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label17'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Paid Amount'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 7673
        mmLeft = 202407
        mmTop = 265
        mmWidth = 16669
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel18: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label18'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Remaining Exposure'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 8996
        mmLeft = 218811
        mmTop = 265
        mmWidth = 17198
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel19: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label19'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Accrual Amount'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 8996
        mmLeft = 235744
        mmTop = 265
        mmWidth = 15346
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel12: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label12'
        HyperlinkEnabled = False
        Caption = 'Utility Company Damaged'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 18521
        mmTop = 265
        mmWidth = 36513
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label4'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Materials'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 57415
        mmTop = 7673
        mmWidth = 22225
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label6'
        HyperlinkEnabled = False
        Caption = '%COMPANY% Responsibility'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 123825
        mmTop = 265
        mmWidth = 41804
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label7'
        HyperlinkEnabled = False
        Caption = 'Invoice Code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 89165
        mmTop = 7673
        mmWidth = 17727
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel8: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label8'
        HyperlinkEnabled = False
        Caption = 'Invoice #, Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 89165
        mmTop = 11377
        mmWidth = 20108
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel20: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label20'
        HyperlinkEnabled = False
        Caption = 'Excavator Resp.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 123825
        mmTop = 3969
        mmWidth = 21167
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel21: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label21'
        HyperlinkEnabled = False
        Caption = 'Utility / Special Resp.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 123825
        mmTop = 7673
        mmWidth = 29633
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label10'
        HyperlinkEnabled = False
        Caption = 'Investigator'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 173566
        mmTop = 11377
        mmWidth = 16404
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel16: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label101'
        HyperlinkEnabled = False
        Caption = 'Office'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 220134
        mmTop = 11377
        mmWidth = 8731
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel38: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label102'
        HyperlinkEnabled = False
        Caption = 'Locator'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 173566
        mmTop = 7408
        mmWidth = 10319
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 14552
      mmPrintPosition = 0
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        HyperlinkEnabled = False
        DataField = 'uq_damage_id'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 2910
        mmLeft = 0
        mmTop = 265
        mmWidth = 14817
        BandType = 4
        LayerName = Foreground
      end
      object DamageTypeDBText: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DamageTypeDBText'
        HyperlinkEnabled = False
        DataField = 'damage_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 89165
        mmTop = 3969
        mmWidth = 34130
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText3: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText3'
        HyperlinkEnabled = False
        DataField = 'damage_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 89165
        mmTop = 265
        mmWidth = 18256
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText7: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText7'
        HyperlinkEnabled = False
        DataField = 'size_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 57415
        mmTop = 11377
        mmWidth = 30956
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText9: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText9'
        HyperlinkEnabled = False
        DataField = 'state'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 34396
        mmTop = 7673
        mmWidth = 7408
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText10: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText10'
        HyperlinkEnabled = False
        DataField = 'facility_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 57415
        mmTop = 265
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText11: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText11'
        HyperlinkEnabled = False
        DataField = 'facility_size'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 57415
        mmTop = 3969
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText12: TppDBText
        Tag = 105
        DesignLayer = ppDesignLayer1
        UserName = 'DBText12'
        HyperlinkEnabled = False
        DataField = 'profit_center'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 173567
        mmTop = 265
        mmWidth = 11377
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText5: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText5'
        HyperlinkEnabled = False
        DataField = 'utility_co_damaged'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 18521
        mmTop = 265
        mmWidth = 37042
        BandType = 4
        LayerName = Foreground
      end
      object ppEstCost: TppDBText
        Tag = 1
        DesignLayer = ppDesignLayer1
        UserName = 'EstCost'
        HyperlinkEnabled = False
        DataField = 'est_cost'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 186002
        mmTop = 265
        mmWidth = 16002
        BandType = 4
        LayerName = Foreground
      end
      object ppPaidAmount: TppDBText
        Tag = 2
        DesignLayer = ppDesignLayer1
        UserName = 'PaidAmount'
        HyperlinkEnabled = False
        DataField = 'paid_amount'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 202407
        mmTop = 265
        mmWidth = 16002
        BandType = 4
        LayerName = Foreground
      end
      object LabRemaining: TppDBText
        Tag = 3
        DesignLayer = ppDesignLayer1
        UserName = 'LabRemaining'
        HyperlinkEnabled = False
        DataField = 'remaining'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 218811
        mmTop = 265
        mmWidth = 16002
        BandType = 4
        LayerName = Foreground
      end
      object ppTotCost: TppDBText
        Tag = 4
        DesignLayer = ppDesignLayer1
        UserName = 'TotCost'
        HyperlinkEnabled = False
        DataField = 'accrual_amount'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 235215
        mmTop = 265
        mmWidth = 16002
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText17: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText17'
        HyperlinkEnabled = False
        DataField = 'invoice_code_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 89165
        mmTop = 7673
        mmWidth = 34131
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText4: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText4'
        HyperlinkEnabled = False
        DataField = 'facility_material'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 57415
        mmTop = 7673
        mmWidth = 30956
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText6: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText6'
        HyperlinkEnabled = False
        DataField = 'location'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 0
        mmTop = 3969
        mmWidth = 55563
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText8: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText8'
        HyperlinkEnabled = False
        DataField = 'city'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 0
        mmTop = 7673
        mmWidth = 33338
        BandType = 4
        LayerName = Foreground
      end
      object ppLine2: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line2'
        ParentHeight = True
        ParentWidth = True
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 14552
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText18: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText18'
        HyperlinkEnabled = False
        DataField = 'invoice_num'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 89165
        mmTop = 11377
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText19: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText19'
        HyperlinkEnabled = False
        DataField = 'invoice_date'
        DataPipeline = Pipe
        DisplayFormat = 'm/d/yy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 117740
        mmTop = 11377
        mmWidth = 13494
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText29: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText201'
        HyperlinkEnabled = False
        DataField = 'uq_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 123825
        mmTop = 265
        mmWidth = 48419
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText30: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText30'
        HyperlinkEnabled = False
        DataField = 'exc_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 123825
        mmTop = 3969
        mmWidth = 48419
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText31: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText31'
        HyperlinkEnabled = False
        DataField = 'spc_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 123825
        mmTop = 7673
        mmWidth = 48419
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText13: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText13'
        HyperlinkEnabled = False
        DataField = 'short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 173567
        mmTop = 11377
        mmWidth = 34396
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText15: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText15'
        HyperlinkEnabled = False
        DataField = 'office_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 218811
        mmTop = 11377
        mmWidth = 32808
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText16: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText16'
        HyperlinkEnabled = False
        DataField = 'locator_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3175
        mmLeft = 173567
        mmTop = 7673
        mmWidth = 34396
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 12700
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 12700
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 8
        LayerName = Foreground
      end
      object TitleLabel2: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'TitleLabel2'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Damage / Investigation List'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 64294
        mmTop = 4233
        mmWidth = 125148
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 218017
        mmTop = 1852
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 228865
        mmTop = 6350
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label13'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = '%COMPANY%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 3175
        mmTop = 2117
        mmWidth = 50006
        BandType = 8
        LayerName = Foreground
      end
    end
    object SummaryBand: TppSummaryBand
      BeforePrint = SummaryBandBeforePrint
      Visible = False
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 14552
      mmPrintPosition = 0
      object RFEstCost: TppDBCalc
        Tag = 101
        DesignLayer = ppDesignLayer1
        UserName = 'RFEstCost'
        HyperlinkEnabled = False
        DataField = 'est_cost'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3440
        mmLeft = 196057
        mmTop = 1852
        mmWidth = 14288
        BandType = 7
        LayerName = Foreground
      end
      object ppDBCalc7: TppDBCalc
        Tag = 102
        DesignLayer = ppDesignLayer1
        UserName = 'DBCalc7'
        HyperlinkEnabled = False
        DataField = 'paid_amount'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3440
        mmLeft = 210609
        mmTop = 1852
        mmWidth = 14288
        BandType = 7
        LayerName = Foreground
      end
      object RFTotCost: TppDBCalc
        Tag = 104
        DesignLayer = ppDesignLayer1
        UserName = 'RFTotCost'
        HyperlinkEnabled = False
        DataField = 'accrual_amount'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3440
        mmLeft = 239713
        mmTop = 1852
        mmWidth = 14288
        BandType = 7
        LayerName = Foreground
      end
      object RFRemaining: TppDBCalc
        Tag = 103
        DesignLayer = ppDesignLayer1
        UserName = 'RFRemaining'
        HyperlinkEnabled = False
        DataField = 'remaining'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3440
        mmLeft = 225161
        mmTop = 1852
        mmWidth = 14288
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel29: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label29'
        HyperlinkEnabled = False
        Caption = 'Totals:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Times New Roman'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3440
        mmLeft = 170392
        mmTop = 1852
        mmWidth = 9260
        BandType = 7
        LayerName = Foreground
      end
      object InitialTotalsSubRep: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'InitialTotalsSubRep'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'TotalsPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 8467
        mmWidth = 254001
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = TotalsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Damage / Investigation List'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 10160
          PrinterSetup.mmMarginLeft = 12700
          PrinterSetup.mmMarginRight = 12700
          PrinterSetup.mmMarginTop = 10160
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '18.03'
          mmColumnWidth = 0
          DataPipelineName = 'TotalsPipe'
          object ppTitleBand2: TppTitleBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 8467
            mmPrintPosition = 0
            object ppLabel30: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label23'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Ratio'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsUnderline]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 191559
              mmTop = 3440
              mmWidth = 9790
              BandType = 1
              LayerName = Foreground1
            end
            object ppLabel31: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label24'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Remaining Exposure'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsUnderline]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 164571
              mmTop = 3704
              mmWidth = 25400
              BandType = 1
              LayerName = Foreground1
            end
            object ppLabel32: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label25'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Accrual Total'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsUnderline]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 204788
              mmTop = 3440
              mmWidth = 19844
              BandType = 1
              LayerName = Foreground1
            end
            object ppLabel33: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label26'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = '#'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsUnderline]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 155311
              mmTop = 3440
              mmWidth = 7673
              BandType = 1
              LayerName = Foreground1
            end
            object ppLabel34: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label27'
              HyperlinkEnabled = False
              AutoSize = False
              Caption = 'Invoice Code'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsUnderline]
              Transparent = True
              mmHeight = 3704
              mmLeft = 114300
              mmTop = 3440
              mmWidth = 39158
              BandType = 1
              LayerName = Foreground1
            end
            object ppLabel35: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label28'
              HyperlinkEnabled = False
              Caption = 'Estimated Initial Claims Liability - Totals'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 0
              mmTop = 3175
              mmWidth = 57415
              BandType = 1
              LayerName = Foreground1
            end
          end
          object ppDetailBand3: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ppDBText21: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText24'
              HyperlinkEnabled = False
              DataField = 'ratio_percent'
              DataPipeline = TotalsPipe
              DisplayFormat = '0 %'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3440
              mmLeft = 191559
              mmTop = 1323
              mmWidth = 9790
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText22: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText25'
              HyperlinkEnabled = False
              DataField = 'remaining_expo'
              DataPipeline = TotalsPipe
              DisplayFormat = '#,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3704
              mmLeft = 170127
              mmTop = 1323
              mmWidth = 19844
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText23: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText26'
              HyperlinkEnabled = False
              DataField = 'total_accrual_amount'
              DataPipeline = TotalsPipe
              DisplayFormat = '#,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3704
              mmLeft = 204788
              mmTop = 1323
              mmWidth = 19844
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText32: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText27'
              HyperlinkEnabled = False
              DataField = 'N'
              DataPipeline = TotalsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3704
              mmLeft = 155311
              mmTop = 1323
              mmWidth = 7673
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText33: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText28'
              HyperlinkEnabled = False
              DataField = 'description'
              DataPipeline = TotalsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3440
              mmLeft = 114300
              mmTop = 1323
              mmWidth = 39158
              BandType = 4
              LayerName = Foreground1
            end
          end
          object ppSummaryBand3: TppSummaryBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 8467
            mmPrintPosition = 0
            object ppDBCalc10: TppDBCalc
              DesignLayer = ppDesignLayer2
              UserName = 'DBCalc5'
              HyperlinkEnabled = False
              DataField = 'total_accrual_amount'
              DataPipeline = TotalsPipe
              DisplayFormat = '#,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TotalsPipe'
              mmHeight = 3440
              mmLeft = 204523
              mmTop = 2646
              mmWidth = 20373
              BandType = 7
              LayerName = Foreground1
            end
            object ppLabel36: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label22'
              HyperlinkEnabled = False
              Caption = 'Total Estimated Liability'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Times New Roman'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 3440
              mmLeft = 127000
              mmTop = 2646
              mmWidth = 33867
              BandType = 7
              LayerName = Foreground1
            end
            object ppLine5: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line3'
              Weight = 0.750000000000000000
              mmHeight = 1588
              mmLeft = 127000
              mmTop = 1323
              mmWidth = 98690
              BandType = 7
              LayerName = Foreground1
            end
            object ppLine6: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line4'
              Weight = 0.750000000000000000
              mmHeight = 1588
              mmLeft = 127000
              mmTop = 6615
              mmWidth = 98690
              BandType = 7
              LayerName = Foreground1
            end
          end
          object ppDesignLayers2: TppDesignLayers
            object ppDesignLayer2: TppDesignLayer
              UserName = 'Foreground1'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppGroup3: TppGroup
      BreakName = 'profit_center'
      DataPipeline = Pipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      NewPage = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group3'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'Pipe'
      NewFile = False
      object PCHeaderBand: TppGroupHeaderBand
        Visible = False
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 9260
        mmPrintPosition = 0
        object ppDBText20: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText20'
          HyperlinkEnabled = False
          DataField = 'profit_center'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 16
          Font.Style = [fsBold]
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 6615
          mmLeft = 28310
          mmTop = 1058
          mmWidth = 39688
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel37: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label37'
          HyperlinkEnabled = False
          Caption = 'Profit Center:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 12
          Font.Style = [fsBold]
          Transparent = True
          mmHeight = 5292
          mmLeft = 0
          mmTop = 1588
          mmWidth = 24342
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine7: TppLine
          DesignLayer = ppDesignLayer1
          UserName = 'Line7'
          ParentHeight = True
          ParentWidth = True
          Position = lpBottom
          Weight = 0.750000000000000000
          mmHeight = 9260
          mmLeft = 0
          mmTop = 0
          mmWidth = 254001
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
      end
      object PCFooterBand: TppGroupFooterBand
        BeforePrint = PCFooterBandBeforePrint
        Visible = False
        Background.Brush.Style = bsClear
        PrintHeight = phDynamic
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 10848
        mmPrintPosition = 0
        object GFEstCost: TppDBCalc
          Tag = 101
          DesignLayer = ppDesignLayer1
          UserName = 'GFEstCost'
          HyperlinkEnabled = False
          DataField = 'est_cost'
          DataPipeline = Pipe
          DisplayFormat = '#,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 8
          Font.Style = [fsBold]
          ResetGroup = ppGroup3
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3440
          mmLeft = 194469
          mmTop = 794
          mmWidth = 14288
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBCalc2: TppDBCalc
          Tag = 102
          DesignLayer = ppDesignLayer1
          UserName = 'DBCalc2'
          HyperlinkEnabled = False
          DataField = 'paid_amount'
          DataPipeline = Pipe
          DisplayFormat = '#,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 8
          Font.Style = [fsBold]
          ResetGroup = ppGroup3
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3440
          mmLeft = 209021
          mmTop = 794
          mmWidth = 14288
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object GFTotCost: TppDBCalc
          Tag = 104
          DesignLayer = ppDesignLayer1
          UserName = 'GFTotCost'
          HyperlinkEnabled = False
          DataField = 'accrual_amount'
          DataPipeline = Pipe
          DisplayFormat = '#,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 8
          Font.Style = [fsBold]
          ResetGroup = ppGroup3
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3440
          mmLeft = 238125
          mmTop = 794
          mmWidth = 14288
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object GFRemaining: TppDBCalc
          Tag = 103
          DesignLayer = ppDesignLayer1
          UserName = 'GFRemaining'
          HyperlinkEnabled = False
          DataField = 'remaining'
          DataPipeline = Pipe
          DisplayFormat = '#,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 8
          Font.Style = [fsBold]
          ResetGroup = ppGroup3
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3440
          mmLeft = 223573
          mmTop = 794
          mmWidth = 14288
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel9: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label9'
          HyperlinkEnabled = False
          Caption = 'Totals:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Times New Roman'
          Font.Size = 8
          Font.Style = [fsBold]
          Transparent = True
          mmHeight = 3440
          mmLeft = 170392
          mmTop = 794
          mmWidth = 9260
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object PCInitialTotalSubRep: TppSubReport
          DesignLayer = ppDesignLayer1
          UserName = 'PCInitialTotalSubRep'
          ExpandAll = False
          NewPrintJob = False
          OutlineSettings.CreateNode = True
          TraverseAllData = False
          DataPipelineName = 'TotalsPipe'
          mmHeight = 5027
          mmLeft = 0
          mmTop = 5556
          mmWidth = 254001
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          object ppChildReport1: TppChildReport
            AutoStop = False
            DataPipeline = TotalsPipe
            PrinterSetup.BinName = 'Default'
            PrinterSetup.DocumentName = '%COMPANY% Damage / Investigation List'
            PrinterSetup.Duplex = dpNone
            PrinterSetup.Orientation = poLandscape
            PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
            PrinterSetup.PrinterName = 'Screen'
            PrinterSetup.SaveDeviceSettings = False
            PrinterSetup.mmMarginBottom = 10160
            PrinterSetup.mmMarginLeft = 12700
            PrinterSetup.mmMarginRight = 12700
            PrinterSetup.mmMarginTop = 10160
            PrinterSetup.mmPaperHeight = 215900
            PrinterSetup.mmPaperWidth = 279401
            PrinterSetup.PaperSize = 1
            Version = '18.03'
            mmColumnWidth = 0
            DataPipelineName = 'TotalsPipe'
            object ppTitleBand1: TppTitleBand
              Background.Brush.Style = bsClear
              mmBottomOffset = 0
              mmHeight = 8467
              mmPrintPosition = 0
              object ppLabel23: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label23'
                HyperlinkEnabled = False
                AutoSize = False
                Caption = 'Ratio'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsUnderline]
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 192882
                mmTop = 3440
                mmWidth = 9790
                BandType = 1
                LayerName = Foreground2
              end
              object ppLabel24: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label24'
                HyperlinkEnabled = False
                AutoSize = False
                Caption = 'Remaining Exposure'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsUnderline]
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 165365
                mmTop = 3440
                mmWidth = 25665
                BandType = 1
                LayerName = Foreground2
              end
              object ppLabel25: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label25'
                HyperlinkEnabled = False
                AutoSize = False
                Caption = 'Accrual Total'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsUnderline]
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 206111
                mmTop = 3440
                mmWidth = 19844
                BandType = 1
                LayerName = Foreground2
              end
              object ppLabel26: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label26'
                HyperlinkEnabled = False
                AutoSize = False
                Caption = '#'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsUnderline]
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 156104
                mmTop = 3440
                mmWidth = 7673
                BandType = 1
                LayerName = Foreground2
              end
              object ppLabel27: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label27'
                HyperlinkEnabled = False
                AutoSize = False
                Caption = 'Invoice Code'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsUnderline]
                Transparent = True
                mmHeight = 3704
                mmLeft = 114300
                mmTop = 3440
                mmWidth = 38894
                BandType = 1
                LayerName = Foreground2
              end
              object ppLabel28: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label28'
                HyperlinkEnabled = False
                Caption = 'Estimated Initial Claims Liability'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 3440
                mmLeft = 0
                mmTop = 3175
                mmWidth = 46567
                BandType = 1
                LayerName = Foreground2
              end
              object ppDBText34: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText34'
                HyperlinkEnabled = False
                DataField = 'profit_center'
                DataPipeline = TotalsPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 10
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 4233
                mmLeft = 56886
                mmTop = 3175
                mmWidth = 17198
                BandType = 1
                LayerName = Foreground2
              end
            end
            object ppDetailBand2: TppDetailBand
              Background1.Brush.Style = bsClear
              Background2.Brush.Style = bsClear
              mmBottomOffset = 0
              mmHeight = 5027
              mmPrintPosition = 0
              object ppDBText24: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText24'
                HyperlinkEnabled = False
                DataField = 'ratio_percent'
                DataPipeline = TotalsPipe
                DisplayFormat = '0 %'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsBold]
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3440
                mmLeft = 192882
                mmTop = 1323
                mmWidth = 9790
                BandType = 4
                LayerName = Foreground2
              end
              object ppDBText25: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText25'
                HyperlinkEnabled = False
                DataField = 'remaining_expo'
                DataPipeline = TotalsPipe
                DisplayFormat = '#,##0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3704
                mmLeft = 171450
                mmTop = 1323
                mmWidth = 19844
                BandType = 4
                LayerName = Foreground2
              end
              object ppDBText26: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText26'
                HyperlinkEnabled = False
                DataField = 'total_accrual_amount'
                DataPipeline = TotalsPipe
                DisplayFormat = '#,##0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3704
                mmLeft = 206111
                mmTop = 1323
                mmWidth = 19844
                BandType = 4
                LayerName = Foreground2
              end
              object ppDBText27: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText27'
                HyperlinkEnabled = False
                DataField = 'N'
                DataPipeline = TotalsPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3704
                mmLeft = 156104
                mmTop = 1323
                mmWidth = 7673
                BandType = 4
                LayerName = Foreground2
              end
              object ppDBText28: TppDBText
                DesignLayer = ppDesignLayer3
                UserName = 'DBText28'
                HyperlinkEnabled = False
                DataField = 'description'
                DataPipeline = TotalsPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3440
                mmLeft = 114300
                mmTop = 1323
                mmWidth = 38894
                BandType = 4
                LayerName = Foreground2
              end
            end
            object ppSummaryBand2: TppSummaryBand
              Background.Brush.Style = bsClear
              mmBottomOffset = 0
              mmHeight = 8467
              mmPrintPosition = 0
              object ppDBCalc5: TppDBCalc
                DesignLayer = ppDesignLayer3
                UserName = 'DBCalc5'
                HyperlinkEnabled = False
                DataField = 'total_accrual_amount'
                DataPipeline = TotalsPipe
                DisplayFormat = '#,##0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsBold]
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'TotalsPipe'
                mmHeight = 3440
                mmLeft = 204523
                mmTop = 2646
                mmWidth = 20373
                BandType = 7
                LayerName = Foreground2
              end
              object ppLabel22: TppLabel
                DesignLayer = ppDesignLayer3
                UserName = 'Label22'
                HyperlinkEnabled = False
                Caption = 'Total Estimated Liability'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Times New Roman'
                Font.Size = 8
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 3440
                mmLeft = 127000
                mmTop = 2646
                mmWidth = 33867
                BandType = 7
                LayerName = Foreground2
              end
              object ppLine3: TppLine
                DesignLayer = ppDesignLayer3
                UserName = 'Line3'
                Weight = 0.750000000000000000
                mmHeight = 1588
                mmLeft = 127000
                mmTop = 1323
                mmWidth = 98690
                BandType = 7
                LayerName = Foreground2
              end
              object ppLine4: TppLine
                DesignLayer = ppDesignLayer3
                UserName = 'Line4'
                Weight = 0.750000000000000000
                mmHeight = 1588
                mmLeft = 127000
                mmTop = 6615
                mmWidth = 98690
                BandType = 7
                LayerName = Foreground2
              end
            end
            object ppDesignLayers3: TppDesignLayers
              object ppDesignLayer3: TppDesignLayer
                UserName = 'Foreground2'
                LayerType = ltBanded
                Index = 0
              end
            end
          end
        end
      end
    end
    object ppGroup1: TppGroup
      BreakName = 'uq_damage_id'
      DataPipeline = Pipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      KeepTogether = True
      OutlineSettings.CreateNode = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'Pipe'
      NewFile = False
      object ppGroupHeaderBand1: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Background.Brush.Style = bsClear
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object Pipe: TppDBPipeline
    DataSource = SPDS
    OpenDataSource = False
    UserName = 'Pipe'
    Left = 208
    Top = 24
    object PipeppField1: TppField
      FieldAlias = 'damage_id'
      FieldName = 'damage_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object PipeppField2: TppField
      FieldAlias = 'office_id'
      FieldName = 'office_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object PipeppField3: TppField
      FieldAlias = 'damage_inv_num'
      FieldName = 'damage_inv_num'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object PipeppField4: TppField
      FieldAlias = 'damage_type'
      FieldName = 'damage_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object PipeppField5: TppField
      FieldAlias = 'damage_date'
      FieldName = 'damage_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object PipeppField6: TppField
      FieldAlias = 'uq_notified_date'
      FieldName = 'uq_notified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object PipeppField7: TppField
      FieldAlias = 'notified_by_person'
      FieldName = 'notified_by_person'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object PipeppField8: TppField
      FieldAlias = 'notified_by_company'
      FieldName = 'notified_by_company'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object PipeppField9: TppField
      FieldAlias = 'notified_by_phone'
      FieldName = 'notified_by_phone'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object PipeppField10: TppField
      FieldAlias = 'client_code'
      FieldName = 'client_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object PipeppField11: TppField
      FieldAlias = 'client_id'
      FieldName = 'client_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object PipeppField12: TppField
      FieldAlias = 'client_claim_id'
      FieldName = 'client_claim_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object PipeppField13: TppField
      FieldAlias = 'date_mailed'
      FieldName = 'date_mailed'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object PipeppField14: TppField
      FieldAlias = 'date_faxed'
      FieldName = 'date_faxed'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object PipeppField15: TppField
      FieldAlias = 'sent_to'
      FieldName = 'sent_to'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object PipeppField16: TppField
      FieldAlias = 'size_type'
      FieldName = 'size_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 15
      Searchable = False
      Sortable = False
    end
    object PipeppField17: TppField
      FieldAlias = 'location'
      FieldName = 'location'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 16
      Searchable = False
      Sortable = False
    end
    object PipeppField18: TppField
      FieldAlias = 'page'
      FieldName = 'page'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 17
      Searchable = False
      Sortable = False
    end
    object PipeppField19: TppField
      FieldAlias = 'grid'
      FieldName = 'grid'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 18
      Searchable = False
      Sortable = False
    end
    object PipeppField20: TppField
      FieldAlias = 'city'
      FieldName = 'city'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 19
      Searchable = False
      Sortable = False
    end
    object PipeppField21: TppField
      FieldAlias = 'county'
      FieldName = 'county'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 20
      Searchable = False
      Sortable = False
    end
    object PipeppField22: TppField
      FieldAlias = 'state'
      FieldName = 'state'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 21
      Searchable = False
      Sortable = False
    end
    object PipeppField23: TppField
      FieldAlias = 'utility_co_damaged'
      FieldName = 'utility_co_damaged'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 22
      Searchable = False
      Sortable = False
    end
    object PipeppField24: TppField
      FieldAlias = 'diagram_number'
      FieldName = 'diagram_number'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 23
      Searchable = False
      Sortable = False
    end
    object PipeppField25: TppField
      FieldAlias = 'remarks'
      FieldName = 'remarks'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 24
      Searchable = False
      Sortable = False
    end
    object PipeppField26: TppField
      FieldAlias = 'investigator_id'
      FieldName = 'investigator_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 25
      Searchable = False
      Sortable = False
    end
    object PipeppField27: TppField
      FieldAlias = 'investigator_arrival'
      FieldName = 'investigator_arrival'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 26
      Searchable = False
      Sortable = False
    end
    object PipeppField28: TppField
      FieldAlias = 'investigator_departure'
      FieldName = 'investigator_departure'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 27
      Searchable = False
      Sortable = False
    end
    object PipeppField29: TppField
      FieldAlias = 'investigator_est_damage_time'
      FieldName = 'investigator_est_damage_time'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 28
      Searchable = False
      Sortable = False
    end
    object PipeppField30: TppField
      FieldAlias = 'investigator_narrative'
      FieldName = 'investigator_narrative'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 29
      Searchable = False
      Sortable = False
    end
    object PipeppField31: TppField
      FieldAlias = 'excavator_company'
      FieldName = 'excavator_company'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 30
      Searchable = False
      Sortable = False
    end
    object PipeppField32: TppField
      FieldAlias = 'excavator_type'
      FieldName = 'excavator_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 31
      Searchable = False
      Sortable = False
    end
    object PipeppField33: TppField
      FieldAlias = 'excavation_type'
      FieldName = 'excavation_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 32
      Searchable = False
      Sortable = False
    end
    object PipeppField34: TppField
      FieldAlias = 'locate_marks_present'
      FieldName = 'locate_marks_present'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 33
      Searchable = False
      Sortable = False
    end
    object PipeppField35: TppField
      FieldAlias = 'locate_requested'
      FieldName = 'locate_requested'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 34
      Searchable = False
      Sortable = False
    end
    object PipeppField36: TppField
      FieldAlias = 'ticket_id'
      FieldName = 'ticket_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 35
      Searchable = False
      Sortable = False
    end
    object PipeppField37: TppField
      FieldAlias = 'site_mark_state'
      FieldName = 'site_mark_state'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 36
      Searchable = False
      Sortable = False
    end
    object PipeppField38: TppField
      FieldAlias = 'site_sewer_marked'
      FieldName = 'site_sewer_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 37
      Searchable = False
      Sortable = False
    end
    object PipeppField39: TppField
      FieldAlias = 'site_water_marked'
      FieldName = 'site_water_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 38
      Searchable = False
      Sortable = False
    end
    object PipeppField40: TppField
      FieldAlias = 'site_catv_marked'
      FieldName = 'site_catv_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 39
      Searchable = False
      Sortable = False
    end
    object PipeppField41: TppField
      FieldAlias = 'site_gas_marked'
      FieldName = 'site_gas_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 40
      Searchable = False
      Sortable = False
    end
    object PipeppField42: TppField
      FieldAlias = 'site_power_marked'
      FieldName = 'site_power_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 41
      Searchable = False
      Sortable = False
    end
    object PipeppField43: TppField
      FieldAlias = 'site_tel_marked'
      FieldName = 'site_tel_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 42
      Searchable = False
      Sortable = False
    end
    object PipeppField44: TppField
      FieldAlias = 'site_other_marked'
      FieldName = 'site_other_marked'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 43
      Searchable = False
      Sortable = False
    end
    object PipeppField45: TppField
      FieldAlias = 'site_pictures'
      FieldName = 'site_pictures'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 44
      Searchable = False
      Sortable = False
    end
    object PipeppField47: TppField
      FieldAlias = 'site_marks_measurement'
      FieldName = 'site_marks_measurement'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 45
      Searchable = False
      Sortable = False
    end
    object PipeppField48: TppField
      FieldAlias = 'site_buried_under'
      FieldName = 'site_buried_under'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 46
      Searchable = False
      Sortable = False
    end
    object PipeppField49: TppField
      FieldAlias = 'site_clarity_of_marks'
      FieldName = 'site_clarity_of_marks'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 47
      Searchable = False
      Sortable = False
    end
    object PipeppField50: TppField
      FieldAlias = 'site_hand_dig'
      FieldName = 'site_hand_dig'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 48
      Searchable = False
      Sortable = False
    end
    object PipeppField51: TppField
      FieldAlias = 'site_mechanized_equip'
      FieldName = 'site_mechanized_equip'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 49
      Searchable = False
      Sortable = False
    end
    object PipeppField52: TppField
      FieldAlias = 'site_paint_present'
      FieldName = 'site_paint_present'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 50
      Searchable = False
      Sortable = False
    end
    object PipeppField53: TppField
      FieldAlias = 'site_flags_present'
      FieldName = 'site_flags_present'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 51
      Searchable = False
      Sortable = False
    end
    object PipeppField54: TppField
      FieldAlias = 'site_exc_boring'
      FieldName = 'site_exc_boring'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 52
      Searchable = False
      Sortable = False
    end
    object PipeppField55: TppField
      FieldAlias = 'site_exc_grading'
      FieldName = 'site_exc_grading'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 53
      Searchable = False
      Sortable = False
    end
    object PipeppField56: TppField
      FieldAlias = 'site_exc_open_trench'
      FieldName = 'site_exc_open_trench'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 54
      Searchable = False
      Sortable = False
    end
    object PipeppField57: TppField
      FieldAlias = 'site_img_35mm'
      FieldName = 'site_img_35mm'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 55
      Searchable = False
      Sortable = False
    end
    object PipeppField58: TppField
      FieldAlias = 'site_img_digital'
      FieldName = 'site_img_digital'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 56
      Searchable = False
      Sortable = False
    end
    object PipeppField59: TppField
      FieldAlias = 'site_img_video'
      FieldName = 'site_img_video'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 57
      Searchable = False
      Sortable = False
    end
    object PipeppField60: TppField
      FieldAlias = 'disc_repair_techs_were'
      FieldName = 'disc_repair_techs_were'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 58
      Searchable = False
      Sortable = False
    end
    object PipeppField61: TppField
      FieldAlias = 'disc_repairs_were'
      FieldName = 'disc_repairs_were'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 59
      Searchable = False
      Sortable = False
    end
    object PipeppField62: TppField
      FieldAlias = 'disc_repair_person'
      FieldName = 'disc_repair_person'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 60
      Searchable = False
      Sortable = False
    end
    object PipeppField63: TppField
      FieldAlias = 'disc_repair_contact'
      FieldName = 'disc_repair_contact'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 61
      Searchable = False
      Sortable = False
    end
    object PipeppField64: TppField
      FieldAlias = 'disc_repair_comment'
      FieldName = 'disc_repair_comment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 62
      Searchable = False
      Sortable = False
    end
    object PipeppField65: TppField
      FieldAlias = 'disc_exc_were'
      FieldName = 'disc_exc_were'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 63
      Searchable = False
      Sortable = False
    end
    object PipeppField66: TppField
      FieldAlias = 'disc_exc_person'
      FieldName = 'disc_exc_person'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 64
      Searchable = False
      Sortable = False
    end
    object PipeppField67: TppField
      FieldAlias = 'disc_exc_contact'
      FieldName = 'disc_exc_contact'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 65
      Searchable = False
      Sortable = False
    end
    object PipeppField68: TppField
      FieldAlias = 'disc_exc_comment'
      FieldName = 'disc_exc_comment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 66
      Searchable = False
      Sortable = False
    end
    object PipeppField69: TppField
      FieldAlias = 'disc_other1_person'
      FieldName = 'disc_other1_person'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 67
      Searchable = False
      Sortable = False
    end
    object PipeppField70: TppField
      FieldAlias = 'disc_other1_contact'
      FieldName = 'disc_other1_contact'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 68
      Searchable = False
      Sortable = False
    end
    object PipeppField71: TppField
      FieldAlias = 'disc_other1_comment'
      FieldName = 'disc_other1_comment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 69
      Searchable = False
      Sortable = False
    end
    object PipeppField72: TppField
      FieldAlias = 'disc_other2_person'
      FieldName = 'disc_other2_person'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 70
      Searchable = False
      Sortable = False
    end
    object PipeppField73: TppField
      FieldAlias = 'disc_other2_contact'
      FieldName = 'disc_other2_contact'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 71
      Searchable = False
      Sortable = False
    end
    object PipeppField74: TppField
      FieldAlias = 'disc_other2_comment'
      FieldName = 'disc_other2_comment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 72
      Searchable = False
      Sortable = False
    end
    object PipeppField75: TppField
      FieldAlias = 'exc_resp_code'
      FieldName = 'exc_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 73
      Searchable = False
      Sortable = False
    end
    object PipeppField76: TppField
      FieldAlias = 'exc_resp_type'
      FieldName = 'exc_resp_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 74
      Searchable = False
      Sortable = False
    end
    object PipeppField77: TppField
      FieldAlias = 'exc_resp_other_desc'
      FieldName = 'exc_resp_other_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 75
      Searchable = False
      Sortable = False
    end
    object PipeppField78: TppField
      FieldAlias = 'exc_resp_details'
      FieldName = 'exc_resp_details'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 76
      Searchable = False
      Sortable = False
    end
    object PipeppField79: TppField
      FieldAlias = 'exc_resp_response'
      FieldName = 'exc_resp_response'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 77
      Searchable = False
      Sortable = False
    end
    object PipeppField80: TppField
      FieldAlias = 'uq_resp_code'
      FieldName = 'uq_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 78
      Searchable = False
      Sortable = False
    end
    object PipeppField81: TppField
      FieldAlias = 'uq_resp_type'
      FieldName = 'uq_resp_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 79
      Searchable = False
      Sortable = False
    end
    object PipeppField82: TppField
      FieldAlias = 'uq_resp_other_desc'
      FieldName = 'uq_resp_other_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 80
      Searchable = False
      Sortable = False
    end
    object PipeppField83: TppField
      FieldAlias = 'uq_resp_details'
      FieldName = 'uq_resp_details'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 81
      Searchable = False
      Sortable = False
    end
    object PipeppField84: TppField
      FieldAlias = 'spc_resp_code'
      FieldName = 'spc_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 82
      Searchable = False
      Sortable = False
    end
    object PipeppField85: TppField
      FieldAlias = 'spc_resp_type'
      FieldName = 'spc_resp_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 83
      Searchable = False
      Sortable = False
    end
    object PipeppField86: TppField
      FieldAlias = 'spc_resp_other_desc'
      FieldName = 'spc_resp_other_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 84
      Searchable = False
      Sortable = False
    end
    object PipeppField87: TppField
      FieldAlias = 'spc_resp_details'
      FieldName = 'spc_resp_details'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 85
      Searchable = False
      Sortable = False
    end
    object PipeppField88: TppField
      FieldAlias = 'modified_date'
      FieldName = 'modified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 86
      Searchable = False
      Sortable = False
    end
    object PipeppField89: TppField
      FieldAlias = 'active'
      FieldName = 'active'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 87
      Searchable = False
      Sortable = False
    end
    object PipeppField90: TppField
      FieldAlias = 'profit_center'
      FieldName = 'profit_center'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 88
      Searchable = False
      Sortable = False
    end
    object PipeppField91: TppField
      FieldAlias = 'due_date'
      FieldName = 'due_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 89
      Searchable = False
      Sortable = False
    end
    object PipeppField92: TppField
      FieldAlias = 'utilistar_id'
      FieldName = 'utilistar_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 90
      Searchable = False
      Sortable = False
    end
    object PipeppField93: TppField
      FieldAlias = 'closed_date'
      FieldName = 'closed_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 91
      Searchable = False
      Sortable = False
    end
    object PipeppField94: TppField
      FieldAlias = 'modified_by'
      FieldName = 'modified_by'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 92
      Searchable = False
      Sortable = False
    end
    object PipeppField95: TppField
      FieldAlias = 'uq_resp_ess_step'
      FieldName = 'uq_resp_ess_step'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 93
      Searchable = False
      Sortable = False
    end
    object PipeppField96: TppField
      FieldAlias = 'facility_type'
      FieldName = 'facility_type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 94
      Searchable = False
      Sortable = False
    end
    object PipeppField97: TppField
      FieldAlias = 'facility_size'
      FieldName = 'facility_size'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 95
      Searchable = False
      Sortable = False
    end
    object PipeppField98: TppField
      FieldAlias = 'locator_experience'
      FieldName = 'locator_experience'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 96
      Searchable = False
      Sortable = False
    end
    object PipeppField99: TppField
      FieldAlias = 'locate_equipment'
      FieldName = 'locate_equipment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 97
      Searchable = False
      Sortable = False
    end
    object PipeppField100: TppField
      FieldAlias = 'was_project'
      FieldName = 'was_project'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 98
      Searchable = False
      Sortable = False
    end
    object PipeppField101: TppField
      FieldAlias = 'claim_status'
      FieldName = 'claim_status'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 99
      Searchable = False
      Sortable = False
    end
    object PipeppField102: TppField
      FieldAlias = 'claim_status_date'
      FieldName = 'claim_status_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 100
      Searchable = False
      Sortable = False
    end
    object PipeppField103: TppField
      FieldAlias = 'invoice_code'
      FieldName = 'invoice_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 101
      Searchable = False
      Sortable = False
    end
    object PipeppField104: TppField
      FieldAlias = 'facility_material'
      FieldName = 'facility_material'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 102
      Searchable = False
      Sortable = False
    end
    object PipeppField105: TppField
      FieldAlias = 'est_cost'
      FieldName = 'est_cost'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 103
      Searchable = False
      Sortable = False
    end
    object PipeppField106: TppField
      FieldAlias = 'orig_est_cost'
      FieldName = 'orig_est_cost'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 104
      Searchable = False
      Sortable = False
    end
    object PipeppField107: TppField
      FieldAlias = 'inv_total'
      FieldName = 'inv_total'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 105
      Searchable = False
      Sortable = False
    end
    object PipeppField108: TppField
      FieldAlias = 'remaining'
      FieldName = 'remaining'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 106
      Searchable = False
      Sortable = False
    end
    object PipeppField109: TppField
      FieldAlias = 'total_cost'
      FieldName = 'total_cost'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 107
      Searchable = False
      Sortable = False
    end
    object PipeppField116: TppField
      FieldAlias = 'curr_est_cost'
      FieldName = 'curr_est_cost'
      FieldLength = 10
      DataType = dtNotKnown
      DisplayWidth = 10
      Position = 108
    end
    object PipeppField117: TppField
      FieldAlias = 'curr_remaining'
      FieldName = 'curr_remaining'
      FieldLength = 10
      DataType = dtNotKnown
      DisplayWidth = 10
      Position = 109
    end
    object PipeppField118: TppField
      FieldAlias = 'curr_total_cost'
      FieldName = 'curr_total_cost'
      FieldLength = 10
      DisplayWidth = 10
      Position = 110
    end
    object PipeppField110: TppField
      FieldAlias = 'invoice_code_desc'
      FieldName = 'invoice_code_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 111
      Searchable = False
      Sortable = False
    end
    object PipeppField111: TppField
      FieldAlias = 'invoice_num'
      FieldName = 'invoice_num'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 112
      Searchable = False
      Sortable = False
    end
    object PipeppField112: TppField
      FieldAlias = 'invoice_date'
      FieldName = 'invoice_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 113
      Searchable = False
      Sortable = False
    end
    object PipeppField113: TppField
      FieldAlias = 'exc_resp_desc'
      FieldName = 'exc_resp_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 114
      Searchable = False
      Sortable = False
    end
    object PipeppField114: TppField
      FieldAlias = 'uq_resp_desc'
      FieldName = 'uq_resp_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 115
      Searchable = False
      Sortable = False
    end
    object PipeppField115: TppField
      FieldAlias = 'spc_resp_desc'
      FieldName = 'spc_resp_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 116
      Searchable = False
      Sortable = False
    end
    object uq_damage_id: TppField
      FieldAlias = 'uq_damage_id'
      FieldName = 'uq_damage_id'
      FieldLength = 10
      DisplayWidth = 10
      Position = 117
    end
    object short_name: TppField
      FieldAlias = 'short_name'
      FieldName = 'short_name'
      FieldLength = 10
      DisplayWidth = 10
      Position = 118
    end
    object office_name: TppField
      FieldAlias = 'office_name'
      FieldName = 'office_name'
      FieldLength = 10
      DisplayWidth = 10
      Position = 119
    end
    object locator_name: TppField
      FieldAlias = 'locator_name'
      FieldName = 'locator_name'
      FieldLength = 10
      DisplayWidth = 10
      Position = 120
    end
    object paid_amount: TppField
      FieldAlias = 'paid_amount'
      FieldName = 'paid_amount'
      FieldLength = 10
      DisplayWidth = 10
      Position = 121
    end
    object accrual_amount: TppField
      FieldAlias = 'accrual_amount'
      FieldName = 'accrual_amount'
      FieldLength = 10
      DisplayWidth = 10
      Position = 122
    end
  end
  object TotalsDS: TDataSource
    DataSet = Totals
    Left = 120
    Top = 72
  end
  object TotalsPipe: TppDBPipeline
    DataSource = TotalsDS
    OpenDataSource = False
    UserName = 'TotalsPipe'
    Left = 208
    Top = 72
    object TotalsPipeppField1: TppField
      FieldAlias = 'profit_center'
      FieldName = 'profit_center'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField2: TppField
      FieldAlias = 'invoice_code'
      FieldName = 'invoice_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField3: TppField
      FieldAlias = 'description'
      FieldName = 'description'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField4: TppField
      FieldAlias = 'ratio'
      FieldName = 'ratio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField5: TppField
      FieldAlias = 'ratio_percent'
      FieldName = 'ratio_percent'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField6: TppField
      FieldAlias = 'N'
      FieldName = 'N'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField7: TppField
      FieldAlias = 'total_cost'
      FieldName = 'total_cost'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object TotalsPipeppField8: TppField
      FieldAlias = 'adj_total_cost'
      FieldName = 'adj_total_cost'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object remaining_expo: TppField
      FieldAlias = 'remaining_expo'
      FieldName = 'remaining_expo'
      FieldLength = 10
      DisplayWidth = 10
      Position = 8
    end
    object total_accrual_amount: TppField
      FieldAlias = 'total_accrual_amount'
      FieldName = 'total_accrual_amount'
      FieldLength = 10
      DisplayWidth = 10
      Position = 9
    end
  end
  object Reference: TADOQuery
    Connection = ReportEngineDM.Conn
    Parameters = <>
    SQL.Strings = (
      'select * from reference'
      'where type like '#39'%resp%'#39
      'or type in ('#39'exres'#39','#39'uqres'#39','#39'utres'#39','#39'claimstat'#39')'
      '')
    Left = 56
    Top = 136
  end
  object Totals: TADOQuery
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 56
    Top = 88
  end
  object Damages: TADOQuery
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 56
    Top = 24
  end
end
