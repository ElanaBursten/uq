unit LocateStatusActivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppVar, ppBands, ppCtrls, ppPrnabl,
  ppClass, ppCache, ppProd, ppReport, ppDB, ppComm, ppRelatv, ppDBPipe,
  ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TLocateStatusActivityDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine1: TppLine;
    ppLabel8: TppLabel;
    TicketActivityTitle: TppLabel;
    ppLabel10: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText10: TppDBText;
    ppFooterBand1: TppFooterBand;
    con_name: TppDBText;
    ppLabel11: TppLabel;
    ppLabel4: TppLabel;
    ppDBText6: TppDBText;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppDBText13: TppDBText;
    ppLabel15: TppLabel;
    len_marked: TppDBText;
    ppShape1: TppShape;
    ManagerLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    LocatingCompanyLabel: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    OfficeLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    WorkLatColLabel: TppLabel;
    WorkLat: TppDBText;
    WorkLong: TppDBText;
    DueDateColLabel: TppLabel;
    DueDate: TppDBText;
    WorkLongColLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    MaxModifiedDate: TADODataSet;
    ppLabel9: TppLabel;
    WorkAddress: TppDBText;
    ppDBText12: TppDBText;
  protected
    procedure SetOutgoingParams; override;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, StrUtils, odADOutils;

{ TLocateStatusActivityDM }

procedure TLocateStatusActivityDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  OfficeID: Integer;
  EmpStatus: Integer;
  SingleEmployee: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := SafeGetInteger('Manager');
  OfficeID := SafeGetInteger('Office');
  EmpStatus := GetInteger('EmployeeStatus');
  SingleEmployee := SafeGetBoolean('SingleEmployee', False);
  with SP do begin
    Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
    Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
    Parameters.ParamByName('@Office').value := OfficeID;
    Parameters.ParamByName('@Manager').value := ManagerID;
    Parameters.ParamByName('@SortBy').value := GetInteger('SortBy');
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    Parameters.ParamByName('@SingleEmployee').value := SingleEmployee;
    Open;
  end;


  Master.Recordset         := SP.Recordset;
  MaxModifiedDate.Recordset:= SP.Recordset.NextRecordset(RecsAffected);

  ShowEmployeeName(ManagerLabel, ManagerID, IfThen(SingleEmployee, 'Single Employee:', 'Manager:'));
  ShowOfficeName(OfficeLabel, OfficeID);
  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo', '', False, True);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
end;

{
function TLocateStatusActivityReportForm.ReportSubTitle: string;
const
  RCaption = 'Ticket Activity';
var
  Header: string;
begin
  Header := RCaption;
  if DateRangeRadio.Checked then
    Header := Header +' From '+ DateTimeToStr(RangeSelect.FromDate) +' To '+ DateTimeToStr(RangeSelect.ToDate)
  else if SinceTimeRadio.Checked then
    Header := Header +' Since '+ DateTimeToStr(DateTimeSelect.DateTime)
  else if LastReportTimeRadio.Checked then
    Header := Header +' Since Last Report On '+ IsoStrToSystemStr(DM.UQState.ActivityReportLastDateTime)
  else
    Assert(False);

  if ManagerRadio.Checked then
    Header := Header + ' for Manager: ' + ManagersComboBox.Text
  else if OfficeRadio.Checked then
    Header := Header + ' for Office: ' + OfficesComboBox.Text
  else
    Assert(False);

  Result := Header;
end;
}

procedure TLocateStatusActivityDM.SetOutgoingParams;
begin
  inherited;
  try
    SetParamDate('MaxModifiedDate', MaxModifiedDate.FieldValues['max_modified_date'].Value)
  except
    SetParamDate('MaxModifiedDate', EncodeDate(1900, 1, 1));
  end;
end;

initialization
  RegisterReport('LocateStatusActivity', TLocateStatusActivityDM);

end.
