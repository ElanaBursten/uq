unit TicketsReceived;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppVar, ppBands, ppStrtch, ppCTMain,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  DB, jpeg, ppMemo, ppDesignLayer, ppParameter, Data.Win.ADODB;

const
  TicketColumn  = 3;
  LocatorColumn = 4;

type
  TTicketsReceivedDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    CT: TppCrossTab;
    ManagerLabel: TppLabel;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ClientsLabel: TppMemo;
    ppLabel2: TppLabel;
    CallCentersLabel: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure CTFormatCell(Sender: TObject; aElement: TppElement; aColumn,
      aRow: Integer);
    procedure CTGetCaptionText(Sender: TObject; aElement: TppElement;
      aColumn, aRow: Integer; const aDisplayFormat: string;
      aValue: Variant; var aText: string);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, StrUtils;

procedure TTicketsReceivedDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  CallCenters: string;
  Clients: string;
begin
  inherited;

  ManagerID := SafeGetInteger('ManagerID', 0);
  CallCenters := GetString('CallCenterList');
  // Default to empty string = no terms, ticket totals only
  Clients := SafeGetString('ClientList', '');
  with SP do begin
    Parameters.ParamByName('@StartDate').Value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').Value := GetDateTime('EndDate');
    Parameters.ParamByName('@CallCenterList').Value := CallCenters;
    Parameters.ParamByName('@ClientList').Value := Clients;
    Parameters.ParamByName('@ManagerID').Value := ManagerID;
    Parameters.ParamByName('@GroupByTicketType').Value := SafeGetInteger('GroupByTicketType', 0);
    Open;
  end;

  ShowDateRange(DateRangeLabel);
  ShowEmployeeName(ManagerLabel, ManagerID);
  CallCentersLabel.Text := StringReplace(CallCenters, ',', ', ', [rfReplaceAll]);
  ClientsLabel.Text := IfThen(Clients='*', 'All', StringReplace(Clients, ',', ', ', [rfReplaceAll]));
end;

procedure TTicketsReceivedDM.CTFormatCell(Sender: TObject;
  aElement: TppElement; aColumn, aRow: Integer);
begin
  inherited;

  if (aColumn >= 3) and (aRow >= 2) then  // headings in smaller text
    aElement.Font.Size := 8
  else
    aElement.Font.Size := 7;

  aElement.Font.Style := [ ];

  if aColumn in [TicketColumn, LocatorColumn] then  // Put the Tickets and Locators number in bold
    aElement.Font.Style := aElement.Font.Style + [fsBold];

  if aRow = 1 then  // Put the Tickets number in bold
    aElement.Font.Style := aElement.Font.Style + [fsUnderline];
end;

procedure TTicketsReceivedDM.CTGetCaptionText(Sender: TObject;
  aElement: TppElement; aColumn, aRow: Integer;
  const aDisplayFormat: string; aValue: Variant; var aText: string);
begin
  inherited;
  if (aRow = 0) then  //(aColumn=0) and
    aText := '';  // suppress annoying default text
  if (aColumn in [TicketColumn, LocatorColumn]) and (aRow = 1) then
    aText := Trim(aText) //Delete space
end;

initialization
  RegisterReport('TicketsReceived', TTicketsReceivedDM);

end.

