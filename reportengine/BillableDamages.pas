unit BillableDamages;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, Data.Win.ADODB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppDBPipe, ppBands, ppVar, ppCtrls, ppPrnabl, ppCache,
  ppDesignLayer, ppParameter;

type
  TBillableDamagesDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    Report: TppReport;
    DS: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppLabel2: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel7: TppLabel;
    ppLabel10: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText6: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel1: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel13: TppLabel;
    ppDBText12: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel3: TppLabel;
    ppDBText2: TppDBText;
    ppLabel4: TppLabel;
    ppDBText3: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel8: TppLabel;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppLabel9: TppLabel;
    ppDBText7: TppDBText;
    DateRangeLabel: TppLabel;
    ProfitCenterLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Data: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  BillableDamagesDM: TBillableDamagesDM;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, QMConst, OdIsoDates, OdMiscUtils, StrUtils;

{ TBillingDamagesDM }

procedure TBillableDamagesDM.Configure(QueryFields: TStrings);
var
  BillingDateFrom, BillingDateTo: TDateTime;
  ProfitCenter: string;
begin
  inherited;
  BillingDateFrom := GetDateTime('BillingFromDate');
  BillingDateTo := GetDateTime('BillingToDate');
  ProfitCenter := SafeGetString('ProfitCenter', '');

  Data.Parameters.ParamByName('@BillingStartDate').value := BillingDateFrom;
  Data.Parameters.ParamByName('@BillingEndDate').value := BillingDateTo;
  Data.Parameters.ParamByName('@ProfitCenter').value := ProfitCenter;
  Data.Open;

  ShowDateRange(DateRangeLabel, 'BillingFromDate', 'BillingToDate');
  ShowText(ProfitCenterLabel, 'Profit Center: ', IfThen(not IsEmpty(ProfitCenter), ProfitCenter, 'All'));
end;

initialization
  RegisterReport('BillableDamages', TBillableDamagesDM);

end.
