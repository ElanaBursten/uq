object ReportTestForm: TReportTestForm
  Left = 384
  Top = 245
  Caption = 'Report Tester and Timer'
  ClientHeight = 612
  ClientWidth = 744
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    744
    612)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 88
    Height = 13
    Caption = 'Available Reports:'
  end
  object Label2: TLabel
    Left = 260
    Top = 16
    Width = 176
    Height = 13
    Caption = 'Enter Parameters, including Report='
  end
  object Label3: TLabel
    Left = 264
    Top = 427
    Width = 52
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'DB Server:'
  end
  object Label4: TLabel
    Left = 264
    Top = 451
    Width = 50
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Database:'
  end
  object Label5: TLabel
    Left = 264
    Top = 499
    Width = 52
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Username:'
  end
  object Label6: TLabel
    Left = 264
    Top = 523
    Width = 50
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Password:'
  end
  object Label7: TLabel
    Left = 8
    Top = 579
    Width = 73
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Command Line:'
  end
  object lblOverrideCmdLine: TLabel
    Left = 264
    Top = 383
    Width = 242
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Override Connection String: (Ini Setting Debug=1)'
  end
  object ReportListBox: TListBox
    Left = 8
    Top = 40
    Width = 233
    Height = 526
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object ReportParamMemo: TMemo
    Left = 260
    Top = 40
    Width = 482
    Height = 337
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
    OnChange = ReportParamMemoChange
  end
  object RunButton: TButton
    Left = 552
    Top = 423
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Run &Report'
    Default = True
    TabOrder = 8
    OnClick = RunButtonClick
  end
  object EditPassword: TEdit
    Left = 336
    Top = 519
    Width = 165
    Height = 21
    Anchors = [akLeft, akBottom]
    PasswordChar = '*'
    TabOrder = 6
  end
  object EditServer: TComboBox
    Left = 336
    Top = 423
    Width = 165
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 2
    Text = '10.1.1.175'
    Items.Strings = (
      '10.1.1.175'
      '10.1.1.173'
      '10.1.1.174'
      '10.1.1.183'
      '10.1.1.24'
      'localhost')
  end
  object EditDB: TComboBox
    Left = 336
    Top = 447
    Width = 165
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 3
    Text = 'QMTesting'
    Items.Strings = (
      'QMTesting'
      'QM'
      'UQReporting'
      'QMReporting'
      'LocQM'
      'TestDB')
  end
  object EditUsername: TComboBox
    Left = 336
    Top = 496
    Width = 165
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 5
    Text = 'sa'
    Items.Strings = (
      'sa'
      'uqweb')
  end
  object RunPDFButton: TButton
    Left = 552
    Top = 455
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Generate &PDF...'
    TabOrder = 9
    OnClick = RunPDFButtonClick
  end
  object ExportButton: TButton
    Left = 552
    Top = 487
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Export. Delim...'
    TabOrder = 10
    OnClick = ExportButtonClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 593
    Width = 744
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object SaveSqlCheckBox: TCheckBox
    Left = 336
    Top = 550
    Width = 169
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Save SQL (where applicable)'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object DelimCombo: TComboBox
    Left = 552
    Top = 550
    Width = 57
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akBottom]
    ItemIndex = 0
    TabOrder = 11
    Text = ','
    Items.Strings = (
      ','
      ';'
      '{tab}')
  end
  object UpdateRightDefs: TButton
    Left = 557
    Top = 8
    Width = 185
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Update Report Rights'
    TabOrder = 13
    OnClick = UpdateRightDefsClick
  end
  object TrustedBox: TCheckBox
    Left = 336
    Top = 473
    Width = 121
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Trusted Connection'
    TabOrder = 4
    OnClick = TrustedBoxClick
  end
  object CommandLineEdit: TEdit
    Left = 87
    Top = 576
    Width = 649
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 12
    OnClick = CommandLineEditClick
  end
  object OverrideCmdStr: TEdit
    Left = 264
    Top = 396
    Width = 472
    Height = 21
    Anchors = [akLeft, akBottom]
    Color = clBtnFace
    HideSelection = False
    ReadOnly = True
    TabOrder = 15
  end
  object SaveTSVDialog: TSaveDialog
    DefaultExt = 'tsv'
    Filter = 
      'Tab Separated Values|*.tsv|Comma Separated Values|*.csv|All File' +
      's|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 88
    Top = 64
  end
end
