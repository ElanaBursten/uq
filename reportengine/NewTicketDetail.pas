unit NewTicketDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, jpeg, ppBarCod,
  ppRichTx, OdContainer, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TNewTicketDetailDM = class(TBaseReportDM)
    MasterDS: TDataSource;
    Report: TppReport;
    MasterPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportDateTime: TppCalc;
    ppLabel13: TppLabel;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppDBText3: TppDBText;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppRegion1: TppRegion;
    ppLabel5: TppLabel;
    ppShape3: TppShape;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLine3: TppLine;
    ppLabel6: TppLabel;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine4: TppLine;
    ppLine9: TppLine;
    ppLine10: TppLine;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppRegion2: TppRegion;
    ppShape4: TppShape;
    ppDBText14: TppDBText;
    ppDBText5: TppDBText;
    myDBCheckBox1: TmyDBCheckBox;
    ppDBText16: TppDBText;
    myDBCheckBox2: TmyDBCheckBox;
    ppDBText18: TppDBText;
    ppLine8: TppLine;
    ppLine12: TppLine;
    ppLine13: TppLine;
    ppLine7: TppLine;
    ppLine11: TppLine;
    ppShape5: TppShape;
    ppLine14: TppLine;
    ppLine15: TppLine;
    ppLine16: TppLine;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLine17: TppLine;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppShape6: TppShape;
    ppShape7: TppShape;
    ImageMemo: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    MasterSP: TADOStoredProc;
    BatchPrintedCommand: TADOCommand;
    DetailData: TADODataSet;
    Master: TADODataSet;
    procedure MasterDSDataChange(Sender: TObject; Field: TField);
    procedure ImageMemoPrint(Sender: TObject);
    procedure ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
  protected
    FBatchId : Integer;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure AfterPrint; override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdMiscUtils, OdRbMemoFit, TicketImage, odADOutils;

procedure TNewTicketDetailDM.Configure(QueryFields: TStrings);
var
  RecsAffected : oleVariant;
begin
  inherited;

  with MasterSP do begin
    Parameters.ParamByName('@All').value := GetInteger('All');
    Parameters.ParamByName('@LocatorList').value := GetString('Locators');
    Parameters.ParamByName('@PrintedForUserId').value := GetInteger('User');
    Open;
  end;

  Master.Recordset := MasterSP.Recordset;
  DetailData.Recordset := MasterSP.Recordset.NextRecordset(RecsAffected);

  FBatchId := Master.FieldByName('ticket_batch_id').AsInteger;
end;

procedure TNewTicketDetailDM.MasterDSDataChange(Sender: TObject; Field: TField);
begin
  DetailData.Filter := 'ticket_id=' + IntToStr(Master.FieldByName('ticket_id').AsInteger)
    + ' and ' +
    'locator_id=' + IntToStr(Master.FieldByName('emp_id').AsInteger);
  DetailData.Filtered := True;
end;

procedure TNewTicketDetailDM.AfterPrint;
begin
  Assert(Assigned(BatchPrintedCommand.Connection), 'BatchPrintedCommand has a connection');

  if FBatchId <> 0 then begin
    BatchPrintedCommand.Parameters.ParamByName('ticket_batch_id').Value := FBatchId;
    BatchPrintedCommand.Execute;
  end;
end;

procedure TNewTicketDetailDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

procedure TNewTicketDetailDM.ppGroupHeaderBand3BeforeGenerate(
  Sender: TObject);
var
  IsXml: Boolean;
begin
  inherited;
  ImageMemo.Lines.Text := RemoveExcessWhitespace(GetDisplayableTicketImage(
    MasterPipe.GetFieldAsString('image'), Master.FieldByName('xml_ticket_format').AsString, IsXml));
end;

initialization
  RegisterReport('NewTicketDetail', TNewTicketDetailDM);

end.

