unit EmployeeCovStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, ppCtrls, ppBands, ppClass, ppVar, ppPrnabl,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB,
  ppStrtch, ppSubRpt, ppRegion, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TEmployeeCovStatusDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    HeaderBand: TppHeaderBand;
    ReportHeaderShape: TppShape;
    ReportTitle: TppLabel;
    DetailBand: TppDetailBand;
    FooterBand: TppFooterBand;
    ppReportFooterShape1: TppShape;
    LocatingCompanyLabel: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    SummaryBand: TppSummaryBand;
    DetailColumnHeadersRegion: TppRegion;
    ProfitCenterColTitle: TppLabel;
    EmpNumberColTitle: TppLabel;
    EmpNameColTitle: TppLabel;
    EmpTitleColTitle: TppLabel;
    StartDateColTitle: TppLabel;
    ManagerLabel: TppLabel;
    SummaryColumnHeadersRegion: TppRegion;
    ppLabel2: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel8: TppLabel;
    SummarySubReport: TppSubReport;
    SummaryChildReport: TppChildReport;
    SummaryTitleBand: TppTitleBand;
    SummaryDetailBand: TppDetailBand;
    SummaryCovCount: TppDBText;
    SummaryEmpCount: TppDBText;
    SummaryNotCovCount: TppDBText;
    SummaryPC: TppDBText;
    SummarySummaryBand: TppSummaryBand;
    SummaryBottomLine: TppLine;
    SummaryTopLine: TppLine;
    TotalCovCount: TppDBCalc;
    TotalNotCovCount: TppDBCalc;
    TotalEmpCount: TppDBCalc;
    DetailSubReport: TppSubReport;
    DetailChildReport: TppChildReport;
    DetailTitleBand: TppTitleBand;
    DetailDetailBand: TppDetailBand;
    DetailSummaryBand: TppSummaryBand;
    EmpNumber: TppDBText;
    ShortName: TppDBText;
    EmpTitle: TppDBText;
    StartDate: TppDBText;
    DetailPC: TppDBText;
    PCContinuedLabel: TppLabel;
    ProfitCenterGroup: TppGroup;
    PCGroupHeaderBand: TppGroupHeaderBand;
    PCGroupFooterBand: TppGroupFooterBand;
    CompanyGroup: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    CompanyGroupFooter: TppGroupFooterBand;
    SummaryCompanyName: TppDBText;
    CompanyCovCount: TppDBCalc;
    CompanyNotCovCount: TppDBCalc;
    CompanyEmpCount: TppDBCalc;
    CompanyCountLabel: TppLabel;
    CompanyGroupTopLine: TppLine;
    ReportTotalLabel: TppLabel;
    SummaryCompanyTotalName: TppDBText;
    SummaryTitleLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Summary: TADODataSet;
    Master: TADODataSet;
    procedure PCGroupHeaderBandBeforePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdRbHierarchy, OdMiscUtils, odADOutils;

procedure TEmployeeCovStatusDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  ShowDetails: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  ShowDetails := GetBoolean('ShowDetails');
  SP.Parameters.ParamByName('@ManagerID').value := ManagerID;
  SP.Open;

  Master.RecordSet     := SP.RecordSet;
  Summary.RecordSet   := SP.RecordSet.NextRecordset(RecsAffected);

  ShowEmployeeName(ManagerLabel, ManagerId);
  DetailSubReport.Visible := ShowDetails;
  DetailColumnHeadersRegion.Visible := ShowDetails;
end;

procedure TEmployeeCovStatusDM.PCGroupHeaderBandBeforePrint(Sender: TObject);
begin
  PCContinuedLabel.Visible := not ProfitCenterGroup.FirstPage;
end;

initialization
  RegisterReport('EmployeeCovStatus', TEmployeeCovStatusDM);

end.
