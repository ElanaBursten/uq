# UtiliQuest Q Manager
# PA High Profile data and ticket export

from report_system import *
Setup(-6)

#SetBaseDir('\\\\utiliweb\\reports\\')
SetBaseDir('c:\\temp\\PAexport\\')

ClearParams()
SetReportId('HighProfileExport')
SsvOutput()
OverrideFileName(yyyymmdd_string(report_date()) + '.txt')
Params['DateFrom'] = '[D]'
Params['DateTo'] = '[D+1]'

# use this to specify where the ticket files go.
#Params['TicketSaveDir'] = '\\\\utiliweb\\reports\\somedir'
Params['TicketSaveDir'] = 'c:\\temp\\PAexport\\'
Params['TicketSaveDir'] = 'c:\\temp\\PAexport\\' + yyyymmdd_string(report_date()) + '\\'

# Put in the relevant (PA) client codes here.  These are for
# testing, they aren't PA.
Params['ClientCodes'] = 'B03,B11,DOM100'
Generate()

Run(0)
