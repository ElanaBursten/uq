inherited HighProfileExportDM: THighProfileExportDM
  OldCreateOrder = True
  Height = 274
  object Data: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_HighProfileExport'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DateFrom'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DateTo'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ClientCodes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end>
    Left = 32
    Top = 24
  end
  object Notes: TADODataSet
    Parameters = <>
    Left = 32
    Top = 176
  end
  object Ticket: TADODataSet
    Parameters = <>
    Left = 32
    Top = 80
  end
  object Master: TADODataSet
    Parameters = <>
    Left = 200
    Top = 96
  end
  object Locate: TADODataSet
    Parameters = <>
    Left = 32
    Top = 128
  end
end
