unit ExcavatorCall;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB,
  ppDBPipe, DB, ppModule, daDataModule, ppPrnabl, ppCtrls, ppBands,
  ppCache, ppVar, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TExcavatorCallDM = class(TBaseReportDM)
    SPDs: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    daDataModule1: TdaDataModule;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText3: TppDBText;
    ppLabel2: TppLabel;
    ppDBText4: TppDBText;
    ppLabel3: TppLabel;
    ppDBText1: TppDBText;
    ppLabel4: TppLabel;
    ppDBText2: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppLabel8: TppLabel;
    ppDBText8: TppDBText;
    ppLabel9: TppLabel;
    ppDBText9: TppDBText;
    ppLabel10: TppLabel;
    ppDBText10: TppDBText;
    ppLabel11: TppLabel;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    ppDBText12: TppDBText;
    ppLabel14: TppLabel;
    ppDBText13: TppDBText;
    ppLabel15: TppLabel;
    ppDBText14: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOQuery;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu;

{ TExcavatorCallDM }

procedure TExcavatorCallDM.Configure(QueryFields: TStrings);
begin
  inherited;
{
  with SP do begin
    Parameters.ParamByName('@DateFrom'] := GetDateTime('DateFrom');
    Parameters.ParamByName('@DateTo'] := GetDateTime('DateTo');
    Parameters.ParamByName('@OfficeId'] := GetString('OfficeId');
    Parameters.ParamByName('@ManagerId'] := GetString('ManagerId');
    Open;
  end;
}
end;

initialization
  RegisterReport('ExcavatorCall', TExcavatorCallDM);

end.

