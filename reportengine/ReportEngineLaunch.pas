unit ReportEngineLaunch;

interface

{$WARN SYMBOL_PLATFORM OFF}

uses
  WebBroker,
  CGIApp,
  Forms,
  JclSysInfo,
  ReportTest,
  ReportMailerDMu,
  ReportWebModuleU,
  StatusAutoFax,
  RepDbMonitorMainFormU,
  Windows;

procedure RunAppropriateMainProgram;

implementation

uses
  SysUtils, OdMiscUtils, StatusAutoEmail;

procedure RunCommandLineReport;
begin
  WriteLn('Q Manager Command Line Report Generator');

  Forms.Application.Initialize;
  with TReportMailerDM.Create(nil) do
    Run;
end;

procedure RunFaxQueue;
var
  Rpt: TStatusAutoFaxDM;
begin
  WriteLn('Q Manager Command Line FAX Queue Generator');
  Forms.Application.Initialize;
  Rpt := TStatusAutoFaxDM.Create(nil);
  try
    Rpt.RunNew;
  finally
    FreeAndNil(Rpt);
  end;
end;

procedure RunEmailQueue;
var
  Rpt: TStatusAutoEmailDM;
begin
  WriteLn('Q Manager Command Line Email Generator');
  Forms.Application.Initialize;
  Rpt := TStatusAutoEmailDM.Create(nil);
  try
    Rpt.Run;
  finally
    FreeAndNil(Rpt);
  end;
end;

procedure RunTestGUI;
begin
  Forms.Application.Initialize;
  Forms.Application.Title := 'QM Reports';
  Forms.Application.CreateForm(TReportTestForm, ReportTestForm);
  Application.CreateForm(TReportTestForm, ReportTestForm);
  Forms.Application.Run;
end;

procedure RunMonitor;
var
  Form: TRepDbMonitorMainForm;
begin
  Forms.Application.Initialize;
  Forms.Application.Title := 'QM Reports';
  Application.CreateForm(TRepDbMonitorMainForm, Form);
  Forms.Application.Run;
end;

procedure SetThreadPriorityBelowNormal;
begin
  Win32Check(SetThreadPriority(GetCurrentThread, THREAD_PRIORITY_BELOW_NORMAL));
end;

procedure RunCGI;
begin
  SetThreadPriorityBelowNormal;

  WebBroker.Application.Initialize;
  WebBroker.Application.CreateForm(TReportWebModule, ReportWebModule);
  WebBroker.Application.Run;
end;

function RunningAsCGI: Boolean;
var
  ServerSoftware: string;
begin
  GetEnvironmentVar('SERVER_SOFTWARE', ServerSoftware, False);
  Result := ServerSoftware <> '';   // for testing
end;

procedure ShowUsage;
var
  ExeName: string;
begin
  ExeName := ExtractFileName(ParamStr(0));
  WriteLn(ExeName + ' Version: ' + AppVersionShort);
  WriteLn('Command line options:');
  WriteLn('  /?             Show this help');
  WriteLn('  /gui           Report testing GUI');
  WriteLn('  /monitor       Report DB monitoring GUI');
  WriteLn('  /fax           Queue FAXes');
  WriteLn('  /faxqueue      Send queued faxes');
  WriteLn('  /emailqueue    Send queued notification emails');
  WriteLn('  ReportName     Run the named report, additional parameters:');
  WriteLn('     Param1=Value1 Param2=Value2 [/t] [/tsv] /o OutputFileName.pdf');
  WriteLn('       /skip to do nothing if the output file already exists');
  WriteLn('       /t to test, without running the report');
  WriteLn('            by default, produce a PDF');
  WriteLn('       /tsv to produce a TSV export instead');
  WriteLn('       /csv to produce a CSV export instead');
  WriteLn('       /ssv to produce a ";" delimited export instead');
end;

function GetServerIniName: string;
begin
  Result := ExtractFilePath(GetThisModulePath) + 'QMServer.ini';
end;

procedure RunAppropriateMainProgram;
begin
  Randomize;

  if RunningAsCGI then
  begin
    IniName := GetServerIniName;
    RunCGI;
    Exit;
  end;

  IniName := 'ReportEngineCGI.ini';
  if ParamCount = 0 then
    ShowUsage
  else if SameText(ParamStr(1), '/?') then
    ShowUsage
  else if SameText(ParamStr(1), '/gui') then
    RunTestGUI
  else if SameText(ParamStr(1), '/monitor') then
    RunMonitor
  else if SameText(ParamStr(1), '/faxqueue') then
    RunFaxQueue
  else if SameText(ParamStr(1), '/emailqueue') then
    RunEmailQueue
  else
    RunCommandLineReport;
end;

end.

