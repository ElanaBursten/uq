unit PayrollExceptionAdHoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppModule, daDataModule, ppBands, ppClass,
  ppCtrls, ppVar, ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppComm,
  ppRelatv, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TPayrollExceptionAdHocDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel15: TppLabel;
    ppLabel2: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel3: TppLabel;
    ppLabel26: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    short_namePPText: TppDBText;
    EmpNumberText: TppDBText;
    ppDBText24: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText6: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppLine1: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText2: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLine2: TppLine;
    DateLabel: TppLabel;
    CriteriaLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppReportCopyright: TppLabel;
    ppLabel1: TppLabel;
    ppShape2: TppShape;
    EmployeeStatusLabel: TppLabel;
    ppLabel4: TppLabel;
    ppDBText1: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    FloatHLabel: TppLabel;
    ppDBText5: TppDBText;
    Data: TADOStoredProc;
  private
    function TranslateCriteriaFieldName(const Criteria: string): string;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  PayrollExceptionAdHocDM: TPayrollExceptionAdHocDM;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, QMConst;

{ TPayrollExceptionAdHocDM }

procedure TPayrollExceptionAdHocDM.Configure(QueryFields: TStrings);
const
  EmpStatusCriteria = ' and h_active=';
var
  ManagerID: Integer;
  Criteria: string;
  ReadableCriteria: string;
  EmpStatus: Integer;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  Criteria := GetString('Criteria');
  EmpStatus := SafeGetInteger('EmployeeStatus', -1);

  // for old client support
  if EmpStatus = -1 then begin
    if StrContainsText(EmpStatusCriteria + IntToStr(StatusActive), Criteria) then
      EmpStatus := StatusActive
    else if StrContainsText(EmpStatusCriteria + IntToStr(StatusInactive), Criteria) then
      EmpStatus := StatusInactive
    else
      EmpStatus := StatusAll;
    Delete(Criteria, Pos(EmpStatusCriteria, Criteria), Length(EmpStatusCriteria) + 1);
  end;
  ReadableCriteria := TranslateCriteriaFieldName(Criteria);

  with Data do begin
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Parameters.ParamByName('@Criteria').value := Criteria;
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    Open;
  end;

  ShowDateRange(DateLabel);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowText(CriteriaLabel, 'Criteria:', ReadableCriteria);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  AlignRBComponentsByTag(Self);
end;

function TPayrollExceptionAdHocDM.TranslateCriteriaFieldName(const Criteria: string): string;
var
  i: Integer;
begin
  Result := Criteria;
  for i := Low(PayrollHourFieldNameArray) to High(PayrollHourFieldNameArray) do
    if StrContainsText(PayrollHourFieldNameArray[i].FieldName, Result) then
      Result := StringReplace(Result, PayrollHourFieldNameArray[i].FieldName,
        PayrollHourFieldNameArray[i].DisplayName, [rfReplaceAll]);
end;

initialization
  RegisterReport('PayrollExceptionAdHoc', TPayrollExceptionAdHocDM);

end.

