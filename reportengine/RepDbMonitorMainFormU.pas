unit RepDbMonitorMainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ReportConfig, odAdoUtils, Grids, ADOdb,
  ComCtrls, UQDbConfig;

type
  TRepDbMonitorMainForm = class(TForm)
    Timer: TTimer;
    Panel1: TPanel;
    Label1: TLabel;
    Grid: TStringGrid;
    Conn: TADOConnection;
    procedure TimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Config: TReportConfig;
    Row: Integer;
    function MakeHeader: string;
    procedure EmitHeader;
    procedure CheckServers;
    procedure AppendToLog(Msg: string);
  end;

const
  LogFileName = 'dbmonitor.log';

implementation

{$R *.dfm}

procedure TRepDbMonitorMainForm.TimerTimer(Sender: TObject);
begin
  Timer.Enabled := False;
  try
    CheckServers;
  finally
    Timer.Enabled := True;
  end;
end;

procedure TRepDbMonitorMainForm.FormShow(Sender: TObject);
begin
  Config := LoadConfig(ExtractFilePath(ParamStr(0))+'ReportEngineCGI.ini');
  Row := 0;
  EmitHeader;
  CheckServers;
  Timer.Enabled := True;
end;

procedure TRepDbMonitorMainForm.CheckServers;
var
  D: TDateTime;
  Status: string;
  I: Integer;
  Dbc: TADODatabaseConfig;
  S: string;
begin
  D := Now;

  Inc(Row);
  if Grid.RowCount<= Row then
    Grid.RowCount := Row + 1;

  Grid.Row := Row;

  Grid.Cells[0,Row] := DateTimeToStr(D);
  S := DateTimeToStr(D);
  Application.ProcessMessages;

  for i := 0 to Config.ReportingDBs.Count - 1 do begin
    Dbc := TADODatabaseConfig(Config.ReportingDBs[i]);

    Status := 'OK';
    try
      ConnectADOConnectionWithConfig(Conn, Dbc);
      Conn.Connected := False;
    except
      on E: Exception do begin
        Status := E.Message;
      end;
    end;

    S := S + #9 + Status;

    if Pos('Cannot open database requested', Status)>0 then
      Status := 'BUSY';
    Grid.Cells[i+1,Row] := Status;
    Application.ProcessMessages;
  end;
  AppendToLog(S);
end;

function TRepDbMonitorMainForm.MakeHeader: string;
var
  I: Integer;
  Dbc: TADODatabaseConfig;
  S: string;
begin
  Grid.ColCount := Config.ReportingDBs.Count + 1;
  Grid.Cells[0,0] := 'Date/Time';
  S := 'Date/Time';
  for i := 0 to Config.ReportingDBs.Count - 1 do begin
    Dbc := TADODatabaseConfig(Config.ReportingDBs[i]);
    S := S + #9 + Dbc.Name;
    Grid.Cells[i+1,0] := Dbc.Name;
  end;
  Result := S;
end;

procedure TRepDbMonitorMainForm.AppendToLog(Msg: string);
var
  LogFile: TFileStream;
begin
  if FileExists(LogFileName) then
    LogFile := TFileStream.Create(LogFileName, fmOpenReadWrite or fmShareDenyWrite)
  else
    LogFile := TFileStream.Create(LogFileName, fmCreate or fmShareDenyWrite);

  try
    LogFile.Seek(0, soFromEnd);
    Msg := Msg + #13#10;
    LogFile.Write(Msg[1], Length(Msg));
  finally
    LogFile.Free;
  end;
end;

procedure TRepDbMonitorMainForm.EmitHeader;
begin
  AppendToLog(MakeHeader);
end;

end.

