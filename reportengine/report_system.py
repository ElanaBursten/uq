# ######################################################
# THIS FIRST SECTION IS GLOBAL CODE
# DON'T EDIT IT TO CONTROL THE REPORT CONFIGURATION
# ONLY DEVELOPERS SHOULD EDIT THIS PORTION
# ######################################################

# This code is global

from time import *
from string import replace, join
from os import system
from os.path import isfile, getsize
import sys
import re
import types

# Don't edit these here.  Edit them later in the file.
m = {}
c = {}
p = {}
Params = {}
report_id = ''
output_base_dir = ''
base_file_name = ''
top_level = ''
generated_file_name = ''
report_commands = []
flat_output = False
override_file_name = ''
output_mode = 'pdf'

# The system is hardcoded to add this to the base:
# centername/year/month/day

# Utility functions, only used right here.

RE_D_MINUS = re.compile("^\[D-([0-9]+)\]$")

class ReportCommand:
    def __init__(self, _file, _cmd, _report_id, _params):
        self.file_name = _file
        self.cmd = _cmd
        self.report_id = _report_id
        self.params = _params
        self.query_time = 0
        self.total_time = 0
        self.result = 0

    def tsv(self):
        return join([self.report_id, self.params,
                    str(self.total_time), str(self.query_time),
                    str(self.pdf_size()), str(self.result)], '\t')

    def pdf_size(self):
        if isfile(self.file_name):
            return getsize(self.file_name)
        else:
            return 0

def get_manager(mid):
    if m.has_key(mid):
        return m[mid]
    else:
        return ('Unknown', 'Manager Name Needed')

def get_center(center):
    if c.has_key(center):
        return c[center]
    else:
        return ('Unknown', 'CenterDirNeeded')

def get_pcenter(center):
    if p.has_key(center):
        return p[center]
    else:
        return ('Unknown', 'PCenterDirNeeded')

def report_date():
    return localtime(today)

def format_date_dir():
    return strftime('\\%Y\\%m\\%d\\', report_date())

def date_string(d):
    return strftime('%Y-%m-%d', d)

def yyyymmdd_string(d):
    return strftime('%Y%m%d', d)

def ddd_string(d):
    return strftime('%a', d)

def start_of_month(d):
    while d[2] != 1:    # while it's not the first
        d = localtime(mktime(d) - 24*60*60)   # go back a day
    return d

def start_of_prev_month(d):
    first_of_this_month = start_of_month(d)
    one_day_back = add_days(first_of_this_month, -1)
    return start_of_month(one_day_back)

def start_of_week(d):
    while d[6] != 0:  # while it's not Monday
        d = localtime(mktime(d) - 24*60*60)   # go back a day
    return d

def with_prev_weekend(d):
    # Include Sat and Sun, if it is Monday
    if d[6] == 0:
        d = localtime(mktime(d) - 2*24*60*60)   # go back two days to get weekend
    return d

def add_days(d, n):
    return localtime(mktime(d) + n*24*60*60)

def file_name_for_manager(mid):
    global flat_output
    top_level_dir = get_manager(mid) [0]
    manager_name = get_manager(mid) [1]
    fn = '%s %s %s.%s' % (base_file_name, date_string(report_date()), manager_name, output_mode)
    if flat_output:
        return output_base_dir + fn
    else:
        return output_base_dir + top_level_dir + format_date_dir() + fn

def file_name_for_center(center):
    global flat_output
    top_level_dir = get_center(center)
    fn = '%s %s %s.%s' % (base_file_name, date_string(report_date()), center, output_mode)
    if flat_output:
        return output_base_dir + fn
    else:
        return output_base_dir + top_level_dir + format_date_dir() + fn

def file_name_for_pcenter(center):
    global flat_output
    top_level_dir = get_pcenter(center)
    fn = '%s %s %s.%s' % (base_file_name, date_string(report_date()), center, output_mode)
    if flat_output:
        return output_base_dir + fn
    else:
        return output_base_dir + top_level_dir + format_date_dir() + fn

def file_name_for_global(ext):
    global flat_output, top_level
    top_level_dir = top_level
    fn = '%s %s%s' % (base_file_name, date_string(report_date()), ext)
    if flat_output:
        return output_base_dir + fn
    else:
        return output_base_dir + top_level_dir + format_date_dir() + fn

def calculate_params(manager_or_center, args):
    final_args = {}

    # first, take the defaults
    for param in Params.keys():
        final_args[param] = Params[param]

    # this, the overrides
    for arg in args:
        parts = arg.split('=')
        final_args[parts[0]] = parts[1]

    result = ''
    # then do the [M] substitution:
    for key in final_args.keys():
        value = final_args[key]
        if value == '$':
            value = manager_or_center
        elif value == '[StartOfMonth]':
            value = date_string(start_of_month(report_date()))
        elif value == '[StartOfPrevMonth]':
            value = date_string(start_of_prev_month(report_date()))
        elif value == '[StartOfWeek]':
            value = date_string(start_of_week(report_date()))
        elif value == '[StartOfPrevWeek]':
            value = date_string(add_days(start_of_week(report_date()), -7))
        elif value == '[ThisSaturday]':
            value = date_string(add_days(start_of_week(report_date()),5))
        elif value == '[TodayWithPrevWeekend]':
            value = date_string(with_prev_weekend(report_date()))
        elif value == '[Tomorrow]':
            value = date_string(add_days(report_date(), 2))
        elif value == '[D]':
            value = date_string(report_date())
        elif value == '[D+1]':
            value = date_string(add_days(report_date(), 1))
        elif type(value) == types.StringType:
            m = RE_D_MINUS.match(value)
            if m:
                days_string = m.group(1)
                offset = - int(days_string)
                value = date_string(add_days(report_date(), offset))

        result = result + ' "%s=%s"' % (key, value)
    return result

def make_output_mode_option():
    if output_mode=='tsv':
        return '/tsv'
    if output_mode=='ssv':
        return '/ssv'
    if output_mode=='csv':
        return '/csv'
    return ''

def do_command(filename, key, extra_args, options=''):
    global report_commands
    global report_id
    global generated_file_name
    generated_file_name = filename
    params = calculate_params(key,extra_args)
    cmd = 'ReportEngineCGI.exe %s %s -o "%s"' % (report_id, options, filename) + params
    new_command = ReportCommand(filename, cmd, report_id, params)
    report_commands.append(new_command)

def log_results(rc):
    f = file('ReportEngineCGI.out', 'r')
    lines = f.readlines()
    f.close()
    rc.query_time = lines[1].rstrip()
    rc.total_time = lines[0].rstrip()
    rc.result = join(lines[2:],' ').replace('\n', ' ')

# Entry points to be used by the rest of the code

def Setup(day_offset = -1):
    global today
    spd = 24*60*60
    today = time() + day_offset * spd

def ClearParams():
    global Params
    global override_file_name
    global output_mode
    Params.clear()
    override_file_name = ''
    output_mode = 'pdf'

def TsvOutput():
    global output_mode
    output_mode = 'tsv'

def SsvOutput():
    global output_mode
    output_mode = 'ssv'

def SetBaseDir(dir, want_flat_output = False):
    global output_base_dir, flat_output
    output_base_dir = dir
    flat_output = want_flat_output

def SetBaseFileName(f):
    global base_file_name
    base_file_name = f

def SetTopLevelDir(d):
    global top_level
    top_level = d

def SetReportId(r):
    global report_id
    ClearParams()
    report_id = r
    # the default base file name is the report ID
    SetBaseFileName(r)

def OverrideFileName(fn):
    global override_file_name
    override_file_name = output_base_dir + fn

def HourOfTheDay():
    return localtime() [3]

def GenerateForManager(manager_id, *args):
    do_command(file_name_for_manager(manager_id), manager_id, args, make_output_mode_option())

def GenerateForCallCenter(center, *args):
    do_command(file_name_for_center(center), center, args, make_output_mode_option())

def GenerateForProfitCenter(center, *args):
    do_command(file_name_for_pcenter(center), center, args, make_output_mode_option())

def GenerateForAllManagers():
    ids = m.keys()
    ids.sort()
    for id in ids:
        GenerateForManager(id)

def GenerateForAllCallCenters():
    ids = c.keys()
    ids.sort()
    for id in ids:
        GenerateForCallCenter(id)

def GenerateForAllProfitCenters():
    ids = p.keys()
    ids.sort()
    for id in ids:
        GenerateForProfitCenter(id)

# Used only once:

def Generate():
    if override_file_name<>'':
        do_command(override_file_name, '', [], make_output_mode_option())
    else:
        do_command(file_name_for_global('.' + output_mode), '', [], make_output_mode_option())

def Run(testmode = 0):
    global report_commands
    print 'Executing Report Commands:'
    print '-------------------------------'
    i = 0
    for r in report_commands:
        i = i + 1
        if isfile(r.file_name):
            print 'Skipping, file already exists: %s' % (r.file_name, )
        else:
            if testmode:
                print '(testing) %d of %d: %s' % (i, len(report_commands), r.cmd)
            else:
                print 'Report %d of %d: %s' % (i, len(report_commands), r.cmd)
                system(r.cmd)
                log_results(r)

def DumpResults():
    global report_commands
    print 'Dumping Results to results.txt'
    outfile = file(output_base_dir + 'results.txt', 'w')
    for r in report_commands:
        outfile.write(r.tsv())
        outfile.write('\n')
    outfile.close()

def CheckResults():
    global report_commands
    print 'Checking Results'
    err = 0
    for r in report_commands:
        # if any r.result contains "ERROR" it means we failed
        if r.result.find('ERROR') > -1 :
            print 'Report %s has an error ... see results.txt for details.' % (r.report_id)
            err = 2
        else :
            # if any r.pdf_size < 1 it means we failed
            if r.pdf_size < 1 :
                print 'Expected file %s does not exist.' % (r.file_name)
                err = 1

    if err == 0:
        print 'All reports were successful!'
    else:
        sys.exit(err)
