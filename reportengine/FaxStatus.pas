unit FaxStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppReport, ppSubRpt, ppRegion,
  ppBands, ppVar, ppStrtch, ppMemo, ppClass, myChkBox, ppCtrls, ppPrnabl,
  ppCache, ppComm, ppRelatv, ppProd, DB, ppModule, daDataModule,
  ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TFaxStatusDM = class(TBaseReportDM)
    FaxDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    FromDateLabel: TppLabel;
    ppLabel12: TppLabel;
    ToDateLabel: TppLabel;
    ppLabel14: TppLabel;
    CallCenterLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    FaxPipe: TppDBPipeline;
    FaxSummaryDS: TDataSource;
    FaxSummaryPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    ppSummaryBand1: TppSummaryBand;
    SummarySubRep: TppSubReport;
    ppChildReport1: TppChildReport;
    FaxPagesSubRep: TppSubReport;
    ppChildReport2: TppChildReport;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppDBText4: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppLabel6: TppLabel;
    ppDBText5: TppDBText;
    ppLabel7: TppLabel;
    ppDBText6: TppDBText;
    ppLabel8: TppLabel;
    ppHeaderBand2: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppLabel9: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel11: TppLabel;
    ppDBText10: TppDBText;
    ppLabel15: TppLabel;
    ppDBText11: TppDBText;
    ppLabel16: TppLabel;
    ppDBText12: TppDBText;
    ReportLabel: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    SyncDateLabel: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    OfficeAddressMemo: TppMemo;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppSummaryBand2: TppSummaryBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    FaxToSubRep: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText20: TppDBText;
    FaxNumberDS: TDataSource;
    FaxNumberPipe: TppDBPipeline;
    ppLabel28: TppLabel;
    ErrorMemo: TppMemo;
    ppLabel29: TppLabel;
    daDataModule1: TdaDataModule;
    ppDBText21: TppDBText;
    ppLabel30: TppLabel;
    DetailPipeppField35: TppField;
    ppDBMemo1: TppDBMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Fax: TADOStoredProc;
    FaxSummary: TADODataSet;
    Detail: TADODataSet;
    FaxNumber: TADODataSet;
    Master: TADODataSet;
    procedure OfficeAddressMemoPrint(Sender: TObject);
    procedure MasterAfterScroll(DataSet: TDataSet);
    procedure ppDetailBand1BeforeGenerate(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, odADOutils;

{$R *.dfm}

{ TFaxStatusDM }

procedure TFaxStatusDM.Configure(QueryFields: TStrings);
var
  RecsAffected : oleVariant;
begin
  inherited;

  CallCenterLabel.Caption := GetString('CallCenter');
  FromDateLabel.Caption := DateTimeToStr(GetDateTime('DateFrom'));
  ToDateLabel.Caption := DateTimeToStr(GetDateTime('DateTo'));

  Fax.Parameters.ParamByName('@CallCenter').value := GetString('CallCenter');
  Fax.Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
  Fax.Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
  Fax.Open;

  Master.Recordset     := Fax.Recordset;
  FaxSummary.Recordset := Fax.Recordset.NextRecordset(RecsAffected);
  Detail.Recordset     := Fax.Recordset.NextRecordset(RecsAffected);
  FaxNumber.Recordset  := Fax.Recordset.NextRecordset(RecsAffected);

  FaxPagesSubRep.Visible := Detail.RecordCount>0;
end;

procedure TFaxStatusDM.OfficeAddressMemoPrint(Sender: TObject);
var
  S: string;

  procedure AddLine(const L: string);
  begin
    if L <> '' then
      if S = '' then
        S := L
      else
        S := S + ^M^J  + L;
  end;

  function CityStateZip(const City, State, Zip: string): string;
  begin
    if City = '' then
      Result := State
    else if State <> '' then
      Result := City + ', ' + State
    else
      Result := City;
    if Zip <> '' then
      if Result = '' then
        Result := Zip
      else
        Result := Result + ' ' + Zip;
  end;

begin
  S := '';
  try
    AddLine(Detail.FieldValues['company_name']);
    AddLine(Detail.FieldValues['address1']);
    AddLine(Detail.FieldValues['address2']);
    AddLine(CityStateZip(
      Detail.FieldValues['city'],
      Trim(Detail.FieldValues['state']),
      Detail.FieldValues['zipcode']));
    AddLine('Phone: ' + Detail.FieldValues['phone']);
    AddLine('Fax: ' + Detail.FieldValues['fax']);
  except
    S := '';
  end;
  OfficeAddressMemo.Text := S;
end;

procedure TFaxStatusDM.MasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  FaxNumber.Filter := 'fm_id=' + IntToStr(Master.FieldByName('fm_id').AsInteger);
  FaxNumber.Filtered := True;
end;

procedure TFaxStatusDM.ppDetailBand1BeforeGenerate(Sender: TObject);
var
  I: Integer;
  Raw, Trimmed: TStringList;
begin
  inherited;
  Raw := TStringList.Create;
  Trimmed := TStringList.Create;
  try
    Raw.Text := Master.FieldByName('error_details').AsString;
    for I := 0 to Raw.Count-1 do
     if (Raw[I]<>'') and (Raw[I][1]<>'*') then
       Trimmed.Add(Raw[I]);
    ErrorMemo.Lines.Assign(Trimmed);
  finally
    Raw.Free;
    Trimmed.Free;
  end;
end;

initialization
  RegisterReport('FaxStatus', TFaxStatusDM);

{
Report=FaxStatus
CallCenter=NewJersey
DateFrom=2003-07-30
DateTo=2003-07-31
}

end.

