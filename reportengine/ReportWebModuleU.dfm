object ReportWebModule: TReportWebModule
  OldCreateOrder = False
  Actions = <
    item
      Name = 'PDFAction'
      PathInfo = '/PDF'
      OnAction = PDFActionAction
    end
    item
      Name = 'TSVAction'
      PathInfo = '/TSV'
      OnAction = TSVActionAction
    end
    item
      Name = 'QMServerAction'
      PathInfo = '/QMServer'
      OnAction = ReportWebModuleQMServerActionAction
    end>
  Height = 350
  Width = 453
end
