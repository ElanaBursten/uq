unit TicketDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, jpeg,
  daDataModule, ppRichTx, ppTypes, ServerAttachmentDMu, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TTicketDetailDM = class(TBaseReportDM)
    MasterDS: TDataSource;
    Report: TppReport;
    MasterPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    NotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportDateTime: TppCalc;
    ppLabel13: TppLabel;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppDBText3: TppDBText;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText18: TppDBText;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape1: TppShape;
    ppShape2: TppShape;
    myDBCheckBox1: TmyDBCheckBox;
    ppLabel3: TppLabel;
    myDBCheckBox2: TmyDBCheckBox;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppSystemVariable1: TppSystemVariable;
    DetailHeaderRegion: TppRegion;
    NotesSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppNotesSummary: TppSummaryBand;
    NoteMemo: TppDBMemo;
    ppSummaryBand2: TppSummaryBand;
    ppDBText5: TppDBText;
    ppLabel5: TppLabel;
    ImageMemo: TppMemo;
    ImagePipeField: TppField;
    ppLabel6: TppLabel;
    ppDBMemo1: TppDBMemo;
    ImagesSubReport: TppSubReport;
    ppChildReportTicketAttachments: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailTicketImages: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    AttachmentsDS: TDataSource;
    AttachmentsPipe: TppDBPipeline;
    ImageRegion: TppRegion;
    ppDBText6: TppDBText;
    ppImage: TppImage;
    ppMemoErrorMessage: TppMemo;
    FacilitiesDS: TDataSource;
    FacilitiesPipe: TppDBPipeline;
    FacilitiesSubReport: TppSubReport;
    ppChildReportFacilities: TppChildReport;
    FacilitySubReportDetailBand: TppDetailBand;
    FacilitySubReportTitleBand: TppTitleBand;
    FacilitySectionTitle: TppLabel;
    FacClient: TppDBText;
    FacilityClientLabel: TppLabel;
    FacType: TppDBText;
    FacilityTypeLabel: TppLabel;
    FacMaterial: TppDBText;
    FacilityMaterialLabel: TppLabel;
    FacSize: TppDBText;
    FacilitySizeLabel: TppLabel;
    FacOffsets: TppDBText;
    FacilityOffsetLabel: TppLabel;
    FacAddedBy: TppDBText;
    FacilityAddedByLabel: TppLabel;
    FacilitySubReportHeaderBand: TppHeaderBand;
    FacilityPressureLabel: TppLabel;
    FacilityPressure: TppDBText;
    ppShape4: TppShape;
    ppDBText7: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    Ticket: TADOStoredProc;
    Locates: TADODataSet;
    Notes: TADODataSet;
    Attachments: TADODataSet;
    Facilities: TADODataSet;
    Master: TADODataSet;
    procedure MasterAfterScroll(DataSet: TDataSet);
    procedure ppGroupHeaderBand3BeforeGenerate(Sender: TObject);
    procedure ImageMemoPrint(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ppDetailTicketImagesBeforeGenerate(Sender: TObject);
    procedure FacOffsetsGetText(Sender: TObject; var Text: string);
  protected
    procedure MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string); override;
  private
    FileList: TStringList;
    ErrorLog: TStringList;
    Attacher: TServerAttachment;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdRbMemoFit, OdExceptions,
  OdMiscUtils, OdRbDownloadedImage, QMConst, OdPdf, TicketImage, CVUtils, OdAdoUtils;

procedure TTicketDetailDM.Configure(QueryFields: TStrings);
var
  TicketId: Integer;
  ImageList: string;
  RecsAffected : oleVariant;
begin
  inherited;
  ImageList := SafeGetString('ImageList', '-1');
  if not IsEmpty(ImageList) then
    if not ValidateIntegerCommaSeparatedList(ImageList) then
      raise Exception.Create('The parameter ImageList=' + ImageList + ' is not valid.');

  TicketId := GetInteger('TicketId');
  Ticket.ProcedureName := 'RPT_TicketDetail';
  Ticket.Parameters.ParamByName('@TicketId').Value := TicketId;
  Ticket.Parameters.ParamByName('@AttachmentList').Value := ImageList;
  Ticket.Open;
  Assert(Ticket.RecordCount < 2, 'Multiple tickets selected for a single ticket report');

 //QMANTWO-533 EB
  Master.Recordset := Ticket.Recordset;
  Locates.Recordset := Ticket.Recordset.NextRecordset(RecsAffected);
  Notes.Recordset := Ticket.Recordset.NextRecordset(RecsAffected);
  Attachments.Recordset := Ticket.Recordset.NextRecordset(RecsAffected);
  Facilities.Recordset := Ticket.Recordset.NextRecordset(RecsAffected);

  NotesSubReport.Visible := SafeGetInteger('IncludeNotes', 1) = 1;

  Attacher := TServerAttachment.Create(Ticket.Connection);
  ImagesSubReport.Visible := Attacher.AttachmentsIncludeFileType(Attachments, Attacher.GetFileExtensionsForPrintableFileType(ImageFilesModifier));
  FacilitiesSubReport.Visible := SafeGetInteger('IncludeFacilities', 1) = 1;

  if (Attachments.RecordCount > 0) and (not Master.IsEmpty) then begin
    Attacher.DownloadAttachments(Master.FieldByName('ticket_id').AsInteger, qmftTicket, FileList, ErrorLog, ImageList, OutputFolder, True);
    FilterForNoPDFs(Attachments);
  end;

  AttachmentsDS.DataSet := ReplaceCVs(Master.FieldByName('ticket_id').AsInteger,
    qmftTicket, Attachments, FileList, GetReportRegEx(Ticket.Connection), IniName);
end;

procedure TTicketDetailDM.MasterAfterScroll(DataSet: TDataSet);
begin
  Locates.Filter := 'ticket_id=' + Master.FieldByName('ticket_id').AsString;
  Locates.Filtered := True;
  Notes.Filter := 'ticket_id=' + Master.FieldByName('ticket_id').AsString;
  Notes.Filtered := True;
end;

procedure TTicketDetailDM.ppGroupHeaderBand3BeforeGenerate(
  Sender: TObject);
var
  IsXml: Boolean;
begin
  inherited;
  ImageMemo.Lines.Text := RemoveExcessWhitespace(GetDisplayableTicketImage(
    Master.FieldByName('image').AsString,
    Master.FieldByName('xml_ticket_format').AsString, IsXml));
end;

procedure TTicketDetailDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

procedure TTicketDetailDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
begin
  MergedPDFFileName := MergePDFs(FileList, OutputDir, ReportFileName, IniName);
end;

procedure TTicketDetailDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  FileList := TStringList.Create;
  ErrorLog := TStringList.Create;
  ppImage.Units := utScreenPixels;
end;

procedure TTicketDetailDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FileList);
  FreeAndNil(ErrorLog);
  FreeAndNil(Attacher);
end;

procedure TTicketDetailDM.FacOffsetsGetText(Sender: TObject;
  var Text: string);
begin
  if Text = '' then
    Text := '-';
end;

procedure TTicketDetailDM.ppDetailTicketImagesBeforeGenerate(Sender: TObject);
begin
  ShowDownloadedImage(FileList, ErrorLog, AttachmentsDS.DataSet, ppImage, ppMemoErrorMessage);
end;

initialization
  RegisterReport('TicketDetail', TTicketDetailDM);

end.
