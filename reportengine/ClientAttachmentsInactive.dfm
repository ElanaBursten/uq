inherited ClientAttachmentsInactiveDM: TClientAttachmentsInactiveDM
  OldCreateOrder = True
  object ClientAttachmentsInactiveSP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_ClientAttachmentsInactive'
    Parameters = <
      item
        Name = '@CallCenters'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@ClientIDs'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@AttachModifyStartDate'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@AttachModifyEndDate'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ForeignTypes'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@IncludeFileTypes'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@ExcludeFileNames'
        DataType = ftString
        Value = Null
      end>
    Left = 80
    Top = 24
  end
end
