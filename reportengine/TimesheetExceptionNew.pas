unit TimesheetExceptionNew;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppRegion, ppBands, ppVar, ppStrtch, ppMemo, ppClass,
  myChkBox, ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB,
  ppComm, ppRelatv, ppDB, ppDBPipe, ppModule, daDataModule, ppSubRpt,
  ppDesignLayer, ppParameter,  Data.Win.ADODB;

type
  TTimesheetExceptionNewDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ShortNameText: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    FSYL: TppShape;
    LSYE: TppShape;
    SYSP: TppShape;
    FSTL: TppShape;
    LSTE: TppShape;
    STSP: TppShape;
    NOHR: TppShape;
    ppLabel11: TppLabel;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppDBText9: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    DetailsDS: TDataSource;
    DetailsPipe: TppDBPipeline;
    DetailSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppLabel22: TppLabel;
    ppDBText1: TppDBText;
    ppLabel23: TppLabel;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppLabel30: TppLabel;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    NoSheetLabel: TppLabel;
    ppDBText29: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    ppDBText36: TppDBText;
    ppDBTextRuleMsg: TppDBText;
    ppDBTextRuleBrokenMsg: TppDBText;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    DateLabel: TppLabel;
    AddtlWorkRegion: TppRegion;
    AddtlCalloutsRegion: TppRegion;
    ppLabel10: TppLabel;
    ppLabel16: TppLabel;
    ppDBText10: TppDBText;
    ppDBText37: TppDBText;
    ppDBText38: TppDBText;
    ppDBText39: TppDBText;
    ppDBText40: TppDBText;
    ppDBText41: TppDBText;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    ppDBText44: TppDBText;
    ppDBText45: TppDBText;
    ppDBText48: TppDBText;
    ppDBText49: TppDBText;
    ppDBText60: TppDBText;
    ppDBText61: TppDBText;
    ppDBText62: TppDBText;
    ppDBText63: TppDBText;
    ppDBText64: TppDBText;
    ppDBText65: TppDBText;
    ppDBText66: TppDBText;
    ppDBText67: TppDBText;
    ppDBText68: TppDBText;
    ppDBText69: TppDBText;
    AddtlWorkRegion2: TppRegion;
    ppDBText90: TppDBText;
    ppDBText91: TppDBText;
    ppDBText94: TppDBText;
    ppDBText95: TppDBText;
    ppDBText96: TppDBText;
    ppDBText97: TppDBText;
    ppDBText98: TppDBText;
    ppDBText99: TppDBText;
    ppDBText100: TppDBText;
    ppDBText101: TppDBText;
    ppDBText102: TppDBText;
    ppDBText103: TppDBText;
    AddtlCalloutsRegion2: TppRegion;
    ppDBText113: TppDBText;
    ppDBText114: TppDBText;
    ppDBText115: TppDBText;
    ppDBText116: TppDBText;
    ppDBText117: TppDBText;
    ppDBText118: TppDBText;
    ppDBText119: TppDBText;
    ppDBText120: TppDBText;
    ppDBText121: TppDBText;
    ppDBText122: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Details: TADODataSet;
    Master: TADODataSet;
    procedure Configure(QueryFields: TStrings); override;
    procedure ppDetailBand1BeforePrint(Sender: TObject);
    procedure DetailSubReportPrint(Sender: TObject);
    procedure ppDetailBand2BeforePrint(Sender: TObject);
  private
    procedure SetBox(Name: string);
  end;

implementation

uses ReportEngineDMu, OdAdoUtils;

{$R *.dfm}

procedure TTimesheetExceptionNewDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
  StartDate: TDateTime;
  RecsAffected : oleVariant;
begin
  inherited;

  ManagerID := GetInteger('ManagerId');
  StartDate := GetDateTime('StartDate');
  EmpStatus := GetInteger('EmployeeStatus');
  with SP do begin
  //QMANTWO-533 EB
    Parameters.ParamByName('@ManagerID').Value := ManagerID;
    Parameters.ParamByName('@StartDate').Value := StartDate;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;
  //QMANTWO-533 EB
  Master.Recordset := SP.Recordset;
  Details.Recordset := SP.Recordset.NextRecordset(RecsAffected);

  DetailSubReport.Visible := SafeGetString('ShowSheetDetail', '1') = '1';

  ShowDateText(DateLabel, 'Work Date:', StartDate);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
end;

procedure TTimesheetExceptionNewDM.ppDetailBand1BeforePrint(Sender: TObject);
//var
//  Level: Integer;
begin
  SetBox('FSYL');
  SetBox('LSYE');
  SetBox('SYSP');
  SetBox('FSTL');
  SetBox('LSTE');
  SetBox('STSP');
  SetBox('NOHR');

  //Level := Master.FieldByName('report_level').AsInteger;
  //ShortNameText.Left := 0.0208 + (Level-1) * 0.2;

  NoSheetLabel.Visible := Master.FieldByName('tse_id').IsNull;
end;

procedure TTimesheetExceptionNewDM.ppDetailBand2BeforePrint(Sender: TObject);
begin
  inherited;
  AddtlWorkRegion.Visible := (not Details.FieldByName('work_start5').IsNull);
  AddtlWorkRegion2.Visible := (not Details.FieldByName('work_start11').IsNull);
  AddtlCalloutsRegion.Visible := (not Details.FieldByName('callout_start7').IsNull);
  AddtlCalloutsRegion2.Visible := (not Details.FieldByName('callout_start12').IsNull);
end;

procedure TTimesheetExceptionNewDM.SetBox(Name: string);
var
  S: TppShape;
begin
  S := FindComponent(Name) as TppShape;
  S.Visible := Master.FieldByName(Name).AsString <>'';
  S.Pen.Style := psClear;
end;

procedure TTimesheetExceptionNewDM.DetailSubReportPrint(Sender: TObject);
begin
  inherited;
  Details.Filtered := True;
  Details.Filter := 'entry_id=' + IntToStr(Master.FieldByName('tse_id').AsInteger);
end;

initialization
  RegisterReport('TimesheetExceptionNew', TTimesheetExceptionNewDM);

end.

