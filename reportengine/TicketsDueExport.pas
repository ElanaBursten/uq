unit TicketsDueExport;   //QMANTWO-784

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, Data.Win.ADODB, Data.DB;

type
  TTicketsDueExportDM = class(TBaseReportDM)
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;     //QMANTWO-783
  end;

implementation

uses ReportEngineDMu, OdDbUtils, OdMiscUtils, OdAdoUtils, JclStrings;

{$R *.dfm}

{ TTicketsDueDM }

procedure TTicketsDueExportDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
  DueDate: TDateTime;
  RecsAffected: OleVariant;
begin
  inherited;

  ManagerID := GetInteger('ManagerId');
  EmpStatus := GetInteger('EmployeeStatus');
  DueDate   := GetDateTime('DueDate');

  with SP do begin
    Parameters.ParamByName('@ManagerId').Value      := ManagerID;
    Parameters.ParamByName('@DueDate').Value        := DueDate;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;

end;

procedure TTicketsDueExportDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
var
  c: char;
  encodingType:TQMEncodingType;
begin
  encodingType:=etANSI; //etUTF8
  SimpleSaveDelimToStream(encodingType, SP, Str, NativeTab, True, NativeComma); //QMANTWO-784
end;

initialization
  RegisterReport('TicketsDueExport', TTicketsDueExportDM);

end.
