inherited TimeExportDM: TTimeExportDM
  OldCreateOrder = True
  Width = 236
  object FindProfitCenter: TADOQuery
    Parameters = <
      item
        Name = 'emp_id'
        DataType = ftInteger
        Value = Null
      end>
    SQL.Strings = (
      'select o.profit_center'
      '  from employee_office eo'
      '  inner join office o on eo.office_id=o.office_id'
      ' where eo.emp_id=:emp_id')
    Left = 136
    Top = 24
  end
  object SP: TADOStoredProc
    ProcedureName = 'payroll_export_adp2'
    Parameters = <
      item
        Name = '@manager'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@datefrom'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@dateto'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@LevelLimit'
        DataType = ftInteger
        Value = Null
      end>
    Left = 40
    Top = 24
  end
end
