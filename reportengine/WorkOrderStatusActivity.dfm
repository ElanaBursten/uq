inherited WorkOrderStatusActivityDM: TWorkOrderStatusActivityDM
  Width = 367
  inherited Report: TppReport
    DataPipelineName = 'Pipe'
    inherited ppHeaderBand1: TppHeaderBand
      mmHeight = 32279
      inherited BaseReportTitle: TppLabel
        SaveOrder = -1
        Caption = 'Work Order Status Activity'
      end
      object WorkOrdersStatusedFrom: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'WorkOrdersStatusedFrom'
        Caption = 'Work Orders Statused From:   '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 0
        mmTop = 14552
        mmWidth = 44186
        BandType = 0
        LayerName = Foreground
      end
      object WorkOrdersTransmittedFrom: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'WorkOrdersTransmittedFrom'
        Caption = 'Work Orders Transmitted From:   '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 86519
        mmTop = 14552
        mmWidth = 48948
        BandType = 0
        LayerName = Foreground
      end
      object WorkOrdersLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'WorkOrdersLabel'
        Caption = 'Work Orders:  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 86519
        mmTop = 10583
        mmWidth = 21167
        BandType = 0
        LayerName = Foreground
      end
      object HeaderRegion: TppRegion
        DesignLayer = ppDesignLayer1
        UserName = 'HeaderRegion'
        Brush.Style = bsClear
        ParentWidth = True
        Pen.Color = clWhite
        Pen.Style = psClear
        Pen.Width = 0
        ShiftRelativeTo = CallCentersRegion
        Stretch = True
        Transparent = True
        mmHeight = 7144
        mmLeft = 0
        mmTop = 23548
        mmWidth = 266701
        BandType = 0
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppLabel2: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label2'
          Caption = 'WO Number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 0
          mmTop = 26194
          mmWidth = 15346
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel5: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label5'
          Caption = 'Ticket #'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 23019
          mmTop = 26193
          mmWidth = 9790
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel11: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label11'
          Caption = 'Status'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 146050
          mmTop = 26193
          mmWidth = 8202
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel8: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label8'
          Caption = 'Employee'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 88106
          mmTop = 26193
          mmWidth = 12171
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel7: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label7'
          Caption = 'Street'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 193146
          mmTop = 26194
          mmWidth = 7673
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel3: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label3'
          Caption = 'County'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 223044
          mmTop = 26194
          mmWidth = 8996
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel6: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label6'
          Caption = 'City / Municipality'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 243682
          mmTop = 26194
          mmWidth = 21697
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel9: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label9'
          Caption = '#'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 181505
          mmTop = 26194
          mmWidth = 1588
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel1: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label1'
          Caption = 'Status Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 155046
          mmTop = 26193
          mmWidth = 14817
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel4: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label4'
          Caption = 'Client'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 127000
          mmTop = 26193
          mmWidth = 6878
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel10: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label10'
          Caption = 'Kind'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 46302
          mmTop = 26193
          mmWidth = 5556
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel12: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label12'
          Caption = 'Transmit Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 62706
          mmTop = 26193
          mmWidth = 17463
          BandType = 0
          LayerName = Foreground
        end
        object ppLine1: TppLine
          DesignLayer = ppDesignLayer1
          UserName = 'Line1'
          ParentWidth = True
          Weight = 0.750000000000000000
          mmHeight = 265
          mmLeft = 0
          mmTop = 29369
          mmWidth = 266701
          BandType = 0
          LayerName = Foreground
        end
        object ppLabel13: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label13'
          Caption = 'Call Center'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 111654
          mmTop = 26193
          mmWidth = 13758
          BandType = 0
          LayerName = Foreground
        end
      end
      object CallCentersRegion: TppRegion
        DesignLayer = ppDesignLayer1
        UserName = 'CallCentersRegion'
        Brush.Style = bsClear
        ParentWidth = True
        Pen.Color = clWhite
        Pen.Style = psClear
        Pen.Width = 0
        Stretch = True
        Transparent = True
        mmHeight = 5027
        mmLeft = 0
        mmTop = 17992
        mmWidth = 266701
        BandType = 0
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object CallCentersLabel: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'CallCentersLabel'
          Caption = 'Call Centers:   '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 0
          mmTop = 18786
          mmWidth = 18521
          BandType = 0
          LayerName = Foreground
        end
        object CallCentersMemo: TppMemo
          DesignLayer = ppDesignLayer1
          UserName = 'CallCentersMemo'
          Caption = 'Call Centers'
          CharWrap = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          RemoveEmptyLines = False
          Stretch = True
          Transparent = True
          mmHeight = 3704
          mmLeft = 19579
          mmTop = 18786
          mmWidth = 65352
          BandType = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          mmLeading = 0
        end
        object ClientsLabel: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'CallCentersLabel1'
          Caption = 'Clients: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3703
          mmLeft = 86519
          mmTop = 18786
          mmWidth = 10054
          BandType = 0
          LayerName = Foreground
        end
        object ClientCodesMemo: TppDBMemo
          DesignLayer = ppDesignLayer1
          UserName = 'ClientCodesMemo'
          CharWrap = False
          DataField = 'client_codes'
          DataPipeline = ClientsListPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          ParentDataPipeline = False
          RemoveEmptyLines = False
          Stretch = True
          Transparent = True
          DataPipelineName = 'ClientsListPipe'
          mmHeight = 3703
          mmLeft = 96838
          mmTop = 18786
          mmWidth = 105304
          BandType = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          mmLeading = 0
        end
      end
      object ManagerLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'WorkOrdersLabel1'
        Caption = 'Manager:   '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 0
        mmTop = 10583
        mmWidth = 15875
        BandType = 0
        LayerName = Foreground
      end
    end
    inherited ppDetailBand1: TppDetailBand
      mmHeight = 4498
      object ppDBText5: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText5'
        AutoSize = True
        DataField = 'wo_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 0
        mmTop = 0
        mmWidth = 21696
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText8: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText8'
        AutoSize = True
        DataField = 'ticket_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 23019
        mmTop = 0
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText11: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText101'
        AutoSize = True
        DataField = 'kind'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 46302
        mmTop = 0
        mmWidth = 15875
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText12: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText12'
        AutoSize = True
        DataField = 'transmit_date'
        DataPipeline = Pipe
        DisplayFormat = 'mm/dd/yyyy hh:mm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 62706
        mmTop = 0
        mmWidth = 24606
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText9: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText9'
        AutoSize = True
        DataField = 'short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 88106
        mmTop = 0
        mmWidth = 23283
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText13: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText102'
        AutoSize = True
        DataField = 'wo_source'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 111654
        mmTop = 0
        mmWidth = 14817
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText10: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText10'
        AutoSize = True
        DataField = 'oc_code'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 127000
        mmTop = 0
        mmWidth = 15875
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText2'
        AutoSize = True
        DataField = 'status'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 146050
        mmTop = 0
        mmWidth = 7938
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        AutoSize = True
        DataField = 'status_date'
        DataPipeline = Pipe
        DisplayFormat = 'mm/dd/yyyy hh:mm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 155046
        mmTop = 0
        mmWidth = 25928
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText4: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText4'
        AutoSize = True
        DataField = 'work_address_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 181240
        mmTop = 0
        mmWidth = 11642
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText7: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText7'
        DataField = 'work_address_street'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 193146
        mmTop = 0
        mmWidth = 29369
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText3: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText3'
        DataField = 'work_county'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 223044
        mmTop = 0
        mmWidth = 20108
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText6: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText6'
        DataField = 'work_city'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 243682
        mmTop = 0
        mmWidth = 23548
        BandType = 4
        LayerName = Foreground
      end
    end
    inherited ppFooterBand1: TppFooterBand
      inherited LocatingCompanyLabel: TppLabel
        SaveOrder = -1
      end
      inherited ReportDateTimeCalc: TppCalc [2]
        SaveOrder = -1
      end
      inherited ReportPageSystemVar: TppSystemVariable [3]
        SaveOrder = -1
      end
      inherited CopyrightLabel: TppLabel [4]
        SaveOrder = -1
        Caption = 'Work Order Status Activity'
      end
    end
  end
  inherited Master: TADODataSet
    Left = 280
    Top = 80
  end
  object ClientsListDS: TDataSource
    DataSet = ClientsList
    Left = 120
    Top = 77
  end
  object ClientsListPipe: TppDBPipeline
    DataSource = ClientsListDS
    UserName = 'ClientsPipe'
    Left = 200
    Top = 77
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_WorkOrderStatusActivity'
    Parameters = <
      item
        Name = '@CallCenterCodes'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@StatusDateFrom'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@StatusDateTo'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@WorkOrderClosed'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@TransmitDateFrom'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TransmitDateTo'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ClientIDList'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@ManagerID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 40
    Top = 24
  end
  object ClientsList: TADODataSet
    Parameters = <>
    Left = 40
    Top = 77
  end
end
