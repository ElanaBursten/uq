unit LateTicket;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppBands, ppClass, ppVar, ppCtrls, ppPrnabl, ppCache,
  ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB, ppModule,
  daDataModule, ppMemo, ppStrtch, ppRegion, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TLateTicketDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ClosedOrArrivedDate: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalc2: TppDBCalc;
    ppLabel14: TppLabel;
    ppLine2: TppLine;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText10: TppDBText;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ppTitleBand1: TppTitleBand;
    ppLabel2: TppLabel;
    ppDBText11: TppDBText;
    ppLabel17: TppLabel;
    ppDBText12: TppDBText;
    ppLabel18: TppLabel;
    ppDBText13: TppDBText;
    ppLabel19: TppLabel;
    ppDBText14: TppDBText;
    ReportSummaryTitle: TppLabel;
    ppLine1: TppLine;
    ppLine3: TppLine;
    ppDBText15: TppDBText;
    ppDBText18: TppDBText;
    ppReportFooterShape1: TppShape;
    ppLabel23: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    ppRegion1: TppRegion;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ClosedOrArrivedDateLabel: TppLabel;
    ppLabel9: TppLabel;
    ppLabel12: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppReportHeaderShape1: TppShape;
    ReportTitle: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    CallCenterLabel: TppLabel;
    CallCentersMemo: TppMemo;
    EmployeeStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    SummaryData: TADODataSet;
  private
    procedure SetupReportLayout(const ArrivalMode: Boolean);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdDbUtils, odADOutils;

{$R *.dfm}

{ TLateTicketDM }

procedure TLateTicketDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
  CallCenters: string;
  ArrivalMode: Boolean;
  RecsAffected : Olevariant;
begin
  inherited;

  ManagerID := SafeGetInteger('ManagerId', 0);
  EmpStatus := GetInteger('EmployeeStatus');
  CallCenters := GetString('CallCenter');
  ArrivalMode := SafeGetBoolean('ArrivalMode', False);

  with SP do begin
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Parameters.ParamByName('@CallCenterCodes').value := CallCenters;
    Parameters.ParamByName('@SortBy').value := GetString('SortBy');
    Parameters.ParamByName('@NowDate').value := Now;
    Parameters.ParamByName('@ManagerID').value := ManagerID;
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    Parameters.ParamByName('@Arrival').value := ArrivalMode;
    Open;
  end;

  Master.Recordset := SP.recordset;
  SummaryData.Recordset := SP.Recordset.NextRecordset(RecsAffected);

  SetupReportLayout(ArrivalMode);
  ShowDateRange(DateRangeLabel);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  CallCentersMemo.Text := StringReplace(CallCenters, ',', ', ', [rfReplaceAll]);
end;

procedure TLateTicketDM.SetupReportLayout(const ArrivalMode: Boolean);
begin
  if ArrivalMode then begin
    ClosedOrArrivedDate.DataField := 'arrival_date';
    ClosedOrArrivedDateLabel.Caption := 'Arrival Date';
    ReportTitle.Caption := 'Late Arrival Ticket Report';
  end else begin
    ClosedOrArrivedDate.DataField := 'closed_date';
    ClosedOrArrivedDateLabel.Caption := 'Closed Date';
    ReportTitle.Caption := 'Late Ticket Report';
  end;
  ReportSummaryTitle.Caption := ReportTitle.Caption + ' Summary:';
end;

initialization
  RegisterReport('LateTicket', TLateTicketDM);

end.

