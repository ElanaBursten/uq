unit TicketsReceivedSummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppVar, ppBands, ppStrtch, ppCTMain,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  DB, ppDesignLayer, ppParameter,  Data.Win.ADODB;

type
  TTicketsReceivedSummaryDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    FromDateLabel: TppLabel;
    ppLabel12: TppLabel;
    ToDateLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    Pipe: TppDBPipeline;
    CT: TppCrossTab;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure CTGetCaptionText(Sender: TObject; aElement: TppElement;
      aColumn, aRow: Integer; const aDisplayFormat: string;
      aValue: Variant; var aText: string);
  private

  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}


{ TTicketsReceivedSummaryDM }

procedure TTicketsReceivedSummaryDM.Configure(QueryFields: TStrings);
begin
  inherited;
  SP.Parameters.ParamByName('@StartDate').Value := GetDateTime('StartDate');
  SP.Parameters.ParamByName('@EndDate').Value := GetDateTime('EndDate');
  SP.Parameters.ParamByName('@IncludeBreakdownByCC').Value := SafeGetInteger('IncludeBreakdownByCC');
  SP.Parameters.ParamByName('@IncludeNoWorkTickets').Value := SafeGetInteger('IncludeNoWorkTickets');
  SP.Open;

  ShowDateRange(FromDateLabel, ToDateLabel);
  // Ideally we'd hide the CC column if not (SafeGetInteger('IncludeBreakdownByCC')=1)
end;

procedure TTicketsReceivedSummaryDM.CTGetCaptionText(Sender: TObject;
  aElement: TppElement; aColumn, aRow: Integer;
  const aDisplayFormat: string; aValue: Variant; var aText: string);
begin
  if (aRow = 0) then  //(aColumn=0) and
    aText := '';  // suppress annoying default text
end;

initialization
  RegisterReport('TicketsReceivedSummary', TTicketsReceivedSummaryDM);

end.

