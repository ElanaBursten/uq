unit DamageProductivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppDBJIT,
  daDataModule, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TDamageProductivityDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    PageHeaderBand: TppHeaderBand;
    ReportHeaderBox: TppShape;
    ReportTitle: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    DetailBand: TppDetailBand;
    PageFooterBand: TppFooterBand;
    ReportFooterBox: TppShape;
    FooterReportTitle: TppLabel;
    ReportDateTime: TppCalc;
    PageNumber: TppSystemVariable;
    Company: TppLabel;
    DamageType: TppDBText;
    DamageCount: TppDBText;
    ClosedDate: TppDBText;
    ColumnHeaderRegion: TppRegion;
    InvestigatorColHeader: TppLabel;
    DamageTypeColHeader: TppLabel;
    DateColHeader: TppLabel;
    TotalColHeader: TppLabel;
    Investigator: TppDBText;
    Line: TppLine;
    InvestigatorGroup: TppGroup;
    InvestigatorGroupHeader: TppGroupHeaderBand;
    InvestigatorGroupFooter: TppGroupFooterBand;
    CompletedCountLabel: TppLabel;
    DamageTypeGroup: TppGroup;
    DamageTypeGroupHeader: TppGroupHeaderBand;
    DamageTypeGroupFooter: TppGroupFooterBand;
    SummaryDamageType: TppDBText;
    DamageTypeCount: TppDBCalc;
    InvestigatorLabel: TppLabel;
    DamageTypesLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure DamageTypeGroupFooterBeforePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdMiscUtils, OdRbHierarchy, QMConst;

{ TProductivityDM }

procedure TDamageProductivityDM.Configure(QueryFields: TStrings);
var
  InvestigatorID: Integer;
  ManagerID: Integer;
  DamageTypes: String;
  EmployeeStatus: Integer;
begin
  inherited;

  ManagerID := GetInteger('Manager');
  InvestigatorID := SafeGetInteger('Investigator',-1);
  DamageTypes := GetString('DamageTypes');
  if StrContains(DamageTypePendingApproval,DamageTypes) then
    DamageTypes := StringReplace(DamageTypes, DamageTypePendingApproval, DamageTypePendingApproval + ',' + DamageTypeCompleted, [rfIgnoreCase]);

  EmployeeStatus := GetInteger('EmployeeStatus');

  with SP do begin
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@InvestigatorId').value := InvestigatorID;
    Parameters.ParamByName('@DamageTypes').value := DamageTypes;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
    Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
    Open;
  end;

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo', 'From:', False, True);
  ShowEmployeeName(ManagerLabel, ManagerID, 'Manager:');
  ShowEmployeeName(InvestigatorLabel, InvestigatorID, 'Investigator:');
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
  ShowText(DamageTypesLabel, 'Damage Types:', DamageTypes);
end;

procedure TDamageProductivityDM.DamageTypeGroupFooterBeforePrint(
  Sender: TObject);
begin
  // only completed damage summary is currently shown
  DamageTypeGroupFooter.Visible := SummaryDamageType.Text = 'COMPLETED';
end;

initialization
  RegisterReport('DamageProductivity', TDamageProductivityDM);

end.


