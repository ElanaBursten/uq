unit ManagerStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppBands, ppClass, ppVar,
  ppModule, daDataModule, ppCtrls, ppPrnabl, ppCache, ppComm, ppRelatv,
  ppProd, ppReport, ppDesignLayer, ppParameter, Data.Win.ADODB;


type
  TManagerStatusReportDM = class(TBaseReportDM)
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    short_nameDBText: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppFooterBand1: TppFooterBand;
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppLine1: TppLine;
    ppDBText15: TppDBCalc;
    ppDBText16: TppDBCalc;
    ppDBText17: TppDBCalc;
    ppDBText18: TppDBCalc;
    ppDBText19: TppDBCalc;
    ppDBText20: TppDBCalc;
    ppDBText21: TppDBCalc;
    ppDBText22: TppDBCalc;
    ppDBText24: TppDBCalc;
    ppDBText25: TppDBCalc;
    ppDBText26: TppDBCalc;
    ppLabel24: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    daDataModule1: TdaDataModule;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppDBCalc77: TppDBCalc;
    ppLabel10: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel11: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses OdRbHierarchy, ReportEngineDMu;

{$R *.dfm}

{ TManagerStatusReportDM }

procedure TManagerStatusReportDM.Configure(QueryFields: TStrings);
begin
  inherited;
  AddHierGroups(Report, short_nameDBText);

  SP.Parameters.ParamByName('@ManagerID').value := GetInteger('manager_id');
  SP.Parameters.ParamByName('@StartDate').value := GetDateTime('start_date');
  SP.Parameters.ParamByName('@EndDate').value := GetDateTime('end_date');
  SP.Open;
end;

initialization
  RegisterReport('ManagerStatus', TManagerStatusReportDM);

end.

