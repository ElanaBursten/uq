unit DamageLiabilityChanges;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, jpeg,
  OdContainer, daDataModule, DBClient, ppDesignLayer, ppParameter,

  Data.Win.ADODB;

type
  TDamageLiabilityChangesDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    EstimateDS: TDataSource;
    EstimatePipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ActivityPeriodLabel: TppLabel;
    MainDetailBand: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    StateLabel: TppLabel;
    ProfitCenterLabel: TppLabel;
    StartingLiabilityLabel: TppLabel;
    InvoiceDS: TDataSource;
    InvoicePipe: TppDBPipeline;
    NotifiedPeriodLabel: TppLabel;
    DamagedLabel: TppLabel;
    ppGroup7: TppGroup;
    ppGroupHeaderBand7: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    EstimateSR: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText26: TppDBText;
    ppDBText31: TppDBText;
    InvoiceSR: TppSubReport;
    ppChildReport5: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppLabel35: TppLabel;
    ppLabel36: TppLabel;
    ppLabel37: TppLabel;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    NewInvoiceDetail: TppDetailBand;
    ppDBText33: TppDBText;
    NewInvoicedAmount: TppDBText;
    ppDBText35: TppDBText;
    ppDBText36: TppDBText;
    NewPaidAmount: TppDBText;
    ppDBText39: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppLabel32: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLine5: TppLine;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText44: TppDBText;
    ppDBText49: TppDBText;
    ppDBText51: TppDBText;
    ppDBText34: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel6: TppLabel;
    ppLabel22: TppLabel;
    ppLabel21: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel54: TppLabel;
    ppLabel58: TppLabel;
    ppLabel62: TppLabel;
    ppLabel78: TppLabel;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppLabel19: TppLabel;
    ppDBText21: TppDBText;
    ppDBText1: TppDBText;
    EndingLiabilityLabel: TppLabel;
    ppLabel82: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppShape2: TppShape;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppDBMemo1: TppDBMemo;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel8: TppLabel;
    ppDBText3: TppDBText;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine1: TppLine;
    ppShape1: TppShape;
    ppLabel11: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppTitleBand2: TppTitleBand;
    ppLine2: TppLine;
    ppLine3: TppLine;
    ppLabel9: TppLabel;
    ppDBText4: TppDBText;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel20: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText17: TppDBText;
    ppLabel31: TppLabel;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppLabel42: TppLabel;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppDBText18: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppDBCalc8: TppDBCalc;
    ppDBCalc9: TppDBCalc;
    ppLine4: TppLine;
    ppLine6: TppLine;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppLine7: TppLine;
    ppLine8: TppLine;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBCalc17: TppDBCalc;
    ppDBCalc18: TppDBCalc;
    ppDBCalc19: TppDBCalc;
    ppDBCalc20: TppDBCalc;
    ppDBCalc21: TppDBCalc;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    ppDBCalc24: TppDBCalc;
    ppDBCalc25: TppDBCalc;
    ppDBCalc26: TppDBCalc;
    ppDBCalc27: TppDBCalc;
    ppDBCalc28: TppDBCalc;
    ppDBCalc29: TppDBCalc;
    ppDBCalc30: TppDBCalc;
    ppDBCalc31: TppDBCalc;
    ppLabel45: TppLabel;
    MinChangeLabel: TppLabel;
    OmitInitialEstimateLabel: TppLabel;
    ppDBText27: TppDBText;
    ppDBText14: TppDBText;
    ppDBText28: TppDBText;
    ppDBText29: TppDBText;
    IncludeZeroUQLiabLabel: TppLabel;
    IncludeThirdPartyLabel: TppLabel;
    IncludeLitigationLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Invoice: TADODataSet;
    Estimate: TADODataSet;
    procedure MasterAfterScroll(DataSet: TDataSet);
  private
    //procedure TraceSPInvocation;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, QMConst, OdIsoDates, OdDBUtils, StrUtils,
  OdMiscUtils, odADOutils;

procedure TDamageLiabilityChangesDM.Configure(QueryFields: TStrings);
var
  DateFrom, DateTo: TDateTime;
  BeginningOfTime, EndOfTime: TDateTime;
  NotifiedBegin: TDateTime;
  NotifiedEnd: TDateTime;
  DamageBegin: TDateTime;
  DamageEnd: TDateTime;
  LiabilityTypeStart: Integer;
  LiabilityTypeEnd: Integer;
  MinimumChange: Integer;
  ExcludeInitialEst: Boolean;
  IncludeZeroUQLiab: Boolean;
  IncludeThirdParty: Boolean;
  IncludeLitigation: Boolean;
  RecsAffected : oleVariant;
  function DateStrOrDash(D: TDateTime): string;
  begin
    if (D = BeginningOfTime) or (D = EndOfTime) then
      Result := '-'
    else
      Result := DateToStr(D);
  end;

begin
  inherited;
  BeginningOfTime := IsoStrToDate('1900-01-01');
  EndOfTime := IsoStrToDate('2500-01-01');

  ShowDateRange(ActivityPeriodLabel, 'DateFrom', 'DateTo', 'Activity Period:');
  DateFrom := GetDateTime('DateFrom');
  DateTo := GetDateTime('DateTo');

  ShowDateRange(NotifiedPeriodLabel, 'NotifiedDateFrom', 'NotifiedDateTo', 'Notified:');
  NotifiedBegin := SafeGetDateTime('NotifiedDateFrom', BeginningOfTime);
  NotifiedEnd := SafeGetDateTime('NotifiedDateTo', EndOfTime);

  ShowDateRange(DamagedLabel, 'DamageDateFrom', 'DamageDateTo', 'Damaged:');
  DamageBegin := SafeGetDateTime('DamageDateFrom', BeginningOfTime);
  DamageEnd := SafeGetDateTime('DamageDateTo', EndOfTime);

  LiabilityTypeStart := GetInteger('LiabilityStart');
  LiabilityTypeEnd := GetInteger('LiabilityEnd');
  ShowText(StartingLiabilityLabel, GetLiabilityString(LiabilityTypeStart));
  ShowText(EndingLiabilityLabel, GetLiabilityString(LiabilityTypeEnd));

  ShowText(StateLabel, SafeGetString('State', ''));
  ShowText(ProfitCenterLabel, SafeGetString('ProfitCenter', ''));

  MinimumChange := SafeGetInteger('MinChange', 0);
  ShowText(MinChangeLabel, IfThen(MinimumChange > 0, IntToStr(MinimumChange), 'ANY/ALL Changes'));

  ExcludeInitialEst := SafeGetString('ExcludeInitialEst', '0')='1';
  ShowText(OmitInitialEstimateLabel, BooleanToStringYesNo(ExcludeInitialEst));

  IncludeZeroUQLiab := GetString('IncludeZeroUQLiab')='1';
  IncludeThirdParty := SafeGetInteger('IncludeThirdParty', 0) = 1;
  IncludeLitigation := SafeGetInteger('IncludeLitigation', 0) = 1;

  ShowText(IncludeZeroUQLiabLabel, BooleanToStringYesNo(IncludeZeroUQLiab));
  ShowText(IncludeThirdPartyLabel, BooleanToStringYesNo(IncludeThirdParty));
  ShowText(IncludeLitigationLabel, BooleanToStringYesNo(IncludeLitigation));

  with SP do begin
    Parameters.ParamByName('@RangeStart').value := DateFrom;
    Parameters.ParamByName('@RangeEnd').value := DateTo;
    Parameters.ParamByName('@RecdStart').value := NotifiedBegin;
    Parameters.ParamByName('@RecdEnd').value := NotifiedEnd;
    Parameters.ParamByName('@DmgStart').value := DamageBegin;
    Parameters.ParamByName('@DmgEnd').value := DamageEnd;
    Parameters.ParamByName('@State').value := SafeGetString('State', '');
    Parameters.ParamByName('@ProfitCenter').value := SafeGetString('ProfitCenter', '');
    Parameters.ParamByName('@LiabilityTypeStart').value := LiabilityTypeStart;
    Parameters.ParamByName('@LiabilityTypeEnd').value := LiabilityTypeEnd;
    Parameters.ParamByName('@IncludeZeroUQLiab').value := IncludeZeroUQLiab;
    Parameters.ParamByName('@MinChange').value := MinimumChange;
    Parameters.ParamByName('@ExcludeInitialEst').value := ExcludeInitialEst;
    Parameters.ParamByName('@IncludeThirdParty').value := IncludeThirdParty;
    Parameters.ParamByName('@IncludeLitigation').value := IncludeLitigation;
    Open;
    //TTrace.Debug.SendValue('SP Execution done', '');
  end;

  Master.RecordSet := SP.RecordSet;
  Estimate.RecordSet := SP.RecordSet.NextRecordset(RecsAffected );
  Invoice.RecordSet := SP.RecordSet.NextRecordset(RecsAffected );
end;

(*
procedure TDamageLiabilityChangesDM.TraceSPInvocation;
var
  S: string;
  I: Integer;
  P: TParameter;
  DateString: string;
begin
  // This code was useful in figuring out a problem with this report,
  // something like it should end up in a reusable library somewhere.
  // S := 'exec dbo.' + Master.ProcedureName + ' ';

  for I := 1 to Master.Params.Count - 1 do begin
    P := Master.Params.Items[I];

    if P.DataType = ftString then begin
      S := S + '''' + VarToStr(P.Value) + '''';
    end else if VarIsType(P.Value, varDate) then begin
      DateString := IsoDateTimeToStr(VarToDateTime(P.Value));
      if Copy(DateString, 11, 13) = 'T00:00:00.000' then
        DateString := Copy(DateString, 1, 10);

      S := S + '''' + DateString  + '''';
    end else
      S := S + VarToStr(P.Value);

    if I < Master.Params.Count - 1 then
      S := S + ', ';
  end;

  TTrace.Debug.SendValue('Master Invocation', S);
end;
*)

procedure TDamageLiabilityChangesDM.MasterAfterScroll(DataSet: TDataSet);
var
  Filter: string;
begin
  Filter := 'damage_id=' + IntToStr(Master.FieldByName('damage_id').AsInteger);
  Estimate.Filter := Filter;
  Estimate.Filtered := True;
  Invoice.Filter := Filter;
  Invoice.Filtered := True;
end;

initialization
  RegisterReport('DamageLiabilityChanges', TDamageLiabilityChangesDM);

end.

