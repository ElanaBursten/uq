unit TimesheetExceptionByEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppRegion, ppBands, ppVar, ppStrtch, ppMemo, ppClass,
  myChkBox, ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB,
  ppComm, ppRelatv, ppDB, ppDBPipe, ppModule, daDataModule, ppSubRpt,
  ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimesheetExceptionByEmpDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    DateLabel: TppLabel;
    ppLabel16: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ShortNameText: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText10: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    FSYL: TppShape;
    LSYE: TppShape;
    SYSP: TppShape;
    FSTL: TppShape;
    LSTE: TppShape;
    STSP: TppShape;
    NOHR: TppShape;
    ppLabel11: TppLabel;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppDBText9: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    DetailsDS: TDataSource;
    DetailsPipe: TppDBPipeline;
    DetailSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppLabel22: TppLabel;
    ppDBText1: TppDBText;
    ppLabel23: TppLabel;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppLabel30: TppLabel;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    NoSheetLabel: TppLabel;
    daDataModule1: TdaDataModule;
    ppDBText29: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    ppDBText36: TppDBText;
    ppLabel24: TppLabel;
    ppDBText37: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Details: TADODataSet;
    Master: TADODataSet;
    procedure Configure(QueryFields: TStrings); override;
    procedure ppDetailBand1BeforePrint(Sender: TObject);
    procedure DetailSubReportPrint(Sender: TObject);
  private
    procedure SetBox(Name: string);
  end;

implementation

uses ReportEngineDMu, OdAdoUtils;

{$R *.dfm}

procedure TTimesheetExceptionByEmpDM.Configure(QueryFields: TStrings);
var
  RecsAffected: oleVariant;
begin
  inherited;

   //QMANTWO-533 EB
  with SP do begin
    Parameters.ParamByName('@EmployeeID').Value := GetInteger('EmployeeId');
    Parameters.ParamByName('@StartDate').Value := GetDateTime('StartDate');
    Open;
  end;
   //QMANTWO-533 EB
  Master.Recordset := SP.Recordset;
  Details.Recordset := SP.Recordset.NextRecordset(RecsAffected);

  DetailSubReport.Visible := SafeGetString('ShowSheetDetail', '1') = '1';

  DateLabel.Caption := FormatDateTime('mmmm yyyy', GetDateTime('StartDate'));
end;

procedure TTimesheetExceptionByEmpDM.ppDetailBand1BeforePrint(Sender: TObject);
//var
//  Level: Integer;
begin
  SetBox('FSYL');
  SetBox('LSYE');
  SetBox('SYSP');
  SetBox('FSTL');
  SetBox('LSTE');
  SetBox('STSP');
  SetBox('NOHR');

  //Level := Master.FieldByName('report_level').AsInteger;
  //ShortNameText.Left := 0.0208 + (Level-1) * 0.2;

  NoSheetLabel.Visible := Master.FieldByName('tse_id').IsNull;
end;

procedure TTimesheetExceptionByEmpDM.SetBox(Name: string);
var
  S: TppShape;
begin
  S := FindComponent(Name) as TppShape;
  S.Visible := Master.FieldByName(Name).AsString <>'';
  S.Pen.Style := psClear;
end;

procedure TTimesheetExceptionByEmpDM.DetailSubReportPrint(Sender: TObject);
begin
  inherited;
  Details.Filtered := True;
  Details.Filter := 'entry_id=' + IntToStr(Master.FieldByName('tse_id').AsInteger);
end;

initialization
  RegisterReport('TimesheetExceptionByEmp', TTimesheetExceptionByEmpDM);

end.

