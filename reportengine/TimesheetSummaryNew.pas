unit TimesheetSummaryNew;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppBands, ppClass, ppCtrls, ppReport,
  ppStrtch, ppSubRpt, ppVar, ppPrnabl, ppCache, ppProd, ppDB, ppComm,
  ppRelatv, ppDBPipe, myChkBox, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimesheetSummaryNewDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    WeekEndingDateLabel: TppLabel;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLabel4: TppLabel;
    ppLine1: TppLine;
    short_namePPText: TppDBText;
    SummarySubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ppTitleBand1: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel32: TppLabel;
    ppDBText22: TppDBText;
    ppLabel21: TppLabel;
    reg_hours_total: TppDBCalc;
    ppLabel20: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText7: TppDBText;
    ppLabel7: TppLabel;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppDBText9: TppDBText;
    ppLabel9: TppLabel;
    ppDBText10: TppDBText;
    ppLabel10: TppLabel;
    ppDBText11: TppDBText;
    ppLabel11: TppLabel;
    ppDBText12: TppDBText;
    ppLabel12: TppLabel;
    ppDBText14: TppDBText;
    ppLabel13: TppLabel;
    ppDBText15: TppDBText;
    ppLabel14: TppLabel;
    ppDBText16: TppDBText;
    ppDBText6: TppDBText;
    ppLabel3: TppLabel;
    ppLine3: TppLine;
    ppDBCalc9: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBCalc17: TppDBCalc;
    ppDBCalc18: TppDBCalc;
    ppDBCalc19: TppDBCalc;
    ppDBCalc20: TppDBCalc;
    ppDBCalc21: TppDBCalc;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    ppDBCalc24: TppDBCalc;
    ppDBCalc25: TppDBCalc;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText23: TppDBText;
    ppLabel5: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc26: TppDBCalc;
    ppLine2: TppLine;
    EmpTotalsDS: TDataSource;
    EmpTotalsPipe: TppDBPipeline;
    ppDBText24: TppDBText;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppDBText25: TppDBText;
    ppDBCalc27: TppDBCalc;
    ppDBCalc28: TppDBCalc;
    ppTitleBand2: TppTitleBand;
    ppPageStyle1: TppPageStyle;
    ppLabel28: TppLabel;
    ppDBText26: TppDBText;
    ppDBCalc29: TppDBCalc;
    FloatingHolidayLabel: TppLabel;
    FloatingHolidayCheckBox: TmyDBCheckBox;
    ppDBCalc30: TppDBCalc;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel1: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    HierOptionLabel: TppLabel;
    ppLabel15: TppLabel;
    ppDBText1: TppDBText;
    ppDBCalc31: TppDBCalc;
    ppLabel29: TppLabel;
    ppDBText27: TppDBText;
    ppDBCalc32: TppDBCalc;
    ppDBCalc33: TppDBCalc;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    ppDBCalc34: TppDBCalc;
    ppLabel30: TppLabel;
    ppDBText28: TppDBText;
    ppDBCalc35: TppDBCalc;
    Data: TADOStoredProc;
    Summary: TADODataSet;
    EmpTotals: TADODataSet;
    Master: TADODataSet;
    procedure ppGroupHeaderBand1BeforePrint(Sender: TObject);
    procedure MasterAfterScroll(DataSet: TDataSet);
    procedure TotalsPrint(Sender: TObject);
  private
    procedure HandleSortOrder(SP: TADOStoredProc);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, StrUtils, QMConst,
     RbCustomTextFit, OdAdoUtils;

{$R *.dfm}

{ TTimesheetSummaryDM }

procedure TTimesheetSummaryNewDM.Configure(QueryFields: TStrings);
var
  EndDate: TDateTime;
  ManagerID: Integer;
  EmpStatus: Integer;
  HierOption: Integer;
  RecsAffected : oleVariant;
begin
  inherited;

  ManagerID := GetInteger('manager_id');
  EndDate := GetDateTime('end_date');
  EmpStatus := SafeGetInteger('EmployeeStatus', StatusAll);
  HierOption := SafeGetInteger('HierarchyDepth', HierarchyOptionFullDepth);
  with Data do begin
    Parameters.ParamByName('@ManagerID').Value := ManagerID;
    Parameters.ParamByName('@EndDate').Value := EndDate;
    Parameters.ParamByName('@LevelLimit').Value := HierOption;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;

//QMANTWO-533 EB
  Master.Recordset := Data.Recordset;
  Summary.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  EmpTotals.Recordset := Data.Recordset.NextRecordset(RecsAffected);

  HandleSortOrder(Data);
  AlignRBComponentsByTag(Self);
  ShowDateText(WeekEndingDateLabel, 'Week Ending:', EndDate);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  ShowHierarchyOption(HierOptionLabel, HierOption);
end;

procedure TTimesheetSummaryNewDM.HandleSortOrder(SP: TADOStoredProc);
var
  Sort: string;
begin
  Sort := SafeGetString('Sort', '');
  if Sort='Emp Number' then
    Master.IndexFieldNames := 'h_numpath'
  else if Sort='Last Name' then
    Master.IndexFieldNames := 'h_lastpath'
  else
    Master.IndexFieldNames := 'h_namepath';
end;

procedure TTimesheetSummaryNewDM.ppGroupHeaderBand1BeforePrint(Sender: TObject);
var
  Level: Integer;
begin
  inherited;
  Level := Master.FieldByName('h_report_level').AsInteger;
  short_namePPText.Left := (Level-1) * 0.18;
end;

procedure TTimesheetSummaryNewDM.MasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  EmpTotals.Filter := 'emp_id=' + IntToStr(Master.FieldByName('emp_id').AsInteger);
end;

procedure TTimesheetSummaryNewDM.TotalsPrint(Sender: TObject);
begin
  ScaleRBTextToFit(Sender as TppDBText, 8);
end;

initialization
  RegisterReport('TimesheetSummaryNew', TTimesheetSummaryNewDM);
end.

