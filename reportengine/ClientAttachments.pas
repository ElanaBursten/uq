unit ClientAttachments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ServerAttachmentDMu,  Data.Win.ADODB;

const
  ReportName = 'ClientAttachments';

type
  TClientAttachmentsDM = class(TBaseReportDM)
    ClientAttachmentsSP: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    AttachmentIDList: TStringList;
    Attacher: TServerAttachment;
    procedure LogMessage(Sender: TObject; const Message: string;
      const IsError: Boolean=False);
    procedure SkipCorruptAttachment(Sender: TObject;
      const AttachmentID: Integer; const DownloadedFilename: string);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  ClientAttachmentsDM: TClientAttachmentsDM;

implementation

{$R *.dfm}

uses OdMiscUtils, ReportEngineLog, ReportEngineDMu, odADOUtils;

{ TClientAttachmentsDM }

procedure TClientAttachmentsDM.Configure(QueryFields: TStrings);
var
  ForeignTypes: string;
  ClientIDs: string;
  ExcludeFileNames: string;
  DestinationFolder: string;
  UseAttachmentOrigFilename: Boolean;
begin
  inherited;
  DestinationFolder := GetString('DestinationFolder');
  UseAttachmentOrigFilename := SafeGetBoolean('UseOrigFilename', True);

  ExcludeFileNames := StringReplace(SafeGetString('ExcludeFileNames', ''), '*', '%', [rfReplaceAll]);
  ClientIDs := GetString('ClientIDs');
  ForeignTypes := SafeGetString('ForeignTypes', '1');  // default to only ticket attachments
  if not ValidateIntegerCommaSeparatedList(ClientIDs) then
    raise Exception.Create('The ClientIDs parameter is a comma separated list of client ids to get attachments for.');
  if not ValidateIntegerCommaSeparatedList(ForeignTypes) then
    raise Exception.Create('The ForeignTypes parameter is a comma separated list of foreign types to get attachments for.');

  ClientAttachmentsSP.Parameters.ParamByName('@CallCenters').value := GetString('CallCenters');
  ClientAttachmentsSP.Parameters.ParamByName('@ClientIDs').value := ClientIDs;
  ClientAttachmentsSP.Parameters.ParamByName('@ForeignTypes').value := ForeignTypes;
  ClientAttachmentsSP.Parameters.ParamByName('@AttachUploadStartDate').value := GetDateTime('UploadStartDate');
  ClientAttachmentsSP.Parameters.ParamByName('@AttachUploadEndDate').value := GetDateTime('UploadEndDate');
  ClientAttachmentsSP.Parameters.ParamByName('@IncludeFileTypes').value := GetString('IncludeFileTypes');
  ClientAttachmentsSP.Parameters.ParamByName('@ExcludeFileNames').value := ExcludeFileNames;
  ClientAttachmentsSP.Open;

  Attacher := TServerAttachment.Create(ClientAttachmentsSP.Connection);
  Attacher.OnLogReportMessage := LogMessage;
  Attacher.OnCorruptAttachment := SkipCorruptAttachment;
  AttachmentIDList.Clear;
  while not ClientAttachmentsSP.Eof do begin
    AttachmentIDList.Add(ClientAttachmentsSP.FieldByName('attachment_id').AsString);
    ClientAttachmentsSP.Next;
  end;

  if ClientAttachmentsSP.RecordCount > 0 then begin
    ClientAttachmentsSP.Filtered := False;
    ClientAttachmentsSP.Filter := '';
    Attacher.DownloadAttachmentsForAttachmentIDs(AttachmentIDList.CommaText,
      DestinationFolder, UseAttachmentOrigFilename);
    ClientAttachmentsSP.Filtered := True;
  end;
end;

procedure TClientAttachmentsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  AttachmentIDList := TStringList.Create;
end;

procedure TClientAttachmentsDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(AttachmentIDList);
  FreeAndNil(Attacher);
end;

procedure TClientAttachmentsDM.LogMessage(Sender: TObject;
  const Message: string; const IsError: Boolean);
begin
  AddLogEntry(ReportName, Message, IsError);
end;

procedure TClientAttachmentsDM.SkipCorruptAttachment(Sender: TObject;
  const AttachmentID: Integer; const DownloadedFilename: string);
begin
  OdDeleteFile(ExtractFilePath(DownloadedFilename),
    ExtractFileName(DownloadedFilename), -1, False, False);

  Assert(ClientAttachmentsSP.Active, 'ClientAttachmentsSP not active');

  if ClientAttachmentsSP.Filter <> '' then
    ClientAttachmentsSP.Filter := ClientAttachmentsSP.Filter + ' and ';
  ClientAttachmentsSP.Filter := ClientAttachmentsSP.Filter +
    'attachment_id <> ''' + IntToStr(AttachmentID) + '''';
end;

initialization
  RegisterReport(ReportName, TClientAttachmentsDM);
{
Report=ClientAttachments
CallCenters=OCC2,OCC3
ClientIDs=3773,3481,3378,3767,3776,2811,2514,3774,2162,1753,3482,2294,3379,3892,2947,1759,1751,2924,1754,3377
ForeignTypes=1,7
UploadStartDate=2010-01-01
UploadEndDate=2011-03-30
IncludeFileTypes=.jpg,.png
ExcludeFileNames=screenshot_c%,screenshot_e%,screenshot_g%,screenshot_w%
DestinationFolder=D:\temp
UseOrigFileName=0
}

end.
