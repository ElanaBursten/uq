select *,
  coalesce(init_est_cost,0) - coalesce(paid_amount,0) as init_remaining,
  case when inv_total>0 then inv_total
      else coalesce(init_est_cost,0) end as init_total_cost,
  coalesce(curr_est_cost, 0) - coalesce(paid_amount,0) as curr_remaining,
  case when inv_total>0 then inv_total
      else coalesce(curr_est_cost, 0) end as curr_total_cost,
 (coalesce(init_est_cost,0) - coalesce(paid_amount,0)) * ratio as init_accrual_amount,
 (coalesce(curr_est_cost,0) - coalesce(paid_amount,0)) * ratio as curr_accrual_amount
FROM
  (select
    d.uq_damage_id,
    d.damage_id,
    d.damage_type,
    d.damage_date,
    d.size_type,
    d.state,
    d.facility_type,
    d.facility_size,
    d.profit_center,
    d.utility_co_damaged,
    d.facility_material,
    d.location,
    d.city,
    d.exc_resp_code,
    d.uq_resp_code,
    d.spc_resp_code,
    (select top 1 amount from damage_estimate de2 where de2.modified_date =
    (select min(modified_date) from damage_estimate
         where damage_estimate.damage_id = d.damage_id
           and damage_estimate.amount>0)
     and de2.damage_id = d.damage_id) as init_est_cost,
    (select top 1 amount from damage_estimate de3 where de3.modified_date =
    (select max(modified_date) from damage_estimate
         where damage_estimate.damage_id = d.damage_id
           and damage_estimate.amount>0)
     and de3.damage_id = d.damage_id) as curr_est_cost,
    --(select count(*) from attachment a where a.foreign_id=d.damage_id and a.foreign_type=2 and a.active=1) as attachments,
    (select count(*) from damage_invoice i where i.damage_id=d.damage_id) as inv_count,
    coalesce((select SUM(i2.amount) from damage_invoice i2 where d.damage_id = i2.damage_id),0) as inv_total,
    -- 1568
    coalesce((select SUM(i2.paid_amount) from damage_invoice i2 where d.damage_id = i2.damage_id),0) as paid_amount,
    (select convert(decimal(5,2), r.modifier) from reference r where type='invcode' and code=d.invoice_code) as ratio,
    -- 1568
    (select description from reference where type='invcode' and code=d.invoice_code) as invoice_code_desc,
    (select top 1 invoice_num from damage_invoice di where di.damage_id=d.damage_id order by di.received_date) as invoice_num,
    (select top 1 cust_invoice_date from damage_invoice di where di.damage_id=d.damage_id order by di.received_date) as invoice_date,
    t.work_description,
    e.short_name,
    pc.pc_name office_name,
    locator.short_name as locator_name
from damage d
    left join ticket t on d.ticket_id=t.ticket_id
    left join employee e on d.investigator_id=e.emp_id
    left join employee_office eo on d.investigator_id=eo.emp_id
    left join employee locator on dbo.get_locator_for_damage(d.damage_id) = locator.emp_id
    left outer join profit_center pc on (pc.pc_code=d.profit_center)   --WHERE
  ) data
--ORDER
