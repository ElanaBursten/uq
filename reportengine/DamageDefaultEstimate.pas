unit DamageDefaultEstimate;

interface

uses
  SysUtils, Classes, BaseReport, ppModule, daDataModule, ppBands, ppClass,
  ppVar, ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageDefaultEstimateDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLabel15: TppLabel;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ProfitCenterLabel: TppLabel;
    UtilityCompanyLabel: TppLabel;
    FacilityTypeLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, StrUtils, OdMiscUtils;

{$R *.dfm}

{ TDamageDefaultEstimateDM }
procedure TDamageDefaultEstimateDM.Configure(QueryFields: TStrings);
var
  FacilityType: string;
  UtilityCo: string;
  ProfitCenter: string;
begin
  inherited;

  FacilityType := SafeGetString('FacilityType', '');
  UtilityCo := SafeGetString('UtilityCo', '');
  ProfitCenter := SafeGetString('ProfitCenter', '');

  with SP do begin
    Parameters.ParamByName('@FacilityType').value := FacilityType;
    Parameters.ParamByName('@UtilityCompany').value := UtilityCo;
    Parameters.ParamByName('@ProfitCenter').value := ProfitCenter;
    Open;
  end;

  ShowText(FacilityTypeLabel, IfThen(IsEmpty(FacilityType), 'All', FacilityType));
  ShowText(UtilityCompanyLabel, IfThen(IsEmpty(UtilityCo), 'All', UtilityCo));
  ShowText(ProfitCenterLabel, IfThen(IsEmpty(ProfitCenter), 'All', ProfitCenter));
end;

initialization
  RegisterReport('DamageDefaultEstimate', TDamageDefaultEstimateDM);

end.
