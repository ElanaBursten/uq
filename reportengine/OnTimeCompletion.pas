unit OnTimeCompletion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, daDataModule,
  ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TOnTimeCompletionDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppTitleBand1: TppTitleBand;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppLine1: TppLine;
    ppLabel13: TppLabel;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    DueDateLabel: TppLabel;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    tickets_due_sum: TppDBCalc;
    ticket_completed_sum: TppDBCalc;
    ppLabel7: TppLabel;
    ppLine2: TppLine;
    completion_percent_summary: TppDBCalc;
    ManagerLabel: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    tickets_initial_sum: TppDBCalc;
    tickets_initial_percent_summary: TppDBCalc;
    tickets_closed_sum: TppDBCalc;
    tickets_closed_percent_summary: TppDBCalc;
    CallCenterLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Detail: TADODataSet;
    Master: TADODataSet;
    procedure completion_percent_summaryCalc(Sender: TObject);
    procedure tickets_initial_percent_summaryCalc(Sender: TObject);
    procedure tickets_closed_percent_summaryCalc(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  private
    procedure CalcPercent(TotalVar, PercentVar: TppDBCalc);
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, odADOutils;

procedure TOnTimeCompletionDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  StartDueDate: TDateTime;
  EndDueDate: TDateTime;
  CallCenters: string;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := SafeGetInteger('ManagerID', -1);
  // clients before .9463 sent a single DueDate
  StartDueDate := GetDateTime('DueDate');
  EndDueDate := SafeGetDateTime('EndDueDate', StartDueDate + 1);
  CallCenters := GetString('CallCenters');
  with SP do begin
    Parameters.ParamByName('@StartDate').value := StartDueDate;
    Parameters.ParamByName('@EndDate').value := EndDueDate;
    Parameters.ParamByName('@ClientIDs').value := GetString('Clients');
    Parameters.ParamByName('@CallCenterCodes').value := CallCenters;
    Parameters.ParamByName('@ManagerID').value := ManagerID;
    Open;
  end;


  Master.Recordset  := SP.Recordset;
  Detail.Recordset  := SP.Recordset.NextRecordset(RecsAffected);


  ShowEmployeeName(ManagerLabel, ManagerID);
  if HaveParam('EndDueDate') then
    ShowDateRange(DueDateLabel, 'DueDate', 'EndDueDate', 'Due Date:')
  else
    ShowDateText(DueDateLabel, 'Due Date:', StartDueDate);
  ShowText(CallCenterLabel, 'Call Centers:', CallCenters);
end;

procedure TOnTimeCompletionDM.CalcPercent(TotalVar, PercentVar: TppDBCalc);
begin
  if tickets_due_sum.Value = 0 then
    PercentVar.Value := 0
  else
    PercentVar.Value := TotalVar.Value * 100.0 / tickets_due_sum.Value;
end;

procedure TOnTimeCompletionDM.completion_percent_summaryCalc(
  Sender: TObject);
begin
  inherited;
  CalcPercent(ticket_completed_sum, completion_percent_summary);
end;

procedure TOnTimeCompletionDM.tickets_initial_percent_summaryCalc(
  Sender: TObject);
begin
  inherited;
  CalcPercent(tickets_initial_sum, tickets_initial_percent_summary);
end;

procedure TOnTimeCompletionDM.tickets_closed_percent_summaryCalc(
  Sender: TObject);
begin
  inherited;
  CalcPercent(tickets_closed_sum, tickets_closed_percent_summary);
end;

initialization
  RegisterReport('OnTimeCompletion', TOnTimeCompletionDM);

end.

