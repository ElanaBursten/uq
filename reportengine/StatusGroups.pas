unit StatusGroups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppModule, daDataModule,
  ppCtrls, ppBands, ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv,
  ppProd, ppReport, ppStrtch, ppSubRpt, ppDesignLayer, ppParameter, 
  Data.Win.ADODB;

type
  TStatusGroupsDM = class(TBaseReportDM)
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    short_nameDBText: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    ByClientDS: TDataSource;
    ByGroupDS: TDataSource;
    ByClientDSPipe: TppDBPipeline;
    ByGroupPipe: TppDBPipeline;
    ppDBText2: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppTitleBand2: TppTitleBand;
    ppLabel7: TppLabel;
    daDataModule1: TdaDataModule;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    ByClient: TADODataSet;
    ByGroup: TADODataSet;
    Master: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, odAdoUtils;

{$R *.dfm}

{ TBaseReportDM1 }

procedure TStatusGroupsDM.Configure(QueryFields: TStrings);
var
  RecsAffected : oleVariant;
begin
  inherited;
  SP.Open;

 //QMANTWO-533 EB
  Master.Recordset := SP.Recordset;
  ByClient.Recordset := SP.Recordset.NextRecordset(RecsAffected);
  ByGroup.Recordset := SP.Recordset.NextRecordset(RecsAffected);
end;

initialization
  RegisterReport('StatusGroups', TStatusGroupsDM);

end.
