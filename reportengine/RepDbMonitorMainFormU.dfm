object RepDbMonitorMainForm: TRepDbMonitorMainForm
  Left = 310
  Top = 242
  Caption = 'Reporting Database Monitor'
  ClientHeight = 602
  ClientWidth = 854
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 854
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 703
      Height = 13
      Caption = 
        'Status of all reporting databases, every ~5 seconds, using Repor' +
        'tEngineCGI.ini in the same directory as this program.  Results l' +
        'ogged to dbmonitor.log'
    end
  end
  object Grid: TStringGrid
    Left = 0
    Top = 41
    Width = 854
    Height = 561
    Align = alClient
    DefaultColWidth = 140
    DefaultRowHeight = 18
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 1
  end
  object Timer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerTimer
    Left = 192
    Top = 192
  end
  object Conn: TADOConnection
    LoginPrompt = False
    Left = 112
    Top = 192
  end
end
