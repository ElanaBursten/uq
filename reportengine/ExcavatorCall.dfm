inherited ExcavatorCallDM: TExcavatorCallDM
  OldCreateOrder = True
  object SPDs: TDataSource
    DataSet = SP
    Left = 112
    Top = 24
  end
  object Pipe: TppDBPipeline
    DataSource = SPDs
    UserName = 'Pipe'
    Left = 192
    Top = 32
    object PipeppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'ticket_id'
      FieldName = 'ticket_id'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 0
      Position = 0
    end
    object PipeppField2: TppField
      FieldAlias = 'ticket_number'
      FieldName = 'ticket_number'
      FieldLength = 20
      DisplayWidth = 20
      Position = 1
    end
    object PipeppField3: TppField
      FieldAlias = 'work_type'
      FieldName = 'work_type'
      FieldLength = 90
      DisplayWidth = 90
      Position = 2
    end
    object PipeppField4: TppField
      FieldAlias = 'transmit_date'
      FieldName = 'transmit_date'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 3
    end
    object PipeppField5: TppField
      FieldAlias = 'due_date'
      FieldName = 'due_date'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 4
    end
    object PipeppField6: TppField
      FieldAlias = 'work_address_number'
      FieldName = 'work_address_number'
      FieldLength = 10
      DisplayWidth = 10
      Position = 5
    end
    object PipeppField7: TppField
      FieldAlias = 'work_address_number_2'
      FieldName = 'work_address_number_2'
      FieldLength = 10
      DisplayWidth = 10
      Position = 6
    end
    object PipeppField8: TppField
      FieldAlias = 'work_address_street'
      FieldName = 'work_address_street'
      FieldLength = 45
      DisplayWidth = 45
      Position = 7
    end
    object PipeppField9: TppField
      FieldAlias = 'work_city'
      FieldName = 'work_city'
      FieldLength = 40
      DisplayWidth = 40
      Position = 8
    end
    object PipeppField10: TppField
      FieldAlias = 'work_state'
      FieldName = 'work_state'
      FieldLength = 2
      DisplayWidth = 2
      Position = 9
    end
    object PipeppField11: TppField
      FieldAlias = 'con_name'
      FieldName = 'con_name'
      FieldLength = 50
      DisplayWidth = 50
      Position = 10
    end
    object PipeppField12: TppField
      FieldAlias = 'caller'
      FieldName = 'caller'
      FieldLength = 50
      DisplayWidth = 50
      Position = 11
    end
    object PipeppField13: TppField
      FieldAlias = 'caller_contact'
      FieldName = 'caller_contact'
      FieldLength = 50
      DisplayWidth = 50
      Position = 12
    end
    object PipeppField14: TppField
      FieldAlias = 'caller_phone'
      FieldName = 'caller_phone'
      FieldLength = 40
      DisplayWidth = 40
      Position = 13
    end
    object PipeppField15: TppField
      FieldAlias = 'client_code'
      FieldName = 'client_code'
      FieldLength = 10
      DisplayWidth = 10
      Position = 14
    end
    object PipeppField16: TppField
      FieldAlias = 'short_name'
      FieldName = 'short_name'
      FieldLength = 20
      DisplayWidth = 20
      Position = 15
    end
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Excavator Call List'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 272
    Top = 32
    Version = '18.03'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 15610
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 15610
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 0
        LayerName = Foreground
      end
      object ppReportHeaderLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Excavator Call List'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 5292
        mmTop = 4498
        mmWidth = 196850
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 67469
      mmPrintPosition = 0
      object ppLabel3: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label3'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'ticket_number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 1852
        mmWidth = 43392
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        HyperlinkEnabled = False
        DataField = 'ticket_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 6879
        mmWidth = 43392
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label4'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'transmit_date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 46567
        mmTop = 1852
        mmWidth = 39158
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText2'
        HyperlinkEnabled = False
        DataField = 'transmit_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 46567
        mmTop = 6879
        mmWidth = 39158
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label5'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'due_date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 87048
        mmTop = 2117
        mmWidth = 39158
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText5: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText5'
        HyperlinkEnabled = False
        DataField = 'due_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 87048
        mmTop = 7144
        mmWidth = 39158
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label6'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_address_number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 128059
        mmTop = 1588
        mmWidth = 30956
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText6: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText6'
        HyperlinkEnabled = False
        DataField = 'work_address_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 128059
        mmTop = 6615
        mmWidth = 30956
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label7'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_address_number_2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 161661
        mmTop = 1852
        mmWidth = 34131
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText7: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText7'
        HyperlinkEnabled = False
        DataField = 'work_address_number_2'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 161661
        mmTop = 6879
        mmWidth = 34131
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel8: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label8'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_address_street'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 794
        mmTop = 16669
        mmWidth = 96309
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText8: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText8'
        HyperlinkEnabled = False
        DataField = 'work_address_street'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 794
        mmTop = 21696
        mmWidth = 96309
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel9: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label9'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_city'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 98690
        mmTop = 16669
        mmWidth = 85725
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText9: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText9'
        HyperlinkEnabled = False
        DataField = 'work_city'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 98690
        mmTop = 21696
        mmWidth = 85725
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label10'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_state'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 185473
        mmTop = 16933
        mmWidth = 15610
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText10: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText10'
        HyperlinkEnabled = False
        DataField = 'work_state'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 185473
        mmTop = 21960
        mmWidth = 15610
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel11: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label11'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'work_type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 31221
        mmWidth = 191559
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText11: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText11'
        HyperlinkEnabled = False
        DataField = 'work_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 36248
        mmWidth = 191559
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel12: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label12'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'caller_contact'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 42333
        mmWidth = 106892
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText12: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText12'
        HyperlinkEnabled = False
        DataField = 'caller_contact'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 48154
        mmWidth = 106892
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel14: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label14'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'short_name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 65617
        mmTop = 56886
        mmWidth = 43392
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText13: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText13'
        HyperlinkEnabled = False
        DataField = 'short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 65617
        mmTop = 61913
        mmWidth = 43392
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel15: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label15'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'client_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 40217
        mmTop = 56886
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText14: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText14'
        HyperlinkEnabled = False
        DataField = 'client_code'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 40217
        mmTop = 61913
        mmWidth = 22225
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 13229
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label13'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 1852
        mmTop = 2910
        mmWidth = 47625
        BandType = 8
        LayerName = Foreground
      end
      object ppReportCopyright: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Excavator Call List'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 57944
        mmTop = 6350
        mmWidth = 91281
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 169069
        mmTop = 3969
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 179917
        mmTop = 8467
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppGroup1: TppGroup
      BreakName = 'ticket_id'
      DataPipeline = Pipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      KeepTogether = True
      OutlineSettings.CreateNode = True
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'Pipe'
      NewFile = False
      object ppGroupHeaderBand1: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 29633
        mmPrintPosition = 0
        object ppDBText3: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText3'
          HyperlinkEnabled = False
          DataField = 'con_name'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 11642
          mmTop = 8202
          mmWidth = 106892
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel1: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label1'
          HyperlinkEnabled = False
          AutoSize = False
          Caption = 'con_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 11642
          mmTop = 2910
          mmWidth = 106892
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel2: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label2'
          HyperlinkEnabled = False
          AutoSize = False
          Caption = 'caller_phone'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          mmHeight = 3704
          mmLeft = 61383
          mmTop = 14288
          mmWidth = 85725
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText4: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText4'
          HyperlinkEnabled = False
          DataField = 'caller_phone'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 61383
          mmTop = 19315
          mmWidth = 85725
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Background.Brush.Style = bsClear
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 11377
        mmPrintPosition = 0
      end
    end
    object daDataModule1: TdaDataModule
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object SP: TADOQuery
    Connection = ReportEngineDM.Conn
    Parameters = <>
    SQL.Strings = (
      
        'select top 50 t.ticket_id, t.ticket_number, t.work_type, t.trans' +
        'mit_date,'
      
        '  t.due_date, t.work_address_number, t.work_address_number_2, t.' +
        'work_address_street,'
      
        '  t.work_city, t.work_state, t.con_name, t.caller, t.caller_cont' +
        'act, t.caller_phone,'
      '  l.client_code,'
      '  e.short_name'
      ' from ticket t'
      '  inner join locate l on t.ticket_id=l.ticket_id'
      '  inner join employee e on l.assigned_to=e.emp_id'
      ' where l.closed=0'
      '  and t.ticket_format='#39'FCL1'#39
      '  and t.due_date < '#39'2002-08-28'#39
      ' order by t.con_name, t.caller_phone')
    Left = 32
    Top = 24
  end
end
