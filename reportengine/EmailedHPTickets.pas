unit EmailedHPTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB,
  ppDBPipe, DB, ppPrnabl, ppCtrls, ppBands, ppCache, ppVar,
  ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TEmailedHPTicketsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ticket_number: TppDBText;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    PrintDateLabel: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppLabel4: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    CallCenterLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  EmailedHPTicketsDM: TEmailedHPTicketsDM;

implementation

uses ReportEngineDMu, OdDbUtils;

{$R *.dfm}

procedure TEmailedHPTicketsDM.Configure(QueryFields: TStrings);
var
  CallCenter: String;
begin
  inherited;

  CallCenter := GetString('CallCenter');

  with SP do begin
    Parameters.ParamByName('@Call_Center').value := CallCenter;
    Open;
  end;

  ShowText(CallCenterLabel, CallCenter);
end;

initialization
  RegisterReport('EmailedHPTickets', TEmailedHPTicketsDM);

end.

