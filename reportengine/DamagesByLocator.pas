unit DamagesByLocator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppVar, ppBands, ppCtrls, ppPrnabl,
  ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport, DB, ppStrtch,
  ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamagesByLocatorDM = class(TBaseReportDM)
    DamagesDS: TDataSource;
    Report: TppReport;
    DamagesPipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppTitleBand1: TppTitleBand;
    ppGroup1: TppGroup;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText7: TppDBText;
    ppLabel7: TppLabel;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppDBText9: TppDBText;
    ppLabel9: TppLabel;
    ppDBText10: TppDBText;
    ppLabel10: TppLabel;
    ppLine4: TppLine;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppLabel6: TppLabel;
    ppDBText5: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppLine1: TppLine;
    ppLabel11: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppLine2: TppLine;
    ppLabel14: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppLine3: TppLine;
    ppLabel15: TppLabel;
    ppDBText6: TppDBText;
    MemoRespCode: TppMemo;
    ppLabel16: TppLabel;
    DateRangeLabel: TppLabel;
    LiabilityLabel: TppLabel;
    ppReportHeaderLabel: TppLabel;
    ProfitCenterLabel: TppLabel;
    ppShape1: TppShape;
    EmployeeLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Damages: TADOStoredProc;
    procedure MemoRespCodePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdMiscUtils, QMConst, OdIsoDates, StrUtils;

procedure TDamagesByLocatorDM.Configure(QueryFields: TStrings);
var
  DateFrom, DateTo: TDateTime;
  ProfitCenter: string;
  EmployeeID: Integer;
  EmployeeStatus: Integer;
  SingleEmployee: Boolean;
begin
  inherited;
  DateFrom := GetDateTime('DateFrom');
  DateTo := GetDateTime('DateTo');
  ProfitCenter := SafeGetString('ProfitCenter', '');
  EmployeeID := SafeGetInteger('EmployeeID', 0);
  EmployeeStatus := SafeGetInteger('EmployeeStatus', StatusActive);
  SingleEmployee := SafeGetBoolean('SingleEmployee', False);

  Damages.Parameters.ParamByName('@DateFrom').value := DateFrom;
  Damages.Parameters.ParamByName('@DateTo').value := DateTo;
  Damages.Parameters.ParamByName('@ProfitCenter').value := ProfitCenter;
  Damages.Parameters.ParamByName('@LiabilityType').value := SafeGetInteger('Liability', 5);
  // If no liab is specified, default to 5 = ALL
  Damages.Parameters.ParamByName('@EmployeeID').value := EmployeeID;
  Damages.Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
  Damages.Parameters.ParamByName('@SingleEmployee').value := SingleEmployee;
  Damages.Open;

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  ShowText(ProfitCenterLabel, IfThen(ProfitCenter='', 'All', ProfitCenter));
  ShowText(LiabilityLabel, GetLiabilityString(SafeGetInteger('Liability', -1)));
  ShowEmployeeName(EmployeeLabel, EmployeeID, IfThen(SingleEmployee, 'Employee:', 'All Under Manager:'));
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
end;

procedure TDamagesByLocatorDM.MemoRespCodePrint(Sender: TObject);
const
  UQ = 'uq';
  EXC = 'exc';
  SPC = 'spc';

  function GetRespText(Who: string): string;
  begin
    Result := Damages.FieldByName(Who + '_resp_code').AsString;
    if IsEmpty(Result) then
      Result := 'Other';
    if not IsEmpty(Damages.FieldByName(Who + '_resp_other_desc').AsString) then
      Result := Result + '(' + Damages.FieldByName(Who + '_resp_other_desc').AsString + ')'
    else
      if Result = 'Other' then
        Result := '';
  end;
var
  Text: String;
begin
  Text := GetRespText(UQ) + ' ';
  Text := Text + GetRespText(EXC) + ' ';
  Text := Text + GetRespText(SPC) + ' ';
  Text := Trim(Text);
  MemoRespCode.Lines.Text := Text;
end;

initialization
  RegisterReport('DamagesByLocator', TDamagesByLocatorDM);

end.

