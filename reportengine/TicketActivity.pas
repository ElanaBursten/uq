unit TicketActivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, ppCtrls, ppBands, ppClass, ppVar, ppPrnabl,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB,
  ppStrtch, ppSubRpt, ppRegion, ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTicketActivityDM = class(TBaseReportDM)
    Report: TppReport;
    ppTitleBand1: TppTitleBand;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppRegion1: TppRegion;
    ppLabel4: TppLabel;
    ppLabel10: TppLabel;
    ppLabel5: TppLabel;
    ppLabel21: TppLabel;
    StatusColumnLabel: TppLabel;
    ppLabel12: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText7: TppDBText;
    ppDBText9: TppDBText;
    ppDBText15: TppDBText;
    ppDBText18: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel22: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    ReportToName: TppDBText;
    EmployeeStatusLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses OdMiscUtils;

procedure TTicketActivityDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmployeeStatus: Integer;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  EmployeeStatus := GetInteger('EmployeeStatus');
  SP.Parameters.ParamByName('@ManagerID').Value := ManagerID;
  SP.Parameters.ParamByName('@StartDate').Value := GetDateTime('StartDate');
  SP.Parameters.ParamByName('@EndDate').Value := GetDateTime('EndDate');
  SP.Parameters.ParamByName('@EmployeeStatus').Value := EmployeeStatus;
  SP.Open;

  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
  ShowDateRange(DateRangeLabel, 'StartDate', 'EndDate', 'Activity Date Range');
end;

initialization
  RegisterReport('TicketActivity', TTicketActivityDM);

end.
