unit LocatesCompletedSummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppCtrls, ppBands, ppVar, ppStrtch, ppRegion, ppPrnabl,
  ppClass, ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB,
  ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TLocatesCompletedSummaryDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    HeaderBand: TppHeaderBand;
    ReportHeaderShape: TppShape;
    ReportTitle: TppLabel;
    ManagerLabel: TppLabel;
    DetailBand: TppDetailBand;
    FooterBand: TppFooterBand;
    ppReportFooterShape1: TppShape;
    LocatingCompanyLabel: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    SummaryBand: TppSummaryBand;
    Company: TppDBText;
    ProfitCenter: TppDBText;
    EmpTitle: TppDBText;
    WorkDate: TppDBText;
    MarkedCount: TppDBText;
    ClearedCount: TppDBText;
    ClosedCount: TppDBText;
    DateRangeLabel: TppLabel;
    EmployeeTypesLabel: TppLabel;
    CompanyGroup: TppGroup;
    CompanyGroupHeaderBand: TppGroupHeaderBand;
    CompanyGroupFooterBand: TppGroupFooterBand;
    PCGroup: TppGroup;
    PCGroupHeaderBand: TppGroupHeaderBand;
    PCGroupFooterBand: TppGroupFooterBand;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    EmpTypeGroup: TppGroup;
    EmpTypeHeaderBand: TppGroupHeaderBand;
    EmpTypeFooterBand: TppGroupFooterBand;
    MarkedCountColTitle: TppLabel;
    LocatesColTitle: TppLabel;
    ClearedCountColTitle: TppLabel;
    ClosedCountColTitle: TppLabel;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    CompanyContinuedLabel: TppLabel;
    PCContinuedLabel: TppLabel;
    EmpTypeContinuedLabel: TppLabel;
    ppLabel3: TppLabel;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc9: TppDBCalc;
    ppShape1: TppShape;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLine1: TppLine;
    ppShape2: TppShape;
    ppLine2: TppLine;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    procedure PCGroupHeaderBandBeforePrint(Sender: TObject);
    procedure EmpTypeHeaderBandBeforePrint(Sender: TObject);
    procedure CompanyGroupHeaderBandBeforePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  LocatesCompletedSummaryDM: TLocatesCompletedSummaryDM;

implementation

{$R *.dfm}

uses  ReportEngineDMu, OdMiscUtils, odADOutils, OdDbUtils;

procedure TLocatesCompletedSummaryDM.Configure(QueryFields: TStrings);
var
  StartDate: TDateTime;
  EndDate: TDateTime;
  ManagerID: Integer;
  EmployeeTypes: string;
  EmpTypes: TADODataset;
  EmpTypeText: string;
  RecsAffected : oleVariant;
begin
  inherited;
  StartDate := GetDateTime('StartDate');
  EndDate := GetDateTime('EndDate');
  ManagerID := GetInteger('ManagerID');
  EmployeeTypes := GetString('EmployeeTypes');
  if EmployeeTypes <> '*' then
    ValidateIntegerCommaSeparatedList(EmployeeTypes);

  SP.Parameters.ParamByName('@StartDate').value := StartDate;
  SP.Parameters.ParamByName('@EndDate').value := EndDate;
  SP.Parameters.ParamByName('@ManagerID').value := ManagerID;
  SP.Parameters.ParamByName('@EmployeeTypes').value := EmployeeTypes;
  SP.Open;

  EmpTypes := TADODataset.Create(nil);
  try

    Master.recordset := SP.Recordset;
    EmpTypes.Recordset := SP.Recordset.NextRecordset(RecsAffected);
    
    ShowEmployeeName(ManagerLabel, ManagerId);
    ShowDateRange(DateRangeLabel);
    EmpTypeText := GetDataSetFieldPlainString(EmpTypes, 'employee_type');    
    ShowText(EmployeeTypesLabel, 'Employee Types:', EmpTypeText);
  finally 
    FreeAndNil(EmpTypes);  
  end;  
end;

procedure TLocatesCompletedSummaryDM.CompanyGroupHeaderBandBeforePrint(Sender: TObject);
begin
  CompanyContinuedLabel.Visible := not CompanyGroup.FirstPage;
end;

procedure TLocatesCompletedSummaryDM.PCGroupHeaderBandBeforePrint(Sender: TObject);
begin
  PCContinuedLabel.Visible := not PCGroup.FirstPage;
end;

procedure TLocatesCompletedSummaryDM.EmpTypeHeaderBandBeforePrint(Sender: TObject);
begin
  EmpTypeContinuedLabel.Visible := not EmpTypeGroup.FirstPage;
end;

initialization
  RegisterReport('LocatesCompletedSummary', TLocatesCompletedSummaryDM);

end.
