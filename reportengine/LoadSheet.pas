unit LoadSheet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TLoadSheetDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateLabel: TppLabel;
    ManagerLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppLabel2: TppLabel;
    ppDBText4: TppDBText;
    ppLabel3: TppLabel;
    ppDBText5: TppDBText;
    ppShape1: TppShape;
    EmployeeCountLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, QMConst, OdMiscUtils, JclStrings;

procedure TLoadSheetDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpCount: Integer;
  TodayDate: TDateTime;
  LocatorsList: string;
begin
  inherited;
  TodayDate := Date;
  ManagerID := GetInteger('ManagerId');
  LocatorsList := GetString('Locators');
  EmpCount := StrCharCount(LocatorsList, ',') + 1;
  with SP do begin
    Parameters.ParamByName('@Today').value := TodayDate;
    Parameters.ParamByName('@LocatorList').value := LocatorsList;
    Open;
  end;
  ShowDateText(DateLabel, 'Date:', TodayDate);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowText(EmployeeCountLabel,
    Format('%d %s Selected', [EmpCount, AddSIfNot1(EmpCount, 'Employee')]));
end;

initialization
  RegisterReport('LoadSheet', TLoadSheetDM);

end.


