unit DamageEstimateChanges;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB, ADODB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, jpeg,
  OdContainer, daDataModule, DBClient,
  // We static link to midas.dll for the report engine to simplify distribution
  MidasLib;

type
  TDamageEstimateChangesDM = class(TBaseReportDM)
    NewDamagesSP: TADOStoredProc;
    NewDamagesDS: TDataSource;
    Report: TppReport;
    NewDamagesPipe: TppDBPipeline;
    DetailDS: TDataSource;
    DetailPipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppLabel10: TppLabel;
    FromDateLabel: TppLabel;
    ppLabel12: TppLabel;
    ToDateLabel: TppLabel;
    MainDetailBand: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    DetailSP: TADOStoredProc;
    NewEstimatesSP: TADOStoredProc;
    NewEstimatesDS: TDataSource;
    NewEstimatesPipe: TppDBPipeline;
    NewDamages: TppSubReport;
    ppChildReport1: TppChildReport;
    ChangedDamages: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDetailBand3: TppDetailBand;
    ppLabel21: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    NewDamageClaimsGroupFooterBand: TppGroupFooterBand;
    ppRegion1: TppRegion;
    ppLabel6: TppLabel;
    ppLabel22: TppLabel;
    ppLabel1: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    NewEstimatesGroupFooterBand: TppGroupFooterBand;
    ppRegion2: TppRegion;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    NewDamageDetails: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppLabel19: TppLabel;
    ppDBText21: TppDBText;
    ppDBText26: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    NewDamagesFooterBand: TppGroupFooterBand;
    ChangedDamageDetails: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand5: TppDetailBand;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    NewEstimatesFooterBand: TppGroupFooterBand;
    ppDBText25: TppDBText;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppDBText29: TppDBText;
    ppLabel2: TppLabel;
    ppDBText32: TppDBText;
    ppShape1: TppShape;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppShape2: TppShape;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLabel17: TppLabel;
    ppShape4: TppShape;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    TotalEstimatesNew: TppVariable;
    PreviousEstimatesEx: TppVariable;
    CurrentEstimatesEx: TppVariable;
    DifferenceEx: TppVariable;
    ppLine4: TppLine;
    StateLabel: TppLabel;
    StateValue: TppLabel;
    ProfitCenterValue: TppLabel;
    ProfitCenterLabel: TppLabel;
    LiabilityValue: TppLabel;
    LiabilityLabel: TppLabel;
    InvoiceDS: TDataSource;
    InvoiceSP: TADODataSet;
    InvoicePipe: TppDBPipeline;
    NewDamageInvoices: TppSubReport;
    ppChildReport5: TppChildReport;
    NewInvoiceDetail: TppDetailBand;
    ppTitleBand1: TppTitleBand;
    ppSummaryBand1: TppSummaryBand;
    ppDBText33: TppDBText;
    ppLabel33: TppLabel;
    NewInvoicedAmount: TppDBText;
    ppLabel34: TppLabel;
    ppDBText35: TppDBText;
    ppLabel35: TppLabel;
    ppDBText36: TppDBText;
    ppLabel36: TppLabel;
    NewPaidAmount: TppDBText;
    ppLabel37: TppLabel;
    ppDBText38: TppDBText;
    ppLabel38: TppLabel;
    ppDBText39: TppDBText;
    ppLabel39: TppLabel;
    ppLabel32: TppLabel;
    ppShape5: TppShape;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLine5: TppLine;
    ChangedDamageInvoices: TppSubReport;
    ppChildReport6: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppShape6: TppShape;
    ppLabel42: TppLabel;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppLabel45: TppLabel;
    ppLabel46: TppLabel;
    ppLabel47: TppLabel;
    ppLabel48: TppLabel;
    ChangedInvoiceDetail: TppDetailBand;
    ppDBText40: TppDBText;
    ChangedInvoiceAmount: TppDBText;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    ChangedInvoicePaid: TppDBText;
    ppDBText45: TppDBText;
    ppDBText46: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel49: TppLabel;
    ppLabel50: TppLabel;
    ppLabel51: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppLine6: TppLine;
    ppLabel55: TppLabel;
    ppLabel59: TppLabel;
    ppShape3: TppShape;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    ppLine3: TppLine;
    ChangedInvoiceTotal: TppVariable;
    ChangedPaidTotal: TppVariable;
    InvoiceTotalNew: TppVariable;
    PaidTotalNew: TppVariable;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    NewInvoiceTotal: TppVariable;
    NewPaidTotal: TppVariable;
    InvoiceTotalChanged: TppVariable;
    PaidTotalChanged: TppVariable;
    NewRespDS: TDataSource;
    NewRespPipe: TppDBPipeline;
    NoRespDS: TDataSource;
    NoRespPipe: TppDBPipeline;
    NoRespSP: TADOStoredProc;
    NewRespSP: TADOStoredProc;
    ChangedToUQ: TppSubReport;
    ppChildReport7: TppChildReport;
    ChangedFromUQ: TppSubReport;
    ppChildReport8: TppChildReport;
    ChangedToUQTitle: TppTitleBand;
    ChangedToUQDetail: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppLabel20: TppLabel;
    ppLabel52: TppLabel;
    ppLabel53: TppLabel;
    ppLabel54: TppLabel;
    ppLabel56: TppLabel;
    ppLabel57: TppLabel;
    ppLabel58: TppLabel;
    ppLabel60: TppLabel;
    ppLabel61: TppLabel;
    R: TppRegion;
    ppLabel62: TppLabel;
    EstAsOf1: TppLabel;
    ToUQDamageIDLabel: TppDBText;
    ppDBText37: TppDBText;
    ppDBText41: TppDBText;
    ppDBText44: TppDBText;
    ppDBText47: TppDBText;
    ppDBText48: TppDBText;
    ppDBText49: TppDBText;
    ppDBText50: TppDBText;
    ppDBText51: TppDBText;
    ppDBText52: TppDBText;
    ppLabel64: TppLabel;
    ChangedToUQTotal: TppDBCalc;
    ppLine7: TppLine;
    ppDetailBand7: TppDetailBand;
    ChangedFromUQTitle: TppTitleBand;
    ppSummaryBand4: TppSummaryBand;
    ppLine8: TppLine;
    ppLabel76: TppLabel;
    ChangedFromUQTotal: TppDBCalc;
    ppLabel77: TppLabel;
    NotifiedFromLabel: TppLabel;
    ppLabel79: TppLabel;
    NotifiedToLabel: TppLabel;
    ppLabel81: TppLabel;
    DamageFromLabel: TppLabel;
    ppLabel83: TppLabel;
    DamageToLabel: TppLabel;
    DummyVar1: TppVariable;
    ppGroup5: TppGroup;
    ppGroupHeaderBand5: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    DummyVar2: TppVariable;
    ppGroup6: TppGroup;
    ppGroupHeaderBand6: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ChangedDamagesTitle: TppTitleBand;
    ppSummaryBand5: TppSummaryBand;
    ppLabel78: TppLabel;
    ppDBText34: TppDBText;
    ppRegion3: TppRegion;
    ppLabel65: TppLabel;
    ppLabel66: TppLabel;
    ppLabel67: TppLabel;
    ppLabel68: TppLabel;
    ppLabel69: TppLabel;
    ppLabel70: TppLabel;
    ppLabel71: TppLabel;
    ppLabel72: TppLabel;
    ppLabel73: TppLabel;
    ppLabel74: TppLabel;
    ppLabel80: TppLabel;
    ppDBText53: TppDBText;
    ppDBText54: TppDBText;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppDBText57: TppDBText;
    ppDBText58: TppDBText;
    ppDBText59: TppDBText;
    ppDBText60: TppDBText;
    ppDBText61: TppDBText;
    ppDBText62: TppDBText;
    ppDBText63: TppDBText;
    ppLabel82: TppLabel;
    ppLine9: TppLine;
    TotalAccrualChange: TppVariable;
    GrandTotalsRegion: TppRegion;
    NewRespData: TClientDataSet;
    EstAsOf2: TppLabel;
    procedure NewDamagesDSDataChange(Sender: TObject; Field: TField);
    procedure NewEstimatesDSDataChange(Sender: TObject; Field: TField);
    procedure NewDamageClaimsGroupFooterBandBeforePrint(Sender: TObject);
    procedure NewEstimatesGroupFooterBandBeforePrint(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure PaidTotalNewCalc(Sender: TObject; var Value: Variant);
    procedure InvoiceTotalNewCalc(Sender: TObject; var Value: Variant);
    procedure ChangedInvoiceTotalCalc(Sender: TObject; var Value: Variant);
    procedure ChangedPaidTotalCalc(Sender: TObject; var Value: Variant);
    procedure InvoiceDSDataChange(Sender: TObject; Field: TField);
    procedure MainDetailBandBeforePrint(Sender: TObject);
    procedure TotalAccrualChangePrint(Sender: TObject);
  private
    FChangedInvoices: Boolean;
    function GetLiabilityString: string;
    procedure GetDamageInvoiceData(DamageId: Integer);
    procedure CalculateInvoiceTotals;
  protected
    FDateFrom             : TDateTime;
    FDateTo               : TDateTime;
    FDamageIdListNew      : TIntegerList;
    FDamageIdListEx       : TIntegerList;
    FPaidIDListNew        : TIntegerList;
    FPaidIDListEx         : TIntegerList;
    FInvoiceIDListNew     : TIntegerList;
    FInvoiceIDListEx      : TIntegerList;
    FDamageIDFilterList   : TIntegerList;
    FPreviousEstimatesNew : Double;
    FCurrentEstimatesNew  : Double;
    FPreviousEstimatesEx  : Double;
    FCurrentEstimatesEx   : Double;
    FNewInvoiceTotal      : Double;
    FNewPaidTotal         : Double;
    FChangedInvoiceTotal  : Double;
    FChangedPaidTotal     : Double;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateTSVStream(Str : TStream); override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdDbUtils, OdMiscUtils, OdCdsUtils, QMConst, ppTypes;

{ TDamageEstimateChangesDM }

function TDamageEstimateChangesDM.GetLiabilityString: string;
var
  LiabilityInt: Integer;
begin
  LiabilityInt := SafeGetInteger('Liability', -1);
  if (LiabilityInt < Ord(Low(TDamageLiability))) or (LiabilityInt > Ord(High(TDamageLiability))) then
    Result := ''
  else begin
    Result := DamageLiabilityDescription[TDamageLiability(LiabilityInt)];
  end;
end;

procedure TDamageEstimateChangesDM.Configure(QueryFields: TStrings);
var
  StateAbbrev : string[2];
  ProfitCenter: string;
  BeginningOfTime: TDateTime;
  EndOfTime: TDateTime;
  NotifiedBegin: TDateTime;
  NotifiedEnd: TDateTime;
  DamageBegin: TDateTime;
  DamageEnd: TDateTime;
  LiabilityType: Integer;
begin
  inherited;
  BeginningOfTime := 0;
  EndOfTime := MaxDateTime;

  InvoiceSP.Prepared := True;

  FDateFrom := GetDateTime('DateFrom');
  FDateTo := GetDateTime('DateTo');
  StateAbbrev := SafeGetString('State', '');
  ProfitCenter := SafeGetString('ProfitCenter', '');
  NotifiedBegin := SafeGetDateTime('NotifiedDateFrom', BeginningOfTime);
  NotifiedEnd := SafeGetDateTime('NotifiedDateTo', EndOfTime);
  DamageBegin := SafeGetDateTime('DamageDateFrom', BeginningOfTime);
  DamageEnd := SafeGetDateTime('DamageDateTo', EndOfTime);
  LiabilityType := GetInteger('Liability');

  if LiabilityType<0 then
    LiabilityType := 5;

  with NewDamagesSP do begin
    if HaveParam('NotifiedDateFrom') then begin
      Parameters.ParamValues['@RecdStart'] := NotifiedBegin;
      Parameters.ParamValues['@RecdEnd'] := NotifiedEnd;
    end else begin
      Parameters.ParamValues['@RecdStart'] := FDateFrom;
      Parameters.ParamValues['@RecdEnd'] := FDateTo;
    end;
    Parameters.ParamValues['@EstStart'] := FDateFrom;
    Parameters.ParamValues['@EstEnd'] := FDateTo;
    Parameters.ParamValues['@State'] := StateAbbrev;
    Parameters.ParamValues['@ProfitCenter'] := ProfitCenter;
    Parameters.ParamValues['@LiabilityType'] := LiabilityType;
    Parameters.ParamValues['@DmgStart'] := DamageBegin;
    Parameters.ParamValues['@DmgEnd'] := DamageEnd;
    Open;
  end;

  with NewEstimatesSP do begin
    if HaveParam('NotifiedDateFrom') then begin
      // Intersect the implicit notified date range with the user-specified range
      if NotifiedBegin <= FDateFrom then
        Parameters.ParamValues['@RecdStart'] := NotifiedBegin
      else
        Parameters.ParamValues['@RecdStart'] := FDateFrom;
      if NotifiedEnd <= FDateFrom then
        Parameters.ParamValues['@RecdEnd'] := NotifiedEnd
      else
        Parameters.ParamValues['@RecdEnd'] := FDateFrom;
      if Parameters.ParamValues['@RecdStart'] > Parameters.ParamValues['@RecdEnd'] then begin
        // Do not send an invalid date range
        Parameters.ParamValues['@RecdStart'] := BeginningOfTime;
        Parameters.ParamValues['@RecdEnd'] := BeginningOfTime
      end;
    end else begin
      Parameters.ParamValues['@RecdStart'] := BeginningOfTime;
      Parameters.ParamValues['@RecdEnd'] := FDateFrom;
    end;
    Parameters.ParamValues['@EstStart'] := FDateFrom;
    Parameters.ParamValues['@EstEnd'] := FDateTo;
    Parameters.ParamValues['@State'] := StateAbbrev;
    Parameters.ParamValues['@ProfitCenter'] := ProfitCenter;
    Parameters.ParamValues['@LiabilityType'] := LiabilityType;
    Parameters.ParamValues['@DmgStart'] := DamageBegin;
    Parameters.ParamValues['@DmgEnd'] := DamageEnd;
    Open;
  end;

  // RB 6.03 does not work with datasets using OnFilterRecord, so we
  // pre-populate the changed damage estimate list here
  NewEstimatesDS.OnDataChange := nil;
  FDamageIDFilterList.Clear;
  while not NewEstimatesSP.Eof do begin
    FDamageIDFilterList.AddIfNeeded(NewEstimatesSP.FieldByName('damage_id').AsInteger);
    NewEstimatesSP.Next;
  end;
  NewEstimatesDS.OnDataChange := NewEstimatesDSDataChange;
  NewEstimatesSP.First;

  with NewRespSP do begin
    Parameters.ParamValues['@FromDate'] := FDateFrom;
    Parameters.ParamValues['@ToDate'] := FDateTo;
    Parameters.ParamValues['@State'] := StateAbbrev;
    Parameters.ParamValues['@ProfitCenter'] := ProfitCenter;
    Parameters.ParamValues['@ChangedToUQ'] := True;
    Parameters.ParamValues['@RecdStart'] := NotifiedBegin;
    Parameters.ParamValues['@RecdEnd'] := NotifiedEnd;
    Parameters.ParamValues['@DmgStart'] := DamageBegin;
    Parameters.ParamValues['@DmgEnd'] := DamageEnd;
    Open;
  end;

  // Generate a new in-memory dataset with non-duplicates only
  NewEstimatesDS.OnDataChange := nil;
  CloneDataSet(NewRespSP, NewRespData);
  NewRespData.First;
  while not NewRespData.Eof do begin
    if FDamageIDFilterList.Has(NewRespData.FieldByName('uq_damage_id').AsInteger) then
      NewRespData.Delete
    else
      NewRespData.Next;
  end;
  NewEstimatesDS.OnDataChange := NewEstimatesDSDataChange;
  NewEstimatesSP.First;

  with NoRespSP do begin
    Parameters.ParamValues['@FromDate'] := FDateFrom;
    Parameters.ParamValues['@ToDate'] := FDateTo;
    Parameters.ParamValues['@State'] := StateAbbrev;
    Parameters.ParamValues['@ProfitCenter'] := ProfitCenter;
    Parameters.ParamValues['@ChangedToUQ'] := False;
    Parameters.ParamValues['@RecdStart'] := NotifiedBegin;
    Parameters.ParamValues['@RecdEnd'] := NotifiedEnd;
    Parameters.ParamValues['@DmgStart'] := DamageBegin;
    Parameters.ParamValues['@DmgEnd'] := DamageEnd;
    Open;
  end;

  FromDateLabel.Caption := DateToStr(FDateFrom);
  ToDateLabel.Caption := DateToStr(FDateTo);
  DamageFromLabel.Caption := DateToStr(DamageBegin);
  DamageToLabel.Caption := DateToStr(DamageEnd);
  NotifiedFromLabel.Caption := DateToStr(NotifiedBegin);
  NotifiedToLabel.Caption := DateToStr(NotifiedEnd);
  StateValue.Caption := StateAbbrev;
  ProfitCenterValue.Caption := ProfitCenter;
  LiabilityValue.Caption := GetLiabilityString;
  EstAsOf1.Caption := 'Estimate as of ' + DateToStr(FDateTo);
  EstAsOf2.Caption := EstAsOf1.Caption;
end;

procedure TDamageEstimateChangesDM.NewDamagesDSDataChange(Sender: TObject; Field: TField);
var
  DamageId : Integer;
begin
  DetailSP.Close;
  if not NewDamagesSP.Eof then begin
    DamageId := NewDamagesSP.FieldValues['damage_id'];
    DetailSP.Parameters.ParamValues['@DamageId'] := DamageId;
    DetailSP.Parameters.ParamValues['@EstStart'] := FDateFrom;
    DetailSP.Parameters.ParamValues['@EstEnd'] := FDateTo;
    DetailSP.Open;

    //this will return True if this DamageId had to be added to the list
    if FDamageIdListNew.AddIfNeeded(DamageId) then begin
      //update totals
      FPreviousEstimatesNew :=
        FPreviousEstimatesNew + VarToFloat(DetailSP.FieldValues['InitialEstimate']);
      FCurrentEstimatesNew :=
        FCurrentEstimatesNew + VarToFloat(DetailSP.FieldValues['CurrentEstimate']);
    end;
    FChangedInvoices := False;
    GetDamageInvoiceData(DamageId);
  end;
end;

procedure TDamageEstimateChangesDM.NewEstimatesDSDataChange(
  Sender: TObject; Field: TField);
var
  DamageId : Integer;
begin
  DetailSP.Close;
  if not NewEstimatesSP.Eof then begin
    DamageId := VarToInt(NewEstimatesSP.FieldValues['damage_id']);
    DetailSP.Parameters.ParamValues['@DamageId'] := DamageId;
    DetailSP.Parameters.ParamValues['@EstStart'] := FDateFrom;
    DetailSP.Parameters.ParamValues['@EstEnd'] := FDateTo;
    DetailSP.Open;

    //this will return True if this DamageId had to be added to the list
    if FDamageIdListEx.AddIfNeeded(DamageId) then begin
      //update totals
      FPreviousEstimatesEx :=
        FPreviousEstimatesEx + VarToFloat(DetailSP.FieldValues['InitialEstimate']);
      FCurrentEstimatesEx :=
        FCurrentEstimatesEx + VarToFloat(DetailSP.FieldValues['CurrentEstimate']);
    end;
    FChangedInvoices := True;
    GetDamageInvoiceData(DamageId);
  end;
end;

procedure TDamageEstimateChangesDM.CreateTSVStream(Str : TStream);
begin
  // Note that details are not included in the text output
  SaveTabDelimToStream(NewDamagesSP, Str);
  SaveTabDelimToStream(NewEstimatesSP, Str);
end;

procedure TDamageEstimateChangesDM.NewDamageClaimsGroupFooterBandBeforePrint(Sender: TObject);
begin
  TotalEstimatesNew.AsFloat := FCurrentEstimatesNew;

  NewInvoiceTotal.Value := FNewInvoiceTotal;
  NewPaidTotal.Value := FNewPaidTotal;
end;

procedure TDamageEstimateChangesDM.NewEstimatesGroupFooterBandBeforePrint(Sender: TObject);
begin
  PreviousEstimatesEx.AsFloat := FPreviousEstimatesEx;
  CurrentEstimatesEx.AsFloat := FCurrentEstimatesEx;
  DifferenceEx.AsFloat := FCurrentEstimatesEx-FPreviousEstimatesEx;

  InvoiceTotalChanged.Value := FChangedInvoiceTotal;
  PaidTotalChanged.Value := FChangedPaidTotal;
end;

procedure TDamageEstimateChangesDM.DataModuleCreate(Sender: TObject);
begin
  FDamageIdListNew := TIntegerList.Create;
  FDamageIdListEx := TIntegerList.Create;
  FInvoiceIDListNew := TIntegerList.Create;
  FInvoiceIDListEx := TIntegerList.Create;
  FDamageIDFilterList := TIntegerList.Create;
end;

procedure TDamageEstimateChangesDM.DataModuleDestroy(Sender: TObject);
begin
  FDamageIdListNew.Free;
  FDamageIdListEx.Free;
  FInvoiceIDListNew.Free;
  FInvoiceIDListEx.Free;
  FDamageIDFilterList.Free;
end;

procedure TDamageEstimateChangesDM.GetDamageInvoiceData(DamageId: Integer);
begin
  InvoiceSP.Close;
  InvoiceSP.Parameters.ParamValues['damage_id'] := DamageId;
  InvoiceSP.Open;
end;

procedure TDamageEstimateChangesDM.PaidTotalNewCalc(Sender: TObject;
  var Value: Variant);
begin
  Value := Value + VarToFloat(InvoiceSP.FieldValues['paid_amount']);
end;

procedure TDamageEstimateChangesDM.InvoiceTotalNewCalc(Sender: TObject; var Value: Variant);
begin
  Value := Value + VarToFloat(InvoiceSP.FieldValues['amount']);
end;

procedure TDamageEstimateChangesDM.ChangedInvoiceTotalCalc(Sender: TObject; var Value: Variant);
begin
  Value := Value + VarToFloat(InvoiceSP.FieldValues['amount']);
end;

procedure TDamageEstimateChangesDM.ChangedPaidTotalCalc(Sender: TObject; var Value: Variant);
begin
  Value := Value + VarToFloat(InvoiceSP.FieldValues['paid_amount']);
end;

procedure TDamageEstimateChangesDM.CalculateInvoiceTotals;
begin
  if (InvoiceSP.IsEmpty) or VarIsNull(InvoiceSP.FieldValues['invoice_id']) then
    Exit;
  if not FChangedInvoices then begin
    if FInvoiceIDListNew.AddIfNeeded(InvoiceSP.FieldValues['invoice_id']) then begin
      FNewInvoiceTotal :=
        FNewInvoiceTotal + VarToFloat(InvoiceSP.FieldValues['amount']);
      FNewPaidTotal :=
        FNewPaidTotal + VarToFloat(InvoiceSP.FieldValues['paid_amount']);
    end;
  end
  else begin
    if FInvoiceIDListEx.AddIfNeeded(InvoiceSP.FieldValues['invoice_id']) then begin
      FChangedInvoiceTotal :=
        FChangedInvoiceTotal + VarToFloat(InvoiceSP.FieldValues['amount']);
      FChangedPaidTotal :=
        FChangedPaidTotal + VarToFloat(InvoiceSP.FieldValues['paid_amount']);
    end;
  end;
end;

procedure TDamageEstimateChangesDM.InvoiceDSDataChange(Sender: TObject; Field: TField);
begin
  CalculateInvoiceTotals;
end;

procedure TDamageEstimateChangesDM.MainDetailBandBeforePrint(Sender: TObject);
begin
  NewDamages.Visible := not (ppdaNoRecords in NewDamagesPipe.State);
  ChangedDamages.Visible := not (ppdaNoRecords in NewEstimatesPipe.State);
  ChangedToUQ.Visible := not (ppdaNoRecords in NewRespPipe.State);
  ChangedFromUQ.Visible := not (ppdaNoRecords in NoRespPipe.State);

  if (not NewDamages.Visible) and (not ChangedDamages.Visible) and (not ChangedToUQ.Visible) then
    ChangedFromUQTitle.NewPage := False
  else if (not NewDamages.Visible) and (not ChangedDamages.Visible) then
    ChangedToUQTitle.NewPage := False
  else if (not NewDamages.Visible) then
    ChangedDamagesTitle.NewPage := False;
end;

procedure TDamageEstimateChangesDM.TotalAccrualChangePrint(Sender: TObject);
begin
  TotalAccrualChange.Value := FCurrentEstimatesNew + (FCurrentEstimatesEx - FPreviousEstimatesEx) +
    ChangedToUQTotal.Value - ChangedFromUQTotal.Value;
end;

initialization
  RegisterReport('DamageEstimateChanges', TDamageEstimateChangesDM);

end.

