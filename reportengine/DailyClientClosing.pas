unit DailyClientClosing;

interface

uses
  SysUtils, Classes, ppCtrls, ppPrnabl, ppClass, ppDB, ppBands, ppCache,
  DB, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, ppVar,
  BaseReport, ppModule, daDataModule, ppStrtch, ppSubRpt, ppMemo, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TDailyClientClosingDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ClientCodeLabel: TppLabel;
    ppLabel4: TppLabel;
    TicketsStatusedFrom: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText5: TppDBText;
    ppDBText3: TppDBText;
    ppDBText6: TppDBText;
    ppDBText4: TppDBText;
    ppDBText7: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    ppLabel5: TppLabel;
    ppDBText1: TppDBText;
    ppLabel3: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppGroupFooterBand1: TppGroupFooterBand;
    TicketsTransmittedFrom: TppLabel;
    CallCenterLabel: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText2: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ImageSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText8: TppDBText;
    ppShape2: TppShape;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ImageMemo: TppMemo;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    daDataModule1: TdaDataModule;
    ppFooterBand2: TppFooterBand;
    ppSystemVariable4: TppSystemVariable;
    ImagePipe: TppDBPipeline;
    ppField1: TppField;
    ppField2: TppField;
    ppField3: TppField;
    ppField4: TppField;
    ppField5: TppField;
    ppField6: TppField;
    ppField7: TppField;
    ppField8: TppField;
    ppField9: TppField;
    ppField10: TppField;
    ppField11: TppField;
    ppField12: TppField;
    ppField13: TppField;
    ppField14: TppField;
    ppField15: TppField;
    ppField16: TppField;
    ppField17: TppField;
    ppField18: TppField;
    ImagesDS: TDataSource;
    ppLabel15: TppLabel;
    ppDBText11: TppDBText;
    PipeppField19: TppField;
    LocatesLabel: TppLabel;
    IncludeImagesLabel: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppReportFooterShape1: TppShape;
    ppLabel2: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    ImagesData: TADODataSet;
    procedure ppDetailBand2BeforeGenerate(Sender: TObject);
    procedure ImageMemoPrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
  end;

implementation

uses ReportEngineDMu, QMConst, OdDbUtils, OdRbMemoFit, OdMiscUtils, TicketImage;

{$R *.dfm}

{ TDailyClientClosingDM }

procedure TDailyClientClosingDM.Configure(QueryFields: TStrings);
var
  FromDateParam: TDateTime;
  ToDateParam: TDateTime;
  ClientCode: string;
  CallCenter: string;
  LocateClosed: Integer;
  Images: Boolean;
begin
  inherited;

  FromDateParam := GetDateTime('date'); // Legacy parameter name
  ToDateParam := SafeGetDateTime('sync_to_date');

  if (FromDateParam = 0) and (ToDateParam = 0) then begin
    FromDateParam := EncodeDate(1900, 1, 1);
    ToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsStatusedFrom, '');
  end
  else if (FromDateParam > 0) and (ToDateParam > 0) then begin
    ShowText(TicketsStatusedFrom, DateToStr(FromDateParam) + ' - ' + DateToStr(ToDateParam - 1))
  end
  else begin
    ToDateParam := FromDateParam + 1;
    ShowText(TicketsStatusedFrom, DateToStr(FromDateParam));
  end;
  SP.Parameters.ParamByName('@SyncDateFrom').value := FromDateParam;
  SP.Parameters.ParamByName('@SyncDateTo').value := ToDateParam;

  FromDateParam := SafeGetDateTime('xmit_from_date');
  ToDateParam := SafeGetDateTime('xmit_to_date');
  if (FromDateParam > 0) and (ToDateParam > 0) then
    ShowText(TicketsTransmittedFrom, DateToStr(FromDateParam) + ' - ' + DateToStr(ToDateParam - 1))
  else begin
    FromDateParam := EncodeDate(1900, 1, 1);
    ToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsTransmittedFrom, '');
  end;

  ClientCode := GetString('client_code');
  CallCenter := GetString('call_center');
  LocateClosed := SafeGetInteger('locate_closed', Ord(ocClosed)); // Old default = Open only
  Images := SafeGetString('images', '0') = '1';

  SP.Parameters.ParamByName('@XmitDateFrom').value := FromDateParam;
  SP.Parameters.ParamByName('@XmitDateTo').value := ToDateParam;
  SP.Parameters.ParamByName('@ClientCode').value := ClientCode;
  SP.Parameters.ParamByName('@CallCenter').value := CallCenter;
  SP.Parameters.ParamByName('@LocateClosed').value := LocateCLosed;
  SP.Open;

  // ImagesData.Recordset := SP.Recordset._xClone; //Clone(ltUnspecified);
  ImagesData.Recordset := SP.Recordset;

  ImageSubReport.Visible := Images;

  ShowText(CallCenterLabel, CallCenter);
  ShowText(ClientCodeLabel, ClientCode);

  case LocateClosed of
    Ord(ocAll): ShowText(LocatesLabel, 'All');
    Ord(ocOpen): ShowText(LocatesLabel, 'Open');
    Ord(ocClosed): ShowText(LocatesLabel, 'Closed');
  end;

  ShowText(IncludeImagesLabel, BooleanToStringYesNo(Images));
end;

procedure TDailyClientClosingDM.ppDetailBand2BeforeGenerate(
  Sender: TObject);
var
  IsXml: Boolean;
begin
  inherited;
  ImageMemo.Lines.Text := RemoveExcessWhitespace(GetDisplayableTicketImage(
    ImagesData.FieldByName('image').AsString, SP.FieldByName('xml_ticket_format').AsString, IsXml));
end;

procedure TDailyClientClosingDM.ImageMemoPrint(Sender: TObject);
begin
  inherited;
  ScaleRBMemoToFit(ImageMemo, 10);
end;

procedure TDailyClientClosingDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
var
  SkipFieldsList : TStringList;
begin
  SkipFieldsList := TStringList.Create;
  try
    SkipFieldsList.Add('xml_ticket_format');
    SkipFieldsList.Add('image');
    SaveDelimToStream(SP, Str, Delimiter, True, nil, SkipFieldsList);
  finally
    FreeAndNil(SkipFieldsList);
  end;
end;

initialization
  RegisterReport('DailyClientClosing', TDailyClientClosingDM);

end.

