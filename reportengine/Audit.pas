unit Audit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TAuditDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    TitleLabel: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppFooterBand1: TppFooterBand;
    ppReportHeaderShape1: TppShape;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    DateLabel: TppLabel;
    CallCenterLabel: TppLabel;
    AuditTypeLabel1: TppLabel;
    AuditTypeLabel2: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    AuditTypeLookup: TADOQuery;
    SP: TADOStoredProc;
    SPclient_code: TStringField;
    SPitem_number: TStringField;
    SPticket_number: TStringField;
    SPtype_code: TStringField;
    SPfound: TIntegerField;
    SPfoundstring: TStringField;
    procedure SPCalcFields(DataSet: TDataSet);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils;

{$R *.dfm}

{ TAuditDM }

procedure TAuditDM.Configure(QueryFields: TStrings);
var
  SummaryDate: TDateTime;
  CallCenter: string;
  AuditMethod: string;
begin
  inherited;
  SummaryDate := GetDateTime('date');
  CallCenter := GetString('call_center');

  AuditTypeLookup.Parameters.ParamByName('cc').value := CallCenter;
  AuditTypeLookup.Open;
  AuditMethod := AuditTypeLookup.Fields[0].AsString;

  //Interim step of calling check_summary is no longer used/needed
  if NotEmpty(AuditMethod) then
    SP.ProcedureName := 'check_summary_by_' + AuditMethod
  else
    SP.ProcedureName := 'check_summary_by_locate';

  if not Assigned(SP.Parameters.FindParam('@SummaryDate')) then
    SP.Parameters.CreateParameter('@SummaryDate',ftDateTime, pdInput,0,NULL);
  
  if not Assigned(SP.Parameters.FindParam('@CallCenter')) then
    SP.Parameters.CreateParameter('@CallCenter', ftString, pdInput,0,null);

  SP.Parameters.ParamByName('@SummaryDate').value := SummaryDate;
  SP.Parameters.ParamByName('@CallCenter').value := CallCenter;

  SP.Open;

  ShowDateText(DateLabel, 'Date:', SummaryDate);
  ShowText(CallCenterLabel, 'Call Center:', CallCenter);

  if AuditMethod = 'work_order' then
    AuditTypeLabel1.Caption := 'Work Order #'
  else
    AuditTypeLabel1.Caption := 'Ticket #';

  AuditTypeLabel2.Caption := AuditTypeLabel1.Caption;
end;

procedure TAuditDM.SPCalcFields(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('found').AsInteger=1 then
    DataSet.FieldByName('foundstring').AsString := '-'
  else
    DataSet.FieldByName('foundstring').AsString := 'MISSING';
end;

initialization
  RegisterReport('Audit', TAuditDM);

end.

