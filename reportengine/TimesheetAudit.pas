unit TimesheetAudit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppModule, daDataModule,
  ppCtrls, ppBands, ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv,
  ppProd, ppReport, ppStrtch, ppSubRpt, ppRegion, myChkBox, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TTimesheetAuditDM = class(TBaseReportDM)
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel6: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    DataDS: TDataSource;
    Pipe: TppDBPipeline;
    PrevDataDS: TDataSource;
    PrevDataPipe: TppDBPipeline;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLabel8: TppLabel;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel10: TppLabel;
    ppDBText9: TppDBText;
    ppLabel11: TppLabel;
    ppDBText10: TppDBText;
    ppLabel12: TppLabel;
    Cwork_start1: TppDBText;
    Cwork_stop1: TppDBText;
    ppLabel14: TppLabel;
    Cwork_start2: TppDBText;
    Cwork_stop2: TppDBText;
    ppLabel16: TppLabel;
    Cwork_start3: TppDBText;
    Cwork_stop3: TppDBText;
    ppLabel18: TppLabel;
    Cwork_start4: TppDBText;
    Cwork_stop4: TppDBText;
    ppLabel22: TppLabel;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppLabel24: TppLabel;
    ppDBText23: TppDBText;
    ppLabel25: TppLabel;
    ppDBText24: TppDBText;
    ppLabel26: TppLabel;
    ppDBText25: TppDBText;
    ppLabel27: TppLabel;
    ppDBText26: TppDBText;
    ppLabel28: TppLabel;
    ppDBText27: TppDBText;
    ppLabel29: TppLabel;
    ppDBText28: TppDBText;
    ppLabel30: TppLabel;
    ppDBText29: TppDBText;
    ppLabel31: TppLabel;
    ppDBText30: TppDBText;
    ppLabel32: TppLabel;
    ppDBText31: TppDBText;
    ppLabel33: TppLabel;
    ppDBText32: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppShape1: TppShape;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel13: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppLabel19: TppLabel;
    ppLabel23: TppLabel;
    ppLabel34: TppLabel;
    ppLabel35: TppLabel;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppLabel42: TppLabel;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppLabel45: TppLabel;
    ppLabel46: TppLabel;
    ppLabel47: TppLabel;
    ppLabel48: TppLabel;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ParamExplanationDS: TDataSource;
    ParamExplanationPipe: TppDBPipeline;
    ppDetailBand2: TppDetailBand;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    ppLabel49: TppLabel;
    CurrentCalloutRegion: TppRegion;
    Ccallout_start1: TppDBText;
    Ccallout_stop1: TppDBText;
    Ccallout_start2: TppDBText;
    Ccallout_stop2: TppDBText;
    Ccallout_stop4: TppDBText;
    Ccallout_start4: TppDBText;
    Ccallout_start3: TppDBText;
    Ccallout_stop3: TppDBText;
    Ccallout_start6: TppDBText;
    Ccallout_stop6: TppDBText;
    Ccallout_stop5: TppDBText;
    Ccallout_start5: TppDBText;
    ppLabel50: TppLabel;
    ppLabel51: TppLabel;
    ppLabel52: TppLabel;
    FloatingHolidayBox: TmyDBCheckBox;
    FloatingHolidayLabel: TppLabel;
    ppLine1: TppLine;
    PrevDataRegion: TppRegion;
    ppLabel20: TppLabel;
    Pwork_start1: TppDBText;
    Pwork_stop1: TppDBText;
    Pwork_start2: TppDBText;
    Pwork_stop2: TppDBText;
    Pwork_start3: TppDBText;
    Pwork_stop3: TppDBText;
    Pwork_start4: TppDBText;
    Pwork_stop4: TppDBText;
    ppDBText51: TppDBText;
    ppDBText52: TppDBText;
    ppDBText53: TppDBText;
    ppDBText54: TppDBText;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppDBText57: TppDBText;
    ppDBText58: TppDBText;
    ppDBText59: TppDBText;
    ppDBText60: TppDBText;
    ppDBText61: TppDBText;
    ppDBText62: TppDBText;
    DetailRegion: TppRegion;
    CurrentDataRegion: TppRegion;
    EntryTypeLabel: TppLabel;
    PrevCalloutRegion: TppRegion;
    Pcallout_start1: TppDBText;
    Pcallout_stop1: TppDBText;
    Pcallout_start2: TppDBText;
    Pcallout_stop2: TppDBText;
    Pcallout_stop4: TppDBText;
    Pcallout_start4: TppDBText;
    Pcallout_start3: TppDBText;
    Pcallout_stop3: TppDBText;
    Pcallout_start6: TppDBText;
    Pcallout_stop6: TppDBText;
    Pcallout_stop5: TppDBText;
    Pcallout_start5: TppDBText;
    ppLabel21: TppLabel;
    ppLabel36: TppLabel;
    ppLabel37: TppLabel;
    EmpTypeLabel1: TppLabel;
    EmpTypeLabel2: TppLabel;
    PrevEmployeeType: TppDBText;
    EmployeeType: TppDBText;
    ReasonChangedCode: TppDBText;
    ppLabel53: TppLabel;
    ppLabel54: TppLabel;
    ppLabel1: TppLabel;
    ppLabel55: TppLabel;
    ppLabel56: TppLabel;
    ppDBText76: TppDBText;
    CurrentDataRegionExtended: TppRegion;
    Ext_work_start5: TppDBText;
    Ext_work_stop5: TppDBText;
    Ext_work_start6: TppDBText;
    Ext_work_stop6: TppDBText;
    Ext_work_start7: TppDBText;
    Ext_work_stop7: TppDBText;
    Ext_work_start8: TppDBText;
    Ext_work_stop8: TppDBText;
    ppLabel58: TppLabel;
    ppLabel59: TppLabel;
    PExt_work_start5: TppDBText;
    PExt_work_stop5: TppDBText;
    PExt_work_start6: TppDBText;
    PExt_work_stop6: TppDBText;
    PExt_work_start7: TppDBText;
    PExt_work_stop7: TppDBText;
    PExt_work_start8: TppDBText;
    PExt_work_start9: TppDBText;
    PExt_work_start10: TppDBText;
    PExt_work_stop8: TppDBText;
    PExt_work_stop9: TppDBText;
    PExt_work_stop10: TppDBText;
    Ext_work_start9: TppDBText;
    Ext_work_stop9: TppDBText;
    Ext_work_start10: TppDBText;
    Ext_work_stop10: TppDBText;
    PrevDataRegionExtended: TppRegion;
    PExt_work_start11: TppDBText;
    PExt_work_stop11: TppDBText;
    PExt_work_start12: TppDBText;
    PExt_work_stop12: TppDBText;
    PExt_work_start13: TppDBText;
    PExt_work_stop13: TppDBText;
    PExt_work_start14: TppDBText;
    PExt_work_start15: TppDBText;
    PExt_work_start16: TppDBText;
    PExt_work_stop14: TppDBText;
    PExt_work_stop15: TppDBText;
    PExt_work_stop16: TppDBText;
    Ext_work_start11: TppDBText;
    Ext_work_stop11: TppDBText;
    Ext_work_start12: TppDBText;
    Ext_work_stop12: TppDBText;
    Ext_work_start13: TppDBText;
    Ext_work_stop13: TppDBText;
    Ext_work_start14: TppDBText;
    Ext_work_start15: TppDBText;
    Ext_work_start16: TppDBText;
    Ext_work_stop14: TppDBText;
    Ext_work_stop15: TppDBText;
    Ext_work_stop16: TppDBText;
    PExt_callout_start7: TppDBText;
    PExt_callout_start8: TppDBText;
    PExt_callout_start9: TppDBText;
    PExt_callout_start10: TppDBText;
    PExt_callout_start11: TppDBText;
    PExt_callout_start12: TppDBText;
    PExt_callout_stop7: TppDBText;
    PExt_callout_stop8: TppDBText;
    PExt_callout_stop9: TppDBText;
    PExt_callout_stop10: TppDBText;
    PExt_callout_stop11: TppDBText;
    PExt_callout_stop12: TppDBText;
    CExt_callout_start7: TppDBText;
    CExt_callout_start8: TppDBText;
    CExt_callout_start9: TppDBText;
    CExt_callout_start10: TppDBText;
    CExt_callout_start11: TppDBText;
    CExt_callout_start12: TppDBText;
    CExt_callout_stop7: TppDBText;
    CExt_callout_stop8: TppDBText;
    CExt_callout_stop9: TppDBText;
    CExt_callout_stop10: TppDBText;
    CExt_callout_stop11: TppDBText;
    CExt_callout_stop12: TppDBText;
    PExt_callout_stop13: TppDBText;
    PExt_callout_start13: TppDBText;
    PExt_callout_start14: TppDBText;
    PExt_callout_stop14: TppDBText;
    PExt_callout_stop15: TppDBText;
    PExt_callout_start15: TppDBText;
    PExt_callout_start16: TppDBText;
    PExt_callout_stop16: TppDBText;
    CExt_callout_start13: TppDBText;
    Ccallout_stop13: TppDBText;
    Ccallout_start14: TppDBText;
    CExt_callout_stop14: TppDBText;
    CExt_callout_start15: TppDBText;
    CExt_callout_stop15: TppDBText;
    CExt_callout_start16: TppDBText;
    CExt_callout_stop16: TppDBText;
    ppLabel57: TppLabel;
    ppLabel60: TppLabel;
    ppDBText3: TppDBText;
    ppDBText11: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Data: TADOStoredProc;
    PrevData: TADODataSet;
    ParamExplanation: TADODataSet;
    Master: TADODataSet;
    procedure ppDetailBand1BeforePrint(Sender: TObject);
    procedure DataGetText(Sender: TObject; var Text: string);
    procedure DataModuleDestroy(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  protected
    procedure MergePDFReportWithAttachments(const OutputDir,
      ReportFileName: string; var MergedPDFFileName: string); override;
  private
    FLastTSEId: Integer;
    PDFFileList: TStringList;
    procedure HighlightChanges(OldDataPresent: Boolean);
    function ColumnHasChanged(FieldName: string): Boolean;
    procedure ShadeComponent(C: TppDBText);
    procedure UnShadeComponent(C: TppDBText);
    function IsComponentShaded(C: TppDBText): Boolean;
    procedure ShowPreviousValue(const FieldName, ComponentName: string; Show: Boolean);
    procedure HookupGetTextEvents(Region: TppRegion);
    function GenerateMgrAlterTimeResponseReport(QueryFields: TStrings): string;
  end;

implementation

uses
  ReportEngineDMu, QMConst, OdMiscUtils, OdPdf, odAdoUtils;

{$R *.dfm}

{ TTimesheetAuditDM }

procedure TTimesheetAuditDM.Configure(QueryFields: TStrings);
var
  SheetsForID: Integer;
  PDFFileName: string;
  RecsAffected : oleVariant;
begin
  inherited;
  PDFFileList := TStringList.Create;
  FLastTSEId := 0;
  SheetsForID := SafeGetInteger('SheetsForID', 0);

  // Work around bug in client that sometimes misnamed the param
  if SheetsForID = 0 then
    SheetsForID := SafeGetInteger('SheetsForManagerID', 0);

  with Data do begin
    Parameters.ParamByName('@WorkStart').Value := GetDateTime('WorkStart');
    Parameters.ParamByName('@WorkEnd').Value := GetDateTime('WorkEnd');
    Parameters.ParamByName('@EntryStart').Value := GetDateTime('EntryStart');
    Parameters.ParamByName('@EntryEnd').Value := GetDateTime('EntryEnd');
    Parameters.ParamByName('@ChangedBy').Value := GetInteger('ChangedBy');
    Parameters.ParamByName('@ChangedByID').Value := SafeGetInteger('ChangedByID', 0);
    Parameters.ParamByName('@SheetsFor').Value := GetInteger('SheetsFor');
    Parameters.ParamByName('@SheetsForID').Value := SheetsForID;
    Parameters.ParamByName('@ChangeType').Value := GetInteger('ChangeType');
    Open;
  end;

  Master.Recordset := Data.Recordset;
  PrevData.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  PrevData.IndexFieldNames := 'new_entry_id';
  ParamExplanation.Recordset := Data.Recordset.NextRecordset(RecsAffected);

  HookupGetTextEvents(PrevDataRegion);
  HookupGetTextEvents(PrevDataRegionExtended);
  HookupGetTextEvents(PrevCalloutRegion);
  HookupGetTextEvents(CurrentDataRegion);
  HookupGetTextEvents(CurrentDataRegionExtended);
  HookupGetTextEvents(CurrentCalloutRegion);

  if SafeGetBoolean('IncludeMgrAlterTimeResponse', False) then begin
    PDFFileName := GenerateMgrAlterTimeResponseReport(QueryFields);
    PDFFileList.Add(PDFFileName);
    //MgrAlterTimeResponseAttachedLabel.Visible := True;
  end
  //else
    //MgrAlterTimeResponseAttachedLabel.Visible := False;
end;

procedure TTimesheetAuditDM.HighlightChanges(OldDataPresent: Boolean);
var
  I: Integer;
  C: TppDBText;
begin
  PrevDataRegion.Visible := False;
  for I := 0 to Self.ComponentCount-1 do begin
    if Self.Components[I] is TppDBText then begin
      C := Self.Components[I] as TppDBText;
      if C.Tag = 1 then begin
        if OldDataPresent and ColumnHasChanged(C.DataField) then begin
          ShadeComponent(C);
          ShowPreviousValue(C.DataField, C.Name, True);
        end
        else begin
          UnShadeComponent(C);
          ShowPreviousValue(C.DataField, C.Name, False);
        end;
      end;
    end;
  end
end;

function TTimesheetAuditDM.ColumnHasChanged(FieldName: string): Boolean;
var
  OldValue, NewValue: string;
begin
  OldValue := PrevData.FieldByName(FieldName).AsString;
  NewValue := Master.FieldByName(FieldName).AsString;
  Result := OldValue <> NewValue;
end;

procedure TTimesheetAuditDM.ShowPreviousValue(const FieldName, ComponentName: string; Show: Boolean);
var
  i: Integer;
  Fld: TppDBText;
  Region: TppRegion;
begin
  if Pos('callout_', FieldName) > 0 then
    Region := PrevCalloutRegion
  else
    if Pos('Ext', ComponentName) > 0 then
      Region := PrevDataRegionExtended
    else
      Region := PrevDataRegion;

  for i := 0 to Region.ObjectCount-1 do begin
    if Region.Objects[i] is TppDBText then begin
      Fld := Region.Objects[i] as TppDBText;
      if Fld.DataField = FieldName then begin
        Fld.Visible := Show;
        if Show then
          Region.Visible := True;
      end;
    end;
  end;
end;

procedure TTimesheetAuditDM.ShadeComponent(C: TppDBText);
begin
  C.Color := clYellow;
end;

procedure TTimesheetAuditDM.UnShadeComponent(C: TppDBText);
begin
  C.Color := clWhite;
end;

function TTimesheetAuditDM.IsComponentShaded(C: TppDBText): Boolean;
begin
  Result := C.Color = clYellow;
end;

procedure TTimesheetAuditDM.ppDetailBand1BeforePrint(Sender: TObject);
var
  I, CalloutFieldsUsed : Integer;
  IsEntry: Boolean;
begin
  inherited;
  IsEntry := Master.FieldByName('activity').AsString = 'ENTRY';
  PrevData.Filtered := True;
  if IsEntry then
    PrevData.Filter := 'new_entry_id = ' + IntToStr(Master.FieldByName('activity_entry_id').AsInteger)
  else
    PrevData.Filter := 'new_entry_id = -1';
  PrevDataRegion.Visible := FLastTSEID <> Master.FieldByName('activity_entry_id').AsInteger;
  PrevDataRegionExtended.Visible := PrevDataRegion.Visible and (not PrevData.FieldByName('work_start5').IsNull);
  HighlightChanges(not PrevData.EOF);
  if PrevDataRegion.Visible then
    EntryTypeLabel.Text := 'Changed To'
  else
    EntryTypeLabel.Text := 'Entered';

  CurrentDataRegionExtended.Visible := (not Master.FieldByName('work_start5').IsNull);

  // make the CalloutRegion visible if any 'callout' fields are used, otherwise
  // it's invisible
  CalloutFieldsUsed := 0;
  for I := 1 to 6 do begin
    if not Master.FieldByName('callout_start' + IntToStr(I)).IsNull then
      Inc(CalloutFieldsUsed);
    if not Master.FieldByName('callout_stop' + IntToStr(I)).IsNull then
      Inc(CalloutFieldsUsed);
  end;
  CurrentCalloutRegion.Visible := (CalloutFieldsUsed > 0);
  PrevCalloutRegion.Visible := (CurrentCalloutRegion.Visible and PrevDataRegion.Visible);
  FloatingHolidayBox.Visible := Master.FieldByName('floating_holiday').AsBoolean = True;
  FloatingHolidayLabel.Visible := FloatingHolidayBox.Visible;
  ReasonChangedCode.Visible := IsEntry;
  FLastTSEID := Master.FieldByName('activity_entry_id').AsInteger;
end;

procedure TTimesheetAuditDM.HookupGetTextEvents(Region: TppRegion);
var
  i: Integer;
begin
  for i := 0 to Region.ObjectCount-1 do
    if Region.Objects[i] is TppDBText then
      TppDBText(Region.Objects[i]).OnGetText := DataGetText;
end;

procedure TTimesheetAuditDM.DataGetText(Sender: TObject; var Text: string);
var
  Fld: TppDBText;
begin
  Assert(Sender is TppDBText, 'Only TppDBText components supported in DataGetText');
  Fld := TppDBText(Sender);
  if (IsComponentShaded(Fld) or (Fld.DataPipeline = PrevDataPipe)) and (Text = '') then
    if not (pos('Ext', Fld.name) > 0) then  //do not show dash in additional time spans
      Text := '-';
end;

procedure TTimesheetAuditDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(PDFFileList);
  inherited;
end;

function TTimesheetAuditDM.GenerateMgrAlterTimeResponseReport(QueryFields: TStrings): string;

  function CreateQueryFields: TStringList;
  var
    I: Integer;
  begin
    Result := TStringList.Create;
    Result.AddStrings(QueryFields);
    for I := 0 to Result.Count - 1 do begin
      if SameText(Result.Names[I], 'EntryStart') then
        Result.Strings[I] := 'DateStart=' + QueryFields.Values['EntryStart']
      else if SameText(Result.Names[I], 'EntryEnd') then
        Result.Strings[I] := 'DateEnd=' + QueryFields.Values['EntryEnd'];
    end;
    Result.Add('RuleType=' + BreakRuleTypeManagerAlteredTime);
    Result.Add('Response=Reject');
  end;

var
  Params: TStringList;
  ResponseReport: TBaseReportDM;
  FS: TFileStream;
  Output: string;
begin
  Params := nil;
  ResponseReport := CreateReport('TimeSubmitMessageResponse', IniName);
  try
    Params := CreateQueryFields;

    ResponseReport.UseConnection(Data.Connection, Data.Connection.CommandTimeout);
    ResponseReport.Configure(Params);

    Output := ChangeFileExt(GetWindowsTempFileName(False, OutputFolder, 'QM'), '.PDF');
    FS := TFileStream.Create(Output, fmCreate);
    try
      ResponseReport.CreatePDFStream(FS);
      Result := Output;
    finally
      FreeAndNil(FS);
    end;
  finally
    FreeAndNil(Params);
    FreeAndNil(ResponseReport);
  end;
end;

procedure TTimesheetAuditDM.MergePDFReportWithAttachments(const OutputDir: string; const ReportFileName: string; var MergedPDFFileName: string);
begin
  MergedPDFFileName := MergePDFs(PDFFileList, OutputDir, ReportFileName, IniName);
end;

initialization
  RegisterReport('TimesheetAudit', TTimesheetAuditDM);

end.

