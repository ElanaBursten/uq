unit TimesheetTotals;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppModule, daDataModule, ppBands, ppClass,
  ppCtrls, ppReport, ppStrtch, ppSubRpt, ppVar, ppPrnabl, ppCache, ppProd,
  ppDB, ppComm, ppRelatv, ppDBPipe, ppDesignLayer, ppParameter,
  ADODB;

type
  TTimesheetTotalsDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel2: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel3: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    SummarySubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel21: TppLabel;
    ppLabel5: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBText22: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText23: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel32: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc26: TppDBCalc;
    ppLine2: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ShortNameDBText: TppDBText;
    ppDBText2: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    reg_hours_total: TppDBCalc;
    ppLine3: TppLine;
    ppDBCalc9: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ppDBText5: TppDBText;
    ppDBCalc17: TppDBCalc;
    ppLabel4: TppLabel;
    ppDBText6: TppDBText;
    ppDBCalc18: TppDBCalc;
    ppLabel20: TppLabel;
    ppDBText24: TppDBText;
    ppDBCalc19: TppDBCalc;
    ppLabel26: TppLabel;
    ppDBText25: TppDBText;
    ppDBCalc20: TppDBCalc;
    ppLabel27: TppLabel;
    ppDBText26: TppDBText;
    pcCodeMarker: TppLabel;
    codeWarningShape: TppShape;
    ppLabel29: TppLabel;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppDBText29: TppDBText;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel33: TppLabel;
    ppDBCalc21: TppDBCalc;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    pcWarnLabel1: TppLabel;
    pcWarnLabel2: TppShape;
    PCSummaryDS: TDataSource;
    PCSummaryPipe: TppDBPipeline;
    PCSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppLabel28: TppLabel;
    ppLabel34: TppLabel;
    ppLabel35: TppLabel;
    ppLabel36: TppLabel;
    ppLabel37: TppLabel;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppLabel42: TppLabel;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppLabel45: TppLabel;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    ppDBText36: TppDBText;
    ppDBText37: TppDBText;
    ppDBText38: TppDBText;
    ppDBText39: TppDBText;
    ppDBText40: TppDBText;
    ppDBText41: TppDBText;
    ppDBText42: TppDBText;
    ppLabel46: TppLabel;
    ppDBCalc24: TppDBCalc;
    ppDBCalc25: TppDBCalc;
    ppDBCalc27: TppDBCalc;
    ppDBCalc28: TppDBCalc;
    ppDBCalc29: TppDBCalc;
    ppDBCalc30: TppDBCalc;
    ppDBCalc31: TppDBCalc;
    ppDBCalc32: TppDBCalc;
    ppDBCalc33: TppDBCalc;
    ppLine1: TppLine;
    ppDBCalc34: TppDBCalc;
    ppDBCalc35: TppDBCalc;
    ppDBCalc36: TppDBCalc;
    ppLabel47: TppLabel;
    ppDBText43: TppDBText;
    ppDBText44: TppDBText;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    HierOptionLabel: TppLabel;
    WeekEndingDateLabel: TppLabel;
    ppLabel1: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportFooterShape1: TppShape;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ReportModeLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    Data: TADOStoredProc;
    Summary: TADODataSet;
    PCSummary: TADODataSet;
    Master: TADODataSet;
    procedure ppDetailBand1BeforePrint(Sender: TObject);
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
    procedure TotalsPrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  private
    FAnyWarnings: Boolean;
    procedure HandleSortOrder(SP: TADOStoredProc);
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, QMConst,
     RbCustomTextFit, OdAdoUtils;

{ TTimesheetTotalsDM }

procedure TTimesheetTotalsDM.Configure(QueryFields: TStrings);
var
  FinalOnlyMode: Integer;
  ManagerID: Integer;
  EmpStatus: Integer;
  HierOption: Integer;
  EndDate: TDateTime;
  RecsAffected : oleVariant;
begin
  inherited;
  AddHierGroups(Report, ShortNameDBText);
  FinalOnlyMode := GetInteger('FinalOnly');
  ManagerID := GetInteger('manager_id');
  EndDate := GetDateTime('end_date');
  EmpStatus := SafeGetInteger('EmployeeStatus', StatusAll);
  HierOption := SafeGetInteger('HierarchyDepth', HierarchyOptionFullDepth);
  with Data do begin  //QMANTWO-533 EB
    Parameters.ParamByName('@ManagerID').Value := ManagerID;
    Parameters.ParamByName('@EndDate').Value := EndDate;
    Parameters.ParamByName('@LevelLimit').Value := HierOption;
    Parameters.ParamByName('@FinalOnly').Value := FinalOnlyMode;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;

  Master.Recordset := Data.Recordset;
  Summary.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  PCSummary.Recordset := Data.Recordset.NextRecordset(RecsAffected);

  AlignRBComponentsByTag(Self);

  case FinalOnlyMode of
    0: ReportModeLabel.Caption := 'Entered Time';
    1: ReportModeLabel.Caption := 'Final Approved Time Only';
    2: ReportModeLabel.Caption := 'Time Not Yet Final Approved';
  end;

  HandleSortOrder(Data);
  ShowDateText(WeekEndingDateLabel, 'Week Ending:', EndDate);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  ShowHierarchyOption(HierOptionLabel, HierOption);
end;

procedure TTimesheetTotalsDM.HandleSortOrder(SP: TADOStoredProc);
var
  Sort: string;
begin
  Sort := SafeGetString('Sort', '');
  if Sort='Emp Number' then
    Master.IndexFieldNames := 'h_numpath'
  else if Sort='Last Name' then
    Master.IndexFieldNames := 'h_lastpath'
  else
    Master.IndexFieldNames := 'h_namepath';
end;

procedure TTimesheetTotalsDM.ppDetailBand1BeforePrint(Sender: TObject);
var
  Warn: Boolean;
begin
  inherited;
  Warn := Master.FieldByName('pc_code_warn').AsBoolean = True;
  codeWarningShape.Visible := Warn;
  pcCodeMarker.Visible := Warn;

  if Warn then
    FAnyWarnings := True;

  Sleep(1);  // Reduce CPU hogging
end;

procedure TTimesheetTotalsDM.ppSummaryBand1BeforePrint(Sender: TObject);
begin
  inherited;
  pcWarnLabel1.Visible := FAnyWarnings;
  pcWarnLabel2.Visible := FAnyWarnings;
end;

procedure TTimesheetTotalsDM.TotalsPrint(Sender: TObject);
begin
  ScaleRBTextToFit(Sender as TppDBText, 8);
end;

initialization
  RegisterReport('TimesheetTotals', TTimesheetTotalsDM);

end.

