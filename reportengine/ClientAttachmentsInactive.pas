unit ClientAttachmentsInactive;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB,
  Data.Win.ADODB;

type
  TClientAttachmentsInactiveDM = class(TBaseReportDM)
    ClientAttachmentsInactiveSP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, odADOUtils, OdMiscUtils;

{ TClientAttachmentsInactiveDM }

procedure TClientAttachmentsInactiveDM.Configure(QueryFields: TStrings);
var
  ForeignTypes: string;
  ClientIDs: string;
  ExcludeFileNames: string;
  DestinationFolder: string;
begin
  inherited;
  DestinationFolder := GetString('DestinationFolder');

  ExcludeFileNames := StringReplace(SafeGetString('ExcludeFileNames', ''), '*', '%', [rfReplaceAll]);
  ClientIDs := GetString('ClientIDs');
  ForeignTypes := SafeGetString('ForeignTypes', '1');  // default to only ticket attachments
  if not ValidateIntegerCommaSeparatedList(ClientIDs) then
    raise Exception.Create('The ClientIDs parameter is a comma separated list of client ids to get attachments for.');
  if not ValidateIntegerCommaSeparatedList(ForeignTypes) then
    raise Exception.Create('The ForeignTypes parameter is a comma separated list of foreign types to get attachments for.');

  ClientAttachmentsInactiveSP.Parameters.ParamByName('@CallCenters').value := GetString('CallCenters');
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@ClientIDs').value := ClientIDs;
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@ForeignTypes').value := ForeignTypes;
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@AttachModifyStartDate').value := GetDateTime('ModifyStartDate');
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@AttachModifyEndDate').value := GetDateTime('ModifyEndDate');
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@IncludeFileTypes').value := GetString('IncludeFileTypes');
  ClientAttachmentsInactiveSP.Parameters.ParamByName('@ExcludeFileNames').value := ExcludeFileNames;
  ClientAttachmentsInactiveSP.Open;

end;

initialization
  RegisterReport('ClientAttachmentsInactive', TClientAttachmentsInactiveDM);
{
Report=ClientAttachmentsInactive
CallCenters=OCC2,OCC3
ClientIDs=3773,3481,3378,3767,3776,2811,2514,3774,2162,1753,3482,2294,3379,3892,2947,1759,1751,2924,1754,3377
ForeignTypes=1,7
ModifyStartDate=2010-01-01
ModifyEndDate=2011-03-30
IncludeFileTypes=.jpg,.png
ExcludeFileNames=screenshot_c%,screenshot_e%,screenshot_g%,screenshot_w%
DestinationFolder=D:\temp
}

end.
