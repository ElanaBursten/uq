inherited PayrollSummaryExDM: TPayrollSummaryExDM
  OldCreateOrder = True
  Height = 280
  object Pipe: TppDBPipeline
    DataSource = Q3DS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'Pipe'
    Left = 120
    Top = 184
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Payroll Summary'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    ThumbnailSettings.PageHighlight.Width = 3
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PDFSettings.PDFAFormat = pafNone
    PreviewFormSettings.PageBorder.mmPadding = 0
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowCancelDialog = False
    ShowPrintDialog = False
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 184
    Top = 184
    Version = '19.0'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 13758
      mmPrintPosition = 0
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Week Ending:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 213519
        mmTop = 1058
        mmWidth = 21167
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'end_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 4233
        mmLeft = 237861
        mmTop = 1058
        mmWidth = 27517
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel15: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label15'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        Caption = '%COMPANY% Payroll Summary GL '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 73290
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel23: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label23'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Employee:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 16669
        mmTop = 9260
        mmWidth = 16140
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel03: TppLabel
        Tag = 114
        DesignLayer = ppDesignLayer1
        UserName = 'Label03'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Earnings'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4498
        mmLeft = 234532
        mmTop = 9260
        mmWidth = 12851
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel31: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label31'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Emp #:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 2117
        mmTop = 9260
        mmWidth = 14288
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel26: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label26'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Reference Num:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 74696
        mmTop = 9260
        mmWidth = 24077
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel01: TppLabel
        Tag = 112
        DesignLayer = ppDesignLayer1
        UserName = 'Label01'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Hours'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4498
        mmLeft = 172704
        mmTop = 9260
        mmWidth = 14288
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel02: TppLabel
        Tag = 113
        DesignLayer = ppDesignLayer1
        UserName = 'Label02'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Unit Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4498
        mmLeft = 204090
        mmTop = 9260
        mmWidth = 14288
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel19: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label19'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Location:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 111645
        mmTop = 9260
        mmWidth = 16140
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        Tag = 111
        DesignLayer = ppDesignLayer1
        UserName = 'Label2'
        HyperlinkEnabled = False
        RTLReading = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsUnderline]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4498
        mmLeft = 141650
        mmTop = 9260
        mmWidth = 12965
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 4233
      mmPrintPosition = 0
      object ppDBText03: TppDBText
        Tag = 14
        DesignLayer = ppDesignLayer1
        UserName = 'DBText03'
        CharWrap = True
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'earnings'
        DataPipeline = Pipe
        DisplayFormat = '$###,##0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 235326
        mmTop = 529
        mmWidth = 11906
        BandType = 4
        LayerName = Foreground
      end
      object short_namePPText: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'short_namePPText'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'last_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 16669
        mmTop = 529
        mmWidth = 21431
        BandType = 4
        LayerName = Foreground
      end
      object EmpNumberText: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpNumberText'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'emp_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 2117
        mmTop = 529
        mmWidth = 13229
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText24: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText24'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'ReferenceNumber'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 74431
        mmTop = 265
        mmWidth = 23019
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText25: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'short_namePPText1'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'first_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 39579
        mmTop = 529
        mmWidth = 24329
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText26: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText26'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'location'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 111381
        mmTop = 529
        mmWidth = 13494
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText01: TppDBText
        Tag = 12
        DesignLayer = ppDesignLayer1
        UserName = 'DBText01'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'reg_hours'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00;-#,##0.00;-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 176937
        mmTop = 265
        mmWidth = 10054
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText02: TppDBText
        Tag = 13
        DesignLayer = ppDesignLayer1
        UserName = 'DBText02'
        HyperlinkEnabled = False
        RTLReading = False
        BlankWhenZero = True
        Border.mmPadding = 0
        DataField = 'unit_rate'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 204619
        mmTop = 529
        mmWidth = 13494
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        Tag = 11
        DesignLayer = ppDesignLayer1
        UserName = 'DBText2'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'earnings_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 142179
        mmTop = 265
        mmWidth = 11906
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 6879
      mmPrintPosition = 0
      object ppSystemVariable2: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'SystemVariable2'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        VarType = vtDocumentName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3440
        mmLeft = 0
        mmTop = 1588
        mmWidth = 58019
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable3: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'SystemVariable3'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        VarType = vtDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 101600
        mmTop = 1588
        mmWidth = 63500
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'SystemVariable1'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4498
        mmLeft = 242623
        mmTop = 1588
        mmWidth = 23548
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppSummaryBand1: TppSummaryBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 14023
      mmPrintPosition = 0
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label4'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        Caption = 'Totals:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3704
        mmLeft = 68263
        mmTop = 7938
        mmWidth = 9790
        BandType = 7
        LayerName = Foreground
      end
      object ppLine1: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line1'
        Border.mmPadding = 0
        Pen.Width = 3
        ParentWidth = True
        Weight = 2.250000000000000000
        mmHeight = 1323
        mmLeft = 40850
        mmTop = 3969
        mmWidth = 266701
        BandType = 7
        LayerName = Foreground
      end
      object ppDBCalcT01: TppDBCalc
        Tag = 112
        OnPrint = TotalsPrint
        DesignLayer = ppDesignLayer1
        UserName = 'DBCalcT01'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'reg_hours'
        DataPipeline = Pipe
        DisplayFormat = '#,##0.00;-#,##0.00;-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 174067
        mmTop = 8202
        mmWidth = 12965
        BandType = 7
        LayerName = Foreground
      end
      object ppDBCalcT02: TppDBCalc
        Tag = 114
        OnPrint = TotalsPrint
        DesignLayer = ppDesignLayer1
        UserName = 'DBCalcT02'
        HyperlinkEnabled = False
        RTLReading = False
        Border.mmPadding = 0
        DataField = 'earnings'
        DataPipeline = Pipe
        DisplayFormat = '$###,##0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 225762
        mmTop = 8236
        mmWidth = 21431
        BandType = 7
        LayerName = Foreground
      end
    end
    object ppGroup1: TppGroup
      BreakName = 'co_code'
      DataPipeline = Pipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'Pipe'
      NewFile = False
      object ppGroupHeaderBand1: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        Border.mmPadding = 0
        mmBottomOffset = 0
        mmHeight = 7938
        mmPrintPosition = 0
        object ppLabel20: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label20'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          Caption = 'Company:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 11
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4763
          mmLeft = 10848
          mmTop = 2117
          mmWidth = 18256
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText23: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText23'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          DataField = 'co_code'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 11
          Font.Style = [fsBold]
          ParentDataPipeline = False
          DataPipelineName = 'Pipe'
          mmHeight = 4796
          mmLeft = 32279
          mmTop = 2198
          mmWidth = 22225
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Background.Brush.Style = bsClear
        Border.mmPadding = 0
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 8467
        mmPrintPosition = 0
        object ppDBCalc03: TppDBCalc
          Tag = 114
          OnPrint = TotalsPrint
          DesignLayer = ppDesignLayer1
          UserName = 'DBCalc03'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          DataField = 'earnings'
          DataPipeline = Pipe
          DisplayFormat = '$###,##0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 235590
          mmTop = 1852
          mmWidth = 11717
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBCalc01: TppDBCalc
          Tag = 112
          OnPrint = TotalsPrint
          DesignLayer = ppDesignLayer1
          UserName = 'DBCalc01'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          DataField = 'reg_hours'
          DataPipeline = Pipe
          DisplayFormat = '#,##0.00;-#,##0.00;-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 176937
          mmTop = 1588
          mmWidth = 10054
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBCalc02: TppDBCalc
          Tag = 113
          OnPrint = TotalsPrint
          DesignLayer = ppDesignLayer1
          UserName = 'DBCalc02'
          HyperlinkEnabled = False
          RTLReading = False
          BlankWhenZero = True
          Border.mmPadding = 0
          DataField = 'unit_rate'
          DataPipeline = Pipe
          DisplayFormat = '$###,##0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 204619
          mmTop = 1852
          mmWidth = 13494
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLabel21: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label201'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          Caption = 'Company ID:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3704
          mmLeft = 11906
          mmTop = 1852
          mmWidth = 15875
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText18: TppDBText
          OnPrint = TotalsPrint
          DesignLayer = ppDesignLayer1
          UserName = 'DBText18'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          DataField = 'co_code'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 30956
          mmTop = 1588
          mmWidth = 15346
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBCalc9: TppDBCalc
          Tag = 111
          OnPrint = TotalsPrint
          DesignLayer = ppDesignLayer1
          UserName = 'DBCalc9'
          HyperlinkEnabled = False
          RTLReading = False
          Border.mmPadding = 0
          DataField = 'earnings_type'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ResetGroup = ppGroup1
          DataPipelineName = 'Pipe'
          mmHeight = 3704
          mmLeft = 142179
          mmTop = 1852
          mmWidth = 11906
          BandType = 5
          GroupNo = 0
          LayerName = Foreground
        end
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object Q1: TADODataSet
    Parameters = <>
    Left = 24
    Top = 72
  end
  object Q3: TADODataSet
    Parameters = <>
    Left = 28
    Top = 184
  end
  object Q3DS: TDataSource
    DataSet = Q3
    Left = 64
    Top = 184
  end
  object DS: TDataSource
    DataSet = Data
    Left = 80
    Top = 24
  end
  object Data: TADOStoredProc
    ProcedureName = 'payroll_export_Ulti'
    Parameters = <
      item
        Name = '@begin_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@end_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@region'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 28
    Top = 20
  end
  object Q2: TADODataSet
    Parameters = <>
    Left = 28
    Top = 128
  end
end
