inherited DamageDetailsDM: TDamageDetailsDM
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 698
  Width = 602
  object SPDS: TDataSource
    DataSet = Master
    Left = 112
    Top = 24
  end
  object Pipe: TppDBPipeline
    DataSource = SPDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'Pipe'
    Left = 200
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 10160
    PrinterSetup.mmMarginRight = 10160
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    ThumbnailSettings.PageHighlight.Width = 3
    ThumbnailSettings.ThumbnailSize = tsSmall
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PDFSettings.PDFAFormat = pafNone
    PreviewFormSettings.PageBorder.mmPadding = 0
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 264
    Top = 24
    Version = '20.01'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppTitleBand1: TppTitleBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 19315
      mmPrintPosition = 0
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = '%COMPANY%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Book Antiqua'
        Font.Size = 20
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 8467
        mmLeft = 2381
        mmTop = 1588
        mmWidth = 79904
        BandType = 1
        LayerName = Foreground
      end
      object ppLabel89: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label89'
        HyperlinkEnabled = False
        AutoSize = False
        Border.mmPadding = 0
        Caption = 'Damage Investigation'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Book Antiqua'
        Font.Size = 20
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 10319
        mmLeft = 84666
        mmTop = 1524
        mmWidth = 80170
        BandType = 1
        LayerName = Foreground
      end
      object WarningSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'WarningSection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'DuplicateRecordsPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 14023
        mmWidth = 195580
        BandType = 1
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport9: TppChildReport
          DataPipeline = DuplicateRecordsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'DuplicateRecordsPipe'
          object ppTitleBand5: TppTitleBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 29898
            mmPrintPosition = 0
            object WarningMemo: TppMemo
              DesignLayer = ppDesignLayer7
              UserName = 'WarningMemo'
              Border.mmPadding = 0
              Caption = 'WarningMemo'
              CharWrap = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 12
              Font.Style = [fsBold]
              Lines.Strings = (
                
                  'Errors/Warnings about the report that should not prevent the rep' +
                  'ort from running should be listed here.')
              RemoveEmptyLines = False
              Transparent = True
              mmHeight = 20373
              mmLeft = 529
              mmTop = 0
              mmWidth = 194469
              BandType = 1
              LayerName = Foreground6
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel152: TppLabel
              DesignLayer = ppDesignLayer7
              UserName = 'Label123'
              Border.mmPadding = 0
              Caption = 'Type'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3969
              mmLeft = 2910
              mmTop = 25135
              mmWidth = 6844
              BandType = 1
              LayerName = Foreground6
            end
            object ppLabel153: TppLabel
              DesignLayer = ppDesignLayer7
              UserName = 'Label135'
              Border.mmPadding = 0
              Caption = 'Code'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3969
              mmLeft = 21960
              mmTop = 25135
              mmWidth = 7549
              BandType = 1
              LayerName = Foreground6
            end
            object ppLabel158: TppLabel
              DesignLayer = ppDesignLayer7
              UserName = 'Label136'
              Border.mmPadding = 0
              Caption = 'Modifier'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3969
              mmLeft = 41010
              mmTop = 25135
              mmWidth = 11289
              BandType = 1
              LayerName = Foreground6
            end
            object ppLabel159: TppLabel
              DesignLayer = ppDesignLayer7
              UserName = 'Label137'
              Border.mmPadding = 0
              Caption = 'Count'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 71967
              mmTop = 25135
              mmWidth = 8467
              BandType = 1
              LayerName = Foreground6
            end
          end
          object ppDetailBand13: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 4498
            mmPrintPosition = 0
            object ppDBText122: TppDBText
              DesignLayer = ppDesignLayer7
              UserName = 'DBText122'
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'type'
              DataPipeline = DuplicateRecordsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DuplicateRecordsPipe'
              mmHeight = 4163
              mmLeft = 2910
              mmTop = 265
              mmWidth = 6703
              BandType = 4
              LayerName = Foreground6
            end
            object ppDBText123: TppDBText
              DesignLayer = ppDesignLayer7
              UserName = 'DBText123'
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'code'
              DataPipeline = DuplicateRecordsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DuplicateRecordsPipe'
              mmHeight = 4163
              mmLeft = 21960
              mmTop = 265
              mmWidth = 7691
              BandType = 4
              LayerName = Foreground6
            end
            object ppDBText80: TppDBText
              DesignLayer = ppDesignLayer7
              UserName = 'DBText124'
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'modifier'
              DataPipeline = DuplicateRecordsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DuplicateRecordsPipe'
              mmHeight = 4163
              mmLeft = 41010
              mmTop = 265
              mmWidth = 12629
              BandType = 4
              LayerName = Foreground6
            end
            object ppDBText120: TppDBText
              DesignLayer = ppDesignLayer7
              UserName = 'DBText125'
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'dupe_code_cnt'
              DataPipeline = DuplicateRecordsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'DuplicateRecordsPipe'
              mmHeight = 4163
              mmLeft = 56092
              mmTop = 265
              mmWidth = 24271
              BandType = 4
              LayerName = Foreground6
            end
          end
          object ppSummaryBand4: TppSummaryBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDesignLayers7: TppDesignLayers
            object ppDesignLayer7: TppDesignLayer
              UserName = 'Foreground6'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppHeaderBand: TppHeaderBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      PrintOnFirstPage = False
      mmBottomOffset = 0
      mmHeight = 19050
      mmPrintPosition = 0
      object ppLine3: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line3'
        Border.mmPadding = 0
        Pen.Width = 2
        ParentWidth = True
        Weight = 1.500000000000000000
        mmHeight = 1588
        mmLeft = 0
        mmTop = 2910
        mmWidth = 195580
        BandType = 0
        LayerName = Foreground
      end
      object ppLine4: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line4'
        Border.mmPadding = 0
        Pen.Width = 2
        ParentWidth = True
        Weight = 1.500000000000000000
        mmHeight = 1588
        mmLeft = 0
        mmTop = 14552
        mmWidth = 195580
        BandType = 0
        LayerName = Foreground
      end
      object ppDBText2: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText2'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'uq_damage_id'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 4995
        mmLeft = 170392
        mmTop = 7144
        mmWidth = 29718
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel27: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label27'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'INVESTIGATION NUMBER:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 131234
        mmTop = 8202
        mmWidth = 35983
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 257176
      mmPrintPosition = 0
      object ppShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape1'
        mmHeight = 5027
        mmLeft = 1323
        mmTop = 32015
        mmWidth = 200555
        BandType = 4
        LayerName = Foreground
      end
      object ppLine1: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line1'
        Border.mmPadding = 0
        Pen.Width = 2
        ParentWidth = True
        Weight = 1.500000000000000000
        mmHeight = 1588
        mmLeft = 0
        mmTop = 794
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText1: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText1'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'uq_damage_id'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 4995
        mmLeft = 42598
        mmTop = 4233
        mmWidth = 32544
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label2'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'INVESTIGATION #:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3175
        mmLeft = 14552
        mmTop = 5292
        mmWidth = 24871
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText5: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText5'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'damage_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 50800
        mmTop = 85196
        mmWidth = 17314
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label6'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'DATE DAMAGE DISCOVERED:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 85196
        mmWidth = 41010
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText6: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText6'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'uq_notified_date'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 50800
        mmTop = 91017
        mmWidth = 20913
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label7'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'DATE %COMPANY% NOTIFIED:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 91017
        mmWidth = 39952
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText7: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText7'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'notified_by_person'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 134673
        mmTop = 85196
        mmWidth = 23876
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel8: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label8'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'NOTIFIED BY CONTACT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 99484
        mmTop = 85196
        mmWidth = 33073
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText8: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText8'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'notified_by_company'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 134673
        mmTop = 91017
        mmWidth = 26755
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel9: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label9'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'NOTIFIED BY COMPANY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 99484
        mmTop = 91017
        mmWidth = 33867
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText9: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText9'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'notified_by_phone'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 134673
        mmTop = 96309
        mmWidth = 23072
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label10'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'PHONE:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 99484
        mmTop = 96309
        mmWidth = 10848
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText12: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText12'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'client_claim_id'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 143934
        mmTop = 41804
        mmWidth = 18669
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label13'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'CLIENT CLAIM ID:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 116946
        mmTop = 41804
        mmWidth = 24077
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText13: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText13'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'date_mailed'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 43656
        mmTop = 14552
        mmWidth = 15452
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel14: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label14'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'DATE MAILED:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 14817
        mmTop = 14552
        mmWidth = 20108
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText14: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText14'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'date_faxed'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 43656
        mmTop = 19844
        mmWidth = 13928
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel15: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label15'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'DATE FAXED:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 14817
        mmTop = 19844
        mmWidth = 18785
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText15: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText15'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'sent_to'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 43656
        mmTop = 25135
        mmWidth = 9313
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel16: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label16'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'SENT TO:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 14817
        mmTop = 25135
        mmWidth = 13229
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText16: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText16'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'facility_size_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 38100
        mmTop = 47096
        mmWidth = 22098
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel17: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label17'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'LINE SIZE:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 47096
        mmWidth = 14288
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText17: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText17'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'location'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 38100
        mmTop = 56621
        mmWidth = 9779
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel18: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label18'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'DAMAGE LOCATION:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 56621
        mmWidth = 28575
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText18: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText18'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'page'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 143140
        mmTop = 62177
        mmWidth = 6265
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel19: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label19'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'MAP PAGE/AREA:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 116417
        mmTop = 62177
        mmWidth = 24342
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText19: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText19'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'grid'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 143140
        mmTop = 68263
        mmWidth = 4699
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel20: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label20'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'MAP GRID:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 116417
        mmTop = 68263
        mmWidth = 14817
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText20: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText20'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'city'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 42333
        mmTop = 62177
        mmWidth = 4276
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel21: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label21'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'CITY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 8467
        mmTop = 62177
        mmWidth = 7408
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText21: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText21'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'county'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 42333
        mmTop = 68263
        mmWidth = 8340
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel22: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label22'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'COUNTY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 8467
        mmTop = 68263
        mmWidth = 12700
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText22: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText22'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'state'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 42333
        mmTop = 74613
        mmWidth = 6181
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel23: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label23'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'STATE:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 8467
        mmTop = 74613
        mmWidth = 10054
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText23: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText23'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'utility_co_damaged'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 38100
        mmTop = 41804
        mmWidth = 24426
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel24: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label24'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'CLIENT DAMAGED:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 41804
        mmWidth = 26194
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText24: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText24'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'diagram_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 143934
        mmTop = 47096
        mmWidth = 21421
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel25: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label25'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'CLIENT PLAT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 116946
        mmTop = 47096
        mmWidth = 19050
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText32: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText32'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'locate_marks_present'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3260
        mmLeft = 50800
        mmTop = 102923
        mmWidth = 27940
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel35: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label35'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'LOCATE MARKS PRESENT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3440
        mmLeft = 4233
        mmTop = 102923
        mmWidth = 37571
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel3: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label3'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'REPORTED DAMAGE INFORMATION'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4234
        mmLeft = 4234
        mmTop = 32512
        mmWidth = 62177
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText43: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText43'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'uq_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3969
        mmLeft = 131498
        mmTop = 13229
        mmWidth = 62706
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText74: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText101'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'exc_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        WordWrap = True
        DataPipelineName = 'Pipe'
        mmHeight = 3969
        mmLeft = 131498
        mmTop = 5027
        mmWidth = 62706
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel83: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label83'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'EXCAVATOR RESPONSIBILITY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3175
        mmLeft = 85725
        mmTop = 5027
        mmWidth = 42333
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText46: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText46'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'exc_resp_type_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        WordWrap = True
        DataPipelineName = 'Pipe'
        mmHeight = 3969
        mmLeft = 131498
        mmTop = 9260
        mmWidth = 62971
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText78: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText78'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'uq_resp_type_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        WordWrap = True
        DataPipelineName = 'Pipe'
        mmHeight = 3969
        mmLeft = 131498
        mmTop = 17198
        mmWidth = 62971
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel92: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label92'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = '%COMPANY% RESPONSIBILITY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3175
        mmLeft = 85725
        mmTop = 13229
        mmWidth = 41275
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText79: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'DBText79'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'spc_resp_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        WordWrap = True
        DataPipelineName = 'Pipe'
        mmHeight = 3969
        mmLeft = 131498
        mmTop = 21431
        mmWidth = 62706
        BandType = 4
        LayerName = Foreground
      end
      object ppLabel93: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label93'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'SPECIAL RESPONSIBILITY:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3302
        mmLeft = 85725
        mmTop = 21431
        mmWidth = 36745
        BandType = 4
        LayerName = Foreground
      end
      object NarrativeSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'NarrativeSection'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = ConclusionsSection
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 245269
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 102923
            mmPrintPosition = 0
            object ppDBMemo2: TppDBMemo
              DesignLayer = ppDesignLayer8
              UserName = 'DBMemo2'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'investigator_narrative'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 9
              Font.Style = []
              RemoveEmptyLines = False
              Stretch = True
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 87048
              mmLeft = 3175
              mmTop = 13229
              mmWidth = 189442
              BandType = 4
              LayerName = Foreground7
              mmBottomOffset = 20320
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppShape6: TppShape
              DesignLayer = ppDesignLayer8
              UserName = 'Shape6'
              ParentWidth = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 0
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground7
            end
            object ppLabel87: TppLabel
              DesignLayer = ppDesignLayer8
              UserName = 'Label87'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'INVESTIGATOR NARRATIVE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 3175
              mmTop = 529
              mmWidth = 48683
              BandType = 4
              LayerName = Foreground7
            end
          end
          object ppDesignLayers8: TppDesignLayers
            object ppDesignLayer8: TppDesignLayer
              UserName = 'Foreground7'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object ConclusionsSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'ConclusionsSection'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = SketchSection
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 239448
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object ppDetailBand3: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 224896
            mmPrintPosition = 0
            object ppShape8: TppShape
              DesignLayer = ppDesignLayer9
              UserName = 'Shape8'
              ParentWidth = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 265
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText3: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText3'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_code'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 10054
              mmWidth = 20902
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel28: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label28'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'EXCAVATOR RESPONSIBILITY CODE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 10054
              mmWidth = 51594
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText4: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText702'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_type'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 16404
              mmWidth = 20108
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel29: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label29'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'EXCAVATOR RESPONSIBILITY TYPE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 16404
              mmWidth = 50800
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText77: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText77'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_other_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 22754
              mmWidth = 28840
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel46: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label46'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'EXCAVATOR RESPONSIBILITY DESCRIPTION:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 22754
              mmWidth = 62706
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBMemo9: TppDBMemo
              DesignLayer = ppDesignLayer9
              UserName = 'DBMemo9'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'exc_resp_details'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              RemoveEmptyLines = False
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 46567
              mmLeft = 83608
              mmTop = 29104
              mmWidth = 111654
              BandType = 4
              LayerName = Foreground8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel84: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label84'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'EXCAVATOR RESPONSIBILITY DETAILS:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 29104
              mmWidth = 55563
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBMemo11: TppDBMemo
              DesignLayer = ppDesignLayer9
              UserName = 'DBMemo11'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'exc_resp_response'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              RemoveEmptyLines = False
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 13229
              mmLeft = 83608
              mmTop = 78846
              mmWidth = 111654
              BandType = 4
              LayerName = Foreground8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel94: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label801'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'EXCAVATOR RESPONSE (IF ANY):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 78846
              mmWidth = 46567
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText81: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText81'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'uq_resp_code'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 100806
              mmWidth = 19844
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel95: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label95'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = '%COMPANY% RESPONSIBILITY CODE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 100806
              mmWidth = 56621
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText82: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText82'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'uq_resp_type'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 107686
              mmWidth = 19050
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel96: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label96'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = '%COMPANY% RESPONSIBILITY TYPE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 107950
              mmWidth = 55827
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText83: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText83'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_code'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 159544
              mmWidth = 21167
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel97: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label97'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'SPECIAL RESPONSIBILITY CODE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 159279
              mmWidth = 46302
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText84: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText84'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_type'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 83608
              mmTop = 165629
              mmWidth = 20373
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel98: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label98'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'SPECIAL RESPONSIBILITY TYPE:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 165629
              mmWidth = 45508
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBMemo12: TppDBMemo
              DesignLayer = ppDesignLayer9
              UserName = 'DBMemo101'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'spc_resp_details'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              RemoveEmptyLines = False
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 53181
              mmLeft = 83608
              mmTop = 171715
              mmWidth = 111654
              BandType = 4
              LayerName = Foreground8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel99: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label99'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'SPECIAL RESPONSIBILITY DETAILS:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 171715
              mmWidth = 50006
              BandType = 4
              LayerName = Foreground8
            end
            object ppLabel100: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label100'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'CONCLUSIONS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 3440
              mmTop = 794
              mmWidth = 26194
              BandType = 4
              LayerName = Foreground8
            end
            object ppLine9: TppLine
              DesignLayer = ppDesignLayer9
              UserName = 'Line9'
              Border.mmPadding = 0
              Pen.Width = 2
              Weight = 1.500000000000000000
              mmHeight = 1588
              mmLeft = 10583
              mmTop = 95779
              mmWidth = 184415
              BandType = 4
              LayerName = Foreground8
            end
            object ppLine10: TppLine
              DesignLayer = ppDesignLayer9
              UserName = 'Line10'
              Border.mmPadding = 0
              Pen.Width = 2
              Weight = 1.500000000000000000
              mmHeight = 1588
              mmLeft = 10583
              mmTop = 153194
              mmWidth = 184680
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText85: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText102'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 114300
              mmTop = 10054
              mmWidth = 19008
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText86: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText86'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_type_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 114300
              mmTop = 16404
              mmWidth = 25908
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText87: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText87'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'uq_resp_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              WordWrap = True
              DataPipelineName = 'Pipe'
              mmHeight = 7144
              mmLeft = 114300
              mmTop = 100806
              mmWidth = 78317
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText88: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText88'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'uq_resp_type_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              WordWrap = True
              DataPipelineName = 'Pipe'
              mmHeight = 6879
              mmLeft = 114300
              mmTop = 107686
              mmWidth = 79111
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText89: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText89'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 114300
              mmTop = 159544
              mmWidth = 19092
              BandType = 4
              LayerName = Foreground8
            end
            object ppDBText90: TppDBText
              DesignLayer = ppDesignLayer9
              UserName = 'DBText90'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_type_desc'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 114300
              mmTop = 165629
              mmWidth = 25993
              BandType = 4
              LayerName = Foreground8
            end
            object flduq_resp_details: TppDBMemo
              DesignLayer = ppDesignLayer9
              UserName = 'flduq_resp_details'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'uq_resp_details'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              RemoveEmptyLines = False
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 36777
              mmLeft = 83608
              mmTop = 114829
              mmWidth = 111654
              BandType = 4
              LayerName = Foreground8
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object ppLabel103: TppLabel
              DesignLayer = ppDesignLayer9
              UserName = 'Label103'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = '%COMPANY% RESPONSIBILITY DETAILS:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15346
              mmTop = 114829
              mmWidth = 57415
              BandType = 4
              LayerName = Foreground8
            end
          end
          object ppDesignLayers9: TppDesignLayers
            object ppDesignLayer9: TppDesignLayer
              UserName = 'Foreground8'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object DiscussionSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'DiscussionSection'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = SiteInfoRegion
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 225425
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport4: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object ppDetailBand5: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 153194
            mmPrintPosition = 0
            object ppShape4: TppShape
              DesignLayer = ppDesignLayer10
              UserName = 'Shape4'
              ParentWidth = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 265
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
            end
            object ppLabel91: TppLabel
              DesignLayer = ppDesignLayer10
              UserName = 'Label91'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'DISCUSSIONS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 3175
              mmTop = 529
              mmWidth = 24606
              BandType = 4
              LayerName = Foreground9
            end
            object DiscAddPerson2Region: TppRegion
              DesignLayer = ppDesignLayer10
              UserName = 'DiscAddPerson2Region'
              Caption = 'DiscAddPerson2Region'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = DiscAddPerson1Region
              Stretch = True
              mmHeight = 24077
              mmLeft = 0
              mmTop = 92340
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText67: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText67'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_other2_person'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 65617
                mmTop = 98161
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel73: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label73'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'ADDITIONAL PERSON 2:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 17992
                mmTop = 98161
                mmWidth = 33073
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText68: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText68'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_other2_contact'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 65617
                mmTop = 103717
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel74: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label74'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'CONTACT INFORMATION 2:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 17992
                mmTop = 103717
                mmWidth = 37571
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBMemo6: TppDBMemo
                DesignLayer = ppDesignLayer10
                UserName = 'DBMemo6'
                Border.mmPadding = 0
                CharWrap = False
                DataField = 'disc_other2_comment'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4233
                mmLeft = 65617
                mmTop = 109273
                mmWidth = 123031
                BandType = 4
                LayerName = Foreground9
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppLabel75: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label75'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'COMMENTS 2:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 17992
                mmTop = 109273
                mmWidth = 19579
                BandType = 4
                LayerName = Foreground9
              end
              object ppLine7: TppLine
                DesignLayer = ppDesignLayer10
                UserName = 'Line7'
                Border.mmPadding = 0
                Pen.Width = 2
                Weight = 1.500000000000000000
                mmHeight = 1588
                mmLeft = 10583
                mmTop = 95779
                mmWidth = 185473
                BandType = 4
                LayerName = Foreground9
              end
            end
            object DiscAddPerson1Region: TppRegion
              DesignLayer = ppDesignLayer10
              UserName = 'DiscAddPerson1Region'
              Caption = 'DiscAddPerson1Region'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = DiscExcRegion
              Stretch = True
              mmHeight = 25135
              mmLeft = 0
              mmTop = 67469
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText65: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText65'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_other1_person'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66146
                mmTop = 73290
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel70: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label70'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'ADDITIONAL PERSON 1:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 73290
                mmWidth = 33073
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText66: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText66'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_other1_contact'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66146
                mmTop = 78846
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel71: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label71'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'CONTACT INFORMATION 1:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 78846
                mmWidth = 37571
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBMemo5: TppDBMemo
                DesignLayer = ppDesignLayer10
                UserName = 'DBMemo5'
                Border.mmPadding = 0
                CharWrap = False
                DataField = 'disc_other1_comment'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3704
                mmLeft = 66146
                mmTop = 84402
                mmWidth = 123031
                BandType = 4
                LayerName = Foreground9
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppLabel72: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label72'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'COMMENTS 1:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 84402
                mmWidth = 19579
                BandType = 4
                LayerName = Foreground9
              end
              object ppLine6: TppLine
                DesignLayer = ppDesignLayer10
                UserName = 'Line6'
                Border.mmPadding = 0
                Pen.Width = 2
                Weight = 1.500000000000000000
                mmHeight = 1588
                mmLeft = 10319
                mmTop = 70908
                mmWidth = 185473
                BandType = 4
                LayerName = Foreground9
              end
            end
            object DiscExcRegion: TppRegion
              DesignLayer = ppDesignLayer10
              UserName = 'DiscExcRegion'
              Caption = 'DiscExcRegion'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = DiscRepairRegion
              Stretch = True
              mmHeight = 27781
              mmLeft = 0
              mmTop = 37835
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText62: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText62'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_exc_were'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66146
                mmTop = 43656
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel66: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label66'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR COMPANY:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 43656
                mmWidth = 34925
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText63: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText63'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_exc_person'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66146
                mmTop = 49213
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel67: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label67'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR PERSON:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 49213
                mmWidth = 31221
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText64: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText64'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_exc_contact'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66146
                mmTop = 54769
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel68: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label68'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR CONTACT INFO:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 54769
                mmWidth = 40481
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBMemo4: TppDBMemo
                DesignLayer = ppDesignLayer10
                UserName = 'DBMemo4'
                Border.mmPadding = 0
                CharWrap = False
                DataField = 'disc_exc_comment'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4233
                mmLeft = 66146
                mmTop = 60325
                mmWidth = 123031
                BandType = 4
                LayerName = Foreground9
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppLabel69: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label69'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'EXCAVATOR COMMENT:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 18521
                mmTop = 60325
                mmWidth = 33867
                BandType = 4
                LayerName = Foreground9
              end
              object ppLine5: TppLine
                DesignLayer = ppDesignLayer10
                UserName = 'Line5'
                Border.mmPadding = 0
                Pen.Width = 2
                Weight = 1.500000000000000000
                mmHeight = 1588
                mmLeft = 10319
                mmTop = 41275
                mmWidth = 185473
                BandType = 4
                LayerName = Foreground9
              end
            end
            object DiscRepairRegion: TppRegion
              DesignLayer = ppDesignLayer10
              UserName = 'DiscRepairRegion'
              Caption = 'DiscRepairRegion'
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              mmHeight = 30427
              mmLeft = 0
              mmTop = 6615
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText58: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText58'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_repair_techs_were'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66940
                mmTop = 7144
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel61: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label61'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'REPAIR TECHNICIANS:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 19315
                mmTop = 7144
                mmWidth = 31485
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText59: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText59'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_repairs_were'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66940
                mmTop = 12701
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel62: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label62'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'REPAIR STATUS:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 19315
                mmTop = 12701
                mmWidth = 23548
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText60: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText60'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_repair_person'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66940
                mmTop = 18257
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel63: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label63'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'REPAIR PERSON:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 19315
                mmTop = 18257
                mmWidth = 24077
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBText61: TppDBText
                DesignLayer = ppDesignLayer10
                UserName = 'DBText61'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'disc_repair_contact'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 3175
                mmLeft = 66940
                mmTop = 23813
                mmWidth = 101600
                BandType = 4
                LayerName = Foreground9
              end
              object ppLabel64: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label64'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'REPAIR CONTACT INFO:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 19315
                mmTop = 23813
                mmWidth = 33338
                BandType = 4
                LayerName = Foreground9
              end
              object ppDBMemo3: TppDBMemo
                DesignLayer = ppDesignLayer10
                UserName = 'DBMemo3'
                Border.mmPadding = 0
                CharWrap = False
                DataField = 'disc_repair_comment'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4233
                mmLeft = 66940
                mmTop = 29369
                mmWidth = 123031
                BandType = 4
                LayerName = Foreground9
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppLabel65: TppLabel
                DesignLayer = ppDesignLayer10
                UserName = 'Label65'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'REPAIR COMMENTS:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3440
                mmLeft = 19315
                mmTop = 29369
                mmWidth = 28575
                BandType = 4
                LayerName = Foreground9
              end
            end
            object DiscFinalRegion: TppRegion
              DesignLayer = ppDesignLayer10
              UserName = 'DiscFinalRegion'
              Caption = 'DiscFinalRegion'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = DiscAddPerson2Region
              mmHeight = 9790
              mmLeft = 0
              mmTop = 118798
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground9
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppLine8: TppLine
                DesignLayer = ppDesignLayer10
                UserName = 'Line8'
                Border.mmPadding = 0
                Pen.Width = 2
                Weight = 1.500000000000000000
                mmHeight = 1588
                mmLeft = 10319
                mmTop = 122238
                mmWidth = 185473
                BandType = 4
                LayerName = Foreground9
              end
            end
          end
          object ppDesignLayers10: TppDesignLayers
            object ppDesignLayer10: TppDesignLayer
              UserName = 'Foreground9'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object SiteInfoRegion: TppRegion
        DesignLayer = ppDesignLayer1
        UserName = 'SiteInfoRegion'
        KeepTogether = True
        Caption = 'SiteInfoRegion'
        ParentWidth = True
        Pen.Style = psClear
        ShiftRelativeTo = ExcavatorRegion
        Stretch = True
        mmHeight = 62442
        mmLeft = 0
        mmTop = 161396
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppShape5: TppShape
          DesignLayer = ppDesignLayer1
          UserName = 'Shape5'
          ParentWidth = True
          mmHeight = 5027
          mmLeft = 0
          mmTop = 161660
          mmWidth = 195580
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText35: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText35'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_mark_state'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 150284
          mmTop = 178065
          mmWidth = 20108
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel38: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label38'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'STATE OF LOCATE MARKS:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 92869
          mmTop = 178065
          mmWidth = 38100
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText36: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText36'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_sewer_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 23019
          mmTop = 197115
          mmWidth = 8731
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel39: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label39'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'SEWER:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 10319
          mmTop = 197115
          mmWidth = 11377
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText37: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText37'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_water_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 46302
          mmTop = 197115
          mmWidth = 8731
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel40: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label40'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'WATER:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 33602
          mmTop = 197115
          mmWidth = 11113
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText38: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText38'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_catv_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 73819
          mmTop = 197115
          mmWidth = 9260
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel41: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label41'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'CABLE TV:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 56886
          mmTop = 197115
          mmWidth = 14817
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText39: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText39'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_gas_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 92869
          mmTop = 197115
          mmWidth = 10054
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel42: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label42'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'GAS:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 84402
          mmTop = 197115
          mmWidth = 6879
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText40: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText40'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_power_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 116152
          mmTop = 197115
          mmWidth = 10054
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel43: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label43'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'POWER:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 103452
          mmTop = 197115
          mmWidth = 11642
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText41: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText41'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_tel_marked'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 145786
          mmTop = 197115
          mmWidth = 8467
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel44: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label44'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'TELEPHONE:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 126736
          mmTop = 197115
          mmWidth = 18256
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText42: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText42'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_other_marked'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 169069
          mmTop = 197115
          mmWidth = 8467
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel45: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label45'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'OTHER:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 156369
          mmTop = 197115
          mmWidth = 10848
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText44: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText44'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'marks_within_tolerance_desc'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 51858
          mmTop = 184415
          mmWidth = 23030
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel47: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label47'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'MARKS WITHIN TOLERANCE:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 184415
          mmWidth = 40217
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText45: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText45'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_marks_measurement'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 150284
          mmTop = 184415
          mmWidth = 32766
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel48: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label48'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'TOLERANCE MEASUREMENT (inches):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 92869
          mmTop = 184415
          mmWidth = 52652
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText47: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText47'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_clarity_of_marks'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 150284
          mmTop = 171715
          mmWidth = 26712
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel50: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label50'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'CLARITY OF MARKS:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 92869
          mmTop = 171715
          mmWidth = 28840
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText50: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText50'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_paint_present'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 51858
          mmTop = 171715
          mmWidth = 23156
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel53: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label53'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'PAINT MARKS PRESENT:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 171715
          mmWidth = 34396
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText51: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText51'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_flags_present'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 51858
          mmTop = 178065
          mmWidth = 23029
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel54: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label54'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'LOCATE FLAGS PRESENT:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 178065
          mmWidth = 36777
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText55: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText55'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_img_35mm'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 47625
          mmTop = 205582
          mmWidth = 12171
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel58: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label58'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'PICTURES (FILM):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 20108
          mmTop = 205582
          mmWidth = 24342
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText56: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText56'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_img_digital'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 92075
          mmTop = 205582
          mmWidth = 11377
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel59: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label59'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'PICTURES (DIGITAL):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 60325
          mmTop = 205582
          mmWidth = 29104
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText57: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText57'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_img_video'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 117475
          mmTop = 205582
          mmWidth = 18923
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel60: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label60'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'VIDEO:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 104775
          mmTop = 205582
          mmWidth = 9790
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel11: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label11'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'SITE INFORMATION'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4233
          mmLeft = 2910
          mmTop = 162189
          mmWidth = 33867
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel12: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label12'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'What other utilities were marked?'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 192881
          mmWidth = 50800
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel90: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label90'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'Images:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 205581
          mmWidth = 13758
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel31: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label31'
          HyperlinkEnabled = False
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'Arrival time:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4233
          mmLeft = 4233
          mmTop = 216430
          mmWidth = 19315
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText10: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText10'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'investigator_arrival'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 24606
          mmTop = 216430
          mmWidth = 23707
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel49: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label49'
          HyperlinkEnabled = False
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'Departure time:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 5027
          mmLeft = 58208
          mmTop = 216430
          mmWidth = 23019
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText11: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText11'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'investigator_departure'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 83344
          mmTop = 216430
          mmWidth = 28109
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel76: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label76'
          HyperlinkEnabled = False
          AutoSize = False
          Border.mmPadding = 0
          Caption = 'Estimate damage time:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 5027
          mmLeft = 119592
          mmTop = 216430
          mmWidth = 33602
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText25: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText25'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'investigator_est_damage_time'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 154517
          mmTop = 216430
          mmWidth = 38650
          BandType = 4
          LayerName = Foreground
        end
      end
      object ExcavatorRegion: TppRegion
        DesignLayer = ppDesignLayer1
        UserName = 'ExcavatorRegion'
        Caption = 'ExcavatorRegion'
        ParentWidth = True
        Pen.Style = psClear
        ShiftRelativeTo = AddInfoRegion
        Stretch = True
        mmHeight = 48948
        mmLeft = 0
        mmTop = 112977
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 5080
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppShape7: TppShape
          DesignLayer = ppDesignLayer1
          UserName = 'Shape7'
          ParentWidth = True
          mmHeight = 5027
          mmLeft = 0
          mmTop = 146050
          mmWidth = 195580
          BandType = 4
          LayerName = Foreground
        end
        object ppShape3: TppShape
          DesignLayer = ppDesignLayer1
          UserName = 'Shape3'
          ParentWidth = True
          mmHeight = 5027
          mmLeft = 0
          mmTop = 113241
          mmWidth = 195580
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText29: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText29'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'excavator_company'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 45508
          mmTop = 122766
          mmWidth = 25273
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel32: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label32'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'EXCAVATION COMPANY:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 3440
          mmTop = 122766
          mmWidth = 34396
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText30: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText30'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'excavator_type'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 154782
          mmTop = 122766
          mmWidth = 19092
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel33: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label33'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'EXCAVATOR TYPE:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 107156
          mmTop = 122766
          mmWidth = 26458
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText31: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText31'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'excavation_type'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 154782
          mmTop = 128058
          mmWidth = 20362
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel34: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label34'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'EXCAVATION TYPE:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 107156
          mmTop = 128058
          mmWidth = 27252
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText33: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText33'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'locate_requested'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 51329
          mmTop = 152665
          mmWidth = 21717
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel36: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label36'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'LOCATE REQUESTED:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 3704
          mmTop = 152665
          mmWidth = 30692
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText34: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText34'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'ticket_number'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 123825
          mmTop = 152665
          mmWidth = 17865
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel37: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label37'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'TICKET NUMBER:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 91017
          mmTop = 152665
          mmWidth = 24342
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText48: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText48'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_hand_dig'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 28840
          mmTop = 135467
          mmWidth = 17611
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel51: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label51'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'HAND DIGGING:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 3440
          mmTop = 135466
          mmWidth = 21960
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText49: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText49'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'site_mechanized_equip'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3175
          mmLeft = 79640
          mmTop = 135467
          mmWidth = 13229
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel52: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label52'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'MECHANIZED DIGGING:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 43656
          mmTop = 135466
          mmWidth = 32544
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText52: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText52'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_exc_boring'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 111390
          mmTop = 135467
          mmWidth = 19770
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel55: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label55'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'BORING:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 94456
          mmTop = 135466
          mmWidth = 12171
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText53: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText53'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_exc_grading'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 143140
          mmTop = 135467
          mmWidth = 21336
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel56: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label56'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'GRADING:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 124090
          mmTop = 135466
          mmWidth = 14023
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText54: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText54'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'site_exc_open_trench'
          DataPipeline = Pipe
          DisplayFormat = 'Yes;No'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 181240
          mmTop = 135466
          mmWidth = 27644
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel57: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label57'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'OPEN TRENCH:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 155840
          mmTop = 135466
          mmWidth = 20638
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel4: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label4'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'EXCAVATOR INFORMATION'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4233
          mmLeft = 3175
          mmTop = 113771
          mmWidth = 48154
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel5: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label5'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'TICKET INFORMATION'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 10
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4233
          mmLeft = 3175
          mmTop = 146579
          mmWidth = 38629
          BandType = 4
          LayerName = Foreground
        end
        object ppLabel88: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label88'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'LOCATOR:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3302
          mmLeft = 91281
          mmTop = 156898
          mmWidth = 14647
          BandType = 4
          LayerName = Foreground
        end
        object ppDBText73: TppDBText
          DesignLayer = ppDesignLayer1
          UserName = 'DBText73'
          HyperlinkEnabled = False
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'locator_name'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 3260
          mmLeft = 123825
          mmTop = 156898
          mmWidth = 17187
          BandType = 4
          LayerName = Foreground
        end
      end
      object SketchSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'SketchSection'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = DiscussionSection
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 232305
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport3: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object ppDetailBand4: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 235215
            mmPrintPosition = 0
            object ppImage1: TppImage
              DesignLayer = ppDesignLayer11
              UserName = 'Image1'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.mmPadding = 0
              Picture.Data = {
                07544269746D6170B6B50000424DB6B50000000000007600000028000000CC01
                0000C8000000010004000000000040B50000C40E0000C40E0000100000000000
                0000000000000000800000800000008080008000000080008000808000008080
                8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
                FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFF000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000FFFFFFFFFFF0000FFFFFFFFFFFFFFF00000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFF00000000
                0FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000
                FFFFFF00000000000000FFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000FFF00000FFFFFFFFF0000FFF000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000F0000FFFFFFFFFFFF0000F000FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F000FFFFFFFFFFF
                FFFFF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F000FFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFF
                FFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
                0FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFF0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFF
                FF0000FF0FFF0FFF0FFF0000FFF0000FFF000FFFFFFF0FFFF0FF0FFFFF000FFF
                F0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFF0FFFFF00000FFFFFFFFFFFFFFFFFF0FFFFFF0FFFFF000FFF0000FFFF00
                0FFF0FFFFF00FFF0FFFFFFFF00000F0FF0FFF0FFF000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFF0FFF0FFF0FF0FFF0FFF0FF0FFF0FF0FFF0FF0FFF0FFFFFF0FFF
                F0FF0FFFF0FFF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFF0FFFF0FFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFF0
                FFF0FF0FFF0FF0FFF0FF0FFFF0FFFFF0FFFFFFFF0FFFFF0FF0FFF0FF0FFF0FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FF0FFF0FF0FFF0FFF0FF0FFF0FF0FFF0F
                F0FFFFFFFFFF000000FF0FFFF0FFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFF0FFFFFFFFF0000000FFF
                FFF0FFFFFF0FFFF0FFF0FF0FFF0FF0FFFFFF0FFFF0FFFF0F0FFFFFFF0FFFFF0F
                F0FFF0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFF0000FF0FFF0FFF
                0FFF0000FF0FFF0FF00000FFFFFFF0FF0FFF0FFFF00000FFF0000FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF0FFF0FFF
                FFFFFFFFFFFFFFFFFFF0000FFF0FFFF0FFF0FF0FFF0FF00000FF0FFFF0FFFF0F
                0FFFFFFF0FFFFF0FF0FFF0FF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0F
                FFFFF0FF0FFF0FFF0FFFFFF0FF0FFF0FF0FFF0FFFFFFF0FF0FFF00FFF0FFF0FF
                FFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFF0FFF0FFF0FFFFFFFFF0000000FFFFFF0FFF0FF00FFF0FFF0FF0FFF0FF0FF
                F0FF00FFF0FFFF0F0FFFFFFF0FFFFF0FF0FFF0FF0FFF0FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFFF0FFF000FFF0000F000FFFF000FFFF0000FFF000FFFFFFFF0FF
                0FFF0F00FF000FFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFF0FFFF0FF0FFFFFFFFFFFFFFFFFFFFFF0FFF0FF0F00FF
                000FFF0000FFFF000FFF0F000000F0FFF0FFFFFF0FFFFF0FF0000FFFF000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFFF0FF0FFFFFFFFFFFFFFFFFFF
                FFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF0FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF0F0FFF
                FFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF
                FFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
                0FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFF
                FFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F000FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0F000FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000F0000FFFFFFFFFFFF0000FF000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFF00000FFFFFFF
                FF0000FFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000FFFFF0FFFFF0FFFFFFFFFFFFFFFFFFF00000FF000F
                FFF000FFF0000FFF00FF000FFFFFFF0FFFFF0FFF0000FF0FFFF0FFF0FF000FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000
                FFFFFF00000000000000FFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFFF0FFFFF0FFFFFFFFFFFFFF
                FFFFF0FFFFF0FFF0FF0FFF0F0FFF0FF0FFF0FFF0FFFFFF0FFFFF0FF0FFF0FF0F
                FFF0FF0FFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF00FFFFFFFFF000000000FFFFFFFFFFF000FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFFF0FF0F
                F0FFFFFF0000000FFFFFF0FFFFF0FFF0FF0FFFFF0FFF0FF0FFF0FFFFFFFFFF0F
                F0FF0FF0FFF0FF0FFFF000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFFF0FFF0FF0FF0FFFFFFFFFFFFFFFFFFF0FFFFF0FFF0FF0FFFFFF0000FF0
                FFF00000FFFFFF0FF0FF0FFF0000FF0FFFF0F0FFFF00FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFF0FFF0F0F0F0FFFFFF0000000FFFFFF0FFFFF0FFF0
                FF0FFF0FFFFF0FF0FFF0FFF0FFFFFF0F0F0F0FFFFFF0FF00FFF0FF0FFF0FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF0FF0F0F0F0FFFFFFFFFFFFFF
                FFFFF0FFFFFF000FFFF000FFF000FF0000FF000FFFFFFF0F0F0F0FFF000FFF0F
                00F0FFF0FFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF0FF00FFF
                00FFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF00
                FFF00FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFFFFF0F00FFF00FFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF0
                FFFFFFFFFFFFFF00FFF00FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFF00
                FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFF0FFFFFFFFFFFF0FFFFFFFFFF0FF
                FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFF0FFFFFFFFF
                FFF0FFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF
                0FFFFF0FFFF0000FFFF0FF0FFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF000000FF
                FFFFFFFFFFFFFFFFFFFFFFFF00000FFFF0000FF0FFFF0FFF000FFFF0000FFFFF
                FF0000FFFF00F0FF0FF0FFF00FFF0FFFFFFFF00000F0FF0FFF0FFF000FFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFF000FFFF0FFFFFFFFFFFFFFF
                FFFFFFFF00000FFF0000FFF0000FFF000FFFFFFFF000FFF0FFFFFFF0FFFFFFF0
                000FFFF0FFFFF000FFF0FFF0FFF0FFF000FFF0FFF0FFF00FFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF
                FFFFFFFFF0FFFFFF0FFFFF0FFF0FFFF0FFF0FF0FFFFFFFF0FFFFFFFFFFFFFFFF
                FFFFFFFF00000000FFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FF0FFF0FF0FFFF0FF0
                FFF0FF0FFF0FFFFFF0FFFF0FF0FFF0FF0FF0FF0FFFFF0FFFFFFFF0FFFFF0FF0F
                FF0FF0FFF0FFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF0FFF0FFF
                0FFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFF0FF0FFF0FF0FFF0FFFFFF0FFF0FF0
                FFFFFFF0FFFFFF0FFF0FFFF0FFFF0FFF0FF0FFF0FFF0FF0FFF0FF0FFF0FF0FFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0000000000
                FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFFFFFF0FFFFF0FFFFFF0FFFFFFFF0
                FFFFFFFFFFFFFFFFFFFFFFFF0000000000FFFFFFFFFFFFFFFFFFFFFF0FFFF0FF
                0FFF0FF0FFFF0FF0FFFFFF0FFF0FFFFFF0FFFF0FF0FFF0FF0FF0FF0FFFF0F0FF
                FFFFF0FFFFF0FF0FFF0FF0FFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFFF0FFFFF0FF0FFFFFFFFFF0000000FFFFFF0FFFFFF0FFF0FF0FFF0FF0FF
                FFFFFFFF0FFF0FF0FFFFFFF0FFFFFF0FFF0FFF0F0FFF0FFFFFF0FFF0FFF0FF0F
                FFFFF0FFF0FF0FFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFF00000000000000000000000000000000F0FFFFFF0FFFFFFFF0FFF000
                FFFFFF0000FFFFF0F00000000000000000000000000000000FFFFFFFFFFFFFFF
                FFFFFFFF0FFFF0FF0FFF0FF0FFFF0FF00000FF0FFF0FFFFFF0FFFF0FF0FFF0FF
                0FF0FF0FFFF0F0FFFFFFF0FFFFF0FF0FFF0FF00000FFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFF0FFFFF0FF0000FFFFFFFFFFFFFFFFFFFF0FFFFFF0
                FFF0FF0FFF0FF00000FFFFFF0FFF0FF0FFFFFFF0000FFFF0000FFF0F0FFF0000
                0FF0FFF0FFF0FF00000FF0FFF0FF0FFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF
                0FFFFFFFF0FFFFFFFFFFFF0FFF0FFFF0FFFFFFFFFFFFFFFFFFFFFFFF0000000F
                FFFFFFFFFFFFFFFFFFFFFFFF00000FFF0FFF0FF00FFF0FF0FFF0FF0FFF0FFFFF
                F0FFFF0FF0FFF0FF0FF0FF0FFFF0F0FFFFFFF0FFFFF0FF0FFF0FF0FFF0FFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FF0FFFFF0FF0FFF0FFFFFF00000
                00FFFFFF00000FF0FFF0FF0FFF0FF0FFF0FFFFFF0FFF0FF0FFFFFFF0FFF0FFFF
                FF0FF0FFF0FF0FFF0FF0FFF0FFF0FF0FFF0FF0FFF0FF0FFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00000FFFFFFFFFFFFFFFF
                FFFFFFFFF0FFFFFF0FFFFFFFF0FFFFFFFFFFFF0FFF0FFFF0FFFFFFFFFFFFFFFF
                FFFFFFFF00000FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFF0FFF0FF0F00F0FFF
                000FFFF0000FFFFFF0FFFF0F0000F0FF0FF0F0000F0FFF0FFFFFF0FFFFF0FF00
                00FFFF000FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFFF0FF
                0FFF0FFFFFFFFFFFFFFFFFFF0FFFFFFF0000FFF0000FFF000FFFFFFFF000FF00
                00FFFFF0FFF0FFF000FFF0FFF0FFF000FFF0000F000FFFF000FFF0000FF0000F
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF000
                FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFFFFFFFF0FFFF0FFFFFF0FFF0FFF0F
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFF0FF0FFFFFF0FFFFF0FFFFFFFFF
                FFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFFFF0FFF0FFF0FFF0FFFFFFFFFFFFFFFFFFF0FFFFFFFFFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFF0FFFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF0FFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFF0FF0000000FFFFFFF0000F
                FFFFFF0000FFFF0FFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFF0FF0FFFFFF
                0FFFFF0FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF00000FFFF000FFFF0000FFFFFFFFFFFFFFFFFFFF00000FFF
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF0000FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFF0FFFFF
                FFFFFFFFFFFFF0FF0FF0FFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF
                FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFF
                FFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFF
                FFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FFFFF000FFF0FFF0
                FFF000FF0FFF0FFFFFF00000F0FF0FFF0FFF000FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFF0FFFFFFF
                0FFF0FFFFFFFFFFFFFFFFFFFFF00000FFFF0FFFF0FFFF0FFFFF000FFFFFFFFF0
                FFF0FFFFF0000FFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF
                FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF0FFFF0
                FFFF0FFF0FF0FFF0FF0FFF0F0FFF0FFFFFF0FFFFF0FF0FFF0FF0FFF0FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFF0FFF0FFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFF0FFFF0FFF0FFFF0FFFF0FF
                FF0FFF0FFFFFFFF0FFF0FFFF0FFF0FFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFF
                FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
                FFFFFFFFFF0FFFF0FFFF0FFFFFF0FFF0FF0FFFFF0FFF0FFFFFF0FFFFF0FF0FFF
                0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFF0FF0FFFFF0F0F0F0FFFFFFF0000000FFFFFF0FFFFF
                0FF0FFFF0FFF0F0FFF0FFFFFFFFFFF0F0F0F0FFF0FFF0FFF0F0FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFF00000000000000000000000000FF000000000000000
                00000000000FFF0000000000000000000000000FFFFF00000000000000000000
                00000FFFFFFFFFFFFFFFFFFFFF0FFFF0FFFF00000FF0FFF0FF0FFFFF0FFF0FFF
                FFF0FFFFF0FF0FFF0FF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFF0FFFF0F0F0F0FFFFFFFFFF
                FFFFFFFFFF0FFFFF0FF0FFFF0FFF0F0FFF00000FFFFFFF0F0F0F0FFFF0000FFF
                0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFF
                FFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFF0FFFF00FFF0FFF0FF0FFF0
                FF0FFF0F0FFF0FFFFFF0FFFFF0FF0FFF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFF0FFFF0
                F0F0F0FFFFFFF0000000FFFFFF0FFFFF0FF00FFF0FF0FFF0FF0FFF0FFFFFFF0F
                0F0F0FFFFFFF0FFF0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFF
                F00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0
                F00FF000FFF0000FFFF000FF0000FFFFFFF0FFFFF0FF0000FFFF000FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFF0FFFF0FFF0F0F0F0FFFFFFFFFFFFFFFFFFFF0FFFFF0FF0F00F0FF0FFF0
                FFF000FFFFFFFF0F0F0F0FFFF000FFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF
                FFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFF0FFFFF0FF0FFF0FFF0FFFFFFFFFFFFFFFFFFF0FFFF0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFF
                F00FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFF
                FFFFFFFFFFFFFFFFFFFFFFF0000000FFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFFFF0F0FFF0FFF0FFFFFFFFF
                FFFFFFFFFF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFF0FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFF
                FFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFF000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0
                000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF00
                00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                F0000FF0FFFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFFF0000FFF0FFF0000FFF000F
                FFFFFFFF0FFF0FFFFF0000FF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFF0FF0FFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFF0FFFF0F
                F0FF0FFF0FF0FFF0FFFFFFFF0FFF0FFFF0FFF0FF0FF0FF0FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0FFFFF0F0F0F0FFFFFFF0000
                000FFFFFFFFFFF0FF0FF0FFF0FF0FFFFFFFFFFF0F0F0F0FFF0FFF0FF0FF000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF0FFFF0F
                0F0F0FFFFFFFFFFFFFFFFFFFFFFF00FFF0FF0FFF0FF00000FFFFFFF0F0F0F0FF
                FF0000FF0FF0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                F00FFFFFF0FFFF0F0F0F0FFFFFFF0000000FFFFFFF00FFFFF0FF0FFF0FF0FFF0
                FFFFFFF0F0F0F0FFFFFFF0FF0FF0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFFFFF0FFF0F0F0F0FFFFFFFFFFFFFFFFFFFF0FFFFFF
                F0FFF0000FFF000FFFFFFFF0F0F0F0FFFF000FFF0FF0FFF0FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FFFF0FF0FFF0FFF0FFFFFFFFFF
                FFFFFFFFF0FFFF0FFFFFFFFF0FFFFFFFFFFFFF0FFF0FFF0FFFFFFFFF0FF0FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF0F0FF
                F0FFF0FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF0FFFFFFFFFFFFF0FFF0FFF0F
                FFFFFFFF0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF0FFFFFFFFFFFFFFFFFFFFF00000FF0FFF0FFF000FFF0000FFFF0FFFFF0000F
                FF00F0FFF000FFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFF0FFFFFFF0F0FFF0FFF0F0FFF
                0FFFF0FFFF0FFF0FF0FFF0FF0FFF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFF0FFFFFFF
                F0FFFF0FFFFF0FFF0FFF0F0FFF0FFF0FF0FFF0FF0FFF0FF0FFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFFFFFF0FFFF0FFFFFF0000FFF0F0FFFF0000FF0FFF0FF0FFF0FF0
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF00000FFF0F0FFF0FFF0FFFFF0FF0FFF0FFFFFF0F
                F0FFF0FF0FFF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF0FFF0FFF000FFF000
                FFF0FFF0FFF000FF0000F0FFF000FFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFF0FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFF
                FFFFFFFF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF0000FF0FFFFF00000FFFFFFFFFFFFFFFFFFFF0000FFFF0
                00FFF0FFF0FFF00FF000FFF0FFFFFFFF00000F0FF0FFF0FFF000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FF0FFFF0FFFFFFFFFFFFFFFFF
                FFFFFF0FFFF0FF0FFF0FF0FFF0FF0FFF0FFF0FF0FFFFFFFF0FFFFF0FF0FFF0FF
                0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF0FFFF0F
                FFFFFFFF0000000FFFFFF0FFFFFFFF0FFFFFF0FFF0FF0FFF0FFFFFF0FFFFFFFF
                0FFFFF0FF0FFF0FF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF0FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFFFFFFF0FFF0FFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF00000FF0FFF0FF0FFF
                00000FF0FFFFFFFF0FFFFF0FF0FFF0FF00000FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFFFFFFF0FFF0FFFFFFFFF0000000FFFFFF0FFFFFFFF0F
                FF0FF0FFF0FF0FFF0FFF0FF00FFFFFFF0FFFFF0FF0FFF0FF0FFF0FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFF0FF0FFFFFFFFFFFFFFFFF
                FFFFF0FFFFFFFFF000FFF0000FF0000FF000FFF0F00FFFFF0FFFFF0FF0000FFF
                F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FFFF0FF0F
                FFFFFFFFFFFFFFFFFFFFFF0FFFF0FFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFF
                0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFF000FFF0000
                FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000FFF0000F
                FF0000FFF0000FFF0000FFF0000FFF00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF0000FFFFFF0F0FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFF0FFF
                FFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFF000000F0FFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF00000FF000FFFF000FFF0000FFF00FF000FFFFFF
                F0FFFFF0FFF0000FF0FFFF0FFF0FF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFF000FFFFFF0000FFFFFFFFF
                FFFFFFFFFFF00000FFFF0000FFF000FF0FFF0FFFFFFF000FFF0FFFFFFFFF0000
                FFFF0000FF0FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF00FFFFF0FFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFF0FF0FFF0F0FFF0
                FF0FFF0FFF0FFFFFF0FFFFF0FF0FFF0FF0FFFF0FF0FFFFFF0FFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0FFF0FFF0FF
                FF0FFFF0FFFFFFFFFFFFFFFFFFF0FFFF0FF0FFF0FF0FFF0F0FF0FFFFFFF0FFF0
                FF0FFFFFFFF0FFFF0FF0FFF0FF0FFFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFF0FFFFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0F
                FF0FF0FFFFF0FFF0FF0FFF0FFFFFFFFFF0FF0FF0FF0FFF0FF0FFFF000FFFFFF0
                0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFFF0FF0FFFFF0FF0FFFFFFFFFFFF0000000FFFFFF0FFFF0FF0FFF0FF0FFFFF
                000FFFFFFFF0FFF0FF0FFFFFFF0FFFFFFFF0FFF0FF0FFFF0FFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFF000000000FF000000000FF000000000FF000000000FFFFFF00FFFFF0FF0
                FF00FFFFFF000000000FF000000000FF000000000FF00000000FFFFFFFFFFFFF
                FFFFFFFF0FFFFF0FFF0FF0FFFFFF0000FF0FFF00000FFFFFF0FF0FF0FFF0000F
                F0FFFF0F0FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0FFFF0FF0FFFFF0FF0FFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
                0FFF0000FF0FFFFF0F0FFFFFFFF0FFF0FF0FFFFFFF0FFFFFFFF0FFF0FF0FFFF0
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFF00FFFFF0F000F00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0FFF0FF0FFF0FFFFF0FF0FFF0FFF0FFFFF
                F0F0F0F0FFFFFF0FF00FFF0FF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFF0FFFFF0FF0FFFFFFFFFFFF00
                00000FFFFFF00000FFFFFFF0FF0FFF0F0FF0FFFFFFF0FFF0FF0FFFFFFF0FFFFF
                FFF0FFF0FF00FFF0FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF00FFFFF0000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF000FFFF000FFF000F
                F0000FF000FFFFFFF0F0F0F0FFF000FFF0F00F0FFF0FFF000FFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFF0FFFFF0F
                F0FFFFFFFFFFFFFFFFFFFFFFFFF0FFF0FFFF000FFFF000FF0FFF0FFFFFFF000F
                F0000FFFFF0FFFFFFFF0FFF0FF0F00F0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFF000FFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF
                FFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF00FFF00FFFFFFFFFFFFFF0FFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                0FFF0FFFF0FFF0FFFF0FFFF0FFFFFFFFFFFFFFFFFFF0FFF0FFFFFFFFFFFFFFFF
                0FFFFFFFFFFFFFFFFF0FFFFFFFF0FFFF0FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFF00FF
                FFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFF00FFF00FFFFFFFF
                FFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFF000FFFFFF0000FFFFFFFFFFFFFFFFFFFF0000F
                FFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFF0FFFFFFFFF0000FFFFFFFFFFFFFFF0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFF
                FF00FFFF00FF00FF00FF00000FFFF0000FFF00FF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                00FFF00FF00000FFF00000FFF00FFFFF0000FFFFF00FFFF00FFF00000FFF000F
                00FFF0000FFF00FF00FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0FFF00FFFF00FFFF00FF00FF00FF00FF00FF00FF00FF00FFFFF00FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF00FFF00FF00FF00FF00FF00FF00FFFF00FFF0FFF0000FFF0
                0FF00FF00FF00FFF00FF00FF00FF00FF00FFFFF00FFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFF0000FFF00FF00FF00FF00FF00FF00FF
                00FF00FFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000FF00FF00FF00FF00FF00FFFF0
                0FFFFFFF0000FFF00FF00FF00FF00FFF00FF00FF00FF00FF00FFF0000FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFF0000FFF00FF00FF
                00FF00FF00FF00FF00FF00FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00F00FFF00FF00F
                F00FF00FF00FFFF000000FF00FF00FF00FFF00000FF00FFF00FF00FF00FF00FF
                00FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFF
                00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F00F00FFF00FF00FF00FF00FF00000F00FF00FF00FF00FF00FFFFFF00FF00FFF
                00FF00FF00FF00FF00FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF00FFFFFF00FF00FF00000F000FFF00000FFFF0000FFF00FFF0000FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFF00F00FFF00000FFF00000FFF00F00FF0000FFF00FF00FF0
                0FFF0000FF00000F00FFF0000FFF00000FFFF0000FFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF00FFF0FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFF
                FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFF00FFFFFF00FFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFF
                FFFF00FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFF00FFFFF
                F00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFF00FFF00FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF
                0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF00FFFFFFFFFFF0000FFFFFFFFFFFFFFF0000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000FFFFFFFFFFF0000FFFFFFFFFFFF
                FFF0000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000000000FFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000}
              mmHeight = 50536
              mmLeft = 35719
              mmTop = 184680
              mmWidth = 121179
              BandType = 4
              LayerName = Foreground10
            end
            object ppShape2: TppShape
              DesignLayer = ppDesignLayer11
              UserName = 'Shape2'
              ParentWidth = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 0
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground10
            end
            object ppLabel30: TppLabel
              DesignLayer = ppDesignLayer11
              UserName = 'Label30'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'INVESTIGATION SKETCH'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 3175
              mmTop = 529
              mmWidth = 42863
              BandType = 4
              LayerName = Foreground10
            end
          end
          object ppDesignLayers11: TppDesignLayers
            object ppDesignLayer11: TppDesignLayer
              UserName = 'Foreground10'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object AddInfoRegion: TppRegion
        DesignLayer = ppDesignLayer1
        UserName = 'AddInfoRegion'
        Caption = 'AddInfoRegion'
        ParentWidth = True
        Pen.Style = psClear
        Stretch = True
        mmHeight = 6879
        mmLeft = 0
        mmTop = 106363
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 6350
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppLabel26: TppLabel
          DesignLayer = ppDesignLayer1
          UserName = 'Label26'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = 'ADDITIONAL INFORMATION:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3440
          mmLeft = 4233
          mmTop = 108215
          mmWidth = 38629
          BandType = 4
          LayerName = Foreground
        end
        object RemarksMemo: TppDBMemo
          DesignLayer = ppDesignLayer1
          UserName = 'RemarksMemo'
          Border.mmPadding = 0
          CharWrap = False
          DataField = 'remarks'
          DataPipeline = Pipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'ARIAL'
          Font.Size = 8
          Font.Style = []
          RemoveEmptyLines = False
          Stretch = True
          Transparent = True
          DataPipelineName = 'Pipe'
          mmHeight = 4233
          mmLeft = 50800
          mmTop = 108215
          mmWidth = 143404
          BandType = 4
          LayerName = Foreground
          mmBottomOffset = 5080
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          mmLeading = 0
        end
      end
      object ApprovalSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'ApprovalSection'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = NarrativeSection
        TraverseAllData = False
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 251884
        mmWidth = 195580
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport7: TppChildReport
          AutoStop = False
          DataPipeline = Pipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'Pipe'
          object ppDetailBand12: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 26723
            mmPrintPosition = 0
            object ppShape22: TppShape
              DesignLayer = ppDesignLayer12
              UserName = 'Shape1'
              mmHeight = 5556
              mmLeft = 0
              mmTop = 1323
              mmWidth = 195527
              BandType = 4
              LayerName = Foreground11
            end
            object ppLabel157: TppLabel
              DesignLayer = ppDesignLayer12
              UserName = 'Label1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'APPROVAL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 10
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 4233
              mmLeft = 2381
              mmTop = 1323
              mmWidth = 19050
              BandType = 4
              LayerName = Foreground11
            end
            object ppDBText117: TppDBText
              DesignLayer = ppDesignLayer12
              UserName = 'DBText117'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'approved'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 53975
              mmTop = 9525
              mmWidth = 11726
              BandType = 4
              LayerName = Foreground11
            end
            object ppLabel154: TppLabel
              DesignLayer = ppDesignLayer12
              UserName = 'Label154'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Investigation Approved: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15347
              mmTop = 9525
              mmWidth = 37305
              BandType = 4
              LayerName = Foreground11
            end
            object ppLabel155: TppLabel
              DesignLayer = ppDesignLayer12
              UserName = 'Label155'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Approved Date: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15347
              mmTop = 15875
              mmWidth = 37305
              BandType = 4
              LayerName = Foreground11
            end
            object ppLabel156: TppLabel
              DesignLayer = ppDesignLayer12
              UserName = 'Label156'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Approved By:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3440
              mmLeft = 15347
              mmTop = 22490
              mmWidth = 37305
              BandType = 4
              LayerName = Foreground11
            end
            object ppDBText118: TppDBText
              DesignLayer = ppDesignLayer12
              UserName = 'DBText118'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'approved_datetime'
              DataPipeline = Pipe
              DisplayFormat = 'mm/dd/yyyy hh:mm:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 53975
              mmTop = 15875
              mmWidth = 24215
              BandType = 4
              LayerName = Foreground11
            end
            object ppDBText119: TppDBText
              DesignLayer = ppDesignLayer12
              UserName = 'DBText119'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'approver_name'
              DataPipeline = Pipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'ARIAL'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'Pipe'
              mmHeight = 3260
              mmLeft = 53975
              mmTop = 22490
              mmWidth = 19770
              BandType = 4
              LayerName = Foreground11
            end
          end
          object ppDesignLayers12: TppDesignLayers
            object ppDesignLayer12: TppDesignLayer
              UserName = 'Foreground11'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 10319
      mmPrintPosition = 0
      object ppLine2: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line2'
        Border.mmPadding = 0
        Pen.Color = clGray
        ParentWidth = True
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 0
        mmTop = 8731
        mmWidth = 195580
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'SystemVariable1'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        VarType = vtPrintDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1588
        mmTop = 4763
        mmWidth = 31221
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable2: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'SystemVariable2'
        HyperlinkEnabled = False
        AutoSize = False
        Border.mmPadding = 0
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'ARIAL'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 168805
        mmTop = 4763
        mmWidth = 25400
        BandType = 8
        LayerName = Foreground
      end
      object ppLine12: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'Line12'
        Border.mmPadding = 0
        Pen.Color = clGray
        ParentWidth = True
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 0
        mmTop = 4233
        mmWidth = 195580
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppSummaryBand: TppSummaryBand
      Background.Brush.Style = bsClear
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 33338
      mmPrintPosition = 0
      object TicketSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'TicketSection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'DetailPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5821
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport5: TppChildReport
          AutoStop = False
          DataPipeline = DetailPipe
          NoDataBehaviors = [ndBlankReport]
          PageLimit = 50
          PassSetting = psTwoPass
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'DetailPipe'
          object ppHeaderBand2: TppHeaderBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 8731
            mmPrintPosition = 0
            object ppShape9: TppShape
              DesignLayer = ppDesignLayer14
              UserName = 'Shape2'
              Brush.Color = 14737632
              ParentHeight = True
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 8731
              mmLeft = 0
              mmTop = 0
              mmWidth = 203200
              BandType = 0
              LayerName = Foreground13
            end
            object ppDBText26: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'ticket_number'
              DataPipeline = MasterPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 20
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'MasterPipe'
              mmHeight = 8731
              mmLeft = 2910
              mmTop = 0
              mmWidth = 60325
              BandType = 0
              LayerName = Foreground13
            end
            object ppLabel77: TppLabel
              DesignLayer = ppDesignLayer14
              UserName = 'Label1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Received date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 144727
              mmTop = 529
              mmWidth = 18256
              BandType = 0
              LayerName = Foreground13
            end
            object ppDBText27: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'transmit_date'
              DataPipeline = MasterPipe
              DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'MasterPipe'
              mmHeight = 3704
              mmLeft = 166159
              mmTop = 529
              mmWidth = 33867
              BandType = 0
              LayerName = Foreground13
            end
            object ppLabel78: TppLabel
              DesignLayer = ppDesignLayer14
              UserName = 'Label2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Due date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 144727
              mmTop = 4498
              mmWidth = 11377
              BandType = 0
              LayerName = Foreground13
            end
            object ppDBText28: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText3'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'due_date'
              DataPipeline = MasterPipe
              DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'MasterPipe'
              mmHeight = 3704
              mmLeft = 166159
              mmTop = 4498
              mmWidth = 33867
              BandType = 0
              LayerName = Foreground13
            end
          end
          object ppDetailBand6: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 4498
            mmPrintPosition = 0
            object ppDBText69: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText69'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'client_code'
              DataPipeline = DetailPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3704
              mmLeft = 2910
              mmTop = 265
              mmWidth = 24077
              BandType = 4
              LayerName = Foreground13
            end
            object ppDBText70: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText70'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'status_name'
              DataPipeline = DetailPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3704
              mmLeft = 28575
              mmTop = 265
              mmWidth = 26988
              BandType = 4
              LayerName = Foreground13
            end
            object ppDBText71: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText71'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'qty_marked'
              DataPipeline = DetailPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3703
              mmLeft = 56092
              mmTop = 265
              mmWidth = 12700
              BandType = 4
              LayerName = Foreground13
            end
            object ppDBText72: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText72'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'closed_date'
              DataPipeline = DetailPipe
              DisplayFormat = 'm/d/yyyy h:nn am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3703
              mmLeft = 71438
              mmTop = 265
              mmWidth = 30480
              BandType = 4
              LayerName = Foreground13
            end
            object myDBCheckBox1: TmyDBCheckBox
              DesignLayer = ppDesignLayer14
              UserName = 'DBCheckBox1'
              CheckBoxColor = clWindowText
              BooleanFalse = 'False'
              BooleanTrue = 'True'
              DataPipeline = DetailPipe
              DataField = 'high_profile'
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 4233
              mmLeft = 114565
              mmTop = 265
              mmWidth = 3704
              BandType = 4
              LayerName = Foreground13
            end
            object myDBCheckBox2: TmyDBCheckBox
              DesignLayer = ppDesignLayer14
              UserName = 'DBCheckBox2'
              CheckBoxColor = clWindowText
              BooleanFalse = 'False'
              BooleanTrue = 'True'
              DataPipeline = DetailPipe
              DataField = 'closed'
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 4233
              mmLeft = 126736
              mmTop = 265
              mmWidth = 3704
              BandType = 4
              LayerName = Foreground13
            end
            object ppDBText125: TppDBText
              DesignLayer = ppDesignLayer14
              UserName = 'DBText4'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'short_name'
              DataPipeline = DetailPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3704
              mmLeft = 133615
              mmTop = 265
              mmWidth = 40217
              BandType = 4
              LayerName = Foreground13
            end
            object ppDBMemo1: TppDBMemo
              DesignLayer = ppDesignLayer14
              UserName = 'DBMemo1'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'plat_list'
              DataPipeline = DetailPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              RemoveEmptyLines = False
              Stretch = True
              Transparent = True
              DataPipelineName = 'DetailPipe'
              mmHeight = 3704
              mmLeft = 174361
              mmTop = 265
              mmWidth = 28840
              BandType = 4
              LayerName = Foreground13
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
          end
          object ppFooterBand2: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5292
            mmPrintPosition = 0
            object ppShape10: TppShape
              DesignLayer = ppDesignLayer14
              UserName = 'Shape1'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 0
              mmWidth = 203200
              BandType = 8
              LayerName = Foreground13
            end
            object ppReportDateTime: TppCalc
              DesignLayer = ppDesignLayer14
              UserName = 'ppReportDateTime'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 51065
              mmTop = 794
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground13
            end
            object ppLabel79: TppLabel
              DesignLayer = ppDesignLayer14
              UserName = 'Label13'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = '%COMPANY% Ticket Detail'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 3175
              mmTop = 794
              mmWidth = 32544
              BandType = 8
              LayerName = Foreground13
            end
            object ppSystemVariable3: TppSystemVariable
              DesignLayer = ppDesignLayer14
              UserName = 'ppReportPageNo1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 177800
              mmTop = 794
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground13
            end
          end
          object ppSummaryBand2: TppSummaryBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 16669
            mmPrintPosition = 0
            object TicketNotesSubReport: TppSubReport
              DesignLayer = ppDesignLayer14
              UserName = 'TicketNotesSubReport'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ShiftRelativeTo = TicketFacilitiesSubReport
              TraverseAllData = False
              DataPipelineName = 'NotesPipe'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 6350
              mmWidth = 203200
              BandType = 7
              LayerName = Foreground13
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport6: TppChildReport
                AutoStop = False
                DataPipeline = NotesPipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Duplex = dpNone
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '20.01'
                mmColumnWidth = 0
                DataPipelineName = 'NotesPipe'
                object ppTitleBand2: TppTitleBand
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDetailBand7: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background2.Brush.Style = bsClear
                  Border.mmPadding = 0
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 12171
                  mmPrintPosition = 0
                  object NoteMemo: TppDBMemo
                    DesignLayer = ppDesignLayer6
                    UserName = 'NoteMemo'
                    Border.mmPadding = 0
                    CharWrap = False
                    DataField = 'note'
                    DataPipeline = NotesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 9
                    Font.Style = []
                    RemoveEmptyLines = False
                    Stretch = True
                    Transparent = True
                    DataPipelineName = 'NotesPipe'
                    mmHeight = 3969
                    mmLeft = 7144
                    mmTop = 6614
                    mmWidth = 195792
                    BandType = 4
                    LayerName = Foreground5
                    mmBottomOffset = 0
                    mmOverFlowOffset = 0
                    mmStopPosition = 0
                    mmMinHeight = 0
                    mmLeading = 0
                  end
                  object ppDBText75: TppDBText
                    DesignLayer = ppDesignLayer6
                    UserName = 'EntryDate'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'entry_date'
                    DataPipeline = NotesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = [fsBold]
                    Transparent = True
                    DataPipelineName = 'NotesPipe'
                    mmHeight = 3704
                    mmLeft = 23019
                    mmTop = 1852
                    mmWidth = 15610
                    BandType = 4
                    LayerName = Foreground5
                  end
                  object ppLabel80: TppLabel
                    DesignLayer = ppDesignLayer6
                    UserName = 'DateLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Note Entered: '
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = [fsBold]
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 265
                    mmTop = 1852
                    mmWidth = 20373
                    BandType = 4
                    LayerName = Foreground5
                  end
                end
                object ppSummaryNotesTicket: TppSummaryBand
                  Save = True
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDesignLayers6: TppDesignLayers
                  object ppDesignLayer6: TppDesignLayer
                    UserName = 'Foreground5'
                    LayerType = ltBanded
                    Index = 0
                  end
                end
              end
            end
            object TicketFacilitiesSubReport: TppSubReport
              DesignLayer = ppDesignLayer14
              UserName = 'TicketFacilitiesSubReport'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              TraverseAllData = False
              DataPipelineName = 'TicketFacilitiesPipe'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 1058
              mmWidth = 203200
              BandType = 7
              LayerName = Foreground13
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppTicketFacilitiesChildReport: TppChildReport
                AutoStop = False
                DataPipeline = TicketFacilitiesPipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Duplex = dpNone
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 10160
                PrinterSetup.mmMarginRight = 10160
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '20.01'
                mmColumnWidth = 0
                DataPipelineName = 'TicketFacilitiesPipe'
                object ppTitleBand3: TppTitleBand
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 13229
                  mmPrintPosition = 0
                  object ppShape20: TppShape
                    DesignLayer = ppDesignLayer3
                    UserName = 'Shape20'
                    Brush.Color = 14737632
                    ParentHeight = True
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 13229
                    mmLeft = 0
                    mmTop = 0
                    mmWidth = 195580
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilitySectionTitle: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilitySectionTitle'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Facilities:'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 794
                    mmTop = 1058
                    mmWidth = 11642
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityClientLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityClientLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Client'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 16404
                    mmTop = 5821
                    mmWidth = 6879
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityTypeLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityTypeLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Facility Type'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 33602
                    mmTop = 5821
                    mmWidth = 15875
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityMaterialLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityMaterialLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Facility Material'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 54504
                    mmTop = 5821
                    mmWidth = 19315
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilitySizeLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilitySizeLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Facility Size'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 82286
                    mmTop = 5821
                    mmWidth = 15081
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityPressureLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityPressureLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Pressure'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 104775
                    mmTop = 5821
                    mmWidth = 11642
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityOffsetLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityOffsetLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'No. of Offsets'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    TextAlignment = taCentered
                    Transparent = True
                    WordWrap = True
                    mmHeight = 7408
                    mmLeft = 127000
                    mmTop = 2117
                    mmWidth = 9790
                    BandType = 1
                    LayerName = Foreground2
                  end
                  object FacilityAddedByLabel: TppLabel
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityAddedByLabel'
                    HyperlinkEnabled = False
                    Border.mmPadding = 0
                    Caption = 'Added By'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                    FormFieldSettings.FormFieldType = fftNone
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 138113
                    mmTop = 5821
                    mmWidth = 12700
                    BandType = 1
                    LayerName = Foreground2
                  end
                end
                object ppDetailBand10: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background2.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 4233
                  mmPrintPosition = 0
                  object FacClient: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacClient'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'client_code'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 16404
                    mmTop = 0
                    mmWidth = 14552
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacType: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacType'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'facility_type'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 33602
                    mmTop = 0
                    mmWidth = 15346
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacMaterial: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacMaterial'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'facility_material'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 54504
                    mmTop = 0
                    mmWidth = 19579
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacSize: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacSize'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'facility_size'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 82286
                    mmTop = 0
                    mmWidth = 15081
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacOffsets: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacOffsets'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'offset'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taCentered
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 128059
                    mmTop = 0
                    mmWidth = 7673
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacAddedBy: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacAddedBy'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'added_by_name'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 138113
                    mmTop = 0
                    mmWidth = 21167
                    BandType = 4
                    LayerName = Foreground2
                  end
                  object FacilityPressure: TppDBText
                    DesignLayer = ppDesignLayer3
                    UserName = 'FacilityPressure'
                    HyperlinkEnabled = False
                    AutoSize = True
                    Border.mmPadding = 0
                    DataField = 'facility_pressure'
                    DataPipeline = TicketFacilitiesPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'TicketFacilitiesPipe'
                    mmHeight = 3704
                    mmLeft = 104775
                    mmTop = 0
                    mmWidth = 21431
                    BandType = 4
                    LayerName = Foreground2
                  end
                end
                object ppSummaryBand3: TppSummaryBand
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDesignLayers3: TppDesignLayers
                  object ppDesignLayer3: TppDesignLayer
                    UserName = 'Foreground2'
                    LayerType = ltBanded
                    Index = 0
                  end
                end
              end
            end
            object TicketImagesSubreport: TppSubReport
              DesignLayer = ppDesignLayer14
              UserName = 'TicketImagesSubreport'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ShiftRelativeTo = TicketNotesSubReport
              TraverseAllData = True
              DataPipelineName = 'TicketAttachmentsPipe'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 11641
              mmWidth = 203200
              BandType = 7
              LayerName = Foreground13
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReportTicketAttachments: TppChildReport
                AutoStop = False
                DataPipeline = TicketAttachmentsPipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Duplex = dpNone
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 10160
                PrinterSetup.mmMarginRight = 10160
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '20.01'
                mmColumnWidth = 0
                DataPipelineName = 'TicketAttachmentsPipe'
                object ppTitleBand4: TppTitleBand
                  Visible = False
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDetailBandTicketAttach: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background2.Brush.Style = bsClear
                  Border.mmPadding = 0
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 221721
                  mmPrintPosition = 0
                  object TicketAttachmentRegion: TppRegion
                    DesignLayer = ppDesignLayer13
                    UserName = 'TicketAttachmentRegion'
                    KeepTogether = True
                    Brush.Style = bsClear
                    ParentHeight = True
                    ParentWidth = True
                    Pen.Color = clWhite
                    Pen.Style = psClear
                    Stretch = True
                    Transparent = True
                    mmHeight = 221721
                    mmLeft = 0
                    mmTop = 0
                    mmWidth = 195580
                    BandType = 4
                    LayerName = Foreground12
                    mmBottomOffset = 0
                    mmOverFlowOffset = 0
                    mmStopPosition = 0
                    mmMinHeight = 0
                    object ppDBText91: TppDBText
                      DesignLayer = ppDesignLayer13
                      UserName = 'DBText91'
                      HyperlinkEnabled = False
                      Border.mmPadding = 0
                      DataField = 'comment'
                      DataPipeline = TicketAttachmentsPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 9
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      WordWrap = True
                      DataPipelineName = 'TicketAttachmentsPipe'
                      mmHeight = 14023
                      mmLeft = 2646
                      mmTop = 9790
                      mmWidth = 190765
                      BandType = 4
                      LayerName = Foreground12
                    end
                    object ppTicketImage: TppImage
                      OnPrint = ppTicketImagePrint
                      DesignLayer = ppDesignLayer13
                      UserName = 'TicketImage'
                      AlignHorizontal = ahCenter
                      AlignVertical = avCenter
                      DirectDraw = True
                      MaintainAspectRatio = True
                      ReprintOnOverFlow = True
                      Border.mmPadding = 0
                      mmHeight = 196321
                      mmLeft = 0
                      mmTop = 24871
                      mmWidth = 195792
                      BandType = 4
                      LayerName = Foreground12
                    end
                    object ppMemoErrorMessageTicket: TppMemo
                      DesignLayer = ppDesignLayer13
                      UserName = 'MemoErrorMessageTicket'
                      Border.mmPadding = 0
                      Caption = 'MemoErrorMessageTicket'
                      CharWrap = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      RemoveEmptyLines = False
                      Stretch = True
                      Transparent = True
                      mmHeight = 23548
                      mmLeft = 2381
                      mmTop = 25400
                      mmWidth = 191030
                      BandType = 4
                      LayerName = Foreground12
                      mmBottomOffset = 0
                      mmOverFlowOffset = 0
                      mmStopPosition = 0
                      mmMinHeight = 0
                      mmLeading = 0
                    end
                    object ppDBText116: TppDBText
                      DesignLayer = ppDesignLayer13
                      UserName = 'DBText1'
                      HyperlinkEnabled = False
                      Border.mmPadding = 0
                      DataField = 'display_name'
                      DataPipeline = TicketAttachmentsPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Verdana'
                      Font.Size = 9
                      Font.Style = [fsBold]
                      ParentDataPipeline = False
                      Transparent = True
                      WordWrap = True
                      DataPipelineName = 'TicketAttachmentsPipe'
                      mmHeight = 7673
                      mmLeft = 2381
                      mmTop = 1323
                      mmWidth = 191030
                      BandType = 4
                      LayerName = Foreground12
                    end
                  end
                end
                object ppSummaryBand5: TppSummaryBand
                  Background.Brush.Style = bsClear
                  Border.mmPadding = 0
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDesignLayers13: TppDesignLayers
                  object ppDesignLayer13: TppDesignLayer
                    UserName = 'Foreground12'
                    LayerType = ltBanded
                    Index = 0
                  end
                end
              end
            end
          end
          object ppGroup3: TppGroup
            BreakName = 'ticket_id'
            DataPipeline = DetailPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            KeepTogether = True
            OutlineSettings.CreateNode = True
            StartOnOddPage = False
            UserName = 'Group3'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'DetailPipe'
            NewFile = False
            object ppGroupHeaderBand3: TppGroupHeaderBand
              BeforeGenerate = ppGroupHeaderBand3BeforeGenerate
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              PrintHeight = phDynamic
              mmBottomOffset = 0
              mmHeight = 202936
              mmPrintPosition = 0
              object DetailHeaderRegion: TppRegion
                DesignLayer = ppDesignLayer14
                UserName = 'DetailHeaderRegion'
                Brush.Color = 14737632
                Caption = 'DetailHeaderRegion'
                ParentWidth = True
                Pen.Style = psClear
                mmHeight = 6085
                mmLeft = 0
                mmTop = 196850
                mmWidth = 203200
                BandType = 3
                GroupNo = 0
                LayerName = Foreground13
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppLabel81: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label81'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Client'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 2910
                  mmTop = 198173
                  mmWidth = 24077
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel82: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label82'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Status'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 28575
                  mmTop = 198173
                  mmWidth = 9790
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel85: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label85'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  Border.mmPadding = 0
                  Caption = 'Qty Marked'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  TextAlignment = taRightJustified
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 53711
                  mmTop = 198173
                  mmWidth = 15081
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel86: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label86'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'When Closed'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 71438
                  mmTop = 198173
                  mmWidth = 37306
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel101: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label101'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  Border.mmPadding = 0
                  Caption = 'High Profile'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  TextAlignment = taCentered
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 109273
                  mmTop = 198173
                  mmWidth = 14288
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel102: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label102'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  Border.mmPadding = 0
                  Caption = 'Closed'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  TextAlignment = taCentered
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 124354
                  mmTop = 198173
                  mmWidth = 8731
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel104: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label104'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Assigned To'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 134409
                  mmTop = 198173
                  mmWidth = 22754
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
                object ppLabel144: TppLabel
                  DesignLayer = ppDesignLayer14
                  UserName = 'Label144'
                  HyperlinkEnabled = False
                  Anchors = [atLeft, atBottom]
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Plat Number'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 174625
                  mmTop = 198173
                  mmWidth = 22754
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground13
                end
              end
              object ImageMemo: TppMemo
                OnPrint = ImageMemoPrint
                DesignLayer = ppDesignLayer14
                UserName = 'ImageMemo'
                Border.mmPadding = 0
                Caption = 'ImageMemo'
                CharWrap = True
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Courier New'
                Font.Size = 10
                Font.Style = []
                RemoveEmptyLines = False
                Transparent = True
                mmHeight = 194469
                mmLeft = 2381
                mmTop = 1323
                mmWidth = 197644
                BandType = 3
                GroupNo = 0
                LayerName = Foreground13
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object NoTicketWarningLabel: TppLabel
                DesignLayer = ppDesignLayer14
                UserName = 'NoTicketWarningLabel'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'There is no ticket associated with this damage investigation.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 14
                Font.Style = []
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taCentered
                Transparent = True
                Visible = False
                WordWrap = True
                mmHeight = 36248
                mmLeft = 58738
                mmTop = 48154
                mmWidth = 84667
                BandType = 3
                GroupNo = 0
                LayerName = Foreground13
              end
            end
            object ppGroupFooterBand3: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              PrintHeight = phDynamic
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object ppDesignLayers14: TppDesignLayers
            object ppDesignLayer14: TppDesignLayer
              UserName = 'Foreground13'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object ThirdPartySection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'ThirdPartySection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ThirdPartyPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 11377
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppThirdPartyChildReport: TppChildReport
          AutoStop = False
          DataPipeline = ThirdPartyPipe
          NoDataBehaviors = [ndBlankReport]
          PassSetting = psTwoPass
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'ThirdPartyPipe'
          object ThirdPartyDetailBand: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 49477
            mmPrintPosition = 0
            object ThirdPartyDetailRegion: TppRegion
              DesignLayer = ppDesignLayer4
              UserName = 'ThirdPartyDetailRegion'
              KeepTogether = True
              Brush.Style = bsClear
              Caption = ' '
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              Transparent = True
              mmHeight = 32808
              mmLeft = 0
              mmTop = 0
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground3
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ThirdPartyClaimDescrMemo: TppDBMemo
                DesignLayer = ppDesignLayer4
                UserName = 'ThirdPartyClaimDescrMemo'
                Border.mmPadding = 0
                CharWrap = False
                DataField = 'claim_desc'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 7673
                mmLeft = 29633
                mmTop = 22754
                mmWidth = 148167
                BandType = 4
                LayerName = Foreground3
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppDBText92: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText92'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'claimant'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 29633
                mmTop = 4234
                mmWidth = 71702
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText93: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText93'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'address1'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 29633
                mmTop = 8996
                mmWidth = 71702
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText94: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText94'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'city'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 29633
                mmTop = 17992
                mmWidth = 38894
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText95: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText95'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'state'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 69586
                mmTop = 17992
                mmWidth = 6615
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText96: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText96'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'demand'
                DataPipeline = ThirdPartyPipe
                DisplayFormat = '#,0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 127000
                mmTop = 17992
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText97: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText97'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'phone'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 127000
                mmTop = 4234
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel105: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label52'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Claimant:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 14373
                mmTop = 3969
                mmWidth = 12615
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel108: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label54'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Address:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 15050
                mmTop = 8731
                mmWidth = 11938
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText108: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText52'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'address2'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 29633
                mmTop = 13494
                mmWidth = 71702
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel118: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label55'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'City State Zip:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 8106
                mmTop = 17727
                mmWidth = 18881
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText109: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText53'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'zipcode'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 77788
                mmTop = 17992
                mmWidth = 23548
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel119: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label56'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Description:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 10901
                mmTop = 22754
                mmWidth = 16087
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel120: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label57'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Telephone:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 110860
                mmTop = 3969
                mmWidth = 14817
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel121: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label58'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Demand:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 113379
                mmTop = 17727
                mmWidth = 11769
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel122: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label59'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Notified Date:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 107813
                mmTop = 8731
                mmWidth = 17865
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText110: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText54'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_notified_date'
                DataPipeline = ThirdPartyPipe
                DisplayFormat = 'mm/dd/yyyy h:nnAMPM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 127000
                mmTop = 8996
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground3
              end
              object ppLabel123: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label116'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Carrier:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 115602
                mmTop = 13229
                mmWidth = 10075
                BandType = 4
                LayerName = Foreground3
              end
              object ppDBText111: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText57'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'carrier_name_third_party'
                DataPipeline = ThirdPartyPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'ThirdPartyPipe'
                mmHeight = 3979
                mmLeft = 127000
                mmTop = 13494
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground3
              end
            end
            object NoThirdPartyWarningLabel: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'NoLitigationWarningLabel1'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 
                'There are no third parties associated with this damage investiga' +
                'tion.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 14
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taCentered
              Transparent = True
              Visible = False
              WordWrap = True
              mmHeight = 16140
              mmLeft = 53446
              mmTop = 33338
              mmWidth = 88636
              BandType = 4
              LayerName = Foreground3
            end
          end
          object ppFooterBand3: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5821
            mmPrintPosition = 0
            object ppShape13: TppShape
              DesignLayer = ppDesignLayer4
              UserName = 'Shape13'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 529
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground3
            end
            object ppSystemVariable4: TppSystemVariable
              DesignLayer = ppDesignLayer4
              UserName = 'SystemVariable4'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 172244
              mmTop = 1058
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground3
            end
            object ppCalc1: TppCalc
              DesignLayer = ppDesignLayer4
              UserName = 'ppReportDateTime1'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 48154
              mmTop = 1058
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground3
            end
            object ppLabel124: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label108'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Third Party Details'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3302
              mmLeft = 265
              mmTop = 1058
              mmWidth = 24469
              BandType = 8
              LayerName = Foreground3
            end
          end
          object ThirdPartyDamgeIDGroup: TppGroup
            BreakName = 'damage_id'
            DataPipeline = ThirdPartyPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            OutlineSettings.CreateNode = True
            NewPage = True
            ReprintOnSubsequentPage = False
            StartOnOddPage = False
            UserName = 'ThirdPartyDamgeIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'ThirdPartyPipe'
            NewFile = False
            object ThirdPartyDamageIDGroupHeaderBand: TppGroupHeaderBand
              BeforeGenerate = ThirdPartyDamageIDGroupHeaderBandBeforeGenerate
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 12435
              mmPrintPosition = 0
              object ppLabel125: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label44'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'INVESTIGATION #:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3440
                mmLeft = 0
                mmTop = 1323
                mmWidth = 28046
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
              object ppDBText112: TppDBText
                DesignLayer = ppDesignLayer4
                UserName = 'DBText49'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_damage_id'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 12
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4995
                mmLeft = 31221
                mmTop = 265
                mmWidth = 32544
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
              object ppShape11: TppShape
                DesignLayer = ppDesignLayer4
                UserName = 'Shape11'
                mmHeight = 5556
                mmLeft = 0
                mmTop = 6615
                mmWidth = 195527
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
              object ppLabel126: TppLabel
                DesignLayer = ppDesignLayer4
                UserName = 'Label126'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'THIRD PARTY'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 529
                mmTop = 7144
                mmWidth = 24077
                BandType = 3
                GroupNo = 0
                LayerName = Foreground3
              end
            end
            object ppGroupFooterBand4: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object ThirdPartyIDGroup: TppGroup
            BreakName = 'third_party_id'
            DataPipeline = ThirdPartyPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            KeepTogether = True
            OutlineSettings.CreateNode = True
            ReprintOnSubsequentPage = False
            StartOnOddPage = False
            UserName = 'ThirdPartyIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'ThirdPartyPipe'
            NewFile = False
            object ppGroupHeaderBand2: TppGroupHeaderBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
            object ppGroupFooterBand2: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 3440
              mmPrintPosition = 0
              object ppLine14: TppLine
                DesignLayer = ppDesignLayer4
                UserName = 'Line3'
                Border.mmPadding = 0
                ParentWidth = True
                StretchWithParent = True
                Weight = 0.750000000000000000
                mmHeight = 3969
                mmLeft = 0
                mmTop = 529
                mmWidth = 195580
                BandType = 5
                GroupNo = 0
                LayerName = Foreground3
              end
            end
          end
          object ppDesignLayers4: TppDesignLayers
            object ppDesignLayer4: TppDesignLayer
              UserName = 'Foreground3'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object LitigationSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'LitigationSection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'LitigationPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 16933
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppLitigationChildReport: TppChildReport
          AutoStop = False
          DataPipeline = LitigationPipe
          NoDataBehaviors = [ndBlankReport]
          PassSetting = psTwoPass
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'LitigationPipe'
          object LitigationDetailBand: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 44715
            mmPrintPosition = 0
            object NoLitigationWarningLabel: TppLabel
              DesignLayer = ppDesignLayer5
              UserName = 'NoLitigationWarningLabel'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 
                'There are no litigations associated with this damage investigati' +
                'on.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 14
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taCentered
              Transparent = True
              Visible = False
              WordWrap = True
              mmHeight = 16140
              mmLeft = 53446
              mmTop = 28575
              mmWidth = 88636
              BandType = 4
              LayerName = Foreground4
            end
            object LitigationDetailRegion: TppRegion
              DesignLayer = ppDesignLayer5
              UserName = 'LitigationDetailRegion'
              KeepTogether = True
              Brush.Style = bsClear
              Caption = 'LitigationDetailRegion'
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              Transparent = True
              mmHeight = 28575
              mmLeft = 0
              mmTop = 265
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText98: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText98'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'plaintiff'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 25929
                mmTop = 8202
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText99: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText99'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'defendant'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 25929
                mmTop = 12700
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText100: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText100'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'file_number'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 25929
                mmTop = 3175
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText101: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText1001'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'venue'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3175
                mmLeft = 108215
                mmTop = 12700
                mmWidth = 84667
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText102: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText1002'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'venue_state'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3175
                mmLeft = 108215
                mmTop = 17727
                mmWidth = 16140
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText103: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText1003'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'service_date'
                DataPipeline = LitigationPipe
                DisplayFormat = 'mm/dd/yyyy'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 108215
                mmTop = 8202
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText104: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText1004'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'attorney_name_description'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 25929
                mmTop = 17727
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText105: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText105'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'demand'
                DataPipeline = LitigationPipe
                DisplayFormat = '#,0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 108215
                mmTop = 22490
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel106: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label60'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'File Number:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 7493
                mmTop = 2910
                mmWidth = 16849
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel107: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label601'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Plaintiff:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 13335
                mmTop = 7938
                mmWidth = 11007
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel109: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label602'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Defendant:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 9949
                mmTop = 12435
                mmWidth = 14393
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText106: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText55'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'summons_date'
                DataPipeline = LitigationPipe
                DisplayFormat = 'mm/dd/yyyy h:nnAMPM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 108215
                mmTop = 3175
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel110: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label107'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Attorney:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 11980
                mmTop = 17463
                mmWidth = 12361
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel111: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label603'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Summons Date:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 85631
                mmTop = 3440
                mmWidth = 20997
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel112: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label111'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Service Date:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 89101
                mmTop = 8467
                mmWidth = 17526
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel113: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label112'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Venue:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 97398
                mmTop = 12965
                mmWidth = 9229
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel114: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label113'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Venue State:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 89863
                mmTop = 17992
                mmWidth = 16764
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel115: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label114'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Carrier:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 14267
                mmTop = 22225
                mmWidth = 10075
                BandType = 4
                LayerName = Foreground4
              end
              object ppDBText107: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText56'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'carrier_name_litigation'
                DataPipeline = LitigationPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                DataPipelineName = 'LitigationPipe'
                mmHeight = 3302
                mmLeft = 25929
                mmTop = 22490
                mmWidth = 50800
                BandType = 4
                LayerName = Foreground4
              end
              object ppLabel116: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label115'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Demand:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3302
                mmLeft = 94858
                mmTop = 22754
                mmWidth = 11769
                BandType = 4
                LayerName = Foreground4
              end
            end
          end
          object LitigationFooterBand: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 6879
            mmPrintPosition = 0
            object ppShape14: TppShape
              DesignLayer = ppDesignLayer5
              UserName = 'Shape14'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 1058
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground4
            end
            object ppSystemVariable5: TppSystemVariable
              DesignLayer = ppDesignLayer5
              UserName = 'SystemVariable5'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 171715
              mmTop = 1588
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground4
            end
            object ppCalc2: TppCalc
              DesignLayer = ppDesignLayer5
              UserName = 'Calc2'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 48419
              mmTop = 1588
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground4
            end
            object ppLabel117: TppLabel
              DesignLayer = ppDesignLayer5
              UserName = 'Label109'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Litigation Details'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3302
              mmLeft = 529
              mmTop = 1588
              mmWidth = 22183
              BandType = 8
              LayerName = Foreground4
            end
          end
          object LitigationDamageIDGroup: TppGroup
            BreakName = 'damage_id'
            DataPipeline = LitigationPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            OutlineSettings.CreateNode = True
            NewPage = True
            ReprintOnSubsequentPage = False
            StartOnOddPage = False
            UserName = 'LitigationDamageIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'LitigationPipe'
            NewFile = False
            object LitigationDamageIDGroupHeaderBand: TppGroupHeaderBand
              BeforeGenerate = LitigationDamageIDGroupHeaderBandBeforeGenerate
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 11906
              mmPrintPosition = 0
              object ppShape12: TppShape
                DesignLayer = ppDesignLayer5
                UserName = 'Shape12'
                mmHeight = 5556
                mmLeft = 265
                mmTop = 6350
                mmWidth = 195527
                BandType = 3
                GroupNo = 0
                LayerName = Foreground4
              end
              object ppDBText113: TppDBText
                DesignLayer = ppDesignLayer5
                UserName = 'DBText51'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_damage_id'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 12
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4995
                mmLeft = 32015
                mmTop = 529
                mmWidth = 32544
                BandType = 3
                GroupNo = 0
                LayerName = Foreground4
              end
              object ppLabel127: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label45'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'INVESTIGATION #:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3440
                mmLeft = 794
                mmTop = 1588
                mmWidth = 28046
                BandType = 3
                GroupNo = 0
                LayerName = Foreground4
              end
              object ppLabel128: TppLabel
                DesignLayer = ppDesignLayer5
                UserName = 'Label51'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'LITIGATION'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 1323
                mmTop = 6879
                mmWidth = 20405
                BandType = 3
                GroupNo = 0
                LayerName = Foreground4
              end
            end
            object ppGroupFooterBand1: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object LitigationIDGroup: TppGroup
            BreakName = 'litigation_id'
            DataPipeline = LitigationPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            KeepTogether = True
            OutlineSettings.CreateNode = True
            ReprintOnSubsequentPage = False
            StartOnOddPage = False
            UserName = 'LitigationIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'LitigationPipe'
            NewFile = False
            object ppGroupHeaderBand5: TppGroupHeaderBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
            object ppGroupFooterBand5: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 3175
              mmPrintPosition = 0
              object ppLine11: TppLine
                DesignLayer = ppDesignLayer5
                UserName = 'Line2'
                Border.mmPadding = 0
                ParentWidth = True
                StretchWithParent = True
                Weight = 0.750000000000000000
                mmHeight = 2910
                mmLeft = 0
                mmTop = 0
                mmWidth = 195580
                BandType = 5
                GroupNo = 1
                LayerName = Foreground4
              end
            end
          end
          object ppDesignLayers5: TppDesignLayers
            object ppDesignLayer5: TppDesignLayer
              UserName = 'Foreground4'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object DamageNotesSection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'DamageNotesSection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'DamageNotesPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 22490
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport8: TppChildReport
          AutoStop = False
          DataPipeline = DamageNotesPipe
          NoDataBehaviors = [ndBlankReport]
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'DamageNotesPipe'
          object ppDetailBand9: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 26194
            mmPrintPosition = 0
            object ppLabel129: TppLabel
              DesignLayer = ppDesignLayer15
              UserName = 'DateLabel1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Note Entered: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 0
              mmTop = 794
              mmWidth = 20373
              BandType = 4
              LayerName = Foreground14
            end
            object ppDBText114: TppDBText
              DesignLayer = ppDesignLayer15
              UserName = 'EntryDate1'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'entry_date'
              DataPipeline = DamageNotesPipe
              DisplayFormat = 'm/d/yyyy h:nn am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              DataPipelineName = 'DamageNotesPipe'
              mmHeight = 3302
              mmLeft = 22754
              mmTop = 794
              mmWidth = 14055
              BandType = 4
              LayerName = Foreground14
            end
            object ppDBMemo7: TppDBMemo
              DesignLayer = ppDesignLayer15
              UserName = 'NoteMemo1'
              Border.mmPadding = 0
              CharWrap = False
              DataField = 'note'
              DataPipeline = DamageNotesPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              RemoveEmptyLines = False
              Stretch = True
              Transparent = True
              DataPipelineName = 'DamageNotesPipe'
              mmHeight = 3969
              mmLeft = 6350
              mmTop = 5556
              mmWidth = 189442
              BandType = 4
              LayerName = Foreground14
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              mmLeading = 0
            end
            object NoNotesWarningLabel: TppLabel
              DesignLayer = ppDesignLayer15
              UserName = 'NoNotesWarningLabel'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 'There are no notes associated with this damage investigation.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 14
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taCentered
              Transparent = True
              Visible = False
              WordWrap = True
              mmHeight = 16140
              mmLeft = 53446
              mmTop = 10054
              mmWidth = 88636
              BandType = 4
              LayerName = Foreground14
            end
          end
          object ppFooterBand4: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5821
            mmPrintPosition = 0
            object ppShape15: TppShape
              DesignLayer = ppDesignLayer15
              UserName = 'Shape15'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 265
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground14
            end
            object ppLabel142: TppLabel
              DesignLayer = ppDesignLayer15
              UserName = 'Label142'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Damage Notes'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3302
              mmLeft = 529
              mmTop = 794
              mmWidth = 19050
              BandType = 8
              LayerName = Foreground14
            end
            object ppCalc3: TppCalc
              DesignLayer = ppDesignLayer15
              UserName = 'Calc3'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 49742
              mmTop = 794
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground14
            end
            object ppSystemVariable6: TppSystemVariable
              DesignLayer = ppDesignLayer15
              UserName = 'SystemVariable6'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 172773
              mmTop = 794
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground14
            end
          end
          object DamageNotesDamageIDGroup: TppGroup
            BreakName = 'damage_id'
            DataPipeline = DamageNotesPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            OutlineSettings.CreateNode = True
            NewPage = True
            StartOnOddPage = False
            UserName = 'DamageNotesDamageIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'DamageNotesPipe'
            NewFile = False
            object ppGroupHeaderBand4: TppGroupHeaderBand
              BeforeGenerate = ppGroupHeaderBand4BeforeGenerate
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 13494
              mmPrintPosition = 0
              object ppLabel130: TppLabel
                DesignLayer = ppDesignLayer15
                UserName = 'Label124'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'INVESTIGATION #:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3440
                mmLeft = 265
                mmTop = 3175
                mmWidth = 28046
                BandType = 3
                GroupNo = 0
                LayerName = Foreground14
              end
              object ppDBText115: TppDBText
                DesignLayer = ppDesignLayer15
                UserName = 'DBText108'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_damage_id'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 12
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4995
                mmLeft = 31750
                mmTop = 1588
                mmWidth = 32544
                BandType = 3
                GroupNo = 0
                LayerName = Foreground14
              end
              object ppShape19: TppShape
                DesignLayer = ppDesignLayer15
                UserName = 'Shape19'
                mmHeight = 5556
                mmLeft = 265
                mmTop = 7938
                mmWidth = 195527
                BandType = 3
                GroupNo = 0
                LayerName = Foreground14
              end
              object ppLabel131: TppLabel
                DesignLayer = ppDesignLayer15
                UserName = 'Label125'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'NOTES'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 1323
                mmTop = 8467
                mmWidth = 12361
                BandType = 3
                GroupNo = 0
                LayerName = Foreground14
              end
            end
            object ppGroupFooterBand8: TppGroupFooterBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object ppDesignLayers15: TppDesignLayers
            object ppDesignLayer15: TppDesignLayer
              UserName = 'Foreground14'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object HistorySection: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'HistorySection'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'HistoryPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 28310
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport10: TppChildReport
          AutoStop = False
          DataPipeline = HistoryPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'HistoryPipe'
          object ppDetailBand11: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 17198
            mmPrintPosition = 0
            object ppLine13: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line13'
              Anchors = [atBottom]
              Border.mmPadding = 0
              ParentHeight = True
              ParentWidth = True
              Position = lpBottom
              ReprintOnOverFlow = True
              StretchWithParent = True
              Weight = 0.750000000000000000
              mmHeight = 17198
              mmLeft = 0
              mmTop = 0
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground1
            end
            object ModifiedDate: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText109'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'modified_date'
              DataPipeline = HistoryPipe
              DisplayFormat = 'm/d/yyyy h:nn AM/PM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 794
              mmTop = 265
              mmWidth = 25665
              BandType = 4
              LayerName = Foreground1
            end
            object ProfitCenter: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText110'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'profit_center'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 30956
              mmTop = 265
              mmWidth = 15081
              BandType = 4
              LayerName = Foreground1
            end
            object ExcRespCode: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText111'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_code'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 46566
              mmTop = 265
              mmWidth = 18256
              BandType = 4
              LayerName = Foreground1
            end
            object ExcRespType: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText112'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'exc_resp_type'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 67998
              mmTop = 265
              mmWidth = 17463
              BandType = 4
              LayerName = Foreground1
            end
            object UQRespType: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText113'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'uq_resp_type'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 110596
              mmTop = 265
              mmWidth = 16404
              BandType = 4
              LayerName = Foreground1
            end
            object UQRespCode: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText114'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'uq_resp_code'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 89165
              mmTop = 265
              mmWidth = 17198
              BandType = 4
              LayerName = Foreground1
            end
            object SPCRespType: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText115'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_type'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 152400
              mmTop = 264
              mmWidth = 17727
              BandType = 4
              LayerName = Foreground1
            end
            object SPCRespCode: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText116'
              HyperlinkEnabled = False
              AutoSize = True
              Border.mmPadding = 0
              DataField = 'spc_resp_code'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 3175
              mmLeft = 131234
              mmTop = 265
              mmWidth = 18521
              BandType = 4
              LayerName = Foreground1
            end
            object dbReasonChanged: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'reason_changed_description'
              DataPipeline = HistoryPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              WordWrap = True
              DataPipelineName = 'HistoryPipe'
              mmHeight = 15875
              mmLeft = 171980
              mmTop = 264
              mmWidth = 23019
              BandType = 4
              LayerName = Foreground1
            end
            object RegionOtherDesc: TppRegion
              DesignLayer = ppDesignLayer2
              UserName = 'RegionOtherDesc'
              KeepTogether = True
              Brush.Style = bsClear
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              Transparent = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 3969
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppLabel146: TppLabel
                DesignLayer = ppDesignLayer2
                UserName = 'Label146'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Other Description'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3175
                mmLeft = 21431
                mmTop = 4498
                mmWidth = 21960
                BandType = 4
                LayerName = Foreground1
              end
              object ExcRespOtherDesc: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'ExcRespOtherDesc'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'exc_resp_other_desc'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 46566
                mmTop = 4498
                mmWidth = 42069
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object UQRespOtherDesc: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'UQRespOtherDesc'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'uq_resp_other_desc'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 89164
                mmTop = 4498
                mmWidth = 42069
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
            end
            object RegionDetails: TppRegion
              DesignLayer = ppDesignLayer2
              UserName = 'RegionDetails'
              KeepTogether = True
              Brush.Style = bsClear
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = RegionOtherDesc
              Stretch = True
              Transparent = True
              mmHeight = 5027
              mmLeft = 0
              mmTop = 8467
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppLabel147: TppLabel
                DesignLayer = ppDesignLayer2
                UserName = 'Label147'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Details'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3175
                mmLeft = 34660
                mmTop = 9261
                mmWidth = 8731
                BandType = 4
                LayerName = Foreground1
              end
              object ExcRespDetails: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'ExcRespDetails'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'exc_resp_details'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 46566
                mmTop = 9261
                mmWidth = 42070
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object UQRespDetails: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'DBMemo102'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'uq_resp_details'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 89164
                mmTop = 9261
                mmWidth = 42070
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object SPCRespDetails: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'SPCRespDetails'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'spc_resp_details'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 131234
                mmTop = 9261
                mmWidth = 40746
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
            end
            object RegionResponse: TppRegion
              DesignLayer = ppDesignLayer2
              UserName = 'RegionResponse'
              KeepTogether = True
              Brush.Style = bsClear
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = RegionDetails
              Stretch = True
              Transparent = True
              mmHeight = 4763
              mmLeft = 0
              mmTop = 12171
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground1
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppLabel148: TppLabel
                DesignLayer = ppDesignLayer2
                UserName = 'Label148'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'Response/Essential Step'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 3260
                mmLeft = 11642
                mmTop = 12964
                mmWidth = 31750
                BandType = 4
                LayerName = Foreground1
              end
              object ExcRespResponse: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'ExcRespResponse'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'exc_resp_response'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 46566
                mmTop = 12965
                mmWidth = 42070
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object UQRespEss: TppDBMemo
                DesignLayer = ppDesignLayer2
                UserName = 'ExcRespResponse1'
                Border.mmPadding = 0
                CharWrap = True
                DataField = 'uq_resp_ess_step_description'
                DataPipeline = HistoryPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                DataPipelineName = 'HistoryPipe'
                mmHeight = 3175
                mmLeft = 89164
                mmTop = 12964
                mmWidth = 42069
                BandType = 4
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
            end
          end
          object ppFooterBand5: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5556
            mmPrintPosition = 0
            object ppShape16: TppShape
              DesignLayer = ppDesignLayer2
              UserName = 'Shape16'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 264
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground1
            end
            object ppLabel143: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label143'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Damage History'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3302
              mmLeft = 529
              mmTop = 1588
              mmWidth = 20997
              BandType = 8
              LayerName = Foreground1
            end
            object ppCalc4: TppCalc
              DesignLayer = ppDesignLayer2
              UserName = 'Calc4'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 58208
              mmTop = 1058
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground1
            end
            object ppSystemVariable7: TppSystemVariable
              DesignLayer = ppDesignLayer2
              UserName = 'SystemVariable7'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 172773
              mmTop = 794
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground1
            end
          end
          object HistoryDamageIDGroup: TppGroup
            BreakName = 'damage_id'
            DataPipeline = HistoryPipe
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            OutlineSettings.CreateNode = True
            NewPage = True
            StartOnOddPage = False
            UserName = 'HistoryDamageIDGroup'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'HistoryPipe'
            NewFile = False
            object ppGroupHeaderBand1: TppGroupHeaderBand
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              mmBottomOffset = 0
              mmHeight = 24077
              mmPrintPosition = 0
              object ppLabel132: TppLabel
                DesignLayer = ppDesignLayer2
                UserName = 'Label121'
                HyperlinkEnabled = False
                AutoSize = False
                Border.mmPadding = 0
                Caption = 'INVESTIGATION #:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 8
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3440
                mmLeft = 0
                mmTop = 3704
                mmWidth = 28046
                BandType = 3
                GroupNo = 0
                LayerName = Foreground1
              end
              object ppDBText124: TppDBText
                DesignLayer = ppDesignLayer2
                UserName = 'DBText107'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'uq_damage_id'
                DataPipeline = Pipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 12
                Font.Style = [fsBold]
                Transparent = True
                DataPipelineName = 'Pipe'
                mmHeight = 4995
                mmLeft = 32015
                mmTop = 2646
                mmWidth = 32544
                BandType = 3
                GroupNo = 0
                LayerName = Foreground1
              end
              object ppShape18: TppShape
                DesignLayer = ppDesignLayer2
                UserName = 'Shape18'
                mmHeight = 5556
                mmLeft = 0
                mmTop = 8467
                mmWidth = 195527
                BandType = 3
                GroupNo = 0
                LayerName = Foreground1
              end
              object ppLabel133: TppLabel
                DesignLayer = ppDesignLayer2
                UserName = 'Label122'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                Caption = 'HISTORY'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'ARIAL'
                Font.Size = 10
                Font.Style = [fsBold]
                FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                FormFieldSettings.FormFieldType = fftNone
                Transparent = True
                mmHeight = 4233
                mmLeft = 1323
                mmTop = 8996
                mmWidth = 15917
                BandType = 3
                GroupNo = 0
                LayerName = Foreground1
              end
              object HistoryHeaderRegion: TppRegion
                DesignLayer = ppDesignLayer2
                UserName = 'HistoryHeaderRegion'
                Brush.Color = 14737632
                Caption = ' '
                ParentWidth = True
                Pen.Style = psClear
                mmHeight = 9525
                mmLeft = 0
                mmTop = 14552
                mmWidth = 195580
                BandType = 3
                GroupNo = 0
                LayerName = Foreground1
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppLabel134: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label127'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Modified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 794
                  mmTop = 19579
                  mmWidth = 22754
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel135: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label128'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Profit Ctr'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 30956
                  mmTop = 19579
                  mmWidth = 15080
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel136: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label129'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Exc Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 46567
                  mmTop = 19579
                  mmWidth = 21167
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel137: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label130'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Exc Resp Type'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 67998
                  mmTop = 19579
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel138: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label131'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Loc Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 89165
                  mmTop = 19579
                  mmWidth = 21167
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel139: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label1301'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Loc Resp Type'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 110596
                  mmTop = 19579
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel140: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label133'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Spc Resp Type'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 152400
                  mmTop = 19579
                  mmWidth = 19579
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel141: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label134'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Spc Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 131234
                  mmTop = 19579
                  mmWidth = 21167
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object labelReasonChanged: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'labelReasonChanged'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Reason Changed'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 171980
                  mmTop = 19579
                  mmWidth = 22490
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel149: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label149'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Excavator'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 46567
                  mmTop = 15875
                  mmWidth = 42069
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel150: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label150'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Utiliquest'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 89165
                  mmTop = 15875
                  mmWidth = 42069
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
                object ppLabel151: TppLabel
                  DesignLayer = ppDesignLayer2
                  UserName = 'Label1501'
                  HyperlinkEnabled = False
                  AutoSize = False
                  Border.mmPadding = 0
                  Caption = 'Utility'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
                  FormFieldSettings.FormFieldType = fftNone
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 131234
                  mmTop = 15875
                  mmWidth = 42069
                  BandType = 3
                  GroupNo = 0
                  LayerName = Foreground1
                end
              end
            end
            object ppGroupFooterBand7: TppGroupFooterBand
              Visible = False
              Background.Brush.Style = bsClear
              Border.mmPadding = 0
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
          object ppDesignLayers2: TppDesignLayers
            object ppDesignLayer2: TppDesignLayer
              UserName = 'Foreground1'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object DamageImagesSubReport: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'DamageImagesSubReport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'DamageAttachmentsPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 265
        mmWidth = 195580
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReportDamageAttachments: TppChildReport
          AutoStop = False
          DataPipeline = DamageAttachmentsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '20.01'
          mmColumnWidth = 0
          DataPipelineName = 'DamageAttachmentsPipe'
          object ppHeaderBand4: TppHeaderBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand8: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            Border.mmPadding = 0
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 221192
            mmPrintPosition = 25400
            object DamageAttachmentsRegion: TppRegion
              DesignLayer = ppDesignLayer16
              UserName = 'DamageAttachmentsRegion'
              KeepTogether = True
              Brush.Style = bsClear
              ParentHeight = True
              ParentWidth = True
              Pen.Color = clNone
              Pen.Style = psClear
              Pen.Width = 0
              Stretch = True
              Transparent = True
              mmHeight = 221192
              mmLeft = 0
              mmTop = 0
              mmWidth = 195580
              BandType = 4
              LayerName = Foreground15
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppDBText126: TppDBText
                DesignLayer = ppDesignLayer16
                UserName = 'DBText76'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'comment'
                DataPipeline = DamageAttachmentsPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 9
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                WordWrap = True
                DataPipelineName = 'DamageAttachmentsPipe'
                mmHeight = 16140
                mmLeft = 2381
                mmTop = 10583
                mmWidth = 190500
                BandType = 4
                LayerName = Foreground15
              end
              object ppDamageImage: TppImage
                OnPrint = ppDamageImagePrint
                DesignLayer = ppDesignLayer16
                UserName = 'ppDamageImage'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                DirectDraw = True
                MaintainAspectRatio = True
                ReprintOnOverFlow = True
                Border.mmPadding = 0
                mmHeight = 193675
                mmLeft = 0
                mmTop = 27517
                mmWidth = 194998
                BandType = 4
                LayerName = Foreground15
              end
              object ppMemoErrorMessageDamage: TppMemo
                DesignLayer = ppDesignLayer16
                UserName = 'ppMemoErrorMessageDamage'
                Border.mmPadding = 0
                Caption = 'MemoErrorMessageDamage'
                CharWrap = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 10
                Font.Style = []
                RemoveEmptyLines = False
                Stretch = True
                Transparent = True
                mmHeight = 23548
                mmLeft = 2381
                mmTop = 29633
                mmWidth = 190765
                BandType = 4
                LayerName = Foreground15
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                mmLeading = 0
              end
              object ppDBText76: TppDBText
                DesignLayer = ppDesignLayer16
                UserName = 'DBText1'
                HyperlinkEnabled = False
                Border.mmPadding = 0
                DataField = 'display_name'
                DataPipeline = DamageAttachmentsPipe
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 10
                Font.Style = [fsBold]
                ParentDataPipeline = False
                Transparent = True
                WordWrap = True
                DataPipelineName = 'DamageAttachmentsPipe'
                mmHeight = 7673
                mmLeft = 2381
                mmTop = 2117
                mmWidth = 190236
                BandType = 4
                LayerName = Foreground15
              end
            end
          end
          object ppFooterBand7: TppFooterBand
            Background.Brush.Style = bsClear
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5821
            mmPrintPosition = 0
            object ppShape17: TppShape
              DesignLayer = ppDesignLayer16
              UserName = 'Shape16'
              Brush.Color = 14737632
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 5292
              mmLeft = 0
              mmTop = 265
              mmWidth = 195580
              BandType = 8
              LayerName = Foreground15
            end
            object ppLabel145: TppLabel
              DesignLayer = ppDesignLayer16
              UserName = 'Label118'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Damage Attachments'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3175
              mmLeft = 1323
              mmTop = 1058
              mmWidth = 32015
              BandType = 8
              LayerName = Foreground15
            end
            object ppCalc5: TppCalc
              DesignLayer = ppDesignLayer16
              UserName = 'Calc5'
              HyperlinkEnabled = False
              Alignment = taCenter
              AutoSize = False
              CalcType = ctDateTime
              CustomType = dtDateTime
              DisplayFormat = 'mmm d, yyyy, h:nn:ss am/pm'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 49213
              mmTop = 1058
              mmWidth = 101336
              BandType = 8
              LayerName = Foreground15
            end
            object ppSystemVariable8: TppSystemVariable
              DesignLayer = ppDesignLayer16
              UserName = 'SystemVariable2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              VarType = vtPageSetDesc
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 171715
              mmTop = 1058
              mmWidth = 22225
              BandType = 8
              LayerName = Foreground15
            end
          end
          object ppDesignLayers16: TppDesignLayers
            object ppDesignLayer16: TppDesignLayer
              UserName = 'Foreground15'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object MasterDS: TDataSource
    DataSet = Ticket
    Left = 112
    Top = 136
  end
  object MasterPipe: TppDBPipeline
    DataSource = MasterDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'MasterPipe'
    Left = 200
    Top = 136
  end
  object DetailDS: TDataSource
    DataSet = Locates
    Left = 112
    Top = 184
  end
  object DetailPipe: TppDBPipeline
    DataSource = DetailDS
    OpenDataSource = False
    AutoCreateFields = False
    SkipWhenNoRecords = False
    UserName = 'DetailPipe'
    Left = 200
    Top = 184
  end
  object TicketNotesDS: TDataSource
    DataSet = TicketNotes
    Left = 112
    Top = 232
  end
  object NotesPipe: TppDBPipeline
    DataSource = TicketNotesDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'NotesPipe'
    Left = 200
    Top = 232
  end
  object DamageAttachmentsDS: TDataSource
    DataSet = DamageAttachments
    Left = 112
    Top = 288
  end
  object DamageAttachmentsPipe: TppDBPipeline
    DataSource = DamageAttachmentsDS
    AutoCreateFields = False
    UserName = 'DamageAttachmentsPipe'
    Left = 200
    Top = 280
  end
  object TicketAttachmentsDS: TDataSource
    DataSet = TicketAttachments
    Left = 112
    Top = 344
  end
  object TicketAttachmentsPipe: TppDBPipeline
    DataSource = TicketAttachmentsDS
    UserName = 'TicketAttachmentsPipe'
    Left = 200
    Top = 344
  end
  object ThirdPartyDS: TDataSource
    DataSet = ThirdParty
    Left = 112
    Top = 400
  end
  object LitigationDS: TDataSource
    DataSet = Litigation
    Left = 112
    Top = 448
  end
  object ThirdPartyPipe: TppDBPipeline
    DataSource = ThirdPartyDS
    OpenDataSource = False
    UserName = 'ThirdPartyPipe'
    Left = 200
    Top = 400
  end
  object LitigationPipe: TppDBPipeline
    DataSource = LitigationDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'LitigationPipe'
    Left = 200
    Top = 448
  end
  object DamageNotesDS: TDataSource
    DataSet = DamageNotes
    Left = 113
    Top = 497
  end
  object HistoryDS: TDataSource
    DataSet = History
    Left = 113
    Top = 545
  end
  object DamageNotesPipe: TppDBPipeline
    DataSource = DamageNotesDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'DamageNotesPipe'
    Left = 199
    Top = 497
  end
  object HistoryPipe: TppDBPipeline
    DataSource = HistoryDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'HistoryPipe'
    Left = 199
    Top = 545
  end
  object TicketFacilitiesDS: TDataSource
    DataSet = TicketFacilities
    Left = 112
    Top = 593
  end
  object TicketFacilitiesPipe: TppDBPipeline
    DataSource = TicketFacilitiesDS
    AutoCreateFields = False
    UserName = 'TicketFacilitiesPipe'
    Left = 192
    Top = 595
  end
  object DuplicateRecordsDS: TDataSource
    DataSet = DuplicateRecords
    Left = 114
    Top = 640
  end
  object DuplicateRecordsPipe: TppDBPipeline
    DataSource = DuplicateRecordsDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'DuplicateRecordsPipe'
    Left = 191
    Top = 640
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    CommandTimeout = 300
    ProcedureName = 'RPT_get_damage_details'
    Parameters = <
      item
        Name = '@damage_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DamageAttachmentList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@TicketAttachmentList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end>
    Prepared = True
    Left = 32
    Top = 24
  end
  object Master: TADODataSet
    Parameters = <>
    Left = 280
    Top = 80
  end
  object Ticket: TADODataSet
    Parameters = <>
    Left = 280
    Top = 136
  end
  object Locates: TADODataSet
    Parameters = <>
    Left = 280
    Top = 184
  end
  object DamageAttachments: TADODataSet
    Parameters = <>
    Left = 280
    Top = 288
  end
  object TicketNotes: TADODataSet
    Parameters = <>
    Left = 280
    Top = 232
  end
  object TicketAttachments: TADODataSet
    Parameters = <>
    Left = 280
    Top = 344
  end
  object ThirdParty: TADODataSet
    Parameters = <>
    Left = 280
    Top = 400
  end
  object DamageNotes: TADODataSet
    Parameters = <>
    Left = 280
    Top = 496
  end
  object Litigation: TADODataSet
    Parameters = <>
    Left = 280
    Top = 448
  end
  object TicketFacilities: TADODataSet
    Parameters = <>
    Left = 280
    Top = 592
  end
  object History: TADODataSet
    Parameters = <>
    Left = 280
    Top = 544
  end
  object DuplicateRecords: TADODataSet
    Parameters = <>
    Left = 280
    Top = 640
  end
  object Damage: TADOQuery
    Connection = ReportEngineDM.Conn
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'uq_damage_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select damage_id '
      'from damage '
      'where uq_damage_id=:uq_damage_id')
    Left = 112
    Top = 88
  end
end
