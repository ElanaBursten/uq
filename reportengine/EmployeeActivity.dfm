inherited EmployeeActivityDM: TEmployeeActivityDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Height = 501
  object Pipe: TppDBPipeline
    DataSource = SPDS
    OpenDataSource = False
    UserName = 'Pipe'
    Left = 200
    Top = 24
    object PipeppField1: TppField
      FieldAlias = 'r_emp_id'
      FieldName = 'r_emp_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object PipeppField2: TppField
      FieldAlias = 'mgr_id'
      FieldName = 'mgr_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object PipeppField3: TppField
      FieldAlias = 'mgr_short_name'
      FieldName = 'mgr_short_name'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object PipeppField4: TppField
      FieldAlias = 'report_level'
      FieldName = 'report_level'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object PipeppField5: TppField
      FieldAlias = 'emp_number'
      FieldName = 'emp_number'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object PipeppField6: TppField
      FieldAlias = 'short_name'
      FieldName = 'short_name'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object PipeppField7: TppField
      FieldAlias = 'last_name'
      FieldName = 'last_name'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object PipeppField8: TppField
      FieldAlias = 'h_report_level'
      FieldName = 'h_report_level'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object PipeppField9: TppField
      FieldAlias = 'h_namepath'
      FieldName = 'h_namepath'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object PipeppField10: TppField
      FieldAlias = 'h_idpath'
      FieldName = 'h_idpath'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object PipeppField11: TppField
      FieldAlias = 'tse_id'
      FieldName = 'tse_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object PipeppField12: TppField
      FieldAlias = 'emp_type_desc'
      FieldName = 'emp_type_desc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
  end
  object SPDS: TDataSource
    DataSet = Master
    Left = 118
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Timesheet Exception Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    ModalPreview = False
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowAutoSearchDialog = True
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 296
    Top = 24
    Version = '18.03'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 20373
      mmPrintPosition = 0
      object ReportHeader: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ReportHeader'
        Brush.Color = cl3DLight
        ParentWidth = True
        mmHeight = 7620
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 0
        LayerName = Foreground
      end
      object ReportHeaderLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ReportHeaderLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Employee Activity Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 13
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5292
        mmLeft = 101336
        mmTop = 1323
        mmWidth = 64294
        BandType = 0
        LayerName = Foreground
      end
      object EmployeeLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmployeeLabel'
        HyperlinkEnabled = False
        Caption = 'Employee:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 57415
        mmTop = 8202
        mmWidth = 12965
        BandType = 0
        LayerName = Foreground
      end
      object EmpTypeLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmpTypeLabel'
        HyperlinkEnabled = False
        Caption = 'Type:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 57415
        mmTop = 12171
        mmWidth = 6879
        BandType = 0
        LayerName = Foreground
      end
      object TopManagerLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'TopManagerLabel'
        HyperlinkEnabled = False
        Caption = 'Top Manager:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 57415
        mmTop = 16140
        mmWidth = 17198
        BandType = 0
        LayerName = Foreground
      end
      object EmpTypeDesc: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmployeeTypeDesc'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_type_desc'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 5027
        mmLeft = 79111
        mmTop = 12171
        mmWidth = 23283
        BandType = 0
        LayerName = Foreground
      end
      object ShortNameText: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'ShortNameText'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 79111
        mmTop = 8202
        mmWidth = 17727
        BandType = 0
        LayerName = Foreground
      end
      object EmpManagerLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmpManagerLabel'
        HyperlinkEnabled = False
        Caption = 'Employee'#39's Manager:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 122238
        mmTop = 8202
        mmWidth = 26988
        BandType = 0
        LayerName = Foreground
      end
      object EmployeeStatusLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmployeeStatusLabel'
        HyperlinkEnabled = False
        Caption = 'EmployeeStatusLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 122238
        mmTop = 12171
        mmWidth = 31485
        BandType = 0
        LayerName = Foreground
      end
      object ActivitiesLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ActivitiesLabel'
        HyperlinkEnabled = False
        Caption = 'ActivitiesLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 122238
        mmTop = 16140
        mmWidth = 21431
        BandType = 0
        LayerName = Foreground
      end
      object EmpManagerName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpManagerName'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'mgr_short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 154252
        mmTop = 8202
        mmWidth = 25400
        BandType = 0
        LayerName = Foreground
      end
      object EmpNumberLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmpNumberLabel'
        HyperlinkEnabled = False
        Caption = 'Emp. Number:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 210873
        mmTop = 8202
        mmWidth = 17463
        BandType = 0
        LayerName = Foreground
      end
      object AllEmployeesLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'AllEmployeesLabel'
        HyperlinkEnabled = False
        Caption = 'All Employees'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 210873
        mmTop = 12171
        mmWidth = 20638
        BandType = 0
        LayerName = Foreground
      end
      object EmpNumber: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpNumber'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 231246
        mmTop = 8202
        mmWidth = 19844
        BandType = 0
        LayerName = Foreground
      end
      object DateRangeLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DateRangeLabel'
        HyperlinkEnabled = False
        Caption = 'DateRangeLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 1588
        mmTop = 8202
        mmWidth = 23283
        BandType = 0
        LayerName = Foreground
      end
      object LSAAttachedLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'LSAAttachedLabel'
        HyperlinkEnabled = False
        Caption = 'Locate Status Activity Report Attached'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        Visible = False
        mmHeight = 3704
        mmLeft = 1588
        mmTop = 16140
        mmWidth = 55563
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand: TppDetailBand
      BeforePrint = ppDetailBandBeforePrint
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintCount = 1
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object DetailSubReport: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'GraphSubReport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'DetailsPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 4
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object GraphSubReport: TppChildReport
          AutoStop = False
          DataPipeline = DetailsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Timesheet Exception Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '18.03'
          mmColumnWidth = 0
          DataPipelineName = 'DetailsPipe'
          object ppGraphBand: TppDetailBand
            BeforePrint = UpdateActivitieDataSet
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            PrintCount = 5
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 40746
            mmPrintPosition = 0
            object ppVerticalLine: TppLine
              DesignLayer = ppDesignLayer3
              UserName = 'VerticalLine'
              Position = lpRight
              Weight = 0.750000000000000000
              mmHeight = 20373
              mmLeft = 5821
              mmTop = 6085
              mmWidth = 2646
              BandType = 4
              LayerName = Foreground2
            end
            object WorkingHoursLabel: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'WorkingHoursLabel'
              HyperlinkEnabled = False
              Caption = 'Work Times'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = []
              Transparent = True
              Visible = False
              WordWrap = True
              mmHeight = 6879
              mmLeft = 0
              mmTop = 6085
              mmWidth = 7673
              BandType = 4
              LayerName = Foreground2
            end
            object LoginLogoutLabel: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'WorkingHoursLabel1'
              HyperlinkEnabled = False
              Caption = 'WH'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              Visible = False
              mmHeight = 3175
              mmLeft = 2381
              mmTop = 18785
              mmWidth = 4233
              BandType = 4
              LayerName = Foreground2
            end
            object ppTimeLine: TppLine
              DesignLayer = ppDesignLayer3
              UserName = 'TimeLine'
              Weight = 0.750000000000000000
              mmHeight = 3969
              mmLeft = 8202
              mmTop = 26194
              mmWidth = 250825
              BandType = 4
              LayerName = Foreground2
            end
            object ActivityDate: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'ActivityDate'
              HyperlinkEnabled = False
              DataField = 'activity_date'
              DataPipeline = DetailsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              DataPipelineName = 'DetailsPipe'
              mmHeight = 4233
              mmLeft = 0
              mmTop = 794
              mmWidth = 48154
              BandType = 4
              LayerName = Foreground2
            end
            object ppDBTextRuleMsg: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBTextRuleMsg'
              HyperlinkEnabled = False
              BlankWhenZero = True
              DataField = 'rule_msg'
              DataPipeline = BreakRulesPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'BreakRulesPipe'
              mmHeight = 3440
              mmLeft = 99219
              mmTop = 794
              mmWidth = 166423
              BandType = 4
              LayerName = Foreground2
            end
            object ppDBTextRuleBrokenMsg: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBTextRuleBrokenMsg'
              HyperlinkEnabled = False
              AutoSize = True
              BlankWhenZero = True
              Color = 14803425
              DataField = 'rule_broke_msg'
              DataPipeline = BreakRulesPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              DataPipelineName = 'BreakRulesPipe'
              mmHeight = 3704
              mmLeft = 259028
              mmTop = 4763
              mmWidth = 6615
              BandType = 4
              LayerName = Foreground2
            end
            object ComputerUsageSubReport: TppSubReport
              DesignLayer = ppDesignLayer3
              UserName = 'ComputerUsageSubReport'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ShiftRelativeTo = ListSubReport
              TraverseAllData = False
              Visible = False
              DataPipelineName = 'ComputerUsagePipe'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 35719
              mmWidth = 266701
              BandType = 4
              LayerName = Foreground2
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ComputerUsageReport: TppChildReport
                AutoStop = False
                DataPipeline = ComputerUsagePipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = '%COMPANY% Timesheet Exception Report'
                PrinterSetup.Duplex = dpVertical
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '18.03'
                mmColumnWidth = 0
                DataPipelineName = 'ComputerUsagePipe'
                object ComputerUsageTitleBand: TppTitleBand
                  Background.Brush.Style = bsClear
                  mmBottomOffset = 0
                  mmHeight = 5292
                  mmPrintPosition = 0
                  object ComputerUsageLabel: TppLabel
                    DesignLayer = ppDesignLayer2
                    UserName = 'ComputerUsageLabel'
                    HyperlinkEnabled = False
                    Caption = 'Computers Used by Employee'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3175
                    mmLeft = 5027
                    mmTop = 2381
                    mmWidth = 34396
                    BandType = 1
                    LayerName = Foreground1
                  end
                  object ComputerSyncDateLabel: TppLabel
                    DesignLayer = ppDesignLayer2
                    UserName = 'ComputerUsageLabel1'
                    HyperlinkEnabled = False
                    Caption = 'Last Synced'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3175
                    mmLeft = 42598
                    mmTop = 2381
                    mmWidth = 14023
                    BandType = 1
                    LayerName = Foreground1
                  end
                end
                object ComputerUsageDetailBand: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background2.Brush.Style = bsClear
                  mmBottomOffset = 0
                  mmHeight = 3969
                  mmPrintPosition = 0
                  object ComputerName: TppDBText
                    DesignLayer = ppDesignLayer2
                    UserName = 'ComputerName'
                    HyperlinkEnabled = False
                    DataField = 'computer_name'
                    DataPipeline = ComputerUsagePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'ComputerUsagePipe'
                    mmHeight = 3175
                    mmLeft = 5027
                    mmTop = 265
                    mmWidth = 17198
                    BandType = 4
                    LayerName = Foreground1
                  end
                  object ComputerSyncDate: TppDBText
                    DesignLayer = ppDesignLayer2
                    UserName = 'DBTextActivityDate1'
                    HyperlinkEnabled = False
                    DataField = 'sync_date'
                    DataPipeline = ComputerUsagePipe
                    DisplayFormat = 'hh:mm:ss'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taCentered
                    DataPipelineName = 'ComputerUsagePipe'
                    mmHeight = 3175
                    mmLeft = 42598
                    mmTop = 265
                    mmWidth = 14552
                    BandType = 4
                    LayerName = Foreground1
                  end
                end
                object ppSummaryBand3: TppSummaryBand
                  Background.Brush.Style = bsClear
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDesignLayers2: TppDesignLayers
                  object ppDesignLayer2: TppDesignLayer
                    UserName = 'Foreground1'
                    LayerType = ltBanded
                    Index = 0
                  end
                end
              end
            end
            object ListSubReport: TppSubReport
              DesignLayer = ppDesignLayer3
              UserName = 'ListSubReport'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ParentPrinterSetup = False
              TraverseAllData = False
              Visible = False
              DataPipelineName = 'ActivityDataPipe'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 30692
              mmWidth = 266701
              BandType = 4
              LayerName = Foreground2
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReportActivityData: TppChildReport
                AutoStop = False
                Columns = 4
                DataPipeline = ActivityDataPipe
                PassSetting = psTwoPass
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = '%COMPANY% Timesheet Exception Report'
                PrinterSetup.Duplex = dpNone
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '18.03'
                mmColumnWidth = 66675
                DataPipelineName = 'ActivityDataPipe'
                object ppColumnHeaderBand1: TppColumnHeaderBand
                  Background.Brush.Style = bsClear
                  mmBottomOffset = 0
                  mmHeight = 3704
                  mmPrintPosition = 0
                  object ActivityTimeLabel: TppLabel
                    DesignLayer = ppDesignLayer4
                    UserName = 'ActivityTimeLabel'
                    HyperlinkEnabled = False
                    AutoSize = False
                    Caption = 'Time'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    TextAlignment = taCentered
                    Transparent = True
                    mmHeight = 3175
                    mmLeft = 529
                    mmTop = 265
                    mmWidth = 12700
                    BandType = 2
                    LayerName = PageLayer1
                  end
                  object ActivityDescriptionLabel: TppLabel
                    DesignLayer = ppDesignLayer4
                    UserName = 'ActivityDescriptionLabel'
                    HyperlinkEnabled = False
                    Caption = 'Activity Description'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3175
                    mmLeft = 13758
                    mmTop = 265
                    mmWidth = 21960
                    BandType = 2
                    LayerName = PageLayer1
                  end
                end
                object ppDetailBandActivityData: TppDetailBand
                  BeforePrint = ppDetailBandActivityDataBeforePrint
                  Background1.Brush.Style = bsClear
                  Background2.Brush.Style = bsClear
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 3969
                  mmPrintPosition = 0
                  object DetailRegion: TppRegion
                    DesignLayer = ppDesignLayer4
                    UserName = 'DetailRegion'
                    KeepTogether = True
                    Caption = 'DetailRegion'
                    ParentHeight = True
                    ParentWidth = True
                    Pen.Style = psClear
                    Pen.Width = 0
                    Stretch = True
                    mmHeight = 3969
                    mmLeft = 0
                    mmTop = 0
                    mmWidth = 66675
                    BandType = 4
                    LayerName = PageLayer1
                    mmBottomOffset = 0
                    mmOverFlowOffset = 0
                    mmStopPosition = 0
                    mmMinHeight = 0
                    object ppDBTextActivityDate: TppDBText
                      DesignLayer = ppDesignLayer4
                      UserName = 'DBTextActivityDate'
                      HyperlinkEnabled = False
                      DataField = 'activity_date'
                      DataPipeline = ActivityDataPipe
                      DisplayFormat = 'hh:mm:ss'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      DataPipelineName = 'ActivityDataPipe'
                      mmHeight = 3175
                      mmLeft = 529
                      mmTop = 529
                      mmWidth = 12700
                      BandType = 4
                      LayerName = PageLayer1
                    end
                    object ppDBTextActivityType: TppDBText
                      DesignLayer = ppDesignLayer4
                      UserName = 'ppDBTextActivityType'
                      HyperlinkEnabled = False
                      OnGetText = ppDBTextActivityTypeGetText
                      DataField = 'activity_type'
                      DataPipeline = ActivityDataPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'ActivityDataPipe'
                      mmHeight = 3175
                      mmLeft = 64294
                      mmTop = 529
                      mmWidth = 2381
                      BandType = 4
                      LayerName = PageLayer1
                    end
                    object ppDBMemoActivityDescription: TppDBMemo
                      DesignLayer = ppDesignLayer4
                      UserName = 'DBMemoActivityDescription'
                      CharWrap = False
                      DataField = 'description'
                      DataPipeline = ActivityDataPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      RemoveEmptyLines = False
                      Stretch = True
                      Transparent = True
                      DataPipelineName = 'ActivityDataPipe'
                      mmHeight = 3175
                      mmLeft = 14023
                      mmTop = 529
                      mmWidth = 49741
                      BandType = 4
                      LayerName = PageLayer1
                      mmBottomOffset = 0
                      mmOverFlowOffset = 0
                      mmStopPosition = 0
                      mmMinHeight = 0
                      mmLeading = 0
                    end
                  end
                end
                object ppColumnFooterBand1: TppColumnFooterBand
                  AlignToBottom = True
                  Background.Brush.Style = bsClear
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDesignLayers3: TppDesignLayers
                  object ppDesignLayer4: TppDesignLayer
                    UserName = 'PageLayer1'
                    LayerType = ltBanded
                    Index = 0
                  end
                end
              end
            end
          end
          object TppDesignLayers
            object ppDesignLayer3: TppDesignLayer
              UserName = 'Foreground2'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 27252
      mmPrintPosition = 0
      object ReportFooter: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ReportFooter'
        Brush.Color = cl3DLight
        ParentWidth = True
        mmHeight = 9790
        mmLeft = 0
        mmTop = 17463
        mmWidth = 266701
        BandType = 8
        LayerName = Foreground
      end
      object ReportCopyright: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Employee Activity Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 63236
        mmTop = 20638
        mmWidth = 139436
        BandType = 8
        LayerName = Foreground
      end
      object ReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 230188
        mmTop = 18785
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ReportPageNumber: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 241036
        mmTop = 22225
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
      object CompanyLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'CompanyLabel'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 13
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 5292
        mmLeft = 2910
        mmTop = 19579
        mmWidth = 29898
        BandType = 8
        LayerName = Foreground
      end
      object ppLabelLoginLogoutLegend: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'LabelLoginLogoutLegend'
        HyperlinkEnabled = False
        Caption = 'Login/Logout'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        Transparent = True
        mmHeight = 3175
        mmLeft = 0
        mmTop = 529
        mmWidth = 14023
        BandType = 8
        LayerName = Foreground
      end
      object LoginLogoutLine: TppLine
        DesignLayer = ppDesignLayer1
        UserName = 'LoginLogoutLine'
        Weight = 0.750000000000000000
        mmHeight = 1058
        mmLeft = 15346
        mmTop = 1852
        mmWidth = 13229
        BandType = 8
        LayerName = Foreground
      end
      object DetailFooterText: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DetailFooterText'
        HyperlinkEnabled = False
        Anchors = [atLeft]
        Caption = 
          'All reported times are based on the user'#39's time zone. Activities' +
          ' marked with [S] rely on current Windows system updates.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 0
        mmTop = 14023
        mmWidth = 154517
        BandType = 8
        LayerName = Foreground
      end
      object ppRichMemoLegend: TppRichText
        DesignLayer = ppDesignLayer1
        UserName = 'RichMemoLegend'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        Caption = 'RichMemoLegend'
        ExportRTFAsBitmap = False
        RemoveEmptyLines = False
        Transparent = True
        mmHeight = 11113
        mmLeft = 0
        mmTop = 3704
        mmWidth = 265378
        BandType = 8
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object DetailsDS: TDataSource
    DataSet = Details
    Left = 118
    Top = 88
  end
  object DetailsPipe: TppDBPipeline
    DataSource = DetailsDS
    UserName = 'DetailsPipe'
    Left = 200
    Top = 88
  end
  object ActivityDataDS: TDataSource
    DataSet = ActivityData
    Left = 118
    Top = 152
  end
  object ActivityDataPipe: TppDBPipeline
    DataSource = ActivityDataDS
    UserName = 'ActivityDataPipe'
    Left = 200
    Top = 152
  end
  object BreakRulesDS: TDataSource
    DataSet = BreakRules
    Left = 118
    Top = 216
  end
  object BreakRulesPipe: TppDBPipeline
    DataSource = BreakRulesDS
    UserName = 'ActivityDataPipe1'
    Left = 200
    Top = 216
  end
  object ComputerUsageDS: TDataSource
    DataSet = ComputerUsage
    Left = 118
    Top = 400
  end
  object ComputerUsagePipe: TppDBPipeline
    DataSource = ComputerUsageDS
    OpenDataSource = False
    AutoCreateFields = False
    UserName = 'ComputerUsagePipe'
    Left = 221
    Top = 400
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_employee_activity'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@ManagerId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@StartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@EndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@OutOfRangeActivities'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@OutOfRangeEmployees'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@HourSpanStart'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@HourSpanEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@EmployeeStatus'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@EmployeeId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 32
    Top = 24
  end
  object ComputerUsage: TADODataSet
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 34
    Top = 400
  end
  object SummaryGroup: TADODataSet
    Parameters = <>
    Left = 32
    Top = 344
  end
  object Summary: TADODataSet
    Parameters = <>
    Left = 32
    Top = 280
  end
  object BreakRules: TADODataSet
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 32
    Top = 216
  end
  object ActivityData: TADODataSet
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 32
    Top = 152
  end
  object Details: TADODataSet
    Connection = ReportEngineDM.Conn
    Parameters = <>
    Left = 32
    Top = 88
  end
  object Master: TADODataSet
    Parameters = <>
    Left = 288
    Top = 88
  end
end
