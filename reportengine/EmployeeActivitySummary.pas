unit EmployeeActivitySummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass,
  ppVar, ppReport, ppStrtch, ppSubRpt, ppCtrls, ppPrnabl, ppCache, ppProd,
  DB, ppComm, ppRelatv, ppDB, ppDBPipe, ExtCtrls, ppMemo, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TEmployeeActivitySummaryDM = class(TBaseReportDM)
    DetailPipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppLabel16: TppLabel;
    DateRangeLabel: TppLabel;
    ppDBText10: TppDBText;
    DetailsDS: TDataSource;
    ppReportHeaderLabel: TppLabel;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText1: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppDBText29: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    ppDBText35: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel12: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppDBCalc9: TppDBCalc;
    ppDBText2: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppDBCalc12: TppDBCalc;
    ppDBCalc13: TppDBCalc;
    ppDBCalc14: TppDBCalc;
    ppDBCalc15: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppDBCalc17: TppDBCalc;
    ppDBCalc18: TppDBCalc;
    ppDBText4: TppDBText;
    ppDBText3: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDBText15: TppDBText;
    ppLine3: TppLine;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel33: TppLabel;
    ppLine4: TppLine;
    ppLabel34: TppLabel;
    ppLabel35: TppLabel;
    ppLabel36: TppLabel;
    ppTitleBand1: TppTitleBand;
    ppLabel1: TppLabel;
    TopManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ActivitiesLabel: TppLabel;
    AllEmployeesLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Dummy3: TADODataSet;
    Dummy2: TADODataSet;
    Dummy1: TADODataSet;
    Details: TADODataSet;
    Summary: TADODataSet;
    procedure Configure(QueryFields: TStrings); override;
    procedure ppGroupHeaderBand1BeforePrint(Sender: TObject);
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, odADOutils;

{$R *.dfm}

{ TEmployeeActivitySummaryDM }

procedure TEmployeeActivitySummaryDM.Configure(QueryFields: TStrings);
var
  EndDate: TDateTime;
  HourSpanStart: TDateTime;
  HourSpanEnd: TDateTime;
  EmployeeStatus: Integer;
  OutOfRangeActivities, OutOfRangeEmployees: Integer;
  ActivitiesText: string;
  ManagerID: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  EndDate := SafeGetDateTime('EndDate');
  if EndDate = 0 then
    EndDate := GetDateTime('StartDate') + 1;

  HourSpanStart := SafeGetDateTime('HourSpanStart');
  HourSpanEnd := SafeGetDateTime('HourSpanEnd');

  EmployeeStatus := SafeGetInteger('EmployeeStatus', 2);
  OutOfRangeActivities := SafeGetInteger('OutOfRangeActivities', 0);
  OutOfRangeEmployees := SafeGetInteger('OutOfRangeEmployees', 0);
  ManagerID := SafeGetInteger('ManagerId', -1);

  with SP do begin
    Parameters.ParamByName('@EmployeeId').value := SafeGetInteger('EmployeeId',-1);
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := EndDate;
    Parameters.ParamByName('@OutOfRangeActivities').value := OutOfRangeActivities;
    Parameters.ParamByName('@OutOfRangeEmployees').value := OutOfRangeEmployees;
    if HourSpanStart <> 0 then
      Parameters.ParamByName('@HourSpanStart').value := HourSpanStart;
    if HourSpanEnd <> 0 then
      Parameters.ParamByName('@HourSpanEnd').value := HourSpanEnd;
    Parameters.ParamByName('@EmployeeStatus').value := EmployeeStatus;
    Open;
  end;

  Master.RecordSet       := SP.RecordSet;
  Dummy1.RecordSet       := SP.RecordSet.NextRecordset(RecsAffected);
  Dummy2.RecordSet       := SP.RecordSet.NextRecordset(RecsAffected);
  Dummy3.RecordSet       := SP.RecordSet.NextRecordset(RecsAffected);
  Details.RecordSet      := SP.RecordSet.NextRecordset(RecsAffected);
  Summary.RecordSet      := SP.RecordSet.NextRecordset(RecsAffected);

  ShowDateRange(DateRangeLabel, 'StartDate' , 'EndDate');
  ShowEmployeeStatus(EmployeeStatusLabel, EmployeeStatus);
  if IntToBoolean(OutOfRangeActivities) then
    ActivitiesText := 'Out of working hours'
  else if HourSpanStart = 0 then
    ActivitiesText := 'All';
  ShowText(ActivitiesLabel, 'Activities:', ActivitiesText);
  ShowEmployeeName(TopManagerLabel, ManagerID, 'Top Manager:');
end;

procedure TEmployeeActivitySummaryDM.ppGroupHeaderBand1BeforePrint(Sender: TObject);
begin
  Details.Filter := 'emp_id = ' + Master.FieldByName('r_emp_id').AsString;
  Details.Filtered := True;
end;

initialization
  RegisterReport('EmployeeActivitySummary', TEmployeeActivitySummaryDM);

end.

