inherited DamageEstimateChangesDM: TDamageEstimateChangesDM
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 316
  Top = 242
  Height = 409
  Width = 461
  object NewDamagesSP: TADOStoredProc
    Connection = DM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'RPT_damageestimatechanges4;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RecdStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@State'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ProfitCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@LiabilityType'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 35
    Top = 24
  end
  object NewDamagesDS: TDataSource
    AutoEdit = False
    DataSet = NewDamagesSP
    OnDataChange = NewDamagesDSDataChange
    Left = 125
    Top = 24
  end
  object Report: TppReport
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    DeviceType = 'Screen'
    ShowAutoSearchDialog = True
    Left = 307
    Top = 24
    Version = '6.03'
    mmColumnWidth = 0
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 16669
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 16669
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 0
      end
      object ppReportHeaderLabel: TppLabel
        UserName = 'ppReportHeaderLabel'
        AutoSize = False
        Caption = 'Damage Estimate Changes Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 3175
        mmTop = 2381
        mmWidth = 260086
        BandType = 0
      end
      object ppLabel10: TppLabel
        UserName = 'Label10'
        Caption = 'Modified'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 2910
        mmTop = 10319
        mmWidth = 12700
        BandType = 0
      end
      object FromDateLabel: TppLabel
        UserName = 'FromDateLabel'
        Caption = 'FromDateLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 16140
        mmTop = 10319
        mmWidth = 13970
        BandType = 0
      end
      object ppLabel12: TppLabel
        UserName = 'Label12'
        Caption = 'To'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 30956
        mmTop = 10319
        mmWidth = 3704
        BandType = 0
      end
      object ToDateLabel: TppLabel
        UserName = 'ToDateLabel'
        Caption = 'ToDateLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 35719
        mmTop = 10319
        mmWidth = 14817
        BandType = 0
      end
      object StateLabel: TppLabel
        UserName = 'StateLabel'
        Caption = 'State'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 187855
        mmTop = 10319
        mmWidth = 7408
        BandType = 0
      end
      object StateValue: TppLabel
        UserName = 'ToDateLabel1'
        Caption = 'ST'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 196586
        mmTop = 10319
        mmWidth = 3440
        BandType = 0
      end
      object ProfitCenterValue: TppLabel
        UserName = 'ProfitCenterValue'
        Caption = 'ZZZZ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 176213
        mmTop = 10319
        mmWidth = 7408
        BandType = 0
      end
      object ProfitCenterLabel: TppLabel
        UserName = 'ProfitCenterLabel'
        Caption = 'Profit Center'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 155840
        mmTop = 10319
        mmWidth = 18785
        BandType = 0
      end
      object LiabilityValue: TppLabel
        UserName = 'LiabilityValue'
        Caption = 'Neither UtiliQuest nor excavator are liable'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 215371
        mmTop = 10319
        mmWidth = 49742
        BandType = 0
      end
      object LiabilityLabel: TppLabel
        UserName = 'LiabilityLabel'
        Caption = 'Liability'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 203465
        mmTop = 10319
        mmWidth = 11113
        BandType = 0
      end
      object ppLabel77: TppLabel
        UserName = 'Label101'
        Caption = 'Notified'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 105834
        mmTop = 10319
        mmWidth = 11113
        BandType = 0
      end
      object NotifiedFromLabel: TppLabel
        UserName = 'FromDateLabel1'
        Caption = 'NotifiedFromLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 117475
        mmTop = 10319
        mmWidth = 13970
        BandType = 0
      end
      object ppLabel79: TppLabel
        UserName = 'Label79'
        Caption = 'To'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 132557
        mmTop = 10319
        mmWidth = 3704
        BandType = 0
      end
      object NotifiedToLabel: TppLabel
        UserName = 'ToDateLabel2'
        Caption = 'NotifiedToLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 137584
        mmTop = 10319
        mmWidth = 13970
        BandType = 0
      end
      object ppLabel81: TppLabel
        UserName = 'Label102'
        Caption = 'Damaged'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 53711
        mmTop = 10319
        mmWidth = 13494
        BandType = 0
      end
      object DamageFromLabel: TppLabel
        UserName = 'FromDateLabel2'
        Caption = 'DamageFromLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 67998
        mmTop = 10319
        mmWidth = 13970
        BandType = 0
      end
      object ppLabel83: TppLabel
        UserName = 'Label83'
        Caption = 'To'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 82815
        mmTop = 10319
        mmWidth = 3704
        BandType = 0
      end
      object DamageToLabel: TppLabel
        UserName = 'ToDateLabel3'
        Caption = 'DamageToLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3704
        mmLeft = 87313
        mmTop = 10319
        mmWidth = 15081
        BandType = 0
      end
    end
    object MainDetailBand: TppDetailBand
      BeforePrint = MainDetailBandBeforePrint
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 39158
      mmPrintPosition = 0
      object NewDamages: TppSubReport
        UserName = 'NewDamages'
        ExpandAll = False
        NewPrintJob = False
        TraverseAllData = False
        DataPipelineName = 'NewDamagesPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = NewDamagesPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '6.03'
          mmColumnWidth = 0
          DataPipelineName = 'NewDamagesPipe'
          object ppDetailBand2: TppDetailBand
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 7408
            mmPrintPosition = 0
            object NewDamageDetails: TppSubReport
              UserName = 'NewDamageDetails'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              TraverseAllData = False
              DataPipelineName = 'DetailPipe'
              mmHeight = 3704
              mmLeft = 0
              mmTop = 0
              mmWidth = 266701
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              object ppChildReport3: TppChildReport
                AutoStop = False
                DataPipeline = DetailPipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '6.03'
                mmColumnWidth = 0
                DataPipelineName = 'DetailPipe'
                object ppDetailBand4: TppDetailBand
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 3704
                  mmPrintPosition = 0
                  object ppDBText26: TppDBText
                    UserName = 'DBText26'
                    DataField = 'amount'
                    DataPipeline = DetailPipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 49742
                    mmTop = 0
                    mmWidth = 15610
                    BandType = 4
                  end
                  object ppDBText30: TppDBText
                    UserName = 'DBText30'
                    DataField = 'comment'
                    DataPipeline = DetailPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 84667
                    mmTop = 0
                    mmWidth = 169334
                    BandType = 4
                  end
                  object ppDBText31: TppDBText
                    UserName = 'DBText31'
                    DataField = 'modified_date'
                    DataPipeline = DetailPipe
                    DisplayFormat = '"Estimate on" m/d/yyyy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 9525
                    mmTop = 0
                    mmWidth = 39158
                    BandType = 4
                  end
                end
                object ppGroup3: TppGroup
                  BreakName = 'InitialDate'
                  DataPipeline = DetailPipe
                  KeepTogether = True
                  UserName = 'Group3'
                  mmNewColumnThreshold = 0
                  mmNewPageThreshold = 0
                  DataPipelineName = 'DetailPipe'
                  object ppGroupHeaderBand3: TppGroupHeaderBand
                    PrintHeight = phDynamic
                    mmBottomOffset = 0
                    mmHeight = 7408
                    mmPrintPosition = 0
                    object ppDBText17: TppDBText
                      UserName = 'DBText17'
                      DataField = 'InitialDate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '"Previous Estimate as of" m/d/yyyy'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 44186
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText19: TppDBText
                      UserName = 'DBText19'
                      DataField = 'InitialEstimate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 3704
                      mmWidth = 15610
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppShape1: TppShape
                      UserName = 'Shape1'
                      Brush.Color = clSilver
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 3704
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 266701
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText1: TppDBText
                      UserName = 'DBText1'
                      DataField = 'damage_id'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 2910
                      mmTop = 0
                      mmWidth = 15081
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText2: TppDBText
                      UserName = 'DBText2'
                      DataField = 'damage_date'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 54504
                      mmTop = 0
                      mmWidth = 18256
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText3: TppDBText
                      UserName = 'DBText3'
                      DataField = 'uq_notified_date'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 19315
                      mmTop = 0
                      mmWidth = 33867
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText4: TppDBText
                      UserName = 'DBText4'
                      DataField = 'client_claim_id'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 74083
                      mmTop = 0
                      mmWidth = 28575
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText5: TppDBText
                      UserName = 'DBText5'
                      DataField = 'utility_co_damaged'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 103981
                      mmTop = 0
                      mmWidth = 26988
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText6: TppDBText
                      UserName = 'DBText6'
                      DataField = 'size_type'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 132292
                      mmTop = 0
                      mmWidth = 46038
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText7: TppDBText
                      UserName = 'DBText7'
                      DataField = 'location'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 179123
                      mmTop = 0
                      mmWidth = 69321
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText8: TppDBText
                      UserName = 'DBText8'
                      DataField = 'uq_resp_code'
                      DataPipeline = NewDamagesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewDamagesPipe'
                      mmHeight = 3704
                      mmLeft = 249767
                      mmTop = 0
                      mmWidth = 14552
                      BandType = 3
                      GroupNo = 0
                    end
                  end
                  object NewDamagesFooterBand: TppGroupFooterBand
                    PrintHeight = phDynamic
                    mmBottomOffset = 0
                    mmHeight = 7408
                    mmPrintPosition = 0
                    object ppDBText18: TppDBText
                      UserName = 'DBText18'
                      DataField = 'CurrentDate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '"Current Estimate as of" m/d/yyyy'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 0
                      mmWidth = 43656
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppDBText20: TppDBText
                      UserName = 'DBText20'
                      DataField = 'CurrentEstimate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 0
                      mmWidth = 15610
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppLabel19: TppLabel
                      UserName = 'Label19'
                      AutoSize = False
                      Caption = 'Change from Previous Estimate'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 43656
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppDBText21: TppDBText
                      UserName = 'DBText21'
                      DataField = 'Delta'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 3704
                      mmWidth = 15610
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppLine1: TppLine
                      UserName = 'Line1'
                      Weight = 1
                      mmHeight = 265
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 74613
                      BandType = 5
                      GroupNo = 0
                    end
                  end
                end
              end
            end
            object NewDamageInvoices: TppSubReport
              UserName = 'NewDamageInvoices'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              ShiftRelativeTo = NewDamageDetails
              TraverseAllData = False
              DataPipelineName = 'InvoicePipe'
              mmHeight = 3704
              mmLeft = 0
              mmTop = 3704
              mmWidth = 266701
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              object ppChildReport5: TppChildReport
                AutoStop = False
                DataPipeline = InvoicePipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '6.03'
                mmColumnWidth = 0
                DataPipelineName = 'InvoicePipe'
                object ppTitleBand1: TppTitleBand
                  mmBottomOffset = 0
                  mmHeight = 5556
                  mmPrintPosition = 0
                  object ppShape5: TppShape
                    UserName = 'Shape5'
                    Brush.Color = 14737632
                    Pen.Style = psClear
                    mmHeight = 3704
                    mmLeft = 8202
                    mmTop = 1588
                    mmWidth = 259292
                    BandType = 1
                  end
                  object ppLabel33: TppLabel
                    UserName = 'Label33'
                    AutoSize = False
                    Caption = 'Invoice Number'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 8731
                    mmTop = 1588
                    mmWidth = 26194
                    BandType = 1
                  end
                  object ppLabel34: TppLabel
                    UserName = 'Label34'
                    AutoSize = False
                    Caption = 'Amount'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 56886
                    mmTop = 1588
                    mmWidth = 13758
                    BandType = 1
                  end
                  object ppLabel35: TppLabel
                    UserName = 'Label35'
                    AutoSize = False
                    Caption = 'Company'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 72761
                    mmTop = 1588
                    mmWidth = 34396
                    BandType = 1
                  end
                  object ppLabel36: TppLabel
                    UserName = 'Label36'
                    AutoSize = False
                    Caption = 'Paid Date'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 108479
                    mmTop = 1588
                    mmWidth = 16933
                    BandType = 1
                  end
                  object ppLabel37: TppLabel
                    UserName = 'Label37'
                    AutoSize = False
                    Caption = 'Paid Amount'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 126736
                    mmTop = 1588
                    mmWidth = 17727
                    BandType = 1
                  end
                  object ppLabel38: TppLabel
                    UserName = 'Label38'
                    AutoSize = False
                    Caption = 'Comment'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 146050
                    mmTop = 1588
                    mmWidth = 63765
                    BandType = 1
                  end
                  object ppLabel39: TppLabel
                    UserName = 'Label39'
                    AutoSize = False
                    Caption = 'Received Date'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 36248
                    mmTop = 1588
                    mmWidth = 19050
                    BandType = 1
                  end
                end
                object NewInvoiceDetail: TppDetailBand
                  mmBottomOffset = 0
                  mmHeight = 4763
                  mmPrintPosition = 0
                  object ppDBText33: TppDBText
                    UserName = 'DBText33'
                    DataField = 'invoice_num'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 8731
                    mmTop = 529
                    mmWidth = 26194
                    BandType = 4
                  end
                  object NewInvoicedAmount: TppDBText
                    UserName = 'NewInvoicedAmount'
                    DataField = 'amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 56886
                    mmTop = 529
                    mmWidth = 13758
                    BandType = 4
                  end
                  object ppDBText35: TppDBText
                    UserName = 'DBText35'
                    DataField = 'company'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 72761
                    mmTop = 529
                    mmWidth = 34396
                    BandType = 4
                  end
                  object ppDBText36: TppDBText
                    UserName = 'DBText36'
                    DataField = 'paid_date'
                    DataPipeline = InvoicePipe
                    DisplayFormat = 'mmm d, yy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 108479
                    mmTop = 529
                    mmWidth = 16933
                    BandType = 4
                  end
                  object NewPaidAmount: TppDBText
                    UserName = 'NewPaidAmount'
                    DataField = 'paid_amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 126736
                    mmTop = 529
                    mmWidth = 17727
                    BandType = 4
                  end
                  object ppDBText38: TppDBText
                    UserName = 'DBText38'
                    DataField = 'comment'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 146315
                    mmTop = 529
                    mmWidth = 63765
                    BandType = 4
                  end
                  object ppDBText39: TppDBText
                    UserName = 'DBText39'
                    DataField = 'received_date'
                    DataPipeline = InvoicePipe
                    DisplayFormat = 'mmm d, yy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 36248
                    mmTop = 529
                    mmWidth = 19050
                    BandType = 4
                  end
                end
                object ppSummaryBand1: TppSummaryBand
                  mmBottomOffset = 0
                  mmHeight = 5027
                  mmPrintPosition = 0
                  object ppLabel32: TppLabel
                    UserName = 'Label32'
                    Caption = 'Totals'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 15346
                    mmTop = 529
                    mmWidth = 7673
                    BandType = 7
                  end
                  object ppLabel40: TppLabel
                    UserName = 'Label40'
                    Caption = 'Invoice Amounts'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 30692
                    mmTop = 529
                    mmWidth = 21167
                    BandType = 7
                  end
                  object ppLabel41: TppLabel
                    UserName = 'Label401'
                    Caption = 'Paid Amounts'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 103717
                    mmTop = 529
                    mmWidth = 17463
                    BandType = 7
                  end
                  object ppDBCalc1: TppDBCalc
                    UserName = 'DBCalc1'
                    DataField = 'paid_amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 123296
                    mmTop = 529
                    mmWidth = 21167
                    BandType = 7
                  end
                  object ppDBCalc2: TppDBCalc
                    UserName = 'DBCalc2'
                    DataField = 'amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 53446
                    mmTop = 529
                    mmWidth = 17198
                    BandType = 7
                  end
                  object ppLine5: TppLine
                    UserName = 'Line5'
                    Weight = 0.75
                    mmHeight = 2117
                    mmLeft = 15346
                    mmTop = 0
                    mmWidth = 129382
                    BandType = 7
                  end
                end
              end
            end
          end
          object ppGroup2: TppGroup
            BreakName = 'dummy'
            DataPipeline = NewDamagesPipe
            KeepTogether = True
            UserName = 'Group2'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'NewDamagesPipe'
            object ppGroupHeaderBand2: TppGroupHeaderBand
              PrintHeight = phDynamic
              mmBottomOffset = 0
              mmHeight = 11642
              mmPrintPosition = 0
              object ppRegion1: TppRegion
                UserName = 'Region1'
                Brush.Style = bsClear
                ParentWidth = True
                Pen.Style = psClear
                Transparent = True
                mmHeight = 9260
                mmLeft = 0
                mmTop = 794
                mmWidth = 266701
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                object ppLabel6: TppLabel
                  UserName = 'Label6'
                  AutoSize = False
                  Caption = 'Damage ID'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 2910
                  mmTop = 5821
                  mmWidth = 15081
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel22: TppLabel
                  UserName = 'Label22'
                  AutoSize = False
                  Caption = 'Notified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 19315
                  mmTop = 5821
                  mmWidth = 33867
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel21: TppLabel
                  UserName = 'Label21'
                  AutoSize = False
                  Caption = 'Damage Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 54504
                  mmTop = 5821
                  mmWidth = 18256
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel23: TppLabel
                  UserName = 'Label23'
                  AutoSize = False
                  Caption = 'Client Claim #'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 74083
                  mmTop = 5821
                  mmWidth = 28575
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel24: TppLabel
                  UserName = 'Label24'
                  AutoSize = False
                  Caption = 'Utility Company'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 103981
                  mmTop = 5821
                  mmWidth = 26988
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel25: TppLabel
                  UserName = 'Label25'
                  AutoSize = False
                  Caption = 'Type / Size of Facility'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 132292
                  mmTop = 5821
                  mmWidth = 46038
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel26: TppLabel
                  UserName = 'Label26'
                  AutoSize = False
                  Caption = 'Location'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 179652
                  mmTop = 5821
                  mmWidth = 67998
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel27: TppLabel
                  UserName = 'Label27'
                  AutoSize = False
                  Caption = 'Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 249767
                  mmTop = 5821
                  mmWidth = 14552
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel1: TppLabel
                  UserName = 'Label1'
                  ShiftWithParent = True
                  Caption = 'New Damage Claims'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4233
                  mmLeft = 1323
                  mmTop = 1852
                  mmWidth = 34925
                  BandType = 3
                  GroupNo = 0
                end
              end
            end
            object NewDamageClaimsGroupFooterBand: TppGroupFooterBand
              BeforePrint = NewDamageClaimsGroupFooterBandBeforePrint
              mmBottomOffset = 0
              mmHeight = 15346
              mmPrintPosition = 0
              object ppShape3: TppShape
                UserName = 'Shape3'
                Brush.Color = clSilver
                ParentHeight = True
                ParentWidth = True
                Pen.Style = psClear
                mmHeight = 15346
                mmLeft = 0
                mmTop = 0
                mmWidth = 266701
                BandType = 5
                GroupNo = 0
              end
              object ppLabel17: TppLabel
                UserName = 'Label17'
                AutoSize = False
                Caption = 'Total, New Damage Claims'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2910
                mmTop = 1852
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object TotalEstimatesNew: TppVariable
                UserName = 'TotalEstimatesNew'
                AutoSize = False
                CalcOrder = 0
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 50006
                mmTop = 1852
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
              object ppLabel55: TppLabel
                UserName = 'Label55'
                AutoSize = False
                Caption = 'Total, New Invoices'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2910
                mmTop = 6085
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object ppLabel59: TppLabel
                UserName = 'Label59'
                AutoSize = False
                Caption = 'Total, New Paid Amounts'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2910
                mmTop = 10319
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object InvoiceTotalNew: TppVariable
                UserName = 'InvoiceTotalNew'
                AutoSize = False
                CalcOrder = 1
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                OnCalc = InvoiceTotalNewCalc
                TextAlignment = taRightJustified
                Transparent = True
                Visible = False
                mmHeight = 3704
                mmLeft = 101600
                mmTop = 2910
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
              object PaidTotalNew: TppVariable
                UserName = 'PaidTotalNew'
                AutoSize = False
                CalcOrder = 2
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                OnCalc = PaidTotalNewCalc
                TextAlignment = taRightJustified
                Transparent = True
                Visible = False
                mmHeight = 3704
                mmLeft = 101600
                mmTop = 7144
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
              object ppDBCalc5: TppDBCalc
                UserName = 'DBCalc5'
                DataField = 'amount'
                DataPipeline = InvoicePipe
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                ResetGroup = ppGroup2
                Transparent = True
                Visible = False
                DataPipelineName = 'InvoicePipe'
                mmHeight = 3704
                mmLeft = 131498
                mmTop = 3175
                mmWidth = 26723
                BandType = 5
                GroupNo = 0
              end
              object ppDBCalc6: TppDBCalc
                UserName = 'DBCalc6'
                DataField = 'paid_amount'
                DataPipeline = InvoicePipe
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                ResetGroup = ppGroup2
                Transparent = True
                Visible = False
                DataPipelineName = 'InvoicePipe'
                mmHeight = 3704
                mmLeft = 131234
                mmTop = 7408
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
              object NewInvoiceTotal: TppVariable
                UserName = 'NewInvoiceTotal'
                CalcOrder = 3
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 50006
                mmTop = 6085
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
              object NewPaidTotal: TppVariable
                UserName = 'NewPaidTotal'
                CalcOrder = 4
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 50006
                mmTop = 10319
                mmWidth = 26988
                BandType = 5
                GroupNo = 0
              end
            end
          end
        end
      end
      object ChangedDamages: TppSubReport
        UserName = 'ChangedDamages'
        ExpandAll = False
        NewPrintJob = False
        ShiftRelativeTo = NewDamages
        TraverseAllData = False
        DataPipelineName = 'NewEstimatesPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5027
        mmWidth = 266701
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = NewEstimatesPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '6.03'
          mmColumnWidth = 0
          DataPipelineName = 'NewEstimatesPipe'
          object ChangedDamagesTitle: TppTitleBand
            NewPage = True
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand3: TppDetailBand
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 8202
            mmPrintPosition = 0
            object ChangedDamageDetails: TppSubReport
              UserName = 'ChangedDamageDetails'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              TraverseAllData = False
              DataPipelineName = 'DetailPipe'
              mmHeight = 3969
              mmLeft = 0
              mmTop = 0
              mmWidth = 266701
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              object ppChildReport4: TppChildReport
                AutoStop = False
                DataPipeline = DetailPipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '6.03'
                mmColumnWidth = 0
                DataPipelineName = 'DetailPipe'
                object ppDetailBand5: TppDetailBand
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 3704
                  mmPrintPosition = 0
                  object ppDBText22: TppDBText
                    UserName = 'DBText22'
                    DataField = 'amount'
                    DataPipeline = DetailPipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 49742
                    mmTop = 0
                    mmWidth = 15610
                    BandType = 4
                  end
                  object ppDBText23: TppDBText
                    UserName = 'DBText301'
                    DataField = 'comment'
                    DataPipeline = DetailPipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 84667
                    mmTop = 0
                    mmWidth = 169334
                    BandType = 4
                  end
                  object ppDBText24: TppDBText
                    UserName = 'DBText24'
                    DataField = 'modified_date'
                    DataPipeline = DetailPipe
                    DisplayFormat = '"Estimate on" m/d/yyyy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'DetailPipe'
                    mmHeight = 3704
                    mmLeft = 9525
                    mmTop = 0
                    mmWidth = 39423
                    BandType = 4
                  end
                end
                object ppGroup4: TppGroup
                  BreakName = 'InitialDate'
                  DataPipeline = DetailPipe
                  UserName = 'Group4'
                  mmNewColumnThreshold = 0
                  mmNewPageThreshold = 0
                  DataPipelineName = 'DetailPipe'
                  object ppGroupHeaderBand4: TppGroupHeaderBand
                    PrintHeight = phDynamic
                    mmBottomOffset = 0
                    mmHeight = 7938
                    mmPrintPosition = 0
                    object ppDBText25: TppDBText
                      UserName = 'DBText25'
                      DataField = 'InitialDate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '"Previous Estimate as of" m/d/yyyy'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 44186
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText27: TppDBText
                      UserName = 'DBText27'
                      DataField = 'InitialEstimate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 3704
                      mmWidth = 15610
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppShape2: TppShape
                      UserName = 'Shape2'
                      Brush.Color = clSilver
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 3704
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 266701
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText9: TppDBText
                      UserName = 'DBText9'
                      DataField = 'damage_id'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 2910
                      mmTop = 0
                      mmWidth = 15081
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText10: TppDBText
                      UserName = 'DBText10'
                      DataField = 'damage_date'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 54504
                      mmTop = 0
                      mmWidth = 18256
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText11: TppDBText
                      UserName = 'DBText11'
                      DataField = 'uq_notified_date'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 19315
                      mmTop = 0
                      mmWidth = 33867
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText12: TppDBText
                      UserName = 'DBText12'
                      DataField = 'client_claim_id'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 74083
                      mmTop = 0
                      mmWidth = 28575
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText13: TppDBText
                      UserName = 'DBText13'
                      DataField = 'utility_co_damaged'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 103981
                      mmTop = 0
                      mmWidth = 26988
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText14: TppDBText
                      UserName = 'DBText14'
                      DataField = 'size_type'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 132292
                      mmTop = 0
                      mmWidth = 46038
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText15: TppDBText
                      UserName = 'DBText15'
                      DataField = 'location'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 179123
                      mmTop = 0
                      mmWidth = 69321
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText16: TppDBText
                      UserName = 'DBText16'
                      DataField = 'uq_resp_code'
                      DataPipeline = NewEstimatesPipe
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'NewEstimatesPipe'
                      mmHeight = 3704
                      mmLeft = 249767
                      mmTop = 0
                      mmWidth = 14552
                      BandType = 3
                      GroupNo = 0
                    end
                  end
                  object NewEstimatesFooterBand: TppGroupFooterBand
                    PrintHeight = phDynamic
                    mmBottomOffset = 0
                    mmHeight = 7408
                    mmPrintPosition = 0
                    object ppDBText28: TppDBText
                      UserName = 'DBText28'
                      DataField = 'CurrentDate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '"Current Estimate as of" m/d/yyyy'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 0
                      mmWidth = 43656
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppDBText29: TppDBText
                      UserName = 'DBText201'
                      DataField = 'CurrentEstimate'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 0
                      mmWidth = 15610
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppLabel2: TppLabel
                      UserName = 'Label2'
                      AutoSize = False
                      Caption = 'Change'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3704
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 30956
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppDBText32: TppDBText
                      UserName = 'DBText32'
                      DataField = 'Delta'
                      DataPipeline = DetailPipe
                      DisplayFormat = '#,0.00;-#,0.00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 8
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'DetailPipe'
                      mmHeight = 3704
                      mmLeft = 65617
                      mmTop = 3704
                      mmWidth = 15610
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppLine2: TppLine
                      UserName = 'Line2'
                      Weight = 1
                      mmHeight = 265
                      mmLeft = 6350
                      mmTop = 3704
                      mmWidth = 74613
                      BandType = 5
                      GroupNo = 0
                    end
                  end
                end
              end
            end
            object ChangedDamageInvoices: TppSubReport
              UserName = 'ChangedDamageInvoices'
              ExpandAll = False
              KeepTogether = True
              NewPrintJob = False
              ShiftRelativeTo = ChangedDamageDetails
              TraverseAllData = False
              DataPipelineName = 'InvoicePipe'
              mmHeight = 3704
              mmLeft = 0
              mmTop = 4233
              mmWidth = 266701
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              object ppChildReport6: TppChildReport
                AutoStop = False
                DataPipeline = InvoicePipe
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.Orientation = poLandscape
                PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
                PrinterSetup.PrinterName = 'Screen'
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 215900
                PrinterSetup.mmPaperWidth = 279401
                PrinterSetup.PaperSize = 1
                Version = '6.03'
                mmColumnWidth = 0
                DataPipelineName = 'InvoicePipe'
                object ppTitleBand2: TppTitleBand
                  mmBottomOffset = 0
                  mmHeight = 5556
                  mmPrintPosition = 0
                  object ppShape6: TppShape
                    UserName = 'Shape5'
                    Brush.Color = 14737632
                    Pen.Style = psClear
                    mmHeight = 3704
                    mmLeft = 8202
                    mmTop = 1588
                    mmWidth = 259292
                    BandType = 1
                  end
                  object ppLabel42: TppLabel
                    UserName = 'Label33'
                    AutoSize = False
                    Caption = 'Invoice Number'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 8731
                    mmTop = 1588
                    mmWidth = 26194
                    BandType = 1
                  end
                  object ppLabel43: TppLabel
                    UserName = 'Label34'
                    AutoSize = False
                    Caption = 'Amount'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 56886
                    mmTop = 1588
                    mmWidth = 13758
                    BandType = 1
                  end
                  object ppLabel44: TppLabel
                    UserName = 'Label35'
                    AutoSize = False
                    Caption = 'Company'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 72761
                    mmTop = 1588
                    mmWidth = 34396
                    BandType = 1
                  end
                  object ppLabel45: TppLabel
                    UserName = 'Label36'
                    AutoSize = False
                    Caption = 'Paid Date'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 108479
                    mmTop = 1588
                    mmWidth = 16933
                    BandType = 1
                  end
                  object ppLabel46: TppLabel
                    UserName = 'Label37'
                    AutoSize = False
                    Caption = 'Paid Amount'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 126736
                    mmTop = 1588
                    mmWidth = 17727
                    BandType = 1
                  end
                  object ppLabel47: TppLabel
                    UserName = 'Label38'
                    AutoSize = False
                    Caption = 'Comment'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 146050
                    mmTop = 1588
                    mmWidth = 63765
                    BandType = 1
                  end
                  object ppLabel48: TppLabel
                    UserName = 'Label39'
                    AutoSize = False
                    Caption = 'Received Date'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 36248
                    mmTop = 1588
                    mmWidth = 19050
                    BandType = 1
                  end
                end
                object ChangedInvoiceDetail: TppDetailBand
                  mmBottomOffset = 0
                  mmHeight = 4763
                  mmPrintPosition = 0
                  object ppDBText40: TppDBText
                    UserName = 'DBText33'
                    DataField = 'invoice_num'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 8731
                    mmTop = 529
                    mmWidth = 26194
                    BandType = 4
                  end
                  object ChangedInvoiceAmount: TppDBText
                    UserName = 'DBText34'
                    DataField = 'amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 56886
                    mmTop = 529
                    mmWidth = 13758
                    BandType = 4
                  end
                  object ppDBText42: TppDBText
                    UserName = 'DBText35'
                    DataField = 'company'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 72761
                    mmTop = 529
                    mmWidth = 34396
                    BandType = 4
                  end
                  object ppDBText43: TppDBText
                    UserName = 'DBText36'
                    DataField = 'paid_date'
                    DataPipeline = InvoicePipe
                    DisplayFormat = 'mmm d, yy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 108479
                    mmTop = 529
                    mmWidth = 16933
                    BandType = 4
                  end
                  object ChangedInvoicePaid: TppDBText
                    UserName = 'DBText37'
                    DataField = 'paid_amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 126736
                    mmTop = 529
                    mmWidth = 17727
                    BandType = 4
                  end
                  object ppDBText45: TppDBText
                    UserName = 'DBText38'
                    DataField = 'comment'
                    DataPipeline = InvoicePipe
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 146315
                    mmTop = 529
                    mmWidth = 63765
                    BandType = 4
                  end
                  object ppDBText46: TppDBText
                    UserName = 'DBText39'
                    DataField = 'received_date'
                    DataPipeline = InvoicePipe
                    DisplayFormat = 'mmm d, yy'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'ARIAL'
                    Font.Size = 8
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 36248
                    mmTop = 529
                    mmWidth = 19050
                    BandType = 4
                  end
                end
                object ppSummaryBand2: TppSummaryBand
                  mmBottomOffset = 0
                  mmHeight = 5292
                  mmPrintPosition = 0
                  object ppLabel49: TppLabel
                    UserName = 'Label32'
                    Caption = 'Totals'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 15346
                    mmTop = 529
                    mmWidth = 7673
                    BandType = 7
                  end
                  object ppLabel50: TppLabel
                    UserName = 'Label40'
                    Caption = 'Invoice Amounts'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 30692
                    mmTop = 529
                    mmWidth = 21167
                    BandType = 7
                  end
                  object ppLabel51: TppLabel
                    UserName = 'Label401'
                    Caption = 'Paid Amounts'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3704
                    mmLeft = 103717
                    mmTop = 529
                    mmWidth = 17463
                    BandType = 7
                  end
                  object ppDBCalc3: TppDBCalc
                    UserName = 'DBCalc1'
                    DataField = 'paid_amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 123296
                    mmTop = 529
                    mmWidth = 21167
                    BandType = 7
                  end
                  object ppDBCalc4: TppDBCalc
                    UserName = 'DBCalc2'
                    DataField = 'amount'
                    DataPipeline = InvoicePipe
                    DisplayFormat = '#,0.00;-#,0.00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'HAETTENSCHWEILER'
                    Font.Size = 8
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'InvoicePipe'
                    mmHeight = 3704
                    mmLeft = 53446
                    mmTop = 529
                    mmWidth = 17198
                    BandType = 7
                  end
                  object ppLine6: TppLine
                    UserName = 'Line5'
                    Weight = 0.75
                    mmHeight = 2117
                    mmLeft = 15346
                    mmTop = 0
                    mmWidth = 129382
                    BandType = 7
                  end
                end
              end
            end
          end
          object ppSummaryBand5: TppSummaryBand
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppGroup1: TppGroup
            BreakName = 'dummy'
            DataPipeline = NewEstimatesPipe
            UserName = 'Group1'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 50800
            DataPipelineName = 'NewEstimatesPipe'
            object ppGroupHeaderBand1: TppGroupHeaderBand
              PrintHeight = phDynamic
              mmBottomOffset = 0
              mmHeight = 11642
              mmPrintPosition = 0
              object ppRegion2: TppRegion
                UserName = 'Region2'
                Brush.Style = bsClear
                ParentWidth = True
                Pen.Style = psClear
                Transparent = True
                mmHeight = 9260
                mmLeft = 0
                mmTop = 794
                mmWidth = 266701
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                object ppLabel3: TppLabel
                  UserName = 'Label3'
                  AutoSize = False
                  Caption = 'Damage ID'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 2910
                  mmTop = 6086
                  mmWidth = 15081
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel4: TppLabel
                  UserName = 'Label4'
                  AutoSize = False
                  Caption = 'Notified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 19315
                  mmTop = 6086
                  mmWidth = 33867
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel5: TppLabel
                  UserName = 'Label5'
                  AutoSize = False
                  Caption = 'Damage Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 54504
                  mmTop = 6086
                  mmWidth = 18256
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel7: TppLabel
                  UserName = 'Label7'
                  AutoSize = False
                  Caption = 'Client Claim #'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 74083
                  mmTop = 6086
                  mmWidth = 28575
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel8: TppLabel
                  UserName = 'Label8'
                  AutoSize = False
                  Caption = 'Utility Company'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 103981
                  mmTop = 6086
                  mmWidth = 26988
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel9: TppLabel
                  UserName = 'Label9'
                  AutoSize = False
                  Caption = 'Type / Size of Facility'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 132292
                  mmTop = 6086
                  mmWidth = 46038
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel11: TppLabel
                  UserName = 'Label11'
                  AutoSize = False
                  Caption = 'Location'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 179652
                  mmTop = 6085
                  mmWidth = 68263
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel14: TppLabel
                  UserName = 'Label14'
                  AutoSize = False
                  Caption = 'Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 249767
                  mmTop = 6086
                  mmWidth = 14552
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel15: TppLabel
                  UserName = 'Label15'
                  ShiftWithParent = True
                  Caption = 'New Estimates for Earlier Claims'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4233
                  mmLeft = 1323
                  mmTop = 2117
                  mmWidth = 54504
                  BandType = 3
                  GroupNo = 0
                end
              end
            end
            object NewEstimatesGroupFooterBand: TppGroupFooterBand
              BeforePrint = NewEstimatesGroupFooterBandBeforePrint
              mmBottomOffset = 0
              mmHeight = 28840
              mmPrintPosition = 0
              object ppShape4: TppShape
                UserName = 'Shape4'
                Brush.Color = clSilver
                ParentHeight = True
                ParentWidth = True
                Pen.Style = psClear
                mmHeight = 28840
                mmLeft = 0
                mmTop = 0
                mmWidth = 266701
                BandType = 5
                GroupNo = 0
              end
              object ppLabel28: TppLabel
                UserName = 'Label28'
                AutoSize = False
                Caption = 'Totals, New Estimates for Earlier Claims'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2910
                mmTop = 1058
                mmWidth = 63500
                BandType = 5
                GroupNo = 0
              end
              object ppLabel29: TppLabel
                UserName = 'Label29'
                AutoSize = False
                Caption = 'Previous Estimates'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 6350
                mmTop = 4763
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object ppLabel30: TppLabel
                UserName = 'Label30'
                AutoSize = False
                Caption = 'Current Estimates'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 6350
                mmTop = 8467
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object ppLabel31: TppLabel
                UserName = 'Label201'
                AutoSize = False
                Caption = 'Change from Previous Estimates'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 6350
                mmTop = 12700
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object PreviousEstimatesEx: TppVariable
                UserName = 'PreviousEstimatesEx'
                AutoSize = False
                CalcOrder = 0
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 54769
                mmTop = 4763
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
              end
              object CurrentEstimatesEx: TppVariable
                UserName = 'CurrentEstimatesEx'
                AutoSize = False
                CalcOrder = 1
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 54769
                mmTop = 8467
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
              end
              object DifferenceEx: TppVariable
                UserName = 'DifferenceEx'
                AutoSize = False
                CalcOrder = 2
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 54769
                mmTop = 12700
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
              end
              object ppLine4: TppLine
                UserName = 'Line4'
                Weight = 1
                mmHeight = 265
                mmLeft = 6350
                mmTop = 12435
                mmWidth = 74613
                BandType = 5
                GroupNo = 0
              end
              object ppLabel16: TppLabel
                UserName = 'Label16'
                AutoSize = False
                Caption = 'Total, Changed Estimate Invoices'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2911
                mmTop = 18785
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object ppLabel18: TppLabel
                UserName = 'Label18'
                AutoSize = False
                Caption = 'Total, Changed Estimate Paid'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3704
                mmLeft = 2911
                mmTop = 23019
                mmWidth = 43656
                BandType = 5
                GroupNo = 0
              end
              object ppLine3: TppLine
                UserName = 'Line3'
                Weight = 1
                mmHeight = 265
                mmLeft = 2910
                mmTop = 17198
                mmWidth = 81756
                BandType = 5
                GroupNo = 0
              end
              object ChangedInvoiceTotal: TppVariable
                UserName = 'ChangedInvoiceTotal'
                AutoSize = False
                CalcOrder = 3
                CalcType = veDataPipelineTraversal
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                OnCalc = ChangedInvoiceTotalCalc
                TextAlignment = taRightJustified
                Transparent = True
                Visible = False
                mmHeight = 3704
                mmLeft = 97102
                mmTop = 17992
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
                CalcDataPipelineName = 'InvoicePipe'
              end
              object ChangedPaidTotal: TppVariable
                UserName = 'ChangedPaidTotal'
                AutoSize = False
                CalcOrder = 4
                CalcType = veDataPipelineTraversal
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                OnCalc = ChangedPaidTotalCalc
                TextAlignment = taRightJustified
                Transparent = True
                Visible = False
                mmHeight = 3704
                mmLeft = 97102
                mmTop = 22225
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
                CalcDataPipelineName = 'InvoicePipe'
              end
              object ppDBCalc7: TppDBCalc
                UserName = 'DBCalc7'
                DataField = 'amount'
                DataPipeline = InvoicePipe
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                ResetGroup = ppGroup1
                Transparent = True
                Visible = False
                DataPipelineName = 'InvoicePipe'
                mmHeight = 3704
                mmLeft = 127000
                mmTop = 17727
                mmWidth = 30427
                BandType = 5
                GroupNo = 0
              end
              object ppDBCalc8: TppDBCalc
                UserName = 'DBCalc8'
                DataField = 'paid_amount'
                DataPipeline = InvoicePipe
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                ResetGroup = ppGroup1
                Transparent = True
                Visible = False
                DataPipelineName = 'InvoicePipe'
                mmHeight = 3704
                mmLeft = 127000
                mmTop = 22225
                mmWidth = 30427
                BandType = 5
                GroupNo = 0
              end
              object InvoiceTotalChanged: TppVariable
                UserName = 'InvoiceTotalChanged'
                CalcOrder = 5
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 54769
                mmTop = 18785
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
              end
              object PaidTotalChanged: TppVariable
                UserName = 'PaidTotalChanged'
                CalcOrder = 6
                DataType = dtDouble
                DisplayFormat = '#,0.00;-#,0.00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                TextAlignment = taRightJustified
                Transparent = True
                mmHeight = 3704
                mmLeft = 54769
                mmTop = 23019
                mmWidth = 26458
                BandType = 5
                GroupNo = 0
              end
            end
          end
        end
      end
      object ChangedToUQ: TppSubReport
        UserName = 'ChangedToUQ'
        ExpandAll = False
        NewPrintJob = False
        ShiftRelativeTo = ChangedDamages
        TraverseAllData = False
        DataPipelineName = 'NewRespPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 10054
        mmWidth = 266701
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppChildReport7: TppChildReport
          AutoStop = False
          DataPipeline = NewRespPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '6.03'
          mmColumnWidth = 0
          DataPipelineName = 'NewRespPipe'
          object ChangedToUQTitle: TppTitleBand
            NewPage = True
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ChangedToUQDetail: TppDetailBand
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ToUQDamageIDLabel: TppDBText
              UserName = 'ToUQDamageIDLabel'
              DataField = 'uq_damage_id'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 3175
              mmTop = 794
              mmWidth = 15346
              BandType = 4
            end
            object ppDBText37: TppDBText
              UserName = 'DBText2'
              DataField = 'uq_notified_date'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 22754
              mmTop = 794
              mmWidth = 33602
              BandType = 4
            end
            object ppDBText41: TppDBText
              UserName = 'DBText41'
              DataField = 'damage_date'
              DataPipeline = NewRespPipe
              DisplayFormat = 'm/d/yyyy'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 57679
              mmTop = 794
              mmWidth = 19315
              BandType = 4
            end
            object ppDBText44: TppDBText
              UserName = 'DBText44'
              DataField = 'profit_center'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 102394
              mmTop = 794
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText47: TppDBText
              UserName = 'DBText47'
              DataField = 'utility_co_damaged'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 124354
              mmTop = 794
              mmWidth = 29369
              BandType = 4
            end
            object ppDBText48: TppDBText
              UserName = 'DBText48'
              DataField = 'city'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 154252
              mmTop = 794
              mmWidth = 27781
              BandType = 4
            end
            object ppDBText49: TppDBText
              UserName = 'DBText49'
              DataField = 'state'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 184150
              mmTop = 794
              mmWidth = 11642
              BandType = 4
            end
            object ppDBText50: TppDBText
              UserName = 'DBText50'
              DataField = 'uq_resp_code'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 197909
              mmTop = 794
              mmWidth = 17992
              BandType = 4
            end
            object ppDBText51: TppDBText
              UserName = 'DBText51'
              DataField = 'previous_uq_resp_code'
              DataPipeline = NewRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 218282
              mmTop = 794
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText52: TppDBText
              UserName = 'DBText52'
              DataField = 'current_estimate'
              DataPipeline = NewRespPipe
              DisplayFormat = '#,0.00;-#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 241036
              mmTop = 794
              mmWidth = 24077
              BandType = 4
            end
            object DummyVar2: TppVariable
              UserName = 'DummyVar2'
              CalcOrder = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              Visible = False
              mmHeight = 4233
              mmLeft = 263261
              mmTop = 529
              mmWidth = 2646
              BandType = 4
            end
            object ppDBText34: TppDBText
              UserName = 'DBText1'
              DataField = 'critical_modified_date'
              DataPipeline = NewRespPipe
              DisplayFormat = 'm/d/yyyy'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 78052
              mmTop = 794
              mmWidth = 22754
              BandType = 4
            end
          end
          object ppSummaryBand3: TppSummaryBand
            mmBottomOffset = 0
            mmHeight = 7938
            mmPrintPosition = 0
            object ppLabel64: TppLabel
              UserName = 'Label64'
              Caption = 'Current Estimate Total'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 212196
              mmTop = 2910
              mmWidth = 27781
              BandType = 7
            end
            object ChangedToUQTotal: TppDBCalc
              UserName = 'ChangedToUQTotal'
              DataField = 'current_estimate'
              DataPipeline = NewRespPipe
              DisplayFormat = '#,0.00;-#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'NewRespPipe'
              mmHeight = 3704
              mmLeft = 241036
              mmTop = 2910
              mmWidth = 24077
              BandType = 7
            end
            object ppLine7: TppLine
              UserName = 'Line7'
              Pen.Width = 2
              Weight = 1.5
              mmHeight = 3175
              mmLeft = 208757
              mmTop = 794
              mmWidth = 58473
              BandType = 7
            end
          end
          object ppGroup6: TppGroup
            BreakName = 'DummyVar2'
            BreakType = btCustomField
            UserName = 'Group6'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = ''
            object ppGroupHeaderBand6: TppGroupHeaderBand
              mmBottomOffset = 0
              mmHeight = 14023
              mmPrintPosition = 0
              object R: TppRegion
                UserName = '_'
                Brush.Style = bsClear
                Caption = '_'
                ParentWidth = True
                Pen.Style = psClear
                Transparent = True
                mmHeight = 12435
                mmLeft = 0
                mmTop = 794
                mmWidth = 266701
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                object ppLabel20: TppLabel
                  UserName = 'Label20'
                  AutoSize = False
                  Caption = 'Damage ID'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 3175
                  mmTop = 8202
                  mmWidth = 15346
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel52: TppLabel
                  UserName = 'Label52'
                  AutoSize = False
                  Caption = 'Notified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 22754
                  mmTop = 8202
                  mmWidth = 33867
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel53: TppLabel
                  UserName = 'Label53'
                  AutoSize = False
                  Caption = 'Damage Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 57679
                  mmTop = 8202
                  mmWidth = 19315
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel54: TppLabel
                  UserName = 'Label54'
                  AutoSize = False
                  Caption = 'Profit Center'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 102394
                  mmTop = 7938
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel56: TppLabel
                  UserName = 'Label56'
                  AutoSize = False
                  Caption = 'Utility Company'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 124354
                  mmTop = 7938
                  mmWidth = 28575
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel57: TppLabel
                  UserName = 'Label57'
                  AutoSize = False
                  Caption = 'City'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 154252
                  mmTop = 8202
                  mmWidth = 28046
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel58: TppLabel
                  UserName = 'Label58'
                  AutoSize = False
                  Caption = 'State'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 184150
                  mmTop = 7938
                  mmWidth = 11642
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel60: TppLabel
                  UserName = 'Label60'
                  AutoSize = False
                  Caption = 'Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 197909
                  mmTop = 7938
                  mmWidth = 18256
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel61: TppLabel
                  UserName = 'Label61'
                  ShiftWithParent = True
                  Caption = 'Damages Changed To UtiliQuest Responsibility'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4233
                  mmLeft = 1323
                  mmTop = 2117
                  mmWidth = 78581
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel62: TppLabel
                  UserName = 'Label601'
                  AutoSize = False
                  Caption = 'Prev Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 218282
                  mmTop = 7938
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                end
                object EstAsOf1: TppLabel
                  UserName = 'EstAsOf1'
                  AutoSize = False
                  Caption = 'Estimate as of 1/1/2003'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  TextAlignment = taRightJustified
                  Transparent = True
                  WordWrap = True
                  mmHeight = 7408
                  mmLeft = 241565
                  mmTop = 4233
                  mmWidth = 23548
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel78: TppLabel
                  UserName = 'Label78'
                  AutoSize = False
                  Caption = 'Modified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 77788
                  mmTop = 8202
                  mmWidth = 22490
                  BandType = 3
                  GroupNo = 0
                end
              end
            end
            object ppGroupFooterBand2: TppGroupFooterBand
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
        end
      end
      object ChangedFromUQ: TppSubReport
        UserName = 'ChangedFromUQ'
        ExpandAll = False
        NewPrintJob = False
        ShiftRelativeTo = ChangedToUQ
        TraverseAllData = False
        DataPipelineName = 'NoRespPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 15081
        mmWidth = 266701
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppChildReport8: TppChildReport
          AutoStop = False
          DataPipeline = NoRespPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '6.03'
          mmColumnWidth = 0
          DataPipelineName = 'NoRespPipe'
          object ChangedFromUQTitle: TppTitleBand
            NewPage = True
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand7: TppDetailBand
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object DummyVar1: TppVariable
              UserName = 'DummyVar1'
              CalcOrder = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              Visible = False
              mmHeight = 3969
              mmLeft = 257969
              mmTop = 529
              mmWidth = 2910
              BandType = 4
            end
            object ppDBText53: TppDBText
              UserName = 'ToUQDamageIDLabel1'
              DataField = 'uq_damage_id'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 3175
              mmTop = 1058
              mmWidth = 15346
              BandType = 4
            end
            object ppDBText54: TppDBText
              UserName = 'DBText54'
              DataField = 'uq_notified_date'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 22225
              mmTop = 1058
              mmWidth = 33602
              BandType = 4
            end
            object ppDBText55: TppDBText
              UserName = 'DBText55'
              DataField = 'damage_date'
              DataPipeline = NoRespPipe
              DisplayFormat = 'm/d/yyyy'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 57415
              mmTop = 1058
              mmWidth = 19315
              BandType = 4
            end
            object ppDBText56: TppDBText
              UserName = 'DBText56'
              DataField = 'critical_modified_date'
              DataPipeline = NoRespPipe
              DisplayFormat = 'm/d/yyyy'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 78052
              mmTop = 1058
              mmWidth = 22754
              BandType = 4
            end
            object ppDBText57: TppDBText
              UserName = 'DBText57'
              DataField = 'profit_center'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 102394
              mmTop = 1058
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText58: TppDBText
              UserName = 'DBText58'
              DataField = 'utility_co_damaged'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 124354
              mmTop = 1058
              mmWidth = 29369
              BandType = 4
            end
            object ppDBText59: TppDBText
              UserName = 'DBText59'
              DataField = 'city'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 154252
              mmTop = 1058
              mmWidth = 27781
              BandType = 4
            end
            object ppDBText60: TppDBText
              UserName = 'DBText60'
              DataField = 'state'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 184150
              mmTop = 1058
              mmWidth = 11642
              BandType = 4
            end
            object ppDBText61: TppDBText
              UserName = 'DBText501'
              DataField = 'uq_resp_code'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 197909
              mmTop = 1058
              mmWidth = 17992
              BandType = 4
            end
            object ppDBText62: TppDBText
              UserName = 'DBText62'
              DataField = 'previous_uq_resp_code'
              DataPipeline = NoRespPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 218282
              mmTop = 1058
              mmWidth = 20902
              BandType = 4
            end
            object ppDBText63: TppDBText
              UserName = 'DBText63'
              DataField = 'current_estimate'
              DataPipeline = NoRespPipe
              DisplayFormat = '#,0.00;-#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 241300
              mmTop = 794
              mmWidth = 24077
              BandType = 4
            end
          end
          object ppSummaryBand4: TppSummaryBand
            mmBottomOffset = 0
            mmHeight = 8996
            mmPrintPosition = 0
            object ppLine8: TppLine
              UserName = 'Line8'
              Pen.Width = 2
              Weight = 1.5
              mmHeight = 3175
              mmLeft = 208227
              mmTop = 794
              mmWidth = 57679
              BandType = 7
            end
            object ppLabel76: TppLabel
              UserName = 'Label76'
              Caption = 'Current Estimate Total'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3704
              mmLeft = 212461
              mmTop = 3175
              mmWidth = 27781
              BandType = 7
            end
            object ChangedFromUQTotal: TppDBCalc
              UserName = 'ChangedFromUQTotal'
              DataField = 'current_estimate'
              DataPipeline = NoRespPipe
              DisplayFormat = '#,0.00;-#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'NoRespPipe'
              mmHeight = 3704
              mmLeft = 241300
              mmTop = 3175
              mmWidth = 24077
              BandType = 7
            end
          end
          object ppGroup5: TppGroup
            BreakName = 'DummyVar1'
            BreakType = btCustomField
            UserName = 'Group5'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = ''
            object ppGroupHeaderBand5: TppGroupHeaderBand
              mmBottomOffset = 0
              mmHeight = 14288
              mmPrintPosition = 0
              object ppRegion3: TppRegion
                UserName = '_1'
                Brush.Style = bsClear
                Caption = '_'
                ParentWidth = True
                Pen.Style = psClear
                Transparent = True
                mmHeight = 11642
                mmLeft = 0
                mmTop = 1852
                mmWidth = 266701
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                object ppLabel65: TppLabel
                  UserName = 'Label202'
                  AutoSize = False
                  Caption = 'Damage ID'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 3175
                  mmTop = 8202
                  mmWidth = 15346
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel66: TppLabel
                  UserName = 'Label66'
                  AutoSize = False
                  Caption = 'Notified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 22225
                  mmTop = 8202
                  mmWidth = 33867
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel67: TppLabel
                  UserName = 'Label67'
                  AutoSize = False
                  Caption = 'Damage Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 57415
                  mmTop = 8202
                  mmWidth = 19315
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel68: TppLabel
                  UserName = 'Label68'
                  AutoSize = False
                  Caption = 'Profit Center'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 102394
                  mmTop = 7938
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel69: TppLabel
                  UserName = 'Label69'
                  AutoSize = False
                  Caption = 'Utility Company'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 124354
                  mmTop = 7938
                  mmWidth = 28575
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel70: TppLabel
                  UserName = 'Label70'
                  AutoSize = False
                  Caption = 'City'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 154252
                  mmTop = 8202
                  mmWidth = 28046
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel71: TppLabel
                  UserName = 'Label71'
                  AutoSize = False
                  Caption = 'State'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 184150
                  mmTop = 7938
                  mmWidth = 11642
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel72: TppLabel
                  UserName = 'Label602'
                  AutoSize = False
                  Caption = 'Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 197909
                  mmTop = 7938
                  mmWidth = 18256
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel73: TppLabel
                  UserName = 'Label73'
                  ShiftWithParent = True
                  Caption = 'Damages Changed From UtiliQuest Responsibility'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4233
                  mmLeft = 1323
                  mmTop = 2117
                  mmWidth = 83079
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel74: TppLabel
                  UserName = 'Label74'
                  AutoSize = False
                  Caption = 'Prev Resp Code'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 218282
                  mmTop = 7938
                  mmWidth = 20902
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel80: TppLabel
                  UserName = 'Label80'
                  AutoSize = False
                  Caption = 'Modified Date'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  Transparent = True
                  mmHeight = 3704
                  mmLeft = 77788
                  mmTop = 8202
                  mmWidth = 22490
                  BandType = 3
                  GroupNo = 0
                end
                object EstAsOf2: TppLabel
                  UserName = 'EstAsOf2'
                  AutoSize = False
                  Caption = 'Estimate as of 1/1/2003'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = []
                  TextAlignment = taRightJustified
                  Transparent = True
                  WordWrap = True
                  mmHeight = 7408
                  mmLeft = 242359
                  mmTop = 4233
                  mmWidth = 23548
                  BandType = 3
                  GroupNo = 0
                end
              end
            end
            object ppGroupFooterBand1: TppGroupFooterBand
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
        end
      end
      object GrandTotalsRegion: TppRegion
        UserName = 'GrandTotalsRegion'
        Pen.Style = psClear
        ShiftRelativeTo = ChangedFromUQ
        mmHeight = 13494
        mmLeft = 265
        mmTop = 24606
        mmWidth = 121709
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppLabel82: TppLabel
          UserName = 'Label82'
          Caption = 'Total Accrual Change'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 16
          Font.Style = [fsBold]
          Transparent = True
          mmHeight = 6615
          mmLeft = 2117
          mmTop = 28575
          mmWidth = 57415
          BandType = 4
        end
        object ppLine9: TppLine
          UserName = 'Line9'
          Pen.Width = 4
          Weight = 3
          mmHeight = 2381
          mmLeft = 1323
          mmTop = 26723
          mmWidth = 115888
          BandType = 4
        end
        object TotalAccrualChange: TppVariable
          OnPrint = TotalAccrualChangePrint
          UserName = 'TotalAccrualChange'
          CalcOrder = 0
          DataType = dtCurrency
          DisplayFormat = '$#,0.00;-$#,0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 16
          Font.Style = [fsBold]
          Transparent = True
          mmHeight = 6615
          mmLeft = 62971
          mmTop = 28575
          mmWidth = 54240
          BandType = 4
        end
      end
    end
    object ppFooterBand1: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 10583
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 10583
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 8
      end
      object ppReportCopyright: TppLabel
        UserName = 'ppReportCopyright'
        AutoSize = False
        Caption = 'Damage Estimate Changes Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 87577
        mmTop = 3440
        mmWidth = 91281
        BandType = 8
      end
      object ppReportDateTime: TppCalc
        UserName = 'ppReportDateTime'
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 230188
        mmTop = 1323
        mmWidth = 33073
        BandType = 8
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'ppReportPageNo1'
        VarType = vtPageNoDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 254530
        mmTop = 5821
        mmWidth = 8731
        BandType = 8
      end
      object ppLabel13: TppLabel
        UserName = 'Label13'
        Caption = 'UtiliQuest'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 3175
        mmTop = 1323
        mmWidth = 29898
        BandType = 8
      end
    end
  end
  object NewDamagesPipe: TppDBPipeline
    DataSource = NewDamagesDS
    OpenDataSource = False
    UserName = 'NewDamagesPipe'
    Left = 216
    Top = 24
  end
  object DetailDS: TDataSource
    AutoEdit = False
    DataSet = DetailSP
    Left = 125
    Top = 118
  end
  object DetailPipe: TppDBPipeline
    DataSource = DetailDS
    UserName = 'DetailPipe'
    Left = 216
    Top = 118
  end
  object DetailSP: TADOStoredProc
    Connection = DM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'RPT_damageestimatechanges_detail;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@DamageId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 30002
      end
      item
        Name = '@EstStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 37257d
      end
      item
        Name = '@EstEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 37437d
      end>
    Left = 35
    Top = 118
  end
  object NewEstimatesSP: TADOStoredProc
    Connection = DM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'RPT_damageestimatechanges4;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RecdStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@State'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ProfitCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@LiabilityType'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 35
    Top = 71
  end
  object NewEstimatesDS: TDataSource
    AutoEdit = False
    DataSet = NewEstimatesSP
    OnDataChange = NewEstimatesDSDataChange
    Left = 125
    Top = 72
  end
  object NewEstimatesPipe: TppDBPipeline
    DataSource = NewEstimatesDS
    OpenDataSource = False
    UserName = 'NewEstimatesPipe'
    Left = 216
    Top = 72
  end
  object InvoiceDS: TDataSource
    AutoEdit = False
    DataSet = InvoiceSP
    OnDataChange = InvoiceDSDataChange
    Left = 124
    Top = 168
  end
  object InvoiceSP: TADODataSet
    Connection = DM.Conn
    CursorType = ctStatic
    CommandText = 
      'select invoice_num, amount, company, received_date, satisfied_da' +
      'te,'#13#10'  comment, approved_date, approved_amount, paid_date, paid_' +
      'amount,'#13#10'  invoice_id'#13#10'from damage_invoice'#13#10'where damage_id = :d' +
      'amage_id'
    Parameters = <
      item
        Name = 'damage_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 36
    Top = 168
  end
  object InvoicePipe: TppDBPipeline
    DataSource = InvoiceDS
    UserName = 'InvoicePipe'
    Left = 216
    Top = 170
    object InvoicePipeppField1: TppField
      FieldAlias = 'invoice_num'
      FieldName = 'invoice_num'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField2: TppField
      FieldAlias = 'amount'
      FieldName = 'amount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField3: TppField
      FieldAlias = 'company'
      FieldName = 'company'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField4: TppField
      FieldAlias = 'received_date'
      FieldName = 'received_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField5: TppField
      FieldAlias = 'satisfied_date'
      FieldName = 'satisfied_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField6: TppField
      FieldAlias = 'comment'
      FieldName = 'comment'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField7: TppField
      FieldAlias = 'approved_date'
      FieldName = 'approved_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField8: TppField
      FieldAlias = 'approved_amount'
      FieldName = 'approved_amount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField9: TppField
      FieldAlias = 'paid_date'
      FieldName = 'paid_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField10: TppField
      FieldAlias = 'paid_amount'
      FieldName = 'paid_amount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object InvoicePipeppField11: TppField
      FieldAlias = 'invoice_id'
      FieldName = 'invoice_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
  end
  object NewRespDS: TDataSource
    AutoEdit = False
    DataSet = NewRespData
    Left = 124
    Top = 220
  end
  object NewRespPipe: TppDBPipeline
    DataSource = NewRespDS
    OpenDataSource = False
    UserName = 'NewRespPipe'
    Left = 216
    Top = 222
    object NewRespPipeppField1: TppField
      FieldAlias = 'uq_damage_id'
      FieldName = 'uq_damage_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField2: TppField
      FieldAlias = 'damage_date'
      FieldName = 'damage_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField3: TppField
      FieldAlias = 'uq_notified_date'
      FieldName = 'uq_notified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField4: TppField
      FieldAlias = 'profit_center'
      FieldName = 'profit_center'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField5: TppField
      FieldAlias = 'utility_co_damaged'
      FieldName = 'utility_co_damaged'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField6: TppField
      FieldAlias = 'city'
      FieldName = 'city'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField7: TppField
      FieldAlias = 'state'
      FieldName = 'state'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField8: TppField
      FieldAlias = 'uq_resp_code'
      FieldName = 'uq_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField9: TppField
      FieldAlias = 'previous_uq_resp_code'
      FieldName = 'previous_uq_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField10: TppField
      FieldAlias = 'previous_modified_date'
      FieldName = 'previous_modified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField11: TppField
      FieldAlias = 'current_estimate'
      FieldName = 'current_estimate'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField12: TppField
      FieldAlias = 'estimate_date'
      FieldName = 'estimate_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object NewRespPipeppField13: TppField
      FieldAlias = 'modified_date'
      FieldName = 'modified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
  end
  object NoRespDS: TDataSource
    AutoEdit = False
    DataSet = NoRespSP
    Left = 124
    Top = 276
  end
  object NoRespPipe: TppDBPipeline
    DataSource = NoRespDS
    OpenDataSource = False
    UserName = 'NoRespPipe'
    Left = 216
    Top = 276
    object NoRespPipeppField1: TppField
      FieldAlias = 'uq_damage_id'
      FieldName = 'uq_damage_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField2: TppField
      FieldAlias = 'damage_date'
      FieldName = 'damage_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField3: TppField
      FieldAlias = 'uq_notified_date'
      FieldName = 'uq_notified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField4: TppField
      FieldAlias = 'profit_center'
      FieldName = 'profit_center'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField5: TppField
      FieldAlias = 'utility_co_damaged'
      FieldName = 'utility_co_damaged'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField6: TppField
      FieldAlias = 'city'
      FieldName = 'city'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField7: TppField
      FieldAlias = 'state'
      FieldName = 'state'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField8: TppField
      FieldAlias = 'uq_resp_code'
      FieldName = 'uq_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField9: TppField
      FieldAlias = 'previous_uq_resp_code'
      FieldName = 'previous_uq_resp_code'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField10: TppField
      FieldAlias = 'previous_modified_date'
      FieldName = 'previous_modified_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField11: TppField
      FieldAlias = 'current_estimate'
      FieldName = 'current_estimate'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object NoRespPipeppField12: TppField
      FieldAlias = 'estimate_date'
      FieldName = 'estimate_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
  end
  object NoRespSP: TADOStoredProc
    Connection = DM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'RPT_damage_resp_changes2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FromDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ToDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@State'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ProfitCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ChangedToUQ'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 36
    Top = 276
  end
  object NewRespSP: TADOStoredProc
    Connection = DM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'RPT_damage_resp_changes2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FromDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ToDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RecdEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgStart'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DmgEnd'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@State'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ProfitCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ChangedToUQ'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 35
    Top = 223
  end
  object NewRespData: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 304
    Top = 224
  end
end
