unit TimeSubmitMessageResponse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm, ppStrtch, ppMemo,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimeSubmitMessageResponseDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDetailBand1: TppDetailBand;
    EmployeeNumber: TppDBText;
    EmployeeName: TppDBText;
    ActivityDate: TppDBText;
    Response: TppDBText;
    ppFooterBand1: TppFooterBand;
    Pipe: TppDBPipeline;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    ppLabel8: TppLabel;
    ProfitCenter: TppDBText;
    ContactInfoLabel: TppLabel;
    ContactPhone: TppDBText;
    RuleTypeLabel: TppLabel;
    DateLabel: TppLabel;
    ParamDescriptionDS: TDataSource;
    ParamDescriptionPipe: TppDBPipeline;
    RuleTypeDescription: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ChangedByShortName: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ChangedReason: TppDBMemo;
    SP: TADOStoredProc;
    ParamDescription: TADODataSet;
    Master: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdAdoUtils;

{ TTimeSubmitMessageResponseDM }

procedure TTimeSubmitMessageResponseDM.Configure(QueryFields: TStrings);
var
 RecsAffected : oleVariant;
begin
  inherited;
  with SP do begin  //QMANTWO-533 EB
    Parameters.ParamByName('@DateStart').Value := GetDateTime('DateStart');
    Parameters.ParamByName('@DateEnd').Value := GetDateTime('DateEnd');
    Parameters.ParamByName('@Response').Value := SafeGetString('Response', '');
    Parameters.ParamByName('@RuleType').Value := SafeGetString('RuleType', '');
    Open;
  end;
  //QMANTWO-533 EB
  Master.Recordset := SP.Recordset;
  ParamDescription.Recordset := SP.Recordset.NextRecordset(RecsAffected);

  ShowDateRange(DateLabel, 'DateStart', 'DateEnd', 'Response Dates:');
end;

initialization
  RegisterReport('TimeSubmitMessageResponse', TTimeSubmitMessageResponseDM);

end.

