unit TimeRuleCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppVar, ppBands, ppCtrls, ppPrnabl, ppClass, ppCache,
  ppProd, ppReport, DB, ppDB, ppComm, ppRelatv, ppDBPipe, HoursCalc,
  ppStrtch, ppRegion, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TTimeRuleCheckDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    EmpFooterBand: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    RuleLabel: TppLabel;
    ppDBText3: TppDBText;
    ppDBText9: TppDBText;
    ppLabel9: TppLabel;
    ppLabel21: TppLabel;
    OKMark: TppLabel;
    XMark: TppLabel;
    NameLabel: TppDBText;
    ppReportHeaderShape1: TppShape;
    ppLabel1: TppLabel;
    DetailRegion: TppRegion;
    RT2Label: TppLabel;
    OT2Label: TppLabel;
    DT2Label: TppLabel;
    WorkingHoursLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel11: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    RT1Label: TppLabel;
    OT1Label: TppLabel;
    DT1Label: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel3: TppLabel;
    CallOutHoursLabel: TppLabel;
    ppLabel4: TppLabel;
    CO1Label: TppLabel;
    ppLabel6: TppLabel;
    CO2Label: TppLabel;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    WeekEndingLabel: TppLabel;
    LevelLimitLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    procedure ppGroupHeaderBand1BeforePrint(Sender: TObject);
    procedure ppDetailBand1BeforePrint(Sender: TObject);
    procedure EmpFooterBandBeforePrint(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    HC: TBaseHoursCalculator;
    StartDate: TDateTime;
    DailyWorkingHours: TWeekData;
    DailyCallOutHours: TWeekData;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, HoursDataset, StrUtils;

{$R *.dfm}

{ TTimeRuleCheckDM }

procedure TTimeRuleCheckDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  LevelLimit: Integer;
  EmpStatus: Integer;
  WeekEnding: TDateTime;
begin
  inherited;
  ManagerID := GetInteger('emp_id');
  WeekEnding := GetDateTime('end_date');
  LevelLimit := SafeGetInteger('HierarchyDepth', 99);
  EmpStatus := GetInteger('EmployeeStatus');
  with SP do begin
    Parameters.ParamByName('@ManagerID').Value       := ManagerID;
    Parameters.ParamByName('@EndDate').Value         := WeekEnding;
    Parameters.ParamByName('@LevelLimit').Value      := LevelLimit;
    Parameters.ParamByName('@EmployeeStatus').Value  := EmpStatus;
    Open;
  end;

  ShowDateText(WeekEndingLabel, 'Week Ending:', WeekEnding);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  ShowText(LevelLimitLabel, 'Hierarchy Level Limit:', IfThen(LevelLimit=99, 'Unlimited', IntToStr(LevelLimit)));
end;

procedure TTimeRuleCheckDM.ppGroupHeaderBand1BeforePrint(Sender: TObject);
var
  I: Integer;
  Code: string;
begin
  inherited;
  Code := SP.FieldByName('Code').AsString;
  HC := CreateHoursCalculator(Code);
  HC.ClearResults;
  RuleLabel.Caption := HC.Identity;
  StartDate := SP.FieldByName('slot_work_date').AsDateTime;

  for I := 0 to 6 do begin
    DailyWorkingHours[I] := 0;
    DailyCallOutHours[I] := 0;
  end;

  Sleep(1);  // reduce CPU hogging
end;

procedure TTimeRuleCheckDM.ppDetailBand1BeforePrint(Sender: TObject);
var
  WorkingHours: Double;
  CallOutHours: Double;
  DayIndex: Integer;
  WorkDate: TDateTime;
begin
  inherited;
  WorkingHours := GetWorkHours(SP);
  CallOutHours := GetCalloutHours(SP);

  WorkDate := SP.FieldByName('slot_work_date').AsDateTime;
  DayIndex := Round(WorkDate - StartDate);

  if (DayIndex>=0) and (DayIndex<=6) then begin
    DailyWorkingHours[DayIndex] := WorkingHours;
    DailyCallOutHours[DayIndex] := CallOutHours;

    HC.Worked[DayIndex] := WorkingHours;
    HC.Callout[DayIndex] := CallOutHours;

    // load up results with the stored results
    HC.Regular[DayIndex] := SP.FieldByName('reg_hours').AsFloat;
    HC.Overtime[DayIndex] := SP.FieldByName('ot_hours').AsFloat;
    HC.DoubleTime[DayIndex] := SP.FieldByName('dt_hours').AsFloat;
    HC.CalloutCalc[DayIndex] := SP.FieldByName('callout_hours').AsFloat;
    Sleep(1);  // reduce CPU hogging
  end;
end;

function Fmt(Hours: Double): string;
const
  FmtString = '%6.2f';
begin
  if Abs(Hours) < 0.01 then
    Result := '      '
  else
    Result := Format(FmtString, [Hours]);
end;

procedure TTimeRuleCheckDM.EmpFooterBandBeforePrint(Sender: TObject);
var
  I: Integer;
  Working, Callout: string;
  CO1, CO2: string;
  RT1, RT2: string;
  OT1, OT2: string;
  DT1, DT2: string;
  TimesMatch: Boolean;
  Level: Integer;
begin
  inherited;
  // build strinngs of the stored results
  for I := 0 to 6 do begin
    Working := Working + Fmt(DailyWorkingHours[I]);
    Callout := Callout + Fmt(DailyCallOutHours[I]);

    RT1 := RT1 + Fmt(HC.Regular[I]);
    OT1 := OT1 + Fmt(HC.Overtime[I]);
    DT1 := DT1 + Fmt(HC.DoubleTime[I]);
    CO1 := CO1 + Fmt(HC.CalloutCalc[I]);
    Sleep(1);  // reduce CPU hogging
  end;

  HC.Calculate;

  // build strings of the calculated results
  for I := 0 to 6 do begin
    RT2 := RT2 + Fmt(HC.Regular[I]);
    OT2 := OT2 + Fmt(HC.Overtime[I]);
    DT2 := DT2 + Fmt(HC.DoubleTime[I]);
    CO2 := CO2 + Fmt(HC.CalloutCalc[I]);
  end;

  WorkingHoursLabel.Caption := Working;
  CallOutHoursLabel.Caption := Callout;

  RT1Label.Caption := RT1;
  OT1Label.Caption := OT1;
  DT1Label.Caption := DT1;
  CO1Label.Caption := CO1;

  RT2Label.Caption := RT2;
  OT2Label.Caption := OT2;
  DT2Label.Caption := DT2;
  CO2Label.Caption := CO2;

  TimesMatch := (RT1=RT2) and (OT1=OT2) and (DT1=DT2) and (CO1=CO2);

  OKMark.Visible := TimesMatch;
  XMark.Visible := not TimesMatch;
  DetailRegion.Visible := not TimesMatch;

  Level := SP.FieldByName('h_report_level').AsInteger;
  NameLabel.Left := (Level-1) * 0.18;
end;

procedure TTimeRuleCheckDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  HC.Free;
end;

initialization
  RegisterReport('TimeRuleCheck', TTimeRuleCheckDM);

end.

