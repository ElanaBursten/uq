unit ResponseLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd,
  ppClass, ppReport, ppVar, ppCtrls, ppPrnabl, ppBands, ppCache, ppModule,
  daDataModule, ppStrtch, ppSubRpt, ppRegion, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TResponseLogDM = class(TBaseReportDM)
    Report: TppReport;
    Pipe: TppDBPipeline;
    DS: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ppLabel11: TppLabel;
    CallCentersLabel: TppLabel;
    SuccessDS: TDataSource;
    FailureDS: TDataSource;
    FailurePipe: TppDBPipeline;
    SuccessPipe: TppDBPipeline;
    subSuccess: TppSubReport;
    SuccessChildReport: TppChildReport;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel14: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    subFailure: TppSubReport;
    FailureChildReport: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppLine1: TppLine;
    ppSubReport1: TppSubReport;
    SummaryChildReport: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppLabel23: TppLabel;
    ppLabel25: TppLabel;
    ppLabel9: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppDBText9: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel24: TppLabel;
    ppLine2: TppLine;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppLabel26: TppLabel;
    ClientsLabel: TppLabel;
    ManagerLabel: TppLabel;
    ResponseTypeLabel: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Success: TADODataSet;
    Failure: TADODataSet;
    ParamTranslation: TADODataSet;
    Master: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, odADOUtils;

{$R *.dfm}

procedure TResponseLogDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  IncludeSuccess: Boolean;
  IncludeFailure: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := SafeGetInteger('ManagerId', -1);
  IncludeSuccess := SafeGetInteger('IncludeSuccess', 0) = 1;
  IncludeFailure := SafeGetInteger('IncludeFailure', 0) = 1;
  with SP do begin
    Parameters.ParamByName('@ManagerId').Value      := ManagerID;
    Parameters.ParamByName('@StartDate').Value     := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').Value        := GetDateTime('EndDate');
    Parameters.ParamByName('@CallCenterList').Value := GetString('CallCenterList');
    Parameters.ParamByName('@ClientList').Value     := SafeGetString('ClientList', '*');
    Parameters.ParamByName('@IncludeSuccess').Value := IncludeSuccess;
    Parameters.ParamByName('@IncludeFailure').Value := IncludeFailure;
    Open;
  end;

  Master.Recordset := SP.Recordset;
  Success.Recordset := SP.Recordset.NextRecordset(RecsAffected);
  Failure.Recordset := SP.Recordset.NextRecordset(RecsAffected);
  ParamTranslation.Recordset := SP.Recordset.NextRecordset(RecsAffected);
  ShowDateRange(DateRangeLabel);
  ShowEmployeeName(ManagerLabel, ManagerID);
  CallCentersLabel.Caption := GetString('CallCenterList');

  ClientsLabel.Caption := '';
  if not ParamTranslation.Eof then
    ClientsLabel.Caption := ParamTranslation.FieldByName('param_value').AsString;

  if IncludeSuccess and IncludeFailure then
    ShowText(ResponseTypeLabel, 'Responses: ', 'Success & Failure')
  else if IncludeSuccess then
    ShowText(ResponseTypeLabel, 'Responses: ', 'Success Only')
  else if IncludeFailure then
    ShowText(ResponseTypeLabel, 'Responses: ', 'Failure Only')
  else
    ResponseTypeLabel.Visible := False;

  subSuccess.Visible := IncludeSuccess;
  subFailure.Visible := IncludeFailure;
end;

initialization
  RegisterReport('ResponseLog', TResponseLogDM);

end.
