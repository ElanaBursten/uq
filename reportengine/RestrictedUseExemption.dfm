inherited RestrictedUseExemptionDM: TRestrictedUseExemptionDM
  OldCreateOrder = True
  Height = 213
  Width = 290
  object Pipeline: TppDBPipeline
    DataSource = DS
    AutoCreateFields = False
    UserName = 'Pipeline'
    Left = 128
    Top = 24
  end
  object DS: TDataSource
    DataSet = SP
    Left = 80
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipeline
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Timesheet Audit History'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 10160
    PrinterSetup.mmMarginLeft = 10160
    PrinterSetup.mmMarginRight = 10160
    PrinterSetup.mmMarginTop = 10160
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 180
    Top = 20
    Version = '18.03'
    mmColumnWidth = 195580
    DataPipelineName = 'Pipeline'
    object ppHeaderBand2: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 30163
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentWidth = True
        mmHeight = 9144
        mmLeft = 0
        mmTop = 265
        mmWidth = 195580
        BandType = 0
        LayerName = Foreground
      end
      object ppReportHeaderLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Restricted Use Exemption Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 529
        mmTop = 2117
        mmWidth = 194205
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        Caption = 'Granted To'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 0
        mmTop = 25665
        mmWidth = 14288
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label2'
        HyperlinkEnabled = False
        Caption = 'Granted By'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 114565
        mmTop = 25665
        mmWidth = 14552
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel3: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label3'
        HyperlinkEnabled = False
        Caption = 'Effective Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 34396
        mmTop = 25665
        mmWidth = 17992
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label4'
        HyperlinkEnabled = False
        Caption = 'Expires Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 61383
        mmTop = 25665
        mmWidth = 16140
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label5'
        HyperlinkEnabled = False
        Caption = 'Revoked Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 86784
        mmTop = 25665
        mmWidth = 17727
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label7'
        HyperlinkEnabled = False
        Caption = 'Revoked By'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 153723
        mmTop = 25665
        mmWidth = 15346
        BandType = 0
        LayerName = Foreground
      end
      object GrantedDateRangeLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'GrantedDateRangeLabel'
        HyperlinkEnabled = False
        Caption = 'GrantedDateRangeLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 123825
        mmTop = 10583
        mmWidth = 34925
        BandType = 0
        LayerName = Foreground
      end
      object RevokedDateRangeLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DateRangeLabel1'
        HyperlinkEnabled = False
        Caption = 'RevokedDateRangeLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 123825
        mmTop = 15346
        mmWidth = 35983
        BandType = 0
        LayerName = Foreground
      end
      object ManagerLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'DateRangeLabel2'
        HyperlinkEnabled = False
        Caption = 'ManagerLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 20108
        mmWidth = 20638
        BandType = 0
        LayerName = Foreground
      end
      object EmployeeStatusLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmployeeStatusLabel'
        HyperlinkEnabled = False
        Caption = 'EmployeeStatusLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 15346
        mmWidth = 31485
        BandType = 0
        LayerName = Foreground
      end
      object EmployeesUnderLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'EmployeeStatusLabel1'
        HyperlinkEnabled = False
        Caption = 'EmployeesUnderLabel'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 1058
        mmTop = 10583
        mmWidth = 32808
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand3: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5027
      mmPrintPosition = 0
      object GrantedToName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'GrantedToName'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'granted_to_name'
        DataPipeline = Pipeline
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 0
        mmTop = 529
        mmWidth = 25929
        BandType = 4
        LayerName = Foreground
      end
      object GrantedByName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'GrantedByName'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'granted_by_name'
        DataPipeline = Pipeline
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 114565
        mmTop = 529
        mmWidth = 26458
        BandType = 4
        LayerName = Foreground
      end
      object EffectiveDate: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EffectiveDate'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'effective_date'
        DataPipeline = Pipeline
        DisplayFormat = 'mm/dd/yyyy hh:nn ampm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 34396
        mmTop = 529
        mmWidth = 19844
        BandType = 4
        LayerName = Foreground
      end
      object ExpireDate: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'ExpireDate'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'expire_date'
        DataPipeline = Pipeline
        DisplayFormat = 'mm/dd/yyyy hh:nn ampm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 61383
        mmTop = 529
        mmWidth = 16933
        BandType = 4
        LayerName = Foreground
      end
      object RevokedDate: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'RevokedDate'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'revoked_date'
        DataPipeline = Pipeline
        DisplayFormat = 'mm/dd/yyyy hh:nn ampm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 86784
        mmTop = 529
        mmWidth = 19579
        BandType = 4
        LayerName = Foreground
      end
      object RevokedByName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'GrantedByName1'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'revoked_by_name'
        DataPipeline = Pipeline
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'Pipeline'
        mmHeight = 3969
        mmLeft = 153723
        mmTop = 529
        mmWidth = 26723
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand2: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 11113
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 11113
        mmLeft = 0
        mmTop = 0
        mmWidth = 195580
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label6'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 1852
        mmTop = 1588
        mmWidth = 41540
        BandType = 8
        LayerName = Foreground
      end
      object ppReportCopyright: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Restricted Use Exemption Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 62442
        mmTop = 2910
        mmWidth = 70644
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 160602
        mmTop = 1588
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 171450
        mmTop = 6085
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_restricted_use_exemption'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ExemptionsFor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ExemptionsForID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@GrantedFromDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@GrantedToDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RevokedFromDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RevokedToDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EmployeeStatus'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 24
  end
end
