unit DamageList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppCtrls, ppBands, ppClass, ppVar,
  ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, DB,
  ppStrtch, ppSubRpt, ppModule, daDataModule, ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageListDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    TitleLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    TitleLabel2: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    Pipe: TppDBPipeline;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    DamageTypeDBText: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppDBText7: TppDBText;
    ppDBText9: TppDBText;
    ppLabel11: TppLabel;
    ppDBText10: TppDBText;
    ppLabel14: TppLabel;
    ppDBText11: TppDBText;
    ppLabel15: TppLabel;
    ppDBText12: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    EstCostLabel: TppLabel;
    ppEstCost: TppDBText;
    ppLabel17: TppLabel;
    ppPaidAmount: TppDBText;
    ppLabel18: TppLabel;
    LabRemaining: TppDBText;
    ppLabel19: TppLabel;
    ppTotCost: TppDBText;
    ppDBText17: TppDBText;
    ppLine1: TppLine;
    ppLabel12: TppLabel;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppDBText6: TppDBText;
    ppDBText8: TppDBText;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    GFEstCost: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    GFTotCost: TppDBCalc;
    GFRemaining: TppDBCalc;
    ppLabel9: TppLabel;
    ppLine2: TppLine;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    TotalsDS: TDataSource;
    TotalsPipe: TppDBPipeline;
    PCInitialTotalSubRep: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel23: TppLabel;
    ppDBText24: TppDBText;
    ppLabel24: TppLabel;
    ppDBText25: TppDBText;
    ppLabel25: TppLabel;
    ppDBText26: TppDBText;
    ppLabel26: TppLabel;
    ppDBText27: TppDBText;
    ppLabel27: TppLabel;
    ppDBText28: TppDBText;
    ppDBCalc5: TppDBCalc;
    ppLabel22: TppLabel;
    ppLabel28: TppLabel;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppDBText29: TppDBText;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppGroup3: TppGroup;
    PCHeaderBand: TppGroupHeaderBand;
    PCFooterBand: TppGroupFooterBand;
    ppDBText20: TppDBText;
    SummaryBand: TppSummaryBand;
    RFEstCost: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    RFTotCost: TppDBCalc;
    RFRemaining: TppDBCalc;
    ppLabel29: TppLabel;
    InitialTotalsSubRep: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppLabel35: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppDBCalc10: TppDBCalc;
    ppLabel36: TppLabel;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel37: TppLabel;
    ppLine7: TppLine;
    ppDBText34: TppDBText;
    PipeppField116: TppField;
    PipeppField117: TppField;
    PipeppField118: TppField;
    uq_damage_id: TppField;
    short_name: TppField;
    office_name: TppField;
    ppLabel10: TppLabel;
    ppLabel16: TppLabel;
    ppDBText13: TppDBText;
    ppDBText15: TppDBText;
    locator_name: TppField;
    ppLabel38: TppLabel;
    ppDBText16: TppDBText;
    paid_amount: TppField;
    remaining_expo: TppField;
    total_accrual_amount: TppField;
    accrual_amount: TppField;
    DamageDateRange: TppLabel;
    DueDateRange: TppLabel;
    NotifyDateRange: TppLabel;
    ProfitCenterLabel: TppLabel;
    ResponsabilitiesLabel: TppLabel;
    LocationLabel: TppLabel;
    InvestigationStatusLabel: TppLabel;
    ManagerLabel: TppLabel;
    InvLocLabel: TppLabel;
    InvoiceCodesLabel: TppLabel;
    ExcavatorLabel: TppLabel;
    WorkTobeDoneLabel: TppLabel;
    DamagedCompanyLabel: TppLabel;
    AttachmentsLabel: TppLabel;
    InvoicesLabel: TppLabel;
    EstimateLabel: TppLabel;
    ppTitleBand3: TppTitleBand;
    InvCodesMemo: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Reference: TADOQuery;
    Totals: TADOQuery;
    Damages: TADOQuery;
    procedure DamagesBeforeOpen(DataSet: TDataSet);
    procedure PCFooterBandBeforePrint(Sender: TObject);
    procedure SummaryBandBeforePrint(Sender: TObject);
  private
    GrandTotalsInsteadOfPC: Boolean;
    procedure ShowCurrentEstimateFields(ShowCurrent: Boolean);
    function MungeCurrInit(const SQL: string; const Current: Boolean): string;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, QMConst, OdSqlBuilder, OdRbHierarchy, OdRbUtils,
  OdMiscUtils, OdDbUtils, StrUtils;

{ TDamageListDM }

const
  AttachmentCountClause = '(select count(*) from attachment a where a.foreign_id=d.damage_id and a.foreign_type=2 and a.active=1)';
  InvoiceCountClause = '(select count(*) from damage_invoice i where i.damage_id=d.damage_id)';
  LocatorSQL1 =
     ' (select '+
     '    case '+
     '      when (d.ticket_id is null) then 0 '+
     '      when ((d.client_id is not null) and (d.ticket_id is not null)) then '+
     '      (select top 1 1 '+
     '          from employee e '+
     '            inner join locate l on e.emp_id = l.assigned_to_id '+
     '          where '+
     '            l.ticket_id = d.ticket_id and '+
     '            l.client_id = d.client_id and '+
     '            l.active = 1 and '+
     '            e.emp_id = %d) '+
     '       when (d.client_id is null) then '+
     '         (select top 1 1 '+
     '          from employee e '+
     '          inner join locate l on e.emp_id = l.assigned_to_id '+
     '          where l.ticket_id = d.ticket_id and '+
     '            l.active = 1 and '+
     '            e.emp_id = %d) '+
     '     end) = 1 ';

  LocatorSQLAll =
     ' (select '+
     '    case '+
     '      when (d.ticket_id is null) then 0 '+
     '      when ((d.client_id is not null) and (d.ticket_id is not null)) then '+
     '      (select top 1 1 '+
     '          from employee e '+
     '            inner join locate l on e.emp_id = l.assigned_to_id '+
     '            inner join (select h_emp_id from dbo.get_report_hier2(%d, 0, 99)) he on e.emp_id = he.h_emp_id '+
     '          where '+
     '            l.ticket_id = d.ticket_id and '+
     '            l.client_id = d.client_id and '+
     '            l.active = 1 ) '+
     '       when (d.client_id is null) then '+
     '         (select top 1 1 '+
     '          from employee e '+
     '            inner join locate l on e.emp_id = l.assigned_to_id '+
     '            inner join (select h_emp_id from dbo.get_report_hier2(%d, 0, 99)) he on e.emp_id = he.h_emp_id '+
     '          where l.ticket_id = d.ticket_id and '+
     '                l.active = 1) '+
     '     end) = 1 ';

procedure TDamageListDM.Configure(QueryFields: TStrings);
var
  SQL: TOdSqlBuilder;
  Attachments, Invoices: Integer;
  InvCodes: string;
  InvCodesClause: string;
  OrderClause: string;
  TotalSQL: string;
  ProfitCenterFilter: string;
  DmgClaimStatus: string;
  StatusSubtitle: string;
  EstimateType: shortint;
  IncludeUQResp, IncludeExcvResp, IncludeSpecialResp: Boolean;
  Location, City, State: string;
  DamageType: string;
  ManagerID, LocatorID, InvestigatorID: Integer;
  ExcavatorCompany, UtilityCoDamaged, WorkToBeDone: String;
  Str: string;
begin
  inherited;

  Reference.Open;
  SQL := TOdSqlBuilder.Create;
  try
    // Note that our idiom is to use the Get and SafeGet operations, never
    // refer directly to QueryFields.

    SQL.AddDateTimeRangeFilter('damage_date', SafeGetDateTime('StartDate'), SafeGetDateTime('EndDate'));
    SQL.AddDateTimeRangeFilter('d.due_date', SafeGetDateTime('StartDueDate'), SafeGetDateTime('EndDueDate'));
    SQL.AddDateTimeRangeFilter('uq_notified_date', SafeGetDateTime('StartNotifyDate'), SafeGetDateTime('EndNotifyDate'));

    IncludeUQResp := SafeGetInteger('IncludeUQResp', 0) = 1;
    IncludeExcvResp := SafeGetInteger('IncludeExcvResp', 0) = 1;
    IncludeSpecialResp := SafeGetInteger('IncludeSpecialResp', 0) = 1;

    if IncludeUQResp then
      SQL.AddWhereCondition('d.uq_resp_code is not null');

    if IncludeExcvResp then
      SQL.AddWhereCondition('d.exc_resp_code is not null');

    if IncludeSpecialResp then
      SQL.AddWhereCondition('d.spc_resp_code is not null');

    ManagerID := SafeGetInteger('Manager', 0);
    LocatorID := SafeGetInteger('Locator', 0);
    InvestigatorID := SafeGetInteger('Investigator', 0);
    if ManagerID <> 0 then begin
      // investigator
      if InvestigatorID = -1 then
        SQL.AddWhereCondition(Format('d.investigator_id in (select h_emp_id from dbo.get_report_hier2(%d, 0, 99)) ', [ManagerID]))
      else if InvestigatorID <> 0 then
        SQL.AddWhereCondition(Format('d.investigator_id = %d',[InvestigatorID]));

      // locator
      if LocatorID = -1 then
        SQL.AddWhereCondition(Format(LocatorSQLAll,[ManagerID, ManagerID]))
      else if LocatorID <> 0 then
        SQL.AddWhereCondition(Format(LocatorSQL1,[LocatorID, LocatorID]));
    end;

    OrderClause := 'ORDER BY uq_damage_id';
    ProfitCenterFilter := SafeGetString('ProfitCenter', '');

    if ProfitCenterFilter = '(Subtotal by PC)' then begin
      OrderClause := 'ORDER BY profit_center, uq_damage_id';
      PCFooterBand.Visible := True;
      PCHeaderBand.Visible := True;
      GrandTotalsInsteadOfPC := False;
    end else begin
      SQL.AddExactStringFilter('profit_center', ProfitCenterFilter);
      SummaryBand.Visible := True;
      GrandTotalsInsteadOfPC := True;
      ppGroup3.BreakName := '';
    end;

    Location := SafeGetString('Location', '');
    City := SafeGetString('City', '');
    State := SafeGetString('State', '');
    DamageType := UpperCase(SafeGetString('DamageType'));

    ExcavatorCompany := SafeGetString('ExcavatorCompany', '');
    UtilityCoDamaged := SafeGetString('UtilityCoDamaged');
    WorkToBeDone := SafeGetString('WorkToBeDone');

    SQL.AddKeywordSearch('location', Location);
    SQL.AddKeywordSearch('city', City);
    SQL.AddKeywordSearch('state', State);
    SQL.AddKeywordSearch('excavator_company', ExcavatorCompany);
    SQL.AddKeywordSearch('utility_co_damaged', UtilityCoDamaged);
    SQL.AddKeywordSearch('t.work_description', WorkToBeDone);

    if (DamageType = DamageTypePendingApproval) then
      SQL.AddWhereCondition('((damage_type = ''' + DamageTypePendingApproval + ''') or (damage_type = ''' + DamageTypeCompleted  + '''))')
    else
      SQL.AddExactStringFilter('damage_type', DamageType);

    Attachments := SafeGetInteger('Attachments', 0);
    Invoices := SafeGetInteger('Invoices', 0);

    if Attachments = Ord(acHas) then
      SQL.AddNumericFilter(AttachmentCountClause, '> 0');

    if Attachments = Ord(acDoesNotHave) then
      SQL.AddNumericFilter(AttachmentCountClause, '= 0');

    if Invoices = Ord(icHas) then
      SQL.AddNumericFilter(InvoiceCountClause, '> 0');

    if Invoices = Ord(icDoesNotHave) then
      SQL.AddNumericFilter(InvoiceCountClause, '= 0');

    DmgClaimStatus := SafeGetString('ClaimStatus', '');
    if SameText(DmgClaimStatus, 'open') or (DmgClaimStatus = '1') then begin // Open Claims
      SQL.AddWhereCondition('claim_status=''OPEN'' or claim_status IS NULL');
      StatusSubtitle := ' - Open Claims';
    end
    else if SameText(DmgClaimStatus, 'closed') or (DmgClaimStatus = '2') then begin // All Closed Claims
      SQL.AddWhereCondition('claim_status in (''CLOSPAID'',''CLOWOPAY'',''CLOSCORP'',''CLOSDUPL'')');
      StatusSubtitle := ' - Closed Claims';
    end
    else if (SameText(DmgClaimStatus, 'all')=False) and (DmgClaimStatus <> '0') then begin // Specific Claim Status
      SQL.AddWhereCondition('claim_status = ''' + DmgClaimStatus + '''');
      if Reference.Locate('type;code', VarArrayOf(['claimstat', DmgClaimStatus]), []) then
        StatusSubtitle := ' - ' + Reference.FieldByName('description').asString
      else
        StatusSubtitle := ' - ' + DmgClaimStatus;
    end;
    TitleLabel.Caption := 'Damage / Investigation List' + StatusSubtitle;

    EstimateType := SafeGetInteger('Estimate',0);
    // TODO: add something for investigation status field

    InvCodes := SafeGetString('InvCodes', '');
    if InvCodes<>'' then begin
      // Convert comma delim list in to IN clause:
      InvCodesClause := 'invoice_code in (''' + StringReplace(InvCodes, ',', ''',''', [rfReplaceAll]) + ''')';

      // NULL does not work in an IN clause, so do this:
      if Pos('---', InvCodes)>0 then
        InvCodesClause := InvCodesClause + ' OR invoice_code IS NULL';

      SQL.AddWhereCondition(InvCodesClause);
    end;

    // just for testing purposed to quickly see how one item appears on the report:
    SQL.AddIntFilterUnlessZero('d.damage_id', SafeGetInteger('damage_id', 0));
    SQL.AddIntFilterUnlessZero('d.uq_damage_id', SafeGetInteger('uq_damage_id', 0));
    {
    *** Still to be converted from old code used in QMLogic:
    SQL.AddExactStringFilter('damage_type', UpperCase(SafeGetString('DamageType', '')));

    // This needs to be handled as an integer, not a string:
    if QueryFields.Values['Investigator'] > '' then
      SQL.AddExactStringFilter('investigator_id', QueryFields.Values['Investigator']);

    // work to be done...?
    SQL.AddExactStringFilter('ticket_number', TicketNumber);
    SQL.AddKeywordSearch('short_name', Investigator);
    SQL.AddKeywordSearch('client_claim_id', ClientClaimID);
    SQL.AddKeywordSearch('d.claim_status', ClaimStatus);

    *** Need to enforce this in the RE, not on the client:
    if DM.IsManager then
      LimitInvestigatorId := 0
    else
      LimitInvestigatorId := DM.EmpID;
    }

    SQL.CheckFilterCount;
    SQL.AddWhereCondition('d.active=1');

    FSQL := GetResourceString('DAMAGE_LIST_SQL');
    FSQL := StringReplace(FSQL, '--WHERE', SQL.WhereClause, [rfReplaceAll]);
    FSQL := StringReplace(FSQL, '--ORDER', OrderClause, [rfReplaceAll]);
    FSQL := MungeCurrInit(FSQL, EstimateType=1);

    DumpSQL;

    Damages.SQL.Text := FSQL;

    TotalSQL := GetResourceString('DAMAGE_TOTALS_SQL');
    TotalSQL := StringReplace(TotalSQL, '--WHERE', SQL.WhereClause, [rfReplaceAll]);
    TotalSQL := MungeCurrInit(TotalSQL, EstimateType=1);

    if GrandTotalsInsteadOfPC then begin
      TotalSQL := StringReplace(TotalSQL, 'profit_center,', '''P'' as profit_center ,', []);
      TotalSQL := StringReplace(TotalSQL, 'profit_center,', '', [rfReplaceAll]);
    end;

    Totals.SQL.Text := TotalSQL;

(*    with TStringList.Create do begin
    try
      Text := TotalSQL;
      SaveToFile('report_query2.sql');
    finally
      Free;
    end; end; *)

    Damages.Open;
    Totals.Open;
  finally
    SQL.Free;
  end;

  ShowCurrentEstimateFields(EstimateType=1);

  TitleLabel2.Caption := TitleLabel.Caption;
  AlignRBComponentsByTag(Self);

  ////////

  ShowDateRange(DamageDateRange, 'StartDate',       'EndDate',       'Damaged:');
  ShowDateRange(DueDateRange,    'StartDueDate',    'EndDueDate',    'Due:');
  ShowDateRange(NotifyDateRange, 'StartNotifyDate', 'EndNotifyDate', 'Notified:');
  ShowText(ProfitCenterLabel, IfThen(IsEmpty(ProfitCenterFilter), 'All', ProfitCenterFilter));

  Str := IfThen(IncludeUQResp, 'UQ/STS', '');
  Str := IfThen(IsEmpty(Str), IfThen(IncludeExcvResp, 'Escavator', ''), Str + IfThen(IncludeExcvResp, '/Escavator', ''));
  Str := IfThen(IsEmpty(Str), IfThen(IncludeSpecialResp, 'Special', ''), Str + IfThen(IncludeSpecialResp, '/Special', ''));
  ShowText(ResponsabilitiesLabel, IfThen(IsEmpty(Str), 'All', Str));

  Str := Location;
  Str := IfThen(IsEmpty(Str), City, Str + IfThen(IsEmpty(City), '', ', ' + City));
  Str := IfThen(IsEmpty(Str), State, Str + IfThen(IsEmpty(State), '', ' - ' + State));
  ShowText(LocationLabel, IfThen(IsEmpty(Str), 'All', Str));

  ShowText(InvestigationStatusLabel, IfThen(IsEmpty(DamageType), 'All', DamageType));

  if ManagerID = 0 then begin
    ShowText(ManagerLabel, 'Manager:', 'All');
    ShowText(InvLocLabel, 'Inv./Loc.:', 'All');
  end else begin
    ShowEmployeeName(ManagerLabel, ManagerID, 'Manager:');
    if InvestigatorID <> 0 then
      ShowEmployeeName(InvLocLabel, InvestigatorID, 'Investigator:')
    else if LocatorID <> 0 then
      ShowEmployeeName(InvLocLabel, LocatorID, 'Locator:')
    else if (LocatorID = 0) and (InvestigatorID = 0) then
      ShowText(InvLocLabel, 'Inv./Loc.:', 'All');
  end;

  InvCodes := StringReplace(InvCodes, '---', 'Blank', [rfReplaceAll]);

  // the dot and spaces in the string above is a little odd, but it is being used for layout purposes.
  // it makes the memo not overlap the ClientsLabel and use the space above this label in case there
  // are 2 lines in the memo.
  if not SameText(InvCodes, '') then
    InvCodesMemo.Text := '.                      ' + StringReplace(InvCodes, ',', ', ', [rfReplaceAll]);

  ShowText(ExcavatorLabel, 'Excavator:', IfThen(IsEmpty(ExcavatorCompany), 'All', ExcavatorCompany));
  ShowText(WorkTobeDoneLabel, 'Work to be done:', IfThen(IsEmpty(WorkToBeDone), 'All', WorkToBeDone));
  ShowText(DamagedCompanyLabel, 'Utility Co. Damaged:', IfThen(IsEmpty(UtilityCoDamaged), 'All', UtilityCoDamaged));

  case Attachments of
    Ord(acAll): AttachmentsLabel.Visible := False;
    Ord(acHas): AttachmentsLabel.Caption := 'Has Attachments';
    Ord(acDoesNotHave): AttachmentsLabel.Caption := 'No Attachments';
  end;

  case Invoices of
    Ord(icAll): InvoicesLabel.Visible := False;
    Ord(icHas): InvoicesLabel.Caption := 'Has Invoices';
    Ord(icDoesNotHave): InvoicesLabel.Caption := 'No Invoices';
  end;

  ShowText(EstimateLabel, 'Estimate:', IfThen(EstimateType = 0, 'Initial', 'Current'));
end;

procedure TDamageListDM.DamagesBeforeOpen(DataSet: TDataSet);
const
  DescriptionLength = 100;
begin
  AddLookupField('exc_resp_desc', Damages, 'exc_resp_code',
    Reference, 'code', 'description', DescriptionLength, 'ExcRespDescField');
  AddLookupField('uq_resp_desc', Damages, 'uq_resp_code',
    Reference, 'code', 'description', DescriptionLength, 'UQRespDescField');
  AddLookupField('spc_resp_desc', Damages, 'spc_resp_code',
    Reference, 'code', 'description', DescriptionLength, 'SpcRespDescField');
end;

function TDamageListDM.MungeCurrInit(const SQL: string;
  const Current: Boolean): string;
begin
  if Current then
    Result := StringReplace(SQL, 'curr_', '', [rfReplaceAll])
  else
    Result := StringReplace(SQL, 'init_', '', [rfReplaceAll]);
end;

procedure TDamageListDM.PCFooterBandBeforePrint(Sender: TObject);
begin
  inherited;
  Totals.Filter := 'profit_center=''' + Damages.FieldByName('profit_center').AsString + '''';
  Totals.Filtered := True;
end;

procedure TDamageListDM.ShowCurrentEstimateFields(ShowCurrent: Boolean);
begin
  if ShowCurrent then
    EstCostLabel.Caption := 'Current Estimated Cost'
  else
    EstCostLabel.Caption := 'Initial Estimated Cost';
end;

procedure TDamageListDM.SummaryBandBeforePrint(Sender: TObject);
begin
  inherited;
  Totals.Filtered := False;
  Totals.Filter := '';
end;

initialization
  RegisterReport('DamageList', TDamageListDM);

end.

