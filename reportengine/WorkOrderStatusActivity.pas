unit WorkOrderStatusActivity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReportLayout, ppCtrls, ppMemo, ppStrtch, ppRegion,
  ppBands, ppVar, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB, ppComm,
  ppRelatv, ppDBPipe, DB, BaseReport, ppParameter, ppDesignLayer, Data.Win.ADODB;

type
  TWorkOrderStatusActivityDM = class(TBaseReportLayoutDM)
    ClientsListDS: TDataSource;
    ClientsListPipe: TppDBPipeline;
    WorkOrdersStatusedFrom: TppLabel;
    WorkOrdersTransmittedFrom: TppLabel;
    WorkOrdersLabel: TppLabel;
    HeaderRegion: TppRegion;
    ppLabel2: TppLabel;
    ppLabel5: TppLabel;
    ppLabel11: TppLabel;
    ppLabel8: TppLabel;
    ppLabel7: TppLabel;
    ppLabel3: TppLabel;
    ppLabel6: TppLabel;
    ppLabel9: TppLabel;
    ppLabel1: TppLabel;
    ppLabel4: TppLabel;
    ppLabel10: TppLabel;
    ppLabel12: TppLabel;
    ppLine1: TppLine;
    ppLabel13: TppLabel;
    CallCentersRegion: TppRegion;
    CallCentersLabel: TppLabel;
    CallCentersMemo: TppMemo;
    ClientsLabel: TppLabel;
    ClientCodesMemo: TppDBMemo;
    ManagerLabel: TppLabel;
    ppDBText5: TppDBText;
    ppDBText8: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText9: TppDBText;
    ppDBText13: TppDBText;
    ppDBText10: TppDBText;
    ppDBText2: TppDBText;
    ppDBText1: TppDBText;
    ppDBText4: TppDBText;
    ppDBText7: TppDBText;
    ppDBText3: TppDBText;
    ppDBText6: TppDBText;
    SP: TADOStoredProc;
    ClientsList: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
  end;

implementation

uses ReportEngineDMu, QMConst, OdDbUtils, OdMiscUtils, OdAdoUtils;

{$R *.dfm}

{ TWorkOrderStatusActivityDM }

procedure TWorkOrderStatusActivityDM.Configure(QueryFields: TStrings);
var
  TransmitDateFromParam: TDateTime;
  TransmitDateToParam: TDateTime;
  StatusDateFromParam: TDateTime;
  StatusDateToParam: TDateTime;
  CallCenterCodesParam: string;
  WorkOrderClosedParam: Integer;
  ClientIDList: string;
  ManagerIDParam: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  StatusDateFromParam := SafeGetDateTime('StatusDateFrom');
  StatusDateToParam := SafeGetDateTime('StatusDateTo');
  TransmitDateFromParam := SafeGetDateTime('TransmitDateFrom');
  TransmitDateToParam := SafeGetDateTime('TransmitDateTo');
  if ((StatusDateFromParam = 0) and (StatusDateToParam = 0) and
     (TransmitDateFromParam = 0) and (TransmitDateToParam = 0)) then begin
    raise Exception.Create('Either the status or transmit date range must be provided.');
  end;

  CallCenterCodesParam := GetString('CallCenterCodes');
  WorkOrderClosedParam := SafeGetInteger('WorkOrderClosed', 2); //default All
  ClientIDList := GetString('ClientIDList');
  ManagerIDParam := SafeGetInteger('ManagerID', -1); // default all
  ShowEmployeeName(ManagerLabel, ManagerIDParam);

  if (StatusDateFromParam = 0) and (StatusDateToParam = 0) then begin
    StatusDateFromParam := EncodeDate(1900, 1, 1);
    StatusDateToParam := EncodeDate(2199, 1, 1);
    ShowText(WorkOrdersStatusedFrom, 'All');
  end
  else if (StatusDateFromParam > 0) and (StatusDateToParam > 0) then begin
    ShowText(WorkOrdersStatusedFrom, DateToStr(StatusDateFromParam) + ' - ' + DateToStr(StatusDateToParam - 1))
  end
  else begin
    StatusDateToParam := StatusDateFromParam + 1;
    ShowText(WorkOrdersStatusedFrom, DateToStr(StatusDateFromParam));
  end;

  if (TransmitDateFromParam > 0) and (TransmitDateToParam > 0) then
    ShowText(WorkOrdersTransmittedFrom, DateToStr(TransmitDateFromParam) + ' - ' + DateToStr(TransmitDateToParam - 1))
  else begin
    TransmitDateFromParam := EncodeDate(1900, 1, 1);
    TransmitDateToParam := EncodeDate(2199, 1, 1);
    ShowText(WorkOrdersTransmittedFrom, 'All');
  end;

  CallCentersMemo.Text := CallCenterCodesParam;

  case WorkOrderClosedParam of
    0: ShowText(WorkOrdersLabel, 'Open');
    1: ShowText(WorkOrdersLabel, 'Closed');
    else ShowText(WorkOrdersLabel, 'All');
  end;

  //QMANTWO-533 EB
  SP.Parameters.ParamByName('@StatusDateFrom').Value := StatusDateFromParam;
  SP.Parameters.ParamByName('@StatusDateTo').Value := StatusDateToParam;
  SP.Parameters.ParamByName('@CallCenterCodes').Value := CallCenterCodesParam;
  SP.Parameters.ParamByName('@WorkOrderClosed').Value := WorkOrderClosedParam;
  SP.Parameters.ParamByName('@TransmitDateFrom').Value := TransmitDateFromParam;
  SP.Parameters.ParamByName('@TransmitDateTo').Value := TransmitDateToParam;
  SP.Parameters.ParamByName('@ClientIDList').Value := ClientIDList;
  SP.Parameters.ParamByName('@ManagerID').Value := ManagerIDParam;
  SP.Open;


//QMANTWO-533 EB
  Master.Recordset := SP.Recordset;
  ClientsList.Recordset := SP.Recordset.NextRecordset(RecsAffected);
end;

procedure TWorkOrderStatusActivityDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
var
  SkipFieldsList : TStringList;
begin
  SkipFieldsList := TStringList.Create;
  try
    SkipFieldsList.Add('wo_source');
    SkipFieldsList.Add('client_id');
    SkipFieldsList.Add('oc_code');
    SaveDelimToStream(SP, Str, Delimiter, True, nil, SkipFieldsList);
  finally
    FreeAndNil(SkipFieldsList);
  end;
end;

initialization
  RegisterReport('WorkOrderStatusActivity', TWorkOrderStatusActivityDM);

end.

