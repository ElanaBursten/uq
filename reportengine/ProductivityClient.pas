unit ProductivityClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppVar, ppBands, ppStrtch, ppCTMain,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  DB, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TProductivityClientDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    CT: TppCrossTab;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    Pipe: TppDBPipeline;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportCopyright: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    SPManagerLocator: TStringField;
    SPemp_id: TIntegerField;
    SPTerm: TStringField;
    SPReceived: TIntegerField;
    procedure CTFormatCell(Sender: TObject; aElement: TppElement; aColumn,
      aRow: Integer);
    procedure CTGetCaptionText(Sender: TObject; aElement: TppElement;
      aColumn, aRow: Integer; const aDisplayFormat: string;
      aValue: Variant; var aText: string);
  public
    procedure Configure(QueryFields: TStrings); override;

  end;


implementation

uses ReportEngineDMu;

{$R *.dfm}

{ TProductivityClientDM }

procedure TProductivityClientDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  EmpStatus := GetInteger('EmployeeStatus');
  with SP do begin
    Parameters.ParamByName('@StartDate').value := GetDateTime('DateFrom');
    Parameters.ParamByName('@EndDate').value   := GetDateTime('DateTo');
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    Open;
  end;

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo', '', True);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
end;

procedure TProductivityClientDM.CTFormatCell(Sender: TObject;
  aElement: TppElement; aColumn, aRow: Integer);
begin
  inherited;
  if (aColumn>=2) and (aRow>=2) and (aRow < CT.RowDefCount) then  // headings in smaller text
    aElement.Font.Size := 8
  else
    aElement.Font.Size := 7;

  aElement.Font.Style := [ ];

  if aColumn = 1 then  // Put the Tickets number in bold
    aElement.Font.Style := aElement.Font.Style + [fsBold];

  if aRow = 1 then  // Underline the column headers
    aElement.Font.Style := aElement.Font.Style + [fsUnderline];
end;

procedure TProductivityClientDM.CTGetCaptionText(Sender: TObject;
  aElement: TppElement; aColumn, aRow: Integer;
  const aDisplayFormat: string; aValue: Variant; var aText: string);
begin
  inherited;
  if (aRow=0) then
    aText := '';  // suppress annoying default text
end;

initialization
  RegisterReport('ProductivityClient', TProductivityClientDM);

end.
