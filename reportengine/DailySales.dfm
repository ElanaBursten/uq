inherited DailySalesReportDM: TDailySalesReportDM
  OldCreateOrder = True
  Height = 153
  Width = 399
  object Pipe: TppDBPipeline
    DataSource = SPDS
    OpenDataSource = False
    UserName = 'Pipe'
    Left = 152
    Top = 24
    object PipeppField1: TppField
      FieldAlias = 'invoice_id'
      FieldName = 'invoice_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
  end
  object SPDS: TDataSource
    DataSet = SP
    Left = 88
    Top = 24
  end
  object Report: TppReport
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Timesheet Exception Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    AllowPrintToArchive = True
    AllowPrintToFile = True
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    CachePages = True
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    ModalPreview = False
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    ShowAutoSearchDialog = True
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 216
    Top = 24
    Version = '18.03'
    mmColumnWidth = 0
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 11113
      mmPrintPosition = 0
      object ppReportHeaderShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 11113
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 0
        LayerName = Foreground
      end
      object ppReportHeaderLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportHeaderLabel'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Daily Manager Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 3175
        mmTop = 2381
        mmWidth = 259821
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 47361
      mmPrintPosition = 0
      object ppLabel1: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label1'
        HyperlinkEnabled = False
        Caption = 'This report is not yet complete.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = []
        Transparent = True
        mmHeight = 6615
        mmLeft = 99219
        mmTop = 30163
        mmWidth = 76200
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 12700
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 12700
        mmLeft = 0
        mmTop = 0
        mmWidth = 266701
        BandType = 8
        LayerName = Foreground
      end
      object ppReportCopyright: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Daily Manager Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 55827
        mmTop = 4763
        mmWidth = 157427
        BandType = 8
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label13'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 3175
        mmTop = 2646
        mmWidth = 41540
        BandType = 8
        LayerName = Foreground
      end
    end
    object daDataModule1: TdaDataModule
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_DailyManager'
    Parameters = <
      item
        Name = '@ManagerID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@StartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@EndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
      end>
    Left = 32
    Top = 24
  end
end
