unit EmployeeRights;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, ppCtrls, ppBands, ppClass, ppVar, ppPrnabl,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB,
  ppStrtch, ppRegion, ppPageBreak, ppSubRpt, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TEmployeeRightsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    HeaderBand: TppHeaderBand;
    DetailBand: TppDetailBand;
    FooterBand: TppFooterBand;
    ppReportFooterShape1: TppShape;
    LocatingCompanyLabel: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    SummaryBand: TppSummaryBand;
    ppShape8: TppShape;
    ppLabel24: TppLabel;
    CompanyGroup: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    ProfitCenter: TppDBText;
    SummaryColumnHeadersRegion: TppRegion;
    SumamryPCLabel: TppLabel;
    CompanyName: TppDBText;
    ppTitleBand3: TppTitleBand;
    EmpNumber: TppDBText;
    EmpShortName: TppDBText;
    EmpFirstName: TppDBText;
    EmpLastName: TppDBText;
    EmpType: TppDBText;
    RightAllowed: TppDBText;
    EmpStatus: TppDBText;
    RightDescription: TppDBText;
    EmpGroup: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLine1: TppLine;
    RightsDS: TDataSource;
    RightsPipe: TppDBPipeline;
    ReportParamsDS: TDataSource;
    ParamsPipe: TppDBPipeline;
    ParamsSubreport: TppSubReport;
    ParamsChildReport: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    LimitRightsSubReport: TppSubReport;
    LimitRightsChildReport: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLabel10: TppLabel;
    ppDetailBand4: TppDetailBand;
    ppDBText21: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppColumnHeaderBand2: TppColumnHeaderBand;
    ppColumnFooterBand2: TppColumnFooterBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Rights: TADODataSet;
    ReportParams: TADODataSet;
    procedure StartDateGetText(Sender: TObject; var Text: string);
    procedure ReportBeforePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdRbHierarchy, OdMiscUtils, odADOutils;

procedure TEmployeeRightsDM.Configure(QueryFields: TStrings);
var
  RecsAffected : oleVariant;
begin
  inherited;
  SP.Parameters.ParamByName('@ManagerID').value := GetInteger('ManagerID');
  SP.Parameters.ParamByName('@EmployeeStatus').value := GetInteger('EmployeeStatus');
  SP.Parameters.ParamByName('@EmployeeID').value := GetInteger('EmployeeId');
  SP.Parameters.ParamByName('@RightStatus').value := GetInteger('RightStatus');
  SP.Parameters.ParamByName('@RightIDList').value := SafeGetString('RightIDList');

  SP.Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
  SP.Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
  SP.Parameters.ParamByName('@OrigGrantStartDate').value := Null;
  SP.Parameters.ParamByName('@OrigGrantEndDate').value := Null;
  SP.Open;


  Master.Recordset  := SP.Recordset;
  Rights.Recordset  := SP.Recordset.NextRecordset(RecsAffected);
  ReportParams.Recordset  := SP.Recordset.NextRecordset(RecsAffected);
end;

procedure TEmployeeRightsDM.ReportBeforePrint(Sender: TObject);
begin
  inherited;
  LimitRightsSubReport.Visible := not Rights.IsEmpty;
end;

procedure TEmployeeRightsDM.StartDateGetText(Sender: TObject;
  var Text: string);
begin
  if Text = '' then
    Text := '-';
end;

initialization
  RegisterReport('EmployeeRights', TEmployeeRightsDM);

end.
