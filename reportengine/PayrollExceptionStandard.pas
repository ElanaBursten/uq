unit PayrollExceptionStandard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppModule, daDataModule, ppCtrls, ppBands,
  ppVar, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppComm, ppRelatv,
  ppDB, ppDBPipe, ppDesignLayer, ppParameter, Data.Win.ADODB;

type
  TPayrollExceptionStandardDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel2: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel3: TppLabel;
    ppLabel26: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    short_namePPText: TppDBText;
    EmpNumberText: TppDBText;
    ppDBText24: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText6: TppDBText;
    ppDBText13: TppDBText;
    ppDBText17: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLine1: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppLine2: TppLine;
    ppShape2: TppShape;
    ppLabel15: TppLabel;
    ManagerLabel: TppLabel;
    DateLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel4: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Data: TADOStoredProc;
  private
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils;

{ TPayrollExceptionStandardDM }

procedure TPayrollExceptionStandardDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EndDate: TDateTime;
begin
  inherited;
  ManagerID := GetInteger('manager_id');
  EndDate := GetDateTime('end_date');
  with Data do begin
    Parameters.ParamByName('@ManagerID').value := ManagerID;
    Parameters.ParamByName('@EndDate').value := EndDate;
    Open;
  end;

  AlignRBComponentsByTag(Self);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowDateText(DateLabel, 'Week Ending:', EndDate);
end;

initialization
  RegisterReport('PayrollExceptionStandard', TPayrollExceptionStandardDM);

end.
