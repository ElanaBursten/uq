unit NoResponseTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppDBPipe, ppBands, ppCtrls, ppClass, ppVar,
  ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport, DB,
  ppStrtch, ppSubRpt, ppModule, daDataModule, ppMemo, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TNoResponseTicketsDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText2: TppDBText;
    Pipe: TppDBPipeline;
    FromDateLabel: TppLabel;
    ToDateLabel: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppSummaryBand1: TppSummaryBand;
    ClientTotalsSubRep: TppSubReport;
    ppChildReport1: TppChildReport;
    ClientTotalsDS: TDataSource;
    ClientTotalsPipe: TppDBPipeline;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppLabel7: TppLabel;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLabel8: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppLine1: TppLine;
    ppDBText3: TppDBText;
    ppLabel14: TppLabel;
    ppReportCopyright: TppLabel;
    ppCalc1: TppCalc;
    ppSystemVariable2: TppSystemVariable;
    ppReportFooterShape1: TppShape;
    ppDBText4: TppDBText;
    ppLabel11: TppLabel;
    ppLabel15: TppLabel;
    ppLabel17: TppLabel;
    ppLabel20: TppLabel;
    CentersLabel: TppLabel;
    ppReportHeaderShape1: TppShape;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    ClientTotals: TADODataSet;
    Master: TADODataSet;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, odADOutils;

{$R *.dfm}

{ TNoResponseTicketsDM }

procedure TNoResponseTicketsDM.Configure(QueryFields: TStrings);
var
  StartDate, EndDate: TDateTime;
  RecsAffected : oleVariant;
begin
  inherited;
  StartDate := GetDateTime('StartDate');
  EndDate := GetDateTime('EndDate');
  CentersLabel.Caption := GetString('CallCenterList');

  // These seem to be used in a different way from the usual, so I did not
  // use ShowDateRange (yet)
  FromDateLabel.Caption := DateToStr(StartDate - 1);
  ToDateLabel.Caption := DateToStr(EndDate);

  with SP do begin
    Parameters.ParamByName('@StartDate').value := StartDate;
    Parameters.ParamByName('@EndDate').value := EndDate;
    Parameters.ParamByName('@CallCenterList').value := GetString('CallCenterList');
    Open;
  end;

  Master.Recordset         := SP.Recordset;
  ClientTotals.Recordset   := SP.Recordset.NextRecordset(RecsAffected);
end;

initialization
  RegisterReport('NoResponseTickets', TNoResponseTicketsDM);

end.
