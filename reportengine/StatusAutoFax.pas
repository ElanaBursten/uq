unit StatusAutoFax;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppCtrls, ppBands, ppStrtch,
  ppMemo, ppClass, ppVar, ppPrnabl, ppCache, ppProd, ppReport, DB,
  ppDB, ppComm, ppRelatv, ppDBPipe, ReportConfig, ppDesignLayer, ppParameter,
  OdSimpleLog, Data.Win.ADODB;

type
  TStatusAutoFaxDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText8: TppDBText;
    ReportLabel: TppLabel;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    ppLabel5: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    SyncDateLabel: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel13: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLabel14: TppLabel;
    ppLabel19: TppLabel;
    OfficeAddressMemo: TppMemo;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppDBText3: TppDBText;
    ppDBText7: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBMemo1: TppDBMemo;
    ppLabel30: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    QueueFaxes3: TADOStoredProc;
    SetQueued: TADOCommand;
    FaxIds: TADODataSet;
    LocalAreaCodes: TADODataSet;
    Master: TADODataSet;
    procedure OfficeAddressMemoPrint(Sender: TObject);
    procedure MasterFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FaxLog : TEnhancedLog;
    Config: TReportConfig;
    ControlFile: TStringList;
    CallCenter: string;
    FaxHeader: string;
    LogCaptureAll: Boolean;

    procedure QueueFaxesNew;
    procedure GenerateFaxJobs;
    procedure GenerateJob(FaxLocalAreaCodes: string);
    procedure SendFaxMaker(FaxNumber, FaxId: string; PdfStream: TStream);
    procedure StartFaxLogging(Path: string; CallCenter: string);
    procedure AddFaxLogEntry(const Message: string; IsError : Boolean = False; Console: Boolean = False);
  public
    procedure RunNew;
  end;

implementation

uses ReportEngineDMu, OdIsoDates, odAdoUtils, OdInternetUtil, OdMiscUtils,
           eFax, ReportEngineLog, QMConst;

{$R *.dfm}

{ TStatusAutoFaxDM }

procedure TStatusAutoFaxDM.StartFaxLogging(Path: string; CallCenter: string);
begin
  FaxLog := TEnhancedLog.Create(FaxResponses + CallCenter + '.log', Path);
end;

procedure TStatusAutoFaxDM.AddFaxLogEntry(const Message: string; IsError : Boolean = False; Console: Boolean = False);
begin
 if Console then
   WriteLn(Message)
 else begin
   Assert(Assigned(FaxLog), 'FaxLog is not assigned');
   FaxLog.AddLogEntry(Message, IsError);
 end;

end;


procedure TStatusAutoFaxDM.OfficeAddressMemoPrint(Sender: TObject);
var
  S: string;

  procedure AddLine(const L: string);
  begin
    if L <> '' then
      if S = '' then
        S := L
      else
        S := S + sLineBreak + L;
  end;

  function CityStateZip(const City, State, Zip: string): string;
  begin
    if City = '' then
      Result := State
    else if State <> '' then
      Result := City + ', ' + State
    else
      Result := City;
    if Zip <> '' then
      if Result = '' then
        Result := Zip
      else
        Result := Result + ' ' + Zip;
  end;

begin
  S := '';
  try
    AddLine(Master.FieldByName('company_name').asString);
    AddLine(Master.FieldByName('address1').asString);
    AddLine(Master.FieldByName('address2').asString);
    AddLine(CityStateZip(
      Master.FieldByName('city').asString,
      Trim(Master.FieldByName('state').asString),
      Master.FieldByName('zipcode').asString));
    AddLine('Phone: ' + Master.FieldByName('phone').asString);
    AddLine('Fax: ' + Master.FieldByName('fax').asString);
  except
    S := '';
  end;
  OfficeAddressMemo.Text := S;
end;

procedure TStatusAutoFaxDM.MasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := DataSet.FieldByName('fm_id').AsInteger =
    FaxIds.FieldByName('fm_id').AsInteger;
end;

procedure TStatusAutoFaxDM.GenerateFaxJobs;
var
  FaxLocalAreaCodes: string;
  RecsAffected : oleVariant;
begin
  try
      AddFaxLogEntry('Generating fax files');
      SP.Open;

      Master.RecordSet := SP.RecordSet;
      LocalAreaCodes.RecordSet   := SP.Recordset.NextRecordset(RecsAffected);
      FaxIds.RecordSet           := SP.Recordset.NextRecordset(RecsAffected);

      if LocalAreaCodes.Eof then
        FaxLocalAreaCodes := ''
      else if LocalAreaCodes.FieldByName('fax_local_area_codes').IsNull then
        FaxLocalAreaCodes := ''
      else
        FaxLocalAreaCodes := LocalAreaCodes.FieldByName('fax_local_area_codes').AsString;

      while not FaxIds.Eof do begin
        if SameText(FaxIds.FieldByName('call_center').AsString, CallCenter) or (CallCenter = '*') then
          GenerateJob(FaxLocalAreaCodes);//this generates the fax from the report

        FaxIds.Next;
      end;
  except on E: Exception do //QMANTWO-497
    AddFaxLogEntry('GenerateFaxJobs failed with error: '+E.Message); //QMANTWO-497
  end; //QMANTWO-497

  FaxIds.Close;
  SP.Close;
  Master.Close;
end;

procedure TStatusAutoFaxDM.GenerateJob(FaxLocalAreaCodes: string);
var
  FmId: Integer;
  OutStream: TStream;
  FaxNumber: string;
  RecipientName: string;
  RecipientCompany: string;
  FaxSystem: string;
  ExtDocID: string;

  function FormattedFaxNumber(const FaxIn, FaxLocalAreaCodes: string): string;
  const
    Prefix = '1';
  var
    i: Integer;
    PhoneDigits: string;
  begin
    PhoneDigits := '';
    for i := 1 to Length(FaxIn) do
      if IsCharNumeric(FaxIn[i]) then
        PhoneDigits := PhoneDigits + FaxIn[i];

    if (IsEmpty(FaxLocalAreaCodes) and NotEmpty(PhoneDigits)) or
      ((Length(PhoneDigits) >= 10) and (not StrBeginsWith(Prefix, PhoneDigits))
      and (StrNotContain(Copy(PhoneDigits, 1, 3), FaxLocalAreaCodes))) then
      Result := Prefix + FaxIn
    else
      Result := FaxIn;
  end;
begin
  Master.Filtered := False;
  Master.Filtered := True;
  FmId := FaxIds.FieldByName('fm_id').AsInteger;
  SetBrandingStrings;

  FaxNumber := FormattedFaxNumber(Master.FieldByName('caller_fax').AsString,
    FaxLocalAreaCodes);
  RecipientName := Master.FieldByName('caller').AsString;
  RecipientCompany := Master.FieldByName('con_name').AsString;
  FaxSystem := Master.FieldByName('send_type').AsString;

  OutStream := TMemoryStream.Create;
  try
    CreatePDFStream(OutStream);

    if SameText(FaxSystem, 'efax') then begin
      SendEFax(FaxNumber, IntToStr(FmId), FaxHeader,
        RecipientName, RecipientCompany, OutStream, Config, ExtDocID);
    end else begin
      SendFaxMaker(FaxNumber, IntToStr(FmId), OutStream);
    end;

    SetQueued.Parameters.ParamByName('id').value := FmId;
    SetQueued.Parameters.ParamByName('ext_doc_id').value := ExtDocID;
    SetQueued.Parameters.ParamByName('send_type').value := FaxSystem;
    SetQueued.Execute;
  finally
    OutStream.Free;
  end;
end;

procedure TStatusAutoFaxDM.SendFaxMaker(FaxNumber, FaxId: string;
  PdfStream: TStream);
var
  OutputFileName: string;
  OutputDir: string;
  FaxMakerOutStream: TFileStream;
begin
  OutputDir := Config.FaxQueueDir;
  OutputFileName := OutputDir + FaxId + '.pdf';
  AddFaxLogEntry('Saving FAX ' + FaxId + ' to ' + OutputFileName);

  //Copy the PDFStream memory stream to a filestream that places the fax file on disk.
  FaxMakerOutStream := TFileStream.Create(OutputFileName, fmCreate);
  try
    PdfStream.Seek(0,0);
    FaxMakerOutStream.CopyFrom(PdfStream,PdfStream.Size);
  finally
    FaxMakerOutStream.Free;
  end;

  ControlFile := TStringList.Create;
  try
    ControlFile.Add('::' + FaxNumber);
    ControlFile.Add('::C=none,B=' + FaxId + ',H,A=' + FaxId + '.pdf');
    ControlFile.SaveToFile(OutputDir + FaxId + '.txt');
  finally
    ControlFile.Free;
  end;
end;

procedure TStatusAutoFaxDM.RunNew;
var
  DM: TReportEngineDM;
  IniFileName: string;
begin
  inherited;
  try
    if ParamCount < 2 then
      raise Exception.Create('Parameters: /faxqueue CallCenter /logalltofile  {CallCenter can be a single center or * for ALL}'
        + #10#13 + '{/logalltofile is optional - use to capture all console output to log file} ');

    CallCenter := ParamStr(2);

    if ParamCount = 3 then
     LogCaptureAll := LowerCase(ParamStr(3))='/logalltofile';

    IniFileName := ChangeFileExt(ParamStr(0), '.ini');
    Config := LoadConfig(IniFileName);

    if NotEmpty(Config.LogFileName) then begin
      StartFaxLogging(Config.LogFileName, CallCenter);
      WriteLn('Output logged to ' + GetDailyFileName(Config.LogFileName, FaxResponses + CallCenter + '.log'));
    end;

    AddFaxLogEntry('Starting at ' + DateTimeToStr(Now), False, not LogCaptureAll);

    DM := TReportEngineDM.Create(nil);
    try
      AddFaxLogEntry('Connecting to ' + Config.MainDB.Server + '/' + Config.MainDB.DB, False, not LogCaptureAll);
      ConnectAdOConnectionWithConfig(DM.Conn, Config.MainDB);
      FaxHeader := DM.GetConfigurationDataValue('FaxHeader','');

      UseConnection(DM.Conn, 800);

      QueueFaxesNew;//This is where any new faxes get inserted into fax_message by pulling from fax_queue where ticket/locate criteria are met
      GenerateFaxJobs;//This joins fax_message with other ticket data to return 'pending' faxes and then sends them to the configured Fax system (i.e. FaxMaker or eFax)

      AddFaxLogEntry('Done', False, not LogCaptureAll)
    finally
      DM.Free;
    end;
  except
    on E: Exception do begin
      AddFaxLogEntry('********** ERROR **************', True);
      AddFaxLogEntry(E.Message, True);
      if StrContains('eFax',E.Message) then begin
        AddFaxLogEntry('', True);
        AddFaxLogEntry('Emailing error notification', True);
        SendEmailWithIniSettings(IniFileName,'Fax','','','Error sending eFax',E.Message);  //QM-137 added placeholder
      end;
      Sleep(2000);
    end;
  end;
end;

procedure TStatusAutoFaxDM.QueueFaxesNew;
begin
  AddFaxLogEntry('Call Center: ' + CallCenter);

  AddFaxLogEntry('Queueing faxes into fax_message table');
  QueueFaxes3.Parameters.ParamByName('@CallCenter').value := CallCenter;
  QueueFaxes3.Parameters.ParamByName('@QueueNow').value := 1;
  QueueFaxes3.Open;

  AddFaxLogEntry('Faxes Queued: ' + QueueFaxes3.FieldByName('FaxesQueued').AsString);
  AddFaxLogEntry('     Locates: ' + QueueFaxes3.FieldByName('LocatesQueued').AsString);
end;

procedure TStatusAutoFaxDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FaxLog);
end;

end.

