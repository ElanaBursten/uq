unit EmployeesInTraining;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, ppCtrls, ppBands, ppClass, ppVar, ppPrnabl,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, DB,
  ppStrtch, ppSubRpt, ppRegion, ppPageBreak, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TEmployeesInTrainingDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    HeaderBand: TppHeaderBand;
    DetailBand: TppDetailBand;
    FooterBand: TppFooterBand;
    ppReportFooterShape1: TppShape;
    LocatingCompanyLabel: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    SummaryDS: TDataSource;
    SummaryPipe: TppDBPipeline;
    SummaryBand: TppSummaryBand;
    ppShape8: TppShape;
    ManagerLabel1: TppLabel;
    DateRangeLabel1: TppLabel;
    ppLabel24: TppLabel;
    SummaryTitleLabel: TppLabel;
    SummaryCompanyGroup: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    SummaryCompanyTotalName: TppDBText;
    CompanyCountLabel: TppLabel;
    CompanyEmpCount: TppDBCalc;
    CompanyAvgTrainDays: TppDBCalc;
    CompanyGroupTopLine: TppLine;
    SummaryPC: TppDBText;
    SummaryTraineeCount: TppDBText;
    SummaryAvgDays: TppDBText;
    ReportTotalLabel: TppLabel;
    ReportTotalTraineeCount: TppDBCalc;
    ReportAvgDays: TppDBCalc;
    ReportTopLine: TppLine;
    ReportBottomLine: TppLine;
    SummaryColumnHeadersRegion: TppRegion;
    SumamryPCLabel: TppLabel;
    SummaryTraineeCountLabel: TppLabel;
    SummaryAvgDaysLabel: TppLabel;
    SummaryCompanyName: TppDBText;
    ppTitleBand3: TppTitleBand;
    DetailSubReport: TppSubReport;
    DetailChildReport: TppChildReport;
    DetailTitleBand: TppTitleBand;
    ppHeaderBand2: TppHeaderBand;
    ReportHeaderShape: TppShape;
    ReportTitle: TppLabel;
    ManagerLabel: TppLabel;
    DateRangeLabel: TppLabel;
    DetailColumnHeadersRegion: TppRegion;
    EmpNumberColTitle: TppLabel;
    EmpNameColTitle: TppLabel;
    StartDateColTitle: TppLabel;
    TrainingDaysColTitle: TppLabel;
    TicketsCompleteColTitle: TppLabel;
    CovStatusColTitle: TppLabel;
    DetailDetailBand: TppDetailBand;
    EmpNumber: TppDBText;
    ShortName: TppDBText;
    StartDate: TppDBText;
    DaysInTraining: TppDBText;
    TicketsClosed: TppDBText;
    CovStatus: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppShape7: TppShape;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppCalc2: TppCalc;
    ppSystemVariable3: TppSystemVariable;
    DetailSummaryBand: TppSummaryBand;
    CompanyGroup: TppGroup;
    CompanyGroupHeader: TppGroupHeaderBand;
    ppShape1: TppShape;
    CompanyContinuedLabel: TppLabel;
    Company: TppDBText;
    CompanyGroupFooter: TppGroupFooterBand;
    ProfitCenterGroup: TppGroup;
    PCGroupHeaderBand: TppGroupHeaderBand;
    DetailPC: TppDBText;
    PCContinuedLabel: TppLabel;
    PCGroupFooterBand: TppGroupFooterBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Summary: TADODataSet;
    procedure PCGroupHeaderBandBeforePrint(Sender: TObject);
    procedure CompanyGroupHeaderBeforePrint(Sender: TObject);
    procedure StartDateGetText(Sender: TObject; var Text: string);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu, OdRbHierarchy, OdMiscUtils, odADOutils;

procedure TEmployeesInTrainingDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  StartDate: TDateTime;
  EndDate: TDateTime;
  ShowDetails: Boolean;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  StartDate := GetDateTime('StartDate');
  EndDate := GetDateTime('EndDate');
  ShowDetails := GetBoolean('ShowDetails');

  SP.Parameters.ParamByName('@ManagerID').value := ManagerID;
  SP.Parameters.ParamByName('@StartDate').value := StartDate;
  SP.Parameters.ParamByName('@EndDate').value := EndDate;
  SP.Open;


  Master.RecordSet        :=  SP.Recordset;
  Summary.RecordSet       :=  SP.Recordset.NextRecordset(RecsAffected);

  ShowEmployeeName(ManagerLabel, ManagerId);
  ShowEmployeeName(ManagerLabel1, ManagerId);
  ShowDateRange(DateRangeLabel);
  ShowDateRange(DateRangeLabel1);
  DetailSubReport.Visible := ShowDetails;
end;

procedure TEmployeesInTrainingDM.PCGroupHeaderBandBeforePrint(Sender: TObject);
begin
  PCContinuedLabel.Visible := not ProfitCenterGroup.FirstPage;
end;

procedure TEmployeesInTrainingDM.StartDateGetText(Sender: TObject;
  var Text: string);
begin
  if Text = '' then
    Text := '-';
end;

procedure TEmployeesInTrainingDM.CompanyGroupHeaderBeforePrint(Sender: TObject);
begin
  CompanyContinuedLabel.Visible := not CompanyGroup.FirstPage;
end;

initialization
  RegisterReport('EmployeesInTraining', TEmployeesInTrainingDM);

end.
