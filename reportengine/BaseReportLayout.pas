unit BaseReportLayout;
{Base class for common visual elements of the report, such as footer and header.  Be
careful making changes to this class, as inherited form layouts will be affected.}

interface

uses
  SysUtils, Classes, DB, Data.Win.ADODB, Graphics, BaseReport,
  ppParameter, ppDesignLayer, ppBands, ppVar, ppCtrls, ppPrnabl,
  ppClass, ppCache, ppProd, ppReport, ppDB, ppComm, ppRelatv, ppDBPipe;

type
  TBaseReportLayoutDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport; 
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    TitleShape: TppShape;
    BaseReportTitle: TppLabel;
    FooterShape: TppShape;
    LocatingCompanyLabel: TppLabel;
    CopyrightLabel: TppLabel;
    ReportDateTimeCalc: TppCalc;
    ReportPageSystemVar: TppSystemVariable;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Master: TADODataSet;
  public
  end;

implementation

uses ReportEngineDMu, QMConst, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TBaseReportLayoutDM }



//initialization
//set this in descendants:  RegisterReport('WorkOrderStatusActivity', TBaseReportLayoutDM);

end.


