unit DamageApprovalDetails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppParameter, ppBands, ppStrtch, ppMemo,
  ppCtrls, ppVar, ppPrnabl, ppClass, ppCache, ppDBPipe, ppDB, ppDBJIT, ppComm,
  ppRelatv, ppProd, ppReport, ppSubRpt, ppRegion, ppDesignLayer, Data.Win.ADODB;

type
  TDamageApprovalDetailsDM = class(TBaseReportDM)
    Report: TppReport;
    Pipe: TppDBPipeline;
    ApprovalDetailsReportDataSource: TDataSource;
    ppHeaderBand: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppTitleBand1: TppTitleBand;
    ppLabel24: TppLabel;
    ppLabel89: TppLabel;
    ppLabel27: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppSystemVariable2: TppSystemVariable;
    ppLine12: TppLine;
    ppDBText13: TppDBText;
    ppLine1: TppLine;
    ppLine4: TppLine;
    EstimateDS: TDataSource;
    RequiredMissingDocsDS: TDataSource;
    EstimatePipe: TppDBPipeline;
    RequiredMissingDocsPipe: TppDBPipeline;
    RequiredMissingDocsSection: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppTitleBand2: TppTitleBand;
    ApprovalSection: TppSubReport;
    ppChildReport7: TppChildReport;
    ppDetailBand12: TppDetailBand;
    ppShape22: TppShape;
    ppLabel157: TppLabel;
    ppLabel19: TppLabel;
    ppDBText8: TppDBText;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppLabel23: TppLabel;
    ppDBText12: TppDBText;
    ppShape1: TppShape;
    ppLabel4: TppLabel;
    ReqDocsLabel: TppLabel;
    ExcavatorResponsibilitySection: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppShape2: TppShape;
    ppLabel1: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppLabel6: TppLabel;
    ppDBText2: TppDBText;
    ppLabel7: TppLabel;
    ppDBText3: TppDBText;
    UQSTSResponsibilitySection: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppShape3: TppShape;
    ppLabel2: TppLabel;
    ppDetailBand4: TppDetailBand;
    ppLabel12: TppLabel;
    ppDBText4: TppDBText;
    ppLabel11: TppLabel;
    ppDBText5: TppDBText;
    ppLabel13: TppLabel;
    ppDBText6: TppDBText;
    ppLabel14: TppLabel;
    ppDBMemo3: TppDBMemo;
    UtilityResponsibilitySection: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand5: TppTitleBand;
    ppShape4: TppShape;
    ppLabel3: TppLabel;
    ppDetailBand5: TppDetailBand;
    ppLabel16: TppLabel;
    ppDBText7: TppDBText;
    ppLabel17: TppLabel;
    ppDBMemo4: TppDBMemo;
    ppDBMemo5: TppDBMemo;
    ppRegion1: TppRegion;
    ResponseRegion: TppRegion;
    ppLabel8: TppLabel;
    ppDBMemo1: TppDBMemo;
    ppLabel9: TppLabel;
    ppDBMemo2: TppDBMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Master: TADODataSet;
    Estimate: TADODataSet;
    RequiredMissingDocs: TADODataSet;
    procedure RequiredMissingDocsSectionPrint(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Configure(QueryFields: TStrings); override;
  end;


implementation

{$R *.dfm}

uses ReportEngineDMu, odAdoUtils;

{ TDamageApprovalDetailsDM }

procedure TDamageApprovalDetailsDM.Configure(QueryFields: TStrings);
var
  DamageId : Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  DamageID := GetInteger('DamageID');
  SP.Parameters.ParamByName('@damage_id').value := DamageID;
  SP.Open;

  Assert(SP.RecordCount <= 1, 'More damage information returned than expected. ' +
    'The system administrator should check for duplicate excavator, excavation, ' +
    'or facility reference codes.');


  Master.Recordset                :=  SP.Recordset;
  Estimate.Recordset              :=  SP.Recordset.NextRecordset(RecsAffected);
  RequiredMissingDocs.Recordset   :=  SP.Recordset.NextRecordset(RecsAffected);

  ExcavatorResponsibilitySection.Visible := (Master.FieldByName('exres').IsNull = False);
  UQSTSResponsibilitySection.Visible := (Master.FieldByName('uqres').IsNull = False);
  UtilityResponsibilitySection.Visible := (Master.FieldByName('utres').IsNull = False);
end;


procedure TDamageApprovalDetailsDM.RequiredMissingDocsSectionPrint(
  Sender: TObject);
begin
  inherited;
  ReqDocsLabel.Visible := (RequiredMissingDocs.RecordCount = 0);
end;

initialization
  RegisterReport('DamageApprovalDetails', TDamageApprovalDetailsDM);

end.
