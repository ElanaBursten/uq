unit TimesheetExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TTimesheetExportDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    WeekEndingLabel: TppLabel;
    FinalOnlyLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdMiscUtils, QMConst, ReportEngineLog;

procedure TTimesheetExportDM.Configure(QueryFields: TStrings);
var
  WeekEnding: TDateTime;
  FinalOnly: Boolean;
begin
  inherited;

  WeekEnding := GetDateTime('WeekEnding');
  FinalOnly := GetInteger('FinalOnly') = 1;

  SP.Parameters.ParamByName('@WeekEnding').Value := WeekEnding;
  SP.Parameters.ParamByName('@FinalOnly').Value := FinalOnly;
  SP.Open;

  ShowDateText(WeekEndingLabel, 'Week Ending:', WeekEnding);
  ShowBooleanText(FinalOnlyLabel, 'Show Final Export Only:', FinalOnly);
end;

initialization
  RegisterReport('TimesheetExport', TTimesheetExportDM);

end.

