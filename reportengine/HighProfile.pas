unit HighProfile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppDB, ppVar, ppCtrls, ppBands, ppPrnabl, ppClass,
  ppCache, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, DB,
  ppStrtch, ppMemo, ppModule, ppSubRpt, myChkBox, ppRegion, ppParameter,
  ppDesignLayer, ADODB;

type
  THighProfileDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    NotesDS: TDataSource;
    NotesPipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    DateRangeLabel: TppLabel;
    ManagerOfficeLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppLabel2: TppLabel;
    ClientCodeText: TppDBText;
    ClosedStatusedLabel: TppLabel;
    ClosedText: TppDBText;
    ppLabel3: TppLabel;
    LocatorText: TppDBText;
    ppLabel11: TppLabel;
    myDBCheckBox1: TmyDBCheckBox;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    TicketNumberText: TppDBText;
    ppLabel4: TppLabel;
    ReceivedText: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    DescriptionMemo: TppDBMemo;
    ppLabel8: TppLabel;
    ContactNameText: TppDBText;
    ContactAddressText: TppDBText;
    ContactCityStateZip: TppVariable;
    ContactNamePhone: TppVariable;
    WorkTypeText: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppRegion1: TppRegion;
    WorkAddress: TppVariable;
    WorkCityState: TppVariable;
    ppLabel15: TppLabel;
    TicketCount: TppVariable;
    ppLabel17: TppLabel;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDBText2: TppDBText;
    ppLabel20: TppLabel;
    ppDBText4: TppDBText;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppDetailBand2: TppDetailBand;
    ppLabel9: TppLabel;
    NoteMemo: TppDBMemo;
    ppFooterBand2: TppFooterBand;
    ppLine1: TppLine;
    ppLabel21: TppLabel;
    ppLine2: TppLine;
    TicketsLabel: TppLabel;
    ppLabel10: TppLabel;
    County: TppVariable;
    ppDBMemo1: TppDBMemo;
    ClientsHeaderRegion: TppRegion;
    MemoClients: TppMemo;
    ClientsLabel: TppLabel;
    HPReasonLabel: TppLabel;
    HPReasonMemo: TppMemo;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    Notes: TADODataSet;
    HPReason: TADODataSet;
    Clients: TADODataSet;
    Master: TADODataSet;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    procedure ContactCityStateZipCalc(Sender: TObject; var Value: Variant);
    procedure ContactNamePhoneCalc(Sender: TObject; var Value: Variant);
    procedure WorkAddressCalc(Sender: TObject; var Value: Variant);
    procedure WorkCityStateCalc(Sender: TObject; var Value: Variant);
    procedure ppGroupHeaderBand1BeforeGenerate(Sender: TObject);
    procedure PipeRecordPositionChange(Sender: TObject);
    procedure CountyCalc(Sender: TObject; var Value: Variant);
    procedure ppDetailBand1BeforePrint(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

{$R *.dfm}

uses
  ReportEngineDMu, OdDbUtils, OdMiscUtils, QMConst, ReportEngineLog, StrUtils,
  odAdoUtils;

procedure THighProfileDM.Configure(QueryFields: TStrings);
var
  ClientIDs: string;
  ManagerID: Integer;
  OfficeID: Integer;
  HPReasonList: String;
  Kind: Integer;
  RecsAffected : oleVariant;
begin
  inherited;
  ClientIDs := SafeGetString('ClientIDs');
  ManagerID := SafeGetInteger('ManagerId', -1);
  OfficeID := SafeGetInteger('OfficeId', -1);
  Kind := SafeGetInteger('Kind', 0); // this is the default value of the TicketsComboBox.ItemIndex in the client - All tickets

  if not IsEmpty(ClientIDs) then
    if not ValidateIntegerCommaSeparatedList(ClientIDs) then
      raise Exception.Create('The parameter ClientIDs=' + ClientIDs + ' is not valid.');

  with SP do begin
    Parameters.ParamByName('@DateFrom').value := GetDateTime('DateFrom');
    Parameters.ParamByName('@DateTo').value := GetDateTime('DateTo');
    Parameters.ParamByName('@OfficeId').value := OfficeID;
    Parameters.ParamByName('@ManagerId').value := ManagerId;
    Parameters.ParamByName('@ClientIDs').value := ClientIDs;
    Parameters.ParamByName('@kind').value := Kind;
    // HighProfileIDs is a comma-delimited list of ids (integers), from new clients
    Parameters.ParamByName('@HighProfileIDs').value := SafeGetString('HighProfileIDs', '');
    Open;
  end;

  Master.Recordset   :=  SP.Recordset;
  Notes.Recordset    :=  SP.Recordset.NextRecordset(RecsAffected);
  HPReason.Recordset :=  SP.Recordset.NextRecordset(RecsAffected);
  Clients.Recordset  :=  SP.Recordset.NextRecordset(RecsAffected);

  ShowDateRange(DateRangeLabel, 'DateFrom', 'DateTo');
  if ManagerId <> -1 then
    ShowEmployeeName(ManagerOfficeLabel, ManagerId)
  else if OfficeID <> -1 then
    ShowText(ManagerOfficeLabel, 'Office:', GetFieldValue(SP.Connection as TADOConnection, 'office', 'office_name', Format('office_id = %d', [OfficeID])));

  HPReasonList := GetDataSetFieldPlainString(HPReason, 'hp_reason');
  HPReasonMemo.Lines.Text := IfThen(IsEmpty(HPReasonList), 'All', HPReasonList);

  if SameText(ClientIDs, '') then
    MemoClients.Lines.Text := 'All'
  else
    MemoClients.Lines.Text := GetDataSetFieldPlainString(Clients, 'client_code', 30);

  case Kind of
    0: ShowText(TicketsLabel, 'All');
    1: ShowText(TicketsLabel, 'Open');
    2: ShowText(TicketsLabel, 'Closed');
  end;
end;

procedure THighProfileDM.ContactCityStateZipCalc(Sender: TObject; var Value: Variant);
var
  City, State, Zip : string;
begin
  City := Master.FieldByName('con_city').AsString;
  State := Master.FieldByName('con_state').AsString;
  Zip := Master.FieldByName('con_zip').AsString;

  if (City = '') then
    Value := State
  else if (State <> '') then
    Value := City + ', ' + State
  else
    Value := City;

  if (Zip <> '') then
    if (Value = '') then
      Value := Zip
    else
      Value := Value + ' ' + Zip;
end;

procedure THighProfileDM.ContactNamePhoneCalc(Sender: TObject;
  var Value: Variant);
var
  Name, Phone, Fax : string;
begin
  Name := Master.FieldByName('caller_contact').AsString;
  Phone := Master.FieldByName('caller_phone').AsString;
  Fax := Master.FieldByName('caller_fax').AsString;
  if (Name <> '') and (Phone <> '') then
    Value := Name + ' (' + Phone + ')'
  else if Name <> '' then
    Value := Name
  else
    Value := Phone;
  if Fax <> '' then Value := Value + '  fax: ' + Fax;
end;

procedure THighProfileDM.CountyCalc(Sender: TObject; var Value: Variant);
begin
  Value := Master.FieldByName('work_county').Value;
end;

procedure THighProfileDM.WorkAddressCalc(Sender: TObject;
  var Value: Variant);
var
  S, Number, Address, Cross : string;
begin
  Number := Master.FieldByName('work_address_number').AsString;
  Address := Master.FieldByName('work_address_street').AsString;
  Cross := Master.FieldByName('work_cross').AsString;
  if Number = '' then
    S := Address
  else if Address = '' then
    S := Number
  else
    S := Number + ' ' + Address;
  if S = '' then
    Value := Cross
  else if Cross = '' then
    Value := S
  else
    Value := S + ' & ' + Cross;
end;

procedure THighProfileDM.WorkCityStateCalc(Sender: TObject;
  var Value: Variant);
var
  City, State : string;
begin
  City := Master.FieldByName('work_city').AsString;
  State := Master.FieldByName('work_state').AsString;

  if (City = '') then
    Value := State
  else if (State <> '') then
    Value := City + ', ' + State
  else
    Value := City;
end;

procedure THighProfileDM.ppDetailBand1BeforePrint(Sender: TObject);
begin
  inherited;
  if Master.FieldByName('closed').AsBoolean then
    ClosedStatusedLabel.Caption := 'Closed'
  else
    ClosedStatusedLabel.Caption := 'Statused';
end;

procedure THighProfileDM.ppGroupHeaderBand1BeforeGenerate(Sender: TObject);
begin
  TicketCount.Value := TicketCount.Value + 1;
end;

procedure THighProfileDM.PipeRecordPositionChange(Sender: TObject);
begin
  inherited;
  Notes.Filter := 'ticket_id=' + IntToStr(Master.FieldByName('ticket_id').AsInteger);
  Notes.Filtered := True;
end;

initialization
  RegisterReport('HighProfile', THighProfileDM);

end.

