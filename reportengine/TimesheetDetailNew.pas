unit TimesheetDetailNew;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppBands, ppClass, ppCtrls, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppComm, ppRelatv, ppDBPipe,
  ppStrtch, ppRegion, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TTimesheetDetailNewDM = class(TBaseReportDM)
    DS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    DayDetailBand: TppDetailBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel13: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    FooterBand: TppFooterBand;
    ppSystemVariable3: TppSystemVariable;
    ppLabel26: TppLabel;
    ppDBText14: TppDBText;
    ppLine3: TppLine;
    ppDBText10: TppDBText;
    ppDBText12: TppDBText;
    fentry_date_local: TppDBText;
    fapprove_date_local: TppDBText;
    ffinal_approve_date_local: TppDBText;
    fmiles_start1: TppDBText;
    fmiles_stop1: TppDBText;
    fvehicle_use: TppDBText;
    freg_hours: TppDBText;
    fot_hours: TppDBText;
    fdt_hours: TppDBText;
    fcallout_hours: TppDBText;
    fvac_hours: TppDBText;
    fleave_hours: TppDBText;
    fbr_hours: TppDBText;
    fhol_hours: TppDBText;
    fjury_hours: TppDBText;
    fent_emp_short_name: TppDBText;
    fapp_emp_short_name: TppDBText;
    ffap_emp_short_name: TppDBText;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppDBText6: TppDBText;
    ppDBText13: TppDBText;
    ppLabel20: TppLabel;
    lentry_date_local: TppLabel;
    lapprove_date_local: TppLabel;
    lfinal_approve_date_local: TppLabel;
    lvehicle_use: TppLabel;
    LeftLine: TppLine;
    ppPageStyle1: TppPageStyle;
    ppLine2: TppLine;
    ppLabel12: TppLabel;
    ppLine5: TppLine;
    lreg_hours: TppLabel;
    lot_hours: TppLabel;
    ldt_hours: TppLabel;
    lcallout_hours: TppLabel;
    lvac_hours: TppLabel;
    ljury_hours: TppLabel;
    lhol_hours: TppLabel;
    lleave_hours: TppLabel;
    lbr_hours: TppLabel;
    ppLabel32: TppLabel;
    lmiles_start1: TppLabel;
    lmiles_stop1: TppLabel;
    lent_emp_short_name: TppLabel;
    lapp_emp_short_name: TppLabel;
    lfap_emp_short_name: TppLabel;
    ppLabel44: TppLabel;
    reg_hours_total: TppDBCalc;
    ot_hours_total: TppDBCalc;
    dt_hours_total: TppDBCalc;
    callout_hours_total: TppDBCalc;
    vac_hours_total: TppDBCalc;
    jury_hours_total: TppDBCalc;
    hol_hours_total: TppDBCalc;
    leave_hours_total: TppDBCalc;
    br_hours_total: TppDBCalc;
    ppLabel14: TppLabel;
    ppLabel23: TppLabel;
    lfdt_hours_total: TppLabel;
    ppLabel25: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    EmpTotalsDS: TDataSource;
    EmpTotalsPipe: TppDBPipeline;
    ppLabel5: TppLabel;
    mileage_total: TppDBCalc;
    PipeppField63: TppField;
    lemp_type: TppLabel;
    femp_type: TppDBText;
    TitleBand: TppTitleBand;
    ppReportHeaderLabel: TppLabel;
    ppReportHeaderShape1: TppShape;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    WeekEndingDateLabel: TppLabel;
    HierOptionLabel: TppLabel;
    SubTitleLabel: TppLabel;
    SortLabel: TppLabel;
    WorkRegion: TppRegion;
    CalloutRegion: TppRegion;
    fwork_stop1: TppDBText;
    fwork_stop2: TppDBText;
    fwork_stop3: TppDBText;
    fwork_stop4: TppDBText;
    lwork_stop1: TppLabel;
    lwork_stop2: TppLabel;
    lwork_stop3: TppLabel;
    lwork_stop4: TppLabel;
    fwork_stop5: TppDBText;
    fwork_stop6: TppDBText;
    fwork_stop7: TppDBText;
    fwork_stop8: TppDBText;
    lwork_stop5: TppLabel;
    lwork_stop6: TppLabel;
    lwork_stop7: TppLabel;
    lwork_stop8: TppLabel;
    fwork_stop9: TppDBText;
    fwork_stop10: TppDBText;
    lwork_stop9: TppLabel;
    lwork_stop10: TppLabel;
    ppDBText25: TppDBText;
    ppDBText27: TppDBText;
    ppDBText29: TppDBText;
    ppDBText31: TppDBText;
    ppDBText15: TppDBText;
    ppDBText18: TppDBText;
    ppDBText20: TppDBText;
    ppDBText22: TppDBText;
    ppDBText24: TppDBText;
    ppLabel6: TppLabel;
    ppLabel10: TppLabel;
    fcallout_stop1: TppDBText;
    fcallout_stop2: TppDBText;
    fcallout_stop3: TppDBText;
    fcallout_stop4: TppDBText;
    fcallout_stop5: TppDBText;
    fcallout_stop6: TppDBText;
    lcallout_stop1: TppLabel;
    lcallout_stop2: TppLabel;
    lcallout_stop3: TppLabel;
    lcallout_stop4: TppLabel;
    lcallout_stop5: TppLabel;
    lcallout_stop6: TppLabel;
    ppDBText11: TppDBText;
    ppDBText16: TppDBText;
    ppDBText35: TppDBText;
    ppDBText37: TppDBText;
    ppDBText5: TppDBText;
    ppDBText8: TppDBText;
    ppDBText7: TppDBText;
    fwork_stop11: TppDBText;
    fwork_stop12: TppDBText;
    fwork_stop13: TppDBText;
    fwork_stop14: TppDBText;
    lwork_stop11: TppLabel;
    lwork_stop12: TppLabel;
    lwork_stop13: TppLabel;
    lwork_stop14: TppLabel;
    fwork_stop15: TppDBText;
    fwork_stop16: TppDBText;
    lwork_stop15: TppLabel;
    lwork_stop16: TppLabel;
    ppDBText28: TppDBText;
    ppDBText30: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppDBText36: TppDBText;
    fcallout_stop7: TppDBText;
    fcallout_stop8: TppDBText;
    fcallout_stop9: TppDBText;
    fcallout_stop10: TppDBText;
    fcallout_stop11: TppDBText;
    fcallout_stop12: TppDBText;
    lcallout_stop7: TppLabel;
    lcallout_stop8: TppLabel;
    lcallout_stop9: TppLabel;
    lcallout_stop10: TppLabel;
    lcallout_stop11: TppLabel;
    lcallout_stop12: TppLabel;
    ppDBText38: TppDBText;
    ppDBText39: TppDBText;
    ppDBText40: TppDBText;
    ppDBText41: TppDBText;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    fcallout_stop13: TppDBText;
    fcallout_stop14: TppDBText;
    fcallout_stop15: TppDBText;
    fcallout_stop16: TppDBText;
    lcallout_stop14: TppLabel;
    lcallout_stop15: TppLabel;
    lcallout_stop16: TppLabel;
    lcallout_stop17: TppLabel;
    ppDBText48: TppDBText;
    ppDBText49: TppDBText;
    ppDBText50: TppDBText;
    ppDBText51: TppDBText;
    lpto_hours: TppLabel;
    fpto_hours: TppDBText;
    PipeppField64: TppField;
    ppLabel7: TppLabel;
    pto_hours_total: TppDBCalc;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDesignLayer2: TppDesignLayer;
    Data: TADOStoredProc;
    EmpTotals: TADODataSet;
    Dummy: TADODataSet;
    Master: TADODataSet;
    MasterMany: TADODataSet;
    ManySheets: TADOStoredProc;
    procedure DayDetailBandBeforePrint(Sender: TObject);
    procedure FooterBandBeforePrint(Sender: TObject);
    procedure FooterBandAfterPrint(Sender: TObject);
    procedure MasterAfterScroll(DataSet: TDataSet);
    procedure MasterManyAfterScroll(DataSet: TDataSet);
  private
    function SingleTimesheet: Boolean;
    procedure SetLabelVisibility;
    procedure ClearTotals;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
  end;

implementation

uses
  ReportEngineDMu, OdMiscUtils, OdDBUtils, ppTypes, QMConst, OdAdoUtils;

{$R *.dfm}

{ TTimesheetDetailDM }

procedure TTimesheetDetailNewDM.Configure(QueryFields: TStrings);
var
  SortMode: string;
  ManagerID: Integer;
  EmpStatus: Integer;
  HierOption: Integer;
  EndDate: TDateTime;
  RecsAffected : oleVariant;
begin
  inherited;
  if SingleTimesheet then begin
    with Data do begin
      Parameters.ParamByName('@EmpID').Value := GetInteger('emp_id');
      Parameters.ParamByName('@EndDate').Value := GetDateTime('end_date');
      Open;
    end;
    //QMANTWO-533 EB
    Master.Recordset := Data.Recordset;
    EmpTotals.Recordset := Data.Recordset.NextRecordset(RecsAffected);

    DS.Dataset := Master;
    TitleBand.Visible := False;
  end else begin
    ManagerID := GetInteger('emp_id');
    HierOption := SafeGetInteger('HierarchyDepth', HierarchyOptionFullDepth);
    EmpStatus := SafeGetInteger('EmployeeStatus', StatusActive);
    EndDate := GetDateTime('end_date');
    with ManySheets do begin
      Parameters.ParamByName('@ManagerID').Value := ManagerID;
      Parameters.ParamByName('@EndDate').Value := GetDateTime('end_date');
      Parameters.ParamByName('@LevelLimit').Value := HierOption;
      Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
      DS.DataSet := MasterMany As TADODataSet;
      Open;
      SortMode := SafeGetString('sort', 'short_name');
      // options are emp_number, last_name, short_name
    end;
     //QMANTWO-533 EB

    MasterMany.Recordset := ManySheets.Recordset;
    Dummy.Recordset := ManySheets.Recordset.NextRecordset(RecsAffected);
    EmpTotals.Recordset := ManySheets.Recordset.NextRecordset(RecsAffected);

    DS.Dataset := MasterMany;
//    MasterMany.IndexFieldNames := SortMode + ':A;emp_id:A;display_work_date:A';
    // Throw away the daily totals, not needed here.
    ShowEmployeeName(ManagerLabel, ManagerID);
    ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
    ShowHierarchyOption(HierOptionLabel, HierOption);
    ShowDateText(WeekEndingDateLabel, 'Week Ending:', EndDate);
    ShowText(SortLabel, 'Sort Mode:', StringReplace(SortMode, '_', ' ', [rfReplaceAll]));
    TitleBand.Visible := True;
  end;
  FooterBand.PrintOnFirstPage := not TitleBand.Visible;
end;

procedure TTimesheetDetailNewDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
begin
  if SingleTimesheet then
    SaveDelimToStream(Data, Str, Delimiter, True)
  else begin
    MasterMany.First;
    SaveDelimToStream(ManySheets, Str, Delimiter, True)
  end;
end;

function TTimesheetDetailNewDM.SingleTimesheet: Boolean;
begin
  Result := GetInteger('one_emp') = 1;
end;

procedure TTimesheetDetailNewDM.DayDetailBandBeforePrint(Sender: TObject);
begin
  inherited;
  SetLabelVisibility;
end;

procedure TTimesheetDetailNewDM.SetLabelVisibility;
var
  I: Integer;
  DBText: TppDBText;
  LabelComponentName: string;
  LabelComponent: TppLabel;
  ShowLabel: Boolean;
begin
  for I := 0 to ComponentCount - 1 do begin
    if (Components[I].Tag = 1) and (Components[I] is TppDBText) then begin
      ShowLabel := False;

      DBText := TppDBText(Components[I]);

      if VarIsNumeric(DBText.FieldValue) and (DBText.FieldValue > 0) then
        ShowLabel := True;

      if VarIsStr(DBText.FieldValue) and (DBText.FieldValue <> '') then
        ShowLabel := True;

      if VarIsFloat(DBText.FieldValue) and (DBText.FieldValue > 0) then
        ShowLabel := True;

      if (VarType(DBText.FieldValue) = varDate) and (DBText.FieldValue > 0) then
        ShowLabel := True;

      LabelComponentName := 'l' + Copy(DBText.Name, 2, 99);
      LabelComponent := (Self.FindComponent(LabelComponentName) as TppLabel);
      if LabelComponent <> nil then
        LabelComponent.Visible := ShowLabel;
    end;
  end;
end;

procedure TTimesheetDetailNewDM.FooterBandBeforePrint(Sender: TObject);
begin
  SetLabelVisibility;
end;

procedure TTimesheetDetailNewDM.ClearTotals;
begin
  // Manually clear totals per-page, RB does not have page total reset built in
  reg_hours_total.Clear;
  ot_hours_total.Clear;
  dt_hours_total.Clear;
  callout_hours_total.Clear;
  vac_hours_total.Clear;
  jury_hours_total.Clear;
  hol_hours_total.Clear;
  leave_hours_total.Clear;
  pto_hours_total.Clear;
  br_hours_total.Clear;
  mileage_total.Clear;
end;

procedure TTimesheetDetailNewDM.FooterBandAfterPrint(Sender: TObject);
begin
  inherited;
  ClearTotals;
end;

procedure TTimesheetDetailNewDM.MasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  EmpTotals.Filter := 'emp_id=' + IntToStr(DataSet.FieldByName('emp_id').AsInteger);
end;

procedure TTimesheetDetailNewDM.MasterManyAfterScroll(DataSet: TDataSet);
begin
  inherited;
  EmpTotals.Filter := 'emp_id=' + IntToStr(DataSet.FieldByName('emp_id').AsInteger);
end;

initialization
  RegisterReport('TimesheetDetailNew', TTimesheetDetailNewDM);

end.

