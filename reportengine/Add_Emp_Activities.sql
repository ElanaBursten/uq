-- This script will load up the employee_activity table for an employee on a certain date
-- Change the @Emp_Id and the @TargetDate
-- No Details are entered. This is to run the Employee_Activity Report

DECLARE @Emp_Id int
  SET @Emp_Id = 10667 -- Emil Gasparian
  --SET @Emp_Id = 3505 -- Marcel Nwadibia
  --SET @Emp_Id = 3832 -- Frank Straub
  --SET @Emp_Id = 9223 -- Phil Torreele
  --SET @Emp_Id = 12561 -- Richie Meeder
  --SET @Emp_Id = 13863 -- Gavin Gobuyan

DECLARE @TargetDate datetime
  SET @TargetDate = '2013-12-08';

 -- It is not necessary to change these times
DECLARE @Time datetime
  SET @Time = @TargetDate +' 07:53:57.107';
  
DECLARE @Sync datetime
  SET @Sync = @TargetDate + ' 08:01:57.101';



--Select DATEADD(minute, 1, @Time12);

-----Windows Logon F
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WINON', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 13, @Time));
SET @Sync = (SELECT DATEADD(minute, 13, @Sync));

-----Work Start 1
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WSTAR', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 5, @Time));
SET @Sync = (SELECT DATEADD(minute, 5, @Sync));

-----View Ticket A
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKVUE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 8, @Time));
SET @Sync = (SELECT DATEADD(minute, 8, @Sync));

-----Add Note M
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'ADDNT', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----New Ticket O
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKNEW', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 11, @Time));
SET @Sync = (SELECT DATEADD(minute, 11, @Sync));

-----Find Ticket V
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKFND', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 10, @Time));
SET @Sync = (SELECT DATEADD(minute, 10, @Sync));

-----View Ticket A
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKVUE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 8, @Time));
SET @Sync = (SELECT DATEADD(minute, 8, @Sync));

-----Add Attachment Z
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'ATADD', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Sync R
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'SYNC', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 13, @Time));
SET @Sync = (SELECT DATEADD(minute, 13, @Sync));

-----Move Ticket
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKMOV', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Locate Status L
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'LOCST', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 6, @Time));
SET @Sync = (SELECT DATEADD(minute, 6, @Sync));


-----Work Stop
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WSTOP', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 27, @Sync));


-----Sleep C
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'SLEEP', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 25, @Time));


-----Wake D
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WAKE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 2, @Time));
SET @Sync = (SELECT DATEADD(minute, 2, @Sync));

-----Work Start
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WSTAR', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Route Ticket
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKRTE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 2, @Time));
SET @Sync = (SELECT DATEADD(minute, 2, @Sync));
---------------------------------
---------------------------------
-----Move Ticket
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKMOV', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 2, @Time));
SET @Sync = (SELECT DATEADD(minute, 2, @Sync));

-----Route Ticket
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKRTE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 2, @Time));
SET @Sync = (SELECT DATEADD(minute, 2, @Sync));

-----Locate Status L
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'LOCST', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 5, @Time));
SET @Sync = (SELECT DATEADD(minute, 5, @Sync));

-----Run Report Q
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'RUNRE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 4, @Time));
SET @Sync = (SELECT DATEADD(minute, 4, @Sync));

-----Run Report Q
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'RUNRE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 5, @Time));
SET @Sync = (SELECT DATEADD(minute, 5, @Sync));

-----Run Report Q
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'RUNRE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 3, @Time));
SET @Sync = (SELECT DATEADD(minute, 3, @Sync));

-----New Ticket O
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKNEW', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 11, @Time));
SET @Sync = (SELECT DATEADD(minute, 11, @Sync));

-----Windows LogOff
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WINOFF', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 45, @Time));
SET @Sync = (SELECT DATEADD(minute, 45, @Sync));

-----Windows LogOn
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WINON', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 45, @Time));
SET @Sync = (SELECT DATEADD(minute, 45, @Sync));

-----Sync R
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'SYNC', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 13, @Time));
SET @Sync = (SELECT DATEADD(minute, 13, @Sync));

-----Find Ticket V
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKFND', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 10, @Time));
SET @Sync = (SELECT DATEADD(minute, 10, @Sync));

-----View Ticket A
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'TKVUE', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 8, @Time));
SET @Sync = (SELECT DATEADD(minute, 8, @Sync));

-----Add Note M
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'ADDNT', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Add Attachment Z
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'ATADD', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Work Stop
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'WSTOP', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));

-----Power Off
INSERT INTO employee_activity(emp_id, activity_date, activity_type, details, insert_date, extra_details)
                       VALUES(@Emp_Id, @Time, 'PWROFF', NULL, @Sync , 'SQL Script');
SET @Time = (SElECT DATEADD(minute, 9, @Time));
SET @Sync = (SELECT DATEADD(minute, 9, @Sync));


