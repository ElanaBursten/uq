unit PayrollSummaryEX;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppModule, daDataModule, ppBands, ppClass,
  ppCtrls, ppReport, ppStrtch, ppSubRpt, ppVar, ppPrnabl, ppCache, ppProd,
  ppDB, ppComm, ppRelatv, ppDBPipe, ppDesignLayer, ppParameter, Data.Win.ADODB,
  myChkBox;

type
  TPayrollSummaryExDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppLabel15: TppLabel;
    ppLabel23: TppLabel;
    ppLabel03: TppLabel;
    ppLabel31: TppLabel;
    ppLabel26: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText03: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppLabel4: TppLabel;
    ppLine1: TppLine;
    short_namePPText: TppDBText;
    EmpNumberText: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppLabel01: TppLabel;
    ppDBText01: TppDBText;
    ppLabel02: TppLabel;
    ppDBText02: TppDBText;
    ppDBCalcT01: TppDBCalc;
    ppDBCalcT02: TppDBCalc;
    ppLabel19: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalc03: TppDBCalc;
    ppDBCalc01: TppDBCalc;
    ppDBCalc02: TppDBCalc;
    ppLabel20: TppLabel;
    ppDBText23: TppDBText;
    ppLabel21: TppLabel;
    ppDBText18: TppDBText;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Q1: TADODataSet;
    Q3: TADODataSet;
    Q3DS: TDataSource;
    DS: TDataSource;
    Data: TADOStoredProc;
    Q2: TADODataSet;
    ppLabel2: TppLabel;
    ppDBText2: TppDBText;
    ppDBCalc9: TppDBCalc;
    procedure TotalsPrint(Sender: TObject);
  private
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu, OdMiscUtils, OdRbHierarchy, OdRbUtils, RbCustomTextFit;

{$R *.dfm}

{ TPayrollSummaryDM }

procedure TPayrollSummaryExDM.Configure(QueryFields: TStrings);
var
  Region: string;
  RecsAffected : oleVariant;
  BeginDate, EndDate: TDateTime;
  i,MyCount: integer;
  FieldName: string;
begin
  inherited;
  BeginDate := GetDateTime('begin_date');
  EndDate := GetDateTime('end_date');
  Data.Parameters.ParamByName('@begin_date').value := BeginDate;
  Data.Parameters.ParamByName('@end_date').value := EndDate;
  Region := GetString('region');
  if Region = 'ALL' then begin
    Data.Parameters.ParamByName('@region').value := Null;
    ppGroupFooterBand1.Visible := True;
  end else begin
    Data.Parameters.ParamByName('@region').value := Region;
    ppGroupFooterBand1.Visible := False;
 end;

  Data.Open;

  //QMANTWO-733 EB
  Q1.Recordset := Data.Recordset;
  Q2.Recordset := Data.Recordset.NextRecordset(RecsAffected); //Skip the secode result (export)
  Q3.Recordset := Data.Recordset.NextRecordset(RecsAffected);
  MyCount := Q3.FieldCount;
  for i := 0 to Q3.FieldCount-1 do
    FieldName := Q3.Fields.Fields[i].FieldName;
  AlignRBComponentsByTag(Self);
end;

procedure TPayrollSummaryExDM.TotalsPrint(Sender: TObject);
begin
  ScaleRBTextToFit(Sender as TppDBText, 8);
end;

initialization
  RegisterReport('PayrollSummaryEx', TPayrollSummaryExDM);

end.
