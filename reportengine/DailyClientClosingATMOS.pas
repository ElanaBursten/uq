unit DailyClientClosingATMOS;

interface

uses
  SysUtils, Classes, ppCtrls, ppPrnabl, ppClass, ppDB, ppBands, ppCache,
  DB, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, ppVar,
  BaseReport, ppModule, daDataModule, ppStrtch, ppSubRpt, ppMemo, ppDesignLayer,
  ppParameter,
  Data.Win.ADODB;

type
  TDailyClientClosingATMOSDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Pipe: TppDBPipeline;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel4: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    ppSystemVariable3: TppSystemVariable;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLabel5: TppLabel;
    ppLabel3: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText5: TppDBText;
    ppDBText3: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ClientCodeLabel: TppLabel;
    TicketsStatusedFrom: TppLabel;
    TicketsTransmittedFrom: TppLabel;
    CallCenterLabel: TppLabel;
    LocatesLabel: TppLabel;
    ShowStatusHistoryLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
    procedure CreateDelimitedStream(Str: TStream; Delimiter: string); override;
  end;

implementation

uses ReportEngineDMu, QMConst, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TDailyClientClosingATMOSDM }

procedure TDailyClientClosingATMOSDM.Configure(QueryFields: TStrings);
var
  SyncFromDateParam: TDateTime;
  SyncToDateParam: TDateTime;
  TransmitFromDateParam: TDateTime;
  TransmitToDateParam: TDateTime;
  ClientCodeList: string;
  CallCenter: string;
  LocateClosed: Integer;
  ShowStatusHistory: Boolean;
begin
  inherited;

  SyncFromDateParam := GetDateTime('sync_from_date');
  SyncToDateParam := SafeGetDateTime('sync_to_date');

  if (SyncFromDateParam = 0) and (SyncToDateParam = 0) then begin
    SyncFromDateParam := EncodeDate(1900, 1, 1);
    SyncToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsStatusedFrom, '');
  end
  else if (SyncFromDateParam > 0) and (SyncToDateParam > 0) then begin
    ShowText(TicketsStatusedFrom, DateToStr(SyncFromDateParam) + ' - ' + DateToStr(SyncToDateParam - 1))
  end
  else begin
    SyncToDateParam := SyncFromDateParam + 1;
    ShowText(TicketsStatusedFrom, DateToStr(SyncFromDateParam));
  end;

  TransmitFromDateParam := SafeGetDateTime('xmit_from_date');
  TransmitToDateParam := SafeGetDateTime('xmit_to_date');
  if (TransmitFromDateParam > 0) and (TransmitToDateParam > 0) then
    ShowText(TicketsTransmittedFrom, DateToStr(TransmitFromDateParam) + ' - ' + DateToStr(TransmitToDateParam - 1))
  else begin
    TransmitFromDateParam := EncodeDate(1900, 1, 1);
    TransmitToDateParam := EncodeDate(2199, 1, 1);
    ShowText(TicketsTransmittedFrom, '');
  end;

  ClientCodeList := GetString('client_code_list');
  CallCenter := GetString('call_center');
  LocateClosed := SafeGetInteger('locate_closed', Ord(ocClosed)); // Old default = Open only
  ShowStatusHistory := SafeGetString('show_status_history', '0') = '1'; // 0 - False, 1 - True

  SP.Parameters.ParamByName('@SyncDateFrom').value := SyncFromDateParam;
  SP.Parameters.ParamByName('@SyncDateTo').value := SyncToDateParam;
  SP.Parameters.ParamByName('@XmitDateFrom').value := TransmitFromDateParam;
  SP.Parameters.ParamByName('@XmitDateTo').value := TransmitToDateParam;
  SP.Parameters.ParamByName('@ClientCodeList').value := ClientCodeList;
  SP.Parameters.ParamByName('@CallCenter').value := CallCenter;
  SP.Parameters.ParamByName('@LocateClosed').value := LocateClosed;
  SP.Parameters.ParamByName('@ShowStatusHistory').value := ShowStatusHistory;
  SP.Open;

  ShowText(CallCenterLabel, CallCenter);
  ShowText(ClientCodeLabel, ClientCodeList);

  case LocateClosed of
    Ord(ocAll): ShowText(LocatesLabel, 'All');
    Ord(ocOpen): ShowText(LocatesLabel, 'Open');
    Ord(ocClosed): ShowText(LocatesLabel, 'Closed');
  end;

  ShowText(ShowStatusHistoryLabel, BooleanToStringYesNo(ShowStatusHistory));
end;

procedure TDailyClientClosingATMOSDM.CreateDelimitedStream(Str: TStream; Delimiter: string);
begin
  SaveDelimToStream(SP, Str, Delimiter, True);
end;

initialization
  RegisterReport('DailyClientClosingATMOS', TDailyClientClosingATMOSDM);

end.


