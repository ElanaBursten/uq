unit SyncSummary;
//qm-972 sr added perseqour and Meme version columns
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, ppModule, daDataModule, ppBands, ppClass, ppVar,
  ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TSyncSummaryDM = class(TBaseReportDM)
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    short_namePPText: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppFooterBand1: TppFooterBand;
    Pipe: TppDBPipeline;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppReportFooterShape1: TppShape;
    ppLabel13: TppLabel;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable4: TppSystemVariable;
    DateRangeLabel: TppLabel;
    ppLabel8: TppLabel;
    LastLocalSyncTextField: TppDBText;
    ManagerLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    MultiPCUserLabel: TppLabel;
    ppSummaryBand1: TppSummaryBand;
    MultiPCUserSummaryLabel: TppLabel;
    AddinVersion: TppDBText;
    AddinVersionLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
    VersionLabel: TppLabel;
    PersequorVer: TppDBText;
    PersequorLabel: TppLabel;
    MemeVersion: TppDBText;
    MemeLabel: TppLabel;
    ppLine1: TppLine;
    procedure ppDetailBand1BeforeGenerate(Sender: TObject);
  public
    procedure Configure(QueryFields: TStrings); override;
  private
    procedure HighlightTimeMismatch;
    procedure HandleSortOrder(SP: TADOStoredProc);
    procedure HighlightMultiComputerUser;
  end;

implementation

{$R *.dfm}

uses ReportEngineDMu;

{ TSyncSummaryDM }

procedure TSyncSummaryDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  EmpStatus := GetInteger('EmployeeStatus');
  with SP do begin
    Parameters.ParamByName('@DateStart').Value      := GetDateTime('DateStart');
    Parameters.ParamByName('@DateEnd').Value        := GetDateTime('DateEnd');
    Parameters.ParamByName('@ManagerID').Value      := ManagerID;
    Parameters.ParamByName('@EmployeeStatus').Value := EmpStatus;
    Open;
  end;
  ShowDateRange(DateRangeLabel, 'DateStart', 'DateEnd');
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
  MultiPCUserSummaryLabel.Visible := False;

  HandleSortOrder(SP);
end;

procedure TSyncSummaryDM.HandleSortOrder(SP: TADOStoredProc);
var
  Sort: string;
begin
  Sort := SafeGetString('Sort', '');
//  if Sort='Short Name' then        // do not work with ADO
//    SP.IndexName := 'h_namepath'
//  else if Sort='Emp Number' then
//    SP.IndexName := 'h_numpath'
//  else if Sort='Last Name' then
//    SP.IndexName := 'h_lastpath'
//  else
//    SP.IndexName := 'h_namepath';
end;

procedure TSyncSummaryDM.HighlightTimeMismatch;
var
  LocalTime, SyncTime, Gap: TDateTime;
const
  SixHours = 0.25;  // days
begin
  LocalTime := SP.FieldByName('LastLocalSync').AsDateTime;
  SyncTime := SP.FieldByName('LastSync').AsDateTime;
  Gap := Abs(LocalTime - SyncTime);
  if Gap >= SixHours then
    LastLocalSyncTextField.Font.Style := [fsBold]
  else
    LastLocalSyncTextField.Font.Style := [];
end;

procedure TSyncSummaryDM.HighlightMultiComputerUser;
begin
  if SP.FieldByName('multi_pc_user').AsBoolean then begin
    MultiPCUserLabel.Text := '*';
    MultiPCUserSummaryLabel.Visible := True;
  end else
    MultiPCUserLabel.Text := ' ';
end;

procedure TSyncSummaryDM.ppDetailBand1BeforeGenerate(Sender: TObject);
var
  Level: Integer;
begin
  inherited;
  Level := SP.FieldByName('h_report_level').AsInteger;
  short_namePPText.Left := 0.0208 + (Level-1) * 0.2;

  HighlightTimeMismatch;
  HighlightMultiComputerUser;
end;

initialization
  RegisterReport('SyncSummary', TSyncSummaryDM);

end.

