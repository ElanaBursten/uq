unit BreakException;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseReport, DB, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd,
  ppClass, ppReport, ppVar, ppCtrls, ppPrnabl, ppBands, ppCache, ppDesignLayer,
  ppParameter, Data.Win.ADODB;

type
  TBreakExceptionDM = class(TBaseReportDM)
    DS: TDataSource;
    Report: TppReport;
    Pipe: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppShape2: TppShape;
    ppLabel15: TppLabel;
    DateLabel: TppLabel;
    EmployeeStatusLabel: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppLabel1: TppLabel;
    Company: TppDBText;
    PC: TppDBText;
    EmpName: TppDBText;
    EmpNumber: TppDBText;
    SupervisorName: TppDBText;
    WorkDate: TppDBText;
    BreakStart: TppDBText;
    BreakStop: TppDBText;
    BreakLength: TppDBText;
    CompanyLabel: TppLabel;
    PCLabel: TppLabel;
    EmpNameLabel: TppLabel;
    EmpNumberLabel: TppLabel;
    SupervisorLabel: TppLabel;
    WorkDateLabel: TppLabel;
    BreakStartLabel: TppLabel;
    BreakEndLabel: TppLabel;
    BreakLenLabel: TppLabel;
    ppLine1: TppLine;
    ManagerLabel: TppLabel;
    ManagerName: TppDBText;
    CenterManagerLabel: TppLabel;
    ExcludedTimeRuleLabel: TppLabel;
    ExcludedTimeRulesText: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    Data: TADOStoredProc;
    TimeRules: TADODataSet;
    Master: TADODataSet;
    procedure BreakTimeGetText(Sender: TObject; var Text: string);
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

var
  BreakExceptionDM: TBreakExceptionDM;

implementation

uses OdMiscUtils, OdDbUtils, StrUtils, odAdoUtils, ReportEngineDMu;

{$R *.dfm}

{ TBreakExceptionDM }

procedure TBreakExceptionDM.BreakTimeGetText(Sender: TObject; var Text: string);
begin
  if Text = '' then
    Text := '-';
end;

procedure TBreakExceptionDM.Configure(QueryFields: TStrings);
var
  ManagerID: Integer;
  EmpStatus: Integer;
  ExcludedTimeRuleIDs: string;
  ExcludedTimeRules: string;
  RecsAffected : oleVariant;
begin
  inherited;
  ManagerID := GetInteger('ManagerID');
  EmpStatus := GetInteger('EmployeeStatus');
  ExcludedTimeRuleIDs := SafeGetString('ExcludeTimeRuleIDs', '');
  if not IsEmpty(ExcludedTimeRuleIDs) then
    if not ValidateIntegerCommaSeparatedList(ExcludedTimeRuleIDs) then
      raise Exception.Create('The parameter ExcludeTimeRuleIDs=' +
        ExcludedTimeRuleIDs + ' is not a valid id list.');

  with Data do begin
    Parameters.ParamByName('@ManagerId').value := ManagerID;
    Parameters.ParamByName('@EmployeeStatus').value := EmpStatus;
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Parameters.ParamByName('@ExcludeTimeRuleIDs').value := ExcludedTimeRuleIDs;
    Open;
  end;


  Master.Recordset    :=  Data.Recordset;
  TimeRules.Recordset :=  Data.Recordset.NextRecordset(RecsAffected);

  ExcludedTimeRules := GetDataSetFieldPlainString(TimeRules, 'code');
  ExcludedTimeRulesText.Caption := IfThen(IsEmpty(ExcludedTimeRules), 'None', ExcludedTimeRules);

  ShowDateRange(DateLabel);
  ShowEmployeeName(ManagerLabel, ManagerID);
  ShowEmployeeStatus(EmployeeStatusLabel, EmpStatus);
end;

initialization
  RegisterReport('PayrollExceptionBreaks', TBreakExceptionDM);

end.
