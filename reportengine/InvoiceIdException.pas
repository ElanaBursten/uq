unit InvoiceIdException;

interface

uses
  SysUtils, Classes, BaseReport, ppModule, daDataModule, ppBands, ppClass,
  ppVar, ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter, 
  Data.Win.ADODB;

type
  TInvoiceIDExceptionDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppLabel1: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    DateRangeLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation
  uses  ReportEngineDMu;
{$R *.dfm}

{ TInvoiceIDExceptionDM }
procedure TInvoiceIDExceptionDM.Configure(QueryFields: TStrings);
begin
  inherited;

  with SP do begin
    Parameters.ParamByName('@StartDate').value := GetDateTime('StartDate');
    Parameters.ParamByName('@EndDate').value := GetDateTime('EndDate');
    Open;
  end;

  ShowDateRange(DateRangeLabel);
end;

initialization
  RegisterReport('InvoiceIDException', TInvoiceIDExceptionDM);
end.
