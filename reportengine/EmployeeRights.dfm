inherited EmployeeRightsDM: TEmployeeRightsDM
  OldCreateOrder = True
  Width = 349
  object SPDS: TDataSource
    AutoEdit = False
    DataSet = Master
    Left = 120
    Top = 24
  end
  object Pipe: TppDBPipeline
    DataSource = SPDS
    OpenDataSource = False
    UserName = 'Pipe'
    Left = 192
    Top = 24
  end
  object Report: TppReport
    AutoStop = False
    DataPipeline = Pipe
    NoDataBehaviors = [ndMessageOnPage, ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = '%COMPANY% Asset Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Screen'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 12700
    PrinterSetup.mmMarginLeft = 12700
    PrinterSetup.mmMarginRight = 12700
    PrinterSetup.mmMarginTop = 12700
    PrinterSetup.mmPaperHeight = 215900
    PrinterSetup.mmPaperWidth = 279401
    PrinterSetup.PaperSize = 1
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    BeforePrint = ReportBeforePrint
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    Left = 280
    Top = 24
    Version = '18.03'
    mmColumnWidth = 0
    DataPipelineName = 'Pipe'
    object ppTitleBand3: TppTitleBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object HeaderBand: TppHeaderBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 29369
      mmPrintPosition = 0
      object ppShape8: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'Shape8'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 29369
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel24: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'Label24'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Employee Permissions Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 2117
        mmTop = 2381
        mmWidth = 249503
        BandType = 0
        LayerName = Foreground
      end
      object ParamsSubreport: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'ParamsSubreport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'ParamsPipe'
        mmHeight = 19050
        mmLeft = 0
        mmTop = 10054
        mmWidth = 254001
        BandType = 0
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ParamsChildReport: TppChildReport
          AutoStop = False
          Columns = 2
          DataPipeline = ParamsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Timesheet Audit History'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.Orientation = poLandscape
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 10160
          PrinterSetup.mmMarginLeft = 10160
          PrinterSetup.mmMarginRight = 10160
          PrinterSetup.mmMarginTop = 10160
          PrinterSetup.mmPaperHeight = 215900
          PrinterSetup.mmPaperWidth = 279401
          PrinterSetup.PaperSize = 1
          Version = '18.03'
          mmColumnWidth = 129540
          DataPipelineName = 'ParamsPipe'
          object ppColumnHeaderBand2: TppColumnHeaderBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            ColumnTraversal = ctLeftToRight
            mmBottomOffset = 0
            mmHeight = 4498
            mmPrintPosition = 0
            object ppDBText34: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText34'
              HyperlinkEnabled = False
              DataField = 'param_name'
              DataPipeline = ParamsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ParamsPipe'
              mmHeight = 3704
              mmLeft = 10583
              mmTop = 794
              mmWidth = 31221
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText35: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText35'
              HyperlinkEnabled = False
              DataField = 'param_value'
              DataPipeline = ParamsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsBold]
              Transparent = True
              DataPipelineName = 'ParamsPipe'
              mmHeight = 3704
              mmLeft = 43127
              mmTop = 794
              mmWidth = 48683
              BandType = 4
              LayerName = Foreground1
            end
          end
          object ppColumnFooterBand2: TppColumnFooterBand
            AlignToBottom = True
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDesignLayers2: TppDesignLayers
            object ppDesignLayer2: TppDesignLayer
              UserName = 'Foreground1'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object DetailBand: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5292
      mmPrintPosition = 0
      object ProfitCenter: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'ProfitCenter'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_pc_code'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 23813
        mmTop = 794
        mmWidth = 9260
        BandType = 4
        LayerName = Foreground
      end
      object EmpNumber: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpNumber'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_number'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 34131
        mmTop = 794
        mmWidth = 11906
        BandType = 4
        LayerName = Foreground
      end
      object EmpShortName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpShortName'
        HyperlinkEnabled = False
        DataField = 'emp_short_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 47625
        mmTop = 794
        mmWidth = 33338
        BandType = 4
        LayerName = Foreground
      end
      object EmpFirstName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpFirstName'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_first_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 81756
        mmTop = 794
        mmWidth = 16404
        BandType = 4
        LayerName = Foreground
      end
      object EmpLastName: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpLastName'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_last_name'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 99484
        mmTop = 794
        mmWidth = 16140
        BandType = 4
        LayerName = Foreground
      end
      object EmpType: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpType'
        HyperlinkEnabled = False
        DataField = 'emp_type'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 116946
        mmTop = 794
        mmWidth = 27252
        BandType = 4
        LayerName = Foreground
      end
      object RightAllowed: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'RightAllowed'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'allowed'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 156634
        mmTop = 794
        mmWidth = 10054
        BandType = 4
        LayerName = Foreground
      end
      object EmpStatus: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpStatus'
        HyperlinkEnabled = False
        DataField = 'emp_active'
        DataPipeline = Pipe
        DisplayFormat = 'Y;N'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 145521
        mmTop = 794
        mmWidth = 8731
        BandType = 4
        LayerName = Foreground
      end
      object RightDescription: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'RightDescription'
        HyperlinkEnabled = False
        DataField = 'right_description'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 167482
        mmTop = 794
        mmWidth = 49742
        BandType = 4
        LayerName = Foreground
      end
      object EmpGroup: TppDBText
        DesignLayer = ppDesignLayer1
        UserName = 'EmpGroup'
        HyperlinkEnabled = False
        AutoSize = True
        DataField = 'emp_group'
        DataPipeline = Pipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'Pipe'
        mmHeight = 3704
        mmLeft = 217753
        mmTop = 794
        mmWidth = 14288
        BandType = 4
        LayerName = Foreground
      end
    end
    object FooterBand: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 10319
      mmPrintPosition = 0
      object ppReportFooterShape1: TppShape
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportFooterShape1'
        Brush.Color = 14737632
        ParentHeight = True
        ParentWidth = True
        mmHeight = 10319
        mmLeft = 0
        mmTop = 0
        mmWidth = 254001
        BandType = 8
        LayerName = Foreground
      end
      object LocatingCompanyLabel: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'LocatingCompanyLabel'
        HyperlinkEnabled = False
        Caption = '%COMPANY%'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 7938
        mmLeft = 1852
        mmTop = 1588
        mmWidth = 55033
        BandType = 8
        LayerName = Foreground
      end
      object ppReportCopyright: TppLabel
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportCopyright'
        HyperlinkEnabled = False
        AutoSize = False
        Caption = 'Employee Permissions Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 3969
        mmLeft = 86254
        mmTop = 3175
        mmWidth = 81227
        BandType = 8
        LayerName = Foreground
      end
      object ppReportDateTime: TppCalc
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportDateTime'
        HyperlinkEnabled = False
        Alignment = taRightJustify
        CalcType = ctDateTime
        CustomType = dtDateTime
        DisplayFormat = 'mmm d, yyyy, h:nn am/pm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 218546
        mmTop = 1588
        mmWidth = 33073
        BandType = 8
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        DesignLayer = ppDesignLayer1
        UserName = 'ppReportPageNo1'
        HyperlinkEnabled = False
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 229394
        mmTop = 6085
        mmWidth = 22225
        BandType = 8
        LayerName = Foreground
      end
    end
    object SummaryBand: TppSummaryBand
      Background.Brush.Style = bsClear
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5027
      mmPrintPosition = 0
      object LimitRightsSubReport: TppSubReport
        DesignLayer = ppDesignLayer1
        UserName = 'LimitRightsSubReport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'RightsPipe'
        mmHeight = 4233
        mmLeft = 0
        mmTop = 794
        mmWidth = 254001
        BandType = 7
        LayerName = Foreground
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object LimitRightsChildReport: TppChildReport
          AutoStop = False
          Columns = 4
          DataPipeline = RightsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = '%COMPANY% Productivity Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Screen'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Version = '18.03'
          mmColumnWidth = 50800
          DataPipelineName = 'RightsPipe'
          object ppTitleBand2: TppTitleBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 6879
            mmPrintPosition = 0
            object ppLabel10: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'Label5'
              HyperlinkEnabled = False
              Caption = 'This report is limited to the following rights:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = [fsUnderline]
              Transparent = True
              mmHeight = 3704
              mmLeft = 1058
              mmTop = 2381
              mmWidth = 54240
              BandType = 1
              LayerName = Foreground2
            end
          end
          object ppColumnHeaderBand1: TppColumnHeaderBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand4: TppDetailBand
            Background1.Brush.Style = bsClear
            Background2.Brush.Style = bsClear
            ColumnTraversal = ctLeftToRight
            mmBottomOffset = 0
            mmHeight = 3969
            mmPrintPosition = 0
            object ppDBText21: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBText21'
              HyperlinkEnabled = False
              AutoSize = True
              DataField = 'right_code'
              DataPipeline = RightsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'RightsPipe'
              mmHeight = 3704
              mmLeft = 1058
              mmTop = 265
              mmWidth = 13494
              BandType = 4
              LayerName = Foreground2
            end
          end
          object ppColumnFooterBand1: TppColumnFooterBand
            AlignToBottom = True
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppSummaryBand2: TppSummaryBand
            Background.Brush.Style = bsClear
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDesignLayers3: TppDesignLayers
            object ppDesignLayer3: TppDesignLayer
              UserName = 'Foreground2'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object CompanyGroup: TppGroup
      BreakName = 'company_name'
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      NewPage = True
      StartOnOddPage = False
      UserName = 'CompanyGroup'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = ''
      NewFile = False
      object ppGroupHeaderBand4: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 13229
        mmPrintPosition = 0
        object SummaryColumnHeadersRegion: TppRegion
          DesignLayer = ppDesignLayer1
          UserName = 'SummaryColumnHeadersRegion1'
          Brush.Style = bsClear
          ParentHeight = True
          ParentWidth = True
          Pen.Style = psClear
          Transparent = True
          mmHeight = 13229
          mmLeft = 0
          mmTop = 0
          mmWidth = 254001
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          object SumamryPCLabel: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'PCLabel1'
            HyperlinkEnabled = False
            Caption = 'Profit Center'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            WordWrap = True
            mmHeight = 7938
            mmLeft = 23813
            mmTop = 1323
            mmWidth = 9525
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object CompanyName: TppDBText
            DesignLayer = ppDesignLayer1
            UserName = 'CompanyName'
            HyperlinkEnabled = False
            AutoSize = True
            DataField = 'emp_company'
            DataPipeline = Pipe
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = [fsBold]
            ParentDataPipeline = False
            Transparent = True
            DataPipelineName = 'Pipe'
            mmHeight = 3704
            mmLeft = 1852
            mmTop = 5292
            mmWidth = 21431
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel1: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label1'
            HyperlinkEnabled = False
            Caption = 'Emp Number'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            WordWrap = True
            mmHeight = 7938
            mmLeft = 34131
            mmTop = 1323
            mmWidth = 10848
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel2: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label2'
            HyperlinkEnabled = False
            Caption = 'Short Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 47361
            mmTop = 5556
            mmWidth = 14817
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel3: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label3'
            HyperlinkEnabled = False
            Caption = 'First Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 81756
            mmTop = 5556
            mmWidth = 13494
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel4: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label4'
            HyperlinkEnabled = False
            Caption = 'Last Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 99484
            mmTop = 5556
            mmWidth = 13494
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel5: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label5'
            HyperlinkEnabled = False
            Caption = 'Job Title'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 116681
            mmTop = 5556
            mmWidth = 10319
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel6: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label6'
            HyperlinkEnabled = False
            Caption = 'Employee Active'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            TextAlignment = taCentered
            Transparent = True
            WordWrap = True
            mmHeight = 7408
            mmLeft = 143669
            mmTop = 1852
            mmWidth = 12171
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel7: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label7'
            HyperlinkEnabled = False
            Caption = 'Allowed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            TextAlignment = taCentered
            Transparent = True
            mmHeight = 3704
            mmLeft = 156369
            mmTop = 5556
            mmWidth = 10583
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel8: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label8'
            HyperlinkEnabled = False
            Caption = 'Right Description'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 167482
            mmTop = 5556
            mmWidth = 21431
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLabel9: TppLabel
            DesignLayer = ppDesignLayer1
            UserName = 'Label9'
            HyperlinkEnabled = False
            Caption = 'Group'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 8
            Font.Style = []
            Transparent = True
            mmHeight = 3704
            mmLeft = 217753
            mmTop = 5556
            mmWidth = 7938
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
          object ppLine1: TppLine
            DesignLayer = ppDesignLayer1
            UserName = 'Line1'
            Weight = 0.750000000000000000
            mmHeight = 1852
            mmLeft = 1852
            mmTop = 10583
            mmWidth = 248444
            BandType = 3
            GroupNo = 0
            LayerName = Foreground
          end
        end
      end
      object ppGroupFooterBand4: TppGroupFooterBand
        Background.Brush.Style = bsClear
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object RightsDS: TDataSource
    AutoEdit = False
    DataSet = Rights
    Left = 120
    Top = 88
  end
  object RightsPipe: TppDBPipeline
    DataSource = RightsDS
    OpenDataSource = False
    UserName = 'RightsPipe'
    Left = 192
    Top = 88
  end
  object ReportParamsDS: TDataSource
    AutoEdit = False
    DataSet = ReportParams
    Left = 120
    Top = 144
  end
  object ParamsPipe: TppDBPipeline
    DataSource = ReportParamsDS
    OpenDataSource = False
    UserName = 'ParamsPipe'
    Left = 192
    Top = 144
  end
  object SP: TADOStoredProc
    Connection = ReportEngineDM.Conn
    ProcedureName = 'RPT_employee_rights'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ManagerId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmployeeStatus'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EmployeeId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RightStatus'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RightIdList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2000
        Value = Null
      end
      item
        Name = '@StartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@OrigGrantStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@OrigGrantEndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 40
    Top = 24
  end
  object Master: TADODataSet
    Parameters = <>
    Left = 256
    Top = 88
  end
  object Rights: TADODataSet
    Parameters = <>
    Left = 40
    Top = 88
  end
  object ReportParams: TADODataSet
    Parameters = <>
    Left = 40
    Top = 144
  end
end
