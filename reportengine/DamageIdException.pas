unit DamageIdException;

interface

uses
  SysUtils, Classes, BaseReport, ppModule, daDataModule, ppBands, ppClass,
  ppVar, ppCtrls, ppPrnabl, ppCache, ppProd, ppReport, DB, ppComm,
  ppRelatv, ppDB, ppDBPipe, ppDesignLayer, ppParameter,
  Data.Win.ADODB;

type
  TDamageIDExceptionDM = class(TBaseReportDM)
    Pipe: TppDBPipeline;
    SPDS: TDataSource;
    Report: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppReportHeaderShape1: TppShape;
    ppReportHeaderLabel: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppReportFooterShape1: TppShape;
    ppReportCopyright: TppLabel;
    ppReportDateTime: TppCalc;
    ppSystemVariable1: TppSystemVariable;
    ppLabel13: TppLabel;
    ppDBText2: TppDBText;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    DamageIdRangeLabel: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    SP: TADOStoredProc;
  public
    procedure Configure(QueryFields: TStrings); override;
  end;

implementation

uses ReportEngineDMu;

{$R *.dfm}

{ TDamageIDExceptionDM }
procedure TDamageIDExceptionDM.Configure(QueryFields: TStrings);
var
  StartDamageID, EndDamageID: Integer;
begin
  inherited;

  StartDamageID := SafeGetInteger('StartDamageID');
  EndDamageID := SafeGetInteger('EndDamageID');

  with SP do begin
    Parameters.ParamByName('@StartDamageID').value := StartDamageID;
    Parameters.ParamByName('@EndDamageID').value  := EndDamageID;
    Open;
  end;
  ShowText(DamageIdRangeLabel, IntToStr(StartDamageID) + ' to ' + IntToStr(EndDamageID));
end;

initialization
  RegisterReport('DamageIDException', TDamageIDExceptionDM);
end.

