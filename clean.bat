@echo This should clean out things we don't want to check in.
@echo Generated files, backup files, DCUs, EXEs, etc. should NEVER
@echo be checked in; only the source files that lead to these things
@echo should be checked in.

del /s .#*
del /s *.dcu
del /s *.~*
del /s *.*~
del /s *.$$$
del /s *.map
del /s *.pyc
del /s *.bak
del /s *.ddp
