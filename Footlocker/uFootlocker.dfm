object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 
    '                                                                ' +
    '         Footlocker OHM Report Processor'
  ClientHeight = 524
  ClientWidth = 1132
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblWeekEnding: TLabel
    Left = 25
    Top = 398
    Width = 82
    Height = 16
    Caption = 'Week Ending'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblCustomer: TLabel
    Left = 25
    Top = 438
    Width = 62
    Height = 16
    Caption = 'Customer'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnSave: TButton
    Left = 299
    Top = 358
    Width = 112
    Height = 25
    Action = Action1
    Caption = 'Save to Footlocker'
    TabOrder = 0
    OnClick = btnSaveClick
  end
  object btnOpen: TButton
    Left = 25
    Top = 358
    Width = 112
    Height = 25
    Caption = 'Open File'
    TabOrder = 1
    OnClick = btnOpenClick
  end
  object btnClose: TButton
    Left = 169
    Top = 358
    Width = 112
    Height = 25
    Action = Action1
    Caption = 'Close File'
    TabOrder = 2
    OnClick = btnCloseClick
  end
  object WeekEnding: TDateTimePicker
    Left = 25
    Top = 416
    Width = 153
    Height = 21
    Date = 0.868523194447334400
    Time = 0.868523194447334400
    TabOrder = 3
  end
  object CustomerLookup: TDBLookupComboBox
    Left = 25
    Top = 456
    Width = 237
    Height = 21
    DropDownRows = 10
    KeyField = 'customer_number'
    ListField = 'customer_name'
    ListSource = dsCustomer
    TabOrder = 4
  end
  object Memo1: TMemo
    Left = 432
    Top = 360
    Width = 673
    Height = 113
    TabOrder = 5
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 483
    Width = 1132
    Height = 41
    Align = alBottom
    TabOrder = 6
    object StatusBar1: TStatusBar
      Left = 1
      Top = 16
      Width = 1130
      Height = 24
      Panels = <
        item
          Text = 'Connected to'
          Width = 80
        end
        item
          Width = 200
        end
        item
          Text = ' Version'
          Width = 50
        end
        item
          Width = 50
        end>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1132
    Height = 337
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 7
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 1130
      Height = 335
      Align = alClient
      DataSource = dsWeeklyPolygon
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Name'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Employee ID'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'JO'
          Width = 25
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Deliverable Number'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Deliverable Name '
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total Hours'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Training Hours'
          Width = 84
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Collection Hours'
          ReadOnly = True
          Width = 83
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL FOOTAGE'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAIN'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERVICE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAIN CHPO FOOTAGE'
          Width = 118
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERVICE CHPO FOOTAGE'
          Width = 131
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ProjectNumber'
          Title.Caption = 'Project Number'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CostPerFoot'
          Title.Caption = 'Cost Per Foot'
          Width = 74
          Visible = True
        end>
    end
  end
  object qryWeeklyPolygon: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from [Weekly Polygon Hourly Report (J$]')
    Left = 967
    Top = 81
  end
  object insBillingOhmWeekly: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'Employee_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'JO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Deliverable_Number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Deliverable_Name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'Total_Hours'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Training_Hours'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Collection_Hours'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Total_Footage'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Main'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Service'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Main_CHPO_Footage'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Service_CHPO_Footage'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ProjectNumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'Customer_Number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CostPerFoot'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'WeekEndingDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO billing_ohm_weekly'
      
        '( Name,Employee_ID,JO,Deliverable_Number,Deliverable_Name,Total_' +
        'Hours,'
      '  Training_Hours,Collection_Hours,Total_Footage,Main,Service, '
      
        '  Main_CHPO_Footage, Service_CHPO_Footage,ProjectNumber, Custome' +
        'r_Number,'
      ' CostPerFoot, WeekEndingDate)'
      'VALUES'
      
        '(:Name,:Employee_ID,:JO,:Deliverable_Number,:Deliverable_Name,:T' +
        'otal_Hours,'
      
        '  :Training_Hours,:Collection_Hours,:Total_Footage,:Main,:Servic' +
        'e, '
      
        '  :Main_CHPO_Footage,:Service_CHPO_Footage,:ProjectNumber,:Custo' +
        'mer_Number,'
      '  :CostPerFoot, :WeekEndingDate)')
    Left = 284
    Top = 402
  end
  object FileADOConnection: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 647
    Top = 192
  end
  object dsWeeklyPolygon: TDataSource
    AutoEdit = False
    DataSet = qryWeeklyPolygon
    Left = 967
    Top = 128
  end
  object OpenDialog1: TOpenDialog
    Left = 956
    Top = 242
  end
  object ActionList1: TActionList
    Left = 972
    Top = 298
    object Action1: TAction
      Caption = 'Action1'
      OnUpdate = Action1Update
    end
  end
  object ADOConn: TADOConnection
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 872
    Top = 304
  end
  object dsCustomer: TDataSource
    DataSet = qryCustomer
    Left = 343
    Top = 448
  end
  object qryCustomer: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select customer_number, customer_name '
      'from customer '
      'where active = 1')
    Left = 276
    Top = 442
  end
end
