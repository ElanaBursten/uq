program FootLocker;

{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R '..\QMIcon.res'}

uses
  Vcl.Forms,
  uFootlocker in 'uFootlocker.pas' {frmMain};

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
