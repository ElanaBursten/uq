unit uFootlocker;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtDlgs, System.Actions, Vcl.ActnList,
  Vcl.WinXPickers, Vcl.ComCtrls, Vcl.DBCtrls, Vcl.ExtCtrls;

type
  EInsertFailed = class(Exception);
  TfrmMain = class(TForm)
    qryWeeklyPolygon: TADOQuery;
    insBillingOhmWeekly: TADOQuery;
    FileADOConnection: TADOConnection;
    dsWeeklyPolygon: TDataSource;
    btnSave: TButton;
    btnOpen: TButton;
    OpenDialog1: TOpenDialog;
    btnClose: TButton;
    ActionList1: TActionList;
    Action1: TAction;
    ADOConn: TADOConnection;
    WeekEnding: TDateTimePicker;
    lblWeekEnding: TLabel;
    dsCustomer: TDataSource;
    qryCustomer: TADOQuery;
    CustomerLookup: TDBLookupComboBox;
    lblCustomer: TLabel;
    Memo1: TMemo;
    pnlBottom: TPanel;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    function OpenFile: String;
    procedure SaveFile;
    procedure Action1Update(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOpenClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    appVersion : string;
    function connectDB: boolean;
    function EnDeCrypt(const Value: String): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses Inifiles, System.StrUtils, odMiscUtils;
{$R *.dfm}

function TfrmMain.OpenFile: String;
var
 Connstr: String;
begin
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  Result := '';
  openDialog1.Filter := 'Excel files|*.xlsx';
  if OpenDialog1.Execute then
  begin
     qryWeeklyPolygon.Close;
     FileADOConnection.Close;
     Connstr := Connstr + OpenDialog1.FileName;
     try
       FileADOConnection.ConnectionString := Connstr;
       FileADOConnection.Open;
       qryWeeklyPolygon.Open;
       qryCustomer.Open;
       CustomerLookup.KeyValue := null;
     except
       On E:Exception do
         Result := E.Message;
     end;
     BtnSave.Enabled := True;
  end;
end;

procedure TfrmMain.SaveFile;
var
  RowsAffected: Integer;
  InsRows: Integer;
  ProjNbr, CostPerFoot: String;
begin
  try
    btnSave.Enabled := false;
    ADOConn.Open;
    InsRows := 0;
    qryWeeklyPolygon.First;
    while not qryWeeklyPolygon.EOF do begin
      if Trim(DBGrid1.Fields[0].AsString) = 'Totals- DO NOT INPUT DATA' then
      begin
        ProjNbr := Trim(DBGrid1.Fields[13].AsString);
        CostPerFoot := Trim(DBGrid1.Fields[14].AsString);
        qryWeeklyPolygon.Next;
        continue;
      end;
      if (Trim(DBGrid1.Fields[0].AsString) = 'Name') or
         (Trim(DBGrid1.Fields[0].AsString) = '') then
      begin
        qryWeeklyPolygon.Next;
        continue
      end;
With insBillingOhmWeekly.Parameters do
      begin
        ParamByName('Name').Value := Trim(DBGrid1.Fields[0].AsString);
        ParamByName('Employee_ID').Value := Trim(DBGrid1.Fields[1].AsString);
        ParamByName('JO').Value := DBGrid1.Fields[2].AsInteger;
        ParamByName('Deliverable_Number').Value := Trim(DBGrid1.Fields[3].AsString);
        ParamByName('Deliverable_Name').Value := Trim(DBGrid1.Fields[4].AsString);
        ParamByName('Total_Hours').Value := DBGrid1.Fields[5].AsInteger;
        ParamByName('Training_Hours').Value := DBGrid1.Fields[6].AsInteger;
        ParamByName('Collection_Hours').Value := DBGrid1.Fields[7].AsInteger;
        ParamByName('Total_Footage').Value := Trim(Copy(DBGrid1.Fields[8].AsString,1,10));//QMANTWO-805
        ParamByName('Main').Value := Trim(Copy(DBGrid1.Fields[9].AsString,1,10)); //QMANTWO-805
        ParamByName('Service').Value := Trim(Copy(DBGrid1.Fields[10].AsString,1,10)); //QMANTWO-805
        ParamByName('Main_CHPO_Footage').Value := Trim(Copy(DBGrid1.Fields[11].AsString,1,10)); //QMANTWO-805
        ParamByName('Service_CHPO_Footage').Value := Trim(Copy(DBGrid1.Fields[12].AsString,1,10)); //QMANTWO-805
        ParamByName('ProjectNumber').Value := ProjNbr;
        ParamByName('CostPerFoot').Value := Copy(CostPerFoot,1,4); //QMANTWO-805
        ParamByName('WeekEndingDate').Value := WeekEnding.Date;
        ParamByName('Customer_Number').Value := CustomerLookup.KeyValue;
      end;

      RowsAffected := insBillingOhmWeekly.ExecSQL;
      if RowsAffected <> 1 then
        raise EInsertFailed.Create('An error occurred while inserting to the footlocker table');
      Inc(InsRows);
      qryWeeklyPolygon.Next;
    end;
    ShowMessage(InttoStr(InsRows) + ' inserted rows');
  except
    raise;
  end;
  ADOConn.Close;
  btnSave.Enabled := False;
end;

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  qryWeeklyPolygon.Close;
  CustomerLookup.KeyValue := null;
  qryCustomer.Close;
end;

procedure TfrmMain.btnOpenClick(Sender: TObject);
var
  Rslt: String;
begin
  Rslt := OpenFile;
  if Rslt <> '' then
    ShowMessage('connection to file error: ' + Rslt);
end;

procedure TfrmMain.btnSaveClick(Sender: TObject);
begin
  if qryWeeklyPolygon.Active then SaveFile;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryWeeklyPolygon.Close;
  qryCustomer.Close;
  FileADOConnection.Close;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  VersionString:string;
begin
  If not(connectDB) then showmessage('Cannot connect to database');
  Weekending.Date := Date;
  if GetFileVersionNumberString(Application.ExeName, VersionString) then
  StatusBar1.Panels[3].Text := VersionString;
end;

function TfrmMain.connectDB: boolean;
var
  myPath:string;
  connStr, LogConnStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  BillingIni: TIniFile;
const
  DECRYPT = 'DEC_';
begin
  Result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');

    aServer := BillingIni.ReadString('Database', 'Server', '');
    aDatabase := BillingIni.ReadString('Database', 'DB', 'QM');
    ausername := BillingIni.ReadString('Database', 'UserName', '');
    apassword := BillingIni.ReadString('Database', 'Password', '');
    LogConnStr := 'Provider=SQLNCLI11.1;;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;
    memo1.Lines.Add(LogConnStr);
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
    Result := ADOConn.Connected;
    if result then
    begin
      StatusBar1.Panels[1].Text :=  aServer;
      memo1.Lines.Add('Connected to database');
    end
    else
      memo1.Lines.Add('Could not connect to DB');
  finally
    freeandnil(BillingIni);
  end;
end;

function TfrmMain.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

procedure TfrmMain.Action1Update(Sender: TObject);
begin
  Action1.Enabled := qryWeeklyPolygon.Active;
end;

end.


