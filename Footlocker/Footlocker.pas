unit Footlocker;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtDlgs, System.Actions, Vcl.ActnList;


type
  EInsertFailed = class(Exception);
  TfrmMain = class(TForm)
    ADOOpen: TADOQuery;
    ADOSave: TADOQuery;
    FileADOConnection: TADOConnection;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    btnSave: TButton;
    btnOpen: TButton;
    OpenDialog1: TOpenDialog;
    btnClose: TButton;
    ActionList1: TActionList;
    Action1: TAction;
    ADOConn: TADOConnection;
    function OpenFile: String;
    procedure SaveFile;
    procedure Action1Update(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOpenClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function connectDB: boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses Inifiles;
{$R *.dfm}

function TfrmMain.OpenFile: String;
var
 Connstr: String;
begin
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
  Result := '';
  openDialog1.Filter := 'Excel files|*.xlsx';
  if OpenDialog1.Execute then
  begin
     ADOOpen.Close;
     FileADOConnection.Close;
     Connstr := Connstr + OpenDialog1.FileName;
     try
       FileADOConnection.ConnectionString := Connstr;
       FileADOConnection.Open;
       ADOOpen.Open;
     except
       On E:Exception do
         Result := E.Message;
     end;
  end;
  BtnSave.Enabled := True;
end;

procedure TfrmMain.SaveFile;
var
  RowsAffected: Integer;
  InsRows: Integer;
begin
  try
    btnSave.Enabled := false;
    ADOConn.Open;
    InsRows := 0;
    while not ADOOpen.EOF do begin
      if (Trim(DBGrid1.Fields[0].AsString) = 'Name') or
         (Trim(DBGrid1.Fields[0].AsString) = '') then
      begin
        ADOOpen.Next;
        continue
      end;
      With ADOSave.Parameters do
      begin
        ParamByName('Name').Value := Trim(DBGrid1.Fields[0].AsString);
        ParamByName('JO').Value := DBGrid1.Fields[1].AsInteger;
        ParamByName('Deliverable_Number').Value := Trim(DBGrid1.Fields[2].AsString);
        ParamByName('Deliverable_Name').Value := Trim(DBGrid1.Fields[3].AsString);
        ParamByName('Total_Hours').Value := DBGrid1.Fields[4].AsInteger;
        ParamByName('Training_Hours').Value := DBGrid1.Fields[5].AsInteger;
        ParamByName('Collection_Hours').Value := DBGrid1.Fields[6].AsInteger;
        ParamByName('Total_Footage').Value := Trim(DBGrid1.Fields[7].AsString);
        ParamByName('Main').Value := Trim(DBGrid1.Fields[8].AsString);
        ParamByName('Service').Value := Trim(DBGrid1.Fields[9].AsString);
        ParamByName('Service_CHPO_Footage').Value := Trim(DBGrid1.Fields[10].AsString);
      end;
      RowsAffected := ADOSave.ExecSQL;
      if RowsAffected <> 1 then
        raise EInsertFailed.Create('An error occurred while inserting to the footlocker table');
      Inc(InsRows);
      ADOOpen.Next;
    end;
    ShowMessage(InttoStr(InsRows) + ' inserted rows');
  except
    raise;
  end;
  ADOConn.Close;
  btnSave.Enabled := False;
end;

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  ADOOpen.Close;
end;

procedure TfrmMain.btnOpenClick(Sender: TObject);
var
  Rslt: String;
begin
  Rslt := OpenFile;
  if Rslt <> '' then
    ShowMessage('connection to file error: ' + Rslt);
end;

procedure TfrmMain.btnSaveClick(Sender: TObject);
begin
  if ADOOpen.Active then SaveFile;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ADOOpen.Close;
  FileADOConnection.Close;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  If <> connectDB then showmessage('Cannot connect to database');
end;


function TfrmMain.connectDB: boolean;
var
  myPath:string;
  connStr: String;
  aDatabase: String;
  aServer: String;
  ausername: string;
  apassword: string;
  BillingIni: TIniFile;
begin
  Result := false;
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');
    aServer := BillingIni.ReadString('Database', 'Server', '');
    aDatabase := BillingIni.ReadString('Database', 'DB', 'QM');
    ausername := BillingIni.ReadString('Database', 'UserName', '');
    apassword := BillingIni.ReadString('Database', 'Password', '');
    connStr := 'Provider=SQLOLEDB.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
      ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer;

    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
    Result := ADOConn.Connected;

  finally
    freeandnil(BillingIni);
  end;
end;




procedure TfrmMain.Action1Update(Sender: TObject);
begin
  Action1.Enabled := ADOOpen.Active;
end;

end.


