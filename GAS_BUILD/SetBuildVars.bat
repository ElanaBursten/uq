@echo 0ff
Pause>nul
rem These environment vars should be injected by Jenkins before building QM.
rem SetBuildVars.bat should only be used when running BuildQM.bat outside of Jenkins.

rem Customize these settings as needed to match your build environment

rem Location of the project source
rem set WORKSPACE=D:\Projects\uq-qm
set WORKSPACE=C:\TRUNK1

rem Set vars needed by the Delphi compiler
call "%ProgramFiles(x86)%\Embarcadero\RAD Studio\10.0\bin\rsvars.bat"
set BDS2007=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0
set BDSCOMMONDIR2007=%PUBLIC%\Documents\RAD Studio\5.0
rem MRH 20150811 NEED TO CHANGE TO THIS = set BDSCOMMONDIR2007=%PUBLIC%\Public Documents\RAD Studio\5.0

rem Locations of 3rd party components
rem SLH Changed this one set DBISAM2007DIR=%Program Files (x86)%\DBISAM 4 VCL-STD\RAD Studio 2007 (Delphi)\code

set DBISAM2007DIR=C:\Delphi Tools\DBISAM 4 VCL-STD\RAD Studio 2007 (Delphi)\code
set DBISAMDIR=C:\Delphi Tools\DBISAM\RAD Studio XE3 (Delphi Win32)\code

set JCL2007DIRS=C:\Delphi Tools\jcl-2.4.1.4571\source\include;C:\Delphi Tools\jcl-2.4.1.4571\lib\d11
set JCLDIRS=C:\Delphi Tools\jcl-2.4.1.4571\source\include;C:\Delphi Tools\jcl-2.4.1.4571\lib\d17\win32

rem set DEVEXDIR=D:\Projects\Components\DevExQM.VCL
set DEVEXDIR=C:\Delphi Tools\DevEx1224
rem MRH 20150812 THE ABOVE WAS USED BY OASIS DIGITAL TO MANAGE THE UPGRADE OF THE DEVEXRPRESS COMPONENTS (APPARENTLY) = WE WILL NEED TO CREATE OUR OWN UTILITY

set RBDIR=%ProgramFiles(x86)%\Embarcadero\RAD Studio\10.0\RBuilder\Lib\Win32
set RB2007DIR=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0\RBuilder\Lib

set FIREDACDIRS=C:\Program Files (x86)\Embarcadero\FireDAC\Dcu\D17\win32

rem Location of any extra unit search paths
rem set OTHERDIRS=

rem Conditional defines
rem set OTHERDEFINES= 



