@echo off

set BUILD_NUMBER=%1
set GIT_COMMIT=%2

if "%TARGET%"=="" set TARGET=Rebuild
if "%CONFIG%"=="" set CONFIG=Release

set WORKSPACE=C:\TRUNK1

call "%ProgramFiles(x86)%\Embarcadero\RAD Studio\10.0\bin\rsvars.bat"
set BDS2007=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0
set BDSCOMMONDIR2007=%PUBLIC%\Documents\RAD Studio\5.0

set DBISAM2007DIR=C:\Delphi Tools\DBISAM 4 VCL-STD\RAD Studio 2007 (Delphi)\code
set DBISAMDIR=C:\Delphi Tools\DBISAM\RAD Studio XE3 (Delphi Win32)\code

set JCL2007DIRS=C:\Delphi Tools\jcl-2.4.1.4571\source\include;C:\Delphi Tools\jcl-2.4.1.4571\lib\d11
set JCLDIRS=C:\Delphi Tools\jcl-2.4.1.4571\source\include;C:\Delphi Tools\jcl-2.4.1.4571\lib\d17\win32

set DEVEXDIR=C:\Delphi Tools\DevEx1224

set RBDIR=%ProgramFiles(x86)%\Embarcadero\RAD Studio\10.0\RBuilder\Lib\Win32
set RB2007DIR=%ProgramFiles(x86)%\CodeGear\RAD Studio\5.0\RBuilder\Lib

set FIREDACDIRS=C:\Program Files (x86)\Embarcadero\FireDAC\Dcu\D17\win32

rem Uncomment SKIP_NEWDB to skip recreating TestDB
rem set SKIP_NEWDB=True
rem Uncomment SKIP_PYTHON to skip the post build Python tests
rem set SKIP_PYTHON=True

cd %WORKSPACE%
REM Get the Version information from version.ini
for /f "tokens=1,2 delims==" %%a in (version.ini) do (
  if %%a==VERSION_MAJOR set VERSION_MAJOR=%%b
  if %%a==VERSION_MINOR set VERSION_MINOR=%%b
  if %%a==VERSION_RELEASE set VERSION_RELEASE=%%b
)
set VERSION_PREFIX=%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_RELEASE%.

REM GIT_COMMIT, GIT_BRANCH, and BUILD_NUMBER variables are set by Jenkins. Default for dev purposes if missing.

if "%GIT_BRANCH%"=="" set GIT_BRANCH=origin/master
set BUILD_BRANCH=
if NOT "%GIT_BRANCH%"=="origin/master" set BUILD_BRANCH=-%GIT_BRANCH:origin/=%
; pause
set GIT_COMMIT_SHORT=%GIT_COMMIT:~0,7%
set VERSION=%VERSION_PREFIX%%BUILD_NUMBER%
set PRODUCT_VERSION=%VERSION% %GIT_COMMIT_SHORT%
set BUILD_NAME=QM-%VERSION%-%GIT_COMMIT_SHORT%%BUILD_BRANCH%

;echo %TARGET% %BUILD_NAME% under %WORKSPACE% using %CONFIG% configuration

REM Build the Delphi XE3 projects
setlocal
rem set OTHERDIRS=%BDSCOMMONDIR%\Dcp;%OTHERDIRS%
set THIRDPARTYPATH=%INDYDIRS%;%JCLDIRS%;%RBDIR%;%FIREDACDIRS%;%DBISAMDIR%
set DCC_Quiet=True
set DCC_MapFile=3
set DCC_UnitSearchPath=%WORKSPACE%\client;%WORKSPACE%\server;%WORKSPACE%\reportengine;%WORKSPACE%\common;%WORKSPACE%\thirdparty;%WORKSPACE%\thirdparty\GPSTools;%WORKSPACE%\thirdparty\PerlRegEx;%WORKSPACE%\thirdparty\zlib;%WORKSPACE%\thirdparty\zlib\zlib-1.1.4;%WORKSPACE%\thirdparty\BetterADO;%WORKSPACE%\thirdparty\SuperObjectv1.2.4;%THIRDPARTYPATH%
rem set DCC_Define=%OTHERDEFINES%
set DCC_IntegerOverflowCheck=True
set DCC_RangeChecking=True
set DCC_GenerateStackFrames=True
set DCC_WriteableConstants=True
set LibraryPath=%BDS%\lib\win32\release;%BDS%\Imports;%BDSCOMMONDIR%\Dcp;%DCC_UnitSearchPath%
rem Use this for more verbose output > MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM.log;Verbosity=Detailed /t:%TARGET% /p:Configuration=%CONFIG% QMXE3.msbuild
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM.log;Verbosity=Normal /t:%TARGET% /v:n /p:Configuration=%CONFIG%;DelphiLibraryPath="%LibraryPath%" QMXE3.msbuild || goto :Failed
;echo here i am
endlocal

REM Build the Delphi 2007 projects
setlocal
set BDS=%BDS2007%
set BDSCOMMONDIR=%BDSCOMMONDIR2007%
set INDYDIRS=%WORKSPACE%\thirdparty\Indy10\Protocols;%WORKSPACE%\thirdparty\Indy10\System;%WORKSPACE%\thirdparty\Indy10\Core
rem set OTHERDIRS=%BDSCOMMONDIR%\Dcp;%OTHERDIRS%
set THIRDPARTYPATH=%INDYDIRS%;%DBISAM2007DIR%;%JCL2007DIRS%;%RB2007DIR%;%DEVEXDIR%
set DCC_Quiet=True
set DCC_MapFile=3
set DCC_UnitSearchPath=%WORKSPACE%\client;%WORKSPACE%\server;%WORKSPACE%\reportengine;%WORKSPACE%\common;%WORKSPACE%\thirdparty;%WORKSPACE%\thirdparty\RemObjects;%WORKSPACE%\thirdparty\GPSTools;%WORKSPACE%\thirdparty\PerlRegEx;%WORKSPACE%\thirdparty\zlib;%WORKSPACE%\thirdparty\zlib\zlib-1.1.4;%WORKSPACE%\thirdparty\BetterADO;%WORKSPACE%\thirdparty\SuperObjectv1.2.4;%THIRDPARTYPATH%
rem set DCC_Define=%OTHERDEFINES%
set DCC_IntegerOverflowCheck=True
set DCC_RangeChecking=True
set DCC_GenerateStackFrames=True
set DCC_WriteableConstants=True
set PATH=%FrameworkDir%;%PATH%
set LibraryPath=%BDS%\lib;%BDS%\Imports;%BDSCOMMONDIR%\Dcp;%DCC_UnitSearchPath%

REM print the vars so we can track down problems:
set

rem Use this for more verbose output > MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM.log;Verbosity=Detailed /t:%TARGET% /p:Configuration=%CONFIG% QM.msbuild
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild /Nologo /fl /flp:LogFile=%WORKSPACE%\BuildQM.log;Verbosity=Normal;Append /t:%TARGET% /v:n /p:Configuration=%CONFIG%;Win32LibraryPath="%LibraryPath%" QM.msbuild || goto :Failed
endlocal

exit

:Failed
;PAUSE
exit 1


