@echo off
Pause>nul
set APPDIR=%cd%

for %%i in ("%~dp0..") do set "PARENTDIR=%%~fi"
FOR %%a IN ("%PARENTDIR:~0,-1%") DO SET GPARENTDIR=%%~dpa
set INIDIR=%GPARENTDIR%\MYSETTINGS_BACKUP
rem SET OTHERDIR=%PARENTDIR%\OTHERAPPS


mkdir %INIDIR% > NUL
XCOPY *.ini %INIDIR%\ /s /e /y

FOR /D /R %%X IN (.git) DO RD /S /Q "%%X" 2>NUL 
FOR /D /R %%X IN (build) DO RD /S /Q "%%X" 2>NUL  
FOR /D /R %%X IN (__history) DO RD /S /Q "%%X" 2>NUL 
FOR /D /R %%X IN (Win32) DO RD /S /Q "%%X" 2>NUL 
FOR /D /R %%X IN (data*) DO RD /S /Q "%%X" 2>NUL
FOR /D /R %%X IN (dataold) DO RD /S /Q "%%X" 2>NUL
FOR /D /R %%X IN (ticket_parse_2) DO RD /S /Q "%%X" 2>NUL


del /s .#* 2>NUL
del /s *.dcu 2>NUL
del /s *.~* 2>NUL
del /s *.*~ 2>NUL
del /s *.$$$ 2>NUL
del /s *.map 2>NUL
del /s *.pyc 2>NUL
del /s *.bak 2>NUL
del /s *.ddp 2>NUL
del /s *.dsk 2>NUL
del /s *.dof 2>NUL
del /s *.drc 2>NUL
del /s *.zip 2>NUL
del /s *.bpl 2>NUL
del /s *.log 2>NUL


del /s .gitattributes 2>NUL
del /s .gitignore 2>NUL

XCOPY *.ini C:\DATA\DEVELOPMENT\WORKING\QManager\MYSETTINGS_BACKUP /s /e /y

del /s QMServer.ini 2>NUL
del /s QMServer*.ini 2>NUL
del /s QManagerAdmin*.ini 2>NUL
del /s ReportEngineCGI.ini 2>NUL
del /s QManager*.ini 2>NUL
del /s TableHelper*.ini 2>NUL
del /s TestConfig*.ini 2>NUL
del /s addins*.ini 2>NUL

del /s HISTORY.txt 2>NUL
del /s adding_call_center.txt 2>NUL
del /s high_profile_grids.txt 2>NUL
del /s stored_procs.txt 2>NUL
del /s tables.txt 2>NUL
del /s test_db_setup.txt 2>NUL
del /s ultra_files.txt 2>NUL
del /s update_call_centers.txt 2>NUL
del /s log.txt 2>NUL

FORFILES /S /M *.exe /D "11/1/2015" /C "CMD /C DEL *.exe" 2>NUL
FORFILES /S ?M *.dll /D "11/1/2015" /C "CMD /C DEL *.dll" 2>NUL

rem mkdir %OTHERDIR% > NUL

rem move %APPDIR%\QtrMinEditor %OTHERDIR%\QtrMinEditor
rem move %APPDIR%\TransParser %OTHERDIR%\TransParser
rem move %APPDIR%\XE10Billing %OTHERDIR%\XE10Billing

exit
