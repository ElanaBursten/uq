program GasBuilder;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmGasBuilder},
  SysUtils,
  GlobalSU in 'GlobalSU.pas';

{$R *.res}
var
  MyInstanceName: string;
begin
  Application.Initialize;
    MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmGasBuilder, frmGasBuilder);
  Application.Run;
  end
  else
    Application.Terminate;
end.
