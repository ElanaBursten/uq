unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, Winapi.ActiveX, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, ShellAPI, TlHelp32,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, IdIntercept,
  IdLogBase, IdLogEvent, IdMessage, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdBaseComponent, IdComponent, IdIOHandler, IdIOHandlerSocket, ComObj,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, Vcl.Buttons, Xml.xmldom, Xml.XMLIntf,
  Xml.Win.msxmldom, Xml.XMLDoc, MSXML;
type
  TbuildState = (bsIdle, bsBuild, bsPost, bsDeploy, bsMail);

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String;
    Status: String;
    ExcepMsg: String;
    DataStream: String;
  end; // TLogResults

type
  TfrmGasBuilder = class(TForm)
    edtBuildNo: TEdit;
    edtHash: TEdit;
    btnBuild: TButton;
    Label1: TLabel;
    Label2: TLabel;
    StatusBar1: TStatusBar;
    ProgressBar1: TProgressBar;
    btnPost: TButton;
    btnDeploy: TButton;
    StatusBar2: TStatusBar;
    cbDoItAll: TCheckBox;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdSMTP1: TIdSMTP;
    IdMessage1: TIdMessage;
    IdLogEvent1: TIdLogEvent;
    btnMail: TButton;
    btnAbort: TBitBtn;
    Memo1: TMemo;
    Label3: TLabel;
    XMLDocument2: TXMLDocument;
    procedure btnBuildClick(Sender: TObject);
    procedure btnAbortClick(Sender: TObject);
    procedure btnPostClick(Sender: TObject);
    procedure btnDeployClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtBuildNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure btnMailClick(Sender: TObject);
  private
       fAbort: boolean;
      aDir: string;
     LogResult: TLogResults;
     flogDir: wideString;
    procedure clearLogRecord;
    function GetAppVersionStr: string;
    function CopyGasBuildFiles: boolean;
    function RunBuild: boolean;
    function CopyDir(const fromDir, toDir: string): boolean;
    function DelDir(dir: string): boolean;
    function MoveDir(const fromDir, toDir: string): boolean;
    function getBuildNo: string;
    function getGitHash: string;
    procedure CleaningSource;
    function getGitHashFromMaster: string;
    procedure BuildTheBitch;
    procedure PostToTrunkBuild;
    procedure DeployToUtiliquest;
    procedure SetbuildState(const Value: TBuildState);
    function getCommitDescript: string;
    { Private declarations }
  public
    { Public declarations }
    property buildState : TBuildState write SetbuildState;
    property BuildNo: string read getBuildNo;
    property GitHash: string read getGitHash;
    procedure WriteLog(LogResult: TLogResults);
  end;
function processExists(exeFileName: string): Boolean;
function Deletefiles2(APath: string): integer;
procedure Deletefiles(APath, AFileSpec: string);
function StartProcess(ExeName: string; CmdLineArgs: string = '';
         ShowWindow: boolean = true; WaitForFinish: boolean = false): integer;
CONST
  TRUNK = 'C:\TRUNK1\';
  GAS_BUILD = 'C:\TRUNK1\GAS_BUILD\';
  DCU_SEATTLE =  'C:\TRUNK1\DCU_Seattle\';
  DCU2007 =  'C:\TRUNK1\DCU2007\';
  TRUNK_BUILD = 'C:\DATA\DEVELOPMENT\WORKING\QManager\TRUNKBUILD\';
  FINAL_BUILD = 'C:\DATA\DEVELOPMENT\WORKING\QManager\build\';
  BUILD_TRUNK1 = TRUNK_BUILD + 'TRUNK1\';
  QMANAGER = 'C:\DATA\DEVELOPMENT\WORKING\QManager\';
//  MY_SETTINGS = QMANAGER+'MYSETTINGS_BACKUP\';
  BASE_NAME = 'QM-2.3.0.';

var
  frmGasBuilder: TfrmGasBuilder;

implementation

uses System.zip;
{$R *.dfm}

procedure TfrmGasBuilder.btnAbortClick(Sender: TObject);
begin
  fAbort := true;
end;

procedure TfrmGasBuilder.btnBuildClick(Sender: TObject);
begin
  if processExists('bds.exe') then
  begin
    showmessage('Please stop ALL instances of Delphi.');
    exit;
  end;

  Tthread.CreateAnonymousThread(
    procedure
    begin
      BuildTheBitch;
    end).Start;

 // if (cbDoItAll.Checked) then btnPostClick(Sender);
end;


procedure TfrmGasBuilder.BuildTheBitch;
const
//  OTHER = 'OTHERAPPS';
  BUILD_DIR = 'build\';
var
  buildZipFile: string;
  sourceBld, destBuild: string;
begin
  edtBuildNo.Enabled := false;
  edtHash.Enabled := false;
  StatusBar2.Panels[3].Text := '';
  StatusBar2.Panels[1].Text := TimeToStr(Now);
  fAbort := false;
  Deletefiles(QMANAGER, '*.zip');

//  DelDir(MY_SETTINGS);
  DelDir(FINAL_BUILD);
  DelDir(BUILD_TRUNK1);
//  DelDir(TRUNK_BUILD + OTHER);
  Deletefiles2(DCU_SEATTLE);
  Deletefiles2(DCU2007);
//{$I-}
//  MkDir(TRUNK_BUILD + OTHER);
//{$I+}
  // if CopyGasBuildFiles then
  if RunBuild then
    if CopyDir(TRUNK, TRUNK_BUILD) then
      MoveDir(TRUNK_BUILD + 'TRUNK1\BUILD\', QMANAGER)
    else
    begin
      showmessage('Failed to copy directory ' + TRUNK + ' to ' + TRUNK_BUILD);
      StatusBar1.SimpleText := 'Build Process Failed';
      exit;
    end;
  buildZipFile := BASE_NAME + BuildNo + '-' + GitHash + '.zip';
  ShellExecute(0, 'open', PChar('C:\Program Files (x86)\Inno Setup 6\ISCC.exe'), PChar('C:\Trunk1\admin\JustQManagerAdmin.iss'), nil, SW_HIDE) ;
//  ShellExecute(  "C:\Program Files (x86)\Inno Setup 6\ISCC.exe" "C:\Trunk1\admin\JustQManagerAdmin.iss"
  sourceBld := QMANAGER + BUILD_DIR + buildZipFile;
  destBuild := QMANAGER + buildZipFile;
  if copyfile(pChar(sourceBld), pChar(QMANAGER + buildZipFile), false) then
    DelDir(FINAL_BUILD)
  else
  begin
    showmessage('Failed to copy file ' + sourceBld + ' to ' + QMANAGER + buildZipFile);
    StatusBar1.SimpleText := 'Build Process Failed';
    exit;
  end;
  StatusBar2.Panels[3].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  StatusBar1.SimpleText := 'Finished Build Process';
  StatusBar1.Refresh;
  SetbuildState(bsPost);
end;

procedure TfrmGasBuilder.btnDeployClick(Sender: TObject);

var
  deployThread: Tthread;
begin
  deployThread := Tthread.CreateAnonymousThread(
    procedure
    begin
      DeployToUtiliquest;
    end);
  deployThread.Start;
end;

procedure TfrmGasBuilder.btnMailClick(Sender: TObject);
var
  J:integer;
  procedure ImportXML;
  var
    XMLDOMDocument  : IXMLDOMDocument;
    XMLDOMNode      : IXMLDOMNode;
    XMLDOMNodeList  : IXMLDOMNodeList;
    aXMLDOMNode     : IXMLDOMNode;
    sName, sAddress : string;
    xml : TStringList;
    i: Integer;
  begin
    xml := TStringList.Create;
    try
      xml.LoadFromFile('C:\TRUNK1\GAS_BUILD\Application\Getters.xml');

      XMLDOMDocument:=CoDOMDocument.Create;
      try
        XMLDOMDocument.loadXML(xml.Text);

        if (XMLDOMDocument.parseError.ErrorCode<>0) then
          raise Exception.CreateFmt('Error in Xml Data %s', [XMLDOMDocument.parseError]);

        XMLDOMNode := XMLDOMDocument.selectSingleNode('//Recipients');

        if XMLDOMNode<>nil then
        begin
          XMLDOMNodeList := XMLDOMNode.selectNodes('.//member');

          if XMLDOMNodeList<>nil then
          begin
            for i:=0 to XMLDOMNodeList.length-1 do
            begin
              aXMLDOMNode := XMLDOMNodeList.item[i];
                with IdMessage1.Recipients.Add do
                begin
                  sName     := aXMLDOMNode.selectSingleNode('Name').text;
                  sAddress  := aXMLDOMNode.selectSingleNode('Address').text;
                  Name      := sName;
                  Address   := sAddress;
                end;
            end;
          end;
        end;
      finally
        XMLDOMDocument:=nil;
      end;
    finally
      xml.Free;
    end;
  end;
begin
  IdMessage1.Recipients.Clear;
  ImportXML;
  IdMessage1.Subject :=  aDir;
  IdMessage1.Body.Clear;
  IdMessage1.Body.Add('<P>');
  IdMessage1.Body.Add('A new build has been posted at '+aDir);
  IdMessage1.Body.Add('</P>');
  IdMessage1.Body.Add('Please read any ReadMe and the Test Plan before testing');
  IdMessage1.Body.Add('<Pre>');
  for J := 0 to memo1.Lines.Count -1 do
  begin
    IdMessage1.Body.Add(memo1.Lines[j]);
  end;
  IdMessage1.Body.Add('</Pre>');
  IdMessage1.Body.Add('Thanks');
  IdMessage1.Body.Add('<P>');
  IdMessage1.Body.Add('the xqTrix Team');
  IdMessage1.Body.Add('</P>');
  IdSMTP1.Connect;
  IdSMTP1.Send(IdMessage1);
  IdSMTP1.Disconnect;
  StatusBar1.SimpleText := 'Interested parties have been informed';
end;

procedure TfrmGasBuilder.DeployToUtiliquest;
var

  BuildSource: string;
  BuildFiles: string;
begin
  aDir := 'P:\StevenHull\ver ' + BuildNo + ' for test';

  BuildSource := QMANAGER + BASE_NAME + BuildNo + '-' + GitHash + '-' +
    'SOURCE.zip';
  BuildFiles := QMANAGER + BASE_NAME + BuildNo + '-' + GitHash + '.zip';
  If ForceDirectories(aDir) then
  begin
    StatusBar1.SimpleText := 'Copying Build and source to ' + aDir;
    StatusBar2.Panels[3].Text := '';
    StatusBar2.Panels[1].Text := TimeToStr(Now);
    if copyfile(pChar(BuildSource), pChar(aDir + '\' + BASE_NAME + BuildNo + '-'
      + GitHash + '-' + 'SOURCE.zip'), false) then
      begin
        Deletefiles2(BuildSource);
        LogResult.LogType := ltInfo;
        LogResult.MethodName:='Deletefiles2(BuildSource)';
        WriteLog(LogResult);
      End;
    if copyfile(pChar(BuildFiles), pChar(aDir + '\' + BASE_NAME + BuildNo + '-'
      + GitHash + '.zip'), false) then
      begin
      Deletefiles2(BuildFiles);
        LogResult.LogType := ltInfo;
        LogResult.MethodName:='Deletefiles2(BuildFiles)';
        WriteLog(LogResult);
      End;
  end;
  StatusBar1.SimpleText := 'Build and Source have been deployed to: '+aDir;

  LogResult.LogType := ltInfo;
  LogResult.MethodName:='DeployToUtiliquest';
  LogResult.Status:='Dir: '+ aDir;
  WriteLog(LogResult);


  StatusBar2.Panels[3].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  SetbuildState(bsMail);
end;

procedure TfrmGasBuilder.edtBuildNoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  cnt : integer;
begin
  cnt := length(edtBuildNo.text);
  if cnt > 4 then
  SetbuildState(bsBuild);
end;

procedure TfrmGasBuilder.CleaningSource;
var
  sCmd: string;
begin
  StatusBar2.Panels[3].Text := '';
  StatusBar2.Panels[1].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  LogResult.LogType := ltInfo;
  LogResult.MethodName:='Cleaning Source';
  LogResult.status:='starting';
  WriteLog(LogResult);
  StatusBar1.SimpleText := 'Cleaning source';
  setCurrentDir(BUILD_TRUNK1);
//  sCmd := '/c C:\DATA\DEVELOPMENT\WORKING\QManager\TrunkBuild\TRUNK1\SOURCECODECLEAN';
  sCmd := '/c C:\DATA\DEVELOPMENT\WORKING\QManager\TRUNKBUILD\Trunk1\tools\GAS_BUILD\SOURCECODECLEAN';
  StatusBar1.SimpleText := 'cleaning source';
  uMain.StartProcess('cmd', sCmd, true, true);

  StatusBar1.SimpleText := 'finished cleaning source';
  StatusBar2.Panels[3].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  LogResult.LogType := ltInfo;
  LogResult.MethodName:='Cleaning Source complete';
  LogResult.DataStream:='Cmd: '+sCmd;
  WriteLog(LogResult);
end;

procedure TfrmGasBuilder.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

procedure TfrmGasBuilder.btnPostClick(Sender: TObject);
var
  postThread: Tthread;
begin
  CoInitialize(nil);
  postThread := Tthread.CreateAnonymousThread(
    procedure
    begin
      PostToTrunkBuild;
    end);
  postThread.Start;

//  if ((cbDoItAll.Checked) and  (postThread.CheckTerminated)) then btnPostClick(Sender);
end;


procedure TfrmGasBuilder.PostToTrunkBuild;
var
  sourceName: string;
  myZipFile: TZipFile;
begin
  StatusBar2.Panels[3].Text := 'Cleaning source';
  StatusBar2.Refresh;
  btnPost.Enabled:=false;
  CleaningSource;
  StatusBar2.Panels[3].Text := '';
  StatusBar2.Panels[1].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  myZipFile := TZipFile.Create;
  setCurrentDir(QMANAGER);

  sourceName := BASE_NAME + BuildNo + '-' + GitHash + '-' + 'SOURCE.zip';
  myZipFile.Open(sourceName, TZipMode.zmWrite);
  myZipFile.Close;
  myZipFile.Free;
  StatusBar1.SimpleText := 'Compressing source...';
  TZipFile.ZipDirectoryContents(sourceName, pWideChar(TRUNK_BUILD));
  LogResult.LogType := ltInfo;
  LogResult.MethodName:='PostToTrunkBuild';
  LogResult.DataStream:='Source: '+sourceName +' TRUNK_BUILD: '+TRUNK_BUILD;
  WriteLog(LogResult);
  StatusBar1.SimpleText := 'Finished Compressing source.  This step complete';
  StatusBar2.Panels[3].Text := TimeToStr(Now);
  StatusBar2.Refresh;
  ProgressBar1.position := 0;
  SetbuildState(bsDeploy);

  LogResult.LogType := ltInfo;
  LogResult.MethodName:='PostToTrunkBuild';
  LogResult.Status:='Completed';
  WriteLog(LogResult);

end;

function TfrmGasBuilder.CopyGasBuildFiles: boolean;
const
  BUILDBAT = 'BuildQM.bat';
  QMBUILD = 'QM.msbuild';
  QMD10BUILD = 'QMD10.msbuild';
  SOURCE_CLEAN = 'SOURCECODECLEAN.bat';
begin
  result := true;
  try
    if fAbort then
    begin
      StatusBar1.SimpleText := 'Aborting process';
      application.Terminate;
      exit;
    end;
    StatusBar1.SimpleText := 'Copying records from ' + GAS_BUILD + BUILDBAT +
      ' to ' + TRUNK + BUILDBAT;
    copyfile(pChar(GAS_BUILD + BUILDBAT), pChar(TRUNK + BUILDBAT), false);
    StatusBar1.SimpleText := 'Copying records from ' + GAS_BUILD + QMBUILD +
      ' to ' + TRUNK + QMBUILD;
    copyfile(pChar(GAS_BUILD + QMBUILD), pChar(TRUNK + QMBUILD), false);
    StatusBar1.SimpleText := 'Copying records from ' + GAS_BUILD + QMD10BUILD +
      ' to ' + TRUNK + QMD10BUILD;
    copyfile(pChar(GAS_BUILD + QMD10BUILD), pChar(TRUNK + QMD10BUILD), false);
    StatusBar1.SimpleText := 'Copying records from ' + GAS_BUILD + BUILDBAT +
      ' to ' + TRUNK + BUILDBAT;
    copyfile(pChar(GAS_BUILD + BUILDBAT), pChar(TRUNK + BUILDBAT), false);
    StatusBar1.SimpleText := 'Copying records from ' + GAS_BUILD + SOURCE_CLEAN
      + ' to ' + TRUNK + SOURCE_CLEAN;
    copyfile(pChar(GAS_BUILD + SOURCE_CLEAN),
      pChar(TRUNK + SOURCE_CLEAN), false);
//  CoInitialize;

  LogResult.LogType := ltInfo;
  LogResult.MethodName:='CopyGasBuildFiles';
  WriteLog(LogResult);

  except
    on E: Exception do
    begin
      showmessage(E.Message);
      result := false;
    end;
  end;
end;

function TfrmGasBuilder.RunBuild: boolean;
var
  sCmd: string;
begin
  result := true;
  if fAbort then
  begin
    StatusBar1.SimpleText := 'Aborting process';
    application.Terminate;
    exit;
  end;

  try
    setCurrentDir(TRUNK);
    StatusBar1.SimpleText := 'Building ' + BASE_NAME+BuildNo;
    sCmd := '/c BuildQM_D10' + ' ' + BuildNo + ' ' + GitHash;

   uMain.StartProcess('cmd', sCmd, true, true);
  LogResult.LogType := ltInfo;
  LogResult.MethodName:='RunBuild';
  LogResult.Status:= 'uMain.StartProcess';
  WriteLog(LogResult);
  except
    on E: Exception do
    begin
      showmessage(E.Message);
      result := false;
    end;
  end;
end;

procedure TfrmGasBuilder.SetbuildState(const Value: TbuildState);
begin
  case Value of
    bsIdle:
    begin
        btnBuild.Enabled := false;
        btnPost.Enabled := false;
        btnDeploy.Enabled := false;
        btnMail.Enabled := true;
    end;
    bsBuild:
      begin
        btnBuild.Enabled := true;
        btnPost.Enabled := false;
        btnDeploy.Enabled := false;
        btnMail.Enabled := false;
      end;
    bsPost:
      begin
        btnBuild.Enabled := false;
        btnPost.Enabled := true;
        btnDeploy.Enabled := false;
        btnMail.Enabled := false;
      end;
    bsDeploy:
      begin
        btnBuild.Enabled := false;
        btnPost.Enabled := false;
        btnDeploy.Enabled := true;
        btnMail.Enabled := false;
      end;
    bsMail:
      begin
        btnBuild.Enabled := false;
        btnPost.Enabled := false;
        btnDeploy.Enabled := false;
        btnMail.Enabled := true;
      end;
  end;
  self.Refresh;
end;

procedure TfrmGasBuilder.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  PRE_PEND = '[hh:nn:ss] ';
begin
  Leader := FormatDateTime(PRE_PEND, NOW);
  LogName := IncludeTrailingBackslash(flogDir) + 'GasBuilder '+edtBuildNo.text +'_'+ FormatDateTime('yyyy-mm-dd', Date) + '.txt';

  case LogResult.LogType of
    ltError:
      EntryType := '**************** ERROR ****************';
    ltInfo:
      EntryType := '****************  INFO  ****************';
    ltNotice:
      EntryType := '**************** NOTICE ****************';
    ltWarning:
      EntryType := '**************** WARNING ****************';
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;

  WriteLn(myFile, EntryType);

  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
  end;

  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;

  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
  end;

  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
  end;
  CloseFile(myFile);
  clearLogRecord;

end;

function TfrmGasBuilder.CopyDir(const fromDir, toDir: string): boolean;
var
  fos: TSHFileOpStruct;
begin
  result := false;
  StatusBar1.SimpleText := 'Copying records from ' + fromDir + ' to ' + toDir;
  if fAbort then
  begin
    StatusBar1.SimpleText := 'Aborting process';
    application.Terminate;
    exit;
  end;
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_COPY;
    fFlags := FOF_FILESONLY;
    pFrom := pChar(fromDir + #0);
    pTo := pChar(toDir)
  end;
  result := (0 = ShFileOperation(fos));
end;

function TfrmGasBuilder.MoveDir(const fromDir, toDir: string): boolean;
var
  fos: TSHFileOpStruct;
begin
  StatusBar1.SimpleText := 'Moving records from ' + fromDir + ' to ' + toDir;
  if fAbort then
  begin
    StatusBar1.SimpleText := 'Aborting process';
    application.Terminate;
  end;
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_MOVE;
    fFlags := FOF_FILESONLY;
    pFrom := pChar(fromDir + #0);
    pTo := pChar(toDir)
  end;
  result := (0 = ShFileOperation(fos));
end;

function TfrmGasBuilder.DelDir(dir: string): boolean;
var
  fos: TSHFileOpStruct;
begin
  StatusBar1.SimpleText := 'Deleting ' + dir;
  if fAbort then
  begin
    StatusBar1.SimpleText := 'Aborting process';
    application.Terminate;
  end;

  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_DELETE;
    fFlags := FOF_SILENT or FOF_NOCONFIRMATION;
    pFrom := pChar(dir + #0);
  end;
  result := (0 = ShFileOperation(fos));
end;

procedure TfrmGasBuilder.FormCreate(Sender: TObject);
begin
  ForceDirectories(TRUNK_BUILD);
  ForceDirectories(FINAL_BUILD);
  ForceDirectories(BUILD_TRUNK1);
  ForceDirectories(QMANAGER);
//  ForceDirectories(MY_SETTINGS);
  flogDir:='E:\QM\Logs';
  edtHash.Text := getGitHashFromMaster;

  Memo1.Lines.Add('running in '+Paramstr(0));
    Memo1.Lines.Add(getCommitDescript);
  statusbar2.Panels[5].Text:= GetAppVersionStr;
end;

procedure TfrmGasBuilder.FormShow(Sender: TObject);
begin
  SetbuildState(bsIdle);
end;

function TfrmGasBuilder.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
  // major
  LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
  LongRec(FixedPtr.dwFileVersionLS).Hi, // release
  LongRec(FixedPtr.dwFileVersionLS).Lo]) // build

end;

function TfrmGasBuilder.getGitHashFromMaster: string;
var
  myFile: TextFile;
  GitHash: string;
begin
  assignFile(myFile, 'C:\TRUNK1\.git\refs\heads\master');
  Reset(myFile);
  ReadLn(myFile, GitHash);
  result := copy(GitHash, 0, 7);
end;

function TfrmGasBuilder.getCommitDescript: string;
var
  headFile: TextFile;
  buffer: string;
  commitLine: string;
  charNo: integer;
const
  PRE_LIMITER = 'commit:';
  HEAD_FILE = 'C:\Trunk1\.git\logs\HEAD';
begin
  commitLine:='';
  try
    assignFile(headFile, HEAD_FILE);
    Reset(headFile);
    while not SeekEof(headFile) do
    begin
      ReadLn(headFile, buffer);
      charNo := pos(PRE_LIMITER, buffer);
      if charNo<>0 then
      commitLine := copy(buffer, charNo + length(PRE_LIMITER)+1, maxChar);
    end;
    result := commitLine;
  finally
    CloseFile(headFile);
  end;
end;

function TfrmGasBuilder.getBuildNo: string;
begin
  result := edtBuildNo.Text;
end;

function TfrmGasBuilder.getGitHash: string;
begin
  result := edtHash.Text;
end;

function Deletefiles2(APath: string): integer;
var
  lSearchRec: TSearchRec;
  lFind, i: integer;
  lPath: string;
  fileExt: string;

begin
  application.ProcessMessages;
  lPath := IncludeTrailingPathDelimiter(APath);
  i := 0;
  lFind := FindFirst(lPath + '*.*', faAnyFile, lSearchRec);
  while lFind = 0 do
  begin
    fileExt := UpperCase(ExtractFileExt(lPath + lSearchRec.Name));
    if DeleteFile(lPath + lSearchRec.Name) then
      inc(i);

    lFind := FindNext(lSearchRec);
  end;
  FindClose(lSearchRec);
  result := i;
end;

procedure Deletefiles(APath, AFileSpec: string);
var
  lSearchRec:TSearchRec;
  lPath:string;
begin
  lPath := IncludeTrailingPathDelimiter(APath);

  if FindFirst(lPath+ AFileSpec,faAnyFile,lSearchRec) = 0 then
  begin
    try
      repeat
      begin
        DeleteFile(lPath+lSearchRec.Name);

        frmGasBuilder.LogResult.LogType := ltInfo;
        frmGasBuilder.LogResult.MethodName:='Deletefiles';
        frmGasBuilder.LogResult.status:='File Spec: '+AFileSpec;
        frmGasBuilder.LogResult.DataStream:=lPath+lSearchRec.Name;
        frmGasBuilder.WriteLog(frmGasBuilder.LogResult);
      end;
      until FindNext(lSearchRec) <> 0;
    finally
      FindClose(lSearchRec);  // Free resources on successful find
    end;
  end;
end;

function StartProcess(ExeName: string; CmdLineArgs: string = '';
ShowWindow: boolean = true; WaitForFinish: boolean = false): integer;
var
  StartInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
begin
  // Simple wrapper for the CreateProcess command
  // returns the process id of the started process.
  FillChar(StartInfo, SizeOf(TStartupInfo), #0);
  FillChar(ProcInfo, SizeOf(TProcessInformation), #0);
  StartInfo.cb := SizeOf(TStartupInfo);

  if not(ShowWindow) then
  begin
    StartInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartInfo.wShowWindow := SW_HIDE;
  end;

  CreateProcess(nil, pChar(ExeName + ' ' + CmdLineArgs), nil, nil, false,
    CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS, nil, nil, StartInfo,
    ProcInfo);

  result := ProcInfo.dwProcessId;

  if WaitForFinish then
  begin
    WaitForSingleObject(ProcInfo.hProcess, Infinite);
    frmGasBuilder.ProgressBar1.StepIt;
  end;

  // close process & thread handles
  CloseHandle(ProcInfo.hProcess);
  CloseHandle(ProcInfo.hThread);
end;




function processExists(exeFileName: string): Boolean;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
    begin
      Result := True;
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;



end.
