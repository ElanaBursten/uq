object frmPlats7: TfrmPlats7
  Left = 0
  Top = 0
  Caption = 'frmPlats7'
  ClientHeight = 426
  ClientWidth = 683
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 100
    Top = 253
    Width = 49
    Height = 13
    Caption = 'Search Dir'
  end
  object Label4: TLabel
    Left = 244
    Top = 253
    Width = 60
    Height = 13
    Caption = 'Search Mask'
  end
  object Label5: TLabel
    Left = 215
    Top = 368
    Width = 52
    Height = 13
    Caption = 'USB Drives'
  end
  object Label1: TLabel
    Left = 312
    Top = 368
    Width = 34
    Height = 13
    Caption = 'Volume'
  end
  object Label2: TLabel
    Left = 100
    Top = 201
    Width = 67
    Height = 13
    Caption = 'Output Folder'
  end
  object Label10: TLabel
    Left = 352
    Top = 201
    Width = 71
    Height = 13
    Caption = 'Space Availible'
  end
  object Label12: TLabel
    Left = 35
    Top = 323
    Width = 67
    Height = 13
    Caption = 'Existing plats:'
  end
  object Label13: TLabel
    Left = 108
    Top = 301
    Width = 45
    Height = 13
    Caption = 'Company'
  end
  object Label14: TLabel
    Left = 188
    Top = 301
    Width = 33
    Height = 13
    Caption = 'Center'
  end
  object Label16: TLabel
    Left = 259
    Top = 301
    Width = 44
    Height = 13
    Caption = 'Plat Date'
  end
  object Label17: TLabel
    Left = 329
    Top = 301
    Width = 40
    Height = 13
    Caption = 'Plat Age'
  end
  object LabelCount: TLabel
    Left = 552
    Top = 181
    Width = 54
    Height = 13
    Caption = 'LabelCount'
  end
  object btnFindUSBDrives: TButton
    Left = 100
    Top = 384
    Width = 105
    Height = 25
    Caption = 'Find USB Drives'
    TabOrder = 0
    OnClick = btnFindUSBDrivesClick
  end
  object btnFindPM7: TButton
    Left = 404
    Top = 270
    Width = 145
    Height = 25
    Caption = 'Find PM7 Records'
    TabOrder = 1
    OnClick = btnFindPM7Click
  end
  object EditStartDir: TEdit
    Left = 100
    Top = 272
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object EditFileMask: TEdit
    Left = 244
    Top = 272
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '*.pm7'
  end
  object cbUSBdrives: TComboBox
    Left = 211
    Top = 387
    Width = 65
    Height = 21
    TabOrder = 4
    OnChange = cbUSBdrivesChange
  end
  object edtVolume: TEdit
    Left = 308
    Top = 387
    Width = 57
    Height = 21
    TabOrder = 5
  end
  object edtOutput: TEdit
    Left = 100
    Top = 179
    Width = 203
    Height = 21
    TabOrder = 6
    Text = 'c:\omnilume'
  end
  object bBtnSaveTo: TBitBtn
    Left = 321
    Top = 176
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 7
    OnClick = bBtnSaveToClick
  end
  object edtSpaceAvail: TEdit
    Left = 352
    Top = 179
    Width = 121
    Height = 21
    TabOrder = 8
    Text = 'edtSpaceAvail'
  end
  object edtCompany: TEdit
    Left = 108
    Top = 320
    Width = 59
    Height = 21
    TabOrder = 9
  end
  object edtPlatDate: TEdit
    Left = 238
    Top = 320
    Width = 85
    Height = 21
    TabOrder = 10
  end
  object edtCenter: TEdit
    Left = 173
    Top = 320
    Width = 59
    Height = 21
    TabOrder = 11
  end
  object edtPlatAge: TEdit
    Left = 329
    Top = 320
    Width = 59
    Height = 21
    TabOrder = 12
  end
  object btnHowOld: TButton
    Left = 429
    Top = 318
    Width = 75
    Height = 25
    Caption = 'How Old'
    TabOrder = 13
    OnClick = btnHowOldClick
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 683
    Height = 170
    Align = alTop
    TabOrder = 14
    RootLevelOptions.DetailTabsPosition = dtpTop
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object PlatsView: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object PlatsViewSelect: TcxGridColumn
        Caption = 'Select'
        DataBinding.ValueType = 'Boolean'
        PropertiesClassName = 'TcxCheckBoxProperties'
      end
      object PlatsViewCompany: TcxGridColumn
        Caption = 'Company'
        Options.Editing = False
        Width = 100
      end
      object PlatsViewCenter: TcxGridColumn
        Caption = 'Center'
        Options.Editing = False
        Width = 100
      end
      object PlatsViewClient: TcxGridColumn
        Caption = 'Client'
        Options.Editing = False
        Width = 200
      end
      object PlatsViewPlatDate: TcxGridColumn
        Caption = 'Plat Date'
        DataBinding.ValueType = 'DateTime'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.ReadOnly = True
        Properties.SaveTime = False
        Properties.ShowTime = False
        Properties.ShowToday = False
        Properties.YearsInMonthList = False
        Options.Editing = False
      end
      object PlatsViewPlatAge: TcxGridColumn
        Caption = 'How Old (Days)'
        PropertiesClassName = 'TcxLabelProperties'
        Options.Editing = False
      end
      object PlatsViewFileOnDisk: TcxGridColumn
        Caption = 'File On Disk'
        Width = 200
      end
    end
    object cxGrid1Level1: TcxGridLevel
      Caption = 'Data Level'
      GridView = cxGrid1DBTableView1
    end
    object cxGrid1Plats: TcxGridLevel
      Caption = 'Plats'
      GridView = PlatsView
    end
  end
  object btnUpdatePlats: TButton
    Left = 19
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Update Plats'
    TabOrder = 15
    OnClick = btnUpdatePlatsClick
  end
end
