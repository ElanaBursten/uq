unit uMainPlates7;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.FileCtrl, System.Types, ShellAPI,
  Vcl.Buttons, Vcl.ComCtrls, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinOffice2019Colorful, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringtime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGridDBTableView,
  cxGrid, cxCalendar, cxLabel;

{$MINENUMSIZE 4}
const
  IOCTL_STORAGE_QUERY_PROPERTY =  $002D1400;
  SELDIRHELP = 1000;
type
  STORAGE_QUERY_TYPE = (PropertyStandardQuery = 0, PropertyExistsQuery, PropertyMaskQuery, PropertyQueryMaxDefined);
  TStorageQueryType = STORAGE_QUERY_TYPE;

  STORAGE_PROPERTY_ID = (StorageDeviceProperty = 0, StorageAdapterProperty);
  TStoragePropertyID = STORAGE_PROPERTY_ID;

  STORAGE_PROPERTY_QUERY = packed record
    PropertyId: STORAGE_PROPERTY_ID;
    QueryType: STORAGE_QUERY_TYPE;
    AdditionalParameters: array [0..9] of AnsiChar;
  end;
  TStoragePropertyQuery = STORAGE_PROPERTY_QUERY;

  STORAGE_BUS_TYPE = (BusTypeUnknown = 0, BusTypeScsi, BusTypeAtapi, BusTypeAta, BusType1394, BusTypeSsa, BusTypeFibre,
    BusTypeUsb, BusTypeRAID, BusTypeiScsi, BusTypeSas, BusTypeSata, BusTypeMaxReserved = $7F);
  TStorageBusType = STORAGE_BUS_TYPE;

  STORAGE_DEVICE_DESCRIPTOR = packed record
    Version: DWORD;
    Size: DWORD;
    DeviceType: Byte;
    DeviceTypeModifier: Byte;
    RemovableMedia: Boolean;
    CommandQueueing: Boolean;
    VendorIdOffset: DWORD;
    ProductIdOffset: DWORD;
    ProductRevisionOffset: DWORD;
    SerialNumberOffset: DWORD;
    BusType: STORAGE_BUS_TYPE;
    RawPropertiesLength: DWORD;
    RawDeviceProperties: array [0..0] of AnsiChar;
  end;
  TStorageDeviceDescriptor = STORAGE_DEVICE_DESCRIPTOR;

  TPM_FILE_PARSED = record
    PM_Client  : String;
    PM_Company : String;
    PM_Center  : String;
    PM_Year    : String;
    PM_Month   : String;
    PM_Day     : String;
    PM_Date    : TDate;
    DaysOld    : integer;
  end;

type
  TfrmPlats7 = class(TForm)
    btnFindUSBDrives: TButton;
    btnFindPM7: TButton;
    EditStartDir: TEdit;
    EditFileMask: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbUSBdrives: TComboBox;
    Label5: TLabel;
    edtVolume: TEdit;
    Label1: TLabel;
    edtOutput: TEdit;
    Label2: TLabel;
    bBtnSaveTo: TBitBtn;
    Label10: TLabel;
    edtSpaceAvail: TEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    edtCompany: TEdit;
    edtPlatDate: TEdit;
    edtCenter: TEdit;
    edtPlatAge: TEdit;
    btnHowOld: TButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1Plats: TcxGridLevel;
    PlatsView: TcxGridTableView;
    PlatsViewSelect: TcxGridColumn;
    PlatsViewCompany: TcxGridColumn;
    PlatsViewCenter: TcxGridColumn;
    PlatsViewClient: TcxGridColumn;
    PlatsViewPlatDate: TcxGridColumn;
    PlatsViewPlatAge: TcxGridColumn;
    btnUpdatePlats: TButton;
    LabelCount: TLabel;
    PlatsViewFileOnDisk: TcxGridColumn;
    procedure btnFindUSBDrivesClick(Sender: TObject);
    procedure btnFindPM7Click(Sender: TObject);
    procedure cbUSBdrivesChange(Sender: TObject);
    procedure bBtnSaveToClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnHowOldClick(Sender: TObject);
    procedure btnUpdatePlatsClick(Sender: TObject);
  private
    fPM_Center: String;
    fPM_Client: String;
    fPM_Year: String;
    fPM_Day: String;
    fPM_Company: String;
    fPM_Month: String;
    fPM_Date: TDate;
    PM_FileParsed: TPM_FILE_PARSED;
    SaveToDir : TFilename;
    fDaysOld: integer;
    lastLine:string;
    procedure FindFiles(FilesList: TStringList; StartDir, FileMask: string);
    procedure PopUsbDrives;
    function ExecuteProcess(const FileName, Params: string; Folder: string;
      WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
      var ErrorCode: integer): Boolean;
    function GetDiskSpace(DrvLtr:char):Int64;
    function GetLastLine(MyFile: TFilename): string;
    function FindInternalUpdatePM(HintsFile: TFileName): TFileName;
    function ParseUpdatePM(PMrecord: string): TPM_FILE_PARSED;

    { Private declarations }
  public
    { Public declarations }

    property PM_Client  : String read fPM_Client write fPM_Client;
    property PM_Company : String read fPM_Company write fPM_Company;
    property PM_Center  : String read fPM_Center write fPM_Center;
    property PM_Year    : String read fPM_Year write fPM_Year;
    property PM_Month   : String read fPM_Month write fPM_Month;
    property PM_Day     : String read fPM_Day write fPM_Day;
    property PM_Date    : TDate  read fPM_Date write fPM_Date;
    property DaysOld    : integer read fDaysOld write fDaysOld;

  end;

var
  frmPlats7: TfrmPlats7;
const
  PLATS7_VOL='Plats7';
  HintsFileLocal = 'C:\tools\_base\hints.txt';  //     QManager


implementation
uses System.StrUtils, System.DateUtils;
{$R *.dfm}

procedure DeleteDirectory(const DirectoryName: string);
//Use the ShellAPI to remove a folder, it's subfolders and files
var
  FileOp: TSHFileOpStruct;
begin
  FillChar(FileOp, SizeOf(FileOp), 0);
  FileOp.wFunc := FO_DELETE;
  FileOp.pFrom := PChar(DirectoryName+#0);//double zero-terminated
  FileOp.fFlags := FOF_SILENT or FOF_NOERRORUI or FOF_NOCONFIRMATION;
  SHFileOperation(FileOp);
end;

function GetBusType(Drive: AnsiChar): TStorageBusType;
var
  H: THandle;
  Query: TStoragePropertyQuery;
  dwBytesReturned: DWORD;
  Buffer: array [0..1023] of Byte;
  sdd: TStorageDeviceDescriptor absolute Buffer;
  OldMode: UINT;
begin
  Result := BusTypeUnknown;

  OldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
    H := CreateFile(PChar(Format('\\.\%s:', [AnsiLowerCase(Drive)])), 0, FILE_SHARE_READ or FILE_SHARE_WRITE, nil,
      OPEN_EXISTING, 0, 0);
    if H <> INVALID_HANDLE_VALUE then
    begin
      try
        dwBytesReturned := 0;
        FillChar(Query, SizeOf(Query), 0);
        FillChar(Buffer, SizeOf(Buffer), 0);
        sdd.Size := SizeOf(Buffer);
        Query.PropertyId := StorageDeviceProperty;
        Query.QueryType := PropertyStandardQuery;
        if DeviceIoControl(H, IOCTL_STORAGE_QUERY_PROPERTY, @Query, SizeOf(Query), @Buffer, SizeOf(Buffer), dwBytesReturned, nil) then
          Result := sdd.BusType;
      finally
        CloseHandle(H);
      end;
    end;
  finally
    SetErrorMode(OldMode);
  end;
end;

procedure TfrmPlats7.PopUsbDrives;
var
  DriveBits: set of 0 .. 25;
  I: Integer;
  Drive: AnsiChar;
begin
  cbUSBdrives.Clear;
  Cardinal(DriveBits) := GetLogicalDrives;

  for I := 0 to 25 do
    if I in DriveBits then
    begin
      Drive := AnsiChar(Chr(Ord('a') + I));
      if GetBusType(Drive) = BusTypeUsb then
        cbUSBdrives.Items.Add(Drive);
    end;
end;

function GetVolumeName(DriveLetter: Char): string;
var
  dummy: DWORD;
  buffer: array[0..MAX_PATH] of Char;
  oldmode: LongInt;
begin
  oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
  try
    GetVolumeInformation(PChar(DriveLetter + ':\'),
                         buffer,
                         SizeOf(buffer),
                         nil,
                         dummy,
                         dummy,
                         nil,
                         0);
    Result := StrPas(buffer);
  finally
    SetErrorMode(oldmode);
  end;
end;

procedure TfrmPlats7.FindFiles(FilesList: TStringList; StartDir, FileMask: string);
var
  SR: TSearchRec;
  DirList: TStringList;
  IsFound: Boolean;
  i: integer;
begin
  if StartDir[length(StartDir)] <> '\' then
    StartDir := StartDir + '\';

  IsFound :=
    FindFirst(StartDir+FileMask, faAnyFile-faDirectory, SR) = 0;
  while IsFound do begin
    FilesList.Add(StartDir + SR.Name);
    IsFound := FindNext(SR) = 0;
  end;
  FindClose(SR);

  // Build a list of subdirectories
  DirList := TStringList.Create;
  IsFound := FindFirst(StartDir+'*.*', faAnyFile, SR) = 0;
  while IsFound do begin
    if ((SR.Attr and faDirectory) <> 0) and
         (SR.Name[1] <> '.') then
      DirList.Add(StartDir + SR.Name);
    IsFound := FindNext(SR) = 0;
  end;
  FindClose(SR);

  // Scan the list of subdirectories
  for i := 0 to DirList.Count - 1 do
    FindFiles(FilesList, DirList[i], FileMask);

  DirList.Free;
end;

function TfrmPlats7.ParseUpdatePM(PMrecord:string):TPM_FILE_PARSED;
const
  PM_DATE_STRUCTURE = '%s/%s/%s';
var
  myrecordArr: TStringDynArray;
begin
  myrecordArr := SplitString(PMrecord, #46);
  With PM_FileParsed do
  begin
    PM_Company  := copy(myrecordArr[0], 0, 3);
    PM_Center   := copy(myrecordArr[0], 4, 3);
    PM_Year     := myrecordArr[1];
    PM_Month    := myrecordArr[2];
    PM_Day      := myrecordArr[3];
    PM_Client   := myrecordArr[4];
    PM_Date     := StrToDate(Format(PM_DATE_STRUCTURE, [PM_Month, PM_Day, PM_Year]));
    DaysOld     := DaysBetween(Today, PM_Date);
  end;
  result := PM_FileParsed;
end;


procedure TfrmPlats7.btnFindUSBDrivesClick(Sender: TObject);
begin
  PopUsbDrives;
end;

procedure TfrmPlats7.btnHowOldClick(Sender: TObject);
const
  PM_DATE_STRUCTURE = '%s/%s/%s';
var
  myrecordArr: TStringDynArray;
  L_Year, L_Month, L_Day, L_DaysOld: string;
  L_Date: TDate;
begin
  myrecordArr := SplitString(lastLine, #46);
  edtCenter.Text := copy(myrecordArr[0], 4, 3);
  edtCompany.Text := copy(myrecordArr[0], 0, 3);
  L_Year := myrecordArr[1];
  L_Month := myrecordArr[2];
  L_Day := myrecordArr[3];
  L_Date := StrToDate(Format(PM_DATE_STRUCTURE, [L_Month, L_Day, L_Year]));
  L_DaysOld := IntToStr(DaysBetween(Today, L_Date));

  edtPlatDate.Text := DateToStr(L_Date);
  edtPlatAge.Text := L_DaysOld;

end;

procedure TfrmPlats7.btnUpdatePlatsClick(Sender: TObject);
var
  record_zip: string;
  AView: TcxGridTableView;
  I: integer;
  Params7: string;
  ErrorCode: integer;
const
  PM_DATE_STRUCTURE = '%s/%s/%s';
  ZIP7_EXE = 'C:\Program Files\7-Zip\7zG.exe';
begin
  SaveToDir := edtOutput.Text;

  DeleteDirectory(SaveToDir);
  ForceDirectories(SaveToDir);

  AView := PlatsView;
  for I := 0 to AView.DataController.RecordCount - 1 do
    if AView.DataController.Values[I, PlatsViewSelect.Index] = True then
    begin
      record_zip := AView.DataController.Values[I, PlatsViewFileOnDisk.Index];
      Params7 := ' x ' + record_zip + ' -o' + SaveToDir +
        ' -pStar_Trek -y -aoa ';

      ExecuteProcess(ZIP7_EXE, Params7, '', True, False, False, ErrorCode);
    end;
end;

procedure TfrmPlats7.cbUSBdrivesChange(Sender: TObject);
var
  DrvLtr:char;
  s:string;
const
  prompt=':\';
begin
  s:=  cbUSBdrives.Text;
  DrvLtr := s[1];
  edtVolume.Text := GetVolumeName(DrvLtr);
  EditStartDir.Text := DrvLtr+prompt;
end;

procedure TfrmPlats7.bBtnSaveToClick(Sender: TObject);
var
  Dir: string;
  DrvLtr: char;
  iDriveSpace: Int64;
  dskUnit: string;
begin
  Dir := edtOutput.Text;
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP)
  then
    edtOutput.Text := Dir;
  DrvLtr := Dir[1];
  iDriveSpace := GetDiskSpace(DrvLtr);
  case iDriveSpace of
    0 .. 1024:
      begin
        dskUnit := 'Byte';
      end;
    1025 .. 1048576:
      begin
        dskUnit := 'KB';
        edtSpaceAvail.Text := FloatToStrF((iDriveSpace / 1024), ffNumber, 20, 0) + ' ' + dskUnit;
      end;
    1048577 .. 1073741824:
      begin
        dskUnit := 'MB';
        edtSpaceAvail.Text := FloatToStrF((iDriveSpace / 1048576), ffNumber, 20, 0) + ' ' + dskUnit;
      end;
    else
    begin
      dskUnit := 'GB';
        edtSpaceAvail.Text := FloatToStrF((iDriveSpace / 1073741824), ffNumber, 20, 0) + ' ' + dskUnit;
    end;
  end;
end;

procedure TfrmPlats7.btnFindPM7Click(Sender: TObject);
var
  FilesList: TStringList;
  I: integer;
  oPM_FileParsed: TPM_FILE_PARSED;
begin // search USB drive for Plat files
  I := 0;
  if (edtVolume.Text <> PLATS7_VOL) then
  // and (MessageDlg('That location is not a Plats7 volume.  A search of this drive could take a long time.  Are you sure you want to continue?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    showmessage
      ('There is no Plats7 volume in this drive.  Please select another.');
    exit;
  end;
  begin
    FilesList := TStringList.Create;
    try
      FindFiles(FilesList, EditStartDir.Text, EditFileMask.Text);


      // pop grid
      PlatsView.BeginUpdate();

      PlatsView.DataController.RecordCount := FilesList.Count;
      for I := 0 to FilesList.Count - 1 do
      begin   //record_name := extractFileName(ListBox1.FileName);
        oPM_FileParsed := ParseUpdatePM(extractFileName(FilesList.Strings[I]));
        PlatsView.DataController.Values[I, PlatsViewSelect.Index] := False;
        PlatsView.DataController.Values[I, PlatsViewCompany.Index] :=
          oPM_FileParsed.PM_Company;
        PlatsView.DataController.Values[I, PlatsViewCenter.Index] :=
          oPM_FileParsed.PM_Center;
        PlatsView.DataController.Values[I, PlatsViewClient.Index] :=
          oPM_FileParsed.PM_Client;
        PlatsView.DataController.Values[I, PlatsViewPlatDate.Index] :=
          oPM_FileParsed.PM_Date;
        PlatsView.DataController.Values[I, PlatsViewPlatAge.Index] :=
          oPM_FileParsed.DaysOld;
        PlatsView.DataController.Values[I, PlatsViewFileOnDisk.Index] :=
          FilesList.Strings[I];

      end;

      LabelCount.Caption := 'Files found: ' + IntToStr(FilesList.Count);
    finally
      PlatsView.EndUpdate;
      FilesList.Free;
    end;
  end;
end;

function TfrmPlats7.FindInternalUpdatePM(HintsFile: TFilename): TFilename;
var
  a: TextFile;
  s: string;
  pmFile: string;
const
  UPDATE_PM = 'update.pm';
begin
  assignfile(a, HintsFile);
  reset(a);
  While not eof(a) do // search all Hints dirs for update.pm file
  begin
    readln(a, s);
    pmFile := '';
    pmFile := FileSearch(UPDATE_PM, s);
    If pmFile <> '' then
    begin
      Result := pmFile;
      exit;
    end;
  end;
  closefile(a);
end;

procedure TfrmPlats7.FormActivate(Sender: TObject);
var
  FoundUpdatePM:TFilename;

begin
  FoundUpdatePM :=  FindInternalUpdatePM(HintsFileLocal);
  if FoundUpdatePM<>'' then
  lastLine:= GetLastLine(FoundUpdatePM)
  else
  begin
    showmessage('No UpdatePM found.');
  end;
end;

function TfrmPlats7.GetLastLine(MyFile: TFilename): string;
var
  a: TextFile;
  s: string;
begin
  assignfile(a, MyFile);
  reset(a);
  While not eof(a) do
    readln(a, s);
  closefile(a);
  Result := s;
end;

function TfrmPlats7.ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
  {SR: though more complicated, this is more powerful than ShellExecute.
  It will also pass thru Windows GetLastError.
  When running a batch file, it will catch batch file errors.  It does not use ActiveX}
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
      repeat
        Application.ProcessMessages;
      until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

function TfrmPlats7.GetDiskSpace(DrvLtr: char): Int64;
var
  I: integer;
  space: Int64;
  testLtr: char;
begin
  Result := 0;
  for I := 2 to 6 do
  begin
    space := DiskFree(I);
    testLtr := Chr(I + 64);
    if ((space >= 0) and (DrvLtr = testLtr)) then
    begin
      Result := space;
      exit;
    end;
  end;
end;

end.
