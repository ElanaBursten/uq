unit RawDataIO;

interface

uses
  Windows, Classes, DB, DBISAMTb;

const
  LocateDataFile = 'LocateData.txt';
  TicketDataFile = 'TicketData.txt';
  HourlyDataFile = 'HourlyData.txt';
  NotesDataFile = 'TicketNotes.txt';

procedure ExportRawBillingData(OutputDir: string; LocateRawData, TicketRawData, TicketNotes: TDataSet; HoursUnits: TDBISAMTable);

implementation

uses
  OdDbUtils, SysUtils, OdDBISAMUtils, OdMiscUtils;

procedure SaveStreamToFile(Stream: TMemoryStream; const FileName: string);
begin
  Assert(Assigned(Stream));
  DeleteFile(FileName);
  Stream.Seek(0, soFromBeginning);
  Stream.SaveToFile(FileName);
end;

procedure ExportRawBillingData(OutputDir: string; LocateRawData, TicketRawData, TicketNotes: TDataSet; HoursUnits: TDBISAMTable);

  procedure ExportTable(Table: TDBISAMTable; const FileName: string);
  begin
    if not Table.IsEmpty then
      Table.ExportTable(OutputDir + FileName, #9, True);
  end;

  procedure ExportDataSet(DataSet: TDataSet; const FileName: string);
  var
    Table: TDBISAMTable;
  begin
    Table := TDBISAMTable.Create(nil);
    try
      Table.DatabaseName := 'Memory';
      Table.TableName := ChangeFileExt(FileName, '');
      ClearTable(Table);
      CloneStructure(DataSet, Table);
      CopyDataFrom(DataSet, Table);
      ExportTable(Table, FileName);
    finally
      FreeAndNil(Table);
    end;
  end;

begin
  if not DirectoryExists(OutputDir) then
    OdForceDirectories(OutputDir);

  OutputDir := IncludeTrailingPathDelimiter(OutputDir);
  ExportDataSet(LocateRawData, LocateDataFile);
  ExportDataSet(TicketRawData, TicketDataFile);
  ExportTable(HoursUnits, HourlyDataFile);
end;

end.
