unit SolomonExportDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, contnrs;

type
  TSolomonExportDM = class(TDataModule)
    GLCodes: TADODataSet;
    DynamicsExport: TADOStoredProc;
    qryDycomCustNo: TADOQuery;
    qryGLflip: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    ProblemLog: TStringList;
    CurrentData: TStringList;
    DataLists: TStringList; // One entry here per locating company, indexed by company name
    FWarningString: string;
    FDefaultGLCode: string;
    FPostingPeriod: string;
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FExportEmployees: Boolean;
    FExportTasks: Boolean;
    NewSolExport: Boolean; //QMANTWO-683
    procedure FormatComma(const Args: array of const; const Names: array of string; const InsertPosition: Integer = -1);
    procedure SaveAllData(const DestinationDir: string; const BillID: Integer);
    procedure SelectCorrectDataFile(const LocCompanyName: string);
    procedure DiscardAttachedLists;
    procedure OpenAndCheckGLCodes;
    function GetGLCode(PeriodType, Source: string; IsGLCode: Boolean): string;
    procedure AssertValidGLCode(GLCode: string);
    procedure InsertLevel0Summary(const TotalAmount: string);
    procedure DoDynamicsExport(const ProcedureName, LocatingCompany, OutputFileName: string; const BillID: Integer = 0);
  public
    procedure Initialize(const StartDate, EndDate: TDateTime; const ExportEmployees, ExportTasks: Boolean);
    procedure AppendSolomonData(CustomerID: Integer; EndDate: string;
      BillingInvoice, InvoiceHeader, SolomonSplit: TDataSet; OutputConfigID: Integer);
    procedure ExportSolomon(const DestinationDir: string; const BillID: Integer;
      var WarningString: string);
  end;

implementation

{$R *.dfm}

uses
  JclStrings, OdMiscUtils, BillingEngineDMu, BillPeriod, BillingConst, Variants, System.IniFiles,
  OdDbUtils, StrUtils, BillingDMu;

const
  UnknownGLCode = '<UNKNOWN_GL_CODE_PLACEHOLDER>';

{ TSolomonExportDM }

procedure TSolomonExportDM.Initialize(const StartDate, EndDate: TDateTime;
  const ExportEmployees, ExportTasks: Boolean);
begin
  FreeAndNil(ProblemLog);
  ProblemLog := TStringList.Create;

  DiscardAttachedLists;
  FreeAndNil(DataLists);
  DataLists := TStringList.Create;

  FWarningString := '';
  FStartDate := StartDate;
  FEndDate := EndDate;
  FExportEmployees := ExportEmployees;
  FExportTasks := ExportTasks;
end;

procedure TSolomonExportDM.InsertLevel0Summary(const TotalAmount: string);
begin
  FormatComma(['Level0', FPostingPeriod, 'B', TotalAmount],
    ['Level', 'Period', 'H', 'Amount'], 0);
end;

procedure TSolomonExportDM.FormatComma(const Args: array of const; const Names: array of string; const InsertPosition: Integer);
const
  BoolChars: array[Boolean] of Char = ('F', 'T');
var
  i: Integer;
  DataLine: string;
  S: string;
  Warn: Boolean;
begin
  Warn := False;
  for i := Low(Args) to High(Args) do begin      //  test value 2
    S := '';
    with Args[i] do
      case VType of
        vtInteger:        S := IntToStr(VInteger);        //0
        vtBoolean:        S := BoolChars[VBoolean];       //1
        vtChar:           S := string(VChar);             //2
        vtExtended:       S := FloatToStr(VExtended^);    //3
        vtString:         S := string(VString^);          //4
        vtPChar:          S := string(VPChar);            //6
        vtObject:         S := VObject.ClassName;         //7
        vtClass:          S := VClass.ClassName;          //8
        vtWideChar:       S := VWideChar;                 //9  //QMANTWO-261
        vtAnsiString:     S := string(VAnsiString);       //10
        vtCurrency:       S := CurrToStr(VCurrency^);     //12
        vtVariant:        S := VarToString(VVariant^);    //13
        vtInt64:          S := IntToStr(VInt64^);         //16
        vtUnicodeString:  S := string(VUnicodeString);    //17 don't dereference  ...sr
    end;

    if S = '' then begin
      ProblemLog.Add('Warning - field "' + Names[i] + '" is empty in the data line below');
      Warn := True;
    end;

    S := StringReplace(S, ',', '', [rfReplaceAll]);

    DataLine := DataLine + S + ',';
  end;

  if InsertPosition < 0 then
    CurrentData.Add(DataLine)
  else
    CurrentData.Insert(InsertPosition, DataLine);

  if Warn then begin
    ProblemLog.Add('Data Line: ' + DataLine);
    ProblemLog.Add('--------------------------------');
  end;
end;

const
  SolomonDateFormat = 'm/d/yyyy';

procedure TSolomonExportDM.AppendSolomonData(CustomerID: Integer; EndDate: string;
      BillingInvoice, InvoiceHeader, SolomonSplit: TDataSet; OutputConfigID: Integer);  //qm-653
var
  TaskID: string;
  Contract: string;
  GLCode: string;
  SolomonOffice: string;
  Amount: Currency;
  AmountString: string;
  PC: string;
  InvoiceDate: TDateTime;
  Source: string;
  IsGLCode: Boolean;
  PeriodType: string;
  SolomonLines: Integer;
  GLCodeTotals: TStringList;
  CurrentGLAmount: Currency;
  DueDays: Integer;
  LocatingCompany: string;
  DueDaysCode : string;
  AdditionalLineItems:string;
  DyComCustomerNumber:string;
const
  GL_CODE_ADD = '30155'; //QM-190

  function CustomerDescription: string;
  begin
    Result := InvoiceHeader.FieldByName('customer_number').AsString + ' ' +
      InvoiceHeader.FieldByName('customer_description').AsString;
  end;

  function GetHighestValueGLCode: string;
  var
    HighestValue: Currency;
    i: Integer;
    Code: string;
    Total: Currency;
  begin
    Assert(NotEmpty(FDefaultGLCode), 'Default GL Code is not set');
    HighestValue := -999999;
    Result := FDefaultGLCode;
    for i := 0 to GLCodeTotals.Count - 1 do begin
      Code := GLCodeTotals.Names[i];
      if not SameText(Code, UnknownGLCode) then begin
        Total := StrToCurr(GLCodeTotals.Values[Code]);
        if Total > HighestValue then begin
          Result := Code;
          HighestValue := Total;
        end;
      end;
    end;
  end;

  function GetDueDays(DaysStr: string): Integer;
  begin
    DaysStr := Trim(DaysStr);
    DaysStr := GetStringBeforeCharacter(DaysStr, ' '); // Account for  '30 Days'
    Result := StrToIntDef(DaysStr, 30);
  end;

begin

  try     //qm-653
    qryDycomCustNo.Parameters.ParamByName('OutputConfigId').Value:=OutputConfigID;
    qryDycomCustNo.open;
    DyComCustomerNumber:=  qryDycomCustNo.FieldByName('dycomCustomerNo').AsString;
  finally
    qryDycomCustNo.Close;
  end;

  ProblemLog.Add('Adding Solomon data for customer: ' +  CustomerDescription);
  PerfLog.Start('Adding Solomon data for customer: ' +  CustomerDescription);
  LocatingCompany := InvoiceHeader.FieldByName('locating_company_name').AsString;
  SelectCorrectDataFile(LocatingCompany);

  if InvoiceHeader.FieldByName('SolOffice').AsString = '' then
    ProblemLog.Add('WARNING - this customer does not have Profit Center / Solomon Data Populated');

  if InvoiceHeader.FieldByName('customer_number').AsString = '' then
    ProblemLog.Add('WARNING - this customer does not have a customer number');

  if  BillingInvoice.FieldByName('invoice_date').IsNull then
    InvoiceDate := Date   // Not commit, just a file to look at.
  else
    InvoiceDate := BillingInvoice.FieldByName('invoice_date').AsDateTime;

  FPostingPeriod := StrRemoveChars(VarToString(InvoiceHeader.FieldValues['SolPeriodToPost']), ['-']);

  ProblemLog.Add(Format('Generating data for customer# %s invoice %s date %s amount %s',
    [ InvoiceHeader.FieldByName('customer_number').AsString,
      BillingInvoice.FieldByName('FullInvoiceNumber').AsString,
      FormatDateTime(SolomonDateFormat, InvoiceDate),
      InvoiceHeader.FieldByName('grand_total').AsString]));

  Amount := InvoiceHeader.FieldByName('grand_total').AsCurrency;
  AmountString := FormatCurr('0.00', Amount);

  (*
  Desired Format:
  Level0,SolomonPeriod,B,Amount   (Batch Line - One per locating company export)
  Level1,IN,InvoiceNumber,CustNumber,CurrentDate,InvoiceDate,Amount,DueDays,CustNumber,PeriodEndDate,DueDate   (One Per Customer)
  Level4,GLCode,CustNumber,TaskID,SolomonOffice,CustomerDescr,Amount   (One Per GL/Cust/Task/SolOffice)
  *)
  DueDays := GetDueDays(InvoiceHeader.FieldByName('payment_terms').AsString);
  Assert((DueDays >= 1) and (DueDays <= 365), 'Error: Payment terms is set to ' + IntToStr(DueDays) +  'days (should be 1-365)');

   case DueDays of  //QMANTWO-364
    30: DueDaysCode := '06';
    31: DueDaysCode := '1R';
    45: DueDaysCode := '07';
    46: DueDaysCode := '1U';
    47: DueDaysCode := '2W';
    60: DueDaysCode := '08';
    62: DueDaysCode := '2M';  //QMANTWO-410
    75: DueDaysCode := 'NY';
    90: DueDaysCode := 'NZ';  //QM-280  sr
  end;

  if AmountString <> '0.00' then begin
    FormatComma(
      [ 'Level1',
        'IN',
        BillingInvoice.FieldByName('FullInvoiceNumber').AsString,
        InvoiceHeader.FieldByName('customer_number').AsString,       //qm-653  727
        FormatDateTime(SolomonDateFormat, Date),
        FormatDateTime(SolomonDateFormat, InvoiceDate),
        AmountString,
        DueDaysCode,
        DyComCustomerNumber,       //qm-653
        EndDate,
        FormatDateTime(SolomonDateFormat, InvoiceDate + DueDays)
        ],
       ['Level',
        'IN',
        'InvoiceNumber',
        'CustomerNumber',     //qm-653 727  sr
        'TodayDate',
        'InvoiceDate',
        'Amount',
        '06',
        'Contract',         //qm-653
        'WorkDate',
        'DueDate'
        ]);
  end;

  GLCodeTotals := TStringList.Create;
  OpenAndCheckGLCodes;
  try
    // Write out Level4 records (all term roll-ups PLUS adjustments)
    SolomonSplit.First;
    while not SolomonSplit.Eof do begin
      PC := SolomonSplit.FieldByName('pc').AsString;
      TaskID := SolomonSplit.FieldByName('TaskID').AsString; //QMANTWO-683  12/31/18 sr
      if NewSolExport then
      begin
        insert('0', TaskID, 4);  //QMANTWO-683  12/31/18 sr
        SolomonOffice := SolomonSplit.FieldByName('SolomonOffice').AsString+'0'; //QMANTWO-683 12/4/18 sr
      end
      else
      begin
        SolomonOffice := SolomonSplit.FieldByName('SolomonOffice').AsString;   //profit_center.timesheet_num, the Solomon "profit center"QMANTWO-683
      end;
      Source := SolomonSplit.FieldByName('Source').AsString;
      IsGLCode := SolomonSplit.FieldByName('IsGLCode').AsBoolean;
      PeriodType := InvoiceHeader.FieldByName('period_type').AsString;
      AdditionalLineItems := SolomonSplit.FieldByName('additional_line_items').AsString; //qm-190/213/222
      if ((AdditionalLineItems = 'Additional Units')  //qm-190
        or (AdditionalLineItems = 'LOC12') //qm-213
        or (AnsiContainsText(AdditionalLineItems, 'AU')))  then  //qm-222
        begin
          GLCode := GL_CODE_ADD; //qm-190/213/222
          PerfLog.Start('Adding split to Solomon GLCode'+GLCode+' for additional Units');
      end
      else
      GLCode := GetGLCode(PeriodType, Source, IsGLCode);
//      try
//        qryGLflip.Parameters.ParamByName('TaskID').Value:=TaskID;   //qm-946
//        qryGLflip.open;   //qm-946
//        if not qryGLflip.FieldByName('PL_GLcode').IsNull then   //qm-946
//        GLCode:= qryGLflip.FieldByName('PL_GLcode').AsString;   //qm-946
//      finally
//        qryGLflip.Close; //qm-946
//      end;


      Contract := SolomonSplit.FieldByName('Contract').AsString;
      Amount := SolomonSplit.FieldByName('Amount').AsCurrency;
      AmountString := FormatCurr('0.00', Amount);

      CurrentGLAmount := StrToCurrDef(GLCodeTotals.Values[GLCode], 0);
      GLCodeTotals.Values[GLCode] := CurrToStr(CurrentGLAmount + Amount);

      if IsEmpty(TaskID) or IsEmpty(SolomonOffice) then begin
        FWarningString := 'Missing SolomonOffice / TaskID in Solomon Export';
        ProblemLog.Add('WARNING - Missing TaskID/SolOffice, for PC split to PC: ' + PC);
        PerfLog.Start('WARNING - Missing TaskID/SolOffice, for PC split to PC: ' + PC);
      end;

      FormatComma(
        ['Level4',  // 1
         GLCode,    // 2
         Contract,  // 3
         TaskID,    // 4
         SolomonOffice,    // 5
         CustomerDescription,  // 6
         AmountString          // 7
        ],
        ['Level',
         'GLCode',
         'Contract',
         'TaskID',
         'SolomonOffice',
         'CustomerDescription',
         'Amount'
        ]);

      DataLists.Values[LocatingCompany] := FormatCurr('0.00', StrToFloat(DataLists.Values[LocatingCompany]) + Amount);
      PerfLog.Start('Adding split to Solomon');
      SolomonSplit.Next;
    end;

    SolomonLines := CurrentData.Count;
    // This is just a hack to replace the unknown GL code items (usually
    // adjustments) with otherwise highest value GL code on this invoice
    CurrentData.Text := StringReplace(CurrentData.Text, UnknownGLCode, GetHighestValueGLCode, [rfReplaceAll]);
    Assert(CurrentData.Count = SolomonLines, Format('Expected %d SolomonLines lines; got %d.', [SolomonLines, CurrentData.Count]));
  finally
    FreeAndNil(GLCodeTotals);
  end;

  // See Mantis 1153 and 1288 for more information on file formats and field mappings:
  // http://oasisaccess.com/qmmantis/view.php?id=1153
  // http://oasisaccess.com/qmmantis/view.php?id=1288
end;

procedure TSolomonExportDM.ExportSolomon(const DestinationDir: string;
  const BillID: Integer; var WarningString: string);
begin
  ProblemLog.Add('Saving Solomon export file(s) for billing run ' + IntToStr(BillID));
  PerfLog.Start('Saving Solomon export file(s) for billing run ' + IntToStr(BillID));
  SaveAllData(DestinationDir, BillID);
//{$IFDEF DEBUG}
//  ProblemLog.SaveToFile('C:\Logs\' + IntToStr(BillID) + '-solomon-LOG.txt');
//{$ELSE}
  ProblemLog.SaveToFile(DestinationDir + IntToStr(BillID) + '-solomon-LOG.txt');
//{$ENDIF}
  WarningString := FWarningString;
end;

procedure TSolomonExportDM.SaveAllData(const DestinationDir: string; const BillID: Integer);
var
  i: Integer;
  L: TStringList;
  FileName: string;
  LocatingCompany, debugOutPut: string;
begin
  for i := 0 to DataLists.Count - 1 do begin
    // locating_company_name in the invoice header dataset
    LocatingCompany := DataLists.Names[i];
    L := TStringList(DataLists.Objects[i]);
    InsertLevel0Summary(DataLists.Values[LocatingCompany]);
    FileName := IntToStr(BillID) + '-' + LocatingCompany + '-solomon.txt';
//{$IFDEF DEBUG}
//      debugOutPut := 'C:\Logs\';
//{$ENDIF}

    ProblemLog.Add('Saving Solomon export file: ' + FileName);
    PerfLog.Start('Saving Solomon export file: ' + FileName);
//
//{$IFDEF DEBUG}
//    L.SaveToFile(debugOutPut + FileName, TEncoding.ASCII);
// {$ELSE}
     L.SaveToFile(DestinationDir + FileName, TEncoding.ASCII); //unicode
//{$ENDIF}
    if FExportEmployees then begin
      Filename := IntToStr(BillID) + '-' + LocatingCompany + '-employees.txt';
      ProblemLog.Add('Generating Dynamics employee export file: ' + FileName);
      DoDynamicsExport('export_employees', LocatingCompany, DestinationDir + FileName);
    end;
    if FExportTasks then begin
      Filename := IntToStr(BillID) + '-' + LocatingCompany + '-tasks.txt';
      ProblemLog.Add('Generating Dynamics task export file: ' + FileName);
      DoDynamicsExport('export_tasks3', LocatingCompany, DestinationDir + FileName, BillID);
    end;
  end;
end;

procedure TSolomonExportDM.DoDynamicsExport(const ProcedureName, LocatingCompany, OutputFileName: string;
  const BillID: Integer);
var
  debugOutPut: string;
begin
  DynamicsExport.ProcedureName := ProcedureName;
  DynamicsExport.Parameters.Refresh;
  DynamicsExport.Parameters.ParamByName('@CompanyName').Value := LocatingCompany;
  DynamicsExport.Parameters.ParamByName('@ProfitCenter').Value := '*';
  if Assigned(DynamicsExport.Parameters.FindParam('@StartDate')) then
    DynamicsExport.Parameters.ParamValues['@StartDate'] := FStartDate;
  if Assigned(DynamicsExport.Parameters.FindParam('@EndDate')) then
    DynamicsExport.Parameters.ParamValues['@EndDate'] := Trunc(FEndDate) + 1; // This expects a more normal (non-inclusive) end date
  if Assigned(DynamicsExport.Parameters.FindParam('@BillID')) then
    DynamicsExport.Parameters.ParamValues['@BillID'] := BillID;
  DynamicsExport.Open;
  try
//{$IFDEF DEBUG}
//      debugOutPut := 'C:\Logs\';
//    SaveDelimToFile(DynamicsExport, debugOutPut, '|', False);
//{$ELSE}
    SaveDelimToFile(DynamicsExport, OutputFileName, '|', False);
//{$ENDIF}
  finally
    DynamicsExport.Close;
  end;
end;

procedure TSolomonExportDM.SelectCorrectDataFile(const LocCompanyName: string);
var
  i: Integer;
begin
  i := DataLists.IndexOfName(LocCompanyName);
  if i < 0 then begin
    i := DataLists.Add(LocCompanyName + '=0.00');
    DataLists.Objects[i] := TStringList.Create;
  end;
  CurrentData := TStringList(DataLists.Objects[i]);
end;

procedure TSolomonExportDM.DiscardAttachedLists;
var
  i: Integer;
  L: TStringList;
begin
  if Assigned(DataLists) then
    for i := 0 to DataLists.Count-1 do begin
      L := TStringList(DataLists.Objects[i]);
      L.Free;
    end;
end;

procedure TSolomonExportDM.DataModuleCreate(Sender: TObject);
var
  BillingIni: TIniFile;
  myPath : string;
begin
  myPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0))); //QMANTWO-683
  try
    BillingIni := TIniFile.Create(myPath + 'QManagerBilling.ini');  //QMANTWO-683
    NewSolExport := BillingIni.ReadBool('Billing', 'NewSolExport', True);   //QMANTWO-683
  finally
    FreeAndNil(BillingIni);  //QMANTWO-683
  end;
end;

procedure TSolomonExportDM.DataModuleDestroy(Sender: TObject);
begin
  DiscardAttachedLists;
  FreeAndNil(DataLists);
end;

function TSolomonExportDM.GetGLCode(PeriodType, Source: string; IsGLCode: Boolean): string;
begin
  Assert(NotEmpty(PeriodType), 'PeriodType is empty');
  Assert(StrContainsText(PeriodType, TBillPeriod.GetBillPeriodList),
    Format('PeriodType [%s] is not in BillPeriodList [%s]', [PeriodType, TBillPeriod.GetBillPeriodList]));
  if NotEmpty(Source) and not IsGLCode then
    Assert(StrContainsText(Source, BillingChargeTypes),
      Format('Source [%s] is not in BillingChargeTypes [%s]', [Source, BillingChargeTypes]));

  Result := '';
  if IsEmpty(Source) then
    Result := UnknownGLCode
  else if IsGLCode then begin
    Result := Source;
    AssertValidGLCode(Result);
  end
  else if GLCodes.Locate('charge_type;period_type', VarArrayOf([Source, PeriodType]), []) then begin
    Result := GLCodes.FieldByName('gl_code').AsString;
    AssertValidGLCode(Result);
  end
  else if GLCodes.Locate('charge_type;period_type', VarArrayOf([Source, '*']), []) then begin
    Result := GLCodes.FieldByName('gl_code').AsString;
    AssertValidGLCode(Result);
  end
  else if GLCodes.Locate('charge_type;period_type', VarArrayOf(['*', PeriodType]), []) then begin
    Result := GLCodes.FieldByName('gl_code').AsString;
    AssertValidGLCode(Result);
  end
  else begin
    Result := FDefaultGLCode;
    AssertValidGLCode(Result);
  end;
  Result := Trim(Result);
end;

procedure TSolomonExportDM.OpenAndCheckGLCodes;
begin
  if not GLCodes.Active then begin
    GLCodes.Open;
    if not GLCodes.Locate('charge_type;period_type', VarArrayOf(['*', '*']), []) then
      raise Exception.Create('You must define a default GL code with a period and billing type of *')
    else begin
      FDefaultGLCode := GLCodes.FieldByName('gl_code').AsString;
      AssertValidGLCode(FDefaultGLCode);
    end;
  end;
end;

procedure TSolomonExportDM.AssertValidGLCode(GLCode: string);
begin
  Assert(NotEmpty(GLCode), 'Invalid GL Code: ' + GLCode);
  // Also check numeric only, no spaces, etc. someday?
end;

end.
