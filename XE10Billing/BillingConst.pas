unit BillingConst;

interface

uses SysUtils;

type
  TGetTieredClientFunc = function(const CallCenter, ClientCode: string): Boolean of object;

  EBillingException = class(Exception);
  ENoRateFound = class(EBillingException);

  TRateEntry = record
    Rate: Currency;
    AdditionalRate: Currency;
    BillCode: string;
    LineItemText: string;
    WhichInvoice: string;
    MatchedStatus: string;
    ValueGroupID: Integer;
    AdditionalLineItemText: string;

//    BillingRateID: Integer;  //qm-948
    CWICode:  string;  //qm-948
	  AddlCWICode:  string;  //qm-948
    Rpt_gl:  string;  //qm-995
	  Additional_Rpt_gl:  string;  //qm-995

  end;

const
  ATnTTransOvrd    : array[0..4] of string = ('PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL', 'ATTDNEVADA');  //QMANTWO-594 QM-770
  ExcludeFromLate  : array[0..9] of string = ('H', 'NC', 'ZZZ','BPM','BPH','XA', 'XB', 'XD', 'XI', 'XLW');   // QM-770
  ExcludeStatus    : array[0..1] of string = ('NC', 'ZZZ');//QM-3

  TicketTypeNormal = 'NORMAL';
  TicketTypeEmerg  = 'EMERG';
  TicketTypeAfter  = 'AFTER';
  TicketTypeWatch  = 'WATCH';
  TicketTypeOver   = 'OVER';
  TicketTypeDamage = 'DAMAGE';
  TicketTypeFTTP   = 'FTTP';
  TicketTypeFIOS   = 'FIOS';
  TicketTypeUpdate = 'UPDATE';
  TicketTypeRemark = 'REMARK';   //qm-884 sr
  TicketTypeNoResponse = 'NORESP';
  TicketTypeDesign = 'DESIGN';
  TicketTypeShortNotice = 'SHORT';
  TicketTypeStatusAfterDue = 'LATE';  //QMANTWO-742
  TicketType800LPM   = 'LPM';

  ChargeTypeNormal     = 'NORMAL';
  ChargeTypeHourly     = 'HOURLY';
  ChargeTypeHourlyLP   = 'HRLYLP';
  ChargeTypeHourlySDGE = 'HRLYSDGE';
  ChargeTypeHourlyPGE  = 'HRLYPGE';
  ChargeTypeTrans      = 'TRANSMISSION';
  ChargeTypeBulkTrans  = 'BULK_TRANSMISSION';
  ChargeTypeTax        = 'TAX';
  ChargeTypeFlatFee    = 'FLAT_FEE';
  ChargeTypeIncentive  = 'INCENTIVE';
  ChargeTypeAdjustment = 'ADJUSTMENT';

  BillingChargeTypes  = ChargeTypeNormal +sLineBreak+ ChargeTypeHourly
    +sLineBreak+ ChargeTypeTrans +sLineBreak+ ChargeTypeBulkTrans
    +sLineBreak+ ChargeTypeTax +sLineBreak+ ChargeTypeFlatFee
    +sLineBreak+ ChargeTypeAdjustment +sLineBreak+ ChargeTypeHourlyLP
    +sLineBreak+ ChargeTypeHourlySDGE +sLineBreak+ ChargeTypeHourlyPGE;

  MANUEL_TICKET = 'MAN';

  BillCodeTicketScreenCharge = 'TSC';
  BillCodeVariableRate       = 'VARRATE';
  BillCodeSplitPrice         = 'SPLITPRICE';
  BillCodeBGEGTerm           = 'BGEGTERM';
  BillCodeCOHWTerm           = 'COHWTERM';
  BillCodePUBTerm            = 'PUBTERM';
  BillCodeHourly             = 'HOURLY';
  BillCodeUnits              = 'UNITS';
  BillCodeHP                 = 'HP';
  BillCodeAdditionalQty      = 'ADDLQTY';
  BillCodeOSTrans            = 'OSTRANS';

  ValueGroupCompanyAllegheny = 'CompanyAllegheny';

implementation

end.
