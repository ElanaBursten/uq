unit BillPeriod;

interface

uses
  SysUtils, DB;

type
  TFiscalPeriod = record
    StartDate: TDateTime;
    EndDate: TDateTime;
  end;

  TBillPeriod = class
  private
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FDisplayEndDate: TDateTime;
    FLookBackDate: TDateTime;
    FSpan: string;
    FFiscalPeriods: array of TFiscalPeriod;
    procedure SetDisplayEndDate(D: TDateTime);
    procedure SetSpan(S: string);
    procedure UpdateFields;
    procedure GoBackToAcceptableEndDate(D: TDateTime);
    function DisplayEndDateAcceptable(D: TDateTime): Boolean;
    function BackMonthFrom(D: TDateTime): TDateTime;
    function BackHalfMonthFrom(D: TDateTime): TDateTime;
    function IsFiscalMonthEndDate(D: TDateTime): Boolean;
    function GetFiscalMonthStartDate(EndDate: TDateTime): TDateTime;
    procedure AssertHaveFiscalPeriods;
    procedure AddFiscalPeriod(StartDate, EndDate: TDateTime);
    function BackMonthAndHalfFrom(D: TDateTime): TDateTime;  //QMANTWO-618
  public
    procedure OverrideToSingleDay(D: TDateTime);
    property StartDate: TDateTime read FStartDate;
    property EndDate: TDateTime read FEndDate;
    property LookBackDate: TDateTime read FLookBackDate;
    property DisplayEndDate: TDateTime read FDisplayEndDate write SetDisplayEndDate;
    property Span: string read FSpan write SetSpan;
    constructor Create(EndDate: TDateTime);
    procedure ReadFiscalPeriodsFrom(Data: TDataSet);
    class function GetBillPeriodList: string;
  end;

  EBillPeriodException = class(Exception);

const
  // New periods need to be added everywhere in this file the existing ones are,
  // in the admin billing UI, customer editor, and the client billing UI
  PERIOD_WEEK    = 'WEEK';
  PERIOD_WEEK_A  = 'WEEK_A';  //QMANTWO-779
  PERIOD_WEEK_B  = 'WEEK_B';  //QMANTWO-779
  PERIOD_WEEK_C  = 'WEEK_C';  //QM-990
  PERIOD_WEEK_D  = 'WEEK_D';  //QM-990
  PERIOD_MONTH   = 'MONTH';
  PERIOD_MONTH_A = 'MONTH_A';  //QMANTWO-779
  PERIOD_MONTH_B = 'MONTH_B';  //QMANTWO-779
  PERIOD_MONTH_C = 'MONTH_C';  //QM-978
  PERIOD_MONTH_D = 'MONTH_D';  //QM-978
  PERIOD_FISCALMONTH = 'FISCMON';
  PERIOD_HALF        = 'HALF';
  PERIOD_HALF2       = 'HALF2';   //qm-640
  PERIOD_DAY         = 'DAY';

implementation

uses
  Math, DateUtils, JclDateTime, OdMiscUtils;

const
  DayNotOK = 'Billing period end date does not match period type';
  DayNotADate = 'Billing periods can only end on date boundaries';
  InvalidPeriod = 'Invalid Period Type';

  { TBillPeriod }

constructor TBillPeriod.Create(EndDate: TDateTime);
begin
  inherited Create;
  FSpan := PERIOD_WEEK;
  GoBackToAcceptableEndDate(DateOf(EndDate - 1));
end;

function TBillPeriod.DisplayEndDateAcceptable(D: TDateTime): Boolean;
begin
  if ((FSpan = PERIOD_WEEK) or (FSpan = PERIOD_WEEK_A) or (FSpan = PERIOD_WEEK_B) or (FSpan = PERIOD_WEEK_C) or (FSpan = PERIOD_WEEK_D)) then  //QMANTWO-779 QM-990
    Result := DayOfTheWeek(D) = DaySaturday
  else if ((FSpan = PERIOD_MONTH) or (FSpan = PERIOD_MONTH_A) or (FSpan = PERIOD_MONTH_B)or (FSpan = PERIOD_MONTH_C) or (FSpan = PERIOD_MONTH_D)) then  //QMANTWO-779  QM-978
    Result := MonthOf(D) <> MonthOf(D + 1)
  else if FSpan = PERIOD_FISCALMONTH then
    Result := IsFiscalMonthEndDate(D)
  else if FSpan = PERIOD_DAY then
    Result := True
  else if FSpan = PERIOD_HALF then
    Result := (MonthOf(D) <> MonthOf(D + 1)) or (DayOfDate(D) = 15)
  else if FSpan = PERIOD_HALF2 then  //qm-640
    Result := MonthOf(D) <> MonthOf(D + 1)
  else
    raise EBillPeriodException.Create(InvalidPeriod);
end;

procedure TBillPeriod.GoBackToAcceptableEndDate(D: TDateTime);
var
  Count: Integer;
begin
  Count := 0;
  while not DisplayEndDateAcceptable(D) do begin
    D := D - 1;
    Inc(Count);
    if Count > 500 then
      raise EBillPeriodException.Create('Recursion detected in TBillPeriod.GoBackToAcceptableEndDate');
  end;

  SetDisplayEndDate(D);
end;

procedure TBillPeriod.SetDisplayEndDate(D: TDateTime);
begin
  if D <> Trunc(D) then
    raise EBillPeriodException.Create(DayNotADate);

  if not DisplayEndDateAcceptable(D) then
    raise EBillPeriodException.Create(DayNotOK);

  FDisplayEndDate := D;

  UpdateFields;
end;

procedure TBillPeriod.SetSpan(S: string);
begin
  if not StringInArray(FSpan, [PERIOD_WEEK, PERIOD_WEEK_A, PERIOD_WEEK_B,PERIOD_WEEK_C, PERIOD_WEEK_D, PERIOD_MONTH, PERIOD_MONTH_A, PERIOD_MONTH_B, PERIOD_MONTH_C, PERIOD_MONTH_D, PERIOD_FISCALMONTH, PERIOD_HALF, PERIOD_DAY]) then //QMANTWO-779  QM-978 qm-990
    raise EBillPeriodException.Create('Unknown billing period span/type: ' + S);
  FSpan := S;
  GoBackToAcceptableEndDate(FDisplayEndDate);
end;

function TBillPeriod.BackMonthFrom(D: TDateTime): TDateTime;
var
  M: Integer;
begin
  D := D - 1;
  M := MonthOfDate(D);
  while MonthOfDate(D) = M do
    D := D - 1;
  Result := D + 1;
end;

function TBillPeriod.BackMonthAndHalfFrom(D: TDateTime): TDateTime;   //QMANTWO-618
begin
  D := D - 1;
  while (DayOfDate(D) <> 1) and (DayOfDate(D) <> 16) do
    D := D - 1;
  Result := D;
  Result := IncMonth(D,-1);  //QMANTWO-618
end;

function TBillPeriod.BackHalfMonthFrom(D: TDateTime): TDateTime;
begin
  D := D - 1;
  while (DayOfDate(D) <> 1) and (DayOfDate(D) <> 16) do
    D := D - 1;
  Result := D;
end;

procedure TBillPeriod.UpdateFields;
begin
  FEndDate := FDisplayEndDate + 1;

  if ((FSpan = PERIOD_WEEK) or (FSpan = PERIOD_WEEK_A)or (FSpan = PERIOD_WEEK_B)or (FSpan = PERIOD_WEEK_C)or (FSpan = PERIOD_WEEK_D)) then begin //QMANTWO-779 qm-990
    FStartDate := FEndDate - 7;
    FLookBackDate := FStartDate - 14;

  end else if ((FSpan = PERIOD_MONTH) or (FSpan = PERIOD_MONTH_A) or (FSpan = PERIOD_MONTH_B) or (FSpan = PERIOD_MONTH_C) or (FSpan = PERIOD_MONTH_D)) then begin //QMANTWO-779 QM-978
    FStartDate := BackMonthFrom(FDisplayEndDate);
    FLookBackDate := BackMonthFrom(FStartDate);

  end else if FSpan = PERIOD_FISCALMONTH then begin
    FStartDate := GetFiscalMonthStartDate(FDisplayEndDate);
    FLookBackDate := IncDay(FStartDate, -35);

  end else if FSpan = PERIOD_HALF then begin
    FStartDate := BackHalfMonthFrom(FDisplayEndDate);
    FLookBackDate := BackMonthAndHalfFrom(FStartDate);  //QMANTWO-618

  end else if FSpan = PERIOD_HALF2 then begin
    FStartDate := BackMonthFrom(FDisplayEndDate);
    FLookBackDate := BackMonthFrom(FStartDate);  //qm-640

  end else if FSpan = PERIOD_DAY then begin
    FStartDate := FDisplayEndDate;
    FLookBackDate := FDisplayEndDate;

  end else
    raise EBillPeriodException.Create(InvalidPeriod);
end;

procedure TBillPeriod.OverrideToSingleDay(D: TDateTime);
begin
  FDisplayEndDate := D;
  FEndDate := D + 1;
  FStartDate := D;
  FLookBackDate := D;
end;

function TBillPeriod.IsFiscalMonthEndDate(D: TDateTime): Boolean;
var
  i: Integer;
begin
  AssertHaveFiscalPeriods;
  for i := Low(FFiscalPeriods) to High(FFiscalPeriods) do begin
    if D = FFiscalPeriods[i].EndDate then begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

procedure TBillPeriod.AddFiscalPeriod(StartDate, EndDate: TDateTime);
begin
  SetLength(FFiscalPeriods, Length(FFiscalPeriods) + 1);
  FFiscalPeriods[Length(FFiscalPeriods) - 1].StartDate := StartDate;;
  FFiscalPeriods[Length(FFiscalPeriods) - 1].EndDate := EndDate;;
end;

procedure TBillPeriod.AssertHaveFiscalPeriods;
begin
  if ((not Assigned(FFiscalPeriods)) or (Length(FFiscalPeriods) < 1)) then
    raise EBillPeriodException.Create('Fiscal periods not loaded into TBillPeriod');
end;

function TBillPeriod.GetFiscalMonthStartDate(EndDate: TDateTime): TDateTime;
var
  i: Integer;
begin
  AssertHaveFiscalPeriods;
  for i := Low(FFiscalPeriods) to High(FFiscalPeriods) do begin
    if EndDate = FFiscalPeriods[i].EndDate then begin
      Result := FFiscalPeriods[i].StartDate;
      Exit;
    end;
  end;
  raise EBillPeriodException.CreateFmt('Fiscal period start date for end date %s not found (check the dycom_period table contents)', [DateToStr(EndDate)]);
end;

procedure TBillPeriod.ReadFiscalPeriodsFrom(Data: TDataSet);
begin
  Assert(Assigned(Data));
  if not Data.Active then
    Data.Open;
  Data.First;
  while not Data.Eof do begin
    AddFiscalPeriod(Data.FieldByName('starting').AsDateTime, Data.FieldByName('ending').AsDateTime);
    Data.Next;
  end;
end;

class function TBillPeriod.GetBillPeriodList: string;
begin
  Result := PERIOD_WEEK    +CRLF+
            PERIOD_WEEK_A  +CRLF+ //QMANTWO-779
            PERIOD_WEEK_B  +CRLF+ //QMANTWO-779
            PERIOD_WEEK_C  +CRLF+ //QM-990
            PERIOD_WEEK_D  +CRLF+ //QM-990
            PERIOD_MONTH   +CRLF+
            PERIOD_MONTH_A +CRLF+ //QMANTWO-779
            PERIOD_MONTH_B +CRLF+ //QMANTWO-779
            PERIOD_MONTH_C +CRLF+ //QM-978
            PERIOD_MONTH_D +CRLF+ //QM-978
            PERIOD_FISCALMONTH +CRLF+
            PERIOD_HALF +CRLF+
            PERIOD_HALF2;
end;

end.

