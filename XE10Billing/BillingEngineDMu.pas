unit BillingEngineDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, DBISAMTb, OdMiscUtils;

type
  TBillingEngineDM = class(TDataModule)
    Conn: TADOConnection;
    LocateRawData: TADODataSet;
    TicketRawData: TADODataSet;
    billing_header: TADOTable;
    billing_detail: TADODataSet;
    MakeEmptyTix: TADOCommand;
    AddCentersToTix: TADOCommand;
    DropTix: TADOCommand;
    CountTickets: TADODataSet;
    CountLocates: TADODataSet;
    SumBilling: TADODataSet;
    AddTixIndex: TADOCommand;
    DailyHoursUnits: TADODataSet;
    HoursUnits: TDBISAMTable;
    BilledTickets: TDBISAMTable;
    BilledTicketsTicketNumber: TStringField;
    BilledTicketsTicketID: TIntegerField;
    BilledTicketsClientCode: TStringField;
    TicketQuery: TDBISAMQuery;
    AddDailyTix: TADOCommand;
    TicketNotes: TDBISAMTable;
    TicketNotesData: TADODataSet;
    TicketFileData: TDBISAMTable;
    LocateFileData: TDBISAMTable;
    ReadConn: TADOConnection;
    CheckBillingSetupSP: TADOStoredProc;
    PerfLogData: TADODataSet;
    DeletePriorBilling: TADOStoredProc;
    LocateLookupData: TADODataSet;
    TransDetailSP: TADOStoredProc;
    insBillingAdjustments: TADOQuery;
    insBillingHeader: TADOQuery;
    qryBillID: TADOQuery;
    updBillingHeader: TADOQuery;
    fixWhichInvoice: TADOQuery;
    updDateWhichInvoice: TADOQuery;
    qryBillDetailMinMax: TADOQuery;
    qryTicketDups: TADOQuery;
    updTicketDups: TADOQuery;
    spAdjustNIPSCO: TADOStoredProc;
    spFMB_BGE_Adjustment: TADOStoredProc;
    spDOM408Adjustment: TADOStoredProc;
    spAdjustDOMCOMB: TADOStoredProc;
    insBillingAdjustmentsCal: TADOQuery; //QM-535
    procedure DataModuleCreate(Sender: TObject);
  private
    FLogger: TLogEvent;
    procedure StatusUpdate(Msg: string);
  public
    procedure Disconnect;
    procedure SetBillingTimeout(Timeout: Integer);
    procedure ConnectReadConn(Logger: TLogEvent; IniFileName: string);
  end;

implementation

uses
  OdAdoUtils, ReportConfig;

{$R *.dfm}

{ TBillingEngineDM }

procedure TBillingEngineDM.ConnectReadConn(Logger: TLogEvent; IniFileName: string);
var
  Config: TReportConfig;
begin
  FLogger := Logger;
  FLogger(Self, 'Loading Reporting DB Configuration');
  Config := LoadConfig(IniFileName);
  try
    if Config.ReportingDBsExist then
      Config.ConnectReportingDb(ReadConn, StatusUpdate)
    else begin
      FLogger(Self, 'No reporting DBs configured, using main DB');
      ReadConn.ConnectionString := Conn.ConnectionString;
      ReadConn.Connected := True;
    end;
  finally
    FreeAndNil(Config);
  end;
end;

procedure TBillingEngineDM.DataModuleCreate(Sender: TObject);
begin
  if Conn.Connected then
    raise Exception.Create('ERROR, Conn left on at design time');
  Conn.KeepConnection := True;

  if ReadConn.Connected then
    raise Exception.Create('ERROR, ReadConn left on at design time');
  ReadConn.KeepConnection := True;

  Conn.ConnectionString := 'Connection not initialized yet';
  ReadConn.ConnectionString := 'Connection not initialized yet';

  // Don't rewire everything to Conn, some of it intentionally
  // uses ReadConn.
  // Don't add anything here that requires a DB connection, since it isn't initialized yet
end;

procedure TBillingEngineDM.Disconnect;
begin
  Conn.Connected := False;
  ReadConn.Connected := False;
end;

procedure TBillingEngineDM.SetBillingTimeout(Timeout: Integer);
begin
  AddCentersToTix.CommandTimeout := Timeout;
  LocateRawData.CommandTimeout := Timeout;
  TicketRawData.CommandTimeout := Timeout;
  TicketNotesData.CommandTimeout := Timeout;
  Conn.CommandTimeout := Timeout;
  DailyHoursUnits.CommandTimeout := Timeout;
  AddDailyTix.CommandTimeout := Timeout;
  TransDetailSP.CommandTimeout := Timeout;
end;

procedure TBillingEngineDM.StatusUpdate(Msg: string);
begin
  FLogger(Self, 'Rep DB: ' + Msg);
end;

end.

