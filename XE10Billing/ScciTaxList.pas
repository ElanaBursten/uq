unit ScciTaxList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit,
  cxCurrencyEdit, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, QMBillingDMu,
  dxSkinsCore, dxDateRanges, dxScrollbarAnnotations;

type
  TScciTaxForm = class(TBaseCxListForm)
    Label1: TLabel;
    ColState: TcxGridDBColumn;
    ColCounty: TcxGridDBColumn;
    ColCity: TcxGridDBColumn;
    ColTaxName: TcxGridDBColumn;
    ColTaxRate: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColSCCIId: TcxGridDBColumn;
  private
  public
  end;

implementation

{$R *.dfm}

end.

