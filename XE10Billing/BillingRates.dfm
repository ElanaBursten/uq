object BillingRateTable: TBillingRateTable
  Height = 203
  Width = 266
  object Rates: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    TableName = 'billing_rates'
    Left = 68
    Top = 20
  end
  object AreaCountyCheck: TDBISAMQuery
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    SQL.Strings = (
      'select count(*) from "\Memory\billing_rates"'
      'where '
      '  ((area_name is not null) and (area_name <> '#39#39')) or'
      '  ((work_county is not null) and (work_county <> '#39#39'))')
    Params = <>
    Left = 68
    Top = 80
  end
  object CityCheck: TDBISAMQuery
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    SQL.Strings = (
      'select count(*) from "\Memory\billing_rates"'
      'where'
      '  (work_city is not null) and (work_city <> '#39#39')')
    Params = <>
    Left = 172
    Top = 80
  end
  object MaxPartyValue: TDBISAMQuery
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    SQL.Strings = (
      'select max(parties) from "\Memory\billing_rates"')
    Params = <>
    Left = 172
    Top = 20
  end
end
