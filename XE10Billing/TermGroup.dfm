inherited TermGroupForm: TTermGroupForm
  Left = 257
  Caption = 'Term Groups'
  ClientHeight = 534
  ClientWidth = 822
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 582
    Height = 497
  end
  inherited HeaderPanel: TPanel
    Width = 822
  end
  inherited MasterPanel: TPanel
    Width = 582
    Height = 497
    inherited MasterGrid: TcxGrid
      Top = 43
      Width = 568
      Height = 446
      inherited MasterGridView: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        DataController.KeyFieldNames = 'term_group_id'
        OptionsView.GroupByBox = False
        object ColTermGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'term_group_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 105
        end
        object ColGroupCode: TcxGridDBColumn
          Caption = 'Term Group Code'
          DataBinding.FieldName = 'group_code'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 161
        end
        object ColCenterGroupID: TcxGridDBColumn
          Caption = 'Center Group ID'
          DataBinding.FieldName = 'center_group_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 110
        end
        object ColCenterGroup: TcxGridDBColumn
          Caption = 'Call Centers'
          DataBinding.FieldName = 'center_group'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxExtLookupComboBoxProperties'
          Properties.DropDownAutoSize = True
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.View = CenterGroupLookupView
          Properties.KeyFieldNames = 'group_code'
          Properties.ListFieldItem = CenterGroupLookupViewgroup_code
          Options.Filtering = False
          Width = 148
        end
        object ColComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          Width = 237
        end
      end
      object CenterGroupLookupView: TcxGridDBTableView [1]
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = CenterGroupSource
        DataController.KeyFieldNames = 'group_code'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object CenterGroupLookupViewgroup_code: TcxGridDBColumn
          Caption = 'Center Group'
          DataBinding.FieldName = 'group_code'
          DataBinding.IsNullValueType = True
          SortIndex = 0
          SortOrder = soAscending
          Width = 91
        end
        object CenterGroupLookupViewcomment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          DataBinding.IsNullValueType = True
          Width = 222
        end
      end
    end
  end
  inherited DetailPanel: TPanel
    Left = 591
    Width = 231
    Height = 497
    DesignSize = (
      231
      497)
    inherited DetailGrid: TcxGrid
      Top = 43
      Width = 210
      Height = 446
      inherited DetailGridView: TcxGridDBTableView
        DataController.DataModeController.GridMode = True
        DataController.KeyFieldNames = 'term_group_id;term'
        OptionsView.GroupByBox = False
        object ColDetailTermGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'term_group_id'
          DataBinding.IsNullValueType = True
          Visible = False
        end
        object ColDetailTerm: TcxGridDBColumn
          Caption = 'Term'
          DataBinding.FieldName = 'term'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownRows = 18
          SortIndex = 0
          SortOrder = soAscending
          Width = 179
        end
      end
    end
  end
  inherited Master: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from term_group'#13#10'order by group_code'
  end
  inherited Detail: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from term_group_detail'#13#10'order by term'
    IndexFieldNames = 'term_group_id'
    MasterFields = 'term_group_id'
    Top = 112
  end
  inherited MasterSource: TDataSource
    Left = 108
    Top = 120
  end
  inherited DetailSource: TDataSource
    Left = 308
    Top = 104
  end
  object CenterGroups: TADODataSet [8]
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from center_group'
    Parameters = <>
    Left = 16
    Top = 160
  end
  object CenterGroupSource: TDataSource [9]
    DataSet = CenterGroups
    Left = 116
    Top = 176
  end
end
