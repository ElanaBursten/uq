unit RealTimeBilling;

interface

uses
  SysUtils, Classes, ADODB, DB, BetterADODataSet, OdEmbeddable,
  StdCtrls, Controls, ExtCtrls;

type
  TRealTimeBillingForm = class(TEmbeddableForm)
    LogMemo: TMemo;
    HeaderPanel: TPanel;
    ProcessQueueButton: TButton;
    QueueNumberEdit: TEdit;
    procedure ProcessQueueButtonClick(Sender: TObject);
  private
    procedure  BillingCallBack(Msg: string);
  end;

implementation

uses BillingCalc;

{$R *.dfm}

{ TRealTimeBillingForm }

procedure TRealTimeBillingForm.BillingCallBack(Msg: string);
begin
  LogMemo.Lines.Add(Msg);
end;

procedure TRealTimeBillingForm.ProcessQueueButtonClick(Sender: TObject);
var
  BillingQueue: TBillingCalcDM;
begin
  BillingQueue := TBillingCalcDM.Create(Self);
  try
    BillingQueue.QueueCount := StrToInt(QueueNumberEdit.Text);
    BillingQueue.OnLog := BillingCallBack;
    BillingQueue.Execute;
  finally
    FreeAndNil(BillingQueue);
  end;
end;

end.
