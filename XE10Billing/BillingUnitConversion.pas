unit BillingUnitConversion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, ActnList, StdCtrls,
  DBCtrls, OdDBUtils, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCalendar, cxCheckBox,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxContainer, cxTextEdit,
  cxDropDownEdit, cxDBEdit, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  System.Actions, dxSkinsCore, dxDateRanges, dxScrollbarAnnotations;

type
  TBillingUnitConversionForm = class(TBaseCxListForm)
    ActionList: TActionList;
    NewAction: TAction;
    Panel1: TPanel;
    UnitTypeCombo: TcxDBComboBox;
    FirstUnitFactor: TcxDBMaskEdit;
    RestUnitsFactor: TcxDBMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    FirstUnitTypeLabel: TLabel;
    RestUnitsTypeLabel: TLabel;
    DescriptionLabel: TLabel;
    ConversionDescription: TDBText;
    NewConversionButton: TButton;
    SaveButton: TButton;
    CancelButton: TButton;
    SaveAction: TAction;
    CancelAction: TAction;
    DeleteAction: TAction;
    Instructions: TLabel;
    ColUnitType: TcxGridDBColumn;
    ColDescription: TcxGridDBColumn;
    ColFirstUnitFactor: TcxGridDBColumn;
    ColRestUnitsFactor: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColInsertDate: TcxGridDBColumn;
    ColUnitConversionId: TcxGridDBColumn;
    ColModifiedBy: TcxGridDBColumn;
    ColAddedBy: TcxGridDBColumn;
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure NewActionExecute(Sender: TObject);
    procedure UnitTypeComboChange(Sender: TObject);
    procedure CancelActionExecute(Sender: TObject);
    procedure DeleteActionExecute(Sender: TObject);
    procedure SaveActionExecute(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure DataCalcFields(DataSet: TDataSet);
  end;

implementation

uses QMBillingDMu;

{$R *.dfm}

procedure TBillingUnitConversionForm.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  NewAction.Enabled := not (DS.State in dsEditModes);
  DeleteAction.Enabled := (not DS.Dataset.IsEmpty) and (DS.State = dsBrowse);
  SaveAction.Enabled := (DS.State in dsEditModes);
  CancelAction.Enabled := (DS.State in dsEditModes);
end;

procedure TBillingUnitConversionForm.UnitTypeComboChange(Sender: TObject);
begin
  FirstUnitTypeLabel.Caption := UnitTypeCombo.Text;
  RestUnitsTypeLabel.Caption := UnitTypeCombo.Text;
end;

procedure TBillingUnitConversionForm.SaveActionExecute(Sender: TObject);
begin
  Data.Post;
end;

procedure TBillingUnitConversionForm.CancelActionExecute(Sender: TObject);
begin
  Data.Cancel;
end;

procedure TBillingUnitConversionForm.NewActionExecute(Sender: TObject);
begin
  Data.Append;
  if UnitTypeCombo.CanFocus then
    UnitTypeCombo.SetFocus;
end;

procedure TBillingUnitConversionForm.DeleteActionExecute(Sender: TObject);
begin
  Data.Delete;
end;

procedure TBillingUnitConversionForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  if Dataset.State = dsInsert then begin
    Dataset.FieldByName('active').Value := True;
    Dataset.FieldByName('insert_date').Value := Now;
  end;

  Dataset.FieldByName('modified_date').Value := Now;
end;

procedure TBillingUnitConversionForm.FormCreate(Sender: TObject);
begin
  inherited;
  Data.CommandText := 'select bu.* from billing_unit_conversion bu';
end;

procedure TBillingUnitConversionForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddCalculatedField('Description', Dataset, TStringField, 100);
end;

procedure TBillingUnitConversionForm.DataCalcFields(DataSet: TDataSet);
begin
  if (Dataset.FieldByName('first_unit_factor').AsInteger < 1) then
    Dataset.FieldByName('description').Value := 'Bill 1 regardless of length'
  else if Dataset.FieldByName('rest_units_factor').IsNull then
    Dataset.FieldByName('description').Value := Format('Bill 1 every %d %s',
      [Dataset.FieldByName('first_unit_factor').AsInteger, Dataset.FieldByName('unit_type').AsString])
  else
    Dataset.FieldByName('description').Value := Format('Bill 1 first %d %s; 1 each additional %d %s',
      [Dataset.FieldByName('first_unit_factor').AsInteger, Dataset.FieldByName('unit_type').AsString,
      Dataset.FieldByName('rest_units_factor').AsInteger, Dataset.FieldByName('unit_type').AsString]);
end;

end.

