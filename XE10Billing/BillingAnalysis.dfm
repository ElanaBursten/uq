inherited BillingAnalysisForm: TBillingAnalysisForm
  Caption = 'Billing Analysis'
  ClientWidth = 753
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 753
  end
  inherited Grid: TcxGrid
    Width = 753
    inherited GridView: TcxGridDBTableView
      FindPanel.DisplayMode = fpdmAlways
      DataController.KeyFieldNames = 'billing_detail_id'
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = 'Rate $0.0'
          Kind = skSum
          FieldName = 'rate'
          Column = Gridrate
          DisplayText = 'Rate'
        end
        item
          Format = 'Price $0.0'
          Kind = skSum
          FieldName = 'price'
          Column = Gridprice
          DisplayText = 'Price'
        end
        item
          Format = 'Marked 0'
          Kind = skSum
          FieldName = 'qty_marked'
          Column = Gridqty_marked
          DisplayText = 'Marked'
        end
        item
          Format = 'Charged 0'
          Kind = skSum
          FieldName = 'qty_charged'
          Column = Gridqty_charged
          DisplayText = 'Charged'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = 'Count 0'
          Kind = skCount
          FieldName = 'locate_id'
          Column = Gridlocate_id
        end
        item
          Format = 'Avg 0.00'
          Kind = skAverage
          FieldName = 'rate'
          Column = Gridrate
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'price'
          Column = Gridprice
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'qty_marked'
          Column = Gridqty_marked
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'qty_charged'
          Column = Gridqty_charged
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'hours'
        end
        item
          Format = 'Avg 0.00'
          Kind = skAverage
          FieldName = 'tax_rate'
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'tax_amount'
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'raw_units'
        end>
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = Gridticket_number
      OptionsCustomize.ColumnFiltering = True
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = True
      object Gridbilling_cc: TcxGridDBColumn
        Caption = 'Term'
        DataBinding.FieldName = 'billing_cc'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 54
      end
      object Gridticket_number: TcxGridDBColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 95
      end
      object Gridrevision: TcxGridDBColumn
        Caption = 'Revision'
        DataBinding.FieldName = 'revision'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 66
      end
      object Gridstatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 40
      end
      object Gridticket_type: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'ticket_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 90
      end
      object Gridticket_id: TcxGridDBColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 50
      end
      object Gridlocate_id: TcxGridDBColumn
        Caption = 'Loc ID'
        DataBinding.FieldName = 'locate_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 38
      end
      object Gridrate: TcxGridDBColumn
        Caption = 'Rate'
        DataBinding.FieldName = 'rate'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 52
      end
      object Gridprice: TcxGridDBColumn
        Caption = 'Price'
        DataBinding.FieldName = 'price'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object Gridbill_code: TcxGridDBColumn
        Caption = 'Bill Code'
        DataBinding.FieldName = 'bill_code'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 47
      end
      object Gridbucket: TcxGridDBColumn
        Caption = 'Bucket'
        DataBinding.FieldName = 'bucket'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 104
      end
      object Gridline_item_text: TcxGridDBColumn
        Caption = 'Line Item Text'
        DataBinding.FieldName = 'line_item_text'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 120
      end
      object Gridqty_marked: TcxGridDBColumn
        Caption = 'Qty Mark'
        DataBinding.FieldName = 'qty_marked'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 66
      end
      object Gridhours: TcxGridDBColumn
        Caption = 'Hours'
        DataBinding.FieldName = 'hours'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 1
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 32
      end
      object Gridqty_charged: TcxGridDBColumn
        Caption = 'Qty Chg'
        DataBinding.FieldName = 'qty_charged'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 61
      end
      object Gridcall_date: TcxGridDBColumn
        Caption = 'Call Date'
        DataBinding.FieldName = 'call_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 89
      end
      object Gridwork_date: TcxGridDBColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'work_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 104
      end
      object Gridcon_name: TcxGridDBColumn
        Caption = 'Contractor'
        DataBinding.FieldName = 'con_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 100
      end
      object Gridclient_id: TcxGridDBColumn
        Caption = 'Client ID'
        DataBinding.FieldName = 'client_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridlocator_id: TcxGridDBColumn
        Caption = 'Locator ID'
        DataBinding.FieldName = 'locator_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 56
      end
      object Gridwork_done_for: TcxGridDBColumn
        Caption = 'Work Done For'
        DataBinding.FieldName = 'work_done_for'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 103
      end
      object Gridclosed_date: TcxGridDBColumn
        Caption = 'Closed Date'
        DataBinding.FieldName = 'closed_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Gridclosed_how: TcxGridDBColumn
        Caption = 'Closed How'
        DataBinding.FieldName = 'closed_how'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 63
      end
      object Griddue_date: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Gridtransmit_date: TcxGridDBColumn
        Caption = 'Transmit Date'
        DataBinding.FieldName = 'transmit_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Gridwork_type: TcxGridDBColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 113
      end
      object Gridwork_lat: TcxGridDBColumn
        Caption = 'Work Lat'
        DataBinding.FieldName = 'work_lat'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 4
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridwork_long: TcxGridDBColumn
        Caption = 'Work Long'
        DataBinding.FieldName = 'work_long'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 4
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 55
      end
      object Gridwork_cross: TcxGridDBColumn
        Caption = 'Work Cross'
        DataBinding.FieldName = 'work_cross'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 85
      end
      object Gridwork_address_number: TcxGridDBColumn
        Caption = 'Address Number'
        DataBinding.FieldName = 'work_address_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 113
      end
      object Gridwork_address_number_2: TcxGridDBColumn
        Caption = 'Address Number 2'
        DataBinding.FieldName = 'work_address_number_2'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 125
      end
      object Gridwork_address_street: TcxGridDBColumn
        Caption = 'Addr Street Name'
        DataBinding.FieldName = 'work_address_street'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 274
      end
      object Gridwork_city: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 82
      end
      object Gridwork_state: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'work_state'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 58
      end
      object Gridwork_county: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 72
      end
      object Gridinitial_status_date: TcxGridDBColumn
        Caption = 'Initial Status Date'
        DataBinding.FieldName = 'initial_status_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        FooterAlignmentHorz = taRightJustify
        Width = 112
      end
      object Gridemp_number: TcxGridDBColumn
        Caption = 'Emp Number'
        DataBinding.FieldName = 'emp_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 94
      end
      object Gridshort_name: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 124
      end
      object Gridwork_description: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'work_description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 114
      end
      object Gridarea_name: TcxGridDBColumn
        Caption = 'Area Name'
        DataBinding.FieldName = 'area_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 82
      end
      object Gridtransmit_count: TcxGridDBColumn
        Caption = 'Transmit Count'
        DataBinding.FieldName = 'transmit_count'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 75
      end
      object Gridmap_ref: TcxGridDBColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 72
      end
      object Gridlocate_hours_id: TcxGridDBColumn
        Caption = 'Locate Hours ID'
        DataBinding.FieldName = 'locate_hours_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 81
      end
      object Gridwhich_invoice: TcxGridDBColumn
        Caption = 'Which Invoice'
        DataBinding.FieldName = 'which_invoice'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 124
      end
      object Gridtax_name: TcxGridDBColumn
        Caption = 'Tax Name'
        DataBinding.FieldName = 'tax_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 77
      end
      object Gridtax_rate: TcxGridDBColumn
        Caption = 'Tax Rate'
        DataBinding.FieldName = 'tax_rate'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridtax_amount: TcxGridDBColumn
        Caption = 'Tax Amount'
        DataBinding.FieldName = 'tax_amount'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 124
      end
      object Gridraw_units: TcxGridDBColumn
        Caption = 'Raw Units'
        DataBinding.FieldName = 'raw_units'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 78
      end
      object Gridupdate_of: TcxGridDBColumn
        Caption = 'Update Of'
        DataBinding.FieldName = 'update_of'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 77
      end
      object Gridhigh_profile: TcxGridDBColumn
        Caption = 'High Profile'
        DataBinding.FieldName = 'high_profile'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 100
      end
      object Gridbill_id: TcxGridDBColumn
        Caption = 'Bill ID'
        DataBinding.FieldName = 'bill_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 32
      end
      object GridParent_id: TcxGridDBColumn
        DataBinding.FieldName = 'parent_ticket_id'
      end
      object Gridbilling_detail_id: TcxGridDBColumn
        Caption = 'Billing Detail ID'
        DataBinding.FieldName = 'billing_detail_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 75
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select bd.*, t.ticket_id, t.work_date'#13#10'from billing_detail bd'#13#10' ' +
      ' left join locate l on l.locate_id = bd.locate_id'#13#10'  left join t' +
      'icket t on t.ticket_id = l.ticket_id'#13#10'where bd.bill_id = :BillID' +
      ' and ((bd.area_name <> '#39'DEL_GAS'#39') or (bd.area_name is null))'
    Parameters = <
      item
        Name = 'BillID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
  end
end
