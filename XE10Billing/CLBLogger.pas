unit CLBLogger;

interface

uses
  Classes, SysUtils;

type
  TCLBLogger = class(TObject)
    LogList: TStringList;
    Directory: string;
    FileName: string;
    procedure Log(Sender: TObject; const Msg: string); overload;
    procedure Log(Msg: string); overload;
    procedure SaveToFile(const AFileName: string = '');
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  OdMiscUtils;

{ TCLBLogger }

constructor TCLBLogger.Create;
begin
  inherited;
  LogList := TStringList.Create;
  FileName := 'Log.txt';
  Directory := GetCurrentDir;
end;

procedure TCLBLogger.SaveToFile(const AFileName: string);
var
  LogFile: string;
begin
  LogFile := Trim(AFileName);
  if LogFile = '' then
    LogList.SaveToFile(AddSlash(Directory) + FileName)
  else
    LogList.SaveToFile(AFileName);
end;

destructor TCLBLogger.Destroy;
begin
  FreeAndNil(LogList);
  inherited;
end;

procedure TCLBLogger.Log(Sender: TObject; const Msg: string);
begin
  Writeln(Msg);
  LogList.Add(Msg);
end;

procedure TCLBLogger.Log(Msg: string);
begin
  Log(nil, Msg);
end;

end.

