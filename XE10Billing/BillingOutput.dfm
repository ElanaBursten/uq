inherited BillingOutputForm: TBillingOutputForm
  Left = 388
  Top = 239
  Caption = 'Billing Output Configuration'
  ClientHeight = 509
  ClientWidth = 1050
  TextHeight = 13
  object Splitter: TSplitter [0]
    Left = 0
    Top = 351
    Width = 1050
    Height = 5
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
  end
  inherited TopPanel: TPanel
    Width = 1050
  end
  inherited Grid: TcxGrid
    Width = 1050
    Height = 310
    object GridBOCView: TcxGridDBBandedTableView [1]
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = DS
      DataController.KeyFieldNames = 'output_config_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColCenterGroupLookup: TcxGridDBBandedColumn
        Caption = 'Call Centers'
        DataBinding.FieldName = 'CenterGroupLookup'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 16
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.ListColumns = <>
        Properties.ListOptions.ShowHeader = False
        Visible = False
        GroupIndex = 0
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColCustomerLookup: TcxGridDBBandedColumn
        Caption = 'Customer'
        DataBinding.FieldName = 'CustomerLookup'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 16
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.ListColumns = <>
        Properties.ListOptions.ShowHeader = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 208
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object Colcomment: TcxGridDBBandedColumn
        Caption = 'Comments'
        DataBinding.FieldName = 'comment'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 208
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object Colwhich_invoice: TcxGridDBBandedColumn
        Caption = 'Which Inv.'
        DataBinding.FieldName = 'which_invoice'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 123
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object Coloutput_template: TcxGridDBBandedColumn
        Caption = 'Output Template'
        DataBinding.FieldName = 'output_template'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object Colcustomer_number: TcxGridDBBandedColumn
        Caption = 'Customer #'
        DataBinding.FieldName = 'customer_number'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 102
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object Colcontract: TcxGridDBBandedColumn
        Caption = 'Contract'
        DataBinding.FieldName = 'contract'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 102
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object Colflat_fee: TcxGridDBBandedColumn
        Caption = 'Flat Fee'
        DataBinding.FieldName = 'flat_fee'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00;#'
        Properties.Nullable = False
        Options.Filtering = False
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object Colpayment_terms: TcxGridDBBandedColumn
        Caption = 'Payment Terms'
        DataBinding.FieldName = 'payment_terms'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          '30 days'
          '31 days'
          '45 days'
          '47 days'
          '60 days'
          '62 days'
          '75 days'
          '90 days')
        Options.Filtering = False
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object Coloutput_fields: TcxGridDBBandedColumn
        Caption = 'output fields'
        DataBinding.FieldName = 'output_fields'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          '30 days'
          '31 days'
          '45 days'
          '47 days'
          '60 days'
          '75 days')
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 90
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object Colcust_po_num: TcxGridDBBandedColumn
        Caption = 'Cust. PO #'
        DataBinding.FieldName = 'cust_po_num'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 123
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object Colconsol_line_item: TcxGridDBBandedColumn
        Caption = 'Consol Line Item'
        DataBinding.FieldName = 'consol_line_item'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object Coloutput_config_id: TcxGridDBBandedColumn
        Caption = 'ID'
        DataBinding.FieldName = 'output_config_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          '30 days'
          '31 days'
          '45 days'
          '47 days'
          '60 days'
          '75 days')
        Visible = False
        Options.Filtering = False
        Width = 90
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object Colgroup_by_price: TcxGridDBBandedColumn
        Caption = 'Group By Price'
        DataBinding.FieldName = 'group_by_price'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 102
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object Colomit_zero_amount: TcxGridDBBandedColumn
        Caption = 'Omit 0 Items'
        DataBinding.FieldName = 'omit_zero_amount'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 102
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object Coltransmission_billing: TcxGridDBBandedColumn
        Caption = 'Trans CA'
        DataBinding.FieldName = 'transmission_billing'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
      object Coltransmission_bulk: TcxGridDBBandedColumn
        Caption = 'Trans Bulk'
        DataBinding.FieldName = 'transmission_bulk'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 1
      end
      object Colsales_tax_included: TcxGridDBBandedColumn
        Caption = 'S Tax Inc'
        DataBinding.FieldName = 'sales_tax_included'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 1
      end
      object Colplat_map_prefix: TcxGridDBBandedColumn
        Caption = 'Plat Map Prefix'
        DataBinding.FieldName = 'plat_map_prefix'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
    end
    inherited GridLevel: TcxGridLevel
      GridView = GridBOCView
    end
  end
  object FooterPanel: TPanel [3]
    Left = 0
    Top = 356
    Width = 1050
    Height = 153
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      1050
      153)
    object Label1: TLabel
      Left = 96
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Output Fields:'
    end
    object Label2: TLabel
      Left = 384
      Top = 8
      Width = 169
      Height = 13
      Caption = 'Percentage Breakdowns (optional):'
    end
    object ShowCustomerID: TDBText
      Left = 815
      Top = 24
      Width = 88
      Height = 17
      DataField = 'customer_id'
      DataSource = DS
    end
    object CustomerIDLabel: TLabel
      Left = 758
      Top = 24
      Width = 40
      Height = 13
      Caption = 'Cust ID:'
    end
    object ShowOutputConfigID: TDBText
      Left = 815
      Top = 39
      Width = 88
      Height = 17
      DataField = 'output_config_id'
      DataSource = DS
    end
    object OutputConfigIDLabel: TLabel
      Left = 758
      Top = 39
      Width = 52
      Height = 13
      Caption = 'Output ID:'
    end
    object FieldNames: TCheckListBox
      Left = 96
      Top = 24
      Width = 161
      Height = 113
      Anchors = [akLeft, akTop, akBottom]
      ItemHeight = 17
      TabOrder = 0
      OnClickCheck = FieldNamesClickCheck
    end
    object UpButton: TButton
      Left = 265
      Top = 24
      Width = 104
      Height = 25
      Caption = 'Up'
      TabOrder = 1
      OnClick = UpButtonClick
    end
    object DownButton: TButton
      Left = 265
      Top = 56
      Width = 104
      Height = 25
      Caption = 'Down'
      TabOrder = 2
      OnClick = DownButtonClick
    end
    object SaveContentsButton: TButton
      Left = 265
      Top = 112
      Width = 104
      Height = 25
      Caption = 'Save Field Setup'
      TabOrder = 3
      OnClick = SaveContentsButtonClick
    end
    object BreakdownGrid: TDBGrid
      Left = 384
      Top = 24
      Width = 361
      Height = 113
      DataSource = BreakdownDS
      TabOrder = 4
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'explanation'
          Title.Caption = 'Explanation'
          Width = 178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ratio'
          Title.Caption = 'Ratio'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'output_config_id'
          ReadOnly = True
          Title.Caption = 'OC ID'
          Width = 42
          Visible = True
        end>
    end
    object EditDetailButton: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Edit  >>>'
      TabOrder = 5
      OnClick = EditDetailButtonClick
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    AfterScroll = DataAfterScroll
    CommandText = 'select * from billing_output_config order by center_group_id'
  end
  object Customer: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from customer'#13#10' order by customer_name'
    Parameters = <>
    Left = 56
    Top = 152
  end
  object CustomerDS: TDataSource
    DataSet = Customer
    Left = 96
    Top = 152
  end
  object BillingDetail: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select '#13#10' billing_cc, ticket_number, status,  qty_marked, bucket' +
      ', price,'#13#10' work_address_number, work_address_number_2, work_addr' +
      'ess_street,'#13#10' work_lat, work_long, area_name, transmit_date, tic' +
      'ket_type, due_date,'#13#10' initial_status_date, closed_date, work_cro' +
      'ss, rate, locate_id,'#13#10' bd.billing_detail_id, bd.bill_id, work_st' +
      'ate, work_county, work_city,'#13#10' con_name, work_done_for, work_typ' +
      'e, emp_number, short_name,'#13#10' call_date, transmit_count, hours, l' +
      'ine_item_text, revision, map_ref,'#13#10' locate_hours_id, work_descri' +
      'ption, work_remarks, '#13#10' bd.tax_name, bd.tax_rate, bd.tax_amount'#13 +
      #10'from billing_detail bd'#13#10' inner join billing_header bh on bd.bil' +
      'l_id=bh.bill_id'#13#10'where 0=1 and ((area_name <> '#39'DEL_GAS'#39')or (area' +
      '_name is null))'
    Parameters = <>
    Left = 72
    Top = 436
  end
  object CallCenterGroups: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select * from center_group'#13#10' where show_for_billing = 1'#13#10'order b' +
      'y group_code'
    Parameters = <>
    Left = 56
    Top = 192
  end
  object CallCenterFamiliesDS: TDataSource
    DataSet = CallCenterGroups
    Left = 96
    Top = 192
  end
  object LocatingCompany: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select company_id, name from locating_company '#13#10'order by name'
    Parameters = <>
    Left = 56
    Top = 232
  end
  object LocatingCompanyDS: TDataSource
    DataSet = LocatingCompany
    Left = 96
    Top = 232
  end
  object Breakdown: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    OnNewRecord = BreakdownNewRecord
    CommandText = 
      'select * from billing_breakdown'#13#10' where output_config_id = :outp' +
      'ut_config_id'#13#10' order by 3'
    IndexFieldNames = 'output_config_id'
    Parameters = <
      item
        Name = 'output_config_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 224
    Top = 112
    object Breakdownbilling_breakdown_id: TAutoIncField
      FieldName = 'billing_breakdown_id'
      ReadOnly = True
    end
    object Breakdownoutput_config_id: TIntegerField
      FieldName = 'output_config_id'
    end
    object Breakdownexplanation: TStringField
      FieldName = 'explanation'
      Size = 60
    end
    object Breakdownratio: TBCDField
      FieldName = 'ratio'
      EditFormat = '0.0000'
      Precision = 6
    end
  end
  object BreakdownDS: TDataSource
    DataSet = Breakdown
    Left = 280
    Top = 112
  end
end
