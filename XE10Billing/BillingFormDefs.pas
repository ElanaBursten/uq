unit BillingFormDefs;

interface

uses Classes, Contnrs, OdVclUtils, IniFiles, OdEmbeddable;

type
  TBillingFormClass = class of TEmbeddableForm;

  TBillingFormDef = class
    DisplayName: string;
    Group: string;
    FormClass: TBillingFormClass;
  end;

  function LookupFormClass(DisplayName: string): TBillingFormClass;

var
  Defs: TObjectList;

implementation

uses
  SysUtils,
  BillingRunnerMain, RateSetup, BillingReview, TermGroup, CallCenterGroup,
  ValueGroup, BillingOutput, BillingInvoiceFormats, BillingUnitConversion,
  RealTimeBilling, ScciList, ScciTaxList, BillingGL, BillingCCMap;

var
  Group: string;

function LookupFormClass(DisplayName: string): TBillingFormClass;
var
  Holder: TBillingFormDef;
  I: Integer;
begin
  for I := 0 to Defs.Count-1 do begin
    Holder := Defs.Items[I] as TBillingFormDef;
    if Holder.DisplayName = DisplayName then begin
      Result := Holder.FormClass;
      Exit;
    end;
  end;

  raise Exception.Create('ERROR: Don''t know how to create form: ' + DisplayName);
end;

procedure RegisterBilling(DisplayName: string; ImplClass: TBillingFormClass);
var
  Holder: TBillingFormDef;
begin
  if not Assigned(Defs) then
    Defs := TObjectList.Create;

  Holder := TBillingFormDef.Create;
  Holder.DisplayName := DisplayName;
  Holder.FormClass := ImplClass;
  Holder.Group := Group;
  Defs.Add(Holder);
end;

function AdminFormCompare(Item1, Item2: Pointer): Integer;
begin
  Result := CompareStr(TBillingFormDef(Item1).DisplayName,
                       TBillingFormDef(Item2).DisplayName);
end;

initialization
  Group := 'Billing';
  RegisterBilling('Run', TBillingRunnerMainForm);
  RegisterBilling('Review', TBillingReviewForm);
  RegisterBilling('Rate Setup', TRateSetupForm);
  RegisterBilling('Term Groups', TTermGroupForm);
  RegisterBilling('Value Groups', TValueGroupForm);
  RegisterBilling('Output Config', TBillingOutputForm);
  RegisterBilling('Real Time', TRealTimeBillingForm);
  RegisterBilling('Invoice Formats', TBillingInvoiceFormatsForm);
  RegisterBilling('Unit Conversions', TBillingUnitConversionForm);
  RegisterBilling('County City Grouping', TScciForm);
  RegisterBilling('County City Taxes', TScciTaxForm);
  RegisterBilling('Billing CC Map', TBillingCCMapForm);
  RegisterBilling('GL Codes', TBillingGLForm);
  RegisterBilling('Call Center Groups', TCallCenterGroupForm);

finalization
  FreeAndNil(Defs);

end.
