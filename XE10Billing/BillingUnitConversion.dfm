inherited BillingUnitConversionForm: TBillingUnitConversionForm
  Caption = 'Billing - Unit Conversions'
  OnCreate = FormCreate
  TextHeight = 13
  inherited TopPanel: TPanel
    object Instructions: TLabel
      Left = 8
      Top = 5
      Width = 491
      Height = 26
      Caption = 
        'Create and edit rules on how to convert length marked into a qua' +
        'ntity to bill. Assign these to Terms in the Customer / Terms scr' +
        'een. An entry with 0 for first billing unit means bill 1 regardl' +
        'ess of length.'
      WordWrap = True
    end
  end
  inherited Grid: TcxGrid
    Height = 188
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Visible = True
      DataController.KeyFieldNames = 'unit_conversion_id'
      OptionsData.Appending = False
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      object ColUnitType: TcxGridDBColumn
        Caption = 'Unit Type'
        DataBinding.FieldName = 'unit_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Width = 86
      end
      object ColDescription: TcxGridDBColumn
        DataBinding.FieldName = 'Description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Width = 413
      end
      object ColFirstUnitFactor: TcxGridDBColumn
        DataBinding.FieldName = 'first_unit_factor'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 78
      end
      object ColRestUnitsFactor: TcxGridDBColumn
        DataBinding.FieldName = 'rest_units_factor'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 84
      end
      object ColModifiedDate: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 61
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        HeaderAlignmentHorz = taCenter
        MinWidth = 16
        Options.Filtering = False
        Width = 53
      end
      object ColInsertDate: TcxGridDBColumn
        DataBinding.FieldName = 'insert_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 61
      end
      object ColUnitConversionId: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'unit_conversion_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 46
      end
      object ColModifiedBy: TcxGridDBColumn
        DataBinding.FieldName = 'modified_by'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 42
      end
      object ColAddedBy: TcxGridDBColumn
        DataBinding.FieldName = 'added_by'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 42
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 229
    Width = 544
    Height = 170
    Align = alBottom
    Caption = ' '
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 53
      Width = 89
      Height = 13
      Caption = 'Units measured in:'
    end
    object Label2: TLabel
      Left = 6
      Top = 72
      Width = 120
      Height = 26
      AutoSize = False
      Caption = 'Measurement to first billing unit:'
      WordWrap = True
    end
    object Label3: TLabel
      Left = 6
      Top = 103
      Width = 112
      Height = 32
      AutoSize = False
      Caption = 'Measurement between remaining billing units:'
      WordWrap = True
    end
    object FirstUnitTypeLabel: TLabel
      Left = 257
      Top = 82
      Width = 3
      Height = 13
      Caption = ' '
    end
    object RestUnitsTypeLabel: TLabel
      Left = 257
      Top = 110
      Width = 3
      Height = 13
      Caption = ' '
    end
    object DescriptionLabel: TLabel
      Left = 65
      Top = 139
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Description:'
    end
    object ConversionDescription: TDBText
      Left = 134
      Top = 139
      Width = 107
      Height = 13
      AutoSize = True
      DataField = 'Description'
      DataSource = DS
    end
    object UnitTypeCombo: TcxDBComboBox
      Left = 132
      Top = 49
      DataBinding.DataField = 'unit_type'
      DataBinding.DataSource = DS
      Properties.Items.Strings = (
        'Units'
        'Feet')
      Properties.OnChange = UnitTypeComboChange
      TabOrder = 0
      Width = 120
    end
    object FirstUnitFactor: TcxDBMaskEdit
      Left = 132
      Top = 78
      DataBinding.DataField = 'first_unit_factor'
      DataBinding.DataSource = DS
      TabOrder = 1
      Width = 120
    end
    object RestUnitsFactor: TcxDBMaskEdit
      Left = 132
      Top = 106
      DataBinding.DataField = 'rest_units_factor'
      DataBinding.DataSource = DS
      TabOrder = 2
      Width = 120
    end
    object NewConversionButton: TButton
      Left = 8
      Top = 8
      Width = 110
      Height = 25
      Action = NewAction
      TabOrder = 3
    end
    object SaveButton: TButton
      Left = 128
      Top = 8
      Width = 110
      Height = 25
      Action = SaveAction
      TabOrder = 4
    end
    object CancelButton: TButton
      Left = 248
      Top = 8
      Width = 110
      Height = 25
      Action = CancelAction
      TabOrder = 5
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforePost = DataBeforePost
    OnCalcFields = DataCalcFields
    CommandText = 
      'select bu.*, CASE'#13#10'  WHEN bu.rest_units_factor IS  Null THEN '#39'Bi' +
      'll 1 every '#39' + cast(bu.first_unit_factor as varchar(10)) + '#39' '#39' +' +
      ' bu.unit_type'#13#10'  ELSE  '#39'Bill 1 for first '#39' + cast(bu.first_unit_' +
      'factor as varchar(10)) + '#39' '#39' + bu.unit_type + '#39'; 1 every '#39' + cas' +
      't(bu.rest_units_factor as varchar(10)) + '#39' '#39' + bu.unit_type + '#39' ' +
      'beyond that'#39#13#10'END as Description'#13#10'from billing_unit_conversion b' +
      'u order by bu.unit_conversion_id'
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 224
    Top = 96
    object NewAction: TAction
      Caption = 'New Conversion'
      OnExecute = NewActionExecute
    end
    object SaveAction: TAction
      Caption = 'Save'
      OnExecute = SaveActionExecute
    end
    object CancelAction: TAction
      Caption = 'Cancel'
      OnExecute = CancelActionExecute
    end
    object DeleteAction: TAction
      Caption = 'Deactivate Selected'
      OnExecute = DeleteActionExecute
    end
  end
end
