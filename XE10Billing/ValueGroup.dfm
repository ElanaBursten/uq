inherited ValueGroupForm: TValueGroupForm
  Left = 284
  Top = 214
  Caption = 'Value Groups'
  ClientHeight = 445
  ClientWidth = 811
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 421
    Height = 408
  end
  inherited HeaderPanel: TPanel
    Width = 811
  end
  inherited MasterPanel: TPanel
    Width = 421
    Height = 408
    inherited MasterGrid: TcxGrid
      Top = 43
      Width = 407
      Height = 358
      inherited MasterGridView: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        OptionsView.GroupByBox = False
        object ColMasterValueGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'value_group_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 353
        end
        object ColMasterGroupCode: TcxGridDBColumn
          Caption = 'Code'
          DataBinding.FieldName = 'group_code'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 126
        end
        object ColMasterComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          Width = 258
        end
      end
    end
  end
  inherited DetailPanel: TPanel
    Left = 430
    Width = 381
    Height = 408
    inherited DetailGrid: TcxGrid
      Top = 43
      Width = 364
      Height = 358
      inherited DetailGridView: TcxGridDBTableView
        DataController.KeyFieldNames = 'value_group_id;match_type;match_value'
        OptionsView.GroupByBox = False
        object ColDetailValueGroupID: TcxGridDBColumn
          DataBinding.FieldName = 'value_group_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 79
        end
        object ColDetailMatchType: TcxGridDBColumn
          Caption = 'Match Type'
          DataBinding.FieldName = 'match_type'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownRows = 7
          Options.Filtering = False
          Width = 91
        end
        object ColDetailMatchValue: TcxGridDBColumn
          Caption = 'Match Value'
          DataBinding.FieldName = 'match_value'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 92
        end
      end
    end
  end
  inherited Master: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from value_group'
  end
  inherited Detail: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from value_group_detail'
    IndexFieldNames = 'value_group_id'
    MasterFields = 'value_group_id'
  end
end
