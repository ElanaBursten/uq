object BillingEngineDM: TBillingEngineDM
  OnCreate = DataModuleCreate
  Height = 605
  Width = 541
  object Conn: TADOConnection
    CommandTimeout = 600
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    Left = 132
    Top = 12
  end
  object LocateRawData: TADODataSet
    Connection = ReadConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'select'#13#10'  l.ticket_id, c.ref_id, l.locate_id, l.client_id, l.clo' +
      'sed, l.closed_date, l.client_code, '#13#10'  l.status, l.qty_marked, l' +
      '.closed_how, l.invoiced, l.high_profile,'#13#10'  l.assigned_to_id as ' +
      'locator_id,'#13#10'  l.added_by as locate_added_by,'#13#10'  ls.first_close_' +
      'date,'#13#10'  e.emp_number, e.short_name, '#13#10'  (select min(locate_stat' +
      'us.status_date)'#13#10'    from locate_status with (index (locate_id))' +
      #13#10'    where locate_status.locate_id = l.locate_id'#13#10'      and loc' +
      'ate_status.status <> '#39'-R'#39') as initial_status_date,'#13#10'  (select to' +
      'p 1 plat from locate_plat lp where lp.locate_id = l.locate_id or' +
      'der by insert_date asc) as plat,'#13#10'  (select sum(lh.units_marked)' +
      ' from locate_hours lh where lh.locate_id = l.locate_id) as units' +
      '_marked'#13#10'from locate l with (index (locate_ticketid))'#13#10'  inner j' +
      'oin #tix on #tix.ticket_id = l.ticket_id'#13#10'  inner join client c ' +
      'on l.client_id = c.client_id'#13#10'  left join employee e on e.emp_id' +
      ' = l.assigned_to_id'#13#10'  left join locate_snap ls on ls.locate_id ' +
      '= l.locate_id'#13#10'where'#13#10'  l.status <>'#39'-N'#39'   --3537'#13#10'  order by tic' +
      'ket_id, c.ref_id'#13#10
    CommandTimeout = 300
    Parameters = <>
    Left = 36
    Top = 296
  end
  object billing_header: TADOTable
    Connection = Conn
    CursorType = ctStatic
    CommandTimeout = 300
    TableName = 'billing_header'
    Left = 128
    Top = 68
  end
  object billing_detail: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select * from billing_detail where bill_id=-1'
    CommandTimeout = 300
    Parameters = <>
    Left = 144
    Top = 140
  end
  object MakeEmptyTix: TADOCommand
    CommandText = 
      'CREATE TABLE #tix ( ticket_id int NOT NULL, TxNo varchar(20) not' +
      ' null, ticket_format varchar(20) not null, Revision varchar(20)n' +
      'ull, TixIsDup Bit NOT NULL DEFAULT(0))'
    Connection = ReadConn
    Parameters = <>
    Left = 32
    Top = 8
  end
  object AddCentersToTix: TADOCommand
    Connection = ReadConn
    Parameters = <>
    Left = 32
    Top = 56
  end
  object DropTix: TADOCommand
    CommandText = 'drop table #tix'
    Connection = ReadConn
    Parameters = <>
    Left = 36
    Top = 396
  end
  object CountTickets: TADODataSet
    Connection = Conn
    CommandText = 
      'select count(distinct ticket_number) from billing_detail where b' +
      'ill_id = :BillID and ((area_name <> '#39'DEL_GAS'#39') or (area_name is ' +
      'null))'
    Parameters = <
      item
        Name = 'BillID'
        Size = -1
        Value = Null
      end>
    Left = 240
    Top = 92
  end
  object CountLocates: TADODataSet
    Connection = Conn
    CommandText = 
      'select count(*) from billing_detail where bill_id = :BillID and ' +
      '((area_name <> '#39'DEL_GAS'#39') or (area_name is null))'
    Parameters = <
      item
        Name = 'BillID'
        Size = -1
        Value = Null
      end>
    Left = 236
    Top = 140
  end
  object SumBilling: TADODataSet
    Connection = Conn
    CommandText = 
      'select sum(price) from billing_detail where bill_id = :BillID an' +
      'd ((area_name <> '#39'DEL_GAS'#39') or (area_name is null))'
    Parameters = <
      item
        Name = 'BillID'
        Size = -1
        Value = Null
      end>
    Left = 240
    Top = 188
  end
  object AddTixIndex: TADOCommand
    CommandText = 'ALTER TABLE #tix ADD PRIMARY KEY(ticket_id)'
    Connection = ReadConn
    Parameters = <>
    Left = 32
    Top = 104
  end
  object DailyHoursUnits: TADODataSet
    Connection = ReadConn
    CursorType = ctOpenForwardOnly
    CommandText = 
      'declare @locates table (locate_id int not null)'#13#10#13#10'insert into @' +
      'locates'#13#10'select distinct locate_id from locate with (index(locat' +
      'e_ticketid) NOLOCK)'#13#10'  inner join #tix on #tix.ticket_id = locat' +
      'e.ticket_id'#13#10'where locate.status not in ('#39'-N'#39', '#39'-P'#39')'#13#10#13#10'select h' +
      '.locate_hours_id, h.locate_id, h.emp_id, h.work_date,'#13#10'  h.regul' +
      'ar_hours, h.overtime_hours, h.units_marked, h.status,'#13#10'  h.units' +
      '_invoiced, h.hours_invoiced, uc.first_unit_factor, uc.rest_units' +
      '_factor'#13#10'from locate_hours h (NOLOCK)'#13#10'  inner join @locates loc' +
      ' on loc.locate_id = h.locate_id'#13#10' left join billing_unit_convers' +
      'ion uc on uc.unit_conversion_id = h.unit_conversion_id'#13#10
    Parameters = <>
    Left = 36
    Top = 248
  end
  object HoursUnits: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    Exclusive = True
    TableName = 'HourlyData'
    Left = 164
    Top = 300
  end
  object BilledTickets: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    IndexDefs = <
      item
        Name = 'TicketNumberIndex'
        Fields = 'ticket_number'
      end>
    TableName = 'BilledTickets'
    StoreDefs = True
    Left = 248
    Top = 300
    object BilledTicketsTicketNumber: TStringField
      DisplayWidth = 25
      FieldName = 'ticket_number'
    end
    object BilledTicketsTicketID: TIntegerField
      FieldName = 'ticket_id'
    end
    object BilledTicketsClientCode: TStringField
      FieldName = 'client_code'
      Size = 25
    end
  end
  object TicketQuery: TDBISAMQuery
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    SQL.Strings = (
      'select * '
      'from "\Memory\BilledTickets"'
      'where'
      '  (ticket_number = :TicketNumber) and'
      '  (client_code = :ClientCode) and'
      '  (ticket_id <> :TicketID)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'TicketNumber'
      end
      item
        DataType = ftUnknown
        Name = 'ClientCode'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
    Left = 316
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TicketNumber'
      end
      item
        DataType = ftUnknown
        Name = 'ClientCode'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
  end
  object AddDailyTix: TADOCommand
    Connection = ReadConn
    Parameters = <
      item
        Name = 'start'
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'end'
        DataType = ftDateTime
        Value = 0d
      end>
    Left = 32
    Top = 152
  end
  object TicketNotes: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    Exclusive = True
    TableName = 'TicketNotes'
    Left = 156
    Top = 404
  end
  object TicketNotesData: TADODataSet
    Connection = ReadConn
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select t.ticket_number, t.ticket_id, n.notes_id, n.entry_date, n' +
      '.note'#13#10'from #tix tx'#13#10'  inner join ticket_notes n on tx.ticket_id' +
      ' = n.ticket_id'#13#10'  inner join ticket t on t.ticket_id = tx.ticket' +
      '_id'#13#10#13#10
    CommandTimeout = 60
    Parameters = <>
    Left = 32
    Top = 200
  end
  object TicketRawData: TADODataSet
    Connection = ReadConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'select'#13#10' t.ticket_id, t.ticket_number, t.ticket_format, t.transm' +
      'it_date, t.ticket_type,'#13#10' t.call_date, t.work_address_number, t.' +
      'source,'#13#10' t.work_address_number_2, t.work_address_street,'#13#10' t.wo' +
      'rk_city, t.work_date, t.work_state, t.due_date, t.work_county, t' +
      '.con_name,'#13#10' t.company, t.work_type, t.work_lat, t.work_long, t.' +
      'parent_ticket_id, t.revision,'#13#10' t.map_ref, t.work_cross, t.updat' +
      'e_of, a.area_name, t.work_description, t.work_remarks,'#13#10' (select' +
      ' count(ticket_id) from ticket_version'#13#10'   where ticket_id = t.ti' +
      'cket_id) as transmit_count,'#13#10'  (select count(*) from ticket_vers' +
      'ion tv where tv.ticket_id = #tix.ticket_id and ticket_type like ' +
      #39'%LATE%'#39') as was_ever_late,'#13#10' (select min(j.arrival_date) from j' +
      'obsite_arrival j where j.ticket_id = #tix.ticket_id) as initial_' +
      'arrival_date, '#13#10' #tix.TixIsDup from ticket t'#13#10'  inner join #tix ' +
      'on #tix.ticket_id = t.ticket_id'#13#10'  left join area a on t.route_a' +
      'rea_id = a.area_id'
    CommandTimeout = 300
    Parameters = <>
    Left = 36
    Top = 348
  end
  object TicketFileData: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    Exclusive = True
    TableName = 'TicketFileData'
    Left = 248
    Top = 348
  end
  object LocateFileData: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    Exclusive = True
    TableName = 'LocateFileData'
    Left = 164
    Top = 348
  end
  object ReadConn: TADOConnection
    CommandTimeout = 600
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=QM;Data Source=DYBOC-DQMGAS0' +
      '1\QM_GAS;'
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 204
    Top = 12
  end
  object CheckBillingSetupSP: TADOStoredProc
    Connection = Conn
    ProcedureName = 'check_billing_setup;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CallCenterList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 800
        Value = Null
      end>
    Left = 336
    Top = 8
  end
  object PerfLogData: TADODataSet
    Connection = Conn
    CommandText = 'select * from billing_performance_log where 0=1'
    Parameters = <>
    Left = 144
    Top = 244
  end
  object DeletePriorBilling: TADOStoredProc
    Connection = Conn
    ProcedureName = 'delete_prior_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TestOnly'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 312
    Top = 83
  end
  object LocateLookupData: TADODataSet
    Connection = ReadConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'select locate_id from locate where ticket_id = :ticket_id and cl' +
      'ient_code = :client_code'
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    Left = 140
    Top = 192
  end
  object TransDetailSP: TADOStoredProc
    Connection = Conn
    CommandTimeout = 600
    ProcedureName = 'trans_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 324
    Top = 160
  end
  object insBillingAdjustments: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'adjustment_date'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'added_date'
        Attributes = [paSigned, paNullable]
        DataType = ftDateTime
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'type'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'description'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'added_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'last_modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'modified_date'
        Attributes = [paSigned, paNullable]
        DataType = ftDateTime
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'active'
        Attributes = [paSigned, paNullable]
        DataType = ftBoolean
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'which_invoice'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'gl_code'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '        delete'
      '        from  [dbo].[billing_adjustment]'
      '        where customer_id= 2083 and'
      '        client_id= 4645'
      ''
      ''
      ''
      '    INSERT INTO [dbo].[billing_adjustment]'
      '           ([adjustment_date]'
      '           ,[customer_id]'
      '           ,[client_id]'
      '           ,[added_date]'
      '           ,[type]'
      '           ,[description]'
      '           ,[amount]'
      '           ,[added_by]'
      '           ,[last_modified_by]'
      '           ,[modified_date]'
      '           ,[active]'
      '           ,[which_invoice]'
      '           ,[gl_code])'
      '     VALUES'
      '           (:adjustment_date'
      '           ,:customer_id'
      '           ,:client_id'
      '           ,:added_date'
      '           ,:type'
      '           ,:description'
      '           ,:amount'
      '           ,:added_by'
      '           ,:last_modified_by'
      '           ,:modified_date'
      '           ,:active'
      '           ,:which_invoice'
      '           ,:gl_code)')
    Left = 320
    Top = 232
  end
  object insBillingHeader: TADOQuery
    Connection = Conn
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'bill_run_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'bill_start_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'bill_end_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'description'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'center_group_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'period_type'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'app_version'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'INSERT INTO [dbo].[billing_header]'
      '           ([bill_run_date]'
      '           ,[bill_start_date]'
      '           ,[bill_end_date]'
      '           ,[description]'
      '           ,[center_group_id]'
      '           ,[period_type]'
      '           ,[app_version])'
      '     VALUES'
      '           (:bill_run_date'
      '           ,:bill_start_date'
      '           ,:bill_end_date'
      '           ,:description'
      '           ,:center_group_id'
      '           ,:period_type'
      '           ,:app_version)'
      ''
      '')
    Left = 192
    Top = 64
  end
  object qryBillID: TADOQuery
    Connection = ReadConn
    CommandTimeout = 300
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 1 bill_id'
      '  FROM [dbo].[billing_header]'
      '  order by bill_id desc')
    Left = 440
    Top = 56
  end
  object updBillingHeader: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'n_tickets'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'n_locates'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'totalamount'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'BillID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'UPDATE [dbo].[billing_header]'
      '   SET [n_tickets] = :n_tickets'
      '      ,[n_locates] = :n_locates'
      '      ,[totalamount] = :totalamount'
      '   '
      ' WHERE Bill_id = :BillID')
    Left = 256
    Top = 48
  end
  object fixWhichInvoice: TADOQuery
    Connection = Conn
    CommandTimeout = 600
    Parameters = <
      item
        Name = 'BillID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'lowID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'highID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'WhichInvoice'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Client'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'declare'
      '@lowBillingID int,'
      '@highBillingID int,'
      '@BillID int'
      ''
      'set @BillID = :BillID'
      'set @lowBillingID = :lowID'
      'set @highBillingID =  :highID'
      ''
      'update billing_detail'
      'set which_invoice = :WhichInvoice'
      'where bill_id = @BillID'
      'and billing_cc = :Client'
      'and (which_invoice is null  or which_invoice = '#39#39')'
      
        'and billing_detail.billing_detail_id between @lowBillingID and @' +
        'highBillingID'
      ''
      '')
    Left = 448
    Top = 296
  end
  object updDateWhichInvoice: TADOQuery
    Connection = Conn
    CommandTimeout = 600
    Parameters = <
      item
        Name = 'BillID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'lowID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'highID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'declare'
      '@lowBillingID int,'
      '@highBillingID int,'
      '@BillID int'
      ''
      ''
      'set @BillID = :BillID'
      'set @lowBillingID = :lowID'
      'set @highBillingID =  :highID'
      ''
      'update billing_detail'
      'set billing_detail.which_invoice =  (select vg.group_code'
      #9#9#9#9#9'            from value_group vg'
      
        #9#9#9#9#9'            inner join [value_group_detail] vgd on vg.value' +
        '_group_id = vgd.value_group_id'
      #9#9#9#9#9'            where vgd.match_value = billing_detail.Plat)'
      'where billing_detail.bill_id = @BillID'
      
        'and billing_detail.billing_cc in ('#39'PACBEL'#39', '#39'ATTDSOUTH'#39', '#39'NEVBEL' +
        #39','#39'ATTDNORCAL'#39','#39'ATTDNEVADA'#39')'
      
        'and billing_detail.billing_detail_id between @lowBillingID and @' +
        'highBillingID'
      'and billing_detail.area_name <> '#39'DEL_GAS'#39
      ''
      '')
    Left = 448
    Top = 232
  end
  object qryBillDetailMinMax: TADOQuery
    Connection = ReadConn
    Parameters = <
      item
        Name = 'BillID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT min(billing_detail_id) minBillDetail, max(billing_detail_' +
        'id)maxBillDetail'
      '  FROM [dbo].[billing_detail]'
      ' where bill_id = :BillID'
      ' and area_name <> '#39'DEL_GAS'#39)
    Left = 440
    Top = 376
  end
  object qryTicketDups: TADOQuery
    Connection = ReadConn
    CommandTimeout = 300
    Parameters = <>
    SQL.Strings = (
      'Select ticket_id, TxNo,  Revision, Ticket_Format,  TixIsDup'
      'From #Tix'
      'Order by TxNo, Revision, Ticket_Format')
    Left = 32
    Top = 456
  end
  object updTicketDups: TADOQuery
    Connection = ReadConn
    Parameters = <
      item
        Name = 'ticketID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'Update #tix'
      'Set TixIsDup = 1'
      'where ticket_id = :ticketID')
    Left = 32
    Top = 512
  end
  object spAdjustNIPSCO: TADOStoredProc
    Connection = Conn
    CommandTimeout = 300
    ProcedureName = 'NIPSCO_Adjustment'
    Parameters = <
      item
        Name = '@BillID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 304
    Top = 384
  end
  object spFMB_BGE_Adjustment: TADOStoredProc
    Connection = Conn
    CommandTimeout = 300
    ProcedureName = 'FMB_BGE_Adjustment'
    Parameters = <
      item
        Name = '@BillID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 320
    Top = 440
  end
  object spDOM408Adjustment: TADOStoredProc
    Connection = Conn
    CommandTimeout = 300
    ProcedureName = 'UpdateDom408Rates'
    Parameters = <
      item
        Name = '@Bill_id'
        DataType = ftInteger
        Value = Null
      end>
    Left = 312
    Top = 512
  end
  object spAdjustDOMCOMB: TADOStoredProc
    Connection = Conn
    CommandTimeout = 300
    ProcedureName = '[SCANA_Adjustment]'
    Parameters = <
      item
        Name = '@BillID'
        DataType = ftInteger
        Value = Null
      end>
    Left = 440
    Top = 432
  end
  object insBillingAdjustmentsCal: TADOQuery
    Connection = Conn
    Parameters = <
      item
        Name = 'adjustment_date'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'added_date'
        Attributes = [paSigned, paNullable]
        DataType = ftDateTime
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'type'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'description'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'added_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'last_modified_by'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'modified_date'
        Attributes = [paSigned, paNullable]
        DataType = ftDateTime
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'active'
        Attributes = [paSigned, paNullable]
        DataType = ftBoolean
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'which_invoice'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'gl_code'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '        delete'
      '        from  [dbo].[billing_adjustment]'
      '        where customer_id= 2083 and'
      '        client_id= 4645'
      ''
      ''
      ''
      '    INSERT INTO [dbo].[billing_adjustment]'
      '           ([adjustment_date]'
      '           ,[customer_id]'
      '           ,[client_id]'
      '           ,[added_date]'
      '           ,[type]'
      '           ,[description]'
      '           ,[amount]'
      '           ,[added_by]'
      '           ,[last_modified_by]'
      '           ,[modified_date]'
      '           ,[active]'
      '           ,[which_invoice]'
      '           ,[gl_code])'
      '     VALUES'
      '           (:adjustment_date'
      '           ,:customer_id'
      '           ,:client_id'
      '           ,:added_date'
      '           ,:type'
      '           ,:description'
      '           ,:amount'
      '           ,:added_by'
      '           ,:last_modified_by'
      '           ,:modified_date'
      '           ,:active'
      '           ,:which_invoice'
      '           ,:gl_code)')
    Left = 408
    Top = 160
  end
end
