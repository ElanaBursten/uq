unit BillingAnalysis2;
//qm-476 free standing form.  Allows looking at multi forms
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  cxMaskEdit, cxCurrencyEdit, cxCalendar, cxCheckBox, Data.Win.ADODB, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.ExtCtrls, dxSkinWXI, dxCore, dxSkinsForm, dxSkinsCore;

type
  TfrmBillAnalysis2 = class(TForm)
    TopPanel: TPanel;
    Grid: TcxGrid;
    GridView: TcxGridDBTableView;
    Gridbilling_cc: TcxGridDBColumn;
    Gridticket_number: TcxGridDBColumn;
    Gridrevision: TcxGridDBColumn;
    Gridstatus: TcxGridDBColumn;
    Gridticket_type: TcxGridDBColumn;
    Gridticket_id: TcxGridDBColumn;
    Gridlocate_id: TcxGridDBColumn;
    Gridrate: TcxGridDBColumn;
    Gridprice: TcxGridDBColumn;
    Gridbill_code: TcxGridDBColumn;
    Gridbucket: TcxGridDBColumn;
    Gridline_item_text: TcxGridDBColumn;
    Gridqty_marked: TcxGridDBColumn;
    Gridhours: TcxGridDBColumn;
    Gridqty_charged: TcxGridDBColumn;
    Gridcall_date: TcxGridDBColumn;
    Gridwork_date: TcxGridDBColumn;
    Gridcon_name: TcxGridDBColumn;
    Gridclient_id: TcxGridDBColumn;
    Gridlocator_id: TcxGridDBColumn;
    Gridwork_done_for: TcxGridDBColumn;
    Gridclosed_date: TcxGridDBColumn;
    Gridclosed_how: TcxGridDBColumn;
    Griddue_date: TcxGridDBColumn;
    Gridtransmit_date: TcxGridDBColumn;
    Gridwork_type: TcxGridDBColumn;
    Gridwork_lat: TcxGridDBColumn;
    Gridwork_long: TcxGridDBColumn;
    Gridwork_cross: TcxGridDBColumn;
    Gridwork_address_number: TcxGridDBColumn;
    Gridwork_address_number_2: TcxGridDBColumn;
    Gridwork_address_street: TcxGridDBColumn;
    Gridwork_city: TcxGridDBColumn;
    Gridwork_state: TcxGridDBColumn;
    Gridwork_county: TcxGridDBColumn;
    Gridinitial_status_date: TcxGridDBColumn;
    Gridemp_number: TcxGridDBColumn;
    Gridshort_name: TcxGridDBColumn;
    Gridwork_description: TcxGridDBColumn;
    Gridarea_name: TcxGridDBColumn;
    Gridtransmit_count: TcxGridDBColumn;
    Gridmap_ref: TcxGridDBColumn;
    Gridlocate_hours_id: TcxGridDBColumn;
    Gridwhich_invoice: TcxGridDBColumn;
    Gridtax_name: TcxGridDBColumn;
    Gridtax_rate: TcxGridDBColumn;
    Gridtax_amount: TcxGridDBColumn;
    Gridraw_units: TcxGridDBColumn;
    Gridupdate_of: TcxGridDBColumn;
    Gridhigh_profile: TcxGridDBColumn;
    Gridbill_id: TcxGridDBColumn;
    GridParent_id: TcxGridDBColumn;
    Gridbilling_detail_id: TcxGridDBColumn;
    GridLevel: TcxGridLevel;
    Data: TADODataSet;
    DS: TDataSource;
    dxSkinController1: TdxSkinController;

  private
    procedure ShowBillID(BillID: Integer);
    { Private declarations }
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent; BillID:integer);reintroduce;overload;
  end;

var
  frmBillAnalysis2: TfrmBillAnalysis2;

implementation

{$R *.dfm}

uses OdHourglass, QMBillingDMu;

{ TfrmBillAnalysis2 }

constructor TfrmBillAnalysis2.Create(AOwner: TComponent; BillID: integer);
begin
  inherited Create(AOwner);
  ShowBillID(BillID);
end;

procedure TfrmBillAnalysis2.ShowBillID(BillID: Integer);
const
  INI_DB='Database';
  INI_CONN_STR='ConnectionString';
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  Data.Close;
  Data.ConnectionString := QMBillingDM.GetIniSetting(INI_DB,INI_CONN_STR,'');//MainConn.ConnectionString;

  Data.Parameters.ParamByName('BillID').Value := BillID;
  Data.Open;
end;

end.
