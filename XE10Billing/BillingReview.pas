unit BillingReview;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, OdEmbeddable,
  ADODB, DB, StdCtrls, ExtCtrls, ActnList, Variants, Menus, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxTextEdit, cxCalendar, cxMaskEdit, cxCurrencyEdit, UITypes,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, System.Actions, dxSkinsCore,
  dxSkinscxPCPainter, cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TBillingReviewForm = class(TEmbeddableForm)
    Header: TADODataSet;
    HeaderDS: TDataSource;
    DeleteBilling: TADOStoredProc;
    ClientsOnBill: TADODataSet;
    ClientsOnBillDS: TDataSource;
    TermsPanel: TPanel;
    TermsHeaderPanel: TPanel;
    TermsLabel: TLabel;
    ShowTermIds: TCheckBox;
    HeaderPanel: TPanel;
    CommitButton: TButton;
    DeleteBillingButton: TButton;
    MarkInvoiced: TADOQuery;
    MarkNotInvoiced: TADOQuery;
    UnCommitBillingButton: TButton;
    ActionList: TActionList;
    CommitBillingAction: TAction;
    UncommitBillingAction: TAction;
    DeleteBillingAction: TAction;
    ReExportButton: TButton;
    ReExportBillingAction: TAction;
    Label1: TLabel;
    EditFilter: TEdit;
    Label2: TLabel;
    EditDest: TEdit;
    Label3: TLabel;
    TermsSplitter: TSplitter;
    GridMenu: TPopupMenu;
    Commit1: TMenuItem;
    UnCommit1: TMenuItem;
    Delete1: TMenuItem;
    ReExport1: TMenuItem;
    AnalyzeBillingAction: TAction;
    Analyze1: TMenuItem;
    EditWeeks: TEdit;
    HeaderGrid: TcxGrid;
    HeaderGridLevel: TcxGridLevel;
    HeaderGridView: TcxGridDBTableView;
    BillIDColumn: TcxGridDBColumn;
    CommittedColumn: TcxGridDBColumn;
    DescriptionColumn: TcxGridDBColumn;
    GroupCodeColumn: TcxGridDBColumn;
    PeriodTypeColumn: TcxGridDBColumn;
    BillStartDateColumn: TcxGridDBColumn;
    BillEndDateColumn: TcxGridDBColumn;
    RunDateColumn: TcxGridDBColumn;
    CommittedDateColumn: TcxGridDBColumn;
    AppVersionColumn: TcxGridDBColumn;
    NTicketsColumn: TcxGridDBColumn;
    NLocatesColumn: TcxGridDBColumn;
    TotalAmountColumn: TcxGridDBColumn;
    DetailGrid: TcxGrid;
    DetailGridView: TcxGridDBTableView;
    CustomerNameColumn: TcxGridDBColumn;
    BillingCCColumn: TcxGridDBColumn;
    WhichInvoiceColumn: TcxGridDBColumn;
    LocateCountColumn: TcxGridDBColumn;
    DetailGridLevel: TcxGridLevel;
    procedure CommitBillingActionExecute(Sender: TObject);
    procedure DeleteBillingActionExecute(Sender: TObject);
    procedure HeaderAfterScroll(DataSet: TDataSet);
    procedure ShowTermIdsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UnCommitBillingActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ReExportBillingActionExecute(Sender: TObject);
    procedure EditFilterChange(Sender: TObject);
    procedure HeaderBeforeOpen(DataSet: TDataSet);
    procedure AnalyzeBillingActionExecute(Sender: TObject);
  private
    LogData: TStringList;
    procedure RepopulateTermIDSummary;
    procedure Log(Sender: TObject; const Msg: string);
  public
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
    procedure ActivatingNow; override;
  end;

implementation

uses QMBillingDMu, BillingEngine, JclShell, OdHourglass, OdAdoUtils,
  BillingCommitDMu, MainFU;

{$R *.dfm}

//TODO: Handle setting invoiced on aggregated hourly records that are billed daily?
procedure TBillingReviewForm.CommitBillingActionExecute(Sender: TObject);
begin
  //TODO: Committing is not allowed here unless the employee doing the commit can be tracked
  if Header.FieldByName('committed').AsBoolean then
    raise Exception.Create('This billing is already committed');

  try
    with TBillingCommitDM.Create(nil) do
      try
        Commit(Header.FieldByName('bill_id').AsInteger, 0);
        ShowMessage('Commit succeeded: ' + Header.FieldByName('bill_id').AsString);
      finally
        Free;
      end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  RefreshNow;
end;

procedure TBillingReviewForm.UnCommitBillingActionExecute(Sender: TObject);
begin
  if not Header.FieldByName('committed').AsBoolean then
    raise Exception.Create('This billing has not been committed');

  try
    with TBillingCommitDM.Create(nil) do
      try
        UnCommit(Header.FieldByName('bill_id').AsInteger);
        ShowMessage('Uncommit succeeded: ' + Header.FieldByName('bill_id').AsString);
      finally
        Free;
      end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  RefreshNow;
end;

procedure TBillingReviewForm.DeleteBillingActionExecute(Sender: TObject);
const
  Msg = 'Are you sure you want to delete this billing?';
begin
  if Header.FieldByName('committed').AsBoolean then
    raise Exception.Create('You cannot delete a committed billing.');
  if MessageDlg(Msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    DeleteBilling.Parameters.ParamValues['@BillID'] := Header.FieldByName('bill_id').AsInteger;
    DeleteBilling.ExecProc;
    CheckForADOError(DeleteBilling.Connection);
    RefreshNow;
  end;
end;

procedure TBillingReviewForm.HeaderAfterScroll(DataSet: TDataSet);
begin
  RepopulateTermIDSummary;
end;

procedure TBillingReviewForm.ShowTermIdsClick(Sender: TObject);
begin
  RepopulateTermIDSummary;
end;

procedure TBillingReviewForm.RepopulateTermIDSummary;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  ClientsOnBill.Close;
  if ShowTermIds.Checked then
    ClientsOnBill.Open;
end;

procedure TBillingReviewForm.FormShow(Sender: TObject);
begin
  OpenDataSets;
end;

procedure TBillingReviewForm.CloseDataSets;
begin
  inherited;
  Header.Close;
  ClientsOnBill.Close;
end;

procedure TBillingReviewForm.OpenDataSets;
begin
  inherited;
  Header.Open;
  RepopulateTermIDSummary;
end;

procedure TBillingReviewForm.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  if Header.IsEmpty then begin
    UncommitBillingAction.Enabled := False;
    CommitBillingAction.Enabled := False;
    DeleteBillingAction.Enabled := False;
    ReExportBillingAction.Enabled := False;
    AnalyzeBillingAction.Enabled := False;
  end
  else begin
    UncommitBillingAction.Enabled := Header.FieldByName('committed').AsBoolean;
    CommitBillingAction.Enabled := not Header.FieldByName('committed').AsBoolean;
    DeleteBillingAction.Enabled := not Header.FieldByName('committed').AsBoolean;
    ReExportBillingAction.Enabled := True;
    AnalyzeBillingAction.Enabled := True;
  end;
end;

procedure TBillingReviewForm.ReExportBillingActionExecute(Sender: TObject);
var
  BE: TBillingEngine;
  Cursor: IInterface;
  Success: Boolean;
begin
  Success := False;
  Cursor := ShowHourGlass;
  SetCurrentDir(ExtractFilePath(Application.ExeName));
  LogData := TStringList.Create;
  Log(Self, 'Re-Exporting');
  BE := TBillingEngine.Create;
  try
    try
      BE.SetConnectionLike(QMBillingDM.Conn);
      BE.SetBillingTimeout(QMBillingDM.BillingTimeout);
      BE.Logger := Log;
      BE.ExportResults(Header.FieldByName('bill_id').AsInteger, EditDest.Text);
      JclShell.OpenFolder(BE.DestinationDir);
      Log(Self, 'Done');
      Success := True;
    except
      on E: Exception do begin
        Log(Self, 'ERROR: ' + E.Message);
      end;
    end;
  finally
    try
      LogData.SaveToFile(BE.DestinationDir + 'export-log.txt');
    except
      on E:Exception do begin
        ShowMessage(E.Message);
        ShowMessage(LogData.Text);
      end;
    end;
    BE.Free;

    if Success then
      ShowMessage('Export complete')
    else
      ShowMessage('Export Failed, see export-log.txt for full details.'#13#10 +
        Copy(LogData.Text, 1, 300));
    LogData.Free;
  end;
end;

procedure TBillingReviewForm.Log(Sender: TObject; const Msg: string);
begin
  LogData.Add(Msg);
end;

procedure TBillingReviewForm.EditFilterChange(Sender: TObject);
begin
  if EditFilter.Text = '' then
    Header.Filtered := False
  else begin
    Header.Filter := 'description like ''%' + EditFilter.Text + '%''';
    Header.Filtered := True;
  end;
end;

procedure TBillingReviewForm.HeaderBeforeOpen(DataSet: TDataSet);
begin
  Header.Parameters.ParamValues['days'] := 0 - 7 * StrToInt(EditWeeks.Text);
end;

procedure TBillingReviewForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(False);
end;

procedure TBillingReviewForm.AnalyzeBillingActionExecute(Sender: TObject);
begin
  MainForm.ShowBillingDetail(Header.FieldByName('bill_id').AsInteger);
end;

end.

