inherited BillingGLForm: TBillingGLForm
  Caption = 'Billing GL Codes'
  ClientHeight = 478
  ClientWidth = 713
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 713
  end
  inherited Grid: TcxGrid
    Width = 713
    Height = 437
    inherited GridView: TcxGridDBTableView
      Navigator.Visible = True
      DataController.KeyFieldNames = 'billing_gl_id'
      OptionsView.GroupByBox = True
      object ColID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'billing_gl_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Width = 63
      end
      object ColChargeType: TcxGridDBColumn
        Caption = 'Charge Type'
        DataBinding.FieldName = 'charge_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        SortIndex = 0
        SortOrder = soAscending
        Width = 144
      end
      object ColPeriodType: TcxGridDBColumn
        Caption = 'Period Type'
        DataBinding.FieldName = 'period_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 105
      end
      object ColGLCode: TcxGridDBColumn
        Caption = 'GL Code'
        DataBinding.FieldName = 'gl_code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 95
      end
      object ColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 213
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 60
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from billing_gl'
  end
  object dxSkinController1: TdxSkinController
    Left = 544
    Top = 264
  end
end
