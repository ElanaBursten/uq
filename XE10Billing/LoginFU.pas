unit LoginFU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, System.UITypes;

type
  TLoginForm = class(TForm)
    Label1: TLabel;
    PasswordLabel: TLabel;
    EditUserName: TEdit;
    EditPassword: TEdit;
    LoginBtn: TButton;
    CancelBtn: TButton;
    AppLabel: TLabel;
    Label5: TLabel;
    RememberBox: TCheckBox;
    VersionLabel: TLabel;
    LogoImage: TImage;
    EditServer: TEdit;
    EditDatabase: TEdit;
    Label3: TLabel;
    TrustedBox: TCheckBox;
    procedure LoginBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure TrustedBoxClick(Sender: TObject);
  private
    IniUserName, IniPassword, IniServer, IniDatabase: string;
    IniTrusted: Boolean;
    IniBillingTimeout: Integer;
    procedure SaveReg;
    procedure SetView;
    procedure GetIniSettings;
  public
    procedure InitializeSettingsFromIni;
  end;

function LogIn: Boolean;

implementation

uses Registry, IniFiles, QMBillingDMu, OdVclUtils, OdHourGlass, OdMiscUtils;

{$R *.DFM}

function LogIn: Boolean;
begin
  Result := QMBillingDM.ConnectWithConnString;
  if Result = False then begin
    with TLoginForm.Create(nil) do try
        InitializeSettingsFromIni;
        RememberBox.Checked := (EditPassword.Text <> '');
        Result := (ShowModal = mrOK);
    finally
      Release;
    end;
  end;
end;

procedure TLoginForm.LoginBtnClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  if EditUserName.Text = '' then
    raise Exception.Create('Please specify a User ID to log in.');

  Cursor := ShowHourGlass;
  try
    QMBillingDM.Server := EditServer.Text;
    QMBillingDM.DB := EditDatabase.Text;
    QMBillingDM.UserName := EditUserName.Text;
    QMBillingDM.Password := EditPassword.Text;
    QMBillingDM.Connect;

    SaveReg;
    Modalresult := mrOk;
  except
    on E: Exception do begin
      // Remember you need to login with a DB user/password (sa, etc.), not a user from the employee table
      MessageDlg('Unable to log in: ' + E.Message, mtError, [mbOK], 0);
      TryFocusControl(EditUserName);
    end;
  end;
end;

procedure TLoginForm.SetView;
begin
  EditPassword.Visible := not QMBillingDM.Trusted;
  PasswordLabel.Visible := not QMBillingDM.Trusted;
  RememberBox.Visible := not QMBillingDM.Trusted;
  EditUserName.Enabled := not QMBillingDM.Trusted;

  if QMBillingDM.Trusted then begin
    EditUserName.Color := clInfoBk;
    EditUserName.Text := GetDomainUserName;
  end
  else begin
    EditUserName.Color := clWindow;
    EditUserName.Text := IniUserName;
    EditPassword.Text := IniPassword;
  end;
end;

procedure TLoginForm.GetIniSettings;
var
  Ini: TIniFile;
begin
  Ini := QMBillingDM.CreateIniFile;
  try
    //retain backward compatibility with AutoLogin section (containing user and pwd);
    //being deprecated in favor of consistent naming across ini's. This check
    //for AutoLogin can be removed once it's confirmed that UQ has the [Database]
    //UserName and Password in all instances of QManagerBilling.ini
    IniUserName := Ini.ReadString('AutoLogin', 'UserName', '');
    IniPassword := Ini.ReadString('AutoLogin', 'PW', '');
    if IsEmpty(IniUserName) then
      IniUserName := Ini.ReadString('Database', 'UserName', '');
    if IsEmpty(IniPassword) then
      IniPassword := Ini.ReadString('Database', 'Password', '');
    IniServer := Ini.ReadString('Database', 'Server', '');
    IniDatabase := Ini.ReadString('Database', 'DB', '');
    IniTrusted := (Ini.ReadInteger('Database', 'Trusted', 1) = 1);
    IniBillingTimeout := Ini.ReadInteger('Database', 'BillingTimeout', 300);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TLoginForm.InitializeSettingsFromIni;
begin
  Assert(Assigned(QMBillingDM));

  GetIniSettings;
  TrustedBox.Checked := IniTrusted;
  EditUserName.Text := IniUserName;
  EditPassword.Text := IniPassword;
  EditServer.Text := IniServer;
  EditDatabase.Text := IniDatabase;

  // This is to support great ugliness in which command line billing
  // uses the login form to load DB settings
  QMBillingDM.Trusted := TrustedBox.Checked;
  QMBillingDM.Server := EditServer.Text;
  QMBillingDM.DB := EditDatabase.Text;
  QMBillingDM.BillingTimeout := IniBillingTimeout;
  SetView;
end;

procedure TLoginForm.SaveReg;
var
  Reg: TIniFile;
  SavePW: string;
  SaveTrusted: string;

  procedure WriteIfNeeded(Section, Setting, NewValue: string);
  begin
    if Reg.ReadString(Section, Setting, '') <> NewValue then
      Reg.WriteString(Section, Setting, NewValue);
  end;

begin
  Reg := QMBillingDM.CreateIniFile;
  try
    if RememberBox.Checked then
      SavePW := EditPassword.Text
    else
      SavePW := '';
    if TrustedBox.Checked then
      SaveTrusted := '1'
    else
      SaveTrusted := '0';

    WriteIfNeeded('Database', 'UserName', EditUserName.Text);
    WriteIfNeeded('Database', 'Password', SavePW);
    WriteIfNeeded('Database', 'Trusted', SaveTrusted);
    WriteIfNeeded('Database', 'Server', EditServer.Text);
    WriteIfNeeded('Database', 'DB', EditDatabase.Text);
  finally
    FreeAndNil(Reg);
  end;
end;

procedure TLoginForm.TrustedBoxClick(Sender: TObject);
begin
  QMBillingDM.Trusted := TrustedBox.Checked;
  SetView;
end;

procedure TLoginForm.FormCreate(Sender: TObject);
begin
  LoadBitmapResource(LogoImage, 'LOGO');

  AppLabel.Caption := AppName;
  VersionLabel.Caption := 'Version ' + AppVersion;
  Caption := AppName + ' - Log In';
end;

procedure TLoginForm.CancelBtnClick(Sender: TObject);
begin
  Close;
end;

end.

