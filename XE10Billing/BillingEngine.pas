unit BillingEngine;

interface

uses
  Classes, SysUtils, Contnrs, BillingEngineDMu, Variants, DB,
  Forms, ADODB, BillingLocate, BillingTicketClientList, OdContainer,
  BillingConst, QMBillingDMu, BillingShared, OdMiscUtils,
  BillingExportDMu, BillingConfiguration, BillPeriod, OdPerfLog;

type
  TLocateArray = array of TLocate;

  // Provides fast indexed access to a static list of locates, by ticket ID
  // This should be temporary until we have indexed access to FLocates directly
  TLocatesByTicket = class(TStringList)
  public
    constructor Create(AllLocates: TObjectList; CallCenters: array of string);
    destructor Destroy; override;
    procedure GetTicket(TicketID: Integer; var Locates: TLocateArray);
    procedure GetTicketByIndex(Index: Integer; var Locates: TLocateArray);
  end;

  EQtyException = class(Exception);

  TForEachTicketProc = procedure(Locates: TLocateArray) of object;

  TBillingEngine = class
  private
    BEDM: TBillingEngineDM;
    BillingConfig: TBillingConfig;
    FTicketClientList: TTicketClientList;
    FBillID: Integer;
    FRunDate: TDateTime;
    FLogEvent: TLogEvent;
    FErrorCount: Integer;
    FBillingCodesInProgress: TStringList;
    FBucketPriceMap: TStringList;
    FDescription: string;
    FLocates: TObjectList;
    FResultRecords: TObjectList;
    FCallCenters: TStrings;
    FNoExportOrSave: Boolean;
    FExportRawDataOnly: Boolean;
    FCallCenterGroupID: Integer;
    FGroupName: string;
    FExportDM: TBillingExportDM;
    FEligibleTerms: TStringList;
    FTicketLocates: TLocatesByTicket;
    FNoOldRunCleanup: Boolean;
    ThisIsCalifornia: Boolean;
    procedure PopulateTicketClientList;
    procedure LoadUsage;
    procedure ProcessUsage;
    procedure ProcessAllLocates;
    procedure ProcessLocate(Locate: TLocate; DoRate: Boolean);
    procedure Log(const Msg: string);
    procedure Log1(Sender: TObject; const Msg: string);
    procedure LogFmt(const Msg: string; Params: array of const);
    procedure LogError(const Msg: string);
    procedure LogErrorFmt(const Msg: string; Params: array of const);
    procedure SetTotals;
    procedure RemoveNJNHack(Locate: TLocate);
    procedure CreateHeaderRecord;
    procedure StoreDetail;
    procedure Initialize;
    procedure MainProcessing;
    procedure GatherTicketsToConsider;
    procedure AddScreenChargeForLocate(Locate: TLocate);
    procedure CheckLoadedLocates;
    procedure AddLocateObjectsForScreenCharges;
    procedure CorrectForDuplicateLocates;
    procedure CorrectFMBCOHWLocates;
    procedure CorrectFMBCOHWTicket(Locates: TLocateArray);
    function GetBilledLocate(LocateID: Integer): TLocate;
    function GetLocate(LocateID: Integer): TLocate;
    function MatchesTermAndWorkDoneForGroup(Locate: TLocate; const TermGroup, DoneForValueGroup: string): Boolean;
    function MatchesTermAndExcavatorGroup(Locate: TLocate; const TermGroup, ExcavatorValueGroup: string): Boolean;
    function MatchesCCDone(Locate: TLocate; const ClientGroupName, DoneForValueGroup: string): Boolean;
    function MatchesCCDoneExc(Locate: TLocate; const ClientGroupName, DoneForValueGroup, ExcValueGroup: string): Boolean;
    procedure GetLocatesMatching(ClientCodes: array of string; Locates: TLocateArray;
      var MatchingLocates: TLocateArray); overload;
    procedure GetLocatesMatching(ClientCodes: array of string; const Status: string; Locates: TLocateArray;
      var MatchingLocates: TLocateArray); overload;
    procedure GetLocatesMatching(ClientCodes, Statuses: array of string; Locates: TLocateArray;
      var MatchingLocates: TLocateArray); overload;
    procedure GetLocatesMatching(const CallCenter: string; Statuses: array of string; var MatchingLocates: TLocateArray); overload;
    procedure GetBilledHPLocatesMatching(CallCenter: string; ClientCode: string; var MatchingLocates: TLocateArray);
    procedure GetBilledLocatesMatching(CallCenters, ClientCodes: array of string; var MatchingLocates: TLocateArray);
    procedure GetBilledAditionalQtyLocatesMatching(var MatchingLocates: TLocateArray);
    procedure GetLocatesFromListMatching(List: TObjectList; const CallCenter, Status: string; var MatchingLocates: TLocateArray); overload;
    procedure GetLocatesFromListMatching(List: TObjectList; const CallCenter: string; Statuses: array of string; var MatchingLocates: TLocateArray); overload;
    function GetLocateMatching(ClientCode: string; Locates: TLocateArray): TLocate;
    procedure GetLocateList(const CallCenter: string; LocateIDs: TStringList);
    procedure CheckForDuplicateLocates;
    (* Tested and ready to use, but unused now because of removed code
    function TicketHasLocateFor(const ClientCode: string; Locates: TLocateArray): Boolean; overload;
    function TicketHasLocateFor(ClientCodes: array of string; Locates: TLocateArray): Boolean; overload;
    function GetBillableLocateCount(Locates: TLocateArray): Integer;
    function CountLocatesWithStatus(const Status: string; Locates: TLocateArray): Integer;
    function GetLocateStatusCountWithAtLeastUnits(const Status: string; Units: Integer; Locates: TLocateArray): Integer;
    function AllLocatesHaveStatus(Locates: TLocateArray; const Status: string): Boolean;
    function GetHighestUnitValueOfStatus(Locates: TLocateArray; const Status: string): Integer;
    function GetFirstAssignedLocateIndex(Locates: TLocateArray): Integer;
    function CountAssignedLocates(Locates: TLocateArray): Integer;
    function TicketHasLocateForTerms(TicketID: Integer; const Terms: TStringArray): Boolean;
    function IsDuplicateLocateForBillingPeriod(Locate: TLocate): Boolean;
    *)
    function TicketHasLocate(const TicketID: Integer; const ClientCode: string): Boolean;
    function GetLocateIdentifier(Locate: TLocate): string;
    procedure ProcessDailyHoursUnits;
    procedure RemoveLocateFromBilledList(Locate: TLocate);
    function AddHourlyLocateRecords(Locate: TLocate; var TotalLocateHours: Currency): Integer;
    function CurrentlyBilling(const CallCenter: string): Boolean; overload;
    function CurrentlyBilling(CallCenters: array of string): Boolean; overload;
    procedure LoadHoursUnitsData;
    procedure LoadTicketNotes;
    function GetCallCenterSQLString: string;
    procedure AddHourlyIndexes;
    procedure InitializeBilledTicketsTable;
    function GetStatusDescription(const Status: string): string;
    function GetLocateFromList(LocateID: Integer; List: TObjectList): TLocate;
    function ConsiderWorkDate(Date: TDateTime): Boolean;
    function CenterBillsHoursDaily(const CallCenter: string): Boolean;
    function HourlyOrUnitBillingEnabled: Boolean;
    function UsesHourlyBilling(const CallCenter: string): Boolean;
    function UsesUnitBilling(const CallCenter: string): Boolean;
    function IsDailyHourly(Locate: TLocate): Boolean;
    procedure SetCallCenters(const Centers: string);
    function GetCallCenterClientIDsSQLString: string;
    procedure LogSQL(const SQL: string);
    procedure Rate(Locate: TLocate; ForceRate: Boolean = False);
    function MapClientCode(Loc: TLocate): string;
    procedure LoadUsageFromDir(Directory: string);
    procedure SetBillCode(Locate: TLocate; const BillCode: string);
    procedure ForEachBilledTicket(CallCenter: string; TicketProc: TForEachTicketProc);
    procedure CreateLocateObjectsFromDataSets(TicketData, LocateData: TDataSet);
    procedure ApplySalesTax(Loc: TLocate);
    procedure LogDates;
    function EligibleTerm(Loc: TLocate): Boolean;
    procedure AddSplitLineItems;
    procedure AddSplitHPCharge(HPLocate: TLocate);
    procedure WarnForExtraChargeLocates(Locate: TLocate);
    procedure AddAdditionalQtyCharge(QtyLocate: TLocate);
    function HourlyLocatePrice(Locate: TLocate; Hours: Currency; Overtime: Boolean): Currency;
    function AddHourlyCharge(Locate: TLocate; DayHours,
      TotalHours: Currency; Overtime, NoCharge: Boolean): TLocate;
    function ConvertToBillingUnits(const UnitsMarked, FirstUnitFactor, RestUnitsFactor: Integer): Integer;
    procedure AddOSTransmissionCharge(Locate: TLocate);
    procedure StartPerformanceLog;
    procedure StopPerformanceLog;
    function DebugBilling: Boolean;
    procedure CleanupPreviousBillingRuns(BillID: Integer);
    procedure FlagTktDups;
  public
    Period: TBillPeriod;
    OutputBaseDir: string;
    constructor Create;
    destructor Destroy; override;
    procedure SetConnectionLike(MainConn: TADOConnection);
    procedure SetBillingTimeout(Timeout: Integer);
    procedure SetCallCenterGroup(GroupName: string);
    function RunDateRangeBilling(const Description: string): Integer;
    function RunBillingFromDir(const Description, Directory: string): Integer;
    procedure ExportResults(BillID: Integer; Dir: string = '');
    function DestinationDir: string;
    function BillingRunName: string;

    property ExportRawDataOnly: Boolean read FExportRawDataOnly write FExportRawDataOnly;
    property NoExportOrSave: Boolean read FNoExportOrSave write FNoExportOrSave;
    property Logger: TLogEvent read FLogEvent write FLogEvent;
    property ErrorCount: Integer read FErrorCount;
    property NoOldRunCleanup: Boolean read FNoOldRunCleanup write FNoOldRunCleanup;
  end;

  procedure ObjectListToLocateArray(Objects: TObjectList; var Locates: TLocateArray);

implementation

uses
  Types, Math, IniFiles, RawDataIO, dialogs,
  OdVclUtils, OdAdoUtils, BillingDMu, DateUtils,
  ComObj, OdDBISAMUtils, DBISAMTb, QMConst, OdIsoDates, OdDbUtils, StrUtils,
  JclFileUtils, OdExceptions;

const
  LocateErrorLimit = 300;

{ TBillingEngine }

// ********************** OUTSIDE ENTRY POINTS ***********************
// This outside interace would ideally be cleaner...

procedure TBillingEngine.SetConnectionLike(MainConn: TADOConnection);
const
  INI_DB='Database';
  INI_CONN_STR='ConnectionString';
begin
  Log('Connecting to the database...');
  BEDM.Conn.Close;//qm-491 in case already open  sr
  BEDM.Conn.ConnectionString := QMBillingDM.GetIniSetting(INI_DB,INI_CONN_STR,'');//MainConn.ConnectionString;
  BEDM.Conn.Open;
end;

procedure TBillingEngine.SetCallCenterGroup(GroupName: string);
var
  Centers: TStringArray;
begin
  FCallCenterGroupID := QMBillingDM.ConfigDM.GetCenterGroupID(GroupName);
  FGroupName := GroupName;
  Centers := CenterGroup(GroupName);
  if Length(Centers) < 1 then
    raise Exception.CreateFmt('The call center group named %s has no defined call centers', [GroupName]);
  SetCallCenters(StringArrayToDelimitedString(Centers, '', sLineBreak));
end;

procedure TBillingEngine.LogDates;
begin
  Log('Running billing for:');
  Log('  Description: ' + FDescription);
  Log('  Period Type: ' + Period.Span);
  Log('  Start Date: ' + DateTimeToStr(Period.StartDate) + ' (midnight)');
  Log('  End Date: ' + DateTimeToStr(Period.EndDate) + ' (midnight)');
  Log('  Look Back Date: ' + DateTimeToStr(Period.LookBackDate) + ' (midnight)');
end;

function TBillingEngine.RunBillingFromDir(const Description, Directory: string): Integer;
begin
  ThisIsCalifornia := false;
  StartPerformanceLog;
  try
    FDescription := Description;
    LogDates;
    Log('  Raw Data: ' + Directory);

    Initialize;

    LoadUsageFromDir(AddSlash(Directory));

    MainProcessing;    //RunBillingFromDir
    Result := FBillID;
  finally
    StopPerformanceLog;
  end;
end;

function TBillingEngine.RunDateRangeBilling(const Description: string): Integer;
begin
  ThisIsCalifornia := false;
  StartPerformanceLog;
  try
    FDescription := Description;
    LogDates;

    Initialize;

    GatherTicketsToConsider;
//    LoadTicketNotes; this method throws a table lock exception.
    LoadHoursUnitsData;
    LoadUsage;
    if not FExportRawDataOnly then
      MainProcessing;   //RunDateRangeBilling
    Result := FBillID;
  finally
    StopPerformanceLog;
    BEDM.Disconnect;
  end;
end;

function TBillingEngine.DestinationDir: string;
begin
  Result := FExportDM.DestinationDir;
end;

// ********************* Private Methods **********************

constructor TBillingEngine.Create;
begin
  inherited;
  BEDM := TBillingEngineDM.Create(nil);
  FExportDM := TBillingExportDM.Create(nil);

  FCallCenterGroupID := -1;
  FTicketClientList := TTicketClientList.Create(nil);
  // FTicketClientList.TieredClientFunc set in Initialize

  FBillingCodesInProgress := TStringList.Create;
  FBucketPriceMap := TStringList.Create;
  FCallCenters := TStringList.Create;
  FEligibleTerms := TStringList.Create;

  FResultRecords := TObjectList.Create;
  FResultRecords.OwnsObjects := False;
end;

destructor TBillingEngine.Destroy;
begin
  FreeAndNil2(TObject(BEDM), Log1);
  FreeAndNil2(TObject(BillingConfig), Log1);
  FreeAndNil2(TObject(FExportDM), Log1);
  FreeAndNil2(TObject(FTicketClientList), Log1);
  FreeAndNil2(TObject(FBillingCodesInProgress), Log1);
  FreeAndNil2(TObject(FBucketPriceMap), Log1);
  FreeAndNil2(TObject(FLocates), Log1);
  FreeAndNil2(TObject(FCallCenters), Log1);
  FreeAndNil2(TObject(FEligibleTerms), Log1);
  FreeAndNil2(TObject(FResultRecords), Log1);
  FreeAndNil2(TObject(FTicketLocates), Log1);
  inherited;
end;

function TBillingEngine.GetStatusDescription(const Status: string): string;
begin
  Result := BillingConfig.GetStatusDescription(Status);
end;

procedure TBillingEngine.GatherTicketsToConsider;
const
  CenterTixSQL =
    'insert into #tix '+
    'select distinct l.ticket_id, t.ticket_number, t.ticket_format, t.revision, 0 '+  //qm-728
    'from locate l with (INDEX (locate_closed_date_client_id) NOLOCK) '+
    '  inner join ticket t on l.ticket_id = t.ticket_id '+
    'where l.closed_date >= ''%s'' and l.closed_date < ''%s'' '+
    '  and l.closed = 1 and l.status <> ''-N'' and (l.invoiced = 0 or invoiced is null) '+
    '  and l.client_id in (%s) ';

  DailyTixSQL =
    'declare @IDs table (ID int not null, TxNo varchar(20) not null, ticket_format varchar(20) not null, Revision varchar(20)null, TixIsDup Bit NOT NULL DEFAULT(0) )'+ //qm-728 add call center here for dups
    'insert into @IDs '+
    'select distinct t.ticket_id, t.ticket_number, t.ticket_format, t.revision, 0 '+   //qm-728
    '  from locate l with (NOLOCK) '+
    '  inner join ticket t on l.ticket_id = t.ticket_id '+
    '  inner join locate_hours h on l.locate_id = h.locate_id '+
    '  where h.work_date >= ''%s'' '+
    '    and h.work_date < ''%s'' '+
    '    and t.ticket_format in (%s) '+
	  '    and l.client_id in (%s) '+
    'delete from @IDs where ID in (select ticket_id from #tix) '+
    'insert into #tix '+
    'select ID, TxNo, ticket_format, Revision, TixIsDup from @IDs ';  //qm-728
var
  Cmd: TADOCommand;
  RecordsAffected: Integer;
  ClientIdList: string;
begin
  RecordsAffected:=0;
  PerfLog.Start('GatherTicketsToConsider');
  BEDM.ConnectReadConn(Log1, QMBillingDM.IniFileName);

  Log('Gathering list of tickets to consider...');
  LogSQL(BEDM.MakeEmptyTix.CommandText);
  BEDM.MakeEmptyTix.Execute;

  Log('Checking configuration...');
  ClientIdList := GetCallCenterClientIDsSQLString;
  if Trim(ClientIdList) = '' then
    raise EOdDataEntryError.CreateFmt('No clients in these centers are assigned a %s billing period', [Period.Span]);

  Cmd := BEDM.AddCentersToTix;
  Log('Client IDs for locate selection: ' + ClientIdList);
  LogFmt('Running usage query (Timeout %d sec)', [Cmd.CommandTimeout]);
  Cmd.CommandText := Format(CenterTixSQL, [IsoDateTimeToStr(Period.LookBackDate),
    IsoDateTimeToStr(Period.EndDate), ClientIdList]);
  LogSQL(Cmd.CommandText);
  Cmd.Execute;

  // We add the index after initial population for speed reasons
  BEDM.AddTixIndex.Execute;
  LogSQL(BEDM.AddTixIndex.CommandText);

  Cmd := BEDM.AddDailyTix;
  if HourlyOrUnitBillingEnabled then begin
    Log('Finding non-closed tickets with daily hours/units...');
    Cmd.CommandText := Format(DailyTixSQL, [IsoDateTimeToStr(Period.LookBackDate),
            IsoDateTimeToStr(Period.EndDate), GetCallCenterSQLString, ClientIdList]);
    LogSQL(Cmd.CommandText);
    Cmd.Execute(RecordsAffected, EmptyParam);
    LogFmt('Found %d non-closed tickets with daily hours/units', [RecordsAffected]);
  end;
  Log('Searching for duplicate tickets...');
  FlagTktDups;

end;

procedure TBillingEngine.FlagTktDups;
var
  TicketID: Integer;
  TicketNumber: string;
  TicketFormat: string;   //qm-728
  Revision: string;
  DupIDsList: TStringList;
  i: Integer;
begin
  try
    DupIDsList := TStringList.Create;
    LogSQL(BEDM.qryTicketDups.SQL.text);
    BEDM.qryTicketDups.Open;
    while not(BEDM.qryTicketDups.Eof) do
    begin
      TicketID := BEDM.qryTicketDups.FieldByName('ticket_id').AsInteger;

      if ((BEDM.qryTicketDups.FieldByName('TxNo').AsString = TicketNumber) and
          (BEDM.qryTicketDups.FieldByName('Ticket_Format').AsString = TicketFormat) and  //qm-728
        (BEDM.qryTicketDups.FieldByName('Revision').AsString = Revision)) then
        DupIDsList.AddObject(TicketNumber + '-' +TicketFormat + '-' + Revision, TObject(TicketID)); //qm-728

      TicketNumber := BEDM.qryTicketDups.FieldByName('TxNo').AsString;
      TicketFormat := BEDM.qryTicketDups.FieldByName('Ticket_Format').AsString;  //qm-728
      Revision := BEDM.qryTicketDups.FieldByName('Revision').AsString;
      BEDM.qryTicketDups.next;
    end;

    for i := 0 to DupIDsList.Count - 1 do
    begin
      BEDM.updTicketDups.Parameters.ParamByName('ticketID').Value :=
        Integer(DupIDsList.Objects[i]);
      BEDM.updTicketDups.execSQL;
    end;
  finally
    Log( IntToStr(DupIDsList.Count) +' duplicate ticket number-ticket format-revision found and flagged');
    FreeAndNil(DupIDsList);
    BEDM.qryTicketDups.close;
  end;

end;


procedure TBillingEngine.LoadUsage;
var
  RawDataDir: string;
begin
  try
    try
      PerfLog.Start('Downloading Tickets');
      Log('Downloading tickets from the database...');
      LogSQL(BEDM.TicketRawData.CommandText);
      BEDM.TicketRawData.Open;
      LogFmt('Loaded %d tickets', [BEDM.TicketRawData.RecordCount]);

      PerfLog.Start('Downloading Locates');
      Log('Downloading locates from the database...');
      LogSQL(BEDM.LocateRawData.CommandText);
      BEDM.LocateRawData.Open;
      LogFmt('Loaded %d locates', [BEDM.LocateRawData.RecordCount]);
    except
      on E: EOleException do begin
        E.Message := Format('%s (Timeout: %d)', [E.Message, BEDM.TicketRawData.CommandTimeout]);
        raise;
      end;
    end;

    PerfLog.Start('Creating TLocate Objects');
    CreateLocateObjectsFromDataSets(BEDM.TicketRawData, BEDM.LocateRawData);

    if FExportRawDataOnly or (QMBillingDM.GetIniSetting('Billing', 'RawDataExport', '0') = '1') then begin
      PerfLog.Start('Export Raw Data');
      RawDataDir := ExpandFileName('Raw Data '+ FGroupName);
      LogFmt('Saving raw data to %s...', [RawDataDir]);
      ExportRawBillingData(RawDataDir, BEDM.LocateRawData, BEDM.TicketRawData, BEDM.TicketNotes, BEDM.HoursUnits);
      Log('Done saving raw data');
    end
    else
      Log('Skipping raw data export. [Billing] RawDataExport=1 not set');
  finally
    BEDM.LocateRawData.Close;
    BEDM.TicketRawData.Close;
    BEDM.DropTix.Execute;
  end;
end;

procedure TBillingEngine.Log1(Sender: TObject; const Msg: string);
begin
  if Assigned(FLogEvent) then
    FLogEvent(Sender, IsoTimeToStrNoMillis(Time) + ': ' +Msg);
  Application.ProcessMessages;
end;

procedure TBillingEngine.Log(const Msg: string);
begin
  Log1(Self, Msg);
end;

procedure TBillingEngine.LogFmt(const Msg: string; Params: array of const);
begin
  Log(Format(Msg, Params));
end;

procedure TBillingEngine.LogError(const Msg: string);
begin
  Log(Msg);
  Inc(FErrorCount);
end;

procedure TBillingEngine.LogErrorFmt(const Msg: string; Params: array of const);
begin
  LogError(Format(Msg, Params));
end;

function TBillingEngine.MapClientCode(Loc: TLocate): string;
var
  Val: string;
begin
  if Loc.ClientCode = '' then
    raise EBillingException.Create('MapClientCode called with blank ClientCode');

  Result := Loc.ClientCode;

  if Loc.ClientCode = 'NJN' then begin
    Val := BillingConfig.LookupCodeMap(Loc.WorkState, Loc.WorkCounty, Loc.WorkCity);
    if Val <> '' then
      Result := Val;
  end;

  if Result = '' then
    raise EBillingException.Create('MapClientCode can not return a blank client code.  Locate: ' + IntToStr(Loc.LocateID));
end;

procedure TBillingEngine.PopulateTicketClientList;
var
  i: Integer;
  Loc: TLocate;
begin
  PerfLog.Start('PopulateTicketClientList');
  Log('Loading ticket-client-status data for tiering...');

  for i := 0 to FLocates.Count - 1 do begin
    Loc := TLocate(FLocates[i]);
    FTicketClientList.Add(Loc.TicketID, Loc.LocatorID, Loc.RefID, Loc.ClientCode, Loc.Status);  //QMANTWO-710
  end;
end;

procedure TBillingEngine.ProcessLocate(Locate: TLocate; DoRate: Boolean);
var
  ForceHourlyRate: Boolean;
begin
  // We need to force rate some H status locates even if the locate is not closed
  // because some centers bill entered hours before the locate is closed.
  ForceHourlyRate := Locate.IsHourly and
    ((not Locate.Closed) or (Locate.Closed and ((Locate.ClosedDate < Period.LookBackDate) or (Locate.ClosedDate >= Period.EndDate)))) and CenterBillsHoursDaily(Locate.CallCenter);

  // We will often have already-invoiced locates to skip, when doing
  // monthly/week mixed centers.
  if Locate.Invoiced and not(ForceHourlyRate) then
    Exit;

  if not EligibleTerm(Locate) then
    Exit;

  if not ((Locate.Closed) or ForceHourlyRate) then
    Exit;

  if (Locate.ClosedDate < Period.LookBackDate) and not(ForceHourlyRate) then
    Exit;

  if (Locate.ClosedDate >= Period.EndDate) and not(ForceHourlyRate) then
    Exit;

  try
    RemoveNJNHack(Locate);
    // We don't rate ticket-level screen charges (manually rated elsewhere)
    if DoRate then
      Rate(Locate, ForceHourlyRate);
    if Locate.Bucket = '' then
      raise EBillingException.Create('Every item must go in a bucket');

    if not ForceHourlyRate then
      FResultRecords.Add(Locate);
  except
    on E: Exception do begin
      LogErrorFmt('ERROR processing %s / %s:', [Locate.TicketNumber, Locate.ClientCode]);
      Log(E.Message);
      if FErrorCount > LocateErrorLimit then
        raise;
    end;
  end;
end;

procedure TBillingEngine.ProcessAllLocates;
var
  i: Integer;
  Locate: TLocate;
begin
  PerfLog.Start('Rating Locates');
  Log('Rating locates...');
  InitializeBilledTicketsTable;
  FBucketPriceMap.Clear;
  FResultRecords.Clear;

  // These are the only centers (LWA1/LID1/NewJersey) that currently use a fast ticket/locate map
  if CurrentlyBilling(CenterGroup('LWA', False)) and (not Assigned(FTicketLocates)) then
    FTicketLocates := TLocatesByTicket.Create(FLocates, CenterGroup('LWA', False))
  else if CurrentlyBilling(CenterGroup('STS750', False)) and (not Assigned(FTicketLocates)) then
    FTicketLocates := TLocatesByTicket.Create(FLocates, CenterGroup('STS750', False))
  else if CurrentlyBilling(CenterGroup('1851', False)) and (not Assigned(FTicketLocates)) then //QMANTWO-335
    FTicketLocates := TLocatesByTicket.Create(FLocates, CenterGroup('1851', False))  //QMANTWO-335
  else if CurrentlyBilling(CenterGroup('1421', False)) and (not Assigned(FTicketLocates)) then //QM-786
    FTicketLocates := TLocatesByTicket.Create(FLocates, CenterGroup('1421', False))  //QM-786
  else if CurrentlyBilling(CenterGroup('MDDC', False)) and (not Assigned(FTicketLocates)) then //QM-309
    FTicketLocates := TLocatesByTicket.Create(FLocates, CenterGroup('MDDC', False));  //QM-309

  for i := 0 to FLocates.Count - 1 do begin
    Locate := TLocate(FLocates[i]);
    ProcessLocate(Locate, True);
    if i mod 50 = 0 then
      Application.ProcessMessages;
    if i mod 2000 = 0 then
      Log(IntToStr(i) + '...');
  end;
  Log('Finished rating');
end;

procedure TBillingEngine.ProcessUsage;
begin
  FResultRecords.Clear;
  FBucketPriceMap.Clear;

  ProcessAllLocates;
  AddLocateObjectsForScreenCharges;
  CorrectForDuplicateLocates;
  CorrectFMBCOHWLocates;
  AddSplitLineItems;
  ProcessDailyHoursUnits;
end;

procedure TBillingEngine.AddLocateObjectsForScreenCharges;
var
  i: Integer;
  Locate: TLocate;
  ScreenCharge: TLocate;
  AddedCount: Integer;
begin
  PerfLog.Start('AddLocateObjectsForScreenCharges');
  Log('Adding ticket-level screen charges...');
  AddedCount := 0;

  for i := FLocates.Count - 1 downto 0 do begin
    Locate := TLocate(FLocates[i]);
    AddScreenChargeForLocate(Locate);

    if Locate.ScreenCharge > 0.0001 then begin

      ScreenCharge := Locate.CreateExtraChargeCopy;
      try
        ScreenCharge.Bucket := 'Ticket Screen Charge';
        ScreenCharge.QtyMarked := 1;
        ScreenCharge.Status := 'S';
        ScreenCharge.QtyCharged := 1;
        ScreenCharge.Price := ScreenCharge.ScreenCharge;
        ScreenCharge.Rate := ScreenCharge.ScreenCharge;
        Assert(ScreenCharge.CallCenter = 'FOL1');
        Assert(StrBeginsWith('OUC', ScreenCharge.ClientCode));
        ScreenCharge.ClientCode := 'OUC-' + BillCodeTicketScreenCharge;
        ScreenCharge.BillingClientCode := 'OUC-' + BillCodeTicketScreenCharge;
        SetBillCode(ScreenCharge, BillCodeTicketScreenCharge);
        // These items are not added to FLocates, so they are lost if we ever need to re-rate for OCC
        ProcessLocate(ScreenCharge, False);
        Inc(AddedCount);
      except
        ScreenCharge.Free;
      end;
    end;
  end;
  LogFmt('Added %d ticket-level screen charges', [AddedCount]);
end;

procedure TBillingEngine.Rate(Locate: TLocate; ForceRate: Boolean);
const
  NoRate = -9999;
var
  Qty: Integer;
  FoundRate: Currency;
  Parties: Integer;
  BillCode: string;
  LineItemText: string;
  ShowParties: Boolean;
  TicketType: string;
  IsBatonRougeNoCharge: Boolean;
  IsNoCharge: Boolean;
  RateEntry: TRateEntry;

  CloseDateHour:Integer;
  ClosedAfterHrs, CalledAfterHrs:boolean;
  TransmitDateHour:Integer;
  DT:TDateTime;
  procedure ForceNoCharge(const Description: string = '');
  begin
    if IsDailyHourly(Locate) then
      Exit;
    FoundRate := 0;
    Locate.Bucket := 'No Charge';
    if Description <> '' then
      Locate.Bucket := Locate.Bucket + ', ' + Description;
    IsNoCharge := True;
    ShowParties := False;
  end;

  procedure ForceNoChargeTraining(const Description: string = '');    //QMANTWO-620
  begin
    FoundRate := 0;
    Locate.Bucket := 'No Charge';
    Locate.Rate := 0.0;
    Locate.AdditionalQtyRate := 0.0;
    Locate.Price := 0.0;
    Locate.ScreenCharge := 0.0;
    Locate.OvertimeRate := 0.0;
    Locate.TaxRate := 0.0;
    Locate.TaxAmount := 0.0;
    Locate.TicketType := Description;
    if Description <> '' then
      Locate.Bucket := Locate.Bucket + ', ' + Description;
    IsNoCharge := True;
    ShowParties := False;
  end;

  procedure MakeEmergency;
  begin
    TicketType := TicketTypeEmerg;
    Locate.Bucket := 'Emergency Notice, ' + GetStatusDescription(Locate.Status);
  end;

  procedure Make800LPM;
  begin
    TicketType := TicketType800LPM;
    Locate.Bucket := 'Ca Tkt >800ft, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeAfterHours;
  begin
    TicketType := TicketTypeAfter;
    Locate.Bucket := 'After Hours Notice, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeLate(LateDate:String); //QM-77
  begin
    TicketType := 'LATE';
    Locate.Bucket := LateDate+' After Due Date, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeDamage;
  begin
    TicketType := TicketTypeDamage;
    Locate.Bucket := 'Damage Ticket, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeUpdate;
  begin
    TicketType := TicketTypeUpdate;
    Locate.Bucket := 'Normal Update, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeNormal;
  begin
    TicketType := TicketTypeNormal;
    Locate.Bucket := 'Normal Notice, ' + GetStatusDescription(Locate.Status);
    IsNoCharge := False;
  end;

  procedure MakeDesign;
  begin
    TicketType := TicketTypeDesign;
    Locate.Bucket := 'Design Notice, ' + GetStatusDescription(Locate.Status);
  end;

  procedure MakeShortNotice;
  begin
    TicketType := TicketTypeShortNotice;
    Locate.Bucket := 'Short Notice, ' + GetStatusDescription(Locate.Status);
  end;

  procedure RunPartyCount;
  const
      BOTH = 'Both';   //QMANTWO-342
  var
    i : integer;
  begin
    if IsNoCharge then
      Exit;
    if BillingConfig.IsTieredClient(Locate.CallCenter, Locate.ClientCode) then begin
      Parties := FTicketClientList.CountRespectedLocates(Locate.TicketID, Locate.ClientID, Locate.ClientCode, Locate.CallCenter);
      if (Locate.CallCenter = '1851') and not(Locate.AreaName=BOTH) then Parties := 1;      //QMANTWO-335
      if Parties > 1 then
        ShowParties := True;
    end;
  end;

  function GetLWALIDBillingCity(Locate: TLocate): string;
  var
    Locates: TLocateArray;

    function GetJointBillingCity(const GasGroup, ElecGroup: string; Statuses: array of string): string;
    var
      GasLocates: TLocateArray;
      ElecLocates: TLocateArray;
    begin
      // This strange rating rule only applies to both locates being M status
      GetLocatesMatching(TermGroup(GasGroup),  Statuses, Locates, GasLocates);
      GetLocatesMatching(TermGroup(ElecGroup), Statuses, Locates, ElecLocates);

      // We assume for now that these terms are not going to use City-specific
      // billing, so we misuse this field to get rates for combined gas/elec M tickets
      Result := Locate.WorkCity;
      if (Length(GasLocates) >= 1) and (Length(ElecLocates) >= 1) then
        Result := 'Both';
    end;

  begin
    Assert(Assigned(FTicketLocates), 'FTicketLocates not present');
    FTicketLocates.GetTicket(Locate.TicketID, Locates);
    Assert(Length(Locates) > 0, 'No locates on ticket ' + IntToStr(Locate.TicketID));

    Result := Locate.WorkCity;

    if MatchesCenterGroup(Locate, 'LWA', False) then begin
      // LWA1
      if MatchesOneOfTermGroups(Locate.ClientCode, ['PSEGasTerms', 'PSEElectricTerms']) then
        Result := GetJointBillingCity('PSEGasTerms', 'PSEElectricTerms', ['*']) // Any status is valid for the PSE terms
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA04GasTerms', 'AVISTAP04ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA04GasTerms', 'AVISTAP04ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA03GasTerms', 'AVISTAP03ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA03GasTerms', 'AVISTAP03ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA15GasTerms', 'AVISTAP15ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA15GasTerms', 'AVISTAP15ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA16GasTerms', 'AVISTAP16ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA16GasTerms', 'AVISTAP16ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA17GasTerms', 'AVISTAP17ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA17GasTerms', 'AVISTAP17ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['WAPOW03GasTerms', 'WAPOWP03ElectricTerms']) then
        Result := GetJointBillingCity('WAPOW03GasTerms', 'WAPOWP03ElectricTerms', ['M', 'NL'])
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['TacomaCableTerms', 'TacomaPowerTerms']) then
        Result := GetJointBillingCity('TacomaCableTerms', 'TacomaPowerTerms', ['M', 'NL'])
      // LID1
      else if MatchesOneOfTermGroups(Locate.ClientCode, ['AVISTA29GasTerms', 'AVISTAP29ElectricTerms']) then
        Result := GetJointBillingCity('AVISTA29GasTerms', 'AVISTAP29ElectricTerms', ['M', 'NL']);
    end
  end;

  function Get7501BillingArea(Locate: TLocate): string;
  var
    Locates: TLocateArray;
  const
    ARR_BGE_Status : array[0..8] of string = ('C','H','M','O','XR','XXBA','DCF','DM','DNC'); //qm-357

    function GetJointBillingArea(const GasGroup, ElecGroup: string; Statuses: array of string): string;
    var
      GasLocates: TLocateArray;
      ElecLocates: TLocateArray;
      i : integer;
    begin   //QMANTWO-335
      // This rating rule is similar to LWA; special rate applies when both locates are on the ticket
      GetLocatesMatching(TermGroup(GasGroup),  Statuses, Locates, GasLocates);
      GetLocatesMatching(TermGroup(ElecGroup), Statuses, Locates, ElecLocates);

      // Assume these terms do not use AREA-specific billing, so it is safe to
      // misuse this field to get rates for combined gas/elec tickets.
      Result := Locate.AreaName;
      if (Length(GasLocates) >= 1) and (Length(ElecLocates) >= 1) then
        Result := 'Both';
    end;
  begin
    Result := '';
    Assert(Assigned(FTicketLocates), 'FTicketLocates not present');
    FTicketLocates.GetTicket(Locate.TicketID, Locates);
    Assert(Length(Locates) > 0, 'No locates on ticket ' + IntToStr(Locate.TicketID));

    Result := Locate.AreaName;
    if Locate.CallCenter = '7501' then begin
      if MatchesOneOfTermGroups(Locate.ClientCode, ['PECOGasTerms', 'PECOElectricTerms']) then
        Result := GetJointBillingArea('PECOGasTerms', 'PECOElectricTerms', ['*']);
    end
    else
    if Locate.CallCenter = '1851' then begin  //QMANTWO-335
      if MatchesOneOfTermGroups(Locate.ClientCode, ['185NipscoGasTerms', '185NipscoElectricTerms']) then //QMANTWO-335
        Result := GetJointBillingArea('185NipscoGasTerms', '185NipscoElectricTerms', ['*']); //QMANTWO-335
    end
    else
    if Locate.CallCenter = '1421' then begin  //QM-786
      if MatchesOneOfTermGroups(Locate.ClientCode, ['142ScanaGasTerms', '142ScanaElectricTerms']) then //QM-786
        Result := GetJointBillingArea('142ScanaGasTerms', '142ScanaElectricTerms', ['*']); //QM-786
    end
    else
    if Locate.CallCenter = 'FMB1' then begin  //QM-308
      if MatchesOneOfTermGroups(Locate.ClientCode, ['FMBBGEGTerms', 'FMBBGEETerms']) then //QM-308
        Result := GetJointBillingArea('FMBBGEGTerms', 'FMBBGEETerms', ARR_BGE_Status); //QM-308  357
    end;
  end;


begin
  CloseDateHour:=0;
  TransmitDateHour:=0;
  if Locate.ClientCode = 'NJNO' then
    raise EBillingException.Create('An NJNO code made it to rating as client_code');

  if Locate.ClientCode = '' then
    raise EBillingException.Create('Can''t rate with empty client_code');

  if not (Locate.Closed) and not(ForceRate) then
    raise EBillingException.Create('Cannot rate an open locate, status=' + Locate.Status);

  if (Locate.ClosedDate < Period.LookBackDate) and not(ForceRate) then
    raise EBillingException.Create('Locate is before billing date range');

  if (Locate.ClosedDate >= Period.EndDate) and not(ForceRate) then
    raise EBillingException.Create('Locate is after billing date range');

  if (Locate.Status = 'OH') and ForceRate then
    Locate.Status := 'H'; // treat an Ongoing Hourly status as an Hourly status if billing daily hours

  if DebugBilling then
    LogFmt('Rating Locate %d/%s/%s on ticket %d/%s/%s', [Locate.LocateID, Locate.ClientCode, Locate.Status, Locate.TicketID, Locate.TicketNumber, Locate.TicketType]);

  FoundRate := NoRate;
  ShowParties := False;
  Parties := 1;

  MakeNormal;

  if Locate.UpdateOf <> '' then
    MakeUpdate;

  // Enforce a default 0-price NC rate, if no NC rates exist for this term
  if Locate.Status = 'NC' then begin
    if not BillingConfig.GetRateTable.HaveRateForTermStatus(Locate) then
      ForceNoCharge;
  end;

  if Locate.ClientCode = '' then
    raise Exception.Create('Client code is blank');
  if Locate.CallCenter = '' then
    raise Exception.Create('Call center is blank');

  Locate.Price := 0;
  Locate.RawUnits := Locate.QtyMarked;
  Qty := Locate.QtyMarked;
  if Qty = 0 then
    Qty := 1;

  Locate.BillingClientCode := MapClientCode(Locate);
  Locate.BillingCity := Locate.WorkCity; // Overriden only for LWA/LID below
  Locate.QtyCharged := Qty;
  Assert(Locate.QtyCharged > 0);

  if BillingConfig.IsExcludedWorkFor(Locate.ClientID, Locate.WorkDoneFor) then begin
    Locate.Bucket := 'Work Done For Client';
    Locate.Price := 0;
    Exit;
  end;

  if BillingConfig.IsExcludedWorkType(Locate.ClientID, Locate.WorkType) then begin
    Locate.Bucket := 'Drops';
    Locate.Price := 0;
    Exit;
  end;

  // NOTE: This is a gigantic set of if/else clauses (one for each center group)

  // New Jersey NJN (NewJersey) billing rules
  if (Locate.CallCenter = CallCenterNewJersey) then
  begin
    // NewJersey locators should never close a locate with the S status,
    // but they do sometimes.  Treat S locates as ST - "Screened Ticket"
    if Locate.Status = 'S' then
      Locate.Status := 'ST';

    if (Locate.ClientCode = 'AE1') then
    begin
      Qty := 1; // only charge this client per locate, not unit
      RunPartyCount;
    end;

    if MatchesTermAndWorkDoneForGroup(Locate, 'FNJAWNoChargeTerms', 'FNJAmericanWaterWDF') and // QMANTWO-346
      MatchesTermAndExcavatorGroup(Locate, 'FNJAWNoChargeTerms', 'FNJAmericanWaterCDW') then
      ForceNoCharge('American Water');

    if MatchesTermAndWorkDoneForGroup(Locate, 'FNJEmbarqTerms', 'EmbarqTermsWDF') then
      ForceNoCharge('Embarq Work');

    if Locate.WorkState = '' then
      Locate.WorkState := 'NJ';

    if (Locate.Status <> 'NC') and (Locate.Status <> 'OS') then
    begin
      if Locate.IsEmergency then
      begin
        if ((Locate.ClientCode = 'EG1') or  (Locate.ClientCode ='EG2')) then // QMANTWO-705  QM-1051
        begin
          MakeEmergency; // QM-705
          if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or // after hours
            (((HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12 + 5))) and
            ((HourOf(Locate.ClosedDate) < (7)) or (HourOf(Locate.ClosedDate) >= (12 + 5)))) then
            MakeAfterHours;

          if HoursBetween(Locate.InitialStatusDate, Locate.TransmitDate) > 1 then
            MakeNormal;
        end // Locate.ClientCode = 'EG1'
        else // not EG1
        begin // QMANTWO-655    //QM-705
          if ((IsWeekend(Locate.CallDate) or IsHoliday(Locate)) or // tests using transmit date
            (HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12 + 5))) then // (CallDate between 7am and 5pm)
            MakeAfterHours
          else // QMANTWO-655
            MakeEmergency;   //QMANTWO-655   hbgfbhfg
        end; // else  QMANTWO-655
      end // Locate.IsEmergency then
      else // or Locate.IsUpdate
        if Locate.IsUpdate then
        begin
          if Locate.ClientCode = 'EG1' then // QMANTWO-705
          begin
            MakeUpdate;
            if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or // after hours
              (((HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12 + 5))) and
              ((HourOf(Locate.ClosedDate) < (7)) or (HourOf(Locate.ClosedDate) >= (12 + 5)))) then
              MakeAfterHours;

            if HoursBetween(Locate.InitialStatusDate, Locate.TransmitDate) > 2 then
                MakeNormal;
          end // Locate.ClientCode = 'EG1'
          else // not EG1
          begin // QMANTWO-655    //QM-705
            if ((IsWeekend(Locate.CallDate) or IsHoliday(Locate)) or // tests using transmit date
              (HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12 + 5))) then // (CallDate between 7am and 5pm)
              MakeAfterHours
            else // QMANTWO-655
              MakeNormal;   //QMANTWO-655   //QM-705  changed  sr
          end; // else  QMANTWO-655  //QM-705
        end; // Locate.IsEmergency then

      if StrBeginsWith('NJN', Locate.ClientCode) then
      begin
        RunPartyCount;
        ShowParties := True;
      end;
    end;
  end // end FNJ rule

  // FCL1/FCL2 billing rules
  else if IsFCL(Locate.CallCenter) then begin
    if Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    if MatchesTermAndExcavatorGroup(Locate, 'FCLTWTerms', 'TimeWarnerCDW') or
      MatchesTermAndWorkDoneForGroup(Locate, 'FCLTWTerms', 'TimeWarnerWDF') then
      ForceNoCharge('Time Warner')
    else if MatchesCCDoneExc(Locate, 'FCLDPTerms', 'DukePowerWDF', 'DukePowerCDW') then
      ForceNoCharge('Duke Power');

    if (Locate.CallCenter = 'FCL1') and (Locate.ParentTicketID > 0) then begin
      if MatchesTermGroup(Locate, 'FCLCTCTerms') then
        ForceNoCharge('Duplicate Ticket');
    end;
  end // FCL

   // Arkansas
  else if IsARK(Locate.CallCenter) then
  begin  //qm-839 sr
    if StrBeginsWith('PROJECT TICKET', locate.TicketType) then //qm-839 sr
    ForceNoCharge(locate.TicketType);
  end //qm-839 sr
  // Atlanta (Atlanta, FAU) billing rules
  else if IsAtlanta(Locate.CallCenter) then begin
    if Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    if MatchesTermGroup(Locate.ClientCode, 'AtlantaTermUnits1') then
      Qty := 1;

    RunPartyCount;

    if MatchesTermAndWorkDoneForGroup(Locate, 'AtlantaCharterTerms', 'CharterComm-GA') then
      ForceNoCharge('Charter');
    if Locate.IsCancel and (not MatchesTermGroup(Locate, 'AtlantaCanBillCancels')) then
      ForceNoCharge('Cancel');
    if Locate.IsDamage and (not MatchesTermGroup(Locate, 'AtlantaCanBillDamages')) then
      ForceNoCharge('Damage');

    if MatchesTermGroup(Locate, 'AtlantaBellSouthTerms') then begin
      if Locate.ParentTicketID > 0 then
        ForceNoCharge('Duplicate Ticket')
      else if (not Locate.IsEmergencyOrManualAfterHoursType) and MatchesValueGroup(Locate.WorkDoneFor, 'BellSouthCompany') then
        ForceNoCharge('BellSouth');
    end;
  end

  // 300: STSAtlanta (300, 3002, 3003, 3004)
  else if IsSTSAtlanta(Locate.CallCenter) then begin
    RunPartyCount;

    if LocateMatchesTermWorkOrExc(Locate, 'AGLTermsNLR', 'AGLTermsWDF', 'AGLTermsCDW') then
      ForceNoCharge('AGL')
    else if LocateMatchesTermWorkOrExc(Locate, 'BSTTermsNLR', 'BSTTermsWDF', 'BSTTermsCDW') then
      ForceNoCharge('BST')
    else if LocateMatchesTermWorkOrExc(Locate, 'ComcastTermsNLR', 'ComcastTermsWDF', 'ComcastTermsCDW') then
      ForceNoCharge('Comcast')
    else if LocateMatchesTermWorkOrExc(Locate, 'CowetaTermsNLR', 'CowetaTermsWDF', 'CowetaTermsCDW') then
      ForceNoCharge('Coweta')
    else if MatchesTermAndWorkDoneForGroup(Locate, '300CharterTerms', 'CharterComm-GA') then
      ForceNoCharge('Charter');

     if Locate.IsCancel then    //qm-244 sr
      ForceNoCharge('Cancel')
    else if Locate.IsDamage then //qm-244 sr
      ForceNoCharge('Damage')  //qm-244 sr
    else if (Locate.IsProject) and (MatchesTermGroup(Locate.ClientCode, '3003ATT_Terms')) then
      ForceNoCharge('Project')  //qm-244 sr
    else if Locate.IsTraining then
      ForceNoCharge('Training')  //qm-244 sr
    else if Locate.IsAudit then
      ForceNoCharge('Audit');  //qm-244 sr
//  Removed the following per QM-424
//    else if ((Locate.TicketType = 'Large Project Excavation -2NDREQ')  //qm-314 sr
//          or (Locate.TicketType = 'LPEXCA -2NDREQ')  //qm-314 sr
//          or (Locate.TicketType = 'LPEXCA 2NDREQ'))  //qm-314 sr
//         and MatchesTermGroup(Locate.ClientCode, '3003ATT_Terms') then  //qm-314 sr   BSCA
//     ForceNoCharge('LPEXCA 2NDREQ');   //qm-314 sr

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

//  Rule removed qm-1040
// We can not bill the same non-meeting ticket more than once in a 21 day period
//    if (Locate.ParentTicketID > 0) and (not Locate.IsMeetingRequest) and
//      TicketHasLocate(Locate.ParentTicketID, Locate.ClientCode) and
//      (not MatchesTermGroup(Locate.ClientCode, '3003CanBillDupliates')) //QMANTWO-724
//    then
//    begin
//      //TODO: Move this into the main billing query and the TLocate objects
//      if ((Locate.CallDate - QMBillingDM.GetTicketCallDate(Locate.ParentTicketID)) < 21)
//        and not(StrContains('-400-', Locate.TicketNumber)) then  //qm-257  sr
//        ForceNoCharge('Duplicate Ticket');
//    end;
  end

  // Richmond (FCV1/2/3/4) billing rules
  else if IsFCV(Locate.CallCenter) then begin
    RunPartyCount;

    if MatchesTermGroup(Locate.ClientCode, 'FCV3IgnoreQtyMarked') then
      Qty := 1;

    if MatchesTermGroup(Locate.ClientCode, 'FCV3NothernNeckTerms') and
        MatchesValueGroup(Locate.WorkDoneFor, 'FCV3WorkDoneForNorthernNeck') then
      ForceNoCharge('Northern Neck');

    if (Locate.ClientCode = 'SHG01') and
        MatchesValueGroup(Locate.WorkDoneFor, 'FCVWorkDoneForShenandoahGas') then
      ForceNoCharge('Shenandoah Gas');

    if (Locate.ParentTicketID > 0) and MatchesOneOfTermGroups(Locate.ClientCode, ['FCV3DuplicateTerms21','FCV3DuplicateTerms18']) then
      ForceNoCharge('Duplicate Ticket');

    //TODO: 2245 - change this only check excavator too?
    if MatchesTermGroup(Locate.ClientCode, 'FCVPEPTerms') and
        MatchesValueGroup(Locate.Excavator, ValueGroupCompanyAllegheny) and
        MatchesValueGroup(Locate.WorkDoneFor, ValueGroupCompanyAllegheny) then
      ForceNoCharge('Allegheny');

    // Verizon terms can not be billed for late locates
    if MatchesTermGroup(Locate, 'FCV3DuplicateTerms21') and (Locate.WasEverLate or MatchesValueGroup(Locate.TicketType, 'VerizonNoChargeTT')) then
      ForceNoCharge('Late/NoShow');

    if MatchesTermGroup(Locate, 'FCVDupTermsVNG') and MatchesValueGroup(Locate.WorkDoneFor, 'VaNaturalGasWDF') then
      ForceNoCharge('VA Natural Gas');

    // Center 9301 is now part of FCV and reflected in these two rules
    if MatchesTermGroup(Locate, 'SprintTerms-VA') then
      Qty := 1;
    if MatchesTermAndWorkDoneForGroup(Locate, 'SprintTerms-VA', 'AllSprintTermsWDF') then
      ForceNoCharge('Work Done for Sprint');

    if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  //Begin OCC2 Fairfax (OCC1/2) billing rules  //QMANTWO-345  QM-98 decide what to do
  else if MatchesCenterGroup(Locate, 'OCC', False) then
  begin
    if (not IsNoCharge) then  //all Charge tickets in this block
    begin
       //OCC Verizon Terms
      if (MatchesTermGroup(Locate, 'OCCVerizonTerms')) then    //QM-179
      begin  //rules for OCCVerizonTerms
        if Locate.IsEmergency then
        begin
          if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or
            (((HourOf(Locate.CallDate) < (6)) or (HourOf(Locate.CallDate) >=
            (12 + 6))) and ((HourOf(Locate.ClosedDate) < (6)) or
            (HourOf(Locate.ClosedDate) >= (12 + 6)))) then
            MakeAfterHours
          else
            MakeEmergency;
        end
        else
        begin
          MakeNormal;
        end;
      end // if (MatchesTermGroup(Locate, 'OCCVerizonTerms'))
      else        //OCC Va American Water
      if (MatchesTermGroup(Locate, 'OCCVaAmericanWater')) then    //QM-627
      begin  //rules for OCCVaAmericanWater
        CloseDateHour:=0;
        TransmitDateHour:=0;
        ClosedAfterHrs:=False; CalledAfterHrs:=False;
        CloseDateHour:= HourOf(Locate.ClosedDate);
        TransmitDateHour:=HourOf(Locate.CallDate);

        CalledAfterHrs:= ((TransmitDateHour< 8) or (TransmitDateHour >= 17));
        ClosedAfterHrs:=((CloseDateHour < 6) or (CloseDateHour >= 18));

        if Locate.IsEmergency then
        begin
          if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) then MakeAfterHours
          else
          if  (CalledAfterHrs and ClosedAfterHrs)
            then MakeAfterHours
          else
            MakeEmergency;
        end
        else
        begin
          MakeNormal;
        end;
      end // if (MatchesTermGroup(Locate, 'OCCVaAmericanWater'))
      else if (MatchesTermGroup(Locate, 'OCCCOXTermsDups')) then    //QM-179
      begin  //rules for OCCCOXTermsDups
        if Locate.IsEmergency then
        begin
          if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or
            (((HourOf(Locate.CallDate) < (8)) or (HourOf(Locate.CallDate) >=
            (12 + 5))) and ((HourOf(Locate.ClosedDate) < (8)) or
            (HourOf(Locate.ClosedDate) >= (12 + 5)))) then
            MakeAfterHours
          else
            MakeEmergency;
        end
        else
        begin
          MakeNormal;
        end;
      end //if (MatchesTermGroup(Locate, 'OCCCOXTermsDups'))
      else if (Locate.IsEmergencyOrManualAfterHoursType) then // If not Verizon or Cox or VAW do...  QM-179
      begin  //default Emer/Afterhours
        if (IsWeekend(Locate.TransmitDate) or IsHoliday(Locate)) or
          ((HourOf(Locate.TransmitDate) < (7)) or
          (HourOf(Locate.TransmitDate) >= (12 + 5))) then
          MakeAfterHours
        else
          MakeEmergency;
      end
      else
      begin
        MakeNormal;
      end;
    End; // if (not IsNoCharge)

    // Note: The UpdateStatus calls here are hacks to cut down on inflation of party counts for OCC
    if (MatchesTermGroup(Locate, 'OCCWashGasTerms')) then
    begin
      if MatchesValueGroup(Locate.WorkDoneFor, 'OCCWashingtonGasWorkDoneFor')
      then
      begin
        if MatchesValueGroup(Locate.Excavator, 'OCCWashingtonGasWorkDoneFor')
        then
        begin
          if Locate.IsEmergencyOrManualAfterHoursType and (Locate.Status = 'XK')
          then
          begin
            ForceNoCharge;
            FTicketClientList.UpdateStatus(Locate.TicketID,
              Locate.ClientCode, 'NC');
          end;
        end;
      end;
    end;

    if (Locate.ParentTicketID > 0) and (NOT MatchesValueGroup(Locate.tickettype, 'OCC2NOTDUPS')) then   //QM-235 696 sr
    begin
      if not(IsTextInArray(Locate.ClientCode,   //QM-235  sr
        TermGroup('OCCCanBillTicketTwiceTerms')))then  // and (Locate.Status<>'M')
      begin
        ForceNoCharge('Duplicate Ticket');
        FTicketClientList.UpdateStatus(Locate.TicketID,
          Locate.ClientCode, 'NC');
      end;
    end;

    // Verizon terms can not be billed for late locates
    if MatchesTermGroup(Locate, 'OCCVerizonTerms') and
      (Locate.WasEverLate or MatchesValueGroup(Locate.TicketType,
      'VerizonNoChargeTT')) then
    begin
      ForceNoCharge('Late/NoShow');
      FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
    end;

    if MatchesTermGroup(Locate.ClientCode, 'OCCAlleghenyTerms') and
      MatchesValueGroup(Locate.Excavator, ValueGroupCompanyAllegheny) then
    begin
      ForceNoCharge('Allegheny');
      FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
    end;

    RunPartyCount;

    if Locate.IsCancel and (not MatchesTermGroup(Locate, 'OCCCanBillCancels'))
    then // QMANTWO-345
      ForceNoCharge('Cancel');
  end // occ2

  // Baton Rouge call centers
  else if IsLouisianaCenter(Locate.CallCenter) then begin
    if Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    if MatchesCCDone(Locate, 'BRBellSouthTermIDs', 'BellSouthCompany') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDoneExc(Locate, 'BRNoChargeTermsCrawford', 'BellSouthCompany', 'BRExcavatorGradyCrawford') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDoneExc(Locate, 'BRNoChargeTermsGulfgate', 'BellSouthCompany', 'BRExcavatorGulfgate') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDone(Locate, 'BRNoChargeTermsEatel', 'BRWorkDoneForEatel') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDone(Locate, 'BRNoChargeTermsCentury', 'BRWorkDoneForCentury') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDone(Locate, 'BRNoChargeTermsTransLA', 'BRWorkDoneForTransLA') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDoneExc(Locate, 'BRNoChargeTermsDeviney', 'BellSouthCompany', 'BRExcavatorDeviney') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDoneExc(Locate, 'BRNoChargeTermsEatelProTechs', 'BRWorkDoneForEatel', 'BRExcavatorProTechs') then
      IsBatonRougeNoCharge := True
    else if MatchesCCDoneExc(Locate, 'BRNoChargeTermsEllington', 'BellSouthCompany', 'BRExcavatorEllington') then
      IsBatonRougeNoCharge := True
    else
      IsBatonRougeNoCharge := False;

    if IsBatonRougeNoCharge then
      ForceNoCharge;

    if Locate.ParentTicketID > 0 then
      ForceNoCharge('Duplicate Ticket');
  end

  // Colorado billing rules (FCO1)
//  else if (Locate.CallCenter = 'FCO1') then begin
//    if MatchesTermGroup(Locate, 'FCOIgnoreQtyMarkedTerms') then
//      Qty := 1;
//
//    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType or Locate.IsDamage) then begin
//      if TreatAsAfterHours(Locate) then
//        MakeAfterHours
//      else
//        MakeEmergency;
//    end;
//
//    if (not IsNoCharge) and (Locate.TicketType = TicketTypeWatchAndProtect) then begin
//      TicketType := TicketTypeWatch;
//      Locate.WatchAndProtect := True;
//      Locate.Bucket := 'Watch and Protect, ' + GetStatusDescription(Locate.Status);
//    end;
//
//    if MatchesTermGroup(Locate, 'FCOCMSTerms') and
//      MatchesValueGroup(Locate.WorkType, 'FCODropBuryWorkTypes') and
//      MatchesValueGroup(Locate.Excavator, 'FCOTCSContractors') then
//    begin
//      ForceNoCharge('TCS Drop Bury 1');
//      Locate.Status := 'NC';
//    end;
//
//    RunPartyCount;
//  end

  // Baltimore billing rules (FMB1) and FMW terms  CenterGroup MDDC
  else if (Locate.CallCenter = 'FMB1') {or (Locate.CallCenter = 'FMW1') or (Locate.CallCenter = 'FMW3')} then // QM-451 merge
  begin
    RunPartyCount;

    if Locate.Status = 'ZZZ' then
    begin
      FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
      ForceNoCharge('ZZZ');
    end;

    If not(MatchesTermGroup(Locate.ClientCode, 'FMBBGEAllTerms')) then // Non BGE rules
    begin
      if not(IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then
      begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;

      if Locate.IsCancellation and not(MatchesTermGroup(Locate.ClientCode, 'FMBCanBillCancels')) then
      begin
        FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        ForceNoCharge('Cancellation');
      end;

      if (Locate.ParentTicketID > 0) then // QM-98/235  sr
      begin
        if not(IsTextInArray(Locate.ClientCode, // QM-98/235  sr
          TermGroup('FMBCanBillTicketTwiceTerms'))) then // QM-98/235  sr
        begin
          ForceNoCharge('Duplicate Ticket');
          FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        end;
      end;

//      if MatchesTermGroup(Locate.ClientCode, 'FMBVerizonTerms') and ((Locate.TicketType = 'NO RESPONSE') // QM-111
//        or (Locate.TicketType = 'FIOS NO RESPONSE')) then
//        ForceNoCharge('No Response');   qm-1019

    end; // Non BGE rules

    // Begin BGE land
    If MatchesTermGroup(Locate.ClientCode, 'FMBBGEAllTerms') then
    Begin
      if not(IsNoCharge) and Locate.IsEmergency then // qm-308
        MakeEmergency
      else
      begin
        if not(IsNoCharge) then
          MakeNormal; // qm-308
      end;

      if StrContains('BGE', Locate.ShortName) then // qm-456
      begin
        ForceNoCharge('No Charge- BGE Locator')
      end;

      if Locate.IsCancel then
      begin
        FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        ForceNoCharge('CANCEL'); // qm-308    392
      end;

      if Locate.ClosedHow = 'HCL' then
      begin
        FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        ForceNoCharge('No Response'); // qm-407
      end;

      if not MatchesValueGroup(Locate.TicketType, 'FMBCompanyBGErates') and Not(StrContains('EMER', Locate.TicketType)) then
      begin // qm-308
        FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        ForceNoCharge('BGE')
      end;

      if Locate.ParentTicketID > 0 then // qm-392 sr
      begin
        FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
        ForceNoCharge('Duplicate Ticket');
      end;

      if MatchesValueGroup(Locate.TicketType, 'FMBCompanyBGErates') and not(IsNoCharge) then // qm-308   392
        Locate.AreaName := Get7501BillingArea(Locate); // saves "BOTH" in AreaName if Party
    End; // BGE land

//    If MatchesTermGroup(Locate.ClientCode,  'FMWAllTerms') then // QM-451 merging FMW terms into FMB run
//    Begin
//      if (Locate.ParentTicketID > 0) then // QM-98/235  sr
//      begin
//        if not(IsTextInArray(Locate.ClientCode, // QM-98/235  sr
//          TermGroup('FMWCanBillTicketTwiceTerms'))) then // QM-98/235  sr
//        begin
//          ForceNoCharge('Duplicate Ticket');
//          FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, 'NC');
//        end;
//      end;
//
//      if ((Locate.IsCancel) and (not(MatchesTermGroup(Locate, 'FMWCanBillCancels')))) then
//        ForceNoCharge('Cancel') // QM-432  add parens  sr
//      else if MatchesTermAndExcavatorGroup(Locate, 'FMWWASATerms', 'FMWWorkDoneForDCWater') then
//        ForceNoCharge('DCWater')
//      else if MatchesTermAndExcavatorGroup(Locate, 'FMWWSSTerms', 'FMWWorkDoneForWASubSanitary') then
//        ForceNoCharge('WASubSanitary')
//      else if MatchesTermAndExcavatorGroup(Locate, 'FMWAlleghenyTerms', ValueGroupCompanyAllegheny) then
//        ForceNoCharge('Allegheny')
//      else if MatchesTermAndExcavatorGroup(Locate, 'FMWBGETerms', 'FMWCompanyBGE') then
//        ForceNoCharge('BGE');
//
//      RunPartyCount;
//
//      if not IsNoCharge then
//        if Locate.IsEmergencyOrManualAfterHoursType and TreatAsAfterHours(Locate) then // QM-111
//          MakeAfterHours;
//
//      if MatchesTermGroup(Locate, 'FMWVerizonTerms') and ((Locate.TicketType = 'NO RESPONSE') // QM-111
//        or (Locate.TicketType = 'FIOS NO RESPONSE')) then
//        ForceNoCharge('No Response');
//
//    end; //end old FMW terms  QM-451

    if FoundRate = 0 then
      ShowParties := False;
  end // end MDDC


  // Washington billing rules MDDC FMW
//  else if (Locate.CallCenter = 'FMW1') or (Locate.CallCenter = 'FMW3') then
//  begin
//    if (Locate.ParentTicketID > 0) then   //QM-98/235  sr
//    begin
//      if not(IsTextInArray(Locate.ClientCode,   //QM-98/235  sr
//        TermGroup('FMWCanBillTicketTwiceTerms')))then   //QM-98/235  sr
//      begin
//        ForceNoCharge('Duplicate Ticket');
//        FTicketClientList.UpdateStatus(Locate.TicketID,
//          Locate.ClientCode, 'NC');
//      end;
//    end;
//
//
//    if ((Locate.IsCancel) and (not(MatchesTermGroup(Locate, 'FMWCanBillCancels')))) then
//      ForceNoCharge('Cancel')  // QM-432  add parens  sr
//    else if MatchesTermAndExcavatorGroup(Locate, 'FMWWASATerms', 'FMWWorkDoneForDCWater') then
//      ForceNoCharge('DCWater')
//    else if MatchesTermAndExcavatorGroup(Locate, 'FMWWSSTerms', 'FMWWorkDoneForWASubSanitary') then
//      ForceNoCharge('WASubSanitary')
//    else if MatchesTermAndExcavatorGroup(Locate, 'FMWAlleghenyTerms', ValueGroupCompanyAllegheny) then
//      ForceNoCharge('Allegheny')
//    else if MatchesTermAndExcavatorGroup(Locate, 'FMWBGETerms', 'FMWCompanyBGE') then
//      ForceNoCharge('BGE');
//
//    RunPartyCount;
//
//    if not IsNoCharge then
//      if Locate.IsEmergencyOrManualAfterHoursType and TreatAsAfterHours(Locate) then  //QM-111
//        MakeAfterHours;
//
//    if MatchesTermGroup(Locate, 'FMWVerizonTerms') and ((Locate.TicketType = 'NO RESPONSE')  //QM-111
//                                                        or (Locate.TicketType = 'FIOS NO RESPONSE')) then
//      ForceNoCharge('No Response');
//  end // Washington (FMW)

  // Tennessee/NV billing rules (FNV1)  TNNV
  else if Locate.CallCenter = 'FNV1' then begin
    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;
{qm-1058
FNV1 of Center Group TNNV
Gallatin Tenn: Normal hours are 7am to 4pm.
After hours are from 4 pm to 7 am.  }
		if (Locate.ClientCode = 'GE') then //QM-1058
    begin
      if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or // after hours
        (((HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12 + 4))) and
        ((HourOf(Locate.ClosedDate) < (7)) or (HourOf(Locate.ClosedDate) >= (12 + 4)))) then
        MakeAfterHours;
		end;


    if MatchesTermGroup(Locate, 'FNVBTerms') and
      (MatchesValueGroup(Locate.WorkDoneFor, 'BellSouthCompany') or
       MatchesValueGroup(Locate.Excavator, 'BellSouthCompany')) then begin
      ForceNoCharge('BellSouth');
    end;
   { TODO -oxqTrix -cbug : add cancel rule }
    if MatchesTermAndExcavatorGroup(Locate, 'FNVMTEMTerms', 'FNVCompanyMidTNElectric') then
      ForceNoCharge('Mid TN Electric');

    if MatchesTermGroup(Locate, 'FNVCLKTerms') and
       MatchesValueGroup(Locate.WorkDoneFor, 'FNVCityOfClarksvilleWDF')and
       MatchesValueGroup(Locate.Excavator, 'FNVCityOfClarksvilleWDB')
       then
      ForceNoCharge('City of Clarksville');  // QM-446


    if Locate.ParentTicketID > 0 then
      ForceNoCharge('Duplicate Ticket');

    if Locate.IsCancel and (not MatchesTermGroup(Locate, 'FNVCanBillCancels'))then
      ForceNoCharge('Cancel');
  end //end of TNNV billing

  // Chesapeake (FPK1/FPK2) billing rules
  else if IsFPK(Locate.CallCenter) then begin
    if MatchesTermGroup(Locate, 'FPKTermUnits1') then
      Qty := 1;

    if MatchesTermGroup(Locate, 'FPKDupTermsVNG') and MatchesValueGroup(Locate.WorkDoneFor, 'VaNaturalGasWDF') then
      ForceNoCharge('VA Natural Gas');

    if (Locate.ParentTicketID > 0) and MatchesOneOfTermGroups(Locate.ClientCode, ['FPKDuplicateTerms1', 'FPKDuplicateTerms2']) then
      ForceNoCharge('Duplicate Ticket');

    // Verizon terms can not be billed for late locates
    if MatchesTermGroup(Locate, 'FPKDuplicateTerms1') and (Locate.WasEverLate or MatchesValueGroup(Locate.TicketType, 'VerizonNoChargeTT')) then
      ForceNoCharge('Late/NoShow');

    RunPartyCount;

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      // Chesapeake uses the call date if available to determine after hours
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;
  end

  // FPL1 billing rules
  else if Locate.CallCenter = 'FPL1' then begin
    if MatchesTermGroup(Locate, 'FPLSBFTerms') then begin
      if MatchesValueGroup(Locate.WorkDoneFor, 'BellSouthCompany') or
         MatchesValueGroup(Locate.Excavator, 'BellSouthCompany') then
      begin
        ForceNoCharge('BellSouth');
      end;
    end;

    if Locate.ParentTicketID > 0 then
      ForceNoCharge('Duplicate Ticket')  //qm-665
    else if Locate.IsCancel then
      ForceNoCharge('Cancel Ticket') //qm-665
    else if Locate.IsDamage then
      ForceNoCharge('Damage Ticket') //qm-665
    else if MatchesTermGroup(Locate, 'FPLSurveyTicketsNC') and Locate.IsSurvey then
      ForceNoCharge('Survey Ticket');
    // After hours tickets are handled manually

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

  end

  // FAM1 (Alabama) billing rules
  else if Locate.CallCenter = 'FAM1' then begin
    if MatchesTermGroup(Locate.ClientCode, 'FAMBillUnits') then
      Qty := Locate.QtyMarked
    else
      Qty := 1;

    if Locate.IsCancel then
      ForceNoCharge('Cancel')
    else if Locate.IsSurvey and MatchesTermGroup(Locate, 'FAMSurveyTicketsNC') then
      ForceNoCharge('Survey');

    if Locate.ClientCode = 'SCMB01' then begin
      if MatchesValueGroup(Locate.WorkDoneFor, 'BellSouthCompany') or
         MatchesValueGroup(Locate.Excavator, 'BellSouthCompany') then
      begin
        ForceNoCharge('BellSouth');
      end;
    end;

    if MatchesTermAndWorkDoneForGroup(Locate, 'FAMMobileGasTerms', 'FAMMobileGas') or
      MatchesTermAndExcavatorGroup(Locate, 'FAMMobileGasTerms', 'FAMMobileGas') then
      ForceNoCharge('Mobile Gas');

    if Locate.ClientCode = 'BEMC01' then
      RunPartyCount;

    if Locate.TicketType = TicketTypeWatchAndProtect then begin
      TicketType := TicketTypeWatch;
      Locate.WatchAndProtect := True;
      // The amount to bill is always 1 for FAM1 WP tickets
      Qty := 1;
    end;

    if Locate.ParentTicketID > 0 then
      ForceNoCharge('Duplicate Ticket');
    // After hours tickets are handled manually
  end

  // FDX1 (Dallas) billing rules
  else if Locate.CallCenter = 'FDX1' then begin
    if StrBeginsWith('P7', Locate.ClientCode) then
      Qty := 1;

    if MatchesTermAndWorkDoneForGroup(Locate, 'AtmosTerms-TX', 'AtmosTermsWDF') then
      ForceNoCharge('Atmos Work');

    if (Locate.ParentTicketID > 0) then
      ForceNoCharge('Duplicate Ticket');

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;
  end

  // FOK1 (Tulsa) billing rules
  else if Locate.CallCenter = 'FOK1' then begin

    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType) then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours;
    end;

    if (Locate.ParentTicketID > 0) then
      ForceNoCharge('Duplicate Ticket');
  end

  // FDE1/2 (Delaware) billing rules
  else if (Locate.CallCenter = 'FDE1') or (Locate.CallCenter = 'FDE2') then
  begin  { TODO -oSR -cTimerule : Need Vzn time rule }

    if MatchesTermGroup(Locate, 'FDENoResponseTerms') and MatchesValueGroup(Locate.TicketType, 'FDENoChargeTicketTypes') then //qm-735
      ForceNoCharge('No Response');

     if (not IsNoCharge) then   //QM-168
     begin
        if Locate.IsEmergency  then
        begin
          if (IsWeekend(Locate.ClosedDate) or IsHoliday(Locate)) or
           (
           ((HourOf(Locate.CallDate) < (7)) or (HourOf(Locate.CallDate) >= (12+5))) and
           ((HourOf(Locate.ClosedDate) < (7)) or (HourOf(Locate.ClosedDate) >= (12+5)))
           )
           then
              MakeAfterHours
          else
            MakeEmergency;
        end
        else
        begin
          MakeNormal;
        end;
     End; //if (not IsNoCharge)   //QM-168

    if MatchesTermGroup(Locate.ClientCode, 'FDE1BellAtlanticTerms') then
      Locate.Bucket := Locate.Bucket + ', ' + IntToStr(Locate.QtyMarked) + AddSIfNot1(Locate.QtyMarked, ' Unit');

    // terms that cannot be billed for late locates
    if MatchesTermGroup(Locate, 'FDENoChargeLateTerms') and (Locate.WasEverLate or MatchesValueGroup(Locate.TicketType, 'VerizonNoChargeTT')) then
      ForceNoCharge('Late/NoShow');


    if Locate.IsCancel and (not MatchesTermGroup(Locate, 'FDECanBillCancels')) then
      ForceNoCharge('Cancel');

  end //FDE

  // OUPS ticket rules
  else if (Locate.CallCenter = 'OUPS1') then  //QMANTWO-489
  begin
    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType) then begin  //QMANTWO-637
      if TreatAsAfterHours(Locate) then
        MakeAfterHours;
    end;

      if Locate.IsCancel and (not MatchesTermGroup(Locate, 'OUPSCanBillCancels')) then //QMANTWO-489
      ForceNoCharge('Cancel');
  end

  // FMS1 (Mississippi 1) billing rules
  else if Locate.CallCenter = 'FMS1' then begin
    Qty := Locate.QtyMarked;

    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType) then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours;
    end;

    if (Locate.ParentTicketID > 0) then
      ForceNoCharge('Duplicate Ticket');

    if MatchesTermGroup(Locate, 'FMS1BellSouthTerms') and
      MatchesValueGroup(Locate.WorkDoneFor, 'FMSCompanyBellSouth') then begin
      ForceNoCharge('BellSouth');
    end;
  end

  // FJL1 (Mississippi 2) billing rules
  else if Locate.CallCenter = 'FJL1' then begin
    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType) then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours;
    end;

    if (Locate.ParentTicketID > 0) then
      ForceNoCharge('Duplicate Ticket');

    if MatchesTermAndWorkDoneForGroup(Locate, 'FJL1BellSouthTerms', 'FJLCompanyBellSouth') then
      ForceNoCharge('BellSouth')
    else if MatchesTermAndWorkDoneForGroup(Locate, 'FJLAtmosTerm', 'AtmosTermsWDF') then
      ForceNoCharge('Atmos');
  end

  // FWP1/3 (West Virginia/Ohio) billing rules
  else if (Locate.CallCenter = 'FWP1') or (Locate.CallCenter = 'FWP3') then begin
    RunPartyCount;
    if MatchesTermGroup(Locate, 'FWPIgnoreQtyMarkedTerms') then
      Qty := 1;

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    //TODO: 2245 - change this only check excavator too?
    if MatchesTermGroup(Locate, 'FWPAlleghenyTerms') and
       MatchesValueGroup(Locate.WorkDoneFor, ValueGroupCompanyAllegheny) and
       MatchesValueGroup(Locate.Excavator, ValueGroupCompanyAllegheny) then
    begin
      ForceNoCharge('Allegheny');
      ShowParties := False;
    end;
  end

  // FAQ1/2 (Albuquerque) billing rules
  else if (Locate.CallCenter = 'FAQ1') or (Locate.CallCenter = 'FAQ2') then begin
    RunPartyCount;

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      // No special emergency rates (only after hours)
      if TreatAsAfterHours(Locate) then begin
        MakeAfterHours;
        Locate.Bucket := 'After Hours Notice' + ', ' + GetStatusDescription(Locate.Status) + ' (' + Locate.WorkCity + ')';
      end;
    end;

    if (not IsNoCharge) and (Locate.TicketType = TicketTypeWatchAndProtect) then begin
      TicketType := TicketTypeWatch;
      Locate.WatchAndProtect := True;
      Locate.Bucket := 'Watch and Protect, ' + GetStatusDescription(Locate.Status);
    end;
  end

  // FOR1 (Oregon) billing rules
  else if (Locate.CallCenter = 'FOR1') then begin
    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then
      MakeEmergency;
  end

  // FHL1 (Houston, TX) billing rules
  else if IsFHL(Locate.CallCenter) then begin
    if MatchesTermGroup(Locate, 'FHLIgnoreQtyMarkedTerms') then
      Qty := 1;

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;
    Assert(Locate.QtyCharged > 0);
  end

  // FGV1 (Greenville, South Carolina) billing rules
  else if IsFGV(Locate.CallCenter) then begin
    if (not IsNoCharge) then begin
      if Locate.IsDamage then
        MakeDamage
      else if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;

    if MatchesTermAndExcavatorGroup(Locate, 'FGVTWTerms', 'TimeWarnerCDW') or
      MatchesTermAndWorkDoneForGroup(Locate, 'FGVTWTerms', 'TimeWarnerWDF') then
      ForceNoCharge('Time Warner')
    else if MatchesCCDoneExc(Locate, 'FGVDPTerms', 'DukePowerWDF', 'DukePowerCDW') then
      ForceNoCharge('Duke Power');
  end

  // FWI1/2 (Wisconsin) billing rules
  else if (Locate.CallCenter = 'FWI1') or (Locate.CallCenter = 'FWI2') then begin
    // After hours tickets are handled manually
    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then
      MakeEmergency;
  end

  // 6001/6002 (STS Texas) billing rules
  else if IsSTSTexas(Locate.CallCenter) then begin

    if MatchesTermAndWorkDoneForGroup(Locate, 'AtmosTerms-TX', 'AtmosTermsWDF') then
      ForceNoCharge('Atmos Work');

    if not IsNoCharge then begin
      RunPartyCount;
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // 1391/1392 (Memphis, TN) billing rules
  else if IsMemphis(Locate.CallCenter) then begin
    if not IsNoCharge then begin
      RunPartyCount;
      if MatchesTermGroup(Locate, '139BellSouthTerms') then begin
        if MatchesWorkDoneForGroup(Locate, 'BellSouthCompany') then
          ForceNoCharge('Work Done for BellSouth');
      end;

      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
    if Locate.IsCancel then
      ForceNoCharge('Cancel');
  end

  // 7501 (Philadelphia, PA) billing rules
  else if Locate.CallCenter = '7501' then begin
    if not IsNoCharge then begin
      RunPartyCount;
      if (Locate.ParentTicketID > 0) and (not StrContains('UPDT', Locate.TicketType)) then
        ForceNoCharge('Duplicate Ticket');

      // Some (PECO) terms can get joint billing rates marked with an "AreaName" of "Both"
      Locate.AreaName := Get7501BillingArea(Locate);

      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // 9001 (STS Florida) billing rules
  else if Locate.CallCenter = '9001' then begin
    Qty := 1;

    if MatchesTermAndWorkDoneForGroup(Locate, '9001SprintTerms', '9001SprintTermsWDF') then
      ForceNoCharge('Work done for Sprint')
    else if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // 9401 (STS Tennessee) billing rules
  else if Locate.CallCenter = '9401' then begin
    if MatchesTermGroup(Locate, 'SprintTerms-TN') then
      Qty := 1;

    if MatchesTermAndWorkDoneForGroup(Locate, 'SprintTerms-TN', 'AllSprintTermsWDF') then
      ForceNoCharge('Work Done for Sprint')
    else if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // 9301 (STS Virginia) billing rules (now part of FCV, can be removed soon)
  else if Locate.CallCenter = '9301' then begin
    if MatchesTermGroup(Locate, 'SprintTerms-VA') then
      Qty := 1;

    if MatchesTermAndWorkDoneForGroup(Locate, 'SprintTerms-VA', 'AllSprintTermsWDF') then
      ForceNoCharge('Work Done for Sprint')
    else if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // 9101 (STS North Carolina) billing rules
  else if Locate.CallCenter = '9101' then begin
    if MatchesOneOfTermGroups(Locate.ClientCode, ['9101ProgressEnergy', 'SprintTerms-NC', '9101TermUnits1']) then
      Qty := 1;

    if MatchesTermAndWorkDoneForGroup(Locate, 'SprintTerms-NC', 'AllSprintTermsWDF') then
      ForceNoCharge('Work Done for Sprint')
    else if MatchesTermAndExcavatorGroup(Locate, '9101FourCountyEMC', '9101FourCountyCDW') then
      ForceNoCharge('Four County CDW')
    else if MatchesTermAndExcavatorGroup(Locate, '9101TimeWarner', '9101TimeWarnerCDW') then
      ForceNoCharge('Time Warner')
    else if MatchesTermAndWorkDoneForGroup(Locate, '9101TimeWarner', '9101TimeWarnerWDF') then
      ForceNoCharge('Time Warner')
    else if (Locate.ParentTicketID > 0) and MatchesTermGroup(Locate, '9101ProgressEnergy') then
      ForceNoCharge('Duplicate Ticket')
    else if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // California billing rules (SCA/NCA)
  else if MatchesCenterGroup(Locate, 'CACallCenters', False) then
  begin
    ThisIsCalifornia := False;
    RunPartyCount;

    if (not IsNoCharge) and Locate.IsEmergencyOrManualAfterHoursType then
    begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    if (MatchesOneOfTermGroups(Locate.ClientCode, ['SCAVerizonDups',
      'SCACoxTerms']) and Locate.IsProject) then
      ForceNoCharge('Project Ticket');

    if ((AnsiMatchStr(Locate.ClientCode, ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA']))  //QM-121   620 770
    and
    (MatchesValueGroup(Locate.TicketType, 'CA_ATT_NO_CHARGE') or    //QM-594
       (AnsiStartsText(BillingConst.MANUEL_TICKET, Locate.TicketNumber)))
       )
    then ForceNoCharge(Locate.TicketType);

    if ((AnsiMatchStr(Locate.ClientCode,  ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA'])) and  //qm-620 770
      (((Pos('NRSP', Locate.TicketType) > 0) and StringInArray(Locate.Status,
      ['OS', 'S'])))) // QM-101
    then
      ForceNoCharge(Locate.TicketType);

    if ((AnsiMatchStr(Locate.ClientCode,  ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA'])) and  //qm-620 770
      (((Pos('AMND', Locate.TicketType) > 0) and StringInArray(Locate.Status,
      ['OS', 'S'])))) // QM-101
    then
      ForceNoCharge(Locate.TicketType);

    if ((AnsiMatchStr(Locate.ClientCode,  ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA'])) and  //qm-620 770
      (((Pos('AMND', Locate.TicketType) > 0) and StringInArray(Locate.Status,
      ['OS', 'S'])))) // QM-101
    then
      ForceNoCharge(Locate.TicketType);

    if ((AnsiMatchStr(Locate.ClientCode,  ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA'])) and  //qm-620 770
      (StrContains('DMEX', Locate.TicketType))) // QM-101
    then
      ForceNoCharge(Locate.TicketType);

//    If (Locate.UnitsMarked>800) and
//     (AnsiMatchStr(Locate.ClientCode,  ['PACBEL', 'ATTDNORCAL', 'ATTDSOUTH', 'NEVBEL','ATTDNEVADA']))
//    then
//      Make800LPM;

    if (MatchesValueGroup(Locate.ClientCode, 'NoDupTixClientList')  // QMANTWO-594
      and Locate.TixIsDup) then
      ForceNoCharge('Duplicate Ticket');

    // Some CA clients (AT&T, etc.) are transmission billed in the invoice SP
  end

  // FTL1/2/3 FJT1/FCT1 (Tennessee (GA/NC) billing rules
  else if MatchesCenterGroup(Locate, 'FTLFJTFCTCallCenters', False) then begin
    RunPartyCount;
    if (not IsNoCharge) and (Locate.IsEmergencyOrManualAfterHoursType) then begin
      if TreatAsAfterHours(Locate) then
        MakeAfterHours
      else
        MakeEmergency;
    end;

    if MatchesTermAndWorkDoneForGroup(Locate, 'FTLNoChargeBellSouthTerms', 'BellSouthCompany') then
      ForceNoCharge('BellSouth');

    if (Locate.ClientCode = 'JCG') and MatchesValueGroup(Locate.WorkDoneFor, 'FTLJeffersonWorkDoneFor') then
      ForceNoCharge('JC County Utility');

    if MatchesTermAndExcavatorGroup(Locate, 'FTLCharterTerms', 'FTLCharterCDW') then
      ForceNoCharge('Charter Work');

    if MatchesTermAndWorkDoneForGroup(Locate, 'FTLChatanoogaGasTerms', 'FTLChatanoogaGasWDF') then
      ForceNoCharge('Chatanooga Gas Work');

    if (Locate.ParentTicketID > 0) then
      ForceNoCharge('Duplicate Ticket');
    if Locate.IsCancel then
      ForceNoCharge('Cancel');
  end

  // 1101/1102 (STS New Mexico) billing rules
  else if MatchesCenterGroup(Locate, 'STSNewMexico', False) then begin
    if not IsNoCharge then begin
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end

  // Locating, Inc. LWA1/LID1 (Portland) billing rules
  else if MatchesCenterGroup(Locate, 'LWA', False) then begin
    if not IsNoCharge then begin
//      RunPartyCount;   QM-87

      if Locate.Status = 'QD' then
        ForceNoCharge(GetStatusDescription('QD'));

      // Some PSE/AVISTA/etc. terms can get joint billing rates marked with a "City" of "Both"
      Locate.BillingCity := GetLWALIDBillingCity(Locate);

      if Locate.IsWatchAndProtect then begin // See the special IsWatchAndProtect() rule for LWA
        TicketType := TicketTypeWatch;
        Locate.Bucket := 'Watch and Protect, ' + GetStatusDescription(Locate.Status);
        // TODO Check missing(?) set of the Locate.WatchAndProtect flag
      end
      else if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end
      else if Locate.IsDesign then
        MakeDesign
      else if Locate.IsShortNotice then
        MakeShortNotice;

      Locate.Bucket := Locate.Bucket + ', ' + Locate.BillingCity; // Prevent mixed rate bucket errors
    end;
  end

  // LOR1/LAK1/(LUT1/LNV1)NWN (Locating, Inc. Oregon, Alaska, (Utah/Nevada)) billing rules
  else if MatchesOneOfCenterGroups(Locate.CallCenter, ['LOR', 'LUT', 'LAK'], False) then begin
    if not IsNoCharge then begin
//      RunPartyCount;   QM-87

      if Locate.Status = 'QD' then
        ForceNoCharge(GetStatusDescription('QD'));

      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end
      else if Locate.IsDesign then
        MakeDesign
      else if Locate.IsShortNotice then
        MakeShortNotice;

//      if not IsNoCharge then
        Locate.Bucket := Locate.Bucket + ', ' + Locate.WorkCity;
    end; //if not IsNoCharge
  end

  // 1421 (Columbia, South Carolina) billing rules  Scana
  else if Locate.CallCenter = '1421' then
  begin
    if MatchesOneOfTermGroups(Locate.ClientCode, ['142DupSCEG', '142DupSCEE',
      '142DupSCGT', '142DupSCET', '142DupSCCC']) and Locate.IsCancel then
      ForceNoCharge('Cancel'); // QMANTWO-757

    if containsText(Locate.TicketType,'Subaq' ) then
      ForceNoCharge('SUBAQ not charged'); //qm-912

    if not IsNoCharge then
    begin
      if Locate.IsUpdate then    //qm-884 sr
        MakeUpdate;

      if Locate.IsRemark then    //qm-884 sr
        MakeNormal;

      if Locate.IsEmergencyOrManualAfterHoursType then
      begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end; // after Nov 30th we are purely going by inital_status_date being lower than the due date
      if MatchesOneOfTermGroups(Locate.ClientCode, ['142SCANALate'])        // QMANTWO-742
        and not Locate.IsEmergency // qm-811
        and Not(StringInArray(Locate.Status, BillingConst.ExcludeFromLate)) then
      begin
        if (Locate.InitialStatusDate > Locate.DueDate) and not (Locate.IsUpdate) then  //qm-899
          MakeLate('Statused Late');
      end;
    end;  //not no charge
  end // 1421 (Columbia, South Carolina) billing rules

  // 1851 (Indiana) billing rules Nipsco
  else if Locate.CallCenter = '1851' then begin
    if Locate.IsCancel and (not MatchesTermGroup(Locate, '185NipscoCanBillCancels'))then
      ForceNoCharge('Cancel');   //QMANTWO-342

    if not IsNoCharge then
    begin
      Locate.AreaName := Get7501BillingArea(Locate); // saves "BOTH" in AreaName if Party
//      RunPartyCount;    // QM-86
      if Locate.IsEmergencyOrManualAfterHoursType then
      begin
        if TreatAsAfterHours(Locate) then   //1
          MakeAfterHours { TODO : Check where Hours are from }
        else
          MakeEmergency;
      end;


     if MatchesOneOfTermGroups(Locate.ClientCode, ['185CloseDueLate']) // QM-77
          and (Locate.ClosedDate > Locate.DueDate)  // QM-77
          and Not(StringInArray(Locate.Status, BillingConst.ExcludeFromLate))
     then MakeLate('Closed Date');  // QM-77

    end;  // if not IsNoCharge

  end  // 1851 (Indiana) billing rules
  // Call centers with the default rules: AFTER, EMERG, Parties, NC, etc.
  // New/unknown centers use this (sometimes temporarily)
  else begin
    if not IsNoCharge then begin
      RunPartyCount;
      if Locate.IsEmergencyOrManualAfterHoursType then begin
        if TreatAsAfterHours(Locate) then
          MakeAfterHours
        else
          MakeEmergency;
      end;
    end;
  end;

// -------------------------------Rates---------------------------------------
  Locate.QtyCharged := Qty;
  Locate.BillingTicketType := TicketType;

  // This either finds a rate, or throws an exception:
  if FoundRate < 0 then begin
    if StringInArray(TicketType, [TicketTypeNormal, TicketTypeEmerg, TicketTypeUpdate]) and Locate.IsFTTP then begin
      RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,
        [TicketTypeFTTP, TicketType, TicketTypeNormal]);
    end else if StringInArray(TicketType, [TicketTypeNormal, TicketTypeEmerg, TicketTypeUpdate]) and Locate.IsFIOS then begin
      RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,
        [TicketTypeFIOS, TicketType, TicketTypeNormal]);
    end else if MatchesCenterGroup(Locate, 'NoResponseCallCenters') and Locate.IsLateNotice then begin
      RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,
        [TicketTypeNoResponse, TicketType, TicketTypeNormal]);
    end else if TicketType = 'LATE' then begin  //QMANTWO-742
        RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,[TicketTypeStatusAfterDue, TicketType]);
    end else begin
      RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,
        [TicketType, TicketTypeNormal]);
    end;


    FoundRate := RateEntry.Rate;
    BillCode := RateEntry.BillCode;
    LineItemText := RateEntry.LineItemText;
    Locate.WhichInvoice := RateEntry.WhichInvoice;
    Locate.RateValueGroupID := RateEntry.ValueGroupID;


    Locate.CWICode        := RateEntry.CWICode;  //qm-948
//    Locate.BillingRateID  := RateEntry.BillingRateID;  //qm-948
    Locate.AddlCWICode    := RateEntry.AddlCWICode;  //qm-948

    Locate.RPT_GL    := RateEntry.Rpt_Gl;  //qm-995
    Locate.Additional_RPT_GL:= RateEntry.Additional_Rpt_gl;  //qm-995

    if (Locate.QtyCharged > 1) and (RateEntry.AdditionalRate > 0) then begin
      Locate.AdditionalLineItemText := RateEntry.AdditionalLineItemText;
      Locate.AdditionalQtyRate := RateEntry.AdditionalRate;
      Locate.AdditionalQty := Locate.QtyCharged - 1;
      Locate.QtyCharged := 1; // The now "missing" Qty is fixed up later when creating the new line item for the additional marks (see TBillingEngine.AddSplitLineItems)
    end;
  end;

  if Trim(LineItemText) <> '' then
    Locate.LineItemText := LineItemText
  else
    Locate.LineItemText := Locate.Bucket;

  Locate.OvertimeRate := FoundRate;
  if (Locate.IsHourly) then begin
    RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, Parties,
      [TicketTypeOver, TicketTypeNormal]);
    Locate.OvertimeRate := RateEntry.Rate;
  end;

  Assert(FoundRate >= 0);
  if Locate.QtyCharged < 1 then
    raise EQtyException.Create('Warning: Qty charged is not > 0 for ' + GetLocateIdentifier(Locate));

  if ((Locate.Status <> 'LPM') and (Locate.WorkState='Ca') and (Locate.UnitsMarked > 800)) then   //qm-951
  Locate.Price := GetPriceFromRateQty(Locate, FoundRate, Qty)
  else
  begin
    Locate.Price := FoundRate;   //qm-951
    Locate.Rate := FoundRate;
  end;

  if ((Locate.Status = 'LPM') and (Locate.WorkState='Ca') and (Locate.UnitsMarked < 800))  then    //qm-951
  begin
    raise EBillingException.Create('LPM status used in Ca when less than 800 feet!! ');
  end;


  ApplySalesTax(Locate);

  Locate.Parties := Parties;
  if ShowParties then
    Locate.Bucket := Locate.Bucket + Format(', %s Party', [NumToWords(Locate.Parties)]);

  if (BillCode <> '') and IsAtlanta(Locate.CallCenter) then
    Locate.Bucket := Locate.Bucket + ', ' + BillCode;

  if (Locate.CallCenter = 'FDX1'){ or (Locate.CallCenter = 'FCO1')} then begin
    if Trim(BillCode) <> '' then
      Locate.Bucket := Locate.Bucket + ', ' + BillCode;
  end;

  if Locate.CallCenter = 'FEF1' then begin
    if Locate.IsEmergency then begin
      if BillCode = 'BASE' then
        Locate.Bucket := 'Emergency Base Charge'
      else
        Locate.Bucket := 'Emergency Notice'
    end
    else begin
      if BillCode = 'BASE' then
        Locate.Bucket := 'Normal Base Charge'
    end;
  end;

  if (Locate.CallCenter = 'FOL1') and (BillCode = 'OTHER') then
    Locate.Bucket := 'Other';

  if IsFCL(Locate.CallCenter) and (Locate.Price = 0) then
    if not StrContains('No Charge', Locate.Bucket) then
      Locate.Bucket := 'No Charge';

//  if (Locate.CallCenter = 'FCO1') and (BillCode = 'NC') then begin
//    if Locate.Price <> 0 then
//      LogErrorFmt('ERROR: Locate has an NC bill code, but a non-0 price (%s)', [GetLocateIdentifier(Locate)]);
//    Locate.Bucket := 'No Charge';
//  end;

  // Combine 0-price things like "No Charge, Two Party" and "Office Screen" into "No Charge"
  if (Locate.CallCenter = CallCenterNewJersey) and (Locate.Price = 0) then
    Locate.Bucket := 'No Charge';

  if IsFCV(Locate.CallCenter) and (BillCode = 'NC') then
    Locate.Bucket := Locate.Bucket + ', No Charge';

  if StrContains('No Charge', Locate.Bucket) and (Locate.Price <> 0) then
    LogErrorFmt('Error: Locate is a no charge, but the price is not 0 (%s)', [GetLocateIdentifier(Locate)]);

  if Locate.IsDailyUnitsHours then // Is this ever true at this point?
    Locate.Bucket := FormatDateTime('mmm d', Locate.ClosedDate) + ' Units: ' + Locate.Bucket;

  if StrContains('TRAINING', locate.TicketType) then ForceNoChargeTraining(locate.TicketType);  //QMANTWO-620 QM-42)

  if StrContains('AUDIT COPY', locate.TicketType) then ForceNoChargeTraining(locate.TicketType);  //QM-42

  if DebugBilling then
    LogFmt('Rated as Price: %m  Rate: %m  Units: %d  Bucket: %s  LineItem: %s  Parties: %d', [Locate.Price, Locate.Rate, Locate.QtyMarked, Locate.Bucket, Locate.LineItemText, Locate.Parties]);
end;

procedure TBillingEngine.RemoveNJNHack(Locate: TLocate);
begin
  if (Locate.CallCenter = CallCenterNewJersey) and StrBeginsWith('NJN', Locate.ClientCode) then
    Locate.ClientCode := 'NJN';
end;

procedure TBillingEngine.SetTotals;

  function OpenQueryReturnValue(DS: TADODataSet): Variant;
  begin
    Assert(Assigned(DS));
    DS.Parameters.ParamByName('BillID').Value := FBillID;
    DS.Open;
    Assert(DS.RecordCount = 1, 'Bad record count writing the header');
    Assert(DS.FieldCount = 1, 'Bad query writing the header totals');
    Result := DS.Fields[0].Value;
    if VarIsNull(Result) then
      Result := 0;
  end;

begin
  Log('Writing billing ticket/locate/amount totals to the header...');
  {
  with BEDM.billing_header do begin
    Edit;
    FieldByName('n_tickets').AsInteger   := OpenQueryReturnValue(BEDM.CountTickets);
    FieldByName('n_locates').AsInteger   := OpenQueryReturnValue(BEDM.CountLocates);
    FieldByName('totalamount').AsInteger := OpenQueryReturnValue(BEDM.SumBilling);
    Post;
  end;
  }
    BEDM.updBillingHeader.Parameters.ParamByName('n_tickets').Value :=  OpenQueryReturnValue(BEDM.CountTickets);
    BEDM.updBillingHeader.Parameters.ParamByName('n_locates').Value :=  OpenQueryReturnValue(BEDM.CountLocates);
    BEDM.updBillingHeader.Parameters.ParamByName('totalamount').Value :=  OpenQueryReturnValue(BEDM.SumBilling);
    BEDM.updBillingHeader.Parameters.ParamByName('BillID').Value :=  FBillID;
    BEDM.updBillingHeader.ExecSQL;
    Log('Billing header updated');

end;

procedure TBillingEngine.CreateHeaderRecord;
var
  Version: string;
begin
  Assert(FCallCenterGroupID > 0);
  Log('Creating billing header record...');
  {
  BEDM.billing_header.Open;
  BEDM.billing_header.Append;
  BEDM.billing_header.FieldByName('center_group_id').AsInteger := FCallCenterGroupID;
  BEDM.billing_header.FieldByName('bill_run_date').AsDateTime := FRunDate;
  BEDM.billing_header.FieldByName('bill_start_date').AsDateTime := Period.StartDate;
  BEDM.billing_header.FieldByName('bill_end_date').AsDateTime := Period.EndDate;
  BEDM.billing_header.FieldByName('period_type').AsString := Period.Span;
  BEDM.billing_header.FieldByName('description').AsString := FDescription;
  if GetFileVersionNumberString(Application.ExeName, Version) then
    BEDM.billing_header.FieldByName('app_version').AsString := Version;
  BEDM.billing_header.Post;
  FBillID := BEDM.billing_header.FieldByName('bill_id').AsInteger;
  Log('Bill ID = ' + IntToStr(FBillID));
  }
  BEDM.insBillingHeader.Parameters.ParamByName('center_group_id').Value  := FCallCenterGroupID;
  BEDM.insBillingHeader.Parameters.ParamByName('bill_run_date').Value  := FRunDate;
  BEDM.insBillingHeader.Parameters.ParamByName('bill_start_date').Value  := Period.StartDate;
  BEDM.insBillingHeader.Parameters.ParamByName('bill_end_date').Value   := Period.EndDate;
  BEDM.insBillingHeader.Parameters.ParamByName('period_type').Value  := Period.Span;
  BEDM.insBillingHeader.Parameters.ParamByName('description').Value  := FDescription;
  if GetFileVersionNumberString(Application.ExeName, Version) then
  BEDM.insBillingHeader.Parameters.ParamByName('app_version').Value :=  Version
  else
  BEDM.insBillingHeader.Parameters.ParamByName('app_version').Value :=  '';
  if BEDM.insBillingHeader.ExecSQL > 0 then
  log('Added Record to Billing Header')
  else
     log('Adding Record to Billing Header failed');

  try
    BEDM.qryBillID.open;
    if not BEDM.qryBillID.IsEmpty then
      FBillID := BEDM.qryBillID.FieldByName('bill_id').AsInteger;
  finally
    BEDM.qryBillID.close;
  end;
  Log('Bill ID = ' + IntToStr(FBillID));
end;

procedure TBillingEngine.StoreDetail;
const
  BatchSize = 400;
  ProgressFrequency = 2000;
  Tab = #09;

  ATTDNORCAL = 'ATT_LANorth';  //qm-770
  ATTDSOUTH_INVOICE = 'ATT_LASouth';
var
  i,n, t: Integer;
  Loc: TLocate;
  DataRows: TStringList;
  DetailExportDir: string;
  ExportFile: string;
  Invoice: string;
  minBillDetail, maxBillDetail, lowID, highID  : Integer;
begin
  DataRows := TStringList.Create;
  PerfLog.Start('StoreDetail: Generate billing detail export');
  try
    DataRows.Capacity := 50000;
    DetailExportDir := QMBillingDM.GetIniSetting('Billing',
      'DetailExportDir', '');
    if Trim(DetailExportDir) <> '' then
    begin // Save detail via export of a .tsv file
      // if FResultRecords.Count > 0 then // Output header row
      // DataRows.Add('billing_detail_id'+Tab+'bill_id'+Tab+'locate_id'+Tab+'billing_cc'+Tab+'ticket_number'+Tab+'work_county'+Tab+'work_state'+Tab+'work_city'+Tab+'work_address_number'+Tab+'work_address_number_2'+Tab+'work_address_street'+Tab+'ticket_type'+Tab+'transmit_date'+Tab+'due_date'+Tab+'status'+Tab+'qty_marked'+Tab+'closed_how'+Tab+'closed_date'+Tab+'bill_code'+Tab+'bucket'+Tab+'price'+Tab+'con_name'+Tab+'client_id'+Tab+'locator_id'+Tab+'work_done_for'+Tab+'work_type'+Tab+'work_lat'+Tab+'work_long'+Tab+'rate'+Tab+'work_cross'+Tab+'call_date'+Tab+'initial_status_date'+Tab+'emp_number'+Tab+'short_name'+Tab+'work_description'+Tab+'area_name'+Tab+'transmit_count'+Tab+'hours'+Tab+'line_item_text'+Tab+'revision'+Tab+'map_ref'+Tab+'locate_hours_id'+Tab+'qty_charged'+Tab+'which_invoice'+Tab+'tax_name'+Tab+'tax_rate'+Tab+'tax_amount'+Tab+'raw_units'+Tab+'update_of'+Tab+'high_profile'+Tab+'plat'+Tab+'arrival_date'+Tab+'units_marked'+Tab+'work_remarks');
      for i := 0 to FResultRecords.Count - 1 do
      begin
        Loc := TLocate(FResultRecords[i]);
        // TODO: Make this read from the actual table (to verify field positions) or generate a .fmt file
        DataRows.Add
          (Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', //QMANTWO-710
          ['1', Tab, IntToStr(FBillID), Tab, IntToStr(Loc.LocateID), Tab,
          Loc.ClientCode, Tab, Loc.TicketNumber, Tab, Loc.WorkCounty, Tab,
          Loc.WorkState, Tab, Loc.WorkCity, Tab, Loc.WorkAddressNumber, Tab,
          Loc.WorkAddressNumber2, Tab, LeftStr(Loc.WorkAddressStreet, 45), Tab,
          LeftStr(Loc.TicketType, 30), Tab, IsoDateTimeToStr(Loc.TransmitDate),
          Tab, IsoDateTimeToStr(Loc.DueDate, ''), Tab, Loc.Status, Tab,
          IntToStr(Loc.QtyMarked), Tab, Loc.ClosedHow, Tab,
          IsoDateTimeToStr(Loc.ClosedDate), Tab, Loc.BillCode, Tab,
          LeftStr(Loc.Bucket, 60), Tab, FloatToStr(Loc.Price), Tab,
          Loc.Excavator, Tab, IntToStr(Loc.ClientID), Tab,
          IntKeyToString(Loc.LocatorID), Tab, LeftStr(Loc.WorkDoneFor, 50), Tab,
          Loc.WorkType, Tab, FloatToStr(Loc.WorkLat), Tab,
          FloatToStr(Loc.WorkLong), Tab, FloatToStr(Loc.Rate), Tab,
          Loc.WorkCross, Tab, IsoDateTimeToStr(Loc.CallDate), Tab,
          IsoDateTimeToStr(Loc.InitialStatusDate), Tab, Loc.EmpNumber, Tab,
          LeftStr(Loc.ShortName, 20), Tab, LeftStr(Loc.WorkDescription, 254),
          Tab, Loc.AreaName, Tab, IntToStr(Loc.TransmitCount), Tab,
          FloatToStr(Loc.Hours), Tab, Loc.LineItemText, Tab, Loc.Revision, Tab,
          Loc.MapRef, Tab, IntKeyToString(Loc.LocateHoursID), Tab,
          IntToStr(Loc.QtyCharged), Tab, Loc.WhichInvoice, Tab, Loc.TaxName,
          Tab, FloatToStr(Loc.TaxRate), Tab, FloatToStr(Loc.TaxAmount), Tab,
          IntToStr(Loc.RawUnits), Tab, Loc.UpdateOf, Tab,
          IntToStr(Loc.HighProfileBit), Tab, Loc.Plat, Tab,
          IsoDateTimeToStr(Loc.InitialArrivalDate), Tab,
          IntToStr(Loc.UnitsMarked), Tab, LeftStr(Loc.WorkRemarks, 254), Tab,
          Loc.LocateAddedBy, Tab, IntToStr(Loc.RateValueGroupID), Tab,
          Loc.AdditionalLineItemText, Tab, BoolToStr(Loc.TixIsDup), Tab, IntToStr(Loc.RefID)])); //QMANTWO-710
      end;
      if DataRows.Count > 0 then
      begin
        PerfLog.Start('StoreDetail: Import billing detail export to DB');
        ExportFile := AddSlash(DetailExportDir) + 'BillingDetail' +MakeLegalFileName(FDescription) + '.tsv';
        OdForceDirectories(DetailExportDir);
        Log('Exporting billing detail results to ' + ExportFile + '...');

{$IFDEF DEBUG}
        ExportFile := 'C:\Billing\' + ExtractFileName(ExportFile);
{$ENDIF}
        DataRows.SaveToFile(ExportFile, TEncoding.ANSI);  //unicode
        BEDM.Conn.Execute
          (Format('bulk insert dbo.billing_detail from ''%s'' with (CHECK_CONSTRAINTS, FIELDTERMINATOR = ''\t'', ROWTERMINATOR = ''\n'')',
          [ExportFile]));
      end
      else
        Log('WARNING - Skipping billing detail export - no result records');
    end
    else
    begin // Save detail via a series of SQL queries
      PerfLog.Start('StoreDetail');
      if FResultRecords.Count < 1 then
        Exit;
      Log('Storing rated data into billing detail...');
      for i := 0 to FResultRecords.Count - 1 do
      begin
        Loc := TLocate(FResultRecords[i]);
        if DataRows.Count = 0 then
        begin
          DataRows.Add(Loc.GetInsertStatement(False, 'billing_detail', FBillID)
            ); // insert into billing_detail ()
          DataRows.Add(Loc.GetInsertStatement(True, 'billing_detail', FBillID));
          // select a,b,c,...
        end
        else
          DataRows.Add('union all ' + Loc.GetInsertStatement(True,
            'billing_detail', FBillID)); // union all select a,b,c,...

        if ((i > 0) and ((i mod BatchSize) = 0)) or
          (i = (FResultRecords.Count - 1)) then
        begin
          try
            BEDM.Conn.Execute(DataRows.Text);
          except
            on E: Exception do
            begin
              LogError(DataRows.Text);
              raise;
            end;
          end;
          DataRows.Clear;
          Application.ProcessMessages;
        end;
        if (i mod ProgressFrequency) = 0 then
          LogFmt('%d of %d...', [i, FResultRecords.Count]);
      end; //for i := 0 to FResultRecords

      if ThisIsCalifornia then        // QMANTWO-594
      begin
        Log('Correcting "Which Invoice" for ATT ATTDNORCAL and ATTDNEVADA');  //qm-770
        Log('This could take some time...');
        with BEDM do
          try
            n := 0;
            minBillDetail := 0;
            maxBillDetail := 0;
            highID := 0;
            lowID  := 0;
            qryBillDetailMinMax.Parameters.ParamByName('BillID').Value := FBillID;
            qryBillDetailMinMax.open;
            minBillDetail:= qryBillDetailMinMax.fieldByName('minBillDetail').asinteger;
            maxBillDetail:= qryBillDetailMinMax.fieldByName('maxBillDetail').asinteger;
            t := maxBillDetail-minBillDetail;
            if minBillDetail = maxBillDetail then exit;
            lowID := minBillDetail;
            while highID < maxBillDetail do
            begin
              highID :=  lowID + 2000;
              if highID > maxBillDetail then highID:=maxBillDetail;

              updDateWhichInvoice.Parameters.ParamByName('BillID').Value := FBillID;
              updDateWhichInvoice.Parameters.ParamByName('lowID').Value := lowID;
              updDateWhichInvoice.Parameters.ParamByName('highID').Value := highID;
              n := updDateWhichInvoice.ExecSQL;

              fixWhichInvoice.Parameters.ParamByName('BillID').Value := FBillID;
              fixWhichInvoice.Parameters.ParamByName('WhichInvoice').Value :=
                ATTDNORCAL;   //qm-770
              fixWhichInvoice.Parameters.ParamByName('Client').Value := 'PACBEL';
              fixWhichInvoice.Parameters.ParamByName('lowID').Value := lowID;
              fixWhichInvoice.Parameters.ParamByName('highID').Value := highID;
              fixWhichInvoice.ExecSQL;

              fixWhichInvoice.Parameters.ParamByName('BillID').Value := FBillID;
              fixWhichInvoice.Parameters.ParamByName('WhichInvoice').Value :=
                ATTDSOUTH_INVOICE;
              fixWhichInvoice.Parameters.ParamByName('Client').Value := 'ATTDSOUTH';
              fixWhichInvoice.Parameters.ParamByName('lowID').Value := lowID;
              fixWhichInvoice.Parameters.ParamByName('highID').Value := highID;
              fixWhichInvoice.ExecSQL;

              if n>0 then
              Log(IntToStr(lowID-minBillDetail) + ' of '+IntToStr(t));
              lowID := highID;
              Application.ProcessMessages;
            end; //while highID<maxBillDetail do

            Log('WhichExport Correction complete');
            Log('Performing Re-Export');
          finally
            qryBillID.Close;

          end;
      end;

    end;
  finally
    FreeAndNil(DataRows);
  end;
  Log('Done');
end;

procedure TBillingEngine.Initialize;
begin
  Assert(FCallCenters.Count > 0, 'No call centers defined to bill');

  BillingConfig := TBillingConfig.CreateForCenterListString(BEDM.Conn, GetCallCenterSQLString);
  BillingConfig.Logger := FLogEvent;
  FTicketClientList.TieredClientFunc := BillingConfig.IsTieredClient;

  BillingConfig.LoadEligibleTerms(FEligibleTerms, GetCallCenterSQLString, Period.Span);
  Log('  # of Eligible Terms: ' + IntToStr(FEligibleTerms.Count));
  Log('  Eligible Terms: ' + FEligibleTerms.CommaText);
  if FEligibleTerms.Count < 1 then
    Log('Error: No elligible terms found for this billing period length');

  FRunDate := Now;
  FErrorCount := 0;

  BEDM.CheckBillingSetupSP.Parameters.ParamValues['@CallCenterList'] := FCallCenters.CommaText;
  try
    BEDM.CheckBillingSetupSP.Open;
    if not BEDM.CheckBillingSetupSP.Eof then begin
      while  not BEDM.CheckBillingSetupSP.Eof do begin
        Log('Configuration Problem: ' + BEDM.CheckBillingSetupSP.Fields[0].AsString);
        BEDM.CheckBillingSetupSP.Next;
      end;
      raise Exception.Create('Billing failed because of configuration problem.');
    end;
  finally
    BEDM.CheckBillingSetupSP.Close;
  end;
end;

// This does not run when only exporting raw data!
procedure TBillingEngine.MainProcessing;
begin
  CheckLoadedLocates;
  PopulateTicketClientList;
  ProcessUsage;

  if not NoExportOrSave then begin
    Log('Starting main process...');

    try
      PerfLog.Start('Creating Header Records');
      CreateHeaderRecord;
     // Execute transmission billing SP (populate billing_trans_detail using FBillID)
      StoreDetail;
      PerfLog.Start('Transmission calculations finished - SetTotals');
{  QM-86 commented for testing- spAdjustNIPSCO runs a post process SP to addjust NIPSCO rates.   }
     if CurrentlyBilling(CenterGroup('1851', True)) then begin  //QM-86
       try
        Log('Preparing to adjust for NIPSCO rates...');
        BEDM.spAdjustNIPSCO.Parameters.ParamByName('@BillID').Value:=FBillID;
        BEDM.spAdjustNIPSCO.ExecProc;
        Log('Adjusted for NIPSCO rates...');
       except
         raise;
       end;
     end;

//     if CurrentlyBilling(CenterGroup('1421', True)) then begin  //QM-786   797
//       try
//        Log('Preparing to adjust for SC DOM Comb rates...');
//        BEDM.spAdjustDOMCOMB.Parameters.ParamByName('@BillID').Value:=FBillID;
//        BEDM.spAdjustDOMCOMB.ExecProc;
//        Log('Adjusted for SC DOM Comb rates...');
//       except
//         raise;
//       end;
//     end;

     if CurrentlyBilling(CenterGroup('MDDC', True)) then begin  //QM-308
       try
        Log('Preparing to adjust for BGE Gas and Elec rates...');
        BEDM.spFMB_BGE_Adjustment.Parameters.ParamByName('@BillID').Value:=FBillID;
        BEDM.spFMB_BGE_Adjustment.ExecProc;
        Log('Adjusted for BGE Gas and Elec rates...');
       except
         raise;
       end;
     end;

//     if CurrentlyBilling(CenterGroup('MDVA', True)) then begin  //QM-525
//       try
//        Log('Preparing to adjust Rates for Dom408 terms paired with other DOMs...');
//        BEDM.spDOM408Adjustment.Parameters.ParamByName('@Bill_ID').Value:=FBillID;
//        BEDM.spDOM408Adjustment.ExecProc;  //Uncomment for release
//        Log('Adjusted rates for DOM408...');
//       except
//         raise;
//       end;
//     end;

      SetTotals;

      PerfLog.Start('Saving');
      Log('Saving to database...');

      Log('Commit completed');
      FResultRecords.Clear;
    except
      on E: Exception do begin
        Log('FAILED: ERROR: ' + E.Message);

        raise;
      end;
    end;

    ExportResults(FBillID, OutputBaseDir);
    if NoOldRunCleanup then
      Log('Skipped cleanup from previous billing runs.')
    else
      CleanupPreviousBillingRuns(FBillID);
  end;
  Log('Done');
end;

procedure TBillingEngine.ExportResults(BillID: Integer; Dir: string);
var
  ExportPhase: TPhase;
begin
  ExportPhase := PerfLog.Start('ExportResults');

  // Subphases disabled for the moment
  //ExportPhase.AddSubPhase('Iniaialize Export');

  FExportDM.OutputBaseDir := Dir;
{$IFDEF DEBUG}
  FExportDM.OutputBaseDir := 'C:\Logs\';
{$ENDIF}
  BEDM.ConnectReadConn(Log1, QMBillingDM.IniFileName);
  FExportDM.ConnectAllDBComponents(BEDM.Conn, BEDM.ReadConn);
  FExportDM.Logger := FLogEvent;
  FExportDM.ParentPhase := ExportPhase;
  FExportDM.SetCallCenters(FCallCenters.CommaText);

//  BEDM.Conn.BeginTrans;
  try
    FExportDM.ExportBillingRun(BillID);
    //ExportPhase.AddSubPhase('Commit Export');
//    BEDM.Conn.CommitTrans;
    Log('Process completed');
  except
    on E: Exception do begin
      Log('FAILED, rolling back: ERROR: ' + E.Message);
//      BEDM.Conn.RollbackTrans;
      raise;
    end;
  end;
end;

procedure TBillingEngine.CleanupPreviousBillingRuns(BillID: Integer);
var
  OutputFolder: string;
begin
  Log('Cleaning up from previous billing runs...');
//  BEDM.Conn.BeginTrans;
  try
    BEDM.DeletePriorBilling.Parameters.ParamByName('@BillID').Value := BillID;
    BEDM.DeletePriorBilling.Open;
    while not BEDM.DeletePriorBilling.Eof do begin
      OutputFolder := ExpandFileName(AddSlash(FExportDM.OutputBaseDir)) +
        'Output-' + BEDM.DeletePriorBilling.FieldByName('bill_id').AsString +
        '-' + BEDM.DeletePriorBilling.FieldByName('group_code').AsString;
      if DeleteDirectory(OutputFolder, True) then
        Log('  Deleted ' + OutputFolder)
      else
        Log('  Did not delete ' + OutputFolder);
      BEDM.DeletePriorBilling.Next;
    end;
//    BEDM.Conn.CommitTrans;
    Log('Clean up completed.');
  except
    on E: Exception do begin
      Log('FAILED, rolling back: ERROR: ' + E.Message);
//      BEDM.Conn.RollbackTrans;
      raise;
    end;
  end;
end;

procedure TBillingEngine.LoadUsageFromDir(Directory: string);
var
  FileName: string;
begin
  PerfLog.Start('Loading tickets from dir');
  if Directory = '' then
    raise Exception.Create('No directory was specified.');
  Directory := AddSlash(Directory);

  BEDM.ConnectReadConn(Log1, QMBillingDM.IniFileName);
  BEDM.MakeEmptyTix.Execute;

  FileName := Directory + TicketDataFile;
  if not FileExists(FileName) then
    raise Exception.Create(FileName + ' is missing');
  Log('Loading tickets from: ' + FileName + '...');
  BEDM.TicketRawData.Open;
  try
    CloneStructure(BEDM.TicketRawData, BEDM.TicketFileData);
  finally
    BEDM.TicketRawData.Close;
  end;

  ClearTable(BEDM.TicketFileData);
  BEDM.TicketFileData.ImportTable(FileName, #09, True);
  BEDM.TicketFileData.DeleteAllIndexes;
  BEDM.TicketFileData.AddIndex('ticket_id', 'ticket_id', []);
  BEDM.TicketFileData.Open;
  LogFmt('Loaded %d tickets', [BEDM.TicketFileData.RecordCount]);

  PerfLog.Start('Loading locates from dir');
  FileName := Directory + LocateDataFile;
  if not FileExists(FileName) then
    raise Exception.Create(FileName + ' is missing');
  Log('Loading locates from: ' + FileName + '...');
  BEDM.LocateRawData.Open;
  try
    CloneStructure(BEDM.LocateRawData, BEDM.LocateFileData);
  finally
    BEDM.LocateRawData.Close;
  end;

  ClearTable(BEDM.LocateFileData);
  BEDM.LocateFileData.ImportTable(FileName, #09, True);
  BEDM.LocateFileData.Open;
  LogFmt('Loaded %d locates', [BEDM.LocateFileData.RecordCount]);

  CreateLocateObjectsFromDataSets(BEDM.TicketFileData, BEDM.LocateFileData);
  ClearTable(BEDM.TicketFileData);
  ClearTable(BEDM.LocateFileData);

  FileName := Directory + HourlyDataFile;
  if FileExists(FileName) and (GetSizeOfFile(FileName) > 0) then begin
    PerfLog.Start('Loading hours/units from Dir');
    Log('Loading hourly data from: ' + FileName + '...');
    // This returns nothing - it is only opened so we get the fields to clone
    BEDM.DailyHoursUnits.Open;
    CloneStructure(BEDM.DailyHoursUnits, BEDM.HoursUnits);
    BEDM.DailyHoursUnits.Close;
    AddHourlyIndexes;
    ClearTable(BEDM.HoursUnits);
    Log('Loading hourly data from file: ' + FileName + '...');
    BEDM.HoursUnits.Open;
    BEDM.HoursUnits.ImportTable(FileName, #09, True);
    Log('Loaded ' + IntToStr(BEDM.HoursUnits.RecordCount) + ' hourly records');
  end;
end;

// The call centers should be in CC1+CRLF+CC2 format (like TStrings.Text)
procedure TBillingEngine.SetCallCenters(const Centers: string);
begin
  FCallCenters.Text := Centers;
end;

procedure TBillingEngine.AddScreenChargeForLocate(Locate: TLocate);

  // TODO: Optimize the FLocates scan here
  function GetOldestClosedOUCLocateOnTicket(Locate: TLocate): TLocate;
  var
    i: Integer;
    SearchForID: Integer;
    OCCode: string;
    OldestClosedDate: TDateTime;
    CheckLocate: TLocate;
  begin
    SearchForID := Locate.TicketID;
    OldestClosedDate := MaxDateTime;
    Result := nil;

    for i := 0 to FLocates.Count - 1 do begin
      CheckLocate := TLocate(FLocates[i]);
      if CheckLocate.TicketID = SearchForID then begin
        OCCode := CheckLocate.ClientCode;
        if CheckLocate.Closed
          and (CheckLocate.ClosedDate < OldestClosedDate)
          and (OCCode = 'OUC553') or (OCCode = 'OUC582') or (OCCode = 'OUC613') then begin
          Result := CheckLocate;
          OldestClosedDate := Result.ClosedDate;
        end;
      end;
    end;
    Assert(Assigned(Result), 'No oldest closed locate found');
  end;

var
  Oldest: TLocate;
  Code: string;
begin
  Assert(Assigned(Locate));

  if Locate.CallCenter = 'FOL1' then begin
    Code := Locate.ClientCode;
    if (Code = 'OUC553') or (Code = 'OUC582') or (Code = 'OUC613') then begin
      Oldest := GetOldestClosedOUCLocateOnTicket(Locate);
      if (Oldest = Locate) and (not Locate.Invoiced) then
        Locate.ScreenCharge := 1.56;
    end;
  end;
end;

procedure TBillingEngine.CheckLoadedLocates;
begin
  if FLocates.Count < 1 then
    raise EOdDataEntryError.Create('No locates found to bill');

  CheckForDuplicateLocates;
end;

// We can only bill the highest price locate for many duplicate terms
procedure TBillingEngine.CorrectForDuplicateLocates;
var
  Locates: TLocateArray;
  ChangeCount: Integer;
  SetNCAndReprocess: Boolean;

  procedure SetLocateNoCharge(Locate: TLocate; SkipHourly: Boolean = True);
  var
    Changed: Boolean;
  begin
    Assert(Assigned(Locate));
    Changed := False;
    // There is an exception for FMB1/FTTP tickets with BGExxG/H locates (Mantis #1222)
    if Locate.IsHourlyFTTP and (IsFMB1BGEGTerm(Locate){ or IsFMWBGEGTerm(Locate)}) then
      Exit;

    if SkipHourly and IsDailyHourly(Locate) then
      Exit;

    if Locate.Price <> 0 then begin
      Locate.Price := 0.0;
      Changed := True;
    end;
    if (Locate.Rate <> 0) or (Locate.OvertimeRate <> 0) then begin
      Locate.Rate := 0.0;
      Locate.OvertimeRate := 0.0;
      Changed := True;
    end;

    if SetNCAndReprocess and (Locate.Status <> 'NC') then begin
      Locate.Status := 'NC';
      FTicketClientList.UpdateStatus(Locate.TicketID, Locate.ClientCode, Locate.Status);
      Changed := True;
    end;
    if Changed then begin
      Locate.Bucket := 'No Charge, Duplicate Term';
      Inc(ChangeCount);
    end;
  end;

  procedure CorrectFMB1AndFMWBGEGTerms(GLocate: TLocate; Locates: TLocateArray);
  var
    ClientCode: string;
    NonGClientCode: string;
    NonGLocate: TLocate;
  begin
    ClientCode := GLocate.ClientCode;
    if IsFMB1BGEGTerm(GLocate){ or IsFMWBGEGTerm(GLocate)} then
    begin
      if (Length(Locates) = 1) then begin
        // The non-G locate was billed last week, or will be next week?
        SetLocateNoCharge(GLocate);
        Exit;
      end;
      NonGClientCode := Copy(ClientCode, 1, Length(ClientCode) - 1);
      Assert(StrBeginsWith('BGE', NonGClientCode));
      NonGLocate := GetLocateMatching(NonGClientCode, Locates);
      if not Assigned(NonGLocate) then
      begin
        GLocate.BillingClientCode := NonGClientCode;
        LogErrorFmt('ERROR: Ticket %s has term %s but no term %s to bill to', [GLocate.TicketNumber, GLocate.ClientCode, NonGClientCode]);
      end
      else
      begin
        NonGLocate.Rate := GLocate.Rate;
        Assert(NonGLocate.QtyCharged = 1, 'BGE terms should have a Qty of 1'); // Non-G locates are always 1 for the Qty
        NonGLocate.Price := GetPriceFromRateQty(NonGLocate, NonGLocate.Rate, NonGLocate.QtyCharged);
        NonGLocate.Bucket := GLocate.Bucket;
        SetBillCode(NonGLocate, BillCodeBGEGTerm);
        SetLocateNoCharge(GLocate);
      end;
    end;
  end;

  procedure CorrectGTerms(GroupLocates: TLocateArray; GroupTerms: array of string; TermGroup: string);
  var
    NonGTerm: TLocate;
    GTerm: TLocate;
  begin
    Assert(Length(GroupTerms) >= 2);
    // If the first term passed in is one we need to assign the G price to, do it now
    if MatchesTermGroup(GroupTerms[0], TermGroup) then begin
      NonGTerm := GetLocateMatching(GroupTerms[0], GroupLocates);
      GTerm := GetLocateMatching(GroupTerms[0] + 'G', GroupLocates);
      if Assigned(NonGTerm) and Assigned(GTerm) then begin
        if GTerm.Price > NonGTerm.Price then begin
          NonGTerm.Price := GTerm.Price;
          NonGTerm.Rate := GTerm.Rate;
          NonGTerm.OvertimeRate := GTerm.OvertimeRate;
          NonGTerm.LineItemText := GTerm.LineItemText;
          NonGTerm.Bucket := GTerm.Bucket;
          NonGTerm.QtyCharged := GTerm.QtyCharged;
        end;
        if not IsDailyHourly(GTerm) then begin
          GTerm.Price := 0;
          GTerm.Rate := 0;
          GTerm.OvertimeRate := 0;
          GTerm.QtyCharged := 0;
        end;
      end;
    end;
  end;

  procedure ProcessLocatesForGroup(GroupTerms: array of string);
  var
    GroupLocates: TLocateArray;
    i: Integer;
    Locate: TLocate;
    BilledLocate: TLocate;
    HighestPriceLocate: TLocate;
    HighestRateLocate: TLocate;
    OneInGroupIsInvoiced: Boolean;
  begin
//    Assert((Length(GroupTerms) > 0), 'Empty term list in ProcessLocatesForGroup');
    if not Length(GroupTerms) > 0 then  //qm-540 no need to throw exception.  Just log and move on.  sr
    begin
      Log('Empty term list in ProcessLocatesForGroup.');
      exit;  //qm-540 move on to next term group
    End;
    SetLength(GroupLocates, 0);
    OneInGroupIsInvoiced := False;
    // Get the list of locates on this ticket in the term ID group requested
    for i := 0 to Length(Locates) - 1 do begin
      Locate := Locates[i];
      if IsTextInArray(Locate.ClientCode, GroupTerms) then begin
        if (Locate.Status <> 'NC') and Locate.Invoiced then
          OneInGroupIsInvoiced := True;
        BilledLocate := GetBilledLocate(Locate.LocateID);
        if Assigned(BilledLocate) then begin
          SetLength(GroupLocates, Length(GroupLocates) + 1);
          GroupLocates[Length(GroupLocates) - 1] := BilledLocate;
        end;
      end;
    end;
    if OneInGroupIsInvoiced then begin
      for i := 0 to Length(GroupLocates) - 1 do
        SetLocateNoCharge(GroupLocates[i]); // We may want to pass in False here, pending the decision in Mantis 2611
      Exit;
    end;
    HighestPriceLocate := nil;
    HighestRateLocate := nil;
    // If we have more than one locate from the group, we might have "duplicates"
    if Length(GroupLocates) > 1 then begin
      if Locate.CallCenter = 'FMB1' then begin
        for i := 0 to Length(GroupLocates) - 1 do begin
          Locate := GroupLocates[i];
          if (not Assigned(HighestPriceLocate)) or
            (Locate.Price > HighestPriceLocate.Price) or
            ((Locate.Price = HighestPriceLocate.Price) and
            (((Locate.CallCenter = 'FMB1') {or MatchesCenterGroup(Locate, 'FMW')}) and
            (StrEndsWith('G', HighestPriceLocate.ClientCode)))) then
            HighestPriceLocate := Locate;
          if (not Assigned(HighestRateLocate)) or
            (Locate.Rate > HighestRateLocate.Rate) or
            ((Locate.Rate = HighestRateLocate.Rate) and
            (((Locate.CallCenter = 'FMB1') {or MatchesCenterGroup(Locate, 'FMW')}) and
            (StrEndsWith('G', HighestRateLocate.ClientCode)))) then
            HighestRateLocate := Locate;
        end;
        Assert(Assigned(HighestPriceLocate));//QMANTWO-398
        Assert(Assigned(HighestRateLocate)); //QMANTWO-398
      end
      else
      begin  //QMANTWO-428  this is the fall through state.
        for i := 0 to Length(GroupLocates) - 1 do begin   //QMANTWO-358
          Locate := GroupLocates[i];
          if (not Assigned(HighestPriceLocate)) or
            (Locate.Parties > 1) or // if party, use that price
            (Locate.Price > HighestPriceLocate.Price) or    //else look for the highest non-party rate.
            (Locate.Price = HighestPriceLocate.Price)
            then
            HighestPriceLocate := Locate;
          if (not Assigned(HighestRateLocate)) or
            (Locate.Parties > 1) or
            (Locate.Rate > HighestRateLocate.Rate) or
            (Locate.Rate = HighestRateLocate.Rate)
            then
            HighestRateLocate := Locate;
        end;
        Assert(Assigned(HighestPriceLocate)); //QMANTWO-398
        Assert(Assigned(HighestRateLocate));  //QMANTWO-398
      end;

      // Set all other group locates on this ticket to be no charges
      for i := 0 to Length(GroupLocates) - 1 do begin
        if not (GroupLocates[i] = HighestPriceLocate) then
          SetLocateNoCharge(GroupLocates[i]);
      end;
    end;
    if (Length(GroupLocates) > 0) then begin
      if HighestRateLocate = nil then // Only one locate in group
        HighestRateLocate := GroupLocates[0];
      if (GroupLocates[0].CallCenter = 'FMB1') {or MatchesCenterGroup(GroupLocates[0].CallCenter, 'FMW')} then
        CorrectFMB1AndFMWBGEGTerms(HighestRateLocate, GroupLocates);
//      else if (GroupLocates[0].CallCenter = 'FCO1') then
//        CorrectGTerms(GroupLocates, GroupTerms, 'FCOTermsWithGTerms');
    end;
  end;

  procedure ProcessFCV3Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms1'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms2'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms3'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms4'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms5'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms6'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms7'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms8'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms9'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms10'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms11'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms12'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms13'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms14'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms15'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms16'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms17'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms18'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms19'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms20'));
    ProcessLocatesForGroup(TermGroup('FCV3DuplicateTerms21'));
  end;

//  procedure ProcessFMW1Duplicates;  //QM-451 this is now call from FMB
//  begin  //QM-451 kept as is to allow for backward comp
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms1'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms2'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms3'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms4'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms5'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms6'));
//    ProcessLocatesForGroup(TermGroup('FMWDuplicateTerms7'));
////    ProcessLocatesForGroup(TermGroup('FMWBGETerms')); QM-451
//  end;

  procedure ProcessOCCDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('OCC1VPWTermList'));
    ProcessLocatesForGroup(TermGroup('OCCAlleghenyTerms'));
    ProcessLocatesForGroup(TermGroup('OCC2DMNTermList'));
    ProcessLocatesForGroup(TermGroup('OCCComcastTerms'));

    ProcessLocatesForGroup(TermGroup('OCCCOXTermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('OCCVZNTermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('OCCRECTermsDups'));   //QMANTWO-562

  end;

  procedure ProcessLORDuplicates;    //QMANTWO-562
  begin
    ProcessLocatesForGroup(TermGroup('LORBCCTermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LORCWSTermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LORCPITermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LOROTECTermsDups'));  //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LORPPLTermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LORPGETermsDups'));   //QMANTWO-562
    ProcessLocatesForGroup(TermGroup('LORPGEFTermsDups'));  //QMANTWO-562
  end;

  procedure ProcessFMB1Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FMBBGEETerms'));
    ProcessLocatesForGroup(TermGroup('FMBBGEGTerms')); //qm-309
    ProcessLocatesForGroup(TermGroup('FMBPTETerms'));
    ProcessLocatesForGroup(TermGroup('FMBTBTerms'));
    ProcessLocatesForGroup(TermGroup('FMBVTerms'));
  end;

  procedure ProcessFNV1Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FNVBTerms'));
    ProcessLocatesForGroup(TermGroup('FNVNESTerms'));
    ProcessLocatesForGroup(TermGroup('FNVINTTerms'));
    ProcessLocatesForGroup(TermGroup('FNVMTEMTerms'));
    ProcessLocatesForGroup(TermGroup('FNVCharterTerms'));
  end;

  procedure Process185Duplicates;  //QMANTWO-358
  begin
    ProcessLocatesForGroup(TermGroup('185NipscoElectricTerms'));
    ProcessLocatesForGroup(TermGroup('185NipscoGasTerms'));
  end;

//  procedure ProcessFCO1Duplicates;
//  var
//    i: Integer;
//    BothTerms: array of string;
//    TempTerms: TStringArray;
//  begin
//    ProcessLocatesForGroup(TermGroup('FCOAQLTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOATTerms'));
//    ProcessLocatesForGroup(TermGroup('FCODIATerms'));
//    ProcessLocatesForGroup(TermGroup('FCOESPTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOIREATerms'));
//    ProcessLocatesForGroup(TermGroup('FCOMCLDTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOMVELTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOPCTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOUNPTerms'));
//    ProcessLocatesForGroup(TermGroup('FCOUTerms'));
//
//    SetLength(BothTerms, 2);
//    TempTerms := TermGroup('FCOTermsWithGTerms');
//    for i := Low(TempTerms) to High(TempTerms) do begin
//      BothTerms[0] := TempTerms[i];
//      BothTerms[1] := BothTerms[0] + 'G';
//      ProcessLocatesForGroup(BothTerms);
//    end;
//  end;

  procedure ProcessFPL1Duplicates;  //qm-679
  begin
    ProcessLocatesForGroup(TermGroup('FPLSBFTerms'));
    ProcessLocatesForGroup(TermGroup('FPLGPTerms'));
  end;

  procedure ProcessFDX1Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FDXPTerms'));
    ProcessLocatesForGroup(TermGroup('FDXDupGas'));
    ProcessLocatesForGroup(TermGroup('FDXDupElectric'));
    ProcessLocatesForGroup(TermGroup('FDXDupValor'));
    ProcessLocatesForGroup(TermGroup('FDXVerizonTerms'));
  end;

  procedure ProcessFCLDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FCLCTCTerms'));
    ProcessLocatesForGroup(TermGroup('FCLDPTerms'));
  end;

  procedure ProcessFGVDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FGVDPTerms'));
    ProcessLocatesForGroup(TermGroup('FGVTWTerms'));
  end;

  procedure ProcessFPKDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FPKDuplicateTerms1'));
    ProcessLocatesForGroup(TermGroup('FPKDuplicateTerms2'));
    ProcessLocatesForGroup(TermGroup('FPKDupTermsVNG'));
  end;

  procedure Process300Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('300DupAGL'));
    ProcessLocatesForGroup(TermGroup('300DupBST'));
    ProcessLocatesForGroup(TermGroup('300DupCOM'));
    ProcessLocatesForGroup(TermGroup('300DupGP'));
    ProcessLocatesForGroup(TermGroup('300DupJCK'));
    ProcessLocatesForGroup(TermGroup('300DupCharter'));
    ProcessLocatesForGroup(TermGroup('300DupCOW'));
  end;

  procedure Process600Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('600DupCox'));
    ProcessLocatesForGroup(TermGroup('600DupElectric'));
    ProcessLocatesForGroup(TermGroup('600DupGas'));
    ProcessLocatesForGroup(TermGroup('600DupPipeline'));
    ProcessLocatesForGroup(TermGroup('600DupTUFCO'));
    ProcessLocatesForGroup(TermGroup('FDXVerizonTerms'));
  end;

  procedure Process1421Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('142DupSCEE'));
    ProcessLocatesForGroup(TermGroup('142DupSCEG'));
    ProcessLocatesForGroup(TermGroup('142DupTimeWarner'));
  end;

  procedure Process7501Duplicates;
  begin
    // No duplicates to process for 7501
  end;

  procedure Process9001Duplicates;
  begin
    ProcessLocatesForGroup(TermGroup('9001DupSprint'));
  end;

  procedure ProcessMemphisDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('139TimeWarnerTerms'));
  end;

  procedure ProcessCADuplicates; // SCA / NCA    California
  begin
    ProcessLocatesForGroup(TermGroup('SCAVerizonDups'));
    ProcessLocatesForGroup(TermGroup('SCAComcastTerms'));
    ProcessLocatesForGroup(TermGroup('SCACharterTerms'));
    ProcessLocatesForGroup(TermGroup('NCA-ComcastConcord'));
    ProcessLocatesForGroup(TermGroup('NCA-ComcastNorthBay'));
    ProcessLocatesForGroup(ATnTTransOvrd);
  end;

  procedure ProcessFDEDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FDEComcastTerms'));
    ProcessLocatesForGroup(TermGroup('FDEDelmarvaPowerTerms'));
    ProcessLocatesForGroup(TermGroup('FDEVerizonTerms'));
  end;

  procedure ProcessFTLDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FTLCharterTerms'));
    ProcessLocatesForGroup(TermGroup('FTLTimeWarnerTerms'));
    ProcessLocatesForGroup(TermGroup('FTLNoChargeBellSouthTerms'));
  end;

  procedure ProcessFCTDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FTLChatanoogaGasTerms'));
  end;

  procedure ProcessFAMDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('FAMMediaComTerms'));
    ProcessLocatesForGroup(TermGroup('FAMMobileGasTerms'));
  end;

  procedure ProcessLouisianaDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('CenturyTermsLA'));
    ProcessLocatesForGroup(TermGroup('CoxTermsLA'));
  end;

  procedure ProcessAtlantaDuplicates;
  begin
    ProcessLocatesForGroup(TermGroup('AtlantaAGLTerms'));
    ProcessLocatesForGroup(TermGroup('AtlantaBellSouthTerms'));
    ProcessLocatesForGroup(TermGroup('AtlantaCharterTerms'));
    ProcessLocatesForGroup(TermGroup('AtlantaComcastTerms'));
    ProcessLocatesForGroup(TermGroup('AtlantaGAPowerTerms'));
  end;

  procedure StartProcessing(CallCenters: array of string);
  var
    Tickets: TLocatesByTicket;
    i: Integer;
  begin
    if not CurrentlyBilling(CallCenters) then
      Exit;
    LogFmt('Consolidating duplicate terms for %s (may take several minutes)...', [StringArrayToString(CallCenters)]);
    ChangeCount := 0;
    Tickets := TLocatesByTicket.Create(FResultRecords, CallCenters);
    try
      for i := 0 to Tickets.Count - 1 do begin
        Tickets.GetTicketByIndex(i, Locates);
        Assert(Length(Locates) > 0);
        if StringInArray('FCV3', CallCenters) then
          ProcessFCV3Duplicates
//        else if StringInArray('FMW1', CallCenters) then   //qm-451 sr merge with FMB backward comp
//          ProcessFMW1Duplicates
        else if StringInArray('OCC1', CallCenters) or StringInArray('OCC2', CallCenters) then
          ProcessOCCDuplicates
        else if StringInArray('LCA1', CallCenters)  //QMANTWO-562
             or StringInArray('LEM1', CallCenters)  //QMANTWO-562
             or StringInArray('LOR1', CallCenters)  //QMANTWO-562
             or StringInArray('LOR2', CallCenters)  //QMANTWO-562
             or StringInArray('LOR3', CallCenters) then  //QMANTWO-562
          ProcessLORDuplicates  //QMANTWO-562
        else if StringInArray('FMB1', CallCenters) then
        begin
          ProcessFMB1Duplicates;
//          ProcessFMW1Duplicates; //qm-451 sr merge with FMB
        end
        else if StringInArray('FNV1', CallCenters) then
          ProcessFNV1Duplicates
//        else if StringInArray('FCO1', CallCenters) then
//          ProcessFCO1Duplicates
        else if StringInArray('FPL1', CallCenters) then
          ProcessFPL1Duplicates
        else if StringInArray('FDX1', CallCenters) then
          ProcessFDX1Duplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FCL')) then
          ProcessFCLDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FGV')) then
          ProcessFGVDuplicates
        else if StringInArray('FPK1', CallCenters) or StringInArray('FPK2', CallCenters) then
          ProcessFPKDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('STSAtlanta', False)) then
          Process300Duplicates
        else if StringInArray('6001', CallCenters) or StringInArray('6002', CallCenters) then
          Process600Duplicates
        else if StringInArray('7501', CallCenters) then
          Process7501Duplicates
        else if StringInArray('9001', CallCenters) then
          Process9001Duplicates
        else if StringInArray('1421', CallCenters) then
          Process1421Duplicates
        else if StringInArray('1851', CallCenters) then       //QMANTWO-358
          Process185Duplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('Memphis', False)) then
          ProcessMemphisDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('California', False)) then  //California
          ProcessCADuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FDE', False)) then
          ProcessFDEDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FTL', False)) then
          ProcessFTLDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FCT', False)) then
          ProcessFCTDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FAM', False)) then
          ProcessFAMDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('Atlanta', False)) then
          ProcessAtlantaDuplicates
        else if HaveMatchingArrayMember(CallCenters, CenterGroup('FBL', False)) or
          HaveMatchingArrayMember(CallCenters, CenterGroup('141', False)) then
          ProcessLouisianaDuplicates
        else
          Assert(False, 'Missing else clause in StartProcessing');
        if i mod 1000 = 0 then
          Application.ProcessMessages;
      end;
    finally
      FreeAndNil(Tickets);
    end;

    LogFmt('Corrected %d duplicate %s locates', [ChangeCount, StringArrayToString(CallCenters)]);
    if SetNCAndReprocess then begin
      // We have to re-rate all locates, since tiering might have changed
      // due to the above changes of duplicate locates to the NC status
      if ChangeCount > 0 then begin
        Log('Re-rating all billable locates due to corrected duplicates...');
        // Note that this clears the bucket map and FResultRecords and recreates them
        ProcessAllLocates;
      end;
    end;
  end;

// TODO: Clean up this awful mess of code.....
{
 A new table could link a call_center to 1 or more term_groups representing
 the duplicates. Then, for the vast majority of the cases, the user could
 configure this in QMBilling.
}
begin // CorrectForDuplicateLocates
  PerfLog.Start('CorrectForDuplicateLocates');
  SetNCAndReprocess := False;
  if CurrentlyBilling('FCV3') then begin
    SetNCAndReprocess := True;
    StartProcessing(['FCV3']);
  end;
//  StartProcessing(['FMW1']); //qm-451 keep after merge for back comp
  StartProcessing(['OCC1', 'OCC2']);
  StartProcessing(['FMB1']);
  StartProcessing(['FNV1']);
//  StartProcessing(['FCO1']);
  StartProcessing(['FPL1']);
  StartProcessing(['FDX1']);
  StartProcessing(['1851', '1852']);
  StartProcessing(CenterGroup('FCL', False));
  StartProcessing(CenterGroup('FGV', False));
  StartProcessing(['FPK1', 'FPK2']);
  StartProcessing(CenterGroup('STSAtlanta', False));
  StartProcessing(['6001', '6002']);
  StartProcessing(['7501']);
  StartProcessing(['9001']);
  StartProcessing(CenterGroup('Memphis', False));
  StartProcessing(['NCA1']);
  StartProcessing(CenterGroup('California', False));
  StartProcessing(['1421']);
  StartProcessing(CenterGroup('Texas-FDX600', False));
  StartProcessing(CenterGroup('FDE', False));
  StartProcessing(CenterGroup('FTL', False));
  StartProcessing(CenterGroup('FCT', False));
  StartProcessing(CenterGroup('FBL', False));
  StartProcessing(CenterGroup('141', False));
  StartProcessing(CenterGroup('Atlanta', False));
  StartProcessing(CenterGroup('LOR', False));   //qm-540 sr
end;

function TBillingEngine.GetLocateFromList(LocateID: Integer; List: TObjectList): TLocate;
var
  i: Integer;
  Locate: TLocate;
begin
  Assert(LocateID > 0, 'Invalid locate ID');
  Assert(Assigned(List));
  Result := nil;
  // This could use some optimization, via a new indexed data structure
  for i := 0 to List.Count - 1 do begin
    Locate := TLocate(List[i]);
    Assert(Assigned(Locate));
    if (Locate.LocateID = LocateID) and (not Locate.IsExtraCharge) then begin // Ignore hours/units/screen type locates
      Result := Locate;
      Exit;
    end;
  end;
end;

function TBillingEngine.GetBilledLocate(LocateID: Integer): TLocate;
begin
  Result := GetLocateFromList(LocateID, FResultRecords);
end;

function TBillingEngine.GetLocate(LocateID: Integer): TLocate;
begin
  Result := GetLocateFromList(LocateID, FLocates);
end;

function TBillingEngine.MatchesCCDone(Locate: TLocate; const ClientGroupName, DoneForValueGroup: string): Boolean;
begin
  Result := MatchesTermGroup(Locate, ClientGroupName) and
    MatchesValueGroup(Locate.WorkDoneFor, DoneForValueGroup);
end;

function TBillingEngine.MatchesCCDoneExc(Locate: TLocate; const ClientGroupName, DoneForValueGroup, ExcValueGroup: string): Boolean;
begin
  Result := MatchesCCDone(Locate, ClientGroupName, DoneForValueGroup) and
    MatchesValueGroup(Locate.Excavator, ExcValueGroup);
end;

procedure TBillingEngine.CorrectFMBCOHWTicket(Locates: TLocateArray);

  procedure SetLocateProperties(Locate: TLocate; Price: Currency);
  begin
    Locate.Price := Price;
    Locate.Rate := Locate.Price;
    SetBillCode(Locate, BillCodeCOHWTerm);
    if Locate.BillingTicketType = TicketTypeAfter then
      Locate.Bucket := 'After Hours'
    else
      Locate.Bucket := 'Normal Notice';
    Locate.Bucket := Locate.Bucket + ', ' + GetStatusDescription(Locate.Status) + ', ' + FormatCurr('#,##0.00', Locate.Price) + ' Rate';
  end;

  procedure ProcessPair(WTerm, STerm: TLocate);
  //TODO: AVISTA out of LWA does something similar that's setup using the rate table; see GetLWALIDBillingCity.
  const
    SingleMarkedRate = 12.50;
    BothMarkedRate1 = 7.63;
    BothMarkedRate2 = 7.62;
    BothNotMarkedRate1 = 3.13;
    BothNotMarkedRate2 = 3.12;
    NotMarkedRate = 6.25;
    NoChargeRate = 0.00;
    AfterHoursSingleMarkedRate = 18.00;
    AfterHoursBothMarkedRate = 9.00;
    arrStatus : array[0..2] of string = ('M','MSI','MIR');  //QMANTWO-390
    arrNC : array[0..1]  of string = ('NC', 'ZZZ'); //QMANTWO-390
  var
    BilledWTerm: TLocate;
    BilledSTerm: TLocate;
    WIsNC: Boolean;
    SIsNC: Boolean;
    WOnlyIsNC: Boolean;
    SOnlyIsNC: Boolean;
    NeitherAreNC: Boolean;
  begin
    if Assigned(WTerm) then
      BilledWTerm := GetBilledLocate(WTerm.LocateID)
    else
      BilledWTerm := nil;
    if Assigned(STerm) then
      BilledSTerm := GetBilledLocate(STerm.LocateID)
    else
      BilledSTerm := nil;

    WisNC := Assigned(BilledWTerm) and (MatchText(BilledWTerm.Status, arrNC)); //QMANTWO-390
    SisNC := Assigned(BilledSTerm) and (MatchText(BilledSTerm.Status, arrNC)); //QMANTWO-390
    WOnlyIsNC := WIsNC and (not SIsNC);
    SOnlyIsNC := SIsNC and (not WIsNC);
    NeitherAreNC := (not WIsNC) and (not SIsNC);

    if Assigned(BilledWTerm) and Assigned(BilledSTerm) then begin
      if BilledWTerm.BillingTicketType = TicketTypeAfter then begin
        if WIsNC then
          SetLocateProperties(BilledWTerm, NoChargeRate);
        if SIsNC then
          SetLocateProperties(BilledSTerm, NoChargeRate);
        if NeitherAreNC then begin
          SetLocateProperties(BilledWTerm, AfterHoursBothMarkedRate);
          SetLocateProperties(BilledSTerm, AfterHoursBothMarkedRate);
        end
        else if SOnlyIsNC then
          SetLocateProperties(BilledWTerm, AfterHoursSingleMarkedRate)
        else if WOnlyIsNC then
          SetLocateProperties(BilledSTerm, AfterHoursSingleMarkedRate);
      end
      else begin  //QMANTWO-390
        // We have a normal (non-after) ticket with both terms
        if ((MatchText(BilledWTerm.Status,arrStatus)) and (MatchText(BilledSTerm.Status,arrStatus)))  then begin
          SetLocateProperties(BilledWTerm, BothMarkedRate1);
          SetLocateProperties(BilledSTerm, BothMarkedRate2);
        end  // treating NL as Not Marked
        else if ((MatchText(BilledWTerm.Status,arrStatus)) and (not(MatchText(BilledSTerm.Status,arrStatus)) or (BilledSTerm.Status='NL'))) then begin
          SetLocateProperties(BilledWTerm, SingleMarkedRate);
          SetLocateProperties(BilledSTerm, NotMarkedRate);
        end
        else if ((not(MatchText(BilledWTerm.Status,arrStatus) or (BilledWTerm.Status='NL'))) and (MatchText(BilledSTerm.Status,arrStatus))) then begin
          SetLocateProperties(BilledWTerm, NotMarkedRate);
          SetLocateProperties(BilledSTerm, SingleMarkedRate);
        end
        else begin
//          Assert(not(MatchText(BilledSTerm.Status,arrStatus)),'' );  //QMANTWO-745 investigate further  ..sr
//          Assert(not(MatchText(BilledWTerm.Status,arrStatus)),'' );  //QMANTWO-745

          //QMANTWO-390
        { TODO : Unless NL is treated as NC, there are more possibilities that must be accounted for. }
        if WIsNC then
          SetLocateProperties(BilledWTerm, NoChargeRate);
        if SIsNC then
          SetLocateProperties(BilledSTerm, NoChargeRate);
        if NeitherAreNC then begin
          SetLocateProperties(BilledWTerm, BothNotMarkedRate1);
          SetLocateProperties(BilledSTerm, BothNotMarkedRate2);
        end
        else if SOnlyIsNC then
          SetLocateProperties(BilledWTerm, NotMarkedRate)
        else if WOnlyIsNC then
          SetLocateProperties(BilledSTerm, NotMarkedRate);
        end;
      end;
    end
    else if Assigned(BilledWTerm) then begin
      // Only the W term on this ticket
      if BilledWTerm.BillingTicketType = TicketTypeAfter then begin
        if WIsNC then
          SetLocateProperties(BilledWTerm, NoChargeRate)
        else
          SetLocateProperties(BilledWTerm, AfterHoursSingleMarkedRate)
      end
      else begin
        if MatchText(BilledWTerm.Status,arrStatus) then    //QMANTWO-390
          SetLocateProperties(BilledWTerm, SingleMarkedRate)
        else
          SetLocateProperties(BilledWTerm, NotMarkedRate)
      end;
    end
    else if Assigned(BilledSTerm) then begin
      // Only the S term is on this ticket
      if BilledSTerm.BillingTicketType = TicketTypeAfter then begin
        if SIsNC then
          SetLocateProperties(BilledSTerm, NoChargeRate)
        else
          SetLocateProperties(BilledSTerm, AfterHoursSingleMarkedRate)
      end
      else begin
        if MatchText(BilledSTerm.Status,arrStatus) then    //QMANTWO-390
          SetLocateProperties(BilledSTerm, SingleMarkedRate)
        else
          SetLocateProperties(BilledSTerm, NotMarkedRate)
      end;
    end;
  end;

begin
  ProcessPair(GetLocateMatching('COHWS02', Locates), GetLocateMatching('COHWS02S', Locates));
  ProcessPair(GetLocateMatching('COHWSLS', Locates), GetLocateMatching('COHWSLSS', Locates));
  ProcessPair(GetLocateMatching('COHWSL01', Locates), GetLocateMatching('COHWSL01S', Locates));
end;

procedure TBillingEngine.CorrectFMBCOHWLocates;
begin
  if CurrentlyBilling('FMB1') then begin
    PerfLog.Start('CorrectFMBCOHWLocates');
    Log('Setting special FMB1 COHW term prices...');
    ForEachBilledTicket('FMB1', CorrectFMBCOHWTicket);
  end;
end;

procedure TBillingEngine.GetLocatesMatching(ClientCodes: array of string;
  Locates: TLocateArray; var MatchingLocates: TLocateArray);
begin
  GetLocatesMatching(ClientCodes, '*', Locates, MatchingLocates);
end;

procedure TBillingEngine.GetLocatesMatching(ClientCodes: array of string; const Status: string; Locates: TLocateArray;
  var MatchingLocates: TLocateArray);
begin
  GetLocatesMatching(ClientCodes, [Status], Locates, MatchingLocates);
end;

procedure TBillingEngine.GetLocatesMatching(ClientCodes,
  Statuses: array of string; Locates: TLocateArray;
  var MatchingLocates: TLocateArray);
var
  i,str1: Integer;
  Locate: TLocate;
  str:string;
begin
  SetLength(MatchingLocates, 0);
  for i := Low(Locates) to High(Locates) do begin
    Locate := Locates[i];
    if IsTextInArray(Locate.ClientCode, ClientCodes) and (IsTextInArray(Locate.Status, Statuses) or IsTextInArray('*', Statuses)) then begin
      WarnForExtraChargeLocates(Locate);
      str:=Locate.ClientCode;
      str1:=Locate.LocateID;
      SetLength(MatchingLocates, Length(MatchingLocates) + 1);
      MatchingLocates[Length(MatchingLocates) - 1] := Locate;
    end;
  end;
end;

function TBillingEngine.GetLocateMatching(ClientCode: string; Locates: TLocateArray): TLocate;
var
  i: Integer;
begin
  Result := nil;
  for i := Low(Locates) to High(Locates) do begin
    if SameText(Locates[i].ClientCode, ClientCode) then begin
      Result := Locates[i];
      WarnForExtraChargeLocates(Result);
      Exit;
    end;
  end;
end;

procedure TBillingEngine.GetLocatesMatching(const CallCenter: string; Statuses: array of string;
  var MatchingLocates: TLocateArray);
begin
  GetLocatesFromListMatching(FLocates, CallCenter, Statuses, MatchingLocates);
end;

procedure TBillingEngine.GetLocateList(const CallCenter: string; LocateIDs: TStringList);
var
  i: Integer;
  Locate: TLocate;
begin
  Assert(Assigned(LocateIDs));
  LocateIDs.Clear;
  LocateIDs.Duplicates := dupIgnore;
  LocateIDs.Sorted := True;
  for i := 0 to FLocates.Count - 1 do begin
    Locate := TLocate(FLocates[i]);
    if (CallCenter = '*') or (Locate.CallCenter = CallCenter) then
      LocateIDs.Add(IntToStr(Locate.LocateID));
  end;
end;

procedure TBillingEngine.CheckForDuplicateLocates;
var
  i: Integer;
  Locates: TStringList;
  Locate: TLocate;
begin
  PerfLog.Start('CheckForDuplicateLocates');
  Log('Checking for duplicate locates...');
  Locates := TStringList.Create;
  try
    GetLocateList('*', Locates);
    if Locates.Count < FLocates.Count then begin
      LogFmt('ERROR: This billing run contains %d duplicate locate IDs (duplicate assignments?)', [FLocates.Count - Locates.Count]);
      Inc(FErrorCount, FLocates.Count - Locates.Count);
    end
    else if Locates.Count > FLocates.Count then
      raise Exception.Create('More locate IDs than locates.  Serious problem!');

    Locates.Clear;
    Locates.Duplicates := dupError;
    Locates.Sorted := True;
    for i := 0 to FLocates.Count - 1 do begin
      Locate := TLocate(FLocates[i]);
      try
        Locates.Add(Format('%d/%s', [Locate.TicketID, Locate.ClientCode]));
      except
        LogErrorFmt('ERROR: Duplicate client %s found on ticket number %s (ID %d)', [Locate.ClientCode, Locate.TicketNumber, Locate.TicketID]);
      end;
    end;
  finally
    FreeAndNil(Locates);
  end;
end;

procedure TBillingEngine.SetBillCode(Locate: TLocate; const BillCode: string);
begin
  Assert(Assigned(Locate));
  if not Locate.SetBillCode(BillCode) then
    LogErrorFmt('Warning: %s Bill code set more than once', [GetLocateIdentifier(Locate)]);
end;

function TBillingEngine.GetLocateIdentifier(Locate: TLocate): string;
begin
  Assert(Assigned(Locate));
  Result := Format('%s/%s/%s/%d', [Locate.TicketNumber, Locate.ClientCode,
    Locate.Status, Locate.LocateID]);
end;

procedure TBillingEngine.ForEachBilledTicket(CallCenter: string; TicketProc: TForEachTicketProc);
var
  TicketLocates: TLocatesByTicket;
  i: Integer;
  j: Integer;
  Locates: TLocateArray;
begin
  TicketLocates := TLocatesByTicket.Create(FResultRecords, [CallCenter]);
  try
    for i := 0 to TicketLocates.Count - 1 do begin
      TicketLocates.GetTicketByIndex(i, Locates);
      Assert(Length(Locates) > 0);
      for j := Low(Locates) to High(Locates) do
        WarnForExtraChargeLocates(Locates[j]);
      TicketProc(Locates);
      if i mod 1000 = 0 then
        Application.ProcessMessages;
    end;
  finally
    FreeAndNil(TicketLocates);
  end;
end;

procedure TBillingEngine.GetLocatesFromListMatching(List: TObjectList;
  const CallCenter, Status: string; var MatchingLocates: TLocateArray);
var
  i: Integer;
  Locate: TLocate;
begin
  SetLength(MatchingLocates, 0);
  for i := 0 to List.Count - 1 do begin
    Locate := TLocate(List[i]);
    if (CallCenter = '*') or (Locate.CallCenter = CallCenter) then begin
      if (Status = '*') or (Locate.Status = Status) then begin
        WarnForExtraChargeLocates(Locate);
        SetLength(MatchingLocates, Length(MatchingLocates) + 1);
        MatchingLocates[Length(MatchingLocates) - 1] := Locate;
      end;
    end;
  end;
end;

procedure TBillingEngine.GetLocatesFromListMatching(List: TObjectList;
  const CallCenter: string; Statuses: array of string;
  var MatchingLocates: TLocateArray);
var
  i: Integer;
  Locate: TLocate;
begin
  SetLength(MatchingLocates, 0);
  for i := 0 to List.Count - 1 do begin
    Locate := TLocate(List[i]);
    if ((CallCenter = '*') or (Locate.CallCenter = CallCenter)) and
      (IsTextInArray(Locate.Status, Statuses) or IsTextInArray('*', Statuses)) then begin
      WarnForExtraChargeLocates(Locate);
      SetLength(MatchingLocates, Length(MatchingLocates) + 1);
      MatchingLocates[Length(MatchingLocates) - 1] := Locate;
    end;
  end;
end;

procedure TBillingEngine.GetBilledHPLocatesMatching(CallCenter,
  ClientCode: string; var MatchingLocates: TLocateArray);
var
  i: Integer;
  Locate: TLocate;
begin
  SetLength(MatchingLocates, 0);
  for i := 0 to FResultRecords.Count - 1 do begin
    Locate := TLocate(FResultRecords[i]);
    if Locate.HighProfile and (not Locate.IsExtraCharge) then begin
      if (CallCenter = '*') or (Locate.CallCenter = CallCenter) then begin
        if (ClientCode = '*') or (Locate.ClientCode = ClientCode) then begin
          SetLength(MatchingLocates, Length(MatchingLocates) + 1);
          MatchingLocates[Length(MatchingLocates) - 1] := Locate;
        end;
      end;
    end;
  end;
end;

procedure TBillingEngine.GetBilledLocatesMatching(CallCenters,
  ClientCodes: array of string; var MatchingLocates: TLocateArray);
var
  i: Integer;
  Locate: TLocate;
begin
  SetLength(MatchingLocates, 0);
  for i := 0 to FResultRecords.Count - 1 do begin
    Locate := TLocate(FResultRecords[i]);
    if not Locate.IsExtraCharge then begin
      if StringInArray(Locate.ClientCode, ClientCodes) and StringInArray(Locate.CallCenter, CallCenters) then begin
        SetLength(MatchingLocates, Length(MatchingLocates) + 1);
        MatchingLocates[Length(MatchingLocates) - 1] := Locate;
      end;
    end;
  end;
end;

procedure TBillingEngine.GetBilledAditionalQtyLocatesMatching(var MatchingLocates: TLocateArray);
var
  i: Integer;
  Locate: TLocate;
begin
  SetLength(MatchingLocates, 0);
  for i := 0 to FResultRecords.Count - 1 do begin
    Locate := TLocate(FResultRecords[i]);
    if (Locate.AdditionalQtyRate > 0) and (Locate.AdditionalQty > 0) then begin
      SetLength(MatchingLocates, Length(MatchingLocates) + 1);
      MatchingLocates[Length(MatchingLocates) - 1] := Locate;
    end;
  end;
end;

procedure TBillingEngine.ProcessDailyHoursUnits;
var
  totalUnits : integer;
  unitsToAdjust : integer;
  procedure ProcessDailyChargesForCallCenter(const CallCenter: string);
  var
    Locates: TLocateArray;
    i: Integer;
    Data: TDBISAMTable;
    Status: string;
    Units: Integer;
    UnitsMarked: Integer;
    WorkDate: TDateTime;
    LocateID: Integer;
    LocateHoursID: Integer;
    WorkEmpID: Integer;
    UnitsInvoiced: Boolean;
    UnitsLocate: TLocate;
    SourceLocate: TLocate;
    HourlyLocate: TLocate;
    TotalLocateHours: Currency;
  begin
    if not CurrentlyBilling(CallCenter) then
      Exit;

    Data := BEDM.HoursUnits;

    if UsesHourlyBilling(CallCenter) then begin
      LogFmt('Adding hourly charges for %s', [CallCenter]);
      GetLocatesMatching(CallCenter, ['H', 'OH'], Locates);
      LogFmt('Found %d hourly locates to bill for %s...', [Length(Locates), CallCenter]);
      for i := 0 to Length(Locates) - 1 do begin
        HourlyLocate := Locates[i];
        Assert(HourlyLocate.IsHourly);
        // Ineligible terms are ones for different billing periods (MONTHLY, etc.)
        if not EligibleTerm(HourlyLocate) then
          Continue
        // If the center bills hours only when the locate is closed, the locate invoice
        // and the hours invoiced flag are the same thing for some legacy hours records
        else if HourlyLocate.Invoiced and (not CenterBillsHoursDaily(HourlyLocate.CallCenter)) then
          Continue
        else if (not CenterBillsHoursDaily(HourlyLocate.CallCenter)) and (not ConsiderWorkDate(HourlyLocate.ClosedDate)) then
          Continue
        // Skip billing hours for non-closed locates for non-daily centers
        else if (not CenterBillsHoursDaily(HourlyLocate.CallCenter)) and (not HourlyLocate.Closed) then
          Continue
        else begin
          try
            AddHourlyLocateRecords(HourlyLocate, TotalLocateHours);
          except
            on E: Exception do begin
              LogErrorFmt('ERROR processing %s: %s', [GetLocateIdentifier(HourlyLocate), E.Message]);
            end;
          end;
          if TotalLocateHours > 0.01 then
            RemoveLocateFromBilledList(HourlyLocate)
          else begin
            HourlyLocate.QtyCharged := 0;
            HourlyLocate.Price := 0;
            HourlyLocate.Rate := 0;
            HourlyLocate.Bucket := 'Hourly, No Hours';
          end;
        end;
      end;
    end; // Hourly billing
    Data.Filtered := False;

    // There are currently no centers using unit billing, and if any ever do, we will have to
    // figure out how to not bill the qty_marked that these units add directly to the locate.
    if UsesUnitBilling(CallCenter) then begin
      LogFmt('Adding daily unit charges for %s', [CallCenter]);
      Data.AddIndex('locate_id','locate_id');
      Data.IndexFieldNames := 'locate_id';
      Data.First;
      totalUnits := 0;
      while not Data.Eof do begin
        Status := Data.FieldByName('status').AsString;
        UnitsMarked := Data.FieldByName('units_marked').AsInteger;
        Units := ConvertToBillingUnits(UnitsMarked,
           Data.FieldByName('first_unit_factor').AsInteger,
           Data.FieldByName('rest_units_factor').AsInteger);
        LocateID := Data.FieldByName('locate_id').AsInteger;
        WorkDate := Data.FieldByName('work_date').AsDateTime;
        WorkEmpID := Data.FieldByName('emp_id').AsInteger;
        UnitsInvoiced := Data.FieldByName('units_invoiced').AsBoolean;
        totalUnits := Units + totalUnits;
        LocateHoursID := Data.FieldByName('locate_hours_id').AsInteger;
        Assert(LocateID > 0, 'Invalid locate ID in daily hours/units data');
        Assert(LocateHoursID > 0, 'Invalid locate hours ID in daily hours/units data');
        SourceLocate := GetLocate(LocateID);
        Assert(Assigned(SourceLocate), Format('Locate ID %d not found in downloaded locates', [LocateID]));
        if (SourceLocate.CallCenter = CallCenter) and (not UnitsInvoiced) and
           (Status <> '') and (Units > 0) and (ConsiderWorkDate(WorkDate)) and (CallCenter<>'1851') then begin   //QM-86
          UnitsLocate := SourceLocate.CreateExtraChargeCopy;
          UnitsLocate.Closed := True; // These units are "closed", the locate may not be
          UnitsLocate.ClosedDate := WorkDate;
          UnitsLocate.QtyMarked := Units;
          UnitsLocate.RawUnits := UnitsMarked;
          UnitsLocate.Status := Status;
          UnitsLocate.LocateHoursID := LocateHoursID;
          UnitsLocate.LocatorID := WorkEmpID;
          UnitsLocate.Invoiced := False;
          ProcessLocate(UnitsLocate, True);
          // This is set last to make sure it is always set for invoicing purposes
          // TODO: Ignore bill code set warnings here in case ProcessLocate set it as well?
          SetBillCode(UnitsLocate, BillCodeUnits);
        end;
        Data.Next;
      end; // Unit billing
      Data.DeleteIndex('locate_id');
      if CallCenter = 'FDE8' then//QMANTWO-452
      begin//QMANTWO-452
        if totalUnits >= 500 then//QMANTWO-452
        unitsToAdjust := 500//QMANTWO-452
        else//QMANTWO-452
        unitsToAdjust := totalUnits;//QMANTWO-452
        with BEDM.insBillingAdjustments.Parameters do//QMANTWO-452
        begin//QMANTWO-452
          ParamByName('adjustment_date').Value := Period.StartDate;//QMANTWO-452
          ParamByName('customer_id').Value := 2083;//QMANTWO-452
          ParamByName('client_id').Value := 4645;//QMANTWO-452
          ParamByName('added_date').Value := Now;//QMANTWO-452
          ParamByName('type').Value := 'AMOUNT';//QMANTWO-452
          ParamByName('description').Value := format('Adjustment for %d units not billed',[unitsToAdjust]);//QMANTWO-452
          ParamByName('amount').Value := ((unitsToAdjust*22.00)*-1);//QMANTWO-452
          ParamByName('added_by').Value := 19763;//QMANTWO-452
          ParamByName('last_modified_by').Value := 19763;//QMANTWO-452
          ParamByName('modified_date').Value := Now;//QMANTWO-452
          ParamByName('active').Value := true;//QMANTWO-452
          ParamByName('which_invoice').Value := '';//QMANTWO-452
          ParamByName('gl_code').Value := '';//QMANTWO-452
        end;//with//QMANTWO-452
        BEDM.insBillingAdjustments.ExecSQL;//QMANTWO-452
      end;

//      showmessage('Units to adjust: '+IntToStr(unitsToAdjust)+' Total Units: '+INtToStr(totalUnits));
    end;
  end;

var
  i: Integer;
begin
  PerfLog.Start('ProcessDailyHoursUnits');
  for i := 0 to FCallCenters.Count - 1 do
    if UsesHourlyBilling(FCallCenters[i]) or UsesUnitBilling(FCallCenters[i]) then
      ProcessDailyChargesForCallCenter(FCallCenters[i]);
end;

procedure TBillingEngine.RemoveLocateFromBilledList(Locate: TLocate);
var
  Index: Integer;
begin
  Index := FResultRecords.IndexOf(Locate);
  if Index >= 0 then
    FResultRecords.Delete(Index);
end;

function GetTotalHours(DataSet: TDataSet; const FieldName: string): Currency;
begin
  Result := 0;
  DataSet.First;
  while not DataSet.Eof do begin
    Result := Result + DataSet.FieldByName(FieldName).AsFloat;
    DataSet.Next;
  end;
  DataSet.First;
end;

function TBillingEngine.HourlyLocatePrice(Locate: TLocate; Hours: Currency; Overtime: Boolean): Currency;
var
  HoursRounded: Integer;
  Rate: Currency;
begin
  HoursRounded := RoundUp(Hours);
  if Overtime then begin
    Rate := Locate.OvertimeRate;
    if MatchesCenterGroup(Locate, 'LOR', False) or MatchesCenterGroup(Locate, 'LWA', False) then //QM-180
    Locate.LineItemText := 'After 4 PM';//QM-180
  end
  else
    Rate := Locate.Rate;

  if (Locate.CallCenter = 'FAM1') then begin
    if (Locate.ClientCode = 'MOBG01') and (not Locate.WatchAndProtect) then begin
      Assert(HoursRounded >= 0);
      case HoursRounded of
        0: Result := 0;
        1: Result := Rate;
      else
        Result := Rate + ((Rate * 2) * (HoursRounded - 1));
      end;
      // Exception where Locate.Hours means # charged, not # worked
      Locate.Hours := HoursRounded;
      if HoursRounded > 1 then
        Locate.Hours := 1 + ((HoursRounded - 1) * 2);
      Assert(FloatEquals(Result, Locate.Hours * Locate.Rate));
    end
    else begin
      if Locate.WatchAndProtect then begin
        if Hours <= 4 then
          Result := Rate
        else
          Result := Rate * 2;
      end
      else
        Result := Rate * Hours;
    end;
  end
  else
    Result := Rate * Hours;
  Locate.Rate := Rate;
  Locate.OvertimeRate := Rate;
  Result := RoundTo(Result, -2);
  Assert(Result >= 0);
end;

function TBillingEngine.AddHourlyCharge(Locate: TLocate; DayHours, TotalHours: Currency;
  Overtime, NoCharge: Boolean): TLocate;
var
  HLocate: TLocate;
begin
  Assert(Trim(Locate.ClientCode) <> '', 'Source client code is blank for hourly locate ' + GetLocateIdentifier(Locate));
  Assert(Trim(Locate.BillingClientCode) <> '', 'Source billing client code is blank for hourly locate ' + GetLocateIdentifier(Locate));

  Assert(DayHours > 0);
  Result := Locate.CreateExtraChargeCopy;
  HLocate := Result;
  Assert(Trim(HLocate.ClientCode) <> '');
  Assert(Trim(HLocate.BillingClientCode) <> '', 'Billing client code is blank for hourly locate ' + GetLocateIdentifier(HLocate));
  HLocate.LocateHoursID := BEDM.HoursUnits.FieldByName('locate_hours_id').AsInteger;
  HLocate.Hours := DayHours;
  HLocate.Bucket := 'Hourly Charges for ' +
    FormatDateTime('mmm d', BEDM.HoursUnits.FieldByName('work_date').AsDateTime);
  if Overtime then
    HLocate.Bucket := 'Overtime ' + HLocate.Bucket;

  if (HLocate.WatchAndProtect) and (HLocate.CallCenter = 'FAM1') then begin
    if NoCharge then
      HLocate.Price := 0.0
    else
      HLocate.Price := HourlyLocatePrice(HLocate, TotalHours, Overtime);
  end
  else
    HLocate.Price := HourlyLocatePrice(HLocate, DayHours, Overtime);
  HLocate.QtyMarked := 0;
  HLocate.QtyCharged := 0;

  // This is set last to make sure it is always set for invoicing purposes
  SetBillCode(HLocate, BillCodeHourly);
  FResultRecords.Add(HLocate);
end;

function TBillingEngine.AddHourlyLocateRecords(Locate: TLocate; var TotalLocateHours: Currency): Integer;
var
  Hours: TDBISAMTable;
  RegularHours: Currency;
  OvertimeHours: Currency;
  WorkDate: TDateTime;
  HoursInvoiced: Boolean;
  NoCharge: Boolean;
  CanBillHoursForLocate: Boolean;
begin
  Assert(Assigned(Locate));
  Assert(Trim(Locate.ClientCode) <> '', 'Incoming client code is blank for hourly locate ' + GetLocateIdentifier(Locate));
  //Assert(Trim(Locate.BillingClientCode) <> '', 'Incoming billing client code is blank for hourly locate ' + GetLocateIdentifier(Locate));

  TotalLocateHours := 0;
  Result := 0;
  NoCharge := False;
  Hours := BEDM.HoursUnits;
  Hours.Filter := 'locate_id=' + IntToStr(Locate.LocateID);
  Hours.Filtered := True;
  Hours.First;

  TotalLocateHours := GetTotalHours(Hours, 'regular_hours') +
    GetTotalHours(Hours, 'overtime_hours');

  while not Hours.Eof do begin
    RegularHours := Hours.FieldByName('regular_hours').AsFloat;
    OvertimeHours := Hours.FieldByName('overtime_hours').AsFloat;
    if MatchesTermGroup(Locate.ClientCode, 'TermsThatBill1HourMin') then  //QMANTWO-465
    begin                                                      //QMANTWO-465
      if (RegularHours > 0) and  (RegularHours < 1.0) then     //QMANTWO-465
      RegularHours := 1.0;                                     //QMANTWO-465
      if (OvertimeHours > 0) and  (OvertimeHours < 1.0) then   //QMANTWO-465
     RegularHours := 1.0;                                      //QMANTWO-465
    end;                                                       //QMANTWO-465

    WorkDate := Hours.FieldByName('work_date').AsDateTime;
    HoursInvoiced := Hours.FieldByName('hours_invoiced').AsBoolean;
    Assert(RegularHours >= 0);
    Assert(OvertimeHours >= 0);

    if CenterBillsHoursDaily(Locate.CallCenter) then begin
      CanBillHoursForLocate := (not HoursInvoiced) and ConsiderWorkDate(WorkDate) and (not Locate.Invoiced);
      if Locate.Invoiced then
        LogFmt('ERROR: Can not bill hourly record for %s/%s for %s because the locate is already invoiced', [Locate.TicketNumber, Locate.ClientCode, DateToStr(WorkDate)]);
    end
    else
      CanBillHoursForLocate := (not HoursInvoiced) and Locate.Closed and ConsiderWorkDate(Locate.ClosedDate);

    if CanBillHoursForLocate then begin
      Assert(not HoursInvoiced);
      if RegularHours > 0 then begin
        AddHourlyCharge(Locate, RegularHours, TotalLocateHours, False, NoCharge);
        Inc(Result);
      end;
      if OvertimeHours > 0 then begin
        AddHourlyCharge(Locate, OvertimeHours, TotalLocateHours, True, NoCharge);
        Inc(Result);
      end;

      // This center bills a single line item for all worked hours at once,
      // rather than billing separate line items per day, since it
      // needs to know the total worked hours to determine the rate.
      if (Locate.CallCenter = 'FAM1') and Locate.WatchAndProtect then
        NoCharge := True;
    end;
    Hours.Next;
  end;
end;

function TBillingEngine.CurrentlyBilling(const CallCenter: string): Boolean;
begin
  Result := FCallCenters.IndexOf(CallCenter) > -1;
end;

function TBillingEngine.CurrentlyBilling(CallCenters: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Length(CallCenters) - 1 do begin
    if FCallCenters.IndexOf(CallCenters[i]) > -1 then begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TBillingEngine.LoadHoursUnitsData;
begin
  if not HourlyOrUnitBillingEnabled then
    Exit;
  PerfLog.Start('LoadHoursUnitsData');
  Log('Downloading hourly locate data...');
  LogSQL(BEDM.DailyHoursUnits.CommandText);
  BEDM.DailyHoursUnits.Open;
  CloneStructure(BEDM.DailyHoursUnits, BEDM.HoursUnits);
  AddHourlyIndexes;
  ClearTable(BEDM.HoursUnits);
  CopyDataFrom(BEDM.DailyHoursUnits, BEDM.HoursUnits);
  BEDM.DailyHoursUnits.Close;
end;

procedure TBillingEngine.LoadTicketNotes;
begin
  PerfLog.Start('LoadTicketNotes');
  Log('Downloading ticket notes...');
  LogSQL(BEDM.TicketNotesData.CommandText);
  BEDM.TicketNotesData.Open;
  CloneStructure(BEDM.TicketNotesData, BEDM.TicketNotes);
  CopyDataFrom(BEDM.TicketNotesData, BEDM.TicketNotes);
end;

function TBillingEngine.GetCallCenterSQLString: string;
const
  Quote = '''';
var
  i: Integer;
begin
  Result := '';
  for i := 0 to FCallCenters.Count - 1 do begin
    if Result = '' then
      Result := Quote + FCallCenters[i] + Quote
    else
      Result := Result + ', ' + Quote + FCallCenters[i] + Quote;
  end;
end;

procedure TBillingEngine.AddHourlyIndexes;
begin
  BEDM.HoursUnits.IndexDefs.Update;
  if BEDM.HoursUnits.IndexDefs.Count < 1 then
    BEDM.HoursUnits.AddIndex('locate_id', 'locate_id', []);
end;

(* Tested and ready to use, but unused now because of removed code

function TBillingEngine.TicketHasLocateFor(const ClientCode: string; Locates: TLocateArray): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Length(Locates) - 1 do begin
    if Locates[i].ClientCode = ClientCode then begin
      Result := True;
      Exit;
    end;
  end;
end;

function TBillingEngine.TicketHasLocateFor(ClientCodes: array of string; Locates: TLocateArray): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  for i := Low(Locates) to High(Locates) do begin
    for j := Low(ClientCodes) to High(ClientCodes) do begin
      if SameText(Locates[i].ClientCode, ClientCodes[j]) then begin
        Result := True;
        Exit;
      end;
    end;
  end;
end;

function TBillingEngine.GetBillableLocateCount(Locates: TLocateArray): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(Locates) to High(Locates) do begin
    if Locates[i].Price > 0.0001 then
      Inc(Result);
  end;
end;

procedure TBillingEngine.GetBilledLocatesMatching(const CallCenter, Status: string;
  var MatchingLocates: TLocateArray);
begin
  GetLocatesFromListMatching(FResultRecords, CallCenter, Status, MatchingLocates);
end;

function TBillingEngine.CountAssignedLocates(Locates: TLocateArray): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(Locates) to High(Locates) do begin
    if Assigned(Locates[i]) then
      Inc(Result);
  end;
end;

function TBillingEngine.CountLocatesWithStatus(const Status: string; Locates: TLocateArray): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(Locates) to High(Locates) do begin
    if Assigned(Locates[i]) then
      if Locates[i].Status = Status then
        Inc(Result);
  end;
end;

function TBillingEngine.GetHighestUnitValueOfStatus(Locates: TLocateArray; const Status: string): Integer;
var
  i: Integer;
  Locate: TLocate;
begin
  Result := 0;
  for i := Low(Locates) to High(Locates) do begin
    Locate := Locates[i];
    if Assigned(Locate) then
      if (Locate.QtyCharged > Result) and (Locate.Status = Status) then
        Result := Locate.QtyCharged;
  end;
end;

function TBillingEngine.GetLocateStatusCountWithAtLeastUnits(
  const Status: string; Units: Integer; Locates: TLocateArray): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(Locates) to High(Locates) do begin
    if Assigned(Locates[i]) then
      if Locates[i].Status = Status then
        if Locates[i].QtyCharged >= Units then
          Inc(Result);
  end;
end;

function TBillingEngine.AllLocatesHaveStatus(Locates: TLocateArray; const Status: string): Boolean;
var
  i: Integer;
  Locate: TLocate;
begin
  if CountAssignedLocates(Locates) = 0 then begin
    Result := False;
    Exit;
  end;

  Result := True;
  for i := Low(Locates) to High(Locates) do begin
    Locate := Locates[i];
    if Assigned(Locate) and (Locate.Status <> Status) then begin
      Result := False;
      Exit;
    end;
  end;
end;

function TBillingEngine.GetFirstAssignedLocateIndex(
  Locates: TLocateArray): Integer;
begin
  for Result := Low(Locates) to High(Locates) do begin
    if Assigned(Locates[Result]) then
      Exit;
  end;
  Result := -1;
end;

function TBillingEngine.TicketHasLocateForTerms(TicketID: Integer; const Terms: TStringArray): Boolean;
var
  TicketTerms: TStringArray;
begin
  Assert(TicketID > 0);
  FTicketClientList.GetTermList(TicketID, TicketTerms);
  Result := StringArraysHaveMatch(Terms, TicketTerms);
end;

function TBillingEngine.IsDuplicateLocateForBillingPeriod(Locate: TLocate): Boolean;
var
  Tix: TDBISAMQuery;
begin
  Assert(Assigned(Locate));
  Result := False;
  if Locate.ParentTicketID = 0 then
   Exit;

  // Look for duplicate locates with this ticket number on a different ticket ID
  Tix := BEDM.TicketQuery;
  Tix.ParamByName('TicketNumber').AsString := Locate.TicketNumber;
  Tix.ParamByName('ClientCode').AsString := Locate.ClientCode;
  Tix.ParamByName('TicketID').AsInteger := Locate.TicketID;
  Tix.Open;
  try
    Result := False;
    while not Tix.Eof do begin
      if Tix.FieldByName('ticket_id').AsInteger < Locate.TicketID then begin
        Result := True;
        Break;
      end;
      Tix.Next;
    end;
  finally
    Tix.Close;
  end;
end;
*)

procedure TBillingEngine.InitializeBilledTicketsTable;
var
  Billed: TDBISAMTable;
begin
  Billed := BEDM.BilledTickets;
  Billed.Close;
  if not Billed.Exists then
    Billed.CreateTable
  else
    Billed.EmptyTable;
  Billed.Open;
  Billed.Exclusive := False;
end;

function TBillingEngine.IsDailyHourly(Locate: TLocate): Boolean;
begin
  Result := Locate.IsHourly and UsesHourlyBilling(Locate.CallCenter) and CenterBillsHoursDaily(Locate.CallCenter);
end;

function TBillingEngine.ConsiderWorkDate(Date: TDateTime): Boolean;
begin
  Result := (Date >= Period.LookBackDate) and (Date < Period.EndDate);
end;

function TBillingEngine.CenterBillsHoursDaily(const CallCenter: string): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup('DailyHoursBilling'));
end;

function TBillingEngine.HourlyOrUnitBillingEnabled: Boolean;
begin
  Result := CurrentlyBilling(CenterGroup('CallCentersWithHourlyBilling'))
      or CurrentlyBilling(CenterGroup('CallCentersWithUnitBilling'));
end;

function TBillingEngine.UsesHourlyBilling(const CallCenter: string): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup('CallCentersWithHourlyBilling'));
end;

function TBillingEngine.UsesUnitBilling(const CallCenter: string): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup('CallCentersWithUnitBilling'));
end;

function TBillingEngine.GetCallCenterClientIDsSQLString: string;
begin
  Result := BillingConfig.GetClientListForCallCenters(StringToDelimitedString(FCallCenters.CommaText), Period.Span);
end;

procedure TBillingEngine.LogSQL(const SQL: string);
begin
  if DebugBilling then
    Log('SQL = ' + SQL);
end;

function TBillingEngine.MatchesTermAndWorkDoneForGroup(Locate: TLocate;
  const TermGroup, DoneForValueGroup: string): Boolean;
begin
  Result := MatchesTermGroup(Locate, TermGroup) and MatchesWorkDoneForGroup(Locate, DoneForValueGroup);
end;

function TBillingEngine.MatchesTermAndExcavatorGroup(Locate: TLocate;
  const TermGroup, ExcavatorValueGroup: string): Boolean;
begin
  Result := MatchesTermGroup(Locate, TermGroup) and MatchesValueGroup(Locate.Excavator, ExcavatorValueGroup);
end;

procedure TBillingEngine.CreateLocateObjectsFromDataSets(TicketData, LocateData: TDataSet);
var
  TicketID: Integer;
  Count: Integer;
begin
  Log('Creating Locate objects...');
  FreeAndNil(FLocates);
  FLocates := TObjectList.Create;
  Count := 0;
  LocateData.First;
  while not LocateData.EOF do begin
    TicketID := LocateData.FieldByName('ticket_id').AsInteger;
    if TicketData.FieldByName('ticket_id').AsInteger <> TicketID then
      if not TicketData.Locate('ticket_id', TicketID, []) then
        raise Exception.CreateFmt('Ticket data for ticket ID %d not found', [TicketID]);

    FLocates.Add(TLocate.CreateFromDataSets(TicketData, LocateData));
    Inc(Count);
    if Count mod 2000 = 0 then
      Log(IntToStr(Count) + '...');
    if Count mod 500 = 0 then
      Application.ProcessMessages;
    LocateData.Next;
  end;
end;

procedure TBillingEngine.ApplySalesTax(Loc: TLocate);
var
  TaxResult: TTaxResult;
begin
  if BillingConfig.LookupSalesTax(Loc, TaxResult) then begin
    Loc.TaxName := TaxResult.TaxName;
    Loc.TaxRate := TaxResult.TaxRate;
    Loc.TaxAmount := RoundTo(Loc.TaxRate * Loc.Price * 0.01, -2);
  end;
end;

function TBillingEngine.BillingRunName: string;
begin
  Result := FDescription + ' ' + Period.Span + ' Ending ' +
    DateToStr(Period.EndDate);
end;

procedure TBillingEngine.SetBillingTimeout(Timeout: Integer);
begin
  BEDM.SetBillingTimeout(Timeout);
end;

function TBillingEngine.EligibleTerm(Loc: TLocate): Boolean;
begin
  Result := FEligibleTerms.IndexOf(Loc.ClientCode) >= 0;
end;

procedure TBillingEngine.AddSplitLineItems;
const
  HPTerms = 'select distinct call_center, billing_cc from billing_rate where bill_type = ''HIGH'' and call_center in (%s)';
var
  SQL: string;
  Terms: TDataSet;
  CallCenter: string;
  ClientCode: string;
  HPLocates: TLocateArray;
  QtyLocates: TLocateArray;
  OSLocates: TLocateArray;
  i: Integer;
begin
  PerfLog.Start('AddSplitLineItems');
  Log('Adding split high profile charges...');
  SQL := Format(HPTerms, [GetCallCenterSQLString]);
  Terms := QMBillingDM.ConfigDM.CreateDataSetForSQL(SQL);
  try
    while not Terms.Eof do begin
      CallCenter := Terms.FieldByName('call_center').AsString;
      ClientCode := Terms.FieldByName('billing_cc').AsString;
      GetBilledHPLocatesMatching(CallCenter, ClientCode, HPLocates);
      for i := Low(HPLocates) to High(HPLocates) do
      begin
        if not StrContains('Duplicate',HPLocates[i].Bucket) then  //qm-575 do not rate if dup
        AddSplitHPCharge(HPLocates[i]);
      end;
      Terms.Next;
    end;
  finally
    FreeAndNil(Terms);
  end;

  GetBilledAditionalQtyLocatesMatching(QtyLocates);
  if Length(QtyLocates) > 0 then begin
    Log('Adding split additional quantity charges...');
    for i := Low(QtyLocates) to High(QtyLocates) do
    begin
     if not StrContains('Duplicate',QtyLocates[i].Bucket) then  //qm-575 do not rate if dup
     AddAdditionalQtyCharge(QtyLocates[i]);
    end;
    LogFmt('Added %d split additional quantity charges', [Length(QtyLocates)]);

  end;

  // Some California terms get separate OS charges for transmissions > 1
  if CurrentlyBilling(CenterGroup('CACallCenters', False)) then begin
    Log('Adding California transmission screen charges...');
    GetBilledLocatesMatching(CenterGroup('CACallCenters'), TermGroup('CaliforniaScreenClients'), OSLocates);
    for i := Low(OSLocates) to High(OSLocates) do
      AddOSTransmissionCharge(OSLocates[i]);
  end;
end;

procedure TBillingEngine.AddSplitHPCharge(HPLocate: TLocate);
var
  FoundRate: TRateEntry;
  SplitLocate: TLocate;
begin
  Assert(Assigned(HPLocate));
  Assert(HPLocate.HighProfile);
  if HPLocate.Status = 'NC' then
    Exit;
  try
    FoundRate := BillingConfig.GetRateTable.LookupRate(HPLocate, HPLocate.Parties, ['HIGH']);
  except
    LogErrorFmt('ERROR: No rate for high profile locate %s', [GetLocateIdentifier(HPLocate)]);
    Exit;
  end;
  if FoundRate.Rate > 0 then begin
    SplitLocate := HPLocate.CreateExtraChargeCopy;
    SplitLocate.Bucket := 'High Profile';
    SplitLocate.LineItemText := 'High Profile';
    SplitLocate.Rate := FoundRate.Rate;
    SplitLocate.Price := FoundRate.Rate; // This is a flat fee, not based on qty_charged
    if SplitLocate.Status = 'H' then
      SplitLocate.Status := 'HP'; // Make sure the Hourly code does not find these...
    SetBillCode(SplitLocate, BillCodeHP);
    FResultRecords.Add(SplitLocate);
  end;
end;

procedure TBillingEngine.AddAdditionalQtyCharge(QtyLocate: TLocate);
var
  SplitLocate: TLocate;
  Suffix: string;
begin
  Assert(Assigned(QtyLocate));
  Assert(QtyLocate.AdditionalQty > 0);
  Assert(QtyLocate.AdditionalQtyRate > 0);
  SplitLocate := QtyLocate.CreateExtraChargeCopy;
  if IsEmpty(SplitLocate.AdditionalLineItemText) then begin
    Suffix := Format(' (AU %0.2f)', [SplitLocate.AdditionalQtyRate]);
    SplitLocate.LineItemText := SplitLocate.LineItemText + ' (AU)';
  end
  else begin
    Suffix := Format(' (%s %0.2f)', [SplitLocate.AdditionalLineItemText, SplitLocate.AdditionalQtyRate]);
    SplitLocate.LineItemText := SplitLocate.AdditionalLineItemText;
  end;
  SplitLocate.Bucket := SplitLocate.Bucket + Suffix;
  SplitLocate.Price := SplitLocate.AdditionalQtyRate * SplitLocate.AdditionalQty;
  SplitLocate.Rate := SplitLocate.AdditionalQtyRate;
  SplitLocate.QtyCharged := SplitLocate.AdditionalQty;
  SplitLocate.AdditionalQty := 0;
  SetBillCode(SplitLocate, BillCodeAdditionalQty);
  QtyLocate.QtyCharged := 1;
  QtyLocate.Price := QtyLocate.Rate;
  FResultRecords.Add(SplitLocate);
end;

procedure TBillingEngine.AddOSTransmissionCharge(Locate: TLocate);
var
  OSLocate: TLocate;
  RateEntry: TRateEntry;
begin
  Assert(Assigned(Locate));
  Assert(Locate.ClientCode <> '');
  Assert(not Locate.IsExtraCharge);
  if Locate.TransmitCount < 2 then
    Exit;
  if (StrBeginsWith('SCA', Locate.CallCenter) and Locate.IsFromCallCenter) or
     (StrBeginsWith('NCA', Locate.CallCenter) and Locate.IsFromClient) then begin
    OSLocate := Locate.CreateExtraChargeCopy;
    OSLocate.Status := 'OS';
    try
      RateEntry := BillingConfig.GetRateTable.LookupRate(OSLocate, Locate.Parties, [Locate.BillingTicketType, TicketTypeNormal]);
    except
      on E: Exception do begin
        LogErrorFmt('Error getting rate for locate %s: %s', [GetLocateIdentifier(Locate), E.Message]);
        Exit;
      end;
    end;
    OSLocate.Bucket := 'Office Screen Transmission Charge';
    OSLocate.LineItemText := OSLocate.Bucket;
    if Trim(RateEntry.LineItemText) <> '' then
      OSLocate.LineItemText := RateEntry.LineItemText;
    OSLocate.Rate := RateEntry.Rate;
    OSLocate.Price := OSLocate.Rate * (Locate.TransmitCount - 1);
    OSLocate.WhichInvoice := RateEntry.WhichInvoice;
    OSLocate.RateValueGroupID := RateEntry.ValueGroupID;
    OSLocate.QtyCharged := Locate.TransmitCount - 1;
    SetBillCode(OSLocate, BillCodeOSTrans);
    FResultRecords.Add(OSLocate);
  end;
end;

procedure TBillingEngine.WarnForExtraChargeLocates(Locate: TLocate);
begin
  Assert(Assigned(Locate));
  if Locate.IsExtraCharge then
    Log('Warning: Extra charge locate found: ' + GetLocateIdentifier(Locate) + ': ' + Locate.Bucket);
end;

function TBillingEngine.ConvertToBillingUnits(const UnitsMarked, FirstUnitFactor, RestUnitsFactor: Integer): Integer;
var
  RemainingUnits: Integer;
  BillingUnits: Integer;
begin
  Assert(FirstUnitFactor >= 0);
  Assert(RestUnitsFactor >= 0);

  if FirstUnitFactor = 0 then
    // There is no conversion to do
    BillingUnits := UnitsMarked
  else if (RestUnitsFactor = 0) or (UnitsMarked <= FirstUnitFactor) or (RestUnitsFactor = FirstUnitFactor) then begin
    // All units are converted using the same factor
    BillingUnits := Trunc(UnitsMarked / FirstUnitFactor);
    if UnitsMarked mod FirstUnitFactor > 0 then
      BillingUnits := BillingUnits + 1;
  end
  else begin
    // The first unit is at one factor, and the rest at a different factor
    RemainingUnits := UnitsMarked - FirstUnitFactor;
    BillingUnits := Trunc(RemainingUnits / RestUnitsFactor) + 1;
    if RemainingUnits mod RestUnitsFactor > 0 then
      BillingUnits := BillingUnits + 1;
  end;
  Result := BillingUnits;
end;

procedure TBillingEngine.StartPerformanceLog;
begin
  BEDM.PerfLogData.Open;
  PerfLog.DataSet := BEDM.PerfLogData;
  PerfLog.Start('Initialize');
end;

procedure TBillingEngine.StopPerformanceLog;
begin
  PerfLog.BillingRunID := FBillID;
  PerfLog.CenterGroupID := FCallCenterGroupID;
  if PerfLog.BillingRunID > 0 then begin
    Log('Writing billing performance data to the database...');
    PerfLog.Stop;
  end;
end;

function TBillingEngine.TicketHasLocate(const TicketID: Integer;
  const ClientCode: string): Boolean;
begin
  Assert(TicketID > 0, 'Invalid TicketID');
  Assert(NotEmpty(ClientCode), 'Missing ClientCode');

  Result := FTicketClientList.Has(TicketID, ClientCode);
  if not Result then begin
    BEDM.LocateLookupData.Parameters.ParamValues['ticket_id'] := TicketID;
    BEDM.LocateLookupData.Parameters.ParamValues['client_code'] := ClientCode;
    BEDM.LocateLookupData.Open;
    try
      Result := HasRecords(BEDM.LocateLookupData);
    finally
      CloseDataSet(BEDM.LocateLookupData);
    end;
  end;
end;

{ TLocatesByTicket }

constructor TLocatesByTicket.Create(AllLocates: TObjectList; CallCenters: array of string);
var
  i: Integer;
  AddLocate: TLocate;
  TicketID: string;
  TicketIndex: Integer;
  TicketLocates: TObjectList;
  GetAll: Boolean;
begin
  inherited Create;
  Assert(Assigned(AllLocates) and (Length(CallCenters) > 0));
  Self.Clear;
  Self.Sorted := True;
  GetAll := StringInArray('*', CallCenters);
  for i := 0 to AllLocates.Count - 1 do begin
    AddLocate := AllLocates[i] as TLocate;
    if GetAll or StringInArray(AddLocate.CallCenter, CallCenters) then begin
      TicketID := IntToStr(AddLocate.TicketID);
      TicketIndex := IndexOf(TicketID);
      if TicketIndex = -1 then begin
        TicketLocates := TObjectList.Create(False);
        TicketLocates.Add(AddLocate);
        AddObject(TicketID, TicketLocates);
      end
      else
        (Objects[TicketIndex] as TObjectList).Add(AddLocate);
    end;
  end;
end;

destructor TLocatesByTicket.Destroy;
begin
  FreeStringListObjects(Self);
  inherited;
end;

procedure ObjectListToLocateArray(Objects: TObjectList; var Locates: TLocateArray);
var
  i: Integer;
begin
  Assert(Assigned(Objects));
  SetLength(Locates, Objects.Count);
  for i := 0 to Objects.Count - 1 do begin
    Locates[i] := Objects[i] as TLocate;
  end;
end;

procedure TLocatesByTicket.GetTicket(TicketID: Integer; var Locates: TLocateArray);
var
  i: Integer;
begin
  SetLength(Locates, 0);
  i := IndexOf(IntToStr(TicketID));
  if i > -1 then
    ObjectListToLocateArray(Objects[i] as TObjectList, Locates);
end;

procedure TLocatesByTicket.GetTicketByIndex(Index: Integer; var Locates: TLocateArray);
begin
  ObjectListToLocateArray(Objects[Index] as TObjectList, Locates);
end;

function TBillingEngine.DebugBilling: Boolean;
begin
  Result := QMBillingDM.GetIniSetting('Billing', 'Debug', '0') = '1';
end;

end.
