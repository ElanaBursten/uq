unit ScciList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList,DB, ADODB, ExtCtrls, StdCtrls, cxStyles, QMBillingDMu,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxDBLookupComboBox, cxMaskEdit, cxTextEdit, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  dxSkinsCore, cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges, dxScrollbarAnnotations;

type
  TScciForm = class(TBaseCxListForm)
    Label1: TLabel;
    Customer: TADODataSet;
    CustomerDS: TDataSource;
    ColCustomerLookup: TcxGridDBColumn;
    ColState: TcxGridDBColumn;
    ColCounty: TcxGridDBColumn;
    ColCity: TcxGridDBColumn;
    ColWhichInvoice: TcxGridDBColumn;
    ColSubcategory: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColSCCIId: TcxGridDBColumn;
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataBeforeOpen(DataSet: TDataSet);
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
  end;

implementation

{$R *.dfm}

uses
  OdDbUtils;

procedure TScciForm.CloseDataSets;
begin
  inherited;
  Customer.Close;
end;

procedure TScciForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TScciForm.OpenDataSets;
begin
  inherited;
  Customer.Open;
end;

procedure TScciForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('CustomerLookup', DataSet, 'customer_id', Customer,
    'customer_id', 'cname');
  DataSet.FieldByName('CustomerLookup').LookupCache := True;
end;

end.
