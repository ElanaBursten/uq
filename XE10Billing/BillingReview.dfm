inherited BillingReviewForm: TBillingReviewForm
  Left = 282
  Top = 194
  Caption = 'Review Billing'
  ClientHeight = 499
  ClientWidth = 922
  Font.Charset = ANSI_CHARSET
  OnShow = FormShow
  TextHeight = 13
  object TermsSplitter: TSplitter
    Left = 0
    Top = 339
    Width = 922
    Height = 6
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
  end
  object TermsPanel: TPanel
    Left = 0
    Top = 345
    Width = 922
    Height = 154
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object TermsHeaderPanel: TPanel
      Left = 0
      Top = 0
      Width = 922
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      FullRepaint = False
      TabOrder = 0
      object TermsLabel: TLabel
        Left = 8
        Top = 9
        Width = 97
        Height = 13
        Caption = 'Term IDs on this bill:'
      end
      object ShowTermIds: TCheckBox
        Left = 160
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Show'
        TabOrder = 0
        OnClick = ShowTermIdsClick
      end
    end
    object DetailGrid: TcxGrid
      Left = 0
      Top = 33
      Width = 922
      Height = 121
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object DetailGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = ClientsOnBillDS
        DataController.Filter.MaxValueListCount = 1000
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsData.Editing = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.NoDataToDisplayInfoText = '<Select a billing run and check Show for customer details>'
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object CustomerNameColumn: TcxGridDBColumn
          Caption = 'Customer'
          DataBinding.FieldName = 'customer_name'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 186
        end
        object BillingCCColumn: TcxGridDBColumn
          Caption = 'Term ID'
          DataBinding.FieldName = 'billing_cc'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Editing = False
          Options.Filtering = False
          Width = 106
        end
        object WhichInvoiceColumn: TcxGridDBColumn
          Caption = 'Which Invoice'
          DataBinding.FieldName = 'which_invoice'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 102
        end
        object LocateCountColumn: TcxGridDBColumn
          Caption = '# Locates'
          DataBinding.FieldName = 'count'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Editing = False
          Options.Filtering = False
          Width = 88
        end
      end
      object DetailGridLevel: TcxGridLevel
        GridView = DetailGridView
      end
    end
  end
  object HeaderGrid: TcxGrid
    Left = 0
    Top = 41
    Width = 922
    Height = 298
    Align = alClient
    PopupMenu = GridMenu
    TabOrder = 1
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object HeaderGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = HeaderDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'bill_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object BillIDColumn: TcxGridDBColumn
        Caption = 'Bill ID'
        DataBinding.FieldName = 'bill_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 40
      end
      object CommittedColumn: TcxGridDBColumn
        Caption = 'Committed'
        DataBinding.FieldName = 'committed'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Width = 56
      end
      object DescriptionColumn: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 120
      end
      object GroupCodeColumn: TcxGridDBColumn
        Caption = 'Center Group'
        DataBinding.FieldName = 'group_code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 88
      end
      object PeriodTypeColumn: TcxGridDBColumn
        Caption = 'Span'
        DataBinding.FieldName = 'period_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 56
      end
      object BillStartDateColumn: TcxGridDBColumn
        Caption = 'Start Date'
        DataBinding.FieldName = 'bill_start_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Editing = False
        Options.Filtering = False
        Width = 66
      end
      object BillEndDateColumn: TcxGridDBColumn
        Caption = 'End Date'
        DataBinding.FieldName = 'bill_end_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Editing = False
        Options.Filtering = False
        Width = 67
      end
      object RunDateColumn: TcxGridDBColumn
        Caption = 'Run Date'
        DataBinding.FieldName = 'bill_run_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Editing = False
        Options.Filtering = False
        Width = 121
      end
      object CommittedDateColumn: TcxGridDBColumn
        Caption = 'Committed Date'
        DataBinding.FieldName = 'committed_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Width = 121
      end
      object AppVersionColumn: TcxGridDBColumn
        Caption = 'Version'
        DataBinding.FieldName = 'app_version'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 52
      end
      object NTicketsColumn: TcxGridDBColumn
        Caption = '# Tix'
        DataBinding.FieldName = 'n_tickets'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 46
      end
      object NLocatesColumn: TcxGridDBColumn
        Caption = '# Loc'
        DataBinding.FieldName = 'n_locates'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 44
      end
      object TotalAmountColumn: TcxGridDBColumn
        Caption = 'Total $'
        DataBinding.FieldName = 'totalamount'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 78
      end
    end
    object HeaderGridLevel: TcxGridLevel
      GridView = HeaderGridView
    end
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 922
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 354
      Top = 14
      Width = 84
      Height = 13
      Caption = 'Filter Description:'
    end
    object Label2: TLabel
      Left = 636
      Top = 14
      Width = 106
      Height = 13
      Caption = 'Override Export Dest:'
    end
    object Label3: TLabel
      Left = 518
      Top = 14
      Width = 61
      Height = 13
      Caption = 'Weeks Back:'
    end
    object CommitButton: TButton
      Left = 9
      Top = 8
      Width = 75
      Height = 25
      Action = CommitBillingAction
      TabOrder = 0
    end
    object DeleteBillingButton: TButton
      Left = 178
      Top = 8
      Width = 75
      Height = 25
      Action = DeleteBillingAction
      TabOrder = 2
    end
    object UnCommitBillingButton: TButton
      Left = 94
      Top = 8
      Width = 75
      Height = 25
      Action = UncommitBillingAction
      TabOrder = 1
    end
    object ReExportButton: TButton
      Left = 262
      Top = 8
      Width = 75
      Height = 25
      Action = ReExportBillingAction
      TabOrder = 3
    end
    object EditFilter: TEdit
      Left = 442
      Top = 10
      Width = 65
      Height = 21
      TabOrder = 4
      OnChange = EditFilterChange
    end
    object EditDest: TEdit
      Left = 746
      Top = 10
      Width = 175
      Height = 21
      TabOrder = 6
    end
    object EditWeeks: TEdit
      Left = 584
      Top = 10
      Width = 42
      Height = 21
      TabOrder = 5
      Text = '8'
    end
  end
  object Header: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    BeforeOpen = HeaderBeforeOpen
    AfterScroll = HeaderAfterScroll
    CommandText = 
      'select bh.*, cg.group_code'#13#10' from billing_header bh'#13#10'  left join' +
      ' center_group cg on bh.center_group_id=cg.center_group_id'#13#10' wher' +
      'e bill_run_date>dateadd(d, :days, getdate()) order by bill_id de' +
      'sc'#13#10
    CommandTimeout = 100
    Parameters = <
      item
        Name = 'days'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 196
      end>
    Left = 376
    Top = 136
  end
  object HeaderDS: TDataSource
    DataSet = Header
    Left = 416
    Top = 136
  end
  object DeleteBilling: TADOStoredProc
    Connection = QMBillingDM.Conn
    CommandTimeout = 120
    ProcedureName = 'delete_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 136
  end
  object ClientsOnBill: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select cu.customer_name, bd.billing_cc, bd.which_invoice, count(' +
      '*) as count'#13#10' from billing_detail bd'#13#10'   left join client c on b' +
      'd.client_id=c.client_id'#13#10'   left join customer cu on c.customer_' +
      'id=cu.customer_id'#13#10'where bill_id=:bill_id'#13#10'  and ((area_name <> ' +
      #39'DEL_GAS'#39') or (area_name is null)) group by cu.customer_name, bi' +
      'lling_cc, bd.which_invoice'#13#10'order by cu.customer_name, billing_c' +
      'c, bd.which_invoice'#13#10
    CommandTimeout = 100
    DataSource = HeaderDS
    Parameters = <
      item
        Name = 'bill_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 376
    Top = 192
  end
  object ClientsOnBillDS: TDataSource
    DataSet = ClientsOnBill
    Left = 432
    Top = 192
  end
  object MarkInvoiced: TADOQuery
    Connection = QMBillingDM.Conn
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'BillID1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'BillID2'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'BillID3'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'set nocount on'
      ''
      'update locate'
      'set locate.invoiced=1'
      'from billing_detail bd'
      'where'
      '  (bd.bill_id = :BillID1)'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and (locate.locate_id = bd.locate_id)'
      '  and ((bd.locate_hours_id is null) or (bd.locate_hours_id = 0))'
      ''
      'update locate_hours'
      'set locate_hours.hours_invoiced=1'
      'from billing_detail bd'
      'where'
      '  (bd.bill_id = :BillID2)'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and (locate_hours.locate_hours_id = bd.locate_hours_id)'
      '  and (bd.locate_hours_id > 0)'
      '  and (bd.bill_code  = '#39'HOURLY'#39')'
      ''
      'update locate_hours'
      'set locate_hours.units_invoiced=1'
      'from billing_detail bd'
      'where'
      '  (bd.bill_id = :BillID3)'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and (locate_hours.locate_hours_id = bd.locate_hours_id)'
      '  and (bd.locate_hours_id > 0)'
      '  and (bd.bill_code  = '#39'UNITS'#39')')
    Left = 336
    Top = 136
  end
  object MarkNotInvoiced: TADOQuery
    Connection = QMBillingDM.Conn
    CommandTimeout = 100
    Parameters = <
      item
        Name = 'BillID1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'BillID2'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'BillID3'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'set nocount on'
      ''
      'update locate'
      'set invoiced=0'
      'from billing_detail bd'
      'where'
      ' locate.locate_id = bd.locate_id'
      '  and bd.bill_id = :BillID1'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and ((locate_hours_id is null) or (locate_hours_id = 0))'
      ''
      'update locate_hours'
      'set locate_hours.hours_invoiced=0'
      'from billing_detail bd'
      'where'
      '  (bd.bill_id = :BillID2)'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and (locate_hours.locate_hours_id = bd.locate_hours_id)'
      '  and (bd.locate_hours_id > 0)'
      '  and (bd.bill_code  = '#39'HOURLY'#39')'
      ''
      'update locate_hours'
      'set locate_hours.units_invoiced=0'
      'from billing_detail bd'
      'where'
      '  (bd.bill_id = :BillID3)'
      '  and ((area_name <>  '#39'DEL_GAS '#39')or (area_name is null)) '
      '  and (locate_hours.locate_hours_id = bd.locate_hours_id)'
      '  and (bd.locate_hours_id > 0)'
      '  and (bd.bill_code  = '#39'UNITS'#39')')
    Left = 332
    Top = 176
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 208
    Top = 152
    object CommitBillingAction: TAction
      Caption = 'Commit'
      OnExecute = CommitBillingActionExecute
    end
    object UncommitBillingAction: TAction
      Caption = 'Un-Commit'
      OnExecute = UnCommitBillingActionExecute
    end
    object DeleteBillingAction: TAction
      Caption = 'Delete'
      OnExecute = DeleteBillingActionExecute
    end
    object ReExportBillingAction: TAction
      Caption = 'Re-Export'
      OnExecute = ReExportBillingActionExecute
    end
    object AnalyzeBillingAction: TAction
      Caption = 'Analyze'
      OnExecute = AnalyzeBillingActionExecute
    end
  end
  object GridMenu: TPopupMenu
    Left = 208
    Top = 208
    object Commit1: TMenuItem
      Action = CommitBillingAction
    end
    object UnCommit1: TMenuItem
      Action = UncommitBillingAction
    end
    object Delete1: TMenuItem
      Action = DeleteBillingAction
    end
    object ReExport1: TMenuItem
      Action = ReExportBillingAction
    end
    object Analyze1: TMenuItem
      Action = AnalyzeBillingAction
    end
  end
end
