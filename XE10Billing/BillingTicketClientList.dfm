object TicketClientList: TTicketClientList
  OnCreate = DataModuleCreate
  Height = 150
  Width = 215
  object Tickets: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.50 Build 5'
    Exclusive = True
    TableName = 'tickets'
    StoreDefs = True
    Left = 36
    Top = 12
    object TicketsTicketID: TIntegerField
      FieldName = 'ticket_id'
    end
    object TicketsClientCode: TStringField
      FieldName = 'client_code'
    end
    object TicketsStatus: TStringField
      FieldName = 'status'
    end
    object TicketsLocatorID: TIntegerField
      FieldName = 'locator_id'
    end
    object TicketsRefID: TIntegerField
      FieldName = 'Ref_id'
    end
  end
  object LocatorIDQuery: TDBISAMQuery
    EngineVersion = '4.50 Build 5'
    SQL.Strings = (
      'select *'
      'from "\Memory\tickets"'
      'where (ticket_id = :TicketID)'
      '  and (client_code = :ClientCode)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end
      item
        DataType = ftUnknown
        Name = 'ClientCode'
      end>
    Left = 104
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end
      item
        DataType = ftUnknown
        Name = 'ClientCode'
      end>
  end
end
