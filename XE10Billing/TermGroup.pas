unit TermGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMasterDetail, DB, ADODB, ExtCtrls, QMBillingDMu, ActnList, StdCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxMaskEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, cxDropDownEdit,
  cxTextEdit, cxDBExtLookupComboBox, cxLookAndFeels, cxLookAndFeelPainters,
  cxNavigator, System.Actions, dxSkinsCore,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges, dxScrollbarAnnotations;

type
  TTermGroupForm = class(TBaseMasterDetailForm)
    CenterGroups: TADODataSet;
    CenterGroupSource: TDataSource;
    ColTermGroupID: TcxGridDBColumn;
    ColGroupCode: TcxGridDBColumn;
    ColCenterGroupID: TcxGridDBColumn;
    ColCenterGroup: TcxGridDBColumn;
    ColComment: TcxGridDBColumn;
    CenterGroupLookupView: TcxGridDBTableView;
    CenterGroupLookupViewgroup_code: TcxGridDBColumn;
    CenterGroupLookupViewcomment: TcxGridDBColumn;
    ColDetailTermGroupID: TcxGridDBColumn;
    ColDetailTerm: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);

  public
    procedure PopulateDropdowns; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
  end;

implementation

uses
  OdDbUtils, OdCxUtils;

{$R *.dfm}

procedure TTermGroupForm.CloseDataSets;
begin
  inherited;
  Master.Close;
  CenterGroups.Close;
  Detail.Close;
end;

procedure TTermGroupForm.FormCreate(Sender: TObject);
begin
  inherited;

  Master.Connection:= QMBillingDM.Conn;
  CenterGroups.Connection := QMBillingDM.Conn;
  Detail.Connection := QMBillingDM.Conn;
  AddLookupField('center_group', Master, 'center_group_id',
    CenterGroups, 'center_group_id', 'group_code', 20, 'CenterGroupCodeField');
    Master.FieldByName('center_group').LookupCache := True;
end;

procedure TTermGroupForm.OpenDataSets;
begin
  inherited;
  CenterGroups.Open;
end;

procedure TTermGroupForm.PopulateDropdowns;
begin
  inherited;
  QMBillingDM.GetClientList(CxComboBoxItems(ColDetailTerm));
end;

end.
