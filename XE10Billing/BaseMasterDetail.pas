unit BaseMasterDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ExtCtrls, DB, ADODB, ActnList, StdCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, System.Actions, dxSkinsCore,AdminDMu, dxDateRanges, dxScrollbarAnnotations;

type
  TBaseMasterDetailForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    MasterPanel: TPanel;
    DetailPanel: TPanel;
    Master: TADODataSet;
    Detail: TADODataSet;
    MasterSource: TDataSource;
    DetailSource: TDataSource;
    MasterNewButton: TButton;
    MasterDeleteButton: TButton;
    DetailDeleteButton: TButton;
    DetailNewButton: TButton;
    ActionList: TActionList;
    MasterNewAction: TAction;
    MasterDeleteAction: TAction;
    DetailNewAction: TAction;
    DetailDeleteAction: TAction;
    Splitter1: TSplitter;
    DetailGrid: TcxGrid;
    MasterGrid: TcxGrid;
    MasterGridLevel: TcxGridLevel;
    MasterGridView: TcxGridDBTableView;
    DetailGridLevel: TcxGridLevel;
    DetailGridView: TcxGridDBTableView;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MasterGridExit(Sender: TObject);
    procedure MasterNewActionExecute(Sender: TObject);
    procedure MasterDeleteActionExecute(Sender: TObject);
    procedure DetailNewActionExecute(Sender: TObject);
    procedure DetailDeleteActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
  protected
    CanDelete: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    constructor Create(AOwner: TComponent); override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure UpdateDeleteActions;
  end;

implementation

uses
  OdMiscUtils, OdDbUtils;

{$R *.dfm}

procedure TBaseMasterDetailForm.CloseDataSets;
begin
  inherited;
  Master.Close;
  Detail.Close;
end;

procedure TBaseMasterDetailForm.FormCreate(Sender: TObject);
begin
  inherited;
  if HeaderPanel.ControlCount < 1 then
    HeaderPanel.Visible := False;
  if (Master.CommandText = '') or (Detail.CommandText = '') then
    raise Exception.Create('The Master/Detail datasets need some SQL');
  if Detail.MasterFields = '' then
    raise Exception.Create('Link the Detail.MasterFields to the Master dataset');
end;

procedure TBaseMasterDetailForm.OpenDataSets;
begin
  inherited;
  // QMANTWO- 548 EB EOleException errors here happen inside the IDE with lookup fields on the dataset
  // Turn on TField.LookupCache or ignore EOleException in the Debugger options to work around this
  // Should keep LookupCache set to true for any lookup field
  Master.Open;
  Detail.Open;
  if (Master.FieldCount > 0) then
    if StrEndsWith('_id', Master.Fields[0].FieldName) then
      Master.Fields[0].Required := False;
end;

procedure TBaseMasterDetailForm.FormShow(Sender: TObject);
begin
  inherited;
  RefreshNow;
end;

procedure TBaseMasterDetailForm.MasterGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(Master);
end;

procedure TBaseMasterDetailForm.MasterNewActionExecute(Sender: TObject);
begin
  SetReadOnly(False);
  Master.Insert;
  if MasterGrid.CanFocus then
    MasterGrid.SetFocus;
end;

procedure TBaseMasterDetailForm.MasterDeleteActionExecute(Sender: TObject);
begin
  SetReadOnly(False);
  if MasterGrid.CanFocus then
    MasterGrid.SetFocus;
  while not Detail.IsEmpty do
    Detail.Delete;
  if not Master.IsEmpty then
    Master.Delete;
end;

procedure TBaseMasterDetailForm.DetailNewActionExecute(Sender: TObject);
begin
  SetReadOnly(False);
  Detail.Insert;
  if DetailGrid.CanFocus then
    DetailGrid.SetFocus;
end;

procedure TBaseMasterDetailForm.DetailDeleteActionExecute(Sender: TObject);
begin
  SetReadOnly(False);
  if DetailGrid.CanFocus then
    DetailGrid.SetFocus;
  if not Detail.IsEmpty then
    Detail.Delete;
end;

procedure TBaseMasterDetailForm.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  UpdateDeleteActions;
end;

constructor TBaseMasterDetailForm.Create(AOwner: TComponent);
begin
  CanDelete := True;
  inherited;
end;

procedure TBaseMasterDetailForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TBaseMasterDetailForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  MasterNewButton.Enabled := True;
  DetailNewButton.Enabled := True;
  UpdateDeleteActions;
end;

procedure TBaseMasterDetailForm.UpdateDeleteActions;
begin
  MasterDeleteAction.Enabled := CanDelete and HasRecords(Master) and (not InsertingDataSet(Master));
  DetailDeleteAction.Enabled := CanDelete and HasRecords(Detail) and (not InsertingDataSet(Detail));
  MasterDeleteButton.Enabled := MasterDeleteAction.Enabled;
  DetailDeleteButton.Enabled := DetailDeleteAction.Enabled;
end;

end.
