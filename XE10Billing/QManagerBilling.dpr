program QManagerBilling;  //use this one  20322 base
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R 'QManagerBillingResources.res' 'QManagerBillingResources.rc'}
{$R '..\QMIcon.res'}
uses
  Windows,
  Forms,
  Dialogs,
  BaseCxList in '..\admin\BaseCxList.pas' {BaseCxListForm},
  BaseList in '..\admin\BaseList.pas' {BaseListForm},
  LoginFU in 'LoginFU.pas' {LoginForm},
  OdHourglass in '..\common\OdHourglass.pas',
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdEmbeddable in '..\common\OdEmbeddable.pas' {EmbeddableForm},
  MainFU in 'MainFU.pas' {MainForm},
  OdIsoDates in '..\common\OdIsoDates.pas',
  BillingEngine in 'BillingEngine.pas',
  BillingEngineDMu in 'BillingEngineDMu.pas' {BillingEngineDM: TDataModule},
  BillingRunnerMain in 'BillingRunnerMain.pas' {BillingRunnerMainForm},
  BillingLocate in 'BillingLocate.pas',
  OdDbUtils in '..\common\OdDbUtils.pas',
  RateSetup in 'RateSetup.pas' {RateSetupForm},
  BillingReview in 'BillingReview.pas' {BillingReviewForm},
  BillingTicketClientList in 'BillingTicketClientList.pas' {TicketClientList: TDataModule},
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  BillingConst in 'BillingConst.pas',
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  OdContainer in '..\common\OdContainer.pas',
  OdDBISAMUtils in '..\common\OdDBISAMUtils.pas',
  BillingRates in 'BillingRates.pas' {BillingRateTable: TDataModule},
  TermGroup in 'TermGroup.pas' {TermGroupForm},
  ValueGroup in 'ValueGroup.pas' {ValueGroupForm},
  RealTimeBilling in 'RealTimeBilling.pas' {RealTimeBillingForm: TDataModule},
  BillingCalc in 'BillingCalc.pas' {BillingCalcDM: TDataModule},
  BillingShared in 'BillingShared.pas',
  BillingOutput in 'BillingOutput.pas' {BillingOutputForm},
  BillingInvoiceFormats in 'BillingInvoiceFormats.pas' {BillingInvoiceFormatsForm},
  IncrementalBilling in 'IncrementalBilling.pas',
  BillingExportDMu in 'BillingExportDMu.pas' {BillingExportDM: TDataModule},
  RawDataIO in 'RawDataIO.pas',
  CLBLogger in 'CLBLogger.pas',
  CommandLineBilling in 'CommandLineBilling.pas',
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  BillingConfiguration in 'BillingConfiguration.pas' {BillingConfig: TDataModule},
  BillingUnitConversion in 'BillingUnitConversion.pas' {BillingUnitConversionForm},
  ScciList in 'ScciList.pas' {ScciForm},
  ScciTaxList in 'ScciTaxList.pas' {ScciTaxForm},
  SolomonExportDMu in 'SolomonExportDMu.pas' {SolomonExportDM: TDataModule},
  BillPeriod in 'BillPeriod.pas',
  ReportConfig in '..\reportengine\ReportConfig.pas',
  BillingCommitDMu in 'BillingCommitDMu.pas' {BillingCommitDM: TDataModule},
  BillingDMu in 'BillingDMu.pas' {BillingDM: TDataModule},
  OdPerfLog in '..\common\OdPerfLog.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  Terminology in '..\common\Terminology.pas',
  PasswordRules in '..\common\PasswordRules.pas',
  BillingGL in 'BillingGL.pas' {BillingGLForm},
  OdCxUtils in '..\common\OdCxUtils.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  Hashes in '..\thirdparty\Hashes.pas',
  BillingFormDefs in 'BillingFormDefs.pas',
  QMBillingDMu in 'QMBillingDMu.pas' {QMBillingDM: TDataModule},
  ConfigDMu in '..\admin\ConfigDMu.pas' {ConfigDM: TDataModule},
  CallCenterGroup in 'CallCenterGroup.pas' {CallCenterGroupForm},
  UQDbConfig in '..\common\UQDbConfig.pas',
  CodeLookupList in '..\common\CodeLookupList.pas',
  QMConst in '..\client\QMConst.pas',
  BaseMasterDetail in 'BaseMasterDetail.pas' {BaseMasterDetailForm},
  BillingAnalysis2 in 'BillingAnalysis2.pas' {frmBillAnalysis2},
  AdminDMu in '..\admin\AdminDMu.pas',
  uADSI in '..\admin\uADSI.pas',
  ActiveDs_TLB in '..\admin\ActiveDs_TLB.pas',
  ADSHLP in '..\admin\ADSHLP.pas',
  BillingCCMap in 'BillingCCMap.pas' {BillingCCMapForm};

begin
  if ParamCount > 0 then
    ConsoleHackBillingDispatch
  else begin
    Application.Initialize;
    Application.Title := 'Q Manager Billing';
    Application.CreateForm(TQMBillingDM, QMBillingDM);
  if LogIn then
      Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  end;
end.

