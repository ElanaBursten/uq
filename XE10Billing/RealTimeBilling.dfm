object RealTimeBillingForm: TRealTimeBillingForm
  Left = 412
  Top = 254
  Caption = 'Real Time Billing'
  ClientHeight = 393
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LogMemo: TMemo
    Left = 0
    Top = 41
    Width = 560
    Height = 352
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 560
    Height = 41
    Align = alTop
    TabOrder = 1
    object ProcessQueueButton: TButton
      Left = 104
      Top = 8
      Width = 113
      Height = 22
      Caption = 'Process Queue'
      TabOrder = 0
      OnClick = ProcessQueueButtonClick
    end
    object QueueNumberEdit: TEdit
      Left = 8
      Top = 8
      Width = 89
      Height = 21
      TabOrder = 1
      Text = '100'
    end
  end
end
