unit BillingOutput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, CheckLst, DBCtrls,
  Grids, DBGrids, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxDBLookupComboBox, cxTextEdit,
  cxCurrencyEdit, cxCheckBox, cxMaskEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxGridBandedTableView, cxGridDBBandedTableView,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, dxSkinsCore,
  dxSkinscxPCPainter, cxDropDownEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges, dxScrollbarAnnotations, dxCore, dxSkinsForm,
  dxSkinDevExpressStyle;

type
  TBillingOutputForm = class(TBaseCxListForm)
    FooterPanel: TPanel;
    FieldNames: TCheckListBox;
    UpButton: TButton;
    DownButton: TButton;
    SaveContentsButton: TButton;
    Customer: TADODataSet;
    CustomerDS: TDataSource;
    Label1: TLabel;
    Splitter: TSplitter;
    BillingDetail: TADODataSet;
    CallCenterGroups: TADODataSet;
    CallCenterFamiliesDS: TDataSource;
    LocatingCompany: TADODataSet;
    LocatingCompanyDS: TDataSource;
    Breakdown: TADODataSet;
    BreakdownDS: TDataSource;
    BreakdownGrid: TDBGrid;
    Label2: TLabel;
    Breakdownbilling_breakdown_id: TAutoIncField;
    Breakdownoutput_config_id: TIntegerField;
    Breakdownexplanation: TStringField;
    Breakdownratio: TBCDField;
    EditDetailButton: TButton;
    ShowCustomerID: TDBText;
    CustomerIDLabel: TLabel;
    ShowOutputConfigID: TDBText;
    OutputConfigIDLabel: TLabel;
    GridBOCView: TcxGridDBBandedTableView;
    ColCenterGroupLookup: TcxGridDBBandedColumn;
    ColCustomerLookup: TcxGridDBBandedColumn;
    Colcomment: TcxGridDBBandedColumn;
    Colwhich_invoice: TcxGridDBBandedColumn;
    Coloutput_template: TcxGridDBBandedColumn;
    Colcustomer_number: TcxGridDBBandedColumn;
    Colcontract: TcxGridDBBandedColumn;
    Colflat_fee: TcxGridDBBandedColumn;
    Colpayment_terms: TcxGridDBBandedColumn;
    Coloutput_fields: TcxGridDBBandedColumn;
    Colcust_po_num: TcxGridDBBandedColumn;
    Colconsol_line_item: TcxGridDBBandedColumn;
    Coloutput_config_id: TcxGridDBBandedColumn;
    Colgroup_by_price: TcxGridDBBandedColumn;
    Colomit_zero_amount: TcxGridDBBandedColumn;
    Coltransmission_billing: TcxGridDBBandedColumn;
    Coltransmission_bulk: TcxGridDBBandedColumn;
    Colsales_tax_included: TcxGridDBBandedColumn;
    Colplat_map_prefix: TcxGridDBBandedColumn;
    procedure SaveContentsButtonClick(Sender: TObject);
    procedure UpButtonClick(Sender: TObject);
    procedure DownButtonClick(Sender: TObject);
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure FieldNamesClickCheck(Sender: TObject);
    procedure BreakdownNewRecord(DataSet: TDataSet);
    procedure EditDetailButtonClick(Sender: TObject);
    procedure DataAfterScroll(DataSet: TDataSet);
  private
    FDisplayingFieldList: string;
    function GetSelectedFieldName: Integer;
    procedure PopulateFieldNamesCheckListBox(FieldList: string);
    procedure EnableDetailForThisItem;
    procedure DisableDetailForThisItem;
  public
    procedure PopulateDropdowns; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

implementation

uses
  OdDbUtils, QMBillingDMu, OdVclUtils;

{$R *.dfm}

procedure TBillingOutputForm.PopulateDropDowns;
var
  i: Integer;
  FieldName: string;
begin
  inherited;
  // Populate FieldNames checklistbox, using field from BillingDetail
  BillingDetail.Open;
  FieldNames.Clear;
  for i := 0 to BillingDetail.FieldCount - 1 do begin
    FieldName := BillingDetail.Fields[i].FieldName;
    FieldNames.Items.Add(FieldName);
  end;
  FieldNames.Sorted := True;
  FieldNames.Sorted := False;
  BillingDetail.Close;
end;

procedure TBillingOutputForm.OpenDataSets;
begin
  Customer.Open;
  CallCenterGroups.Open;
  LocatingCompany.Open;
  Breakdown.Open;
  inherited;
end;

procedure TBillingOutputForm.CloseDataSets;
begin
  Breakdown.Close;
  Customer.Close;
  CallCenterGroups.Close;
  LocatingCompany.Close;
  inherited;
end;

procedure TBillingOutputForm.SaveContentsButtonClick(Sender: TObject);
var
  i: Integer;
  Output: string;
begin
  Output := '';
  if FieldNames.Items.Count > 0 then begin
    for i := 0 to FieldNames.Items.Count - 1 do
      if FieldNames.Checked[i] then
        Output := Output + FieldNames.Items[i] + ',';
    // Remove trailing comma
    if Output > '' then Output := Copy(Output, 1, Length(Output) - 1);
  end;

  Data.Edit;
  Data.FieldByName('output_fields').AsString := Output;
  Data.Post;
end;

procedure TBillingOutputForm.PopulateFieldNamesCheckListBox(FieldList: string);
var
  Output: string;
  FieldName: string;
  i, Count: Integer;
  C: Char;

  procedure CheckFieldName(FieldName: string);
  var
    j: Integer;
  begin
    for j := 0 to FieldNames.Items.Count - 1 do begin
      if FieldNames.Items[j] = FieldName then
        FieldNames.Checked[j] := True;
    end;
  end;

  procedure MoveToPosition(FieldName: string; Position: Integer);
  var
    j: Integer;
    Value: string;
    Checked: Boolean;
  begin
    for j := 0 to FieldNames.Items.Count - 1 do begin
      if FieldNames.Items[j] = FieldName then begin
        if j = Position then
          Break;
        // Delete field at <j>, then re-insert at <Position>
        Value := FieldNames.Items[j];
        Checked := FieldNames.Checked[j];
        FieldNames.Items.Delete(j);
        FieldNames.Items.Insert(Position, Value);
        FieldNames.Checked[Position] := Checked;
        Break;
      end;
    end;
  end;

begin
  FieldNames.Items.BeginUpdate;
  try
    // Sort everything
    FieldNames.Sorted := True;
    FieldNames.Sorted := False;
    // Clear all checkboxes
    SetCheckListBox(FieldNames, False);

    // Read output_fields and check the appropriate names
    Output := FieldList;
    if Output > '' then begin
      Count := 0;
      for i := 1 to Length(Output) do begin
        C := Output[i];
        if C = ',' then begin
          CheckFieldName(FieldName);
          // move field up so all checked fields are on top, in correct order
          MoveToPosition(FieldName, Count);
          FieldName := '';
          Inc(Count);
        end
        else FieldName := FieldName + C;
      end;
      CheckFieldName(FieldName);
      MoveToPosition(FieldName, Count);
    end;
  finally
    FieldNames.Items.EndUpdate;
  end;
end;

procedure TBillingOutputForm.UpButtonClick(Sender: TObject);
var
  Index: Integer;
  Checked: Boolean;
  Value: string;
begin
  Index := GetSelectedFieldName;
  // If index is 0, the field is already at the top, so don't do anything;
  // otherwise move it up
  if Index > 0 then begin
    // Swap field with the one directly above it
    Value := FieldNames.Items[Index - 1];
    Checked := FieldNames.Checked[Index - 1];
    FieldNames.Items[Index - 1] := FieldNames.Items[Index];
    FieldNames.Checked[Index - 1] := FieldNames.Checked[Index];
    FieldNames.Items[Index] := Value;
    FieldNames.Checked[Index] := Checked;
    // Keep original field selected
    FieldNames.Selected[Index - 1] := True;
  end;
end;

function TBillingOutputForm.GetSelectedFieldName: Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FieldNames.Items.Count - 1 do begin
    if FieldNames.Selected[i] then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TBillingOutputForm.DownButtonClick(Sender: TObject);
var
  Index: Integer;
  Checked: Boolean;
  Value: string;
begin
  Index := GetSelectedFieldName;
  if Index < FieldNames.Items.Count - 1 then begin
    // Swap field with the one directly below it
    Value := FieldNames.Items[Index + 1];
    Checked := FieldNames.Checked[Index + 1];
    FieldNames.Items[Index + 1] := FieldNames.Items[Index];
    FieldNames.Checked[Index + 1] := FieldNames.Checked[Index];
    FieldNames.Items[Index] := Value;
    FieldNames.Checked[Index] := Checked;
    // Keep original field selected
    FieldNames.Selected[Index + 1] := True;
  end;
end;

procedure TBillingOutputForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  FDisplayingFieldList := '-';
  AddLookupField('CustomerLookup', DataSet, 'customer_id', Customer,
    'customer_id', 'customer_name');
 DataSet.FieldByName('CustomerLookup').LookupCache := True;

  AddLookupField('CenterGroupLookup', DataSet, 'center_group_id',
    CallCenterGroups, 'center_group_id', 'group_code');
 DataSet.FieldByName('CenterGroupLookup').LookupCache := True;
end;

procedure TBillingOutputForm.FieldNamesClickCheck(Sender: TObject);
begin
  inherited;
  FDisplayingFieldList := '-'; // to force a refresh on move
end;

procedure TBillingOutputForm.BreakdownNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('output_config_id').AsInteger :=
    Data.FieldByName('output_config_id').AsInteger;
end;

procedure TBillingOutputForm.EditDetailButtonClick(Sender: TObject);
begin
  inherited;
  EnableDetailForThisItem;
end;

procedure TBillingOutputForm.DataAfterScroll(DataSet: TDataSet);
begin
  inherited;
  DisableDetailForThisItem;
end;

procedure TBillingOutputForm.DisableDetailForThisItem;
begin
  PostDataSet(Breakdown);

  Breakdown.Close;
  SetCheckListBox(FieldNames, False);
  FieldNames.Enabled := False;
  UpButton.Enabled := False;
  DownButton.Enabled := False;
  SaveContentsButton.Enabled := False;
  BreakdownGrid.Enabled := False;
end;

procedure TBillingOutputForm.EnableDetailForThisItem;
var
  FieldList: string;
begin
  FieldNames.Enabled := True;
  UpButton.Enabled := True;
  DownButton.Enabled := True;
  SaveContentsButton.Enabled := True;
  BreakdownGrid.Enabled := True;

  FieldList := Data.FieldByName('output_fields').AsString;
  if FDisplayingFieldList <> FieldList then begin
    FDisplayingFieldList := FieldList;
    PopulateFieldNamesCheckListBox(FieldList);
  end;

  Breakdown.Close;
  Breakdown.Parameters.ParamValues['output_config_id'] := Data.FieldByName('output_config_id').Value;
  Breakdown.Open;
end;

procedure TBillingOutputForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  DisableDetailForThisItem;
end;

end.

