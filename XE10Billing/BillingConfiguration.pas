unit BillingConfiguration;

interface

uses
  SysUtils, Classes, DB, ADODB, OdMiscUtils, BillingRates, BillingLocate,
  BetterADODataSet, ConfigDMu, BillingConst, Contnrs, IniFiles, Variants;

type
  TTaxResult = record
    TaxName: string;
    TaxRate: Single;
  end;

  TBillingConfig = class(TDataModule)
    ExcludeWorkFor: TADODataSet;
    ExcludeWorkType: TADODataSet;
    BillingRates: TADODataSet;
    TieredClientList: TADODataSet;
    StatusDescriptions: TADODataSet;
    CodeMap: TADODataSet;
    Taxes: TADODataSet;
    EligibleTerms: TADODataSet;
    ClientsForCenters: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FLogEvent: TLogEvent;
    FRateTable: TBillingRateTable;
    FExcludeWork: TStrings;
    FExcludeWorkType: TStrings;
    FTieredClientList: TStringList;
    FStatusDescriptions: TStringList;
    FCenterWhere: string;
    FConn: TADOConnection;
    FCodeMapTable: TStrings;

    procedure Log(Msg: string);
    procedure LoadExcludeWorkFor;
    procedure LoadExcludeWorkType;
    procedure LoadTieredClientList;
    procedure LoadRatesWhere(Where: string);
    procedure LoadCodeMap;
    procedure LoadSalesTaxData;
    procedure Initialize;
  public
    ConfigDM: TConfigDM;
    constructor CreateForCenterListString(Conn: TADOConnection; Centers: string);
    constructor CreateForAllCenters(Conn: TADOConnection);
    function IsExcludedWorkFor(ClientID: Integer; WorkDoneFor: string): Boolean;
    function IsExcludedWorkType(ClientID: Integer; WorkType: string): Boolean;
    function IsTieredClient(const CallCenter, ClientCode: string): Boolean;
    function GetStatusDescription(const Status: string): string;
    function LookupCodeMap(const State, County, City: string): string;
    function GetRateTable: TBillingRateTable;
    function LookupSalesTax(Locate: TLocate; var TaxResult: TTaxResult): Boolean;
    procedure LoadEligibleTerms(List: TStrings; const CallCenters, Span: string);
    function GetClientListForCallCenters(const CallCenters, Span: string): string;
    property Logger: TLogEvent read FLogEvent write FLogEvent;
  end;

implementation

uses
  OdIsoDates, OdAdoUtils, QMBillingDMu;

{$R *.dfm}

{ TBillingConfig }

procedure TBillingConfig.Log(Msg: string);
begin
  if Assigned(FLogEvent) then
    FLogEvent(Self, IsoTimeToStrNoMillis(Time) + ': ' + Msg);
end;

procedure TBillingConfig.DataModuleCreate(Sender: TObject);
begin
  FRateTable := TBillingRateTable.Create(nil);
  FExcludeWork := TStringList.Create;
  FExcludeWorkType := TStringList.Create;
  FStatusDescriptions := TStringList.Create;
  FCodeMapTable := THashedStringList.Create;
  Initialize;
end;

procedure TBillingConfig.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FRateTable);
  FreeAndNil(FExcludeWork);
  FreeAndNil(FExcludeWorkType);
  FreeAndNil(FTieredClientList);
  FreeAndNil(FStatusDescriptions);
  FreeAndNil(FCodeMapTable);
  FreeAndNil(ConfigDM);
end;

procedure TBillingConfig.LoadExcludeWorkFor;
begin
  Log('Loading exclude work done for table...');
  with ExcludeWorkFor do begin
    Open;
    while not EOF do begin
      FExcludeWork.Add(FieldByName('billing_term_id').AsString + '/' +
        FieldByName('work_done_for').AsString);
      Next;
    end;
    Close;
  end;
  if FExcludeWork.Count = 0 then
    Log('WARNING: No records in FExcludeWork');
end;

procedure TBillingConfig.LoadExcludeWorkType;
begin
  Log('Loading exclude work type for table...');
  with ExcludeWorkType do begin
    Open;
    while not EOF do begin
      FExcludeWorkType.Add(FieldByName('billing_term_id').AsString + '/' +
        FieldByName('work_type').AsString);
      Next;
    end;
    Close;
  end;
  if FExcludeWorkType.Count = 0 then
    Log('WARNING: No records in FExcludeWorkType');
end;

function TBillingConfig.IsExcludedWorkFor(ClientID: Integer; WorkDoneFor: string): Boolean;
begin
  Result := FExcludeWork.IndexOf(IntToStr(ClientID) + '/' + WorkDoneFor) >= 0;
end;

function TBillingConfig.IsExcludedWorkType(ClientID: Integer; WorkType: string): Boolean;
begin
  Result := FExcludeWorkType.IndexOf(IntToStr(ClientID) + '/' + WorkType) >= 0;
end;

function TBillingConfig.GetRateTable: TBillingRateTable;
begin
  Result := FRateTable;
end;

function TBillingConfig.GetStatusDescription(const Status: string): string;
var
  SD: TDataSet;
begin
  if FStatusDescriptions.Count < 1 then begin
    SD := StatusDescriptions;
    SD.Open;
    try
      while not SD.Eof do begin
        FStatusDescriptions.Add(SD.FieldValues['status'] + '=' +
          SD.FieldValues['status_name']);
        SD.Next;
      end;
    finally
      StatusDescriptions.Close;
    end;
  end;

  Result := FStatusDescriptions.Values[Status];
  if Result = '' then
    Result := Status;
end;

function TBillingConfig.IsTieredClient(const CallCenter, ClientCode: string): Boolean;
begin
  if not Assigned(FTieredClientList) then begin
    FTieredClientList := TStringList.Create;
    LoadTieredClientList;
  end;

  Result := FTieredClientList.IndexOf(CallCenter + '/' + ClientCode) > -1;
end;

procedure TBillingConfig.LoadTieredClientList;
var
  List: TDataSet;
begin
  FTieredClientList.Clear;
  List := TieredClientList;
  List.Open;
  try
    while not List.Eof do begin
      FTieredClientList.Add(List.FieldByName('call_center').AsString +
        '/' + List.FieldByName('billing_cc').AsString);
      List.Next;
    end;
  finally
    List.Close;
  end;
  FTieredClientList.Sort;
end;

procedure TBillingConfig.LoadRatesWhere(Where: string);
var
  Rate: TADODataSet;

const
  BILLING_RATES =  //qm-670
  ' SELECT [br_id] '+
  ' ,[call_center] '+
  ' ,[billing_cc] '+
  ' ,[status]  '+
  ' ,[parties]  '+
  ' ,[rate] '+
  ' ,[bill_code] '+
  ' ,[bill_type] '+
  ' ,[area_name] '+
  ' ,upper([work_county]) as work_county '+
  ' ,[line_item_text]'+
  ' ,upper([work_city]) as work_city '+
  ' ,[modified_date] '+
  ' ,[which_invoice] '+
  ' ,[additional_rate] '+
  ' ,[value_group_id]  '+
  ' ,[additional_line_item_text] '+
  ' ,[CWICode] '+                       //qm-948
  ' ,[AddlCWICode] '+                   //qm-948
  ' ,[Rpt_gl] '+                       //qm-995
  ' ,[Additional_Rpt_gl] '+            //qm-995
  ' FROM [dbo].[billing_rate]  ';

begin
  Log('Loading rate table...');
  Rate := BillingRates;
  Rate.CommandText := BILLING_RATES + Where;
  Rate.Open;
  FRateTable.LoadFrom(Rate);
  Rate.Close;
end;

constructor TBillingConfig.CreateForAllCenters(Conn: TADOConnection);
begin
  FConn := Conn;
  FCenterWhere := '';
  inherited Create(nil);
end;

constructor TBillingConfig.CreateForCenterListString(Conn: TADOConnection;
  Centers: string);
begin
  FConn := Conn;
  FCenterWhere := Centers;
  inherited Create(nil);
end;

procedure TBillingConfig.Initialize;
begin
  SetConnections(FConn, Self);
  ConfigDM := TConfigDM.Create(nil);
  ConfigDM.SetConnection(FConn);
  if Trim(FCenterWhere) <> '' then
    LoadRatesWhere('where call_center in (' + FCenterWhere + ')')
  else
    LoadRatesWhere('');
  LoadExcludeWorkFor;
  LoadExcludeWorkType;
  LoadCodeMap;
  LoadSalesTaxData;
end;

procedure TBillingConfig.LoadCodeMap;
var
  Key: string;
begin
  if FCodeMapTable.Count = 0 then begin
    Log('Loading billing code map...');
    with CodeMap do begin
      Open;
      while not EOF do begin
        Key := FieldByName('work_state').AsString + '/' +
          FieldByName('work_county').AsString + '/' +
          FieldByName('work_city').AsString;
        FCodeMapTable.Values[Key] := FieldByName('billing_cc').AsString;
        Next;
      end;
      Close;
    end;
  end;
end;

function TBillingConfig.LookupCodeMap(const State, County,
  City: string): string;
var
  Key: string;
begin
  Key := State + '/' + County + '/' + City;
  Result := FCodeMapTable.Values[Key];
end;

procedure TBillingConfig.LoadSalesTaxData;
begin
  Taxes.Open;
end;

function TBillingConfig.LookupSalesTax(Locate: TLocate; var TaxResult: TTaxResult): Boolean;
begin
  Result := False;

  if Taxes.Locate('state;county;city',
    VarArrayOf([Locate.WorkState, Locate.WorkCounty, Locate.WorkCity]), []) then begin
    TaxResult.TaxName := Taxes.FieldByName('tax_name').AsString;
    TaxResult.TaxRate := Taxes.FieldByName('tax_rate').AsFloat;
    Result := True;
    Exit;
  end;

  if Taxes.Locate('state;county;city',
    VarArrayOf([Locate.WorkState, Locate.WorkCounty, '*']), []) then begin
    TaxResult.TaxName := Taxes.FieldByName('tax_name').AsString;
    TaxResult.TaxRate := Taxes.FieldByName('tax_rate').AsFloat;
    Result := True;
    Exit;
  end;
end;

procedure TBillingConfig.LoadEligibleTerms(List: TStrings; const CallCenters, Span: string);
const
  SQL =
    'select c.oc_code from client c ' +
    '  left join customer cu on cu.customer_id=c.customer_id ' +
    ' where c.call_center in (%s) ' +
    '    and (cu.period_type=%s or ' +
    '     (%s=''WEEK'' and cu.customer_id IS NULL)) ' +
    ' order by c.oc_code ';
begin
  EligibleTerms.CommandText := Format(SQL, [CallCenters,
    QuotedStr(Span), QuotedStr(Span)]);
  List.Clear;
  EligibleTerms.Open;
  try
    while not EligibleTerms.EOF do begin
      List.Add(EligibleTerms.Fields[0].AsString);
      EligibleTerms.Next;
    end;
  finally
    EligibleTerms.Close;
  end;
end;

function TBillingConfig.GetClientListForCallCenters(const CallCenters, Span: string): string; //QMANTWO-707
const
  SQL =
    'select c.client_id from client c ' +
    '  left join customer cu on cu.customer_id=c.customer_id ' +
    ' where c.call_center in (%s) ' +
    '    and (cu.period_type=%s or ' +
    '     (%s=''WEEK'' and cu.customer_id IS NULL)) ' +
    ' order by c.client_id';
begin
  Result := '';
  ClientsForCenters.Close;
  ClientsForCenters.CommandText := Format(SQL, [CallCenters,
    QuotedStr(Span), QuotedStr(Span)]);
  ClientsForCenters.Open;
  while not ClientsForCenters.Eof do begin
    if Result = '' then
      Result := ClientsForCenters.FieldByName('client_id').AsString
    else
      Result := Result + ',' +
        ClientsForCenters.FieldByName('client_id').AsString;
    ClientsForCenters.Next;
  end;
  ClientsForCenters.Close;
end;

end.

