unit QMBillingDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, IniFiles, Forms, Dialogs,
  OdMiscUtils, OdIsoDates, ConfigDMu, ExtCtrls;

const
  AppName = 'Q Manager Billing';
  {$I ..\version.inc} // AppVersion = 'x.x.x.x';

  ErrorConnFailure00        = 0;
  ErrorConnFailure17        = 17;
  ConnectedToDBMessage      = 'Connected to the database';
  DisconnectedFromDBMessage = 'Disconnected from the database';

type
  TConnectionStatus = (csConnected, csDisconnected);
  TUpdateDBConnectionStatusInfo = procedure (ConnectionStatus: TConnectionStatus) of object;

  TQMBillingDM = class(TDataModule)
    Conn: TADOConnection;
    BillingQueue: TADODataSet;
    Clients: TADODataSet;
    ConnectionCheck: TTimer;
    AdminRestriction: TADODataSet;
    Ticket: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ConnAfterConnect(Sender: TObject);
    procedure ConnectionCheckTimer(Sender: TObject);
    procedure ConnBeforeConnect(Sender: TObject);
  private
    FOnUpdateDBConnectionStatusInfo: TUpdateDBConnectionStatusInfo;
    FConnectionStatus: TConnectionStatus;
    FAllowedFormList: TStringList;
    FOriginalConnString: string;
    function GetConnectionStatus: TConnectionStatus;
  public
    Server, DB, Username, Password, ConnString: string;
    BillingTimeout: Integer;
    ConfigDM: TConfigDM;
    Trusted: Boolean;
    UserHasRestrictedAccess: Boolean;
    function IniFileName: string;
    function ConnectWithConnString: Boolean;
    procedure Connect;
    procedure ConnectFromIni;
    function ServerString: string;
    function CreateIniFile: TIniFile;
    function QueuedBillingAvailable(var DateEnd: TDateTime; var Span, GroupName, Dir,
      RequestType: string; var RunID, ReQueueBillID: Integer): Boolean;
    procedure MarkStarted(RunID: Integer);
    procedure MarkDone(RunID: Integer; BillID: Variant; LogOutput: string);
    function GetIniSetting(const Section, SettingName: string; Default: string = ''): string;
    function AddGroupsToBillingQueue(const CallCenterGroups, Span: string;
      PeriodEndDate: TDateTime; const QueuedByEmpID: Integer): Integer;
    function GetTicketCallDate(TicketID: Integer): TDateTime;
    function GetAdminRestriction(FormList: TStringlist; const UserName: string): Boolean;
    procedure GetAllowedFormList(List: TStrings);
    procedure GetClientList(List: TStrings);
    procedure GetStatusList(List: TStrings; IncludeStar: Boolean = False);
    procedure GetCallCenterList(List: TStrings; IncludeBlank: Boolean = False; IncludeStar: Boolean = False);
    property ConnectionStatus: TConnectionStatus read FConnectionStatus;
    property OnUpdateDBConnectionStatusInfo: TUpdateDBConnectionStatusInfo Read FOnUpdateDBConnectionStatusInfo Write FOnUpdateDBConnectionStatusInfo;
  end;

var
  QMBillingDM: TQMBillingDM;

implementation

uses
  QMConst, DateUtils, Variants, OdDbUtils, OdAdoUtils;

{$R *.dfm}

{ TDM }

function TQMBillingDM.ConnectWithConnString: Boolean;
begin
  //If a connection string is present in the ini file, use that instead of showing the login form.
  Result := False;
  if NotEmpty(GetIniSetting('Database', 'ConnectionString', '')) then begin
    Conn.KeepConnection := True;
    ConnectAdoConnectionWithIni(Conn, IniFileName);
    Result := Conn.Connected;
  end;
end;

procedure TQMBillingDM.Connect;
begin
  Conn.KeepConnection := True;
  ConnectAdoConnection(Conn, Server, DB, Trusted, Username, Password);
  UserHasRestrictedAccess := GetAdminRestriction(FAllowedFormList, UserName);
end;

procedure TQMBillingDM.ConnectFromIni;
//Command line billing uses this
begin
  ConnectAdoConnectionWithIni(Conn, IniFileName);
end;

function TQMBillingDM.CreateIniFile: TIniFile;
begin
  Result := TIniFile.Create(IniFileName);
end;

function TQMBillingDM.ServerString: string;
begin
  Result := ConnStringDescription(FOriginalConnString, Conn.DefaultDatabase);
end;

function TQMBillingDM.IniFileName: string;
begin
  // The INI file is always in the same diretory as the exe
  Result := ChangeFileExt(ParamStr(0), '.ini');
end;

procedure TQMBillingDM.DataModuleCreate(Sender: TObject);
begin
  // This should produce a halting error if a programmer accidentally relies
  // on this Connection working with the design time settings.
  Conn.ConnectionString := 'Not initialized yet';
  ConfigDM := TConfigDM.Create(nil);
  ConfigDM.SetConnection(Conn);
  FAllowedFormList := TStringList.Create;
end;

function TQMBillingDM.QueuedBillingAvailable(var DateEnd: TDateTime;
  var Span, GroupName, Dir, RequestType: string;
  var RunID, ReQueueBillID: Integer): Boolean;
begin
  BillingQueue.Open;
  if BillingQueue.IsEmpty then begin
    BillingQueue.Close;
    Result := False;
    Exit;
  end;

  DateEnd := BillingQueue.FieldByName('period_end_date').AsDateTime;
  Span := BillingQueue.FieldByName('span').AsString;
  GroupName :=
    ConfigDM.GetCenterGroupNameFromID(BillingQueue.FieldByName('center_group_id').AsInteger);
  Dir := GetIniSetting('Billing', 'ExportDir', '');
  if Trim(Dir) = '' then
    Dir := AddSlash(ExtractFilePath(Application.ExeName)) + 'output\';

  RunID := BillingQueue.FieldByName('run_id').AsInteger;
  RequestType := VarToString(BillingQueue.FieldByName('request_type').Value);
  ReQueueBillID := VarToInt(BillingQueue.FieldByName('bill_id').Value, -1);

  Result := True;
end;

procedure TQMBillingDM.MarkStarted(RunID: Integer);
begin
  Assert(BillingQueue.FieldByName('run_id').AsInteger = RunID);
  BillingQueue.Edit;
  BillingQueue.FieldByName('run_date').AsDateTime := Now;
  BillingQueue.Post;
end;

procedure TQMBillingDM.MarkDone(RunID: Integer; BillID: Variant; LogOutput: string);
begin
  Assert(BillingQueue.FieldByName('run_id').AsInteger = RunID);
  BillingQueue.Edit;
  BillingQueue.FieldByName('completed_date').AsDateTime := Now;
  BillingQueue.FieldByName('run_log').AsString := LogOutput;
  BillingQueue.FieldByName('bill_id').Value := BillID;
  BillingQueue.Post;
end;

function TQMBillingDM.GetIniSetting(const Section, SettingName: string; Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := CreateIniFile;
  try
    Result := Ini.ReadString(Section, SettingName, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

function TQMBillingDM.AddGroupsToBillingQueue(const CallCenterGroups, Span: string;
  PeriodEndDate: TDateTime; const QueuedByEmpID: Integer): Integer;
const
  CheckQueueSQL = 'select * from billing_run where period_end_date=%s and span=%s ' +
    'and completed_date is null and center_group_id in ' +
    '(select center_group_id from center_group where active=1 and show_for_billing=1 %s)';
  AddToQueueSQL = 'insert into billing_run (request_by, request_date, center_group_id, period_end_date, span) ' +
    'select %d, getdate(), center_group_id, %s, %s from center_group where active=1 and show_for_billing=1 %s';
var
  CenterGroupFilter: string;
  BillQ: TDataSet;
begin
  Assert(Trim(CallCenterGroups) <> '');
  Result := 0;
  if not SameText(CallCenterGroups, 'all') then
    CenterGroupFilter := ' and group_code in (' +
      StringToDelimitedString(CallCenterGroups, '''', ',') + ')';

  BillQ := CreateDatasetWithQuery(Conn, Format(CheckQueueSQL,
    [QuotedStr(IsoDateToStr(PeriodEndDate)),
    QuotedStr(Span), CenterGroupFilter]));
  try
    if HasRecords(BillQ) then
      raise Exception.CreateFmt('Cannot add %s to the queue. ' +
        'Found pending requests for the selected centers and period.',
        [CallCenterGroups]);

    Result := ExecuteQuery(Conn, Format(AddToQueueSQL, [QueuedByEmpID,
      QuotedStr(IsoDateToStr(PeriodEndDate)), QuotedStr(Span), CenterGroupFilter]));
  finally
    FreeAndNil(BillQ);
  end;
end;

procedure TQMBillingDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ConfigDM);
  FreeAndNil(FAllowedFormList);
end;

// Close the connection if it is in fact disconnected.
// Execute event UpdateDBConnectionStatusInfo to refresh status bar in MainFU
// This might be better in a thread, so the UI does not block
procedure TQMBillingDM.ConnectionCheckTimer(Sender: TObject);
begin
  ConnectionCheck.Enabled := False;
  try
    FConnectionStatus := GetConnectionStatus;
    Conn.Connected := (FConnectionStatus = csConnected);
    if Assigned(FOnUpdateDBConnectionStatusInfo) then
      FOnUpdateDBConnectionStatusInfo(FConnectionStatus);
  finally
    ConnectionCheck.Enabled := True;
  end;
end;

procedure TQMBillingDM.ConnAfterConnect(Sender: TObject);
begin
  ConnectionCheck.Enabled := True;
end;

procedure TQMBillingDM.ConnBeforeConnect(Sender: TObject);
begin
  FOriginalConnString := Conn.ConnectionString;
end;

// Check the connection and return its status.  This is basically a ping to the server DB.
function TQMBillingDM.GetConnectionStatus: TConnectionStatus;
var
  i: Integer;
begin
  Result := csConnected;
  try
    Conn.Execute('select 1', i, [eoExecuteNoRecords]);
   except
     if (Conn.Errors[0].NativeError in [ErrorConnFailure00, ErrorConnFailure17]) then
       Result := csDisconnected;
   end;
end;

function TQMBillingDM.GetAdminRestriction(FormList: TStringlist; const UserName: string): Boolean;
var
  S: string;
begin
  Assert(Assigned(FormList), 'FormList is not assigned');

  // Restrictions are applied if a row with non-blank allowed_forms exists for the user
  AdminRestriction.Parameters.ParamValues['login_id'] := UserName;
  AdminRestriction.Open;
  try
    S := AdminRestriction.FieldByName('allowed_forms').AsString;
    // This is for backward compatibility with the restrictions, which used to use
    // some form names with a "Billing - " or "Damage " prefix.
    S := StringReplace(S, 'Billing - ', '', [rfReplaceAll]);
    FormList.CommaText := StringReplace(S, 'Damage ', '', [rFReplaceAll]);
    Result := FormList.Count > 0;
  finally
    AdminRestriction.Close;
  end;
end;

procedure TQMBillingDM.GetAllowedFormList(List: TStrings);
begin
  Assert(Assigned(List), 'No List assigned in GetAllowedFormList');
  Assert(Assigned(FAllowedFormList), 'No AllowFormList assigned in GetAllowedFormList');
  List.Clear;
  List.AddStrings(FAllowedFormList);
end;

function TQMBillingDM.GetTicketCallDate(TicketID: Integer): TDateTime;
begin
  Assert(TicketID > 0);
  Ticket.Parameters.ParamValues['ticket_id'] := TicketID;
  Ticket.Open;
  try
    Assert(Ticket.FieldByName('ticket_id').AsInteger = TicketID,
      'Parent ticket ID not found to get call date');
    Result := Ticket.FieldByName('call_date').AsDateTime;
  finally
    Ticket.Close;
  end;
end;

procedure TQMBillingDM.GetClientList(List: TStrings);
begin
  ConfigDM.GetClientList(List);
end;

procedure TQMBillingDM.GetStatusList(List: TStrings; IncludeStar: Boolean);
begin
  ConfigDM.GetStatusList(List, IncludeStar);
end;

procedure TQMBillingDM.GetCallCenterList(List: TStrings; IncludeBlank: Boolean; IncludeStar: Boolean);
begin
  ConfigDM.GetCallCenterList(List, IncludeBlank, IncludeStar);
end;

end.
