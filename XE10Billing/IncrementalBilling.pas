unit IncrementalBilling;

interface

uses
  SysUtils, Classes, DB, BillingRates, BillingShared,
  BillingTicketClientList, DateUtils, BillingConfiguration;

type
  TBillingLogEvent = procedure(Msg: string) of object;

  TBillingTransaction = record
    TicketID: Integer;
    LocateID: Variant;
    LocateStatusID: Variant;
    CallCenter: string;
    EventDate: TDateTime;
    EffectiveDate: TDateTime;
    Amount: Double;
    Description: string;
  end;

function IncrementalRate(QueueRecord: TDataSet; Ticket: TDataSet;
  TicketVersions: TDataSet; Locates: TDataSet;
  BillingConfig: TBillingConfig; Log: TBillingLogEvent): TBillingTransaction;

implementation

uses
  OdDbUtils, OdMiscUtils, BillingLocate, BillingConst, QMConst, QMBillingDMu;

function IncrementalRate(QueueRecord, Ticket, TicketVersions: TDataSet;
  Locates: TDataSet; BillingConfig: TBillingConfig; Log: TBillingLogEvent): TBillingTransaction;
const
  NoRate = -999999;
var
  Locate: TLocate;
  TicketType: string;
  BillCode: string;
  LineItemText: string;
  PartyCount: Integer;
  FoundRate: Double;
  EventType: string;
  TicketClients: TTicketClientList;
  RateEntry: TRateEntry;

  procedure ForceNoCharge(const Description: string = '');
  begin
    FoundRate := 0;
    Result.Description := 'No Charge';
    if Description <> '' then
      Result.Description := Result.Description + ', ' + Description;
  end;

begin
  TicketClients := TTicketClientList.Create(nil);
  TicketClients.TieredClientFunc := BillingConfig.IsTieredClient;
  TicketClients.CheckForDuplicates := True;

  FoundRate := NoRate;
  TicketType := TicketTypeNormal;
  EventType := QueueRecord.FieldByName('event_type').AsString;
  Result.TicketID := Ticket.FieldByName('ticket_id').AsInteger;
  Assert(Result.TicketID > 0);

  Assert(Ticket.FieldByName('ticket_id').AsInteger = Result.TicketID);
  Result.LocateID := QueueRecord.FieldByName('locate_id').Value;
  Result.LocateStatusID := QueueRecord.FieldByName('locate_status_id').Value;
  Result.CallCenter := Ticket.FieldByName('ticket_format').AsString;
  //TODO: Handle null event_date values
  Result.EventDate := QueueRecord.FieldByName('event_date').AsDateTime;
  Result.EffectiveDate := Now;
  Result.Amount := 0;
  Result.Description := '';

  Locates.First;
  while not Locates.Eof do begin
    TicketClients.Add(Result.TicketID, Locates.FieldByName('locator_id').AsInteger, Locates.FieldByName('ref_id').AsInteger, Locates.FieldByName('client_code').AsString, Locates.FieldByName('status').AsString);//QMANTWO-710
    Locates.Next;
  end;

  if not Locates.Locate('locate_id', Result.LocateID, []) then
    raise Exception.CreateFmt('Locate ID %d not found', [Result.LocateID]);

  PartyCount := TicketClients.CountRespectedLocates(Result.TicketID, Locates.FieldByName('client_id').AsInteger, Locates.FieldByName('client_code').AsString, Ticket.FieldByName('ticket_format').AsString);

  Locate := TLocate.CreateFromDataSets(Locates, Ticket);
  Locate.Bucket := 'Normal Notice';
  if not Locate.Closed then begin
    Log(Format('Locate %s/%d on ticket %s/%d for center %s is not closed, skipping', [Locate.ClientCode, Locate.LocateID, Locate.TicketNumber, Locate.TicketID, Locate.CallCenter]));
    Result.Description := 'Not Closed';
    Exit;
  end;
  // Some defaults that apply to all call centers
  if Locate.QtyMarked < 1 then
    Locate.QtyMarked := 1;
  Locate.QtyCharged := Locate.QtyMarked;
  if Locate.Status = 'NC' then
    ForceNoCharge;

  if (EventType <> BillingEventLocate) then
  begin
    Log(Format('Billing event type %s not supported, skipping', [EventType]));
    Result.Description := EventType;
    Exit;
  end;

  // 300: STS Atlanta
  if Locate.CallCenter = '300' then begin
    Locate.QtyCharged := 1;

    if LocateMatchesTermWorkOrExc(Locate, 'AGLTermsNLR', 'AGLTermsWDF', 'AGLTermsCDW') then
      ForceNoCharge('AGL')
    else if LocateMatchesTermWorkOrExc(Locate, 'BSTTermsNLR', 'BSTTermsWDF', 'BSTTermsCDW') then
      ForceNoCharge('BST')
    else if LocateMatchesTermWorkOrExc(Locate, 'ComcastTermsNLR', 'ComcastTermsWDF', 'ComcastTermsCDW') then
      ForceNoCharge('Comcast')
    else if LocateMatchesTermWorkOrExc(Locate, 'CowetaTermsNLR', 'CowetaTermsWDF', 'CowetaTermsCDW') then
      ForceNoCharge('Coweta');

    // We can not bill the same ticket more than once in a 21 day period
    if Locate.ParentTicketID > 0 then begin
      if ((Locate.CallDate - QMBillingDM.GetTicketCallDate(Locate.ParentTicketID)) < 21) then
        ForceNoCharge('Duplicate Ticket');
    end;
  end;

  if Locate.Status <> 'NC' then begin
    //if Locate.IsDamage then begin
    //  TicketType := TicketTypeDamage;
    //  Locate.Bucket := 'Damage Ticket' + ', ' + BSDM.GetStatusDescription(Locate.Status);
    //end else
    if Locate.IsEmergencyOrManualAfterHoursType then begin
      if TreatAsAfterHours(Locate) then begin
        Locate.Bucket := 'After Hours Notice' + ', ' + BillingConfig.GetStatusDescription(Locate.Status);
        TicketType := TicketTypeAfter;
      end else begin
        Locate.Bucket := 'Emergency Notice' + ', ' + BillingConfig.GetStatusDescription(Locate.Status);
        TicketType := TicketTypeEmerg;
      end;
    end
  end;

  if FoundRate = NoRate then begin
    RateEntry := BillingConfig.GetRateTable.LookupRate(Locate, PartyCount, TicketType);
    FoundRate := RateEntry.Rate;
    BillCode := RateEntry.BillCode;
    LineItemText := RateEntry.LineItemText;

    if Trim(LineItemText) <> '' then
      Result.Description := LineItemText;
  end;
  if Trim(Result.Description) = '' then
    Result.Description := Locate.Bucket;

  Result.Amount := GetPriceFromRateQty(Locate, FoundRate, Locate.QtyCharged);
  Locate.BillingTicketType := TicketType;
  Locate.BillingClientCode := Locate.ClientCode;

  with Result do
    Log(Format('Billing Center: %s  Ticket: %d  Locate %d  Term: %s  Status: %s  Rate: %f  Price: %f  Type: %s  Event: %s',
               [CallCenter, VarToInt(TicketID), VarToInt(LocateID), Locate.ClientCode, Locate.Status, FoundRate, Amount, TicketType, DateTimeToStr(EventDate)]));

  TicketClients.Free;
end;

end.
