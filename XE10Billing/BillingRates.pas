unit BillingRates;

interface

uses
  SysUtils, Classes, DB, DBISAMTb, BillingLocate, BillingConst, BillingShared, OdMiscUtils;

type
  TBillingRateTable = class(TDataModule)
    Rates: TDBISAMTable;
    AreaCountyCheck: TDBISAMQuery;
    CityCheck: TDBISAMQuery;
    MaxPartyValue: TDBISAMQuery;
  private
    // 3 variables to help us optimize PartyScan by removing unnecessary checks
    FHaveAreaCountyValues: Boolean;
    FHaveWorkCityValues: Boolean;
    FMaxPartyValue: Integer;
    function InternalLookup(const Status, TicketType, AreaName, County, City: string; Loc: TLocate;
      Parties: Integer; var RateEntry: TRateEntry): Boolean;
    function PartyScan(Loc: TLocate; const Status: string; const TicketType: string;
      Parties: Integer; var RateEntry: TRateEntry): Boolean;
    procedure CheckForRateFields;
  public
    function LookupRate(Loc: TLocate; Parties: Integer; TicketTypes: array of string): TRateEntry;
    function LoadFrom(DataSet: TDataSet): Integer;
    function HaveRateForTermStatus(Locate: TLocate): Boolean;
  end;

implementation

{$R *.dfm}

uses Variants, OdDBISAMUtils;

const
  LookupIndexFields = 'billing_cc;call_center;status;parties;bill_type;area_name;work_county;work_city';

{ TBillingRateTable }

// Public: Lookup the specific locate both by status and then * if there is not exact match
function TBillingRateTable.LookupRate(Loc: TLocate; Parties: Integer;
  TicketTypes: array of string): TRateEntry;
var
  i: Integer;
begin
  for i := Low(TicketTypes) to High(TicketTypes) do begin
    if PartyScan(Loc, Loc.Status, TicketTypes[i], Parties, Result) then
      Exit;

    if PartyScan(Loc, '*', TicketTypes[i], Parties, Result) then
      Exit;
  end;

  raise
    ENoRateFound.CreateFmt('No matching rate for locate %d.  Client: %s  Type: %s  Center: %s  Status: %s  Parties: %d  County: %s  City: %s WDF Value Group: %s',
    [Loc.LocateID, Loc.ClientCode, TicketTypes[0], Loc.CallCenter, Loc.Status,
      Parties, Loc.WorkCounty, Loc.BillingCity, Loc.WorkDoneFor]);
end;

function TBillingRateTable.InternalLookup(const Status, TicketType, AreaName,
  County, City: string; Loc: TLocate; Parties: Integer; var RateEntry: TRateEntry): Boolean;
var
  I: Integer;

  function RatesFilter: string;
  begin
    Result := 'billing_cc=' + QuotedStr(Loc.ClientCode) + ' and call_center=' + QuotedStr(Loc.CallCenter) +
    ' and status=' + QuotedStr(Status) + ' and parties=' + IntToStr(Parties) + ' and bill_type=' + QuotedStr(TicketType);
    if not IsEmpty(AreaName) then
      Result := Result + ' and area_name=' + QuotedStr(AreaName);
    if not IsEmpty(County) then
      Result := Result + ' and work_county=' + QuotedStr(County);
    if not IsEmpty(City) then
      Result := Result + ' and work_city=' + QuotedStr(City);
  end;
begin
  RateEntry.Rate := 0;
  RateEntry.BillCode := '';
  Result := False;

  //Filter out the rates for all criteria except value_group_id
  Rates.Filtered := False;
  Rates.Filter := RatesFilter;
  Rates.Filtered := True;
  if Rates.RecordCount > 0 then begin
    for I := 0 to Rates.RecordCount - 1 do begin
      Result := ( (Rates.FieldByName('value_group_id').AsInteger <> 0) and
        MatchesWorkDoneForGroup(Loc, GetValueGroupNameForID(Rates.FieldByName('value_group_id').AsInteger)) );
      if Result then
        Break;
      Rates.Next;
    end;
  end;

  if not Result then begin //then there was no match for criteria + value_group_id; proceed normally
    Rates.Filtered := False;

    Rates.Filter := RatesFilter;
    Rates.Filtered := True;
    if Rates.RecordCount > 0 then
    Result := Rates.Locate(LookupIndexFields,
      VarArrayOf([Loc.ClientCode, Loc.CallCenter, Status, Parties, TicketType, AreaName, uppercase(County), uppercase(City)]), []);
  end;


  if Result then begin
    RateEntry.Rate := Rates.FieldByName('rate').AsCurrency;
    RateEntry.BillCode := Rates.FieldByName('bill_code').AsString;
    RateEntry.LineItemText := Rates.FieldByName('line_item_text').AsString;
    RateEntry.WhichInvoice := Rates.FieldByName('which_invoice').AsString;
    RateEntry.AdditionalRate := Rates.FieldByName('additional_rate').AsCurrency;
    RateEntry.MatchedStatus := Rates.FieldByName('status').AsString;
    RateEntry.ValueGroupID := Rates.FieldByName('value_group_id').AsInteger;
    RateEntry.AdditionalLineItemText := Rates.FieldByName('additional_line_item_text').AsString;
//    RateEntry.BillingRateID := Rates.FieldByName('br_id').AsInteger; //qm-948
    RateEntry.CWICode := Rates.FieldByName('CWICode').AsString; //qm-948
    RateEntry.AddlCWICode := Rates.FieldByName('AddlCWICode').AsString; //qm-948

    RateEntry.Rpt_gl := Rates.FieldByName('Rpt_gl').AsString; //qm-995
    RateEntry.Additional_Rpt_gl := Rates.FieldByName('Additional_Rpt_gl').AsString; //qm-995
  end;
    Rates.Filtered := False;
end;

// Gradually decrease the party count until we find a matching rate with the count
// Never check for party counts > than the max for this billing run
// ALso, handle partial matches on county, area, city is any exist
function TBillingRateTable.PartyScan(Loc: TLocate; const Status: string;
  const TicketType: string; Parties: Integer; var RateEntry: TRateEntry): Boolean;
var
  PartyCheck: Integer;
begin
  PartyCheck := Parties;
  Assert(PartyCheck > 0);
  if PartyCheck > FMaxPartyValue then
    PartyCheck := FMaxPartyValue;

  repeat
    // Check for everything present, or just the city missing
    if FHaveAreaCountyValues then begin
      if FHaveWorkCityValues then begin
        Result := InternalLookup(Status, TicketType, Loc.AreaName, uppercase(Loc.WorkCounty),
          uppercase(Loc.BillingCity), Loc, PartyCheck, RateEntry);
        if Result then
          Exit;
      end;
      Result := InternalLookup(Status, TicketType, Loc.AreaName, uppercase(Loc.WorkCounty),
        {City}'', Loc, PartyCheck, RateEntry);
      if Result then
        Exit;

      // Check for just area name missing, or area name and city
      if FHaveWorkCityValues then begin
        Result := InternalLookup(Status, TicketType, {Area}'', uppercase(Loc.WorkCounty),   //qm-670
          uppercase(Loc.BillingCity), Loc, PartyCheck, RateEntry); //qm-670
        if Result then
          Exit;
      end;
      Result := InternalLookup(Status, TicketType, {Area}'', uppercase(Loc.WorkCounty),   //qm-670
        {City}'', Loc, PartyCheck, RateEntry);
      if Result then
        Exit;

      // Check just county missing, or county and city
      if FHaveWorkCityValues then begin
        Result := InternalLookup(Status, TicketType, Loc.AreaName, {County}'',
          uppercase(Loc.BillingCity), Loc, PartyCheck, RateEntry); //qm-670
        if Result then
          Exit;
      end;
      Result := InternalLookup(Status, TicketType, Loc.AreaName, {County}'',
        {City}'', Loc, PartyCheck, RateEntry);
      if Result then
        Exit;
    end;

    // Check for area and county missing, as well as all three (area, county, and city)
    if FHaveWorkCityValues then begin
      Result := InternalLookup(Status, TicketType, {Area}'', {County}'',
        uppercase(Loc.BillingCity), Loc, PartyCheck, RateEntry);   //qm-670
      if Result then
        Exit;
    end;

    Result := InternalLookup(Status, TicketType, '', '', '', Loc, PartyCheck, RateEntry);
    if Result then
      Exit;

    Dec(PartyCheck);
  until PartyCheck = 0;
end;

function TBillingRateTable.LoadFrom(DataSet: TDataSet): Integer;
begin
  Assert(Assigned(DataSet));
  DataSet.First;
  Rates.Close;
  if Rates.Exists then
    Rates.EmptyTable;
  Rates.FieldDefs.Clear;
  if Rates.Exists then
    Rates.DeleteTable;

  CloneStructure(DataSet, Rates);
  CopyDataFrom(DataSet, Rates);
  Rates.Close;
  Rates.Exclusive := True;
  Rates.AddIndex('Main', LookupIndexFields, []);
  Rates.IndexName := 'Main';
  Rates.OptimizeTable('Main', True);
  Rates.Exclusive := False;
  Rates.Open;
  CheckForRateFields;
  Result := Rates.RecordCount;
end;

procedure TBillingRateTable.CheckForRateFields;
begin
  AreaCountyCheck.Open;
  try
    FHaveAreaCountyValues := (AreaCountyCheck.Fields[0].AsInteger > 0);
  finally
    AreaCountyCheck.Close;
  end;

  CityCheck.Open;
  try
    FHaveWorkCityValues := (CityCheck.Fields[0].AsInteger > 0);
  finally
    CityCheck.Close;
  end;

  MaxPartyValue.Open;
  try
    FMaxPartyValue := (MaxPartyValue.Fields[0].AsInteger);
    if FMaxPartyValue < 1 then
      FMaxPartyValue := 1;
  finally
    MaxPartyValue.Close;
  end;
end;

function TBillingRateTable.HaveRateForTermStatus(Locate: TLocate): Boolean;
begin
  Result := Rates.Locate('billing_cc;call_center;status',
    VarArrayOf([Locate.ClientCode, Locate.CallCenter, Locate.Status]), []);
end;

end.

