object BillingCalcDM: TBillingCalcDM
  OldCreateOrder = False
  Height = 268
  Width = 408
  object Queue: TBetterADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      '/* Query is over written by code */'#13#10'select'#13#10'top 100'#13#10'*'#13#10'from bi' +
      'lling_queue '#13#10'where failed_processing=0'#13#10'order by insert_date'#13#10
    Parameters = <>
    IndexDefs = <>
    Left = 40
    Top = 16
  end
  object DeleteFromQueue: TADOCommand
    CommandText = 'delete from billing_queue where queue_id = :queue_id'
    Connection = QMBillingDM.Conn
    Parameters = <
      item
        Name = 'queue_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 40
    Top = 80
  end
  object InternalTicket: TBetterADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      'select ticket_id, ticket_number, ticket_format, kind, map_page, ' +
      'revision,'#13#10'transmit_date, due_date, work_description, work_state' +
      ', work_county,'#13#10'work_city, work_address_number, work_address_num' +
      'ber_2,'#13#10'work_address_street, work_cross, work_type, work_date, c' +
      'ompany,'#13#10'con_name, call_date, work_lat, work_long, modified_date' +
      ', ticket_type,'#13#10'parent_ticket_id, route_area_id, watch_and_prote' +
      'ct, map_ref,'#13#10'  (select count(ticket_id) from ticket_version (NO' +
      'LOCK)'#13#10'   where ticket_id = ticket.ticket_id) as transmit_count,' +
      #13#10'area.area_name'#13#10'from ticket'#13#10'left join area on area_id=route_a' +
      'rea_id'#13#10'where ticket_id = :ticket_id'
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    IndexDefs = <>
    Left = 208
    Top = 16
  end
  object InternalTicketVersions: TBetterADODataSet
    Connection = QMBillingDM.Conn
    Parameters = <>
    IndexDefs = <>
    Left = 296
    Top = 16
  end
  object InternalLocates: TBetterADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      'select l.*, a.locator_id, e.emp_number, e.short_name,'#13#10'(select m' +
      'in(locate_status.status_date) from locate_status (NOLOCK)'#13#10'where' +
      ' locate_status.locate_id = l.locate_id and'#13#10'           locate_st' +
      'atus.status <> '#39'-R'#39') as initial_status_date'#13#10'from locate l'#13#10'  le' +
      'ft join assignment a on a.locate_id = l.locate_id '#13#10'  left join ' +
      'employee e on a.locator_id = e.emp_id'#13#10'where l.ticket_id = :tick' +
      'et_id and'#13#10'  a.active = 1'
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    IndexDefs = <>
    Left = 208
    Top = 88
  end
  object TransRec: TBetterADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 'select * from billing_transaction where 0=1'
    Parameters = <>
    IndexDefs = <>
    Left = 40
    Top = 136
  end
  object MarkFailed: TADOCommand
    CommandText = 
      'update billing_queue set failed_processing=1 where queue_id=:que' +
      'ue_id'
    Connection = QMBillingDM.Conn
    Parameters = <
      item
        Name = 'queue_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 128
    Top = 80
  end
  object ReviveFailed: TADOCommand
    CommandText = 
      'update billing_queue set failed_processing=1 where queue_id=:que' +
      'ue_id'
    Connection = QMBillingDM.Conn
    Parameters = <
      item
        Name = 'queue_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 128
    Top = 136
  end
end
