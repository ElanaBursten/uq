unit BillingAnalysis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCurrencyEdit,
  cxCalendar, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, dxSkinsCore,
  dxSkinscxPCPainter, cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinTheBezier, dxDateRanges, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxSkinOffice2019Colorful, dxScrollbarAnnotations, dxSkinBasic, dxSkinOffice2019Black, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White;

type
  TBillingAnalysisForm = class(TBaseCxListForm)
    Gridbilling_detail_id: TcxGridDBColumn;
    Gridbill_id: TcxGridDBColumn;
    Gridlocate_id: TcxGridDBColumn;
    Gridbilling_cc: TcxGridDBColumn;
    Gridticket_number: TcxGridDBColumn;
    Gridwork_county: TcxGridDBColumn;
    Gridwork_state: TcxGridDBColumn;
    Gridwork_city: TcxGridDBColumn;
    Gridwork_address_number: TcxGridDBColumn;
    Gridwork_address_number_2: TcxGridDBColumn;
    Gridwork_address_street: TcxGridDBColumn;
    Gridticket_type: TcxGridDBColumn;
    Gridtransmit_date: TcxGridDBColumn;
    Griddue_date: TcxGridDBColumn;
    Gridstatus: TcxGridDBColumn;
    Gridqty_marked: TcxGridDBColumn;
    Gridclosed_how: TcxGridDBColumn;
    Gridclosed_date: TcxGridDBColumn;
    Gridbill_code: TcxGridDBColumn;
    Gridbucket: TcxGridDBColumn;
    Gridprice: TcxGridDBColumn;
    Gridcon_name: TcxGridDBColumn;
    Gridclient_id: TcxGridDBColumn;
    Gridlocator_id: TcxGridDBColumn;
    Gridwork_done_for: TcxGridDBColumn;
    Gridwork_type: TcxGridDBColumn;
    Gridwork_lat: TcxGridDBColumn;
    Gridwork_long: TcxGridDBColumn;
    Gridrate: TcxGridDBColumn;
    Gridwork_cross: TcxGridDBColumn;
    Gridcall_date: TcxGridDBColumn;
    Gridinitial_status_date: TcxGridDBColumn;
    Gridemp_number: TcxGridDBColumn;
    Gridshort_name: TcxGridDBColumn;
    Gridwork_description: TcxGridDBColumn;
    Gridarea_name: TcxGridDBColumn;
    Gridtransmit_count: TcxGridDBColumn;
    Gridhours: TcxGridDBColumn;
    Gridline_item_text: TcxGridDBColumn;
    Gridrevision: TcxGridDBColumn;
    Gridmap_ref: TcxGridDBColumn;
    Gridlocate_hours_id: TcxGridDBColumn;
    Gridqty_charged: TcxGridDBColumn;
    Gridwhich_invoice: TcxGridDBColumn;
    Gridtax_name: TcxGridDBColumn;
    Gridtax_rate: TcxGridDBColumn;
    Gridtax_amount: TcxGridDBColumn;
    Gridraw_units: TcxGridDBColumn;
    Gridupdate_of: TcxGridDBColumn;
    Gridhigh_profile: TcxGridDBColumn;
    Gridticket_id: TcxGridDBColumn;
    Gridwork_date: TcxGridDBColumn;
    GridParent_id: TcxGridDBColumn;
  public
    procedure ShowBillID(BillID: Integer);
    procedure ActivatingNow; override;
  end;

implementation

uses
  OdHourglass, QMBillingDMu;

{$R *.dfm}

{ TBillingAnalysisForm }

procedure TBillingAnalysisForm.ShowBillID(BillID: Integer);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  Data.Close;
  Data.Parameters.ParamByName('BillID').Value := BillID;
  Data.Open;
end;

procedure TBillingAnalysisForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(False);
end;

end.
