unit BillingShared;

interface

uses
  SysUtils, Classes, DB, ADODB, BillingConst, BillingLocate, StrUtils,
  OdMiscUtils;
function GetWorkHoursBetweenDates(const CallCenter:string; startDate, endDate: TdateTime): double;
function IsFCV(const CallCenter: string): Boolean;
function IsFPK(const CallCenter: string): Boolean;
//function IsFCO(const CallCenter: string): Boolean;
function IsFCL(const CallCenter: string): Boolean;
function IsFGV(const CallCenter: string): Boolean;
function IsFHL(const CallCenter: string): Boolean;
function IsARK(const CallCenter: string): Boolean;  //qm-839 sr
function IsSTSTexas(const CallCenter: string): Boolean;
function IsMemphis(const CallCenter: string): Boolean;
function IsLouisianaCenter(const CallCenter: string): Boolean;
function IsAtlanta(const CallCenter: string): Boolean;
function IsSTSAtlanta(const CallCenter: string): Boolean;
function IsAfterHours(DT: TDateTime; const CallCenter, ClientCode: string): Boolean; overload;
function AfterHoursCalculationDate(Locate: TLocate): TDateTime;
function IsOutsideNormalWorkingHours(Locate: TLocate): Boolean;
function TreatAsAfterHours(Locate: TLocate): Boolean;
function TermGroup(const Group: string; Required: Boolean = True): TStringArray;
function CenterGroup(const Group: string; Required: Boolean = True): TStringArray;
function CenterGroups(Groups: array of string; Required: Boolean = True): TStringArray;
function IsHoliday(Date: TDateTime; const CallCenter: string): Boolean; overload;
function IsHoliday(Locate: TLocate): Boolean; overload;
function IsWeekend(Locate: TLocate): Boolean;
function MatchesValueGroup(const Value, GroupName: string): Boolean;
function MatchesTermGroup(const Value, GroupName: string; Required: Boolean = True): Boolean; overload;
function MatchesTermGroup(Locate: TLocate; const GroupName: string; Required: Boolean = True): Boolean; overload;
function MatchesWorkDoneForGroup(Locate: TLocate; const DoneForValueGroup: string): Boolean;
function MatchesOneOfTermGroups(const Value: string; GroupNames: array of string): Boolean;
function MatchesCenterGroup(Locate: TLocate; const GroupName: string; Required: Boolean = True): Boolean; overload;
function MatchesCenterGroup(const CallCenter, GroupName: string; Required: Boolean = True): Boolean; overload;
function MatchesCenterGroup(CallCenters: TStringArray; const GroupName: string; Required: Boolean = True): Boolean; overload;
function MatchesOneOfCenterGroups(const CallCenter: string; GroupNames: array of string; Required: Boolean = True): Boolean;
function GetPriceFromRateQty(Locate: TLocate; Rate: Double; Qty: Integer): Double;
function GetValueGroupNameForID(const ValueGroupID: Integer): String;
function LocateMatchesTermWorkOrExc(Locate: TLocate; const ATermGroup, AWorkDoneForGroup, AExcavatorGroup: string): Boolean;
function IsFMB1BGEGTerm(Locate: TLocate): Boolean;
function IsFMB1BGENonGTerm(Locate: TLocate): Boolean;
function IsFMWBGEGTerm(Locate: TLocate): Boolean;
function IsFMWBGENonGTerm(Locate: TLocate): Boolean;

implementation

uses
  Variants, DateUtils, QMConst, QMBillingDMu;

function TermGroup(const Group: string; Required: Boolean): TStringArray;
begin
  Result := QMBillingDM.ConfigDM.GetTermGroup(Group, Required);
end;

function CenterGroup(const Group: string; Required: Boolean): TStringArray;
begin
  Result := QMBillingDM.ConfigDM.GetCenterGroup(Group, Required);
end;

function CenterGroups(Groups: array of string; Required: Boolean): TStringArray;
begin
  Result := QMBillingDM.ConfigDM.GetCenterGroups(Groups, Required);
end;

function GetWorkHoursBetweenDates(const CallCenter:string; startDate, endDate: TdateTime): double;
// QM-811 sr
var
  cnt: integer;
  isDayHoliday,isWeekEnd: boolean;
  HoursBetween:Double;

begin
  cnt := 0;
  HoursBetween:=0.0;
  HoursBetween := HourSpan(startDate, endDate);
  while endDate >= startDate do
  begin
    isDayHoliday := false;
    isWeekEnd := false;
    isDayHoliday := QMBillingDM.ConfigDM.IsHoliday(startDate, CallCenter, True); //qm-811
    isWeekEnd := DayOfTheWeek(startDate) > 5;

    if ((isDayHoliday) or (isWeekEnd))
    then HoursBetween:= HoursBetween-24;

    startDate := startDate + 1;
  end;
  result := HoursBetween;
end;

function IsHoliday(Date: TDateTime; const CallCenter: string): Boolean;
begin
  Result := QMBillingDM.ConfigDM.IsHoliday(Date, CallCenter);
end;

function MatchesValueGroup(const Value, GroupName: string): Boolean;
begin
  Result := QMBillingDM.ConfigDM.MatchesValueGroup(Value, GroupName);
end;

function IsFCV(const CallCenter: string): Boolean;
begin
  Result := (CallCenter = 'FCV1') or (CallCenter = 'FCV2') or (CallCenter = 'FCV3') or (CallCenter = 'FCV4');
end;

function IsFPK(const CallCenter: string): Boolean;
begin
  Result := (CallCenter = 'FPK1') or (CallCenter = 'FPK2');
end;

function IsSTSTexas(const CallCenter: string): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup('STSTexas', False));
end;

function IsMemphis(const CallCenter: string): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup('Memphis', False));
end;

//function IsFCO(const CallCenter: string): Boolean;
//begin
//  Result := StrBeginsWith('FCO', CallCenter);
//end;

function IsFCL(const CallCenter: string): Boolean;
begin
  Result := MatchesCenterGroup(CallCenter, 'FCL', False);
end;

function IsARK(const CallCenter: string): Boolean;  //qm-839 sr
begin
  Result := MatchesCenterGroup(CallCenter, 'ARK', False);
end;

function IsFGV(const CallCenter: string): Boolean;
begin
  Result := MatchesCenterGroup(CallCenter, 'FGV', False);
end;

function IsFHL(const CallCenter: string): Boolean;
begin
  Result := StrBeginsWith('FHL', CallCenter);
end;

function IsLouisianaCenter(const CallCenter: string): Boolean;
begin
  Result := MatchesCenterGroup(CallCenter, 'FBL', False) or
    MatchesCenterGroup(CallCenter, 'FNL', False) or
    MatchesCenterGroup(CallCenter, '141', False);
end;

function IsAtlanta(const CallCenter: string): Boolean;
begin
  Result := MatchesCenterGroup(CallCenter, 'Atlanta', False);
end;

function IsSTSAtlanta(const CallCenter: string): Boolean;
begin
  Result := MatchesCenterGroup(CallCenter, 'STSAtlanta', False);
end;

function IsAfterHours(DT: TDateTime; const CallCenter, ClientCode: string): Boolean;
var
  Hour, Min, Sec, MSec: Word;

  function IsFTLAfterHours: Boolean;
  begin
    if ClientCode = 'B05' then // 7:00 am - 4:30 pm
      Result := (Hour < 7) or ((Hour > 16) or ((Hour = 16) and (Min > 30)))
    else // 6:00 am - 3:30 pm
      Result := (Hour < 6) or ((Hour > 15) or ((Hour = 15) and (Min > 30)))
  end;

  function IsFHLAfterHours: Boolean;
  begin
    // 7:00 - 3:30 is the default
    Result := (Hour < 7) or ((Hour > 15) or ((Hour = 15) and (Min > 30)));
    if StrBeginsWith('SUGAR-AT', ClientCode) then // 7:00 - 4:00 (this customer only)
      Result := (Hour < 7) or (Hour >= 16);
    if MatchesTermGroup(ClientCode, 'FHLAfterHours-COP') then // 6:30 am - 3:00 pm
      Result := ((Hour < 6) or ((Hour = 6) and (Min < 30))) or (Hour >= 15)
  end;

  function IsLAfterHours: Boolean;
  begin
    Result := (Hour < 7) or (Hour >= 17); // 7:00 am - 5:00 pm
    if MatchesTermGroup(ClientCode, 'LORAlternateAfterHours') then // This term group is empty as of Oct 2007
      Result := (Hour < 7) or (Hour >= 19);  // 7:00 am - 7:00 pm
  end;

  function IsSCAfterHours: Boolean;
  const
    SpecialAfterHoursTerms: array[0..6] of string = ('142DupTimeWarner',
      '142BellSouthTerm',
      '142DupSCEG',
      '142DupSCEE',
      'FGVTWTerms',
      'FGVDPTerms',
      'FGVTermsATT');
  begin
    Result := ((Hour < 7) or ((Hour = 7) and (Min < 30))) or
      ((Hour > 17) or ((Hour = 17) and (Min >= 30))); // 7:30 am - 5:30 pm is the default
    if MatchesOneOfTermGroups(ClientCode, SpecialAfterHoursTerms) then
      Result := ((Hour < 7) or (Hour >= 17)); // 7:00 am - 5:00 pm for these special case clients
  end;

begin
  DecodeTime(DT, Hour, Min, Sec, MSec);

  if MatchesOneOfCenterGroups(CallCenter, ['LWA', 'LOR', 'LUT', 'LAK', 'LID'], False) then // 7:00 am - 4:00 pm
  begin
    Result := (Hour < 7) or (Hour >= 16); //QM-87
    if MatchesTermGroup(ClientCode, 'LORPGEAfterHours') then
    Result := (Hour < 7) or (Hour >= 17); // 7:00 am - 5:00 pm  QM-108
  end
  else if MatchesOneOfCenterGroups(CallCenter, ['FMB', 'FMW', 'FDE', 'OCC'], False) then // 6:00 am - 6:00 pm  QM-111
  begin
    if MatchesTermGroup(ClientCode, 'FMBVerizonTerms')or
       MatchesTermGroup(ClientCode, 'FMWVerizonTerms')or
       MatchesTermGroup(ClientCode, 'FDEVerizonTerms')  then
    Result := (Hour < 6) or (Hour >= 18); //QM-111

    if MatchesTermGroup(ClientCode, 'FMBPepcoTerms')then
    Result := (Hour < 7) or (Hour >= 17)


  end
  else if StringInArray(CallCenter, CenterGroup('FTLFJTFCTCallCenters', False)) then
    Result := IsFTLAfterHours
  else if IsFHL(CallCenter) then
    Result := IsFHLAfterHours
  else if (CallCenter= '1851') then
    Result := (Hour < 7) or (Hour >= 17)
//  else if IsFCO(CallCenter) and MatchesOneOfTermGroups(ClientCode, ['FCOSpecialAfterHoursTerms', 'FCOSpecialAfterHoursSat']) then
//    Result := (Hour < 7) or (Hour >= 19) // 7:00 am - 7:00 pm
  else if MatchesOneOfCenterGroups(CallCenter, ['1421','FGV'], False) then
    Result := IsSCAfterHours
  else if (MatchesCenterGroup(CallCenter, 'OCC', False)) or
    (CallCenter = 'FPL1') or //qm-679
//    (CallCenter = 'FCO1') or
    (CallCenter = 'FOK1') or
    (CallCenter = 'FDE1') or    //non Verizon
    (CallCenter = 'FDE2') or    //non Verizon
    (CallCenter = 'FMS1') or
    (CallCenter = 'FJL1') or
    (CallCenter = 'FAQ1') or
    (CallCenter = 'FAQ2') or
    (CallCenter = 'FWP1') or
    (CallCenter = 'FWP3') or
    (CallCenter = '7501') or
    (CallCenter = 'FDX1') or
    (CallCenter = '9001') or
    (CallCenter = '9101') or
    (CallCenter = '9301') or
    (CallCenter = '9401') or
    (CallCenter = 'OUPS1') or  //QMANTWO-637
    (CallCenter = 'OUPS2') or  //QMANTWO-637
    IsSTSTexas(CallCenter) or
    IsMemphis(CallCenter) or
    IsAtlanta(CallCenter) or
    IsSTSAtlanta(CallCenter) or
    IsFCL(CallCenter) or
    MatchesCenterGroup(CallCenter, 'STSNewMexico', False) or
    IsLouisianaCenter(CallCenter) then
    Result := (Hour < 7) or (Hour >= 17) // 7:00 am - 5:00 pm
  else if (MatchesCenterGroup(CallCenter, 'CACallCenters', False) and (AnsiMatchStr(ClientCode, BillingConst.ATnTTransOvrd)))  then
    Result :=  (Hour < 6) or (Hour >= 19)  //QMANTWO-594
  else if (MatchesCenterGroup(CallCenter, 'CACallCenters', False) and not(AnsiMatchStr(ClientCode, BillingConst.ATnTTransOvrd)))  then
    Result := (Hour < 7) or (Hour >= 17)
  else if (CallCenter = 'FMW1') or
    (CallCenter = 'FMW3') or
    (CallCenter = 'FMB1') or
    (CallCenter = 'FNV1') then // 7:00 am - 4:00 pm
    Result := (Hour < 7) or (Hour >= 16)
  else if (CallCenter = CallCenterNewJersey) and MatchesTermGroup(ClientCode, 'NJUTerms') then
    Result := (Hour < 7) or ((Hour = 7) and (Min < 30)) or (Hour >= 17) // 7:30 am - 5:00 pm
  else // 8:00 am - 4:00 pm
    Result := (Hour < 8) or (Hour >= 16);
end;

function IsLocateAfterHours(Locate: TLocate): Boolean;
begin
  Result := IsAfterHours(AfterHoursCalculationDate(Locate), Locate.CallCenter, Locate.ClientCode);
end;

function AfterHoursCalculationDate(Locate: TLocate): TDateTime;
begin
  Result := Locate.CallDate;
  if MatchesCenterGroup(Locate, 'AfterHoursByTransmitDate', False) or (Result < 0.001) then
    Result := Locate.TransmitDate
  else if MatchesCenterGroup(Locate, 'AfterHoursByWorkDate', False) and (Locate.WorkDate > 0.001) then
    Result := Locate.WorkDate;
end;

// Determine if a locate's date is outside the normal working hours
// Usually this is weekends/holidays and before/after weekday business hours
function IsOutsideNormalWorkingHours(Locate: TLocate): Boolean;
begin
  if MatchesCenterGroup(Locate, 'AfterHoursByWorkAndCallDates', False) then
  begin
    Result := (IsAfterHours(Locate.CallDate, Locate.CallCenter,
      Locate.ClientCode) or OdMiscUtils.IsWeekend(Locate.CallDate) or
      IsHoliday(Locate.CallDate, Locate.CallCenter)) and
      (IsAfterHours(Locate.WorkDate, Locate.CallCenter, Locate.ClientCode) or
      OdMiscUtils.IsWeekend(Locate.WorkDate) or IsHoliday(Locate.WorkDate,
      Locate.CallCenter));
  end   { TODO 3 -oSR -cDateTime :
Need to Add a After Hours By Close And Call Dates Test.
See FDE }
  else
    Result := IsWeekend(Locate) or IsLocateAfterHours(Locate) or
      IsHoliday(Locate);

  if Result and ((MatchesCenterGroup(Locate, '1421', False) and
    MatchesOneOfTermGroups(Locate.ClientCode, ['142DupSCEE', '142DupSCEG'])) or
    (MatchesCenterGroup(Locate, 'FCL', False) and MatchesTermGroup(Locate,
    'FCLTermsATT', False)) or (MatchesCenterGroup(Locate, 'STSAtlanta', False)
    and MatchesTermGroup(Locate, '300DupAGL', False))) then
  begin
    // Both the normal time and the first close date must be after hours to bill as after hours
    Result := Result and (Locate.FirstCloseDate > 0.001) and
      (IsAfterHours(Locate.FirstCloseDate, Locate.CallCenter, Locate.ClientCode)
      or OdMiscUtils.IsWeekend(Locate.FirstCloseDate) or
      IsHoliday(Locate.FirstCloseDate, Locate.CallCenter));
  end
end;

function TreatAsAfterHours(Locate: TLocate): Boolean;
begin
  Result := IsOutsideNormalWorkingHours(Locate) or Locate.IsManualAfterHoursType;
end;

function IsHoliday(Locate: TLocate): Boolean;
begin
  Result := IsHoliday(AfterHoursCalculationDate(Locate), Locate.CallCenter);
end;

function IsWeekend(Locate: TLocate): Boolean;
begin
  // Two groups of locates consider Saturday as a normal work day
//  if IsFCO(Locate.CallCenter) and MatchesTermGroup(Locate, 'FCOSpecialAfterHoursSat') then
//    Result := DayOfTheWeek(AfterHoursCalculationDate(Locate)) = DaySunday
//  else
    Result := OdMiscUtils.IsWeekend(AfterHoursCalculationDate(Locate));
end;

function MatchesTermGroup(const Value, GroupName: string; Required: Boolean): Boolean;
begin
  Result := StringInArray(Value, TermGroup(GroupName, Required));
end;

function MatchesOneOfTermGroups(const Value: string; GroupNames: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(GroupNames) to High(GroupNames) do begin
    if MatchesTermGroup(Value, GroupNames[i]) then begin
      Result := True;
      Break;
    end;
  end;
end;

function MatchesTermGroup(Locate: TLocate; const GroupName: string; Required: Boolean): Boolean;
begin
  Assert(Assigned(Locate));
  Result := MatchesTermGroup(Locate.ClientCode, GroupName, Required);
end;

function MatchesWorkDoneForGroup(Locate: TLocate; const DoneForValueGroup: string): Boolean;
begin
  Result := MatchesValueGroup(Locate.WorkDoneFor, DoneForValueGroup);
end;

function MatchesCenterGroup(const CallCenter, GroupName: string; Required: Boolean = True): Boolean;
begin
  Result := StringInArray(CallCenter, CenterGroup(GroupName, Required));
end;

function MatchesCenterGroup(Locate: TLocate; const GroupName: string; Required: Boolean): Boolean;
begin
  Assert(Assigned(Locate));
  Result := StringInArray(Locate.CallCenter, CenterGroup(GroupName, Required));
end;

function MatchesCenterGroup(CallCenters: TStringArray; const GroupName: string; Required: Boolean): Boolean;
var
  i: Integer;
  Center: string;
begin
  Result := False;
  for i := Low(CallCenters) to High(CallCenters) do begin
    Center := CallCenters[i];
    if MatchesCenterGroup(Center, GroupName, Required) then begin
      Result := True;
      Break;
    end;
  end;
end;

function MatchesOneOfCenterGroups(const CallCenter: string; GroupNames: array of string; Required: Boolean): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(GroupNames) to High(GroupNames) do begin
    if MatchesCenterGroup(CallCenter, GroupNames[i], Required) then begin
      Result := True;
      Break;
    end;
  end;
end;

function GetPriceFromRateQty(Locate: TLocate; Rate: Double; Qty: Integer): Double;
begin
  Result := Rate * Qty;
end;

function GetValueGroupNameForID(const ValueGroupID: Integer): String;
begin
  Result := QMBillingDM.ConfigDM.GetValueGroupNameFromID(ValueGroupID);
end;

function LocateMatchesTermWorkOrExc(Locate: TLocate; const ATermGroup, AWorkDoneForGroup, AExcavatorGroup: string): Boolean;
begin
  Result := StringInArray(Locate.ClientCode, TermGroup(ATermGroup))
    and (MatchesValueGroup(Locate.WorkDoneFor, AWorkDoneForGroup)
         or MatchesValueGroup(Locate.Excavator, AExcavatorGroup));
end;

function IsFMB1BGEGTerm(Locate: TLocate): Boolean;
begin
  Result := (Locate.CallCenter = 'FMB1') and MatchesTermGroup(Locate, 'FMBBGEETerms', False)  //Electric Terms
    and (MatchesTermGroup(Locate, 'FMBBGEGTerms', False)); //Gas Terms
end;

function IsFMB1BGENonGTerm(Locate: TLocate): Boolean;
begin
  Result := (Locate.CallCenter = 'FMB1') and MatchesTermGroup(Locate, 'FMBBGEETerms', False)  //Electric Terms
    and (not MatchesTermGroup(Locate, 'FMBBGEGTerms', False));  //Gas Terms
end;

function IsFMWBGEGTerm(Locate: TLocate): Boolean;
begin
  Result := MatchesCenterGroup(Locate, 'FMW')
    and MatchesTermGroup(Locate, 'FMWBGETerms', False)
    and (MatchesTermGroup(Locate, 'FMWBGEGTerms', False));
end;

function IsFMWBGENonGTerm(Locate: TLocate): Boolean;
begin
  Result := MatchesCenterGroup(Locate, 'FMW')
    and MatchesTermGroup(Locate, 'FMWBGETerms', False)
    and (not MatchesTermGroup(Locate, 'FMWBGEGTerms', False));
end;

end.
