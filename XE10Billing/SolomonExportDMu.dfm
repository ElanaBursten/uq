object SolomonExportDM: TSolomonExportDM
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 223
  Width = 463
  object GLCodes: TADODataSet
    Connection = BillingEngineDM.Conn
    CommandText = 'select *'#13#10'from billing_gl'#13#10'where active=1'
    Parameters = <>
    Left = 40
    Top = 16
  end
  object DynamicsExport: TADOStoredProc
    CacheSize = 100
    Connection = BillingEngineDM.Conn
    CursorType = ctOpenForwardOnly
    CommandTimeout = 180
    ProcedureName = 'export_tasks;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CompanyName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ProfitCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@StartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 41
    Top = 70
  end
  object qryDycomCustNo: TADOQuery
    Connection = BillingEngineDM.Conn
    Parameters = <
      item
        Name = 'OutputConfigId'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      '  select'
      
        '    case when boc.dycom_customer_number is null or boc.dycom_cus' +
        'tomer_number='#39#39
      '    then c.dycom_customer_number'
      '    else boc.dycom_customer_number end as dycomCustomerNo'
      '  from billing_output_config boc'
      '    inner join customer c on boc.customer_id = c.customer_id'
      '  where boc.output_config_id= :OutputConfigId')
    Left = 40
    Top = 152
  end
  object qryGLflip: TADOQuery
    Connection = BillingEngineDM.Conn
    Parameters = <
      item
        Name = 'TaskID'
        DataType = ftString
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'use QM;  '
      'select  PL_GLcode = case R.ref_id'
      #9'When 1189 then 30145'
      #9'When 1675 then 30150 '
      #9#9#9'end '
      'from employee e'
      'join reference r on (e.type_id = R.ref_id)'
      'where r.code in ('#39'PL1'#39','#39'PL3'#39')'
      'and e.emp_number=  Substring(:TaskID,11,16)')
    Left = 256
    Top = 72
  end
end
