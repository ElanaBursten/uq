object frmBillAnalysis2: TfrmBillAnalysis2
  Left = 0
  Top = 0
  Caption = 'Billing Analysis'
  ClientHeight = 417
  ClientWidth = 946
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 946
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Grid: TcxGrid
    Left = 0
    Top = 41
    Width = 946
    Height = 376
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object GridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmAlways
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DS
      DataController.Filter.MaxValueListCount = 10
      DataController.KeyFieldNames = 'billing_detail_id'
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = 'Rate $0.0'
          Kind = skSum
          FieldName = 'rate'
          Column = Gridrate
          DisplayText = 'Rate'
        end
        item
          Format = 'Price $0.0'
          Kind = skSum
          FieldName = 'price'
          Column = Gridprice
          DisplayText = 'Price'
        end
        item
          Format = 'Marked 0'
          Kind = skSum
          FieldName = 'qty_marked'
          Column = Gridqty_marked
          DisplayText = 'Marked'
        end
        item
          Format = 'Charged 0'
          Kind = skSum
          FieldName = 'qty_charged'
          Column = Gridqty_charged
          DisplayText = 'Charged'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = 'Count 0'
          Kind = skCount
          FieldName = 'locate_id'
          Column = Gridlocate_id
        end
        item
          Format = 'Avg 0.00'
          Kind = skAverage
          FieldName = 'rate'
          Column = Gridrate
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'price'
          Column = Gridprice
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'qty_marked'
          Column = Gridqty_marked
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'qty_charged'
          Column = Gridqty_charged
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'hours'
        end
        item
          Format = 'Avg 0.00'
          Kind = skAverage
          FieldName = 'tax_rate'
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'tax_amount'
        end
        item
          Format = 'Sum 0.00'
          Kind = skSum
          FieldName = 'raw_units'
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.MRUItemsList = False
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = Gridticket_number
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
      OptionsView.Footer = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object Gridbilling_cc: TcxGridDBColumn
        Caption = 'Term'
        DataBinding.FieldName = 'billing_cc'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 54
      end
      object Gridticket_number: TcxGridDBColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 95
      end
      object Gridrevision: TcxGridDBColumn
        Caption = 'Revision'
        DataBinding.FieldName = 'revision'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 66
      end
      object Gridstatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 40
      end
      object Gridticket_type: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'ticket_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 90
      end
      object Gridticket_id: TcxGridDBColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 50
      end
      object Gridlocate_id: TcxGridDBColumn
        Caption = 'Loc ID'
        DataBinding.FieldName = 'locate_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 38
      end
      object Gridrate: TcxGridDBColumn
        Caption = 'Rate'
        DataBinding.FieldName = 'rate'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 52
      end
      object Gridprice: TcxGridDBColumn
        Caption = 'Price'
        DataBinding.FieldName = 'price'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object Gridbill_code: TcxGridDBColumn
        Caption = 'Bill Code'
        DataBinding.FieldName = 'bill_code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 47
      end
      object Gridbucket: TcxGridDBColumn
        Caption = 'Bucket'
        DataBinding.FieldName = 'bucket'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 104
      end
      object Gridline_item_text: TcxGridDBColumn
        Caption = 'Line Item Text'
        DataBinding.FieldName = 'line_item_text'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 120
      end
      object Gridqty_marked: TcxGridDBColumn
        Caption = 'Qty Mark'
        DataBinding.FieldName = 'qty_marked'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 66
      end
      object Gridhours: TcxGridDBColumn
        Caption = 'Hours'
        DataBinding.FieldName = 'hours'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 1
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 32
      end
      object Gridqty_charged: TcxGridDBColumn
        Caption = 'Qty Chg'
        DataBinding.FieldName = 'qty_charged'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 61
      end
      object Gridcall_date: TcxGridDBColumn
        Caption = 'Call Date'
        DataBinding.FieldName = 'call_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 89
      end
      object Gridwork_date: TcxGridDBColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'work_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 104
      end
      object Gridcon_name: TcxGridDBColumn
        Caption = 'Contractor'
        DataBinding.FieldName = 'con_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 100
      end
      object Gridclient_id: TcxGridDBColumn
        Caption = 'Client ID'
        DataBinding.FieldName = 'client_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridlocator_id: TcxGridDBColumn
        Caption = 'Locator ID'
        DataBinding.FieldName = 'locator_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 56
      end
      object Gridwork_done_for: TcxGridDBColumn
        Caption = 'Work Done For'
        DataBinding.FieldName = 'work_done_for'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 103
      end
      object Gridtransmit_date: TcxGridDBColumn
        Caption = 'Transmit Date'
        DataBinding.FieldName = 'transmit_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Griddue_date: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Gridclosed_date: TcxGridDBColumn
        Caption = 'Closed Date'
        DataBinding.FieldName = 'closed_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 112
      end
      object Gridinitial_status_date: TcxGridDBColumn
        Caption = 'Initial Status Date'
        DataBinding.FieldName = 'initial_status_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        FooterAlignmentHorz = taRightJustify
        Width = 112
      end
      object Gridclosed_how: TcxGridDBColumn
        Caption = 'Closed How'
        DataBinding.FieldName = 'closed_how'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 63
      end
      object Gridwork_type: TcxGridDBColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 113
      end
      object Gridwork_lat: TcxGridDBColumn
        Caption = 'Work Lat'
        DataBinding.FieldName = 'work_lat'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 4
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridwork_long: TcxGridDBColumn
        Caption = 'Work Long'
        DataBinding.FieldName = 'work_long'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 4
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 55
      end
      object Gridwork_cross: TcxGridDBColumn
        Caption = 'Work Cross'
        DataBinding.FieldName = 'work_cross'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 85
      end
      object Gridwork_address_number: TcxGridDBColumn
        Caption = 'Address Number'
        DataBinding.FieldName = 'work_address_number'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 113
      end
      object Gridwork_address_number_2: TcxGridDBColumn
        Caption = 'Address Number 2'
        DataBinding.FieldName = 'work_address_number_2'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 125
      end
      object Gridwork_address_street: TcxGridDBColumn
        Caption = 'Addr Street Name'
        DataBinding.FieldName = 'work_address_street'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 274
      end
      object Gridwork_city: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 82
      end
      object Gridwork_state: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'work_state'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 58
      end
      object Gridwork_county: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 72
      end
      object Gridemp_number: TcxGridDBColumn
        Caption = 'Emp Number'
        DataBinding.FieldName = 'emp_number'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 94
      end
      object Gridshort_name: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 124
      end
      object Gridwork_description: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'work_description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 114
      end
      object Gridarea_name: TcxGridDBColumn
        Caption = 'Area Name'
        DataBinding.FieldName = 'area_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 82
      end
      object Gridtransmit_count: TcxGridDBColumn
        Caption = 'Transmit Count'
        DataBinding.FieldName = 'transmit_count'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 75
      end
      object Gridmap_ref: TcxGridDBColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 72
      end
      object Gridlocate_hours_id: TcxGridDBColumn
        Caption = 'Locate Hours ID'
        DataBinding.FieldName = 'locate_hours_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 81
      end
      object Gridwhich_invoice: TcxGridDBColumn
        Caption = 'Which Invoice'
        DataBinding.FieldName = 'which_invoice'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 124
      end
      object Gridtax_name: TcxGridDBColumn
        Caption = 'Tax Name'
        DataBinding.FieldName = 'tax_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 77
      end
      object Gridtax_rate: TcxGridDBColumn
        Caption = 'Tax Rate'
        DataBinding.FieldName = 'tax_rate'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object Gridtax_amount: TcxGridDBColumn
        Caption = 'Tax Amount'
        DataBinding.FieldName = 'tax_amount'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 124
      end
      object Gridraw_units: TcxGridDBColumn
        Caption = 'Raw Units'
        DataBinding.FieldName = 'raw_units'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 78
      end
      object Gridupdate_of: TcxGridDBColumn
        Caption = 'Update Of'
        DataBinding.FieldName = 'update_of'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Width = 77
      end
      object Gridhigh_profile: TcxGridDBColumn
        Caption = 'High Profile'
        DataBinding.FieldName = 'high_profile'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 100
      end
      object Gridbill_id: TcxGridDBColumn
        Caption = 'Bill ID'
        DataBinding.FieldName = 'bill_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 32
      end
      object GridParent_id: TcxGridDBColumn
        DataBinding.FieldName = 'parent_ticket_id'
        DataBinding.IsNullValueType = True
      end
      object Gridbilling_detail_id: TcxGridDBColumn
        Caption = 'Billing Detail ID'
        DataBinding.FieldName = 'billing_detail_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 75
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select bd.*, t.ticket_id, t.work_date'#13#10'from billing_detail bd'#13#10' ' +
      ' left join locate l on l.locate_id = bd.locate_id'#13#10'  left join t' +
      'icket t on t.ticket_id = l.ticket_id'#13#10'where bd.bill_id = :BillID' +
      ' and ((bd.area_name <> '#39'DEL_GAS'#39') or (bd.area_name is null))'
    Parameters = <
      item
        Name = 'BillID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 56
    Top = 112
  end
  object DS: TDataSource
    DataSet = Data
    Left = 96
    Top = 112
  end
  object ADODataSet1: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select bd.*, t.ticket_id, t.work_date'#13#10'from billing_detail bd'#13#10' ' +
      ' left join locate l on l.locate_id = bd.locate_id'#13#10'  left join t' +
      'icket t on t.ticket_id = l.ticket_id'#13#10'where bd.bill_id = :BillID' +
      ' and ((bd.area_name <> '#39'DEL_GAS'#39') or (bd.area_name is null))'
    Parameters = <
      item
        Name = 'BillID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 64
    Top = 120
  end
  object DataSource1: TDataSource
    DataSet = ADODataSet1
    Left = 104
    Top = 120
  end
  object dxSkinController1: TdxSkinController
    Left = 544
    Top = 264
  end
end
