unit ValueGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMasterDetail, ActnList, DB, ADODB, StdCtrls, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxMaskEdit,
  cxDropDownEdit, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  System.Actions, dxSkinsCore, cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges, dxScrollbarAnnotations;

type
  TValueGroupForm = class(TBaseMasterDetailForm)
    ColMasterValueGroupID: TcxGridDBColumn;
    ColMasterGroupCode: TcxGridDBColumn;
    ColMasterComment: TcxGridDBColumn;
    ColDetailValueGroupID: TcxGridDBColumn;
    ColDetailMatchType: TcxGridDBColumn;
    ColDetailMatchValue: TcxGridDBColumn;
    procedure ColDetailMatchValueChange(Sender: TObject);
  public
    procedure PopulateDropdowns; override;
  end;

implementation

uses
  QMBillingDMu, OdDbUtils, QMConst, OdCxUtils;

{$R *.dfm}

procedure TValueGroupForm.PopulateDropdowns;
begin
  inherited;
  QMBillingDM.ConfigDM.GetValueMatchTypeList(CxComboBoxItems(ColDetailMatchType));
end;

procedure TValueGroupForm.ColDetailMatchValueChange(Sender: TObject);
begin
  inherited;
  if Detail.FieldByName('match_type').IsNull then begin
    EditDataSet(Detail);
    Detail.FieldByName('match_type').AsString := MatchTypeIS;
  end;
end;

end.
