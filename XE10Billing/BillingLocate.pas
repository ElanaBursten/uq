unit BillingLocate;

interface

uses
  DB, SysUtils, Classes, Contnrs;

type
  TNameValueSource = class;

  TDataSetArray = array of TDataSet;

  TLocate = class(TPersistent)
  private
    FLocateID: Integer;
    FClientCode: string;
    FCallCenter: string;
    FStatus: string;
    FTicketID: Integer;
    FTicketNumber: string;
    FRate: Currency;
    FOvertimeRate: Currency;
    FQtyMarked: Integer;
    FPrice: Currency;
    FInvoiced: Boolean;
    FClosed: Boolean;
    FWorkLong: Double;
    FWorkLat: Double;
    FQtyCharged: Integer;
    FClientID: Integer;
    FLocatorID: Integer;
    FEmpNumber: string;
    FShortName: string;
    FScreenCharge: Currency;
    FClosedHow: string;
    FExcavator: string;
    FWorkAddressNumber: string;
    FBucket: string;
    FWorkAddressNumber2: string;
    FWorkDoneFor: string;
    FTicketType: string;
    FBillingTicketType: string;
    FWorkCross: string;
    FWorkType: string;
    FBillCode: string;
    FWorkState: string;
    FWorkAddressStreet: string;
    FWorkCity: string;
    FAreaName: string;
    FBillingClientCode: string;
    FWorkCounty: string;
    FClosedDate: TDateTime;
    FInitialStatusDate: TDateTime;
    FDueDate: TDateTime;
    FTransmitDate: TDateTime;
    FCallDate: TDateTime;
    FWorkDate: TDateTime;
    FLate: Boolean;
    FParentTicketID: Integer;
    FParties: Cardinal;
    FTransmitCount: Cardinal;
    FWorkDescription: string;
    FWorkRemarks: string;
    FHours: Currency;
    FWatchAndProtect: Boolean;
    FLineItemText: string;
    FRevision: string;
    FMapRef: string;
    FLocateHoursID: Integer;
    FWhichInvoice: string;
    FTaxName: string;
    FTaxRate: Currency;
    FTaxAmount: Currency;
    FHighProfile: Boolean;
    FIsExtraCharge: Boolean;
    FAdditionalQtyRate: Currency;
    FAdditionalQty: Integer;
    FSource: string;
    FRawUnits: Integer;
    FUpdateOf: string;
    FBillingCity: string;
    FWasEverLate: Boolean;
    FPlat: string;
    FInitialArrivalDate: TDateTime;
    FUnitsMarked: Integer; // Sum of all locate_hours units added to the locate (usually # of feet)
    FFirstCloseDate: TDateTime;
    FLocateAddedBy: string;
    FRateValueGroupID: Integer;
    FAdditionalLineItemText: string;
    fTixIsDup: boolean;
    fRefID: integer;
    FCWICode: string;   //qm-948
    FAddlCWICode: string;
    FAdditional_RPT_GL: string;
    FRPT_GL: string;   //qm-948
//    FBillingRateID: Integer;  //qm-948
    procedure SetFieldsFromSource(Source: TNameValueSource);


  public
    constructor CreateFromDataSets(D: TDataSetArray); overload;
    constructor CreateFromDataSets(D1, D2: TDataSet); overload;
    constructor CreateFromDataSet(D: TDataSet);
    procedure StoreInDataSet(D: TDataSet);
    function IsEmergency: Boolean;
    function IsEmergencyOrManualAfterHoursType: Boolean;
    function IsEmergencyCancel: Boolean; //qm-308
    function IsManualTicket: Boolean;
    function IsManualAfterHoursType: Boolean;
    function IsWatchAndProtect: Boolean;
    function IsDamage: Boolean;
    function IsUpdate: Boolean;
    function IsAfterHoursType: Boolean;
    function IsCancellation: Boolean;
    function IsCancel: Boolean;
    function SetBillCode(const Value: string): Boolean;
    function IsDailyUnitsHours: Boolean;
    function IsFTTP: Boolean;
    function IsFIOS: Boolean;
    function IsHourlyFTTP: Boolean;
    function IsHourly: Boolean;
    function MatchesTermPrefixSuffix(const Prefix, Suffix: string): Boolean;
    function GetInsertStatement(Values: Boolean; const TableName: string; BillID: Integer): string;
    function IsFromCallCenter: Boolean;
    function IsFromClient: Boolean;
    function CreateExtraChargeCopy: TLocate;
    function HighProfileBit: Integer; // 0 or 1, for SQL
    function IsNoShow: Boolean;
    function IsProject: Boolean;
    function IsLateNotice: Boolean;
    function IsSurvey: Boolean;
    function IsMeetingRequest: Boolean;
    function IsDesign: Boolean;
    function IsShortNotice: Boolean;
    function IsAudit: Boolean;  //qm-244 sr
    function IsTraining: Boolean; //qm-244 sr
    function IsRemark: Boolean;   //qm-884 sr
  published
    // Everything published can be copied using ClonePersistent
    // Properties directly downloaded from the database:
    property LocateID: Integer read FLocateID write FLocateID;
    property ClientID: Integer read FClientID write FClientID;
    property CallCenter: string read FCallCenter write FCallCenter;
    property Status: string read FStatus write FStatus;
    property ClientCode: string read FClientCode write FClientCode;
    property Closed: Boolean read FClosed write FClosed;
    property QtyMarked: Integer read FQtyMarked write FQtyMarked;
    property TicketNumber: string read FTicketNumber write FTicketNumber;
    property WorkState: string read FWorkState write FWorkState;
    property WorkCounty: string read FWorkCounty write FWorkCounty;
    property WorkCity: string read FWorkCity write FWorkCity;
    property WorkAddressNumber: string read FWorkAddressNumber write FWorkAddressNumber;
    property WorkAddressNumber2: string read FWorkAddressNumber2 write FWorkAddressNumber2;
    property WorkAddressStreet: string read FWorkAddressStreet write FWorkAddressStreet;
    property WorkCross: string read FWorkCross write FWorkCross;
    property WorkLat: Double read FWorkLat write FWorkLat;
    property WorkLong: Double read FWorkLong write FWorkLong;
    property AreaName: string read FAreaName write FAreaName;
    property Excavator: string read FExcavator write FExcavator; //	'con_name' field, excavator, name of contractor, company doing work
    property WorkDoneFor: string read FWorkDoneFor write FWorkDoneFor; // 'company' field
    property WorkType: string read FWorkType write FWorkType;
    property TicketID: Integer read FTicketID write FTicketID;
    property TransmitDate: TDateTime read FTransmitDate write FTransmitDate;
    property CallDate: TDateTime read FCallDate write FCallDate;
    property WorkDate: TDateTime read FWorkDate write FWorkDate;
    property DueDate: TDateTime read FDueDate write FDueDate;
    property ClosedDate: TDateTime read FClosedDate write FClosedDate;
    property InitialStatusDate: TDateTime read FInitialStatusDate write FInitialStatusDate;
    property TicketType: string read FTicketType write FTicketType;
    property ClosedHow: string read FClosedHow write FClosedHow;
    property LocatorID: Integer read FLocatorID write FLocatorID;
    property EmpNumber: string read FEmpNumber write FEmpNumber;
    property ShortName: string read FShortName write FShortName;
    property Invoiced: Boolean read FInvoiced write FInvoiced;
    property ParentTicketID: Integer read FParentTicketID write FParentTicketID;
    property WorkDescription: string read FWorkDescription write FWorkDescription;
    property WorkRemarks: string read FWorkRemarks write FWorkRemarks;
    property TransmitCount: Cardinal read FTransmitCount write FTransmitCount;
    property MapRef: string read FMapRef write FMapRef;
    property Source: string read FSource write FSource;
    property RawUnits: Integer read FRawUnits write FRawUnits;
    property UpdateOf: string read FUpdateOf write FUpdateOf;
    property WasEverLate: Boolean read FWasEverLate write FWasEverLate;
    property Plat: string read FPlat write FPlat;
    property InitialArrivalDate: TDateTime read FInitialArrivalDate write FInitialArrivalDate;
    property UnitsMarked: Integer read FUnitsMarked write FUnitsMarked;
    property FirstCloseDate: TDateTime read FFirstCloseDate write FFirstCloseDate;
    property LocateAddedBy: string read FLocateAddedBy write FLocateAddedBy;
    property RefID: integer read fRefID write fRefID;//QMANTWO-710
    // Places to add new data fields: billing_detail, LocateRawData/TicketRawData, TLocate.SetFieldsFromSource, TLocate.StoreInDataSet, TLocate.GetInsertStatement, TBillingEngine.StoreDetail

    // Properties calculated during billing/processing:
    property QtyCharged: Integer read FQtyCharged write FQtyCharged;
    property BillingClientCode: string read FBillingClientCode write FBillingClientCode;
    property BillingTicketType: string read FBillingTicketType write FBillingTicketType;
    property BillingCity: string read FBillingCity write FBillingCity; // Used to send LWA-specific  GasOnly/ElecOnly/Both to the rating function
    property Bucket: string read FBucket write FBucket;
    property Price: Currency read FPrice write FPrice;
    property Rate: Currency read FRate write FRate;
    property OvertimeRate: Currency read FOvertimeRate write FOvertimeRate;
    property ScreenCharge: Currency read FScreenCharge write FScreenCharge;
    property Late: Boolean read FLate write FLate;
    property Parties: Cardinal read FParties write FParties;
    property Hours: Currency read FHours write FHours;
    property WatchAndProtect: Boolean read FWatchAndProtect write FWatchAndProtect;
    property LineItemText: string read FLineItemText write FLineItemText;
    property Revision: string read FRevision write FRevision;
    property BillCode: string read FBillCode write FBillCode;
    property LocateHoursID: Integer read FLocateHoursID write FLocateHoursID;
    property WhichInvoice: string read FWhichInvoice write FWhichInvoice;
    property TaxName: string read FTaxName write FTaxName;
    property TaxRate: Currency read FTaxRate write FTaxRate;
    property TaxAmount: Currency read FTaxAmount write FTaxAmount;
    property HighProfile: Boolean read FHighProfile write FHighProfile;
    property AdditionalQtyRate: Currency read FAdditionalQtyRate write FAdditionalQtyRate;
    property AdditionalQty: Integer read FAdditionalQty write FAdditionalQty;
    property IsExtraCharge: Boolean read FIsExtraCharge write FIsExtraCharge;
    property RateValueGroupID: Integer read FRateValueGroupID write FRateValueGroupID;
    property AdditionalLineItemText: string read FAdditionalLineItemText write FAdditionalLineItemText;
    property TixIsDup :boolean  read fTixIsDup write fTixIsDup;

//    property BillingRateID :Integer  read FBillingRateID write FBillingRateID; //qm-948
    property CWICode: string read FCWICode write FCWICode;                     //qm-948
    property AddlCWICode: string read FAddlCWICode write FAddlCWICode;        //qm-948
    property RPT_GL: string read FRPT_GL write FRPT_GL;                     //qm-995
    property Additional_RPT_GL: string read FAdditional_RPT_GL write FAdditional_RPT_GL;//qm-995

  end;

  TNameValueSource = class
  public
    function GetString(const FN: string): string; virtual; abstract;
    function GetInteger(const FN: string): Integer; virtual; abstract;
    function GetDateTime(const FN: string): TDateTime; virtual; abstract;
    function GetBoolean(const FN: string): Boolean; virtual; abstract;
    function GetDouble(const FN: string): Double; virtual; abstract;
    function FieldExists(const FN: string): Boolean; virtual; abstract;
  end;

  TDataSetNameValueSource = class(TNameValueSource)
  private
    FDataSets: TDataSetArray;
    function GetFieldValue(const FieldName: string): Variant;
  public
    function GetString(const FN: string): string; override;
    function GetInteger(const FN: string): Integer; override;
    function GetDateTime(const FN: string): TDateTime; override;
    function GetBoolean(const FN: string): Boolean; override;
    function GetDouble(const FN: string): Double; override;
    function FieldExists(const FN: string): Boolean; override;
    constructor Create(DS: TDataSetArray);
  end;

  TCsvNameValueSource = class(TNameValueSource)
  private
    FFieldList: TStrings;
    FData: TStrings;
  public
    function GetString(const FN: string): string; override;
    function GetInteger(const FN: string): Integer; override;
    function GetDateTime(const FN: string): TDateTime; override;
    function GetBoolean(const FN: string): Boolean; override;
    function GetDouble(const FN: string): Double; override;
    procedure SetData(S: string);
    function FieldExists(const FN: string): Boolean; override;
    constructor Create(FieldList: TStrings);
    destructor Destroy; override;
  end;

  TLocateReader = class
  private
    FSource: TCsvNameValueSource;
    procedure SetupFields(S: string);
    function ReadLocate(S: string): TLocate;

  public
    function ReadFromFile(FileName: string): TObjectList;
    destructor Destroy; override;
  end;

implementation

uses OdMiscUtils, QMConst, OdDbUtils, OdIsoDates, StrUtils, BillingConst,
  BillingShared;

const
  CacheFieldLimit = 300;

var
  FieldCache: array[0..CacheFieldLimit] of TField;
  FieldCacheForDataSet: TDataSet;

{ TLocate }

constructor TLocate.CreateFromDataSets(D: TDataSetArray);
var
  Source: TNameValueSource;
begin
  Source := TDataSetNameValueSource.Create(D);
  SetFieldsFromSource(Source);
end;

function TLocate.IsEmergency: Boolean;
begin
  Result := StrContainsText('EMER', TicketType) and (not StrContainsText('NON-EMER', TicketType));
end;

function TLocate.IsEmergencyCancel: Boolean;
begin
  Result := StrContainsText('EMERGENCY CANCEL', TicketType);  //QM-308
end;

function TLocate.IsEmergencyOrManualAfterHoursType: Boolean;
begin
  Result := IsEmergency or IsManualAfterHoursType;
end;

function TLocate.IsDamage: Boolean;
begin
  Result := StrContains('DAMAGE', TicketType) or
            StrContains('DAMG', TicketType) or
            MatchesValueGroup(TicketType, 'DamageTicketTypes');
end;

function TLocate.IsCancel: Boolean;
begin
  Result := SameText('Cancel', TicketType) or
            MatchesValueGroup(TicketType, 'CancelTicketTypes')
            or StrContainsText('CANCEL', TicketType);//QM-308
end;

function TLocate.IsCancellation: Boolean;
begin
  Result := StrContains('CANCELLATION', TicketType);
end;

function TLocate.IsProject: Boolean;
begin
  Result := StrContains('PROJECT', TicketType);
end;

function TLocate.IsAudit: Boolean;    //qm-244 sr
begin
  Result := StrContains('AUDIT', TicketType);
end;

function TLocate.IsTraining: Boolean;    //qm-244 sr
begin
  Result := StrContains('TRAINING', TicketType);
end;

function TLocate.IsLateNotice: Boolean;
begin
  Result := MatchesValueGroup(TicketType, 'NoResponses');
end;

function TLocate.IsMeetingRequest: Boolean;
begin
  Result := MatchesValueGroup(TicketType, 'MeetingTicketTypes');
end;

function TLocate.IsDesign: Boolean;
begin
  Result := MatchesValueGroup(TicketType, 'DesignTicketTypes');
end;

function TLocate.IsShortNotice: Boolean;
begin
  Result := MatchesValueGroup(TicketType, 'ShortNoticeTicketTypes');
end;

procedure TLocate.SetFieldsFromSource(Source: TNameValueSource);
begin
  Status := Source.GetString('status');
  ClientCode := Source.GetString('client_code');
  WorkState := Source.GetString('work_state');
  WorkCounty := Source.GetString('work_county');
  WorkCity := Source.GetString('work_city');
  AreaName := Source.GetString('area_name');
  Closed := Source.GetBoolean('closed');
  QtyMarked := Source.GetInteger('qty_marked');
  TicketNumber := Source.GetString('ticket_number');
  WorkAddressNumber := Source.GetString('work_address_number');
  WorkAddressNumber2 := Source.GetString('work_address_number_2');
  WorkAddressStreet := Source.GetString('work_address_street');
  WorkCross := Source.GetString('work_cross');
  WorkLat := Source.GetDouble('work_lat');
  WorkLong := Source.GetDouble('work_long');
  WorkDescription := Source.GetString('work_description');
  WorkRemarks := Source.GetString('work_remarks');
  TransmitCount := Source.GetInteger('transmit_count');
  CallCenter := Source.GetString('ticket_format');

  TicketID := Source.GetInteger('ticket_id');
  LocateID := Source.GetInteger('locate_id');
  try
    ClientID := Source.GetInteger('client_id');
  except
    ClientID := 0;
  end;

  TransmitDate := Source.GetDateTime('transmit_date');
  CallDate := Source.GetDateTime('call_date');
  WorkDate := Source.GetDateTime('work_date');
  DueDate := Source.GetDateTime('due_date');
  ClosedDate := Source.GetDateTime('closed_date');
  InitialStatusDate := Source.GetDateTime('initial_status_date');
  LocatorID := Source.GetInteger('locator_id');
  EmpNumber := Source.GetString('emp_number');
  ShortName := Source.GetString('short_name');

  ParentTicketID := Source.GetInteger('parent_ticket_id');

  TicketType := Source.GetString('ticket_type');
  ClosedHow := Source.GetString('closed_how');
  Excavator := Source.GetString('con_name');
  WorkDoneFor := Source.GetString('company');
  WorkType := Source.GetString('work_type');
  LocateAddedBy := Source.GetString('locate_added_by');

  Invoiced := Source.GetBoolean('invoiced');
  if Source.FieldExists('rate') then
    Rate := Source.GetDouble('rate');
  if Source.FieldExists('hours') then
    Hours := Source.GetDouble('hours');
  if Source.FieldExists('revision') then
    Revision := Source.GetString('revision');
  if Source.FieldExists('map_ref') then
    MapRef := Source.GetString('map_ref');
  if Source.FieldExists('qty_charged') then
    QtyCharged := Source.GetInteger('qty_charged');

  if Source.FieldExists('tax_name') then
    TaxName := Source.GetString('tax_name');
  if Source.FieldExists('tax_rate') then
    TaxRate := Source.GetDouble('tax_rate');
  if Source.FieldExists('tax_amount') then
    TaxAmount := Source.GetDouble('tax_amount');
  if Source.FieldExists('high_profile') then
    HighProfile := Source.GetBoolean('high_profile');
  if Source.FieldExists('source') then
    FSource := Source.GetString('source');
  if Source.FieldExists('raw_units') then
    FRawUnits := Source.GetInteger('raw_units');
  if Source.FieldExists('update_of') then
    FUpdateOf := Source.GetString('update_of');
  if Source.FieldExists('was_ever_late') then
    FWasEverLate := Source.GetBoolean('was_ever_late');
  if Source.FieldExists('plat') then
    FPlat := Source.GetString('plat');
  if Source.FieldExists('initial_arrival_date') then
    FInitialArrivalDate := Source.GetDateTime('initial_arrival_date');
  if Source.FieldExists('units_marked') then
    FUnitsMarked := Source.GetInteger('units_marked');
  if Source.FieldExists('first_close_date') then
    FFirstCloseDate := Source.GetDateTime('first_close_date');
   if Source.FieldExists('TixIsDup') then
    FTixIsDup := Source.GetBoolean('TixIsDup');
   if Source.FieldExists('Ref_ID') then  //QMANTWO-710
    FRefID := Source.GetInteger('Ref_ID');
end;

procedure TLocate.StoreInDataSet(D: TDataSet);
var
  CacheSlot: Integer;

  procedure SetUpCache;
  var
    I: Integer;
  begin
    if FieldCacheForDataSet <> D then begin
      for I := Low(FieldCache) to High(FieldCache) do
        FieldCache[I] := nil;
      FieldCacheForDataSet := D;
    end;
    CacheSlot := 0;
  end;

  function GetField(const FieldName: string): TField;
  var
    F: TField;
  begin
    if not Assigned(FieldCache[CacheSlot]) then begin
      F := FieldCacheForDataSet.FindField(FieldName);
      if F = nil then
        F := NullField.Create(nil);  // Leak a few of these.
      FieldCache[CacheSlot] := F;
    end;
    Result := FieldCache[CacheSlot];
    Inc(CacheSlot);
    if CacheSlot > High(FieldCache) then
      raise Exception.Create('Cache slots full, for ' + FieldName);
  end;

  procedure SkipSlot;
  begin
    Inc(CacheSlot);
  end;

begin
  SetUpCache;

  GetField('locate_id').AsInteger := LocateID;
  GetField('client_id').AsInteger := ClientID;
  GetField('status').AsString := Status;
  GetField('billing_cc').AsString := BillingClientCode;

  if TransmitDate <> 0.0 then
    GetField('transmit_date').AsDateTime := TransmitDate
  else
    SkipSlot;

  if CallDate <> 0.0 then
    GetField('call_date').AsDateTime := CallDate
  else
    SkipSlot;

  if DueDate <> 0.0 then
    GetField('due_date').AsDateTime := DueDate
  else
    SkipSlot;

  if (WorkDate <> 0.0) then
    GetField('work_date').AsDateTime := DueDate
  else
    SkipSlot;

  GetField('closed_date').AsDateTime := ClosedDate;
  GetField('initial_status_date').AsDateTime := InitialStatusDate;

  GetField('ticket_type').AsString := TicketType;
  GetField('closed_how').AsString := ClosedHow;
  GetField('bill_code').AsString := BillCode;

  GetField('qty_marked').AsInteger := QtyCharged;
  GetField('Price').AsFloat := Price;
  GetField('Bucket').AsString := Bucket;

  GetField('ticket_number').AsString := TicketNumber;

  GetField('work_state').AsString := WorkState;
  GetField('work_county').AsString := WorkCounty;
  GetField('work_city').AsString := WorkCity;
  GetField('area_name').AsString := AreaName;
  GetField('work_address_number').AsString := WorkAddressNumber;
  GetField('work_address_number_2').AsString := WorkAddressNumber2;
  GetField('work_address_street').AsString := WorkAddressStreet;
  GetField('work_cross').AsString := WorkCross;
  GetField('work_lat').AsFloat := WorkLat;
  GetField('work_long').AsFloat := WorkLong;
  GetField('work_description').AsString := WorkDescription;
  GetField('work_remarks').AsString := WorkRemarks;
  GetField('con_name').AsString := Excavator;
  GetField('work_done_for').AsString := WorkDoneFor;
  GetField('work_type').AsString := WorkType;
  GetField('transmit_count').AsInteger := TransmitCount;
  // Note: we don't set invoiced here for some reason?

  GetField('locator_id').AsInteger := LocatorID;
  GetField('emp_number').AsString := EmpNumber;
  GetField('short_name').AsString := ShortName;

  GetField('rate').AsFloat := Rate;
  GetField('parent_ticket_id').AsInteger := ParentTicketID;
  GetField('hours').AsFloat := Hours;
  GetField('line_item_text').AsString := LineItemText;
  GetField('revision').AsString := Revision;
  GetField('map_ref').AsString := MapRef;
  GetField('locate_hours_id').AsInteger := LocateHoursID;
  GetField('qty_charged').AsInteger := QtyCharged;
  GetField('which_invoice').AsString := FWhichInvoice;

  GetField('tax_name').AsString := FTaxName;
  GetField('tax_rate').AsFloat := FTaxRate;
  GetField('tax_amount').AsFloat := FTaxAmount;
  GetField('source').AsString := FSource;
  GetField('raw_units').AsInteger := FRawUnits;
  GetField('update_of').AsString := FUpdateOf;
  GetField('high_profile').AsBoolean := FHighProfile;
  GetField('was_ever_late').AsBoolean := FWasEverLate;
  GetField('plat').AsString := FPlat;
  GetField('initial_arrival_date').AsDateTime := FInitialArrivalDate;
  GetField('units_marked').AsInteger := FUnitsMarked;
  GetField('locate_added_by').AsString := FLocateAddedBy;
  GetField('rate_value_group_id').AsInteger := FRateValueGroupID;
  GetField('additional_line_item_text').AsString := FAdditionalLineItemText;
  GetField('TixIsDup').AsBoolean := fTixIsDup;
  GetField('RefID').AsInteger := fRefID; //weird  see history
  //GetField('RefID').AsInteger := fRefID;  //QMANTWO-710  { TODO : investigate }
end;

constructor TLocate.CreateFromDataSet(D: TDataSet);
var
  DataSet: TDataSetArray;
begin
  SetLength(DataSet, 1);
  DataSet[0] := D;
  CreateFromDataSets(DataSet);
end;

constructor TLocate.CreateFromDataSets(D1, D2: TDataSet);
var
  DataSet: TDataSetArray;
begin
  SetLength(DataSet, 2);
  DataSet[0] := D1;
  DataSet[1] := D2;
  CreateFromDataSets(DataSet);
end;

function TLocate.SetBillCode(const Value: string): Boolean;
begin
  Result := BillCode = '';
  FBillCode := Value;
end;

function TLocate.IsDailyUnitsHours: Boolean;
begin
  Result := LocateHoursID > 0;
end;

function TLocate.IsWatchAndProtect: Boolean;
begin
  Result := (TicketType = TicketTypeWatchAndProtect);
  if StringInArray(CallCenter, ['LWA1', 'LWA2']) then
    Result := Result or StrContains('HIGH PROFILE', TicketType);
end;

function TLocate.IsFTTP: Boolean;
begin
  Result := StrContainsText('FTTP', TicketType);
end;

function TLocate.IsFIOS: Boolean;
begin
  Result := StrContainsText('FIOS', TicketType);
end;

function TLocate.IsHourlyFTTP: Boolean;
begin
  Result := IsFTTP and IsHourly;
end;

function TLocate.IsManualAfterHoursType: Boolean;
begin
  Result := IsManualTicket and IsAfterHoursType;
end;

function TLocate.IsManualTicket: Boolean;
begin
  Result := StrBeginsWithText(ManualTicketNumberPrefix + CallCenter, TicketNumber);
end;

//TODO: Rate table config for NORESP supports this rule, so it can be removed (ref Mantis 2685)
function TLocate.IsNoShow: Boolean;
begin
  Result := StrContainsText('No Show', TicketType);
end;

function TLocate.IsHourly: Boolean;
begin
  Result := (Status = 'H') or (Status = 'OH');
end;

function TLocate.MatchesTermPrefixSuffix(const Prefix, Suffix: string): Boolean;
begin
  Result := StrBeginsWith(Prefix, ClientCode) and StrEndsWith(Suffix, ClientCode);
end;

function TLocate.GetInsertStatement(Values: Boolean; const TableName: string; BillID: Integer): string;

  procedure AddValue(const FieldName, Value: string);
  begin
    if Values then
      Result := Result + ',' + Value
    else
      Result := Result + ',' + FieldName;
  end;

  procedure AddIntValue(const FieldName: string; Value: Integer; NullForZero: Boolean = False);
  begin
    if NullForZero and (Value = 0) then
      AddValue(FieldName, 'null')
    else
      AddValue(FieldName, IntToStr(Value));
  end;

  procedure AddBooleanValue(const FieldName: string; Value: Boolean);
  begin
    if Value then
      AddValue(FieldName, '1')
    else
      AddValue(FieldName, '0');
  end;

  procedure AddDTValue(const FieldName: string; Value: TDateTime; NullForZero: Boolean = True);
  begin
    if NullForZero and (Value = 0) then
      AddValue(FieldName, 'null')
    else
      AddValue(FieldName, IsoDateTimeToStrQuoted(Value));
  end;

  procedure AddStrValue(const FieldName, Value: string; NullForBlank: Boolean = True);
  begin
    if NullForBlank and (Value = '') then
      AddValue(FieldName, 'null')
    else
      AddValue(FieldName, QuotedStr(Value));
  end;

  procedure AddFloatValue(const FieldName: string; const Value: Double; NullForZero: Boolean = False);
  begin
    if NullForZero and (Value = 0.0) then
      AddValue(FieldName, 'null')
    else
      AddValue(FieldName, FloatToStr(Value));
  end;

begin
  if Values then
    Result := 'select ' + IntToStr(BillID)
  else begin
    Assert(TableName <> '');
    Result := Format('insert into %s (%s', [TableName, 'bill_id']);
  end;

  AddIntValue('locate_id', LocateID);
  AddIntValue('client_id', ClientID);
  AddStrValue('status', Status);
  AddStrValue('billing_cc', BillingClientCode);
  AddDTValue('transmit_date', TransmitDate);
  AddDTValue('call_date', CallDate);
  AddDTValue('due_date', DueDate);
  //AddDTValue('work_date', WorkDate);
  AddDTValue('closed_date', ClosedDate);
  AddDTValue('initial_status_date', InitialStatusDate);
  AddStrValue('ticket_type', LeftStr(TicketType, 30));
  AddStrValue('closed_how', ClosedHow);
  AddStrValue('bill_code', BillCode);
  AddIntValue('qty_marked', QtyMarked);
  AddFloatValue('price', Price);
  AddStrValue('bucket', LeftStr(Bucket, 60));
  AddStrValue('ticket_number', TicketNumber);
  AddStrValue('work_state', WorkState);
  AddStrValue('work_county', WorkCounty);
  AddStrValue('work_city', WorkCity);
  AddStrValue('area_name', AreaName);
  AddStrValue('work_address_number', WorkAddressNumber);
  AddStrValue('work_address_number_2', WorkAddressNumber2);
  AddStrValue('work_address_street', LeftStr(WorkAddressStreet, 45));
  AddStrValue('work_cross', WorkCross);
  AddFloatValue('work_lat', WorkLat);
  AddFloatValue('work_long', WorkLong);
  AddStrValue('work_description', LeftStr(WorkDescription, 254));
  AddStrValue('work_remarks', LeftStr(WorkRemarks, 254));
  AddStrValue('con_name', Excavator);
  AddStrValue('work_done_for', LeftStr(WorkDoneFor, 50));
  AddStrValue('work_type', WorkType);
  AddIntValue('transmit_count', TransmitCount);
  AddIntValue('locator_id', LocatorID, True);
  AddStrValue('emp_number', EmpNumber);
  AddStrValue('short_name', LeftStr(ShortName, 20));
  AddFloatValue('rate', Rate);
  //AddIntValue('parent_ticket_id', ParentTicketID, True);
  AddFloatValue('hours', Hours);
  AddStrValue('line_item_text', LineItemText);
  AddStrValue('revision', Revision);
  AddStrValue('map_ref', MapRef);
  AddIntValue('locate_hours_id', LocateHoursID, True);
  AddIntValue('qty_charged', QtyCharged);
  AddStrValue('which_invoice', WhichInvoice);
  AddStrValue('tax_name', TaxName);
  AddFloatValue('tax_rate', TaxRate);
  AddFloatValue('tax_amount', TaxAmount);
  AddIntValue('raw_units', FRawUnits);
  AddStrValue('update_of', FUpdateOf);
  //AddBooleanValue('was_ever_late', FWasEverLate);
  AddBooleanValue('high_profile', FHighProfile);
  AddStrValue('plat', FPlat);
  AddDTValue('initial_arrival_date', FInitialArrivalDate);
  AddIntValue('units_marked', FUnitsMarked);
  AddStrValue('locate_added_by', FLocateAddedBy);
  AddIntValue('rate_value_group_id', FRateValueGroupID);
  AddStrValue('additional_line_item_text', FAdditionalLineItemText);
  AddBooleanValue('TixIsDup', FTixIsDup);
  AddIntValue('Ref_id', RefID);   //QMANTWO-710

//  AddIntValue('Br_ID', FBillingRateID);     //qm-948
  AddStrValue('CWICode',FCWICode  );        //qm-948
	AddStrValue('AddlCWICode',FAddlCWICode);  //qm-948
  AddStrValue('Rpt_gl',FRpt_gl  );        //qm-995
	AddStrValue('Additional_Rpt_gl',FAdditional_Rpt_gl);  //qm-995

  if not Values then
    Result := Result + ')';
end;

function TLocate.IsAfterHoursType: Boolean;
begin
  Result := StringInArray(TicketType, ['AFTER HOURS']);
end;

function TLocate.IsUpdate: Boolean;
begin
  Result := SameText(TicketType, TicketTypeUpdate);
end;

function TLocate.IsRemark: Boolean;     //qm-884 sr
begin
  Result := SameText(TicketType, TicketTypeRemark);
end;

function TLocate.IsFromCallCenter: Boolean;
begin
  Result := SameText(CallCenter, Source);
end;

function TLocate.IsFromClient: Boolean;
begin
  Result := SameText(ClientCode, Source);
end;

function TLocate.IsSurvey: Boolean;
begin
  Result := MatchesValueGroup(TicketType, 'SurveyTicketTypes');
end;

function TLocate.CreateExtraChargeCopy: TLocate;
begin
  Result := TLocate.Create;
  ClonePersistent(Self, Result);
  Result.IsExtraCharge := True;
end;

function TLocate.HighProfileBit: Integer;
begin
  if FHighProfile then
    Result := 1
  else
    Result := 0;
end;

{ TLocateReader }

destructor TLocateReader.Destroy;
begin
  FreeAndNil(FSource);
  inherited;
end;

function TLocateReader.ReadFromFile(FileName: string): TObjectList;
var
  I: Integer;
  Data: TStringList;
  S: string;
begin
  Result := TObjectList.Create;

  Data := TStringList.Create;
  try
    Data.LoadFromFile(FileName);
    SetupFields(Data[0]);
    for I := 1 to Data.Count-1 do begin
      S := Data[i];
      Result.Add(ReadLocate(S));
    end;
  finally
    Data.Free;
  end;
end;

function TLocateReader.ReadLocate(S: string): TLocate;
var
  SL: TStringList;
begin
  Result := TLocate.Create;

  SL := TStringList.Create;
  try
    FSource.SetData(S);
    Result.SetFieldsFromSource(FSource);
  finally
    SL.Free;
  end;
end;

// The following procedure copied from JclStrings, which had a bug.

procedure StrToStrings(S, Sep: AnsiString; const List: TStrings; const AllowEmptyString: Boolean = False);
var
  I, L: Integer;
  Left: AnsiString;
begin
  Assert(List <> nil);
  List.Clear;
  L := Length(Sep);
  I := Pos(Sep, S);
  while (I > 0) do
  begin
    Left := Copy(S, 1, I - 1);
    if (Left <> '') or AllowEmptyString then
      List.Add(string(Left));
    System.Delete(S, 1, I + L - 1);
    I := Pos(Sep, S);
  end;
  if S <> '' then
    List.Add(string(S));  // Ignore empty strings at the end.
end;

procedure TLocateReader.SetupFields(S: string);
var
  Fields: TStringList;
begin
  Fields := TStringList.Create;
  try
    StrToStrings(AnsiString(S), #9, Fields, True);
    FSource := TCsvNameValueSource.Create(Fields);
  finally
    Fields.Free;
  end;
end;

{ TDataSetNameValueSource }

constructor TDataSetNameValueSource.Create(DS: TDataSetArray);
begin
  FDataSets := DS;
end;

function TDataSetNameValueSource.GetString(const FN: string): string;
begin
  Result := Trim(VarToString(GetFieldValue(FN)));
end;

function TDataSetNameValueSource.GetInteger(const FN: string): Integer;
begin
  Result := VarToInt(GetFieldValue(FN));
end;

function TDataSetNameValueSource.GetDateTime(const FN: string): TDateTime;
begin
  Result := VarToDateTime(GetFieldValue(FN));
end;

function TDataSetNameValueSource.GetBoolean(const FN: string): Boolean;
begin
  Result := VarToBoolean(GetFieldValue(FN));
end;

function TDataSetNameValueSource.GetDouble(const FN: string): Double;
begin
  Result := VarToFloat(GetFieldValue(FN));
end;

function TDataSetNameValueSource.FieldExists(const FN: string): Boolean;
var
  i: Integer;
  Field: TField;
begin
  Result := False;
  for i := Low(FDataSets) to High(FDataSets) do begin
    Field := FDataSets[i].FindField(FN);
    if Assigned(Field) then begin
      Result := True;
      Exit;
    end;
  end;
end;

function TDataSetNameValueSource.GetFieldValue(const FieldName: string): Variant;
var
  i: Integer;
  Field: TField;
begin
  for i := Low(FDataSets) to High(FDataSets) do begin
    Field := FDataSets[i].FindField(FieldName);
    if Assigned(Field) then begin
      Result := Field.Value;
      Exit;
    end;
  end;
  raise Exception.CreateFmt('Field %s not found', [FieldName]);
end;

{ TCsvNameValueSource }

constructor TCsvNameValueSource.Create(FieldList: TStrings);
begin
  FFieldList := TStringList.Create;
  FFieldList.Assign(FieldList);
  FData := TStringList.Create;
end;

destructor TCsvNameValueSource.Destroy;
begin
  FreeAndNil(FFieldList);
  FreeAndNil(FData);
  inherited;
end;

function TCsvNameValueSource.FieldExists(const FN: string): Boolean;
begin
  Result := (FFieldList.IndexOf(FN) > -1);
end;

function TCsvNameValueSource.GetBoolean(const FN: string): Boolean;
var
  S: string;
begin
  S := GetString(FN);
  Result := (S = 'True') or (S = 'TRUE') or (S = 'true') or (S = '1');
end;

function TCsvNameValueSource.GetDateTime(const FN: string): TDateTime;
var
  S: string;
begin
  S := GetString(FN);
  if S = '' then
    Result := 0.0
  else
    Result := StrToDateTime(GetString(FN));
end;

function TCsvNameValueSource.GetDouble(const FN: string): Double;
var
  S: string;
begin
  S := GetString(FN);
  if S = '' then
    Result := 0.0
  else
    Result := StrToFloat(S);
end;

function TCsvNameValueSource.GetInteger(const FN: string): Integer;
var
  S: string;
begin
  S := GetString(FN);
  if S='' then
    Result := 0
  else
    Result := StrToInt(GetString(FN));
end;

function TCsvNameValueSource.GetString(const FN: string): string;
var
  p: Integer;
begin
  p := FFieldList.IndexOf(FN); // IndexOf is case insensitive
  if p < 0 then
    raise Exception.Create('Field not found: ' + FN + '  Field list = ' + FFieldList.CommaText);
  if p >= FData.Count then
    Result := ''
  else
    Result := FData[p];
end;

procedure TCsvNameValueSource.SetData(S: string);
begin
  FData.Clear;
  StrToStrings(AnsiString(S), #9, FData, True);
end;

end.

