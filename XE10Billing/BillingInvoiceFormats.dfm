inherited BillingInvoiceFormatsForm: TBillingInvoiceFormatsForm
  Caption = 'Invoice Formats'
  ClientHeight = 377
  ClientWidth = 309
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object EditTemplateButton: TButton
    Left = 200
    Top = 24
    Width = 75
    Height = 25
    Action = EditTemplateAction
    TabOrder = 0
  end
  object TemplateListbox: TListBox
    Left = 24
    Top = 24
    Width = 169
    Height = 329
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = TemplateListboxDblClick
  end
  object NewButton: TButton
    Left = 200
    Top = 56
    Width = 75
    Height = 25
    Action = NewTemplateAction
    TabOrder = 2
  end
  object ActionList1: TActionList
    Left = 32
    Top = 32
    object EditTemplateAction: TAction
      Caption = '&Edit'
      OnExecute = EditTemplateActionExecute
      OnUpdate = EditTemplateActionUpdate
    end
    object NewTemplateAction: TAction
      Caption = '&New'
      OnExecute = NewTemplateActionExecute
    end
  end
end
