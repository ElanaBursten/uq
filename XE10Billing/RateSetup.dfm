inherited RateSetupForm: TRateSetupForm
  Left = 139
  Top = 156
  Caption = 'Billing Rate Setup'
  ClientHeight = 489
  ClientWidth = 866
  OnClose = FormClose
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 866
    object CenterGroupLabel: TLabel
      Left = 21
      Top = 14
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = '&Center Group'
      FocusControl = CenterGroupCombo
    end
    object CenterGroupCombo: TComboBox
      Left = 93
      Top = 11
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 22
      TabOrder = 0
      OnChange = CenterGroupComboChange
    end
  end
  inherited Grid: TcxGrid
    Width = 866
    Height = 448
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Enabled = False
      Navigator.Buttons.NextPage.Enabled = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmAlways
      DataController.KeyFieldNames = 'br_id'
      OptionsBehavior.IncSearch = True
      OptionsBehavior.NavigatorHints = True
      OptionsCustomize.ColumnExpressionEditing = True
      OptionsCustomize.ColumnFiltering = True
      OptionsCustomize.ColumnsQuickCustomizationMaxDropDownCount = 8
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object GridCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 61
      end
      object GridCWICode: TcxGridDBColumn
        DataBinding.FieldName = 'CWICode'
        DataBinding.IsNullValueType = True
        Width = 70
      end
      object GridModifiedDate: TcxGridDBColumn
        Caption = 'Modified date'
        DataBinding.FieldName = 'modified_date'
        DataBinding.IsNullValueType = True
        Width = 80
      end
      object GridUtilCo: TcxGridDBColumn
        Caption = 'Utility Co'
        DataBinding.FieldName = 'UtilityCo'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 60
      end
      object GridBillingCC: TcxGridDBColumn
        Caption = 'Term'
        DataBinding.FieldName = 'billing_cc'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 62
      end
      object GridBillType: TcxGridDBColumn
        Caption = 'Bill Type'
        DataBinding.FieldName = 'bill_type'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Properties.Items.Strings = (
          'NORMAL'
          'DAMAGE'
          'DMGCHARGE'
          'EMERG'
          'AFTER'
          'WATCH'
          'AWATCH'
          'OVER'
          'FTTP'
          'FIOS'
          'HIGH'
          'BULK1K'
          'UPDATE'
          'NORESP'
          'SHORT'
          'DESIGN'
          'LATE')
        Width = 67
      end
      object GridStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 46
      end
      object GridParties: TcxGridDBColumn
        Caption = 'Parties'
        DataBinding.FieldName = 'parties'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Width = 44
      end
      object GridRate: TcxGridDBColumn
        Caption = 'Rate'
        DataBinding.FieldName = 'rate'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        HeaderAlignmentHorz = taRightJustify
        Width = 50
      end
      object GridAdditionalRate: TcxGridDBColumn
        Caption = 'Adl Rate'
        DataBinding.FieldName = 'additional_rate'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        HeaderAlignmentHorz = taRightJustify
        Width = 46
      end
      object GridBillCode: TcxGridDBColumn
        Caption = 'Bill Code'
        DataBinding.FieldName = 'bill_code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 51
      end
      object GridWorkCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 78
      end
      object GridWorkCity: TcxGridDBColumn
        Caption = 'Work City'
        DataBinding.FieldName = 'work_city'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 72
      end
      object GridAreaName: TcxGridDBColumn
        Caption = 'Area Name'
        DataBinding.FieldName = 'area_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 72
      end
      object GridLineItemText: TcxGridDBColumn
        Caption = 'Line Item Text'
        DataBinding.FieldName = 'line_item_text'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownRows = 15
        Properties.Items.Strings = (
          'After Hours - In House Area'
          'After Hours Notice'
          'Call Out'
          'Design Notice'
          'Emergency Notice'
          'Field Locate'
          'Hourly'
          'Hourly Charges'
          'Hourly: Callout'
          'Hourly: Overtime'
          'Hourly: Regular'
          'Hourly: Standby'
          'No Charge - No Response'
          'No Locate Required'
          'Normal Notice'
          'Normal Notice - Cleared'
          'Normal Notice - Marked'
          'Normal Ticket'
          'Office Screen'
          'Screen'
          'Short Notice'
          'Site Surveillance: Response Only'
          'Transmission')
        Width = 118
      end
      object GridWhichInvoice: TcxGridDBColumn
        Caption = 'Invoice'
        DataBinding.FieldName = 'which_invoice'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 46
      end
      object GridValueGroup: TcxGridDBColumn
        Caption = 'WDF Value Group'
        DataBinding.FieldName = 'value_group_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.View = ValueGroupLookupView
        Properties.KeyFieldNames = 'value_group_id'
        Properties.ListFieldItem = VGColGroupCode
        Width = 72
      end
      object GridAdditionalLineItemText: TcxGridDBColumn
        Caption = 'Adl Line Item Text'
        DataBinding.FieldName = 'additional_line_item_text'
        DataBinding.IsNullValueType = True
        Width = 118
      end
      object GridAddlCWICode: TcxGridDBColumn
        DataBinding.FieldName = 'AddlCWICode'
        DataBinding.IsNullValueType = True
      end
      object GridRptGL: TcxGridDBColumn
        Caption = 'Rpt GL'
        DataBinding.FieldName = 'Rpt_gl'
        DataBinding.IsNullValueType = True
      end
      object GridAdditionalRptGL: TcxGridDBColumn
        DataBinding.FieldName = 'Additional_Rpt_gl'
        DataBinding.IsNullValueType = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    AfterPost = DataAfterPost
    CommandText = 
      'declare @GroupCode varchar(12);  '#13#10'set @GroupCode =:GroupCode'#13#10's' +
      'elect distinct ref.description as UtilityCo,  br.* '#13#10'from billin' +
      'g_rate br'#13#10'join client c on (c.oc_code=br.billing_cc)'#13#10'join refe' +
      'rence ref on (c.ref_id = ref.ref_id)'#13#10'where br.call_center in ('#13 +
      #10'  select cgd.call_center'#13#10'  from center_group_detail cgd'#13#10'    i' +
      'nner join center_group cg on cg.center_group_id = cgd.center_gro' +
      'up_id'#13#10'  where (@GroupCode = '#39'*'#39') or (cg.group_code = @GroupCode' +
      ')'#13#10')'#13#10#13#10'order by br.call_center, br.billing_cc, br.status, br.pa' +
      'rties'
    IndexFieldNames = 'br_id'
    Parameters = <
      item
        Name = 'GroupCode'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = ''
      end>
    Left = 96
    Top = 168
  end
  inherited DS: TDataSource
    Left = 152
    Top = 168
  end
  object Clients: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 'select * from client'
    Parameters = <>
    Left = 104
    Top = 240
  end
  object ValueGroupLookup: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from value_group order by group_code'
    Parameters = <>
    Left = 96
    Top = 352
  end
  object GridViewRepository: TcxGridViewRepository
    Left = 96
    Top = 296
    object ValueGroupLookupView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.KeyFieldNames = 'value_group_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object VGColValueGroupID: TcxGridDBColumn
        Caption = 'Value Group ID'
        DataBinding.FieldName = 'value_group_id'
        DataBinding.IsNullValueType = True
        MinWidth = 10
        Width = 81
      end
      object VGColGroupCode: TcxGridDBColumn
        Caption = 'Group Code'
        DataBinding.FieldName = 'group_code'
        DataBinding.IsNullValueType = True
      end
    end
  end
end
