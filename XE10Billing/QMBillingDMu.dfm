object QMBillingDM: TQMBillingDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 364
  Width = 265
  object Conn: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11;Initial Catalog=QM;Database=QM;Server=tcp:SSD' +
      'S-UTQ-QM-02-DV,1433;User ID=uqweb;'
    ConnectionTimeout = 10
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLNCLI11'
    AfterConnect = ConnAfterConnect
    BeforeConnect = ConnBeforeConnect
    Left = 40
    Top = 12
  end
  object BillingQueue: TADODataSet
    Connection = Conn
    CommandText = 
      'select * from billing_run where completed_date is null'#13#10'order by' +
      ' request_date'
    Parameters = <>
    Left = 104
    Top = 72
  end
  object Clients: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select distinct oc_code'#13#10'from client'#13#10'order by oc_code'
    Parameters = <>
    Left = 40
    Top = 76
  end
  object ConnectionCheck: TTimer
    Enabled = False
    Interval = 20000
    OnTimer = ConnectionCheckTimer
    Left = 104
    Top = 16
  end
  object AdminRestriction: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 'select * from admin_restriction where login_id = :login_id'
    Parameters = <
      item
        Name = 'login_id'
        Size = -1
        Value = Null
      end>
    Left = 176
    Top = 72
  end
  object Ticket: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    CommandText = 
      'select ticket_id, transmit_date, call_date'#13#10'from ticket'#13#10'where t' +
      'icket_id = :ticket_id'
    Parameters = <
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 40
    Top = 128
  end
end
