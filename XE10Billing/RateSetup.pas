unit RateSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxMaskEdit, cxCurrencyEdit, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters,
  cxDBExtLookupComboBox, cxNavigator, dxSkinsCore, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxScrollbarAnnotations, dxCore, dxSkinsForm, dxSkinBasic;

type
  TRateSetupForm = class(TBaseCxListForm)
    Clients: TADODataSet;
    GridCallCenter: TcxGridDBColumn;
    GridBillingCC: TcxGridDBColumn;
    GridBillType: TcxGridDBColumn;
    GridStatus: TcxGridDBColumn;
    GridParties: TcxGridDBColumn;
    GridRate: TcxGridDBColumn;
    GridAdditionalRate: TcxGridDBColumn;
    GridBillCode: TcxGridDBColumn;
    GridWorkCounty: TcxGridDBColumn;
    GridWorkCity: TcxGridDBColumn;
    GridAreaName: TcxGridDBColumn;
    GridLineItemText: TcxGridDBColumn;
    GridWhichInvoice: TcxGridDBColumn;
    CenterGroupLabel: TLabel;
    CenterGroupCombo: TComboBox;
    GridValueGroup: TcxGridDBColumn;  //qm-936
    ValueGroupLookup: TADODataSet;
    GridViewRepository: TcxGridViewRepository;
    ValueGroupLookupView: TcxGridDBTableView;
    VGColGroupCode: TcxGridDBColumn;
    VGColValueGroupID: TcxGridDBColumn;
    GridAdditionalLineItemText: TcxGridDBColumn;
    GridUtilCo: TcxGridDBColumn;
    GridModifiedDate: TcxGridDBColumn;  //qm-850
    GridCWICode: TcxGridDBColumn;
    GridAddlCWICode: TcxGridDBColumn;
    GridRptGL: TcxGridDBColumn;   //qm-995
    GridAdditionalRptGL: TcxGridDBColumn;   //qm-995
    procedure DataAfterPost(DataSet: TDataSet);
    procedure CenterGroupComboChange(Sender: TObject);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ApplyFilter;
  public
    procedure PopulateDropdowns; override;
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
  end;

implementation

uses QMBillingDMu, OdAdoUtils, JclSysInfo, OdCxUtils, OdDbUtils, OdHourglass,
  OdVclUtils, OdMiscUtils;

{$R *.dfm}

procedure TRateSetupForm.PopulateDropdowns;
var
  Selected: string;
begin
  inherited;
  ValueGroupLookup.Open;
  QMBillingDM.ConfigDM.GetCallCenterList(CxComboBoxItems(GridCallCenter));
  QMBillingDM.GetClientList(CxComboBoxItems(GridBillingCC));
  QMBillingDM.GetStatusList(CxComboBoxItems(GridStatus), True);
  Selected := CenterGroupCombo.Text;
  try
    QMBillingDM.ConfigDM.GetBillingCallCenterGroupList(CenterGroupCombo.Items);
    CenterGroupCombo.Items.Insert(0, '*');
    CenterGroupCombo.ItemIndex := -1;
  finally
    if NotEmpty(Selected) then
      SelectComboItem(CenterGroupCombo, Selected, -1);
  end;
  ApplyFilter;
end;

procedure TRateSetupForm.ApplyFilter;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourglass;
  SetParams(Data, 'GroupCode', CenterGroupCombo.Text);
  RefreshDataSet(Data);
end;

procedure TRateSetupForm.CenterGroupComboChange(Sender: TObject);
begin
  ApplyFilter;
end;

procedure TRateSetupForm.CloseDataSets;
begin
  inherited;
end;

procedure TRateSetupForm.DataAfterPost(DataSet: TDataSet);
const
  SQL = 'update billing_rate_history set os_user_name = %s '+
        'where br_id = %d and archive_date = (select max(archive_date) from billing_rate_history where br_id = %d)';
begin
  inherited;
  ExecuteQuery(Data.Connection, Format(SQL, [QuotedStr(GetLocalUserName), Dataset.FieldByName('br_id').AsInteger, Dataset.FieldByName('br_id').AsInteger]));
end;

procedure TRateSetupForm.DataBeforeDelete(DataSet: TDataSet);
begin
  Data.Properties['Unique Table'].Value := 'billing_rate';   // bp QM-307
  if MessageDlg('Delete this rate?', mtConfirmation, [mbYes,mbNo], 0) = mrNo then
    Abort;
end;

procedure TRateSetupForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  ValueGroupLookup.Close;
end;

procedure TRateSetupForm.OpenDataSets;
begin
  inherited;
end;

end.
