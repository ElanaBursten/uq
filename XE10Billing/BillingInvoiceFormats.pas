unit BillingInvoiceFormats;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ActnList, BillingExportDMu, System.Actions;

type
  TBillingInvoiceFormatsForm = class(TEmbeddableForm)
    EditTemplateButton: TButton;
    TemplateListbox: TListBox;
    ActionList1: TActionList;
    EditTemplateAction: TAction;
    NewButton: TButton;
    NewTemplateAction: TAction;
    procedure TemplateListboxDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EditTemplateActionExecute(Sender: TObject);
    procedure EditTemplateActionUpdate(Sender: TObject);
    procedure NewTemplateActionExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FBaseDir: string;
    ExportDM: TBillingExportDM;
  public
    procedure RefreshNow; override;
  end;

implementation

uses
  ppTypes, QMBillingDMu, OdMiscUtils;

{$R *.dfm}

//TODO: Do we want to allow the user to navigate the entire HD, or is it good
//      enough to just put the tempaltes in the app dir?
procedure TBillingInvoiceFormatsForm.FormCreate(Sender: TObject);
begin
  inherited;
  FBaseDir := ExtractFilePath(Application.ExeName) + 'Templates\';
  RefreshNow;
  ExportDM := TBillingExportDM.Create(nil);
  ExportDM.ConnectAllDBComponents(QMBillingDM.Conn, QMBillingDM.Conn);
end;

procedure TBillingInvoiceFormatsForm.EditTemplateActionExecute(Sender: TObject);
begin
  ExportDM.InvoiceReport.Template.Format := ftAscii;
  ExportDM.InvoiceReport.Template.FileName := FBaseDir + TemplateListBox.Items[TemplateListbox.ItemIndex];
  ExportDM.InvoiceReport.Template.LoadFromFile;
  ExportDM.Designer.ShowModal;
  RefreshNow;
end;

procedure TBillingInvoiceFormatsForm.EditTemplateActionUpdate(Sender: TObject);
begin
  EditTemplateAction.Enabled := TemplateListbox.ItemIndex > -1;
end;

procedure TBillingInvoiceFormatsForm.TemplateListboxDblClick(Sender: TObject);
begin
  EditTemplateActionExecute(Sender);
end;

procedure TBillingInvoiceFormatsForm.RefreshNow;
var
  sr: TSearchRec;
begin
  inherited;
  TemplateListBox.Clear;

  // Find the templates that are in the application directory
  if SysUtils.FindFirst(FBaseDir + '*.rtm', faAnyFile, sr) = 0 then begin
    repeat
      // Only evaluate files that really end with *.rtm. FindFirst will also return *.rtm~, etc.
      if not SameText(ExtractFileExt(sr.Name), '.rtm') then
        Continue;
      TemplateListBox.Items.Add(sr.Name);
    until SysUtils.FindNext(sr) <> 0;
    SysUtils.FindClose(sr);
  end;
end;

procedure TBillingInvoiceFormatsForm.NewTemplateActionExecute(Sender: TObject);
begin
  ExportDM.ResetToDefaultTemplate;
  OdForceDirectories(FBaseDir);
  ExportDM.TemplateSaveDlg.InitialDir := FBaseDir;
  if ExportDM.TemplateSaveDlg.Execute then begin
    ExportDM.InvoiceReport.Template.Format := ftAscii;
    ExportDM.InvoiceReport.Template.SaveTo := stFile;
    ExportDM.InvoiceReport.Template.FileName := ExportDM.TemplateSaveDlg.FileName;
    ExportDM.InvoiceReport.Template.SaveToFile;
    RefreshNow;
  end;
end;

procedure TBillingInvoiceFormatsForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(ExportDM);
end;

end.
