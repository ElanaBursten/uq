unit BillingExportDMu;

interface

uses
  SysUtils, Classes, DB, ADODB,
 DBISAMTb,
BillingShared, SolomonExportDMu,
  // Used for template designer functions
  System.UITypes,
  Forms, Dialogs, Controls, ppRegion, ppModule, daDataModule, Contnrs,
  OdPerfLog, Graphics, ppParameter, ppDesignLayer, ppCtrls, ppPrnabl, ppDB, ppBands, ppClass, ppEndUsr, ppDBPipe, ppReport,
  ppSubRpt, ppStrtch, ppVar, ppCache, ppComm, ppRelatv, ppProd;

type
  TBillingExportDM = class(TDataModule)
    Summary: TADODataSet;
    DetailResults: TADODataSet;
    InvoiceHeader: TADODataSet;
    InvoiceDetails: TADODataSet;
    InvoiceReport: TppReport;
    InvoiceHeaderPipe: TppDBPipeline;
    InvoiceDetailsPipe: TppDBPipeline;
    InvoiceHeaderDS: TDataSource;
    InvoiceDetailsDS: TDataSource;
    OutputConfig: TADODataSet;
    BillingInvoice: TADODataSet;
    BillingInvoiceDS: TDataSource;
    BillingInvoicePipe: TppDBPipeline;
    Designer: TppDesigner;
    TemplateSaveDlg: TSaveDialog;
    InvoiceSP: TADOStoredProc;
    InvoiceAdjustments: TADODataSet;
    InvoiceAdjustmentsPipe: TppDBPipeline;
    InvoiceAdjustmentsDS: TDataSource;
    InvoiceDetailsExport: TADODataSet;
    BillingConfigDir: TADODataSet;
    CreateBOCRecords: TADOCommand;
    BillRunInfo: TADODataSet;
    NextInvoiceID: TADOStoredProc;
    RecBillingInvoice: TADODataSet;
    BillingInvoicePreview: TADODataSet;
    IntegerField1: TIntegerField;
    DateTimeField1: TDateTimeField;
    StringField1: TStringField;
    StringField2: TStringField;
    Taxes: TADODataSet;
    TaxesDS: TDataSource;
    TaxesPipe: TppDBPipeline;
    GrayKielyPipe: TppDBPipeline;
    GrayKielyDS: TDataSource;
    GrayKiely: TADODataSet;
    StatusSummary: TADODataSet;
    StatusSummaryDS: TDataSource;
    StatusSummaryPipe: TppDBPipeline;
    Breakdown: TADODataSet;
    BreakdownDS: TDataSource;
    BreakdownPipe: TppDBPipeline;
    TransDetail: TADODataSet;
    TicketNotesData: TADODataSet;
    SolomonSplit: TADODataSet;
    XignExport: TADODataSet;
    ppHeaderBand3: TppHeaderBand;
    ppLabel23: TppLabel;
    ppLabel9: TppLabel;
    ppLabel12: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppDBText20: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppLogoImage: TppImage;
    ppSystemVariable4: TppSystemVariable;
    ppShape1: TppShape;
    ppLabel28: TppLabel;
    ppLine8: TppLine;
    ppLabel29: TppLabel;
    ppLine9: TppLine;
    ppLabel30: TppLabel;
    ppLine12: TppLine;
    ppLabel31: TppLabel;
    ppLine13: TppLine;
    ppLabel32: TppLabel;
    ppLine14: TppLine;
    ppLabel33: TppLabel;
    ppPeriod: TppLabel;
    ppRegion1: TppRegion;
    ppDBText27: TppDBText;
    ppCityStateZipLabel: TppLabel;
    ppDBText30: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLine17: TppLine;
    ppLabel13: TppLabel;
    InvoiceNumberText: TppDBText;
    LineItemDetailBand: TppDetailBand;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText31: TppDBText;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLine10: TppLine;
    ppDBText32: TppDBText;
    ppDBText16: TppDBText;
    ppLine18: TppLine;
    TaxLineItemDetailSubRep: TppSubReport;
    ppChildReport5: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppLabel15: TppLabel;
    ppDBText40: TppDBText;
    ppDBText41: TppDBText;
    ppDBText42: TppDBText;
    ppLabel16: TppLabel;
    ppDBText43: TppDBText;
    ppLine23: TppLine;
    ppLine27: TppLine;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppDBText14: TppDBText;
    ppFooterBand3: TppFooterBand;
    ppDBText28: TppDBText;
    BillingRunIDText: TppDBText;
    BillingContactInfo: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    AdjustmentSubReport: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText29: TppDBText;
    ppDBText34: TppDBText;
    ppLine19: TppLine;
    ppLine21: TppLine;
    ppLabel39: TppLabel;
    ppSummaryBand3: TppSummaryBand;
    ppLine11: TppLine;
    ppLabel26: TppLabel;
    ppDBText21: TppDBText;
    ppDBCalc9: TppDBCalc;
    ppLine20: TppLine;
    ppLine22: TppLine;
    ppLabel40: TppLabel;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    TotalsRegion: TppRegion;
    ppShape3: TppShape;
    ppLabel37: TppLabel;
    ppLabel38: TppLabel;
    ppDBText36: TppDBText;
    SubTotalRegion: TppRegion;
    ppDBText35: TppDBText;
    ppLabel34: TppLabel;
    ppDBText33: TppDBText;
    ppLabel36: TppLabel;
    ppLabel10: TppLabel;
    TaxSubReport: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDBText4: TppDBText;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDesignLayers3: TppDesignLayers;
    ppDesignLayer3: TppDesignLayer;
    GrayKielySubReport: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel5: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDesignLayers4: TppDesignLayers;
    ppDesignLayer4: TppDesignLayer;
    BreakdownSub: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLabel11: TppLabel;
    ppLine3: TppLine;
    ppDetailBand5: TppDetailBand;
    ppDBText13: TppDBText;
    ppDBText37: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppLine4: TppLine;
    ppDesignLayers5: TppDesignLayers;
    ppDesignLayer5: TppDesignLayer;
    ppPageStyle1: TppPageStyle;
    ppWatermark: TppLabel;
    ppLabel41: TppLabel;
    ppGroup1: TppGroup;
    SubCategoryHeaderBand: TppGroupHeaderBand;
    SubCategoryLabel: TppDBText;
    ppLine6: TppLine;
    ppLine5: TppLine;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBCalc4: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppLine7: TppLine;
    ppLabel27: TppLabel;
    ppLine15: TppLine;
    ppLine16: TppLine;
    ppDBCalc1: TppDBCalc;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine25: TppLine;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppDBCalc8: TppDBCalc;
    ppLabel14: TppLabel;
    ppLine24: TppLine;
    ppDBText39: TppDBText;
    ppLabel47: TppLabel;
    ppLine26: TppLine;
    ppDesignLayers6: TppDesignLayers;
    ppDesignLayer7: TppDesignLayer;
    ppDesignLayer6: TppDesignLayer;
    procedure ppCityStateZipLabelPrint(Sender: TObject);
    procedure ppLogoImagePrint(Sender: TObject);
    procedure ppPageStyle1BeforePrint(Sender: TObject);
    procedure DesignerClose(Sender: TObject; var Action: TCloseAction);
    procedure InvoiceNumberTextPrint(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DesignerShow(Sender: TObject);
    procedure DesignerHide(Sender: TObject);
    procedure SubCategoryHeaderBandBeforePrint(Sender: TObject);
    procedure LineItemDetailBandBeforePrint(Sender: TObject);
  private
    FBillID: Integer;
    FCallCenterGroupID: Integer;
    FCallCenters: string;
    FGroupCode: string;
    FOutputBaseDir: string;
    FOutputDir: string;
    FInvoiceStream: TMemoryStream;
    FStartDate, FEndDate: TDateTime;
    FPeriodType: string;
    FLogEvent: TLogEvent;
    FSolomonDM: TSolomonExportDM;

    FCommitted: Boolean;
    FDetailMemoFields: TStringList;
    procedure ExportCustomerInvoices;

    procedure SetInvoiceTemplate(const TemplateName: string);
    procedure SetPeriodLabel;
    function WeekEndingString: string;
    procedure ExportDetail;
    procedure ExportSummaryTxt;
    procedure ExportCustomerDetails(CustomerID: Integer; const BaseFileName, WhichInvoice: string);
    procedure ExportBillingExceptionReport;
    procedure ExportXign(const OutputConfigID: Integer; const BaseFileName, CustomerName: string); //qm-542
    function SetPeriodRange(const Before: string): string;
    procedure Log(Msg: string);
    procedure GetBillingRunInfo;
    procedure PrepareOutputDirectory;
    procedure StoreDefaultReport;
    procedure RecordInvoice(CustomerID: Integer;
      EndDate: TDateTime; InvoiceAmount: Double; WhichInvoice: string);
    function GetNextInvoiceID: Integer;
    function CleanUp(Name: string): string;
    procedure ChooseDefaultOutputDir;
    procedure CloseAll;
    function InvoiceIsCommitted: Boolean;
    procedure ExportTicketNotes;
    function StartPhase(PhaseName: string): TPhase;
    procedure DeleteOldUncommittedFiles;
    function GetOutputBaseDir: string;
    procedure SetOutputBaseDir(const Value: string);
    procedure ExportToPDF(Report: TppReport; const FileName: string);
  public
    ParentPhase: TPhase;
    procedure ConnectAllDBComponents(WriteConn, ReadConn: TADOConnection);
    procedure ExportBillingRun(BillID: Integer);
    procedure SetCallCenters(const CallCenters: string);
    procedure ResetToDefaultTemplate;
    property DestinationDir: string read FOutputDir;
    property Logger: TLogEvent read FLogEvent write FLogEvent;
    property OutputBaseDir: string read GetOutputBaseDir write SetOutputBaseDir;
  end;

implementation

uses
  ppTypes, JPEG, Variants, BillingEngineDMu, OdDbUtils, OdMiscUtils,
  DateUtils, Types, OdIsoDates, OdAdoUtils, JclStrings, QMBillingDMu, Windows,
  JclFileUtils,
  ppPDFDevice, ppPDFSettings;

{$R *.dfm}

{$WARN SYMBOL_PLATFORM OFF}
const
  InvalidExportChars = ['"'];

// ******************** Entry Points *************************

{ TBillingExportDM }

procedure TBillingExportDM.ConnectAllDBComponents(WriteConn, ReadConn: TADOConnection);
begin
  SetConnections(WriteConn, Self);
  TicketNotesData.Connection := ReadConn;
end;

procedure TBillingExportDM.ExportBillingRun(BillID: Integer);
begin
  Log('Running export');
  FBillID := BillID;
  GetBillingRunInfo;
  PrepareOutputDirectory;

  if FOutputBaseDir <> '' then begin
    // hackery: if the user specs an override, they are probably a dev,
    // so they want to go straight to the invoices.
    ExportCustomerInvoices;
    ExportTicketNotes;
    ExportSummaryTxt;
    ExportDetail;
    ExportBillingExceptionReport;
  end else begin
    ExportSummaryTxt;
    ExportDetail;
    ExportCustomerInvoices;
    ExportBillingExceptionReport;
  end;
  CloseAll;
end;

// ******************** Private Methods *************************

procedure TBillingExportDM.ppCityStateZipLabelPrint(Sender: TObject);
var
  Line, State, ZIP: string;
  CityExists: Boolean;
begin
  Line := InvoiceHeader.FieldByName('City').AsString;
  State := InvoiceHeader.FieldByName('State').AsString;
  ZIP := InvoiceHeader.FieldByName('ZIP').AsString;

  CityExists := Trim(Line) <> '';
  if Trim(State) <> '' then begin
    if CityExists then
      Line := Line + ', ' + State
    else
      Line := State;
  end;
  if Trim(ZIP) <> '' then
    Line := Line + ' ' + ZIP;

  ppCityStateZIPLabel.Caption := Line;
end;

procedure TBillingExportDM.ppLogoImagePrint(Sender: TObject);
var
  FileName: string;
  lGraphic :TGraphic;  //QMANTWO-785
begin
  Assert(Assigned(ppLogoImage));
  lGraphic := ppLogoImage.Picture.Graphic; //QMANTWO-785
  if DirectoryExists(ExtractFilePath(Application.ExeName) + 'Logos') then
    FileName := ExtractFilePath(Application.ExeName) + 'Logos\' + InvoiceHeader.FieldByName('logo_filename').AsString
  else
    FileName := DestinationDir + 'Logos\' + InvoiceHeader.FieldByName('logo_filename').AsString;

  if FileExists(FileName) then
  begin
    if (lGraphic = nil) or (lGraphic.Empty) then  //QMANTWO-785 no default Logo assigned to template
      ppLogoImage.Picture.LoadFromFile(FileName); // so use the one assigned to the Locating Company
  end
  else
    ppLogoImage.Clear; //if I can't find the one assigned, I will clear any logo (safest)
end;

procedure TBillingExportDM.ppPageStyle1BeforePrint(Sender: TObject);
begin
  // Show the watermark for invoices that aren't committed
  (Sender as TppPageStyle).Visible := not InvoiceIsCommitted;
end;

procedure TBillingExportDM.DesignerClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if InvoiceReport.Modified then begin
    case MessageDlg('Report changed. Do you wish to save?', mtConfirmation,
      [mbYes, mbNo, mbCancel], 0) of
      mrYes: begin
          if TemplateSaveDlg.Execute then begin
            InvoiceReport.Template.Format := ftAscii;
            InvoiceReport.Template.SaveTo := stFile;
            InvoiceReport.Template.FileName := TemplateSaveDlg.FileName;
            InvoiceReport.Template.SaveToFile;
            Action := caFree;
          end
          else
            Action := caNone;
        end;
      mrNo: Action := caFree;
      mrCancel: Action := caNone;
    end;
  end;
end;

procedure TBillingExportDM.InvoiceNumberTextPrint(Sender: TObject);
begin
  // We do this above now
end;

procedure TBillingExportDM.SetPeriodLabel;
var
  i: Integer;
  Lab: TppLabel;
begin
  // Set the Period field on the report properly (date range of start to end date)
  // The fields are not in the data query, but *are* in BillingEngine, so use
  // this approach vs. a data-binding approach, or even a TppLabel.OnPrint approach.
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TppLabel then begin
      Lab := Components[i] as TppLabel;
      Lab.Caption := SetPeriodRange(Lab.Caption);
    end;
  end;
end;

procedure TBillingExportDM.SetInvoiceTemplate(const TemplateName: string);
var
  LocalTemplatePath: string;
  FileName: string;
begin
  // Deal with the case where we're using custom RB templates
  if TemplateName <> '' then begin
    LocalTemplatePath := ExtractFilePath(Application.ExeName) + 'Templates';
    // Use the templates dir if it exists. Else default to using the RootDir.
    if DirectoryExists(LocalTemplatePath) then begin
      FileName := LocalTemplatePath + '\' + TemplateName
    end else begin
      FileName := DestinationDir + 'Templates\' + TemplateName;
    end;
  end;

  if FileName <> '' then begin
    if ExtractFileExt(FileName) = '' then
      FileName := FileName + '.rtm';

    if FileExists(FileName) then begin
      Log('Loading template from file: ' + FileName);
      InvoiceReport.Template.Format := ftASCII;
      InvoiceReport.Template.SaveTo := stFile;
      InvoiceReport.Template.FileName := FileName;
      InvoiceReport.Template.LoadFromFile;
    end else begin
      Log('Error: Missing template file: ' + FileName);
      ResetToDefaultTemplate;
    end;
  end else
    ResetToDefaultTemplate;
end;

procedure TBillingExportDM.ExportCustomerInvoices;
const
  InsertMissingBOCRecs = 'insert into billing_output_config ' +
    '(customer_id, payment_terms, center_group_id, which_invoice) ' +
    'select c.customer_id, ''30 days'' as payment_terms, ' +
    '%d as center_group_id, ' +
    'bd.which_invoice ' +
    'from billing_detail bd ' +
    'inner join client c on bd.client_id = c.client_id and c.customer_id is not null ' +
    'where bd.bill_id = %d ' +
    'and ((bd.area_name <> ''DEL_GAS'')or (bd.area_name is null)) '+ //qm-145 SR
    'group by c.customer_id, bd.which_invoice ' +
    'having not exists ' +
    '(select * from billing_output_config boc ' +
    'where boc.customer_id = c.customer_id and boc.center_group_id = %d ' +
    'and coalesce(boc.which_invoice,'''') = coalesce(bd.which_invoice,''''))';
var
  FileName: string;
  CustomerID: Integer;
  CustomerName: string;
  CustomerNumber: string;
  RecsAffected: Integer;
  OutputConfigID: Integer;
  DateToShow: string;
  InvoiceFileBaseName: string;
  WhichInvoice: string;
  InvoiceAmount: Double;
  TemplateName: string;
  ExportWarning: string;
  InvoiceNumberPart: string;
  RecAffected: Integer;
  InvoicePhase: TPhase;
  CustomerPhase: TPhase;
  IgnoreInvoice: Boolean;
  ExportEmployees: Boolean;
  ExportTasks: Boolean;
begin
  CustomerPhase := nil;
  InvoicePhase := StartPhase('ExportCustomerInvoices');

  if Assigned(InvoicePhase) then
    InvoicePhase.AddSubPhase('ExportCustomerInvoices Init');
  // For ranges ending at midnight, we show the previous day's date for clarity
  if CompareTime(FEndDate, 0.0) = EqualsValue then
    DateToShow := FormatDateTime(FormatSettings.ShortDateFormat, FEndDate - 1.0)
  else
    DateToShow := FormatDateTime(FormatSettings.ShortDateFormat, FEndDate);

  // Add any missing BOC's for the customers in the billing run
  Log('Creating default invoice configurations records as needed');
  Log('  Bill Run ID: ' + IntToStr(FBillID));
  Log('  CCGroup ID: ' + IntToStr(FCallCenterGroupID));
  CreateBOCRecords.CommandText := Format(InsertMissingBOCRecs,
    [FCallCenterGroupID, FBillID, FCallCenterGroupID]);
  CreateBOCRecords.Execute(RecAffected, Null);
  Log('Output Config Rows Created: ' + IntToStr(RecAffected));

  // Loop over all billing_output_config records that have this CCG
  Log('Getting list of invoices to generate');
  OutputConfig.Parameters.ParamValues['cgid1'] := FCallCenterGroupID;
  OutputConfig.Parameters.ParamValues['cgid2'] := FCallCenterGroupID;
  OutputConfig.Parameters.ParamValues['pt'] := FPeriodType;
  OutputConfig.Parameters.ParamValues['bill_id'] := FBillID;
  OutputConfig.Open;
  if OutputConfig.IsEmpty then
    Log('ERROR: No Billing Output Config records found. No invoices will be generated.');

  Log('Setting up Solomon data accumulation');
  ExportEmployees := QMBillingDM.GetIniSetting('Billing', 'ExportEmployees', '1') = '1';
  ExportTasks := QMBillingDM.GetIniSetting('Billing', 'ExportTasks', '1') = '1';
  FSolomonDM.Initialize(FStartDate, FEndDate, ExportEmployees, ExportTasks);

  Log('Writing out invoices...');
  Log('  Date Range: Start: ' + IsoDateTimeToStr(FStartDate) + '  End: ' + IsoDateTimeToStr(FEndDate));
  Assert(Assigned(OutputConfig));
  while not OutputConfig.Eof do begin
    OutputConfigID := OutputConfig.FieldByName('output_config_id').AsInteger;
    if Assigned(InvoicePhase) then
      CustomerPhase := InvoicePhase.AddSubPhase('Invoices For: ' + IntToStr(OutputConfigID));

    // We can't use the reporting DB to gather this data because the saved
    // billing detail/headers may not exist in the Rep DB yet
    Assert(Assigned(InvoiceSP) and Assigned(InvoiceDetails) and Assigned(InvoiceHeader) and Assigned(InvoiceAdjustments));
    // InvoiceSP contains all of the data for the report
    InvoiceSP.Close;

    InvoiceSP.Parameters.Refresh;
    InvoiceDetails.Close;
    InvoiceHeader.Close;
    InvoiceAdjustments.Close;

    if Assigned(CustomerPhase) then
      CustomerPhase.AddSubPhase('Invoice Data For: ' + IntToStr(OutputConfigID));
    Log('Gathering invoice data: ------------------------------');
    Log('  Invoice: ' + WhichInvoice + '   BOC ID: ' + IntToStr(OutputConfigID) + '  BillID: ' + IntToStr(FBillID));
    InvoiceSP.Parameters.ParamValues['@OutputConfigID'] := OutputConfigID;
    InvoiceSP.Parameters.ParamValues['@BillID'] := FBillID;
    InvoiceSP.Open;

    // Grab all the datasets from the SP
    InvoiceDetails.Recordset := InvoiceSP.Recordset;
    InvoiceAdjustments.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    Taxes.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    InvoiceHeader.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    GrayKiely.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    StatusSummary.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    Breakdown.Recordset := InvoiceSP.NextRecordset(RecsAffected);
    TransDetail.RecordSet := InvoiceSP.NextRecordset(RecsAffected);
    SolomonSplit.RecordSet := InvoiceSP.NextRecordset(RecsAffected);
    Log('  Got data from SP');

    Assert(not InvoiceHeader.IsEmpty, 'No invoice header record found, verify the customer''s locating company is set');
    Assert(not InvoiceDetails.IsEmpty, 'No invoice detail records found');

    TemplateName := InvoiceHeader.FieldByName('output_template').AsString;
    InvoiceAmount := InvoiceHeader.FieldByName('grand_total').AsCurrency;
    WhichInvoice := InvoiceHeader.FieldByName('which_invoice').AsString;
    CustomerNumber := InvoiceHeader.FieldByName('customer_number').AsString;
    IgnoreInvoice := False;
    if Assigned(InvoiceHeader.FindField('ignore_invoice')) then
      IgnoreInvoice := InvoiceHeader.FieldByName('ignore_invoice').AsInteger = 1;

    CustomerID := InvoiceHeader.FieldByName('customer_id').AsInteger;
    CustomerName := Trim(InvoiceHeader.FieldByName('customer_name').AsString);
    Log(Format('  Customer: %s  (ID: %d)', [CustomerName, CustomerID]));

    if Assigned(CustomerPhase) then
      CustomerPhase.Phase := Format('Invoices for: %s (ID %d, BOC %d)', [CustomerName, CustomerID, OutputConfigID]);

    if InvoiceHeader.FieldByName('SolOffice').AsString = '' then
      Log('WARNING - this customer does not have Profit Center / Solomon Data Populated');

    Log('Generating invoice exports; template=' + TemplateName);

    Assert(not InvoiceHeader.FieldByName('customer_id').IsNull, 'InvoiceHeader.customer_id is null');
    // If there are no details, no need to create the invoice
    if (not IgnoreInvoice) and (not InvoiceDetails.IsEmpty) or
                               (not InvoiceAdjustments.IsEmpty) or
                               (InvoiceAmount > 0.001) or
                               (InvoiceAmount < -0.001) then begin
      if InvoiceIsCommitted then
        RecordInvoice(CustomerID, FEndDate, InvoiceAmount, WhichInvoice);

      if Assigned(CustomerPhase) then
        CustomerPhase.AddSubPhase('Invoice Generation For: ' + IntToStr(OutputConfigID));
      SetInvoiceTemplate(TemplateName);
      SetPeriodLabel;
      if Assigned(InvoiceNumberText) then
        InvoiceNumberText.Visible := InvoiceIsCommitted
      else
        Log('ERROR: The invoice number display label called InvoiceNumberText is not present');

      Assert(Assigned(ppChildReport2), 'ppChildReport2 is null');
      ppChildReport2.AutoStop := False;

      // We must do this outside the scope of the SP, since the call to RecordInvoice
      // is what we use to populate the billing_invoice table
      BillingInvoiceDS.DataSet := BillingInvoice;
      BillingInvoice.Parameters.ParamValues['billing_header_id'] := FBillID;
      BillingInvoice.Parameters.ParamValues['customer_id'] := CustomerID;
      BillingInvoice.Parameters.ParamValues['end_date'] := FEndDate;
      BillingInvoice.Parameters.ParamValues['which_invoice'] := WhichInvoice;
      BillingInvoice.Open;

      InvoiceFileBaseName := DestinationDir + CleanUp(CustomerName);

      if CustomerNumber <> '' then
        InvoiceFileBaseName := InvoiceFileBaseName + '-' + CustomerNumber;

      InvoiceFileBaseName := InvoiceFileBaseName + '-' + WeekEndingString;

      if WhichInvoice <> '' then
        InvoiceFileBaseName := InvoiceFileBaseName + '-' + WhichInvoice;

      SubCategoryHeaderBand.Visible := False;

      if InvoiceIsCommitted then
        InvoiceNumberPart := VarToStr(BillingInvoice.FieldValues['FullInvoiceNumber'])
      else
        InvoiceNumberPart := 'UNCOM';

      InvoiceFileBaseName := InvoiceFileBaseName + '-' + InvoiceNumberPart;

      FileName := InvoiceFileBaseName + '.pdf';

{$IFDEF DEBUG}
      FileName := 'C:\billing\'+ExtractFileName(FileName);
{$ENDIF}

      Log('Writing ' + FileName);
      ExportToPDF(InvoiceReport, FileName);

      if Assigned(CustomerPhase) then
        CustomerPhase.AddSubPhase('Customer Detail Generation: ' + IntToStr(OutputConfigID));
      // Output a text file (.tsv) with the configured output columns for either
      // this customer or each customer client individually
      ExportCustomerDetails(CustomerID, InvoiceFileBaseName, WhichInvoice);

      if Assigned(CustomerPhase) then
        CustomerPhase.AddSubPhase('Export Xign data for: ' + IntToStr(OutputConfigID));
      ExportXign(OutputConfigID, InvoiceFileBaseName, CustomerName); //qm-542

      if Assigned(CustomerPhase) then
        CustomerPhase.AddSubPhase('Append Solomon Data For: ' + IntToStr(OutputConfigID));
      // Populate the solomon datasets with the data that will be needed later
      Log('Appending Solomon data');
      FSolomonDM.AppendSolomonData(CustomerID, DateToShow, BillingInvoice,
        InvoiceHeader, SolomonSplit, OutputConfigID);
    end
    else
      Log('Warning: No billing data in this billing run for customer ' +
        CustomerName + '. Invoice not generated.');

    // Close all of the datasets
    BillingInvoice.Close;
    InvoiceAdjustments.Close;
    InvoiceDetails.Close;
    InvoiceHeader.Close;
    TransDetail.Close;
    OutputConfig.Next;
  end;

  if Assigned(InvoicePhase) then
    InvoicePhase.AddSubPhase('ExportSolomon');
  // Write the accumulated Solomon data to text file
  Log('Saving Appended Solomon data');
  FSolomonDM.ExportSolomon(DestinationDir, FBillID, ExportWarning);
  if ExportWarning <> '' then
    Log('Solomon Warning: ' + ExportWarning);
  Log('Saved Appended Solomon data');

  OutputConfig.Close;
end;

procedure TBillingExportDM.ExportSummaryTxt;  //utf-8
begin
  StartPhase('ExportSummaryTxt');
  Log('Exporting billing summary...');
  Summary.Parameters[0].Value := FBillID;
  Summary.Open;
  try
    ExportTSV(Summary, DestinationDir + 'billing_summary.txt', nil, IsInvalidExportChar);
  finally
    Summary.Close;
  end;
end;

procedure TBillingExportDM.ExportDetail;  //utf-8
begin
  StartPhase('ExportDetail');
  Log('Exporting billing detail...');
  DetailResults.Parameters[0].Value := FBillID;
  DetailResults.Open;
  try
    ExportTSV(DetailResults, DestinationDir + 'billing_detail.txt', FDetailMemoFields, IsInvalidExportChar);
    Log('Exported billing detail: ' + IntToStr(DetailResults.RecordCount));
  finally
    DetailResults.Close;
  end;
end;

procedure TBillingExportDM.PrepareOutputDirectory;
var
  SummaryFile: string;
  DetailFile: string;
begin
  if FOutputBaseDir = '' then
    ChooseDefaultOutputDir;

  Log('Preparing output directory...');
  Assert(FBillID > 0);
  {$IFDEF DEBUG}
  FOutputDir :=  'C:\billing';
  {$ELSE}
  FOutputDir :=  ExpandFileName(AddSlash(FOutputBaseDir)) +
    'Output-' + IntToStr(FBillID) + '-' +  FGroupCode;
  {$ENDIF}
  FOutputDir := IncludeTrailingPathDelimiter(FOutputDir);

  if not DirectoryExists(DestinationDir) then
    OdForceDirectories(DestinationDir);

  SummaryFile := DestinationDir + 'billing_summary.txt';
  DetailFile := DestinationDir + 'billing_detail.txt';
  if FileExists(SummaryFile) or FileExists(DetailFile) then begin
    Log(Format('Deleting previous log files in %s...', [DestinationDir]));
    SysUtils.DeleteFile(SummaryFile);
    SysUtils.DeleteFile(DetailFile);
  end;

  DeleteOldUncommittedFiles;
end;

procedure TBillingExportDM.DeleteOldUncommittedFiles;
var
  Files: TStringList;
  FileName: string;
  i: Integer;
begin
  Files := TStringList.Create;
  try
    if BuildFileList(DestinationDir + '*-UNCOM*.*', faAnyFile, Files) then
      Log(Format('Deleting %d uncommitted %s in %s...', [Files.Count,
        AddSIfNot1(Files.Count, 'file'), DestinationDir]));
    for i := 0 to Files.Count - 1 do begin
      FileName := DestinationDir + Files[i];
      if not SysUtils.DeleteFile(FileName) then
        Log(Format(' Cannot delete %s. There may be a sharing conflict or permission problem.',
          [FileName]));
    end;
  finally
    FreeAndNil(Files);
  end;
end;

procedure TBillingExportDM.ChooseDefaultOutputDir;
var
  DirConfiguredInDB: string;
  DirConfiguredInINI: string;
begin
  BillingConfigDir.Open;
  try
    if not BillingConfigDir.EOF then
      DirConfiguredInDB := BillingConfigDir.FieldByName('value').AsString;
  finally
    BillingConfigDir.Close;
  end;

  DirConfiguredInINI := QMBillingDM.GetIniSetting('Billing', 'OutputDirOverride', '');

  if DirConfiguredInINI <> '' then
    FOutputBaseDir := DirConfiguredInINI
  else
    FOutputBaseDir := DirConfiguredInDB;
end;

procedure TBillingExportDM.ExportCustomerDetails(CustomerID: Integer; const
  BaseFileName, WhichInvoice: string);
var
  Stream: TMemoryStream;
  OutputFields: TStrings;
  FieldList: string;
begin
  // Get output_fields value for this customer
  Assert(OutputConfig.Active);
  FieldList := OutputConfig.FieldByName('output_fields').AsString;

  Log('Writing ' + BaseFileName + '-detail.txt');

  InvoiceDetailsExport.Parameters.ParamValues['bill_id'] := FBillID;
  InvoiceDetailsExport.Parameters.ParamValues['customer_id'] := CustomerID;
  InvoiceDetailsExport.Parameters.ParamValues['which_invoice'] := WhichInvoice;
  InvoiceDetailsExport.Open;
  Stream := TMemoryStream.Create;
  try
    Stream.SetSize(100000); // Start with it large to avoid reallocations
    if FieldList = '' then
      SaveDelimToStream(etANSI, InvoiceDetailsExport, Stream, #9, True, FDetailMemoFields, nil, IsInvalidExportChar)  //QMANTWO-625
    else begin
      OutputFields := CreateCommaStringList(FieldList);
      try
        SaveOrderedDelimToStream(etANSI, InvoiceDetailsExport, Stream, #9, True, OutputFields, FDetailMemoFields, IsInvalidExportChar); //QMANTWO-625
      finally
        FreeAndNil(OutputFields);
      end;
    end;

    Stream.SaveToFile(BaseFileName + '-detail.txt');

  finally
    FreeAndNil(Stream);
    InvoiceDetailsExport.Close;
  end;

  if not TransDetail.EOF then begin
    Log('Writing ' + BaseFileName + '-trans-detail.txt');
    Stream := TMemoryStream.Create;
    try
      Stream.SetSize(100000);
      SaveDelimToStream(etANSI, TransDetail, Stream, #9, True, nil, nil, IsInvalidExportChar);
      Stream.SaveToFile(BaseFileName + '-trans-detail.txt');
    finally
      FreeAndNil(Stream);
    end;
  end;
end;

procedure TBillingExportDM.Log(Msg: string);
begin
  if Assigned(FLogEvent) then
    FLogEvent(Self, IsoTimeToStrNoMillis(Time) + ': ' + Msg);
  Application.ProcessMessages;
end;

function TBillingExportDM.WeekEndingString: string;
begin
  Result := FormatDateTime('yyyy-mm-dd', FEndDate - 1);
end;

procedure TBillingExportDM.GetBillingRunInfo;
begin
  Log('Getting billing run info for bill ' + IntToStr(FBillID));
  with BillRunInfo do begin
    Parameters.ParamValues['bid'] := FBillID;
    Open;
    FStartDate := FieldByName('bill_start_date').AsDateTime;
    FEndDate := FieldByName('bill_end_date').AsDateTime;
    FPeriodType := FieldByName('period_type').AsString;
    FCallCenterGroupID := FieldByName('center_group_id').AsInteger;
    FGroupCode := FieldByName('group_code').AsString;
    FCommitted := FieldByName('committed').AsBoolean;
  end;
end;

function TBillingExportDM.SetPeriodRange(const Before: string): string;
begin
  Result := StringReplace(Before, '%PERIOD%', FormatDateTime('m/d/yyyy',
    FStartDate) + ' to ' + FormatDateTime('m/d/yyyy', FEndDate - 1), [rfReplaceAll]);
end;

procedure TBillingExportDM.ExportXign(const OutputConfigID: Integer; const BaseFileName, CustomerName: string);

  procedure SetupDateFormats;
  var
    i: Integer;
    Field: TField;
  begin
    for i := 0 to XignExport.FieldCount-1 do begin
      Field := XignExport.Fields[i];
      if Field is TDateTimeField then
        (Field as TDateTimeField).DisplayFormat := 'mm/dd/yyyy';
    end;
  end;

var
  FileName: string;
begin
  StartPhase('ExportXign');
  XignExport.Parameters.ParamByName('@OutputConfigID').Value := OutputConfigID;
  XignExport.Parameters.ParamByName('@BillID').Value := FBillID;
  XignExport.Parameters.ParamByName('@CustomerName').Value := CustomerName; //qm-542
  XignExport.Open;
  try
    SetupDateFormats;
    if not XignExport.EOF then begin
      FileName := BaseFileName + '.csv';
      Log('Writing ' + FileName);
      ExportCSV(XignExport, FileName, nil, nil);
    end;
  finally
    XignExport.Close;
  end;
end;

procedure TBillingExportDM.DataModuleCreate(Sender: TObject);
begin
  FInvoiceStream := TMemoryStream.Create;
  FSolomonDM := TSolomonExportDM.Create(Self); // Auto-freed
  FDetailMemoFields := TStringList.Create;
  FDetailMemoFields.Text := 'work_description';
  FDetailMemoFields.Add('work_remarks');

  StoreDefaultReport;
end;

procedure TBillingExportDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FInvoiceStream);
  FreeAndNil(FDetailMemoFields);
end;

procedure TBillingExportDM.StoreDefaultReport;
begin
  if FInvoiceStream.Size = 0 then
    InvoiceReport.Template.SaveToStream(FInvoiceStream);
end;

function GetNextLetter(v: Variant): string;
begin
  if (VarIsNull(v)) or (Length(v) = 0) or (Trim(v) = '') then
    Result := 'a'
  else if v = 'z' then
    raise Exception.Create('Maximum revisions reached for this invoice') else
  Result := Char(Ord(string(v)[1]) + 1);
end;

procedure TBillingExportDM.RecordInvoice(CustomerID: Integer;
  EndDate: TDateTime; InvoiceAmount: Double; WhichInvoice: string);
begin
  Assert((FBillID > 0) and (CustomerID > 0) and (EndDate > 1));
  RecBillingInvoice.Parameters.ParamValues['billing_header_id'] := FBillID;
  RecBillingInvoice.Parameters.ParamValues['customer_id'] := CustomerID;
  RecBillingInvoice.Parameters.ParamValues['end_date'] := EndDate;
  RecBillingInvoice.Parameters.ParamValues['which_invoice'] := WhichInvoice;
  RecBillingInvoice.Open;
  try
    if RecBillingInvoice.EOF then begin
      RecBillingInvoice.Insert;
      RecBillingInvoice.FieldByName('invoice_id').AsInteger := GetNextInvoiceID;
      RecBillingInvoice.FieldByName('billing_header_id').AsInteger := FBillID;
      RecBillingInvoice.FieldByName('customer_id').AsInteger := CustomerID;
      RecBillingInvoice.FieldByName('end_date').AsDateTime := EndDate;
      {TODO: Once this is accessible from the client GUI, InvoiceDate should be
             passed from the caller instead of using "Date"}
      RecBillingInvoice.FieldByName('invoice_date').AsDateTime := Date;
      RecBillingInvoice.FieldByName('total_amount').AsFloat := InvoiceAmount;
      RecBillingInvoice.FieldByName('which_invoice').AsString := WhichInvoice;
    end else begin
      RecBillingInvoice.Edit;
      // If the invoice amount changed, increment the revision field, but only for committed invoices
      if (InvoiceAmount <> RecBillingInvoice.FieldByName('total_amount').AsFloat) then
        RecBillingInvoice.FieldByName('revision').AsString :=
          GetNextLetter(RecBillingInvoice.FieldByName('revision').Value);
      RecBillingInvoice.FieldByName('total_amount').AsFloat := InvoiceAmount;
    end;
    RecBillingInvoice.Post;
  finally
    RecBillingInvoice.Close;
  end;
end;

function TBillingExportDM.GetNextInvoiceID: Integer;
begin
  NextInvoiceID.Close;
  NextInvoiceID.ExecProc;
  Result := NextInvoiceID.Parameters.ParamValues['@RETURN_VALUE'];
  NextInvoiceID.Close;
end;

procedure TBillingExportDM.DesignerShow(Sender: TObject);
begin
  InvoiceHeader.Open;
  InvoiceDetails.Open;
  BillingInvoiceDS.DataSet := BillingInvoicePreview;
  BillingInvoicePreview.Open;
  Taxes.Open;
  GrayKiely.Open;
  InvoiceAdjustments.Open;
  Breakdown.Open;
end;

procedure TBillingExportDM.DesignerHide(Sender: TObject);
begin
  InvoiceHeader.Close;
  InvoiceDetails.Close;
  BillingInvoicePreview.Close;
  InvoiceAdjustments.Close;
  Taxes.Close;
  GrayKiely.Close;
  Breakdown.Close;
end;

procedure TBillingExportDM.SubCategoryHeaderBandBeforePrint(Sender: TObject);
begin
  // This is this way for a reason, please don't attempt to simplify it
  // in to an assignment.
  if not InvoiceDetails.FieldByName('subcategory').IsNull then
    SubCategoryHeaderBand.Visible := True;
end;

procedure TBillingExportDM.ResetToDefaultTemplate;
begin
  InvoiceReport.Template.LoadFromStream(FInvoiceStream);
end;

function TBillingExportDM.CleanUp(Name: string): string;
begin
  Result := Name;
  Result := StrReplaceChar(Result, ':', '-');
  Result := StrReplaceChar(Result, ';', '-');
  Result := StrReplaceChar(Result, '/', '-');
  Result := StrReplaceChar(Result, '\', '-');
  Result := StrReplaceChar(Result, '&', '-');
  Result := StrReplaceChar(Result, '*', '-');
  Result := StrReplaceChar(Result, '$', '-');
  Result := StrReplaceChar(Result, '`', '-');
  Result := StrReplaceChar(Result, '|', '-');
  Result := StrReplaceChar(Result, '?', '-');
end;

procedure TBillingExportDM.SetOutputBaseDir(const Value: string);
begin
  FOutputBaseDir := Value;
end;

function TBillingExportDM.GetOutputBaseDir: string;
begin
  if IsEmpty(FOutputBaseDir) then
    ChooseDefaultOutputDir;
  Result := FOutputBaseDir;
end;

procedure TBillingExportDM.CloseAll;
begin
  CloseAllADODataSets(Self);
end;

function TBillingExportDM.InvoiceIsCommitted: Boolean;
begin
  Result := InvoiceHeader.FieldByName('committed').AsBoolean;
end;

procedure TBillingExportDM.LineItemDetailBandBeforePrint(Sender: TObject);
begin
  Assert(Assigned(TaxLineItemDetailSubRep));
  TaxLineItemDetailSubRep.Visible := InvoiceDetails.FieldByName('tax_amount').AsFloat > 0.001;
end;

procedure TBillingExportDM.ExportTicketNotes;
var
  MemoFields: TStringList;
  Centers: TStringArray;
begin
  Assert(Trim(FGroupCode) <> '', 'No groups code specified in TBillingExportDM.ExportTicketNotes');

  Centers := BillingShared.CenterGroup(FGroupCode);
  if MatchesCenterGroup(Centers, 'ExportBilledTicketNotes', False) then begin
    Log('Exporting ticket notes...');
    MemoFields := TStringList.Create;
    try
      MemoFields.Add('note');
      TicketNotesData.Parameters[0].Value := FBillID;
      TicketNotesData.Open;
      try
        ExportTSV(TicketNotesData, DestinationDir + 'ticket_notes.txt', MemoFields, IsInvalidExportChar);
      finally
        TicketNotesData.Close;
      end;
    finally
      FreeAndNil(MemoFields);
    end;
  end
  else
    Log('No ticket notes to export (centers not in ExportBilledTicketNotes center group)');
end;

function TBillingExportDM.StartPhase(PhaseName: string): TPhase;
begin
  // Enable once we have better ways to analyze nested phase times
  //if Assigned(ParentPhase) then
  //  Result := ParentPhase.AddSubPhase(PhaseName)
  //else
    Result := nil;
end;

procedure TBillingExportDM.SetCallCenters(const CallCenters: string);
begin
  FCallCenters := CallCenters;
end;

procedure TBillingExportDM.ExportBillingExceptionReport;
const
  REParams = 'BillingException BeginDate=%s EndDate=%s CallCenters=%s Units=%s /o %sBillingException.pdf';
  UnitsValue = '1';
  Exe = 'ReportEngineCGI.exe';
var
  ReportExe: string;
begin
  ReportExe := ExtractFilePath(Application.ExeName) + Exe;
  if FileExists(ReportExe) then begin
    Log('Generating Billing Exception Report with parameters '+Format(REParams,[IsoDateTimeToStr(FStartDate), IsoDateTimeToStr(FEndDate), FCallCenters, UnitsValue, FOutputDir]));
    OdShellExecute(ReportExe, Format(REParams, [IsoDateTimeToStr(FStartDate), IsoDateTimeToStr(FEndDate), FCallCenters, UnitsValue, FOutputDir]), True, SW_HIDE);
  end else
    Log(Format('Warning: Skipping Billing Exception Report because %s is not present in this directory', [Exe]));
end;

procedure TBillingExportDM.ExportToPDF(Report: TppReport; const FileName: string);
var
  Device: TppPDFDevice;
begin
  Report.ShowPrintDialog := False;
  Report.PDFSettings.Author := 'QManager';
  Report.PDFSettings.Title := '';
  Report.PDFSettings.OpenPDFFile := False;
  Device := TppPDFDevice.Create(nil);



  try
    Device.PDFSettings := Report.PDFSettings;
    Device.FileName := FileName;
    Device.Publisher := Report.Publisher;
    Report.PrintToDevices;
  finally
    FreeAndNil(Device);
  end;
end;

end.
