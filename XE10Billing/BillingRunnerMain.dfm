inherited BillingRunnerMainForm: TBillingRunnerMainForm
  Left = 327
  Top = 239
  Caption = 'Billing'
  ClientHeight = 354
  ClientWidth = 656
  Font.Charset = ANSI_CHARSET
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object DataEntryPanel: TPanel
    Left = 0
    Top = 0
    Width = 656
    Height = 161
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label6: TLabel
      Left = 225
      Top = 37
      Width = 65
      Height = 13
      Caption = 'Period Dates:'
    end
    object DescriptionLabel: TLabel
      Left = 225
      Top = 111
      Width = 57
      Height = 13
      Caption = 'Description:'
    end
    object Label1: TLabel
      Left = 225
      Top = 136
      Width = 70
      Height = 13
      Caption = 'Raw Data File:'
    end
    object CenterGroupLabel: TLabel
      Left = 225
      Top = 85
      Width = 62
      Height = 13
      Caption = 'Call Centers:'
    end
    object Label2: TLabel
      Left = 225
      Top = 13
      Width = 88
      Height = 13
      AutoSize = False
      Caption = 'Period Type:'
    end
    object Label3: TLabel
      Left = 386
      Top = 37
      Width = 4
      Height = 13
      Caption = '-'
    end
    object Label4: TLabel
      Left = 225
      Top = 61
      Width = 73
      Height = 13
      AutoSize = False
      Caption = 'Look Back To:'
    end
    object PeriodStarting: TLabel
      Left = 300
      Top = 37
      Width = 56
      Height = 13
      Caption = '--------------'
    end
    object PeriodLookBackDate: TLabel
      Left = 300
      Top = 61
      Width = 56
      Height = 13
      Caption = '--------------'
    end
    object BillingType: TRadioGroup
      Left = 10
      Top = 14
      Width = 185
      Height = 81
      Caption = 'Billing Source / Mode'
      ItemIndex = 0
      Items.Strings = (
        'Run billing from database'
        'Run billing from raw data files'
        'Export raw data files')
      TabOrder = 0
      OnClick = BillingTypeClick
    end
    object RunBillingButton: TButton
      Left = 570
      Top = 129
      Width = 85
      Height = 25
      Caption = 'Run Billing'
      Default = True
      TabOrder = 10
      OnClick = RunBillingButtonClick
    end
    object DescriptionEdit: TEdit
      Left = 300
      Top = 108
      Width = 250
      Height = 21
      TabOrder = 8
      OnChange = DescriptionEditChange
    end
    object OpenFolderCheckBox: TCheckBox
      Left = 16
      Top = 99
      Width = 175
      Height = 17
      Caption = 'Open folder when complete'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object NoExportSaveCheckBox: TCheckBox
      Left = 16
      Top = 117
      Width = 175
      Height = 17
      Caption = 'Don'#39't export/save (testing only)'
      Enabled = False
      TabOrder = 2
    end
    object CenterGroupCombo: TComboBox
      Left = 300
      Top = 82
      Width = 250
      Height = 21
      Style = csDropDownList
      DropDownCount = 28
      TabOrder = 7
      OnChange = CenterGroupChanged
    end
    object PeriodTypeCombo: TComboBox
      Left = 300
      Top = 10
      Width = 77
      Height = 21
      Style = csDropDownList
      TabOrder = 4
      OnChange = PeriodTypeComboChange
    end
    object DevDateOverride: TButton
      Left = 570
      Top = 32
      Width = 85
      Height = 25
      Caption = 'Dev Override...'
      TabOrder = 6
      OnClick = DevDateOverrideClick
    end
    object NoOldRunCleanupCheckBox: TCheckBox
      Left = 16
      Top = 135
      Width = 175
      Height = 17
      Caption = 'Keep old billing runs (testing only)'
      Enabled = False
      TabOrder = 3
    end
    object PeriodEndingDate: TcxDateEdit
      Left = 398
      Top = 34
      Properties.ButtonGlyph.SourceDPI = 96
      Properties.ButtonGlyph.Data = {
        424D420300000000000036000000280000000F0000000D000000010020000000
        000000000000C40E0000C40E0000000000000000000080800000808080FF8080
        80FF808080FF808080FF808080FF808080FF808080FF808080FF808080FF8080
        80FF808080FF808080FF808080FF8080000080800000808080FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF808080FF8080000080800000808080FFFFFFFFFF800000FFFFFF
        FFFF800000FFFFFFFFFF800000FFFFFFFFFF800000FFFFFFFFFF800000FFFFFF
        FFFF808080FF8080000080800000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8080
        80FF8080000080800000808080FFFFFFFFFF800000FFFFFFFFFF800000FFFFFF
        FFFF800000FFFFFFFFFF800000FFFFFFFFFF800000FFFFFFFFFF808080FF8080
        000080800000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF808000008080
        0000808080FFFFFFFFFF800000FFFFFFFFFF800000FFFFFFFFFF800000FFFFFF
        FFFF800000FFFFFFFFFF800000FFFFFFFFFF808080FF80800000808000008080
        80FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF8080000080800000808080FF8080
        80FF808080FF808080FF808080FF808080FF808080FF808080FF808080FF8080
        80FF808080FF808080FF808080FF8080000080800000808080FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF808080FF8080000080800000808080FFFFFFFFFF800000FFFFFF
        FFFF800000FF800000FF800000FF800000FF800000FFFFFFFFFF800000FFFFFF
        FFFF808080FF8080000080800000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8080
        80FF8080000080800000808080FF808080FF808080FF808080FF808080FF8080
        80FF808080FF808080FF808080FF808080FF808080FF808080FF808080FF8080
        0000}
      Properties.OnChange = PeriodEndingDateChange
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      StyleReadOnly.LookAndFeel.NativeStyle = True
      TabOrder = 5
      Width = 116
    end
    object RawDataFileEdit: TcxButtonEdit
      Left = 300
      Top = 133
      Enabled = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = RawDataFileEditButtonClick
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      StyleReadOnly.LookAndFeel.NativeStyle = True
      TabOrder = 9
      Width = 250
    end
    object ClearLogButton: TButton
      Left = 570
      Top = 63
      Width = 85
      Height = 25
      Caption = 'Clear Log'
      TabOrder = 11
      OnClick = ClearLogButtonClick
    end
  end
  object LogMemo: TMemo
    Left = 0
    Top = 161
    Width = 656
    Height = 193
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object CallCenterQuery: TADODataSet
    CommandText = 'select cc_code from call_center'#13#10'where active=1'#13#10'order by 1'
    Parameters = <>
    Left = 512
    Top = 280
  end
  object RawDataDialog: TOpenDialog
    Filter = 'Text Files (*.txt)|*.txt|All Files (*.*)|*.*'
    Left = 572
    Top = 80
  end
end
