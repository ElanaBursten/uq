unit BillingCommitDMu;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TBillingCommitDM = class(TDataModule)
    CommitBilling: TADOStoredProc;
    UncommitBilling: TADOStoredProc;
  public
    procedure Commit(BillID: Integer; CommitedBy: Integer);
    procedure UnCommit(BillID: Integer);
  end;

implementation

{$R *.dfm}

uses
  Variants, OdAdoUtils, QMBillingDMu;

{ TBillingCommitDM }

procedure TBillingCommitDM.Commit(BillID, CommitedBy: Integer);
begin
  CommitBilling.Parameters.ParamValues['@BillID'] := BillID;
  if CommitedBy >0 then
    CommitBilling.Parameters.ParamByName('@CommittedBy').Value := CommitedBy
  else
    CommitBilling.Parameters.ParamByName('@CommittedBy').Value := Null;

  CommitBilling.ExecProc;
  CheckForADOError(CommitBilling.Connection);
end;

procedure TBillingCommitDM.UnCommit(BillID: Integer);
begin
  UncommitBilling.Parameters.ParamValues['@BillID'] := BillID;
  UncommitBilling.ExecProc;
  CheckForADOError(UncommitBilling.Connection);
end;

end.

