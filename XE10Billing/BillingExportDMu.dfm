object BillingExportDM: TBillingExportDM
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 527
  Width = 797
  object Summary: TADODataSet
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select billing_cc, bucket, count(locate_id) as locates,'#13#10'  sum(q' +
      'ty_marked) as qty_marked, sum(price) as price '#13#10'from billing_det' +
      'ail bd'#13#10' inner join billing_header bh on bd.bill_id=bh.bill_id'#13#10 +
      ' where bh.bill_id = :bill_id'#13#10'group by billing_cc, bucket'#13#10'order' +
      ' by 1, 2'
    Parameters = <
      item
        Name = 'bill_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 476
    Top = 80
  end
  object DetailResults: TADODataSet
    CacheSize = 100
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select '#13#10' billing_cc, ticket_number, status,  qty_marked, bucket' +
      ', price,'#13#10' work_address_number, work_address_number_2, work_addr' +
      'ess_street,'#13#10' work_lat, work_long, area_name, transmit_date, tic' +
      'ket_type, due_date,'#13#10' initial_status_date, closed_date, work_cro' +
      'ss, rate, locate_id,'#13#10' bd.billing_detail_id, bd.bill_id, work_st' +
      'ate, work_county, work_city,'#13#10' con_name, work_done_for, work_typ' +
      'e, emp_number, short_name,'#13#10' call_date, transmit_count, hours, l' +
      'ine_item_text, revision, map_ref,'#13#10' locate_hours_id, work_descri' +
      'ption, update_of,'#13#10' bd.tax_name, bd.tax_rate, bd.tax_amount, qty' +
      '_charged,'#13#10' bd.high_profile, bd.plat, bd.units_marked, bd.work_r' +
      'emarks, '#13#10' bd.locate_added_by,rate_value_group_id, additional_li' +
      'ne_item_text '#13#10'from billing_detail bd'#13#10' inner join billing_heade' +
      'r bh on bd.bill_id=bh.bill_id '#13#10'where bh.bill_id = :bill_id'
    Parameters = <
      item
        Name = 'bill_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 476
    Top = 128
  end
  object InvoiceHeader: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select '#13#10#39'ACME Utilities'#39' as customer_name, '#13#10#39'123 Main Street'#39' ' +
      'as street,'#13#10#39'Box 4437'#39' as street_2,'#13#10#39'John Smith'#39' as attention,'#13 +
      #10#39'Attn: John Smith'#39' as attention_tagged,'#13#10#39'Contact Name'#39' as cont' +
      'act_name ,'#13#10#39'someperson@email.com'#39' as contact_email,'#13#10#39'Los Angel' +
      'es'#39' as city,'#13#10#39'CA'#39' as state,'#13#10#39'90210'#39' as zip,'#13#10#39'555-555-1212'#39' as' +
      ' phone,'#13#10'GetDate() as invoice_date,'#13#10#39'ACME101'#39' as customer_numbe' +
      'r,'#13#10#39'30 days'#39' as payment_terms,'#13#10#39'PO 101'#39' as cust_po_num,'#13#10#39'1234' +
      '5'#39' as contract,'#13#10#39'Custom ACME Corporation footer'#39' as billing_foo' +
      'ter,'#13#10#39#39' as logo_filename,'#13#10' 100 as total_line_items,'#13#10' 15 as to' +
      'tal_adjustments_percent,'#13#10' 25 as total_adjustments_dollar,'#13#10' 40 ' +
      'as total_adjustments,'#13#10' 140 as subtotal_with_adjustments,'#13#10' 12 a' +
      's total_tax,'#13#10' 162 as grand_total,'#13#10' cast(0 as bit) as committed' +
      ','#13#10' 34343 as billing_run_id,'#13#10' '#39'Custom Profit Center billing con' +
      'tact name and phone'#39' as billing_contact'#13#10
    CommandTimeout = 120
    Parameters = <>
    Left = 40
    Top = 284
  end
  object InvoiceDetails: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select '#13#10#39'MEGACORP'#39' as billing_cc,'#13#10#39'One party charge'#39' as line_i' +
      'tem_text,'#13#10'10 as Tickets,'#13#10'10 as Units,'#13#10' 4.3 as Hours,'#13#10'10.00 a' +
      's rate,'#13#10'10.00 as price,'#13#10'100.00 as amount,'#13#10'0 as committed,'#13#10#39'C' +
      'ategory1'#39' as subcategory,'#13#10' 92.50 as pre_tax_amount,'#13#10' '#39'Some Tax' +
      ' Here'#39' as tax_name,'#13#10' 4.453 as tax_rate,'#13#10' 7.50 as tax_amount,'#13#10 +
      ' 0 as omit_zero_amount,'#13#10'10 as UnitsCharged'#13#10
    CommandTimeout = 120
    Parameters = <>
    Left = 136
    Top = 284
  end
  object InvoiceReport: TppReport
    AutoStop = False
    DataPipeline = InvoiceDetailsPipe
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Duplex = dpNone
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    Template.FileName = 'C:\Trunk1\XE10Billing\Templates\UQ-default-Plain.rtm'
    Template.Format = ftASCII
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    EmailSettings.ConnectionSettings.MailService = 'SMTP'
    EmailSettings.ConnectionSettings.WebMail.GmailSettings.OAuth2.AuthStorage = [oasAccessToken, oasRefreshToken]
    EmailSettings.ConnectionSettings.WebMail.GmailSettings.OAuth2.RedirectURI = 'http://localhost'
    EmailSettings.ConnectionSettings.WebMail.GmailSettings.OAuth2.RedirectPort = 0
    EmailSettings.ConnectionSettings.WebMail.Outlook365Settings.OAuth2.AuthStorage = [oasAccessToken, oasRefreshToken]
    EmailSettings.ConnectionSettings.WebMail.Outlook365Settings.OAuth2.RedirectURI = 'http://localhost'
    EmailSettings.ConnectionSettings.WebMail.Outlook365Settings.OAuth2.RedirectPort = 0
    EmailSettings.ConnectionSettings.EnableMultiPlugin = False
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    ThumbnailSettings.PageHighlight.Width = 3
    ThumbnailSettings.ThumbnailSize = tsSmall
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.AllowExtract = True
    PDFSettings.EncryptSettings.AllowAssemble = True
    PDFSettings.EncryptSettings.AllowQualityPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.EncryptSettings.EncryptionType = etRC4
    PDFSettings.DigitalSignatureSettings.SignPDF = False
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    PDFSettings.PDFAFormat = pafNone
    PreviewFormSettings.PageBorder.mmPadding = 0
    RTFSettings.AppName = 'ReportBuilder'
    RTFSettings.Author = 'ReportBuilder'
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    RTFSettings.Title = 'Report'
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    XLSSettings.WorksheetName = 'Report'
    CloudDriveSettings.DropBoxSettings.OAuth2.AuthStorage = [oasAccessToken, oasRefreshToken]
    CloudDriveSettings.DropBoxSettings.OAuth2.RedirectURI = 'http://localhost'
    CloudDriveSettings.DropBoxSettings.OAuth2.RedirectPort = 0
    CloudDriveSettings.DropBoxSettings.DirectorySupport = True
    CloudDriveSettings.GoogleDriveSettings.OAuth2.AuthStorage = [oasAccessToken, oasRefreshToken]
    CloudDriveSettings.GoogleDriveSettings.OAuth2.RedirectURI = 'http://localhost'
    CloudDriveSettings.GoogleDriveSettings.OAuth2.RedirectPort = 0
    CloudDriveSettings.GoogleDriveSettings.DirectorySupport = False
    CloudDriveSettings.OneDriveSettings.OAuth2.AuthStorage = [oasAccessToken, oasRefreshToken]
    CloudDriveSettings.OneDriveSettings.OAuth2.RedirectURI = 'http://localhost'
    CloudDriveSettings.OneDriveSettings.OAuth2.RedirectPort = 0
    CloudDriveSettings.OneDriveSettings.DirectorySupport = True
    Left = 36
    Top = 128
    Version = '22.02'
    mmColumnWidth = 0
    DataPipelineName = 'InvoiceDetailsPipe'
    object ppHeaderBand3: TppHeaderBand
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 67469
      mmPrintPosition = 0
      object ppLabel23: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label23'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'INVOICE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 18
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 7673
        mmLeft = 174890
        mmTop = 794
        mmWidth = 26194
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel9: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label9'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Invoice:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 25400
        mmWidth = 13462
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel12: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label12'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Invoice Date:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 30163
        mmWidth = 22098
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel18: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label18'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Period:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 34925
        mmWidth = 12277
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel19: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label19'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Client Code:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 39688
        mmWidth = 20997
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel20: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label20'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Terms:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 44450
        mmWidth = 11853
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel21: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label21'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Customer P.O.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 49213
        mmWidth = 25908
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel22: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label22'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Contract No.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 135732
        mmTop = 53975
        mmWidth = 22521
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText20: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText20'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'invoice_date'
        DataPipeline = BillingInvoicePipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'BillingInvoicePipe'
        mmHeight = 4233
        mmLeft = 163513
        mmTop = 30163
        mmWidth = 35719
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText22: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText22'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'customer_number'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 4233
        mmLeft = 163513
        mmTop = 39688
        mmWidth = 15610
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText23: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText23'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'payment_terms'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 4233
        mmLeft = 163513
        mmTop = 44450
        mmWidth = 12171
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText24: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText24'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'cust_po_num'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 4233
        mmLeft = 163513
        mmTop = 49213
        mmWidth = 11642
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText25: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText25'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'contract'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 4233
        mmLeft = 163513
        mmTop = 53975
        mmWidth = 9260
        BandType = 0
        LayerName = Foreground5
      end
      object ppDBText26: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText26'
        HyperlinkEnabled = False
        AutoSize = True
        Border.mmPadding = 0
        DataField = 'customer_name'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 4233
        mmLeft = 1852
        mmTop = 27517
        mmWidth = 23283
        BandType = 0
        LayerName = Foreground5
      end
      object ppLogoImage: TppImage
        OnPrint = ppLogoImagePrint
        DesignLayer = ppDesignLayer6
        UserName = 'LogoImage'
        AlignHorizontal = ahCenter
        AlignVertical = avCenter
        MaintainAspectRatio = True
        Stretch = True
        Border.mmPadding = 0
        mmHeight = 23548
        mmLeft = 1852
        mmTop = 1323
        mmWidth = 120121
        BandType = 0
        LayerName = Foreground5
      end
      object ppSystemVariable4: TppSystemVariable
        DesignLayer = ppDesignLayer6
        UserName = 'SystemVariable4'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 184680
        mmTop = 9790
        mmWidth = 16404
        BandType = 0
        LayerName = Foreground5
      end
      object ppShape1: TppShape
        DesignLayer = ppDesignLayer6
        UserName = 'Shape1'
        mmHeight = 6086
        mmLeft = 0
        mmTop = 61382
        mmWidth = 203465
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel28: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label28'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'TermID'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 9790
        mmTop = 62442
        mmWidth = 12171
        BandType = 0
        LayerName = Foreground5
      end
      object ppLine8: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line8'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6086
        mmLeft = 28840
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel29: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label29'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Itemized Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 41540
        mmTop = 62177
        mmWidth = 34925
        BandType = 0
        LayerName = Foreground5
      end
      object ppLine9: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line9'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6085
        mmLeft = 83873
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel30: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label30'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Tickets'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 87842
        mmTop = 62177
        mmWidth = 12171
        BandType = 0
        LayerName = Foreground5
      end
      object ppLine12: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line12'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6085
        mmLeft = 105304
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel31: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label31'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Units'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 110861
        mmTop = 62177
        mmWidth = 8996
        BandType = 0
        LayerName = Foreground5
      end
      object ppLine13: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line13'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6085
        mmLeft = 125148
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel32: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label32'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 156369
        mmTop = 62442
        mmWidth = 7673
        BandType = 0
        LayerName = Foreground5
      end
      object ppLine14: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line14'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6085
        mmLeft = 173567
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel33: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label33'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Amount'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4233
        mmLeft = 183621
        mmTop = 62442
        mmWidth = 13494
        BandType = 0
        LayerName = Foreground5
      end
      object ppPeriod: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Period'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = '%PERIOD%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4064
        mmLeft = 163513
        mmTop = 34925
        mmWidth = 19897
        BandType = 0
        LayerName = Foreground5
      end
      object ppRegion1: TppRegion
        DesignLayer = ppDesignLayer6
        UserName = 'Region1'
        Brush.Style = bsClear
        Caption = 'Region1'
        Pen.Style = psClear
        Pen.Width = 0
        Transparent = True
        mmHeight = 19579
        mmLeft = 1588
        mmTop = 32015
        mmWidth = 123296
        BandType = 0
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppDBText27: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText27'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'street'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 3440
          mmTop = 36513
          mmWidth = 19843
          BandType = 0
          LayerName = Foreground5
        end
        object ppCityStateZipLabel: TppLabel
          OnPrint = ppCityStateZipLabelPrint
          DesignLayer = ppDesignLayer6
          UserName = 'CityStateZipLabel'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = 'Los Angeles, CA 90210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3175
          mmLeft = 3440
          mmTop = 43392
          mmWidth = 24342
          BandType = 0
          LayerName = Foreground5
        end
        object ppDBText30: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText30'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'phone'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3705
          mmLeft = 3440
          mmTop = 46831
          mmWidth = 17991
          BandType = 0
          LayerName = Foreground5
        end
        object ppDBText5: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText5'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'street_2'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 3440
          mmTop = 39952
          mmWidth = 12170
          BandType = 0
          LayerName = Foreground5
        end
        object ppDBText6: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText6'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'attention_tagged'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 3440
          mmTop = 32544
          mmWidth = 20637
          BandType = 0
          LayerName = Foreground5
        end
        object ppDBText7: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText7'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          DataField = 'contact_name'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 77788
          mmTop = 37836
          mmWidth = 44978
          BandType = 0
          LayerName = Foreground5
        end
        object ppDBText8: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText8'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          DataField = 'contact_email'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = []
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 77788
          mmTop = 41805
          mmWidth = 44978
          BandType = 0
          LayerName = Foreground5
        end
      end
      object ppLine17: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line17'
        Border.mmPadding = 0
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 6085
        mmLeft = 146844
        mmTop = 61383
        mmWidth = 2910
        BandType = 0
        LayerName = Foreground5
      end
      object ppLabel13: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label13'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'Hours'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 4191
        mmLeft = 131234
        mmTop = 62177
        mmWidth = 10160
        BandType = 0
        LayerName = Foreground5
      end
      object InvoiceNumberText: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'InvoiceNumberText'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'FullInvoiceNumber'
        DataPipeline = BillingInvoicePipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'BillingInvoicePipe'
        mmHeight = 3969
        mmLeft = 163513
        mmTop = 25400
        mmWidth = 32015
        BandType = 0
        LayerName = Foreground5
      end
    end
    object LineItemDetailBand: TppDetailBand
      BeforePrint = LineItemDetailBandBeforePrint
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object ppDBText17: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText17'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'Tickets'
        DataPipeline = InvoiceDetailsPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3704
        mmLeft = 84931
        mmTop = 529
        mmWidth = 17198
        BandType = 4
        LayerName = Foreground5
      end
      object ppDBText18: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText18'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'UnitsCharged'
        DataPipeline = InvoiceDetailsPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3704
        mmLeft = 105569
        mmTop = 529
        mmWidth = 17198
        BandType = 4
        LayerName = Foreground5
      end
      object ppDBText19: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText19'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'rate'
        DataPipeline = InvoiceDetailsPipe
        DisplayFormat = '#,0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3704
        mmLeft = 151077
        mmTop = 529
        mmWidth = 20373
        BandType = 4
        LayerName = Foreground5
      end
      object ppDBText31: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText31'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'Amount'
        DataPipeline = InvoiceDetailsPipe
        DisplayFormat = '#,0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3704
        mmLeft = 177271
        mmTop = 529
        mmWidth = 21438
        BandType = 4
        LayerName = Foreground5
      end
      object ppLabel24: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label24'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = '$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3704
        mmLeft = 148696
        mmTop = 529
        mmWidth = 1852
        BandType = 4
        LayerName = Foreground5
      end
      object ppLabel25: TppLabel
        DesignLayer = ppDesignLayer6
        UserName = 'Label25'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = '$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        Transparent = True
        mmHeight = 3704
        mmLeft = 174890
        mmTop = 529
        mmWidth = 1852
        BandType = 4
        LayerName = Foreground5
      end
      object ppLine10: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line10'
        Border.mmPadding = 0
        ParentHeight = True
        Position = lpLeft
        Weight = 0.750000000000000000
        mmHeight = 5556
        mmLeft = 0
        mmTop = 0
        mmWidth = 2910
        BandType = 4
        LayerName = Foreground5
      end
      object ppDBText32: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText32'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'line_item_text'
        DataPipeline = InvoiceDetailsPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3704
        mmLeft = 32281
        mmTop = 529
        mmWidth = 51858
        BandType = 4
        LayerName = Foreground5
      end
      object ppDBText16: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText16'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'billing_cc'
        DataPipeline = InvoiceDetailsPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = [fsBold]
        SuppressRepeatedValues = True
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3969
        mmLeft = 1851
        mmTop = 529
        mmWidth = 29899
        BandType = 4
        LayerName = Foreground5
      end
      object ppLine18: TppLine
        DesignLayer = ppDesignLayer6
        UserName = 'Line18'
        Border.mmPadding = 0
        ParentHeight = True
        Position = lpRight
        Weight = 0.750000000000000000
        mmHeight = 5556
        mmLeft = 200290
        mmTop = 0
        mmWidth = 3175
        BandType = 4
        LayerName = Foreground5
      end
      object TaxLineItemDetailSubRep: TppSubReport
        DesignLayer = ppDesignLayer6
        UserName = 'TaxLineItemDetailSubRep'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        mmHeight = 794
        mmLeft = 0
        mmTop = 4763
        mmWidth = 203200
        BandType = 4
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport5: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Template.Format = ftASCII
          Version = '22.02'
          mmColumnWidth = 0
          object ppDetailBand3: TppDetailBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 13229
            mmPrintPosition = 0
            object ppLabel15: TppLabel
              DesignLayer = ppDesignLayer1
              UserName = 'Label15'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Pre Tax Subtotal:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 46038
              mmTop = 794
              mmWidth = 21960
              BandType = 4
              LayerName = Foreground
            end
            object ppDBText40: TppDBText
              DesignLayer = ppDesignLayer1
              UserName = 'DBText40'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Color = clBlack
              DataField = 'pre_tax_amount'
              DataPipeline = InvoiceDetailsPipe
              DisplayFormat = '#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'InvoiceDetailsPipe'
              mmHeight = 3704
              mmLeft = 86519
              mmTop = 794
              mmWidth = 20373
              BandType = 4
              LayerName = Foreground
            end
            object ppDBText41: TppDBText
              DesignLayer = ppDesignLayer1
              UserName = 'DBText41'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Color = clBlack
              DataField = 'tax_name'
              DataPipeline = InvoiceDetailsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              DataPipelineName = 'InvoiceDetailsPipe'
              mmHeight = 3704
              mmLeft = 46038
              mmTop = 4763
              mmWidth = 26988
              BandType = 4
              LayerName = Foreground
            end
            object ppDBText42: TppDBText
              DesignLayer = ppDesignLayer1
              UserName = 'DBText42'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Color = clBlack
              DataField = 'tax_rate'
              DataPipeline = InvoiceDetailsPipe
              DisplayFormat = '#0.0000 %'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'InvoiceDetailsPipe'
              mmHeight = 3704
              mmLeft = 86519
              mmTop = 4763
              mmWidth = 24077
              BandType = 4
              LayerName = Foreground
            end
            object ppLabel16: TppLabel
              DesignLayer = ppDesignLayer1
              UserName = 'Label16'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Tax Amount:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 46038
              mmTop = 8731
              mmWidth = 16140
              BandType = 4
              LayerName = Foreground
            end
            object ppDBText43: TppDBText
              DesignLayer = ppDesignLayer1
              UserName = 'DBText43'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Color = clBlack
              DataField = 'tax_amount'
              DataPipeline = InvoiceDetailsPipe
              DisplayFormat = '#,0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'InvoiceDetailsPipe'
              mmHeight = 3704
              mmLeft = 86519
              mmTop = 8731
              mmWidth = 20373
              BandType = 4
              LayerName = Foreground
            end
            object ppLine23: TppLine
              DesignLayer = ppDesignLayer1
              UserName = 'Line23'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpRight
              Weight = 0.750000000000000000
              mmHeight = 13229
              mmLeft = 200555
              mmTop = 0
              mmWidth = 2910
              BandType = 4
              LayerName = Foreground
            end
            object ppLine27: TppLine
              DesignLayer = ppDesignLayer1
              UserName = 'Line27'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpLeft
              Weight = 0.750000000000000000
              mmHeight = 13229
              mmLeft = 0
              mmTop = 0
              mmWidth = 2910
              BandType = 4
              LayerName = Foreground
            end
          end
          object ppDesignLayers1: TppDesignLayers
            object ppDesignLayer1: TppDesignLayer
              UserName = 'Foreground'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object ppDBText14: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText14'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'Hours'
        DataPipeline = InvoiceDetailsPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'InvoiceDetailsPipe'
        mmHeight = 3703
        mmLeft = 128324
        mmTop = 528
        mmWidth = 17198
        BandType = 4
        LayerName = Foreground5
      end
    end
    object ppFooterBand3: TppFooterBand
      Border.mmPadding = 0
      mmBottomOffset = 0
      mmHeight = 10319
      mmPrintPosition = 0
      object ppDBText28: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'DBText28'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'billing_footer'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 3969
        mmLeft = 0
        mmTop = 1058
        mmWidth = 203200
        BandType = 8
        LayerName = Foreground5
      end
      object BillingRunIDText: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'BillingRunIDText'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'billing_run_id'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 2879
        mmLeft = 0
        mmTop = 1058
        mmWidth = 17198
        BandType = 8
        LayerName = Foreground5
      end
      object BillingContactInfo: TppDBText
        DesignLayer = ppDesignLayer6
        UserName = 'BillingContactInfo'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        DataField = 'billing_contact'
        DataPipeline = InvoiceHeaderPipe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'InvoiceHeaderPipe'
        mmHeight = 3969
        mmLeft = 0
        mmTop = 5292
        mmWidth = 203200
        BandType = 8
        LayerName = Foreground5
      end
    end
    object ppSummaryBand2: TppSummaryBand
      Border.mmPadding = 0
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 35983
      mmPrintPosition = 0
      object AdjustmentSubReport: TppSubReport
        DesignLayer = ppDesignLayer6
        UserName = 'AdjustmentSubReport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        TraverseAllData = False
        DataPipelineName = 'InvoiceAdjustmentsPipe'
        mmHeight = 4498
        mmLeft = 0
        mmTop = 0
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = InvoiceAdjustmentsPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Template.Format = ftASCII
          Version = '22.02'
          mmColumnWidth = 0
          DataPipelineName = 'InvoiceAdjustmentsPipe'
          object ppDetailBand4: TppDetailBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5292
            mmPrintPosition = 0
            object ppDBText29: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText29'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'AdjustmentAmount'
              DataPipeline = InvoiceAdjustmentsPipe
              DisplayFormat = '#,0.00;(#,0.00)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'InvoiceAdjustmentsPipe'
              mmHeight = 3704
              mmLeft = 177271
              mmTop = 794
              mmWidth = 21431
              BandType = 4
              LayerName = Foreground1
            end
            object ppDBText34: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText34'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'description'
              DataPipeline = InvoiceAdjustmentsPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'InvoiceAdjustmentsPipe'
              mmHeight = 3704
              mmLeft = 33073
              mmTop = 794
              mmWidth = 71702
              BandType = 4
              LayerName = Foreground1
            end
            object ppLine19: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line19'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpLeft
              Weight = 0.750000000000000000
              mmHeight = 5292
              mmLeft = 0
              mmTop = 0
              mmWidth = 2910
              BandType = 4
              LayerName = Foreground1
            end
            object ppLine21: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line21'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpRight
              Weight = 0.750000000000000000
              mmHeight = 5292
              mmLeft = 200819
              mmTop = 0
              mmWidth = 2646
              BandType = 4
              LayerName = Foreground1
            end
            object ppLabel39: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label39'
              HyperlinkEnabled = False
              Anchors = [atLeft, atBottom]
              Border.mmPadding = 0
              Caption = '$'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 174890
              mmTop = 794
              mmWidth = 1852
              BandType = 4
              LayerName = Foreground1
            end
          end
          object ppSummaryBand3: TppSummaryBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 6350
            mmPrintPosition = 0
            object ppLine11: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line11'
              Border.mmPadding = 0
              Weight = 0.750000000000000000
              mmHeight = 1852
              mmLeft = 30956
              mmTop = 1058
              mmWidth = 172244
              BandType = 7
              LayerName = Foreground1
            end
            object ppLabel26: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label26'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Subtotal for'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3810
              mmLeft = 31750
              mmTop = 1852
              mmWidth = 17865
              BandType = 7
              LayerName = Foreground1
            end
            object ppDBText21: TppDBText
              DesignLayer = ppDesignLayer2
              UserName = 'DBText21'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'customer_number'
              DataPipeline = InvoiceHeaderPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'InvoiceHeaderPipe'
              mmHeight = 3969
              mmLeft = 53975
              mmTop = 1852
              mmWidth = 53446
              BandType = 7
              LayerName = Foreground1
            end
            object ppDBCalc9: TppDBCalc
              DesignLayer = ppDesignLayer2
              UserName = 'DBCalc9'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'AdjustmentAmount'
              DataPipeline = InvoiceAdjustmentsPipe
              DisplayFormat = '#,0.00;(#,0.00)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'InvoiceAdjustmentsPipe'
              mmHeight = 3969
              mmLeft = 177007
              mmTop = 1852
              mmWidth = 21696
              BandType = 7
              LayerName = Foreground1
            end
            object ppLine20: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line20'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpLeft
              Weight = 0.750000000000000000
              mmHeight = 6350
              mmLeft = 0
              mmTop = 0
              mmWidth = 2910
              BandType = 7
              LayerName = Foreground1
            end
            object ppLine22: TppLine
              DesignLayer = ppDesignLayer2
              UserName = 'Line22'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpRight
              Weight = 0.750000000000000000
              mmHeight = 6350
              mmLeft = 200819
              mmTop = 0
              mmWidth = 2646
              BandType = 7
              LayerName = Foreground1
            end
            object ppLabel40: TppLabel
              DesignLayer = ppDesignLayer2
              UserName = 'Label40'
              HyperlinkEnabled = False
              Anchors = [atLeft, atBottom]
              Border.mmPadding = 0
              Caption = '$'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 174890
              mmTop = 1852
              mmWidth = 1852
              BandType = 7
              LayerName = Foreground1
            end
          end
          object ppDesignLayers2: TppDesignLayers
            object ppDesignLayer2: TppDesignLayer
              UserName = 'Foreground1'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object TotalsRegion: TppRegion
        DesignLayer = ppDesignLayer6
        UserName = 'TotalsRegion'
        Caption = 'TotalsRegion'
        ParentWidth = True
        ShiftRelativeTo = TaxSubReport
        Stretch = True
        mmHeight = 6615
        mmLeft = 0
        mmTop = 17991
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppShape3: TppShape
          DesignLayer = ppDesignLayer6
          UserName = 'Shape3'
          Anchors = [atLeft, atBottom]
          mmHeight = 6615
          mmLeft = 0
          mmTop = 17991
          mmWidth = 203465
          BandType = 7
          LayerName = Foreground5
        end
        object ppLabel37: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label37'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = 'Please Pay This Amount:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3969
          mmLeft = 133879
          mmTop = 19049
          mmWidth = 38365
          BandType = 7
          LayerName = Foreground5
        end
        object ppLabel38: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label38'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = '$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3969
          mmLeft = 174890
          mmTop = 19050
          mmWidth = 1852
          BandType = 7
          LayerName = Foreground5
        end
        object ppDBText36: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText36'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          DataField = 'grand_total'
          DataPipeline = InvoiceHeaderPipe
          DisplayFormat = ',#.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3969
          mmLeft = 177271
          mmTop = 19050
          mmWidth = 21431
          BandType = 7
          LayerName = Foreground5
        end
      end
      object SubTotalRegion: TppRegion
        DesignLayer = ppDesignLayer6
        UserName = 'SubTotalRegion'
        Caption = 'SubTotalRegion'
        ShiftRelativeTo = AdjustmentSubReport
        Stretch = True
        mmHeight = 8202
        mmLeft = 0
        mmTop = 4498
        mmWidth = 203465
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppDBText35: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText35'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          DataField = 'subtotal_with_adjustments'
          DataPipeline = InvoiceHeaderPipe
          DisplayFormat = ',#.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = []
          ParentDataPipeline = False
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3704
          mmLeft = 103187
          mmTop = 6615
          mmWidth = 21438
          BandType = 7
          LayerName = Foreground5
        end
        object ppLabel34: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label34'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = 'Subtotal for Client'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3704
          mmLeft = 794
          mmTop = 6615
          mmWidth = 27517
          BandType = 7
          LayerName = Foreground5
        end
        object ppDBText33: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText33'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          AutoSize = True
          Border.mmPadding = 0
          DataField = 'customer_number'
          DataPipeline = InvoiceHeaderPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          Transparent = True
          DataPipelineName = 'InvoiceHeaderPipe'
          mmHeight = 3968
          mmLeft = 30163
          mmTop = 6615
          mmWidth = 14287
          BandType = 7
          LayerName = Foreground5
        end
        object ppLabel36: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label36'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = '$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = []
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3969
          mmLeft = 103187
          mmTop = 6615
          mmWidth = 1852
          BandType = 7
          LayerName = Foreground5
        end
        object ppLabel10: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label10'
          HyperlinkEnabled = False
          AutoSize = False
          Border.mmPadding = 0
          Caption = ' '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 4233
          mmLeft = 66411
          mmTop = 7938
          mmWidth = 11377
          BandType = 7
          LayerName = Foreground5
        end
      end
      object TaxSubReport: TppSubReport
        DesignLayer = ppDesignLayer6
        UserName = 'TaxSubReport'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = SubTotalRegion
        TraverseAllData = False
        DataPipelineName = 'TaxesPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 12700
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport3: TppChildReport
          AutoStop = False
          DataPipeline = TaxesPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Template.Format = ftASCII
          Version = '22.02'
          mmColumnWidth = 0
          DataPipelineName = 'TaxesPipe'
          object ppDetailBand1: TppDetailBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 5292
            mmPrintPosition = 0
            object ppDBText1: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBText1'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'tax_name'
              DataPipeline = TaxesPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'TaxesPipe'
              mmHeight = 3598
              mmLeft = 24606
              mmTop = 795
              mmWidth = 61119
              BandType = 4
              LayerName = Foreground2
            end
            object ppDBText2: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBText2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'tax_rate'
              DataPipeline = TaxesPipe
              DisplayFormat = '#0.0000 %'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TaxesPipe'
              mmHeight = 3704
              mmLeft = 95779
              mmTop = 794
              mmWidth = 16933
              BandType = 4
              LayerName = Foreground2
            end
            object ppDBText3: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBText3'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'tax_amount'
              DataPipeline = TaxesPipe
              DisplayFormat = '#,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TaxesPipe'
              mmHeight = 3704
              mmLeft = 177271
              mmTop = 794
              mmWidth = 21431
              BandType = 4
              LayerName = Foreground2
            end
            object ppLine1: TppLine
              DesignLayer = ppDesignLayer3
              UserName = 'Line201'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpLeft
              StretchWithParent = True
              Weight = 0.750000000000000000
              mmHeight = 5292
              mmLeft = 0
              mmTop = 0
              mmWidth = 2910
              BandType = 4
              LayerName = Foreground2
            end
            object ppLine2: TppLine
              DesignLayer = ppDesignLayer3
              UserName = 'Line2'
              Border.mmPadding = 0
              ParentHeight = True
              Position = lpRight
              Weight = 0.750000000000000000
              mmHeight = 5292
              mmLeft = 201084
              mmTop = 0
              mmWidth = 2381
              BandType = 4
              LayerName = Foreground2
            end
            object ppDBText4: TppDBText
              DesignLayer = ppDesignLayer3
              UserName = 'DBText4'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'amount'
              DataPipeline = TaxesPipe
              DisplayFormat = '$ #,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'TaxesPipe'
              mmHeight = 3704
              mmLeft = 145786
              mmTop = 794
              mmWidth = 19579
              BandType = 4
              LayerName = Foreground2
            end
            object ppLabel1: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'Label1'
              HyperlinkEnabled = False
              Anchors = [atLeft, atBottom]
              Border.mmPadding = 0
              Caption = '$'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 174890
              mmTop = 794
              mmWidth = 1852
              BandType = 4
              LayerName = Foreground2
            end
            object ppLabel2: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'Label2'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Sales Tax:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 7408
              mmTop = 794
              mmWidth = 15081
              BandType = 4
              LayerName = Foreground2
            end
            object ppLabel3: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'Label3'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Taxable Amount:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 119592
              mmTop = 794
              mmWidth = 24077
              BandType = 4
              LayerName = Foreground2
            end
            object ppLabel4: TppLabel
              DesignLayer = ppDesignLayer3
              UserName = 'Label4'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Rate:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 86519
              mmTop = 794
              mmWidth = 7673
              BandType = 4
              LayerName = Foreground2
            end
          end
          object ppDesignLayers3: TppDesignLayers
            object ppDesignLayer3: TppDesignLayer
              UserName = 'Foreground2'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object GrayKielySubReport: TppSubReport
        DesignLayer = ppDesignLayer6
        UserName = 'GrayKielySubReport'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = TotalsRegion
        TraverseAllData = False
        Visible = False
        DataPipelineName = 'GrayKielyPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 25135
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = GrayKielyPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Template.Format = ftASCII
          Version = '22.02'
          mmColumnWidth = 0
          DataPipelineName = 'GrayKielyPipe'
          object ppTitleBand1: TppTitleBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 10319
            mmPrintPosition = 0
            object ppLabel6: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label6'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 'Contractor'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3598
              mmLeft = 11642
              mmTop = 5292
              mmWidth = 24342
              BandType = 1
              LayerName = Foreground3
            end
            object ppLabel7: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label7'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = '# Tickets'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3598
              mmLeft = 79375
              mmTop = 5292
              mmWidth = 14023
              BandType = 1
              LayerName = Foreground3
            end
            object ppLabel8: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label8'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 'Total Amount'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3598
              mmLeft = 93927
              mmTop = 5292
              mmWidth = 23019
              BandType = 1
              LayerName = Foreground3
            end
            object ppLabel5: TppLabel
              DesignLayer = ppDesignLayer4
              UserName = 'Label5'
              HyperlinkEnabled = False
              AutoSize = False
              Border.mmPadding = 0
              Caption = 'Term Code'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsUnderline]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3704
              mmLeft = 61383
              mmTop = 5292
              mmWidth = 16404
              BandType = 1
              LayerName = Foreground3
            end
          end
          object ppDetailBand2: TppDetailBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 4763
            mmPrintPosition = 0
            object ppDBText9: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText9'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'billing_cc'
              DataPipeline = GrayKielyPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'GrayKielyPipe'
              mmHeight = 4763
              mmLeft = 61913
              mmTop = 0
              mmWidth = 12700
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText10: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText10'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'con_name'
              DataPipeline = GrayKielyPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'GrayKielyPipe'
              mmHeight = 4763
              mmLeft = 11377
              mmTop = 0
              mmWidth = 48683
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText11: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText11'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'Cnt'
              DataPipeline = GrayKielyPipe
              DisplayFormat = '#,##0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'GrayKielyPipe'
              mmHeight = 4763
              mmLeft = 79111
              mmTop = 0
              mmWidth = 14023
              BandType = 4
              LayerName = Foreground3
            end
            object ppDBText12: TppDBText
              DesignLayer = ppDesignLayer4
              UserName = 'DBText12'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'TotalPrice'
              DataPipeline = GrayKielyPipe
              DisplayFormat = '$ #,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'GrayKielyPipe'
              mmHeight = 4763
              mmLeft = 93663
              mmTop = 0
              mmWidth = 23019
              BandType = 4
              LayerName = Foreground3
            end
          end
          object ppDesignLayers4: TppDesignLayers
            object ppDesignLayer4: TppDesignLayer
              UserName = 'Foreground3'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
      object BreakdownSub: TppSubReport
        DesignLayer = ppDesignLayer6
        UserName = 'BreakdownSub'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = GrayKielySubReport
        TraverseAllData = False
        DataPipelineName = 'BreakdownPipe'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 30956
        mmWidth = 203200
        BandType = 7
        LayerName = Foreground5
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport4: TppChildReport
          AutoStop = False
          DataPipeline = BreakdownPipe
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.Duplex = dpNone
          PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 279401
          PrinterSetup.mmPaperWidth = 215900
          PrinterSetup.PaperSize = 1
          Template.Format = ftASCII
          Version = '22.02'
          mmColumnWidth = 0
          DataPipelineName = 'BreakdownPipe'
          object ppTitleBand2: TppTitleBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 9525
            mmPrintPosition = 0
            object ppLabel11: TppLabel
              DesignLayer = ppDesignLayer5
              UserName = 'Label11'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              Caption = 'Line Item Breakdown'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
              FormFieldSettings.FormFieldType = fftNone
              Transparent = True
              mmHeight = 3969
              mmLeft = 1323
              mmTop = 5292
              mmWidth = 31750
              BandType = 1
              LayerName = Foreground4
            end
            object ppLine3: TppLine
              DesignLayer = ppDesignLayer5
              UserName = 'Line3'
              Border.mmPadding = 0
              ParentWidth = True
              Weight = 0.750000000000000000
              mmHeight = 2381
              mmLeft = 0
              mmTop = 4498
              mmWidth = 203200
              BandType = 1
              LayerName = Foreground4
            end
          end
          object ppDetailBand5: TppDetailBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 6085
            mmPrintPosition = 0
            object ppDBText13: TppDBText
              DesignLayer = ppDesignLayer5
              UserName = 'DBText13'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'explanation'
              DataPipeline = BreakdownPipe
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'BreakdownPipe'
              mmHeight = 4233
              mmLeft = 11377
              mmTop = 1058
              mmWidth = 61913
              BandType = 4
              LayerName = Foreground4
            end
            object ppDBText37: TppDBText
              DesignLayer = ppDesignLayer5
              UserName = 'DBText37'
              HyperlinkEnabled = False
              Border.mmPadding = 0
              DataField = 'amount'
              DataPipeline = BreakdownPipe
              DisplayFormat = '#,##0.00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'BreakdownPipe'
              mmHeight = 4498
              mmLeft = 77258
              mmTop = 1058
              mmWidth = 23019
              BandType = 4
              LayerName = Foreground4
            end
          end
          object ppSummaryBand1: TppSummaryBand
            Border.mmPadding = 0
            mmBottomOffset = 0
            mmHeight = 3175
            mmPrintPosition = 0
            object ppLine4: TppLine
              DesignLayer = ppDesignLayer5
              UserName = 'Line4'
              Border.mmPadding = 0
              ParentWidth = True
              Weight = 0.750000000000000000
              mmHeight = 2646
              mmLeft = 0
              mmTop = 529
              mmWidth = 203200
              BandType = 7
              LayerName = Foreground4
            end
          end
          object ppDesignLayers5: TppDesignLayers
            object ppDesignLayer5: TppDesignLayer
              UserName = 'Foreground4'
              LayerType = ltBanded
              Index = 0
            end
          end
        end
      end
    end
    object ppPageStyle1: TppPageStyle
      BeforePrint = ppPageStyle1BeforePrint
      Border.mmPadding = 0
      EndPage = 0
      SinglePage = 0
      StartPage = 0
      mmBottomOffset = 0
      mmHeight = 266701
      mmPrintPosition = 0
      object ppWatermark: TppLabel
        DesignLayer = ppDesignLayer7
        UserName = 'Watermark'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'UNCOMMITTED'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Name = 'Arial'
        Font.Size = 27
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        mmHeight = 11113
        mmLeft = 65617
        mmTop = 39952
        mmWidth = 71702
        BandType = 10
        LayerName = PageLayer1
      end
      object ppLabel41: TppLabel
        DesignLayer = ppDesignLayer7
        UserName = 'Watermark1'
        HyperlinkEnabled = False
        Border.mmPadding = 0
        Caption = 'This is not an invoice'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = []
        FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
        FormFieldSettings.FormFieldType = fftNone
        mmHeight = 6615
        mmLeft = 74877
        mmTop = 51329
        mmWidth = 53181
        BandType = 10
        LayerName = PageLayer1
      end
    end
    object ppGroup1: TppGroup
      BreakName = 'subcategory'
      DataPipeline = InvoiceDetailsPipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'InvoiceDetailsPipe'
      NewFile = False
      object SubCategoryHeaderBand: TppGroupHeaderBand
        BeforePrint = SubCategoryHeaderBandBeforePrint
        Border.mmPadding = 0
        mmBottomOffset = 0
        mmHeight = 8731
        mmPrintPosition = 0
        object SubCategoryLabel: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'SubCategoryLabel'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'subcategory'
          DataPipeline = InvoiceDetailsPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 10
          Font.Style = [fsBold, fsUnderline]
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 4233
          mmLeft = 1852
          mmTop = 2910
          mmWidth = 54240
          BandType = 3
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLine6: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line6'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpRight
          Weight = 0.750000000000000000
          mmHeight = 8731
          mmLeft = 200555
          mmTop = 0
          mmWidth = 2910
          BandType = 3
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLine5: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line101'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpLeft
          Weight = 0.750000000000000000
          mmHeight = 8731
          mmLeft = 0
          mmTop = 0
          mmWidth = 2910
          BandType = 3
          GroupNo = 0
          LayerName = Foreground5
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Border.mmPadding = 0
        PrintHeight = phDynamic
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 8731
        mmPrintPosition = 0
        object ppDBCalc4: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc1'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Tickets'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = '#,'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3969
          mmLeft = 85196
          mmTop = 2117
          mmWidth = 17198
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppDBCalc5: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc2'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Units'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = '#,'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3969
          mmLeft = 105834
          mmTop = 2117
          mmWidth = 17198
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppDBCalc6: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc3'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Amount'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = ',#.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3969
          mmLeft = 177271
          mmTop = 2646
          mmWidth = 21438
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLine7: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line1'
          Border.mmPadding = 0
          Weight = 0.750000000000000000
          mmHeight = 1852
          mmLeft = 31485
          mmTop = 1058
          mmWidth = 171715
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLabel27: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label27'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = '$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3969
          mmLeft = 174890
          mmTop = 2646
          mmWidth = 1852
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLine15: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line102'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpLeft
          Weight = 0.750000000000000000
          mmHeight = 8731
          mmLeft = 0
          mmTop = 0
          mmWidth = 2910
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppLine16: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line16'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpRight
          Weight = 0.750000000000000000
          mmHeight = 8731
          mmLeft = 200290
          mmTop = 0
          mmWidth = 3175
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
        object ppDBCalc1: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc4'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Hours'
          DataPipeline = InvoiceDetailsPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ResetGroup = ppGroup1
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3810
          mmLeft = 128588
          mmTop = 2117
          mmWidth = 17198
          BandType = 5
          GroupNo = 0
          LayerName = Foreground5
        end
      end
    end
    object ppGroup2: TppGroup
      BreakName = 'line_item_text'
      DataPipeline = InvoiceDetailsPipe
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group2'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'InvoiceDetailsPipe'
      NewFile = False
      object ppGroupHeaderBand2: TppGroupHeaderBand
        Border.mmPadding = 0
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
      object ppGroupFooterBand2: TppGroupFooterBand
        Border.mmPadding = 0
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 8202
        mmPrintPosition = 0
        object ppLine25: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line25'
          Border.mmPadding = 0
          Weight = 0.750000000000000000
          mmHeight = 2910
          mmLeft = 2381
          mmTop = 0
          mmWidth = 200819
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppDBCalc2: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc5'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Tickets'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = '#,'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup2
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3970
          mmLeft = 84930
          mmTop = 1059
          mmWidth = 17198
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppDBCalc3: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc6'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Amount'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = ',#.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup2
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3969
          mmLeft = 177271
          mmTop = 794
          mmWidth = 21438
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppDBCalc7: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc7'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'UnitsCharged'
          DataPipeline = InvoiceDetailsPipe
          DisplayFormat = '#,'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ParentDataPipeline = False
          ResetGroup = ppGroup2
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3970
          mmLeft = 105570
          mmTop = 1059
          mmWidth = 17198
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppDBCalc8: TppDBCalc
          DesignLayer = ppDesignLayer6
          UserName = 'DBCalc8'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'Hours'
          DataPipeline = InvoiceDetailsPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          ResetGroup = ppGroup2
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3703
          mmLeft = 128324
          mmTop = 1059
          mmWidth = 17198
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppLabel14: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label14'
          HyperlinkEnabled = False
          Anchors = [atLeft, atBottom]
          Border.mmPadding = 0
          Caption = 'Subtotal for'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3810
          mmLeft = 2646
          mmTop = 1058
          mmWidth = 17865
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppLine24: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line103'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpLeft
          Weight = 0.750000000000000000
          mmHeight = 8202
          mmLeft = 0
          mmTop = 0
          mmWidth = 2911
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppDBText39: TppDBText
          DesignLayer = ppDesignLayer6
          UserName = 'DBText39'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          DataField = 'billing_cc'
          DataPipeline = InvoiceDetailsPipe
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          Transparent = True
          DataPipelineName = 'InvoiceDetailsPipe'
          mmHeight = 3704
          mmLeft = 21696
          mmTop = 1058
          mmWidth = 61913
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppLabel47: TppLabel
          DesignLayer = ppDesignLayer6
          UserName = 'Label47'
          HyperlinkEnabled = False
          Border.mmPadding = 0
          Caption = '$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 9
          Font.Style = [fsBold]
          FormFieldSettings.FormSubmitInfo.SubmitMethod = fstPost
          FormFieldSettings.FormFieldType = fftNone
          Transparent = True
          mmHeight = 3969
          mmLeft = 174890
          mmTop = 794
          mmWidth = 1852
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
        object ppLine26: TppLine
          DesignLayer = ppDesignLayer6
          UserName = 'Line26'
          Border.mmPadding = 0
          ParentHeight = True
          Position = lpRight
          Weight = 0.750000000000000000
          mmHeight = 8202
          mmLeft = 201084
          mmTop = 0
          mmWidth = 2381
          BandType = 5
          GroupNo = 1
          LayerName = Foreground5
        end
      end
    end
    object ppDesignLayers6: TppDesignLayers
      object ppDesignLayer7: TppDesignLayer
        UserName = 'PageLayer1'
        LayerType = ltPage
        Index = 0
      end
      object ppDesignLayer6: TppDesignLayer
        UserName = 'Foreground5'
        LayerType = ltBanded
        Index = 1
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object InvoiceHeaderPipe: TppDBPipeline
    DataSource = InvoiceHeaderDS
    OpenDataSource = False
    UserName = 'InvoiceHeaderPipe'
    Left = 36
    Top = 384
  end
  object InvoiceDetailsPipe: TppDBPipeline
    DataSource = InvoiceDetailsDS
    OpenDataSource = False
    UserName = 'InvoiceDetailsPipe'
    Left = 140
    Top = 388
    MasterDataPipelineName = 'InvoiceHeaderPipe'
  end
  object InvoiceHeaderDS: TDataSource
    DataSet = InvoiceHeader
    Left = 44
    Top = 332
  end
  object InvoiceDetailsDS: TDataSource
    DataSet = InvoiceDetails
    Left = 140
    Top = 332
  end
  object OutputConfig: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select boc.*, cu.customer_name'#13#10' from billing_output_config boc'#13 +
      #10' inner join customer cu on boc.customer_id=cu.customer_id'#13#10'wher' +
      'e boc.center_group_id=:cgid1'#13#10' and (cu.period_type=:pt)'#13#10#13#10'union' +
      #13#10#13#10'select boc.*, cu.customer_name'#13#10' from billing_output_config ' +
      'boc'#13#10' inner join customer cu on boc.customer_id=cu.customer_id'#13#10 +
      'where boc.center_group_id=:cgid2'#13#10' and boc.customer_id in ('#13#10'   ' +
      ' select distinct c.customer_id'#13#10'     from billing_detail bd'#13#10'   ' +
      '  inner join client c on bd.client_id=c.client_id'#13#10'     where bd' +
      '.bill_id=:bill_id'#13#10'      and c.customer_id is not null)'#13#10#13#10'order' +
      ' by cu.customer_name'#13#10
    CommandTimeout = 90
    Parameters = <
      item
        Name = 'cgid1'
        Size = -1
        Value = Null
      end
      item
        Name = 'pt'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'cgid2'
        Size = -1
        Value = Null
      end
      item
        Name = 'bill_id'
        Size = -1
        Value = Null
      end>
    Left = 112
    Top = 28
  end
  object BillingInvoice: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select invoice_id, invoice_date, revision,'#13#10' convert(varchar(20)' +
      ' , invoice_id ) + upper(COALESCE(revision, '#39#39') ) + dbo.GetConfig' +
      'Value('#39'InvoiceSuffix'#39','#39'-Q'#39') as FullInvoiceNumber'#13#10'from billing_i' +
      'nvoice '#13#10'where billing_header_id=:billing_header_id'#13#10' and custom' +
      'er_id=:customer_id'#13#10' and end_date=:end_date'#13#10' and which_invoice=' +
      ':which_invoice'#13#10
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'billing_header_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'end_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0d
      end
      item
        Name = 'which_invoice'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 264
    Top = 292
  end
  object BillingInvoiceDS: TDataSource
    DataSet = BillingInvoicePreview
    Left = 268
    Top = 340
  end
  object BillingInvoicePipe: TppDBPipeline
    DataSource = BillingInvoiceDS
    OpenDataSource = False
    UserName = 'BillingInvoicePipe'
    Left = 268
    Top = 388
    object BillingInvoicePipeppField1: TppField
      FieldAlias = 'invoice_id'
      FieldName = 'invoice_id'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object BillingInvoicePipeppField2: TppField
      FieldAlias = 'invoice_date'
      FieldName = 'invoice_date'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object BillingInvoicePipeppField3: TppField
      FieldAlias = 'revision'
      FieldName = 'revision'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object BillingInvoicePipeppField4: TppField
      FieldAlias = 'FullInvoiceNumber'
      FieldName = 'FullInvoiceNumber'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
  end
  object Designer: TppDesigner
    Caption = 'ReportBuilder'
    DataSettings.SessionType = 'BDESession'
    DataSettings.AllowEditSQL = False
    DataSettings.DatabaseType = dtParadox
    DataSettings.GuidCollationType = gcString
    DataSettings.IsCaseSensitive = True
    DataSettings.SQLType = sqBDELocal
    Position = poScreenCenter
    Report = InvoiceReport
    IniStorageType = 'IniFile'
    IniStorageName = '($WINSYS)\RBuilder.ini'
    WindowHeight = 400
    WindowLeft = 100
    WindowTop = 50
    WindowWidth = 600
    OnClose = DesignerClose
    OnHide = DesignerHide
    OnShow = DesignerShow
    Left = 120
    Top = 128
  end
  object TemplateSaveDlg: TSaveDialog
    DefaultExt = '*.rtm'
    Filter = 'Template files (*.rtm)|*.rtm|All files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 200
    Top = 128
  end
  object InvoiceSP: TADOStoredProc
    CursorType = ctStatic
    CommandTimeout = 600
    ProcedureName = 'RPT_invoice8;1'
    Parameters = <>
    Left = 272
    Top = 16
  end
  object InvoiceAdjustments: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select'#13#10#39'Test Adjustment'#39' as description,'#13#10'3.00 as AdjustmentAmo' +
      'unt,'#13#10#39'AMOUNT'#39' as type'#13#10#13#10'/*'#13#10'select description, sum(amount) as' +
      ' AdjustmentAmount, type'#13#10'from billing_adjustment '#13#10'where '#13#10'custo' +
      'mer_id=:customer_id'#13#10'and adjustment_date between :start_date and' +
      ' :end_date'#13#10'and type='#39'AMOUNT'#39#13#10'group by description, type'#13#10'*/'
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'customer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'start_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0d
      end
      item
        Name = 'end_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0d
      end>
    Left = 384
    Top = 292
  end
  object InvoiceAdjustmentsPipe: TppDBPipeline
    DataSource = InvoiceAdjustmentsDS
    OpenDataSource = False
    SkipWhenNoRecords = False
    UserName = 'InvoiceAdjustmentsPipe'
    Left = 388
    Top = 388
    object InvoiceAdjustmentsPipeppField1: TppField
      FieldAlias = 'description'
      FieldName = 'description'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object InvoiceAdjustmentsPipeppField2: TppField
      FieldAlias = 'AdjustmentAmount'
      FieldName = 'AdjustmentAmount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object InvoiceAdjustmentsPipeppField3: TppField
      FieldAlias = 'type'
      FieldName = 'type'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
  end
  object InvoiceAdjustmentsDS: TDataSource
    DataSet = InvoiceAdjustments
    Left = 388
    Top = 340
  end
  object InvoiceDetailsExport: TADODataSet
    CacheSize = 100
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select '#13#10' billing_cc, ticket_number, status,  qty_marked, bucket' +
      ', price,'#13#10' work_address_number, work_address_number_2, work_addr' +
      'ess_street,'#13#10' work_lat, work_long, area_name, transmit_date, tic' +
      'ket_type, due_date,'#13#10' initial_status_date, closed_date, work_cro' +
      'ss, rate, locate_id,'#13#10' bd.billing_detail_id, bd.bill_id, work_st' +
      'ate, work_county, work_city,'#13#10' con_name, work_done_for, work_typ' +
      'e, emp_number, short_name,'#13#10' call_date, transmit_count, hours, l' +
      'ine_item_text, revision, map_ref,'#13#10' locate_hours_id, work_descri' +
      'ption,'#13#10' bd.tax_name, bd.tax_rate, bd.tax_amount, qty_charged, r' +
      'aw_units,'#13#10' bd.high_profile, bd.plat, bd.units_marked, work_rema' +
      'rks '#13#10'from billing_detail bd'#13#10' inner join client cli on cli.clie' +
      'nt_id=bd.client_id'#13#10' inner join billing_header bh on bd.bill_id=' +
      'bh.bill_id'#13#10'where bh.bill_id = :bill_id'#13#10'and customer_id = :cust' +
      'omer_id '#13#10'and coalesce(bd.which_invoice, '#39#39')=coalesce(:which_inv' +
      'oice, '#39#39')'#13#10
    Parameters = <
      item
        Name = 'bill_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'which_invoice'
        DataType = ftString
        Size = 1
        Value = '_'
      end>
    Left = 284
    Top = 80
  end
  object BillingConfigDir: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select value from configuration_data'#13#10'where name='#39'BillingConfigR' +
      'ootDir'#39
    Parameters = <>
    Left = 472
    Top = 28
  end
  object CreateBOCRecords: TADOCommand
    CommandText = '/* sql set at runtime */'
    CommandTimeout = 120
    Parameters = <>
    Left = 192
    Top = 32
  end
  object BillRunInfo: TADODataSet
    CommandText = 
      'select bh.*,'#13#10'   cg.group_code'#13#10' from billing_header bh'#13#10'  left ' +
      'join center_group cg on bh.center_group_id = cg.center_group_id'#13 +
      #10' where bh.bill_id=:bid'#13#10
    Parameters = <
      item
        Name = 'bid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 40
    Top = 24
  end
  object NextInvoiceID: TADOStoredProc
    ProcedureName = 'NextUniqueID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Name'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = 'NextInvoiceID'
      end>
    Left = 361
    Top = 28
  end
  object RecBillingInvoice: TADODataSet
    CommandText = 
      'select * from billing_invoice '#13#10'where billing_header_id=:billing' +
      '_header_id'#13#10' and customer_id=:customer_id'#13#10' and end_date=:end_da' +
      'te'#13#10' and which_invoice = :which_invoice'
    Parameters = <
      item
        Name = 'billing_header_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'end_date'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0d
      end
      item
        Name = 'which_invoice'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 368
    Top = 80
  end
  object BillingInvoicePreview: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select'#13#10'GetDate() as invoice_date,'#13#10'1100 as invoice_id,'#13#10#39#39' as r' +
      'evision,'#13#10#39'1100'#39' as FullInvoiceNumber'#13#10
    CommandTimeout = 120
    Parameters = <>
    Left = 264
    Top = 244
    object IntegerField1: TIntegerField
      FieldName = 'invoice_id'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'invoice_date'
    end
    object StringField1: TStringField
      FieldName = 'revision'
      Size = 2
    end
    object StringField2: TStringField
      FieldKind = fkCalculated
      FieldName = 'FullInvoiceNumber'
      Calculated = True
    end
  end
  object Taxes: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select '#39'Some Tax Here'#39' as tax_name,'#13#10' 6.755 as tax_rate,'#13#10' 2333 ' +
      'as amount,'#13#10' 43.34 as tax_amount'#13#10
    Parameters = <>
    Left = 472
    Top = 296
  end
  object TaxesDS: TDataSource
    DataSet = Taxes
    Left = 472
    Top = 344
  end
  object TaxesPipe: TppDBPipeline
    DataSource = TaxesDS
    OpenDataSource = False
    UserName = 'TaxesPipe'
    Left = 472
    Top = 392
    object TaxesPipeppField1: TppField
      FieldAlias = 'tax_name'
      FieldName = 'tax_name'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object TaxesPipeppField2: TppField
      FieldAlias = 'tax_rate'
      FieldName = 'tax_rate'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object TaxesPipeppField3: TppField
      FieldAlias = 'amount'
      FieldName = 'amount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object TaxesPipeppField4: TppField
      FieldAlias = 'tax_amount'
      FieldName = 'tax_amount'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
  end
  object GrayKielyPipe: TppDBPipeline
    DataSource = GrayKielyDS
    OpenDataSource = False
    UserName = 'GrayKielyPipe'
    Left = 536
    Top = 400
    object GrayKielyPipeppField1: TppField
      FieldAlias = 'billing_cc'
      FieldName = 'billing_cc'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object GrayKielyPipeppField2: TppField
      FieldAlias = 'con_name'
      FieldName = 'con_name'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object GrayKielyPipeppField3: TppField
      FieldAlias = 'Cnt'
      FieldName = 'Cnt'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object GrayKielyPipeppField4: TppField
      FieldAlias = 'TotalPrice'
      FieldName = 'TotalPrice'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
  end
  object GrayKielyDS: TDataSource
    DataSet = GrayKiely
    Left = 536
    Top = 352
  end
  object GrayKiely: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select '#39'AAA'#39' as billing_cc,'#13#10' '#39'BBB'#39' as con_name,'#13#10' 100 as Cnt,'#13#10 +
      ' 154.23 as TotalPrice'#13#10
    Parameters = <>
    Left = 536
    Top = 296
  end
  object StatusSummary: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select'#13#10#39'ABC'#39' as billing_cc,'#13#10#39'M'#39' as status,'#13#10'100 as Tickets,'#13#10'1' +
      '10 as Units, '#13#10'34.3 as Hours,'#13#10'345.34 as Amount'
    Parameters = <>
    Left = 608
    Top = 296
  end
  object StatusSummaryDS: TDataSource
    DataSet = StatusSummary
    Left = 608
    Top = 352
  end
  object StatusSummaryPipe: TppDBPipeline
    DataSource = StatusSummaryDS
    OpenDataSource = False
    UserName = 'StatusSummaryPipe'
    Left = 608
    Top = 400
  end
  object Breakdown: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select '#39'Billing breakdown, detail'#39' as explanation, .5 as ratio, ' +
      '245.45 as amount, 67.89 as flat_fee_amount'
    Parameters = <>
    Left = 704
    Top = 296
  end
  object BreakdownDS: TDataSource
    DataSet = Breakdown
    Left = 704
    Top = 352
  end
  object BreakdownPipe: TppDBPipeline
    DataSource = BreakdownDS
    OpenDataSource = False
    UserName = 'BreakdownPipe'
    Left = 704
    Top = 400
  end
  object TransDetail: TADODataSet
    Parameters = <>
    Left = 552
    Top = 32
  end
  object TicketNotesData: TADODataSet
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select t.ticket_number, t.ticket_id, n.notes_id, n.entry_date, n' +
      '.note'#13#10'from '#13#10'     (select ticket_id'#13#10'      from billing_detail ' +
      'bd'#13#10'       inner join locate l on bd.locate_id=l.locate_id'#13#10'    ' +
      '  where bill_id=:bill_id'#13#10'      group by ticket_id) tx'#13#10'  inner ' +
      'join ticket_notes n on tx.ticket_id = n.ticket_id'#13#10'  inner join ' +
      'ticket t on t.ticket_id = tx.ticket_id'#13#10
    CommandTimeout = 180
    Parameters = <
      item
        Name = 'bill_id'
        Size = -1
        Value = Null
      end>
    Left = 664
    Top = 96
  end
  object SolomonSplit: TADODataSet
    Parameters = <>
    Left = 696
    Top = 168
  end
  object XignExport: TADODataSet
    CursorType = ctStatic
    CommandText = 'RPT_xign_export;1'
    CommandType = cmdStoredProc
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OutputConfigID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CustomerName'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 696
    Top = 216
  end
end
