inherited BillingCCMapForm: TBillingCCMapForm
  Caption = 'Billing CC Map'
  ClientHeight = 461
  ClientWidth = 902
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 902
    object chkboxBillingccSearch: TCheckBox
      Left = 23
      Top = 15
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxBillingccSearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 902
    Height = 420
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      object ColCol_Center: TcxGridDBColumn
        Caption = 'call center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'cleint code'
        DataBinding.FieldName = 'client_code'
        DataBinding.IsNullValueType = True
      end
      object ColWorkState: TcxGridDBColumn
        Caption = 'work state'
        DataBinding.FieldName = 'work_state'
        DataBinding.IsNullValueType = True
        Width = 62
      end
      object ColWorkCounty: TcxGridDBColumn
        Caption = 'work county'
        DataBinding.FieldName = 'work_county'
        DataBinding.IsNullValueType = True
        Width = 170
      end
      object ColWorkCity: TcxGridDBColumn
        Caption = 'work city'
        DataBinding.FieldName = 'work_city'
        DataBinding.IsNullValueType = True
        Width = 252
      end
      object ColBillingCC: TcxGridDBColumn
        Caption = 'billing cc'
        DataBinding.FieldName = 'billing_cc'
        DataBinding.IsNullValueType = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from billing_cc_map'
    Left = 136
    Top = 120
  end
  inherited DS: TDataSource
    Left = 200
    Top = 120
  end
end
