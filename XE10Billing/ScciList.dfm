inherited ScciForm: TScciForm
  Left = 413
  Top = 145
  Caption = 'Billing - County City Groupings'
  ClientWidth = 750
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 750
    Height = 25
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 507
      Height = 13
      Caption = 
        'Note: "*" wildcards are not currently supported but you can leav' +
        'e the Customer field blank to apply to all.'
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 750
    Height = 374
    inherited GridView: TcxGridDBTableView
      DataController.KeyFieldNames = 'scci_id'
      OptionsView.GroupByBox = True
      object ColCustomerLookup: TcxGridDBColumn
        Caption = 'Customer'
        DataBinding.FieldName = 'CustomerLookup'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 20
        Properties.DropDownWidth = 250
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.ListColumns = <>
        Properties.ListOptions.ShowHeader = False
        Options.Filtering = False
        Width = 287
      end
      object ColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 41
      end
      object ColCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 96
      end
      object ColCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 85
      end
      object ColWhichInvoice: TcxGridDBColumn
        Caption = 'Which Invoice'
        DataBinding.FieldName = 'which_invoice'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 57
      end
      object ColSubcategory: TcxGridDBColumn
        Caption = 'Grouping'
        DataBinding.FieldName = 'subcategory'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 117
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 57
      end
      object ColSCCIId: TcxGridDBColumn
        Caption = 'SCCI ID'
        DataBinding.FieldName = 'scci_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 63
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    OnNewRecord = DataNewRecord
    CommandText = 'select * from billing_scc_invoice order by 2,3,4'
  end
  object Customer: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select customer_id,'#13#10' customer_name + '#39' '#39' +'#13#10' coalesce(customer_' +
      'number, '#39#39') + '#39' '#39'  +'#13#10' coalesce(city, '#39#39') + '#39' '#39'  +'#13#10' coalesce(st' +
      'ate, '#39#39') as cname from customer'#13#10'order by 2'#13#10#13#10
    Parameters = <>
    Left = 56
    Top = 152
  end
  object CustomerDS: TDataSource
    DataSet = Customer
    Left = 96
    Top = 152
  end
end
