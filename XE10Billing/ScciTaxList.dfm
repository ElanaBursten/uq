inherited ScciTaxForm: TScciTaxForm
  Left = 408
  Top = 353
  Caption = 'Billing - County City Taxes'
  ClientWidth = 626
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 626
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 582
      Height = 13
      Caption = 
        'Note: Make sure the tax rate matches for each tax name.  They wi' +
        'll be combined in to one invoice line item per tax name.'
    end
  end
  inherited Grid: TcxGrid
    Width = 626
    inherited GridView: TcxGridDBTableView
      Navigator.Visible = True
      DataController.KeyFieldNames = 'scci_id'
      object ColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 49
      end
      object ColCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 97
      end
      object ColCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 103
      end
      object ColTaxName: TcxGridDBColumn
        Caption = 'Tax Name'
        DataBinding.FieldName = 'tax_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Filtering = False
        Width = 138
      end
      object ColTaxRate: TcxGridDBColumn
        Caption = 'Tax Rate'
        DataBinding.FieldName = 'tax_rate'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '0.0000;-0.0000'
        Properties.Nullable = False
        HeaderAlignmentHorz = taRightJustify
        Options.Filtering = False
        Width = 59
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 58
      end
      object ColSCCIId: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'scci_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 48
      end
    end
  end
  inherited Data: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from billing_scc_tax order by 2,3,4'
  end
end
