unit BillingCalc;

interface

uses
  SysUtils, Classes, ADODB, DB, BetterADODataSet, BillingRates, BillingShared,
  BillingTicketClientList, DateUtils, IncrementalBilling, BillingConfiguration;

type
  TBillingCalcDM = class(TDataModule)
    Queue: TBetterADODataSet;
    DeleteFromQueue: TADOCommand;
    InternalTicket: TBetterADODataSet;
    InternalTicketVersions: TBetterADODataSet;
    InternalLocates: TBetterADODataSet;
    TransRec: TBetterADODataSet;
    MarkFailed: TADOCommand;
    ReviveFailed: TADOCommand;
  private
    FQueueCount: Integer;
    FOnLog: TBillingLogEvent;
    FTicketID: Integer;
    BillingConfig: TBillingConfig;

    procedure AddTransactionRecord(Transaction: TBillingTransaction);
    function CalculateBilling(QueueRecord: TDataSet): TBillingTransaction;
    procedure GoToTicketAndLocates(TicketID: Integer);
    procedure ProcessQueue(Number: Integer);
    procedure Initialize;
    procedure Log(Msg: string);
    procedure CommitItem(QueueID: Integer; Transaction: TBillingTransaction);
  public
    property QueueCount: Integer read FQueueCount write FQueueCount;
    property OnLog: TBillingLogEvent read FOnLog write FOnLog;
    procedure Execute;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses QMBillingDMu, OdDbUtils, OdMiscUtils, BillingLocate, BillingConst, QMConst,
  Windows;

const
  MutexName = 'QManagerRealtimeBillingMutex';

var
  Mutex: HWND;

{$R *.dfm}

procedure TBillingCalcDM.AddTransactionRecord(Transaction: TBillingTransaction);
begin
  TransRec.Open;
  try
    TransRec.Insert;
    TransRec.FieldByName('ticket_id').AsInteger := Transaction.TicketID;
    TransRec.FieldByName('locate_id').Value := Transaction.LocateID;
    TransRec.FieldByName('locate_status_id').Value := Transaction.LocateStatusID;
    TransRec.FieldByName('call_center').AsString := Transaction.CallCenter;
    TransRec.FieldByName('event_date').AsDateTime := Transaction.EventDate;
    TransRec.FieldByName('effective_date').AsDateTime := Transaction.EffectiveDate;
    TransRec.FieldByName('amount').AsFloat := Transaction.Amount;
    TransRec.FieldByName('description').AsString := Transaction.Description;
    TransRec.Post;
  finally
    TransRec.Close;
  end;
end;

function TBillingCalcDM.CalculateBilling(QueueRecord: TDataSet): TBillingTransaction;
begin
  GoToTicketAndLocates(Result.TicketID);
  Assert(not InternalTicket.Eof);

  Result := IncrementalRate(QueueRecord, InternalTicket, InternalTicketVersions, InternalLocates, BillingConfig, FOnLog);
end;

procedure TBillingCalcDM.CommitItem(QueueID: Integer; Transaction: TBillingTransaction);
begin
  QMBillingDM.Conn.BeginTrans;
  try
    AddTransactionRecord(Transaction);

    DeleteFromQueue.Parameters.ParamValues['queue_id'] := QueueID;
    DeleteFromQueue.Execute;

    QMBillingDM.Conn.CommitTrans;
  except
    QMBillingDM.Conn.RollbackTrans;
    raise;
  end;
end;

constructor TBillingCalcDM.Create(AOwner: TComponent);
begin
  inherited;
  BillingConfig := TBillingConfig.Create(nil);
end;

procedure TBillingCalcDM.Execute;
begin
  Mutex := CreateMutex(nil, False, MutexName);
  try
    if (GetLastError = ERROR_ALREADY_EXISTS) or (Mutex = 0) then
      Log('Error: Realtime billing is already running')
    else
      ProcessQueue(FQueueCount);
  finally
    CloseHandle(Mutex);
  end;
end;

procedure TBillingCalcDM.GoToTicketAndLocates(TicketID: Integer);
begin
  //TODO: Use values at time of event, not current values (join to locate_status, etc.)
  if TicketID = FTicketID then
    Exit;
  InternalTicket.Close;
  InternalLocates.Close;
  InternalTicket.Parameters.ParamValues['ticket_id'] := TicketID;
  InternalLocates.Parameters.ParamValues['ticket_id'] := TicketID;
  InternalTicket.Open;
  InternalLocates.Open;
  Assert((not InternalTicket.Eof) and (not InternalLocates.Eof));
  FTicketID := TicketID;
end;

procedure TBillingCalcDM.Initialize;
begin
  if not Assigned(BillingConfig) then
    BillingConfig := TBillingConfig.CreateForAllCenters(QMBillingDM.Conn);
end;

procedure TBillingCalcDM.Log(Msg: string);
begin
  if Assigned(FOnLog) then
    FOnLog(Msg);
end;

procedure TBillingCalcDM.ProcessQueue(Number: Integer);

  procedure MarkQueueEntryFailed(QueueId: Integer);
  begin
    MarkFailed.Parameters.ParamValues['queue_id'] := QueueID;
    MarkFailed.Execute;
  end;

  // TODO: We'll need something more sophisticated here to stop queue clogging
  procedure ReviveFailedQueue;
  const
    SQL = 'update billing_queue set failed_processing=0 ' +
          'where failed_processing=1 and queue_id in ' +
          ' (select top %d queue_id from billing_queue ' +
          ' where failed_processing=1 order by insert_date )';
  begin
    // Marks top 25% (number) of failed transactions as not failed
    ReviveFailed.CommandText := Format(SQL, [Number div 4]);
    ReviveFailed.Execute;
  end;

const
  Query = 'select top %d * from billing_queue where failed_processing=0 order by insert_date';

var
  QueueID: Integer;
  LocateID: Integer;
  Transaction: TBillingTransaction;
  StopTime: TDateTime;
  StartTime: TDateTime;
  Total: Int64;
begin
  StartTime := Now;

  Initialize;

  ReviveFailedQueue;
  Queue.CommandText := Format(Query, [Number]);
  Queue.Open;

  try
    while not Queue.Eof do begin
      QueueID := Queue.FieldByName('queue_id').AsInteger;
      Assert(QueueID > 0);
      try
        Transaction := CalculateBilling(Queue);

        CommitItem(QueueID, Transaction);
      except
        on E: Exception do begin
          MarkQueueEntryFailed(QueueID);
          LocateID := Queue.FieldByName('locate_id').AsInteger;
          Log(Format('  Failed  QueueID: %d  LocateID: %d  Error: %s', [QueueID, LocateID, E.Message]));
        end;
      end;

      Queue.Next;
    end;
  finally
    Queue.Close;
  end;

  StopTime := Now;
  Total := SecondsBetween(StopTime, StartTime);
  Log(Format('StartTime : %s  StopTime: %s Total: %s Billing Rate : %.2f/min', [TimeToStr(StartTime), TimeToStr(StopTime), SecondsToHMS(Total), (Number / (Total / 60))]));
end;

end.

