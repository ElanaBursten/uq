unit CommandLineBilling;

interface

uses
  Classes, SysUtils, Dialogs;

procedure ConsoleHackBillingDispatch;
procedure CommandLineBillingDispatch;

implementation

uses
  QMBillingDMu, BillingEngine, OdIsoDates, DateUtils, OdMiscUtils,
  BillingCalc, Windows, Variants, CLBLogger, OdInternetUtil, ActiveX,
  BillPeriod;

var
  BE: TBillingEngine;
  DateEnd: TDateTime;
  Span: string;
  Logger: TCLBLogger;
  DirectoryParam: string;
  CallCenterGroupParam: string;
  LogOutput: string;
  RequestType: string;
  ReexportBillID: Integer;
  QueuedByEmpId: Integer;

function CommandLineBillingMode: Boolean;
begin
  Result := SameText(ParamStr(1), 'runbilling');
end;

function RealTimeBillingMode: Boolean;
begin
  Result := SameText(ParamStr(1), 'realtime');
end;

function ProcessQueueMode: Boolean;
begin
  Result := SameText(ParamStr(1), 'processqueue');
end;

function AddToQueueMode: Boolean;
begin
  Result := SameText(ParamStr(1), 'addtoqueue');
end;

//  *************** Worker Code ********************

procedure Log(Msg: string);
begin
  if Assigned(Logger) then
    Logger.Log(nil, Msg);
end;

procedure LoginWithIniSettings;
begin
  QMBillingDM.ConnectFromIni;;
end;

function RunBilling: Integer;
var
  Period: TBillPeriod;
begin
  Result := -1;
  Log('Billing Period Type: ' + Span);
  Log('Billing Period Ending Date: ' + IsoDateToStr(DateEnd));

  if CallCenterGroupParam = '' then
    raise Exception.Create('You must specify a call center group');
  Log('Call Centers: ' + CallCenterGroupParam);
  Log('Running from directory: ' + GetCurrentDir);

  // Only export and save if a directory was specified!
  if DirectoryParam <> '' then begin
    Log('Using base directory: ' + DirectoryParam);
    OdForceDirectories(DirectoryParam);
    SetCurrentDir(DirectoryParam);
    BE.NoExportOrSave := False;
  end else begin
    Log('No base directory specified, using no-export no-save mode.');
    BE.NoExportOrSave := True;
  end;

  BE.SetBillingTimeout(QMBillingDM.BillingTimeout);
  BE.ExportRawDataOnly := False;
  BE.NoOldRunCleanup := False;

  BE.SetConnectionLike(QMBillingDM.Conn);
  BE.Logger := Logger.Log;

  BE.SetCallCenterGroup(CallCenterGroupParam);
  Period := TBillPeriod.Create(Now);
  try
    if Span = PERIOD_FISCALMONTH then
      Period.ReadFiscalPeriodsFrom(QMBillingDM.ConfigDM.DycomPeriods);
    Period.Span := Span;
    Period.DisplayEndDate := DateEnd - 1; // The client uses the exclusive end date when adding to the queue
    BE.Period := Period;

    if RequestType = 'X' then begin
      if ReexportBillID < 1 then
        raise Exception.Create('No Bill ID specified for re-export');
      Logger.Log('Re-export of existing bill #' + IntToStr(ReexportBillID));
      BE.ExportResults(ReexportBillID);
      Result := ReexportBillID;
    end
    else
      Result := BE.RunDateRangeBilling(QMBillingDM.ConfigDM.GetCenterGroupString(CallCenterGroupParam));
  finally
    Period.Free;
  end;

  if BE.ErrorCount > 0 then
    Logger.Log(nil, Format('***** This billing run encountered %d %s. *****',
      [BE.ErrorCount, AddSIfNot1(BE.ErrorCount, 'error')]));
end;

function RunBillingWithLog: Integer;
var
  Subject: string;
begin
  Result := -1;
  try
    try
      Log('Starting');
      Result := RunBilling;
      Log('Done.');
    except
      on E: Exception do begin
        Log('ERROR: ' + E.Message);
        raise;
      end;
    end;
  finally
    Subject := 'Billing Error Notification ';
    if Assigned(BE) and (Trim(BE.DestinationDir) <> '') then begin
      Logger.Directory := AddSlash(BE.DestinationDir);
      Subject := Subject + 'for ' + BE.BillingRunName;
    end else
      Logger.Directory := AddSlash(DirectoryParam);
    LogOutput := Logger.LogList.Text;
  end;
  if Pos('ERROR', LogOutput) > 0 then
    SendEmailWithIniSettings(QMBillingDM.IniFileName, 'ErrorEmail',
    '', '', Subject, LogOutput); //QM-137 BCC placeholder
end;

procedure DoLog(const Msg: string);
begin
  Writeln(Msg);
end;

procedure AddGroupsToQueue;
var
  CenterGroupsAdded: Integer;
begin
  Log('Billing Period Type: ' + Span);
  Log('Billing Period Ending Date: ' + IsoDateToStr(DateEnd));

  // We count locates closed until to the end of the selected day
  DateEnd := DateEnd + 1;

  if CallCenterGroupParam = '' then
    raise Exception.Create('You must specify a call center group');
  Log('Requested Call Centers: ' + CallCenterGroupParam);

  if QueuedByEmpID < 1 then
    raise Exception.Create('You must specify a queued by emp id');
  Log('Request Queued By Emp ID: ' + IntToStr(QueuedByEmpID));

  Assert(Assigned(QMBillingDM));
  CenterGroupsAdded := QMBillingDM.AddGroupsToBillingQueue(CallCenterGroupParam,
    Span, DateEnd, QueuedByEmpID);
  Log(IntToStr(CenterGroupsAdded) + ' request(s) added to the billing queue');
end;

procedure AddGroupsToQueueWithLog;
begin
  try
    try
      Log('Starting');
      AddGroupsToQueue;
      Log('Done.');
    except
      on E: Exception do begin
        Log('ERROR: ' + E.Message);
        raise;
      end;
    end;
  finally
    Logger.FileName := 'QueuedBillingLog-' + FormatDateTime('yyyy-mm-dd-hh-nn-ss', Now) + '.txt';
    LogOutput := Logger.LogList.Text;
  end;
end;

// **************** Entry Points ********************

procedure RunCommandLineBilling;
begin
  try
    if ParamCount < 4 then
      raise Exception.Create('Not enough parameters for runbilling.  ' +
        'Expected: runbilling WEEK|MONTH|FISCMON|HALF|DAY EndDate CenterGroupName [destdir]');

    Span := ParamStr(2);
    DateEnd := IsoStrToDate(ParamStr(3));
    CallCenterGroupParam := ParamStr(4);
    if ParamCount >= 5 then
      DirectoryParam := ExpandFileName(AddSlash(ParamStr(5)));
  except
    on E: Exception do begin
      Log('ERROR: ' + E.Message);
      Exit;
    end;
  end;

  try
    QMBillingDM := TQMBillingDM.Create(nil);
    BE := TBillingEngine.Create;
    try
      BE.OutputBaseDir := DirectoryParam;
      LoginWithIniSettings;
      RunBillingWithLog;
    finally
      FreeAndNil(BE);
      FreeAndNil(QMBillingDM);
    end;
  except
    on E: Exception do begin
      Log('ERROR: ' + E.Message);
    end;
  end;
end;

procedure RunRealTimeBilling;
var
  BillingQueue: TBillingCalcDM;
begin
  Logger.FileName := 'RealTimeLog-' + FormatDateTime('yyyy-mm-dd-hh-nn-ss', Now) + '.txt';

  try
    BillingQueue := nil;
    QMBillingDM := TQMBillingDM.Create(nil);
    try
      LoginWithIniSettings;
      BillingQueue := TBillingCalcDM.Create(nil);
      BillingQueue.QueueCount := StrToIntDef(ParamStr(2), 100);
      BillingQueue.OnLog := Logger.Log;
      BillingQueue.Execute;
    finally
      FreeAndNil(QMBillingDM);
      FreeAndNil(BillingQueue);
    end;
  except
    on E: Exception do begin
      Logger.Log('ERROR: ' + E.Message);
    end;
  end;
end;

procedure ProcessBillingQueue;
var
  RunID: Integer;
  BillID: Variant;
begin
  DoLog('Processing billing queue...');
  CreateMutex(nil, False, 'QManager.ProcessBillingQueue');
  if GetLastError = ERROR_ALREADY_EXISTS then begin
    DoLog('Error: QManagerBilling.exe is already processing the billing queue on this machine.');
    Exit;
  end;

  BillID := Null;
  try
    BE := nil;
    DoLog('Initializing application...');
    QMBillingDM := TQMBillingDM.Create(nil);
    try
      DoLog('Attempting login...');
      LoginWithIniSettings;
      DoLog('Creating billing engine...');
      BE := TBillingEngine.Create;
      DoLog('Setting up connection...');
      BE.SetConnectionLike(QMBillingDM.Conn);

      DoLog('Checking billing queue...');
      if QMBillingDM.QueuedBillingAvailable(DateEnd, Span, CallCenterGroupParam,
        DirectoryParam, RequestType, RunID, ReexportBillID) then begin
        DoLog('Billing queue has an item in it for ' + CallCenterGroupParam);
        QMBillingDM.MarkStarted(RunID);
        try
          DoLog('Running billing');
          BillID := RunBillingWithLog;
          DoLog('Billing completed');
        finally
          DoLog('Marking done');
          QMBillingDM.MarkDone(RunID, BillID, LogOutput);
          DoLog('Marked done');
          if QMBillingDM.GetIniSetting('Billing', 'SignalDone', '0') = '1' then begin
            MessageBeep(MB_OK);
            Sleep(10000);
          end;
        end;
      end else
        DoLog('The billing queue is empty');
    finally
      FreeAndNil(BE);
      FreeAndNil(QMBillingDM);
    end;
    DoLog('Queue processing completed');
  except
    on E: Exception do begin
      DoLog('ERROR: ' + E.Message);
    end;
  end;
end;

procedure AddToBillingQueue;
begin
  try
    if ParamCount < 5 then
      raise Exception.Create('Not enough parameters for addtoqueue.  ' +
        'Expected: addtoqueue WEEK|WEEK_A|WEEK_B|WEEK_C|WEEK_D|MONTH|MONTH_A|MONTH_B|HALF|DAY EndDate CenterGroupName QueuedByEmpId'); //qm-990

    Span := ParamStr(2);
    DateEnd := IsoStrToDate(ParamStr(3));
    CallCenterGroupParam := ParamStr(4);
    QueuedByEmpID := StrToIntDef(ParamStr(5), -1);

    QMBillingDM := TQMBillingDM.Create(nil);
    try
      LoginWithIniSettings;
      AddGroupsToQueueWithLog;
    finally
      FreeAndNil(QMBillingDM);
    end;
  except
    on E: Exception do begin
      DoLog('ERROR: ' + E.Message);
    end;
  end;
end;

procedure CommandLineBillingDispatch;
begin
  Logger := nil;
  try
    CoInitialize(nil);
    Logger := TCLBLogger.Create;
    // These are just defaults, and can be changed later by the Run* methods
    Logger.FileName := 'BillingLog-' + FormatDateTime('yyyy-mm-dd-hh-nn-ss', Now) + '.txt';
    Logger.Directory := GetCurrentDir;

    if CommandLineBillingMode then begin
      RunCommandLineBilling;
    end else if RealTimeBillingMode then begin
      RunRealTimeBilling;
    end else if ProcessQueueMode then begin
      ProcessBillingQueue;
    end else if AddToQueueMode then begin
      AddToBillingQueue;
    end else
      Logger.Log('Command line options: runbilling|realtime|processqueue|addtoqueue');

  finally
    if Assigned(Logger) then
      Logger.SaveToFile;
    FreeAndNil(Logger);
  end;
end;

procedure ConsoleHackBillingDispatch;
var
  ShouldFreeConsole: Boolean;
begin
  ConnectToConsole(ShouldFreeConsole);
  try
    CommandLineBillingDispatch;
    Sleep(1500);
  finally
    if ShouldFreeConsole then
      FreeConsole;
  end;
end;

end.

