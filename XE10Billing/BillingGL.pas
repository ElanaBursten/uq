unit BillingGL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit, cxMaskEdit,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxStyles,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, dxSkinsCore, dxSkinBasic, dxCore,
  dxSkinsForm, dxDateRanges, dxScrollbarAnnotations;

type
  TBillingGLForm = class(TBaseCxListForm)
    ColID: TcxGridDBColumn;
    ColGLCode: TcxGridDBColumn;
    ColDescription: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColPeriodType: TcxGridDBColumn;
    ColChargeType: TcxGridDBColumn;
    dxSkinController1: TdxSkinController;
  public
    procedure PopulateDropdowns; override;
  end;

implementation

uses BillPeriod, BillingConst, OdCxUtils, QMBillingDMu;


{$R *.dfm}

{ TBillingGLForm }

procedure TBillingGLForm.PopulateDropdowns;
begin
  inherited;
  CxComboBoxItems(ColPeriodType).Text := TBillPeriod.GetBillPeriodList;
  CxComboBoxItems(ColPeriodType).Insert(0, '*');
  CxComboBoxItems(ColChargeType).Text := BillingConst.BillingChargeTypes;
  CxComboBoxItems(ColChargeType).Insert(0, '*');
end;

end.
