object BillingCommitDM: TBillingCommitDM
  OldCreateOrder = False
  Height = 238
  Width = 440
  object CommitBilling: TADOStoredProc
    Connection = QMBillingDM.Conn
    CommandTimeout = 240
    ProcedureName = 'commit_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CommittedBy'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 40
  end
  object UncommitBilling: TADOStoredProc
    Connection = QMBillingDM.Conn
    CommandTimeout = 240
    ProcedureName = 'uncommit_billing;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BillID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 184
    Top = 48
  end
end
