inherited CallCenterGroupForm: TCallCenterGroupForm
  Caption = 'Call Center Groups'
  ClientHeight = 500
  ClientWidth = 707
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 489
    Height = 463
  end
  inherited HeaderPanel: TPanel
    Width = 707
    BevelOuter = bvNone
  end
  inherited MasterPanel: TPanel
    Width = 489
    Height = 463
    BevelOuter = bvNone
    object NoteLabel: TLabel [0]
      Left = 180
      Top = 11
      Width = 201
      Height = 33
      AutoSize = False
      Caption = 
        'The billing checkbox causes the group to show up to select cente' +
        'rs for a billing run'
      WordWrap = True
    end
    inherited MasterGrid: TcxGrid
      Top = 43
      Width = 473
      Height = 412
      inherited MasterGridView: TcxGridDBTableView
        DataController.KeyFieldNames = 'center_group_id'
        OptionsData.Appending = True
        OptionsData.Deleting = False
        OptionsView.GroupByBox = False
        object ColMasterCenterGroupId: TcxGridDBColumn
          DataBinding.FieldName = 'center_group_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Visible = False
          Options.Filtering = False
          Width = 111
        end
        object ColMasterCenterGroupCode: TcxGridDBColumn
          Caption = 'Center Group Code'
          DataBinding.FieldName = 'group_code'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 112
        end
        object ColMasterShowForBilling: TcxGridDBColumn
          Caption = 'Billing'
          DataBinding.FieldName = 'show_for_billing'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taLeftJustify
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = 'True'
          Properties.ValueGrayed = ''
          Properties.ValueUnchecked = 'False'
          MinWidth = 16
          Width = 36
        end
        object ColMasterComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Options.Filtering = False
          Width = 229
        end
        object ColMasterActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taLeftJustify
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = 'True'
          Properties.ValueGrayed = ''
          Properties.ValueUnchecked = 'False'
          MinWidth = 16
          Width = 57
        end
      end
    end
  end
  inherited DetailPanel: TPanel
    Left = 498
    Width = 209
    Height = 463
    BevelOuter = bvNone
    inherited DetailGrid: TcxGrid
      Top = 43
      Width = 189
      Height = 412
      inherited DetailGridView: TcxGridDBTableView
        DataController.KeyFieldNames = 'center_group_id;call_center'
        OptionsView.GroupByBox = False
        object ColDetailCenterGroupID: TcxGridDBColumn
          Caption = 'Center Group ID'
          DataBinding.FieldName = 'center_group_id'
          DataBinding.IsNullValueType = True
          Visible = False
          Options.Filtering = False
        end
        object ColDetailCallCenter: TcxGridDBColumn
          Caption = 'Call Center'
          DataBinding.FieldName = 'call_center'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownRows = 18
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 154
        end
        object ColDetailActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          Width = 57
        end
      end
    end
  end
  inherited Master: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from center_group'#13#10'order by group_code'
  end
  inherited Detail: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from center_group_detail'#13#10'order by call_center'
    IndexFieldNames = 'center_group_id'
    MasterFields = 'center_group_id'
    Top = 112
  end
  inherited DetailSource: TDataSource
    Top = 112
  end
end
