unit BillingDMu;

interface

uses
  SysUtils, Classes, OdPerfLog;

type
  TBillingDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  public
    Logger: TOdDBPerfLog;
  end;

function BillingDM: TBillingDM;
function PerfLog: TOdDbPerfLog;

implementation

var
  PrivateBillingDM: TBillingDM;

function BillingDM: TBillingDM;
begin
  if not Assigned(PrivateBillingDM) then
    PrivateBillingDM := TBillingDM.Create(nil);
  Result := PrivateBillingDM;
end;

function PerfLog: TOdDBPerfLog;
begin
  Result := BillingDM.Logger;
end;

{$R *.dfm}

procedure TBillingDM.DataModuleCreate(Sender: TObject);
begin
  Logger := TOdDbPerfLog.Create;
end;

procedure TBillingDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(Logger);
end;

initialization
finalization
  FreeAndNil(PrivateBillingDM);
end.
