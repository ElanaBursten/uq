unit BillingTicketClientList;

interface

uses
  SysUtils, Classes, DB, DBISAMTb, BillingConst, OdMiscUtils;

type
  TTicketClientList = class(TDataModule)
    Tickets: TDBISAMTable;
    TicketsTicketID: TIntegerField;
    TicketsClientCode: TStringField;
    TicketsStatus: TStringField;
    TicketsLocatorID: TIntegerField;
    TicketsRefID: TIntegerField;  //QMANTWO-710
    LocatorIDQuery: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FCheckForDuplicates: Boolean;
    function InternalCountLocates(TicketID: Integer; ClientID: Integer = -1;
      const ClientCode: string = ''; const CallCenter: string = ''): Integer;
    procedure IndexTicketsIfNecessary;
    function GetLocatorForLocate(TicketID: Integer; const ClientCode: string): Integer;
    procedure AdjustNewJersey5PartyCount(const TicketID: Integer; var PartyCount: Integer);
    procedure GetNIPSCOPartyCount(const TicketID: Integer;   //QMANTWO-335
      var PartyCount: Integer);
    procedure GetNWNPartyCount(const TicketID: Integer; var PartyCount: Integer);
  public
    TieredClientFunc: TGetTieredClientFunc;
    procedure Add(TicketID, LocatorID, RefID: Integer; const ClientCode, Status: string);  //QMANTWO-710
    function Has(TicketID: Integer; const ClientCode: string): Boolean;
    procedure GetTermList(TicketID: Integer;UseExlcludes:boolean; var Terms: TStringArray); //QM-3
    procedure UpdateStatus(TicketID: Integer; const ClientCode, Status: string);
    function CountLocatesOnTicket(TicketID: Integer): Integer;
    property CheckForDuplicates: Boolean read FCheckForDuplicates write FCheckForDuplicates;

    function CountRespectedLocates(TicketID: Integer; ClientID: Integer;
      const ClientCode, CallCenter: string): Integer;
    function DoesTermIDRespectCode(TermID: Integer;
      const ClientCode, RespectCode, CallCenter: string): Boolean;
    function IsTieredClient(ClientID: Integer; const ClientCode, CallCenter: string): Boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

uses Variants, BillingShared, dialogs;

{ TTicketClientList }

procedure TTicketClientList.Add(TicketID, LocatorID, RefID: Integer; const ClientCode, Status: string);  //QMANTWO-710
begin
  if CheckForDuplicates then begin
    IndexTicketsIfNecessary;
    if not Tickets.Locate('ticket_id;client_code', VarArrayOf([TicketID, ClientCode]), []) then
      Tickets.AppendRecord([TicketID, ClientCode, Status, LocatorID, RefID]);  //QMANTWO-710
  end else
    Tickets.AppendRecord([TicketID, ClientCode, Status, LocatorID, RefID]);  //QMANTWO-710
end;

function TTicketClientList.CountLocatesOnTicket(TicketID: Integer): Integer;
begin
  Result := InternalCountLocates(TicketID);
end;

function TTicketClientList.CountRespectedLocates(TicketID, ClientID: Integer;
  const ClientCode, CallCenter: string): Integer;
begin
  Assert((ClientID >= 0) and (TicketID >= 0) and (ClientCode <> ''));
  Result := InternalCountLocates(TicketID, ClientID, ClientCode, CallCenter);
end;

function TTicketClientList.DoesTermIDRespectCode(TermID: Integer;
  const ClientCode, RespectCode, CallCenter: string): Boolean;
begin
  if CallCenter = 'FMB1' then
    // COHWS02/COHWS02S, COHWSLS/COHWSLSS, and COHWSL01/COHWSL01S are the same
    // client and always appear together on a ticket.  Do not double-count them.
    Result := (not MatchesTermGroup(RespectCode, 'FMBCOHWShadowTerms')) and
      (not (StrBeginsWith('BGE', RespectCode) and StrEndsWith('G', RespectCode)))
  else if CallCenter = 'FAM1' then
    // BEMC01 is the only tiered client for FAM1 and only respects SCMB01
    Result := (ClientCode = RespectCode) or ((ClientCode = 'BEMC01') and (RespectCode = 'SCMB01'))
  else // By default, clients respect all potential UQ clients on the ticket
    Result := True;
end;

function TTicketClientList.Has(TicketID: Integer; const ClientCode: string): Boolean;
begin
  IndexTicketsIfNecessary;
  Result := Tickets.Locate('ticket_id;client_code', VarArrayOf([TicketID, ClientCode]), []);
end;

procedure TTicketClientList.DataModuleCreate(Sender: TObject);
begin
  Tickets.CreateTable;
  Tickets.Exclusive := False;
  Tickets.Open;
end;

destructor TTicketClientList.Destroy;
begin
  Tickets.Close;
  Tickets.Exclusive := True;
  Tickets.DeleteTable;
  inherited;
end;

procedure TTicketClientList.IndexTicketsIfNecessary;
begin
  if Tickets.IndexFieldCount < 1 then begin
    Tickets.Close;
    Tickets.AddIndex('TicketIDIndex', 'ticket_id; ref_id', []);
    Tickets.IndexFieldNames := 'ticket_id; ref_id';
    Tickets.Open;
  end;
end;

// A ClientID of -1 means count all parties on the ticket (ignore billing_party_respect)
function TTicketClientList.InternalCountLocates(TicketID, ClientID: Integer; const ClientCode, CallCenter: string): Integer;
var
  Parties, oClientRef: Integer; // qmantwo-710
  // myFile  : textfile;       //test
  // outputFile  : string;     //test
  // cnt:integer;       //test
  firstParty:boolean;
begin
  firstParty:=false;
  IndexTicketsIfNecessary;
  // cnt:= 0;       //test
  Result := 0;
  // For non-tiered clients, parties = 1
  if (ClientID > -1) and (not IsTieredClient(ClientID, ClientCode, CallCenter)) then
  begin
    Result := 1;
    Exit;
  end;

  // outputFile:= 'c:\logs\ticketList.txt';     //test
  // assignFile(myFile, outputFile);            //test
  // Rewrite(myFile);                           //test
  Tickets.first; // qmantwo-707
  // while not(Tickets.eof) do      //test
  // begin                          //test
  // writeln(myFile, TicketsTicketID.AsString+'  '+TicketsRefID.AsString);      //test
  // Tickets.next;      //test
  // inc(cnt);            //test
  // end;                    //test
  // CloseFile(myFile);         //test
  // showmessage(intToStr(cnt)+ ' tickets counted');     //test
  // Locate/Next is faster than SetRange or using a query
  // This loop assumes the table is indexed by ticket_id
  oClientRef := 0;
  if Tickets.Locate('ticket_id', TicketID, []) then
  begin
    while (TicketsTicketID.AsInteger = TicketID) and not Tickets.Eof do
    begin
      if (ClientID = -1) or DoesTermIDRespectCode(ClientID, ClientCode, TicketsClientCode.AsString, CallCenter) then
      begin
        if StringInArray(CallCenter, CenterGroup('CentersRespectSelfAndNonNC')) then
        begin
          if (MatchesCenterGroup(CallCenter, 'LWA', False)  or  MatchesCenterGroup(CallCenter, 'LOR', False))
          and (not MatchesTermGroup(ClientCode, 'LWARespectNLLocates')) then
          begin
            // LWA by default ignores NL-status locates (except the LWARespectNLLocates terms)
            if (not(StringInArray(TicketsStatus.AsString, ['NC', 'NL']))) then
            begin
              if ((oClientRef = 0) or (oClientRef = TicketsRefID.AsInteger)) then // qmantwo-710
                Inc(Result); // qmantwo-710
            end
          end  //LWA LOR
          else if not((TicketsStatus.AsString = 'NC') or (TicketsStatus.AsString = 'ZZZ')) then
          begin
            if ((oClientRef = 0) or (oClientRef = TicketsRefID.AsInteger)) then // qmantwo-710
              Inc(Result); // qmantwo-710
          end
        end // if StringInArray(CallCenter
        else if CallCenter = 'FMW1' then
        begin
          if MatchesTermGroup(ClientCode, 'FMWVerizonTerms') and MatchesTermGroup(TicketsClientCode.AsString, 'FMWVerizonTerms')
          then
          begin
            if ((oClientRef = 0) or (oClientRef = TicketsRefID.AsInteger)) then // qmantwo-710
              Inc(Result); // qmantwo-710
          end
          else if (TicketsStatus.AsString = 'M') or (TicketsStatus.AsString = 'N') then
          begin
            if ((oClientRef = 0) or (oClientRef = TicketsRefID.AsInteger)) then // qmantwo-710
              Inc(Result); // qmantwo-710
          end
        end
        else if (CallCenter = 'OCC1') or (CallCenter = 'OCC2') {or (CallCenter = 'FCO1')} or (CallCenter = 'FMB1') then
        begin
          if (TicketsStatus.AsString <> 'NC') then
          begin
            if ((oClientRef = 0) or (oClientRef = TicketsRefID.AsInteger)) then // qmantwo-710
              Inc(Result); // qmantwo-710
          end
        end;

      end; // if (ClientID = -1)
      Tickets.Next;
      oClientRef := TicketsRefID.AsInteger; // qmantwo-710
    end;
  end; // while
 // Below are special overrides for centers with party restrictions and limitations
  if (CallCenter = 'NewJersey5') and (Result > 1) then
    AdjustNewJersey5PartyCount(TicketID, Result);

  if (CallCenter = '1851') or (CallCenter = '1852') then
  begin // QMANTWO-335
    GetNIPSCOPartyCount(TicketID, Parties);
    Result := Parties;
  end;

  if StringInArray(CallCenter, CenterGroup('LOR')) or StringInArray(CallCenter, CenterGroup('LWA'))then   //QM-3
//  if (MatchesTermGroup('LOR',CallCenter)) or  (MatchesTermGroup('LWA',CallCenter)) then
  begin // QMANTWO-
    GetNWNPartyCount(TicketID, Parties);
    Result := Parties;
  end;

  if (Result < 1) then
    Result := 1;
end;

procedure TTicketClientList.GetNWNPartyCount(const TicketID: Integer; var PartyCount: Integer);
var
  TermsOnTicket: TStringArray;
  firstParty:boolean;
  ClientCode:string;
  i:integer;
  len:integer;
begin   //QM-3
  i := 0;
  firstParty:=false;
  GetTermList(TicketID, True, TermsOnTicket);
  len := length(TermsOnTicket);

    for I := Low(TermsOnTicket) to High(TermsOnTicket) do
    begin
      ClientCode:=  TermsOnTicket[i];
      if (MatchesTermGroup(ClientCode, 'NWNGasTerms') and (firstParty=false)) then
      begin
        firstParty:=true;
        Inc(PartyCount);
      end
      else
      begin
        if not MatchesTermGroup(ClientCode, 'NWNGasTerms') then Inc(PartyCount);
      end;
    end;
end;

procedure TTicketClientList.AdjustNewJersey5PartyCount(const TicketID: Integer; var PartyCount: Integer);
var
  TermsOnTicket: TStringArray;
  PECOTerms: TOdStringList;
begin
  // for all NewJersey5 (PA) clients, only count a single PECO term, regardless of how many are on the ticket
  GetTermList(TicketID, False, TermsOnTicket);

  PECOTerms := TOdStringList.Create;
  try
    PECOTerms.Union(TermGroup('PECOGasTerms'));
    PECOTerms.Union(TermGroup('PECOElectricTerms'));
    PECOTerms.Intersect(TermsOnTicket); // PECOTerms now has just PECO terms from the ticket
    PartyCount := PartyCount - PECOTerms.Count + 1;
  finally
    FreeAndNil(PECOTerms);
  end;
end;

procedure TTicketClientList.GetNIPSCOPartyCount(const TicketID: Integer; var PartyCount: Integer);
var
  TermsOnTicket: TStringArray;
  NIPSCOterms: TOdStringList;
begin   //QMANTWO-335
  GetTermList(TicketID, False, TermsOnTicket);

  NIPSCOterms := TOdStringList.Create;
  try
    NIPSCOterms.Union(TermGroup('185NipscoGasTerms'));
    NIPSCOterms.Union(TermGroup('185NipscoElectricTerms'));
    NIPSCOterms.Intersect(TermsOnTicket);
    PartyCount := NIPSCOterms.Count; // removed plus 1  qm-89  sr
  finally
    FreeAndNil(NIPSCOterms);
  end;
end;


function TTicketClientList.IsTieredClient(ClientID: Integer; const ClientCode, CallCenter: string): Boolean;
begin
  Result := False;
  if Assigned(TieredClientFunc) then
    Result := TieredClientFunc(CallCenter, ClientCode);
end;

procedure TTicketClientList.UpdateStatus(TicketID: Integer;
  const ClientCode, Status: string);
begin
  if Tickets.Locate('ticket_id;client_code', VarArrayOf([TicketID, ClientCode]), []) then begin
    Tickets.Edit;
    Tickets.FieldByName('status').AsString := Status;
    Tickets.Post;
  end
  else
    raise Exception.Create('Ticket ID not found in TTicketClientList.UpdateStatus: ' + IntToStr(TicketID));
end;

function TTicketClientList.GetLocatorForLocate(TicketID: Integer; const ClientCode: string): Integer;
begin
  Result := -1;
  LocatorIDQuery.ParamByName('TicketID').AsInteger := TicketID;
  LocatorIDQuery.ParamByName('ClientCode').AsString := ClientCode;
  LocatorIDQuery.Open;
  try
    if LocatorIDQuery.Eof then
      raise Exception.CreateFmt('Locate %d/%s not found in GetLocatorForLocate', [TicketID, ClientCode]);
    if LocatorIDQuery.RecordCount > 1 then
      raise Exception.CreateFmt('Locate %d/%s found multiple times in GetLocatorForLocate', [TicketID, ClientCode]);
    Result := LocatorIDQuery.FieldByName('locator_id').AsInteger;
  finally
    LocatorIDQuery.Close;
  end;
  Assert(Result > 0);
end;

constructor TTicketClientList.Create(AOwner: TComponent);
begin
  inherited;
  CheckForDuplicates := False;
end;

procedure TTicketClientList.GetTermList(TicketID: Integer;UseExlcludes:boolean; var Terms: TStringArray);
var   //
  TermLength: Integer;
begin
  IndexTicketsIfNecessary;
  SetLength(Terms, 0);
  if Tickets.Locate('ticket_id', TicketID, []) then begin
    // Tickets is always indexed by tikcet_id
    while (TicketsTicketID.AsInteger = TicketID) and (not Tickets.Eof) do
    begin
      TermLength := Length(Terms) + 1;
      SetLength(Terms, TermLength);
      if UseExlcludes then

        begin
                if (not(StringInArray(TicketsStatus.AsString, ['NC', 'ZZZ']))) then  //QM-3
                Terms[TermLength - 1] := TicketsClientCode.AsString
                else SetLength(Terms, TermLength-1);   //QM-3  shrink dynamic array if term not used
        end
        else
        begin
          Terms[TermLength - 1] := TicketsClientCode.AsString;
        end;
      Tickets.Next;
    end;
  end;
end;

end.
