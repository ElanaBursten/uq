unit BillingCCMap;
//qm-852  sr this is different that the one in Admin.  Do not switch.
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDmu, Vcl.StdCtrls, dxSkinsCore, dxCore, dxSkinsForm;

type
  TBillingCCMapForm = class(TBaseCxListForm)
    ColCol_Center: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColWorkState: TcxGridDBColumn;
    ColWorkCounty: TcxGridDBColumn;
    ColWorkCity: TcxGridDBColumn;
    ColBillingCC: TcxGridDBColumn;
    chkboxBillingccSearch: TCheckBox;
    procedure chkboxBillingccSearchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  BillingCCMapForm: TBillingCCMapForm;

implementation

{$R *.dfm}

procedure TBillingCCMapForm.chkboxBillingccSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxBillingccSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TBillingCCMapForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TBillingCCMapForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxbillingccsearch.Enabled := true;
end;

end.
