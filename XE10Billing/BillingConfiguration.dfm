object BillingConfig: TBillingConfig
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 298
  Width = 333
  object ExcludeWorkFor: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from  billing_exclude_work_for'
    Parameters = <>
    Left = 44
    Top = 100
  end
  object ExcludeWorkType: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from  billing_exclude_work_type'
    Parameters = <>
    Left = 44
    Top = 156
  end
  object BillingRates: TADODataSet
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from  billing_rate'
    Parameters = <>
    Left = 36
    Top = 32
  end
  object TieredClientList: TADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      'select distinct call_center, billing_cc'#13#10'from billing_rate'#13#10'wher' +
      'e parties > 1'
    Parameters = <>
    Left = 152
    Top = 24
  end
  object StatusDescriptions: TADODataSet
    CacheSize = 100
    Connection = QMBillingDM.Conn
    CursorType = ctOpenForwardOnly
    CommandText = 'select status, status_name from statuslist'
    CommandTimeout = 60
    Parameters = <>
    Left = 152
    Top = 80
  end
  object CodeMap: TADODataSet
    CacheSize = 100
    Connection = QMBillingDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from billing_cc_map'
    Parameters = <>
    Left = 148
    Top = 152
  end
  object Taxes: TADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 'select * from billing_scc_tax order by state, county, city'
    Parameters = <>
    Left = 248
    Top = 32
  end
  object EligibleTerms: TADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      'select c.oc_code'#13#10' from customer cu'#13#10'  inner join client c on cu' +
      '.customer_id=c.customer_id'#13#10' where c.call_center in ('#39'FCV2'#39')'#13#10'  ' +
      'and cu.period_type='#39'WEEK'#39#13#10
    Parameters = <>
    Left = 248
    Top = 168
  end
  object ClientsForCenters: TADODataSet
    Connection = QMBillingDM.Conn
    CommandText = 
      '/* Overwritten at runtime */'#13#10'select client_id from client where' +
      ' call_center in ('#39'FCV1'#39')'
    Parameters = <>
    Left = 252
    Top = 112
  end
end
