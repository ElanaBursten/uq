unit BillingRunnerMain;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, StdCtrls, ExtCtrls, OdEmbeddable,
  CheckLst, ComCtrls, BillPeriod, cxButtonEdit, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxCore, cxDateUtils, dxSkinsCore, dxCoreGraphics;

type
  TBillingRunnerMainForm = class(TEmbeddableForm)
    DataEntryPanel: TPanel;
    Label6: TLabel;
    BillingType: TRadioGroup;
    RunBillingButton: TButton;
    LogMemo: TMemo;
    CallCenterQuery: TADODataSet;
    CenterGroupCombo: TComboBox;
    CenterGroupLabel: TLabel;
    DescriptionEdit: TEdit;
    DescriptionLabel: TLabel;
    RawDataDialog: TOpenDialog;
    Label1: TLabel;
    OpenFolderCheckBox: TCheckBox;
    NoExportSaveCheckBox: TCheckBox;
    Label2: TLabel;
    PeriodTypeCombo: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    PeriodStarting: TLabel;
    PeriodLookBackDate: TLabel;
    DevDateOverride: TButton;
    NoOldRunCleanupCheckBox: TCheckBox;
    PeriodEndingDate: TcxDateEdit;
    RawDataFileEdit: TcxButtonEdit;
    ClearLogButton: TButton;
    procedure RunBillingButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CenterGroupChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BillingTypeClick(Sender: TObject);
    procedure RawDataFileEditButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure DescriptionEditChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PeriodEndingDateChange(Sender: TObject);
    procedure PeriodTypeComboChange(Sender: TObject);
    procedure DevDateOverrideClick(Sender: TObject);
    procedure ClearLogButtonClick(Sender: TObject);
  private
    FDescriptionChanged: Boolean;
    FPeriod: TBillPeriod;
    FUpdatingPeriodDates: Boolean;
    procedure PopulateCallCenterList;
    function HaveSelectedCenters: Boolean;
    procedure SetDefaultBillingDescription;
    procedure SetDefaultFieldValues;
    procedure SaveBillingLogTo(const Directory, BillingRunName: string);
    procedure ShowPeriodDates;
    procedure EMailLog(const BillingRunName: string; BillingLog: TStrings);
  public
    procedure Log(Sender: TObject; const Msg: string);
    procedure RefreshNow; override;
    procedure ActivatingNow; override;
    procedure PopulateDropdowns; override;
  end;

implementation

uses DateUtils, JclShell, BillingEngine, QMBillingDMu, OdMiscUtils, OdInternetUtil;

{$R *.dfm}

procedure TBillingRunnerMainForm.RunBillingButtonClick(Sender: TObject);
var
  BE: TBillingEngine;
begin
  RunBillingButton.Enabled := False;
  try
    SetCurrentDir(ExtractFilePath(Application.ExeName));

    try
      BE := TBillingEngine.Create;
      try
        BE.SetConnectionLike(QMBillingDM.Conn);
        BE.ExportRawDataOnly := BillingType.ItemIndex = 2;
        BE.NoExportOrSave := NoExportSaveCheckBox.Checked;
        BE.SetBillingTimeout(QMBillingDM.BillingTimeout);
        BE.Logger := Log;
        BE.SetCallCenterGroup(CenterGroupCombo.Text);
        BE.Period := FPeriod;
        BE.NoOldRunCleanup := NoOldRunCleanupCheckBox.Checked;

        case BillingType.ItemIndex of
          0,2: begin
            if not HaveSelectedCenters then
              raise Exception.Create('Please select at least one call center');
            BE.RunDateRangeBilling(DescriptionEdit.Text);
          end;

          1: begin
            BE.RunBillingFromDir(DescriptionEdit.Text, ExtractFileDir(RawDataFileEdit.Text));
          end;
        end;
        if BE.ErrorCount > 0 then
          Log(Self, Format('************** This billing run encountered %d %s.  Please scroll up to review them. **************', [BE.ErrorCount, AddSIfNot1(BE.ErrorCount, 'error')]));
        if OpenFolderCheckBox.Checked and (BE.DestinationDir <> '') then begin
          Log(Self, 'Opening folder: ' + BE.DestinationDir);
          JclShell.OpenFolder(BE.DestinationDir);
        end;
        SaveBillingLogTo(BE.DestinationDir, BE.BillingRunName);
      finally
        FreeAndNil(BE);
      end;
      FDescriptionChanged := False;
    except
      on E: Exception do
        Log(Self, 'ERROR: ' + E.Message);
    end;
  finally
    RunBillingButton.Enabled := True;
  end;
end;

procedure TBillingRunnerMainForm.Log(Sender: TObject; const Msg: string);
begin
  LogMemo.Lines.Add(Msg);
end;

procedure TBillingRunnerMainForm.PopulateCallCenterList;
begin
  QMBillingDM.ConfigDM.RefreshGroups;
  QMBillingDM.ConfigDM.GetBillingCallCenterGroupList(CenterGroupCombo.Items);
  DescriptionEdit.Clear;
end;

procedure TBillingRunnerMainForm.RefreshNow;
begin
  inherited;
  PopulateCallCenterList;
end;

procedure TBillingRunnerMainForm.CenterGroupChanged(Sender: TObject);
begin
  SetDefaultBillingDescription;
end;

procedure TBillingRunnerMainForm.ClearLogButtonClick(Sender: TObject);
begin
  inherited;
  LogMemo.Clear;
end;

procedure TBillingRunnerMainForm.SetDefaultBillingDescription;
var
  Centers: TStringArray;
begin
  if FDescriptionChanged then
    Exit;
  SetLength(Centers, 1);
  Centers := nil;
  if CenterGroupCombo.ItemIndex >= 0 then begin
    Centers := QMBillingDM.ConfigDM.GetCenterGroup(CenterGroupCombo.Text);
    DescriptionEdit.Text := StringArrayToDelimitedString(Centers, '', ',');
    FDescriptionChanged := False;
  end
  else
    DescriptionEdit.Clear;
end;

procedure TBillingRunnerMainForm.FormCreate(Sender: TObject);
begin
  FPeriod := TBillPeriod.Create(Now);
  FPeriod.ReadFiscalPeriodsFrom(QMBillingDM.ConfigDM.DycomPeriods);
  PeriodTypeCombo.Items.Text := TBillPeriod.GetBillPeriodList;
  SetDefaultFieldValues;
end;

procedure TBillingRunnerMainForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FPeriod);
end;

procedure TBillingRunnerMainForm.SetDefaultFieldValues;
begin
  PeriodEndingDate.Date := FPeriod.DisplayEndDate;
  PeriodTypeCombo.ItemIndex := 0;
end;

procedure TBillingRunnerMainForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(False);
end;

procedure TBillingRunnerMainForm.BillingTypeClick(Sender: TObject);
begin
  RawDataFileEdit.Enabled := BillingType.ItemIndex = 1;
end;

procedure TBillingRunnerMainForm.RawDataFileEditButtonClick(
  Sender: TObject; AbsoluteIndex: Integer);
begin
  RawDataDialog.FileName := RawDataFileEdit.Text;
  if RawDataDialog.Execute then
    RawDataFileEdit.Text := RawDataDialog.FileName;
end;

procedure TBillingRunnerMainForm.DescriptionEditChange(Sender: TObject);
begin
  FDescriptionChanged := Trim(DescriptionEdit.Text) <> '';
end;

function TBillingRunnerMainForm.HaveSelectedCenters: Boolean;
begin
  Result := CenterGroupCombo.ItemIndex >= 0;
end;

procedure TBillingRunnerMainForm.EMailLog(const BillingRunName: string; BillingLog: TStrings);
begin
  if Pos('ERROR', BillingLog.Text) > 0 then
    SendEmailWithIniSettings(QMBillingDM.IniFileName, 'ErrorEmail',
    '', '', 'Billing Error Notification for ' + BillingRunName, BillingLog.Text); //QM-137 BCC placeholder
end;

procedure TBillingRunnerMainForm.SaveBillingLogTo(const Directory, BillingRunName: string);
var
  i, j: Integer;
  Log: TStringList;
  FileName: string;
begin
  FileName := IncludeTrailingPathDelimiter(Directory) + 'Log-' + FormatDateTime('yyyy-mm-dd', Now) + '.txt';
  for i := LogMemo.Lines.Count - 1 downto 0 do begin
    if Pos('Running billing for:', LogMemo.Lines[i]) > 0 then begin
      Log := TStringList.Create;
      try
        for j := LogMemo.Lines.Count - 1 downto i do begin
          Log.Insert(0, LogMemo.Lines[j]);
        end;
{$IFDEF DEBUG}
      FileName := 'C:\Logs\'+ExtractFileName(FileName);
{$ENDIF}
        Log.SaveToFile(FileName);
        EMailLog(BillingRunName, Log);
        Exit;
      finally
        FreeAndNil(Log);
      end;
    end;
  end;
  // If the log begin delimiter isn't found, just save everything
  LogMemo.Lines.SaveToFile(FileName);
  EMailLog(BillingRunName, LogMemo.Lines);
end;

procedure TBillingRunnerMainForm.PopulateDropdowns;
begin
  inherited;
  PopulateCallCenterList;
end;

procedure TBillingRunnerMainForm.FormShow(Sender: TObject);
begin
  inherited;
  RefreshNow;
end;

procedure TBillingRunnerMainForm.PeriodEndingDateChange(Sender: TObject);
var
  MyDate: TDateTime;
begin
  if FUpdatingPeriodDates then Exit;

  FPeriod.DisplayEndDate := StrToDateTime(PeriodEndingDate.EditText); //qm-811 sr
  ShowPeriodDates;
end;

procedure TBillingRunnerMainForm.PeriodTypeComboChange(Sender: TObject);
begin
  inherited;
  FPeriod.Span := PeriodTypeCombo.Text;
  ShowPeriodDates;
end;

procedure TBillingRunnerMainForm.ShowPeriodDates;
begin
  FUpdatingPeriodDates := True;
  try
    PeriodStarting.Caption := DateToStr(FPeriod.StartDate);
    PeriodLookBackDate.Caption := DateToStr(FPeriod.LookBackDate);
    PeriodEndingDate.Date := StrToDateTime(PeriodEndingDate.EditText); //qm-811 sr
  finally
    FUpdatingPeriodDates := False;
  end;
end;

procedure TBillingRunnerMainForm.DevDateOverrideClick(Sender: TObject);
var
  S: string;
begin
  inherited;
  S := InputBox('Override Date', 'Please enter the single date to bill for', '');
  if S <> '' then begin
    FPeriod.OverrideToSingleDay(StrToDate(S));
    ShowPeriodDates;
  end;
end;

end.

