unit CallCenterGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMasterDetail, DB, ADODB,  ExtCtrls, ActnList, StdCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxDropDownEdit, cxTextEdit, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  System.Actions, dxSkinsCore, dxDateRanges, dxScrollbarAnnotations;

type
  TCallCenterGroupForm = class(TBaseMasterDetailForm)
    NoteLabel: TLabel;
    ColMasterCenterGroupId: TcxGridDBColumn;
    ColMasterCenterGroupCode: TcxGridDBColumn;
    ColMasterShowForBilling: TcxGridDBColumn;
    ColMasterComment: TcxGridDBColumn;
    ColMasterActive: TcxGridDBColumn;
    ColDetailCenterGroupID: TcxGridDBColumn;
    ColDetailCallCenter: TcxGridDBColumn;
    ColDetailActive: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  public
    procedure PopulateDropdowns; override;
  end;

implementation

uses
  OdDbUtils, QMBillingDMu, OdCxUtils;

{$R *.dfm}

procedure TCallCenterGroupForm.FormCreate(Sender: TObject);
begin
  inherited;
  CanDelete := False;
end;

procedure TCallCenterGroupForm.PopulateDropdowns;
begin
  inherited;
  QMBillingDM.GetCallCenterList(CxComboBoxItems(ColDetailCallCenter));
end;

end.
