﻿unit EMDispatch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Vcl.ExtCtrls, Vcl.ComCtrls,
  cxMaskEdit, Vcl.StdCtrls, Vcl.Menus, cxGridCustomPopupMenu, dxSkinsCore,
  cxMemo, dxSkinsForm, dxScrollbarAnnotations, dxCore, dxSkinBasic;


const
  UM_SET_TREE = WM_USER + $107;  //QM-11
  UM_SHUT_DOWN = WM_USER + $108;  //QM-11

  TMR_MULTI=60000;
type
  TfrmEMDispatch = class(TForm)
    EMDispatchView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOConn: TADOConnection;
    EMDispatchViewColemt_ack_date: TcxGridDBColumn;
    EMDispatchViewColemt_ack_emp_id: TcxGridDBColumn;
    EMDispatchViewColemt_call_center: TcxGridDBColumn;
    EMDispatchViewColemt_call_center_time_zone: TcxGridDBColumn;
    EMDispatchViewColemt_contact_phone: TcxGridDBColumn;
    EMDispatchViewColemt_dbsource: TcxGridDBColumn;
    EMDispatchViewColemt_has_ack: TcxGridDBColumn;
    EMDispatchViewColemt_image: TcxGridDBColumn;
    EMDispatchViewColemt_emt_insert_date: TcxGridDBColumn;
    EMDispatchViewColemt_local_utc_bias: TcxGridDBColumn;
    EMDispatchViewColemt_locator_emp_id: TcxGridDBColumn;
    EMDispatchViewColemt_short_name: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_ack_id: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_format: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_id: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_kind: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_number: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_type: TcxGridDBColumn;
    EMDispatchViewColemt_ticket_version_id: TcxGridDBColumn;
    EMDispatchViewColemt_timesheet_status: TcxGridDBColumn;
    EMDispatchViewColemt_transmit_date: TcxGridDBColumn;
    EMDispatchViewColemt_work_state: TcxGridDBColumn;
    EMStoredProcDS: TDataSource;
    PopupMenu: TPopupMenu;
    EmpTextMsg: TMenuItem;
    EmpEM: TMenuItem;
    qryEmergencyTickets: TADOQuery;
    Panel2: TPanel;
    FloorDatePicker: TDateTimePicker;
    Label1: TLabel;
    qryEmergencyTicketsemt_dbsource: TWideStringField;
    qryEmergencyTicketsemt_ticket_id: TIntegerField;
    qryEmergencyTicketsemt_has_ack: TBooleanField;
    qryEmergencyTicketsemt_ack_date: TDateTimeField;
    qryEmergencyTicketsemt_ack_emp_id: TIntegerField;
    qryEmergencyTicketsemt_insert_date: TDateTimeField;
    qryEmergencyTicketsemt_locator_emp_id: TIntegerField;
    qryEmergencyTicketsemt_ticket_version_id: TIntegerField;
    qryEmergencyTicketsemt_ticket_ack_id: TIntegerField;
    qryEmergencyTicketsemt_ticket_type: TStringField;
    qryEmergencyTicketsemt_ticket_number: TStringField;
    qryEmergencyTicketsemt_ticket_kind: TStringField;
    qryEmergencyTicketsemt_ticket_format: TStringField;
    qryEmergencyTicketsemt_transmit_date: TDateTimeField;
    qryEmergencyTicketsemt_work_state: TStringField;
    qryEmergencyTicketsemt_image: TMemoField;
    qryEmergencyTicketsemt_short_name: TStringField;
    qryEmergencyTicketsemt_contact_phone: TStringField;
    qryEmergencyTicketsemt_phoneco_ref: TStringField;
    qryEmergencyTicketsemt_local_utc_bias: TIntegerField;
    qryEmergencyTicketsemt_call_center: TStringField;
    qryEmergencyTicketsemt_call_center_time_zone: TStringField;
    qryEmergencyTicketsemt_timesheet_status: TStringField;
    qryEmergencyTicketsStaleTime: TIntegerField;
    EMDispatchViewColemt_Age: TcxGridDBColumn;
    Label2: TLabel;
    edtElapseTime: TEdit;
    Label3: TLabel;
    tmrElapse: TTimer;
    cbExcludeDones: TCheckBox;
    EMDispatchViewColemt_Min: TcxGridDBColumn;
    qryEmergencyTicketsStaleMin: TIntegerField;
    btnRefresh: TButton;
    StatusBar1: TStatusBar;
    btnClaim: TButton;
    updClaims: TADOQuery;
    EMDispatchViewColemt_aduser: TcxGridDBColumn;
    qryEmergencyTicketsemt_claimedBy_AD_UserName: TStringField;
    qryEmergencyTicketsemt_claimedBy_Emp_id: TIntegerField;
    qryUserEmpID: TADOQuery;
    updTicketPriority: TADOQuery;
    EMDispatchViewColemt_priority: TcxGridDBColumn;   //qm-940 sr
    qryEmergencyTicketsemt_priority: TStringField;
    dxSkinController1: TdxSkinController;   //qm-940 uses Ref table tkPriorty
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FloorDatePickerCloseUp(Sender: TObject);
    procedure EmpTextMsgClick(Sender: TObject);
    procedure EmpEMClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryEmergencyTicketsCalcFields(DataSet: TDataSet);
    procedure tmrElapseTimer(Sender: TObject);
    procedure edtElapseTimeKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnClaimClick(Sender: TObject);
  private
    aDatabase: String;
    aServer: String;
    ausername: string;
    apassword: string;
    aTrusted: string;
    fConnectedDB: string;
    fUserName: string;
    UserEmpID:integer;
    procedure OnSetTree(var Msg: TMessage); message UM_SHUT_DOWN; //QM-11 SR
    function CalcDaysPrior(const FloorDate: TDateTime): Integer;
    function connectDB: boolean;
    function EnDeCrypt(const Value: String): String;
    function ReadINI: boolean;
    procedure RefreshQuery;
    function GetAppVersionStr: string;
    { Private declarations }
  public
    { Public declarations }
    WkManFormHandle:THandle;
    property UserName:string read fUserName write fUserName;//QM-44   SR
    property ConnectedDB:string read fConnectedDB write fConnectedDB;   //QM-44   SR
  end;

var
  frmEMDispatch: TfrmEMDispatch;

implementation
 USES System.IniFiles, System.StrUtils, DateUtils;

{$R *.dfm}

procedure TfrmEMDispatch.EmpTextMsgClick(Sender: TObject);
var
  PhoneNo: String;
begin
//    qryEmergencyTickets.Parameters.ParamByName('DaysBack').Value
//  PhoneNo :=           //field may be null
//    VartoStr(EMDispatchView.Controller.FocusedRecord.Values[EMDispatchViewColemt_contact_phone.Index]);
//    ShowMessage('text message sent: ' + PhoneNo);
   //the below is just some sample coding for Verizon  (the phoneco_ref field in employee table is included in SQL)
  {
  //StatusMemo.Clear; //Memo or dialog for error messages
  //setup SMTP
  SMTP.Host := smtp.dynutil.com;
  SMTP.Port := 25;
  //setup mail message
  MailMessage.From.Address := '????';
  MailMessage.Recipients.EMailAddresses := PhoneNo +   +'@vtext.com';
  MailMessage.Subject := '???'; //Email input.Subject.Text;
  MailMessage.Body.Text := '???'; //Email input form. Body.Text;
  //if FileExists(FileAttachment.Text) then
  //  TIdAttachment.Create(MailMessage.MessageParts, FileAttachment.Text) ;
  //send mail
  try
    try
      SMTP.Connect(1000) ;
      SMTP.Send(MailMessage) ;
    except on E:Exception do
      //StatusMemo.Lines.Insert(0, 'ERROR: ' + E.Message) ;
    end;
  finally
    if SMTP.Connected then SMTP.Disconnect;
  end;  }
end;

procedure TfrmEMDispatch.EmpEMClick(Sender: TObject);
var
  EmpID: Integer;
  TicketID: Integer;
  AHandle: UINT;
begin
  TicketID := qryEmergencyTickets.fieldByName('emt_ticket_id').AsInteger; // emt_ticket_id
  EmpID := qryEmergencyTickets.fieldByName('emt_locator_emp_id').AsInteger;

  AHandle := WkManFormHandle; // retrieve handle to WorkManagementForm
  PostMessage(AHandle, UM_SET_TREE, TicketID, EmpID);
end;

procedure TfrmEMDispatch.FloorDatePickerCloseUp(Sender: TObject);
begin
  RefreshQuery;
end;

procedure TfrmEMDispatch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryEmergencyTickets.Close;
  ADOConn.Connected := False;
end;

procedure TfrmEMDispatch.FormCreate(Sender: TObject);
begin
  if ParamCount < 1 then
  begin
    showmessage('This program can only be started with QManager');
    application.Terminate;
  end
  else
  begin
    WkManFormHandle := StrToInt64(paramstr(1));
    UserName        := paramstr(2);
    ConnectedDB     := paramstr(3);
  end;

  ConnectDB;
end;

procedure TfrmEMDispatch.FormShow(Sender: TObject);
var
  DaysBack: integer;
begin
  DaysBack := CalcDaysPrior(Today);
  FloorDatePicker.DateTime := Today + DaysBack;
  qryEmergencyTickets.Parameters.ParamByName('DaysBack').Value := DaysBack;
  if cbExcludeDones.Checked then
    qryEmergencyTickets.Parameters.ParamByName('ExcludeDone').Value := 'DONE'
  else
    qryEmergencyTickets.Parameters.ParamByName('ExcludeDone').Value := '';

  qryEmergencyTickets.Open;
  tmrElapse.interval := StrToInt(edtElapseTime.text) * (TMR_MULTI);
  StatusBar1.Panels[1].text := ConnectedDB + ' Database';
  StatusBar1.Panels[3].text := UserName;
  StatusBar1.Panels[5].text := GetAppVersionStr;

  try
    qryUserEmpID.Close;
    qryUserEmpID.Parameters.ParamByName('ShortName').Value := UserName;
    qryUserEmpID.Open;
    UserEmpID := qryUserEmpID.fieldByName('emp_id').AsInteger;

  finally
    qryUserEmpID.Close;
  end;

end;

procedure TfrmEMDispatch.OnSetTree(var Msg: TMessage);
begin
  Application.Terminate;
end;

procedure TfrmEMDispatch.qryEmergencyTicketsCalcFields(DataSet: TDataSet);
var
  Hours, Minutes : integer;
begin
  Minutes := MinutesBetween(Now, DataSet.FieldByName('emt_insert_date').AsDateTime);
  Hours   := HoursBetween(Now, DataSet.FieldByName('emt_insert_date').AsDateTime);
  Minutes := (Minutes-Hours*60);
  DataSet.FieldByName('StaleTime').AsInteger := Hours;
  DataSet.FieldByName('StaleMin').AsInteger := Minutes;  //MinutesBetween(Now, DataSet.FieldByName('emt_insert_date').AsDateTime);
end;

procedure TfrmEMDispatch.btnClaimClick(Sender: TObject);
var
  AView: TcxGridDBTableView;
  I, RecIdx, ColIdx, ColIdx2: Integer;
  OutputVal, OutputVal2: Variant;
  FinalStr,FinalStr2 :string;
  recUpd, ticketID :integer;
const
UPD_CLAIM=
        'update [dbo].[ticket_ack] '+
        'set [claimedBy_Emp_id] = %d  '+
        'where ticket_ack_id in ( '+
        '%s '+
        ') ' +
        'and [claimedBy_Emp_id] IS NULL ';      //qm-843


UPD_CLAIM_TICKET=     //qm-801  qm-842
                'update ticket '+
                'set work_priority_id =( '+
                        'select  ref_id '+
                        'from reference '+
                        'where type=''tkpriority'' '+
                'and code = ''Dispatch'') '+
                'where ticket_id in ( '+
                '%s '+
                ') ';
begin
//updClaims    QM-45  SR    //qm-801
  AView:=  EMDispatchView;

  for I := 0 to AView.Controller.SelectedRecordCount - 1 do
  begin
    // Gets the index of the selected record
    // NOTE: Do not use the Record.Index property
    // as it represents the index of a record taking sorting, grouping, etc.
    // into account
    // RecordIndex provides the absolute index
    RecIdx := AView.Controller.SelectedRecords[I].RecordIndex;
    // Gets the column index of the Description field
    ColIdx := AView.DataController.GetItemByFieldName('emt_ticket_ack_id').Index;
    // Obtains the value of the required field
    OutputVal := AView.DataController.Values[RecIdx, ColIdx];
    // Test output

    ColIdx2 := AView.DataController.GetItemByFieldName('emt_ticket_id').Index;  //qm-801
    // Obtains the value of the required field
    OutputVal2 := AView.DataController.Values[RecIdx, ColIdx2];  //qm-801
    // Test output

    VarCast(OutputVal, OutputVal, varString);
    VarCast(OutputVal2, OutputVal2, varString); //qm-801
    if I>0 then
    begin
      FinalStr := FinalStr+','+ OutputVal;
      FinalStr2 := FinalStr2+','+ OutputVal2;   //qm-801
    end
    else
    begin
      FinalStr :=OutputVal;
      FinalStr2 :=OutputVal2;   //qm-801
    end;
  end;

  updClaims.sql.text := Format(UPD_CLAIM,[UserEmpID,FinalStr]);
  recUpd:= updClaims.ExecSQL;

  if recUpd>0 then
  begin
    updTicketPriority.sql.text :=   Format(UPD_CLAIM_TICKET,[FinalStr2]); //qm-801
    updTicketPriority.ExecSQL;   //qm-801

    ShowMessage(IntToStr(recUpd)+' tickets claimed and Dispatched');  //qm-801
    RefreshQuery;
  end
  else
  ShowMessage('You cannot claim a ticket that is already claimed');  //qm-843
  RefreshQuery;

end;

procedure TfrmEMDispatch.btnRefreshClick(Sender: TObject);
begin
  RefreshQuery;
end;

function TfrmEMDispatch.CalcDaysPrior(const FloorDate: TDateTime): Integer;
var
  ACurrDate, AFloorDate: TDateTime;
begin
  Result := 0;
  ACurrDate := Today;
  AFloorDate := FloorDate;

  if DateOf(AFloorDate) > DateOf(ACurrDate)  then
    AFloorDate := ACurrDate;

  if DateOf(AFloorDate) = DateOf(ACurrDate) then
  begin
  // Saturday and Sunday go back to Thursday, change if needed
    Case DayofWeek(AFloorDate) of
      1,7: AFloorDate := AFloorDate -3;
      2,3,4,5: AFloorDate := AFloorDate - 1;
      6: AFloorDate := AFloorDate -2;
    end;
  end;

  AFloorDate := AFloorDate + 1;
  while DateOf(AFloorDate) <= DateOf(ACurrDate) do
  begin
    AFloorDate := AFloorDate + 1;
    dec(Result);
  end;
end;

function TfrmEMDispatch.ReadINI: boolean;  // not used
var
  EmerView: TIniFile;
  aPath:string;
begin
  Result := false;
  aPath := IncludeTrailingBackslash(ExtractFilePath(paramstr(0)));
  try
    EmerView := TIniFile.Create(aPath + 'EmerView.ini');

    aServer   := EmerView.ReadString('Database', 'Server', '');
    aDatabase := EmerView.ReadString('Database', 'DB', 'QM');
    ausername := EmerView.ReadString('Database', 'UserName', '');
    apassword := EmerView.ReadString('Database', 'Password', '');
    aTrusted  := EmerView.ReadString('Database', 'Trusted', '1');

  finally
    freeandnil(EmerView);
    Result := True;
  end;
end;

procedure TfrmEMDispatch.tmrElapseTimer(Sender: TObject);
begin
  RefreshQuery;
end;

procedure TfrmEMDispatch.RefreshQuery;
begin
  qryEmergencyTickets.DisableControls;
  tmrElapse.Enabled := false;
  qryEmergencyTickets.Close;

  qryEmergencyTickets.Parameters.ParamByName('DaysBack').Value :=  CalcDaysPrior(FloorDatePicker.Date);
  if cbExcludeDones.Checked then
  qryEmergencyTickets.Parameters.ParamByName('ExcludeDone').Value := 'DONE'
  else
    qryEmergencyTickets.Parameters.ParamByName('ExcludeDone').Value := '';

  qryEmergencyTickets.Open;
  tmrElapse.Enabled := true;
  qryEmergencyTickets.EnableControls;
end;

function TfrmEMDispatch.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function TfrmEMDispatch.connectDB: boolean;
const
  DECRYPT = 'DEC_';
var
  connStr, LogConnStr: String;

  function EnDeCrypt(const Value: String): String;
  var
    CharIndex : integer;
  begin
    Result := Value;
    for CharIndex := 1 to Length(Value) do
      Result[CharIndex] := chr(not(ord(Value[CharIndex])));
  end;

begin
  ADOConn.Close;
  Result := false;
  apassword :='';
  aServer:='';
  If ConnectedDB='Prod' then     //QM-44   SR
  begin
    ausername := 'svc-utq-qm-emrgncydsptch';
    aServer:= 'SSDS-UTQ-QM-02';
    apassword := 'DEC_ﾪﾔﾫﾑﾇﾾ￞ￕﾮￊￇￆﾹﾵﾗﾞ';
  end
  else
  if ConnectedDB='Test' then     //QM-44   SR
  begin
    ausername := 'svc-utq-qm-emrgncydsptch-dv';
    aServer:= 'SSDS-UTQ-QM-02-DV';
    apassword := 'DEC_ￎﾬﾗﾛﾗ﾿ﾱﾽﾯﾩﾙﾰﾔﾺﾬￕ';
  end;


  aTrusted := '0';
  aDatabase := 'QM';

  try

    LogConnStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    if LeftStr(apassword, 4) = DECRYPT then
    begin
      apassword := copy(apassword, 5, maxint);
      apassword := EnDeCrypt(apassword);
    end
    else
    begin
      Showmessage('You are using a Clear Text Password.  Please contact IT for the correct Password');
    end;

    connStr := 'Provider=SQLNCLI11.1;Password=' + apassword + ';Persist Security Info=True;User ID=' + ausername +
     ';Initial Catalog=' + aDatabase + ';Data Source=' + aServer +';Trusted_Connection ='+aTrusted+';';
    ADOConn.ConnectionString := connStr;
    ADOConn.Open;
  finally
     Result := ADOConn.Connected;
  end;
end;

procedure TfrmEMDispatch.edtElapseTimeKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  tmrElapse.Enabled:=false;
  tmrElapse.interval := StrToInt(edtElapseTime.text)*(TMR_MULTI);
  tmrElapse.Enabled:=true;
end;

function TfrmEMDispatch.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(pchar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(pchar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  result := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

end.
