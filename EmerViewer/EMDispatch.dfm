object frmEMDispatch: TfrmEMDispatch
  Left = 0
  Top = 0
  Caption = 'Emergency Tickets'
  ClientHeight = 524
  ClientWidth = 1343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000000000000130B0000130B000000000000000000000000
    000000000000000000000000000000000000251FBFFF2118BAFF1C13B7FF1912
    B7FF1E19B7FF251FBBFF00000000000000000000000000000000000000000000
    000000000000000000002520C2FF1A13B7FF1814B0FF130CA8FF0F0C9DFF0F06
    9EFF1210A0FF1410A7FF180EADFF1C14B7FF0000000000000000000000000000
    0000000000003630C6FF1715B5FF1210A7FF0A0392FF272295FF4642A6FF4643
    A6FF342C9CFF0F0790FF100E9BFF1710ABFF2622BAFF00000000000000000000
    0000362ECBFF1410B5FF1009A7FF24219BFF9391C4FFD6D4E4FFE6E8EBFFE6E8
    E9FFDDDBE5FFB1B1D4FF4442ABFF0F0BA4FF1613B4FF2A23C2FF000000000000
    00002019BAFF130BB0FF3A37AAFFCDCCDFFFECECECFFE7E7E7FFE4E5EBFFE2DF
    E7FFEAEAEAFFECEBE8FFEDEDEFFF8583CFFF1310B2FF1614B4FF000000003531
    C4FF1512B3FF1408AAFFAFACD8FFF2F1EDFFE4E3E8FFCBCADDFF4944A2FF2E2C
    9CFF9D9CCDFFDEDDE8FFCFCEDFFFB4B3DAFF251FB1FF120AB3FF2822BAFF221D
    B6FF1412B0FF2825ABFFD8D8E6FFE7E7E9FFEFEFF1FF7572B4FF0B0689FF1811
    99FF222097FF322D95FF241F89FF1D178CFF1A149FFF1810AEFF1E18B3FF1912
    AFFF1A14BFFF4A45CBFFE8E6EEFFE9E7EFFFE9E7EDFFDBD9E9FFD6D5EAFFD9D6
    EFFFD4D2EAFFCAC9E2FFCDCBE3FFD7D7E8FF6D69C9FF130BB3FF1814ADFF2B25
    CAFF2A26D9FF5049D8FFECEAF2FFEDECF1FFEDECF1FFD2D0E6FFC5C4E5FFC6C6
    E4FFD1CFE5FFF4F1F6FFEFF0F3FFFCFBF5FF716ED6FF231BCEFF2A23C8FF3933
    CFFF2D28D7FF4039DBFFE1E1F2FFF3F0F4FFFAFAFAFF7B78BBFF090886FF0E0C
    8CFF5A58B0FFFAF8FAFFEFEEF3FFEFEEF3FF514BD1FF2921D5FF3832D2FF4B45
    D4FF332CD6FF312ADCFFB7B6F1FFFFFEFAFFF3F2F7FFDBDBECFF6662B3FF4B47
    A6FFBDBBDEFFF9F6FBFFFBF9F9FFCECDF0FF3631D7FF312CD7FF423DD2FF0000
    00003C37D3FF3530DDFF514CE4FFD9D7F5FFFFFFF9FFFDFCFFFFFBFBFBFFF8F7
    FAFFFFFCFCFFFCFDFAFFEDECF7FF6C68E5FF322EDCFF3631D1FF000000000000
    0000000000003831D4FF3A32E2FF4D45E7FFA29EF0FFE6E4FAFFF9F8FBFFFAFA
    FCFFEAEAF9FFB3B1F1FF5852E4FF3731DFFF3930D6FF524CD8FF000000000000
    0000000000005E59D8FF3E37D6FF4137E0FF3C34E4FF544DE5FF6A64E9FF6B68
    E9FF5752E5FF3B32E2FF3D37E0FF3E37DAFF4F48D4FF00000000000000000000
    0000000000000000000000000000443FD1FF433ED7FF413BDDFF3F3ADFFF3F38
    E1FF403ADDFF423CDAFF443FD1FF4B46D5FF0000000000000000000000000000
    0000000000000000000000000000000000005D58DAFF514CD5FF4641D3FF443E
    D3FF4B47D3FF4D48D4FF0000000000000000000000000000000000000000F81F
    0000E0070000C003000080010000800100000000000000000000000000000000
    0000000000000000000080010000C0010000C0030000F0070000F81FFFFF}
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1343
    Height = 472
    Align = alTop
    PopupMenu = PopupMenu
    TabOrder = 0
    LookAndFeel.NativeStyle = False
    LookAndFeel.ScrollbarMode = sbmClassic
    LookAndFeel.SkinName = ''
    object EMDispatchView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.Active = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EMStoredProcDS
      DataController.KeyFieldNames = 'emt_ticket_ack_id'
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skCount
          FieldName = 'emt_ticket_id'
          Column = EMDispatchViewColemt_ticket_id
        end
        item
          Kind = skAverage
          FieldName = 'StaleTime'
          Column = EMDispatchViewColemt_Age
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          FieldName = 'emt_ticket_number'
          Column = EMDispatchViewColemt_ticket_number
        end
        item
          Kind = skAverage
          FieldName = 'StaleTime'
          Column = EMDispatchViewColemt_Age
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      Styles.UseOddEvenStyles = bTrue
      object EMDispatchViewColemt_aduser: TcxGridDBColumn
        Caption = 'Claimed'
        DataBinding.FieldName = 'emt_claimedBy_AD_UserName'
      end
      object EMDispatchViewColemt_emt_insert_date: TcxGridDBColumn
        Caption = 'Ack Insert Date'
        DataBinding.FieldName = 'emt_insert_date'
        Width = 126
      end
      object EMDispatchViewColemt_transmit_date: TcxGridDBColumn
        Caption = 'Transmit Date'
        DataBinding.FieldName = 'emt_transmit_date'
        Width = 109
      end
      object EMDispatchViewColemt_ticket_number: TcxGridDBColumn
        Caption = 'Ticket Nbr'
        DataBinding.FieldName = 'emt_ticket_number'
        Width = 102
      end
      object EMDispatchViewColemt_ticket_id: TcxGridDBColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'emt_ticket_id'
        Visible = False
      end
      object EMDispatchViewColemt_short_name: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'emt_short_name'
        Width = 145
      end
      object EMDispatchViewColemt_priority: TcxGridDBColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'emt_priority'
        Width = 80
      end
      object EMDispatchViewColemt_Age: TcxGridDBColumn
        Caption = 'Hrs'
        DataBinding.FieldName = 'StaleTime'
      end
      object EMDispatchViewColemt_Min: TcxGridDBColumn
        Caption = 'Min'
        DataBinding.FieldName = 'StaleMin'
      end
      object EMDispatchViewColemt_timesheet_status: TcxGridDBColumn
        Caption = 'Wk Status'
        DataBinding.FieldName = 'emt_timesheet_status'
        Width = 78
      end
      object EMDispatchViewColemt_ticket_kind: TcxGridDBColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'emt_ticket_kind'
        Width = 106
      end
      object EMDispatchViewColemt_ticket_type: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'emt_ticket_type'
        Width = 69
      end
      object EMDispatchViewColemt_ticket_format: TcxGridDBColumn
        Caption = ' Format'
        DataBinding.FieldName = 'emt_ticket_format'
        Width = 72
      end
      object EMDispatchViewColemt_contact_phone: TcxGridDBColumn
        Caption = 'Contact Phone'
        DataBinding.FieldName = 'emt_contact_phone'
        Width = 114
      end
      object EMDispatchViewColemt_work_state: TcxGridDBColumn
        Caption = 'Wk St'
        DataBinding.FieldName = 'emt_work_state'
        Width = 41
      end
      object EMDispatchViewColemt_call_center: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'emt_call_center'
        PropertiesClassName = 'TcxMaskEditProperties'
        Width = 89
      end
      object EMDispatchViewColemt_image: TcxGridDBColumn
        Caption = 'Image'
        DataBinding.FieldName = 'emt_image'
        PropertiesClassName = 'TcxMemoProperties'
        Visible = False
        Width = 53
      end
      object EMDispatchViewColemt_dbsource: TcxGridDBColumn
        Caption = 'DBSource'
        DataBinding.FieldName = 'emt_dbsource'
        Visible = False
        Width = 61
      end
      object EMDispatchViewColemt_call_center_time_zone: TcxGridDBColumn
        Caption = 'CC Time Zone'
        DataBinding.FieldName = 'emt_call_center_time_zone'
        Visible = False
        Width = 83
      end
      object EMDispatchViewColemt_ticket_ack_id: TcxGridDBColumn
        Caption = 'Ticket Ack ID'
        DataBinding.FieldName = 'emt_ticket_ack_id'
        Visible = False
        Width = 80
      end
      object EMDispatchViewColemt_ticket_version_id: TcxGridDBColumn
        Caption = 'Ticket Version ID'
        DataBinding.FieldName = 'emt_ticket_version_id'
        Visible = False
        Width = 90
      end
      object EMDispatchViewColemt_has_ack: TcxGridDBColumn
        Caption = 'Has Ack'
        DataBinding.FieldName = 'emt_has_ack'
        Visible = False
        Width = 63
      end
      object EMDispatchViewColemt_locator_emp_id: TcxGridDBColumn
        Caption = 'Locator Emp ID'
        DataBinding.FieldName = 'emt_locator_emp_id'
        Visible = False
        Width = 81
      end
      object EMDispatchViewColemt_ack_emp_id: TcxGridDBColumn
        Caption = 'Ack Emp ID'
        DataBinding.FieldName = 'emt_ack_emp_id'
        Visible = False
      end
      object EMDispatchViewColemt_ack_date: TcxGridDBColumn
        Caption = 'Ack Date'
        DataBinding.FieldName = 'emt_ack_date'
        Visible = False
      end
      object EMDispatchViewColemt_local_utc_bias: TcxGridDBColumn
        Caption = 'Local UTC Bias'
        DataBinding.FieldName = 'emt_local_utc_bias'
        Visible = False
        Width = 79
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = EMDispatchView
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 472
    Width = 1343
    Height = 52
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 55
      Top = 10
      Width = 107
      Height = 13
      Caption = 'Ack Insert Date After:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 344
      Top = 10
      Width = 76
      Height = 13
      Caption = 'Refresh every: '
    end
    object Label3: TLabel
      Left = 462
      Top = 10
      Width = 37
      Height = 13
      Caption = 'Minutes'
    end
    object FloorDatePicker: TDateTimePicker
      Left = 167
      Top = 6
      Width = 129
      Height = 21
      Date = 43764.000000000000000000
      Time = 0.638204027767642400
      TabOrder = 0
      OnCloseUp = FloorDatePickerCloseUp
    end
    object edtElapseTime: TEdit
      Left = 422
      Top = 6
      Width = 35
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '5'
      OnKeyUp = edtElapseTimeKeyUp
    end
    object cbExcludeDones: TCheckBox
      Left = 584
      Top = 6
      Width = 153
      Height = 17
      Caption = 'Exclude Dones Tickets'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object btnRefresh: TButton
      Left = 992
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Refresh'
      TabOrder = 3
      OnClick = btnRefreshClick
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 33
      Width = 1345
      Height = 19
      Panels = <
        item
          Text = 'Connected to'
          Width = 80
        end
        item
          Width = 100
        end
        item
          Text = ' as User'
          Width = 50
        end
        item
          Width = 100
        end
        item
          Text = 'App Version'
          Width = 80
        end
        item
          Width = 50
        end>
    end
    object btnClaim: TButton
      Left = 760
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Select Ticket(s) above and click to make yours'
      Caption = 'Claim'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnClaimClick
    end
  end
  object ADOConn: TADOConnection
    CommandTimeout = 300
    Connected = True
    ConnectionString = 
      'Provider=SQLNCLI11.1;Password=1Shdh@NBPVfOkES*;Persist Security ' +
      'Info=True;User ID=svc-utq-qm-emrgncydsptch-dv;Initial Catalog=QM' +
      ';Data Source=SSDS-UTQ-QM-02-DV;Trusted_Connection =0;'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 96
    Top = 168
  end
  object EMStoredProcDS: TDataSource
    DataSet = qryEmergencyTickets
    Left = 312
    Top = 168
  end
  object PopupMenu: TPopupMenu
    Left = 208
    Top = 256
    object EmpEM: TMenuItem
      Caption = 'Go To Ticket Bucket'
      OnClick = EmpEMClick
    end
    object EmpTextMsg: TMenuItem
      Caption = 'Send Text Msg'
      Enabled = False
      OnClick = EmpTextMsgClick
    end
  end
  object qryEmergencyTickets: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qryEmergencyTicketsCalcFields
    CommandTimeout = 300
    Parameters = <
      item
        Name = 'DaysBack'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ExcludeDone'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = 'NULL'
      end>
    SQL.Strings = (
      'USE [QM]'
      ''
      ''
      'SET ANSI_NULLS ON'
      ''
      ''
      'SET QUOTED_IDENTIFIER ON'
      ''
      ''
      'Declare @floor_days int = -10;'
      ''
      'Set @floor_days = :DaysBack'
      ''
      'SET NOCOUNT ON;'
      ''
      'DECLARE @emtDispatch TABLE ('
      '    [emt_dbsource]'#9#9#9#9'[nvarchar] (128) NOT NULL'
      '   ,[emt_priority]         [varchar] (40) NULL'
      '   ,[emt_ticket_id]'#9#9#9#9'[int] NOT NULL'
      '   ,[emt_has_ack]'#9#9#9#9'[bit] NULL'
      '   ,[emt_ack_date]'#9#9#9#9'[datetime] NULL'
      '   ,[emt_ack_emp_id]'#9#9#9'[int] NULL'
      '   ,[emt_insert_date]'#9#9#9'[datetime] NULL'
      '   ,[emt_locator_emp_id]'#9#9'[int] NULL'
      '   ,[emt_ticket_version_id]'#9#9'[int] NULL'
      '   ,[emt_ticket_ack_id]'#9#9#9'[int] NULL'
      '   ,[emt_ticket_type]'#9#9#9'[varchar] (38) NULL'
      '   ,[emt_ticket_number]'#9#9#9'[varchar] (20) NOT NULL'
      '   ,[emt_ticket_kind]'#9#9#9'[varchar] (20) Null'
      '   ,[emt_ticket_format]'#9#9#9'[varchar] (20) NULL'
      '   ,[emt_transmit_date]'#9#9#9'[datetime]NULL'
      '   ,[emt_work_state]'#9#9#9'[varchar] (2) NULL'
      '   ,[emt_image]'#9#9#9#9#9'[text] NULL'
      '   ,[emt_short_name]'#9#9#9'[varchar] (30) NULL'
      '   ,[emt_contact_phone]'#9#9'    [varchar] (20) NULL'
      '   ,[emt_phoneco_ref]'#9#9'    [varchar] (20) NULL'
      '   ,[emt_local_utc_bias]'#9#9'[int] NULL'
      '   ,[emt_call_center]'#9#9#9'[varchar] (40) NULL'
      '   ,[emt_call_center_time_zone] [varchar] (100) NULL'
      '   ,[emt_timesheet_status]      [varchar] (6) NULL'
      '   ,[emt_claimedBy_Emp_id]      [int] NULL'
      '   ,[emt_claimedBy_AD_UserName] varchar(8)'
      '   );'
      ''
      'INSERT INTO @emtDispatch ('
      '    emt_dbsource'
      '   ,emt_priority'
      '   ,emt_ticket_id'
      '   ,emt_has_ack'
      '   ,emt_ack_date'
      '   ,emt_ack_emp_id'
      '   ,emt_insert_date'
      '   ,emt_locator_emp_id'
      '   ,emt_ticket_version_id'
      '   ,emt_ticket_ack_id'
      '   ,emt_ticket_type'
      '   ,emt_ticket_number'
      '   ,emt_ticket_kind'
      '   ,emt_ticket_format'
      '   ,emt_transmit_date'
      '   ,emt_work_state'
      '   ,emt_image'
      '   ,emt_short_name'
      '   ,emt_contact_phone'
      '   ,emt_phoneco_ref'
      '   ,emt_local_utc_bias'
      '   ,emt_call_center'
      '   ,emt_call_center_time_zone'
      '   ,emt_timesheet_status'
      '   ,emt_claimedBy_Emp_id'
      '   ,emt_claimedBy_AD_UserName'
      '   )'
      ''
      'SELECT'
      ''
      '    (SELECT DB_NAME() AS [Current Database])'
      #9'  ,R2.code'
      '    ,ticket_ack.ticket_id'
      '    ,ticket_ack.has_ack'
      '    ,ticket_ack.ack_date'
      '    ,ticket_ack.ack_emp_id'
      '    ,ticket_ack.insert_date'
      '    ,ticket_ack.locator_emp_id'
      '    ,ticket_ack.ticket_version_id'
      '    ,ticket_ack.ticket_ack_id'
      '    ,ticket.ticket_type'
      '    ,ticket.ticket_number'
      '    ,ticket.kind'
      '    ,ticket.ticket_format'
      '    ,ticket.transmit_date'
      '    ,ticket.work_state'
      '    ,ticket.[image]'
      '    ,employee.short_name'
      '    ,employee.contact_phone'
      '    ,employee.phoneco_ref'
      '    ,employee.local_utc_bias'
      '    ,call_center.cc_name'
      '    ,reference.[description]'
      '    ,dbo.get_current_work_status(ticket_ack.locator_emp_id)'
      '    ,ticket_ack.claimedBy_Emp_id'
      '    ,e2.ad_username'
      'FROM ticket_ack'
      #9'inner join ticket on ticket_ack.ticket_id = ticket.ticket_id'
      
        #9'inner join employee on ticket_ack.locator_emp_id = employee.emp' +
        '_id'
      
        #9'left outer join employee e2 on ticket_ack.claimedBy_Emp_id = e2' +
        '.emp_id'
      ''
      
        #9'inner join call_center on ticket.ticket_format = call_center.cc' +
        '_code'
      #9'inner join reference on ref_id = call_center.time_zone'
      
        '  left outer join reference R2 on R2.ref_id = ticket.work_priori' +
        'ty_id'
      ''
      'WHERE'
      
        #9'ticket.ticket_format not in ('#39'2101'#39','#39'2051'#39','#39'AZ1PC'#39')  --we are n' +
        'ot intrested in Ansco or Pauley Const tickets.'
      
        #9'and ticket_ack.insert_date >= Convert(varchar(8), DateAdd(Day, ' +
        '@floor_days, getdate()), 112)'
      #9'and has_ack = 0'
      #9'and not ticket.kind IN (:ExcludeDone)'
      'ORDER BY ticket_format,ticket_number;'
      ''
      'SELECT *'
      'FROM @emtDispatch'
      'order by emt_insert_date'
      '')
    Left = 208
    Top = 120
    object qryEmergencyTicketsemt_dbsource: TWideStringField
      FieldName = 'emt_dbsource'
      Size = 128
    end
    object qryEmergencyTicketsemt_ticket_id: TIntegerField
      FieldName = 'emt_ticket_id'
    end
    object qryEmergencyTicketsemt_priority: TStringField
      FieldName = 'emt_priority'
      Size = 40
    end
    object qryEmergencyTicketsemt_has_ack: TBooleanField
      FieldName = 'emt_has_ack'
    end
    object qryEmergencyTicketsemt_ack_date: TDateTimeField
      FieldName = 'emt_ack_date'
    end
    object qryEmergencyTicketsemt_ack_emp_id: TIntegerField
      FieldName = 'emt_ack_emp_id'
    end
    object qryEmergencyTicketsemt_insert_date: TDateTimeField
      FieldName = 'emt_insert_date'
    end
    object qryEmergencyTicketsemt_locator_emp_id: TIntegerField
      FieldName = 'emt_locator_emp_id'
    end
    object qryEmergencyTicketsemt_ticket_version_id: TIntegerField
      FieldName = 'emt_ticket_version_id'
    end
    object qryEmergencyTicketsemt_ticket_ack_id: TIntegerField
      FieldName = 'emt_ticket_ack_id'
    end
    object qryEmergencyTicketsemt_ticket_type: TStringField
      FieldName = 'emt_ticket_type'
      Size = 38
    end
    object qryEmergencyTicketsemt_claimedBy_AD_UserName: TStringField
      FieldName = 'emt_claimedBy_AD_UserName'
      Size = 8
    end
    object qryEmergencyTicketsemt_ticket_number: TStringField
      FieldName = 'emt_ticket_number'
    end
    object qryEmergencyTicketsemt_ticket_kind: TStringField
      FieldName = 'emt_ticket_kind'
    end
    object qryEmergencyTicketsemt_ticket_format: TStringField
      FieldName = 'emt_ticket_format'
    end
    object qryEmergencyTicketsemt_transmit_date: TDateTimeField
      FieldName = 'emt_transmit_date'
    end
    object qryEmergencyTicketsemt_work_state: TStringField
      FieldName = 'emt_work_state'
      Size = 2
    end
    object qryEmergencyTicketsemt_image: TMemoField
      FieldName = 'emt_image'
      BlobType = ftMemo
    end
    object qryEmergencyTicketsemt_short_name: TStringField
      FieldName = 'emt_short_name'
      Size = 30
    end
    object qryEmergencyTicketsemt_contact_phone: TStringField
      FieldName = 'emt_contact_phone'
    end
    object qryEmergencyTicketsemt_phoneco_ref: TStringField
      FieldName = 'emt_phoneco_ref'
    end
    object qryEmergencyTicketsemt_local_utc_bias: TIntegerField
      FieldName = 'emt_local_utc_bias'
    end
    object qryEmergencyTicketsemt_call_center: TStringField
      FieldName = 'emt_call_center'
      Size = 40
    end
    object qryEmergencyTicketsemt_call_center_time_zone: TStringField
      FieldName = 'emt_call_center_time_zone'
      Size = 100
    end
    object qryEmergencyTicketsemt_timesheet_status: TStringField
      FieldName = 'emt_timesheet_status'
      Size = 6
    end
    object qryEmergencyTicketsStaleTime: TIntegerField
      DisplayLabel = 'Age'
      FieldKind = fkCalculated
      FieldName = 'StaleTime'
      Calculated = True
    end
    object qryEmergencyTicketsStaleMin: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'StaleMin'
      Calculated = True
    end
    object qryEmergencyTicketsemt_claimedBy_Emp_id: TIntegerField
      FieldName = 'emt_claimedBy_Emp_id'
    end
  end
  object tmrElapse: TTimer
    Interval = 18000
    OnTimer = tmrElapseTimer
    Left = 512
    Top = 480
  end
  object updClaims: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 872
    Top = 472
  end
  object qryUserEmpID: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ShortName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'Select emp_id'
      'from employee'
      'where short_name = :ShortName')
    Left = 48
    Top = 368
  end
  object updTicketPriority: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'update ticket'
      'set work_priority_id =('
      '        select  ref_id'
      '        from reference'
      '        where type='#39'tkpriority'#39
      'and code = '#39'Dispatch'#39')'
      'where ticket_id  in (123,456,789)')
    Left = 208
    Top = 360
  end
  object dxSkinController1: TdxSkinController
    SkinName = 'DevExpressStyle'
    Left = 920
    Top = 304
  end
end
