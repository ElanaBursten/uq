program EmerViewer;
  {$R EMDispatchProj.dres}
  {$R 'QMVersion.res' '..\QMVersion.rc'}
uses
  Vcl.Forms,
  SysUtils,
  Windows,
  EMDispatch in 'EMDispatch.pas' {frmEMDispatch},
  GlobalSU in 'GlobalSU.pas';


var
  MyInstanceName: string;

begin
  MyInstanceName := ExtractFileName(Application.ExeName);
  if CreateSingleInstance(MyInstanceName) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    ReportMemoryLeaksOnShutdown := DebugHook <> 0;
    Application.CreateForm(TfrmEMDispatch, frmEMDispatch);
  Application.Run;
  end
  else
    Application.Terminate;
end.
