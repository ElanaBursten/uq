unit User;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls,
  BaseList,cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxMaskEdit, cxCheckBox, cxCalendar, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  dxSkinsCore,cxDBExtLookupComboBox, dxDateRanges, Vcl.Mask,
  Vcl.DBCtrls, cxContainer, cxTextEdit, cxDBEdit, System.Actions, Vcl.ActnList,
  Vcl.Menus, cxButtons, ADOInt, dxScrollbarAnnotations, AdminDMu, dxSkinBasic, dxCore, dxSkinsForm;

type
  TUserForm = class(TBaseCxListForm)
    Panel1: TPanel;
    UserClients: TADODataSet;
    UserClientsDS: TDataSource;
    Label1: TLabel;
    Clients: TADODataSet;
    ClientsDS: TDataSource;
    Label2: TLabel;
    AddButton: TButton;
    Datauid: TAutoIncField;
    Datagrp_id: TIntegerField;
    Dataemp_id: TIntegerField;
    Datafirst_name: TStringField;
    Datalast_name: TStringField;
    Datalogin_id: TStringField;
    Datapassword: TStringField;
    Datachg_pwd: TBooleanField;
    Datalast_login: TDateTimeField;
    Dataactive_ind: TBooleanField;
    Datacan_view_notes: TBooleanField;
    Datacan_view_history: TBooleanField;
    Datachg_pwd_date: TDateTimeField;
    Dataemail_address: TStringField;
    Dataetl_access: TBooleanField;
    ChangePassword: TButton;
    ColLoginID: TcxGridDBColumn;
    ColPassword: TcxGridDBColumn;
    ColFirstName: TcxGridDBColumn;
    ColLastName: TcxGridDBColumn;
    ColChgPwd: TcxGridDBColumn;
    ColChgPwdDate: TcxGridDBColumn;
    ColGrpID: TcxGridDBColumn;
    ColEmpID: TcxGridDBColumn;
    ColActiveInd: TcxGridDBColumn;
    ColUID: TcxGridDBColumn;
    ColCanViewHistory: TcxGridDBColumn;
    ColCanViewNotes: TcxGridDBColumn;
    ColEmailAddress: TcxGridDBColumn;
    ColETL: TcxGridDBColumn;
    AccessClientsGridView: TcxGridDBTableView;
    AccessClientsGridLevel1: TcxGridLevel;
    AccessClientsGrid: TcxGrid;
    AccessClientsGridViewoc_code: TcxGridDBColumn;
    AccessClientsGridViewcall_center: TcxGridDBColumn;
    AllClientsGridView: TcxGridDBTableView;
    AllClientsGridLevel1: TcxGridLevel;
    AllClientsGrid: TcxGrid;
    AllClientsGridViewColcall_center: TcxGridDBColumn;
    AllClientsGridViewColoc_code: TcxGridDBColumn;
    ColAcctDisabledDate: TcxGridDBColumn;
    chkboxClientSearch: TCheckBox;
    chkboxUserSearch: TCheckBox;
    ActiveUsersFilter: TCheckBox;
    DataAccount_Disabled_date: TDateTimeField;
    btnEmployeeInfo: TButton;
    TermEmpInfo: TADODataSet;
    btnEmployeeLink: TcxButton;
    Splitter1: TSplitter;
    ColorDialog1: TColorDialog;
    dbPassword: TcxDBCheckBox;
    procedure UserClientsNewRecord(DataSet: TDataSet);
    procedure AddButtonClick(Sender: TObject);
    procedure DataAfterOpen(DataSet: TDataSet);
    procedure UserClientsBeforeDelete(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure UserClientsBeforeInsert(DataSet: TDataSet);
    procedure ChangePasswordClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure chkboxUserSearchClick(Sender: TObject);
    procedure chkboxClientSearchClick(Sender: TObject);
    procedure ActiveUsersFilterClick(Sender: TObject);
    procedure btnEmployeeInfoClick(Sender: TObject);
    procedure DSDataChange(Sender: TObject; Field: TField);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure btnEmployeeLinkClick(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure UserClientsAfterPost(DataSet: TDataSet);
    procedure UserClientsAfterDelete(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
  private
    FSelectedID: Integer;
    UsersClientArray: TFieldValuesArray;
    UserCliStr: String;
    UserInserted: Boolean;
    procedure SetupControls;
  public
    procedure ShowUser(ID: Integer);
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses MainFU, OdHourglass, ChangePassword, TermEmp, OdDbUtils, OdMiscUtils;

{$R *.dfm}

procedure TUserForm.UserClientsNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('client_id').AsInteger := Clients.FieldByName('client_id').AsInteger;
  DataSet.FieldByName('uid').AsInteger := Data.FieldByName('uid').AsInteger;
end;

procedure TUserForm.AddButtonClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  UserClients.Append;
  UserClients.Post;
  UserClients.Close;
  UserClients.Open;
end;

procedure TUserForm.btnEmployeeInfoClick(Sender: TObject);
begin
  inherited;
  DisplayTermEmpInfo(Data.FieldByName('emp_id').AsInteger);
end;

procedure TUserForm.btnEmployeeLinkClick(Sender: TObject);
begin
  inherited;
  chkboxUserSearch.Checked := False;
  GridView.Controller.HideFindPanel;
  MainForm.ShowEmployee(Data.FieldByName('emp_id').AsInteger);
end;

procedure TUserForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  UserInserted := False;
end;

procedure TUserForm.DataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  UserClients.Open;
  Clients.Open;
  AllClientsGridView.DataController.FocusedRowIndex := 0;
end;

procedure TUserForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, ATableArray, 'users', 'uid', UserInserted);
end;

procedure TUserForm.UserClientsAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.AdminChangeTable('users_client', UserCliStr, now); //QM-847
end;

procedure TUserForm.UserClientsBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  UserCliStr := 'client_id ' + clients.FieldByName('client_id').AsString +
    ' added to user_id ' + Data.FieldByName('uid').AsString;
  if Data.State = dsInsert then
    Data.Post;
end;

procedure TUserForm.UserClientsAfterDelete(DataSet: TDataSet);
begin
  inherited;
   AdminDM.AdminChangeTable('users_client', UserCliStr, now); //QM-847
end;

procedure TUserForm.DataBeforeDelete(DataSet: TDataSet);
var
  Field: TField;
begin
  Field := DataSet.FindField('active_ind');
  if Field<>nil then begin
    DataSet.Edit;
    Field.AsBoolean := False;
    DataSet.Post;
  end;
  Abort;
end;

procedure TUserForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  // get the current values before editing
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-847
end;

procedure TUserForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  UserInserted := True;
end;

procedure TUserForm.DataBeforePost(DataSet: TDataSet);
var
  empid: integer;
  UserMessage: String;
  acount: integer;
  s: String;
begin
  inherited;
  if BtnEmployeeInfo.Enabled then
   if Data.FieldByName('active_ind').AsBoolean = False then
   begin
     empid := Data.FieldByName('emp_id').AsInteger;
     With TermEmpInfo, TermEmpInfo.Parameters do
     begin
       ParamByName('woempid').Value := empid;
       ParamByName('tempid').Value := empid;
       ParamByName('dempid').Value := empid;
       ParamByName('aempid').Value := empid;
       Open;
       if FieldbyName('total').AsInteger > 0 then
       begin
         UserMessage := 'Cannot deactivate account. Terminated employee has ' + slinebreak;
         acount := FieldbyName('wocount').AsInteger;
         if acount >0 then
           UserMessage := UserMessage + slinebreak + 'open work orders: ' + InttoStr(acount);
         acount := FieldbyName('tcount').AsInteger;
         if acount > 0  then
           UserMessage := UserMessage + slinebreak + 'open tickets: ' + InttoStr(acount);
         acount := FieldByName('dcount').AsInteger;
         if acount > 0  then
           UserMessage := UserMessage + slinebreak + 'open damages: ' + InttoStr(acount);
         acount := FieldByName('acount').AsInteger;
         if acount > 0 then
           UserMessage := UserMessage + slinebreak + 'assigned areas: ' + InttoStr(acount);
         MessageDlg(UserMessage, mtWarning, [mbOK], 0);
         Data.FieldByName('active_ind').AsBoolean := true;
       end;
       Close;
     end;
   end;
end;

procedure TUserForm.UserClientsBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  UserClients.Properties['Unique Table'].Value:='users_client';
   UserCliStr := 'client_id ' + DataSet.FieldByName('client_id').AsString +
     ' deleted from user_id ' + DataSet.FieldByName('uid').AsString;
end;

procedure TUserForm.FormShow(Sender: TObject);
begin
  inherited;
  Grid.SetFocus;
  SetupControls;
  Data.Properties['Update Criteria'].value :=adCriteriaKey;
end;

procedure TUserForm.ShowUser(ID: Integer);
var
  Msgstr: String;
begin
  If not Data.Locate('emp_id', ID, []) then
  begin
    MsgStr := 'Emp ID #' + InttoStr(ID) +
     ' not found in User view.';
    if ActiveUsersFilter.Checked then
       MsgStr := MsgStr + #13#10 + 'Check the inactive fields';
    MessageDlg(MsgStr, mtWarning, [mbOK], 0);
  end;
end;

procedure TUserForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  if Sender.Name <> 'GridView' then
  begin
    ShowMessage(sender.name);
    Exit;
  end;
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow and (VarToInt(GridRec.Values[ColUID.Index]) > 0) then
  begin
    if VarToInt(GridRec.Values[ColAcctDisabledDate.Index]) > 0 then
      ACanvas.Brush.Color :=   clWebLightSlateGray
    else
    if not VarToBoolean(GridRec.Values[ColActiveInd.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TUserForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TUserForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  //For this form, these grids ALWAYS stay read-only.
  AllClientsGridView.OptionsData.Appending := False;
  AllClientsGridView.OptionsData.Inserting := False;
  AllClientsGridView.OptionsData.Deleting := False;
  AllClientsGridView.OptionsData.Editing := False;

  AccessClientsGridView.OptionsData.Appending := False;
  AccessClientsGridView.OptionsData.Inserting := False;
  AccessClientsGridView.OptionsData.Editing := False;
  chkboxUserSearch.Enabled := True;
  chkboxClientSearch.Enabled := True;
  ActiveUsersFilter.Enabled := True;
  BtnEmployeeLink.Enabled := True;
  BtnEmployeeInfo.Enabled := True;
  btnEmployeeInfo.Enabled :=  not Data.FieldbyName('emp_id').IsNull;    //bp
//  (not Data.FieldbyName('emp_id').IsNull)   //qm-374  sr
//     and (not Data.FieldByName('Account_Disabled_date').IsNull);   //qm-374  sr
end;

procedure TUserForm.SetupControls;
begin
  if AdminDM.UserHasRestrictedAccess then begin
    GridView.OptionsData.Editing := False;
    GridView.OptionsBehavior.ImmediateEditor := False;
    AccessClientsGrid.Enabled := False;
    AddButton.Enabled := False;
  end else begin
    GridView.OptionsData.Editing := True;
    GridView.OptionsBehavior.ImmediateEditor := True;
    AccessClientsGrid.Enabled := True;
    AddButton.Enabled := True;
  end;
end;

procedure TUserForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('chg_pwd').AsBoolean := False;
  DataSet.FieldByName('active_ind').AsBoolean := True;
end;

procedure TUserForm.DSDataChange(Sender: TObject; Field: TField);
var
  A: String;
begin
  inherited;
  BtnEmployeeInfo.Enabled := (not Data.FieldbyName('emp_id').IsNull);
    // and (not Data.FieldByName('Account_Disabled_date').IsNull);
  btnEmployeeLink.Enabled := (not Data.FieldbyName('emp_id').IsNull);
end;

procedure TUserForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;


procedure TUserForm.CloseDataSets;
begin
  UserClients.Close;
  Clients.Close;
  inherited;
end;

procedure TUserForm.OpenDataSets;
var
  Cursor: IInterface;
  WherePos: Integer;
begin
  inherited;
  Cursor := ShowHourGlass;
end;

procedure TUserForm.ChangePasswordClick(Sender: TObject);
begin
  ChangeUserPassword(Data.FieldByName('uid').AsInteger, dbpassword, Data);
end;

procedure TUserForm.chkboxClientSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxClientSearch.Checked then
    AllClientsGridView.Controller.ShowFindPanel
  else
    AllClientsGridView.Controller.HideFindPanel;
end;

procedure TUserForm.chkboxUserSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxUserSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TUserForm.ActivatingNow;
begin
  inherited;
  UserInserted := False;
end;

procedure TUserForm.ActiveUsersFilterClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('uid').AsInteger;
  Data.Close;
  if ActiveUsersFilter.Checked then
    Data.CommandText := 'select * from users where active_ind = 1 order by login_id' else
      Data.CommandText := 'select * from users order by login_id';
  Data.Open;
  Data.Locate('uid', FSelectedID, []);
end;

end.

