inherited EPRNotificationConfigForm: TEPRNotificationConfigForm
  Caption = 'EPR Notification Config'
  ClientHeight = 399
  ClientWidth = 1200
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 1200
    Height = 28
    object cbNotificationSearch: TCheckBox
      Left = 26
      Top = 5
      Width = 102
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbNotificationSearchClick
    end
    object ActiveNotificationFilter: TCheckBox
      Left = 149
      Top = 5
      Width = 191
      Height = 17
      Caption = 'Active Notification Config Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveNotificationFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 1200
    Height = 371
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object NotificationConfigIDCol: TcxGridDBColumn
        Caption = 'Config ID'
        DataBinding.FieldName = 'notification_config_id'
        DataBinding.IsNullValueType = True
        Visible = False
        MinWidth = 71
        Options.Editing = False
        Options.Filtering = False
        Options.Focusing = False
        Options.FilteringFilteredItemsList = False
        Options.FilteringMRUItemsList = False
        Options.FilteringPopup = False
        Options.FilteringPopupMultiSelect = False
        Options.GroupFooters = False
        Options.Grouping = False
        Options.HorzSizing = False
        Options.Moving = False
        Options.ShowCaption = False
        Width = 71
      end
      object EPRNotificationCallCenterCol: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 103
      end
      object GridViewactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 43
      end
      object GridViewexclude_ticket_type: TcxGridDBColumn
        Caption = 'Exclude Ticket Type'
        DataBinding.FieldName = 'exclude_ticket_type'
        DataBinding.IsNullValueType = True
        Width = 207
      end
      object GridViewwork_done_for: TcxGridDBColumn
        Caption = 'Done For'
        DataBinding.FieldName = 'work_done_for'
        DataBinding.IsNullValueType = True
        Width = 162
      end
      object GridViewcontractor: TcxGridDBColumn
        Caption = 'Contractor'
        DataBinding.FieldName = 'contractor'
        DataBinding.IsNullValueType = True
        Width = 147
      end
      object GridViewdisclaimer_id: TcxGridDBColumn
        Caption = 'Disclaimer'
        DataBinding.FieldName = 'disclaimer_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'disclaimer_id'
        Properties.ListColumns = <
          item
            Caption = 'id'
            Fixed = True
            Width = 20
            FieldName = 'disclaimer_id'
          end
          item
            Caption = 'Disclaimer'
            Sorting = False
            Width = 65
            FieldName = 'disclaimer_text'
          end>
        Properties.ListSource = DisclaimerDS
        MinWidth = 5
        Width = 138
      end
      object GridViewscreenshots_include: TcxGridDBColumn
        Caption = 'Screenshots Inc'
        DataBinding.FieldName = 'screenshots_include'
        DataBinding.IsNullValueType = True
        Width = 115
      end
      object GridViewdelayer_client_codes: TcxGridDBColumn
        Caption = 'Delayer CC'
        DataBinding.FieldName = 'delayer_client_codes'
        DataBinding.IsNullValueType = True
        Width = 99
      end
      object GridViewclient_codes_require_cv: TcxGridDBColumn
        Caption = 'CC Require CV'
        DataBinding.FieldName = 'client_codes_require_cv'
        DataBinding.IsNullValueType = True
        Width = 100
      end
      object GridViewcv_Wait_Count: TcxGridDBColumn
        Caption = 'CV Wait Count'
        DataBinding.FieldName = 'cv_Wait_Count'
        DataBinding.IsNullValueType = True
        Width = 92
      end
      object GridViewinc_att_Wait_Hrs: TcxGridDBColumn
        Caption = 'Inc ATT Wait Hrs'
        DataBinding.FieldName = 'inc_att_Wait_Hrs'
        DataBinding.IsNullValueType = True
        Width = 105
      end
      object GridViewsurvey_url: TcxGridDBColumn
        Caption = 'Survey URL'
        DataBinding.FieldName = 'survey_url'
        DataBinding.IsNullValueType = True
        Width = 100
      end
      object GridViewviewable_days: TcxGridDBColumn
        Caption = 'Viewable Days'
        DataBinding.FieldName = 'viewable_days'
        DataBinding.IsNullValueType = True
        Width = 78
      end
      object GridViewcontact_to: TcxGridDBColumn
        Caption = 'Contact To'
        DataBinding.FieldName = 'contact_to'
        DataBinding.IsNullValueType = True
        Width = 100
      end
      object GridViewcontact_cc: TcxGridDBColumn
        Caption = 'Contact CC'
        DataBinding.FieldName = 'contact_cc'
        DataBinding.IsNullValueType = True
        Width = 100
      end
      object GridViewcontact_phone: TcxGridDBColumn
        Caption = 'Contact Phone'
        DataBinding.FieldName = 'contact_phone'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.EditMask = '!\(999\)000-0000;1;_'
        Width = 100
      end
      object GridViewcontact_name: TcxGridDBColumn
        Caption = 'Contact Name'
        DataBinding.FieldName = 'contact_name'
        DataBinding.IsNullValueType = True
        Width = 119
      end
      object GridViewremove_all_tie_downs: TcxGridDBColumn
        Caption = 'Remove Tie Downs'
        DataBinding.FieldName = 'remove_all_tie_downs'
        DataBinding.IsNullValueType = True
        Width = 105
      end
      object GridViewcompany_token: TcxGridDBColumn
        Caption = 'Company Token'
        DataBinding.FieldName = 'company_token'
        DataBinding.IsNullValueType = True
        Width = 86
      end
      object GridViewin_EPR: TcxGridDBColumn
        Caption = 'in EPR'
        DataBinding.FieldName = 'in_EPR'
        DataBinding.IsNullValueType = True
        Width = 36
      end
      object GridViewcontains_clients: TcxGridDBColumn
        Caption = 'Contains Clients'
        DataBinding.FieldName = 'contains_clients'
        DataBinding.IsNullValueType = True
        Width = 92
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'Select * from EPR_Notification_Config where active = 1'
  end
  object LookupDisclaimer: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'Select disclaimer_id, disclaimer_text from disclaimer'
    Parameters = <>
    Left = 616
    Top = 104
  end
  object DisclaimerDS: TDataSource
    DataSet = LookupDisclaimer
    Left = 704
    Top = 104
  end
end
