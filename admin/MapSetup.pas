unit MapSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, Buttons, ComCtrls,
  DBCtrls, Mask, AreaLocator, OdEmbeddable, BaseMap, BetterADODataSet, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, GridAxis,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,  AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxSplitter,
  dxDateRanges, dxScrollbarAnnotations, dxSkinsCore, dxSkinBasic;

type
  TMapSetupForm = class(TBaseMapForm)
    TabSheet2: TTabSheet;
    PC: TPageControl;
    map_pageGrid: TDBGrid;
    Label5: TLabel;
    Label7: TLabel;
    Label16: TLabel;
    MapGrid: TcxGrid;
    MapGridView: TcxGridDBTableView;
    ColMapName: TcxGridDBColumn;
    ColMapState: TcxGridDBColumn;
    ColMapCounty: TcxGridDBColumn;
    ColMapTicketCode: TcxGridDBColumn;
    ColMapId: TcxGridDBColumn;
    MapGridLevel: TcxGridLevel;
    ColMap_Type: TcxGridDBColumn;
    AssignmentSheet: TTabSheet;
    Panel1: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    AssignToLocatorBtn: TBitBtn;
    AssignAreaButton: TBitBtn;
    SelPageButton: TBitBtn;
    AreaGrid: TcxGrid;
    AreaGridView: TcxGridDBTableView;
    AreaGridViewColAreaName: TcxGridDBColumn;
    AreaGridViewColShortName: TcxGridDBColumn;
    AreaGridViewColEmpNumber: TcxGridDBColumn;
    AreaGridLevel1: TcxGridLevel;
    LocatorGrid: TcxGrid;
    LocatorGridView: TcxGridDBTableView;
    LocatorGridViewColLocatorName: TcxGridDBColumn;
    LocatorGridViewColEmpNum: TcxGridDBColumn;
    LocatorGridViewColActive: TcxGridDBColumn;
    LocatorGridLevel1: TcxGridLevel;
    chkboxLocatorSearch: TCheckBox;
    ActiveLocatorsFilter: TCheckBox;
    MapPageGrid: TcxGrid;
    MapPageGridView: TcxGridDBTableView;
    ColMapPagePageNum: TcxGridDBColumn;
    MapPageGridLevel: TcxGridLevel;
    Splitter1: TSplitter;
    TheGrid: TStringGrid;
    LocatorDS: TDataSource;
    MapDS: TDataSource;
    AreaDS: TDataSource;
    MapPageDS: TDataSource;
    chkboxMapSearch: TCheckBox;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    chkboxAreaSearch: TCheckBox;
    Area: TADODataSet;
    MapPage: TADODataSet;
    GridData: TADODataSet;
    Map: TADODataSet;
    ActiveAreasFilter: TCheckBox;
    AreaGridViewColActive: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure PCChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AssignAreaButtonClick(Sender: TObject);
    procedure AreaAfterPost(DataSet: TDataSet);
    procedure MapPageAfterScroll(DataSet: TDataSet);
    procedure AssignToLocatorBtnClick(Sender: TObject);
    procedure map_pageGridDblClick(Sender: TObject);
    procedure TheGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SelPageButtonClick(Sender: TObject);
    procedure AreaNewRecord(DataSet: TDataSet);
    procedure MapDSDataChange(Sender: TObject; Field: TField);
    procedure AreaBeforePost(DataSet: TDataSet);
    procedure chkboxLocatorSearchClick(Sender: TObject);
    procedure ActiveLocatorsFilterClick(Sender: TObject);
    procedure LocatorGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure chkboxMapSearchClick(Sender: TObject);
    procedure AreaAfterOpen(DataSet: TDataSet);
    procedure chkboxAreaSearchClick(Sender: TObject);
    procedure MapGridExit(Sender: TObject);
    procedure AreaGridExit(Sender: TObject);
    procedure DatasetBeforeDelete(DataSet: TDataSet);
    procedure ActiveAreasFilterClick(Sender: TObject);
    procedure AreaGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure GridDataAfterCancel(DataSet: TDataSet);
    procedure GridDataBeforeInsert(DataSet: TDataSet);
    procedure GridDataBeforeEdit(DataSet: TDataSet);
    procedure GridDataAfterPost(DataSet: TDataSet);
    procedure AreaAfterCancel(DataSet: TDataSet);
    procedure AreaBeforeInsert(DataSet: TDataSet);
    procedure AreaBeforeEdit(DataSet: TDataSet);
    procedure MapAfterCancel(DataSet: TDataSet);
    procedure MapBeforeInsert(DataSet: TDataSet);
    procedure MapBeforeEdit(DataSet: TDataSet);
    procedure MapAfterPost(DataSet: TDataSet);
  private
    SelectedPage: Integer;
    ColAxis, RowAxis: TAxis;
    SavedAreaName: string;
    mapcellInserted: Boolean;
    mapcellArray: TFieldValuesArray;
    areaInserted: Boolean;
    areaArray: TFieldValuesArray;
    mapInserted: Boolean;
    mapArray: TFieldValuesArray;
    procedure PopulateGrid;
    procedure SetColumnLabels;
    procedure SetRowLabels;
    procedure SetCellToArea(Col, Row: Integer; Area_id: Integer);
    procedure RefreshGridData;
    procedure RefreshArea;
    function Map_id: Integer;
    procedure SetMapLabels;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure RefreshNow; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure LeavingNow; override;
  end;

implementation

uses OdMiscUtils, OddbUtils, OdHourglass;

{$R *.dfm}

procedure TMapSetupForm.FormCreate(Sender: TObject);
begin
  Grid := TheGrid;
  PC.ActivePageIndex := 0;
end;

procedure TMapSetupForm.PCChange(Sender: TObject);
begin
  if PC.ActivePage = AssignmentSheet then
    PopulateGrid;

  AssignAreaButton.Enabled := (PC.ActivePage = AssignmentSheet) and (not IsReadOnly);
end;

procedure TMapSetupForm.PopulateGrid;
begin
  ClearGrid;
  SetMapLabels;
  SetRowLabels;
  SetColumnLabels;
  RefreshGridData;
  Grid.Enabled := not IsReadOnly;
end;

procedure TMapSetupForm.FormShow(Sender: TObject);
begin
  OpenDataSets;
  ClearGrid;
end;

procedure TMapSetupForm.GridDataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  mapcellInserted := False;
end;

procedure TMapSetupForm.GridDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, mapcellArray, 'map_cell', 'map_id', mapcellInserted);
end;

procedure TMapSetupForm.GridDataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, mapcellArray);
end;

procedure TMapSetupForm.GridDataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  mapcellInserted := True;
end;

procedure TMapSetupForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TMapSetupForm.LocatorGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[LocatorGridViewColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TMapSetupForm.AreaGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[AreaGridViewColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TMapSetupForm.AssignAreaButtonClick(Sender: TObject);
var
  row, col: Integer;
begin
  if Area.State in dsEditModes then
    Area.Post;

  for Col := Grid.Selection.Left to Grid.Selection.Right do
    for Row := Grid.Selection.Top to Grid.Selection.Bottom do
      SetCellToArea(Col, Row, Area.FieldByName('Area_id').AsInteger);

  GridData.UpdateBatch;
end;

procedure TMapSetupForm.ActiveAreasFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Area.FieldByName('area_id').AsInteger;
  Area.Close;
  if ActiveAreasFilter.Checked then
    Area.CommandText :=
    'select area.*, employee.short_name, employee.emp_number from area ' +
    'left join employee on area.locator_id=employee.emp_id ' +
    'where area.active = 1 and map_id = :map_id order by area.area_name' else
    Area.CommandText :=
    'select area.*, employee.short_name, employee.emp_number from area ' +
    'left join employee on area.locator_id=employee.emp_id ' +
    'where map_id = :map_id order by area.area_name';
  Area.Open;
  Area.Locate('area_id', FSelectedID, []);
end;

procedure TMapSetupForm.ActiveLocatorsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := AdminDM.Locator.FieldByName('emp_id').AsInteger;
  AdminDM.Locator.Close;
  if ActiveLocatorsFilter.Checked then
    AdminDM.Locator.CommandText := 'select * from employee where active = 1 order by short_name' else
      AdminDM.Locator.CommandText := 'select * from employee order by short_name';
  AdminDM.Locator.Open;
  AdminDM.Locator.Locate('emp_id', FSelectedID, []);
end;

procedure TMapSetupForm.AreaAfterCancel(DataSet: TDataSet);
begin
  inherited;
  areaInserted := False;
end;

procedure TMapSetupForm.AreaAfterOpen(DataSet: TDataSet);
begin
  inherited;
  AreaGridView.DataController.FocusedRowIndex := 0;
end;

procedure TMapSetupForm.AreaAfterPost(DataSet: TDataSet);
begin
  AdminDM.TrackAdminChanges(Dataset, areaArray, 'area', 'area_id', areaInserted);
  GetAreaRepr.Close;
  Area.Requery;
  Area.Locate('area_name', SavedAreaName, []);
end;

procedure TMapSetupForm.RefreshArea;
var
  id: Integer;
begin
  id := Area.FieldByName('area_id').AsInteger;
  Area.Close;
  Area.Open;
  Area.Locate('area_id', id, []);
end;

procedure TMapSetupForm.MapPageAfterScroll(DataSet: TDataSet);
begin
  SelectedPage := DataSet.FieldByName('map_page_num').AsInteger;

  if PC.ActivePage = AssignmentSheet then begin
    PopulateGrid;
  end;
end;

procedure TMapSetupForm.AssignToLocatorBtnClick(Sender: TObject);
begin
  AreaDS.DataSet.Edit;
  AreaDS.DataSet.FieldByName('locator_id').AsInteger :=
    locatorDS.DataSet.FieldByName('emp_id').AsInteger;
  AreaDS.DataSet.Post;

  RefreshArea;

  if PC.ActivePage = AssignmentSheet then
    RefreshGridData;
end;

procedure TMapSetupForm.chkboxAreaSearchClick(Sender: TObject);
begin
inherited;
  if chkboxAreaSearch.Checked then
    AreaGridView.Controller.ShowFindPanel
  else
    AreaGridView.Controller.HideFindPanel;
end;

procedure TMapSetupForm.chkboxLocatorSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxLocatorSearch.Checked then
    LocatorGridView.Controller.ShowFindPanel
  else
    LocatorGridView.Controller.HideFindPanel;
end;

procedure TMapSetupForm.chkboxMapSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxMapSearch.Checked then
    MapGridView.Controller.ShowFindPanel
  else
    MapGridView.Controller.HideFindPanel;
end;

procedure TMapSetupForm.map_pageGridDblClick(Sender: TObject);
begin
  PC.ActivePageIndex := 1;
  PCChange(Self);
end;

function TMapSetupForm.Map_id: Integer;
begin
  Result := map.Fields[0].AsInteger;
end;

procedure TMapSetupForm.SetMapLabels;
begin
  AssignmentSheet.Caption := 'Grid Assignments for ' + map.FieldByName('map_name').AsString + ' / ' + IntToStr(SelectedPage);
end;

procedure TMapSetupForm.TheGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  S, Area, Locator: string;
  p: Integer;
begin
  if (ARow>0) and (ACol>0) then begin
    with (Sender as TStringGrid).Canvas do begin
      S := Grid.Cells[ACol,ARow];
      p := Pos('/' , S);
      if p>=0 then begin
        Area := Copy(S,1,p-1);
        Locator := Copy(S, p+1, 50);

        Brush.Style := bsSolid;
        if gdSelected in State then begin
          Brush.Color := clYellow;  {blue}
        end else begin
          Brush.Color := clWhite;
        end;
        FillRect(Rect);

        Pen.Style := psSolid;
        Pen.Color := clBlack;

        Brush.Style := bsClear;
        Font.Color := clBlack;
        Font.Style := [fsBold];
        TextRect(Rect, Rect.Left + 2, Rect.Top+2, Area);

        Font.Color := clBlue;
        Font.Style := [ ];
        TextRect(Rect, Rect.Left + 6, Rect.Top+14, Locator);
      end;
    end;
  end;

end;

function CreateAxis(const MinVal, MaxVal: string): TAxis;
begin
  if StrToIntDef(MinVal, -999) = -999 then begin
    // it's an alpha
    Result := TAlphaAxis.Create(MinVal, MaxVal);
  end else begin
    // it's numeric
    Result := TNumAxis.Create(MinVal, MaxVal);
  end;
end;

procedure TMapSetupForm.SetCellToArea(Col, Row, Area_id: Integer);
var
  x_part, y_part: string;
begin
  x_part := ColAxis.IndexToCode(Col);
  y_part := RowAxis.IndexToCode(Row);

  if GridData.Locate('x_part;y_part', VarArrayOf([x_part, y_part]), []) then begin
    GridData.Edit;
  end else begin
    GridData.Append;
    GridData.FieldByName('map_id').AsInteger := map_id;
    GridData.FieldByName('map_page_num').AsInteger := SelectedPage;
    GridData.FieldByName('x_part').AsString := x_part;
    GridData.FieldByName('y_part').AsString := y_part;
  end;

  GridData.FieldByName('area_id').AsInteger := Area_id;
  GridData.Post;    // doesn't go to the server, we are using batch updates

  Grid.Cells[Col, Row] := CellContentsFor(area_id);
end;

procedure TMapSetupForm.SetColumnLabels;
var
  x: Integer;
begin
  FreeAndNil(ColAxis);
  ColAxis := CreateAxis(MapPage.FieldByName('x_min').AsString,
                        MapPage.FieldByName('x_max').AsString);

  Grid.ColCount := ColAxis.Size + 1;
  for x := 1 to Grid.ColCount-1 do begin
    Grid.Cells[x, 0] := ColAxis.IndexToCode(x);
  end;
end;


procedure TMapSetupForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  AreaGridView.OptionsSelection.CellSelect := not IsReadOnly;
  Grid.Enabled := not IsReadOnly;

  //For this form, this grid ALWAYS stays read-only.
  LocatorGridView.OptionsData.Appending := False;
  LocatorGridView.OptionsData.Inserting := False;
  LocatorGridView.OptionsData.Deleting := False;
  LocatorGridView.OptionsData.Editing := False;
  chkboxMapSearch.Enabled := True;
  chkboxLocatorSearch.Enabled := True;
end;

procedure TMapSetupForm.SetRowLabels;
var
  y: Integer;
begin
  FreeAndNil(RowAxis);
  if (Trim(MapPage.FieldByName('y_min').AsString) = '') or
     (Trim(MapPage.FieldByName('y_max').AsString) = '')
    then
      raise Exception.Create('Please enter values for the minimum and maximum page cells first');
  RowAxis := CreateAxis(MapPage.FieldByName('y_min').AsString,
                        MapPage.FieldByName('y_max').AsString);

  Grid.RowCount := RowAxis.Size + 1;
  for y := 1 to Grid.RowCount-1 do begin
    Grid.Cells[0, y] := RowAxis.IndexToCode(y);
  end;

  Grid.ColWidths[0] := 50;
end;


procedure TMapSetupForm.RefreshGridData;
var
  Cursor: IInterface;
  Contents: string;
  Col, Row: Integer;
begin
  Assert(Assigned(ColAxis));
  Assert(Assigned(RowAxis));

  Cursor := ShowHourGlass;

  GridData.Close;
  GridData.Parameters[0].Value := map_id;
  GridData.Parameters[1].Value := SelectedPage;
  GridData.Open;
  while not GridData.Eof do begin
    Col := ColAxis.CodeToIndex(GridData.FieldByName('x_part').AsString);
    Row := RowAxis.CodeToIndex(GridData.FieldByName('y_part').AsString);
    Contents := CellContentsFor(GridData.FieldByName('area_id').AsInteger);
    Grid.Cells[Col, Row] := Contents;
    GridData.Next;
  end;
end;

procedure TMapSetupForm.OpenDataSets;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  With AdminDM.Locator do
  begin
    if not Active then
    begin
      CommandText := 'select * from employee where active = 1 order by short_name';
      Open;
    end;
  end;
  AdminDM.locator.Open;
  Map.Open;
  Area.Open;
  MapPage.Open;
end;

procedure TMapSetupForm.CloseDataSets;
begin
  inherited;
  Map.Close;
  Area.Close;
  MapPage.Close;
  AdminDM.locator.Close;
end;

procedure TMapSetupForm.EditNow;
begin
  inherited;
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
  AssignAreaButton.Enabled := (PC.ActivePage = AssignmentSheet) and (not IsReadOnly);
  Grid.Enabled := not IsReadOnly;
end;

procedure TMapSetupForm.RefreshNow;
begin
  inherited;
  PopulateGrid;
end;

procedure TMapSetupForm.SelPageButtonClick(Sender: TObject);
var
  myRect: TGridRect;
begin
  inherited;
  myRect.Left := 1;
  myRect.Top := 1;
  myRect.Right := Grid.ColCount-1;
  myRect.Bottom := Grid.RowCount-1;
  Grid.Selection := myRect;
end;

procedure TMapSetupForm.AreaNewRecord(DataSet: TDataSet);
begin
  inherited;
  Area.FieldByName('map_id').AsInteger := Map.FieldByName('map_id').AsInteger;
end;

procedure TMapSetupForm.MapAfterCancel(DataSet: TDataSet);
begin
  inherited;
  mapInserted := False;
end;

procedure TMapSetupForm.MapAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'map', 'map_id', mapInserted);
end;

procedure TMapSetupForm.MapBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TMapSetupForm.MapBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  mapInserted := True;
end;

procedure TMapSetupForm.MapDSDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  Area.Close;
  Area.Parameters.ParamByName('map_id').Value := Map.FieldByName('map_id').AsInteger;
  Area.Open;
end;

procedure TMapSetupForm.DataSetBeforeDelete(DataSet: TDataSet);
var
  Field: TField;
begin
  inherited;
  Field := DataSet.FindField('Active');
  if Assigned(Field) then begin
    DataSet.Edit;
    Field.AsBoolean := False;
    DataSet.Post;
  end;
  Abort;
end;

procedure TMapSetupForm.MapGridExit(Sender: TObject);
begin
  inherited;
   PostDataSet(Map);
end;

procedure TMapSetupForm.AreaGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(Area);
end;

procedure TMapSetupForm.AreaBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, areaArray);
end;

procedure TMapSetupForm.AreaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  areaInserted := True;
end;

procedure TMapSetupForm.AreaBeforePost(DataSet: TDataSet);
begin
  inherited;
  SavedAreaName := Area.FieldByName('area_name').AsString;
end;

procedure TMapSetupForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
  ActiveLocatorsFilter.Enabled := True;
  ActiveAreasFilter.Enabled := True;
  chkboxAreaSearch.Enabled := True;
  mapcellInserted := False;
  AdminDM.LocInserted := False;
  areaInserted := False;
end;

end.

