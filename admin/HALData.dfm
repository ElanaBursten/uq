object DMHalSentinel: TDMHalSentinel
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 445
  Width = 870
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=TestDB;Data Source=BRIAN-PC;'
    IsolationLevel = ilReadCommitted
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 16
  end
  object Attachments: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select short_name, coalesce(e.contact_phone, '#39'No Phone'#39') as [Pho' +
      'ne], count(attach_date) as '#39'Pending'#39#13#10'from attachment a with (no' +
      'lock)'#13#10'join employee e on e.emp_id = a.attached_by'#13#10'where (attac' +
      'h_date between getdate()- :daysback and getdate()-1)'#13#10' and uploa' +
      'd_date is null'#13#10'group by short_name, e.contact_phone'#13#10'order by s' +
      'hort_name'
    Parameters = <
      item
        Name = 'daysback'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 192
    Top = 16
    object Attachmentsshort_name: TStringField
      DisplayLabel = 'Short Name'
      FieldName = 'short_name'
      Size = 30
    end
    object AttachmentsPhone: TStringField
      FieldName = 'Phone'
      ReadOnly = True
    end
    object AttachmentsPending: TIntegerField
      FieldName = 'Pending'
      ReadOnly = True
    end
  end
  object HalConfig: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select hal_module,  emails, daysback from hal where hal_module =' +
      ' :hmodule'
    Parameters = <
      item
        Name = 'hmodule'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 118
    Top = 16
  end
  object Assigned_Tickets: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select e.short_name,t.ticket_number,l.client_code,l.status,t.due' +
      '_date,t.ticket_format '#13#10'from locate l with (nolock)'#13#10'inner join ' +
      'employee e on e.emp_id = l.assigned_to_id'#13#10'inner join ticket t o' +
      'n l.ticket_id = t.ticket_id '#13#10'where (l.modified_date between get' +
      'date()- :daysback and getdate()-1)'#13#10'and l.status ='#39'-R'#39' '#13#10'and  l.' +
      'closed = 0'#13#10'and e.active =0'#13#10'and l.active = 1'
    Parameters = <
      item
        Name = 'daysback'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 192
    Top = 72
    object Assigned_Ticketsshort_name: TStringField
      DisplayLabel = 'Short Name'
      FieldName = 'short_name'
      Size = 30
    end
    object Assigned_Ticketsticket_number: TStringField
      DisplayLabel = 'Ticket Nbr'
      FieldName = 'ticket_number'
    end
    object Assigned_Ticketsclient_code: TStringField
      DisplayLabel = 'Client Code'
      FieldName = 'client_code'
      Size = 10
    end
    object Assigned_Ticketsstatus: TStringField
      DisplayLabel = 'Status'
      FieldName = 'status'
      Size = 5
    end
    object Assigned_Ticketsdue_date: TDateTimeField
      DisplayLabel = 'Due Date'
      FieldName = 'due_date'
    end
    object Assigned_Ticketsticket_format: TStringField
      DisplayLabel = 'Ticket Format'
      FieldName = 'ticket_format'
    end
  end
  object dsHalModule: TDataSource
    Left = 279
    Top = 16
  end
  object DataSetTableProducer: TDataSetTableProducer
    MaxRows = -1
    TableAttributes.Align = haCenter
    TableAttributes.CellSpacing = 0
    TableAttributes.CellPadding = 0
    Left = 301
    Top = 72
  end
  object Call_Center: TADODataSet
    Connection = ADOConn
    CommandText = 'Select cc_code from call_center where active = 1'
    Parameters = <>
    Left = 112
    Top = 72
  end
  object ADOConnADS: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=ADsDSOObject;Password=":6r-((f4'#39' + Chr(34) + '#39'Ud$gW.d";' +
      'User ID=dynutil\UTQ-ARS;Encrypt Password=False;Location="LDAP://' +
      'dynutil.com/ou=company,dc=dynutil,dc=com";Mode=Read;Bind Flags=0' +
      ';ADSI Flag=-2147483648'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'ADsDSOObject'
    Left = 30
    Top = 72
  end
  object dsReportTo: TDataSource
    DataSet = ReportTo
    Left = 30
    Top = 204
  end
  object dsMDRInfo: TDataSource
    DataSet = MDRInfo
    Left = 110
    Top = 204
  end
  object dsADActiveUsers: TDataSource
    DataSet = ADActiveUsers
    Left = 193
    Top = 204
  end
  object dsEmpbyCompany: TDataSource
    DataSet = EmpbyCompany
    Left = 293
    Top = 197
  end
  object ReportTo: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select firstname, lastname from [UQVIEW].[dbo].[MDR_Import]'#13#10'whe' +
      're workerid = :ReportToID'
    Parameters = <
      item
        Name = 'ReportToID'
        Attributes = [paSigned, paNullable]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = '888'
      end>
    Left = 30
    Top = 148
    object ReportTofirstname: TStringField
      FieldName = 'firstname'
      Size = 30
    end
    object ReportTolastname: TStringField
      FieldName = 'lastname'
      Size = 30
    end
  end
  object MDRInfo: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select WorkerstatusDescription, ReportToWorkerId'#13#10' from [UQVIEW]' +
      '.[dbo].[MDR_Import]'#13#10'where WorkerNumber = :adplogin'
    Parameters = <
      item
        Name = 'adplogin'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 110
    Top = 148
    object MDRInfoWorkerstatusDescription: TStringField
      DisplayLabel = 'Worker Status'
      DisplayWidth = 30
      FieldName = 'WorkerstatusDescription'
      Size = 100
    end
    object MDRInfoReportToWorkerId: TLargeintField
      DisplayLabel = 'Report To Worker ID'
      FieldName = 'ReportToWorkerId'
    end
  end
  object ADActiveUsers: TADODataSet
    Connection = ADOConnADS
    CommandText = 
      'select lastlogon, lastlogontimestamp, samaccountname, '#13#10'useracco' +
      'untcontrol, givenname, sn, company '#13#10'from LDAP://ou=Sales,dc=MyD' +
      'omain,dc=com'#13#10'where sAMAccountName= :adusername'
    Parameters = <
      item
        Name = 'adusername'
        Size = -1
        Value = Null
      end>
    Left = 193
    Top = 148
  end
  object EmpbyCompany: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select lc.name as comp_name, uqv.timesheet_num as timesheet_num,' +
      ' '#13#10'e.first_name as fn, e.last_name as ln, * from users u '#13#10'inner' +
      ' join employee e on u.emp_id = e.emp_id '#13#10'join locating_company ' +
      'lc on lc.company_id = e.company_id '#13#10'inner join uqview_employees' +
      ' uqv on uqv.employee_number = e.emp_number '#13#10'where u.active_ind ' +
      '= 1 and u.login_id IS NOT NULL '#13#10'and e.ad_username IS NOT NULL a' +
      'nd e.ad_username <> '#39#39' '#13#10'and e.ad_username <> '#39'NULL'#39' and lc.name' +
      ' = :company'#13#10'order by uqv.timesheet_num'
    Parameters = <
      item
        Name = 'company'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = 'Lambert'
      end>
    Left = 285
    Top = 149
  end
  object Companies: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 'select distinct name from locating_company order by name'
    Parameters = <>
    Left = 373
    Top = 149
  end
  object TicketCt: TADODataSet
    Connection = ADOConn
    Parameters = <>
    Left = 40
    Top = 280
  end
  object DamageCt: TADODataSet
    Connection = ADOConn
    Parameters = <>
    Left = 120
    Top = 280
  end
  object WOCt: TADODataSet
    Connection = ADOConn
    Parameters = <>
    Left = 184
    Top = 280
  end
  object InactiveEmps: TADODataSet
    Connection = ADOConn
    CommandText = 
      'select e.emp_id, e.emp_number, e.first_name, e.last_name, e.shor' +
      't_name, '#13#10'     e.type_id, e.timesheet_id, e.active as emp_active' +
      ', e.company_id, u.account_disabled_date from users u'#13#10'     inner' +
      ' join employee e on u.emp_id = e.emp_id where DATEDIFF(D,account' +
      '_disabled_date, GETDATE()) >= 7 '#13#10'     and e.active = 1 order by' +
      ' short_name'
    Parameters = <>
    Left = 48
    Top = 344
  end
  object AreaCt: TADODataSet
    Connection = ADOConn
    Parameters = <>
    Left = 248
    Top = 280
  end
  object dsCompanies: TDataSource
    DataSet = Companies
    Left = 384
    Top = 208
  end
  object dsInactiveEmps: TDataSource
    DataSet = InactiveEmps
    Left = 137
    Top = 342
  end
  object EPR: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select call_center, status, min(insert_date) as oldest, max(inse' +
      'rt_date) as newest, count(*) as count,'#13#10' datediff(d,min(insert_d' +
      'ate),getdate()) as AgeInDays,datediff(hh,min(insert_date),getdat' +
      'e()) as AgeInHours '#13#10'from epr_response group by call_Center, sta' +
      'tus'
    FieldDefs = <
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'status'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'oldest'
        Attributes = [faReadonly, faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'newest'
        Attributes = [faReadonly, faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'count'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end
      item
        Name = 'AgeInDays'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end
      item
        Name = 'AgeInHours'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end>
    Parameters = <>
    StoreDefs = True
    Left = 360
    Top = 16
  end
  object Blank_Locators: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'select locate_id,ticket_id,client_code,status,'#13#10'assigned_to,modi' +
      'fied_date '#13#10'from locate with(nolock)'#13#10'where status not in ('#39'-N'#39',' +
      #39'-P'#39')'#13#10'and closed = 0'#13#10'And active = 1'#13#10'and assigned_to is null'#13#10 +
      'and modified_date >= getdate() - :daysback'
    Parameters = <
      item
        Name = 'daysback'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 408
    Top = 80
    object Blank_Locatorslocate_id: TAutoIncField
      FieldName = 'locate_id'
      ReadOnly = True
    end
    object Blank_Locatorsticket_id: TIntegerField
      FieldName = 'ticket_id'
    end
    object Blank_Locatorsclient_code: TStringField
      FieldName = 'client_code'
      Size = 10
    end
    object Blank_Locatorsstatus: TStringField
      FieldName = 'status'
      Size = 5
    end
    object Blank_Locatorsassigned_to: TIntegerField
      FieldName = 'assigned_to'
    end
    object Blank_Locatorsmodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
  end
  object Ticket_Versions: TADODataSet
    Connection = ADOConn
    CursorType = ctStatic
    CommandText = 
      'Select ticket_id, count(source) As howmany from ticket_version W' +
      'ith (nolock)'#13#10' where cast(transmit_date as date) '#13#10' between getd' +
      'ate() and '#13#10' cast(dateadd(d, :StartDateOffset, getdate()) as dat' +
      'e) '#13#10' group by ticket_id'#13#10' having count(source) > :count'
    Parameters = <
      item
        Name = 'StartDateOffset'
        Size = -1
        Value = Null
      end
      item
        Name = 'count'
        Size = -1
        Value = Null
      end>
    Left = 464
    Top = 144
    object StringField1: TStringField
      DisplayLabel = 'Short Name'
      FieldName = 'short_name'
      Size = 30
    end
    object StringField2: TStringField
      FieldName = 'Phone'
      ReadOnly = True
    end
    object IntegerField1: TIntegerField
      FieldName = 'Pending'
      ReadOnly = True
    end
  end
end
