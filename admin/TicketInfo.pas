unit TicketInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxDropDownEdit,
  cxCheckBox, cxTextEdit, cxCalendar, cxTimeEdit, cxSplitter, Vcl.ComCtrls,
  cxContainer, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  Vcl.StdCtrls, Datasnap.DBClient, dxDateRanges, cxDBExtLookupComboBox,
  cxDataControllerConditionalFormattingRulesManagerDialog, AdminDMu,
  dxScrollbarAnnotations;

type
  TTicketInfoForm = class(TBaseCxListForm)
    TicketInfoPageControl: TPageControl;
    cxSplitter1: TcxSplitter;
    MapQuestionsTabSheet: TTabSheet;
    AssignClientsTabSheet: TTabSheet;
    QuestionTabData: TADODataSet;
    QuestionTabDS: TDataSource;
    QuestionsTab: TTabSheet;
    QuestGroupDetailView: TcxGridDBTableView;
    QuestGroupDetailLevel: TcxGridLevel;
    QuestGroupDetailGrid: TcxGrid;
    GridViewRowNumCol: TcxGridDBColumn;
    GridViewQh_idCol: TcxGridDBColumn;
    GridViewSetDescCol: TcxGridDBColumn;
    GridViewModDateCol: TcxGridDBColumn;
    GridViewActiveCol: TcxGridDBColumn;
    GridViewClientIDCol: TcxGridDBColumn;
    GridViewClientName: TcxGridDBColumn;
    GridViewOCCodeCol: TcxGridDBColumn;
    GridViewCallCenterCol: TcxGridDBColumn;
    GridViewqgd_idCol: TcxGridDBColumn;
    GridViewTStatusCol: TcxGridDBColumn;
    GridViewItemActiveCol: TcxGridDBColumn;
    GridViewqi_idCol: TcxGridDBColumn;
    GridViewQuestionCol: TcxGridDBColumn;
    GridViewQuestionCodeCol: TcxGridDBColumn;
    GridViewFormFieldCol: TcxGridDBColumn;
    ClientAssignmentsGrid: TcxGrid;
    ClientAssignmentsGridDBTableView: TcxGridDBTableView;
    ClientAssignmentsGridLevel: TcxGridLevel;
    Questqi_idCol: TcxGridDBColumn;
    QuestQIDescCol: TcxGridDBColumn;
    QuestInfoTypeCol: TcxGridDBColumn;
    QuestFormFieldCol: TcxGridDBColumn;
    QuestModDateCol: TcxGridDBColumn;
    QuestActiveCol: TcxGridDBColumn;
    QuestHeaderData: TADODataSet;
    QuestHeaderDS: TDataSource;
    AssignClient: TADODataSet;
    AssignClientDS: TDataSource;
    QuestionsPanel: TPanel;
    cxSplitter2: TcxSplitter;
    QuestionHeaderGrid: TcxGrid;
    QHGridView: TcxGridDBTableView;
    QHGridViewqh_id: TcxGridDBColumn;
    QHGridViewqh_description: TcxGridDBColumn;
    QHGridViewmodified_date: TcxGridDBColumn;
    QHGridViewactive: TcxGridDBColumn;
    QHGridLevel: TcxGridLevel;
    QuestionSetsPanel: TPanel;
    QuestionSetsLabel: TLabel;
    AssignPanel: TPanel;
    Panel2: TPanel;
    AssignQuestionsLabel: TLabel;
    ClientGridView: TcxGridDBTableView;
    ClientGridLevel: TcxGridLevel;
    ClientGrid: TcxGrid;
    ClientGridViewclient_id: TcxGridDBColumn;
    ClientGridViewclient_name: TcxGridDBColumn;
    ClientGridViewoc_code: TcxGridDBColumn;
    ClientGridViewmodified_date: TcxGridDBColumn;
    ClientGridViewcall_center: TcxGridDBColumn;
    ClientGridViewutility_type: TcxGridDBColumn;
    AssignClientCommand: TADOCommand;
    ClientGridViewqh_id: TcxGridDBColumn;
    AssignClientclient_id: TAutoIncField;
    AssignClientclient_name: TStringField;
    AssignClientoc_code: TStringField;
    AssignClientmodified_date: TDateTimeField;
    AssignClientcall_center: TStringField;
    AssignClientutility_type: TStringField;
    AssignClientqh_id: TIntegerField;
    AssignClientCalcChecked: TBooleanField;
    ClientGridViewCalcChecked: TcxGridDBColumn;
    QuestGroupDetail: TADODataSet;
    QuestGroupDetailDS: TDataSource;
    QuestGroupDetailViewqgd_id: TcxGridDBColumn;
    QuestGroupDetailViewqh_id: TcxGridDBColumn;
    QuestGroupDetailViewqi_id: TcxGridDBColumn;
    QuestGroupDetailViewtstatus: TcxGridDBColumn;
    QuestGroupDetailViewmodified_date: TcxGridDBColumn;
    QuestGroupDetailViewactive: TcxGridDBColumn;
    QuestionLookup: TADODataSet;
    QuestionLookupDS: TDataSource;
    QuestionLookupqi_id: TAutoIncField;
    QuestionLookupqi_description: TStringField;
    QuestionLookupinfo_type: TStringField;
    QuestionLookupform_field: TStringField;
    TopLabel: TLabel;
    LookupStatus: TADODataSet;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    HeaderTab: TTabSheet;
    HeaderGrid: TcxGrid;
    HeaderGridView: TcxGridDBTableView;
    HeaderGridLevel: TcxGridLevel;
    HeaderGridViewqh_id: TcxGridDBColumn;
    HeaderGridViewqh_description: TcxGridDBColumn;
    HeaderGridViewmodified_date: TcxGridDBColumn;
    HeaderGridViewactive: TcxGridDBColumn;
    QryNextID: TADOQuery;
    ClientGridViewactive: TcxGridDBColumn;
    AssignClientactive: TBooleanField;
    cbAssignClientSearch: TCheckBox;
    ActiveAssignClientFilter: TCheckBox;
    ActiveQuestionsFilter: TCheckBox;
    Panel1: TPanel;
    ActiveQuestionDetailFilter: TCheckBox;
    cbQuestionDetailSearch: TCheckBox;
    Panel3: TPanel;
    ActiveQuestionHeaderFilter: TCheckBox;
    Panel4: TPanel;
    ActiveAllQuestionsFilter: TCheckBox;
    cbAllQuestionsSearch: TCheckBox;
    QuestGroupHeaderLookupView: TcxGridDBTableView;
    QuestHeaderLookupData: TADODataSet;
    QuestHeaderLookupDS: TDataSource;
    QuestGroupHeaderLookupViewqh_id: TcxGridDBColumn;
    QuestGroupHeaderLookupViewdescription: TcxGridDBColumn;
    QuestGroupQuestionLookup: TcxGridDBTableView;
    QuestGroupQuestionLookupdescription: TcxGridDBColumn;
    QuestGroupQuestionLookupinfo_type: TcxGridDBColumn;
    QuestGroupQuestionLookupform_field: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure QHGridViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ClientGridViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure AssignClientCalcFields(DataSet: TDataSet);
    procedure QuestHeaderDSDataChange(Sender: TObject; Field: TField);
    procedure TicketInfoPageControlChange(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure QuestionTabDataBeforePost(DataSet: TDataSet);
    procedure QuestGroupDetailBeforePost(DataSet: TDataSet);
    procedure QuestHeaderDataBeforePost(DataSet: TDataSet);
    procedure cbAssignClientSearchClick(Sender: TObject);
    procedure ActiveAssignClientFilterClick(Sender: TObject);
    procedure ActiveQuestionsFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cbQuestionDetailSearchClick(Sender: TObject);
    procedure ActiveQuestionDetailFilterClick(Sender: TObject);
    procedure ActiveAllQuestionsFilterClick(Sender: TObject);
    procedure cbAllQuestionsSearchClick(Sender: TObject);
    procedure ActiveQuestionHeaderFilterClick(Sender: TObject);
    procedure QuestionTabDataNewRecord(DataSet: TDataSet);
    procedure QuestGroupDetailNewRecord(DataSet: TDataSet);
    procedure QuestHeaderDataNewRecord(DataSet: TDataSet);
    procedure ClientAssignmentsGridExit(Sender: TObject);
    procedure HeaderGridExit(Sender: TObject);
    procedure QuestGroupDetailGridExit(Sender: TObject);
    procedure QuestionTabDataAfterCancel(DataSet: TDataSet);
    procedure QuestionTabDataBeforeInsert(DataSet: TDataSet);
    procedure QuestionTabDataBeforeEdit(DataSet: TDataSet);
    procedure QuestionTabDataAfterPost(DataSet: TDataSet);
    procedure QuestHeaderDataAfterCancel(DataSet: TDataSet);
    procedure QuestHeaderDataBeforeInsert(DataSet: TDataSet);
    procedure QuestHeaderDataBeforeEdit(DataSet: TDataSet);
    procedure QuestHeaderDataAfterPost(DataSet: TDataSet);
    procedure QuestGroupDetailAfterCancel(DataSet: TDataSet);
    procedure QuestGroupDetailBeforeInsert(DataSet: TDataSet);
    procedure QuestGroupDetailBeforeEdit(DataSet: TDataSet);
    procedure QuestGroupDetailAfterPost(DataSet: TDataSet);
  private
    qhArray, qgdArray:  TFieldValuesArray;
    qiInserted, qhInserted, qgdInserted: Boolean;
    procedure LoadValidInfoTypes;
    procedure ChangeQuestionSetLabel;
    procedure ToggleCurrentClientToQH;
    function GetNextID(IDName: string; TableName: string): integer;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure AdjustGridViewOptions;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

const
  ClientAssignmentSQL = 'select qh.*, c.client_id, c.client_name, c.oc_code, c.call_center ' +
                        'from question_header qh left outer join client c ' +
                        'on qh.qh_id = c.qh_id';
var
  TicketInfoForm: TTicketInfoForm;

implementation
uses
  QMConst, OdCxUtils, ConfigDMu, odHourGlass, OdMiscUtils, OdDBUtils;

{$R *.dfm}

{ TTicketInfoForm }

procedure TTicketInfoForm.ActiveAllQuestionsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := QuestionTabData.FieldByName('qi_id').AsInteger;
  QuestionTabData.Close;
  if ActiveAllQuestionsFilter.Checked then
    QuestionTabData.CommandText := 'Select * from question_info where active = 1' else
      QuestionTabData.CommandText := 'Select * from question_info';
  QuestionTabData.Open;
  QuestionTabData.Locate('qi_id', FSelectedID, []);
end;

procedure TTicketInfoForm.ActiveAssignClientFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  ClientSQL: String;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := AssignClient.FieldByName('client_id').AsInteger;
  AssignClient.Close;
  ClientSQL := 'Select client_id, client_name, oc_code, modified_date, ' +
      'call_center, utility_type, qh_id, active from client';
  if ActiveAssignClientFilter.Checked then
    AssignClient.CommandText := ClientSQL + ' where active = 1' else
      AssignClient.CommandText := ClientSQL;
  AssignClient.Open;
  AssignClient.Locate('client_id', FSelectedID, []);
end;

procedure TTicketInfoForm.ActiveQuestionDetailFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := QuestGroupDetail.FieldByName('qgd_id').AsInteger;
  QuestGroupDetail.Close;
  if ActiveQuestionDetailFilter.Checked then
    QuestGroupDetail.CommandText := 'Select * from question_group_detail where active = 1 order by qh_id'
  else
    QuestGroupDetail.CommandText := 'Select * from question_group_detail order by qh_id';
  QuestGroupDetail.Open;
  QuestGroupDetail.Locate('qgd_id', FSelectedID, []);
end;

procedure TTicketInfoForm.ActiveQuestionsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := QuestHeaderData.FieldByName('qh_id').AsInteger;
  QuestHeaderData.Close;
  if ActiveQuestionsFilter.Checked then
    QuestHeaderData.CommandText := 'Select * from question_header where active = 1' else
      QuestHeaderData.CommandText := 'Select * from question_header';
  QuestHeaderData.Open;
  QuestHeaderData.Locate('qh_id', FSelectedID, []);
end;

procedure TTicketInfoForm.ActiveQuestionHeaderFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := QuestHeaderData.FieldByName('qh_id').AsInteger;
  QuestHeaderData.Close;
  if ActiveQuestionHeaderFilter.Checked then
    QuestHeaderData.CommandText := 'Select * from question_header where active = 1' else
      QuestHeaderData.CommandText := 'Select * from question_header';
  QuestHeaderData.Open;
  QuestHeaderData.Locate('qh_id', FSelectedID, []);
end;

procedure TTicketInfoForm.AdjustGridViewOptions;
begin
  GridView.ViewData.Collapse(True);
  GridView.DataController.Filter.Clear;
  //GridView.DataController.Filter.Root.AddItem(GridViewItemActiveCol, foEqual, True, 'True');
  AdminDM.ConfigDM.GetStatusList(CxComboBoxItems(QuestGroupDetailViewtStatus), False);
//  ClientGridViewCalcChecked.Properties.
//  CxCheckBox
end;

procedure TTicketInfoForm.TicketInfoPageControlChange(Sender: TObject);
begin
  inherited;
  if TicketInfoPageControl.ActivePage = MapQuestionsTabSheet then
    TopLabel.Caption := 'Map Questions to Question Set'
  else if TicketInfoPageControl.ActivePage = QuestionsTab then
    TopLabel.Caption := 'Questions'
  else if TicketInfoPageControl.ActivePage = HeaderTab then
    TopLabel.Caption := 'Question Groups'
  else if TicketInfoPageControl.ActivePage = AssignClientsTabSheet  then
    TopLabel.Caption := 'Assign Clients to Question Sets';
end;

procedure TTicketInfoForm.ToggleCurrentClientToQH;
var
  HeaderId: integer;
  ClientId: integer;
  ClientName: String;
  s, qhid: String;
begin
   HeaderId := QuestHeaderData.FieldByName('qh_id').AsInteger;
   ClientId := AssignClient.FieldByName('client_id').AsInteger;
   ClientName :=  AssignClient.FieldByName('client_name').AsString;

   AssignClientCommand.Parameters.ParamByName('ClientID').Value := ClientId;
   //If it is already assigned, then unassign it (toggle)
   if (AssignClient.FieldByName('qh_id').AsInteger = HeaderId) then begin
     AssignClientCommand.Parameters.ParamByName('HeaderID').Value := NULL;
     qhid:= 'null';
   end
   else begin
     AssignClientCommand.Parameters.ParamByName('HeaderID').Value := HeaderId;
     qhid := inttostr(HeaderId);
   end;
   AssignClientCommand.Execute;
   s := 'Question Header: ' +  qhid + ' assigned to client id ' + inttostr(ClientID) + ', client name ' + ClientName;
end;

procedure TTicketInfoForm.AssignClientCalcFields(DataSet: TDataSet);
begin
  inherited;
//  if QuestHeaderData.FieldByName('qh_id').AsInteger = AssignClient.FieldByName('qh_id').AsInteger then
  AssignClient.FieldByName('CalcChecked').AsBoolean :=
    (QuestHeaderData.FieldByName('qh_id').AsInteger = AssignClient.FieldByName('qh_id').AsInteger);
end;

procedure TTicketInfoForm.cbAllQuestionsSearchClick(Sender: TObject);
begin
  inherited;
  if cbAllQuestionsSearch.Checked then
    ClientAssignmentsGridDBTableView.Controller.ShowFindPanel
  else
    ClientAssignmentsGridDBTableView.Controller.HideFindPanel;
end;

procedure TTicketInfoForm.cbAssignClientSearchClick(Sender: TObject);
begin
  inherited;
  if cbAssignClientSearch.Checked then
    ClientGridView.Controller.ShowFindPanel
  else
    ClientGridView.Controller.HideFindPanel;
end;

procedure TTicketInfoForm.cbQuestionDetailSearchClick(Sender: TObject);
begin
  inherited;
  if cbQuestionDetailSearch.Checked then
    QuestGroupDetailView.Controller.ShowFindPanel
  else
    QuestGroupDetailView.Controller.HideFindPanel;
end;

procedure TTicketInfoForm.ChangeQuestionSetLabel;
begin
 if QuestHeaderData.Active then
   AssignQuestionsLabel.Caption := 'Clients Assigned for: ' + '"' + QuestHeaderData.FieldByName('qh_description').AsString + '"';
end;

procedure TTicketInfoForm.ClientGridViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
 RecordMark: TBookMark;
begin
  inherited;
  //Move this...it is not getting called when for the checkbox click, need to manually set event I guess
  if not Self.IsReadOnly then
  begin
    with ACellViewInfo.Item do
      RecordMark := AssignClient.Bookmark;
      ToggleCurrentClientToQH;

      AssignClient.Requery([]);
      AssignClient.GotoBookmark(RecordMark);
    end;
    AHandled := True;
   //    end;
   //  ClientGridView.DataController.RefreshExternalData;
  //  AssignClient.Close;
  //  AssignClient.Open;
end;

procedure TTicketInfoForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
  ActiveCol: TcXGridDBColumn;
begin
  if Sender.Name = 'QHGridView' then
    ActiveCol := QHGridViewActive else
  if Sender.Name = 'ClientGridView' then
    ActiveCol := ClientGridViewActive else
  if Sender.Name = 'QuestGroupDetailView' then
    ActiveCol := QuestGroupDetailViewActive else
  if Sender.Name = 'ClientAssignmentsGridDBTableView' then
    ActiveCol := QuestActiveCol else
  if Sender.Name = 'HeaderGridView' then
    ActiveCol := HeaderGridViewActive;

  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ActiveCol.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TTicketInfoForm.HeaderGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(QuestHeaderData);
end;

procedure TTicketInfoForm.QuestGroupDetailGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(QuestGroupDetail);
end;

procedure TTicketInfoForm.ClientAssignmentsGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(QuestionTabData);
end;

procedure TTicketInfoForm.cxCheckBox1Click(Sender: TObject);
begin
  inherited;
    ToggleCurrentClientToQH;
end;

procedure TTicketInfoForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
     inherited;
end;

procedure TTicketInfoForm.ActivatingNow;
begin
  inherited;
  qiInserted := False;
  qhInserted := False;
  qgdInserted := False;
end;

procedure TTicketInfoForm.FormShow(Sender: TObject);
begin
  inherited;
  LoadValidInfoTypes;
  AdjustGridViewOptions;

end;

function TTicketInfoForm.GetNextID(IDName: string; TableName: string): integer;
const
  //MaxSQL = 'Select max(%s)+1 MaxID FROM %s';
  MaxSQL = 'Select coalesce(max(%s)+1, 1) MaxID FROM %s';
begin
   if QryNextID.Active then
     QrynextID.Close;

   QryNextID.SQL.Clear;
   QryNextID.SQL.Add(Format(MaxSQL, [IDName, TableName]));

   Try
     QryNextID.Open;
     if not QryNextID.EOF then
       Result := QryNextID.FieldByName('MaxID').AsInteger
     else
       Result := 1
   except
     Result := 1;
   end;
end;

procedure TTicketInfoForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TTicketInfoForm.LoadValidInfoTypes;
var
  i : Integer;
begin
  for i := 1 to Length(QuestionFieldTypes) do
    CxComboBoxItems(QuestFormFieldCol).Add(QuestionFieldTypes[i]);
end;

procedure TTicketInfoForm.OpenDataSets;
begin
//  inherited;  (Data Grid not used}
  if not QuestionTabData.Active then QuestionTabData.Open;
  if not QuestHeaderData.Active then QuestHeaderData.Open;
  if not AssignClient.Active then AssignClient.Open;
  if not QuestGroupDetail.Active then QuestGroupDetail.Open;
  if not QuestionLookup.Active then QuestionLookup.Open;
  if not QuestHeaderLookupData.Active then QuestHeaderLookupData.Open;
  TicketInfoPageControlChange(self);
  ChangeQuestionSetLabel;
end;

procedure TTicketInfoForm.CloseDataSets;
begin
  inherited;
  AssignClient.OnCalcFields := nil;
  if QuestionTabData.Active then
    QuestionTabData.Close;
  if QuestHeaderLookupData.Active then QuestHeaderLookupData.Close;
  if QuestHeaderData.Active then
    QuestHeaderData.Close;
  if QuestionLookup.Active then QuestionLookup.Close;
  if QuestGroupDetail.Active then
    QuestGroupDetail.Close;
  if AssignClient.Active then AssignClient.Close;
  AssignClient.OnCalcFields := AssignClientCalcFields;
end;

procedure TTicketInfoForm.QHGridViewCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  ChangeQuestionSetLabel;
end;

procedure TTicketInfoForm.QuestGroupDetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qgdInserted := False;
end;

procedure TTicketInfoForm.QuestGroupDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, qgdArray, 'question_group_detail', 'qgd_id', qgdInserted);
end;

procedure TTicketInfoForm.QuestGroupDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, qgdArray);
end;

procedure TTicketInfoForm.QuestGroupDetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  qgdInserted := True;
end;

procedure TTicketInfoForm.QuestGroupDetailBeforePost(DataSet: TDataSet);
const
  Msg = 'Locate Status, QHeader (qh_id) and QInfo (qi_id) are required fields. ' +
        'Click OK to Continue or Abort to Cancel changes.';
var
  Status: string;
  QHID, QIID: integer;
  NeedID: Boolean;
begin
  inherited;
  Status := TRIM(DataSet.FieldByName('tstatus').AsString);
  QHID := DataSet.FieldByName('qh_id').AsInteger;
  QIID := DataSet.FieldByName('qi_id').AsInteger;
  NeedID := DataSet.FieldByName('qgd_id').IsNull;

  { All required values}
  if (Status <> '') and (QHID > 0) and (QIID > 0) then begin
    EditDataSet(DataSet);
    if NeedID then
      DataSet.FieldByName('qgd_id').AsInteger := GetNextID('qgd_id', 'question_group_detail');
    DataSet.FieldByName('modified_date').AsDateTime := Now;
  end
  else begin{Incomplete}
    {Cancel changes}
    If MessageDlg(Msg, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
    end
    {Continue with changes, but don't try to post}
    else
      Abort;
  end;
end;

procedure TTicketInfoForm.QuestGroupDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TTicketInfoForm.QuestHeaderDataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qhInserted := False;
end;

procedure TTicketInfoForm.QuestHeaderDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, qhArray, 'question_header', 'qh_id', qhInserted);
end;

procedure TTicketInfoForm.QuestHeaderDataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  qhInserted := True;
end;

procedure TTicketInfoForm.QuestHeaderDataBeforePost(DataSet: TDataSet);
const
  Msg = 'Description is a required field. Click OK to Continue or Abort to Cancel changes.';
var
  Desc: string;
  NeedID : Boolean;
begin
  inherited;
  Desc := TRIM(DataSet.FieldByName('qh_description').AsString);
  NeedID := DataSet.FieldByName('qh_id').IsNull;
  if (Desc = '') then begin
    if MessageDlg(Msg, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
      Exit;
    end
    else
      Abort;
  end
  else
    DataSet.FieldByName('modified_date').AsDateTime := Now;
    if NeedID then
      DataSet.FieldByName('qh_id').AsInteger := GetNextID('qh_id','question_header');
end;

procedure TTicketInfoForm.QuestHeaderDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TTicketInfoForm.QuestHeaderDSDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  ChangeQuestionSetLabel;
  If AssignClient.Active then AssignClient.Requery([]);
end;

procedure TTicketInfoForm.QuestionTabDataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qiInserted := False;
end;

procedure TTicketInfoForm.QuestionTabDataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'question_info', 'qi_id', qiInserted);
end;

procedure TTicketInfoForm.QuestionTabDataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TTicketInfoForm.QuestHeaderDataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, qhArray);
end;

procedure TTicketInfoForm.QuestionTabDataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  qiInserted := True;
end;

procedure TTicketInfoForm.QuestionTabDataBeforePost(DataSet: TDataSet);
const
  Msg = 'Description, Info Type and Form Field are required fields. ' +
        'Click OK to Continue or Abort to Cancel changes.';
var
  Desc, IType, FF: string;
  NeedID: Boolean;
begin
  inherited;
  Desc := TRIM(DataSet.FieldByName('qi_description').AsString);
  IType := TRIM(DataSet.FieldByName('info_type').AsString);
  FF := TRIM(DataSet.FieldByName('form_field').AsString);
  NeedID := DataSet.FieldByName('qi_id').IsNull;

  if (Desc = '') and (IType = '') and (FF = '') then begin
    DataSet.ClearFields;
    DataSet.Cancel;
    Abort;
    Exit;
  end;
  { All required values}
  if (Desc <> '') and (IType <> '') and (FF <> '') then begin
    EditDataSet(DataSet);
    if NeedID then
      DataSet.FieldByName('qi_id').AsInteger := GetNextID('qi_id', 'question_info');
    DataSet.FieldByName('modified_date').AsDateTime := Now;

  end
  else begin{Incomplete}
    {Cancel changes}
    If MessageDlg(Msg, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
    end
    {Continue with changes, but don't try to post}
    else
      Abort;
  end;
end;

procedure TTicketInfoForm.QuestionTabDataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TTicketInfoForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbAssignClientSearch.Enabled := True;
  ActiveAssignClientFilter.Enabled := True;
  ActiveQuestionsFilter.Enabled := True;
  ActiveQuestionHeaderFilter.Enabled := True;
  cbQuestionDetailSearch.Enabled := True;
  ActiveQuestionDetailFilter.Enabled := True;
  cbAllQuestionsSearch.Enabled := True;
  ActiveAllQuestionsFilter.Enabled := True;
end;

end.
