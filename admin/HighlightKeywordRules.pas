unit HighlightKeywordRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxDropDownEdit, cxBlobEdit, cxDBExtLookupComboBox,
  cxDBLookupComboBox, cxTextEdit, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  Vcl.StdCtrls, dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  THighlightKeywordRulesForm = class(TBaseCxListForm)
    ColCallCenter: TcxGridDBColumn;
    ColHighlightFieldName: TcxGridDBColumn;
    ColWordRegex: TcxGridDBColumn;
    ColHighlightBold: TcxGridDBColumn;
    ColHighlightColorLookup: TcxGridDBColumn;
    HighlightColorsRef: TADODataSet;
    HighlightColorDS: TDataSource;
    ColRegexModifier: TcxGridDBColumn;
    ColWhichMatch: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    cbHighlightRulesSearch: TCheckBox;
    ActiveHighlightRuleFilter: TCheckBox;
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure ActiveHighlightRuleFilterClick(Sender: TObject);
    procedure cbHighlightRulesSearchClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    FNoColorID: Integer;
    HighlightKeywordInserted: Boolean;
    procedure GetHighlightFieldNameList(List: TStrings);
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  HighlightKeywordRulesForm: THighlightKeywordRulesForm;

implementation

uses AdminDMu, OdCxUtils, OdDbUtils, OdExceptions, OdMiscUtils, JclStrings;

{$R *.dfm}

{ THighlightKeywordRulesForm }

procedure THighlightKeywordRulesForm.OpenDataSets;
begin
  inherited;
  HighlightColorsRef.Open;
  if not HighlightColorsRef.Locate('code', 'none', []) then
    raise Exception.Create('Missing required hlcolor reference data with code = None.');
  FNoColorID := HighlightColorsRef.FieldByName('ref_id').AsInteger;
end;

procedure THighlightKeywordRulesForm.ActivatingNow;
begin
  inherited;
  HighlightKeywordInserted := False;
end;

procedure THighlightKeywordRulesForm.ActiveHighlightRuleFilterClick(
  Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('highlight_rule_id').AsInteger;
  Data.Close;
  if ActiveHighlightRuleFilter.Checked then
    Data.CommandText := 'select * from highlight_rule where active = 1 order by call_center' else
      Data.CommandText := 'select * from highlight_rule order by call_center';
  Data.Open;
  Data.Locate('highlight_rule_id', FSelectedID, []);
end;


procedure THighlightKeywordRulesForm.cbHighlightRulesSearchClick(
  Sender: TObject);
begin
  inherited;
  if cbHighlightRulesSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure THighlightKeywordRulesForm.CloseDataSets;
begin
  HighlightColorsRef.Close;
  inherited;
end;

procedure THighlightKeywordRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  HighlightKeywordInserted := false;
end;

procedure THighlightKeywordRulesForm.DataAfterPost(DataSet: TDataSet);
begin
 inherited;  //QM 847 track reportto changes
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'highlight_rule', 'highlight_rule_id', HighlightKeywordInserted);
end;

procedure THighlightKeywordRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure THighlightKeywordRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;  //QM 847 track reportto changes
  HighlightKeywordInserted := True;
end;

procedure THighlightKeywordRulesForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('HighlightColor', Data, 'highlight_color', HighlightColorsRef, 'ref_id', 'code');
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  Data.FieldByName('HighlightColor').LookupCache := True;
end;

procedure THighlightKeywordRulesForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter), False, True);
  GetHighlightFieldNameList(CxComboBoxItems(ColHighlightFieldName));
end;

procedure THighlightKeywordRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbHighlightRulesSearch.Enabled := True;
  ActiveHighlightRuleFilter.Enabled := True;
end;

procedure THighlightKeywordRulesForm.DataBeforePost(DataSet: TDataSet);
var
  OpenParenCount: Integer;
  RegExMod: string;
begin
  inherited;
  OpenParenCount := StrCharCount(DataSet.FieldByName('word_regex').AsString, '(');
  if (DataSet.FieldByName('which_match').AsInteger > OpenParenCount) then
    raise EOdDataEntryError.CreateFmt('The Which Match value must be less than or equal to %d for the current Word Regex.',
      [OpenParenCount]);

  if IsEmpty(DataSet.FieldByName('word_regex').AsString) then
    raise EOdDataEntryError.Create('A value for Word Regex is required.');

  if (not DataSet.FieldByName('highlight_bold').AsBoolean) and
    SameText(DataSet.FieldByName('HighlightColor').AsString, 'None') then
    raise EOdDataEntryError.Create('A rule must have at least a color or bold highlight.');

  RegExMod := DataSet.FieldByName('regex_modifier').AsString;
  if NotEmpty(RegExMod) then
    if not (StrIsSubset(RegExMod, ['i','m','s','x','A'])) then
      raise EOdDataEntryError.Create('Regex Modifier can only be blank or a subset of these characters: imsxA.');
end;

procedure THighlightKeywordRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('highlight_color').Value := FNoColorID;
  DataSet.FieldByName('active').Value := True;
  DataSet.FieldByName('highlight_bold').Value := False;
end;

procedure THighlightKeywordRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure THighlightKeywordRulesForm.GetHighlightFieldNameList(List: TStrings);
begin
//For now, hardcode the field name list until more fields are supported in the client
  Assert(Assigned(List));
  List.Clear;
  List.Add('work_description');
  List.Add('work_address');
  List.Add('work_remarks');
end;

procedure THighlightKeywordRulesForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure THighlightKeywordRulesForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

end.
