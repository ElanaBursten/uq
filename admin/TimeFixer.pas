unit TimeFixer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, DB, ADODB, ExtCtrls;

type
  TTimeFixerForm = class(TEmbeddableForm)
    Tix: TADODataSet;
    Memo1: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    TestParsing: TButton;
    Updater: TADOCommand;
    StopButton: TButton;
    procedure Button1Click(Sender: TObject);
    procedure TestParsingClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
  private
    FStop: Boolean;
    function ParseDate(Image: string): TDateTime;
    procedure CheckEquals(Expected: TDateTime; Actual: TDateTime);
    procedure Log(Msg: string);
  end;

implementation

uses AdminDMu, OdIsoDates;

{$R *.dfm}

procedure TTimeFixerForm.Button1Click(Sender: TObject);
var
  Image: string;
  TransmitDate: TDateTime;
  NewDate: TDateTime;
  Cnt: Integer;
begin
  inherited;
  Cnt := 1;
  Updater.Prepared;
  FStop := False;
  with Tix do begin
    Open;
    while (not EOF) {and (Cnt<100)} and (not FStop) do begin
      Application.ProcessMessages;
      Image := FieldByName('image').AsString;
      TransmitDate := FieldByName('Transmit_Date').AsDateTime;

      NewDate := ParseDate(Image);
      if NewDate<>TransmitDate then begin
        Log(IntToStr(Cnt) + '   Was: ' + DateTimeToStr(TransmitDate) + '  Changing to: ' + DateTimeToStr(NewDate));

        Updater.Parameters[0].Value := NewDate;
        Updater.Parameters[1].Value := FieldByName('ticket_id').AsInteger;
        Updater.Execute;
      end;

      Next;
      Inc(Cnt);
    end;
    Close;
  end;
end;

procedure TTimeFixerForm.CheckEquals(Expected, Actual: TDateTime);
begin
  if Expected <> Actual then
    raise Exception.Create('Expected: ' + DateTimeToStr(Expected) + ',  got ' +DateTimeToStr(Actual));
end;

procedure TTimeFixerForm.Log(Msg: string);
begin
  Memo1.Lines.Add(Msg);
end;

function TTimeFixerForm.ParseDate(Image: string): TDateTime;
var
  p: Integer;
  DateStr: string;
  TimeStr: string;
begin
  p := Pos('Transmit:  Date: ', Image);
  if p=0 then
    raise Exception.Create('Could not find date in image');

  DateStr := Copy(Image, p+17, 8);
  TimeStr := Copy(Image, p+32, 4);

  Result := StrToDate(DateStr) + EncodeTime(StrToInt(Copy(TimeStr,1,2)), StrToInt(Copy(TimeStr,3,2)), 0,0)
end;

procedure TTimeFixerForm.TestParsingClick(Sender: TObject);
begin
  inherited;
  CheckEquals(IsoStrToDateTime('2002-03-26T12:23:00'), ParseDate('XXXX   Transmit:  Date: 03/26/02   At: 1223   '));
  CheckEquals(IsoStrToDateTime('2002-04-01T09:12:00'), ParseDate('XXXX   Transmit:  Date: 04/01/02   At: 0912   '));
  CheckEquals(IsoStrToDateTime('2002-04-01T22:12:00'), ParseDate('XXXX   Transmit:  Date: 04/01/02   At: 2212   '));
  ShowMessage('All Tests Passed');
end;

procedure TTimeFixerForm.StopButtonClick(Sender: TObject);
begin
  inherited;
  FStop:=True;
end;

end.

