inherited HighlightKeywordRulesForm: THighlightKeywordRulesForm
  Caption = 'Highlight Keyword Rules'
  ClientWidth = 853
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 853
    Height = 28
    object cbHighlightRulesSearch: TCheckBox
      Left = 19
      Top = 7
      Width = 97
      Height = 15
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbHighlightRulesSearchClick
    end
    object ActiveHighlightRuleFilter: TCheckBox
      Left = 151
      Top = 7
      Width = 169
      Height = 14
      Caption = 'Active Highlight Rules Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveHighlightRuleFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 853
    Height = 372
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'highlight_rule_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 18
        Properties.MaxLength = 20
        Width = 132
      end
      object ColHighlightFieldName: TcxGridDBColumn
        Caption = 'Ticket Field Name'
        DataBinding.FieldName = 'field_name'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 18
        Properties.MaxLength = 50
        Width = 125
      end
      object ColWordRegex: TcxGridDBColumn
        Caption = 'Word Regex'
        DataBinding.FieldName = 'word_regex'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.BlobPaintStyle = bpsText
        Properties.MemoMaxLength = 300
        Properties.MemoScrollBars = ssVertical
        Properties.MemoWantReturns = False
        Properties.MemoWantTabs = False
        Properties.PopupHeight = 140
        Properties.PopupWidth = 500
        Properties.ShowExPopupItems = False
        Properties.ShowPicturePopup = False
        Width = 235
      end
      object ColRegexModifier: TcxGridDBColumn
        Caption = 'Regex Modifier'
        DataBinding.FieldName = 'regex_modifier'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 88
      end
      object ColWhichMatch: TcxGridDBColumn
        Caption = 'Which Match?'
        DataBinding.FieldName = 'which_match'
        Width = 85
      end
      object ColHighlightBold: TcxGridDBColumn
        Caption = 'Bold?'
        DataBinding.FieldName = 'highlight_bold'
        Width = 50
      end
      object ColHighlightColorLookup: TcxGridDBColumn
        Caption = 'Color'
        DataBinding.FieldName = 'HighlightColor'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ClearKey = 46
        Properties.DropDownRows = 20
        Properties.DropDownWidth = 100
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.ListColumns = <>
        Properties.ListOptions.ShowHeader = False
        Width = 67
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Options.Filtering = False
        Width = 67
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 
      'select * from highlight_rule where active = 1 order by call_cent' +
      'er'
  end
  object HighlightColorsRef: TADODataSet
    CacheSize = 3
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, code, description from reference where type='#39'hlco' +
      'lor'#39' and active_ind=1 order by description'
    Parameters = <>
    Left = 64
    Top = 177
  end
  object HighlightColorDS: TDataSource
    DataSet = HighlightColorsRef
    Left = 168
    Top = 176
  end
end
