object ComputerAssetsForm: TComputerAssetsForm
  Left = 0
  Top = 0
  Caption = 'Computer Assets'
  ClientHeight = 269
  ClientWidth = 636
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblWorkOrders: TLabel
    Left = 12
    Top = 17
    Width = 82
    Height = 13
    Caption = 'Computer Assets'
  end
  object OkBtn: TButton
    Left = 275
    Top = 231
    Width = 75
    Height = 29
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object cxGrid1: TcxGrid
    Left = 8
    Top = 38
    Width = 620
    Height = 179
    TabOrder = 1
    object GridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsComputerInfo
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object GridViewColumn1: TcxGridDBColumn
        Caption = 'Computer Serial'
        DataBinding.FieldName = 'computer_serial'
        Width = 143
      end
      object GridViewColumn2: TcxGridDBColumn
        Caption = 'Computer Model'
        DataBinding.FieldName = 'computer_model'
        Width = 118
      end
      object GridViewColumn3: TcxGridDBColumn
        Caption = 'Asset Tag'
        DataBinding.FieldName = 'asset_tag'
        Width = 116
      end
      object GridViewColumn4: TcxGridDBColumn
        Caption = 'Aircard Phone nbr'
        DataBinding.FieldName = 'aircard_phone_no'
        Width = 120
      end
      object GridViewColumn5: TcxGridDBColumn
        Caption = 'Aircard Phone esn'
        DataBinding.FieldName = 'aircard_phone_esn'
        Width = 121
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridView
    end
  end
  object qryComputerInfo: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'empid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 11623
      end>
    SQL.Strings = (
      'select distinct computer_serial, computer_model, '
      'asset_tag, aircard_phone_no, aircard_phone_esn '
      'from computer_info'
      'where emp_id = :empid'
      'order by computer_model')
    Left = 400
    Top = 88
  end
  object dsComputerInfo: TDataSource
    DataSet = qryComputerInfo
    Left = 304
    Top = 80
  end
end
