inherited ReferenceForm: TReferenceForm
  Caption = 'Reference / Lookup Tables'
  ClientHeight = 334
  ClientWidth = 666
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 666
    Height = 26
    object chkboxReferenceSearch: TCheckBox
      Left = 26
      Top = 6
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxReferenceSearchClick
    end
    object ActiveReferencesFilter: TCheckBox
      Left = 150
      Top = 6
      Width = 155
      Height = 13
      Caption = 'Active References Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveReferencesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 666
    Height = 308
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Hint = 'Because of dependancies, Ref deletions take a long time'
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'ref_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object Gridtype: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 15
        Width = 80
      end
      object Gridcode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 127
      end
      object GridCodeLength: TcxGridDBColumn
        Caption = 'Code Len.'
        DataBinding.FieldName = 'CodeLength'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 64
      end
      object Griddescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 226
      end
      object Gridsortby: TcxGridDBColumn
        Caption = 'Sort Order'
        DataBinding.FieldName = 'sortby'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        HeaderAlignmentHorz = taRightJustify
        Width = 59
      end
      object Gridmodifier: TcxGridDBColumn
        Caption = 'Modifier'
        DataBinding.FieldName = 'modifier'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 81
      end
      object Gridactive_ind: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active_ind'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 43
      end
      object Gridref_id: TcxGridDBColumn
        Caption = 'Ref ID'
        DataBinding.FieldName = 'ref_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 47
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnCalcFields = DataCalcFields
    OnNewRecord = DataNewRecord
    CommandText = 
      'select * from reference where active_ind = 1  '#13#10'order by type, c' +
      'ode, description'
    object Dataref_id: TAutoIncField
      FieldName = 'ref_id'
      ReadOnly = True
    end
    object Datatype: TStringField
      FieldName = 'type'
      Size = 10
    end
    object Datacode: TStringField
      FieldName = 'code'
      Size = 15
    end
    object Datadescription: TStringField
      FieldName = 'description'
      Size = 100
    end
    object Datasortby: TIntegerField
      FieldName = 'sortby'
    end
    object Dataactive_ind: TBooleanField
      FieldName = 'active_ind'
    end
    object Datamodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object Datamodifier: TStringField
      FieldName = 'modifier'
    end
    object DataCodeLength: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CodeLength'
      Calculated = True
    end
  end
  object TypeList: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandText = 'select distinct type from reference'
    Parameters = <>
    Left = 56
    Top = 168
  end
end
