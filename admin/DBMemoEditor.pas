unit DBMemoEditor;    //QMANTWO-173

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TmemoEditorForm = class(TForm)     //QMANTWO-173
    DBMemoEditor: TMemo;
    Panel1: TPanel;
    btnSave: TBitBtn;
    btnCancel: TBitBtn;
    btnClear: TBitBtn;
    procedure btnClearClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  memoEditorForm: TmemoEditorForm;     //QMANTWO-173

implementation

{$R *.dfm}

procedure TmemoEditorForm.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TmemoEditorForm.btnClearClick(Sender: TObject);
begin
  DBMemoEditor.Clear;
end;

end.
