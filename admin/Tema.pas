unit Tema;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Vcl.ExtCtrls,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.ComCtrls,
  Data.Win.ADODB, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, AdminDmu,
  cxCalendar, OdDbUtils;

type
  TTemaForm = class(TBaseCxListForm)
    PC: TPageControl;
    TemaDetailsSheet: TTabSheet;
    TemaHeaderSheet: TTabSheet;
    TemaFieldTypeSheet: TTabSheet;
    FieldTypeGrid: TcxGrid;
    FieldTypeGridView: TcxGridDBTableView;
    FieldTypeGridLevel: TcxGridLevel;
    HeaderGrid: TcxGrid;
    HeaderGridView: TcxGridDBTableView;
    HeaderGridLevel: TcxGridLevel;
    DetailGrid: TcxGrid;
    DetailGridView: TcxGridDBTableView;
    DetailGridLevel: TcxGridLevel;
    Splitter1: TSplitter;
    ColFormName: TcxGridDBColumn;
    ColFormDescription: TcxGridDBColumn;
    ColCalCenter: TcxGridDBColumn;
    Coludf00: TcxGridDBColumn;
    Coludf01: TcxGridDBColumn;
    Coludf02: TcxGridDBColumn;
    Coludf03: TcxGridDBColumn;
    Coludf04: TcxGridDBColumn;
    Coludf05: TcxGridDBColumn;
    Coludf06: TcxGridDBColumn;
    Coludf07: TcxGridDBColumn;
    Coludf08: TcxGridDBColumn;
    Coludf09: TcxGridDBColumn;
    Coludf10: TcxGridDBColumn;
    Coludf11: TcxGridDBColumn;
    Coludf12: TcxGridDBColumn;
    Coludf13: TcxGridDBColumn;
    TemaDetails: TADODataSet;
    dsTemaDetails: TDataSource;
    ColDetailudf00: TcxGridDBColumn;
    ColDetailudf01: TcxGridDBColumn;
    ColDetailudf02: TcxGridDBColumn;
    ColDetailudf03: TcxGridDBColumn;
    ColDetailudf04: TcxGridDBColumn;
    ColDetailudf05: TcxGridDBColumn;
    ColDetailudf06: TcxGridDBColumn;
    ColDetailudf07: TcxGridDBColumn;
    ColDetailudf08: TcxGridDBColumn;
    ColDetailudf09: TcxGridDBColumn;
    ColDetailudf10: TcxGridDBColumn;
    ColDetailudf11: TcxGridDBColumn;
    ColDetailudf12: TcxGridDBColumn;
    ColDetailudf13: TcxGridDBColumn;
    TemaHeader: TADODataSet;
    dsTemaHeader: TDataSource;
    cxGridColFormName: TcxGridDBColumn;
    ColTicketID: TcxGridDBColumn;
    ColEmpID: TcxGridDBColumn;
    ColInsertDate: TcxGridDBColumn;
    TemaFieldType: TADODataSet;
    dsTemaFieldType: TDataSource;
    ColFieldTypeFormName: TcxGridDBColumn;
    ColFieldTypeudf00: TcxGridDBColumn;
    ColFieldTypeudf01: TcxGridDBColumn;
    ColFieldTypeudf02: TcxGridDBColumn;
    ColFieldTypeudf03: TcxGridDBColumn;
    ColFieldTypeudf04: TcxGridDBColumn;
    ColFieldTypeudf05: TcxGridDBColumn;
    ColFieldTypeudf06: TcxGridDBColumn;
    ColFieldTypeudf07: TcxGridDBColumn;
    ColFieldTypeudf08: TcxGridDBColumn;
    ColFieldTypeudf09: TcxGridDBColumn;
    ColFieldTypeudf10: TcxGridDBColumn;
    ColFieldTypeudf11: TcxGridDBColumn;
    ColFieldTypeudf12: TcxGridDBColumn;
    ColFieldTypeudf13: TcxGridDBColumn;
    procedure FieldTypeGridExit(Sender: TObject);
    procedure HeaderGridExit(Sender: TObject);
    procedure DetailGridExit(Sender: TObject);
    procedure TemaDetailsBeforeEdit(DataSet: TDataSet);
    procedure TemaDetailsAfterPost(DataSet: TDataSet);
    procedure TemaDetailsBeforeInsert(DataSet: TDataSet);
    procedure TemaDetailsAfterCancel(DataSet: TDataSet);
    procedure TemaFieldTypeBeforeEdit(DataSet: TDataSet);
    procedure TemaFieldTypeAfterCancel(DataSet: TDataSet);
    procedure TemaFieldTypeBeforeInsert(DataSet: TDataSet);
    procedure TemaFieldTypeAfterPost(DataSet: TDataSet);
    procedure TemaHeaderAfterCancel(DataSet: TDataSet);
    procedure TemaHeaderBeforeInsert(DataSet: TDataSet);
    procedure TemaHeaderBeforeEdit(DataSet: TDataSet);
    procedure TemaHeaderAfterPost(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    ADetailsArray: TFieldValuesArray;
    AFieldTypeArray: TFieldValuesArray;
    AHeaderArray: TFieldValuesArray;
    TemaFormIns: Boolean;
    DetailsIns: Boolean;
    FieldTypeIns: Boolean;
    HeaderIns: Boolean;
  public
    { Public declarations }
     procedure OpenDataSets; override;
     procedure CloseDataSets; override;
     procedure LeavingNow; override;
     procedure EditNow; override;
     procedure ActivatingNow; override;
  end;

var
  TemaForm: TTemaForm;

implementation

{$R *.dfm}

{ TBaseCxListForm2 }

procedure TTemaForm.ActivatingNow;
begin
  inherited;
  DetailsIns := False;
  FieldTypeIns := False;
  HeaderIns := False;
  TemaFormIns := False;
end;

procedure TTemaForm.CloseDataSets;
begin
  inherited;
  TemaDetails.Close;
  TemaHeader.Close;
  TemaFieldType.Close;
end;

procedure TTemaForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  TemaFormIns := False;
end;

procedure TTemaForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(DataSet, ATableArray, 'tema_form', 'definition_id', TemaFormIns);
end;

procedure TTemaForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-823
end;

procedure TTemaForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  TemaFormIns := True;
end;

procedure TTemaForm.DetailGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(TemaDetails);
end;

procedure TTemaForm.EditNow;
begin
  inherited;
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TTemaForm.FieldTypeGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(TemaFieldType);
end;

procedure TTemaForm.HeaderGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(TemaHeader);
end;

procedure TTemaForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TTemaForm.OpenDataSets;
begin
  TemaDetails.Open;
  TemaHeader.Open;
  TemaFieldType.Open;
  inherited;
end;

procedure TTemaForm.TemaDetailsAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DetailsIns := False;
end;

procedure TTemaForm.TemaDetailsAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, ADetailsArray, 'tema_details', 'header_id', DetailsIns);
end;

procedure TTemaForm.TemaDetailsBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, ADetailsArray); //QM-823
end;

procedure TTemaForm.TemaDetailsBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  DetailsIns := True;
end;

procedure TTemaForm.TemaFieldTypeAfterCancel(DataSet: TDataSet);
begin
  inherited;
  FieldTypeIns := False;
end;

procedure TTemaForm.TemaFieldTypeAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, AFieldTypeArray, 'tema_fieldtype', 'definition_id', FieldTypeIns);
end;

procedure TTemaForm.TemaFieldTypeBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, AFieldTypeArray); //QM-823
end;

procedure TTemaForm.TemaFieldTypeBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  FieldTypeIns := True;
end;

procedure TTemaForm.TemaHeaderAfterCancel(DataSet: TDataSet);
begin
  inherited;
  HeaderIns := False;
end;

procedure TTemaForm.TemaHeaderAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, AHeaderArray, 'tema_header', 'header_id', HeaderIns);
end;

procedure TTemaForm.TemaHeaderBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, AHeaderArray); //QM-823
end;

procedure TTemaForm.TemaHeaderBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  HeaderIns := True;
end;

end.
