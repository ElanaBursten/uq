inherited AssetTypeForm: TAssetTypeForm
  Caption = 'Asset Types'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 24
    object ActiveAssetTypeFilter: TCheckBox
      Left = 12
      Top = 5
      Width = 149
      Height = 14
      Caption = 'Active Alert Type Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveAssetTypeFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 24
    Height = 376
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'asset_code'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColAssetCode: TcxGridDBColumn
        Caption = 'Asset Code'
        DataBinding.FieldName = 'asset_code'
        Width = 129
      end
      object ColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        Width = 338
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Options.Filtering = False
        Width = 76
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from asset_type where active = 1'
  end
end
