unit EmployeeBalance;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, AdminDmu, strUtils, Inifiles;

type
  TEmployeeBalanceForm = class(TBaseCxListForm)
    FileADOConnection: TADOConnection;
    OpenDialog1: TOpenDialog;
    btnOpen: TButton;
    qryEmployeeBalance: TADOQuery;
    dsAEmployeeBalance: TDataSource;
    qryEmployeeBalanceCompanyCode: TWideStringField;
    qryEmployeeBalanceEmployeeNumber: TWideStringField;
    qryEmployeeBalanceFirstName: TWideStringField;
    qryEmployeeBalanceLastName: TWideStringField;
    qryEmployeeBalanceEmploymentStatus: TWideStringField;
    qryEmployeeBalanceSICKOption: TWideStringField;
    qryEmployeeBalanceSICKBalance: TFloatField;
    qryEmployeeBalancePTOOption: TWideStringField;
    qryEmployeeBalancePTOBalance: TFloatField;
    qryEmployeeBalancePTOAccrual: TFloatField;
    qryEmployeeBalanceWeeksRemaining: TFloatField;
    qryEmployeeBalanceAccrualtoEOY: TFloatField;
    qryEmployeeBalanceEstBalatEOY: TFloatField;
    qryEmployeeBalanceSupervisorNameFirstMILastSuffix: TWideStringField;
    qryEmployeeBalanceEmailSupervisor: TWideStringField;
    qryEmpid: TADOQuery;
    GridViewCompany_Code: TcxGridDBColumn;
    GridViewEmployee_Number: TcxGridDBColumn;
    GridViewemp_id: TcxGridDBColumn;
    GridViewFirst_Name: TcxGridDBColumn;
    GridViewLast_Name: TcxGridDBColumn;
    GridViewEmployment_Status: TcxGridDBColumn;
    GridViewSICK_Option: TcxGridDBColumn;
    GridViewSICK_Balance: TcxGridDBColumn;
    GridViewPTO_Option: TcxGridDBColumn;
    GridViewPTO_Balance: TcxGridDBColumn;
    GridViewPTO_Accrual: TcxGridDBColumn;
    GridViewWeeksRemaining: TcxGridDBColumn;
    GridViewAccrual_to_EOY: TcxGridDBColumn;
    GridViewEst_Bal_at_EOY: TcxGridDBColumn;
    GridViewSupervisor_Name: TcxGridDBColumn;
    GridViewEMail_Sup: TcxGridDBColumn;
    procedure btnOpenClick(Sender: TObject);
  private
    AirwatchPath:string;
    procedure OpenFile;
    procedure ImportExcelData;
    function LeftPad(value: string; length: integer=6; pad: char='0'): string;
    { Private declarations }
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  EmployeeBalanceForm: TEmployeeBalanceForm;

implementation

{$R *.dfm}

{ TFormEmployeeBalance }

procedure TEmployeeBalanceForm.btnOpenClick(Sender: TObject);
begin
  inherited;
  OpenFile;
 { try
    OpenFile;
  except
    on E:Exception do
    begin
      Msg :=  'connection to file error: ' + E.Message;
      raise Exception.Create(Msg);
    end;
  end; }
end;

{begin
  if dlgOpen_FileSelect.Execute then
  begin
    if dlgOpen_FileSelect.FileName<>'' then
    begin
      FilePath:=dlgOpen_FileSelect.FileName;
      Edit1.Text:=FilePath;
    end
    else
      ShowMessage('Please select the correct file');
  end;   }

function TEmployeeBalanceForm.LeftPad(value: string; length: integer=6; pad: char='0'): string;
begin
  result := RightStr(StringOfChar(pad,length) + value, length );
end;

procedure TEmployeeBalanceForm.OpenFile;
var
  Connstr: String;
  searchResult : TSearchRec;
  SheetList: TStringList;
  FilePath: TFileName;
  SheetName: String;
  I: Integer;
begin
  Connstr := 'Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0;Data Source=';
   openDialog1.Filter := 'Excel files|*.xlsx';
   if OpenDialog1.Execute then
   begin
     If OpenDialog1.FileName<>'' then
       FilePath:= OpenDialog1.FileName
     else
      ShowMessage('Please select the correct file');
   end;
    FileADOConnection.Close;
    Connstr := Connstr +
    'C:\SickLeave\PTO and Sick Balance w Annual Estimate for UTQ & LOC Week 18 Through 04 30 2022.xlsx'; //OpenDialog1.FileName;
    try
      FileADOConnection.ConnectionString := Connstr;
      FileADOConnection.Open;
      SheetList := TStringlist.Create;
      FileADOConnection.GetTableNames(SheetList);
      SheetName := '';
      for I := 0 to SheetList.Count-1 do
      //Excel will ignore the ampersand
       if SheetList[I] = '''UTQ & LOC$''' then
       begin
        SheetName := SheetList[I];
        break;
       end;
       if SheetName = '' then
        raise Exception.Create('Excel Sheet "UT & LOC" not found');
      qryEmployeeBalance.SQL.Text := 'Select * from [' + SheetName + ']';
      qryEmployeeBalance.Open;
      SheetList.Free;

       //First clear the employee_balance table
      Data.Connection.Execute('delete from employee_balance');

      ImportExcelData;

       Data.CommandText :='select * from employee_balance';
       Data.Open;

    except
      raise;
    end;
  end;

procedure TEmployeeBalanceForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  btnOpen.Enabled := True;
end;

procedure TEmployeeBalanceForm.ImportExcelData;
var
  I: Integer;
  EmpNumber: String;
begin
  //Import the EXCEL data into the table
  if qryEmployeeBalance.RecordCount>0 then
  begin

    qryEmployeeBalance.First;
    for I := 0 to qryEmployeeBalance.RecordCount-1 do
    begin
      Data.Append;
      EmpNumber := qryEmployeeBalance.FieldValues['Employee Number'];
      EmpNumber := LeftPad(empnumber);
      qryEmpid.Parameters.ParamByName('empnumber').Value := EmpNumber;
      qryEmpId.Open;
      if not qryEmpId.EOF then
        Data.FieldValues['emp_id'] := qryEmpID.FieldByName('emp_id').AsInteger
      else
      begin
        Data.Cancel;
        Continue;
      end;
      qryEmpId.Close;
      Data.FieldValues['Employee_Number'] := EmpNumber;
      Data.FieldValues['Company_Code'] := qryEmployeeBalance.FieldValues['Company Code'];
      Data.FieldValues['First_Name'] := qryEmployeeBalance.FieldValues['First Name'];
      Data.FieldValues['Last_Name'] := qryEmployeeBalance.FieldValues['Last Name'];
      Data.FieldValues['Employment_Status'] := qryEmployeeBalance.FieldValues['Employment Status'];
      Data.FieldValues['SICK_Option'] := qryEmployeeBalance.FieldValues['SICK Option'];
      Data.FieldValues['SICK_Balance'] := qryEmployeeBalance.FieldValues['SICK Balance'];
      Data.FieldValues['PTO_Option'] := qryEmployeeBalance.FieldValues['PTO Option'];
      Data.FieldValues['PTO_Balance'] := qryEmployeeBalance.FieldValues['PTO Balance'];
      Data.FieldValues['PTO_Accrual'] := qryEmployeeBalance.FieldValues['PTO Accrual'];
      Data.FieldValues['WeeksRemaining'] := qryEmployeeBalance.FieldValues['WeeksRemaining'];
      Data.FieldValues['Accrual_to_EOY'] := qryEmployeeBalance.FieldValues['Accrual to EOY'];
      Data.FieldValues['Est_Bal_at_EOY'] := qryEmployeeBalance.FieldValues['Est Bal at EOY'];
      Data.FieldValues['Supervisor_Name'] := qryEmployeeBalance.FieldValues['Supervisor Name (First MI Last Suffix)'];
      Data.FieldValues['Email_Sup'] := qryEmployeeBalance.FieldValues['Email (Supervisor)'];
      Data.Post;

      qryEmployeeBalance.Next;
    end;
  end;
end;


procedure TEmployeeBalanceForm.OpenDataSets;
begin
  inherited;
end;

end.
