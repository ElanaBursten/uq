unit ExtractEmpData;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB,
  Vcl.Grids, Vcl.DBGrids, Inifiles, Vcl.ExtCtrls, AdminDMu, MainFU, OdEmbeddable;

type
  TExtractEmpForm = class(TEmbeddableForm)
    qryEmpData: TADOQuery;
    ExtractEmpMemo: TMemo;
    Panel1: TPanel;
    btnExtractEmpData: TButton;
    procedure btnExtractEmpDataClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    InputPath, OutputPath, ErrorPath, InputFileName, OutputFilePath, ErrorFilePath: String;
    function extractempid(line: string): string;
    procedure LogErrors(Filename: TFilename; ErrMsg: String);
    procedure ProcessFile;
    function IsReadOnly: Boolean; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ExtractEmpForm: TExtractEmpForm;

implementation

{$R *.dfm}

procedure TExtractEmpForm.ProcessFile;
var
  MyFile: TextFile;
  OutputFile: Textfile;
  openDialog : TOpenDialog;
  line: String;
  Outputline: String;
  empid: Integer;
  ErrMsg: String;
begin
   openDialog := TOpenDialog.Create(self);
   if DirectoryExists(InputPath) then
     openDialog.InitialDir := InputPath
   else
     OpenDialog.InitialDir := ExtractFilePath(Application.ExeName);
   openDialog.Options := [ofFileMustExist];
   openDialog.Filter := 'Employee List Files|*.txt';
   if openDialog.Execute then
   begin
     try
       InputFileName := ExtractFileName(OpenDialog.FileName);
       OutputFilePath := OutputPath + ExtractFileName(ChangeFileExt(InputFileName, '')) + '_Output.txt';
       ErrorFilePath := ErrorPath + ExtractFileName(ChangeFileExt(InputFileName, '')) + '.log';
       AssignFile(myFile,OpenDialog.FileName);
       Reset(myFile);
       if Eof(myFile) then
       begin
         ErrMsg := format('file %s is empty', [InputFileName]);
         ShowMessage(ErrMsg);
         LogErrors(ErrorFilePath,ErrMsg);
         CloseFile(myFile);
         Exit;
       end;
       AssignFile(OutputFile, OutputFilePath);
       Rewrite(OutputFile);
       OutputLine := 'EmpNumber ShortName LocatingCo EmpType ADName Active';
       ExtractEmpMemo.Lines.Add(OutputLine);
       Writeln(OutputFile, OutputLine );
       Readln(myFile);
       while not EOF(myFile) do
       begin
         Readln(MyFile, line);
         empid := StrtoIntDef(extractempid(line), 0);
         if empid = 0 then
         begin
           ErrMsg := format( 'Error extracting employee id %', [empid]);
           LogErrors(ErrorFilePath,ErrMsg);
           ShowMessage(ErrMsg)
         end
         else
         begin
           qryEmpData.Parameters.ParamByName('emp_id').Value := empid;
           qryEmpData.Open;
           if qryEmpData.Eof then
           begin
             ErrMsg := format('emp id %d not found', [empid]);
             LogErrors(ErrorFilePath,ErrMsg);
             ShowMessage(ErrMsg);
           end
           else
           begin
             OutputLine :=
             qryEmpData.FieldByName('emp_number').AsString + ' ' +
             qryEmpData.FieldByName('short_name').AsString + ' ' +
             qryEmpData.FieldByName('company').AsString + ' ' +
             qryEmpData.FieldByName('code').AsString + ' ' +
             qryEmpData.FieldByName('ad_username').AsString + ' ' +
             qryEmpData.FieldByName('active').AsString;
             ExtractEmpMemo.Lines.Add(OutputLine);
             Writeln(OutputFile, OutputLine);
           end;
           qryEmpData.Close;
         end
       end;
     finally
      CloseFile(myFile);
      CloseFile(OutputFile);
      openDialog.Free;
     end;
   end;
end;

function TExtractEmpForm.extractempid(line: string): string;
const
  n = ['0'..'9'];
var
  i: integer;
  extractnumbers: String;
begin
  i := 1;
  extractnumbers :='';
  while  (i < length(line)) and (line[i] <> ' ') do
     inc(i);
  while line[i] = ' ' do
    inc(i);
  while (line[i] in n) and (i < length(line))  do
  begin
    extractnumbers := extractnumbers + line[i];
    inc(i);
  end;
  Result := trim(extractnumbers);
end;

procedure TExtractEmpForm.btnExtractEmpDataClick(Sender: TObject);
begin
  ProcessFile;
end;

Procedure TExtractEmpForm.LogErrors(Filename: TFilename; ErrMsg: String);
var
  LogFile: TextFile;
begin
  AssignFile(LogFile, Filename);
  if FileExists(FileName) then
    Append (LogFile)
  else
    Rewrite(LogFile);
  try
    Writeln(LogFile, DateTimeToStr(Now) + ': ' + ErrMsg);
  finally
    CloseFile (LogFile);
  end;
end;

function TExtractEmpForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainForm.RefreshButton.Visible := False;
end;

procedure TExtractEmpForm.FormCreate(Sender: TObject);
var
  Ini: TIniFile;
  Test: String;
begin
  try
    Ini := AdminDM.CreateIniFile;
    InputPath := Ini.ReadString('ExtractEmpPaths', 'InputPath', '');
    OutputPath := Ini.ReadString('ExtractEmpPaths', 'OutputPath', '');
    ErrorPath := Ini.ReadString('ExtractEmpPaths', 'ErrorPath', '');

    if not DirectoryExists(OutputPath) then
    begin
      OutputPath := 'C:\EmpData\Output\';
      if not ForceDirectories(OutputPath) then
       raise Exception.Create('Unable to create output directory');
    end;
    if not DirectoryExists(ErrorPath) then
    begin
      ErrorPath := 'C:\EmpData\Error\';
      if not ForceDirectories(ErrorPath) then
       raise Exception.Create('Unable to create error directory');
    end;
  finally
    Ini.Free;
  end;
end;

end.
