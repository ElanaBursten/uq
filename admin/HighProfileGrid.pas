unit HighProfileGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxMaskEdit, cxGridCustomTableView, cxGridTableView, cxControls, cxGridCustomView,
  cxClasses, cxGridLevel, cxGrid, cxGridDBTableView, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxScrollbarAnnotations;

type
  THighProfileGridForm = class(TBaseCxListForm)
    Label1: TLabel;
    CallCenterFilter: TComboBox;
    ColCallCenter: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColGrid: TcxGridDBColumn;
    cbHPGridSearch: TCheckBox;
    procedure DataNewRecord(DataSet: TDataSet);
    procedure CallCenterFilterSelect(Sender: TObject);
    procedure cbHPGridSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    hpgInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ THighProfileGridForm }

procedure THighProfileGridForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetCallCenterList(CallCenterFilter.Items);
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter));
  AdminDM.GetClientList(CxComboBoxItems(ColClientCode));
  CallCenterFilter.Items.Insert(0, '(ALL)');
  CallCenterFilter.ItemIndex := 0;
end;

procedure THighProfileGridForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbHPGridSearch.Enabled := True;
end;

procedure THighProfileGridForm.cbHPGridSearchClick(Sender: TObject);
begin
  inherited;
  if cbHPGridSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure THighProfileGridForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  hpgInserted := False;
end;

procedure THighProfileGridForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'hp_grid', 'hp_grid_id', hpgInserted)
end;

procedure THighProfileGridForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
   AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure THighProfileGridForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
   hpgInserted := True;
end;

procedure THighProfileGridForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  if CallCenterFilter.Text <> '(ALL)' then
    Data.FieldByName('call_center').AsString := CallCenterFilter.Text;
end;

procedure THighProfileGridForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure THighProfileGridForm.ActivatingNow;
begin
  inherited;
  hpgInserted := False;
end;

procedure THighProfileGridForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure THighProfileGridForm.CallCenterFilterSelect(Sender: TObject);
begin
  Data.Close;
  if CallCenterFilter.Text = '(ALL)' then begin
    Data.Filter := '';
    Data.Filtered := False;
  end else begin
    Data.Filter := Format('call_center = ''%s''', [CallCenterFilter.Text]);
    Data.Filtered := True;
  end;
  Data.Open;
end;

end.
