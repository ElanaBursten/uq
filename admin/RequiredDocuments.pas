unit RequiredDocuments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMasterDetail, DB, ADODB,  ExtCtrls, ActnList, StdCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxDropDownEdit, cxTextEdit, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxLookAndFeels, cxLookAndFeelPainters,cxDBLookupComboBox, 
  cxNavigator,cxDataControllerConditionalFormattingRulesManagerDialog,System.Actions, 
  dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore, AdminDMu;

type
  TRequiredDocumentsForm = class(TBaseMasterDetailForm)
    ColMasterForeignType: TcxGridDBColumn;
    ColMasterName: TcxGridDBColumn;
    ColMasterRequireForLocate: TcxGridDBColumn;
    ColMasterRequireForNoLocate: TcxGridDBColumn;
    ColMasterRequireForMinEstimate: TcxGridDBColumn;
    ColMasterUserSelectable: TcxGridDBColumn;
    ColMasterActive: TcxGridDBColumn;
    ColDetailDocTypeName: TcxGridDBColumn;
    ColDetailActive: TcxGridDBColumn;
    DocTypeRef: TADODataSet;
    DocTypeRefSource: TDataSource;
    cbRequiredDocsSearch: TCheckBox;
    ActiveRequiredDocsFilter: TCheckBox;
    ActiveRequiredDocTypesFilter: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DetailNewRecord(DataSet: TDataSet);
    procedure MasterNewRecord(DataSet: TDataSet);
    procedure cbRequiredDocsSearchClick(Sender: TObject);
    procedure ActiveRequiredDocsFilterClick(Sender: TObject);
    procedure MasterGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ActiveRequiredDocTypesFilterClick(Sender: TObject);
    procedure MasterAfterCancel(DataSet: TDataSet);
    procedure DetailAfterCancel(DataSet: TDataSet);
    procedure MasterBeforeInsert(DataSet: TDataSet);
    procedure DetailBeforeInsert(DataSet: TDataSet);
    procedure MasterBeforeEdit(DataSet: TDataSet);
    procedure DetailBeforeEdit(DataSet: TDataSet);
    procedure MasterAfterPost(DataSet: TDataSet);
    procedure DetailAfterPost(DataSet: TDataSet);
  private
    ReqDocInserted: Boolean;
    ReqDocTypeInserted: Boolean;
    ReqDocTypeArray: TFieldValuesArray;
  public
    procedure PopulateDropdowns; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  OdDbUtils, odMiscUtils, OdCxUtils;

{$R *.dfm}

procedure TRequiredDocumentsForm.ActiveRequiredDocsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Master.FieldByName('required_doc_id').AsInteger;
  Master.Close;
  if ActiveRequiredDocsFilter.Checked then
    Master.CommandText := 'select * from required_document where active = 1 order by name' else
      Master.CommandText := 'select * from required_document order by name';
  Master.Open;
  Master.Locate('required_doc_id', FSelectedID, []);
end;

procedure TRequiredDocumentsForm.ActiveRequiredDocTypesFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Detail.FieldByName('rdt_id').AsInteger;
  Detail.Close;
  if ActiveRequiredDocTypesFilter.Checked then
  Detail.CommandText :=  'select * from required_document_type where active = 1 order by doc_type' else
    Detail.CommandText := 'select * from required_document_type order by doc_type';
  Detail.Open;
  Detail.Locate('rdt_id', FSelectedID, []);
end;

procedure TRequiredDocumentsForm.cbRequiredDocsSearchClick(Sender: TObject);
begin
  inherited;
  if cbRequiredDocsSearch.Checked then
    MasterGridView.Controller.ShowFindPanel
  else
    MasterGridView.Controller.HideFindPanel;
end;

procedure TRequiredDocumentsForm.CloseDataSets;
begin
  DocTypeRef.Open;
  if DocTypeRef.IsEmpty then
    raise Exception.Create('Missing required doctype reference data. Configure the valid document types using the Reference option.');
  AddLookupField('doc_type_name', Detail, 'doc_type', DocTypeRef, 'code', 'description');
    //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
   Detail.Close;
   Detail.FieldByName('doc_type_name').LookupCache := True;
   Detail.Open;
  inherited;
end;

procedure TRequiredDocumentsForm.DetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').Value := True;
end;

procedure TRequiredDocumentsForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TRequiredDocumentsForm.FormCreate(Sender: TObject);
begin
  inherited;
//  CanDelete := False;
end;

procedure TRequiredDocumentsForm.LeavingNow;
begin
  inherited;
  Master.Close;
  Detail.Close;
  DocTypeRef.Close;
end;

procedure TRequiredDocumentsForm.MasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ReqDocInserted := False;
end;

procedure TRequiredDocumentsForm.MasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'required_document', 'required_doc_id', ReqDocInserted);
end;

procedure TRequiredDocumentsForm.DetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ReqDocTypeArray, 'required_document_type', 'rdt_id', ReqDocTypeInserted);
end;

procedure TRequiredDocumentsForm.MasterBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TRequiredDocumentsForm.MasterBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ReqDocInserted := True;
end;

procedure TRequiredDocumentsForm.DetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ReqDocTypeArray);
end;

procedure TRequiredDocumentsForm.DetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ReqDocTypeInserted := True;
end;

procedure TRequiredDocumentsForm.DetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ReqDocTypeInserted := False;
end;

procedure TRequiredDocumentsForm.ActivatingNow;
begin
  inherited;
  ReqDocInserted := False;
  ReqDocTypeInserted := False;
end;

procedure TRequiredDocumentsForm.MasterGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColMasterActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TRequiredDocumentsForm.MasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('foreign_type').Value := 2; // Only usable for damages for now.
  DataSet.FieldByName('active').Value := True;
end;

procedure TRequiredDocumentsForm.PopulateDropdowns;
begin
  inherited;
  CxComboBoxItems(ColMasterForeignType).Clear;
  CxComboBoxItems(ColMasterForeignType).Add('2'); // Damages
end;

procedure TRequiredDocumentsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbRequiredDocsSearch.Enabled := True;
  ActiveRequiredDocsFilter.Enabled := True;
  ActiveRequiredDocTypesFilter.Enabled := True;
end;

end.
