object HalForm: THalForm
  Left = 0
  Top = 0
  Caption = 'Test Data'
  ClientHeight = 443
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HalGrid: TDBGrid
    Left = 0
    Top = 39
    Width = 660
    Height = 363
    Align = alClient
    DataSource = DMHalSentinel.dsHalModule
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object WebBrowser1: TWebBrowser
    Left = 608
    Top = 408
    Width = 33
    Height = 12
    TabOrder = 1
    ControlData = {
      4C000000690300003E0100000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object Panel1: TPanel
    Left = 0
    Top = 402
    Width = 660
    Height = 41
    Align = alBottom
    TabOrder = 2
    object labldaysback: TLabel
      Left = 369
      Top = 13
      Width = 110
      Height = 13
      Caption = '#days from ticket date'
    end
    object lablVersionCount: TLabel
      Left = 201
      Top = 13
      Width = 108
      Height = 13
      Caption = 'minimum version count'
      Visible = False
    end
    object DBNavigator1: TDBNavigator
      Left = 32
      Top = 8
      Width = 144
      Height = 20
      DataSource = DMHalSentinel.dsHalModule
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 0
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 490
      Top = 10
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 9
      MinValue = 1
      TabOrder = 1
      Value = 7
    end
    object btnRefresh: TButton
      Left = 542
      Top = 7
      Width = 75
      Height = 25
      Caption = 'Refresh'
      TabOrder = 2
      OnClick = btnRefreshClick
    end
    object SpinEditCount: TSpinEdit
      Left = 315
      Top = 10
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 9
      MinValue = 1
      TabOrder = 3
      Value = 1
      Visible = False
    end
  end
  object pnlCallCenter: TPanel
    Left = 0
    Top = 0
    Width = 660
    Height = 39
    Align = alTop
    TabOrder = 3
    Visible = False
    object lblCallCenter: TLabel
      Left = 166
      Top = 13
      Width = 53
      Height = 13
      Caption = 'Call Center'
    end
    object lblThreshhold: TLabel
      Left = 402
      Top = 13
      Width = 53
      Height = 13
      Caption = 'Threshhold'
    end
    object edtCallCenter: TEdit
      Left = 233
      Top = 9
      Width = 145
      Height = 21
      TabOrder = 0
    end
    object edtThreshhold: TEdit
      Left = 474
      Top = 9
      Width = 57
      Height = 21
      NumbersOnly = True
      TabOrder = 1
    end
    object btnCallCenter: TButton
      Left = 32
      Top = 8
      Width = 108
      Height = 25
      Caption = 'Check Call Center'
      TabOrder = 2
      OnClick = btnCallCenterClick
    end
  end
end
