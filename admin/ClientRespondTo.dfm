inherited frmClientRespondTo: TfrmClientRespondTo
  Caption = 'Client Respond To'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 25
    object chkboxRespondToSearch: TCheckBox
      Left = 15
      Top = 7
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxRespondToSearchClick
    end
    object ActiveRespondToFilter: TCheckBox
      Left = 160
      Top = 7
      Width = 161
      Height = 13
      Caption = 'Active RespondTo Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveRespondToFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.KeyFieldNames = 'client_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColClientID: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.HideSelection = False
        Properties.ImmediateDropDownWhenActivated = True
        Properties.View = ClientLookupView
        Properties.KeyFieldNames = 'client_id'
        Properties.ListFieldItem = ColClientLookupClientName
        Properties.ReadOnly = False
        Properties.OnEditValueChanged = ColClientIDPropertiesEditValueChanged
        Width = 178
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client Code'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Options.FilteringPopupIncrementalFiltering = True
        Width = 95
      end
      object ColRespondTo: TcxGridDBColumn
        Caption = 'Respond To'
        DataBinding.FieldName = 'client_respond_to'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = RespondToLookupView
        Properties.KeyFieldNames = 'respond_to'
        Properties.ListFieldItem = ColRespondToLookupRespondTo
        Properties.ReadOnly = False
        Width = 125
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ReadOnly = False
        Width = 69
      end
    end
    object ClientLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      OnCustomDrawCell = ClientLookupViewCustomDrawCell
      DataController.DataSource = ClientLookupDS
      DataController.KeyFieldNames = 'client_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.DataRowSizing = True
      OptionsView.ColumnAutoWidth = True
      object ColClientLookupClientName: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_name'
      end
      object ColClientLookupOCCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'oc_code'
        Options.Editing = False
      end
      object ColClientLookupActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
      end
    end
    object RespondToLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = RespondToLookupDS
      DataController.KeyFieldNames = 'respond_to'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      object ColRespondToLookupRespondTo: TcxGridDBColumn
        Caption = 'Respond To'
        DataBinding.FieldName = 'respond_to'
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 
      'Select * from client_respond_to where active = 1 order by client' +
      '_code'
    Left = 48
  end
  object ClientLookupDS: TDataSource
    AutoEdit = False
    DataSet = ClientLookup
    Left = 488
    Top = 89
  end
  object ClientLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select client_id, client_name, oc_code, active  from client orde' +
      'r by client_id'
    Parameters = <>
    Left = 392
    Top = 96
  end
  object RespondToLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select respond_to from respond_to'
    Parameters = <>
    Left = 376
    Top = 144
  end
  object RespondToLookupDS: TDataSource
    AutoEdit = False
    DataSet = RespondToLookup
    Left = 480
    Top = 145
  end
end
