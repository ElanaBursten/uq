inherited AssignedTicketsForm: TAssignedTicketsForm
  Caption = 'Tickets Assigned to Inactive Techs'
  ClientHeight = 563
  ClientWidth = 1027
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 473
    Width = 1027
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 1027
    Height = 30
    object Label1: TLabel
      Left = 198
      Top = 9
      Width = 110
      Height = 13
      Caption = '#days from ticket date'
    end
    object chkboxAssignedTicketsSearch: TCheckBox
      Left = 31
      Top = 10
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxAssignedTicketsSearchClick
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 324
      Top = 7
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 9
      MinValue = 1
      TabOrder = 1
      Value = 7
    end
    object btnDateAdd: TButton
      Left = 380
      Top = 4
      Width = 80
      Height = 22
      Caption = 'Requery'
      TabOrder = 2
      OnClick = btnDateAddClick
    end
  end
  inherited Grid: TcxGrid
    Top = 30
    Width = 1027
    Height = 443
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = True
      object ColShortName: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        Options.Editing = False
        Width = 118
      end
      object ColTicketNumber: TcxGridDBColumn
        Caption = 'Ticket Number'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Width = 91
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client Code'
        DataBinding.FieldName = 'client_code'
        Options.Editing = False
        Width = 78
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Options.Editing = False
        Width = 81
      end
      object ColDueDate: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Width = 89
      end
      object ColTicketFormat: TcxGridDBColumn
        Caption = 'Ticket Format'
        DataBinding.FieldName = 'ticket_format'
        Options.Editing = False
        Width = 103
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 481
    Width = 1027
    Height = 82
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 90
      end
      object ColDaysback: TcxGridDBColumn
        Caption = 'Days Back'
        DataBinding.FieldName = 'daysback'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColDaysbackPropertiesEditValueChanged
        Width = 62
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select e.short_name,t.ticket_number,l.client_code,l.status,t.due' +
      '_date,t.ticket_format '#13#10'from locate l with (nolock)'#13#10'inner join ' +
      'employee e on e.emp_id = l.assigned_to_id'#13#10'inner join ticket t o' +
      'n l.ticket_id = t.ticket_id '#13#10'where (l.modified_date between get' +
      'date()- :dateadd and getdate()-1)'#13#10'and l.status ='#39'-R'#39' '#13#10'and  l.c' +
      'losed = 0'#13#10'and e.active =0'#13#10'and l.active = 1'#13#10
    Parameters = <
      item
        Name = 'dateadd'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Prepared = True
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from hal where hal_module = '#39'assigned_tickets'#39
    Parameters = <>
    Left = 264
    Top = 120
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 328
    Top = 128
  end
end
