inherited ApprovalQueueHistForm: TApprovalQueueHistForm
  Caption = 'TSE Approval Queue History'
  ClientHeight = 408
  ClientWidth = 901
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 901
    Height = 35
    object chkboxSearch: TCheckBox
      Left = 23
      Top = 11
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 35
    Width = 901
    Height = 373
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ColEntryID: TcxGridDBColumn
        Caption = 'Entry ID'
        DataBinding.FieldName = 'TSE_Entry_Id'
        DataBinding.IsNullValueType = True
        Width = 79
      end
      object ColWorkEmpID: TcxGridDBColumn
        Caption = 'Work Emp ID'
        DataBinding.FieldName = 'Work_Emp_ID'
        DataBinding.IsNullValueType = True
        Width = 83
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'Work_Emp_ID'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = EmployeeView
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListFieldItem = EmpNameCol
        Options.Editing = False
        Width = 102
      end
      object ColStatus: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        DataBinding.IsNullValueType = True
        Width = 82
      end
      object ColWorkDate: TcxGridDBColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'WorkDate'
        DataBinding.IsNullValueType = True
        Width = 98
      end
      object ColQR: TcxGridDBColumn
        DataBinding.FieldName = 'QR'
        DataBinding.IsNullValueType = True
        Width = 41
      end
      object ColQC: TcxGridDBColumn
        DataBinding.FieldName = 'QC'
        DataBinding.IsNullValueType = True
        Width = 45
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        DataBinding.IsNullValueType = True
        Width = 109
      end
      object ColNote: TcxGridDBColumn
        Caption = 'Note'
        DataBinding.FieldName = 'note'
        DataBinding.IsNullValueType = True
        Width = 282
      end
    end
    object EmployeeView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmployeeDS
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Appending = True
      object EmpNameCol: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        DataBinding.IsNullValueType = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from TSE_ApprovalQueueHistory'
  end
  object Employees: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      #13#10'Select  emp_id, '#13#10'case'#13#10'  when (last_name is not NULL) and (fi' +
      'rst_name is not NULL) then'#13#10'       concat(last_name, '#39', '#39', first' +
      '_name)'#13#10' when last_name is not NULL then'#13#10'         last_name'#13#10'  ' +
      'else NULL'#13#10'end as Name'#13#10'from employee '#13#10' where  active = 1 order' +
      ' by Name'
    Parameters = <>
    Left = 56
    Top = 166
  end
  object EmployeeDS: TDataSource
    DataSet = Employees
    Left = 120
    Top = 166
  end
end
