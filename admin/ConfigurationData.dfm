inherited ConfigurationDataForm: TConfigurationDataForm
  Caption = 'Configuration Data'
  ClientWidth = 951
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 951
    object SearchEdit: TEdit
      Left = 555
      Top = 13
      Width = 158
      Height = 21
      TabOrder = 0
      OnChange = SearchEditChange
    end
    object FilterRecordsButton: TButton
      Left = 453
      Top = 13
      Width = 94
      Height = 20
      Caption = 'Filter Setting'
      TabOrder = 1
      OnClick = FilterRecordsButtonClick
    end
    object ClearFilterButton: TButton
      Left = 719
      Top = 13
      Width = 67
      Height = 20
      Caption = 'Clear'
      TabOrder = 2
      OnClick = ClearFilterButtonClick
    end
  end
  inherited Grid: TDBGrid
    Width = 951
    Height = 325
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OnDblClick = GridDblClick
    OnExit = GridExit
    OnKeyDown = GridKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'name'
        Title.Caption = 'Setting Name'
        Width = 138
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'value'
        Title.Caption = 'Value'
        Width = 532
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'description'
        Title.Caption = 'Description'
        Width = 104
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'sync'
        ReadOnly = True
        Title.Caption = 'Sync'
        Width = 57
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 366
    Width = 951
    Height = 34
    Align = alBottom
    TabOrder = 2
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 6
      Width = 240
      Height = 25
      DataSource = DS
      Enabled = False
      TabOrder = 0
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 
      'select name, value, description, sync from configuration_data wh' +
      'ere editable=1'
    object Dataname: TStringField
      FieldName = 'name'
      Size = 25
    end
    object Datavalue: TStringField
      FieldName = 'value'
      Size = 255
    end
    object Datadescription: TMemoField
      FieldName = 'description'
      BlobType = ftMemo
    end
    object Datasync: TBooleanField
      FieldName = 'sync'
    end
  end
end
