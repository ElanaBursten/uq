inherited MapSetupForm: TMapSetupForm
  Left = 208
  Top = 160
  Caption = 'Map Setup'
  ClientHeight = 519
  ClientWidth = 1034
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object PC: TPageControl [0]
    Left = 0
    Top = 0
    Width = 1034
    Height = 320
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = PCChange
    object TabSheet2: TTabSheet
      Caption = 'Map Definition'
      ImageIndex = 1
      DesignSize = (
        1026
        292)
      object Label5: TLabel
        Left = 10
        Top = 8
        Width = 29
        Height = 13
        Caption = 'Maps:'
      end
      object Label7: TLabel
        Left = 529
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Pages in selected map:'
      end
      object Label16: TLabel
        Left = 4
        Top = 267
        Width = 353
        Height = 31
        Anchors = [akLeft, akBottom]
        AutoSize = False
        Caption = 
          'Use this screen to set up maps, pages, and areas.  Assignment of' +
          ' locators, areas, etc. is on the Assignment tab.'
        WordWrap = True
      end
      object map_pageGrid: TDBGrid
        Left = 529
        Top = 27
        Width = 318
        Height = 237
        Anchors = [akLeft, akTop, akBottom]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = map_pageGridDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'map_page_num'
            Title.Caption = 'Page #'
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'x_min'
            Title.Caption = 'Left C.'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'x_max'
            Title.Caption = 'Right C.'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'y_min'
            Title.Caption = 'Top R.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'y_max'
            Title.Caption = 'Bot. R.'
            Width = 56
            Visible = True
          end>
      end
      object MapGrid: TcxGrid
        Left = 4
        Top = 26
        Width = 516
        Height = 238
        Anchors = [akLeft, akTop, akBottom]
        TabOrder = 1
        OnExit = MapGridExit
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        object MapGridView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmManual
          FindPanel.ShowCloseButton = False
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = MapDS
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'map_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object ColMapName: TcxGridDBColumn
            Caption = 'Map Name'
            DataBinding.FieldName = 'map_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 129
          end
          object ColMapState: TcxGridDBColumn
            Caption = 'State'
            DataBinding.FieldName = 'state'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 57
          end
          object ColMapCounty: TcxGridDBColumn
            Caption = 'County'
            DataBinding.FieldName = 'county'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 107
          end
          object ColMapTicketCode: TcxGridDBColumn
            Caption = 'Tkt Code'
            DataBinding.FieldName = 'ticket_code'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 54
          end
          object ColMap_Type: TcxGridDBColumn
            Caption = 'MapType'
            DataBinding.FieldName = 'map_type'
            DataBinding.IsNullValueType = True
            Width = 86
          end
          object ColMapId: TcxGridDBColumn
            Caption = 'Map ID'
            DataBinding.FieldName = 'map_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = True
            HeaderAlignmentHorz = taRightJustify
            Options.Editing = False
            Width = 54
          end
        end
        object MapGridLevel: TcxGridLevel
          GridView = MapGridView
        end
      end
      object chkboxMapSearch: TCheckBox
        Left = 368
        Top = 6
        Width = 97
        Height = 14
        Caption = 'Show Find Panel'
        TabOrder = 2
        OnClick = chkboxMapSearchClick
      end
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 0
        Width = 8
        Height = 292
      end
    end
    object AssignmentSheet: TTabSheet
      Caption = 'Grid Assignment'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 60
        Top = 0
        Width = 7
        Height = 292
      end
      object MapPageGrid: TcxGrid
        Left = 0
        Top = 0
        Width = 60
        Height = 292
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object MapPageGridView: TcxGridDBTableView
          Navigator.Buttons.ConfirmDelete = True
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = False
          Navigator.Buttons.Next.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = MapPageDS
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'map_page_num'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GridLines = glNone
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object ColMapPagePageNum: TcxGridDBColumn
            Caption = 'Page'
            DataBinding.FieldName = 'map_page_num'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taCenter
            Properties.ReadOnly = True
            Options.Editing = False
            Width = 58
          end
        end
        object MapPageGridLevel: TcxGridLevel
          GridView = MapPageGridView
        end
      end
      object TheGrid: TStringGrid
        Left = 67
        Top = 0
        Width = 959
        Height = 292
        Align = alClient
        DefaultColWidth = 80
        DefaultRowHeight = 32
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goAlwaysShowEditor]
        TabOrder = 1
        OnDrawCell = TheGridDrawCell
        ColWidths = (
          80
          80
          80
          80
          80)
        RowHeights = (
          32
          32
          32
          32
          32)
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 328
    Width = 1034
    Height = 191
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      1034
      191)
    object Label4: TLabel
      Left = 540
      Top = 4
      Width = 45
      Height = 13
      Caption = 'Locators:'
    end
    object Label6: TLabel
      Left = 2
      Top = 4
      Width = 109
      Height = 13
      Caption = 'Areas in selected map:'
    end
    object AssignToLocatorBtn: TBitBtn
      Left = 379
      Top = 72
      Width = 153
      Height = 25
      Caption = 'Assign area to locator'
      Glyph.Data = {
        DE000000424DDE0000000000000076000000280000000D0000000D0000000100
        0400000000006800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7000777777077777700077777700777770007777770607777000770000066077
        7000770666666607700077066666666070007706666666077000770000066077
        7000777777060777700077777700777770007777770777777000777777777777
        7000}
      TabOrder = 1
      OnClick = AssignToLocatorBtnClick
    end
    object AssignAreaButton: TBitBtn
      Left = 378
      Top = 24
      Width = 153
      Height = 25
      Caption = 'Assign Area to Grid Cells'
      Enabled = False
      Glyph.Data = {
        DE000000424DDE0000000000000076000000280000000D0000000D0000000100
        0400000000006800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7000777777777777700077770000077770007777066607777000777706660777
        7000777706660777700070000666000070007706666666077000777066666077
        7000777706660777700077777060777770007777770777777000777777777777
        7000}
      TabOrder = 0
      OnClick = AssignAreaButtonClick
    end
    object SelPageButton: TBitBtn
      Left = 380
      Top = 128
      Width = 153
      Height = 25
      Caption = 'Select Entire Page'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777877877877877777787787787787777880880880880887777877877877
        8777777877877877877778808808808808877778778778778777777877877877
        8777788088088088088777787787787787777778778778778777788088088088
        0887777877877877877777787787787787777777777777777777}
      TabOrder = 2
      OnClick = SelPageButtonClick
    end
    object AreaGrid: TcxGrid
      Left = 3
      Top = 23
      Width = 368
      Height = 153
      Anchors = [akLeft, akTop, akBottom]
      BevelEdges = [beLeft, beRight, beBottom]
      TabOrder = 3
      OnExit = AreaGridExit
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object AreaGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = True
        Navigator.Buttons.Next.Visible = True
        Navigator.Buttons.NextPage.Visible = True
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = True
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCustomDrawCell = AreaGridViewCustomDrawCell
        DataController.DataSource = AreaDS
        DataController.Filter.Active = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsData.Appending = True
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object AreaGridViewColAreaName: TcxGridDBColumn
          Caption = 'Area Name'
          DataBinding.FieldName = 'area_name'
          DataBinding.IsNullValueType = True
          Width = 123
        end
        object AreaGridViewColShortName: TcxGridDBColumn
          Caption = 'Locator'
          DataBinding.FieldName = 'short_name'
          DataBinding.IsNullValueType = True
          Options.Editing = False
          Width = 120
        end
        object AreaGridViewColEmpNumber: TcxGridDBColumn
          Caption = 'Emp #'
          DataBinding.FieldName = 'emp_number'
          DataBinding.IsNullValueType = True
          Options.Editing = False
          Width = 63
        end
        object AreaGridViewColActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
        end
      end
      object AreaGridLevel1: TcxGridLevel
        GridView = AreaGridView
      end
    end
    object LocatorGrid: TcxGrid
      Left = 542
      Top = 23
      Width = 317
      Height = 153
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 4
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object LocatorGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCustomDrawCell = LocatorGridViewCustomDrawCell
        DataController.DataSource = LocatorDS
        DataController.Filter.Active = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object LocatorGridViewColLocatorName: TcxGridDBColumn
          Caption = 'Locator Name'
          DataBinding.FieldName = 'short_name'
          DataBinding.IsNullValueType = True
          Width = 183
        end
        object LocatorGridViewColEmpNum: TcxGridDBColumn
          Caption = 'Emp #'
          DataBinding.FieldName = 'emp_number'
          DataBinding.IsNullValueType = True
          Width = 71
        end
        object LocatorGridViewColActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          Options.Filtering = False
          Width = 52
        end
      end
      object LocatorGridLevel1: TcxGridLevel
        GridView = LocatorGridView
      end
    end
    object chkboxLocatorSearch: TCheckBox
      Left = 603
      Top = 5
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 5
      OnClick = chkboxLocatorSearchClick
    end
    object ActiveLocatorsFilter: TCheckBox
      Left = 719
      Top = 5
      Width = 140
      Height = 12
      Caption = 'Active Locators Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 6
      OnClick = ActiveLocatorsFilterClick
    end
    object chkboxAreaSearch: TCheckBox
      Left = 127
      Top = 5
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 7
      OnClick = chkboxAreaSearchClick
    end
    object ActiveAreasFilter: TCheckBox
      Left = 245
      Top = 6
      Width = 116
      Height = 12
      Caption = 'Active Areas Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 8
      OnClick = ActiveAreasFilterClick
    end
  end
  object cxSplitter2: TcxSplitter [2]
    Left = 0
    Top = 320
    Width = 1034
    Height = 8
    AlignSplitter = salBottom
  end
  inherited GetAreaRepr: TADODataSet
    Left = 64
    Top = 128
  end
  object LocatorDS: TDataSource
    DataSet = AdminDM.Locator
    Left = 812
    Top = 259
  end
  object MapDS: TDataSource
    DataSet = Map
    OnDataChange = MapDSDataChange
    Left = 812
    Top = 203
  end
  object AreaDS: TDataSource
    DataSet = Area
    Left = 860
    Top = 203
  end
  object MapPageDS: TDataSource
    DataSet = MapPage
    Left = 924
    Top = 211
  end
  object Area: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterOpen = AreaAfterOpen
    BeforeInsert = AreaBeforeInsert
    BeforeEdit = AreaBeforeEdit
    BeforePost = AreaBeforePost
    AfterPost = AreaAfterPost
    AfterCancel = AreaAfterCancel
    BeforeDelete = DatasetBeforeDelete
    OnNewRecord = AreaNewRecord
    CommandText = 
      'select area.*,'#13#10' employee.short_name, employee.emp_number'#13#10' from' +
      ' area'#13#10'  left join employee on area.locator_id=employee.emp_id'#13#10 +
      'where map_id = :map_id'#13#10'and area.active = 1'#13#10'order by area.area_' +
      'name'
    Parameters = <
      item
        Name = 'map_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 600
      end>
    Left = 861
    Top = 152
  end
  object MapPage: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterScroll = MapPageAfterScroll
    CommandText = 
      'select * from map_page'#13#10' where map_id=:map_id'#13#10' order by map_pag' +
      'e_num'
    DataSource = MapDS
    EnableBCD = False
    IndexFieldNames = 'map_id'
    Parameters = <
      item
        Name = 'map_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 600
      end>
    Left = 921
    Top = 154
  end
  object GridData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeInsert = GridDataBeforeInsert
    BeforeEdit = GridDataBeforeEdit
    AfterPost = GridDataAfterPost
    AfterCancel = GridDataAfterCancel
    CommandText = 
      'select * from map_cell'#13#10' where map_id=:map_id'#13#10' and map_page_num' +
      '=:num'#13#10' order by y_part, x_part'
    EnableBCD = False
    Parameters = <
      item
        Name = 'map_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'num'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 856
    Top = 88
  end
  object Map: TADODataSet
    CacheSize = 100
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = MapBeforeInsert
    BeforeEdit = MapBeforeEdit
    AfterPost = MapAfterPost
    AfterCancel = MapAfterCancel
    BeforeDelete = DatasetBeforeDelete
    CommandText = 'select * from map '#13#10'order by map_name'
    EnableBCD = False
    Parameters = <>
    Left = 812
    Top = 153
  end
end
