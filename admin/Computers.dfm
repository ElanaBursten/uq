inherited ComputersForm: TComputersForm
  Left = 267
  Caption = 'Computers'
  ClientHeight = 580
  ClientWidth = 1139
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 1139
    object NoteLabel: TLabel
      Left = 8
      Top = 4
      Width = 523
      Height = 29
      AutoSize = False
      Caption = 
        'The Computers shown here are those that have synced some data fr' +
        'om QManager to the database. Check the Retired checkbox and sele' +
        'ct a Retired Reason to remove entries from the Active Computer S' +
        'ync Report.'
      WordWrap = True
    end
    object ActiveComputersFilter: TCheckBox
      Left = 563
      Top = 15
      Width = 173
      Height = 17
      Caption = 'Show active computers only'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveComputersFilterClick
    end
    object chkboxSearch: TCheckBox
      Left = 923
      Top = 13
      Width = 97
      Height = 22
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = chkboxSearchClick
    end
    object ActiveEmployeesFilter: TCheckBox
      Left = 742
      Top = 15
      Width = 175
      Height = 17
      Hint = 'Showing All employees will result in very slow results'
      Caption = 'Show active employees only'
      Checked = True
      ParentShowHint = False
      ShowHint = True
      State = cbChecked
      TabOrder = 2
      OnClick = ActiveComputersFilterClick
    end
  end
  inherited Grid: TcxGrid
    Width = 1139
    Height = 539
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'computer_info_id'
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.FocusCellOnCycle = False
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object ColComputerInfoID: TcxGridDBColumn
        Caption = 'Computer Info ID'
        DataBinding.FieldName = 'computer_info_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 42
      end
      object ColComputerSerial: TcxGridDBColumn
        Caption = 'Serial No.'
        DataBinding.FieldName = 'computer_serial'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 150
      end
      object ColComputerName: TcxGridDBColumn
        Caption = 'Computer Name'
        DataBinding.FieldName = 'computer_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 86
      end
      object ColInsertDate: TcxGridDBColumn
        Caption = 'Insert Date'
        DataBinding.FieldName = 'insert_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Properties.ReadOnly = False
        Options.Editing = False
        Width = 130
      end
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 52
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = EmployeeView
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListFieldItem = EmpNameCol
        Properties.ReadOnly = False
        Options.Sorting = False
        Width = 95
      end
      object ColEmpActive: TcxGridDBColumn
        Caption = 'Active Emp'
        DataBinding.FieldName = 'active_emp'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        Width = 58
      end
      object ColWindowsUser: TcxGridDBColumn
        Caption = 'Windows User'
        DataBinding.FieldName = 'windows_user'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 98
      end
      object ColDomainLogin: TcxGridDBColumn
        Caption = 'Login Domain'
        DataBinding.FieldName = 'domain_login'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 73
      end
      object ColOSPlatform: TcxGridDBColumn
        Caption = 'OS Platform'
        DataBinding.FieldName = 'os_platform'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 49
      end
      object ColOSMajorVersion: TcxGridDBColumn
        Caption = 'OS Major Ver.'
        DataBinding.FieldName = 'os_major_version'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 58
      end
      object ColOSMinorVersion: TcxGridDBColumn
        Caption = 'OS Minor Ver.'
        DataBinding.FieldName = 'os_minor_version'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 57
      end
      object ColOSServicePack: TcxGridDBColumn
        Caption = 'OS Service Pack'
        DataBinding.FieldName = 'os_service_pack'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 69
      end
      object ColServiceTag: TcxGridDBColumn
        Caption = 'Service Tag'
        DataBinding.FieldName = 'service_tag'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 74
      end
      object ColComputerModel: TcxGridDBColumn
        Caption = 'Model'
        DataBinding.FieldName = 'computer_model'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 58
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 43
      end
      object ColRetireReason: TcxGridDBColumn
        Caption = 'Retire Reason'
        DataBinding.FieldName = 'retire_reason'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = ReasonView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = ColReasonDescrption
        Width = 81
      end
      object ColSyncID: TcxGridDBColumn
        Caption = 'Last Sync ID'
        DataBinding.FieldName = 'sync_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 28
      end
    end
    object ReasonView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = RetireReasonDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ColReasonCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 74
      end
      object ColReasonDescrption: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 153
      end
    end
    object EmployeeView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmployeeDS
      DataController.Filter.Options = [fcoSoftCompare, fcoIgnoreNull]
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsView.GroupByBox = False
      object EmpidCol: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxTextEditProperties'
      end
      object EmpNameCol: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Options.FilteringPopupIncrementalFiltering = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 
      'select e.active as active_emp, c.* from computer_info c left joi' +
      'n employee e on c.emp_id = e.emp_id where e.active = 1 and c.act' +
      'ive = 1'
    Top = 104
  end
  inherited DS: TDataSource
    Left = 128
    Top = 104
  end
  object RetireReason: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      'select * from reference where type = '#39'compretr'#39' and active_ind=1' +
      ' order by sortby'
    Parameters = <>
    Left = 48
    Top = 160
  end
  object RetireReasonDS: TDataSource
    DataSet = RetireReason
    Left = 128
    Top = 160
  end
  object EmployeeDS: TDataSource
    DataSet = Employees
    Left = 128
    Top = 214
  end
  object Employees: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      'Select emp_id, '#13#10'case'#13#10'  when (last_name is not NULL) and (first' +
      '_name is not NULL) then'#13#10'       concat(last_name, '#39', '#39', first_na' +
      'me)'#13#10' when last_name is not NULL then'#13#10'         last_name'#13#10'  els' +
      'e NULL'#13#10'end as Name'#13#10'from employee where  active = 1 order by Na' +
      'me'
    Parameters = <>
    Left = 48
    Top = 214
  end
end
