inherited StatusListForm: TStatusListForm
  Caption = 'StatusListForm'
  ClientHeight = 437
  ClientWidth = 1296
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 772
    Top = 0
    Width = 0
    Height = 437
  end
  object Splitter2: TSplitter [1]
    Left = 765
    Top = 0
    Width = 7
    Height = 437
  end
  inherited HeaderPanel: TPanel
    Width = 1296
    Height = 0
  end
  inherited MasterPanel: TPanel
    Top = 0
    Width = 765
    Height = 437
    inherited MasterGrid: TcxGrid
      Left = 2
      Width = 757
      Height = 382
      inherited MasterGridView: TcxGridDBTableView
        Navigator.Buttons.First.Visible = True
        Navigator.Buttons.PriorPage.Visible = True
        Navigator.Buttons.NextPage.Visible = True
        Navigator.Buttons.Last.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = True
        Navigator.Buttons.GotoBookmark.Visible = True
        Navigator.Buttons.Filter.Visible = True
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        OnCustomDrawCell = MasterGridViewCustomDrawCell
        DataController.Filter.Active = True
        Filtering.MRUItemsList = False
        Filtering.ColumnMRUItemsList = False
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.Appending = True
        OptionsView.HeaderHeight = 32
        object MasterGridViewstatus: TcxGridDBColumn
          Caption = 'Status'
          DataBinding.FieldName = 'status'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Width = 54
        end
        object MasterGridViewstatus_name: TcxGridDBColumn
          Caption = 'Status Name'
          DataBinding.FieldName = 'status_name'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Width = 163
        end
        object MasterGridViewbillable: TcxGridDBColumn
          Caption = 'Billable'
          DataBinding.FieldName = 'billable'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          MinWidth = 16
          Width = 59
        end
        object MasterGridViewcomplete: TcxGridDBColumn
          Caption = 'Complete'
          DataBinding.FieldName = 'complete'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          MinWidth = 16
          Width = 67
        end
        object MasterGridViewallow_locate_units: TcxGridDBColumn
          Caption = 'Allow Length Entry'
          DataBinding.FieldName = 'allow_locate_units'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          MinWidth = 16
          Width = 67
        end
        object MasterGridViewmark_type_required: TcxGridDBColumn
          Caption = 'Require Mark Type'
          DataBinding.FieldName = 'mark_type_required'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          MinWidth = 16
          Width = 67
        end
        object MasterGridViewdefault_mark_type: TcxGridDBColumn
          Caption = 'Default Mark Type'
          DataBinding.FieldName = 'default_mark_type'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownRows = 7
          Properties.MaxLength = 0
          MinWidth = 70
          Width = 84
        end
        object MasterGridViewmeet_only: TcxGridDBColumn
          Caption = 'Meet Only'
          DataBinding.FieldName = 'meet_only'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          Width = 48
        end
        object MasterGridViewactive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          Width = 48
        end
        object MasterGridNoteReqd: TcxGridDBColumn
          Caption = 'note req'
          DataBinding.FieldName = 'note_required'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
        end
        object MasterGridViewmodified_date: TcxGridDBColumn
          Caption = 'Modified Date'
          DataBinding.FieldName = 'modified_date'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikStandard
          Width = 150
        end
      end
    end
    inherited MasterNewButton: TButton
      Top = 10
    end
    inherited MasterDeleteButton: TButton
      Left = 97
      Top = 10
    end
    object ActiveStatusesFilter: TCheckBox
      Left = 345
      Top = 16
      Width = 140
      Height = 14
      Caption = 'Active Statuses Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      OnClick = ActiveStatusesFilterClick
    end
    object chkboxStatusSearch: TCheckBox
      Left = 228
      Top = 16
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 4
      OnClick = chkboxStatusSearchClick
    end
  end
  inherited DetailPanel: TPanel
    Left = 772
    Top = 0
    Width = 524
    Height = 437
    DesignSize = (
      524
      437)
    inherited DetailGrid: TcxGrid
      Width = 518
      Height = 382
      inherited DetailGridView: TcxGridDBTableView
        OnKeyDown = DetailGridViewKeyDown
        Navigator.Buttons.First.Visible = True
        Navigator.Buttons.PriorPage.Visible = True
        Navigator.Buttons.NextPage.Visible = True
        Navigator.Buttons.Last.Visible = True
        Navigator.Buttons.Insert.Enabled = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Enabled = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Enabled = False
        Navigator.Buttons.Edit.Enabled = False
        Navigator.Buttons.Post.Enabled = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Enabled = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = True
        Navigator.Buttons.GotoBookmark.Visible = True
        Navigator.Buttons.Filter.Visible = True
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        OnCustomDrawCell = DetailGridViewCustomDrawCell
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object DetailGridViewClientName: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'client_name'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
        end
        object DetailGridViewStatus: TcxGridDBColumn
          Caption = 'Status'
          DataBinding.FieldName = 'status'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
        end
        object DetailGridViewActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
        end
        object DetailGridViewCallCenter: TcxGridDBColumn
          Caption = 'Call Center'
          DataBinding.FieldName = 'call_center'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
        end
        object DetailGridViewCustomerID: TcxGridDBColumn
          Caption = 'Customer ID'
          DataBinding.FieldName = 'customer_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
          Width = 74
        end
        object DetailGridViewUtility: TcxGridDBColumn
          Caption = 'Utility'
          DataBinding.FieldName = 'utility_name'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxExtLookupComboBoxProperties'
          Properties.View = UtilityLookupView
          Properties.KeyFieldNames = 'description'
          Properties.ListFieldItem = UtilityLookupViewDescription
          Properties.ReadOnly = True
          MinWidth = 126
          Options.HorzSizing = False
          Width = 126
        end
        object DetailGridViewResponder: TcxGridDBColumn
          Caption = 'Responder'
          DataBinding.FieldName = 'RefID_Responder'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
          Options.Grouping = False
        end
        object DetailGridViewDamageCo: TcxGridDBColumn
          Caption = 'Damage Co'
          DataBinding.FieldName = 'DamageCompany'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxExtLookupComboBoxProperties'
          Properties.View = DamageCompanyLookupView
          Properties.KeyFieldNames = 'ref_id'
          Properties.ListFieldItem = DamageCompanyLookupViewDamageCo
          Properties.ReadOnly = True
          MinWidth = 126
          Width = 148
        end
      end
      object UtilityLookupView: TcxGridDBTableView [1]
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = UtilityLookupSource
        DataController.KeyFieldNames = 'description'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object UtilityLookupViewCode: TcxGridDBColumn
          DataBinding.FieldName = 'code'
          DataBinding.IsNullValueType = True
        end
        object UtilityLookupViewDescription: TcxGridDBColumn
          DataBinding.FieldName = 'description'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        end
      end
      object DamageCompanyLookupView: TcxGridDBTableView [2]
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DamageCoLookupSource
        DataController.KeyFieldNames = 'ref_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object DamageCompanyLookupViewDamageCo: TcxGridDBColumn
          DataBinding.FieldName = 'DamageCompany'
          DataBinding.IsNullValueType = True
        end
        object DamageCompanyLookupViewRefID: TcxGridDBColumn
          DataBinding.FieldName = 'ref_id'
          DataBinding.IsNullValueType = True
        end
        object DamageCompanyLookupViewActiveID: TcxGridDBColumn
          DataBinding.FieldName = 'active_ind'
          DataBinding.IsNullValueType = True
        end
      end
    end
    inherited DetailNewButton: TButton
      Left = 344
      Top = 9
      Visible = False
    end
    inherited DetailDeleteButton: TButton
      Left = 430
      Top = 9
      Visible = False
    end
    object ActiveClientsFilter: TCheckBox
      Left = 150
      Top = 17
      Width = 129
      Height = 14
      Caption = 'Active Clients Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      OnClick = ActiveClientsFilterClick
    end
    object chkboxClientSearch: TCheckBox
      Left = 30
      Top = 17
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 4
      OnClick = chkboxClientSearchClick
    end
  end
  inherited Master: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = MasterBeforeInsert
    BeforeEdit = MasterBeforeEdit
    BeforePost = MasterBeforePost
    AfterPost = MasterAfterPost
    AfterCancel = MasterAfterCancel
    OnNewRecord = MasterNewRecord
    CommandText = 'select * from statuslist'#13#10' where active = 1 order by status'
    Left = 104
    Top = 192
  end
  inherited Detail: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DetailBeforeOpen
    CommandText = 
      'select c.*,  a.status from client c'#13#10'join '#13#10'(select distinct sta' +
      'tus, sg_id from status_group_item) a'#13#10'on c.status_group_id = a.s' +
      'g_id'#13#10'where c.active = 1'
    IndexFieldNames = 'status'
    MasterFields = 'status'
    Left = 184
    Top = 188
  end
  inherited MasterSource: TDataSource
    Left = 108
    Top = 144
  end
  inherited DetailSource: TDataSource
    Left = 188
    Top = 148
  end
  inherited ActionList: TActionList
    Left = 268
    Top = 164
  end
  object MarkTypes: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference where type='#39'MARK'#39' order by sortby'
    Parameters = <>
    Left = 40
    Top = 165
  end
  object UtilityLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference where type='#39'utility'#39
    Parameters = <>
    Left = 376
    Top = 152
  end
  object UtilityLookupSource: TDataSource
    DataSet = UtilityLookup
    Left = 376
    Top = 208
  end
  object DamageCoLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, description as DamageCompany, active_ind  '#13#10'from ' +
      'reference'#13#10'where type = '#39'dmgco'#39#13#10'order by description '
    Parameters = <>
    Prepared = True
    Left = 481
    Top = 153
  end
  object DamageCoLookupSource: TDataSource
    DataSet = DamageCoLookup
    Left = 489
    Top = 209
  end
end
