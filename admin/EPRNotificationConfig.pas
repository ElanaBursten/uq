unit EPRNotificationConfig;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, AdminDMu,
  cxDropDownEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, cxMaskEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TEPRNotificationConfigForm = class(TBaseCxListForm)
    NotificationConfigIDCol: TcxGridDBColumn;
    EPRNotificationCallCenterCol: TcxGridDBColumn;
    GridViewexclude_ticket_type: TcxGridDBColumn;
    GridViewwork_done_for: TcxGridDBColumn;
    GridViewcontractor: TcxGridDBColumn;
    GridViewdisclaimer_id: TcxGridDBColumn;
    GridViewscreenshots_include: TcxGridDBColumn;
    GridViewdelayer_client_codes: TcxGridDBColumn;
    GridViewclient_codes_require_cv: TcxGridDBColumn;
    GridViewcv_Wait_Count: TcxGridDBColumn;
    GridViewinc_att_Wait_Hrs: TcxGridDBColumn;
    GridViewsurvey_url: TcxGridDBColumn;
    GridViewviewable_days: TcxGridDBColumn;
    GridViewcontact_to: TcxGridDBColumn;
    GridViewcontact_cc: TcxGridDBColumn;
    GridViewcontact_phone: TcxGridDBColumn;
    GridViewcontact_name: TcxGridDBColumn;
    GridViewactive: TcxGridDBColumn;
    GridViewremove_all_tie_downs: TcxGridDBColumn;
    GridViewcompany_token: TcxGridDBColumn;
    GridViewin_EPR: TcxGridDBColumn;
    GridViewcontains_clients: TcxGridDBColumn;
    LookupDisclaimer: TADODataSet;
    DisclaimerDS: TDataSource;
    cbNotificationSearch: TCheckBox;
    ActiveNotificationFilter: TCheckBox;
    procedure DataBeforePost(DataSet: TDataSet);
    procedure ActiveNotificationFilterClick(Sender: TObject);
    procedure cbNotificationSearchClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    EPRConfigInserted: Boolean;
    procedure InitializeGrid;
  public
    procedure PopulateDropDowns; override;
    procedure LeavingNow; Override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure CloseDatasets; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  EPRNotificationConfigForm: TEPRNotificationConfigForm;

implementation
uses
ConfigDMu, OdCxUtils, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TEPRNotificationConfigForm }

procedure TEPRNotificationConfigForm.ActiveNotificationFilterClick(
  Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('notification_config_id').AsInteger;
  Data.Close;
  if ActiveNotificationFilter.Checked then
    Data.CommandText := 'select * from epr_notification_config where active = 1' else
      Data.CommandText := 'select * from epr_notification_config';
  Data.Open;
  Data.Locate('notification_config_id', FSelectedID, []);
end;

procedure TEPRNotificationConfigForm.cbNotificationSearchClick(Sender: TObject);
begin
  inherited;
  if cbNotificationSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TEPRNotificationConfigForm.CloseDatasets;
begin
  inherited;
  LookupDisclaimer.Close;
end;

procedure TEPRNotificationConfigForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  EPRConfigInserted := False;
end;

procedure TEPRNotificationConfigForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'EPR_Notification_Config', 'notification_config_id', EPRConfigInserted);
end;

procedure TEPRNotificationConfigForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TEPRNotificationConfigForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
   EPRConfigInserted := True;
end;

procedure TEPRNotificationConfigForm.DataBeforePost(DataSet: TDataSet);
const
  Msg = 'Required Fields are missing. ' + #13#10;
  MsgReq = 'Required: Call Center, Done For, Contractor, Screenshots Inc, Disclaimer, ' +
           'Delayer CC, CC Require CV, CV Wait Count, Inc ATT Wait Hrs, Survey_URL' +
           'Viewable Days, Contact To, Company Token ' + #13#10 + #13#10;
  Msg2 = 'Click OK to Continue or Abort to Cancel changes.';
begin
  inherited;

  if (DataSet.FieldByName('call_center').isNull) or
     (DataSet.FieldByName('work_done_for').isNull) or
     (DataSet.FieldByName('disclaimer_id').IsNull) or
     (DataSet.FieldByName('contractor').isNull) or
     (DataSet.FieldByName('screenshots_include').isNull) or
     (DataSet.FieldByName('delayer_client_codes').isNull) or
     (DataSet.FieldByName('client_codes_require_cv').isNull) or
     (DataSet.FieldByName('cv_Wait_Count').isNULL) or
     (DataSet.FieldByName('inc_att_Wait_Hrs').IsNull) or
     (DataSet.FieldByName('survey_url').IsNull) or
     (DataSet.FieldByName('viewable_days').IsNull) or
     (DataSet.FieldByName('contact_to').IsNull) or
     (DataSet.FieldByName('company_token').IsNull) then begin
    {Cancel changes}
    If MessageDlg(Msg +MsgReq + Msg2, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
    end
    {Continue with changes, but don't try to post}
    else
      Abort;
  end;
//  else
//    DataSet.FieldByName('modified_date').AsDateTime := Now;

end;

procedure TEPRNotificationConfigForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TEPRNotificationConfigForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEPRNotificationConfigForm.FormCreate(Sender: TObject);
begin
  inherited;
  GridView.LookAndFeel.ScrollbarMode := (sbmClassic);
end;

procedure TEPRNotificationConfigForm.ActivatingNow;
begin
  inherited;
  EPRConfigInserted := False;
end;

procedure TEPRNotificationConfigForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[GridViewActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TEPRNotificationConfigForm.InitializeGrid;
begin

end;

procedure TEPRNotificationConfigForm.LeavingNow;
begin
  inherited;
  CloseDatasets;
end;

procedure TEPRNotificationConfigForm.PopulateDropDowns;
begin
  inherited;
  AdminDM.ConfigDM.GetCallCenterList(cxComboBoxItems(EPRNotificationCallCenterCol), False, False);
  if not LookupDisclaimer.Active then LookupDisclaimer.Open;
end;

procedure TEPRNotificationConfigForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbNotificationSearch.Enabled := True;
  ActiveNotificationFilter.Enabled := True;
end;

end.
