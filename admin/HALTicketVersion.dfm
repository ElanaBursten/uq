inherited TicketVersionForm: TTicketVersionForm
  Caption = 'Ticket Versions'
  ClientHeight = 472
  ClientWidth = 766
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 389
    Width = 766
    Height = 7
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 766
    object Label1: TLabel
      Left = 193
      Top = 14
      Width = 108
      Height = 13
      Caption = 'minimum version count'
    end
    object Label2: TLabel
      Left = 368
      Top = 14
      Width = 56
      Height = 13
      Caption = '#days back'
    end
    object SpinEditCount: TSpinEdit
      Left = 308
      Top = 11
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 30
      MinValue = 1
      TabOrder = 0
      Value = 1
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 430
      Top = 11
      Width = 39
      Height = 22
      MaxValue = 15
      MinValue = 1
      TabOrder = 1
      Value = 5
    end
    object btnDateAdd: TButton
      Left = 566
      Top = 10
      Width = 80
      Height = 22
      Caption = 'Requery'
      TabOrder = 2
      OnClick = btnDateAddClick
    end
    object chkboxTicketVersionsSearch: TCheckBox
      Left = 31
      Top = 14
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 3
      OnClick = chkboxTicketVersionsSearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 766
    Height = 348
    inherited GridView: TcxGridDBTableView
      FindPanel.DisplayMode = fpdmManual
      object GridViewticket_id: TcxGridDBColumn
        Caption = 'ticket id'
        DataBinding.FieldName = 'ticket_id'
      end
      object GridViewhowmany: TcxGridDBColumn
        Caption = 'how many'
        DataBinding.FieldName = 'howmany'
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 396
    Width = 766
    Height = 76
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 90
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select ticket_id, count(source) As howmany from ticket_version w' +
      'ith (nolock)'#13#10' where cast(transmit_date as date)  between  '#13#10' ca' +
      'st(dateadd(d,  :StartDateOffset, getdate()) as date) and getdate' +
      '() '#13#10' group by ticket_id'#13#10' having count(source) > :count'#13#10
    Parameters = <
      item
        Name = 'StartDateOffset'
        Size = -1
        Value = Null
      end
      item
        Name = 'count'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = '1'
      end>
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from hal where hal_module = '#39'ticket_versions'#39
    Parameters = <>
    Left = 360
    Top = 80
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 432
    Top = 80
  end
end
