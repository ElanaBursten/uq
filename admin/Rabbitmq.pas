unit Rabbitmq;
//added to proj QM-987  sr

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, AdminDmu,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, odHourGlass, OdMiscUtils, dxSkinsCore, dxSkinBasic, dxCore, dxSkinsForm;

type
  TRabbitmqForm = class(TBaseCxListForm)
    GridViewqueue_name: TcxGridDBColumn;
    GridViewProcess_App: TcxGridDBColumn;
    GridViewactive: TcxGridDBColumn;
    GridViewhost: TcxGridDBColumn;
    GridViewport: TcxGridDBColumn;
    GridViewvirtual_host: TcxGridDBColumn;
    GridViewusername: TcxGridDBColumn;
    GridViewpassword: TcxGridDBColumn;
    GridViewuse_ssl: TcxGridDBColumn;
    GridViewexchange: TcxGridDBColumn;
    GridViewrouting_key: TcxGridDBColumn;
    GridViewreadlimit: TcxGridDBColumn;
    GridViewrequeue: TcxGridDBColumn;
    cbRabbitmqSearch: TCheckBox;
    ActiveQueuesFilter: TCheckBox;
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure cbRabbitmqSearchClick(Sender: TObject);
    procedure ActiveQueuesFilterClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    RabbitMQInserted: Boolean;
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure ActivatingNow; override;
    procedure EditNow; override;
  end;

var
  RabbitmqForm: TRabbitmqForm;

implementation

{$R *.dfm}

{ TRabbitmqForm }

procedure TRabbitmqForm.ActiveQueuesFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: Integer;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('mq_admin_id').AsInteger;
  Data.Close;
  if ActiveQueuesFilter.Checked then
    Data.CommandText := 'select * from rabbitmq_admin where active = 1' else
      Data.CommandText := 'select * from rabbitmq_admin';
  Data.Open;
  Data.Locate('mq_admin_id', FSelectedID, []);

end;

procedure TRabbitmqForm.cbRabbitmqSearchClick(Sender: TObject);
begin
  inherited;
    if cbRabbitmqSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TRabbitmqForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  RabbitMQInserted := False;
end;

procedure TRabbitmqForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'rabbitmq_admin', 'mq_admin_id', RabbitMQInserted);
end;

procedure TRabbitmqForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TRabbitmqForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  RabbitMQInserted := True;
end;

procedure TRabbitmqForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  Dataset.FieldByName('active').AsBoolean := True;
end;

procedure TRabbitmqForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[GridViewactive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TRabbitmqForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TRabbitmqForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
  RabbitMQInserted := False;
end;

procedure TRabbitmqForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TRabbitmqForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
   ActiveQueuesFilter.Enabled := True;
   cbRabbitMqSearch.Enabled := True;
end;

end.
