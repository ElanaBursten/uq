inherited AttachmentsForm: TAttachmentsForm
  Caption = 'Missing Attachments'
  ClientHeight = 540
  ClientWidth = 1064
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 457
    Width = 1064
    Height = 7
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 1064
    Height = 30
    object Label1: TLabel
      Left = 155
      Top = 7
      Width = 115
      Height = 13
      Caption = '#days from attach date'
    end
    object chkboxRespondToSearch: TCheckBox
      Left = 15
      Top = 7
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxRespondToSearchClick
    end
    object btnDateAdd: TButton
      Left = 321
      Top = 1
      Width = 80
      Height = 23
      Caption = 'Requery'
      TabOrder = 1
      OnClick = btnDateAddClick
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 276
      Top = 3
      Width = 39
      Height = 22
      MaxValue = 9
      MinValue = 1
      TabOrder = 2
      Value = 5
    end
    object btnEmails: TButton
      Left = 704
      Top = 1
      Width = 121
      Height = 25
      Caption = 'Send Emails Test'
      TabOrder = 3
      Visible = False
    end
  end
  inherited Grid: TcxGrid
    Top = 30
    Width = 1064
    Height = 427
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = True
      object ColShortName: TcxGridDBColumn
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 178
      end
      object ColContactPhone: TcxGridDBColumn
        DataBinding.FieldName = 'Phone'
        PropertiesClassName = 'TcxMaskEditProperties'
        Width = 125
      end
      object ColPending: TcxGridDBColumn
        DataBinding.FieldName = 'Pending'
        Width = 84
      end
    end
  end
  object WebBrowser1: TWebBrowser [3]
    Left = 640
    Top = 168
    Width = 393
    Height = 161
    TabOrder = 2
    ControlData = {
      4C0000009E280000A41000000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object cxGrid1: TcxGrid [4]
    Left = 0
    Top = 464
    Width = 1064
    Height = 76
    Align = alBottom
    TabOrder = 3
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 90
      end
      object ColDaysback: TcxGridDBColumn
        Caption = 'Days Back'
        DataBinding.FieldName = 'daysback'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 62
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select short_name, coalesce(e.contact_phone, '#39'No Phone'#39') as [Pho' +
      'ne], count(attach_date) as '#39'Pending'#39#13#10'from attachment a with (no' +
      'lock)'#13#10'join employee e on e.emp_id = a.attached_by'#13#10'where (attac' +
      'h_date between getdate()- :dateadd and getdate()-1)'#13#10' and upload' +
      '_date is null'#13#10'group by short_name, e.contact_phone'#13#10'order by sh' +
      'ort_name'
    Parameters = <
      item
        Name = 'dateadd'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 50
      end>
    Left = 64
    Top = 224
    object Datashort_name: TStringField
      DisplayLabel = 'Short Name'
      FieldName = 'short_name'
      Size = 30
    end
    object DataPhone: TStringField
      FieldName = 'Phone'
      ReadOnly = True
    end
    object DataPending: TIntegerField
      FieldName = 'Pending'
      ReadOnly = True
    end
  end
  inherited DS: TDataSource
    Left = 120
    Top = 224
  end
  object DataSetTableProducer: TDataSetTableProducer
    Columns = <
      item
        FieldName = 'short_name'
        Title.Align = haLeft
      end
      item
        FieldName = 'Phone'
        Title.Align = haLeft
      end
      item
        FieldName = 'Pending'
        Title.Align = haLeft
      end>
    MaxRows = -1
    DataSet = Data
    TableAttributes.CellSpacing = 0
    TableAttributes.CellPadding = 0
    Left = 720
    Top = 80
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select hal_module,  emails, daysback from hal '#13#10'where hal_module' +
      ' = '#39'missing_attachments'#39
    Parameters = <>
    Left = 560
    Top = 80
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 624
    Top = 80
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 480
    Top = 606
  end
end
