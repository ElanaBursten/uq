unit EPRQueueRules;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxDropDownEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges;

type
  TEPRQueueRulesForm = class(TBaseCxListForm)
    ColCallCenter: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColState: TcxGridDBColumn;
    ColStatusMessage: TcxGridDBColumn;
    cbQueueRulesSearch: TCheckBox;
    procedure DataBeforePost(DataSet: TDataSet);
    procedure cbQueueRulesSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    EPRQueueInserted: Boolean;
  public
    procedure PopulateDropDowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  EPRQueueRulesForm: TEPRQueueRulesForm;

implementation
uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TEPRQueueRulesForm }

procedure TEPRQueueRulesForm.cbQueueRulesSearchClick(Sender: TObject);
begin
  inherited;
  if cbQueueRulesSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TEPRQueueRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  EPRQueueInserted := False;
end;

procedure TEPRQueueRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'epr_queue_rules', 'epr_id', EPRQueueInserted);
end;

procedure TEPRQueueRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TEPRQueueRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  EPRQueueInserted := True;
end;

procedure TEPRQueueRulesForm.DataBeforePost(DataSet: TDataSet);
const
  Msg = 'Required Fields are missing. ' + #13#10;
  MsgReq = 'Required: Call Center, Client/Term Code, Status, State' + #13#10 + #13#10;
  Msg2 = 'Click OK to Continue or Abort to Cancel changes.';
begin
  inherited;
  if (DataSet.FieldByName('call_center').isNull) or
     (DataSet.FieldByName('client_code').isNull) or
     (DataSet.FieldByName('status').IsNull) or
     (DataSet.FieldByName('state').isNull) then begin
    {Cancel changes}
    If MessageDlg(Msg + MsgReq + Msg2, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
    end
    {Continue with changes, but don't try to post}
    else
      Abort;
  end;
//  else
//    DataSet.FieldByName('modified_date').AsDateTime := Now;

end;

procedure TEPRQueueRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEPRQueueRulesForm.ActivatingNow;
begin
  inherited;
   EPRQueueInserted := False;
end;

procedure TEPRQueueRulesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TEPRQueueRulesForm.PopulateDropDowns;
begin
  inherited;
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter));
  AdminDM.GetStatusList(CxComboBoxItems(ColStatus), True);
  AdminDM.GetClientList(CxComboBoxItems(ColClientCode));
  CxComboBoxItems(ColClientCode).Insert(0, '*');
  AdminDM.GetStateList(CxComboBoxItems(ColState));
  CxComboBoxItems(ColState).Insert(0, '*');
end;

procedure TEPRQueueRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbQueueRulesSearch.Enabled := True;
end;

end.
