unit AuditDueDateRules;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TAuditDueDateRulesForm = class(TBaseCxListForm)
    RuleIDCol: TcxGridDBColumn;
    AuditSourceCol: TcxGridDBColumn;
    TicketFormatCol: TcxGridDBColumn;
    HoursDueCol: TcxGridDBColumn;
    ModifiedDateCol: TcxGridDBColumn;
    ActiveCol: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ActiveAuditDueDateFilter: TCheckBox;
    procedure ActiveAuditDueDateFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    ActiveModified: Boolean;
    AuditRulesInserted: Boolean;
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  AuditDueDateRulesForm: TAuditDueDateRulesForm;

implementation

uses OdMiscUtils;

{$R *.dfm}


procedure TAuditDueDateRulesForm.ActiveAuditDueDateFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('rule_id').AsInteger;
  Data.Close;
  if ActiveAuditDueDateFilter.Checked then
    Data.CommandText := 'select * from audit_due_date_rule where active = 1' else
      Data.CommandText := 'select * from audit_due_date_rule';
  Data.Open;
  Data.Locate('rule_id', FSelectedID, []);
end;

procedure TAuditDueDateRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  AuditRulesInserted := False;
end;

procedure TAuditDueDateRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;  //QM 847 track reportto changes
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'audit_due_date_rule', 'rule_id', AuditRulesInserted);
end;

procedure TAuditDueDateRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TAuditDueDateRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  AuditRulesInserted := True;
end;

procedure TAuditDueDateRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TAuditDueDateRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TAuditDueDateRulesForm.ActivatingNow;
begin
  inherited;
  AuditRulesInserted := False;
end;

procedure TAuditDueDateRulesForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ActiveCol.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TAuditDueDateRulesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TAuditDueDateRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveAuditDueDateFilter.Enabled := True;
end;

end.
