inherited QtrMinPageMapForm: TQtrMinPageMapForm
  Left = 332
  Top = 224
  Caption = 'Quarter-Minute Page Map Setup'
  ClientHeight = 473
  ClientWidth = 770
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 306
    Width = 770
    Height = 8
    Cursor = crVSplit
    Align = alBottom
    ResizeStyle = rsUpdate
  end
  object TheGrid: TStringGrid [1]
    Left = 0
    Top = 41
    Width = 770
    Height = 265
    Align = alClient
    DefaultColWidth = 65
    DefaultRowHeight = 32
    Enabled = False
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goAlwaysShowEditor]
    TabOrder = 0
    OnDrawCell = TheGridDrawCell
  end
  object AreaGrid: TcxGrid [2]
    Left = 0
    Top = 314
    Width = 770
    Height = 159
    Align = alBottom
    TabOrder = 1
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object AreaGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = areaDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'area_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object AreaGridmap_name: TcxGridDBColumn
        Caption = 'Map Name'
        DataBinding.FieldName = 'map_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 30
        Properties.ReadOnly = True
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 293
      end
      object AreaGridstate: TcxGridDBColumn
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 5
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
      end
      object AreaGridcounty: TcxGridDBColumn
        DataBinding.FieldName = 'county'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 50
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
      end
      object AreaGridarea_id: TcxGridDBColumn
        DataBinding.FieldName = 'area_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
      end
      object AreaGridarea_name: TcxGridDBColumn
        Caption = 'Area Name'
        DataBinding.FieldName = 'area_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 40
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 246
      end
      object AreaGridlocator_id: TcxGridDBColumn
        DataBinding.FieldName = 'locator_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
      end
      object AreaGridEmpName: TcxGridDBColumn
        Caption = 'Emp Name'
        DataBinding.FieldName = 'EmpName'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 7
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListColumns = <
          item
            FieldName = 'short_name'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.MaxLength = 50
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 221
      end
    end
    object AreaGridLevel: TcxGridLevel
      GridView = AreaGridView
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 0
    Width = 770
    Height = 41
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 89
      Height = 28
      AutoSize = False
      Caption = 'Upper Left corner of page:'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 240
      Top = 12
      Width = 60
      Height = 13
      Caption = 'Latitude (Y):'
    end
    object Label3: TLabel
      Left = 104
      Top = 12
      Width = 68
      Height = 13
      Caption = 'Longitude (X):'
    end
    object Label4: TLabel
      Left = 368
      Top = 12
      Width = 55
      Height = 13
      Caption = 'Call center:'
    end
    object EditLong: TEdit
      Left = 176
      Top = 9
      Width = 57
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 5
      TabOrder = 0
    end
    object EditLat: TEdit
      Left = 304
      Top = 9
      Width = 57
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 5
      TabOrder = 1
    end
    object ShowPageButton: TButton
      Left = 536
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Show Page'
      TabOrder = 2
      OnClick = ShowPageButtonClick
    end
    object AssignAreaButton: TBitBtn
      Left = 624
      Top = 8
      Width = 137
      Height = 25
      Caption = 'Assign Area to Grid Cells'
      TabOrder = 3
      OnClick = AssignAreaButtonClick
    end
    object EditCallCenter: TComboBox
      Left = 432
      Top = 8
      Width = 81
      Height = 21
      TabOrder = 4
      Text = 'EditCallCenter'
    end
  end
  object GridData: TADODataSet [4]
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'select * from map_qtrmin'#13#10'where qtrmin_lat between :lat1 and :la' +
      't2'#13#10' and qtrmin_long between :long1 and :long2'#13#10' and call_center' +
      ' = :callcenter'#13#10'order by map_qtrmin.qtrmin_lat, map_qtrmin.qtrmi' +
      'n_long'
    Parameters = <
      item
        Name = 'lat1'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'lat2'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'long1'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'long2'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'callcenter'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 104
    Top = 232
  end
  object area: TADODataSet [5]
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select map.map_name, map.state, map.county, '#13#10' area.area_id, are' +
      'a.area_name,'#13#10' area.locator_id'#13#10' from area'#13#10'  inner join map on ' +
      'area.map_id=map.map_id'#13#10'order by area.area_name'
    Parameters = <>
    Left = 536
    Top = 136
    object areamap_name: TStringField
      FieldName = 'map_name'
      Size = 30
    end
    object areastate: TStringField
      FieldName = 'state'
      Size = 5
    end
    object areacounty: TStringField
      FieldName = 'county'
      Size = 50
    end
    object areaarea_id: TAutoIncField
      FieldName = 'area_id'
      ReadOnly = True
    end
    object areaarea_name: TStringField
      FieldName = 'area_name'
      Size = 40
    end
    object arealocator_id: TIntegerField
      FieldName = 'locator_id'
    end
    object areaEmpName: TStringField
      FieldKind = fkLookup
      FieldName = 'EmpName'
      LookupDataSet = locator
      LookupKeyFields = 'emp_id'
      LookupResultField = 'short_name'
      KeyFields = 'locator_id'
      Size = 50
      Lookup = True
    end
  end
  object areaDS: TDataSource [6]
    DataSet = area
    Left = 536
    Top = 176
  end
  object locator: TADODataSet [7]
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select emp_id, short_name from employee'#13#10'order by'#13#10'short_name'
    Parameters = <>
    Left = 592
    Top = 136
  end
  object locatorDs: TDataSource [8]
    DataSet = locator
    Left = 592
    Top = 176
  end
end
