inherited TicketInfoForm: TTicketInfoForm
  Caption = 'Ticket Info Questions'
  ClientHeight = 595
  ClientWidth = 1188
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Top = 128
    Width = 1188
    object TopLabel: TLabel
      Left = 4
      Top = 14
      Width = 4
      Height = 14
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  inherited Grid: TcxGrid
    Top = 0
    Width = 1188
    Height = 128
    Align = alTop
    Visible = False
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Buttons.Refresh.Visible = False
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      DataController.Filter.Active = True
      OptionsView.GroupByBox = True
      object GridViewRowNumCol: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'row_number'
        Width = 20
      end
      object GridViewSetDescCol: TcxGridDBColumn
        Caption = 'Set Description'
        DataBinding.FieldName = 'qh_description'
        Visible = False
        GroupIndex = 0
      end
      object GridViewQh_idCol: TcxGridDBColumn
        Caption = 'Set ID'
        DataBinding.FieldName = 'qh_id'
        Width = 46
      end
      object GridViewModDateCol: TcxGridDBColumn
        Caption = 'Mod Date'
        DataBinding.FieldName = 'modified_date'
      end
      object GridViewActiveCol: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
      end
      object GridViewClientIDCol: TcxGridDBColumn
        Caption = 'Client ID'
        DataBinding.FieldName = 'client_id'
        Width = 50
      end
      object GridViewClientName: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_name'
        Width = 86
      end
      object GridViewOCCodeCol: TcxGridDBColumn
        Caption = 'OC Code'
        DataBinding.FieldName = 'oc_code'
      end
      object GridViewCallCenterCol: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        Visible = False
        GroupIndex = 1
      end
      object GridViewqgd_idCol: TcxGridDBColumn
        Caption = 'qgd ID'
        DataBinding.FieldName = 'qgd_id'
      end
      object GridViewTStatusCol: TcxGridDBColumn
        Caption = 'Status Code'
        DataBinding.FieldName = 'tstatus'
        Width = 69
      end
      object GridViewItemActiveCol: TcxGridDBColumn
        Caption = 'Item Active'
        DataBinding.FieldName = 'item_active'
        PropertiesClassName = 'TcxCheckBoxProperties'
      end
      object GridViewqi_idCol: TcxGridDBColumn
        Caption = 'Question ID'
        DataBinding.FieldName = 'qi_id'
      end
      object GridViewQuestionCol: TcxGridDBColumn
        Caption = 'Question'
        DataBinding.FieldName = 'qi_description'
        Visible = False
        GroupIndex = 2
      end
      object GridViewQuestionCodeCol: TcxGridDBColumn
        Caption = 'Question'
        DataBinding.FieldName = 'info_type'
        Width = 145
      end
      object GridViewFormFieldCol: TcxGridDBColumn
        Caption = 'Form Field'
        DataBinding.FieldName = 'form_field'
        PropertiesClassName = 'TcxComboBoxProperties'
      end
    end
  end
  object TicketInfoPageControl: TPageControl [2]
    Left = 0
    Top = 169
    Width = 1188
    Height = 426
    ActivePage = QuestionsTab
    Align = alClient
    TabOrder = 2
    OnChange = TicketInfoPageControlChange
    object QuestionsTab: TTabSheet
      Caption = 'All Questions'
      ImageIndex = 2
      object ClientAssignmentsGrid: TcxGrid
        Left = 0
        Top = 26
        Width = 1180
        Height = 372
        Align = alClient
        TabOrder = 0
        OnExit = ClientAssignmentsGridExit
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object ClientAssignmentsGridDBTableView: TcxGridDBTableView
          Navigator.Buttons.ConfirmDelete = True
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Enabled = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.NextPage.Enabled = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = True
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = True
          Navigator.Buttons.Cancel.Visible = True
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Enabled = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Enabled = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.Visible = True
          FilterBox.CustomizeDialog = False
          FilterBox.Position = fpTop
          FindPanel.DisplayMode = fpdmManual
          FindPanel.ShowCloseButton = False
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = GridViewCustomDrawCell
          DataController.DataSource = QuestionTabDS
          DataController.Filter.Active = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          object Questqi_idCol: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'qi_id'
            Options.Editing = False
            Options.Focusing = False
            Width = 50
          end
          object QuestQIDescCol: TcxGridDBColumn
            Caption = 'Question'
            DataBinding.FieldName = 'qi_description'
            Width = 318
          end
          object QuestInfoTypeCol: TcxGridDBColumn
            Caption = 'Info Type'
            DataBinding.FieldName = 'info_type'
            Width = 74
          end
          object QuestFormFieldCol: TcxGridDBColumn
            Caption = 'Form Field'
            DataBinding.FieldName = 'form_field'
            PropertiesClassName = 'TcxComboBoxProperties'
            Width = 77
          end
          object QuestActiveCol: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Options.Filtering = False
            Width = 49
          end
          object QuestModDateCol: TcxGridDBColumn
            Caption = 'Mod Date'
            DataBinding.FieldName = 'modified_date'
            Options.Editing = False
            Options.Focusing = False
            Width = 157
          end
        end
        object ClientAssignmentsGridLevel: TcxGridLevel
          GridView = ClientAssignmentsGridDBTableView
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1180
        Height = 26
        Align = alTop
        TabOrder = 1
        object ActiveAllQuestionsFilter: TCheckBox
          Left = 124
          Top = 3
          Width = 143
          Height = 15
          Caption = 'Active Questions Only'
          Checked = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnClick = ActiveAllQuestionsFilterClick
        end
        object cbAllQuestionsSearch: TCheckBox
          Left = 13
          Top = 4
          Width = 97
          Height = 14
          Caption = 'Show Find Panel'
          TabOrder = 1
          OnClick = cbAllQuestionsSearchClick
        end
      end
    end
    object HeaderTab: TTabSheet
      Caption = 'Question Groups (Header)'
      ImageIndex = 1
      object HeaderGrid: TcxGrid
        Left = 0
        Top = 26
        Width = 1180
        Height = 372
        Align = alClient
        TabOrder = 0
        OnExit = HeaderGridExit
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object HeaderGridView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Enabled = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.NextPage.Enabled = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = True
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Visible = True
          Navigator.Buttons.Cancel.Visible = True
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Enabled = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Enabled = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.Visible = True
          FilterBox.CustomizeDialog = False
          FilterBox.Position = fpTop
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = GridViewCustomDrawCell
          DataController.DataSource = QuestHeaderDS
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          object HeaderGridViewqh_id: TcxGridDBColumn
            Caption = 'qh ID'
            DataBinding.FieldName = 'qh_id'
            Options.Editing = False
            Options.Focusing = False
          end
          object HeaderGridViewqh_description: TcxGridDBColumn
            Caption = 'Description'
            DataBinding.FieldName = 'qh_description'
            Width = 355
          end
          object HeaderGridViewactive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Options.Filtering = False
            Width = 53
          end
          object HeaderGridViewmodified_date: TcxGridDBColumn
            Caption = 'Mod Date'
            DataBinding.FieldName = 'modified_date'
            Options.Editing = False
            Options.Focusing = False
            Width = 205
          end
        end
        object HeaderGridLevel: TcxGridLevel
          GridView = HeaderGridView
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1180
        Height = 26
        Align = alTop
        TabOrder = 1
        object ActiveQuestionHeaderFilter: TCheckBox
          Left = 15
          Top = 4
          Width = 135
          Height = 15
          Caption = 'Active Headers Only'
          Checked = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnClick = ActiveQuestionHeaderFilterClick
        end
      end
    end
    object MapQuestionsTabSheet: TTabSheet
      Caption = 'Map Questions'
      object QuestGroupDetailGrid: TcxGrid
        Left = 0
        Top = 26
        Width = 1180
        Height = 372
        Align = alClient
        TabOrder = 0
        OnExit = QuestGroupDetailGridExit
        object QuestGroupDetailView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmManual
          FindPanel.ShowCloseButton = False
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = GridViewCustomDrawCell
          DataController.DataSource = QuestGroupDetailDS
          DataController.Filter.Active = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          OptionsView.ColumnAutoWidth = True
          object QuestGroupDetailViewqgd_id: TcxGridDBColumn
            DataBinding.FieldName = 'qgd_id'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            Options.Focusing = False
            Width = 83
          end
          object QuestGroupDetailViewqh_id: TcxGridDBColumn
            Caption = 'QHeader ID (qh)'
            DataBinding.FieldName = 'qh_id'
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.IncrementalFiltering = False
            Properties.View = QuestGroupHeaderLookupView
            Properties.KeyFieldNames = 'qh_id'
            Properties.ListFieldItem = QuestGroupHeaderLookupViewdescription
            Options.IncSearch = False
            Width = 198
          end
          object QuestGroupDetailViewqi_id: TcxGridDBColumn
            Caption = 'QInfo ID (qi)'
            DataBinding.FieldName = 'qi_id'
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.DropDownAutoSize = True
            Properties.IncrementalFiltering = False
            Properties.View = QuestGroupQuestionLookup
            Properties.KeyFieldNames = 'qi_id'
            Properties.ListFieldItem = QuestGroupQuestionLookupdescription
            Width = 213
          end
          object QuestGroupDetailViewtstatus: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'tstatus'
            PropertiesClassName = 'TcxComboBoxProperties'
            Width = 136
          end
          object QuestGroupDetailViewactive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            Options.Filtering = False
            Width = 150
          end
          object QuestGroupDetailViewmodified_date: TcxGridDBColumn
            Caption = 'Mod Date'
            DataBinding.FieldName = 'modified_date'
            Options.Editing = False
            Options.Focusing = False
            Width = 398
          end
        end
        object QuestGroupHeaderLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = QuestHeaderLookupDS
          DataController.KeyFieldNames = 'qh_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsView.GroupByBox = False
          object QuestGroupHeaderLookupViewqh_id: TcxGridDBColumn
            DataBinding.FieldName = 'qh_id'
            PropertiesClassName = 'TcxMaskEditProperties'
            Visible = False
          end
          object QuestGroupHeaderLookupViewdescription: TcxGridDBColumn
            DataBinding.FieldName = 'qh_description'
            PropertiesClassName = 'TcxMaskEditProperties'
          end
        end
        object QuestGroupQuestionLookup: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = QuestionLookupDS
          DataController.KeyFieldNames = 'qi_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = QuestGroupQuestionLookupdescription
          OptionsView.GroupByBox = False
          object QuestGroupQuestionLookupdescription: TcxGridDBColumn
            Caption = 'Description'
            DataBinding.FieldName = 'qi_description'
            PropertiesClassName = 'TcxMaskEditProperties'
          end
          object QuestGroupQuestionLookupinfo_type: TcxGridDBColumn
            Caption = 'Info Type'
            DataBinding.FieldName = 'info_type'
            PropertiesClassName = 'TcxMaskEditProperties'
          end
          object QuestGroupQuestionLookupform_field: TcxGridDBColumn
            Caption = 'Form Field'
            DataBinding.FieldName = 'form_field'
          end
        end
        object QuestGroupDetailLevel: TcxGridLevel
          GridView = QuestGroupDetailView
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1180
        Height = 26
        Align = alTop
        TabOrder = 1
        object ActiveQuestionDetailFilter: TCheckBox
          Left = 146
          Top = 4
          Width = 151
          Height = 15
          Caption = 'Active Details Only'
          Checked = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnClick = ActiveQuestionDetailFilterClick
        end
        object cbQuestionDetailSearch: TCheckBox
          Left = 18
          Top = 5
          Width = 97
          Height = 14
          Caption = 'Show Find Panel'
          TabOrder = 1
          OnClick = cbQuestionDetailSearchClick
        end
      end
    end
    object AssignClientsTabSheet: TTabSheet
      Caption = 'Assign Clients'
      ImageIndex = 1
      object QuestionsPanel: TPanel
        Left = 0
        Top = 0
        Width = 412
        Height = 398
        Align = alLeft
        TabOrder = 0
        object QuestionHeaderGrid: TcxGrid
          Left = 1
          Top = 27
          Width = 410
          Height = 370
          Align = alClient
          TabOrder = 0
          object QHGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = False
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.Prior.Visible = True
            Navigator.Buttons.Next.Visible = True
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Last.Visible = False
            Navigator.Buttons.Insert.Visible = True
            Navigator.Buttons.Append.Visible = True
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Visible = True
            ScrollbarAnnotations.CustomAnnotations = <>
            OnCellClick = QHGridViewCellClick
            OnCustomDrawCell = GridViewCustomDrawCell
            DataController.DataSource = QuestHeaderDS
            DataController.Filter.Active = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            object QHGridViewqh_id: TcxGridDBColumn
              DataBinding.FieldName = 'qh_id'
              Options.Editing = False
              Width = 39
            end
            object QHGridViewqh_description: TcxGridDBColumn
              DataBinding.FieldName = 'qh_description'
              Width = 205
            end
            object QHGridViewmodified_date: TcxGridDBColumn
              DataBinding.FieldName = 'modified_date'
              Options.Editing = False
              Width = 121
            end
            object QHGridViewactive: TcxGridDBColumn
              DataBinding.FieldName = 'active'
              Options.Filtering = False
              Width = 42
            end
          end
          object QHGridLevel: TcxGridLevel
            GridView = QHGridView
          end
        end
        object QuestionSetsPanel: TPanel
          Left = 1
          Top = 1
          Width = 410
          Height = 26
          Align = alTop
          TabOrder = 1
          object QuestionSetsLabel: TLabel
            Left = 0
            Top = 4
            Width = 81
            Height = 14
            Caption = 'Question Sets:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object ActiveQuestionsFilter: TCheckBox
            Left = 200
            Top = 5
            Width = 140
            Height = 15
            Caption = 'Active Questions Only'
            Checked = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 0
            OnClick = ActiveQuestionsFilterClick
          end
        end
      end
      object cxSplitter2: TcxSplitter
        Left = 412
        Top = 0
        Width = 7
        Height = 398
        Control = QuestionsPanel
      end
      object AssignPanel: TPanel
        Left = 419
        Top = 0
        Width = 761
        Height = 398
        Align = alClient
        TabOrder = 2
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 759
          Height = 26
          Align = alTop
          TabOrder = 0
          object AssignQuestionsLabel: TLabel
            Left = 5
            Top = 4
            Width = 92
            Height = 14
            Caption = 'Assign Questions'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cbAssignClientSearch: TCheckBox
            Left = 389
            Top = 6
            Width = 104
            Height = 12
            Caption = 'Show Find Panel'
            TabOrder = 0
            OnClick = cbAssignClientSearchClick
          end
          object ActiveAssignClientFilter: TCheckBox
            Left = 605
            Top = 2
            Width = 129
            Height = 21
            Caption = 'Active Clients Only'
            Checked = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 1
            OnClick = ActiveAssignClientFilterClick
          end
        end
        object ClientGrid: TcxGrid
          Left = 1
          Top = 27
          Width = 759
          Height = 370
          Align = alClient
          TabOrder = 1
          object ClientGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Append.Enabled = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Visible = True
            FindPanel.DisplayMode = fpdmManual
            FindPanel.ShowCloseButton = False
            ScrollbarAnnotations.CustomAnnotations = <>
            OnCellClick = ClientGridViewCellClick
            OnCustomDrawCell = GridViewCustomDrawCell
            DataController.DataSource = AssignClientDS
            DataController.Filter.Active = True
            DataController.KeyFieldNames = 'client_id'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            object ClientGridViewCalcChecked: TcxGridDBColumn
              DataBinding.FieldName = 'CalcChecked'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Options.Editing = False
              Options.Filtering = False
              Width = 31
              IsCaptionAssigned = True
            end
            object ClientGridViewclient_id: TcxGridDBColumn
              DataBinding.FieldName = 'client_id'
              Width = 55
            end
            object ClientGridViewclient_name: TcxGridDBColumn
              DataBinding.FieldName = 'client_name'
              Width = 215
            end
            object ClientGridViewoc_code: TcxGridDBColumn
              DataBinding.FieldName = 'oc_code'
              Width = 83
            end
            object ClientGridViewmodified_date: TcxGridDBColumn
              DataBinding.FieldName = 'modified_date'
              Width = 118
            end
            object ClientGridViewcall_center: TcxGridDBColumn
              DataBinding.FieldName = 'call_center'
              Width = 90
            end
            object ClientGridViewutility_type: TcxGridDBColumn
              DataBinding.FieldName = 'utility_type'
              Width = 72
            end
            object ClientGridViewqh_id: TcxGridDBColumn
              DataBinding.FieldName = 'qh_id'
              Width = 46
            end
            object ClientGridViewactive: TcxGridDBColumn
              DataBinding.FieldName = 'active'
              Options.Filtering = False
              Width = 50
            end
          end
          object ClientGridLevel: TcxGridLevel
            GridView = ClientGridView
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter [3]
    Left = 0
    Top = 169
    Width = 1188
    Height = 0
    AlignSplitter = salTop
    Control = TicketInfoPageControl
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ROW_NUMBER() OVER(ORDER BY qi_description) AS '#39'row_number' +
      #39', '#13#10'             qh.*, c.client_id, c.client_name, c.oc_code, c' +
      '.call_center, '#13#10'             qgd.qgd_id, qgd.tstatus, qgd.active' +
      ' item_active,'#13#10#9#9#9' qi.qi_id, qi.qi_description, qi.info_type, qi' +
      '.form_field '#13#10'from question_header qh '#13#10'left outer join client c' +
      ' '#13#10'  on qh.qh_id = c.qh_id '#13#10'left outer join question_group_deta' +
      'il qgd'#13#10'  on c.qh_id = qgd.qh_id'#13#10'left outer join question_info ' +
      'qi'#13#10'  on qgd.qi_id = qi.qi_id'#13#10'where c.active = 1'
  end
  object QuestionTabData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = QuestionTabDataBeforeInsert
    BeforeEdit = QuestionTabDataBeforeEdit
    BeforePost = QuestionTabDataBeforePost
    AfterPost = QuestionTabDataAfterPost
    AfterCancel = QuestionTabDataAfterCancel
    BeforeDelete = DataBeforeDelete
    OnNewRecord = QuestionTabDataNewRecord
    CommandText = 'select * from question_info where active = 1'
    Parameters = <>
    Left = 40
    Top = 288
  end
  object QuestionTabDS: TDataSource
    DataSet = QuestionTabData
    Left = 136
    Top = 286
  end
  object QuestHeaderData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = QuestHeaderDataBeforeInsert
    BeforeEdit = QuestHeaderDataBeforeEdit
    BeforePost = QuestHeaderDataBeforePost
    AfterPost = QuestHeaderDataAfterPost
    AfterCancel = QuestHeaderDataAfterCancel
    BeforeDelete = DataBeforeDelete
    OnNewRecord = QuestHeaderDataNewRecord
    CommandText = ' Select * from question_header where active = 1'
    Parameters = <>
    Left = 40
    Top = 350
  end
  object QuestHeaderDS: TDataSource
    DataSet = QuestHeaderData
    OnDataChange = QuestHeaderDSDataChange
    Left = 144
    Top = 339
  end
  object AssignClient: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    OnCalcFields = AssignClientCalcFields
    CommandText = 
      '  Select  c.client_id, c.client_name, c.oc_code, c.modified_date' +
      ', '#13#10'  c.call_center, c.utility_type, c.qh_id, c.active'#13#10'  From c' +
      'lient c where c.active = 1'
    DataSource = QuestHeaderDS
    Parameters = <>
    Left = 32
    Top = 456
    object AssignClientCalcChecked: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CalcChecked'
      Calculated = True
    end
    object AssignClientclient_id: TAutoIncField
      FieldName = 'client_id'
      ReadOnly = True
    end
    object AssignClientclient_name: TStringField
      FieldName = 'client_name'
      Size = 40
    end
    object AssignClientoc_code: TStringField
      FieldName = 'oc_code'
      Size = 10
    end
    object AssignClientmodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object AssignClientcall_center: TStringField
      FieldName = 'call_center'
    end
    object AssignClientutility_type: TStringField
      FieldName = 'utility_type'
      Size = 15
    end
    object AssignClientqh_id: TIntegerField
      FieldName = 'qh_id'
    end
    object AssignClientactive: TBooleanField
      FieldName = 'active'
    end
  end
  object AssignClientDS: TDataSource
    DataSet = AssignClient
    Left = 136
    Top = 454
  end
  object AssignClientCommand: TADOCommand
    CommandText = 
      '  update client'#13#10'  set qh_id =:HeaderID'#13#10'  where client_id =:Cli' +
      'entID'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'HeaderId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ClientId'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 504
    Top = 288
  end
  object QuestGroupDetail: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = QuestGroupDetailBeforeInsert
    BeforeEdit = QuestGroupDetailBeforeEdit
    BeforePost = QuestGroupDetailBeforePost
    AfterPost = QuestGroupDetailAfterPost
    AfterCancel = QuestGroupDetailAfterCancel
    BeforeDelete = DataBeforeDelete
    OnNewRecord = QuestGroupDetailNewRecord
    CommandText = 
      ' Select * '#13#10'from question_group_detail where active = 1'#13#10'order b' +
      'y qh_id'
    Parameters = <>
    Left = 40
    Top = 400
  end
  object QuestGroupDetailDS: TDataSource
    DataSet = QuestGroupDetail
    OnDataChange = QuestHeaderDSDataChange
    Left = 144
    Top = 397
  end
  object QuestionLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'Select * from question_info'
    Parameters = <>
    Left = 624
    Top = 336
    object QuestionLookupqi_id: TAutoIncField
      FieldName = 'qi_id'
      ReadOnly = True
    end
    object QuestionLookupqi_description: TStringField
      DisplayWidth = 30
      FieldName = 'qi_description'
      Size = 255
    end
    object QuestionLookupinfo_type: TStringField
      DisplayWidth = 10
      FieldName = 'info_type'
      Size = 8
    end
    object QuestionLookupform_field: TStringField
      DisplayWidth = 10
      FieldName = 'form_field'
      Size = 8
    end
  end
  object QuestionLookupDS: TDataSource
    DataSet = QuestionLookup
    OnDataChange = QuestHeaderDSDataChange
    Left = 760
    Top = 336
  end
  object LookupStatus: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'Select * from question_info'
    Parameters = <>
    Left = 504
    Top = 336
    object AutoIncField1: TAutoIncField
      FieldName = 'qi_id'
      ReadOnly = True
    end
    object StringField1: TStringField
      DisplayWidth = 30
      FieldName = 'qi_description'
      Size = 255
    end
    object StringField2: TStringField
      DisplayWidth = 10
      FieldName = 'info_type'
      Size = 8
    end
    object StringField3: TStringField
      DisplayWidth = 10
      FieldName = 'form_field'
      Size = 8
    end
  end
  object QryNextID: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'MaxID'
        DataType = ftInteger
        Direction = pdOutput
        Value = Null
      end>
    Left = 856
    Top = 280
  end
  object QuestHeaderLookupData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforePost = QuestHeaderDataBeforePost
    BeforeDelete = DataBeforeDelete
    OnNewRecord = QuestHeaderDataNewRecord
    CommandText = ' Select * from question_header'
    Parameters = <>
    Left = 632
    Top = 286
  end
  object QuestHeaderLookupDS: TDataSource
    DataSet = QuestHeaderLookupData
    OnDataChange = QuestHeaderDSDataChange
    Left = 760
    Top = 283
  end
end
