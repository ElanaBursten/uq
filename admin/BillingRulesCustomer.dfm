inherited BillingRulesCustomerForm: TBillingRulesCustomerForm
  Caption = 'Billing Rules Customer '
  ClientHeight = 450
  ClientWidth = 1333
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 411
    Width = 8
    Height = 413
  end
  inherited HeaderPanel: TPanel
    Width = 1333
    object ActiveCustomerFilter: TCheckBox
      Left = 12
      Top = 15
      Width = 149
      Height = 14
      Caption = 'Active Customer Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveCustomerFilterClick
    end
    object chkboxBillingRulesCustSearch: TCheckBox
      Left = 167
      Top = 17
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = chkboxBillingRulesCustSearchClick
    end
  end
  inherited MasterPanel: TPanel
    Width = 411
    Height = 413
    inherited MasterGrid: TcxGrid
      Width = 405
      Height = 356
      inherited MasterGridView: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmManual
        OnCustomDrawCell = MasterGridViewCustomDrawCell
        DataController.KeyFieldNames = 'rule_id'
        object ColMasterCustomerID: TcxGridDBColumn
          Caption = 'Customer ID'
          DataBinding.FieldName = 'customer_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
          Width = 67
        end
        object ColMasterCustomerName: TcxGridDBColumn
          Caption = 'Customer Name'
          DataBinding.FieldName = 'customer_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxExtLookupComboBoxProperties'
          Properties.View = CustLookupView
          Properties.KeyFieldNames = 'customer_id'
          Properties.ListFieldItem = CustLookupViewCustName
          Width = 180
        end
        object ColMasterRuleID: TcxGridDBColumn
          Caption = 'Rule ID'
          DataBinding.FieldName = 'rule_id'
          DataBinding.IsNullValueType = True
          Width = 47
        end
        object ColMasterRuleStatus: TcxGridDBColumn
          Caption = 'Rule Status'
          DataBinding.FieldName = 'rule_status'
          DataBinding.IsNullValueType = True
          Width = 65
        end
        object ColMasterActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
        end
      end
      object CustLookupView: TcxGridDBTableView [1]
        Navigator.Buttons.CustomButtons = <>
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsLookupCust
        DataController.KeyFieldNames = 'customer_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object CustLookupViewCustomerID: TcxGridDBColumn
          Caption = 'Customer ID'
          DataBinding.FieldName = 'customer_id'
          DataBinding.IsNullValueType = True
        end
        object CustLookupViewCustName: TcxGridDBColumn
          Caption = 'Customer  Name'
          DataBinding.FieldName = 'Cust  Name'
          DataBinding.IsNullValueType = True
          Width = 300
        end
      end
    end
  end
  inherited DetailPanel: TPanel
    Left = 419
    Width = 914
    Height = 413
    inherited DetailGrid: TcxGrid
      Left = 0
      Width = 873
      Height = 358
      inherited DetailGridView: TcxGridDBTableView
        OnCustomDrawCell = DetailGridViewCustomDrawCell
        DataController.KeyFieldNames = 'rule_id'
        object ColDetailRuleID: TcxGridDBColumn
          Caption = 'Rule ID'
          DataBinding.FieldName = 'rule_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
          Width = 48
        end
        object ColDetailRuleExecutionSequence: TcxGridDBColumn
          Caption = 'Rule Execution Sequence'
          DataBinding.FieldName = 'rule_execution_sequence'
          Width = 94
        end
        object ColDetailRuleHeaderDescription: TcxGridDBColumn
          Caption = 'Rule Header Description'
          DataBinding.FieldName = 'rule_header_description'
          Width = 124
        end
        object ColDetailRuleDetailDescription: TcxGridDBColumn
          Caption = 'Rule Detail Description'
          DataBinding.FieldName = 'rule_detail_description'
          Width = 88
        end
        object ColDetalRuleSql: TcxGridDBColumn
          Caption = 'Rule SQL'
          DataBinding.FieldName = 'rule_sql'
          Width = 53
        end
        object ColDetailRuleParameters: TcxGridDBColumn
          Caption = 'Rule Parameters'
          DataBinding.FieldName = 'rule_parameters'
          Width = 92
        end
        object ColDetailShowColumns: TcxGridDBColumn
          Caption = 'Show Columns'
          DataBinding.FieldName = 'show_columns'
          Width = 81
        end
        object ColDetailEditColumns: TcxGridDBColumn
          Caption = 'Edit Columns'
          DataBinding.FieldName = 'edit_columns'
          Width = 65
        end
        object ColDetailRuleButtonText: TcxGridDBColumn
          Caption = 'Rule Button Text'
          DataBinding.FieldName = 'rule_button_text'
          Width = 95
        end
        object ColDtailUpdateBillingSql: TcxGridDBColumn
          Caption = 'Update Billing SQL'
          DataBinding.FieldName = 'update_billing_sql'
          Width = 79
        end
        object ColDetailUpdateLocateSql: TcxGridDBColumn
          Caption = 'Update Locate SQL'
          DataBinding.FieldName = 'update_locate_sql'
          Width = 87
        end
        object ColDetailActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          Width = 74
        end
      end
    end
  end
  inherited Master: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = MasterBeforeInsert
    BeforeEdit = MasterBeforeEdit
    BeforePost = MasterBeforePost
    AfterPost = MasterAfterPost
    AfterCancel = MasterAfterCancel
    CommandText = 
      'select * from billing_rules_customer where active = 1 order by c' +
      'ustomer_id, rule_id'
    Top = 176
  end
  inherited Detail: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DetailBeforeInsert
    BeforeEdit = DetailBeforeEdit
    BeforePost = DetailBeforePost
    AfterPost = DetailAfterPost
    AfterCancel = DetailAfterCancel
    OnNewRecord = DetailNewRecord
    CommandText = 'select * from billing_rules_detail'
    IndexFieldNames = 'rule_id'
    MasterFields = 'rule_id'
    Left = 168
    Top = 180
    object Detailrule_id: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'rule_id'
    end
    object Detailrule_execution_sequence: TIntegerField
      FieldName = 'rule_execution_sequence'
    end
    object Detailrule_header_description: TStringField
      FieldName = 'rule_header_description'
      Size = 500
    end
    object Detailrule_detail_description: TMemoField
      FieldName = 'rule_detail_description'
      BlobType = ftMemo
    end
    object Detailrule_sql: TMemoField
      FieldName = 'rule_sql'
      BlobType = ftMemo
    end
    object Detailrule_parameters: TStringField
      FieldName = 'rule_parameters'
      Size = 2000
    end
    object Detailshow_columns: TMemoField
      FieldName = 'show_columns'
      BlobType = ftMemo
    end
    object Detailedit_columns: TMemoField
      FieldName = 'edit_columns'
      BlobType = ftMemo
    end
    object Detailactive: TBooleanField
      FieldName = 'active'
    end
    object Detailmodified_by: TStringField
      FieldName = 'modified_by'
    end
    object Detailmodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object Detailrule_button_text: TStringField
      FieldName = 'rule_button_text'
      Size = 25
    end
    object Detailupdate_billing_sql: TMemoField
      FieldName = 'update_billing_sql'
      BlobType = ftMemo
    end
    object Detailupdate_locate_sql: TMemoField
      FieldName = 'update_locate_sql'
      BlobType = ftMemo
    end
  end
  inherited MasterSource: TDataSource
    Top = 200
  end
  inherited DetailSource: TDataSource
    Left = 228
    Top = 164
  end
  inherited ActionList: TActionList
    Left = 836
  end
  object dsLookupCust: TDataSource
    DataSet = qryLookupCust
    Left = 472
    Top = 169
  end
  object qryLookupCust: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select customer_id, customer_name as [Cust  Name] from customer'
      'where active = 1 '
      ' order by customer_name')
    Left = 472
    Top = 217
  end
end
