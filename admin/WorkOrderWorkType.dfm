inherited WorkOrderWorkTypeForm: TWorkOrderWorkTypeForm
  Caption = 'Work Order Work Types'
  ClientHeight = 478
  ClientWidth = 771
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 771
    Height = 27
    object chkboxWorkTypesSearch: TCheckBox
      Left = 26
      Top = 5
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxWorkTypesSearchClick
    end
    object ActiveWorkTypesFilter: TCheckBox
      Left = 149
      Top = 7
      Width = 164
      Height = 14
      Caption = 'Active Work Types Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveWorkTypesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 27
    Width = 771
    Height = 451
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'work_type_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object GridViewwork_type_id: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'work_type_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Width = 63
      end
      object GridViewwork_type: TcxGridDBColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 106
      end
      object GridViewwork_description: TcxGridDBColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 340
      end
      object GridViewflag_color: TcxGridDBColumn
        Caption = 'Flag Color'
        DataBinding.FieldName = 'flag_color'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'test'
          'blah'
          'none')
        Width = 106
      end
      object GridViewWO_source: TcxGridDBColumn
        Caption = 'Source'
        DataBinding.FieldName = 'wo_source'
        Width = 98
      end
      object GridViewalert: TcxGridDBColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 63
      end
      object GridViewactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 61
      end
      object GridViewmodified_date: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Width = 150
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from work_order_work_type where active = 1'
  end
end
