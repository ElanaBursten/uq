unit GridAxis;

interface

uses
  SysUtils, Classes;

type
  TAxis = class
  private
    FMin, FMax: Integer;
  public
    function IndexToCode(i: Integer): string; virtual;
    function CodeToIndex(code: string): Integer; virtual; abstract;
    function Size: Integer;
  end;

  TNumAxis = class(TAxis)
    function IndexToCode(i: Integer): string; override;
    function CodeToIndex(code: string): Integer; override;
    constructor Create(MinVal, MaxVal: string);
  end;

  TAlphaAxis = class(TAxis)
  public
    function LettersToInt(s: string): Integer;
    function IntToLetters(i: Integer): string;
    function IndexToCode(i: Integer): string; override;
    function CodeToIndex(code: string): Integer; override;
    constructor Create(MinVal, MaxVal: string);
  end;

  TQtrAxis = class(TAxis)
  public
    class function QtrToInt(const s: string): Integer;
    class function IntToQtr(const i: Integer): string;
    function IndexToCode(i: Integer): string; override;
    function CodeToIndex(code: string): Integer; override;
    procedure BufferEdges;
    constructor Create(MinVal, MaxVal: string);
    constructor CreateSize(MinVal: string; Size: Integer);
  end;

  TExplicitAxis = class(TAxis)
  private
    FValues: TStrings;
  public
    function IndexToCode(i: Integer): string; override;
    function CodeToIndex(code: string): Integer; override;
    procedure AddValue(S: string);
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TAxis }

function TAxis.Size: Integer;
begin
  Result := FMax - FMin + 1;
end;

function TAxis.IndexToCode(i: Integer): string;
begin
  if (i<1) or (i>Size) then
    raise Exception.Create('tried to convert index out of range: ' + intToStr(I));
end;

{ TAlphaAxis }

function TAlphaAxis.CodeToIndex(code: string): Integer;
begin
  Result := LettersToInt(code) - FMin + 1;
end;

constructor TAlphaAxis.Create(MinVal, MaxVal: string);
begin
  FMin := LettersToInt(MinVal);
  FMax := LettersToInt(MaxVal);
end;

function TAlphaAxis.IndexToCode(i: Integer): string;
begin
  Result := IntToLetters(i + FMin - 1)
end;

function TAlphaAxis.IntToLetters(i: Integer): string;

  function FirstDigit(I: Integer): string;
  var
    LetterBase0: Integer;
  begin
    if I<27 then
      Result := ''
    else begin
      LetterBase0 := ((i-1) div 26) - 1;  // -1 because its ' ABC' not 'ABC'
      Result := Chr(LetterBase0 + Ord('A'));
    end;
  end;

  function SecondDigit(I: Integer): string;
  var
    LetterBase0: Integer;
  begin
    LetterBase0 := ((i-1) mod 26);
    Result := Chr(LetterBase0 + Ord('A'));
  end;

begin
  Result := FirstDigit(i) + SecondDigit(i);
end;

function TAlphaAxis.LettersToInt(s: string): Integer;
var
  C: char;
  I: Integer;
begin
  Result := 0;

  if S = '' then
    raise Exception.Create('Empty column code');

  for I := 1 to Length(S) do begin
    Result := Result * 26;
    C := S[I];
    if (C >= 'A') and (C <= 'Z') then
      Result := Result + Ord(C) - Ord('A') + 1
    else
      raise Exception.Create('Code contained a non alpha character ' + C);
  end;

  if Result = 0 then
    raise Exception.Create('Column code could not be converted in to an Integer: ' + S);
end;


{ TNumAxis }

function TNumAxis.CodeToIndex(code: string): Integer;
begin
  Result := StrToInt(code) - FMin + 1;
end;

constructor TNumAxis.Create(MinVal, MaxVal: string);
begin
  FMin := StrToInt(MinVal);
  FMax := StrToInt(MaxVal);
end;

function TNumAxis.IndexToCode(i: Integer): string;
begin
  Result := IntToStr(i + Fmin - 1)
end;

{ TExplicitAxis }

procedure TExplicitAxis.AddValue(S: string);
begin
  FValues.Add(S);
  FMax := FValues.Count - 1;
end;

function TExplicitAxis.CodeToIndex(code: string): Integer;
begin
  Result := FValues.IndexOf(code) + 1;
end;

constructor TExplicitAxis.Create;
begin
  FValues := TStringList.Create;
  FMin := 0;
  FMax := -1;
end;

destructor TExplicitAxis.Destroy;
begin
  FreeAndNil(FValues);
  inherited;
end;

function TExplicitAxis.IndexToCode(i: Integer): string;
begin
  if (i<1) or (i>Size) then
    raise Exception.Create('tried to convert index out of range: ' + intToStr(I));

  Result := FValues[i-1];
end;

{ TQtrAxis }

procedure TQtrAxis.BufferEdges;
begin
  FMin := FMin - 4;
  FMax := FMax + 4;
end;

function TQtrAxis.CodeToIndex(code: string): Integer;
begin
  Result := QtrToInt(code) - FMin + 1;
end;

constructor TQtrAxis.Create(MinVal, MaxVal: string);
begin
  FMin := QtrToInt(MinVal);
  FMax := QtrToInt(MaxVal);
end;

constructor TQtrAxis.CreateSize(MinVal: string; Size: Integer);
begin
  FMin := QtrToInt(MinVal);
  FMax := FMin + Size - 1;
end;

function TQtrAxis.IndexToCode(i: Integer): string;
begin
  Result := IntToQtr(i + FMin - 1)
end;

const
  Abcd = 'ABCD';
  MaxMinutes = 180 * 60;

class function TQtrAxis.IntToQtr(const i: Integer): string;
var
  Degrees, Minutes: Integer;
  Letter: Char;
begin
  Letter := Abcd[(i mod 4) + 1];
  Minutes := MaxMinutes - (i div 4);
  Degrees := Minutes div 60;
  Minutes := Minutes mod 60;

  Result := Format('%.2d%.2d%s', [Degrees, Minutes, Letter]);
end;

function LetterInc(const Letter: Char): Integer;
var
  p: Integer;
begin
  p := Pos(Letter, Abcd);
  if p=0 then
    raise Exception.Create('Expect ABCD, found: ' + Letter);
  Result := p-1;
end;

class function TQtrAxis.QtrToInt(const s: string): Integer;
var
  Degrees, Minutes: Integer;
  Letter: Char;
begin
  if Length(s)=5 then begin
    Degrees := StrToInt(Copy(S, 1, 2));
    Minutes := StrToInt(Copy(S, 3, 2));
    Letter := S[5];
  end else if Length(s)=6 then begin
    Degrees := StrToInt(Copy(S, 1, 3));
    Minutes := StrToInt(Copy(S, 4, 2));
    Letter := S[6];
  end else
    raise Exception.Create('Qtr code must be 5-6 characters long: ' + s);

  if (Minutes<0) or (Minutes>59) then
    raise Exception.Create('Minutes must be 00..59: ' + s);

  Minutes := Degrees * 60 + Minutes;

  Result := (MaxMinutes - Minutes) * 4 + LetterInc(Letter);
end;

end.

