unit ProfitCenter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxMaskEdit,
  cxDBLookupComboBox, cxMemo, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  Vcl.StdCtrls, dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  TProfitCenterForm = class(TBaseCxListForm)
    CompanyRefDS: TDataSource;
    CompanyRef: TADODataSet;
    PCCodeColumn: TcxGridDBColumn;
    PCNameColumn: TcxGridDBColumn;
    GLCodeColumn: TcxGridDBColumn;
    TimesheetNumColumn: TcxGridDBColumn;
    ADPCodeColumn: TcxGridDBColumn;
    CompanyLookupColumn: TcxGridDBColumn;
    BillingContactColumn: TcxGridDBColumn;
    CompanyLookupView: TcxGridDBTableView;
    CompanyViewCompanyColumn: TcxGridDBColumn;
    PCCityColumn: TcxGridDBColumn;
    ManagerLookupColumn: TcxGridDBColumn;
    ManagerRef: TADODataSet;
    ManagerRefDS: TDataSource;
    ManagerLookupView: TcxGridDBTableView;
    ManagerViewManagerColumn: TcxGridDBColumn;
    Panel1: TPanel;
    cbProfitCenterSearch: TCheckBox;
    SolomonPCColumn: TcxGridDBColumn;
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure cbProfitCenterSearchClick(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    PCInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils;

{$R *.dfm}

{ TProfitCenterForm }

procedure TProfitCenterForm.cbProfitCenterSearchClick(Sender: TObject);
begin
  inherited;
  if cbProfitCenterSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TProfitCenterForm.CloseDataSets;
begin
  inherited;
  CompanyRef.Close;
  ManagerRef.Close;
end;

procedure TProfitCenterForm.OpenDataSets;
begin
  CompanyRef.Open;
  ManagerRef.Open;
  inherited;
end;

procedure TProfitCenterForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbProfitCenterSearch.Enabled := True;
end;

procedure TProfitCenterForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  PCInserted := False;
end;

procedure TProfitCenterForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'profit_center', 'pc_code', PCInserted);
end;

procedure TProfitCenterForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
     //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TProfitCenterForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  PCInserted := True;
end;

procedure TProfitCenterForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)

  AddLookupField('CompanyLookup', Data, 'company_id', CompanyRef, 'company_id', 'name');
    Data.FieldByName('CompanyLookup').LookupCache := True;
  AddLookupField('ManagerLookup', Data, 'manager_emp_id', ManagerRef, 'emp_id', 'short_name');
    Data.FieldByName('ManagerLookup').LookupCache := True;
end;

procedure TProfitCenterForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TProfitCenterForm.ActivatingNow;
begin
  inherited;
  PCInserted := False;
end;

procedure TProfitCenterForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

end.

