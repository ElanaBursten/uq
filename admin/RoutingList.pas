unit RoutingList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, Grids, DBGrids, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxDropDownEdit, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges, cxCheckBox,
  dxScrollbarAnnotations;

type
  TRoutingListForm = class(TBaseCxListForm)
    Area: TADODataSet;
    Areamap_name: TStringField;
    Areastate: TStringField;
    Areacounty: TStringField;
    Areaarea_id: TAutoIncField;
    Areaarea_name: TStringField;
    Arealocator_id: TIntegerField;
    AreaDS: TDataSource;
    Datarouting_list_id: TAutoIncField;
    Datacall_center: TStringField;
    Datafield_name: TStringField;
    Datafield_value: TStringField;
    Dataarea_id: TIntegerField;
    DataAreaLookup: TStringField;
    Panel1: TPanel;
    Button1: TButton;
    Label2: TPanel;
    ColCallCenter: TcxGridDBColumn;
    ColFieldName: TcxGridDBColumn;
    ColFieldValue: TcxGridDBColumn;
    ColAreaID: TcxGridDBColumn;
    ColRoutingListID: TcxGridDBColumn;
    AreaGridView: TcxGridDBTableView;
    AreaGridLevel1: TcxGridLevel;
    AreaGrid: TcxGrid;
    AreaGridViewmap_name: TcxGridDBColumn;
    AreaGridViewstate: TcxGridDBColumn;
    AreaGridViewcounty: TcxGridDBColumn;
    AreaGridViewarea_id: TcxGridDBColumn;
    AreaGridViewarea_name: TcxGridDBColumn;
    AreaGridViewlocator_id: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    Datashort_name: TStringField;
    chkboxAreaSearch: TCheckBox;
    cbRoutingListSearch: TCheckBox;
    ColActive: TcxGridDBColumn;  //qm-363 SR
    Dataactive: TBooleanField;
    procedure Button1Click(Sender: TObject);
    procedure chkboxAreaSearchClick(Sender: TObject);
    procedure cbRoutingListSearchClick(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    rlInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  RoutingListForm: TRoutingListForm;

implementation

uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}
//QMANTWO-545 added wire_center
const FieldNames : array[1..20] of string[20] = (
  'work_address_street',
  'work_state',
  'work_county',
  'work_city',
  'work_cross',
  'work_subdivision',
  'company',
  'work_type',
  'work_remarks',
  'con_name',
  'con_address',
  'con_city',
  'con_state',
  'con_zip',
  'work_description',
  'ticket_type',
  'ward',
  'kind',
  'client_code',
  'wire_center'
);

procedure TRoutingListForm.PopulateDropdowns;
var
  i : Integer;
begin
//  inherited;
  AdminDM.GetCallCenterList(cxComboBoxItems(ColCallCenter));
  for i := Low(FieldNames) to High(FieldNames) do begin
    CxComboBoxItems(ColFieldName).Add(string(FieldNames[i]));
  end;
end;

procedure TRoutingListForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  //This grid always stays read-only
  AreaGridView.OptionsData.Appending := False;
  AreaGridView.OptionsData.Inserting := False;
  AreaGridView.OptionsData.Deleting := False;
  AreaGridView.OptionsData.Editing := False;
  chkBoxAreaSearch.Enabled := True;
  cbRoutingListSearch.Enabled := True;
end;

procedure TRoutingListForm.cbRoutingListSearchClick(Sender: TObject);
begin
  inherited;
  if cbRoutingListSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TRoutingListForm.chkboxAreaSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxAreaSearch.Checked then
    AreaGridView.Controller.ShowFindPanel
  else
    AreaGridView.Controller.HideFindPanel;
end;

procedure TRoutingListForm.CloseDataSets;
begin
  inherited;
  Area.Close;
end;

procedure TRoutingListForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  rlInserted := False;
end;

procedure TRoutingListForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset, ATableArray, 'routing_list', 'routing_list_id', rlInserted);
end;

procedure TRoutingListForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TRoutingListForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  rlInserted := True;
end;

procedure TRoutingListForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TRoutingListForm.ActivatingNow;
begin
  inherited;
  rlInserted := False;
end;

procedure TRoutingListForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TRoutingListForm.OpenDataSets;
begin
  inherited;
  Area.Open;
end;

procedure TRoutingListForm.Button1Click(Sender: TObject);
var AreaId: Integer;
begin
  AreaId := Area.FieldByName('area_id').AsInteger;
  if Data.RecordCount > 0 then begin
    Data.Edit();
    Data.FieldByName('area_id').AsInteger := AreaId;
    Data.Post();
  end;
end;

end.

