unit TSEApprovalQueueHist;
//qm-868
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDmu, Vcl.StdCtrls,
  cxDBExtLookupComboBox, cxMaskEdit, dxSkinsCore, dxSkinBasic;

type
  TApprovalQueueHistForm = class(TBaseCxListForm)
    chkboxSearch: TCheckBox;
    ColEntryID: TcxGridDBColumn;
    ColWorkEmpID: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColWorkDate: TcxGridDBColumn;
    ColQR: TcxGridDBColumn;
    ColQC: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColNote: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    Employees: TADODataSet;
    EmployeeDS: TDataSource;
    EmployeeView: TcxGridDBTableView;
    EmpNameCol: TcxGridDBColumn;
    procedure chkboxSearchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  ApprovalQueueHistForm: TApprovalQueueHistForm;

implementation

{$R *.dfm}

{ TBaseCxListForm1 }

procedure TApprovalQueueHistForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(False);
end;

procedure TApprovalQueueHistForm.chkboxSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TApprovalQueueHistForm.CloseDataSets;
begin
  inherited;
  Employees.Close;
end;

procedure TApprovalQueueHistForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TApprovalQueueHistForm.OpenDataSets;
begin
  Employees.Open;
  inherited;
end;

procedure TApprovalQueueHistForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxSearch.Enabled := True;
end;

end.
