unit StatusGroups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Grids, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, CheckLst, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxDBExtLookupComboBox,
  cxDropDownEdit, cxDBLookupComboBox, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations, dxSkinsCore, dxSkinBasic, dxCore, dxSkinsForm;

type
  TStatusGroupForm = class(TBaseCxListForm)
    GroupLabel: TLabel;
    StatusPanel: TPanel;
    StatusHeaderPanel: TPanel;
    StatusDetail: TADODataSet;
    UpdateStatusGroupSP: TADOStoredProc;
    Gridsg_id: TcxGridDBColumn;
    Gridsg_name: TcxGridDBColumn;
    Gridactive: TcxGridDBColumn;
    Gridright_id: TcxGridDBColumn;
    RightDefinitions: TADODataSet;
    cbStatusGroupSearch: TCheckBox;
    ActiveStatusGroupsFilter: TCheckBox;
    Panel1: TPanel;
    CLGrid: TStringGrid;
    StatusLabel: TLabel;
    SaveContentsButton: TButton;
    FindDialog1: TFindDialog;
    FindButton: TButton;
    TempDataSet: TADODataSet;
    TVPDataset: TADODataSet;
    procedure SaveContentsButtonClick(Sender: TObject);
    procedure Gridright_idGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure cbStatusGroupSearchClick(Sender: TObject);
    procedure ActiveStatusGroupsFilterClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DSDataChange(Sender: TObject; Field: TField); //BP QM-509
    procedure FindButtonClick(Sender: TObject); //BP QM-509
    procedure CLGridTopLeftChanged(Sender: TObject); //BP QM-509
    procedure CLGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CLGridKeyPress(Sender: TObject; var Key: Char);  //BP QM-509
    procedure FormCreate(Sender: TObject);    //BP QM-509
    procedure FormDestroy(Sender: TObject);   //BP QM-509
    procedure CLGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);  //BP QM-509
    procedure FindDialog1Find(Sender: TObject);
    procedure CLGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure CLGridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string); //BP QM-509
  private
    FCheck, FNoCheck: TBitmap; //BP QM-509
    FOldStatusList: string;
    FOldOutgoingList: String;
    FOldStExplList: String;
    FTemp: String;
    FTextChanged: Boolean;
    FsgID: Integer;
    FLastGroupID: Integer;
    procedure PopulateList;
    procedure UpdateSavable;
    procedure BuildTableValuedParameter;
    procedure ClearStringGrid(const Grid: TStringGrid); //BP QM-509
    procedure ToggleCheckbox(ACol, ARow: Integer);
    function GetSelectedStatusList(CLCheckGrid: TStringGrid): string;
    function GetSelectedOutGoingList(CLCheckGrid: TStringGrid): string;
    function GetSelectedStatusExplList(CLCheckGrid: TStringGrid): string;  //BP QM-509
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

implementation

uses AdminDMu, OdCxUtils, OdDbUtils, OdMiscUtils;

{$R *.dfm}

type
  TGridCracker = Class(TStringGrid);  //BP QM-509 Expose private methods

procedure TStatusGroupForm.OpenDataSets;
begin
  inherited;
  CLGrid.Enabled := True;
  RightDefinitions.Open;
end;

procedure TStatusGroupForm.ActiveStatusGroupsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('sg_id').AsInteger;
  Data.Close;
  if ActiveStatusGroupsFilter.Checked then
    Data.CommandText := 'select * from status_group where active = 1 order by sg_name' else
      Data.CommandText := 'select * from status_group order by sg_name';
  Data.Open;
  Data.Locate('sg_id', FSelectedID, []);
end;

procedure TStatusGroupForm.cbStatusGroupSearchClick(Sender: TObject);
begin
  inherited;
  if cbStatusGroupSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TStatusGroupForm.CloseDataSets;
begin
  RightDefinitions.Close;
  inherited;
end;

procedure TStatusGroupForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
   DataSet.FieldByName('active').AsBoolean := True;
end;


procedure TStatusGroupForm.Gridright_idGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  if RightDefinitions.Active then
    if RightDefinitions.Locate('right_id',StrToIntDef(AText,0),[]) then
      AText := RightDefinitions.FieldByName('right_description').AsString;
end;

procedure TStatusGroupForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[Gridactive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TStatusGroupForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

function TStatusGroupForm.GetSelectedOutGoingList(CLCheckGrid: TStringGrid): string;
var
  I: Integer;
  Text: String;
begin
  for I := 1 to CLCheckGrid.RowCount - 1 do begin
    if CLCheckGrid.Objects[0,I] <> nil then
    begin
      Text := Trim(CLCheckGrid.Cells[2,I]);
       if Text = '' then Text := '*';
        Result :=  Result + Text + ',';
    end;
  end;
  If Result <> '' then
  if Result[Length(Result)] = ',' then
    SetLength(Result, Length(Result) - 1);
end;

function TStatusGroupForm.GetSelectedStatusExplList(CLCheckGrid: TStringGrid): string;
var
  I: Integer;
  Text: String;
begin
  for I := 1 to CLCheckGrid.RowCount - 1 do begin
    if CLCheckGrid.Objects[0,I] <> nil then
    begin
      Text :=  Trim(CLCheckGrid.Cells[3,I]);
     if Text = '' then Text := '*';
        Result :=  Result + Text + ',';
    end;
  end;
  If Result <> '' then
  if Result[Length(Result)] = ',' then
    SetLength(Result, Length(Result) - 1);
end;

function TStatusGroupForm.GetSelectedStatusList(CLCheckGrid: TStringGrid): string;
var
  I: Integer;
  Codes: string;
  Text,Code: string;
begin
  for I := 1 to CLCheckGrid.RowCount - 1 do begin //BP QM-509 StringGrid change
    if CLCheckGrid.Objects[0,I] <> nil then
    begin
      Text := CLCheckGrid.Cells[1,I];
      Code := Copy(Text, 1, Pos(' - ', Text));
      if Codes <>'' then
        Codes := Codes + ',';
      Codes := Codes + Code;
    end;
  end;
  If Codes <> '' then
  if Codes[Length(Codes)] = ',' then
    SetLength(Codes, Length(Codes) - 1);
  Result := Codes;
end;

procedure TStatusGroupForm.ClearStringGrid(const Grid: TStringGrid);
begin
  //BP QM-509
  CLGrid.ColCount := 0;
  CLGrid.RowCount := 0;
end;

procedure TStatusGroupForm.CLGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  //BP QM-509
var
  Grid: TStringGrid;
begin
  If not (gdFixed In State) and (aCol = 0) Then
  begin
    Grid:= Sender As TStringgrid;
    With Grid.Canvas do
    begin
      //Checkbox state is encoded by the Objects property
      If Assigned(Grid.Objects[aCol, aRow]) then
        Draw((rect.right + rect.left - FCheck.width) div 2,
          (rect.bottom + rect.top - FCheck.height) div 2,
          FCheck )
      else
        Draw((rect.right + rect.left - FNoCheck.width) div 2,
          (rect.bottom + rect.top - FNoCheck.height) div 2,
          FNoCheck)
    end;
  end;

  If CLGrid.ControlCount > 0 then
  begin
    If (CLGrid.Col = 2) then
      TEdit(CLGrid.Controls[0]).MaxLength := 40 else
    If (CLGrid.Col = 3) then
      TEdit(CLGrid.Controls[0]).MaxLength := 20;
  end;
end;

procedure TStatusGroupForm.CLGridKeyPress(Sender: TObject; var Key: Char);
begin
    //BP QM-509 space bar toggles checkbox
   If Key = #32 Then
  begin
    With Sender As TStringGrid Do
    begin
      If Col = 0 Then Begin
        ToggleCheckbox(Col, Row);
        Key := #0;
      end;
    end;
  end;
end;

procedure TStatusGroupForm.CLGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 Row, Col: integer;
begin
  //BP QM-509  mouse click toggles checkbox
  inherited;
  CLGrid.MouseToCell(X, Y, Col, Row);
  If Col = 0 then
    ToggleCheckbox(Col, Row);
end;

procedure TStatusGroupForm.CLGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if (ACol > 1) and (ARow > 0) then begin
  CLGrid.Options := CLGrid.Options +[goEditing];
  FTemp := Trim(CLGrid.Cells[ACol,ARow]);
  end
else
  CLGrid.Options := CLGrid.Options -[goEditing];
end;


procedure TStatusGroupForm.CLGridSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  inherited;
  if Trim(Value) <> FTemp then
  begin
    FTextChanged := True;
    SaveContentsButton.Enabled := True;
  end;
end;

procedure TStatusGroupForm.ToggleCheckbox(ACol, ARow: Integer);
begin
  //BP QM-509 Objects property encodes state (1=checked, nil=unchecked)
  If aCol = 0 Then
  begin
    With TGridCracker(CLGrid) Do
    begin
       If Assigned(Objects[aCol, aRow]) then
         Objects[aCol, aRow] := nil
       else
         Objects[aCol, aRow] := Pointer(1);
    end;
    UpdateSavable;
  end;
end;

procedure TStatusGroupForm.CLGridTopLeftChanged(Sender: TObject);
begin
  //BP QM-509
  TGridCracker(Sender).InvalidateCol(0);
end;

procedure TStatusGroupForm.FindButtonClick(Sender: TObject);
begin
  inherited;
  FindDialog1.Execute(CLGrid.Handle);
end;

procedure TStatusGroupForm.FindDialog1Find(Sender: TObject);
var
  CurX, CurY, GridWidth, GridHeight: integer;
  X, Y: integer;
  TargetText: string;
  CellText: string;
  i: integer;
  GridRect: TGridRect;
begin
  //BP QM-509 Find Dialog for string grid
  CLGrid.SetFocus;
  CurX := CLGrid.Selection.Left + 1;
  CurY := CLGrid.Selection.Top;
  GridWidth := CLGrid.ColCount;
  GridHeight := CLGrid.RowCount;
  Y := CurY;
  X := CurX;
  if frMatchCase in FindDialog1.Options then
    TargetText := FindDialog1.FindText
  else
    TargetText := AnsiLowerCase(FindDialog1.FindText);
  while Y < GridHeight do
  begin
    while X < GridWidth do
    begin
      if frMatchCase in FindDialog1.Options then
        CellText := CLGrid.Cells[X, Y]
      else
        CellText := AnsiLowerCase(CLGrid.Cells[X, Y]);
      i := Pos(TargetText, CellText) ;
      if i > 0 then
      begin
        CLGrid.Col := X;
        CLGrid.Row := Y;
        CLGrid.SetFocus;
        GridRect.Left := X;
        GridRect.Right := X;
        GridRect.Top := Y;
        GridRect.Bottom := Y;
        CLGrid.Selection := GridRect;
        Exit;
      end;
      inc(X);
    end;
    inc(Y);
    X := CLGrid.FixedCols;
  end;
end;

procedure TStatusGroupForm.FormCreate(Sender: TObject);  //BP QM-509
var
  Bmp: TBitmap;
begin
  inherited;
  FindDialog1.Options := [frDown, frHideWholeWord, frHideUpDown];
  CLGrid.ColWidths[0] := 33;
  FCheck:= TBitmap.Create;
  FNoCheck:= TBitmap.Create;
  bmp:= TBitmap.create;
  try
    bmp.handle := LoadBitmap(0, PChar(OBM_CHECKBOXES));
    // bmp has bitmap of state images used by checkboxes
    with FNoCheck do begin
      //the first subimage is the unchecked box
      width := bmp.width div 4;
      height := bmp.height div 3;
      canvas.copyrect(canvas.cliprect, bmp.canvas, canvas.cliprect);
    end;
    with FCheck do
    begin
      //the second subimage is the checked box
      width := bmp.width div 4;
      height := bmp.height div 3;
      canvas.copyrect(
        canvas.cliprect,
        bmp.canvas,
        rect(width, 0, 2*width, height));
    end;
  finally
    bmp.free;
  end;
end;

procedure TStatusGroupForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FNoCheck.Free;
  FCheck.Free;
end;

procedure TStatusGroupForm.PopulateList;
var Col,Row: Integer;
begin
  StatusDetail.Parameters.ParamValues['sgid'] := Data.FieldByName('sg_id').AsInteger;
  StatusDetail.Open;
  try
     //BP QM-509 checkboxlist replaced with stringgrid columns
     ClearStringGrid(CLGrid);
    try
      Col := 0; Row := 1;
      CLGrid.Perform(WM_SETREDRAW, 0, 0);
      CLGrid.ColCount := 4;
      CLGrid.RowCount := StatusDetail.RecordCount + 1;
      CLGrid.FixedRows := 1;
      CLGrid.Cells[1,0] := 'Status';
      CLGrid.Cells[2,0] := 'Outgoing Status';
      CLGrid.Cells[3,0] := 'Status Explanation';
      while not StatusDetail.EOF do begin
        if StatusDetail.FieldByName('active').AsBoolean then
          CLGrid.Objects[Col,Row] := Pointer(1) else
            CLGrid.Objects[Col,Row] := nil;
        CLGrid.Cells[1, Row] := StatusDetail.FieldbyName('status').AsString;
        CLGrid.Cells[2, Row] := Trim(StatusDetail.FieldbyName('outgoing_status').AsString);
        CLGrid.Cells[3, Row] := Trim(StatusDetail.FieldbyName('status_explanation').AsString);
        StatusDetail.Next;
        Inc(Row);
      end;
    finally
      CLGrid.Perform(WM_SETREDRAW, 1, 0);
      CLGrid.Invalidate; //force repaint after all
    end;
  finally
    StatusDetail.Close;
    SaveContentsButton.Enabled := False;
  end;

  FOldStatusList := GetSelectedStatusList(CLGrid);
  FLastGroupID := Data.FieldByName('sg_id').AsInteger;
end;

procedure TStatusGroupForm.SaveContentsButtonClick(Sender: TObject);
begin
  inherited;
  if Data.State in dsEditModes then
    Data.Post;

  BuildTableValuedParameter;
  FOldStatusList := GetSelectedStatusList(CLGrid);
  FTextChanged := False;
  SaveContentsButton.Enabled := False;
end;

procedure TStatusGroupForm.UpdateSavable;
begin
  //BP QM-509 save button enabled on stringgrid checkbox toggles
  SaveContentsButton.Enabled := FTextChanged or (GetSelectedStatusList(CLGrid) <> FOldStatusList);
end;

procedure TStatusGroupForm.DSDataChange(Sender: TObject; Field: TField); //BP QM-509
begin
  //BP QM-509 populate stringgrid on status group row changes
  inherited;
  if Data.FieldByName('sg_id').AsInteger <> FLastGroupID then
    PopulateList;
end;

procedure TStatusGroupForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  CLGrid.Enabled := True;
  FindButton.Enabled := True;
  cbStatusGroupSearch.Enabled := True;
  ActiveStatusGroupsFilter.Enabled := True;
end;

procedure TStatusGroupForm.BuildTableValuedParameter;
//BP QM-509 Table Valued Parameter for StoredProc
var
  i: Integer;
  StatusList, OutgoingList, StatusExplList: TStringLIst;
begin
  try
  StatusList := TStringList.Create;
  StatusList.CommaText := GetSelectedStatusList(CLGrid);
  OutgoingList  := TStringList.Create;
    OutgoingList.Delimiter := ',';
    OutgoingList.StrictDelimiter := true;
  OutgoingList.CommaText := GetSelectedOutgoingList(CLGrid);
  StatusExplList := TStringList.Create;
    StatusExplList.Delimiter := ',';
    StatusExplList.StrictDelimiter := True;
  StatusExplList.CommaText := GetSelectedStatusExplList(CLGrid);
  // create a temporary table since a table variable can only be used for a single call
  TempDataSet.Connection.Execute('If Object_ID(''TEMPDB..#mytemp'') is not NULL Drop Table #mytemp');
  TempDataSet.Connection.Execute('Create Table #mytemp(status varchar(5), outgoingstatus nchar(40), statusexpl nchar(20))');
  TempDataSet.CommandText := 'Select * from #mytemp';
  TempDataSet.Open;
  for i := 0 to StatusList.Count-1  do
  begin
    TempDataSet.Append;
    TempDataSet.Fields[0].Value := StatusList[I];
    if OutgoingList[I] = '*' then
     TempDataSet.Fields[1].Value := NULL else
       TempDataSet.Fields[1].Value := OutgoingList[I];
     If  StatusExplList[I] = '*' then
       TempDataSet.Fields[2].Value := NULL else
    TempDataSet.Fields[2].Value :=  StatusExplList[I];
    TempDataSet.Post;
  end;
  TVPDataset.CommandText := 'Begin Transaction '
                         + 'Declare @mytemp as StatusTableType '
                         + 'Insert into @mytemp select * from #mytemp '// copy data to typed table variable
                         + 'EXEC update_status_group @SGID = ' +
                            Data.FieldByName('sg_id').AsString + ', @Tab = @mytemp '
                         + 'Commit';
  TVPDataset.Open;
  TempDataSet.Close;
  TempDataSet.Connection.Execute('Drop Table #mytemp');
  FsgID := Data.FieldByName('sg_id').AsInteger;
  AdminDM.Conn.Close;
  AdminDM.Conn.Open();
  Data.Close;
  Data.Open;
  Data.Locate('sg_id', FsgID, []);
  finally
    StatusList.Free;
    OutgoingList.Free;
    StatusExplList.Free;
  end;
end;

end.

