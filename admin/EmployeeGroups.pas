unit EmployeeGroups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls,
  BetterADODataSet, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxSpinEdit, cxCheckBox,
  cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, Rights, System.Actions, Vcl.ActnList,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TEmployeeGroupsForm = class(TBaseCxListForm)
    PC: TPageControl;
    RightsSheet: TTabSheet;
    GroupRightsDS: TDataSource;
    RightsSplitter: TSplitter;
    InsertRightCommand: TADOCommand;
    UpdateRightCommand: TADOCommand;
    TypeRef: TADODataSet;
    TypeRefDS: TDataSource;
    ColGroupName: TcxGridDBColumn;
    ColPriority: TcxGridDBColumn;
    ColIsDefault: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColGroupID: TcxGridDBColumn;
    ActionList: TActionList;
    GrantAction: TAction;
    RevokeAction: TAction;
    RightsFrame: TRightsFrame;
    cbEmpGroupsSearch: TCheckBox;
    ActiveEmpGroupsFilter: TCheckBox;
    GroupRights: TADODataSet;
    procedure GroupRightsAfterScroll(DataSet: TDataSet);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure GrantActionExecute(Sender: TObject);
    procedure RevokeActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure cbEmpGroupsSearchClick(Sender: TObject);
    procedure ActiveEmpGroupsFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
  private
    GroupDefInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdMiscUtils, QMConst, OdExceptions, MainFU;

{$R *.dfm}

procedure TEmployeeGroupsForm.GroupRightsAfterScroll(DataSet: TDataSet);
begin
  if NotEmpty(DataSet.FieldByName('modifier_desc').AsString) then
    RightsFrame.LimitationHintLabel.Caption := DataSet.FieldByName('modifier_desc').AsString
  else
    RightsFrame.LimitationHintLabel.Caption := RightLimitationUndefined;
end;

procedure TEmployeeGroupsForm.GrantActionExecute(Sender: TObject);
var
  s: String;
begin
  inherited;
  RightsFrame.SetEmpGroupRight('Y', RightsFrame.EditLimitation.Text, Data, InsertRightCommand,
    UpdateRightCommand);

   With RightsFrame.RightsGridDBTableView1.DataController.DataSet do
   begin
      s := '[group_right_id ' + FieldbyName('group_right_id').AsString + '] ' +
     ' Right Granted to group_id ' + FieldByName('group_id').AsString +
     '. right_id ' + FieldByName('right_id').AsString + ', right_description ' +
     FieldByName('right_description').AsString;
       AdminDM.AdminChangeTable('group_right', s, now); //QM-823
   end;
end;

procedure TEmployeeGroupsForm.RevokeActionExecute(Sender: TObject);
var
  s: string;
begin
  inherited;
  RightsFrame.SetEmpGroupRight('N', '', Data, InsertRightCommand, UpdateRightCommand);

  With RightsFrame.RightsGridDBTableView1.DataController.DataSet do
  begin
     s := '[group_right_id ' + FieldbyName('group_right_id').AsString + '] ' +
     ' Right Revoked from group_id ' + FieldByName('group_id').AsString +
     ', right_id ' + FieldByName('right_id').AsString + ', right_description ' +
     FieldByName('right_description').AsString;
     AdminDM.AdminChangeTable('group_right', s, now); //QM-823
  end;
end;

procedure TEmployeeGroupsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TEmployeeGroupsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbEmpGroupsSearch.Enabled := True;
  ActiveEmpGroupsFilter.Enabled := True;
end;

procedure TEmployeeGroupsForm.OpenDataSets;
begin
  inherited;
  GroupRights.Open;
end;

procedure TEmployeeGroupsForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
  RightsRow: TcxCustomGridRow;
begin
  inherited;
  try
  RightsRow := RightsFrame.RightsGridDBTableView1.Controller.FocusedRow;
  if RightsRow <> nil then
  begin
    RightsFrame.GrantButton.Enabled := (RightsFrame.RightsGridDBTableView1.Controller.FocusedRow.IsData) and (not MainForm.EditButton.Enabled);
    RightsFrame.RevokeButton.Enabled := (RightsFrame.RightsGridDBTableView1.Controller.FocusedRow.IsData) and (not MainForm.EditButton.Enabled);
  end;
  except
    RightsFrame.GrantButton.Enabled := False;
    RightsFrame.RevokeButton.Enabled := False;
  end;
end;

procedure TEmployeeGroupsForm.ActivatingNow;
begin
  inherited;
  GroupDefInserted := False;
end;

procedure TEmployeeGroupsForm.ActiveEmpGroupsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('group_id').AsInteger;
  Data.Close;
  if ActiveEmpGroupsFilter.Checked then
    Data.CommandText := 'select * from group_definition where active = 1 order by group_name' else
      Data.CommandText := 'select * from group_definition order by group_name';
  Data.Open;
  Data.Locate('group_id', FSelectedID, []);
end;

procedure TEmployeeGroupsForm.cbEmpGroupsSearchClick(Sender: TObject);
begin
  inherited;
  if cbEmpGroupsSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TEmployeeGroupsForm.CloseDataSets;
begin
  inherited;
  GroupRights.Close;
end;

procedure TEmployeeGroupsForm.LeavingNow;
begin
  inherited;
  PostDataSet(GroupRights);
  CloseDataSets;
end;

procedure TEmployeeGroupsForm.DataBeforeDelete(DataSet: TDataSet);
begin
  raise EOdNotAllowed.Create('You cannot delete employee groups.  You can deactivate them by unchecking Active.');
end;

procedure TEmployeeGroupsForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   // get the current values before editing.
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-847
end;

procedure TEmployeeGroupsForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  GroupDefInserted := True;
end;

procedure TEmployeeGroupsForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TEmployeeGroupsForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEmployeeGroupsForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  GroupDefInserted := False;
end;

procedure TEmployeeGroupsForm.DataAfterPost(DataSet: TDataSet);
var
 s:String;
begin
  inherited;
  Dataset.Refresh;
  AdminDM.TrackAdminChanges(DataSet, ATableArray, 'group_defintion', 'group_id', GroupDefInserted);
end;

end.

