unit ComputerAssets;
 //qm-599 BP  displays all assets for individual
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxLabel, cxDBLabel, Vcl.StdCtrls,
  Data.DB, Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TComputerAssetsForm = class(TForm)
    lblWorkOrders: TLabel;
    qryComputerInfo: TADOQuery;
    OkBtn: TButton;
    dsComputerInfo: TDataSource;
    GridView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    GridViewColumn1: TcxGridDBColumn;
    GridViewColumn2: TcxGridDBColumn;
    GridViewColumn3: TcxGridDBColumn;
    GridViewColumn4: TcxGridDBColumn;
    GridViewColumn5: TcxGridDBColumn;
  private
    { Private declarations }
    EmpID: Integer;
  public
    { Public declarations }
  end;

var
  ComputerAssetsForm: TComputerAssetsForm;

procedure DisplayComputerInfo(const empid: Integer);

implementation

{$R *.dfm}

procedure DisplayComputerInfo(const empid: Integer);
var
  Dialog: TComputerAssetsForm;
begin
  Dialog := TComputerAssetsForm.Create(nil);
  try
    Dialog.qryComputerInfo.Parameters.ParamByName('empid').Value := empid;
    Dialog.qryComputerInfo.Open;
    Dialog.ShowModal;
  finally
    Dialog.qryComputerInfo.Close;
    FreeAndNil(Dialog);
  end;
end;

end.
