unit uRobomain;
//  qm-416 Pump
//  qm-418 Admin Setup  B.P.
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ComCtrls, Vcl.ExtCtrls, FileCtrl, cxCheckBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, dxSkinBasic, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCheckComboBox, Vcl.Mask, Vcl.Menus, cxLabel, cxButtons, MainFU, OdEmbeddable,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Vcl.DBCtrls, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxSpinEdit, cxTimeEdit, Vcl.Samples.Spin, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxCore, dxSkinsForm;
 const
  SELDIRHELP = 1000;
type
  TRoboCopyForm = class(TEmbeddableForm)
    pnlTop: TPanel;
    pnlBot: TPanel;
    btnCopy: TcxButton;
    btnInsertCopyArrs: TcxButton;
    arrTextEdit: TcxTextEdit;
    cxLabel1: TcxLabel;
    btnUpdateCopyArr: TcxButton;
    dsCustomer: TDataSource;
    qryCustomer: TADOQuery;
    insCustomer: TADOQuery;
    updCustomer: TADOQuery;
    cboLookupEmpID: TcxLookupComboBox;
    dsLookupEmp: TDataSource;
    qryLookupEmp: TADOQuery;
    cboLookupCustID: TcxLookupComboBox;
    qryLookupCust: TADOQuery;
    dsLookupCust: TDataSource;
    cboLookupEmpID2: TcxDBLookupComboBox;
    cboLookupCustID2: TcxDBLookupComboBox;
    MidPanel: TPanel;
    Splitter2: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    SourceDir: TcxTextEdit;
    DestDir: TcxTextEdit;
    logfolder: TcxTextEdit;
    btnFindSource: TcxButton;
    btnFindDest: TcxButton;
    btnLogFolder: TcxButton;
    Label1: TcxLabel;
    Label2: TcxLabel;
    Label3: TcxLabel;
    TabSheet2: TTabSheet;
    cxCheckComboBox1: TcxCheckComboBox;
    CheckBox1: TcxCheckBox;
    CheckBox2: TcxCheckBox;
    CheckBox3: TcxCheckBox;
    CheckBox4: TcxCheckBox;
    CheckBox5: TcxCheckBox;
    CheckBox6: TcxCheckBox;
    CheckBox7: TcxCheckBox;
    CheckBox8: TcxCheckBox;
    CheckBox9: TcxCheckBox;
    CheckBox10: TcxCheckBox;
    CheckBox11: TcxCheckBox;
    CheckBox12: TcxCheckBox;
    CheckBox13: TcxCheckBox;
    CheckBox14: TcxCheckBox;
    CheckBox15: TcxCheckBox;
    CheckBox16: TcxCheckBox;
    CheckBox17: TcxCheckBox;
    CheckBox18: TcxCheckBox;
    CheckBox19: TcxCheckBox;
    CheckBox20: TcxCheckBox;
    CheckBox21: TcxCheckBox;
    CheckBox22: TcxCheckBox;
    CheckBox23: TcxCheckBox;
    CheckBox24: TcxCheckBox;
    CheckBox25: TcxCheckBox;
    edtLev: TcxTextEdit;
    edtnChanges: TcxTextEdit;
    edtnMins: TcxTextEdit;
    edtMSGap: TcxTextEdit;
    TabSheet3: TTabSheet;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    cxCheckBox4: TcxCheckBox;
    cxCheckBox5: TcxCheckBox;
    cxCheckBox6: TcxCheckBox;
    cxCheckBox7: TcxCheckBox;
    cxCheckBox8: TcxCheckBox;
    cxCheckBox9: TcxCheckBox;
    cxCheckBox10: TcxCheckBox;
    cxCheckBox11: TcxCheckBox;
    cxCheckBox12: TcxCheckBox;
    cxCheckBox13: TcxCheckBox;
    cxCheckBox14: TcxCheckBox;
    cxCheckBox15: TcxCheckBox;
    cxCheckBox16: TcxCheckBox;
    cxCheckBox17: TcxCheckBox;
    cxCheckBox18: TcxCheckBox;
    cxCheckBox19: TcxCheckBox;
    cxCheckBox20: TcxCheckBox;
    edtMaxAge: TcxTextEdit;
    edtMinAge: TcxTextEdit;
    edtMaxDays: TcxTextEdit;
    edtMinDays: TcxTextEdit;
    edtMaxBytes: TcxTextEdit;
    edtMinBytes: TcxTextEdit;
    TabSheet4: TTabSheet;
    cxCheckBox21: TcxCheckBox;
    cxCheckBox22: TcxCheckBox;
    cxCheckBox23: TcxCheckBox;
    cxCheckBox24: TcxCheckBox;
    TabSheet5: TTabSheet;
    cxCheckBox25: TcxCheckBox;
    cxCheckBox26: TcxCheckBox;
    cxCheckBox27: TcxCheckBox;
    cxCheckBox28: TcxCheckBox;
    cxCheckBox29: TcxCheckBox;
    cxCheckBox30: TcxCheckBox;
    cxCheckBox31: TcxCheckBox;
    cxCheckBox32: TcxCheckBox;
    cxCheckBox33: TcxCheckBox;
    cxCheckBox34: TcxCheckBox;
    cxCheckBox35: TcxCheckBox;
    cxCheckBox36: TcxCheckBox;
    cxCheckBox37: TcxCheckBox;
    cxCheckBox38: TcxCheckBox;
    cxCheckBox39: TcxCheckBox;
    cxCheckBox40: TcxCheckBox;
    Splitter1: TSplitter;
    cxCheckComboBox2: TcxCheckComboBox;
    cxCheckComboBox3: TcxCheckComboBox;
    cxCheckComboBox4: TcxCheckComboBox;
    cxCheckComboBox5: TcxCheckComboBox;
    edtstatuslogoverwrite: TcxTextEdit;
    edtstatuslogappend: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel2: TcxLabel;
    CustLocatorView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    CustNameCol: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    ColRoboSource: TcxGridDBColumn;
    ColRoboDest: TcxGridDBColumn;
    ColRoboOptions: TcxGridDBColumn;
    ColShortName: TcxGridDBColumn;
    Panel2: TPanel;
    chkboxSearch: TCheckBox;
    edtRchanges: TcxTextEdit;
    edtWchanges: TcxTextEdit;
    edtStartTime: TcxTimeEdit;
    edtStopTime: TcxTimeEdit;
    SpinEdit1: TSpinEdit;
    cxLabel4: TcxLabel;
    btnDirExclusions: TButton;
    btnDupRow: TButton;
    cbCopiedArgs: TCheckBox;
    cxLabel5: TcxLabel;
    SpinEdit2: TSpinEdit;
    BtnFileExclusions: TButton;
    dxSkinController1: TdxSkinController;
    procedure btnCopyClick(Sender: TObject);
    procedure btnFindSourceClick(Sender: TObject);
    procedure btnFindDestClick(Sender: TObject);
    procedure btnLogFolderClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure CheckBox6Click(Sender: TObject);
    procedure CheckBox8Click(Sender: TObject);
    procedure CheckBox9Click(Sender: TObject);
    procedure CheckBox10Click(Sender: TObject);
    procedure CheckBox11Click(Sender: TObject);
    procedure CheckBox12Click(Sender: TObject);
    procedure CheckBox13Click(Sender: TObject);
    procedure CheckBox14Click(Sender: TObject);
    procedure CheckBox15Click(Sender: TObject);
    procedure CheckBox16Click(Sender: TObject);
    procedure CheckBox17Click(Sender: TObject);
    procedure CheckBox18Click(Sender: TObject);
    procedure CheckBox19Click(Sender: TObject);
    procedure CheckBox20Click(Sender: TObject);
    procedure CheckBox21Click(Sender: TObject);
    procedure CheckBox22Click(Sender: TObject);
    procedure CheckBox23Click(Sender: TObject);
    procedure CheckBox24Click(Sender: TObject);
    procedure CheckBox25Click(Sender: TObject);
    procedure CheckBox7Click(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxCheckBox2Click(Sender: TObject);
    procedure cxCheckBox3Click(Sender: TObject);
    procedure cxCheckBox4Click(Sender: TObject);
    procedure cxCheckBox6Click(Sender: TObject);
    procedure cxCheckBox7Click(Sender: TObject);
    procedure cxCheckBox8Click(Sender: TObject);
    procedure cxCheckBox9Click(Sender: TObject);
    procedure cxCheckBox10Click(Sender: TObject);
    procedure cxCheckBox11Click(Sender: TObject);
    procedure cxCheckBox12Click(Sender: TObject);
    procedure cxCheckBox13Click(Sender: TObject);
    procedure cxCheckBox14Click(Sender: TObject);
    procedure cxCheckBox15Click(Sender: TObject);
    procedure cxCheckBox16Click(Sender: TObject);
    procedure cxCheckBox17Click(Sender: TObject);
    procedure cxCheckBox18Click(Sender: TObject);
    procedure cxCheckBox19Click(Sender: TObject);
    procedure cxCheckBox20Click(Sender: TObject);
    procedure cxCheckBox25Click(Sender: TObject);
    procedure cxCheckBox26Click(Sender: TObject);
    procedure cxCheckBox27Click(Sender: TObject);
    procedure cxCheckBox28Click(Sender: TObject);
    procedure cxCheckBox29Click(Sender: TObject);
    procedure cxCheckBox30Click(Sender: TObject);
    procedure cxCheckBox31Click(Sender: TObject);
    procedure cxCheckBox32Click(Sender: TObject);
    procedure cxCheckBox33Click(Sender: TObject);
    procedure cxCheckBox34Click(Sender: TObject);
    procedure cxCheckBox35Click(Sender: TObject);
    procedure cxCheckBox36Click(Sender: TObject);
    procedure cxCheckBox37Click(Sender: TObject);
    procedure cxCheckBox38Click(Sender: TObject);
    procedure cxCheckBox39Click(Sender: TObject);
    procedure cxCheckBox40Click(Sender: TObject);
    procedure cxCheckBox21Click(Sender: TObject);
    procedure cxCheckBox22Click(Sender: TObject);
    procedure cxCheckBox23Click(Sender: TObject);
    procedure cxCheckBox24Click(Sender: TObject);
    procedure btnInsertCopyArrsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxCheckBox5Click(Sender: TObject);
    procedure btnUpdateCopyArrClick(Sender: TObject);
    procedure cxCheckComboBox1PropertiesPopup(Sender: TObject);
    procedure cxCheckComboBox2PropertiesPopup(Sender: TObject);
    procedure cxCheckComboBox3PropertiesPopup(Sender: TObject);
    procedure cxCheckComboBox4PropertiesPopup(Sender: TObject);
    procedure cxCheckComboBox5PropertiesPopup(Sender: TObject);
    procedure dsCustomerDataChange(Sender: TObject; Field: TField);
    procedure qryCustomerBeforeDelete(DataSet: TDataSet);
    procedure chkboxSearchClick(Sender: TObject);
    procedure cboLookupEmpIDPropertiesInitPopup(Sender: TObject);
    procedure btnDirExclusionsClick(Sender: TObject);
    procedure btnDupRowClick(Sender: TObject);
    procedure BtnFileExclusionsClick(Sender: TObject);
    procedure cbCopiedArgsClick(Sender: TObject);
    procedure CustLocatorViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure SourceDirKeyPress(Sender: TObject; var Key: Char);
    procedure insCustomerAfterInsert(DataSet: TDataSet);
    procedure qryCustomerBeforeInsert(DataSet: TDataSet);
  private
    fcopyParameters: string;
    fCopyFlags: String;
    fAddAttrFlags: String;
    fRemoveAttrFlags: String;
    fCopyAttrFiles: String;
    fExcludeAttrFiles: String;
    fCustomerLocatorID: Integer;
    CopiedArguments: String;
    DirValues, FileValues, DirNames, FileNames: TArray<string>;


    procedure GetDirectory(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
    property copyParameters : string read  fcopyParameters write  fcopyParameters;
    property CopyFlags: string read fCopyFlags write fCopyFlags;
    //B.P. QM-418
    property AddAttrFlags: string read fAddAttrFlags write fAddAttrFlags;
    property RemoveAttrFlags: string read fRemoveAttrFlags write fRemoveAttrFlags;
    property CopyAttrFiles: string read fCopyAttrFiles write fCopyAttrFiles;
    property ExcludeAttrFiles: string read fExcludeAttrFiles write fExcludeAttrFiles;
    function IsReadOnly: Boolean; override;
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
  end;
var
  RoboCopyForm: TRoboCopyForm;

implementation

{$R *.dfm}

uses AdminDMu, System.IOUtils, System.StrUtils, System.AnsiStrings, ADOInt;

function ExecuteProcess(const FileName, Params: string; Folder: string;
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean; var ErrorCode: integer): Boolean;
  {SR: though more complicated, this is more powerful than ShellExecute.
  It will also pass thru Windows GetLastError.
  When running a batch file, it will catch batch file errors.  It does not use ActiveX}
var
  CmdLine: string;
  WorkingDirP: PChar;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Result := true;
  CmdLine := '"' + FileName + '" ' + Params;
  if Folder = '' then
    Folder := ExcludeTrailingPathDelimiter(ExtractFilePath(FileName));
  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(StartupInfo);
  if RunMinimized then
  begin
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := SW_SHOWMINIMIZED;
  end;
  if Folder <> '' then
    WorkingDirP := PChar(Folder)
  else
    WorkingDirP := nil;
  if not CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, WorkingDirP, StartupInfo, ProcessInfo) then
  begin
    Result := False;
    ErrorCode := GetLastError;
    exit;
  end;
  with ProcessInfo do
  begin
    CloseHandle(hThread);
    if WaitUntilIdle then
      WaitForInputIdle(hProcess, INFINITE);
    if WaitUntilTerminated then
      repeat
        Application.ProcessMessages;
      until MsgWaitForMultipleObjects(1, hProcess, False, INFINITE, QS_ALLINPUT) <> WAIT_OBJECT_0 + 1;
    CloseHandle(hProcess);
  end;
end;

procedure TRoboCopyForm.btnCopyClick(Sender: TObject);
const
  APP_ROBOCOPY = 'robocopy';
  SEP = ' ';
var
  sParams : string;
  sFolder: string;
  iErrorCode: integer;
begin
  if (Trim(SourceDir.Text) = '') or (Trim(DestDir.Text) = '') then
  begin
    ShowMessage('source/destination directory not specified');
    Exit;
  end;
  if cbCopiedArgs.Checked then
     sParams := '"' + SourceDir.text + '"' +SEP+ '"' + DestDir.text + '"' + SEP + CopiedArguments else
  sParams :=  '"' + SourceDir.text + '"' + SEP+  '"' + DestDir.text + '"' + SEP + arrTextEdit.text;
  if not cxCheckBox36.Checked and not cxCheckBox37.checked then
  begin
    //default log folder is 'C:\Plats\log'
    if Trim(logfolder.Text) <> '' then
      sParams := sParams +SEP+'/log:'+ '"' + IncludeTrailingPathDelimiter(logfolder.text) + 'robocopy.log' + '"';
  end;
  sFolder := SourceDir.text;
  If ExecuteProcess(APP_ROBOCOPY, sParams, sFolder, True, False, False, iErrorCode) then
  showmessage('It works')
  else
  showmessage('Error code: '+IntToStr(iErrorCode));
end;

procedure TRoboCopyForm.btnDupRowClick(Sender: TObject);
  var
   Arguments: String;
begin
  if qryCustomer.EOF then exit;
  CopiedArguments := qryCustomer.FieldbyName('robooptions').AsString;
end;

procedure TRoboCopyForm.btnFindDestClick(Sender: TObject);
begin
  GetDirectory(Sender);
end;

procedure TRoboCopyForm.btnFindSourceClick(Sender: TObject);
begin
  GetDirectory(Sender);
end;

procedure TRoboCopyForm.btnInsertCopyArrsClick(Sender: TObject);
var
 AIns: Boolean;
begin
  //QM-418 B.P.
  inherited;
  if (cboLookupEmpid.ItemIndex = -1) or (cboLookupCustID.ItemIndex = -1) then
  begin
   Showmessage('Select employee and customer');
   Exit;
  end;
  if (SourceDir.Text = '') or (DestDir.Text = '') then
  begin
    Showmessage('Select source directory and destination directory');
    Exit;
  end;

  With insCustomer.Parameters do
  begin
    ParamByName('robosource').Value := SourceDir.Text;
    ParamByName('robodest').Value := DestDir.Text;
    if cbCopiedArgs.Checked then
      ParamByName('robooptions').Value := CopiedArguments else
    ParamByName('robooptions').Value := ArrTextEdit.Text;
    ParamByName('customer_id').Value := cboLookupCustID.EditingValue;
    ParamByName('loc_emp_id').Value := cboLookupEmpID.EditingValue;
  end;
  InsCustomer.ExecSQL;
  FCustomerLocatorID := insCustomer.FieldByName('ID').AsInteger;

  insCustomer.Close;
  qryCustomer.Close;
  qryCustomer.Open;
  qryCustomer.Locate('customer_locator_id',FCustomerLocatorID, []);
  AIns := True;
  AdminDM.TrackAdminChanges(qryCustomer, ATableArray, 'customer_locator', 'customer_locator_id', AIns);
end;

procedure TRoboCopyForm.btnUpdateCopyArrClick(Sender: TObject);
var
  AIns: Boolean;
begin
  //QM-418 B.P.
  inherited;
  if (cboLookupEmpid2.Text = '') or (cboLookupCustID2.Text = '') then
  begin
   Showmessage('Select employee and customer');
   Exit;
  end;
  if (SourceDir.Text = '') or (DestDir.Text = '') then
  begin
    Showmessage('Select source directory and destination directory');
    Exit;
  end;

  AdminDM.StoreFieldValues(qryCustomer, ATableArray); //QM-823

  With updCustomer.Parameters do
  begin
    ParamByName('robosource').Value := SourceDir.Text;
    ParamByName('robodest').Value := DestDir.Text;
    if cbCopiedArgs.Checked then
      ParamByName('robooptions').Value := CopiedArguments else
    ParamByName('robooptions').Value := ArrTextEdit.Text;
    ParamByName('customer_id').Value := cboLookupCustID2.EditValue;
    ParamByName('loc_emp_id').Value := cboLookupEmpID2.EditValue;
    ParamByName('customer_locator_id').Value := qryCustomer.FieldByName('customer_locator_id').Value;
  end;
  FCustomerLocatorID := qryCustomer.FieldByName('customer_locator_id').Value;
  updCustomer.ExecSQL;
  qryCustomer.Close;
  qryCustomer.Open;
  qryCustomer.Locate('customer_locator_id', FCustomerLocatorID, []);

  AIns := False;
  AdminDM.TrackAdminChanges(qryCustomer, ATableArray, 'customer_locator', 'customer_locator_id', AIns);
end;

procedure TRoboCopyForm.btnDirExclusionsClick(Sender: TObject);
  var  I:Integer;
begin
  inherited;
   SetLength(DirValues, SpinEdit1.Value);
   SetLength(DirNames, SpinEdit1.Value);
   for I := Low(DirNames) to High(DirNames) do
     Dirnames[I] := 'Directory #' + InttoStr(I+1);
   InputQuery('Folder Exclusions', DirNames, DirValues);
end;

procedure TRoboCopyForm.BtnFileExclusionsClick(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  SetLength(FileValues, SpinEdit2.Value);
  SetLength(FileNames, SpinEdit2.Value);
  for I := Low(FileNames) to High(FileNames) do
    FileNames[I] := 'File #' + InttoStr(I+1);
  InputQuery('File Exclusions', FileNames, FileValues);
end;

procedure TRoboCopyForm.cbCopiedArgsClick(Sender: TObject);
begin
  inherited;
  if cbCopiedArgs.Checked then
  begin
    if trim(CopiedArguments) = '' then
    begin
     ShowMessage('no copied arguments');
     cbCopiedArgs.Checked := false;
    end;
  end;
end;

procedure TRoboCopyForm.cboLookupEmpIDPropertiesInitPopup(Sender: TObject);
begin
  inherited;
 cboLookupEmpID.Properties.ListColumns[0].Width := 175;
end;

procedure TRoboCopyForm.btnLogFolderClick(Sender: TObject);
begin
  GetDirectory(Sender);
end;

procedure TRoboCopyForm.CheckBox10Click(Sender: TObject);
const
  ARR = ' /NOCOPY';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox11Click(Sender: TObject);
const
  ARR = ' /PURGE';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox12Click(Sender: TObject);
const
  ARR = ' /MIR';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox13Click(Sender: TObject);
const
  ARR = ' /MOV';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox14Click(Sender: TObject);
const
  ARR = ' /MOVE';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox15Click(Sender: TObject);
//B.P. QM-418
const
  ARR = ' /A+';
  var I: Integer;
begin
  if TcxCheckBox(Sender).Checked then
  begin
    AddAttrFlags := ':';
    if (cxCheckCombobox2.ItemIndex <> -1) then
    begin
      copyParameters := copyParameters+ARR;
      with cxCheckComboBox2.Properties do
      for I := 0  to Items.Count - 1 do
      begin
        if cxCheckComboBox2.States[I] = cbsChecked then
         AddAttrFlags := AddAttrFlags +  cxCheckComboBox2.Properties.Items[I].ShortDescription;
      end;
      if AddAttrFlags <> ':' then
       copyParameters := copyParameters+AddAttrFlags
      else
        AddAttrFlags := '';
    end;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+AddAttrFlags, '',[rfIgnoreCase ]);
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox16Click(Sender: TObject);
//B.P. QM-418
const
  ARR = ' /A-';
  var I: Integer;
begin
  if TcxCheckBox(Sender).Checked then
  begin
    RemoveAttrFlags := ':';
    if (cxCheckCombobox3.ItemIndex <> -1) then
    begin
      copyParameters := copyParameters+ARR;
      with cxCheckComboBox3.Properties do
      for I := 0  to Items.Count - 1 do
      begin
        if cxCheckComboBox3.States[I] = cbsChecked then
         RemoveAttrFlags := RemoveAttrFlags +  cxCheckComboBox3.Properties.Items[I].ShortDescription;
      end;
      if AddAttrFlags <> ':' then
       copyParameters := copyParameters+RemoveAttrFlags
      else
        RemoveAttrFlags := '';
    end;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+RemoveAttrFlags, '',[rfIgnoreCase ]);
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox17Click(Sender: TObject);
const
  ARR = ' /CREATE';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox18Click(Sender: TObject);
const
  ARR = ' /FAT';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox19Click(Sender: TObject);
const
  ARR = ' /FFT';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox1Click(Sender: TObject);
const
  ARR = ' /S';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox20Click(Sender: TObject);
const
  ARR = ' /256';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox21Click(Sender: TObject);
const
  ARR=' /MON:';
begin
  if TcxCheckBox(Sender).Checked and (edtnChanges.Text='') then
  begin
    showmessage('Number of Changes seen required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtnChanges.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtnChanges.Text, '',[rfIgnoreCase ]);
   edtnChanges.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox22Click(Sender: TObject);
const
  ARR=' /MOT:';
begin
  if TcxCheckBox(Sender).Checked and (edtnMins.Text='') then
  begin
    showmessage('Number of minutes to run required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtnMins.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtnMins.Text, '',[rfIgnoreCase ]);
   edtnMins.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox23Click(Sender: TObject);
var
  TimePar: String;
const
  ARR=' /RH:';
begin
  TimePar := edtStartTime.Text+'-' +edtStopTime.Text;
  TimePar :=  StringReplace(TimePar, ':', '',[rfReplaceAll]);;
  if TcxCheckBox(Sender).Checked then
  begin
    if strtotime(edtstoptime.Text) <= strtotime(edtStartTime.Text)  then
    begin
      showMessage('Stop time value must be greater than start time');
      TcxCheckBox(Sender).Checked := False;
      exit;
    end;
    copyParameters := copyParameters+Arr+TimePar;
  end
  else
  begin
    copyParameters:=StringReplace(copyParameters, ARR+TimePar, '',[rfIgnoreCase ]);
    edtStartTime.Clear;
    edtStopTime.Clear;
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox24Click(Sender: TObject);
const
  ARR=' /PF';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox25Click(Sender: TObject);
const
  ARR=' /IPG:';
begin
  if TcxCheckBox(Sender).Checked and (edtMSGap.Text='') then
  begin
    showmessage('Number of milli-secs required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMSGap.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMSGap.Text, '',[rfIgnoreCase ]);
   edtMSGap.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox2Click(Sender: TObject);
const
  ARR=' /E';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox3Click(Sender: TObject);
const
  ARR=' /LEV:';
begin
  if TcxCheckBox(Sender).Checked and (edtLev.Text='') then
  begin
    showmessage('Number of Levels required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtLev.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtLev.Text, '',[rfIgnoreCase ]);   //add
   edtLev.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox4Click(Sender: TObject);
const
  ARR=' /Z';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox5Click(Sender: TObject);
const
  ARR=' /B';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox6Click(Sender: TObject);
const
  ARR=' /ZB';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox7Click(Sender: TObject);
//B.P. QM-418
var I: Integer;
const
  ARR=' /COPY';
begin
  if TcxCheckBox(Sender).Checked then
  begin
    CopyFlags := ':';
    if (cxCheckCombobox1.ItemIndex <> -1) then
    begin
      copyParameters := copyParameters+ARR;
      with cxCheckComboBox1.Properties do
      for I := 0  to Items.Count - 1 do
      begin
        if cxCheckComboBox1.States[I] = cbsChecked then
         CopyFlags := CopyFlags +  cxCheckComboBox1.Properties.Items[I].ShortDescription;
      end;
      if CopyFlags <> ':' then
       copyParameters := copyParameters+CopyFlags
      else
        CopyFlags := '';
    end;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+CopyFlags, '',[rfIgnoreCase ]);
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.CheckBox8Click(Sender: TObject);
const
  ARR=' /SEC';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;



procedure TRoboCopyForm.CheckBox9Click(Sender: TObject);
const
  ARR=' /COPYALL';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;


procedure TRoboCopyForm.chkboxSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxSearch.Checked then
    CustLocatorView.Controller.ShowFindPanel
  else
    CustLocatorView.Controller.HideFindPanel;
end;

procedure TRoboCopyForm.cxCheckBox10Click(Sender: TObject);
const
  ARR=' /XX';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox11Click(Sender: TObject);
const
  ARR=' /XL';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox12Click(Sender: TObject);
const
  ARR=' /IS';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox13Click(Sender: TObject);
const
  ARR=' /IT';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox14Click(Sender: TObject);
const
  ARR=' /MAX:';
begin
  if TcxCheckBox(Sender).Checked and (edtMaxBytes.Text='') then
  begin
    showmessage('Max number of bytes required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMaxBytes.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMaxBytes.Text, '',[rfIgnoreCase ]);
   edtMaxBytes.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox15Click(Sender: TObject);
const
  ARR=' /MIN:';
begin
  if TcxCheckBox(Sender).Checked and (edtMinBytes.Text='') then
  begin
    showmessage('Number of Levels required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMinBytes.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMinBytes.Text, '',[rfIgnoreCase ]);
   edtMinBytes.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox16Click(Sender: TObject);
const
  ARR=' /MAXAGE:';
begin
  if TcxCheckBox(Sender).Checked and (edtMaxAge.Text='') then   //add
  begin
    showmessage('Number of days required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMaxAge.Text   //add
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMaxAge.Text, '',[rfIgnoreCase ]);   //add
   edtMaxAge.Text:='';   //add
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox17Click(Sender: TObject);
const
  ARR=' /R:';
begin
  if TcxCheckBox(Sender).Checked and (edtMinAge.Text='') then
  begin
    showmessage('Number of days required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMinAge.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMinAge.Text, '',[rfIgnoreCase ]);
   edtMinAge.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox18Click(Sender: TObject);
const
  ARR=' /MAXLAD:';
begin
  if TcxCheckBox(Sender).Checked and (edtMaxDays.Text='') then
  begin
    showmessage('Number of Levels required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMaxDays.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMaxDays.Text, '',[rfIgnoreCase ]);
   edtMaxDays.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox19Click(Sender: TObject);
const
  ARR=' /MINLAD:';
begin
  if TcxCheckBox(Sender).Checked and (edtMinDays.Text='') then
  begin
    showmessage('Number of days required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR+edtMinDays.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtMinDays.Text, '',[rfIgnoreCase ]);   //add
   edtMinDays.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox1Click(Sender: TObject);
const
  ARR=' /A ';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox20Click(Sender: TObject);
const
  ARR=' /XJ';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox21Click(Sender: TObject);
const
  ARR=' /R:';
begin
  if TcxCheckBox(Sender).Checked and (edtRChanges.Text='') then
  begin
    showmessage('Number of Retries required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
    if TcxCheckBox(Sender).Checked then
    copyParameters := copyParameters+ARR+edtRChanges.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtRChanges.Text, '',[rfIgnoreCase ]);
   edtRChanges.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox22Click(Sender: TObject);
const
  ARR=' /W:';
begin
  if TcxCheckBox(Sender).Checked and (edtWChanges.Text='') then
  begin
    showmessage('Wait time required');
    TcxCheckBox(Sender).Checked := False;
    exit;
  end else
    if TcxCheckBox(Sender).Checked then
    copyParameters := copyParameters+ARR+edtWChanges.Text
  else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+edtWChanges.Text, '',[rfIgnoreCase ]);
   edtWChanges.Text:='';
  end;
   arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox23Click(Sender: TObject);
const
  ARR=' /REG';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox24Click(Sender: TObject);
const
  ARR=' /TBD';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox25Click(Sender: TObject);
const
  ARR=' /L';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox26Click(Sender: TObject);
const
  ARR=' /X';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox27Click(Sender: TObject);
const
  ARR=' /V';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox28Click(Sender: TObject);
const
  ARR=' /TS';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox29Click(Sender: TObject);
const
  ARR=' /FP';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox2Click(Sender: TObject);
const
  ARR=' /M';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox30Click(Sender: TObject);
const
  ARR=' /NS';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox31Click(Sender: TObject);
const
  ARR=' /NC';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox32Click(Sender: TObject);
const
  ARR=' /NFL';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox33Click(Sender: TObject);
const
  ARR=' /NDL';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox34Click(Sender: TObject);
const
  ARR=' /NP';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox35Click(Sender: TObject);
const
  ARR=' /ETA';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox36Click(Sender: TObject);
//B.P. QM-418
const
  ARR=' /LOG:';
var FilePath: String;
begin
  FilePath := edtstatuslogoverwrite.Text;
  if TcxCheckBox(Sender).Checked then
  begin
    If (FilePath ='') then
    begin
      showmessage('File path required');
      TcxCheckBox(Sender).Checked := False;
      exit;
    end else
    begin
      if (ExtractFilePath(FilePath) = '') or (ExtractFileName(FilePath) = '') then
      begin
        showmessage('Invalid file path');
        TcxCheckBox(Sender).Checked := False;
        Exit;
      end;
    end;
    copyParameters := copyParameters+ARR+'"'+edtstatuslogoverwrite.Text+'"';
  end
  else
  begin
    copyParameters:=StringReplace(copyParameters, ARR+'"'+edtstatuslogoverwrite.Text+'"', '',[rfIgnoreCase ]);   //add
    edtstatuslogoverwrite.Text:='';
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox37Click(Sender: TObject);
//B.P. QM-418
const
  ARR=' /LOG+:';
  var FilePath: String;
begin
  FilePath := edtstatuslogappend.Text;
  if TcxCheckBox(Sender).Checked then
  begin
    If (FilePath ='') then   //add
    begin
      showmessage('File path required');
      TcxCheckBox(Sender).Checked := False;
      exit;
    end else
    begin
      if (ExtractFilePath(FilePath) = '') or (ExtractFileName(FilePath) = '') then
      begin
        showmessage('Invalid file path');
        TcxCheckBox(Sender).Checked := False;
        Exit;
      end else
      begin
        if not FileExists(FilePath) then
        begin
         showmessage('Log file for append not found');
         TcxCheckBox(Sender).Checked := False;
         Exit;
        end;
      end;
    end;
    copyParameters := copyParameters+ARR+'"'+edtstatuslogappend.Text+'"';
  end
  else
  begin
    copyParameters:=StringReplace(copyParameters, ARR+'"'+edtstatuslogappend.Text+'"', '',[rfIgnoreCase ]);   //add
    edtstatuslogappend.Text:='';
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox38Click(Sender: TObject);
const
  ARR=' /TEE';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox39Click(Sender: TObject);
const
  ARR=' /NJH';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox3Click(Sender: TObject);
//B.P. QM-418
var I: Integer;
const
  ARR=' /IA';
begin
  if TcxCheckBox(Sender).Checked then
  begin
    CopyAttrFiles := ':';
    if (cxCheckCombobox4.ItemIndex <> -1) then
    begin
      copyParameters := copyParameters+ARR;
      with cxCheckComboBox4.Properties do
      for I := 0  to Items.Count - 1 do
      begin
        if cxCheckComboBox4.States[I] = cbsChecked then
         CopyAttrFiles := CopyAttrFiles +  cxCheckComboBox4.Properties.Items[I].ShortDescription;
      end;
      if CopyAttrFiles <> ':' then
       copyParameters := copyParameters+CopyAttrFiles
      else
        CopyAttrFiles := '';
    end;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+CopyAttrFiles, '',[rfIgnoreCase ]);
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox40Click(Sender: TObject);
const
  ARR=' /NJS';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox4Click(Sender: TObject);
const
  //B.P. QM-418
  ARR=' /XA';        //edtRASHCNETOexc
var I: Integer;
begin
  if TcxCheckBox(Sender).Checked then
  begin
    ExcludeAttrFiles := ':';
    if (cxCheckCombobox5.ItemIndex <> -1) then
    begin
      copyParameters := copyParameters+ARR;
      with cxCheckComboBox5.Properties do
      for I := 0  to Items.Count - 1 do
      begin
        if cxCheckComboBox5.States[I] = cbsChecked then
         ExcludeAttrFiles := ExcludeAttrFiles +  cxCheckComboBox5.Properties.Items[I].ShortDescription;
      end;
      if ExcludeAttrFiles <> ':' then
       copyParameters := copyParameters+ExcludeAttrFiles
      else
        ExcludeAttrFiles := '';
    end;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, ARR+ExcludeAttrFiles, '',[rfIgnoreCase ]);
  end;
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox5Click(Sender: TObject);
const
  ARR=' /XF ';
var
  I: Integer;
  FilesExcluded: String;
begin
  FilesExcluded := ARR;
  for I := 0 to Length(FileValues)-1 do
    FilesExcluded := FilesExcluded + FileValues[I] + ' ';
  Delete(FilesExcluded, Length(FilesExcluded), 1);
  if TcxCheckBox(Sender).Checked then
  begin
    for I := 0 to Length(FileValues)-1 do
    begin
      if Trim(FileValues[I]) = '' then
      begin
        showmessage('Enter all File Values');
        TcxCheckBox(Sender).Checked := False;
        exit;
      end;
    end;
    copyParameters := copyParameters+ FilesExcluded;
    arrTextEdit.text := copyParameters;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, FilesExcluded, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
  end;
end;

procedure TRoboCopyForm.cxCheckBox6Click(Sender: TObject);
var
  I: Integer;
  FolderPaths: String;
const
  ARR=' /XD ';
begin
  FolderPaths := ARR;
  for I := 0 to Length(DirValues)-1 do
    FolderPaths := FolderPaths + '"' + DirValues[I] + '"' + ' ';
  Delete(FolderPaths, Length(FolderPaths), 1);
  if TcxCheckBox(Sender).Checked then
  begin
    for I := 0 to Length(DirValues)-1 do
    begin
      if not DirectoryExists(DirValues[I]) then
      begin
        showmessage('Invalid Folder: ' + DirValues[I]);
        TcxCheckBox(Sender).Checked := False;
        exit;
      end;
    end;
    copyParameters := copyParameters+ FolderPaths;
    arrTextEdit.text := copyParameters;
  end else
  begin
   copyParameters:=StringReplace(copyParameters, FolderPaths, '',[rfIgnoreCase ]);
   arrTextEdit.text := copyParameters;
  end;
end;

procedure TRoboCopyForm.cxCheckBox7Click(Sender: TObject);
const
  ARR=' /XC';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox8Click(Sender: TObject);
const
  ARR=' /XN';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckBox9Click(Sender: TObject);
const
  ARR=' /XO';
begin
  if TcxCheckBox(Sender).Checked then
  copyParameters := copyParameters+ARR
  else
   copyParameters:=StringReplace(copyParameters, ARR, '',[rfIgnoreCase ]);
  arrTextEdit.text := copyParameters;
end;

procedure TRoboCopyForm.cxCheckComboBox1PropertiesPopup(Sender: TObject);
begin
  inherited;
  CheckBox7.Checked := False;
end;

procedure TRoboCopyForm.cxCheckComboBox2PropertiesPopup(Sender: TObject);
begin
  inherited;
  CheckBox15.Checked := False;
end;

procedure TRoboCopyForm.cxCheckComboBox3PropertiesPopup(Sender: TObject);
begin
  inherited;
  CheckBox16.Checked := False;
end;

procedure TRoboCopyForm.cxCheckComboBox4PropertiesPopup(Sender: TObject);
begin
  inherited;
  cxCheckBox3.Checked := False;
end;

procedure TRoboCopyForm.cxCheckComboBox5PropertiesPopup(Sender: TObject);
begin
  inherited;
  cxCheckBox4.Checked := False;
end;

procedure TRoboCopyForm.dsCustomerDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  SourceDir.Text := qryCustomer.FieldByName('RoboSource').AsString;
  DestDir.Text := qryCustomer.FieldByName('RoboDest').AsString;
end;

procedure TRoboCopyForm.FormShow(Sender: TObject);
begin
  inherited;
  OpenDataSets;
  qryCustomer.Properties['Update Criteria'].value :=adCriteriaKey;
  ForceDirectories('C:\Plats\log');
end;

procedure TRoboCopyForm.GetDirectory(Sender: TObject);
var
  OpenDialog: TFileOpenDialog;
  SelectedFolder: string;
begin
  OpenDialog := TFileOpenDialog.Create(self);
  try
    OpenDialog.Options := OpenDialog.Options + [fdoPickFolders];
    if not OpenDialog.Execute then
      Abort;

    if (Sender as TcxButton).Name = 'btnFindSource' then
      SourceDir.text := OpenDialog.FileName
    else if (Sender as TcxButton).Name = 'btnFindDest' then
      DestDir.text := OpenDialog.FileName
    else if (Sender as TcxButton).Name = 'btnLogFolder' then
      logfolder.text := OpenDialog.FileName;

  finally
    OpenDialog.Free;
  end;
end;

procedure TRoboCopyForm.insCustomerAfterInsert(DataSet: TDataSet);
begin
  inherited;

end;

//B.P. QM-418
function TRoboCopyForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TRoboCopyForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TRoboCopyForm.CloseDataSets;
begin
  inherited;
  qryCustomer.Close;
  qryLookupCust.Close;
  qryLookupEmp.Close;
  SetLength(DirValues,0);
  SetLength(FileValues,0);
  SetLength(DirNames,0);
  SetLength(FileNames,0);
end;

procedure TRoboCopyForm.CustLocatorViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
 if AViewInfo.GridRecord.Focused = true then
  begin
    ACanvas.Brush.Color :=  clSkyBlue;
    ACanvas.Font.Color := clWindowText;
  end
  else
  begin
    ACanvas.Brush.Color :=   clBtnFace;
    ACanvas.Font.Color := clWindowText;
  end;
end;

procedure TRoboCopyForm.OpenDataSets;
begin
  inherited;
  qryCustomer.Open;
  qryLookupCust.Open;
  qryLookupEmp.Open;
end;

procedure TRoboCopyForm.qryCustomerBeforeDelete(DataSet: TDataSet);
var
  s: String;
begin
  inherited;
  qryCustomer.Properties['Unique Table'].Value:='customer_locator';
  s := 'Customer ' +  qryCustomer.FieldByName('customer_id').AsString + ', emp_id  ' +
   qryCustomer.FieldByName('loc_emp_id').AsString + ' deleted from customer_locator';
   AdminDM.AdminChangeTable('customer_locator', s, now);
end;

procedure TRoboCopyForm.qryCustomerBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  showmessage('inserted');
end;

procedure TRoboCopyForm.SourceDirKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #34 then key := #0;
end;

end.
