unit DycomPeriod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, StdCtrls, cxNavigator, AdminDmu,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TDycomPeriodForm = class(TBaseCxListForm)
    ColStartingDate: TcxGridDBColumn;
    ColEndingDate: TcxGridDBColumn;
    ColPeriod: TcxGridDBColumn;
    ColShortPeriod: TcxGridDBColumn;
    cbDycomPeriodSearch: TCheckBox;
    procedure DataBeforePost(DataSet: TDataSet);
    procedure cbDycomPeriodSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    DPInserted: Boolean;
  public
    procedure AbortWithMessage(const MessageString: string);
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  DycomPeriodForm: TDycomPeriodForm;

implementation

uses DateUtils, OdIsoDates, OdDbUtils, OdExceptions;

{$R *.dfm}

procedure TDycomPeriodForm.AbortWithMessage(const MessageString: string);
begin
  raise EOdDataEntryError.Create(MessageString);
end;

procedure TDycomPeriodForm.cbDycomPeriodSearchClick(Sender: TObject);
begin
 inherited;
 if cbDycomPeriodSearch.Checked then
   GridView.Controller.ShowFindPanel
 else
   GridView.Controller.HideFindPanel;
end;

procedure TDycomPeriodForm.ActivatingNow;
begin
  inherited;
  DPInserted := False;
end;

procedure TDycomPeriodForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DPInserted := False;
end;

procedure TDycomPeriodForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'dycom_period', 'starting', DPInserted);
end;

procedure TDycomPeriodForm.DataBeforeEdit(DataSet: TDataSet);
begin
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TDycomPeriodForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  DPInserted := True;
end;

procedure TDycomPeriodForm.DataBeforePost(DataSet: TDataSet);
var
  Period: string;
begin
  inherited;
  Period := DataSet.FieldByName('period').AsString;
  if DataSet.FieldByName('starting').AsDateTime >= DataSet.FieldByName('ending').AsDateTime then
    AbortWithMessage('Starting Date must be before Ending Date.');
  if DayOfTheWeek(DataSet.FieldByName('starting').AsDateTime) <> DaySunday then
    AbortWithMessage('Starting Date must be on a Sunday.');
  if DayOfTheWeek(DataSet.FieldByName('ending').AsDateTime) <> DaySunday then
    AbortWithMessage('Ending Date must be on a Sunday.');
  if IsoStrToDateDef(Period + '-01') = 0 then
    AbortWithMessage('Period must be in the form of YYYY-MM');

  //passed all validation, now generate short_period
  DataSet.FieldByName('short_period').AsString := Copy(Period,6,2) + '-' + Copy(Period,3,2);
end;

procedure TDycomPeriodForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TDycomPeriodForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TDycomPeriodForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbDycomPeriodSearch.Enabled := True;
end;

end.
