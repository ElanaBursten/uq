unit EPRDisclaimer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxMemo,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges;

type
  TEPRDisclaimerForm = class(TBaseCxListForm)
    GridViewdisclaimer_id: TcxGridDBColumn;
    GridViewdisclaimer_text: TcxGridDBColumn;
    chkboxDisclaimerSearch: TCheckBox;
    procedure DataBeforePost(DataSet: TDataSet);
    procedure chkboxDisclaimerSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    {Private declarations}
    EPRDisclaimerInserted: Boolean;
  public
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  EPRDisclaimerForm: TEPRDisclaimerForm;

implementation
uses OdCxUtils, OdDbUtils, AdminDMu;

{$R *.dfm}

{ TEPRQueueRulesForm }

procedure TEPRDisclaimerForm.chkboxDisclaimerSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxDisclaimerSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TEPRDisclaimerForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  EPRDisclaimerInserted := False;
end;

procedure TEPRDisclaimerForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'disclaimer', 'disclaimer_id', EPRDisclaimerInserted);
end;

procedure TEPRDisclaimerForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TEPRDisclaimerForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  EPRDisclaimerInserted := True;
end;

procedure TEPRDisclaimerForm.DataBeforePost(DataSet: TDataSet);
const
  Msg = 'Required Fields are missing. ' + #13#10;
  MsgReq = 'Required: Disclaimer Text' + #13#10 + #13#10;
  Msg2 = 'Click OK to Continue or Abort to Cancel changes.';
begin
  inherited;
  if (DataSet.FieldByName('disclaimer_text').isNull) then begin
    {Cancel changes}
    If MessageDlg(Msg + MsgReq + Msg2, mtWarning, [mbOK, mbAbort], 0) = mrAbort then begin
      DataSet.ClearFields;
      DataSet.Cancel;
      Abort;
    end
    {Continue with changes, but don't try to post}
    else
      Abort;
  end;
//  else
//    DataSet.FieldByName('modified_date').AsDateTime := Now;

end;

procedure TEPRDisclaimerForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEPRDisclaimerForm.ActivatingNow;
begin
  inherited;
  EPRDisclaimerInserted := False;
end;

procedure TEPRDisclaimerForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TEPRDisclaimerForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxDisclaimerSearch.Enabled := True;
end;

end.
