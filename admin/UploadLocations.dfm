inherited UploadLocationsForm: TUploadLocationsForm
  Left = 290
  Top = 218
  Caption = 'Upload Locations'
  ClientHeight = 389
  ClientWidth = 910
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 910
    Height = 28
    object cbUploadLocationSearch: TCheckBox
      Left = 18
      Top = 8
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbUploadLocationSearchClick
    end
    object ActiveLocationsFilter: TCheckBox
      Left = 151
      Top = 8
      Width = 140
      Height = 12
      Caption = 'Active Locations Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveLocationsFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 910
    Height = 334
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'location_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 139
      end
      object ColGroupName: TcxGridDBColumn
        Caption = 'Upload Group'
        DataBinding.FieldName = 'group_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 91
      end
      object ColServer: TcxGridDBColumn
        Caption = 'Server'
        DataBinding.FieldName = 'server'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 122
      end
      object ColPort: TcxGridDBColumn
        Caption = 'Port'
        DataBinding.FieldName = 'port'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 64
      end
      object ColDirectory: TcxGridDBColumn
        Caption = 'Directory'
        DataBinding.FieldName = 'directory'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 137
      end
      object ColUsername: TcxGridDBColumn
        Caption = 'User Name'
        DataBinding.FieldName = 'username'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 147
      end
      object ColPassword: TcxGridDBColumn
        Caption = 'Password'
        DataBinding.FieldName = 'password'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 129
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 51
      end
      object ColLocationID: TcxGridDBColumn
        Caption = 'Location ID'
        DataBinding.FieldName = 'location_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 64
      end
    end
  end
  object InfoPanel: TPanel [2]
    Left = 0
    Top = 362
    Width = 910
    Height = 27
    Align = alBottom
    BevelOuter = bvNone
    Caption = 
      'The desired upload group should be typed in as the modifier for ' +
      'the employee'#39's "Upload Group" right.  Use * for global locations' +
      '.'
    TabOrder = 2
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from upload_location where active = 1'
  end
end
