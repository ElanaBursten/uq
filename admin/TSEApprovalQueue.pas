unit TSEApprovalQueue;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Vcl.StdCtrls, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls,
  cxDBExtLookupComboBox, cxMaskEdit, AdminDmu;

type
  TApprovalQueueForm = class(TBaseCxListForm)
    chkboxSearch: TCheckBox;
    ColEntryID: TcxGridDBColumn;
    ColWorkEmpID: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColWorkDate: TcxGridDBColumn;
    ColQR: TcxGridDBColumn;
    ColQC: TcxGridDBColumn;
    ColNote: TcxGridDBColumn;
    Employees: TADODataSet;
    EmployeeDS: TDataSource;
    ColEmpName: TcxGridDBColumn;
    EmployeeView: TcxGridDBTableView;
    EmpNameCol: TcxGridDBColumn;
    EmpIDCol: TcxGridDBColumn;
    DisableTrigger: TADOQuery;
    procedure chkboxSearchClick(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
    tseApprovalInserted: Boolean;
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;

var
  ApprovalQueueForm: TApprovalQueueForm;

implementation

{$R *.dfm}

procedure TApprovalQueueForm.chkboxSearchClick(Sender: TObject);
begin
  inherited;
    inherited;
  if chkboxSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TApprovalQueueForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  tseApprovalInserted := False;
end;

procedure TApprovalQueueForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  //disallow Trigger for Admin updates
  DisableTrigger.SQL.Text := 'alter table tse_approvalqueue enable trigger tseApprovalUpdTrigger';
  DisableTrigger.ExecSQL;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'TSE_ApprovalQueue', 'TSE_Entry_Id', tseApprovalInserted);
end;

procedure TApprovalQueueForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TApprovalQueueForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  tseApprovalInserted := True;
end;

procedure TApprovalQueueForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  if Data.FieldByName('work_emp_id').IsNull then
   raise Exception.Create('An emp_id is required to save a TSEApproval record');
  DisableTrigger.SQL.Text := 'alter table tse_approvalqueue disable trigger tseApprovalUpdTrigger';
  DisableTrigger.ExecSQL;
end;

procedure TApprovalQueueForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TApprovalQueueForm.CloseDataSets;
begin
  inherited;
  Employees.Close;
end;

procedure TApprovalQueueForm.ActivatingNow;
begin
  inherited;
  tseApprovalInserted := False;
 // SetReadOnly(True);
end;

procedure TApprovalQueueForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TApprovalQueueForm.OpenDataSets;
begin
  inherited;
   Employees.Open;
end;

procedure TApprovalQueueForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxSearch.Enabled := True;
end;

end.
