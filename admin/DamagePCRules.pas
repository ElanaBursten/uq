unit DamagePCRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, ActnList, StdCtrls, AdminDMu,
  OdExceptions, QMConst, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxDropDownEdit, cxBlobEdit,
  cxTextEdit, cxSpinEdit, cxCalendar, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxDBLookupComboBox,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  TDamagePCRulesForm = class(TBaseCxListForm)
    BottomPanel: TPanel;
    InstructionLabel: TLabel;
    ColRuleId: TcxGridDBColumn;
    ColPCCode: TcxGridDBColumn;
    LookupColState: TcxGridDBColumn;
    ColCounty: TcxGridDBColumn;
    ColCity: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    StatesRef: TADODataSet;
    StatesDS: TDataSource;
    chkboxPCRulesSearch: TCheckBox;
    ActivePCRulesFilter: TCheckBox;
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure chkboxPCRulesSearchClick(Sender: TObject);
    procedure ActivePCRulesFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    DamagePCInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses OdCxUtils, OdDbUtils, odHourGlass, OdMiscUtils;

{$R *.dfm}

procedure TDamagePCRulesForm.OpenDataSets;
begin
  inherited;
  StatesRef.Open;
  GridView.DataController.FocusedRowIndex := 0;
end;

procedure TDamagePCRulesForm.ActivePCRulesFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('rule_id').AsInteger;
  Data.Close;
  if ActivePCRulesFilter.Checked then
    Data.CommandText := 'select * from damage_profit_center_rule where active = 1' else
      Data.CommandText := 'select * from damage_profit_center_rule';
  Data.Open;
  Data.Locate('rule_id', FSelectedID, []);
end;

procedure TDamagePCRulesForm.chkboxPCRulesSearchClick(Sender: TObject);
begin
 inherited;
 if chkboxPCRulesSearch.Checked then
   GridView.Controller.ShowFindPanel
 else
   GridView.Controller.HideFindPanel;
end;

procedure TDamagePCRulesForm.CloseDataSets;
begin
  StatesRef.Close;
  inherited;
end;

procedure TDamagePCRulesForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetProfitCenterList(CxComboBoxItems(ColPCCode), False);
end;

procedure TDamagePCRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxPCRulesSearch.Enabled := True;
  ActivePCRulesFilter.Enabled := True;
end;

procedure TDamagePCRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DamagePCInserted := False;
end;

procedure TDamagePCRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'damage_profit_center_rule', 'rule_id', DamagePCInserted);
end;

procedure TDamagePCRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TDamagePCRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  DamagePCInserted := True;
end;

procedure TDamagePCRulesForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('DamagePCState', Data, 'state', StatesRef, 'code', 'description');
    //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  Data.FieldByName('DamagePCState').LookupCache := True;
end;

procedure TDamagePCRulesForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (trim(DataSet.FieldByName('state').AsString) = '*') then
    raise EOdDataEntryError.Create('State cannot be a wildcard (*) value.  Please enter a valid state.');
  DataSet.FieldByName('modified_date').Value := Now;
end;

procedure TDamagePCRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('active').Value := True;
  DataSet.FieldByName('modified_date').Value := Now;
end;

procedure TDamagePCRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TDamagePCRulesForm.ActivatingNow;
begin
  inherited;
  DamagePCInserted := False;
end;

procedure TDamagePCRulesForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TDamagePCRulesForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

end.
