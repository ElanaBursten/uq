unit HALParserLogErrors;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, OdEmbeddable, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Samples.Spin, System.IOUtils, System.StrUtils, System.Types, MainFu;

type
  TParserLogErrorsForm = class(TEmbeddableForm)
    Panel1: TPanel;
    btnOpen: TButton;
    Label1: TLabel;
    SpinEditDaysBack: TSpinEdit;
    OpenDialog1: TOpenDialog;
    lvParserLog: TListView;
    Memo1: TMemo;
    procedure btnOpenClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    function IsReadOnly: Boolean; override;
  end;

var
  ParserLogErrorsForm: TParserLogErrorsForm;

implementation

{$R *.dfm}

procedure TParserLogErrorsForm.btnOpenClick(Sender: TObject);
var
  StringArr: TStringDynArray;
  FileNameDate: TDateTime;
  FileNameDateStr: String;
  CurrentTime: TDateTime;
  CurrentTimeToCompareWith: String;
  StrList: TStringList;
  Itm: TListItem;
begin
  if OpenDialog1.Execute then
  begin
    try
     StrList := TStringList.Create;
     StringArr := SplitString(TPath.GetFileNameWithoutExtension(OpenDialog1.FileName), '-');
     try
       FileNameDate := EncodeDate(StrtoInt(StringArr[2]), StrtoInt(StringArr[3]),StrtoInt(StringArr[4]));
       FileNameDateStr := FormatDateTime('yyyy-mm-dd', FileNameDate);
       CurrentTime := Now - SpinEditDaysBack.Value;
       CurrentTimetoComparewith := FormatDateTime('yyyy-mm-dd', CurrentTime);

       If String.Equals(FileNameDateStr, currentTimeToCompareWith) Then
       begin
         Memo1.Clear;
         StrList.Clear;
         StrList.LoadFromFile(OpenDialog1.FileName);
         if UpperCase(StrList.Text).Contains('DEADLOCK') then
         begin
           Itm := lvParserLog.Items.Add;
           Itm.Caption := OpenDialog1.FileName;
           Memo1.Lines.Assign(StrList);
         end else Showmessage('No deadlock error found');
       end;
     except
       On E:EConvertError do
         showmessage(OpenDialog1.Filename + ':' + #13#10 +
         'invalid file name (sample name: Ticketparser-FCO2-2023-02-31.txt)');
     end;
    finally
     StrList.Free;
    end;
  end;
end;

function TParserLogErrorsForm.IsReadOnly: Boolean;
begin
   Result := False;
   MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TParserLogErrorsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
end;

end.

