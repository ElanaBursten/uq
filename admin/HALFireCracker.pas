unit HALFireCracker;

interface

uses
  Winapi.Windows, Winapi.Messages, System.TimeSpan, System.IOUtils, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, OdEmbeddable,
  Vcl.ComCtrls, DateUtils, MainFu;

type
  TFireCrackerForm = class(TEmbeddableForm)
    Panel1: TPanel;
    btnOpen: TButton;
    Label1: TLabel;
    lvFireCracker: TListView;
    FileOpenDialog1: TFileOpenDialog;
    btrnClear: TButton;
    procedure btnOpenClick(Sender: TObject);
    procedure lvFireCrackerColumnClick(Sender: TObject; Column: TListColumn);
    procedure lvFireCrackerCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure lvFireCrackerCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure btrnClearClick(Sender: TObject);
  private
    { Private declarations }
    Descending: Boolean;
    ColumnToSort: Integer;
    FilePath :String;
  public
    { Public declarations }
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    function IsReadOnly: Boolean; override;
  end;

var
  FireCrackerForm: TFireCrackerForm;

implementation

{$R *.dfm}

//lists Firecracker directory files and lastwritetime filedates
procedure TFireCrackerForm.btnOpenClick(Sender: TObject);
var
  SR : TSearchRec;
  Itm: TListItem;
  SearchResult: TSearchRec;
  s: Integer;
  FileDate:String;
begin
  lvFireCracker.Clear;
  if FileOpenDialog1.Execute then
  begin
    FilePath := IncludeTrailingBackslash(FileOpenDialog1.FileName);
    try
      if FindFirst(FilePath + '*', faAnyFile, SR) = 0 then
      begin
        lvFireCracker.Items.BeginUpdate;
        repeat
          if (SR.attr and faDirectory) <> faDirectory then
          begin
            With lvFireCracker do
            begin
              Itm := Items.Add;
              canvas.Font.Assign(font);
              Itm.Caption := SR.Name;
              FileDate := DateTimetoStr(TFile.GetLastWriteTime(IncludeTrailingBackslash(FilePath)+SR.Name));
              Itm.SubItems.Add(FileDate);
              s := canvas.TextWidth(Items[Itm.Index].Caption) + 10;
              if s > Columns[0].Width then
              Columns[0].Width := s;
            end;
          end;
        until FindNext(SR) <> 0;
        lvFireCracker.Items.EndUpdate;
      end;
    finally
      FindClose(SR);
    end;
  end;
end;

procedure TFireCrackerForm.lvFireCrackerColumnClick(Sender: TObject;
  Column: TListColumn);    //column sort functions
begin
  TListView(Sender).SortType := stNone;
  if Column.Index<>ColumnToSort then
  begin
    ColumnToSort := Column.Index;
    Descending := False;
  end
  else
    Descending := not Descending;
  TListView(Sender).SortType := stText;
end;

function CompareTextAsDateTime(const s1, s2: string): Integer;
begin
  Result := CompareDateTime(StrToDateTime(s1), StrToDateTime(s2));
end;

procedure TFireCrackerForm.lvFireCrackerCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  Case ColumnToSort of
    0:
     Compare := CompareText(Item1.Caption, Item2.Caption);
    1:
     Compare := CompareTextAsDateTime(Item1.subitems[0],Item2.subitems[0]);
   end;
   if Descending then Compare := -Compare;
end;

procedure TFireCrackerForm.lvFireCrackerCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
const
  cStripe = $CCFFCC;  //color files older than 4 hours
var
  TS: TTimeSpan;
  StartDate, EndDate: TDateTime;
  Temp: Double;
begin
  EndDate := Now;
  StartDate := StrtoDateTime(Item.SubItems[0]);
  TS := TTimeSpan.Subtract(EndDate, StartDate);
  Temp := TS.TotalHours;

  if Temp > 4.0 then
    // Items older than 4 hours have a green background
   lvFireCracker.Canvas.Brush.Color := cStripe
 else
   lvFireCracker.Canvas.Brush.Color := clWindow;
end;

procedure TFireCrackerForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  lvFireCracker.Enabled := True;
end;

procedure TFireCrackerForm.btrnClearClick(Sender: TObject);
begin
  inherited;
   lvFireCracker.Clear;
end;

function TFireCrackerForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

end.
