unit HALAttachments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, AdminDMu, mshtml,
  Vcl.Samples.Spin, Web.HTTPApp, Web.DBWeb, Vcl.OleCtrls, SHDocVw, OdInternetUtil, ActiveX,
  Vcl.Grids, Vcl.DBGrids, IniFiles, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxMaskEdit, cxTextEdit, MainFu, OdDbUtils;

type
  TAttachmentsForm = class(TBaseCxListForm)
    chkboxRespondToSearch: TCheckBox;
    ColShortName: TcxGridDBColumn;
    ColContactPhone: TcxGridDBColumn;
    ColPending: TcxGridDBColumn;
    Label1: TLabel;
    btnDateAdd: TButton;
    SpinEditDateAdd: TSpinEdit;
    DataSetTableProducer: TDataSetTableProducer;
    btnEmails: TButton;
    Datashort_name: TStringField;
    DataPhone: TStringField;
    DataPending: TIntegerField;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    WebBrowser1: TWebBrowser;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColDaysback: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    procedure chkboxRespondToSearchClick(Sender: TObject);
    procedure btnDateAddClick(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
  private
    { Private declarations }
    procedure LoadHTML(HTMLCode: string);
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  AttachmentsForm: TAttachmentsForm;

implementation

{$R *.dfm}

procedure TAttachmentsForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  if not HalConfig.EOF then
    spineditdateadd.Value := HalConfig.FieldByName('daysback').AsInteger;
  SetReadOnly(True);
end;

procedure TAttachmentsForm.btnDateAddClick(Sender: TObject);
begin
  inherited;
  CloseDataSets;
  OpenDataSets;
end;


procedure TAttachmentsForm.LoadHTML(HTMLCode: string);
var
  sl: TStringList;
  ms: TMemoryStream;
  Doc: IHTMLDocument2;
begin
  WebBrowser1.Navigate('about:blank');
  while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do
   Application.ProcessMessages;

  if Assigned(WebBrowser1.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := HTMLCode;
        sl.SaveToStream(ms);
        ms.Seek(0, 0);
        (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms));
      finally
        ms.Free;
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure TAttachmentsForm.chkboxRespondToSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxRespondToSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TAttachmentsForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TAttachmentsForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TAttachmentsForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TAttachmentsForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;;
end;

function TAttachmentsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TAttachmentsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  chkboxRespondToSearch.Enabled := True;
  btnDateAdd.Enabled := True;
  btnEmails.Enabled := True;
end;

procedure TAttachmentsForm.LeavingNow;
begin
  CloseDataSets;
end;

procedure TAttachmentsForm.OpenDataSets;
begin
  HalConfig.Open;
  Data.Parameters.ParamByName('dateadd').Value := SpinEditDateAdd.Value;
  inherited;
end;


end.
