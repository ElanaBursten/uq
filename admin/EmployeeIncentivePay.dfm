inherited EmployeeIncentivePayForm: TEmployeeIncentivePayForm
  Left = 273
  Top = 209
  Caption = 'Employee Incentive Pay'
  ClientWidth = 706
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 706
    Height = 28
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 706
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object cbEmpIncentiveSearch: TCheckBox
        Left = 26
        Top = 8
        Width = 97
        Height = 14
        Caption = 'Show Find Panel'
        TabOrder = 0
        OnClick = cbEmpIncentiveSearchClick
      end
      object ActiveEmpIncentiveFilter: TCheckBox
        Left = 148
        Top = 8
        Width = 140
        Height = 12
        Caption = 'Active Dates Only'
        Checked = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        OnClick = ActiveEmpIncentiveFilterClick
      end
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 706
    Height = 276
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'incentive_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.HeaderEndEllipsis = True
      object GridViewEmployeeLookup: TcxGridDBColumn
        Caption = 'Employee Name'
        DataBinding.FieldName = 'EmployeeLookup'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListColumns = <
          item
            FieldName = 'short_name'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = EmployeeRefDS
        Width = 169
      end
      object GridViewEmpNumber: TcxGridDBColumn
        Caption = 'Emp Number'
        DataBinding.FieldName = 'EmployeeNumLookup'
        Options.Editing = False
        Options.Focusing = False
        Width = 77
      end
      object GridViewemp_id: TcxGridDBColumn
        Caption = 'Employee ID'
        DataBinding.FieldName = 'emp_id'
        Options.Editing = False
        Options.Focusing = False
        Width = 75
      end
      object GridViewIncentiveTypeLookup: TcxGridDBColumn
        Caption = 'Incentive Type'
        DataBinding.FieldName = 'IncentiveTypeLookup'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'code'
        Properties.ListColumns = <
          item
            FieldName = 'description'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = IncPayTypeRefDS
        Width = 102
      end
      object GridViewrate: TcxGridDBColumn
        Caption = 'Rate'
        DataBinding.FieldName = 'rate'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DecimalPlaces = 2
        Properties.EditFormat = '$,0.00;($,0.00)'
        Properties.MaxLength = 8
        Properties.MaxValue = 99999.990000000010000000
        Properties.Nullable = False
        Width = 92
      end
      object GridViewactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Options.Filtering = False
        Width = 44
      end
    end
    object EmployeeLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmployeeRefDS
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object EmployeeViewLookupColumn: TcxGridDBColumn
        DataBinding.FieldName = 'short_name'
      end
    end
    object IncentivePayTypeLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = IncPayTypeRefDS
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object IncentivePayTypeLookupViewColumn: TcxGridDBColumn
        DataBinding.FieldName = 'description'
      end
    end
  end
  object InstrPanel: TPanel [2]
    Left = 0
    Top = 304
    Width = 706
    Height = 96
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object InfoLabelStat: TLabel
      Left = 8
      Top = 12
      Width = 369
      Height = 70
      AutoSize = False
      Caption = 
        'The Incentive Pay rates are inherited by employees from their ma' +
        'nager'#39's setting entered here. A rate set at the top of the hiera' +
        'rchy applies to all employees in the Incentive Pay program. A di' +
        'fferent rate can be set lower in the hierarchy if some employees' +
        ' get a different rate than other employees.'
      WordWrap = True
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from employee_pay_incentive where active = 1'
  end
  object EmployeeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select emp_id, emp_number, short_name from employee where active' +
      ' = 1'
    Parameters = <>
    Left = 128
    Top = 192
  end
  object IncPayTypeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference where type = '#39'incpaytype'#39
    Parameters = <>
    Left = 48
    Top = 200
  end
  object IncPayTypeRefDS: TDataSource
    DataSet = IncPayTypeRef
    Left = 64
    Top = 248
  end
  object EmployeeRefDS: TDataSource
    DataSet = EmployeeRef
    Left = 160
    Top = 248
  end
end
