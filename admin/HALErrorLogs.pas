unit HALErrorLogs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, OdEmbeddable, Vcl.StdCtrls,
  Vcl.Samples.Spin, Vcl.ExtCtrls, Vcl.ComCtrls, System.Types, System.IOUtils,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxTextEdit,
  Data.Win.ADODB, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, MainFu;

type
  TErrorLogsForm = class(TEmbeddableForm)
    TopPanel: TPanel;
    lvErrorLog: TListView;
    btnErrorLogs: TButton;
    Label1: TLabel;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    procedure btnErrorLogsClick(Sender: TObject);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    function IsReadOnly: Boolean; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
  end;

var
  ErrorLogsForm: TErrorLogsForm;

implementation

{$R *.dfm}

procedure TErrorLogsForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  SetReadOnly(True);
end;

procedure TErrorLogsForm.btnErrorLogsClick(Sender: TObject);
var
 Folders: TStringDynArray;
 Files: TStringDynArray;
 I,J: Integer;
 Itm: TListItem;
const
 baseDirectory = '\\dyatl-pqmras01\E$\QM\Tickets';
begin
  inherited;
  try
   Folders := TDirectory.GetDirectories(baseDirectory, 'error', TSearchOption.soAllDirectories) ;
   if Length(Folders) > 0 then
   begin
     for I := 0 to Length(Folders)-1 do
     begin
       Files := TDirectory.GetFiles(Folders[I]);
       if Length(Files) > 0 then
       begin
         for J := 0 to Length(Files)-1 do
         begin
           Itm := lvErrorLog.Items.Add;
           Itm.Caption := Folders[I];
           Itm.SubItems.Add(ExtractFileName(Files[J]));
         end;
       end else showmessage('No files found in ' + Folders[I])
     end;
   end else showmessage('No error folders found in ' +  baseDirectory);
  except
   on E:Exception do
    Showmessage(E.Message);
  end;
end;

procedure TErrorLogsForm.CloseDataSets;
begin
  inherited;
   HalConfig.Close;
end;


procedure TErrorLogsForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TErrorLogsForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TErrorLogsForm.OpenDataSets;
begin
  inherited;
  HalConfig.Open;
end;

procedure TErrorLogsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
end;

function TErrorLogsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

end.
