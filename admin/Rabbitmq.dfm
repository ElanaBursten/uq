inherited RabbitmqForm: TRabbitmqForm
  Caption = 'Rabbit MQ Admin'
  ClientHeight = 402
  ClientWidth = 927
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 927
    object cbRabbitmqSearch: TCheckBox
      Left = 34
      Top = 10
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbRabbitmqSearchClick
    end
    object ActiveQueuesFilter: TCheckBox
      Left = 173
      Top = 10
      Width = 156
      Height = 17
      Caption = 'Active Queues Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveQueuesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Width = 927
    Height = 361
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Delete.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      object GridViewqueue_name: TcxGridDBColumn
        DataBinding.FieldName = 'queue_name'
        DataBinding.IsNullValueType = True
        Width = 107
      end
      object GridViewProcess_App: TcxGridDBColumn
        Caption = 'Process App'
        DataBinding.FieldName = 'Process_App'
        DataBinding.IsNullValueType = True
        Width = 96
      end
      object GridViewactive: TcxGridDBColumn
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
      end
      object GridViewhost: TcxGridDBColumn
        DataBinding.FieldName = 'host'
        DataBinding.IsNullValueType = True
        Width = 57
      end
      object GridViewport: TcxGridDBColumn
        DataBinding.FieldName = 'port'
        DataBinding.IsNullValueType = True
      end
      object GridViewvirtual_host: TcxGridDBColumn
        Caption = 'virtual host'
        DataBinding.FieldName = 'virtual_host'
        DataBinding.IsNullValueType = True
        Width = 92
      end
      object GridViewusername: TcxGridDBColumn
        DataBinding.FieldName = 'username'
        DataBinding.IsNullValueType = True
        Width = 95
      end
      object GridViewpassword: TcxGridDBColumn
        DataBinding.FieldName = 'password'
        DataBinding.IsNullValueType = True
        Width = 70
      end
      object GridViewuse_ssl: TcxGridDBColumn
        Caption = 'use ssl'
        DataBinding.FieldName = 'use_ssl'
        DataBinding.IsNullValueType = True
      end
      object GridViewexchange: TcxGridDBColumn
        DataBinding.FieldName = 'exchange'
        DataBinding.IsNullValueType = True
        Width = 67
      end
      object GridViewrouting_key: TcxGridDBColumn
        Caption = 'routing key'
        DataBinding.FieldName = 'routing_key'
        DataBinding.IsNullValueType = True
        Width = 87
      end
      object GridViewreadlimit: TcxGridDBColumn
        DataBinding.FieldName = 'readlimit'
        DataBinding.IsNullValueType = True
        Width = 64
      end
      object GridViewrequeue: TcxGridDBColumn
        DataBinding.FieldName = 'requeue'
        DataBinding.IsNullValueType = True
        Width = 64
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from rabbitmq_admin where active = 1'
  end
end
