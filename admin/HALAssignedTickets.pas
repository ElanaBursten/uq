unit HALAssignedTickets;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Samples.Spin, AdminDmu, OddbUtils, cxTextEdit, MainFU;

type
  TAssignedTicketsForm = class(TBaseCxListForm)
    chkboxAssignedTicketsSearch: TCheckBox;
    Label1: TLabel;
    SpinEditDateAdd: TSpinEdit;
    ColShortName: TcxGridDBColumn;
    ColTicketNumber: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColDueDate: TcxGridDBColumn;
    ColTicketFormat: TcxGridDBColumn;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    Splitter1: TSplitter;
    btnDateAdd: TButton;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColDaysback: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    procedure chkboxAssignedTicketsSearchClick(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure ColDaysbackPropertiesEditValueChanged(Sender: TObject);
    procedure btnDateAddClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  AssignedTicketsForm: TAssignedTicketsForm;

implementation

{$R *.dfm}

procedure TAssignedTicketsForm.btnDateAddClick(Sender: TObject);
begin
  inherited;
  CloseDataSets;
  OpenDataSets;
end;

procedure TAssignedTicketsForm.chkboxAssignedTicketsSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxAssignedTicketsSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TAssignedTicketsForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TAssignedTicketsForm.ColDaysbackPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  If StrToInt(TcxCustomEdit(Sender).EditValue) < 1 then
  begin
    showmessage('Minimum daysback value is 1');
     HalConfig.Cancel;
  end;
end;

procedure TAssignedTicketsForm.ColEmailsPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
     HalConfig.Cancel;
  end;
end;

procedure TAssignedTicketsForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TAssignedTicketsForm.FormCreate(Sender: TObject);
begin
  inherited;
  HalConfig.Open;
  if not HalConfig.EOF then
   spineditdateadd.Value := HalConfig.FieldByName('daysback').AsInteger;
end;

procedure TAssignedTicketsForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function TAssignedTicketsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TAssignedTicketsForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  if not HalConfig.EOF then
    spineditdateadd.Value := HalConfig.FieldByName('daysback').AsInteger;
  SetReadOnly(True);
end;

procedure TAssignedTicketsForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TAssignedTicketsForm.OpenDataSets;
begin
  HalConfig.Open;
  Data.Parameters.ParamByName('dateadd').Value := SpinEditDateAdd.Value;
  inherited;
end;

procedure TAssignedTicketsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  chkboxAssignedTicketsSearch.Enabled := True;
  btnDateAdd.Enabled := True;
end;


end.
