inherited TKAttachCVForm: TTKAttachCVForm
  Caption = 'TKAttachCV'
  ClientWidth = 876
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 450
      Top = 15
      Width = 225
      Height = 13
      Caption = 'green highlighted rows are older than 4 houors'
      Color = clGrayText
      ParentColor = False
    end
    object btnOpen: TButton
      Left = 39
      Top = 8
      Width = 159
      Height = 25
      Caption = 'Open tkAttachCV Directory '
      TabOrder = 0
      OnClick = btnOpenClick
    end
    object btrnClear: TButton
      Left = 763
      Top = 8
      Width = 77
      Height = 25
      Caption = 'Clear File List'
      TabOrder = 1
      OnClick = btrnClearClick
    end
  end
  object lvtkAttachCV: TListView
    Left = 0
    Top = 41
    Width = 876
    Height = 359
    Align = alClient
    Columns = <
      item
        Caption = 'Files'
        Width = 200
      end
      item
        Caption = 'Last Write Time'
        Width = 150
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    RowSelect = True
    ParentFont = False
    SortType = stText
    TabOrder = 1
    ViewStyle = vsReport
    OnColumnClick = lvtkAttachCVColumnClick
    OnCompare = lvtkAttachCVCompare
    OnCustomDrawItem = lvtkAttachCVCustomDrawItem
  end
  object FileOpenDialog1: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = [fdoPickFolders, fdoPathMustExist, fdoHidePinnedPlaces]
    Left = 648
    Top = 96
  end
end
