unit HALtkAttachCV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, OdEmbeddable, Vcl.ComCtrls, System.TimeSpan, System.IOUtils,
  DateUtils, MainFU, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TTKAttachCVForm = class(TEmbeddableForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnOpen: TButton;
    btrnClear: TButton;
    lvtkAttachCV: TListView;
    FileOpenDialog1: TFileOpenDialog;
    procedure btnOpenClick(Sender: TObject);
    procedure lvtkAttachCVColumnClick(Sender: TObject; Column: TListColumn);
    procedure lvtkAttachCVCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure lvtkAttachCVCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure btrnClearClick(Sender: TObject);
  private
    { Private declarations }
    Descending: Boolean;
    ColumnToSort: Integer;
    function CompareTextAsDateTime(const s1, s2: string): Integer;
  public
    { Public declarations }
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    function IsReadOnly: Boolean; override;
  end;

var
  TKAttachCVForm: TTKAttachCVForm;

implementation

{$R *.dfm}

procedure TTKAttachCVForm.btnOpenClick(Sender: TObject);
var
  SR : TSearchRec;
  Itm: TListItem;
  SearchResult: TSearchRec;
  s: Integer;
  FileDate:String;
  FilePath: String;
begin
  inherited;
  lvtkAttachCV.Clear;
  if FileOpenDialog1.Execute then
  begin
    FilePath := IncludeTrailingBackslash(FileOpenDialog1.FileName);
    try
      if FindFirst(FilePath + '*', faAnyFile, SR) = 0 then
      begin
        lvtkAttachCV.Items.BeginUpdate;
        repeat
          if (SR.attr and faDirectory) <> faDirectory then
          begin
            With lvtkAttachCV do
            begin
              Itm := Items.Add;
              canvas.Font.Assign(font);
              Itm.Caption := SR.Name;
              FileDate := DateTimetoStr(TFile.GetLastWriteTime(IncludeTrailingBackslash(FilePath)+SR.Name));
              Itm.SubItems.Add(FileDate);
              s := canvas.TextWidth(Items[Itm.Index].Caption) + 10;
              if s > Columns[0].Width then
              Columns[0].Width := s;
            end;
          end;
        until FindNext(SR) <> 0;
        lvtkAttachCV.Items.EndUpdate;
      end;
    finally
      FindClose(SR);
    end;
  end;
end;

procedure TTKAttachCVForm.btrnClearClick(Sender: TObject);
begin
  inherited;
  lvtkAttachCV.Clear;
end;

function TTKAttachCVForm.CompareTextAsDateTime(const s1, s2: string): Integer;
begin
  Result := CompareDateTime(StrToDateTime(s1), StrToDateTime(s2));
end;

procedure TTKAttachCVForm.lvtkAttachCVColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  inherited;
   TListView(Sender).SortType := stNone;
  if Column.Index<>ColumnToSort then
  begin
    ColumnToSort := Column.Index;
    Descending := False;
  end
  else
    Descending := not Descending;
  TListView(Sender).SortType := stText;
end;

procedure TTKAttachCVForm.lvtkAttachCVCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  inherited;
  Case ColumnToSort of
    0:
     Compare := CompareText(Item1.Caption, Item2.Caption);
    1:
     Compare := CompareTextAsDateTime(Item1.subitems[0],Item2.subitems[0]);
   end;
   if Descending then Compare := -Compare;
end;

procedure TTKAttachCVForm.lvtkAttachCVCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
const
  cStripe = $CCFFCC;  //color files older than 4 hours
var
  TS: TTimeSpan;
  StartDate, EndDate: TDateTime;
  Temp: Double;
begin
  EndDate := Now;
  StartDate := StrtoDateTime(Item.SubItems[0]);
  TS := TTimeSpan.Subtract(EndDate, StartDate);
  Temp := TS.TotalHours;

  if Temp > 4.0 then
    // Items older than 4 hours have a green background
   lvtkAttachCV.Canvas.Brush.Color := cStripe
 else
   lvtkAttachCV.Canvas.Brush.Color := clWindow;
end;

procedure TTKAttachCVForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  lvtkAttachCV.Enabled := True;
end;

function TTKAttachCVForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False
end;

end.
