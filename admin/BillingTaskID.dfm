inherited BillingTaskIDForm: TBillingTaskIDForm
  Caption = 'Billing Task ID'
  ClientWidth = 1212
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 1212
    Height = 32
    object cbTaskidSearch: TCheckBox
      Left = 89
      Top = 7
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbTaskidSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 32
    Width = 1212
    Height = 368
    inherited GridView: TcxGridDBTableView
      FindPanel.DisplayMode = fpdmManual
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = True
      object ColCustNumber: TcxGridDBColumn
        Caption = 'Customer Number'
        DataBinding.FieldName = 'customer_number'
        Width = 106
      end
      object ColCustName: TcxGridDBColumn
        Caption = 'Customer Name'
        DataBinding.FieldName = 'customer_name'
        Width = 128
      end
      object ColTaskID: TcxGridDBColumn
        Caption = 'Task ID'
        DataBinding.FieldName = 'task_id'
        Width = 112
      end
      object ColSolomonpc: TcxGridDBColumn
        Caption = 'Solomon PC'
        DataBinding.FieldName = 'solomon_pc'
        Width = 73
      end
      object ColpcCode: TcxGridDBColumn
        Caption = 'PC Code'
        DataBinding.FieldName = 'pc_code'
        Width = 55
      end
      object ColpcName: TcxGridDBColumn
        Caption = 'PC Name'
        DataBinding.FieldName = 'pc_name'
        Width = 97
      end
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        Width = 59
      end
      object ColEmpNumber: TcxGridDBColumn
        Caption = 'Emp Number'
        DataBinding.FieldName = 'emp_number'
        Width = 87
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Emp Name'
        DataBinding.FieldName = 'emp_name'
        Width = 105
      end
      object ColShortName: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        Width = 113
      end
      object ColEmpManID: TcxGridDBColumn
        Caption = 'EmpMan ID'
        DataBinding.FieldName = 'empman_id'
      end
      object ColEmpManNumber: TcxGridDBColumn
        Caption = 'EmpMan Number'
        DataBinding.FieldName = 'empman_number'
        Width = 104
      end
      object ColEmpManName: TcxGridDBColumn
        Caption = 'EmpMan Name'
        DataBinding.FieldName = 'empman_name'
        Width = 100
      end
      object ColEmpManShortName: TcxGridDBColumn
        Caption = 'EmpMan Short Name'
        DataBinding.FieldName = 'empman_short_name'
        Width = 109
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'create table #Temp'#13#10'(   '#13#10'    emp_id int,'#13#10'   customer_number va' +
      'rchar(20),'#13#10'   customer_name varchar(40),'#13#10'   task_id varchar(16' +
      '),'#13#10'   solomon_pc varchar(4),'#13#10'   pc_code varchar(15),'#13#10'   pc_na' +
      'me varchar(40),'#13#10'  emp_number varchar(15),'#13#10'  emp_name varchar(5' +
      '0),'#13#10'  short_name varchar(30),'#13#10'  empman_id int,'#13#10'  empman_numbe' +
      'r varchar(15),'#13#10'  empman_name varchar(50),'#13#10'  empman_short_name ' +
      'varchar(30)'#13#10')'#13#10#13#10'insert into #Temp (customer_number, task_id, s' +
      'olomon_pc, emp_number, empman_number)'#13#10'select customer_number, t' +
      'ask_id, cast(substring(task_id, 1, 4) as int),'#13#10'substring(task_i' +
      'd,5, 6), substring(task_id, 11, 6) from billing_task_id'#13#10#13#10'UPDAT' +
      'E tmp'#13#10'SET  tmp.customer_name = cust.customer_name'#13#10'FROM  custom' +
      'er cust'#13#10'  INNER JOIN #Temp tmp'#13#10'  ON cust.customer_number = tmp' +
      '.customer_number'#13#10#13#10'UPDATE tmp'#13#10'SET tmp.emp_id = emp.emp_id, tmp' +
      '.short_name = emp.short_name, tmp.emp_name = emp.first_name + '#39' ' +
      #39' + emp. last_name'#13#10'FROM employee emp'#13#10'  INNER JOIN #TEMP tmp'#13#10' ' +
      'ON dbo.udfLeftSQLPadding(emp.emp_number,6,'#39'0'#39') = tmp.emp_number'#13 +
      #10#13#10'UPDATE tmp'#13#10'SET tmp.empman_id = emp.emp_id, tmp.empman_short_' +
      'name = emp.short_name, tmp.empman_name = emp.first_name + '#39' '#39' + ' +
      'emp. last_name'#13#10'FROM employee emp'#13#10'  INNER JOIN #TEMP tmp'#13#10'  ON ' +
      'dbo.udfLeftSQLPadding(emp.emp_number,6,'#39'0'#39') = tmp.empman_number'#13 +
      #10#13#10'UPDATE tmp'#13#10'SET  tmp.pc_code = pc.pc_code, tmp.pc_name = pc.p' +
      'c_name'#13#10'FROM  profit_center pc'#13#10'  INNER JOIN #Temp tmp'#13#10'  ON (db' +
      'o.get_employee_pc(tmp.emp_id,2) = pc.[pc_code] )'#13#10#13#10'select * fro' +
      'm #Temp '#13#10
  end
  object droptemp: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb.dbo.#Temp'#39', '#39'U'#39') IS NOT NULL'
      '  DROP TABLE #Temp'
      '')
    Left = 168
    Top = 112
  end
end
