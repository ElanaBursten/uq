inherited DamagePCRulesForm: TDamagePCRulesForm
  Left = 441
  Top = 208
  Caption = 'Damage Profit Center Rules'
  ClientHeight = 416
  ClientWidth = 727
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 727
    Height = 28
    object chkboxPCRulesSearch: TCheckBox
      Left = 26
      Top = 6
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxPCRulesSearchClick
    end
    object ActivePCRulesFilter: TCheckBox
      Left = 149
      Top = 6
      Width = 188
      Height = 17
      Caption = 'Active Profit Center Rules Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActivePCRulesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 727
    Height = 309
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.KeyFieldNames = 'rule_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.HeaderHeight = 32
      object ColRuleId: TcxGridDBColumn
        DataBinding.FieldName = 'rule_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 87
      end
      object ColPCCode: TcxGridDBColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'pc_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        SortIndex = 0
        SortOrder = soAscending
        Width = 107
      end
      object LookupColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'DamagePCState'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.ListColumns = <>
        Properties.ListOptions.ShowHeader = False
        Width = 81
      end
      object ColCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 164
      end
      object ColCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 217
      end
      object ColModifiedDate: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 149
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 45
      end
    end
  end
  object BottomPanel: TPanel [2]
    Left = 0
    Top = 337
    Width = 727
    Height = 79
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 2
    object InstructionLabel: TLabel
      Left = 0
      Top = 0
      Width = 727
      Height = 79
      Align = alClient
      AutoSize = False
      Caption = 
        'Damage Profit Center Assignment Rules are matched in this order:' +
        #13'- Exact matched State, County, and City.'#13'- Exact matched State ' +
        'and City and * in County.'#13'- Exact matched State and County and *' +
        ' in City.'#13'- Exact matched State and * in County and * in City'#13'- ' +
        'If none of the above rules are matched, no profit center can be ' +
        'determined. '
      WordWrap = True
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from damage_profit_center_rule where active = 1'
  end
  object StatesRef: TADODataSet
    CacheSize = 3
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, code, description from reference where type='#39'stat' +
      'e'#39' and active_ind=1 order by description'
    Parameters = <>
    Left = 120
    Top = 185
  end
  object StatesDS: TDataSource
    DataSet = StatesRef
    Left = 168
    Top = 192
  end
end
