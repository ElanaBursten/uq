unit HALBlankLocators;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Samples.Spin,
  cxTextEdit,oddbUtils, MainFu;

type
  TBlankLocatorsForm = class(TBaseCxListForm)
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    chkboxBlankLocatorSearch: TCheckBox;
    Label1: TLabel;
    SpinEditDateAdd: TSpinEdit;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColDaysback: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Datalocate_id: TAutoIncField;
    Dataticket_id: TIntegerField;
    Dataclient_code: TStringField;
    Datastatus: TStringField;
    Dataassigned_to: TIntegerField;
    Datamodified_date: TDateTimeField;
    GridViewlocate_id: TcxGridDBColumn;
    GridViewticket_id: TcxGridDBColumn;
    GridViewclient_code: TcxGridDBColumn;
    GridViewstatus: TcxGridDBColumn;
    GridViewassigned_to: TcxGridDBColumn;
    GridViewmodified_date: TcxGridDBColumn;
    Splitter1: TSplitter;
    btnDateAdd: TButton;
    procedure chkboxBlankLocatorSearchClick(Sender: TObject);
    procedure ColDaysbackPropertiesEditValueChanged(Sender: TObject);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure btnDateAddClick(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  BlankLocatorsForm: TBlankLocatorsForm;

implementation

{$R *.dfm}

{ TBaseCxListForm1 }

procedure TBlankLocatorsForm.btnDateAddClick(Sender: TObject);
begin
  inherited;
  CloseDataSets;
  OpenDataSets;
end;

procedure TBlankLocatorsForm.chkboxBlankLocatorSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxBlankLocatorSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TBlankLocatorsForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  if not HALConfig.EOF then
    spineditdateadd.Value := HalConfig.FieldByName('daysback').AsInteger;
  SetReadOnly(True);
end;

procedure TBlankLocatorsForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TBlankLocatorsForm.ColDaysbackPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  If StrToInt(TcxCustomEdit(Sender).EditValue) < 1 then
  begin
    showmessage('Minimum daysback value is 1');
    HalConfig.Cancel;
  end;
end;

procedure TBlankLocatorsForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TBlankLocatorsForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TBlankLocatorsForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
    If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function TBlankLocatorsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TBlankLocatorsForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TBlankLocatorsForm.OpenDataSets;
begin
  HalConfig.Open;
  Data.Parameters.ParamByName('daysback').Value := SpinEditDateAdd.Value;
  inherited;
end;

procedure TBlankLocatorsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  chkboxBlankLocatorSearch.Enabled := True;
end;

end.
