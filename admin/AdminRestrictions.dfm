inherited AdminRestrictionForm: TAdminRestrictionForm
  Caption = 'Admin Restrictions'
  ClientHeight = 478
  ClientWidth = 836
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 836
    Height = 35
    object ActiveRestrictionsFilter: TCheckBox
      Left = 173
      Top = 10
      Width = 156
      Height = 17
      Caption = 'Active Restrictions Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveRestrictionsFilterClick
    end
    object cbActiveRestrictionsSearch: TCheckBox
      Left = 34
      Top = 10
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbActiveRestrictionsSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 35
    Width = 836
    Height = 443
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      object ColAllowedForms: TcxGridDBColumn
        Caption = 'Allowed Forms'
        DataBinding.FieldName = 'allowed_forms'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxPopupEditProperties'
        Properties.PopupControl = pnlAdiminRestriction
        Properties.ReadOnly = True
        Properties.OnCloseUp = ColAllowedFormsPropertiesCloseUp
        Properties.OnInitPopup = ColAllowedFormsPropertiesInitPopup
        Width = 582
      end
      object ColLoginID: TcxGridDBColumn
        Caption = 'Login ID'
        DataBinding.FieldName = 'login_id'
        DataBinding.IsNullValueType = True
        Width = 196
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        Width = 53
      end
    end
  end
  object pnlAdiminRestriction: TPanel [2]
    Left = 34
    Top = 168
    Width = 351
    Height = 233
    TabOrder = 2
    Visible = False
    object lblAdd: TLabel
      Left = 40
      Top = 14
      Width = 19
      Height = 13
      Caption = 'Add'
    end
    object lblDelete: TLabel
      Left = 190
      Top = 14
      Width = 31
      Height = 13
      Caption = 'Delete'
    end
    object cboAddForms: TcxComboBox
      Left = 32
      Top = 32
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ReadOnly = False
      Properties.OnCloseUp = cboAddFormsPropertiesCloseUp
      TabOrder = 0
      Width = 137
    end
    object memoAllowedForms: TMemo
      Left = 32
      Top = 72
      Width = 289
      Height = 113
      ScrollBars = ssVertical
      TabOrder = 1
    end
    object cboDeleteForms: TcxComboBox
      Left = 190
      Top = 33
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ReadOnly = False
      Properties.OnCloseUp = cboDeleteFormsPropertiesCloseUp
      TabOrder = 2
      Width = 137
    end
    object btnClose: TBitBtn
      Left = 247
      Top = 199
      Width = 74
      Height = 24
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 3
      OnClick = btnCloseClick
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 
      'select * from admin_restriction where active = 1 order by login_' +
      'id'
  end
  inherited dxSkinController2: TdxSkinController
    Left = 560
    Top = 192
  end
end
