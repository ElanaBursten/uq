inherited UploadFileTypesForm: TUploadFileTypesForm
  Caption = 'Upload File Types'
  ClientWidth = 616
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 616
    Height = 26
    object cbFileTypesSearch: TCheckBox
      Left = 26
      Top = 6
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbFileTypesSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 616
    Height = 374
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.Filter.IgnoreOrigin = False
      DataController.KeyFieldNames = 'upload_file_type_id'
      OptionsCustomize.ColumnFiltering = True
      object ColUploadFileTypeID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'upload_file_type_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 111
      end
      object ColExtension: TcxGridDBColumn
        Caption = 'File Extension'
        DataBinding.FieldName = 'extension'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 86
      end
      object ColMaxKilobytes: TcxGridDBColumn
        Caption = 'Max Size KB'
        DataBinding.FieldName = 'max_kilobytes'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Width = 77
      end
      object ColSizeErrorMessage: TcxGridDBColumn
        Caption = 'Oversize Error Message'
        DataBinding.FieldName = 'size_error_message'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 395
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 54
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 126
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterOpen = DataAfterOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select *'#13#10'from upload_file_type'#13#10'order by extension'
  end
end
