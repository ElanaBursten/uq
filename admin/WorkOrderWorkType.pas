unit WorkOrderWorkType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit, cxMaskEdit,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxStyles,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, cxTextEdit, cxCalendar,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, AdminDmu, dxScrollbarAnnotations;

type
  TWorkOrderWorkTypeForm = class(TBaseCxListForm)
    GridViewwork_type_id: TcxGridDBColumn;
    GridViewwork_type: TcxGridDBColumn;
    GridViewwork_description: TcxGridDBColumn;
    GridViewflag_color: TcxGridDBColumn;
    GridViewalert: TcxGridDBColumn;
    GridViewactive: TcxGridDBColumn;
    GridViewmodified_date: TcxGridDBColumn;
    GridViewWO_source: TcxGridDBColumn;
    chkboxWorkTypesSearch: TCheckBox;
    ActiveWorkTypesFilter: TCheckBox;
    procedure chkboxWorkTypesSearchClick(Sender: TObject);
    procedure ActiveWorkTypesFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    WorkTypesInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses QMConst, OdMiscUtils, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TWorkOrderWorkTypeForm }

procedure TWorkOrderWorkTypeForm.ActiveWorkTypesFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('work_type_id').AsInteger;
  Data.Close;
  if ActiveWorkTypesFilter.Checked then
    Data.CommandText := 'select * from work_order_work_type where active = 1' else
      Data.CommandText := 'select * from work_order_work_type';
  Data.Open;
  Data.Locate('work_type_id', FSelectedID, []);
end;

procedure TWorkOrderWorkTypeForm.chkboxWorkTypesSearchClick(Sender: TObject);
begin
 inherited;
 if chkboxWorkTypesSearch.Checked then
   GridView.Controller.ShowFindPanel
 else
   GridView.Controller.HideFindPanel;
end;

procedure TWorkOrderWorkTypeForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  WorkTypesInserted := False;
end;

procedure TWorkOrderWorkTypeForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'work_order_work_type', 'work_type_id', WorkTypesInserted);
end;

procedure TWorkOrderWorkTypeForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
     //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TWorkOrderWorkTypeForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  WorkTypesInserted := True;
end;

procedure TWorkOrderWorkTypeForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TWorkOrderWorkTypeForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TWorkOrderWorkTypeForm.ActivatingNow;
begin
  inherited;
  WorkTypesInserted := False;
end;

procedure TWorkOrderWorkTypeForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[GridViewActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TWorkOrderWorkTypeForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TWorkOrderWorkTypeForm.PopulateDropdowns;
begin
  inherited;
  CxComboBoxItems(GridViewflag_color).Clear;
  CxComboBoxItems(GridViewflag_color).Add(FlagColorNone);
  CxComboBoxItems(GridViewflag_color).Add(FlagColorOrange);
  CxComboBoxItems(GridViewflag_color).Add(FlagColorRed);
end;

procedure TWorkOrderWorkTypeForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxWorkTypesSearch.Enabled := True;
  ActiveWorkTypesFilter.Enabled := True;
end;

end.
