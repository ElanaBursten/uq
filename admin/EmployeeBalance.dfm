inherited EmployeeBalanceForm: TEmployeeBalanceForm
  Caption = 'Employee Balance'
  ClientWidth = 980
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 980
    object btnOpen: TButton
      Left = 366
      Top = 10
      Width = 112
      Height = 25
      Caption = 'Open File'
      TabOrder = 0
      OnClick = btnOpenClick
    end
  end
  inherited Grid: TcxGrid
    Width = 980
    inherited GridView: TcxGridDBTableView
      object GridViewCompany_Code: TcxGridDBColumn
        DataBinding.FieldName = 'Company_Code'
        Width = 128
      end
      object GridViewEmployee_Number: TcxGridDBColumn
        DataBinding.FieldName = 'Employee_Number'
      end
      object GridViewemp_id: TcxGridDBColumn
        DataBinding.FieldName = 'emp_id'
      end
      object GridViewFirst_Name: TcxGridDBColumn
        DataBinding.FieldName = 'First_Name'
      end
      object GridViewLast_Name: TcxGridDBColumn
        DataBinding.FieldName = 'Last_Name'
      end
      object GridViewEmployment_Status: TcxGridDBColumn
        DataBinding.FieldName = 'Employment_Status'
      end
      object GridViewSICK_Option: TcxGridDBColumn
        DataBinding.FieldName = 'SICK_Option'
      end
      object GridViewSICK_Balance: TcxGridDBColumn
        DataBinding.FieldName = 'SICK_Balance'
      end
      object GridViewPTO_Option: TcxGridDBColumn
        DataBinding.FieldName = 'PTO_Option'
      end
      object GridViewPTO_Balance: TcxGridDBColumn
        DataBinding.FieldName = 'PTO_Balance'
      end
      object GridViewPTO_Accrual: TcxGridDBColumn
        DataBinding.FieldName = 'PTO_Accrual'
      end
      object GridViewWeeksRemaining: TcxGridDBColumn
        DataBinding.FieldName = 'WeeksRemaining'
      end
      object GridViewAccrual_to_EOY: TcxGridDBColumn
        DataBinding.FieldName = 'Accrual_to_EOY'
      end
      object GridViewEst_Bal_at_EOY: TcxGridDBColumn
        DataBinding.FieldName = 'Est_Bal_at_EOY'
      end
      object GridViewSupervisor_Name: TcxGridDBColumn
        DataBinding.FieldName = 'Supervisor_Name'
      end
      object GridViewEMail_Sup: TcxGridDBColumn
        DataBinding.FieldName = 'EMail_Sup'
      end
    end
  end
  inherited Data: TADODataSet
    Active = True
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from employee_balance'
  end
  object FileADOConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.ACE.OLEDB.12.0;User ID=Admin; Extended Proper' +
      'ties="Excel 12.0 Xml;HDR=YES";Data Source=C:\SickLeave\PTO and S' +
      'ick Balance w Annual Estimate for UTQ & LOC Week 18 Through 04 3' +
      '0 2022.xlsx'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.ACE.OLEDB.12.0'
    Left = 191
    Top = 72
  end
  object OpenDialog1: TOpenDialog
    Left = 312
    Top = 82
  end
  object qryEmployeeBalance: TADOQuery
    Connection = FileADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select * from ['#39'UTQ & LOC$'#39']')
    Left = 259
    Top = 145
    object qryEmployeeBalanceCompanyCode: TWideStringField
      FieldName = 'Company Code'
      Size = 255
    end
    object qryEmployeeBalanceEmployeeNumber: TWideStringField
      FieldName = 'Employee Number'
      Size = 255
    end
    object qryEmployeeBalanceFirstName: TWideStringField
      FieldName = 'First Name'
      Size = 255
    end
    object qryEmployeeBalanceLastName: TWideStringField
      FieldName = 'Last Name'
      Size = 255
    end
    object qryEmployeeBalanceEmploymentStatus: TWideStringField
      FieldName = 'Employment Status'
      Size = 255
    end
    object qryEmployeeBalanceSICKOption: TWideStringField
      FieldName = 'SICK Option'
      Size = 255
    end
    object qryEmployeeBalanceSICKBalance: TFloatField
      FieldName = 'SICK Balance'
    end
    object qryEmployeeBalancePTOOption: TWideStringField
      FieldName = 'PTO Option'
      Size = 255
    end
    object qryEmployeeBalancePTOBalance: TFloatField
      FieldName = 'PTO Balance'
    end
    object qryEmployeeBalancePTOAccrual: TFloatField
      FieldName = 'PTO Accrual'
    end
    object qryEmployeeBalanceWeeksRemaining: TFloatField
      FieldName = 'WeeksRemaining'
    end
    object qryEmployeeBalanceAccrualtoEOY: TFloatField
      FieldName = 'Accrual to EOY'
    end
    object qryEmployeeBalanceEstBalatEOY: TFloatField
      FieldName = 'Est Bal at EOY'
    end
    object qryEmployeeBalanceSupervisorNameFirstMILastSuffix: TWideStringField
      FieldName = 'Supervisor Name (First MI Last Suffix)'
      Size = 255
    end
    object qryEmployeeBalanceEmailSupervisor: TWideStringField
      FieldName = 'Email (Supervisor)'
      Size = 255
    end
  end
  object dsAEmployeeBalance: TDataSource
    AutoEdit = False
    DataSet = qryEmployeeBalance
    Left = 400
    Top = 136
  end
  object qryEmpid: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'empnumber'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
      end>
    SQL.Strings = (
      'select emp_id from employee where '
      'emp_number = :empnumber '
      'and ad_username is not null')
    Left = 208
    Top = 232
  end
end
