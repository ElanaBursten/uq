object ChangePasswordForm: TChangePasswordForm
  Left = 478
  Top = 663
  BorderStyle = bsDialog
  Caption = 'Change User Password'
  ClientHeight = 239
  ClientWidth = 229
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 13
  object UserLoginLabel: TLabel
    Left = 14
    Top = 16
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'User Login:'
  end
  object UserLogin: TLabel
    Left = 84
    Top = 16
    Width = 47
    Height = 13
    Caption = 'UserLogin'
  end
  object Password1Label: TLabel
    Left = 18
    Top = 44
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Password:'
  end
  object Password2Label: TLabel
    Left = 18
    Top = 72
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Password:'
  end
  object ProblemLabel: TLabel
    Left = 6
    Top = 102
    Width = 218
    Height = 58
    AutoSize = False
    WordWrap = True
    OnMouseDown = FormMouseDown
  end
  object lblExtra: TLabel
    Left = 24
    Top = 211
    Width = 167
    Height = 13
    AutoSize = False
  end
  object Password1: TEdit
    Left = 84
    Top = 40
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object Password2: TEdit
    Left = 84
    Top = 68
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnExit = Password1Change
  end
  object OkBtn: TButton
    Left = 24
    Top = 172
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 116
    Top = 172
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
end
