unit HALEPRs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxMaskEdit, Mainfu, odDbUtils;

type
  THALEPRsForm = class(TBaseCxListForm)
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ColModule: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    ColCallCenter: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColOldest: TcxGridDBColumn;
    ColNewest: TcxGridDBColumn;
    ColCount: TcxGridDBColumn;
    ColAgeInDays: TcxGridDBColumn;
    ColAgeInHours: TcxGridDBColumn;
    Splitter1: TSplitter;
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  HALEPRsForm: THALEPRsForm;

implementation

{$R *.dfm}

{ TEPRsForm }

procedure THALEPRsForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure THALEPRsForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure THALEPRsForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
     HalConfig.Cancel;
  end;
end;

procedure THALEPRsForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure THALEPRsForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function THALEPRsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure THALEPRsForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure THALEPRsForm.OpenDataSets;
begin
  inherited;
  HalConfig.Open;
end;

procedure THALEPRsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
end;

end.
