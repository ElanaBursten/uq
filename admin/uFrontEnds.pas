unit uFrontEnds;
//bp QM-
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.OleCtrls, SHDocVw, Vcl.StdCtrls, odEmbeddable;

type
  TMonitorForm = class(TEmbeddableForm)
    WebBrowser: TWebBrowser;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function IsReadOnly: Boolean; override;
  end;

var
  MonitorForm: TMonitorForm;

implementation

{$R *.dfm}

uses MainFu, AdminDMu;

procedure TMonitorForm.FormShow(Sender: TObject);
begin
  if AdminDM.DB = 'QM' then
  begin
    WebBrowser.Navigate('file://' +
    ExtractFilePath(Application.ExeName) + 'FrontendsPROD.html');
    Caption := 'Monitor Prod Front Ends';
  end else
  begin
    WebBrowser.Navigate('file://' +
    ExtractFilePath(Application.ExeName) + 'FrontendsDEV.html');
    Caption := 'Monitor Dev Front Ends';
  end;
end;

function TMonitorForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;


end.
