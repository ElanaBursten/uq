inherited ParserLogErrorsForm: TParserLogErrorsForm
  Caption = 'Parser Log Errors'
  ClientHeight = 450
  ClientWidth = 885
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 885
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 504
      Top = 15
      Width = 74
      Height = 13
      Caption = 'Look Back Days'
    end
    object btnOpen: TButton
      Left = 39
      Top = 8
      Width = 159
      Height = 25
      Caption = 'Check Log File for Deadlocks'
      TabOrder = 0
      OnClick = btnOpenClick
    end
    object SpinEditDaysBack: TSpinEdit
      Left = 584
      Top = 12
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 9
      MinValue = 1
      TabOrder = 1
      Value = 3
    end
  end
  object lvParserLog: TListView
    Left = 0
    Top = 41
    Width = 885
    Height = 64
    Align = alTop
    Columns = <
      item
        Caption = 'Parser Log File'
        Width = 555
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    RowSelect = True
    ParentFont = False
    SortType = stText
    TabOrder = 1
    ViewStyle = vsReport
  end
  object Memo1: TMemo
    Left = 0
    Top = 105
    Width = 885
    Height = 345
    Align = alClient
    ParentShowHint = False
    ReadOnly = True
    ScrollBars = ssBoth
    ShowHint = False
    TabOrder = 2
  end
  object OpenDialog1: TOpenDialog
    Left = 688
    Top = 152
  end
end
