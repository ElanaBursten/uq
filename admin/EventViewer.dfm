inherited EventViewerForm: TEventViewerForm
  Caption = 'View Event Store'
  ClientHeight = 491
  ClientWidth = 774
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter [0]
    Left = 0
    Top = 300
    Width = 774
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 774
    Height = 60
    object StartingEventIdLabel: TLabel
      Left = 8
      Top = 11
      Width = 87
      Height = 13
      Caption = 'Starting Event ID:'
    end
    object EndingEventIDLabel: TLabel
      Left = 8
      Top = 37
      Width = 81
      Height = 13
      Caption = 'Ending Event ID:'
    end
    object MaxLabel: TLabel
      Left = 208
      Top = 34
      Width = 132
      Height = 13
      Caption = '(maximum of 5,000 events)'
    end
    object StartingEventID: TEdit
      Left = 104
      Top = 8
      Width = 84
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object EndingEventID: TEdit
      Left = 104
      Top = 31
      Width = 84
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object SearchButton: TButton
      Left = 208
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Search'
      Default = True
      TabOrder = 2
      OnClick = SearchButtonClick
    end
  end
  inherited Grid: TcxGrid
    Top = 60
    Width = 774
    Height = 212
    inherited GridView: TcxGridDBTableView
      OnFocusedRecordChanged = GridViewFocusedRecordChanged
      DataController.KeyFieldNames = 'event_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DateTimeHandling.Grouping = dtgRelativeToToday
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '<No events to display>'
      OptionsView.GroupByBox = True
      object ColEventID: TcxGridDBColumn
        Caption = 'Event ID'
        DataBinding.FieldName = 'event_id'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 73
      end
      object ColAggrType: TcxGridDBColumn
        Caption = 'Aggr Type'
        DataBinding.FieldName = 'aggregate_type'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
      end
      object ColAggrID: TcxGridDBColumn
        Caption = 'Aggr ID'
        DataBinding.FieldName = 'aggregate_id'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 79
      end
      object ColVerNum: TcxGridDBColumn
        Caption = 'Ver'
        DataBinding.FieldName = 'version_num'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 49
      end
      object ColEventAdded: TcxGridDBColumn
        Caption = 'Date Added'
        DataBinding.FieldName = 'event_added'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'mm/dd/yyyy HH:NN:SS'
        Properties.Kind = ckDateTime
        Options.Editing = False
        Width = 115
      end
      object ColEventData: TcxGridDBColumn
        Caption = 'Event Data'
        DataBinding.FieldName = 'event_data'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.MaxLength = 800
        BestFitMaxWidth = 1000
        Options.Editing = False
        Width = 368
      end
    end
  end
  object BottomPanel: TPanel [3]
    Left = 0
    Top = 303
    Width = 774
    Height = 188
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object EventDetailsMemo: TMemo
      Left = 0
      Top = 0
      Width = 774
      Height = 188
      Align = alClient
      Lines.Strings = (
        'Select an event in the list to see its details.')
      ParentColor = True
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
      WordWrap = False
    end
  end
  object GridSummPanel: TPanel [4]
    Left = 0
    Top = 272
    Width = 774
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object EventCntLabel: TLabel
      Left = 8
      Top = 8
      Width = 42
      Height = 13
      Caption = 'Count: 0'
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from event_log where 0=1'
  end
end
