unit LocatingCompany;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges;

type
  TLocatingCompanyForm = class(TBaseCxListForm)
    NoteLabel: TLabel;
    ColName: TcxGridDBColumn;
    ColAddressStreet: TcxGridDBColumn;
    ColAddressCity: TcxGridDBColumn;
    ColAddressState: TcxGridDBColumn;
    ColAddressZip: TcxGridDBColumn;
    ColPhone: TcxGridDBColumn;
    ColLogoFilename: TcxGridDBColumn;
    ColBillingFooter: TcxGridDBColumn;
    ColCompanyID: TcxGridDBColumn;
    ColFloatHolidays: TcxGridDBColumn;
    cbLocatingCompanySearch: TCheckBox;
    procedure cbLocatingCompanySearchClick(Sender: TObject);
  public
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
  end;

implementation

{$R *.dfm}

uses
  OdDBUtils;

procedure TLocatingCompanyForm.cbLocatingCompanySearchClick(Sender: TObject);
begin
  inherited;
  if cbLocatingCompanySearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TLocatingCompanyForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TLocatingCompanyForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbLocatingCompanySearch.Enabled := True;
end;

end.
