unit UploadLocations;

interface

uses
  Forms, BaseCxList, DB, ADODB, Classes, Controls, Dialogs, ExtCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.Graphics, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TUploadLocationsForm = class(TBaseCxListForm)
    InfoPanel: TPanel;
    ColLocationID: TcxGridDBColumn;
    ColDescription: TcxGridDBColumn;
    ColGroupName: TcxGridDBColumn;
    ColServer: TcxGridDBColumn;
    ColDirectory: TcxGridDBColumn;
    ColUsername: TcxGridDBColumn;
    ColPassword: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColPort: TcxGridDBColumn;
    cbUploadLocationSearch: TCheckBox;
    ActiveLocationsFilter: TCheckBox;
    procedure cbUploadLocationSearchClick(Sender: TObject);
    procedure ActiveLocationsFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    ULInserted: Boolean;
  public
   procedure LeavingNow; override;
   procedure SetReadOnly(const IsReadOnly: Boolean); override;
   procedure EditNow; override;
   procedure ActivatingNow; override;
  end;

implementation

uses OdMiscUtils, OdDbUtils, OdEmbeddable, AdminDmu;

{$R *.dfm}

procedure TUploadLocationsForm.ActiveLocationsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('location_id').AsInteger;
  Data.Close;
  if ActiveLocationsFilter.Checked then
    Data.CommandText := 'select * from upload_location where active = 1' else
      Data.CommandText := 'select * from upload_location';
  Data.Open;
  Data.Locate('location_id', FSelectedID, []);
end;

procedure TUploadLocationsForm.cbUploadLocationSearchClick(Sender: TObject);
begin
  inherited;
  if cbUploadLocationSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TUploadLocationsForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ULInserted := False;
end;

procedure TUploadLocationsForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'upload_location', 'location_id', ULInserted);
end;

procedure TUploadLocationsForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TUploadLocationsForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ULInserted := True;
end;

procedure TUploadLocationsForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TUploadLocationsForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TUploadLocationsForm.ActivatingNow;
begin
  inherited;
  ULInserted := False;
end;

procedure TUploadLocationsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TUploadLocationsForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TUploadLocationsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbUploadLocationSearch.Enabled := True;
  ActiveLocationsFilter.Enabled := True;
end;

end.
