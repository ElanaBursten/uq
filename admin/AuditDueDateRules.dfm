inherited AuditDueDateRulesForm: TAuditDueDateRulesForm
  Caption = 'Audit Due Date Rules'
  ClientWidth = 725
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 725
    Height = 26
    object ActiveAuditDueDateFilter: TCheckBox
      Left = 16
      Top = 5
      Width = 147
      Height = 16
      Caption = 'Active Due Date Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveAuditDueDateFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 725
    Height = 374
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object RuleIDCol: TcxGridDBColumn
        Caption = 'Rule ID'
        DataBinding.FieldName = 'rule_id'
        Options.Editing = False
        Options.Focusing = False
        Styles.Content = cxStyle1
        Width = 51
      end
      object AuditSourceCol: TcxGridDBColumn
        Caption = 'Audit Source'
        DataBinding.FieldName = 'audit_source'
        Width = 111
      end
      object TicketFormatCol: TcxGridDBColumn
        Caption = 'Ticket Format'
        DataBinding.FieldName = 'ticket_format'
        Width = 166
      end
      object HoursDueCol: TcxGridDBColumn
        Caption = 'Hours Due'
        DataBinding.FieldName = 'hours_due'
      end
      object ModifiedDateCol: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        Options.Editing = False
        Options.Focusing = False
        Styles.Content = cxStyle1
        Width = 132
      end
      object ActiveCol: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Options.Filtering = False
        Width = 57
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorLocation = clUseServer
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from audit_due_date_rule where active = 1'
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 648
    Top = 48
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGrayText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = cl3DDkShadow
    end
  end
end
