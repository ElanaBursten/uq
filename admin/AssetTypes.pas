unit AssetTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,  AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TAssetTypeForm = class(TBaseCxListForm)
    ColAssetCode: TcxGridDBColumn;
    ColDescription: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ActiveAssetTypeFilter: TCheckBox;
    procedure ActiveAssetTypeFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
   private
     AssetTypeInserted: Boolean;
   public
    procedure PopulateCallCenterList;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
end;

implementation

uses OdMiscUtils, OdDbUtils, OdEmbeddable;

{$R *.dfm}

procedure TAssetTypeForm.ActiveAssetTypeFilterClick(Sender: TObject);
var
  FSelectedID: String;
begin
  inherited;
  FSelectedID := Data.FieldByName('asset_code').AsString;
  Data.Close;
  if ActiveAssetTypeFilter.Checked then
    Data.CommandText := ' select * from asset_type where active = 1' else
      Data.CommandText := 'select * from asset_type';
  Data.Open;
  Data.Locate('asset_code', FSelectedID, []);
end;

procedure TAssetTypeForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'asset_type', 'asset_code', AssetTypeInserted);
end;


procedure TAssetTypeForm.ActivatingNow;
begin
  inherited;
  AssetTypeInserted := False;
end;

procedure TAssetTypeForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TAssetTypeForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TAssetTypeForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  AssetTypeInserted := False;
end;

procedure TAssetTypeForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TAssetTypeForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
   AssetTypeInserted := True;
end;

procedure TAssetTypeForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TAssetTypeForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TAssetTypeForm.PopulateCallCenterList;
begin

end;

procedure TAssetTypeForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveAssetTypeFilter.Enabled := True;
end;

end.
