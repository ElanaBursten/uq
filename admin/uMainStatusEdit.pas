unit uMainStatusEdit;
//QM-266 sr
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxCheckBox, AdminDmu, BaseCxList;

type
  TfrmMainStatusEdit = class(TBaseCxListForm)
    qryTktStatusExclusions: TADOQuery;
    dsTktStatusExclusions: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1call_center: TcxGridDBColumn;
    cxGrid1DBTableView1sg_name: TcxGridDBColumn;
    cxGrid1DBTableView1status: TcxGridDBColumn;
    cxGrid1DBTableView1ticketTypeExclusion: TcxGridDBColumn;
    cxGrid1DBTableView1sg_item_id: TcxGridDBColumn;
    cxGrid1DBTableView1NotesRequired: TcxGridDBColumn;
    cxGrid1DBTableView1PicturesRequired: TcxGridDBColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryTktStatusExclusionsAfterCancel(DataSet: TDataSet);
    procedure qryTktStatusExclusionsBeforeInsert(DataSet: TDataSet);
    procedure qryTktStatusExclusionsBeforeEdit(DataSet: TDataSet);
    procedure qryTktStatusExclusionsAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    seInserted: Boolean;
  public
    { Public declarations }
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  frmMainStatusEdit: TfrmMainStatusEdit;

implementation
uses StrUtils,System.IniFiles;
{$R *.dfm}

procedure TfrmMainStatusEdit.ActivatingNow;
begin
  inherited;
  seInserted := False;
end;

procedure TfrmMainStatusEdit.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TfrmMainStatusEdit.FormDestroy(Sender: TObject);
begin
  qryTktStatusExclusions.Close;
end;

procedure TfrmMainStatusEdit.FormShow(Sender: TObject);
begin
  qryTktStatusExclusions.open;
end;

procedure TfrmMainStatusEdit.qryTktStatusExclusionsAfterCancel(
  DataSet: TDataSet);
begin
  inherited;
  seInserted := False;
end;

procedure TfrmMainStatusEdit.qryTktStatusExclusionsAfterPost(DataSet: TDataSet);
begin
  inherited;
    AdminDM.TrackAdminChanges(Dataset, ATableArray, 'status_group', 'sg_item_id', seInserted);
end;

procedure TfrmMainStatusEdit.qryTktStatusExclusionsBeforeEdit(
  DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TfrmMainStatusEdit.qryTktStatusExclusionsBeforeInsert(
  DataSet: TDataSet);
begin
  inherited;
  seInserted := True;
end;

end.
