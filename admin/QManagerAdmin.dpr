program QManagerAdmin;

// Compile with the following packages for end-user distribution:
// rtl;vcl;vclx;dbrtl;vcldb;adortl;vclsmp;vclimg;dbisamr;rbCT1417;rbDAD1417;rbDB1417;rbIDE1417;rbRCL1417;rbUSER1417;rbRTL1417;DevExDXE3RT1224;
// Relative directories (plus JCL, DBISAM, etc.):
// ..\thirdparty\betterado





uses
  Windows,
  Forms,
  Dialogs,
  MapSetup in 'MapSetup.pas' {MapSetupForm},
  LoginFU in 'LoginFU.pas' {LoginForm},
  AdminDMu in 'AdminDMu.pas' {AdminDM: TDataModule},
  OdHourglass in '..\common\OdHourglass.pas',
  AreaLocator in 'AreaLocator.pas' {AreaLocatorFrame: TFrame},
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdEmbeddable in '..\common\OdEmbeddable.pas' {EmbeddableForm},
  MainFU in 'MainFU.pas' {MainForm},
  StatusList in 'StatusList.pas' {StatusListForm},
  BaseCxList in 'BaseCxList.pas' {BaseCxListForm},
  Offices in 'Offices.pas' {OfficesForm},
  Locator in 'Locator.pas' {LocatorForm},
  CallCenter in 'CallCenter.pas' {CallCenterForm},
  Reference in 'Reference.pas' {ReferenceForm},
  TimeFixer in 'TimeFixer.pas' {TimeFixerForm},
  OdIsoDates in '..\common\OdIsoDates.pas',
  User in 'User.pas' {UserForm},
  BaseList in 'BaseList.pas' {BaseListForm},
  OdDbUtils in '..\common\OdDbUtils.pas',
  CountyRouting in 'CountyRouting.pas' {CountyRoutingForm},
  GridAxis in 'GridAxis.pas',
  BaseMap in 'BaseMap.pas' {BaseMapForm},
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  ProfitCenter in 'ProfitCenter.pas' {ProfitCenterForm},
  OdAdoUtils in '..\common\OdAdoUtils.pas',
  TownshipRange in 'TownshipRange.pas' {TownshipRangeForm},
  UnRouting in 'UnRouting.pas' {UnRoutingForm},
  OdContainer in '..\common\OdContainer.pas',
  FollowupTickets in 'FollowupTickets.pas' {FollowupTicketsForm},
  AssetTypes in 'AssetTypes.pas' {AssetTypeForm},
  UploadLocations in 'UploadLocations.pas' {UploadLocationsForm},
  TicketAckMatch in 'TicketAckMatch.pas' {TicketAckMatchForm},
  Hierarchy in 'Hierarchy.pas' {HierarchyForm},
  DamageDefEst in 'DamageDefEst.pas' {DamageDefEstForm},
  Holiday in 'Holiday.pas' {HolidayForm},
  BaseMasterDetail in 'BaseMasterDetail.pas' {BaseMasterDetailForm},
  QMConst in '..\client\QMConst.pas',
  CustomerTerms in 'CustomerTerms.pas' {CustomerTermForm},
  RoutingList in 'RoutingList.pas' {RoutingListForm},
  Carrier in 'Carrier.pas' {CarrierForm},
  StatusGroups in 'StatusGroups.pas' {StatusGroupForm},
  LocatingCompany in 'LocatingCompany.pas' {LocatingCompanyForm},
  ConfigurationData in 'ConfigurationData.pas' {ConfigurationDataForm},
  ErrorDialog in 'ErrorDialog.pas' {ExceptionDialog},
  OdInternetUtil in '..\common\OdInternetUtil.pas',
  ConfigDMu in 'ConfigDMu.pas' {ConfigDM: TDataModule},
  FaxQueueRules in 'FaxQueueRules.pas' {FaxQueueRulesForm},
  ReportConfig in '..\reportengine\ReportConfig.pas',
  OdPerfLog in '..\common\OdPerfLog.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  Terminology in '..\common\Terminology.pas',
  ChangePassword in 'ChangePassword.pas' {ChangePasswordForm},
  PasswordRules in '..\common\PasswordRules.pas',
  HighProfileGrid in 'HighProfileGrid.pas' {HighProfileGridForm},
  BreakRules in 'BreakRules.pas' {BreakRulesForm},
  UploadFileTypes in 'UploadFileTypes.pas' {UploadFileTypesForm},
  RestrictUsage in 'RestrictUsage.pas' {RestrictUsageForm},
  EmployeeGroups in 'EmployeeGroups.pas' {EmployeeGroupsForm},
  OdCxUtils in '..\common\OdCxUtils.pas',
  HighProfileAddress in 'HighProfileAddress.pas' {HighProfileAddressForm},
  Computers in 'Computers.pas' {ComputersForm},
  EmailQueueRules in 'EmailQueueRules.pas' {EmailQueueRulesForm},
  TicketAlertKeyword in 'TicketAlertKeyword.pas' {TicketAlertKeywordForm},
  HighlightKeywordRules in 'HighlightKeywordRules.pas' {HighlightKeywordRulesForm},
  RequiredDocuments in 'RequiredDocuments.pas' {RequiredDocumentsForm},
  DamagePCRules in 'DamagePCRules.pas' {DamagePCRulesForm},
  DamageDueDateRules in 'DamageDueDateRules.pas' {DamageDueDateRulesForm},
  AdminFormDefs in 'AdminFormDefs.pas',
  OdSecureHash in '..\common\OdSecureHash.pas',
  DycomPeriod in 'DycomPeriod.pas' {DycomPeriodForm},
  Hashes in '..\thirdparty\Hashes.pas',
  EmployeeIncentivePay in 'EmployeeIncentivePay.pas' {EmployeeIncentivePayForm},
  WorkOrderWorkType in 'WorkOrderWorkType.pas' {WorkOrderWorkTypeForm},
  UQDbConfig in '..\common\UQDbConfig.pas',
  Rights in 'Rights.pas' {RightsFrame: TFrame},
  CodeLookupList in '..\common\CodeLookupList.pas',
  TicketInfo in 'TicketInfo.pas' {TicketInfoForm},
  EPRAdmin in 'EPRAdmin.pas' {EPRAdminForm},
  EPRNotificationConfig in 'EPRNotificationConfig.pas' {EPRNotificationConfigForm},
  TableViewer2 in 'TableViewer2.pas' {TableViewerForm2},
  SaveQueryDialog in 'SaveQueryDialog.pas' {SaveQueryDlg},
  EPRQueueRules in 'EPRQueueRules.pas' {EPRQueueRulesForm},
  DBMemoEditor in 'DBMemoEditor.pas' {memoEditorForm},
  EPRDisclaimer in 'EPRDisclaimer.pas' {EPRDisclaimerForm},
  AuditDueDateRules in 'AuditDueDateRules.pas' {AuditDueDateRulesForm},
  BillPeriod in '..\XE10Billing\BillPeriod.pas',
  TermEmp in 'TermEmp.pas' {TermEmployeeInfoForm},
  Esketch in 'Esketch.pas' {frmESketchCodes},
  ExtractEmpData in 'ExtractEmpData.pas' {ExtractEmpForm},
  PlatMain in 'PlatMain.pas' {PlatManagerForm},
  AdminRestrictions in 'AdminRestrictions.pas' {AdminRestrictionForm},
  uRoboMain in 'uRoboMain.pas' {RoboCopyForm},
  uOQData in 'uOQData.pas' {OQDataForm},
  uFrontEnds in 'uFrontEnds.pas' {MonitorForm},
  ClientRespondTo in 'ClientRespondTo.pas' {frmClientRespondTo},
  uMainStatusEdit in 'uMainStatusEdit.pas' {frmMainStatusEdit},
  RespondTo in 'RespondTo.pas' {RespondToForm},
  ComputerAssets in 'ComputerAssets.pas' {ComputerAssetsForm},
  uADSI in 'uADSI.pas',
  HALActiveUsers in 'HALActiveUsers.pas' {ActiveUsersForm},
  HALAssignedTickets in 'HALAssignedTickets.pas' {AssignedTicketsForm},
  HALAttachments in 'HALAttachments.pas' {AttachmentsForm},
  HALBlankLocators in 'HALBlankLocators.pas' {BlankLocatorsForm},
  HALCallCenters in 'HALCallCenters.pas' {HALCallCenterForm},
  HALData in 'HALData.pas' {DMHalSentinel: TDataModule},
  HALEPRs in 'HALEPRs.pas' {HALEPRsForm},
  HALErrorLogs in 'HALErrorLogs.pas' {ErrorLogsForm},
  HALEvalParser in 'HALEvalParser.pas' {EvalParserForm},
  HALFireCracker in 'HALFireCracker.pas' {FireCrackerForm},
  HALMissingAttachments in 'HALMissingAttachments.pas' {MissingAttachmentsForm},
  HALParserLogErrors in 'HALParserLogErrors.pas' {ParserLogErrorsForm},
  HalSentinelForm in 'HalSentinelForm.pas' {HalForm},
  HALTicketVersion in 'HALTicketVersion.pas' {TicketVersionForm},
  HALtkAttachCV in 'HALtkAttachCV.pas' {TKAttachCVForm},
  BillingRulesVirtualGroup in 'BillingRulesVirtualGroup.pas',
  BillingRulesCustomer in 'BillingRulesCustomer.pas',
  BillingCCMap in 'BillingCCMap.pas' {BillingCCMapForm},
  TSEApprovalQueue in 'TSEApprovalQueue.pas' {ApprovalQueueForm},
  TSEApprovalQueueHist in 'TSEApprovalQueueHist.pas' {ApprovalQueueHistForm},
  Rabbitmq in 'Rabbitmq.pas' {RabbitmqForm};

//added back QM-987 sr

//qm-868

//QM-243  sr

{$R '..\QMIcon.res'}
{$R 'QMVersion.res' '..\QMVersion.rc'}
{$R 'QManagerAdminResources.res' 'QManagerAdminResources.rc'}

begin
  Application.Initialize;
  Application.Title := 'Q Manager Admin';
  Application.CreateForm(TAdminDM, AdminDM);
  if LogIn then
    Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.



