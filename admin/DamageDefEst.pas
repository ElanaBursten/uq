unit DamageDefEst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxDBLookupComboBox, cxCurrencyEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  Vcl.StdCtrls, dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  TDamageDefEstForm = class(TBaseCxListForm)
    TypeCodes: TADODataSet;
    SizeCodes: TADODataSet;
    MatCodes: TADODataSet;
    ProfitCenters: TADODataSet;
    UtilityCompanys: TADODataSet;
    ColFacilityType: TcxGridDBColumn;
    ColFacilitySize: TcxGridDBColumn;
    ColFacilityMaterial: TcxGridDBColumn;
    ColProfitCenter: TcxGridDBColumn;
    ColUtilityCoName: TcxGridDBColumn;
    ColDefaultAmount: TcxGridDBColumn;
    cbDefaultEstSearch: TCheckBox;
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure cbDefaultEstSearchClick(Sender: TObject);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    DamageDefInserted: Boolean;
    procedure LoadLookupLists;
  public
    procedure OpenDataSets; override;
    procedure RefreshNow; override;
    procedure LeavingNow; override;
    procedure CloseDatasets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TDamageDefEstForm }

procedure TDamageDefEstForm.OpenDataSets;
begin
  inherited;
  TypeCodes.Open;
  SizeCodes.Open;
  MatCodes.Open;
  ProfitCenters.Open;
  UtilityCompanys.Open;
  LoadLookupLists;
end;

procedure TDamageDefEstForm.CloseDatasets;
begin
  inherited;
  TypeCodes.Close;
  SizeCodes.Close;
  MatCodes.Close;
  ProfitCenters.Close;
  UtilityCompanys.Close;
end;

procedure TDamageDefEstForm.RefreshNow;
begin
  LoadLookupLists;
  UtilityCompanys.Requery;
  Data.Requery;
end;

procedure TDamageDefEstForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbDefaultEstSearch.Enabled := True;
end;

procedure TDamageDefEstForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TDamageDefEstForm.LoadLookupLists;
begin
  GetDataSetFieldValueList(CxComboBoxItems(ColFacilityType), TypeCodes, 'code');
  GetDataSetFieldValueList(CxComboBoxItems(ColFacilitySize), SizeCodes, 'code', '*');
  GetDataSetFieldValueList(CxComboBoxItems(ColFacilityMaterial), MatCodes, 'code', '*');
  GetDataSetFieldValueList(CxComboBoxItems(ColProfitCenter), ProfitCenters, 'pc_code', '*');
end;

procedure TDamageDefEstForm.cbDefaultEstSearchClick(Sender: TObject);
begin
  inherited;
  if cbDefaultEstSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TDamageDefEstForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DamageDefInserted := False;
end;

procedure TDamageDefEstForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'damage_default_est', 'ddest_id', DamageDefInserted);
end;

procedure TDamageDefEstForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TDamageDefEstForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  DamageDefInserted := True;
end;

procedure TDamageDefEstForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('utility_co_name', Data, 'utility_co_id', UtilityCompanys, 'ref_id', 'description', 35);
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  Data.FieldByName('utility_co_name').LookupCache := True;
end;

procedure TDamageDefEstForm.ActivatingNow;
begin
  inherited;
  DamageDefInserted := False;
end;

procedure TDamageDefEstForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

end.
