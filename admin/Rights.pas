unit Rights;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, ADODB, cxDBData, Vcl.StdCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxCustomStatusKeeper, cxStatusKeeper,
  cxGridViewStorage, dxSkinsCore, dxSkinBasic, dxDateRanges, dxScrollbarAnnotations;

type
  TRightsFrame = class(TFrame)
    RightsGrid: TcxGrid;
    RightsGridDBTableView1: TcxGridDBTableView;
    ColGroupRightsGridCategory: TcxGridDBColumn;
    ColGroupRightsGridRight: TcxGridDBColumn;
    ColGroupRightsGridID: TcxGridDBColumn;
    ColGroupRightsGridAllowed: TcxGridDBColumn;
    ColGroupRightsGridLimitation: TcxGridDBColumn;
    RightsGridLevel1: TcxGridLevel;
    RightHeaderLabel: TLabel;
    GrantButton: TButton;
    RevokeButton: TButton;
    ModifierLabel: TLabel;
    EditLimitation: TEdit;
    LimitationHintLabel: TLabel;
  strict private
    FStatusKeeper: TcxStatusKeeper;
    FSelectedRightID: Integer;
    procedure CreateStatusKeeper;
    procedure SetRight(Allowed, Limitation, ParentKeyField, PK: string; ParentData: TDataSet; InsertCommand, UpdateCommand: TADOCommand);
  public
    procedure SetEmployeeRight(Allowed, Limitation: string; EmpData: TDataSet; InsertRightCommand, UpdateRightCommand: TADOCommand);
    procedure SetEmpGroupRight(Allowed, Limitation: string; GroupData: TDataSet; InsertRightCommand, UpdateRightCommand: TADOCommand);
    procedure SaveState;
    procedure RestoreState;
  end;

implementation

{$R *.dfm}

{ TRightsFrame }

procedure TRightsFrame.CreateStatusKeeper;
begin
  if not Assigned(FStatusKeeper) then begin
    FStatusKeeper := TcxStatusKeeper.Create(Self); // destroyed when the frame is destroyed
    FStatusKeeper.AddComponent(RightsGridDBTableView1);
  end;
end;

procedure TRightsFrame.SaveState;
begin
  FSelectedRightID := RightsGridDBTableView1.DataController.DataSet.FieldByName('right_id').AsInteger;
  CreateStatusKeeper;
  FStatusKeeper.SaveState;
end;

procedure TRightsFrame.RestoreState;
begin
  RightsGrid.SetFocus;
  FStatusKeeper.LoadState;
  RightsGridDBTableView1.DataController.DataSet.Locate('right_id', FSelectedRightID, []);

  // When right_id references the 1st row in a group, the group row gets focus; move to the first data row.
  if (not RightsGridDBTableView1.Controller.FocusedRow.IsData) then
    RightsGridDBTableView1.Controller.GoToNext(True);
end;

procedure TRightsFrame.SetEmpGroupRight(Allowed, Limitation: string; GroupData: TDataSet;
  InsertRightCommand, UpdateRightCommand: TADOCommand);
begin
  SetRight(Allowed, Limitation, 'group_id', 'group_right_id', GroupData, InsertRightCommand,
    UpdateRightCommand);
end;

procedure TRightsFrame.SetEmployeeRight(Allowed, Limitation: string; EmpData: TDataSet;
  InsertRightCommand, UpdateRightCommand: TADOCommand);
begin
  SetRight(Allowed, Limitation, 'emp_id', 'emp_right_id', EmpData, InsertRightCommand,
    UpdateRightCommand);
end;

procedure TRightsFrame.SetRight(Allowed, Limitation, ParentKeyField, PK: string;
  ParentData: TDataSet; InsertCommand, UpdateCommand: TADOCommand);
var
  Id: Integer;
  ParentId: Integer;
  RightsData: TDataSet;
begin
  RightsData := RightsGridDBTableView1.DataController.DataSet;
  Assert(Assigned(RightsData));
  Assert(Assigned(ParentData));
  Assert(Assigned(InsertCommand));
  Assert(Assigned(UpdateCommand));

  Id := RightsData.FieldByName(PK).AsInteger;
  SaveState;
  if Limitation = '' then
    Limitation := '-';
  if Id = 0 then begin
    ParentId := ParentData.FieldByName(ParentKeyField).AsInteger;
    InsertCommand.Parameters.ParamValues['allowed'] := Allowed;
    InsertCommand.Parameters.ParamValues['limitation'] := Limitation;
    InsertCommand.Parameters.ParamValues[ParentKeyField] := ParentId;
    InsertCommand.Parameters.ParamValues['right_id'] := RightsData.FieldByName('right_id').AsInteger;
    InsertCommand.Execute;
  end else begin
    UpdateCommand.Parameters.ParamValues['allowed'] := Allowed;
    UpdateCommand.Parameters.ParamValues['limitation'] := Limitation;
    UpdateCommand.Parameters.ParamValues['id'] := Id;
    UpdateCommand.Execute;
  end;
  RightsData.Close;
  RightsData.Open;
  RestoreState;
end;

end.
