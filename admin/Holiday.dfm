inherited HolidayForm: THolidayForm
  Caption = 'Holidays'
  ClientHeight = 234
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 22
    object chkboxHolidaySearch: TCheckBox
      Left = 26
      Top = 4
      Width = 97
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxHolidaySearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 22
    Height = 212
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'cc_code;holiday_date'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'cc_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 18
        Width = 196
      end
      object ColHolidayDate: TcxGridDBColumn
        Caption = 'Holiday Date'
        DataBinding.FieldName = 'holiday_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 276
      end
      object ColBillingHoliday: TcxGridDBColumn
        Caption = 'Billing Holiday'
        DataBinding.FieldName = 'billing_holiday'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Width = 82
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from holiday order by holiday_date desc, cc_code'
  end
end
