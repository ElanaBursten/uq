inherited StatusGroupForm: TStatusGroupForm
  Left = 438
  Top = 211
  Caption = 'Status Groups'
  ClientHeight = 475
  ClientWidth = 836
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 836
    Height = 27
    object GroupLabel: TLabel
      Left = 8
      Top = 7
      Width = 175
      Height = 13
      Caption = 'Press the Insert key to add a group.'
    end
    object cbStatusGroupSearch: TCheckBox
      Left = 210
      Top = 8
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbStatusGroupSearchClick
    end
    object ActiveStatusGroupsFilter: TCheckBox
      Left = 334
      Top = 7
      Width = 163
      Height = 14
      Caption = 'Active Status Groups Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveStatusGroupsFilterClick
    end
  end
  object StatusPanel: TPanel [1]
    Left = 425
    Top = 27
    Width = 411
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    TabOrder = 1
    object StatusHeaderPanel: TPanel
      Left = 8
      Top = 8
      Width = 395
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object StatusLabel: TLabel
        Left = 0
        Top = 6
        Width = 131
        Height = 13
        Caption = 'Statuses in selected group:'
      end
      object SaveContentsButton: TButton
        Left = 157
        Top = -1
        Width = 113
        Height = 25
        Caption = 'Save Statuses'
        Enabled = False
        TabOrder = 0
        OnClick = SaveContentsButtonClick
      end
    end
    object Panel1: TPanel
      Left = 8
      Top = 38
      Width = 395
      Height = 402
      Align = alClient
      TabOrder = 1
      object CLGrid: TStringGrid
        Left = 1
        Top = 1
        Width = 393
        Height = 400
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alClient
        ColCount = 1
        DefaultColWidth = 230
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 0
        OnDrawCell = CLGridDrawCell
        OnKeyPress = CLGridKeyPress
        OnMouseDown = CLGridMouseDown
        OnSelectCell = CLGridSelectCell
        OnSetEditText = CLGridSetEditText
        OnTopLeftChanged = CLGridTopLeftChanged
      end
    end
    object FindButton: TButton
      Left = 304
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Find'
      TabOrder = 2
      OnClick = FindButtonClick
    end
  end
  inherited Grid: TcxGrid
    Top = 27
    Width = 425
    Height = 448
    Align = alLeft
    TabOrder = 2
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'sg_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object Gridsg_id: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'sg_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 20
      end
      object Gridsg_name: TcxGridDBColumn
        Caption = 'Group Name'
        DataBinding.FieldName = 'sg_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 201
      end
      object Gridactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 50
      end
      object Gridright_id: TcxGridDBColumn
        Caption = 'Required Right'
        DataBinding.FieldName = 'right_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        OnGetDisplayText = Gridright_idGetDisplayText
        Options.Editing = False
        Width = 171
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    OnNewRecord = DataNewRecord
    CommandText = 'select * from status_group where active = 1  order by sg_name'
  end
  inherited DS: TDataSource
    OnDataChange = DSDataChange
  end
  object StatusDetail: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select  statuslist.status + '#39' - '#39' + status_name +'#13#10' case when st' +
      'atuslist.active=0 then '#39' (inactive)'#39' else '#39#39' end as status,'#13#10'sgi' +
      '.sg_item_id, sgi.active, sgi.status_explanation,sgi.outgoing_sta' +
      'tus'#13#10'from statuslist'#13#10' left join status_group_item sgi'#13#10'   on st' +
      'atuslist.status = sgi.status  and sgi.sg_id=:sgid'#13#10
    Parameters = <
      item
        Name = 'sgid'
        Size = -1
        Value = Null
      end>
    Left = 56
    Top = 168
  end
  object UpdateStatusGroupSP: TADOStoredProc
    Connection = AdminDM.Conn
    ProcedureName = 'update_status_group;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SGID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Statuses'
        Attributes = [paNullable]
        DataType = ftString
        Size = 400
        Value = Null
      end>
    Left = 56
    Top = 224
  end
  object RightDefinitions: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from right_definition order by right_description'
    Parameters = <>
    Left = 32
    Top = 276
  end
  object FindDialog1: TFindDialog
    OnFind = FindDialog1Find
    Left = 177
    Top = 139
  end
  object TempDataSet: TADODataSet
    Connection = AdminDM.Conn
    Parameters = <>
    Left = 184
    Top = 208
  end
  object TVPDataset: TADODataSet
    Connection = AdminDM.Conn
    Parameters = <>
    Left = 176
    Top = 280
  end
end
