unit ClientRespondTo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, odMiscUtils, odADOUtils, odHourGlass,
  dxScrollbarAnnotations, Data.DB, cxDBData, Vcl.StdCtrls, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDMu,
  cxDBExtLookupComboBox, cxDropDownEdit, cxDBLookupComboBox, cxTextEdit,
  cxCheckBox;

type
  TfrmClientRespondTo = class(TBaseCxListForm)
    chkboxRespondToSearch: TCheckBox;
    ActiveRespondToFilter: TCheckBox;
    ClientLookupDS: TDataSource;
    ClientLookup: TADODataSet;
    ColClientID: TcxGridDBColumn;
    ClientLookupView: TcxGridDBTableView;
    ColClientLookupClientName: TcxGridDBColumn;
    ColClientLookupOCCode: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    RespondToLookupView: TcxGridDBTableView;
    RespondToLookup: TADODataSet;
    RespondToLookupDS: TDataSource;
    ColRespondTo: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColRespondToLookupRespondTo: TcxGridDBColumn;
    ColClientLookupActive: TcxGridDBColumn;
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure ActiveRespondToFilterClick(Sender: TObject);
    procedure chkboxRespondToSearchClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure ClientLookupViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ColClientIDPropertiesEditValueChanged(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    clientcode: String;
    CRTInserted: Boolean;
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;

var
  frmClientRespondTo: TfrmClientRespondTo;

implementation

{$R *.dfm}

{ TfrmClientRespondTo }

procedure TfrmClientRespondTo.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TfrmClientRespondTo.OpenDataSets;
begin
  ClientLookup.Open;
  RespondToLookup.Open;
  inherited;
end;

procedure TfrmClientRespondTo.ActiveRespondToFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: Integer;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('crt_id').AsInteger;
  Data.Close;
  if ActiveRespondToFilter.Checked then
    Data.CommandText := 'Select * from client_respond_to where active = 1 order by client_code' else
      Data.CommandText := 'select * from client_respond_to order by client_code';
  Data.Open;
  Data.Locate('crt_id', FSelectedID, []);
end;

procedure TfrmClientRespondTo.chkboxRespondToSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxRespondToSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TfrmClientRespondTo.ClientLookupViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColClientLookupActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TfrmClientRespondTo.CloseDataSets;
begin
  ClientLookup.Close;
  RespondToLookup.Close;
  inherited;
end;

procedure TfrmClientRespondTo.ColClientIDPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  clientcode := ClientLookup.FieldByName('oc_code').AsString;
end;

procedure TfrmClientRespondTo.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CRTInserted := False;
end;

procedure TfrmClientRespondTo.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'client_respond_to', 'crt_id', CRTInserted);
end;

procedure TfrmClientRespondTo.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TfrmClientRespondTo.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  CRTInserted := True;
end;

procedure TfrmClientRespondTo.DataBeforePost(DataSet: TDataSet);
   var
   ColIdx, RecIdx: Integer;
begin
  inherited;
    if Data.FieldByName('client_id').IsNull then
       raise Exception.Create('A client_id is required to save a client respond to record');
    if Data.FieldByName('client_respond_to').IsNull then
       raise Exception.Create('respond_to field is required to save a client respond to record');

    if clientcode <> '' then
    begin
      Data.FieldByName('client_code').AsString := clientcode;
      clientcode := '';
    end;
end;

procedure TfrmClientRespondTo.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TfrmClientRespondTo.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TfrmClientRespondTo.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
  CRTInserted := False;
end;

procedure TfrmClientRespondTo.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveRespondToFilter.Enabled := True;
  chkboxRespondToSearch.Enabled := True;
end;

procedure TfrmClientRespondTo.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) and
      (VarToInt(GridRec.Values[ColClientID.Index]) > 0) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;


end.
