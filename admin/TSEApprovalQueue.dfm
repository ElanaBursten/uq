inherited ApprovalQueueForm: TApprovalQueueForm
  Caption = 'TSE Approval Queue'
  ClientWidth = 856
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 856
    Height = 29
    object chkboxSearch: TCheckBox
      Left = 23
      Top = 9
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 29
    Width = 856
    Height = 371
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      object ColEntryID: TcxGridDBColumn
        Caption = 'TSE Entry Id'
        DataBinding.FieldName = 'TSE_Entry_Id'
        Width = 92
      end
      object ColWorkEmpID: TcxGridDBColumn
        Caption = 'Work Emp ID'
        DataBinding.FieldName = 'Work_Emp_ID'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 84
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'Work_Emp_ID'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = EmployeeView
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListFieldItem = EmpNameCol
        Properties.ReadOnly = False
        Width = 121
      end
      object ColStatus: TcxGridDBColumn
        DataBinding.FieldName = 'Status'
        Width = 75
      end
      object ColWorkDate: TcxGridDBColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'WorkDate'
        Width = 73
      end
      object ColQR: TcxGridDBColumn
        DataBinding.FieldName = 'QR'
      end
      object ColQC: TcxGridDBColumn
        DataBinding.FieldName = 'QC'
      end
      object ColNote: TcxGridDBColumn
        Caption = 'Note'
        DataBinding.FieldName = 'note'
        Width = 238
      end
    end
    object EmployeeView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmployeeDS
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object EmpIDCol: TcxGridDBColumn
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = False
      end
      object EmpNameCol: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from TSE_ApprovalQueue'
  end
  inherited DS: TDataSource
    Left = 120
  end
  object Employees: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      #13#10'Select  emp_id, '#13#10'case'#13#10'  when (last_name is not NULL) and (fi' +
      'rst_name is not NULL) then'#13#10'       concat(last_name, '#39', '#39', first' +
      '_name)'#13#10' when last_name is not NULL then'#13#10'         last_name'#13#10'  ' +
      'else NULL'#13#10'end as Name'#13#10'from employee '#13#10' where  active = 1 order' +
      ' by Name'
    Parameters = <>
    Left = 56
    Top = 166
  end
  object EmployeeDS: TDataSource
    DataSet = Employees
    Left = 120
    Top = 166
  end
  object DisableTrigger: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <>
    Left = 64
    Top = 224
  end
end
