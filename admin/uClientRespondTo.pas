unit uClientRespondTo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, odMiscUtils, odADOUtils, odHourGlass,
  dxScrollbarAnnotations, Data.DB, cxDBData, Vcl.StdCtrls, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDMu,
  cxDBExtLookupComboBox, cxDropDownEdit, cxDBLookupComboBox, cxTextEdit;

type
  TfrmClientRespondTo = class(TBaseCxListForm)
    chkboxRespondToSearch: TCheckBox;
    ActiveRespondToFilter: TCheckBox;
    ClientLookupDS: TDataSource;
    ClientLookup: TADODataSet;
    ColClientID: TcxGridDBColumn;
    ClientLookupView: TcxGridDBTableView;
    ColClientLookupClientName: TcxGridDBColumn;
    ColClientLookupOCCode: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    StatusLookupView: TcxGridDBTableView;
    StatusLookup: TADODataSet;
    StatusLookupDS: TDataSource;
    ColStatusLookupName: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure ActiveRespondToFilterClick(Sender: TObject);
    procedure chkboxRespondToSearchClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  frmClientRespondTo: TfrmClientRespondTo;

implementation

{$R *.dfm}

{ TfrmClientRespondTo }

procedure TfrmClientRespondTo.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TfrmClientRespondTo.OpenDataSets;
begin
  ClientLookup.Open;
  StatusLookup.Open;
  inherited;
end;

procedure TfrmClientRespondTo.ActiveRespondToFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: Integer;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('crt_id').AsInteger;
  Data.Close;
  if ActiveRespondToFilter.Checked then
    Data.CommandText := 'Select * from client_respond_to where active = 1 order by client_code' else
      Data.CommandText := 'select * from client_respond_to order by client_code';
  Data.Open;
  Data.Locate('crt_id', FSelectedID, []);
end;

procedure TfrmClientRespondTo.chkboxRespondToSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxRespondToSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TfrmClientRespondTo.CloseDataSets;
begin
  ClientLookup.Close;
  StatusLookup.Close;
  inherited;
end;

procedure TfrmClientRespondTo.DataBeforePost(DataSet: TDataSet);
   var ClientCode: String;
   ColIdx, RecIdx: Integer;
begin
  inherited;
    if Data.FieldByName('client_id').IsNull then
       raise Exception.Create('A client_id is required to save a respondto record');
    Data.FieldByName('client_code').AsString := ClientLookup.FieldByName('oc_code').AsString;
end;
procedure TfrmClientRespondTo.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TfrmClientRespondTo.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TfrmClientRespondTo.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveRespondToFilter.Enabled := True;
  chkboxRespondToSearch.Enabled := True;
end;

procedure TfrmClientRespondTo.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) and
      (VarToInt(GridRec.Values[ColClientID.Index]) > 0) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;


end.
