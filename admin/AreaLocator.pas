unit AreaLocator;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  DB, StdCtrls, Buttons, Grids, DBGrids;

type
  TAreaLocatorFrame = class(TFrame)
    Label3: TLabel;
    Label4: TLabel;
    AssignToLocatorBtn: TBitBtn;
    LocatorGrid: TDBGrid;
    areaDS: TDataSource;
    locatorDs: TDataSource;
    areaGrid: TDBGrid;
    procedure AssignToLocatorBtnClick(Sender: TObject);
  private
    FOnChangesMade: TNotifyEvent;
  public
    property OnChangesMade: TNotifyEvent read FOnChangesMade write FOnChangesMade;
  end;

implementation

{$R *.dfm}

procedure TAreaLocatorFrame.AssignToLocatorBtnClick(Sender: TObject);
begin
  AreaDS.DataSet.Edit;
  AreaDS.DataSet.FieldByName('locator_id').AsInteger :=
    locatorDS.DataSet.FieldByName('emp_id').AsInteger;
  AreaDS.DataSet.Post;

  if Assigned(FOnChangesMade) then
    FOnChangesMade(Self);
end;

end.

