unit StatusPlus;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges,dxScrollbarAnnotations, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Data.Win.ADODB, cxGridLevel, cxClasses,cxGridCustomView, cxGrid, Vcl.ExtCtrls,
  Vcl.StdCtrls, AdminDmu, OdMiscUtils, OdHourGlass;

type
  TStatusPlusForm = class(TBaseCxListForm)
    ColCallCenter: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColStatusPlus: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ActiveSPFilter: TCheckBox;
    chkboxStatusPlusSearch: TCheckBox;
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure ActiveSPFilterClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure chkboxStatusPlusSearchClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    SPInserted: Boolean;
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  public
    { Public declarations }
    procedure EditNow; override;
  end;

var
  StatusPlusForm: TStatusPlusForm;

implementation

{$R *.dfm}

{ TFormStatusPlus }

procedure TStatusPlusForm.ActivatingNow;
begin
  inherited;
  SPInserted := false;
end;

procedure TStatusPlusForm.ActiveSPFilterClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  Data.Close;
  if ActiveSPFilter.Checked then
    Data.CommandText := 'select * from status_plus where active = 1' else
      Data.CommandText := 'select * from status_plus';
  Data.Open;
end;

procedure TStatusPlusForm.chkboxStatusPlusSearchClick(Sender: TObject);
begin
   inherited;
  if chkboxStatusPlusSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TStatusPlusForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  SPInserted := False;
end;

procedure TStatusPlusForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'status_plus', 'status_plus_id', SPInserted);
end;

procedure TStatusPlusForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TStatusPlusForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
 SPInserted := True;
end;

procedure TStatusPlusForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TStatusPlusForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TStatusPlusForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TStatusPlusForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveSPFilter.Enabled := True;
  chkboxStatusPlusSearch.Enabled := True;
end;

end.
