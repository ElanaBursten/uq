inherited EmailQueueRulesForm: TEmailQueueRulesForm
  Caption = 'Locate Notification Emails'
  ClientWidth = 923
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 923
    Height = 24
    object chkboxEmailSearch: TCheckBox
      Left = 19
      Top = 4
      Width = 97
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxEmailSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 24
    Width = 923
    Height = 376
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnInitEdit = GridViewInitEdit
      OnUpdateEdit = GridViewUpdateEdit
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'eqr_id'
      OptionsBehavior.AlwaysShowEditor = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Centers'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 89
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client / Term Code'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 112
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 75
      end
      object ColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 33
      end
      object ColStatusMessage: TcxGridDBColumn
        Caption = 'Status Message'
        DataBinding.FieldName = 'status_message'
        Width = 229
      end
      object GridViewincl_bcc: TcxGridDBColumn
        DataBinding.FieldName = 'incl_bcc'
        Width = 52
      end
      object GridViewbcc_address: TcxGridDBColumn
        DataBinding.FieldName = 'bcc_address'
        Width = 200
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from email_queue_rules'#13#10'order by call_center'
  end
end
