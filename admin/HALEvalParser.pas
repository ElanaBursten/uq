unit HALEvalParser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, oddbUtils,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDmu, cxTextEdit, Vcl.StdCtrls;

type
  TEvalParserForm = class(TBaseCxListForm)
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Splitter1: TSplitter;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    ColHistoryID: TcxGridDBColumn;
    ColSourceFile: TcxGridDBColumn;
    ColMessage: TcxGridDBColumn;
    ColStatusCode: TcxGridDBColumn;
    lblFileCount: TLabel;
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  EvalParserForm: TEvalParserForm;

implementation

{$R *.dfm}

uses MainFU;

{ TEvalParserForm }

procedure TEvalParserForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  SetReadOnly(True);
end;

procedure TEvalParserForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TEvalParserForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TEvalParserForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TEvalParserForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function TEvalParserForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TEvalParserForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TEvalParserForm.OpenDataSets;
begin
  inherited;
  lblFileCount.Caption := lblFileCount.Caption +
     InttoStr(Data.RecordCount);
  HalConfig.Open;
  SetReadOnly(True);
end;

procedure TEvalParserForm.SetReadOnly(const IsReadOnly: Boolean);
begin
end;

end.
