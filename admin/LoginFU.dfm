object LoginForm: TLoginForm
  Left = 306
  Top = 206
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsDialog
  Caption = 'Log In'
  ClientHeight = 295
  ClientWidth = 403
  Color = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 164
    Width = 40
    Height = 13
    Caption = 'User ID:'
  end
  object AppLabel: TLabel
    Left = 40
    Top = 88
    Width = 313
    Height = 22
    Alignment = taCenter
    AutoSize = False
    Caption = '[Application Label]'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 64
    Top = 211
    Width = 36
    Height = 13
    Caption = 'Server:'
  end
  object VersionLabel: TLabel
    Left = 168
    Top = 120
    Width = 56
    Height = 13
    Caption = 'Version ___'
  end
  object LogoImage: TImage
    Left = 1
    Top = 1
    Width = 400
    Height = 83
    Center = True
  end
  object Label3: TLabel
    Left = 64
    Top = 235
    Width = 50
    Height = 13
    Caption = 'Database:'
  end
  object PasswordLabel: TLabel
    Left = 64
    Top = 186
    Width = 50
    Height = 13
    Caption = 'Password:'
  end
  object EditUserName: TEdit
    Left = 119
    Top = 159
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object EditPassword: TEdit
    Left = 119
    Top = 183
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object LoginBtn: TButton
    Left = 272
    Top = 197
    Width = 65
    Height = 25
    Caption = 'Login'
    Default = True
    TabOrder = 5
    OnClick = LoginBtnClick
  end
  object CancelBtn: TButton
    Left = 272
    Top = 228
    Width = 65
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
    OnClick = CancelBtnClick
  end
  object RememberBox: TCheckBox
    Left = 119
    Top = 259
    Width = 120
    Height = 17
    Caption = 'Remember Password'
    TabOrder = 4
  end
  object EditServer: TEdit
    Left = 119
    Top = 208
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object EditDatabase: TEdit
    Left = 119
    Top = 232
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object TrustedBox: TCheckBox
    Left = 255
    Top = 163
    Width = 120
    Height = 17
    Caption = 'Trusted Connection'
    TabOrder = 7
    OnClick = TrustedBoxClick
  end
end
