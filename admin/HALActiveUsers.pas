unit HALActiveUsers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdminDmU, BaseCxList, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Vcl.StdCtrls, Vcl.DBCtrls,
  Data.Win.ADODB, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, System.Actions,
  Vcl.ActnList, MainFU, TermEmp;

type
  TActiveUsersForm = class(TBaseCxListForm)
    btnEmpByCompany: TButton;
    Companies: TADODataSet;
    ADActiveUsers: TADODataSet;
    dsADActiveUsers: TDataSource;
    dsCompanies: TDataSource;
    cbCompany: TDBLookupComboBox;
    Panel1: TPanel;
    P: TPageControl;
    ADINfoSheet: TTabSheet;
    DBGrid1: TDBGrid;
    btnADInfo: TButton;
    Splitter1: TSplitter;
    ActionList1: TActionList;
    btnAction: TAction;
    ConnAD: TADOConnection;
    cxGrid1: TcxGrid;
    InactiveGridView: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Panel2: TPanel;
    btnInactiveEmp: TButton;
    InactiveEmp: TADODataSet;
    dsInactiveEmp: TDataSource;
    cbEmpByCompanySearch: TCheckBox;
    cbInactiveSearch: TCheckBox;
    InactiveEmpemp_id: TAutoIncField;
    InactiveEmpemp_number: TStringField;
    InactiveEmpfirst_name: TStringField;
    InactiveEmplast_name: TStringField;
    InactiveEmpshort_name: TStringField;
    InactiveEmptype_id: TIntegerField;
    InactiveEmptimesheet_id: TIntegerField;
    InactiveEmpemp_active: TBooleanField;
    InactiveEmpcompany_id: TIntegerField;
    InactiveEmpaccount_disabled_date: TDateTimeField;
    InactiveGridViewemp_id: TcxGridDBColumn;
    InactiveGridViewemp_number: TcxGridDBColumn;
    InactiveGridViewfirst_name: TcxGridDBColumn;
    InactiveGridViewlast_name: TcxGridDBColumn;
    InactiveGridViewshort_name: TcxGridDBColumn;
    InactiveGridViewtype_id: TcxGridDBColumn;
    InactiveGridViewtimesheet_id: TcxGridDBColumn;
    InactiveGridViewemp_active: TcxGridDBColumn;
    InactiveGridViewcompany_id: TcxGridDBColumn;
    InactiveGridViewaccount_disabled_date: TcxGridDBColumn;
    btnInactiveEmpInfo: TButton;
    procedure btnEmpByCompanyClick(Sender: TObject);
    procedure btnADInfoClick(Sender: TObject);
    procedure btnActionUpdate(Sender: TObject);
    procedure cbCompanyCloseUp(Sender: TObject);
    procedure btnInactiveEmpClick(Sender: TObject);
    procedure btnInactiveEmpInfoClick(Sender: TObject);
    procedure cbEmpByCompanySearchClick(Sender: TObject);
    procedure cbInactiveSearchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  ActiveUsersForm: TActiveUsersForm;

implementation

{$R *.dfm}

procedure TActiveUsersForm.btnEmpByCompanyClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  Data.Close;
  Data.Parameters.ParamByName('company').Value := cbCompany.Text;
  Data.Open;
  Screen.Cursor := crDefault;
end;

procedure TActiveUsersForm.btnInactiveEmpClick(Sender: TObject);
begin
  inherited;
  InactiveEmp.Open;
end;

procedure TActiveUsersForm.btnADInfoClick(Sender: TObject);
begin
  inherited;
  ConnAD.Open;
  ADActiveUsers.Parameters.ParamByName('adusername').Value := Data.FieldByName('ad_username').AsString;
  ADActiveUsers.Open;
end;

procedure TActiveUsersForm.btnInactiveEmpInfoClick(Sender: TObject);
begin
  inherited;
   if InactiveEmp.Active and not InactiveEmp.EOF then
     DisplayTermEmpInfo(InactiveEmp.FieldByName('emp_id').AsInteger);
end;

procedure TActiveUsersForm.cbCompanyCloseUp(Sender: TObject);
begin
  inherited;
  if cbCompany.Text <> '' then
    btnEmpByCompany.Enabled := True;
end;

procedure TActiveUsersForm.cbEmpByCompanySearchClick(Sender: TObject);
begin
  inherited;
  if cbEmpByCompanySearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TActiveUsersForm.btnActionUpdate(Sender: TObject);
begin
  inherited;
  TAction(Sender).Enabled := not Data.EOF;
end;

procedure TActiveUsersForm.ActivatingNow;
begin
  OpenDataSets;
end;

procedure TActiveUsersForm.cbInactiveSearchClick(Sender: TObject);
begin
  inherited;
   if cbInactiveSearch.Checked then
    InactiveGridView.Controller.ShowFindPanel
  else
    InactiveGridView.Controller.HideFindPanel;
end;

procedure TActiveUsersForm.CloseDataSets;
begin
  Data.Close;
  Companies.Close;
  if ConnAd.Connected then
  begin
    ConnAD.Close;
    ADActiveUsers.Close;
  end;
end;

function TActiveUsersForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TActiveUsersForm.LeavingNow;
begin
  CloseDataSets;
  btnEmpByCompany.Enabled := False;
  cbCompany.KeyValue := '';
end;

procedure TActiveUsersForm.OpenDataSets;
begin
  Companies.Open;
end;

end.
