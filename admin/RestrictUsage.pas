
unit RestrictUsage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, DateUtils, QMConst,
  OdDbUtils, OdAdoUtils, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxMemo, cxCheckBox,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxDropDownEdit,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, AdminDmu,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges;

type
  TRestrictUsageForm = class(TBaseCxListForm)
    BottomPanel: TPanel;
    TimeLimitInstructionLabel: TLabel;
    WarningMessageInstructionLabel: TLabel;
    RestrictedUseMessage: TADODataSet;
    RestrictedUseMessageDS: TDataSource;
    Splitter1: TSplitter;
    MessageGrid: TcxGrid;
    MessageGridLevel: TcxGridLevel;
    MessageGridView: TcxGridDBTableView;
    MessageGridrum_id: TcxGridDBColumn;
    MessageGridcountdown_minutes: TcxGridDBColumn;
    MessageGridmessage_text: TcxGridDBColumn;
    MessageGridactive: TcxGridDBColumn;
    Gridright_restriction_id: TcxGridDBColumn;
    Gridright_id: TcxGridDBColumn;
    Gridgroup_name: TcxGridDBColumn;
    Gridrestriction_type: TcxGridDBColumn;
    Gridstart_value: TcxGridDBColumn;
    Gridend_value: TcxGridDBColumn;
    Gridactive: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure RestrictedUseMessageAfterPost(DataSet: TDataSet);
    procedure RestrictedUseMessageNewRecord(DataSet: TDataSet);
    procedure MessageGridExit(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure RestrictedUseMessageAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure RestrictedUseMessageBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure RestrictedUseMessageBeforeEdit(DataSet: TDataSet);
  private
    RRInserted: Boolean;
    RRMsgInserted: Boolean;
    RRMsgArray: TFieldValuesArray;
    function GetRightIDForRightCode(const Code: string): Integer;
  public
    procedure LeavingNow; override;
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses OdCxUtils;

{$R *.dfm}

{ TRestrictUsageForm }


procedure TRestrictUsageForm.OpenDataSets;
begin
  Data.Parameters.ParamByName('right_id').Value := 114;//GetRightIDForRightCode(RightRestrictUsageLimitHours);
  RestrictedUseMessage.Open;
  inherited;
end;

procedure TRestrictUsageForm.CloseDataSets;
begin
  inherited;
  RestrictedUseMessage.Close;
end;

procedure TRestrictUsageForm.LeavingNow;
begin
  inherited;
  if RestrictedUseMessage.State in dsEditModes then
    RestrictedUseMessage.Post;
end;

procedure TRestrictUsageForm.MessageGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(RestrictedUseMessage);
end;

procedure TRestrictUsageForm.FormShow(Sender: TObject);
begin
  CxComboBoxItems(Gridrestriction_type).Clear;
  CxComboBoxItems(Gridrestriction_type).Add(UsageRestrictionTypeWeekday);
  CxComboBoxItems(Gridrestriction_type).Add(UsageRestrictionTypeWeekend);
  inherited;
end;

function TRestrictUsageForm.GetRightIDForRightCode(const Code: string): Integer;
const
  SQL = 'select right_id from right_definition where entity_data=';
var
  RightData: TDataSet;
begin
  Result := -1;
  RightData := CreateDatasetWithQuery(Data.Connection, SQL+QuotedStr(Code));
  try
    if not RightData.Eof then
      Result := RightData.FieldByName('right_id').AsInteger;
  finally
    FreeAndNil(RightData);
  end;
end;

procedure TRestrictUsageForm.DataBeforePost(DataSet: TDataSet);
var
  StartTime: TDateTime;
  EndTime: TDateTime;
begin
  inherited;
  try
    StartTime := StrToTime(DataSet.FieldByName('start_value').AsString);
    EndTime := StrToTime(DataSet.FieldByName('end_value').AsString);
    if StartTime > EndTime then
      raise Exception.Create('The Start Time must occur on or before the End Time.');
  except
    on EConvertError do begin
      MessageDlg('Enter the Start Time and End Time in HH:MM format.', mtError, [mbOk], 0);
      Abort;
    end;
  end;
end;

procedure TRestrictUsageForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('right_id').Value := GetRightIDForRightCode(RightRestrictUsageLimitHours);
  DataSet.FieldByName('active').Value := 1;
end;

procedure TRestrictUsageForm.RestrictedUseMessageNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('active').Value := 1;
end;


procedure TRestrictUsageForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TRestrictUsageForm.ActivatingNow;
begin
  inherited;
  RRInserted := False;
  RRMsgInserted := False;
end;

procedure TRestrictUsageForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  RRInserted := False;
end;

procedure TRestrictUsageForm.RestrictedUseMessageAfterCancel(DataSet: TDataSet);
begin
  inherited;
  RRMsgInserted := False;
end;

procedure TRestrictUsageForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'right_restriction', 'right_restriction_id', RRInserted);
  DataSet.Close;
  DataSet.Open;
end;

procedure TRestrictUsageForm.RestrictedUseMessageAfterPost(DataSet: TDataSet);
begin
  AdminDM.TrackAdminChanges(Dataset, RRMsgArray, 'right_restriction', 'right_restriction_id', RRMsgInserted);
  DataSet.Close;
  DataSet.Open;
end;

procedure TRestrictUsageForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TRestrictUsageForm.RestrictedUseMessageBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, RRMsgArray);
end;

procedure TRestrictUsageForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  RRInserted := True;
end;

procedure TRestrictUsageForm.RestrictedUseMessageBeforeInsert(
  DataSet: TDataSet);
begin
  inherited;
  RRMsgInserted := True;
end;

end.
