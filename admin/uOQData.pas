unit uOQData;
//QM 447 BP

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxDBExtLookupComboBox, cxMaskEdit,
  cxTextEdit, cxCalendar, Vcl.StdCtrls;

type
  TOQDataForm = class(TBaseCxListForm)
    StatusRef: TADODataSet;
    EmpLookup: TADODataSet;
    StatusRefDS: TDataSource;
    EmpLookupDS: TDataSource;
    EmpLookupView: TcxGridDBTableView;
    StatusLookupView: TcxGridDBTableView;
    EmpIDCol: TcxGridDBColumn;
    EmpNameCol: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColExpiredDate: TcxGridDBColumn;
    ColQualifiedDate: TcxGridDBColumn;
    StatusCol: TcxGridDBColumn;
    ColEmpID: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    cboqDataSearch: TCheckBox;
    oqDataFilter: TCheckBox;
    CustLookupCol: TcxGridDBColumn;
    CustRef: TADODataSet;
    CustLookupView: TcxGridDBTableView;
    CustCol: TcxGridDBColumn;
    CustRefDS: TDataSource;
    procedure cboqDataSearchClick(Sender: TObject);
    procedure oqDataFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
     OQInserted: Boolean;
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  OQDataForm: TOQDataForm;

implementation

uses
 OdHourglass, odMiscUtils, odADOUtils, AdminDMu;

{$R *.dfm}

{ TOQDataForm }

procedure TOQDataForm.cboqDataSearchClick(Sender: TObject);
begin
  inherited;
    if cboqDataSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TOQDataForm.CloseDataSets;
begin
  inherited;
  EmpLookup.Close;
  StatusRef.Close;
  CustRef.Close;
end;

procedure TOQDataForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  OQInserted := False;
end;

procedure TOQDataForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'oq_data', 'oq_data_id', OQInserted);
end;

procedure TOQDataForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TOQDataForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  OQInserted := True;
end;

procedure TOQDataForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TOQDataForm.ActivatingNow;
begin
  inherited;
   OQInserted := False;
end;

procedure TOQDataForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TOQDataForm.OpenDataSets;
begin
  inherited;
  Data.Properties['Update Criteria'].Value := 0;
  EmpLookup.Open;
  StatusRef.Open;
  CustRef.Open;
end;

procedure TOQDataForm.oqDataFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('oq_data_id').AsInteger;
  Data.Close;
  if oqDataFilter.Checked then
    Data.CommandText := 'select * from oq_data where active = 1' else
      Data.CommandText := 'select * from oq_data';
  Data.Open;
  Data.Locate('oq_data_id', FSelectedID, []);

end;

procedure TOQDataForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  oqDataFilter.Enabled :=  True;
  cboqDataSearch.Enabled := True;
end;

end.
