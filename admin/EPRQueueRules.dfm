inherited EPRQueueRulesForm: TEPRQueueRulesForm
  Caption = 'EPR Queue Rules'
  ClientWidth = 688
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 688
    Height = 26
    object cbQueueRulesSearch: TCheckBox
      Left = 23
      Top = 4
      Width = 99
      Height = 16
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbQueueRulesSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 688
    Height = 374
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client / Term Code'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 104
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 78
      end
      object ColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 61
      end
      object ColStatusMessage: TcxGridDBColumn
        Caption = 'Status Message'
        DataBinding.FieldName = 'status_message'
        Width = 407
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from epr_queue_rules'
  end
end
