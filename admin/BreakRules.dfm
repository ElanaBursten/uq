inherited BreakRulesForm: TBreakRulesForm
  Left = 441
  Top = 208
  Caption = 'Break Rules'
  ClientHeight = 403
  ClientWidth = 878
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 878
    Height = 28
    object cbBReakRulesSearch: TCheckBox
      Left = 29
      Top = 7
      Width = 101
      Height = 16
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbBReakRulesSearchClick
    end
    object ActiveBreakRulesFilter: TCheckBox
      Left = 160
      Top = 6
      Width = 158
      Height = 17
      Caption = 'Active Break Rules Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveBreakRulesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 878
    Height = 296
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'rule_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.HeaderHeight = 32
      object ColRuleId: TcxGridDBColumn
        DataBinding.FieldName = 'rule_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 87
      end
      object ColPCCode: TcxGridDBColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'pc_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        SortIndex = 0
        SortOrder = soAscending
        Width = 58
      end
      object ColBreakLength: TcxGridDBColumn
        Caption = 'Break Length (min)'
        DataBinding.FieldName = 'Break_length'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 74
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 45
      end
      object ColRuleType: TcxGridDBColumn
        Caption = 'Rule Type'
        DataBinding.FieldName = 'rule_type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 7
        Width = 92
      end
      object ColBreakNeededAfter: TcxGridDBColumn
        Caption = 'Time For Break (hrs)'
        DataBinding.FieldName = 'Break_needed_after'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.EditMask = '!90:00;1;_'
        Width = 86
      end
      object ColRuleMessage: TcxGridDBColumn
        Caption = 'Message Text'
        DataBinding.FieldName = 'rule_message'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.AlwaysSaveData = False
        Properties.BlobEditKind = bekMemo
        Properties.BlobPaintStyle = bpsText
        Properties.MemoScrollBars = ssVertical
        Properties.MemoWantTabs = False
        Properties.ShowExPopupItems = False
        Properties.ShowPicturePopup = False
        Width = 251
      end
      object ColButtonText: TcxGridDBColumn
        Caption = 'Button Text'
        DataBinding.FieldName = 'button_text'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 193
      end
      object ColMessageFrequency: TcxGridDBColumn
        Caption = 'Frequency (days)'
        DataBinding.FieldName = 'message_frequency'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MinValue = True
        Properties.MaxValue = 365.000000000000000000
        HeaderAlignmentHorz = taRightJustify
        Width = 71
      end
      object ColModifiedDate: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 149
      end
    end
  end
  object BottomPanel: TPanel [2]
    Left = 0
    Top = 324
    Width = 878
    Height = 79
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 2
    object InstructionLabel: TLabel
      Left = 0
      Top = 0
      Width = 878
      Height = 79
      Align = alClient
      AutoSize = False
      Caption = 
        'Break Rules Guidelines:'#13'- No Profit Center should have more than' +
        ' one active SUBMIT Rule Type.'#13'- No Profit Center should have mor' +
        'e than one active EVERY or AFTER Rule Type.'#13'- Frequency && Butto' +
        'n Text only apply to SUBMIT rules.'#13'- Break Length and Time For B' +
        'reak only apply to EVERY && AFTER rule.'
      WordWrap = True
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from break_rules where active = 1'
    object Datarule_id: TAutoIncField
      FieldName = 'rule_id'
      ReadOnly = True
    end
    object Datapc_code: TStringField
      FieldName = 'pc_code'
      Size = 15
    end
    object Databreak_needed_after: TIntegerField
      FieldName = 'break_needed_after'
      OnGetText = DataBreak_needed_afterGetText
      OnSetText = DataBreak_needed_afterSetText
    end
    object Databreak_length: TIntegerField
      FieldName = 'break_length'
    end
    object Datarule_type: TStringField
      DisplayWidth = 20
      FieldName = 'rule_type'
    end
    object Datarule_message: TStringField
      FieldName = 'rule_message'
      Size = 3000
    end
    object Databutton_text: TStringField
      FieldName = 'button_text'
      Size = 100
    end
    object Datamodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object Dataactive: TBooleanField
      FieldName = 'active'
    end
    object Datamessage_frequency: TIntegerField
      FieldName = 'message_frequency'
    end
  end
end
