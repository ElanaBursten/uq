unit ChangePassword;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, cxdbEdit,ADODB;

type
  TChangePasswordForm = class(TForm)
    UserLoginLabel: TLabel;
    UserLogin: TLabel;
    Password1Label: TLabel;
    Password2Label: TLabel;
    Password1: TEdit;
    Password2: TEdit;
    OkBtn: TButton;
    CancelBtn: TButton;
    ProblemLabel: TLabel;
    lblExtra: TLabel;
    procedure OkBtnClick(Sender: TObject);
    procedure Password1Change(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FUserID: Integer;
    FisEmp: boolean; //QMANTWO-420
    FLogin: string;
    FFirstName: string;
    FLastName: string;
    FOldPasswordHash: string;
    chkboxpwd: TcxDBCheckBox;
    AData: TADODataset;
  protected
    procedure SetUserID(const ID: Integer);
  end;

procedure ChangeUserPassword(const UserID: Integer; const cbxpasswd: TcxDBCheckBox = nil; const ADataSet: TADODataset = nil);
//last two parameters used for a password sync checkbox

implementation

{$R *.dfm}

uses
  AdminDMu, ConfigDMu, PasswordRules, OdMiscUtils;

procedure ChangeUserPassword(const UserID: Integer; const cbxpasswd: TcxDBCheckBox = nil; const ADataSet: TADODataset = nil);
var
  Dialog: TChangePasswordForm;
begin
  Dialog := TChangePasswordForm.Create(nil);
  try
    Dialog.chkboxpwd := cbxpasswd;
    Dialog.AData := ADataSet;
    if (Dialog.FisEmp = True)  then
    Dialog.lblExtra.caption := 'Is Locator' else
    Dialog.lblExtra.caption :=  'Is NOT Locator';
    Dialog.SetUserID(UserID);
    Dialog.ShowModal;
  finally
    FreeAndNil(Dialog);
  end;
end;

procedure TChangePasswordForm.CancelBtnClick(Sender: TObject);
begin
  self.Close;
end;

procedure TChangePasswordForm.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Password1Change(self);
end;

procedure TChangePasswordForm.OkBtnClick(Sender: TObject);
var
  SQL: string;
  Hash: string;
  Problem: string;
  Count: Integer;
  ChangeStr: String;
  FSelectedID: Integer;
  pwdFlag: Boolean;
  Chgpwd: Integer;
begin
  if ModalResult=mrCancel then exit;  //QMANTWO-420

  Problem := '';
  if (FisEmp = True) then  // these are employees (hash is not case sensitive
  Hash := GeneratePasswordHash(FLogin, Password1.Text, False)
  else  // these are not employees (hash is case sensitive
  Hash := GeneratePasswordHash(FLogin, Password1.Text, True);
  if FOldPasswordHash = Hash then
    Problem := 'The new password cannot be the same as the old password'
  else
    PasswordMeetsRequirements(FLogin, FOldPasswordHash, Password1.Text, Problem, FFirstName, FLastName);
  ProblemLabel.Caption := Problem;
  if Problem <> '' then
    Exit;

  AdminDM.Conn.BeginTrans;
  try
    chgpwd := 0;
    pwdflag := false;
    if (chkboxpwd <> nil) and  (aData <> nil)
     and not chkboxpwd.Checked then
    If MessageDlg('Do you want to force the user to change his password at the next sync?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Chgpwd := 1;
      pwdflag := true;
    end;

    // Change the password
    SQL := 'update users set password = ' + QuotedStr(Hash) + ', chg_pwd = ' + inttostr(chgpwd) +
      ', chg_pwd_date = convert(char(10), dateadd(d, Convert(int, dbo.GetConfigValue(''PasswordExpirationDays'', ''120'')), getdate()), 102) ' +
      'where uid = ' + IntToStr(FUserID);
    AdminDM.Conn.Execute(SQL,Count);
    ModalResult := mrOk;
    if (Count <> 1) then
      MessageDlg('Error changing user password (Count was ' + IntToStr(Count) + ')',mtWarning,[mbOk],0);

    // Record password change in the user_password_history table
    SQL := 'insert user_password_history (uid, password_hash) values (' +
      IntToStr(FUserID) + ',' + QuotedStr(Hash) + ')';
    AdminDM.Conn.Execute(SQL);
    AdminDM.Conn.CommitTrans;

    if pwdflag then
    begin
      FSelectedID := AData.FieldbyName('emp_id').AsInteger;
      AData.Close;
      AData.Open;
      AData.Locate('emp_id', FSelectedID, []);
    end;

    ChangeStr := 'Password changed for User ID: ' + InttoStr(FUserID);  //QM-853
    AdminDM.AdminChangeTable('users', ChangeStr, now); //QM-823
  except
    on E:Exception do
      begin
        MessageDlg('Error changing user password' + #13 + E.Message + ' (' + E.ClassName + ')',mtWarning,[mbOk],0);
        AdminDM.Conn.RollbackTrans;
      end;
  end;

end;

procedure TChangePasswordForm.Password1Change(Sender: TObject);
var
  Problem: string;  //QMANTWO-417  this handler was move from the OnClick to onExit box exit.
begin
  PasswordMeetsRequirements(FLogin, FOldPasswordHash, Password1.Text, Problem, FFirstName, FLastName);
  if (Problem = '') and (Password1.Text <> Password2.Text) then
    Problem := 'The new password must be entered twice exactly the same';

  ProblemLabel.Caption := Problem;
  OkBtn.Enabled := NotEmpty(Password1.Text) and (Password1.Text = Password2.Text) and (Problem='');
end;

procedure TChangePasswordForm.SetUserID(const ID: Integer);
const   //QMANTWO-417  changed from inner to LOJ
  Select = 'select e.first_name, e.last_name, u.login_id, u.emp_id, ' +
    'u.password from users u left outer join employee e on (e.emp_id=u.emp_id) where uid = ';
var
  Query: TDataSet;
begin
  try
    Query := AdminDM.ConfigDM.CreateDataSetForSQL(Select + IntToStr(ID));
    try
      Assert(Query.RecordCount = 1, Format('Expected one user with uid=%d, but found % users', [IntToStr(ID), Query.RecordCount]));
      FUserID := ID;
      FisEmp := not(Query.FieldByName('emp_id').IsNull);  //QMANTWO-420
      FLogin := Query.FieldByName('login_id').AsString;
      FFirstName := Query.FieldByName('first_name').AsString;
      FLastName := Query.FieldByName('last_name').AsString;
      FOldPasswordHash := Query.FieldByName('password').AsString;
      UserLogin.Caption := FLogin;
      if (FisEmp = True)  then     //QMANTWO-420
      lblExtra.caption := 'Is a Locator' else     //QMANTWO-420
      lblExtra.caption :=  'Is NOT a Locator';   //QMANTWO-420
    finally
      FreeAndNil(Query);
    end;
  except
    on E: Exception do begin
      MessageDlg('Unable to locate user to change password' + #13 + E.Message + ' (' + E.ClassName + ')', mtWarning, [mbOk], 0);
      Abort;
    end;
  end;
end;

end.

