inherited RequiredDocumentsForm: TRequiredDocumentsForm
  Caption = 'Required Documents'
  ClientHeight = 500
  ClientWidth = 923
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 557
    Top = 35
    Height = 465
  end
  inherited HeaderPanel: TPanel
    Width = 923
    Height = 35
    BevelOuter = bvNone
  end
  inherited MasterPanel: TPanel
    Top = 35
    Width = 557
    Height = 465
    BevelOuter = bvNone
    inherited MasterGrid: TcxGrid
      Width = 541
      Height = 414
      inherited MasterGridView: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        OnCustomDrawCell = MasterGridViewCustomDrawCell
        DataController.KeyFieldNames = 'required_doc_id'
        OptionsData.Appending = True
        OptionsView.HeaderAutoHeight = True
        OptionsView.HeaderHeight = 36
        object ColMasterForeignType: TcxGridDBColumn
          Caption = 'Type'
          DataBinding.FieldName = 'foreign_type'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Visible = False
          Width = 35
        end
        object ColMasterName: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'name'
          Width = 198
        end
        object ColMasterRequireForLocate: TcxGridDBColumn
          Caption = 'Required For Locate'
          DataBinding.FieldName = 'require_for_locate'
          Width = 79
        end
        object ColMasterRequireForNoLocate: TcxGridDBColumn
          Caption = 'Required For No Locate'
          DataBinding.FieldName = 'require_for_no_locate'
          Width = 96
        end
        object ColMasterRequireForMinEstimate: TcxGridDBColumn
          Caption = 'Min. Estimate'
          DataBinding.FieldName = 'require_for_min_estimate'
          FooterAlignmentHorz = taRightJustify
          HeaderAlignmentHorz = taRightJustify
          Width = 57
        end
        object ColMasterUserSelectable: TcxGridDBColumn
          Caption = 'Can Pick'
          DataBinding.FieldName = 'user_selectable'
          Width = 50
        end
        object ColMasterActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          Options.Filtering = False
          Width = 51
        end
      end
    end
    object cbRequiredDocsSearch: TCheckBox
      Left = 189
      Top = 19
      Width = 99
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 3
      OnClick = cbRequiredDocsSearchClick
    end
    object ActiveRequiredDocsFilter: TCheckBox
      Left = 308
      Top = 18
      Width = 166
      Height = 14
      Caption = 'Active Documents Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      OnClick = ActiveRequiredDocsFilterClick
    end
  end
  inherited DetailPanel: TPanel
    Left = 566
    Top = 35
    Width = 357
    Height = 465
    BevelOuter = bvNone
    inherited DetailGrid: TcxGrid
      Top = 99
      Width = 347
      Height = 414
      inherited DetailGridView: TcxGridDBTableView
        DataController.KeyFieldNames = 'rdt_id'
        OptionsView.GroupByBox = False
        object ColDetailDocTypeName: TcxGridDBColumn
          Caption = 'Doc Type'
          DataBinding.FieldName = 'doc_type_name'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownRows = 16
          Properties.ListColumns = <
            item
              FieldName = 'description'
            end>
          Properties.ListSource = DocTypeRefSource
          MinWidth = 120
          Width = 120
        end
        object ColDetailActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          Width = 40
        end
      end
    end
    object ActiveRequiredDocTypesFilter: TCheckBox [2]
      Left = 189
      Top = 18
      Width = 166
      Height = 14
      Caption = 'Active Doc Types Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      OnClick = ActiveRequiredDocTypesFilterClick
    end
    inherited DetailDeleteButton: TButton
      TabOrder = 3
    end
  end
  inherited Master: TADODataSet
    CursorType = ctStatic
    BeforeInsert = MasterBeforeInsert
    BeforeEdit = MasterBeforeEdit
    AfterPost = MasterAfterPost
    AfterCancel = MasterAfterCancel
    OnNewRecord = MasterNewRecord
    CommandText = 'select * from required_document where active = 1'#13#10'order by name'
    Left = 24
    Top = 232
  end
  inherited Detail: TADODataSet
    CursorType = ctStatic
    BeforeInsert = DetailBeforeInsert
    BeforeEdit = DetailBeforeEdit
    AfterPost = DetailAfterPost
    AfterCancel = DetailAfterCancel
    OnNewRecord = DetailNewRecord
    CommandText = 
      'select * from required_document_type where active = 1'#13#10'order by ' +
      'doc_type'
    IndexFieldNames = 'required_doc_id'
    MasterFields = 'required_doc_id'
    Left = 16
    Top = 296
  end
  inherited MasterSource: TDataSource
    Top = 232
  end
  inherited DetailSource: TDataSource
    Left = 76
    Top = 296
  end
  object DocTypeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select * from reference '#13#10'where type='#39'doctype'#39' and active_ind=1 ' +
      #13#10'order by sortby, description '
    Parameters = <>
    Left = 32
    Top = 168
  end
  object DocTypeRefSource: TDataSource
    AutoEdit = False
    DataSet = DocTypeRef
    Left = 96
    Top = 168
  end
end
