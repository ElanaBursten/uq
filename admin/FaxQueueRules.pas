unit FaxQueueRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxBlobEdit, EmailQueueRules, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TFaxQueueRulesForm = class(TEmailQueueRulesForm)
    ColSendType: TcxGridDBColumn;
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  end;

implementation

uses AdminDMu, OdCxUtils;

{$R *.dfm}

procedure TFaxQueueRulesForm.DataAfterPost(DataSet: TDataSet);
begin
   AdminDM.TrackAdminChanges(Dataset, ATableArray, 'fax_queue_rules', 'call_center', emqRulesInserted);
end;

procedure TFaxQueueRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('send_type').Value := 'FAXMAKER';
end;

end.
