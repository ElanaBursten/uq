inherited frmClientRespondTo: TfrmClientRespondTo
  Caption = 'Client Respond To'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 25
    object chkboxRespondToSearch: TCheckBox
      Left = 15
      Top = 7
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxRespondToSearchClick
    end
    object ActiveRespondToFilter: TCheckBox
      Left = 160
      Top = 7
      Width = 161
      Height = 13
      Caption = 'Active RespondTo Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveRespondToFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.KeyFieldNames = 'client_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColClientID: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = ClientLookupView
        Properties.KeyFieldNames = 'client_id'
        Properties.ListFieldItem = ColClientLookupClientName
        Properties.ReadOnly = False
        Width = 178
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client Code'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Width = 95
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status Name'
        DataBinding.FieldName = 'client_respond_to'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = StatusLookupView
        Properties.KeyFieldNames = 'sg_name'
        Properties.ListFieldItem = ColStatusLookupName
        Properties.ReadOnly = False
        Width = 158
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Width = 69
      end
    end
    object ClientLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = ClientLookupDS
      DataController.KeyFieldNames = 'client_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object ColClientLookupClientName: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_name'
      end
      object ColClientLookupOCCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'oc_code'
        Options.Editing = False
      end
    end
    object StatusLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = StatusLookupDS
      DataController.KeyFieldNames = 'sg_name'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsView.GroupByBox = False
      object ColStatusLookupName: TcxGridDBColumn
        Caption = 'sg name'
        DataBinding.FieldName = 'sg_name'
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforePost = DataBeforePost
    OnNewRecord = DataNewRecord
    CommandText = 
      'Select * from client_respond_to where active = 1 order by client' +
      '_code'
    Left = 48
  end
  object ClientLookupDS: TDataSource
    AutoEdit = False
    DataSet = ClientLookup
    Left = 480
    Top = 65
  end
  object ClientLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select client_id, client_name, oc_code from client order by clie' +
      'nt_id'
    Parameters = <>
    Left = 392
    Top = 64
  end
  object StatusLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select sg_name from status_group'
    Parameters = <>
    Left = 392
    Top = 112
  end
  object StatusLookupDS: TDataSource
    AutoEdit = False
    DataSet = StatusLookup
    Left = 480
    Top = 113
  end
end
