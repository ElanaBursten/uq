object AreaLocatorFrame: TAreaLocatorFrame
  Left = 0
  Top = 0
  Width = 770
  Height = 189
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  DesignSize = (
    770
    189)
  object Label3: TLabel
    Left = 8
    Top = 5
    Width = 32
    Height = 13
    Caption = 'Areas:'
  end
  object Label4: TLabel
    Left = 520
    Top = 8
    Width = 45
    Height = 13
    Caption = 'Locators:'
  end
  object AssignToLocatorBtn: TBitBtn
    Left = 352
    Top = 80
    Width = 161
    Height = 25
    Caption = '<< Assign area to locator >>'
    TabOrder = 0
    OnClick = AssignToLocatorBtnClick
  end
  object LocatorGrid: TDBGrid
    Left = 520
    Top = 24
    Width = 225
    Height = 161
    Anchors = [akLeft, akTop, akBottom]
    DataSource = locatorDs
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'short_name'
        Title.Caption = 'Locator Name'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'emp_number'
        Title.Caption = 'Emp #'
        Width = 70
        Visible = True
      end>
  end
  object areaGrid: TDBGrid
    Left = 8
    Top = 21
    Width = 329
    Height = 164
    Anchors = [akLeft, akTop, akBottom]
    DataSource = areaDS
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Area Name'
        Width = 97
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'short_name'
        ReadOnly = True
        Title.Caption = 'Locator'
        Width = 93
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'emp_number'
        ReadOnly = True
        Title.Caption = 'Emp #'
        Visible = True
      end>
  end
  object areaDS: TDataSource
    Left = 8
    Top = 64
  end
  object locatorDs: TDataSource
    Left = 8
    Top = 96
  end
end
