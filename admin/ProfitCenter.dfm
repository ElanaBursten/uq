inherited ProfitCenterForm: TProfitCenterForm
  Left = 273
  Top = 209
  Caption = 'Profit Centers'
  ClientWidth = 1036
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 1036
    Height = 25
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1036
      Height = 22
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object cbProfitCenterSearch: TCheckBox
        Left = 29
        Top = 5
        Width = 99
        Height = 12
        Caption = 'Show Find Panel'
        TabOrder = 0
        OnClick = cbProfitCenterSearchClick
      end
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 1036
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'pc_code'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.HeaderEndEllipsis = True
      object PCCodeColumn: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'pc_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = False
        Width = 70
      end
      object PCNameColumn: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'pc_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 177
      end
      object PCCityColumn: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'pc_city'
        Width = 128
      end
      object GLCodeColumn: TcxGridDBColumn
        Caption = 'GL Code'
        DataBinding.FieldName = 'gl_code'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 115
      end
      object TimesheetNumColumn: TcxGridDBColumn
        Caption = 'Solomon TS Num'
        DataBinding.FieldName = 'timesheet_num'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 98
      end
      object ADPCodeColumn: TcxGridDBColumn
        Caption = 'ADP Code'
        DataBinding.FieldName = 'adp_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 65
      end
      object CompanyLookupColumn: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'CompanyLookup'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'company_id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListOptions.GridLines = glNone
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = CompanyRefDS
        Width = 99
      end
      object BillingContactColumn: TcxGridDBColumn
        Caption = 'Billing Contact'
        DataBinding.FieldName = 'billing_contact'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.ScrollBars = ssVertical
        Width = 107
      end
      object ManagerLookupColumn: TcxGridDBColumn
        Caption = 'Manager'
        DataBinding.FieldName = 'ManagerLookup'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListColumns = <
          item
            FieldName = 'short_name'
          end>
        Properties.ListOptions.GridLines = glNone
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = ManagerRefDS
        Width = 102
      end
      object SolomonPCColumn: TcxGridDBColumn
        Caption = 'Solomon PC'
        DataBinding.FieldName = 'solomon_pc'
        Width = 90
      end
    end
    object CompanyLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      FilterBox.Visible = fvNever
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = CompanyRefDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'company_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object CompanyViewCompanyColumn: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'name'
      end
    end
    object ManagerLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      FilterBox.Visible = fvNever
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = ManagerRefDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ManagerViewManagerColumn: TcxGridDBColumn
        Caption = 'Manager'
        DataBinding.FieldName = 'short_name'
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select *'#13#10'from profit_center'#13#10'order by 1'
  end
  object CompanyRefDS: TDataSource
    DataSet = CompanyRef
    Left = 56
    Top = 272
  end
  object CompanyRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select company_id, name'#13#10'from locating_company'#13#10'order by name'
    Parameters = <>
    Left = 64
    Top = 216
  end
  object ManagerRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select h_emp_id as emp_id, h_short_name as short_name from dbo.g' +
      'et_report_hier3('#13#10'   (select max(emp_id) from employee where rep' +
      'ort_to is null and active = 1), 1, 1000, 1)'#13#10
    Parameters = <>
    Left = 144
    Top = 208
  end
  object ManagerRefDS: TDataSource
    DataSet = ManagerRef
    Left = 136
    Top = 264
  end
end
