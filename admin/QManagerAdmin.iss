; Use Inno Setup 5.0.8+
#define Version GetEnv("VERSION")
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager Admin
AppVerName=UtiliQuest Q Manager Admin {#Version}
AppID=UtiliQuestQManagerAdmin
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\Q Manager
DefaultGroupName=UtiliQuest
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/                                                     
AppVersion={#Version}{#GitCommitIDWithDash}
AppMutex=UtiliQuestQManagerAdmin
DisableProgramGroupPage=yes
OutputDir=..\build
OutputBaseFilename=QManagerAdminSetup-{#Version}{#GitCommitIDWithDash}
PrivilegesRequired=none
OutputManifestFile=QManagerAdminSetup-Manifest.txt

[Files]
Source: QManagerAdmin.exe; DestDir: {app}; Flags: ignoreversion; Components: QMAdmin; 
Source: QManagerAdmin.ini; DestDir: {app}; DestName: QManagerAdmin.ini; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 
Source: mySavedQueries.xml; DestDir: {app}; DestName: mySavedQueries.xml; Flags: ignoreversion uninsneveruninstall; Components: QMAdmin; 
Source: FrontEndsDev.HTML; DestDir: {app}; DestName: FrontEndsDev.HTML; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 
Source: FrontEndsProd.HTML; DestDir: {app}; DestName: FrontEndsProd.HTML; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 
Source: "..\reportengine\ReportEngineCGI.exe"; DestDir: {app}; Flags: ignoreversion; Components: QMBilling eFax ReportEngine;
Source: "..\reportengine\sample_ReportEngineCGI.ini"; DestDir: {app}; DestName: "ReportEngineCGI.ini"; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMBilling eFax ReportEngine; 
Source: "..\XE10Billing\QManagerBilling.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: QMBilling; 
Source: "..\XE10Billing\Templates\*.rtm"; DestDir: "{app}\Templates"; Flags: ignoreversion; Components: QMBilling; 
Source: "..\XE10Billing\Logos\*.*"; DestDir: "{app}\Logos"; Flags: ignoreversion; Components: QMBilling; 
Source: "..\XE10Billing\QManagerBilling.ini"; DestDir: {app}; DestName: "QManagerBilling.ini"; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMBilling; 
//Source: "..\TransParser\TrannyParser.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TrannyParser; 
Source: "..\QTip\QTip.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: QTip; 
Source: "..\FootLocker\FootLocker.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: FootLocker; 
Source: "..\PAL\PALprj.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: PAL; 
Source: "..\PAL\DycomPeriod\DycomPeriod.xlsx"; DestDir: "{app}\DycomPeriod"; Flags: onlyifdoesntexist uninsneveruninstall; Components: PAL;

Source: "..\ACE\ACE.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: ACE;  
Source: "..\ACE\ACE.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: ACE;  

Source: "..\Airwatch\AirQ.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AirQ;  
Source: "..\Airwatch\AirWatch.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: AirQ;  

Source: "..\tools\AboutTimeShifts\AboutTimeShifts.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AboutTimeShifts;  
Source: "..\tools\AboutTimeShifts\AboutTimeShifts.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: AboutTimeShifts;  
Source: "..\tools\AboutTimeShifts\AboutTimeShifts.bat"; DestDir: "{app}"; Flags: ignoreversion; Components: AboutTimeShifts; 

Source: "..\tools\TSEApprovalQueue\TSEApprovalQueue.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TSEApproval;  
Source: "..\tools\TSEApprovalQueue\TSEApprovalQueue.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: TSEApproval;  
Source: "..\tools\TSEApprovalQueue\TSEApprovalQueue.bat"; DestDir: "{app}"; Flags: ignoreversion; Components: TSEApproval; 

Source: "..\tools\BillingRateImport\RatersGotToRate.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: RatersGotToRate;  
Source: "..\tools\BillingRateImport\RatersGotToRate.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: RatersGotToRate;  

Source: "..\tools\AboutTime\AboutTime.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AboutTime;  
Source: "..\tools\AboutTime\AboutTime.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: AboutTime; 

Source: "..\tools\BigFoot\BigFoot.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: BigFoot;  
Source: "..\tools\BigFoot\BigFoot.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: BigFoot; 
Source: "..\tools\BigFoot\BigFoot.bat"; DestDir: "{app}"; Flags: ignoreversion; Components: BigFoot; 

Source: "..\tools\EmployeeType\BusyBodies.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: BusyBodies;  
Source: "..\tools\EmployeeType\BusyBodies.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: BusyBodies; 

Source: "..\tools\UrBintResponder\UrbintResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: UrbintResp;  
Source: "..\tools\UrBintResponder\UrbintResp.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: UrbintResp;  

Source: "..\tools\AttWebServiceResponder\AttWebServiceResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AttWebResponder;  
Source: "..\tools\AttWebServiceResponder\AttWebServiceResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: AttWebResponder;  
Source: "..\tools\AttWebServiceResponder\RunAT&TWebService.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: AttWebResponder; 

Source: "..\tools\AttWebResponderCal\AttCalWebResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AttWebResponderCal;  
Source: "..\tools\AttWebResponderCal\AttCalWebResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: AttWebResponderCal;  
Source: "..\tools\AttWebResponderCal\RunCalAT&TWebService.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: AttWebResponderCal; 

Source: "..\tools\Boss811Responder\Boss811RespPrj.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: Boss811Responder;  
Source: "..\tools\Boss811Responder\Boss811RespPrj.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: Boss811Responder;  
Source: "..\tools\Boss811Responder\RunBoss811Resp.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: Boss811Responder; 

Source: "..\tools\BossUniversalResponder\Boss811UniResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: Boss811UniResponder;  
Source: "..\tools\BossUniversalResponder\Boss811UniResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: Boss811UniResponder;  
Source: "..\tools\BossUniversalResponder\RunBoss811UniResp.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: Boss811UniResponder; 

Source: "..\tools\FlAttAlcs\FL_ATT_ALCS_Resp.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: FlAttAlcsResp;  
Source: "..\tools\FlAttAlcs\FL_ATT_ALCS_Resp.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: FlAttAlcsResp;  
Source: "..\tools\FlAttAlcs\FL_ATT_ALCS.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: FlAttAlcsResp; 

Source: "..\tools\SunshineResponder\SunshineResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: SunshineResp;  
Source: "..\tools\SunshineResponder\SunshineResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: SunshineResp;  
Source: "..\tools\SunshineResponder\SunshineResponder.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: SunshineResp; 

Source: "..\tools\UrbintUniversalResponder\UrbintUniResponder.exe"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: UrbintUniResponder; 
Source: "..\tools\UrbintUniversalResponder\UrbintUniResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: UrbintUniResponder; 

Source: "..\tools\UrbintUniversalResponderVA\UrbintUniResponderVA.exe"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: UrbintUniResponderVA; 
Source: "..\tools\UrbintUniversalResponderVA\UrbintUniResponderVA.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: UrbintUniResponderVA; 

Source: "..\tools\EmergencyReQue\EmergencyReQue.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: EmerRequeue;  
Source: "..\tools\EmergencyReQue\EmerRequeue.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmerRequeue;  

Source: "..\tools\EmergencyAlerter\EmergencyAlert.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: EmerAlert;  
Source: "..\tools\EmergencyAlerter\EmergencyAlert.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmerAlert; 
Source: "..\tools\EmergencyAlerter\EmergencyAlert.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmerAlert; 
Source: "..\tools\EmergencyAlerter\libeay32.dll"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmerAlert; 
Source: "..\tools\EmergencyAlerter\ssleay32.dll"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmerAlert; 

Source: "..\tools\PTOSickBalance\PTOSickBalanceImport.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: PTOSickBalanceImport;  
Source: "..\tools\PTOSickBalance\PTOSickBalance.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: PTOSickBalanceImport;  

Source: "..\tools\TwentyTwoAndMe\TwentyTwoAndMe.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TwentyTwoAndMe;  
Source: "..\tools\TwentyTwoAndMe\TwentyTwoAndMe.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TwentyTwoAndMe;  
Source: "..\tools\TwentyTwoAndMe\WinSCP.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TwentyTwoAndMe;  

Source: "..\tools\TicketAlert\TicketAlert.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TicketAlert;  
Source: "..\tools\TicketAlert\TicketAlert.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TicketAlert;  
Source: "..\tools\TicketAlert\TicketAlert.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TicketAlert;  



Source: "..\TicketRerouter\Rerouter.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TicketRouter;  
Source: "..\TicketRerouter\Rerouter.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TicketRouter;  

//Source: "..\ImageMoverAWS\ImageMover.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: ImageMover; 
//Source: "..\ImageMoverAWS\imageMover.ini"; DestDir: "{app}"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: ImageMover; 
//Source: "..\ImageMoverAWS\adoConnQM.udl"; DestDir: "{app}"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: ImageMover; 
//Source: "..\ImageMoverAWS\eprCertusViewConn.ini"; DestDir: "{app}"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: ImageMover; 
//Source: "..\ImageMoverAWS\ImageMover.Bat"; DestDir: "{app}"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: ImageMover; 

//Source: "..\DamageRecovery\DRAT.exe"; DestDir: "{app}\DamageRecovery"; Flags: ignoreversion; Components: DRAT; 
//Source: "..\DamageRecovery\libeay32.dll"; DestDir: "{app}\DamageRecovery"; Flags: ignoreversion; Components: DRAT; 
//Source: "..\DamageRecovery\ssleay32.dll"; DestDir: "{app}\DamageRecovery"; Flags: ignoreversion; Components: DRAT; 

//Source: "..\SharedSecureAttachments\DSSS.DLL"; DestDir: "{app}\DamageRecovery"; Flags: ignoreversion; Components: DRAT; 
//Source: "..\DamageRecovery\damageCRM.ini"; DestDir: "{app}\DamageRecovery"; Flags:  ignoreversion uninsneveruninstall; Components: DRAT; 
//Source: "..\DamageRecovery\damageCRMLocQM.ini"; DestDir: "{app}\DamageRecovery"; Flags:  ignoreversion uninsneveruninstall; Components: DRAT; 

//Source: "..\DamageRecovery\RunDRAT_GUI.bat"; DestDir: "{app}\DamageRecovery"; Flags:  ignoreversion uninsneveruninstall; Components: DRAT; 
//Source: "..\DamageRecovery\RunDRAT_QM.bat"; DestDir: "{app}\DamageRecovery"; Flags:  ignoreversion uninsneveruninstall; Components: DRAT; 

//Source: "..\DamageRecovery\eprCertusViewConn.ini"; DestDir: "{app}\DamageRecovery"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: DRAT; 
//Source: "..\DamageRecovery\winscp\WinSCP.bat"; DestDir: "{app}\DamageRecovery\WinSCP"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: DRAT; 
//Source: "..\DamageRecovery\winscp\WinSCP.exe"; DestDir: "{app}\DamageRecovery\WinSCP"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: DRAT; 
//Source: "..\SharedSecureAttachments\DSSS.DLL"; DestDir: "{app}\DamageRecovery\WinSCP"; Flags: ignoreversion; Components: DRAT; 
//Source: "..\DamageRecovery\winscp\WinSCP.ini"; DestDir: "{app}\DamageRecovery\WinSCP"; Flags:  onlyifdoesntexist uninsneveruninstall; Components: DRAT; 
//Source: "..\DamageRecovery\ICONS\*.*";         DestDir: "{app}\DamageRecovery\ICONS";   Flags:  onlyifdoesntexist uninsneveruninstall; Components: DRAT; 

Source: "..\AutoClose\AutoClose.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: AutoClose;  
Source: "..\AutoClose\AutoClose.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: AutoClose;  

Source: "..\ThrillTheBill\Wedge.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: BillingWedge; 
Source: "..\ThrillTheBill\billing_RunAuto.bat"; DestDir: "{app}"; Flags: ignoreversion; Components: BillingWedge; 

Source: "..\tools\ScannaWorkers\ScannaWorker.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: ScannaWorkerImport; 
Source: "..\tools\ScannaWorkers\ScannaWorker.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: ScannaWorkerImport; 

Source: "..\EmerViewer\EmerViewer.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: EmergencyView; 
 
Source: "..\tools\HAL\HalSentinel.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: HalSentinel;  
Source: "..\tools\HAL\HALSentinel.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: HalSentinel;  

Source: "..\tools\EmployeesAreGood\EmployeesAreGood.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: EmployeesAreGood;  
Source: "..\tools\EmployeesAreGood\EmployeesAreGood.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: EmployeesAreGood;  

Source: "..\tools\UTAimport\UTAJammer.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: UTAJammer;  
Source: "..\tools\UTAimport\UTAJammer.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: UTAJammer;  

Source: "..\tools\Pelican\PelicanResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: PeliCan;  
Source: "..\tools\Pelican\PelicanResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: PeliCan; 
Source: "..\tools\Pelican\RunPelicanResp.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: PeliCan;  


Source: "..\tools\Maximo\Maximo.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: Maximo;  
Source: "..\tools\Maximo\Maximo.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: Maximo;  

Source: "..\tools\ParForTheCourse\ParForTheCourse.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: ParForTheCourse;  
Source: "..\tools\ParForTheCourse\ParForTheCourse.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: ParForTheCourse; 
Source: "..\tools\ParForTheCourse\ParForTheCourse.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: ParForTheCourse;


Source: "..\tools\DiGTiX\DigTixResponder.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: DigTix;  
Source: "..\tools\DiGTiX\DigTixResponder.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: DigTix; 
Source: "..\tools\DiGTiX\DigTixResponder.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: DigTix;


Source: "..\tools\TicketPuraImport\TicketPURAimporter.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: TicketPuraImport;  
Source: "..\tools\TicketPuraImport\TicketPURAimporter.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist; Components: TicketPuraImport;  

Source: "..\tools\TicketPSID_Importer\TicketPSIDimporter.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: CGOSVC; 
Source: "..\tools\TicketPSID_Importer\TicketPSIDimporter.ini"; DestDir: "{app}"; Flags: ignoreversion; Components: CGOSVC; 
Source: "..\tools\TicketPSID_Importer\OH_Counties.txt"; DestDir: "{app}"; Flags: ignoreversion; Components: CGOSVC;
Source: "..\tools\TicketPSID_Importer\PSIDsWithoutTapCards 11-29-2022.txt"; DestDir: "{app}"; Flags: ignoreversion; Components: CGOSVC;
 
Source: "..\tools\TicketPSID_Importer\Geocode.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: CGOSVC; 

Source: "..\tools\libeay32.dll";DestDir: {app}; Flags: ignoreversion;  
Source: "..\tools\ssleay32.dll";DestDir: {app}; Flags: ignoreversion;  
Source: "..\tools\openssl.exe";DestDir: {app}; Flags: ignoreversion;  

; Developer Express (non-official, consolidated Version 12.2.4 package)
;Source: "..\lib\DevExDXE3RT1224.bpl"; DestDir: "{sys}"; Flags: sharedfile

;eFax API files 
Source: ..\lib\eFaxDeveloper.dll; DestDir: {sys}; Flags: sharedfile gacinstall; StrongAssemblyName: "eFaxDeveloper, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b715e7d32ebddfc8WW0, ProcessorArchitecture=MSIL"; Components: eFax; 
Source: ..\lib\eFaxDeveloper.tlb; DestDir: {sys}; Flags: sharedfile; Components: eFax; 

[Icons]
Name: "{group}\Q Manager Admin";   Filename: "{app}\QManagerAdmin.exe"; Components: QMAdmin
Name: "{group}\Q Manager Billing"; Filename: "{app}\QManagerBilling.exe"; Components: QMBilling

[Run]
Filename: "{app}\QManagerAdmin.exe"; Description: "Start Q Manager Admin Now"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent unchecked; Components: QMAdmin
;deRegister old eFax API
Filename: "{dotnet20}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb /u"; Flags: nowait
;Register eFax API
Filename: "{dotnet40}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb"; Description: "Register eFax API"; Flags: nowait; Components: eFax

[UninstallRun]
;Unregister eFax API
Filename: "{dotnet20}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb /u"; Flags: nowait

[Components]
Name: QMAdmin; Description: "Q Manager Admin"; Flags: checkablealone; Types: full
Name: QMBilling; Description: "Q Manager Billing"; Flags: checkablealone; Types: full
//Name: TrannyParser; Description: "Tranny Parser"; Flags: checkablealone; Types: full
Name: QTip; Description: "Q Ticket in Polygon"; Flags: checkablealone; Types: full
Name: FootLocker; Description: "Weekly Parser for OHM Report"; Flags: checkablealone; Types: full
Name: PAL; Description: "Payroll Auto Loader"; Flags: checkablealone; Types: full
Name: ACE; Description: "Asset Cost Exporter"; Flags:  checkablealone; Types: full
Name: eFax; Description: "eFax Outbound Faxing Support"; Flags: checkablealone; Types: full
Name: ReportEngine; Description: "Report Engine"; Flags: checkablealone; Types: full
Name: AboutTime; Description: "rTasq Time Importer"; Flags: checkablealone; Types: full
Name: BigFoot; Description: "Bigfoot to process footage"; Flags: checkablealone; Types: full
Name: AboutTimeShifts; Description: "rTasq Approval Time Importer"; Flags: checkablealone; Types: full
Name: ScannaWorkerImport; Description: "Imports Scanna DOM Spreadsheet"; Flags: checkablealone; Types: full
Name: AutoClose; Description: "Auto Close"; Flags: checkablealone; Types: full
Name: EmergencyView; Description: "Emergency Viewer"; Flags: checkablealone; Types: full
Name: AirQ; Description: "AirQ Phone Importer"; Flags:  checkablealone; Types: full
Name: TicketRouter; Description: "Custom Ticket Router"; Flags:  checkablealone; Types: full
Name: BillingWedge; Description: "Run Billing From Batch"; Flags:  checkablealone; Types: full
//Name: NiSourceMover; Description: "NiSource Data Mover"; Flags:  checkablealone; Types: full     
Name: UrbintResp; Description: "Urbint Responder"; Flags:  checkablealone; Types: full
Name: EmerRequeue; Description: "Emergency ReQueue"; Flags:  checkablealone; Types: full
Name: EmerAlert; Description: "Emergency Alerter"; Flags:  checkablealone; Types: full
Name: TwentyTwoAndMe; Description: "sFTP Plat Sync"; Flags:  checkablealone; Types: full
Name: AttWebResponder; Description: "AT&T Web Responder"; Flags:  checkablealone; Types: full
Name: AttWebResponderCal; Description: "AT&T Web Responder for Cal"; Flags:  checkablealone; Types: full      
Name: Boss811Responder; Description: "Boss811 Web Responder"; Flags:  checkablealone; Types: full
Name: Boss811UniResponder; Description: "Boss811 Universal Responder"; Flags:  checkablealone; Types: full
Name: HalSentinel; Description: "HAL Sentinel Service"; Flags:  checkablealone; Types: full
Name: EmployeesAreGood; Description: "Verify Employee Data"; Flags:  checkablealone; Types: full
Name: PTOSickBalanceImport; Description: "Importer for Sick and PTO data"; Flags:  checkablealone; Types: full
Name: UTAJammer; Description: "Jammin & Slammin Time in TimeSheets"; Flags:  checkablealone; Types: full
Name: PeliCan; Description: "PeliCan if you can"; Flags:  checkablealone; Types: full
Name: TicketPuraImport; Description: "Imports PURA data into tickets"; Flags:  checkablealone; Types: full
Name: UrbintUniResponder; Description: "Urbint Universal Responder"; Flags:  checkablealone; Types: full
Name: UrbintUniResponderVA; Description: "Urbint Universal Responder for VA"; Flags:  checkablealone; Types: full
Name: FlAttAlcsResp; Description: "Florida ATT Responder";  Flags:  checkablealone; Types: full
Name: SunshineResp; Description: "Florida Sunshine Responder";  Flags:  checkablealone; Types: full
Name: ParForTheCourse; Description: "Par For The Course";  Flags:  checkablealone; Types: full
Name: CGOSVC; Description: "Importer for PSIDs w/o Tap";  Flags:  checkablealone; Types: full
Name: Maximo; Description: "Welcome to Maximo's";  Flags:  checkablealone; Types: full
Name: TicketAlert; Description: "Ticket Alert Pump";  Flags:  checkablealone; Types: full
Name: DigTix; Description: "DigTix New Jersey Responder";  Flags:  checkablealone; Types: full      
Name: TSEApproval; Description: "Puts TSE Timesheets in RabbitMQOut";  Flags:  checkablealone; Types: full
Name: RatersGotToRate; Description: "Imports Rates into Billing Rates";  Flags:  checkablealone; Types: full      
Name: BusyBodies; Description: "Correct Employee types";  Flags:  checkablealone; Types: full

