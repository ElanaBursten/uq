unit BaseCxList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, Grids, DBGrids, DB, ADODB, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridLevel, cxDropDownEdit, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, 
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore, dxDateRanges, dxScrollbarAnnotations, dxSkinBasic,
  dxCore, dxSkinsForm, dxSkinDevExpressStyle;

type
  TBaseCxListForm = class(TEmbeddableForm)
    TopPanel: TPanel;
    Data: TADODataSet;
    DS: TDataSource;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    GridView: TcxGridDBTableView;
    dxSkinController2: TdxSkinController;
    procedure FormShow(Sender: TObject);
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure GridExit(Sender: TObject);
  public
    procedure LeavingNow; override;
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

implementation

{$R *.dfm}

uses
  OdDbUtils;

{ TBaseListForm }

procedure TBaseCxListForm.LeavingNow;
begin
  inherited;
  if Data.State in dsEditModes then
    Data.Post;
end;

procedure TBaseCxListForm.FormShow(Sender: TObject);
begin
  inherited;
  if TopPanel.ControlCount = 0 then
    TopPanel.Hide;
  OpenDataSets;
end;

procedure TBaseCxListForm.GridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(Data);
end;

procedure TBaseCxListForm.DataBeforeDelete(DataSet: TDataSet);
var
  Field: TField;
begin
  inherited;
  Field := DataSet.FindField('Active');
  if Assigned(Field) then begin
    DataSet.Edit;
    Field.AsBoolean := False;
    DataSet.Post;
    RefreshNow;
  end;
  if Assigned(Field) then Abort;  //QM-96
end;

procedure TBaseCxListForm.CloseDataSets;
begin
  inherited;
  Data.Close;
end;

procedure TBaseCxListForm.OpenDataSets;
begin
  inherited;
  if Data.CommandText <> '' then
    Data.Open;
end;

procedure TBaseCxListForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TBaseCxListForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  if Grid.CanFocus then
    Grid.SetFocus;
end;

end.

