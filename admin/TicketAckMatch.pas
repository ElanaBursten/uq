unit TicketAckMatch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxDBExtLookupComboBox, cxMaskEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxDropDownEdit, ExtCtrls,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  Vcl.StdCtrls, dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  TTicketAckMatchForm = class(TBaseCxListForm)
    WorkPriorityRef: TADODataSet;
    WorkPriorityRefDS: TDataSource;
    ColTicketAckMatchID: TcxGridDBColumn;
    ColCallCenter: TcxGridDBColumn;
    ColMatchString: TcxGridDBColumn;
    ColWorkPriority: TcxGridDBColumn;
    WorkPriorityLookupView: TcxGridDBTableView;
    WorkPriorityLookupViewcode: TcxGridDBColumn;
    WorkPriorityLookupViewdescription: TcxGridDBColumn;
    WorkPriorityLookupViewref_id: TcxGridDBColumn;
    chkboxTicketSearch: TCheckBox;
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure chkboxTicketSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
  private
    TTMInserted: Boolean;
    procedure PopulateCallCenterList;
  public
    procedure PopulateDropdowns; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TTicketAckMatchForm }

procedure TTicketAckMatchForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('WorkPriority', Data, 'work_priority', WorkPriorityRef, 'ref_id', 'description');
    //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
    Data.FieldByName('WorkPriority').LookupCache := True;
end;

procedure TTicketAckMatchForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TTicketAckMatchForm.OpenDataSets;
begin
  WorkPriorityRef.Open;
  inherited;
end;

procedure TTicketAckMatchForm.ActivatingNow;
begin
  inherited;
   TTMInserted := False;
end;

procedure TTicketAckMatchForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TTicketAckMatchForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  TTMInserted := True;
end;

procedure TTicketAckMatchForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TTicketAckMatchForm.chkboxTicketSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxTicketSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TTicketAckMatchForm.CloseDataSets;
begin
  inherited;
  WorkPriorityRef.Close;
end;

procedure TTicketAckMatchForm.PopulateCallCenterList;
begin
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter), False, True);
end;

procedure TTicketAckMatchForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  TTMInserted := False;
end;

procedure TTicketAckMatchForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'ticket_ack_match', 'ticket_ack_match_id', TTMInserted);
  RefreshNow;
end;

procedure TTicketAckMatchForm.PopulateDropdowns;
begin
  inherited;
  PopulateCallCenterList;
end;

procedure TTicketAckMatchForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxTicketSearch.Enabled := True;
end;

end.
