object HierarchyForm: THierarchyForm
  Left = 21
  Top = 166
  Caption = 'Employee Hierarchy'
  ClientHeight = 541
  ClientWidth = 1042
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object Splitter: TSplitter
    Left = 355
    Top = 0
    Width = 8
    Height = 342
  end
  object SplitterBottom: TSplitter
    Left = 0
    Top = 342
    Width = 1042
    Height = 10
    Cursor = crVSplit
    Align = alBottom
  end
  object Panel1: TPanel
    Left = 363
    Top = 0
    Width = 679
    Height = 342
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SearchResultsPanel: TPanel
      Left = 0
      Top = 94
      Width = 679
      Height = 248
      Align = alClient
      Caption = 'SearchResultsPanel'
      TabOrder = 0
      object SearchResultGrid: TDBGrid
        Left = 1
        Top = 1
        Width = 677
        Height = 246
        Align = alClient
        DataSource = GridDataSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = SearchResultGridDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'h_emp_number'
            Title.Caption = 'Emp #'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_first_name'
            Title.Caption = 'First Name'
            Width = 93
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_last_name'
            Title.Caption = 'Last Name'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_short_name'
            Title.Caption = 'Short Name'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_ad_username'
            Title.Caption = 'AD Name'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_adp_login'
            Title.Caption = 'ADP'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TypeLookup'
            Title.Caption = 'Type'
            Width = 69
            Visible = True
          end>
      end
    end
    object CriteriaPanel: TPanel
      Left = 0
      Top = 0
      Width = 679
      Height = 94
      Align = alTop
      TabOrder = 1
      object Label8: TLabel
        Left = 12
        Top = 38
        Width = 412
        Height = 29
        AutoSize = False
        Caption = 
          'Enter search criteria above the field you want to search.  Only ' +
          'the portion of the hierarchy below the person you select on the ' +
          'left, will be searched.'
        WordWrap = True
      end
      object Label16: TLabel
        Left = -232
        Top = 8
        Width = 37
        Height = 13
        Caption = 'Label16'
      end
      object Label17: TLabel
        Left = -112
        Top = 0
        Width = 37
        Height = 13
        Caption = 'Label17'
      end
      object FindUpdateButton: TButton
        Left = 11
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Search'
        TabOrder = 0
        OnClick = FindUpdateButtonClick
      end
      object ClearButton: TButton
        Left = 97
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Clear'
        TabOrder = 1
        OnClick = ClearButtonClick
      end
      object EmployeeSearchEdit: TEdit
        Left = 2
        Top = 70
        Width = 79
        Height = 21
        TabOrder = 2
        OnKeyPress = EmployeeSearchEditKeyPress
      end
      object FirstNameSearchEdit: TEdit
        Left = 82
        Top = 70
        Width = 93
        Height = 21
        TabOrder = 3
        OnKeyPress = EmployeeSearchEditKeyPress
      end
      object LastNameSearchEdit: TEdit
        Left = 177
        Top = 70
        Width = 153
        Height = 21
        TabOrder = 4
        OnKeyPress = EmployeeSearchEditKeyPress
      end
      object ShortNameSearchEdit: TEdit
        Left = 330
        Top = 70
        Width = 111
        Height = 21
        TabOrder = 5
        OnKeyPress = EmployeeSearchEditKeyPress
      end
      object TypeComboBox: TComboBox
        Left = 598
        Top = 70
        Width = 71
        Height = 21
        DropDownCount = 15
        TabOrder = 6
      end
      object ShowActiveCheckBox: TCheckBox
        Left = 203
        Top = 12
        Width = 169
        Height = 17
        Caption = 'Show active employees only'
        Checked = True
        State = cbChecked
        TabOrder = 7
        OnClick = ShowActiveCheckBoxClick
      end
      object ADNameSearchEdit: TEdit
        Left = 442
        Top = 70
        Width = 82
        Height = 21
        TabOrder = 8
        OnKeyPress = EmployeeSearchEditKeyPress
      end
      object ADPSearchEdit: TEdit
        Left = 524
        Top = 70
        Width = 73
        Height = 21
        TabOrder = 9
        OnKeyPress = EmployeeSearchEditKeyPress
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 352
    Width = 1042
    Height = 189
    ActivePage = TabSheet1
    Align = alBottom
    Images = ImageList
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Employee Details'
      ImageIndex = -1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1034
        Height = 98
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object DBText1: TDBText
          Left = 48
          Top = 16
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'first_name'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 48
          Top = 32
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'TypeLookup'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText3: TDBText
          Left = 110
          Top = 16
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'middle_init'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText4: TDBText
          Left = 143
          Top = 16
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'last_name'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText5: TDBText
          Left = 48
          Top = 0
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'emp_id'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText6: TDBText
          Left = 336
          Top = 32
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'emp_number'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 38
          Height = 13
          Caption = 'Emp ID:'
        end
        object Label2: TLabel
          Left = 226
          Top = 32
          Width = 63
          Height = 13
          Caption = 'Emp number:'
        end
        object Label3: TLabel
          Left = 0
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Name:'
        end
        object Label4: TLabel
          Left = 0
          Top = 32
          Width = 28
          Height = 13
          Caption = 'Type:'
        end
        object Label5: TLabel
          Left = 226
          Top = 48
          Width = 34
          Height = 13
          Caption = 'Active:'
        end
        object Label6: TLabel
          Left = 226
          Top = 64
          Width = 95
          Height = 13
          Caption = 'Can receive tickets:'
        end
        object DBText7: TDBText
          Left = 337
          Top = 48
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'active'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText8: TDBText
          Left = 335
          Top = 64
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'can_receive_tickets'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 226
          Top = 80
          Width = 79
          Height = 13
          Caption = 'Reports To (ID):'
        end
        object DBText9: TDBText
          Left = 335
          Top = 80
          Width = 48
          Height = 13
          AutoSize = True
          DataField = 'report_to'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 226
          Top = 0
          Width = 103
          Height = 13
          Caption = 'Root of profit center:'
        end
        object DBText10: TDBText
          Left = 335
          Top = 1
          Width = 55
          Height = 13
          AutoSize = True
          DataField = 'repr_pc_code'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText11: TDBText
          Left = 335
          Top = 16
          Width = 55
          Height = 13
          AutoSize = True
          DataField = 'payroll_pc_code'
          DataSource = TreeDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 226
          Top = 16
          Width = 99
          Height = 13
          Caption = 'Payroll profit center:'
        end
        object Label11: TLabel
          Left = 399
          Top = 0
          Width = 16
          Height = 13
          Caption = '<--'
        end
        object EditButton: TButton
          Left = 2
          Top = 55
          Width = 145
          Height = 25
          Caption = 'Edit in Employee Screen'
          TabOrder = 0
          OnClick = EditButtonClick
        end
        object ProfitCenterCombo: TComboBox
          Left = 422
          Top = 24
          Width = 72
          Height = 21
          TabOrder = 1
        end
        object SetProfitCenter: TButton
          Left = 422
          Top = -1
          Width = 105
          Height = 19
          Caption = 'Set Profit Ctr Root:'
          TabOrder = 2
          OnClick = SetProfitCenterClick
        end
      end
      object NotificationEmailGrid: TDBGrid
        Left = 0
        Top = 98
        Width = 1034
        Height = 62
        Align = alClient
        DataSource = NotificationEmailDataDS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'email_type'
            Title.Caption = 'Type'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'email_destination'
            Title.Caption = 'Email Destination'
            Width = 463
            Visible = True
          end>
      end
    end
    object ChangeLogSheet: TTabSheet
      Caption = 'Change Log'
      ImageIndex = -1
      object ChangeLogMemo: TMemo
        Left = 0
        Top = 0
        Width = 1034
        Height = 160
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object ProblemsSheet: TTabSheet
      Caption = 'Problems'
      object ProblemsGrid: TDBGrid
        Left = 0
        Top = 30
        Width = 1034
        Height = 130
        Align = alClient
        DataSource = ProblemsDS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = ProblemsGridDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'short_name'
            Title.Caption = 'Employee Name'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'emp_id'
            Title.Caption = 'Emp ID'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'problem'
            Title.Caption = 'Hierarchy Problem'
            Width = 323
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1034
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label12: TLabel
          Left = 286
          Top = 6
          Width = 327
          Height = 13
          Caption = 
            'Editing above may require unchecking "Show active employees only' +
            '"'
        end
        object ProblemEditAboveButton: TButton
          Left = 171
          Top = 2
          Width = 94
          Height = 25
          Caption = 'Edit Above'
          TabOrder = 0
          OnClick = ProblemEditAboveButtonClick
        end
        object ProblemEditEmpScreenButton: TButton
          Left = 5
          Top = 2
          Width = 160
          Height = 25
          Caption = 'Edit in Employee Screen'
          TabOrder = 1
          OnClick = ProblemEditEmpScreenButtonClick
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 355
    Height = 342
    Align = alLeft
    Caption = 'Panel5'
    TabOrder = 2
    object TreeList: TcxDBTreeList
      Left = 1
      Top = 21
      Width = 353
      Height = 320
      Align = alClient
      Bands = <
        item
        end>
      DataController.DataSource = TreeDataSource
      DataController.ParentField = 'report_to'
      DataController.KeyField = 'emp_id'
      DragMode = dmAutomatic
      FindPanel.DisplayMode = fpdmManual
      LookAndFeel.NativeStyle = True
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OptionsBehavior.GoToNextCellOnTab = True
      OptionsBehavior.ChangeDelay = 1000
      OptionsBehavior.DragFocusing = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.AutoCalcKeyValue = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.TreeLineColor = clGray
      PopupMenu = TreePopup
      RootValue = '0'
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      OnBeginDragNode = TreeListBeginDragNode
      OnCustomDrawDataCell = TreeListCustomDrawDataCell
      OnDragOver = TreeListDragOver
      OnFilterNode = TreeListFilterNode
      OnFindPanelVisibilityChanged = TreeListFindPanelVisibilityChanged
      OnFocusedNodeChanged = TreeListFocusedNodeChanged
      OnMoveTo = TreeListMoveTo
      object EmployeeColumn: TcxDBTreeListColumn
        Caption.Text = 'Employee'
        DataBinding.FieldName = 'short_name'
        MinWidth = 99
        Options.Editing = False
        Width = 124
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ActiveColumn: TcxDBTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ValueChecked = 'True'
        Properties.ValueUnchecked = 'False'
        Caption.Text = 'Act'
        DataBinding.FieldName = 'active'
        Options.Editing = False
        Width = 30
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object EmpNumColumn: TcxDBTreeListColumn
        Caption.Text = 'Emp #'
        DataBinding.FieldName = 'emp_number'
        Options.Editing = False
        Width = 55
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object TypeColumn: TcxDBTreeListColumn
        Caption.Text = 'Type'
        DataBinding.FieldName = 'TypeLookup'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ReprPCColumn: TcxDBTreeListColumn
        Caption.Text = 'PC'
        DataBinding.FieldName = 'repr_pc_code'
        Options.Editing = False
        Width = 42
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
        OnGetDisplayText = ReprPCColumnGetDisplayText
      end
      object PayrollPCCodeColumn: TcxDBTreeListColumn
        Caption.Text = 'Pay PC'
        DataBinding.FieldName = 'payroll_pc_code'
        Options.Editing = False
        Width = 50
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 353
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object cbEmpTreeSearch: TCheckBox
        Left = 5
        Top = 2
        Width = 97
        Height = 12
        Caption = 'Show Find Panel'
        TabOrder = 0
        OnClick = cbEmpTreeSearchClick
      end
    end
  end
  object TreeDataSet: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = TreeDataSetBeforeOpen
    BeforeEdit = TreeDataSetBeforeEdit
    BeforePost = TreeDataSetBeforePost
    AfterPost = TreeDataSetAfterPost
    CommandText = 
      'select emp_id, type_id, emp_number, short_name, ad_username,'#13#10' f' +
      'irst_name,last_name, report_to, active, can_receive_tickets,'#13#10' r' +
      'epr_pc_code,  payroll_pc_code, status_id, middle_init,'#13#10' e.emp_n' +
      'umber + '#39' '#39' + e.short_name as number_and_name'#13#10'from employee e'#13#10 +
      'where active = 1'
    Parameters = <>
    Left = 52
    Top = 72
  end
  object TreeDataSource: TDataSource
    DataSet = TreeDataSet
    Enabled = False
    Left = 140
    Top = 64
  end
  object GridDataSource: TDataSource
    DataSet = GridDataSet
    Left = 120
    Top = 120
  end
  object GridDataSet: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    BeforeOpen = GridDataSetBeforeOpen
    CommandText = 'select * from dbo.get_hier3(211,0)'
    Parameters = <>
    Left = 56
    Top = 120
  end
  object TypeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference'#13#10' where type='#39'emptype'#39#13#10' order by ref_id'
    Parameters = <>
    Left = 248
    Top = 144
  end
  object TypeRefDS: TDataSource
    DataSet = TypeRef
    Left = 280
    Top = 184
  end
  object NotificationEmailData: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'select * from notification_email_user'#13#10'where emp_id = :emp_id'
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 144
    Top = 200
  end
  object NotificationEmailDataDS: TDataSource
    DataSet = NotificationEmailData
    Left = 232
    Top = 232
  end
  object TreePopup: TPopupMenu
    Left = 200
    Top = 72
    object ExpandAll: TMenuItem
      Caption = 'Expand All'
      OnClick = ExpandAllClick
    end
    object ContractAll: TMenuItem
      Caption = 'Contract All'
      OnClick = ContractAllClick
    end
    object ExpandNodeMenuItem: TMenuItem
      Caption = 'Expand Node'
      OnClick = ExpandNodeMenuItemClick
    end
    object ContractNodeMenuItem: TMenuItem
      Caption = 'Contract Node'
      OnClick = ContractNodeMenuItemClick
    end
  end
  object SetReprPC: TADOCommand
    CommandText = 
      'update employee set repr_pc_code = null'#13#10' where repr_pc_code = :' +
      'PCCode'#13#10'update employee set repr_pc_code = :PCCode2'#13#10'  where emp' +
      '_id=:id'#13#10
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'PCCode'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'PCCode2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 120
    Top = 280
  end
  object ProblemsDS: TDataSource
    DataSet = ProblemsSP
    Left = 248
    Top = 288
  end
  object ProblemsSP: TADOStoredProc
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterOpen = ProblemsSPAfterOpen
    ProcedureName = 'employee_hierarchy_problems;1'
    Parameters = <>
    Left = 200
    Top = 288
  end
  object ImageList: TImageList
    Left = 232
    Top = 72
    Bitmap = {
      494C010101000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F000000
      000000000000000000000000000000000000000000004047940030357D003035
      7D0030357D0030357D0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E6100363B
      7F0000000000000000000000000000000000313790008287DC00888FF5006D75
      F3006169E0005159CB0030357D0030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A005D64
      C200757DF6005D64D2004D54BD00333CA600262C9F00A3A8FF00AEB3FF00949A
      FF007D85FF006972FE004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC00A1A7FF00989EFF007D85FF003842D100181FA2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004C54C60030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00636A
      CC009EA3F900949AFF006972FE003742CE001A21A2009EA3F900AEB3FF008E95
      FF00757EFF00666FF9004F57C90030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E610057575A00646B
      D000A3A8FF009EA3F900757EFF003742CE001A21A200A1A7FF00ABB0FF008E95
      FF00757EFF00666FF900535BD6002F347C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005D5C5F00565C
      B4007179EA006D75F3006770F1003842D100181FA2005D63B1004D53A8005158
      C4005158C4004C54C600474EB90030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E61005051
      7D002F3587004C53B1005158C40031379000272E8D0030358100000000000000
      000000000000000000002D32760030357D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F5E61005F5E61000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF009FFF0000000000009FFF000000000000
      9FFF0000000000009FFF0000000000009FFF0000000000009F83000000000000
      8F00000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      803C0000000000009FFF00000000000000000000000000000000000000000000
      000000000000}
  end
  object TomQuery: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'dialup_user'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'select active from employee where dialup_user = :dialup_user and' +
        ' active = 1')
    Left = 56
    Top = 186
  end
end
