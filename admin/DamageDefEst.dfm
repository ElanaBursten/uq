inherited DamageDefEstForm: TDamageDefEstForm
  Left = 420
  Top = 206
  Caption = 'Damage Default Cost Estimates'
  ClientWidth = 613
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 613
    Height = 25
    object cbDefaultEstSearch: TCheckBox
      Left = 23
      Top = 3
      Width = 99
      Height = 18
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbDefaultEstSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 613
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'ddest_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object ColFacilityType: TcxGridDBColumn
        Caption = 'Facility Type'
        DataBinding.FieldName = 'facility_type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 20
        Width = 80
      end
      object ColFacilitySize: TcxGridDBColumn
        Caption = 'Facility Size'
        DataBinding.FieldName = 'facility_size'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 20
        Width = 90
      end
      object ColFacilityMaterial: TcxGridDBColumn
        Caption = 'Facility Material'
        DataBinding.FieldName = 'facility_material'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 20
        Width = 109
      end
      object ColProfitCenter: TcxGridDBColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'profit_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 20
        Width = 80
      end
      object ColUtilityCoName: TcxGridDBColumn
        Caption = 'Utility Co'
        DataBinding.FieldName = 'utility_co_name'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ClearKey = 46
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 20
        Properties.ListColumns = <
          item
            FieldName = 'description'
          end>
        Properties.ListOptions.ShowHeader = False
        Width = 200
      end
      object ColDefaultAmount: TcxGridDBColumn
        Caption = 'Estimate Amount'
        DataBinding.FieldName = 'default_amount'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        HeaderAlignmentHorz = taRightJustify
        Width = 93
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 
      'select * from damage_default_est order by facility_type, facilit' +
      'y_size'
    Left = 64
  end
  inherited DS: TDataSource
    Left = 120
  end
  object TypeCodes: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'select code from reference where type='#39'factype'#39' order by 1'
    Parameters = <>
    Left = 64
    Top = 208
  end
  object SizeCodes: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select code from reference where type='#39'facsize'#39' order by modifie' +
      'r, code'
    Parameters = <>
    Left = 64
    Top = 248
  end
  object MatCodes: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select code from reference where type='#39'facmtrl'#39' order by modifie' +
      'r, code'
    Parameters = <>
    Left = 64
    Top = 292
  end
  object ProfitCenters: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'select pc_code'#13#10'from profit_center'#13#10'order by pc_code'
    Parameters = <>
    Left = 64
    Top = 328
  end
  object UtilityCompanys: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, description from reference where type='#39'dmgco'#39' and' +
      ' active_ind=1 order by description'
    Parameters = <>
    Left = 64
    Top = 364
  end
end
