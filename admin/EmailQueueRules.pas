unit EmailQueueRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxBlobEdit, cxNavigator,  dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  ClipBrd, dxScrollbarAnnotations;

type
  // Note: TFaxQueueRulesForm descends from this and changes the form name, SQL, and caption
  TEmailQueueRulesForm = class(TBaseCxListForm)
    ColCallCenter: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColState: TcxGridDBColumn;
    ColStatusMessage: TcxGridDBColumn;
    chkboxEmailSearch: TCheckBox;
    GridViewincl_bcc: TcxGridDBColumn;
    GridViewbcc_address: TcxGridDBColumn;
    procedure chkboxEmailSearchClick(Sender: TObject);
    procedure GridViewUpdateEdit(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
    procedure GridViewInitEdit(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  protected
    emqRulesInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation
uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

procedure TEmailQueueRulesForm.chkboxEmailSearchClick(Sender: TObject);
begin
 inherited;
 if chkboxEMailSearch.Checked then
   GridView.Controller.ShowFindPanel
 else
   GridView.Controller.HideFindPanel;
end;

procedure TEmailQueueRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
   emqRulesInserted := False;
end;

procedure TEmailQueueRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset, ATableArray, 'email_queue_rules', 'eqr_id', emqRulesInserted);
end;

procedure TEmailQueueRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TEmailQueueRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
   emqRulesInserted := True;
end;

//BP QM-687
procedure TEmailQueueRulesForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEmailQueueRulesForm.ActivatingNow;
begin
  inherited;
  emqRulesInserted := False;
end;

procedure TEmailQueueRulesForm.GridViewInitEdit(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
begin
  inherited;
  if Clipboard.HasFormat(CF_TEXT) then
    Clipboard.AsText := StringReplace(StringReplace(Clipboard.AsText, sLineBreak, ' ', [rfReplaceAll]), #10#13, ' ', [rfReplaceAll]);
end;

//BP QM-687
procedure TEmailQueueRulesForm.GridViewUpdateEdit(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit);
begin
  inherited;
  if Clipboard.HasFormat(CF_TEXT) then
    Clipboard.AsText := StringReplace(StringReplace(Clipboard.AsText, sLineBreak, ' ', [rfReplaceAll]), #10#13, ' ', [rfReplaceAll]);
end;

procedure TEmailQueueRulesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TEmailQueueRulesForm.PopulateDropdowns;
begin
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter));
  AdminDM.GetStatusList(CxComboBoxItems(ColStatus), True);
  AdminDM.GetClientList(CxComboBoxItems(ColClientCode));
  CxComboBoxItems(ColClientCode).Insert(0, '*');
  AdminDM.GetStateList(CxComboBoxItems(ColState));
  CxComboBoxItems(ColState).Insert(0, '*');
end;

procedure TEmailQueueRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxEmailSearch.Enabled := True;
end;

end.
