unit BillingCCMap;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDmu, Vcl.StdCtrls, dxSkinsCore, dxSkinBasic, dxCore, dxSkinsForm;

type
  TBillingCCMapForm = class(TBaseCxListForm)
    ColCol_Center: TcxGridDBColumn;
    ColClientCode: TcxGridDBColumn;
    ColWorkState: TcxGridDBColumn;
    ColWorkCounty: TcxGridDBColumn;
    ColWorkCity: TcxGridDBColumn;
    ColBillingCC: TcxGridDBColumn;
    chkboxBillingccSearch: TCheckBox;
    dxSkinController1: TdxSkinController;
    procedure chkboxBillingccSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    BillingCCInserted: Boolean;
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  BillingCCMapForm: TBillingCCMapForm;

implementation

{$R *.dfm}

procedure TBillingCCMapForm.ActivatingNow;
begin
  inherited;
  BillingCCInserted := False;
end;

procedure TBillingCCMapForm.chkboxBillingccSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxBillingccSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TBillingCCMapForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  BillingCCInserted := False;
end;

procedure TBillingCCMapForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'billing_cc_map', 'bcm_id', BillingCCInserted);
end;

procedure TBillingCCMapForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TBillingCCMapForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  BillingCCInserted := True;
end;

procedure TBillingCCMapForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TBillingCCMapForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TBillingCCMapForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxbillingccsearch.Enabled := true;
end;

end.
