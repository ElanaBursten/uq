unit QtrMinRouting;

{QMANTWO-548- This unit has been removed from QManager Admin. It was replaced
  Qtr-Min Editor by XQtrix.}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ExtCtrls, DB, ADODB, Grids, StdCtrls, GridAxis, BaseMap;

type
  TQtrMinForm = class(TBaseMapForm)
    Panel2: TPanel;
    QtrMin: TADODataSet;
    QtrMinDS: TDataSource;
    locator: TADODataSet;
    locatorDs: TDataSource;
    Splitter1: TSplitter;
    TheGrid: TStringGrid;
    PopulateButton: TButton;
    FindLats: TADODataSet;
    FindLongs: TADODataSet;
    StatusLabel: TLabel;
    Label1: TLabel;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    procedure PopulateButtonClick(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure TheGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure TheGridDblClick(Sender: TObject);
  private
    ColAxis, RowAxis: TQtrAxis;
    BitMap: TBitMap;
    FColorAssignments: TStrings;
    procedure SetColumnLabels;
    procedure SetRowLabels;
    procedure ClearGrid;
    procedure FillGridData;
    function CellContentsFor(area_id: Integer): string;
    procedure ShowStatus(S: string);
    procedure CreateNavImage;
    procedure ShowNavImage;
    procedure SetPixel(X, Y: Integer; Color: TColor);
    procedure CenterOn(X, Y: Integer);
    procedure InitColorAssignments;
    function GetColorFor(AreaID: Integer): TColor;
  public
    procedure Populate;
    procedure RefreshNow; override;
  end;

implementation

uses AdminDMu, OdHourGlass, JclGraphUtils, MainFU;

{$R *.dfm}

procedure TQtrMinForm.PopulateButtonClick(Sender: TObject);
begin
  Populate;
end;

function TQtrMinForm.CellContentsFor(area_id: Integer): string;
begin
{
  if not GetAreaRepr.Active then
    GetAreaRepr.Open;

  if GetAreaRepr.Locate('area_id', area_id, []) then
    Result := GetAreaRepr.FieldByName('area_name').AsString + '/' +
      GetAreaRepr.FieldByName('short_name').AsString;
      }
    Result := IntToStr(area_id);
end;

procedure TQtrMinForm.ClearGrid;
var
  R, C: Integer;
begin
  ShowStatus('Clearing Grid');
  for R := 0 to Grid.RowCount-1 do
    for C := 0 to Grid.ColCount-1 do
      Grid.Cells[C, R] := '';
  Grid.ColCount := 2;
  Grid.RowCount := 2;
end;

procedure TQtrMinForm.CreateNavImage;
begin
  Bitmap := TBitmap.Create;
  Bitmap.Width := Grid.ColCount;
  Bitmap.Height := Grid.RowCount;
end;

procedure TQtrMinForm.FillGridData;
var
  Cnt: Integer;
  R, C: Integer;
  AreaID: Integer;
  LatField, LongField, AreaField: TField;

  FailedCount : Integer;
begin
  ShowStatus('Filling Grid');

  QtrMin.Close;
  QtrMin.Open;
  Cnt := 0;
  LatField := QtrMin.FieldByName('qtrmin_lat');
  LongField := QtrMin.FieldByName('qtrmin_long');
  AreaField := QtrMin.FieldByName('area_id');
  FailedCount := 0;

  //Temporarily put a limit on this so it does not freeze (until we decide what to do with page)
  while (not QtrMin.Eof) and (Cnt < 30) do begin
    try
      R := RowAxis.CodeToIndex(LatField.AsString);
      C := ColAxis.CodeToIndex(LongField.AsString);

      AreaID := AreaField.AsInteger;
      if (C > 0) and (R > 0) and
         (C < Grid.ColCount) and (R < Grid.RowCount) then begin
        Grid.Cells[C, R] := CellContentsFor(AreaID);

        SetPixel(C, R, GetColorFor(AreaID));
      end;
    except
      Inc(FailedCount);
    end;

    QtrMin.Next;

    Inc(Cnt);
    if Cnt mod 500 = 0 then
      ShowStatus('Loaded Records: ' + IntToStr(Cnt));
  end;
  if FailedCount > 0 then
    ShowMessage(format('Failed loading %d entries (non critical error)', [FailedCount]));
end;

procedure TQtrMinForm.Populate;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;

  ClearGrid;
  SetRowLabels;
  SetColumnLabels;
  CreateNavImage;
  InitColorAssignments;
  FillGridData;
  ShowNavImage;
end;

procedure TQtrMinForm.SetColumnLabels;
var
  x: Integer;
begin
  ShowStatus('Setting Column Labels');
  FreeAndNil(ColAxis);

  with FindLongs do begin
    Open;
    ColAxis := TQtrAxis.Create(Fields[0].AsString, Fields[1].AsString);
    Close;
  end;

  ColAxis.BufferEdges;

  Grid.ColCount := ColAxis.Size + 1;
  for x := 1 to Grid.ColCount-1 do begin
    Grid.Cells[x, 0] := ColAxis.IndexToCode(x);
  end;
end;

procedure TQtrMinForm.SetPixel(X, Y: Integer; Color: TColor);
begin
  with Bitmap.Canvas do begin
    Pen.Color := Color;
    Pen.Style := psSolid;
    MoveTo(X, Y);
    LineTo(X+1, Y);
  end;
end;

procedure TQtrMinForm.SetRowLabels;
var
  y: Integer;
begin
  ShowStatus('Setting Row Labels');
  FreeAndNil(RowAxis);

  with FindLats do begin
    Open;
    RowAxis := TQtrAxis.Create(Fields[0].AsString, Fields[1].AsString);
    Close;
  end;

  RowAxis.BufferEdges;

  Grid.RowCount := RowAxis.Size + 1;
  for y := 1 to Grid.RowCount-1 do begin
    Grid.Cells[0, y] := RowAxis.IndexToCode(y);
  end;

  Grid.ColWidths[0] := 45;
end;

procedure TQtrMinForm.ShowNavImage;
begin
  Image1.Picture.Graphic := Bitmap;
  FreeAndNil(Bitmap);
  Image1.Width := Image1.Picture.Width;
  Image1.Height := Image1.Picture.Height;
end;

procedure TQtrMinForm.ShowStatus(S: string);
begin
  StatusLabel.Caption := S;
  StatusLabel.Update;
  Application.ProcessMessages;
  Image1.Update;
end;

procedure TQtrMinForm.Image1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CenterOn(X, Y);
end;

procedure TQtrMinForm.Image1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if ssLeft in Shift then
    CenterOn(X, Y);
end;

procedure TQtrMinForm.CenterOn(X, Y: Integer);
begin
  if (Y>=0) then
    Grid.TopRow := Y+1;
  if (X>=0) then
    Grid.LeftCol := X+1;
end;

procedure TQtrMinForm.InitColorAssignments;
begin
  FColorAssignments := TStringList.Create;
end;

function TQtrMinForm.GetColorFor(AreaID: Integer): TColor;
var
  ColorString: string;
  Color: TColor;
begin
  if AreaID = 0 then
    Result := clLtGray
  else begin
    ColorString := FColorAssignments.Values[IntToStr(AreaID)];
    if ColorString='' then begin
      Color := SetRgbValue(Random(170)+80, Random(170)+80, Random(170)+80);
      ColorString := IntToStr(Color);
      FColorAssignments.Values[IntToStr(AreaID)] := ColorString;
    end;
    Result := TColor(StrToInt(ColorString));
  end;
end;

procedure TQtrMinForm.RefreshNow;
begin
  Populate;
end;

procedure TQtrMinForm.TheGridDrawCell(Sender: TObject; ACol, ARow: Integer;  Rect: TRect; State: TGridDrawState);
var
  S: string;
  C: TColor;
begin
  if (ARow>0) and (ACol>0) then begin
    with (Sender as TStringGrid).Canvas do begin
      S := Grid.Cells[ACol,ARow];
      if S <> '' then begin
        if gdSelected in State then begin
          C := clYellow;
        end else begin
          C := GetColorFor(StrToInt(s));
          C := BrightColor(C, 30);
        end;
        Brush.Color := C;
      end else
        Brush.Color := clWhite;

      Brush.Style := bsSolid;
      FillRect(Rect);

      Pen.Style := psSolid;
      Pen.Color := clBlack;

      Brush.Style := bsClear;
      Font.Color := clBlack;
      Font.Style := [ ];
      TextRect(Rect, Rect.Left + 2, Rect.Top+2, S);
    end;
  end;
end;

procedure TQtrMinForm.FormCreate(Sender: TObject);
begin
  inherited;
  Grid := TheGrid;
end;

procedure TQtrMinForm.TheGridDblClick(Sender: TObject);
var
  Lat, Long : string;
begin
  inherited;
  // Go to the selected item for editing?
  Lat := TheGrid.Cells[0, TheGrid.TopRow];
  Long := TheGrid.Cells[TheGrid.LeftCol, 0];
  MainForm.ShowQtrMapPage(Lat, Long);
end;

end.

