object RightsFrame: TRightsFrame
  Left = 0
  Top = 0
  Width = 788
  Height = 224
  TabOrder = 0
  object RightHeaderLabel: TLabel
    Left = 447
    Top = 8
    Width = 167
    Height = 14
    Caption = 'Grant / Revoke Selected Right'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ModifierLabel: TLabel
    Left = 536
    Top = 32
    Width = 144
    Height = 15
    Caption = ' With Limitation / Modifier:'
  end
  object LimitationHintLabel: TLabel
    Left = 536
    Top = 73
    Width = 232
    Height = 63
    AutoSize = False
    Caption = 'Limitation description'
    WordWrap = True
  end
  object GrantButton: TButton
    Left = 447
    Top = 40
    Width = 75
    Height = 25
    Caption = '&Grant'
    TabOrder = 1
  end
  object EditLimitation: TEdit
    Left = 536
    Top = 48
    Width = 233
    Height = 21
    TabOrder = 2
  end
  object RevokeButton: TButton
    Left = 447
    Top = 88
    Width = 75
    Height = 25
    Caption = '&Revoke'
    TabOrder = 3
  end
  object RightsGrid: TcxGrid
    Left = 0
    Top = 0
    Width = 433
    Height = 224
    Align = alLeft
    TabOrder = 0
    object RightsGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsSelection.CellSelect = False
      OptionsView.GroupByBox = False
      object ColGroupRightsGridCategory: TcxGridDBColumn
        DataBinding.FieldName = 'right_category'
        DataBinding.IsNullValueType = True
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        Width = 20
        IsCaptionAssigned = True
      end
      object ColGroupRightsGridRight: TcxGridDBColumn
        Caption = 'Right'
        DataBinding.FieldName = 'right_description'
        DataBinding.IsNullValueType = True
        MinWidth = 180
        Options.Editing = False
        Options.Filtering = False
        Options.Grouping = False
        Width = 180
      end
      object ColGroupRightsGridID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'right_id'
        DataBinding.IsNullValueType = True
        Visible = False
        MinWidth = 35
        Options.Editing = False
        Options.Filtering = False
        Options.Grouping = False
        Width = 35
      end
      object ColGroupRightsGridAllowed: TcxGridDBColumn
        Caption = 'Y/N'
        DataBinding.FieldName = 'allowed'
        DataBinding.IsNullValueType = True
        FooterAlignmentHorz = taCenter
        HeaderAlignmentHorz = taCenter
        MinWidth = 25
        Options.Editing = False
        Options.Filtering = False
        Width = 25
      end
      object ColGroupRightsGridLimitation: TcxGridDBColumn
        Caption = 'Limitation / Modifier'
        DataBinding.FieldName = 'limitation'
        DataBinding.IsNullValueType = True
        MinWidth = 160
        Options.Editing = False
        Options.Filtering = False
        Width = 199
      end
    end
    object RightsGridLevel1: TcxGridLevel
      GridView = RightsGridDBTableView1
    end
  end
end
