object frmMainStatusEdit: TfrmMainStatusEdit
  Left = 0
  Top = 0
  Caption = 'Edit Status Exclusions'
  ClientHeight = 496
  ClientWidth = 1097
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1097
    Height = 496
    Align = alClient
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Enabled = False
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.SearchInGroupRows = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsTktStatusExclusions
      DataController.DetailKeyFieldNames = 'sg_item_id'
      DataController.KeyFieldNames = 'sg_item_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1sg_item_id: TcxGridDBColumn
        DataBinding.FieldName = 'sg_item_id'
        DataBinding.IsNullValueType = True
        Visible = False
      end
      object cxGrid1DBTableView1call_center: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
        Visible = False
        GroupIndex = 0
      end
      object cxGrid1DBTableView1sg_name: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'sg_name'
        DataBinding.IsNullValueType = True
        Visible = False
        GroupIndex = 1
      end
      object cxGrid1DBTableView1status: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        DataBinding.IsNullValueType = True
      end
      object cxGrid1DBTableView1ticketTypeExclusion: TcxGridDBColumn
        Caption = 'Ticket Type Exclusion'
        DataBinding.FieldName = 'ticketTypeExclusion'
        DataBinding.IsNullValueType = True
        Width = 650
      end
      object cxGrid1DBTableView1NotesRequired: TcxGridDBColumn
        Caption = 'Notes Required'
        DataBinding.FieldName = 'NotesRequired'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Width = 116
      end
      object cxGrid1DBTableView1PicturesRequired: TcxGridDBColumn
        DataBinding.FieldName = 'PicturesRequired'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Width = 135
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryTktStatusExclusions: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = qryTktStatusExclusionsBeforeInsert
    BeforeEdit = qryTktStatusExclusionsBeforeEdit
    AfterPost = qryTktStatusExclusionsAfterPost
    AfterCancel = qryTktStatusExclusionsAfterCancel
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      
        'Select distinct sg_item_id, c.call_center, sg_name, status, tick' +
        'etTypeExclusion, PicturesRequired, NotesRequired'
      'from status_group sg'
      'left join status_group_item sgi on (sgi.sg_id=sg.sg_id)'
      'left join client c on (c.status_group_id = sg.sg_id)'
      'where sg.active=1'
      'and c.active = 1'
      'order by c.call_center, sg_name, status'
      '')
    Left = 184
    Top = 40
  end
  object dsTktStatusExclusions: TDataSource
    DataSet = qryTktStatusExclusions
    Left = 240
    Top = 40
  end
end
