unit HALCallCenters;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseList, Data.DB, Vcl.StdCtrls,
  Vcl.Samples.Spin, Vcl.DBCtrls, Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, cxDBData, cxTextEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, OddbUtils, AdminDMu, cxContainer, cxMaskEdit,
  cxDropDownEdit, MainFu;

type
  THALCallCenterForm = class(TBaseListForm)
    lblCallCenter: TLabel;
    btnCallCenter: TButton;
    Label1: TLabel;
    SpinEditDateAdd: TSpinEdit;
    lblThreshhold: TLabel;
    edtThreshhold: TEdit;
    CallCenters: TADODataSet;
    dsCallCenters: TDataSource;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColDaysback: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cbCallCenters: TComboBox;
    procedure btnCallCenterClick(Sender: TObject);
    procedure ColDaysbackPropertiesEditValueChanged(Sender: TObject);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1Exit(Sender: TObject);
  private
    procedure CheckCallCenter(callcenter: string; daysback: integer; threshhold: integer);
    procedure GetCallCenterList(List: TStrings);
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly:Boolean; override;
  end;

var
  HALCallCenterForm: THALCallCenterForm;

implementation

{$R *.dfm}

procedure THALCallCenterForm.btnCallCenterClick(Sender: TObject);
begin
  if cbCallCenters.ItemIndex = -1 then
    showmessage('Choose call center') else
     CheckCallCenter(cbCallCenters.Text, spinEditDateAdd.Value, StrtoInt(edtThreshhold.Text));
end;

procedure THALCallCenterForm.CheckCallCenter(callcenter: string; daysback: integer; threshhold:integer);
var
  Centers: Tstringlist;
  QueryStr: String;
  whichFieldName: String;
  ccExists: Boolean;
  AMessage: String;
begin
  try
    //sanity check for a more than a day
    If threshhold > 1440 then
      raise Exception.Create('Greater than 1,440 minutes in argument (One Day), exiting');

    //Currently NCA1 is hosed up. See JIRA QMAN-3234 for the reason why.  What we have to do as one
    //of our only options for now is to look at the ticket_format field instead of the source field
    //in the ticket table if NCA1.
    //This will work for now, but do note that it limits our ability somewhat to monitor Northern CA
    //because other call centers like NCA2 (AT&T) and NCA4 (CVIN) update NCA1.  More work will be
    //needed later on to look at other aspects of NCA1 to truly monitor that center.

    If callcenter = 'NCA1' then
      whichFieldName := 'ticket_format'
    else whichFieldName := 'source';

    QueryStr := 'DECLARE @hal TABLE (lastTicket_version_id int, call_center varchar(12)); '+
      'INSERT INTO @hal (lastTicket_version_id, call_center) ' +
      'select max(ticket_version_id), ' + whichFieldName + ' ' +
      'from ticket_version ' +
      'where ' + whichFieldName + ' = (' + QuotedStr(callcenter) + ') ' +
      'and transmit_date between getdate()-' + inttostr(daysback) + ' and getdate()-1 ' +
      'group by ' + whichFieldName + ' ' +
      'select ticket_version.source, call_center.cc_name, lastTicket_version_id,' +
      'ticket_version.ticket_number, ticket_version.arrival_date, ticket_version.ticket_type,' +
      'DateDiff(minute,ticket_version.arrival_date,getDate()) as delta ' +
      'from @hal h ' +
      'inner join ticket_version on ticket_version.ticket_version_id = h.lastTicket_version_id ' +
      'left join call_center on call_center.cc_code = ticket_version.' + whichFieldName;
    Data.Close;
    Data.CommandText := QueryStr;
    Data.Open;
  except
    CallCenters.Close;
    Data.Close;
    raise;
  end;
end;

procedure THALCallCenterForm.ColDaysbackPropertiesEditValueChanged(
  Sender: TObject);
begin
  If StrToInt(TcxCustomEdit(Sender).EditValue) < 1 then
  begin
    showmessage('Minimum daysback value is 1');
     HalConfig.Cancel;
  end;
end;

procedure THALCallCenterForm.ColEmailsPropertiesEditValueChanged(Sender: TObject);
begin
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
     HalConfig.Cancel;
  end;
end;

procedure THALCallCenterForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure THALCallCenterForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function THALCallCenterForm.IsReadOnly: Boolean;
begin
  inherited;
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure THALCallCenterForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure THALCallCenterForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure THALCallCenterForm.OpenDataSets;
begin
   GetCallCenterList(cbCallCenters.Items);
end;

procedure THALCallCenterForm.GetCallCenterList(List: TStrings);
begin
   Assert(Assigned(List));
   CallCenters.Open;
  try
    List.Clear;
    while not CallCenters.Eof do begin
      List.Add(CallCenters.FieldByName('cc_code').DisplayText);
      CallCenters.Next;
    end;
  finally
    CallCenters.Close;
  end;
end;

procedure THALCallCenterForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  cbCallCenters.Enabled := True;
  edtThreshhold.Enabled := True;
  btnCallCenter.Enabled := True;
end;

procedure THALCallCenterForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  if not HALConfig.EOF then
    spineditdateadd.Value := HalConfig.FieldByName('daysback').AsInteger;
  SetReadOnly(True);
end;

end.
