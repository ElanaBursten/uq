inherited HighProfileAddressForm: THighProfileAddressForm
  Left = 247
  Top = 202
  Caption = 'High Profile Address'
  ClientHeight = 534
  ClientWidth = 895
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 895
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 57
      Height = 13
      Caption = 'Call Center:'
    end
    object CallCenterFilter: TComboBox
      Left = 72
      Top = 11
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      TabOrder = 0
      OnSelect = CallCenterFilterSelect
    end
    object cbHPAddressSearch: TCheckBox
      Left = 258
      Top = 16
      Width = 97
      Height = 15
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbHPAddressSearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 895
    Height = 493
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'hp_address_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object Colcall_center: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 7
        Visible = False
        GroupIndex = 0
        Options.Filtering = False
        Width = 72
      end
      object Colclient_code: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 7
        Width = 127
      end
      object Colstreet: TcxGridDBColumn
        Caption = 'Street Address'
        DataBinding.FieldName = 'street'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.CharCase = ecUpperCase
        Width = 250
      end
      object Colcity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.CharCase = ecUpperCase
        Width = 200
      end
      object Colstate: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 7
        Width = 47
      end
      object Colcounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.CharCase = ecUpperCase
        Width = 200
      end
      object Colhp_address_id: TcxGridDBColumn
        DataBinding.FieldName = 'hp_address_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 77
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from hp_address order by call_center, client_code'
  end
  inherited DS: TDataSource
    Left = 112
  end
end
