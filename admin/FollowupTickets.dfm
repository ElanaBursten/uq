inherited FollowupTicketsForm: TFollowupTicketsForm
  Caption = 'Followup Tickets'
  ClientWidth = 556
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 556
    Height = 26
    object cbFollowupSearch: TCheckBox
      Left = 26
      Top = 7
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbFollowupSearchClick
    end
    object ActiveFollowupFilter: TCheckBox
      Left = 150
      Top = 8
      Width = 140
      Height = 12
      Caption = 'Active Tickets Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveFollowupFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 556
    Height = 374
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'type_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColTicketType: TcxGridDBColumn
        Caption = 'Ticket Type'
        DataBinding.FieldName = 'ticket_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 269
      end
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 194
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 91
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from followup_ticket_type where active = 1'
  end
end
