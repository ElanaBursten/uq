inherited BaseMasterDetailForm: TBaseMasterDetailForm
  Caption = 'BaseMasterDetailForm'
  ClientHeight = 408
  ClientWidth = 540
  Font.Charset = ANSI_CHARSET
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 209
    Top = 37
    Width = 9
    Height = 371
    Beveled = True
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 540
    Height = 37
    Align = alTop
    TabOrder = 0
  end
  object MasterPanel: TPanel
    Left = 0
    Top = 37
    Width = 209
    Height = 371
    Align = alLeft
    TabOrder = 1
    DesignSize = (
      209
      371)
    object MasterGrid: TcxGrid
      Left = 8
      Top = 48
      Width = 193
      Height = 316
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      OnExit = MasterGridExit
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object MasterGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = True
        Navigator.Buttons.Next.Visible = True
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = True
        Navigator.Buttons.Append.Visible = True
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = MasterSource
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.NoDataToDisplayInfoText = '<No data to display. Click New to add data.>'
      end
      object MasterGridLevel: TcxGridLevel
        GridView = MasterGridView
      end
    end
    object MasterNewButton: TButton
      Left = 8
      Top = 12
      Width = 75
      Height = 25
      Action = MasterNewAction
      TabOrder = 1
    end
    object MasterDeleteButton: TButton
      Left = 92
      Top = 12
      Width = 75
      Height = 25
      Action = MasterDeleteAction
      TabOrder = 2
    end
  end
  object DetailPanel: TPanel
    Left = 218
    Top = 37
    Width = 322
    Height = 371
    Align = alClient
    TabOrder = 2
    DesignSize = (
      322
      371)
    object DetailGrid: TcxGrid
      Left = 6
      Top = 48
      Width = 316
      Height = 316
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      OnExit = DetailGridExit
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object DetailGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Append.Visible = True
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DetailSource
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsSelection.InvertSelect = False
        OptionsView.NoDataToDisplayInfoText = '<No data to display. Click New to add data.>'
      end
      object DetailGridLevel: TcxGridLevel
        GridView = DetailGridView
      end
    end
    object DetailNewButton: TButton
      Left = 8
      Top = 12
      Width = 75
      Height = 25
      Action = DetailNewAction
      TabOrder = 1
    end
    object DetailDeleteButton: TButton
      Left = 89
      Top = 12
      Width = 75
      Height = 25
      Action = DetailDeleteAction
      TabOrder = 2
    end
  end
  object Master: TADODataSet
    BeforeDelete = MasterDetailBeforeDelete
    Parameters = <>
    Left = 16
    Top = 112
  end
  object Detail: TADODataSet
    BeforeDelete = MasterDetailBeforeDelete
    DataSource = MasterSource
    Parameters = <>
    Left = 232
    Top = 116
  end
  object MasterSource: TDataSource
    DataSet = Master
    Left = 52
    Top = 112
  end
  object DetailSource: TDataSource
    DataSet = Detail
    Left = 268
    Top = 116
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 4
    Top = 4
    object MasterNewAction: TAction
      Caption = 'New'
      OnExecute = MasterNewActionExecute
    end
    object MasterDeleteAction: TAction
      Caption = 'Delete'
      OnExecute = MasterDeleteActionExecute
    end
    object DetailNewAction: TAction
      Caption = 'New'
      OnExecute = DetailNewActionExecute
    end
    object DetailDeleteAction: TAction
      Caption = 'Delete'
      OnExecute = DetailDeleteActionExecute
    end
  end
end
