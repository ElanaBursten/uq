unit UnRouting;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ADODB, StdCtrls, ExtCtrls, Grids, DBGrids, DB;

type
  TUnRoutingForm = class(TEmbeddableForm)
    Locators: TADODataSet;
    LocatorsDS: TDataSource;
    LocatorsGrid: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    btnUnroute: TButton;
    Label2: TLabel;
    Unroute: TADOCommand;
    btnUnrouteTop500: TButton;
    Unroute500: TADOCommand;
    FilterRecordsButton: TButton;
    SearchEdit: TEdit;
    ClearFilterButton: TButton;
    procedure btnUnrouteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUnrouteTop500Click(Sender: TObject);
    procedure FilterRecordsButtonClick(Sender: TObject);
    procedure ClearFilterButtonClick(Sender: TObject);
  public
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure EditNow; override;
  end;

implementation

uses AdminDMu, OdHourglass;

{$R *.dfm}

procedure TUnRoutingForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TUnRoutingForm.btnUnrouteClick(Sender: TObject);
var
  Cursor: IInterface;
  ID: Integer;
  s: String;
begin
  inherited;
  ID := Locators.FieldByName('emp_id').AsInteger;
  Cursor := ShowHourglass;
  AdminDM.Conn.BeginTrans;
  try
    Unroute.Parameters.ParamValues['id'] := ID;
    Unroute.Execute;
    AdminDM.Conn.CommitTrans;
    ShowMessage('Locates have been un-routed for locator ID ' + IntToStr(ID));
    AdminDM.AdminChangeTable('Locate', 'Locates unrouted for locator ID ' + IntToStr(ID), Now); //qm-955 BP
  except
    AdminDM.Conn.RollbackTrans;
    raise;
  end
end;

procedure TUnRoutingForm.ClearFilterButtonClick(Sender: TObject);
begin
  inherited;
  Locators.Filtered := False;
  Locators.Filter := '';
  Locators.First;
  ClearFilterButton.Enabled := False;
  FilterRecordsButton.Enabled := True;
  SearchEdit.Text := '';
end;

procedure TUnRoutingForm.CloseDataSets;
begin
  inherited;
  Locators.Close;
end;

procedure TUnRoutingForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TUnRoutingForm.OpenDataSets;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourglass;
  Locators.Open;
end;

procedure TUnRoutingForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
end;

procedure TUnRoutingForm.FilterRecordsButtonClick(Sender: TObject);
begin
  inherited;
  if SearchEdit.Text <> '' then
  begin
    Locators.Filter := 'short_name like ''%' + SearchEdit.Text + '%'' ';
    Locators.Filtered := True;
    Locators.First;
    FilterRecordsButton.Enabled := False;
    ClearFilterButton.Enabled := True;
  end;
end;

procedure TUnRoutingForm.FormShow(Sender: TObject);
begin
  inherited;
  OpenDataSets;
end;

procedure TUnRoutingForm.btnUnrouteTop500Click(Sender: TObject);
var
  Cursor: IInterface;
  ID: Integer;
begin
  inherited;
   ID := Locators.FieldByName('emp_id').AsInteger;
  Cursor := ShowHourglass;
  AdminDM.Conn.BeginTrans;
  try
    Unroute500.Parameters.ParamValues['id'] := Id;
    Unroute500.Execute;
    AdminDM.Conn.CommitTrans;
    ShowMessage('Top 500 Locates have been un-routed for locator ID ' + IntToStr(Id));
    AdminDM.AdminChangeTable('Locate', 'Top 500 Locates unrouted for locator ID ' + IntToStr(ID), Now);  //qm-955 BP
  except
    AdminDM.Conn.RollbackTrans;
    raise;
  end
end;

end.

