inherited ActiveUsersForm: TActiveUsersForm
  Caption = 'Active Users'
  ClientHeight = 607
  ClientWidth = 1037
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 297
    Width = 1037
    Height = 9
    Cursor = crVSplit
    Align = alTop
  end
  inherited TopPanel: TPanel
    Width = 1037
    object btnEmpByCompany: TButton
      Left = 272
      Top = 8
      Width = 125
      Height = 25
      Caption = 'List Employees'
      Enabled = False
      TabOrder = 0
      OnClick = btnEmpByCompanyClick
    end
    object cbCompany: TDBLookupComboBox
      Left = 32
      Top = 10
      Width = 145
      Height = 21
      KeyField = 'name'
      ListField = 'name'
      ListSource = dsCompanies
      TabOrder = 1
      OnCloseUp = cbCompanyCloseUp
    end
    object cbEmpByCompanySearch: TCheckBox
      Left = 518
      Top = 12
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 2
      OnClick = cbEmpByCompanySearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 1037
    Height = 256
    Align = alTop
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OptionsCustomize.ColumnFiltering = True
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.CellEndEllipsis = True
      OptionsView.GroupByBox = True
      object cxGrid1DBTableView1fn: TcxGridDBColumn
        Caption = 'first name'
        DataBinding.FieldName = 'fn'
        Width = 83
      end
      object cxGrid1DBTableView1ln: TcxGridDBColumn
        Caption = 'last name'
        DataBinding.FieldName = 'ln'
        Width = 96
      end
      object cxGrid1DBTableView1middle_init: TcxGridDBColumn
        Caption = 'middle init'
        DataBinding.FieldName = 'middle_init'
        Width = 60
      end
      object cxGrid1DBTableView1short_name: TcxGridDBColumn
        Caption = 'short name'
        DataBinding.FieldName = 'short_name'
        Width = 155
      end
      object cxGrid1DBTableView1ad_username: TcxGridDBColumn
        Caption = 'ad username'
        DataBinding.FieldName = 'ad_username'
        Width = 90
      end
      object cxGrid1DBTableView1email_address: TcxGridDBColumn
        Caption = 'email address'
        DataBinding.FieldName = 'email_address'
        Width = 248
      end
      object cxGrid1DBTableView1emp_number: TcxGridDBColumn
        Caption = 'emp number'
        DataBinding.FieldName = 'emp_number'
      end
      object cxGrid1DBTableView1emp_id: TcxGridDBColumn
        Caption = 'emp id'
        DataBinding.FieldName = 'emp_id'
        Width = 81
      end
      object cxGrid1DBTableView1address_street: TcxGridDBColumn
        Caption = 'address street'
        DataBinding.FieldName = 'address_street'
      end
      object cxGrid1DBTableView1address_state: TcxGridDBColumn
        Caption = 'address state'
        DataBinding.FieldName = 'address_state'
        Width = 81
      end
      object cxGrid1DBTableView1address_zip: TcxGridDBColumn
        Caption = 'address zip'
        DataBinding.FieldName = 'address_zip'
        Width = 71
      end
      object cxGrid1DBTableView1phone: TcxGridDBColumn
        DataBinding.FieldName = 'phone'
        Width = 108
      end
      object cxGrid1DBTableView1contact_phone: TcxGridDBColumn
        Caption = 'contact phone'
        DataBinding.FieldName = 'contact_phone'
      end
      object cxGrid1DBTableView1comp_name: TcxGridDBColumn
        Caption = 'company name'
        DataBinding.FieldName = 'comp_name'
        Width = 113
      end
      object cxGrid1DBTableView1active: TcxGridDBColumn
        DataBinding.FieldName = 'active'
      end
      object cxGrid1DBTableView1timesheet_num: TcxGridDBColumn
        Caption = 'timesheet num'
        DataBinding.FieldName = 'timesheet_num'
        Width = 91
      end
      object cxGrid1DBTableView1uid: TcxGridDBColumn
        DataBinding.FieldName = 'uid'
      end
      object cxGrid1DBTableView1grp_id: TcxGridDBColumn
        Caption = 'grp id'
        DataBinding.FieldName = 'grp_id'
      end
      object cxGrid1DBTableView1login_id: TcxGridDBColumn
        Caption = 'login id'
        DataBinding.FieldName = 'login_id'
        Width = 64
      end
      object cxGrid1DBTableView1password: TcxGridDBColumn
        DataBinding.FieldName = 'password'
      end
      object cxGrid1DBTableView1chg_pwd: TcxGridDBColumn
        Caption = 'chg pwd'
        DataBinding.FieldName = 'chg_pwd'
        Width = 93
      end
      object cxGrid1DBTableView1chg_pwd_date: TcxGridDBColumn
        Caption = 'chg pwd date'
        DataBinding.FieldName = 'chg_pwd_date'
      end
      object cxGrid1DBTableView1last_login: TcxGridDBColumn
        DataBinding.FieldName = 'last_login'
        Width = 71
      end
      object cxGrid1DBTableView1active_ind: TcxGridDBColumn
        DataBinding.FieldName = 'active_ind'
      end
      object cxGrid1DBTableView1can_view_notes: TcxGridDBColumn
        DataBinding.FieldName = 'can_view_notes'
        Width = 55
      end
      object cxGrid1DBTableView1can_view_history: TcxGridDBColumn
        DataBinding.FieldName = 'can_view_history'
        Width = 53
      end
      object cxGrid1DBTableView1modified_date: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
      end
      object cxGrid1DBTableView1etl_access: TcxGridDBColumn
        DataBinding.FieldName = 'etl_access'
        Width = 67
      end
      object cxGrid1DBTableView1api_key: TcxGridDBColumn
        DataBinding.FieldName = 'api_key'
      end
      object cxGrid1DBTableView1account_disabled_date: TcxGridDBColumn
        DataBinding.FieldName = 'account_disabled_date'
        Width = 122
      end
      object cxGrid1DBTableView1type_id: TcxGridDBColumn
        DataBinding.FieldName = 'type_id'
      end
      object cxGrid1DBTableView1status_id: TcxGridDBColumn
        DataBinding.FieldName = 'status_id'
      end
      object cxGrid1DBTableView1timesheet_id: TcxGridDBColumn
        DataBinding.FieldName = 'timesheet_id'
        Width = 85
      end
      object cxGrid1DBTableView1create_date: TcxGridDBColumn
        DataBinding.FieldName = 'create_date'
      end
      object cxGrid1DBTableView1create_uid: TcxGridDBColumn
        DataBinding.FieldName = 'create_uid'
      end
      object cxGrid1DBTableView1modified_uid: TcxGridDBColumn
        DataBinding.FieldName = 'modified_uid'
        Width = 73
      end
      object cxGrid1DBTableView1charge_cov: TcxGridDBColumn
        DataBinding.FieldName = 'charge_cov'
        Width = 44
      end
      object cxGrid1DBTableView1can_receive_tickets: TcxGridDBColumn
        DataBinding.FieldName = 'can_receive_tickets'
        Width = 46
      end
      object cxGrid1DBTableView1timerule_id: TcxGridDBColumn
        DataBinding.FieldName = 'timerule_id'
      end
      object cxGrid1DBTableView1dialup_user: TcxGridDBColumn
        DataBinding.FieldName = 'dialup_user'
        Width = 77
      end
      object cxGrid1DBTableView1company_car: TcxGridDBColumn
        DataBinding.FieldName = 'company_car'
        Width = 74
      end
      object cxGrid1DBTableView1repr_pc_code: TcxGridDBColumn
        DataBinding.FieldName = 'repr_pc_code'
      end
      object cxGrid1DBTableView1payroll_pc_code: TcxGridDBColumn
        DataBinding.FieldName = 'payroll_pc_code'
      end
      object cxGrid1DBTableView1crew_num: TcxGridDBColumn
        DataBinding.FieldName = 'crew_num'
        Width = 57
      end
      object cxGrid1DBTableView1autoclose: TcxGridDBColumn
        DataBinding.FieldName = 'autoclose'
      end
      object cxGrid1DBTableView1company_id: TcxGridDBColumn
        DataBinding.FieldName = 'company_id'
        Width = 88
      end
      object cxGrid1DBTableView1rights_modified_date: TcxGridDBColumn
        DataBinding.FieldName = 'rights_modified_date'
      end
      object cxGrid1DBTableView1ticket_view_limit: TcxGridDBColumn
        DataBinding.FieldName = 'ticket_view_limit'
      end
      object cxGrid1DBTableView1hire_date: TcxGridDBColumn
        DataBinding.FieldName = 'hire_date'
      end
      object cxGrid1DBTableView1show_future_tickets: TcxGridDBColumn
        DataBinding.FieldName = 'show_future_tickets'
      end
      object cxGrid1DBTableView1incentive_pay: TcxGridDBColumn
        DataBinding.FieldName = 'incentive_pay'
        Width = 53
      end
      object cxGrid1DBTableView1local_utc_bias: TcxGridDBColumn
        DataBinding.FieldName = 'local_utc_bias'
        Width = 72
      end
      object cxGrid1DBTableView1test_com_login: TcxGridDBColumn
        DataBinding.FieldName = 'test_com_login'
      end
      object cxGrid1DBTableView1adp_login: TcxGridDBColumn
        DataBinding.FieldName = 'adp_login'
      end
      object cxGrid1DBTableView1home_lat: TcxGridDBColumn
        DataBinding.FieldName = 'home_lat'
      end
      object cxGrid1DBTableView1home_lng: TcxGridDBColumn
        DataBinding.FieldName = 'home_lng'
      end
      object cxGrid1DBTableView1alt_lat: TcxGridDBColumn
        DataBinding.FieldName = 'alt_lat'
      end
      object cxGrid1DBTableView1alt_lng: TcxGridDBColumn
        DataBinding.FieldName = 'alt_lng'
      end
      object cxGrid1DBTableView1plat_update_date: TcxGridDBColumn
        DataBinding.FieldName = 'plat_update_date'
      end
      object cxGrid1DBTableView1plat_age: TcxGridDBColumn
        DataBinding.FieldName = 'plat_age'
      end
      object cxGrid1DBTableView1changeby: TcxGridDBColumn
        DataBinding.FieldName = 'changeby'
        Width = 133
      end
      object cxGrid1DBTableView1phoneco_ref: TcxGridDBColumn
        DataBinding.FieldName = 'phoneco_ref'
        Width = 76
      end
      object cxGrid1DBTableView1atlas_number: TcxGridDBColumn
        DataBinding.FieldName = 'atlas_number'
        Width = 110
      end
      object cxGrid1DBTableView1first_task_reminder: TcxGridDBColumn
        DataBinding.FieldName = 'first_task_reminder'
      end
      object cxGrid1DBTableView1terminated: TcxGridDBColumn
        DataBinding.FieldName = 'terminated'
      end
      object cxGrid1DBTableView1termination_date: TcxGridDBColumn
        DataBinding.FieldName = 'termination_date'
        Width = 106
      end
      object cxGrid1DBTableView1logo_filename: TcxGridDBColumn
        DataBinding.FieldName = 'logo_filename'
        Width = 109
      end
      object cxGrid1DBTableView1address_city: TcxGridDBColumn
        DataBinding.FieldName = 'address_city'
      end
      object cxGrid1DBTableView1billing_footer: TcxGridDBColumn
        DataBinding.FieldName = 'billing_footer'
        Width = 105
      end
      object cxGrid1DBTableView1floating_holidays_per_year: TcxGridDBColumn
        DataBinding.FieldName = 'floating_holidays_per_year'
      end
      object cxGrid1DBTableView1payroll_company_code: TcxGridDBColumn
        DataBinding.FieldName = 'payroll_company_code'
      end
      object cxGrid1DBTableView1report_to: TcxGridDBColumn
        DataBinding.FieldName = 'report_to'
      end
      object cxGrid1DBTableView1report_to_number: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_number'
        Width = 101
      end
      object cxGrid1DBTableView1report_to_first_name: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_first_name'
      end
      object cxGrid1DBTableView1report_to_last_name: TcxGridDBColumn
        DataBinding.FieldName = 'report_to_last_name'
        Width = 143
      end
      object cxGrid1DBTableView1manager_number: TcxGridDBColumn
        DataBinding.FieldName = 'manager_number'
      end
      object cxGrid1DBTableView1manager_first_name: TcxGridDBColumn
        DataBinding.FieldName = 'manager_first_name'
      end
      object cxGrid1DBTableView1manager_last_name: TcxGridDBColumn
        DataBinding.FieldName = 'manager_last_name'
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 306
    Width = 1037
    Height = 95
    Align = alTop
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    object P: TPageControl
      Left = 1
      Top = 1
      Width = 1035
      Height = 93
      Cursor = crCross
      ActivePage = ADINfoSheet
      Align = alClient
      TabOrder = 0
      object ADINfoSheet: TTabSheet
        AlignWithMargins = True
        Caption = 'AD Info'
        ImageIndex = 2
        object DBGrid1: TDBGrid
          Left = 120
          Top = 3
          Width = 885
          Height = 54
          DataSource = dsADActiveUsers
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
        object btnADInfo: TButton
          Left = 12
          Top = 21
          Width = 88
          Height = 25
          Action = btnAction
          Caption = 'AD Info'
          TabOrder = 1
          OnClick = btnADInfoClick
        end
      end
    end
  end
  object cxGrid1: TcxGrid [4]
    Left = 0
    Top = 442
    Width = 1037
    Height = 165
    Align = alClient
    TabOrder = 3
    OnExit = GridExit
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object InactiveGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = dsInactiveEmp
      DataController.Filter.MaxValueListCount = 10
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.MRUItemsList = False
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.CellEndEllipsis = True
      OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object InactiveGridViewemp_id: TcxGridDBColumn
        Caption = 'emp id'
        DataBinding.FieldName = 'emp_id'
      end
      object InactiveGridViewemp_number: TcxGridDBColumn
        Caption = 'emp number'
        DataBinding.FieldName = 'emp_number'
        Width = 82
      end
      object InactiveGridViewfirst_name: TcxGridDBColumn
        Caption = 'first name'
        DataBinding.FieldName = 'first_name'
        Width = 93
      end
      object InactiveGridViewlast_name: TcxGridDBColumn
        Caption = 'last name'
        DataBinding.FieldName = 'last_name'
        Width = 97
      end
      object InactiveGridViewshort_name: TcxGridDBColumn
        Caption = 'short name'
        DataBinding.FieldName = 'short_name'
        Width = 96
      end
      object InactiveGridViewtype_id: TcxGridDBColumn
        Caption = 'type id'
        DataBinding.FieldName = 'type_id'
      end
      object InactiveGridViewtimesheet_id: TcxGridDBColumn
        Caption = 'timesheet id'
        DataBinding.FieldName = 'timesheet_id'
        Width = 85
      end
      object InactiveGridViewcompany_id: TcxGridDBColumn
        Caption = 'company id'
        DataBinding.FieldName = 'company_id'
        Width = 87
      end
      object InactiveGridViewemp_active: TcxGridDBColumn
        Caption = 'active'
        DataBinding.FieldName = 'emp_active'
        Width = 42
      end
      object InactiveGridViewaccount_disabled_date: TcxGridDBColumn
        Caption = 'disabled date'
        DataBinding.FieldName = 'account_disabled_date'
        Width = 147
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = InactiveGridView
    end
  end
  object Panel2: TPanel [5]
    Left = 0
    Top = 401
    Width = 1037
    Height = 41
    Align = alTop
    TabOrder = 4
    object btnInactiveEmp: TButton
      Left = 48
      Top = 5
      Width = 217
      Height = 25
      Caption = 'List Past 7 Days Inactive Employees'
      TabOrder = 0
      OnClick = btnInactiveEmpClick
    end
    object cbInactiveSearch: TCheckBox
      Left = 518
      Top = 10
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbInactiveSearchClick
    end
    object btnInactiveEmpInfo: TButton
      Left = 328
      Top = 6
      Width = 111
      Height = 25
      Caption = 'Emp Info'
      TabOrder = 2
      OnClick = btnInactiveEmpInfoClick
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select lc.name as comp_name, uqv.timesheet_num as timesheet_num,' +
      ' '#13#10'e.first_name as fn, e.last_name as ln, * from users u '#13#10'inner' +
      ' join employee e on u.emp_id = e.emp_id '#13#10'join locating_company ' +
      'lc on lc.company_id = e.company_id '#13#10'inner join uqview_employees' +
      ' uqv on uqv.employee_number = e.emp_number '#13#10'where u.active_ind ' +
      '= 1 and u.login_id IS NOT NULL '#13#10'and e.ad_username IS NOT NULL a' +
      'nd e.ad_username <> '#39#39' '#13#10'and e.ad_username <> '#39'NULL'#39' and lc.name' +
      ' = :company'#13#10'order by uqv.timesheet_num'
    Parameters = <
      item
        Name = 'company'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end>
    Left = 43
    Top = 160
  end
  inherited DS: TDataSource
    Left = 120
    Top = 160
  end
  object Companies: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select distinct name from locating_company order by name'
    Parameters = <>
    Left = 197
    Top = 157
  end
  object ADActiveUsers: TADODataSet
    Connection = ConnAD
    CommandText = 
      'select lastlogon, lastlogontimestamp, samaccountname, '#13#10'useracco' +
      'untcontrol, givenname, sn, company '#13#10'from LDAP://ou=Sales,dc=MyD' +
      'omain,dc=com'#13#10'where sAMAccountName= :adusername'
    Parameters = <
      item
        Name = 'adusername'
        Size = -1
        Value = Null
      end>
    Left = 310
    Top = 217
  end
  object dsADActiveUsers: TDataSource
    DataSet = ADActiveUsers
    Left = 370
    Top = 218
  end
  object dsCompanies: TDataSource
    DataSet = Companies
    Left = 272
    Top = 157
  end
  object ActionList1: TActionList
    Left = 384
    Top = 160
    object btnAction: TAction
      Caption = 'btnAction'
      OnUpdate = btnActionUpdate
    end
  end
  object ConnAD: TADOConnection
    ConnectionString = 
      'Provider=ADsDSOObject;User ID=dynutil\UTQ-ARS;Encrypt Password=F' +
      'alse;Location="LDAP://dynutil.com/ou=company,dc=dynutil,dc=com";' +
      'Mode=Read;Bind Flags=0;ADSI Flag=-2147483648;'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'ADsDSOObject'
    Left = 62
    Top = 216
  end
  object InactiveEmp: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select e.emp_id, e.emp_number, e.first_name, e.last_name, e.shor' +
      't_name, '#13#10'     e.type_id, e.timesheet_id, e.active as emp_active' +
      ', e.company_id, u.account_disabled_date from users u'#13#10'     inner' +
      ' join employee e on u.emp_id = e.emp_id where DATEDIFF(D,account' +
      '_disabled_date, GETDATE()) >= 7 '#13#10'     and e.active = 1 order by' +
      ' short_name'
    Parameters = <>
    Left = 137
    Top = 213
    object InactiveEmpemp_id: TAutoIncField
      FieldName = 'emp_id'
      ReadOnly = True
    end
    object InactiveEmpemp_number: TStringField
      FieldName = 'emp_number'
      Size = 15
    end
    object InactiveEmpfirst_name: TStringField
      FieldName = 'first_name'
    end
    object InactiveEmplast_name: TStringField
      FieldName = 'last_name'
      Size = 30
    end
    object InactiveEmpshort_name: TStringField
      FieldName = 'short_name'
      Size = 30
    end
    object InactiveEmptype_id: TIntegerField
      FieldName = 'type_id'
    end
    object InactiveEmptimesheet_id: TIntegerField
      FieldName = 'timesheet_id'
    end
    object InactiveEmpemp_active: TBooleanField
      FieldName = 'emp_active'
    end
    object InactiveEmpcompany_id: TIntegerField
      FieldName = 'company_id'
    end
    object InactiveEmpaccount_disabled_date: TDateTimeField
      FieldName = 'account_disabled_date'
    end
  end
  object dsInactiveEmp: TDataSource
    DataSet = InactiveEmp
    Left = 225
    Top = 214
  end
end
