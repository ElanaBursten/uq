inherited MarsForm: TMarsForm
  Caption = 'Mars'
  ClientWidth = 725
  KeyPreview = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 725
    Height = 26
    object MarsFilter: TCheckBox
      Left = 16
      Top = 5
      Width = 147
      Height = 16
      Caption = 'Active Mars Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = MarsFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 26
    Width = 725
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = True
      object ColTreeEmpID: TcxGridDBColumn
        Caption = 'Tree EmpID'
        DataBinding.FieldName = 'tree_emp_id'
        DataBinding.IsNullValueType = True
      end
      object ColNewStatus: TcxGridDBColumn
        DataBinding.FieldName = 'new_status'
        DataBinding.IsNullValueType = True
        Width = 68
      end
      object ColINsertDate: TcxGridDBColumn
        Caption = 'Insert Date'
        DataBinding.FieldName = 'insert_date'
        DataBinding.IsNullValueType = True
        Width = 128
      end
      object ColInsertedBy: TcxGridDBColumn
        Caption = 'Inserted By'
        DataBinding.FieldName = 'inserted_by'
        DataBinding.IsNullValueType = True
        Width = 66
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        Width = 39
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorLocation = clUseServer
    OnNewRecord = DataNewRecord
    CommandText = 'select * from mars where active = 1'
    Left = 432
  end
  inherited DS: TDataSource
    Left = 480
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 648
    Top = 48
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGrayText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = cl3DDkShadow
    end
  end
end
