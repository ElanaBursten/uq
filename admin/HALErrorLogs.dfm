inherited ErrorLogsForm: TErrorLogsForm
  Caption = 'Error Logs'
  ClientHeight = 440
  ClientWidth = 700
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 357
    Width = 700
    Height = 7
    Cursor = crVSplit
    Align = alBottom
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 700
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 446
      Top = 15
      Width = 236
      Height = 13
      Caption = 'base Directory:  \\dyatl-pqmras01\E$\QM\Tickets'
    end
    object btnErrorLogs: TButton
      Left = 26
      Top = 7
      Width = 143
      Height = 24
      Caption = 'Check  for Error Logs'
      TabOrder = 0
      OnClick = btnErrorLogsClick
    end
  end
  object lvErrorLog: TListView
    Left = 0
    Top = 41
    Width = 700
    Height = 316
    Align = alClient
    Columns = <
      item
        Caption = 'Folder'
        Width = 200
      end
      item
        Caption = 'Error Log'
        Width = 555
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    RowSelect = True
    ParentFont = False
    SortType = stText
    TabOrder = 1
    ViewStyle = vsReport
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 364
    Width = 700
    Height = 76
    Align = alBottom
    TabOrder = 2
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 90
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select hal_module, emails  from hal '#13#10'where hal_module = '#39'error_' +
      'logs'#39
    Parameters = <>
    Left = 556
    Top = 88
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 620
    Top = 88
  end
end
