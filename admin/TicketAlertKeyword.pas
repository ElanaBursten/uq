unit TicketAlertKeyword;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMasterDetail, ActnList, DB, ADODB, StdCtrls, ExtCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxDropDownEdit, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, System.Actions,
  dxDateRanges, AdminDMu, dxScrollbarAnnotations;

type
  TTicketAlertKeywordForm = class(TBaseMasterDetailForm)
    ColMasterAlertTicketTypeID: TcxGridDBColumn;
    ColMasterCallCenter: TcxGridDBColumn;
    ColMasterTicketType: TcxGridDBColumn;
    ColMasterActive: TcxGridDBColumn;
    ColDetailAlertTicketKeywordID: TcxGridDBColumn;
    ColDetailAlertTicketTypeID: TcxGridDBColumn;
    ColDetailKeyword: TcxGridDBColumn;
    ActiveTicketAlertFilter: TCheckBox;
    procedure MasterDeleteActionExecute(Sender: TObject);
    procedure ActiveTicketAlertFilterClick(Sender: TObject);
    procedure MasterGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure MasterNewRecord(DataSet: TDataSet);
    procedure MasterAfterCancel(DataSet: TDataSet);
    procedure DetailAfterCancel(DataSet: TDataSet);
    procedure MasterBeforeInsert(DataSet: TDataSet);
    procedure DetailBeforeInsert(DataSet: TDataSet);
    procedure MasterBeforeEdit(DataSet: TDataSet);
    procedure DetailBeforeEdit(DataSet: TDataSet);
    procedure MasterAfterPost(DataSet: TDataSet);
    procedure DetailAfterPost(DataSet: TDataSet);
  private
    atkArray: TFieldValuesArray;
    attInserted: Boolean;
    atkInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  OdDbUtils, QMConst, OdMiscUtils, OdCxUtils;

{$R *.dfm}

procedure TTicketAlertKeywordForm.ActiveTicketAlertFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Master.FieldByName('alert_ticket_type_id').AsInteger;
  Master.Close;
  if ActiveTicketAlertFilter.Checked then
    Master.CommandText := 'select * from alert_ticket_type where active = 1' else
      Master.CommandText := 'select * from alert_ticket_type';
  Master.Open;
  Master.Locate('alert_ticket_type_id', FSelectedID, []);
end;

procedure TTicketAlertKeywordForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
     inherited;
end;

procedure TTicketAlertKeywordForm.ActivatingNow;
begin
  inherited;
  attInserted := False;
  atkInserted := False;
end;

procedure TTicketAlertKeywordForm.LeavingNow;
begin
  inherited;
   Master.Close;
   Detail.Close;
end;

procedure TTicketAlertKeywordForm.MasterBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
   AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TTicketAlertKeywordForm.MasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  attInserted := False;
end;

procedure TTicketAlertKeywordForm.MasterBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  attInserted := True;
end;

procedure TTicketAlertKeywordForm.MasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'alert_ticket_type', 'alert_ticket_type_id', attInserted)
end;

procedure TTicketAlertKeywordForm.DetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  atkInserted := False;
end;

procedure TTicketAlertKeywordForm.DetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
   AdminDM.StoreFieldValues(DataSet, atkArray);
end;

procedure TTicketAlertKeywordForm.DetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  atkInserted := True;
end;

procedure TTicketAlertKeywordForm.DetailAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset, AtkArray, 'alert_ticket_keyword', 'alert_ticket_keyword_id', atkInserted)
end;

procedure TTicketAlertKeywordForm.MasterDeleteActionExecute(Sender: TObject);
begin
  EditDataSet(Master);
  Master.FieldByName('active').Value := False;
  PostDataSet(Master);
end;

procedure TTicketAlertKeywordForm.MasterGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColMasterActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TTicketAlertKeywordForm.MasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TTicketAlertKeywordForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.ConfigDM.GetCallCenterList(CxComboBoxItems(ColMasterCallCenter));
end;

procedure TTicketAlertKeywordForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveTicketAlertFilter.Enabled := True;
end;

end.
