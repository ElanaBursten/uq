inherited QtrMinForm: TQtrMinForm
  Left = 531
  Top = 130
  Caption = 'Quarter-Minute Map Routing'
  ClientHeight = 445
  ClientWidth = 646
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 312
    Top = 41
    Width = 5
    Height = 404
    Align = alRight
    ResizeStyle = rsUpdate
  end
  object Panel2: TPanel [1]
    Left = 0
    Top = 0
    Width = 646
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object StatusLabel: TLabel
      Left = 128
      Top = 16
      Width = 24
      Height = 13
      Caption = '------'
    end
    object Label1: TLabel
      Left = 216
      Top = 14
      Width = 371
      Height = 13
      Caption = 
        'This screen is a work-in-progress, it is not yet useful. Record ' +
        'Limit is set to 30'
    end
    object PopulateButton: TButton
      Left = 16
      Top = 8
      Width = 89
      Height = 25
      Caption = 'Populate Grid'
      TabOrder = 0
      OnClick = PopulateButtonClick
    end
  end
  object TheGrid: TStringGrid [2]
    Left = 0
    Top = 41
    Width = 312
    Height = 404
    Align = alClient
    DefaultColWidth = 45
    DefaultRowHeight = 32
    TabOrder = 1
    OnDblClick = TheGridDblClick
    OnDrawCell = TheGridDrawCell
  end
  object ScrollBox1: TScrollBox [3]
    Left = 317
    Top = 41
    Width = 329
    Height = 404
    HorzScrollBar.Smooth = True
    HorzScrollBar.Style = ssHotTrack
    HorzScrollBar.Tracking = True
    VertScrollBar.Smooth = True
    VertScrollBar.Style = ssHotTrack
    VertScrollBar.Tracking = True
    Align = alRight
    TabOrder = 2
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 210
      Height = 37
      OnMouseDown = Image1MouseDown
      OnMouseMove = Image1MouseMove
    end
  end
  object QtrMin: TADODataSet [4]
    CacheSize = 1000
    Connection = AdminDM.Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select * from map_qtrmin'#13#10'where qtrmin_long > '#39'65'#39' and qtrmin_lo' +
      'ng < '#39'99'#39' and'#13#10'           qtrmin_lat > '#39'20'#39' and qtrmin_lat < '#39'50' +
      #39#13#10
    Parameters = <>
    Left = 24
    Top = 368
  end
  object QtrMinDS: TDataSource [5]
    DataSet = QtrMin
    Left = 104
    Top = 368
  end
  object locator: TADODataSet [6]
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select emp_id, short_name from employee'#13#10'order by'#13#10'short_name'
    Parameters = <>
    Left = 224
    Top = 368
  end
  object locatorDs: TDataSource [7]
    DataSet = locator
    Left = 272
    Top = 368
  end
  object FindLats: TADODataSet [8]
    Connection = AdminDM.Conn
    CommandText = 
      'select max(qtrmin_lat), min(qtrmin_lat) from map_qtrmin where qt' +
      'rmin_lat > '#39'20'#39' and qtrmin_lat < '#39'50'#39
    Parameters = <>
    Left = 168
    Top = 256
  end
  object FindLongs: TADODataSet [9]
    Connection = AdminDM.Conn
    CommandText = 
      'select max(qtrmin_long), min(qtrmin_long) from map_qtrmin where ' +
      'qtrmin_long > '#39'65'#39' and qtrmin_long < '#39'99'#39
    Parameters = <>
    Left = 200
    Top = 256
  end
  inherited GetAreaRepr: TADODataSet
    Connection = nil
    CommandText = 
      'select area_id, area_name, short_name'#13#10' from area a left join em' +
      'ployee e on a.locator_id=e.emp_id'#13#10'order by a.area_id'
  end
end
