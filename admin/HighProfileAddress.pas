unit HighProfileAddress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit,
  cxMaskEdit, cxGridCustomTableView, cxGridTableView, cxControls, cxGridCustomView,
  cxClasses, cxGridLevel, cxGrid, cxGridDBTableView, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  THighProfileAddressForm = class(TBaseCxListForm)
    Label1: TLabel;
    CallCenterFilter: TComboBox;
    Colcall_center: TcxGridDBColumn;
    Colclient_code: TcxGridDBColumn;
    Colstreet: TcxGridDBColumn;
    Colcity: TcxGridDBColumn;
    Colstate: TcxGridDBColumn;
    Colcounty: TcxGridDBColumn;
    Colhp_address_id: TcxGridDBColumn;
    cbHPAddressSearch: TCheckBox;
    procedure DataNewRecord(DataSet: TDataSet);
    procedure CallCenterFilterSelect(Sender: TObject);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure cbHPAddressSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    private
      hpaInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdExceptions, OdCxUtils;

{$R *.dfm}

{ THighProfileAddressForm }

procedure THighProfileAddressForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetCallCenterList(CallCenterFilter.Items);
  AdminDM.GetCallCenterList(CxComboBoxItems(Colcall_center));
  AdminDM.GetClientList(CxComboBoxItems(Colclient_code));
  AdminDM.GetStateList(CxComboBoxItems(Colstate));
  CallCenterFilter.Items.Insert(0, '(ALL)');
  CallCenterFilter.ItemIndex := 0;
end;

procedure THighProfileAddressForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbHPAddressSearch.Enabled := True;
end;

procedure THighProfileAddressForm.cbHPAddressSearchClick(Sender: TObject);
begin
  inherited;
  if cbHPAddressSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure THighProfileAddressForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  hpaInserted := False;
end;

procedure THighProfileAddressForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'hp_address', 'hp_address_id', hpaInserted);
end;

procedure THighProfileAddressForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure THighProfileAddressForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if Data.IsEmpty and (CallCenterFilter.Text = '(ALL)') then begin
    CallCenterFilter.SetFocus;
    raise EOdDataEntryError.Create('Please select a call center before adding a new row.');
  end;
  hpaInserted := True;
end;

procedure THighProfileAddressForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  if CallCenterFilter.Text <> '(ALL)' then
    Data.FieldByName('call_center').AsString := CallCenterFilter.Text;

  Data.FieldByName('county').AsString := '*';
end;

procedure THighProfileAddressForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure THighProfileAddressForm.ActivatingNow;
begin
  inherited;
  hpaInserted := False;
end;

procedure THighProfileAddressForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure THighProfileAddressForm.CallCenterFilterSelect(Sender: TObject);
begin
  if CallCenterFilter.Text = '(ALL)' then begin
    Data.Filter := '';
    Data.Filtered := False;
  end else begin
    Data.Filter := Format('call_center = ''%s''', [CallCenterFilter.Text]);
    Data.Filtered := True;
    GridView.DataController.Groups.FullExpand;
  end;
end;

end.
