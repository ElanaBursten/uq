unit Esketch;    //QM-203  SR

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, BaseCxList,
  cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.ExtDlgs, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, Vcl.StdCtrls, Vcl.ComCtrls, AdminDMu, dxSkinBasic, dxSkinsCore;

type
  TfrmESketchCodes = class(TBaseCxListForm)
    qryEsketchCodes: TADOQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsEsketchCodes: TDataSource;
    SaveQMTextFileDlg: TSaveTextFileDialog;
    cxGrid1DBTableView1oc_code: TcxGridDBColumn;
    cxGrid1DBTableView1customer_description: TcxGridDBColumn;
    cxGrid1DBTableView1utility_type: TcxGridDBColumn;
    cxGrid1DBTableView1customer_name: TcxGridDBColumn;
    cxGrid1DBTableView1active: TcxGridDBColumn;
    btnExport: TButton;
    StatusBar1: TStatusBar;
    procedure btnExportClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    procedure ExporteSketchCodes;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmESketchCodes: TfrmESketchCodes;

implementation
Uses StrUtils, Types;
{$R *.dfm}

procedure TfrmESketchCodes.btnExportClick(Sender: TObject);
begin
  If SaveQMTextFileDlg.Execute then ExporteSketchCodes;
end;

procedure TfrmESketchCodes.ExporteSketchCodes;
var
  lineText, OutputFile: string;
  part : string;
  myFile: TextFile;
  cnt : integer;
  descripArray:TStringDynArray;
const
  SEP1 = ', ';
begin
  try
    cnt:= 0;
    OutputFile := SaveQMTextFileDlg.FileName;
    StatusBar1.Panels[1].text := OutputFile;


    AssignFile(myFile, OutputFile);
    ReWrite(myFile);
    btnExport.Cursor := crHourGlass;
    with qryEsketchCodes do
    begin
      first;
      while not eof do
      begin
        descripArray:= SplitString(FieldByName('customer_description').AsString, '|');
        lineText := FieldByName('oc_code').Value + SEP1 +
                    descripArray[1] + SEP1 +
          FieldByName('utility_type').Value + SEP1 +
          descripArray[0];
        WriteLn(myFile, lineText);

        next;
        inc(cnt);
        StatusBar1.Panels[3].text := IntToStr(cnt);
        StatusBar1.Refresh;
      end; // while not eof do
    end; // with spAssetTracker do
  finally
    btnExport.Cursor := crDefault;
    btnExport.Refresh;
    qryEsketchCodes.Close;
    CloseFile(myFile);
    Showmessage('Export complete');
  end;
end;


procedure TfrmESketchCodes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryEsketchCodes.Active:= false;
end;

procedure TfrmESketchCodes.FormShow(Sender: TObject);
begin
  qryEsketchCodes.open;
end;

end.
