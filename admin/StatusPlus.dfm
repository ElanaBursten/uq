inherited StatusPlusForm: TStatusPlusForm
  Caption = 'Status Plus'
  ClientWidth = 950
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 950
    Height = 29
    object ActiveSPFilter: TCheckBox
      Left = 317
      Top = 6
      Width = 136
      Height = 17
      Caption = 'Active Status Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveSPFilterClick
    end
    object chkboxStatusPlusSearch: TCheckBox
      Left = 52
      Top = 6
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = chkboxStatusPlusSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 29
    Width = 950
    Height = 371
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      OptionsCustomize.GroupBySorting = True
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        Width = 95
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client Code'
        DataBinding.FieldName = 'client_code'
        Width = 102
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Width = 95
      end
      object ColStatusPlus: TcxGridDBColumn
        Caption = 'Status Plus'
        DataBinding.FieldName = 'status_plus'
        Width = 354
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Width = 60
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'Select  * from  status_plus where active = 1'
  end
end
