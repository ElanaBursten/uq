unit BillingRulesCustomer;
 //qm-996 wired ADOConn
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseMasterDetail, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, System.Actions, Vcl.ActnList,
  Data.Win.ADODB, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxDBLookupComboBox, cxDBExtLookupComboBox, cxMaskEdit, AdminDmu,
  dxCore, dxSkinsForm, dxSkinsCore, dxSkinBasic;

type
  TBillingRulesCustomerForm = class(TBaseMasterDetailForm)
    ActiveCustomerFilter: TCheckBox;
    ColMasterCustomerID: TcxGridDBColumn;
    ColMasterRuleID: TcxGridDBColumn;
    ColMasterRuleStatus: TcxGridDBColumn;
    ColMasterActive: TcxGridDBColumn;
    ColDetailRuleID: TcxGridDBColumn;
    ColDetailRuleExecutionSequence: TcxGridDBColumn;
    ColDetailRuleHeaderDescription: TcxGridDBColumn;
    ColDetailRuleDetailDescription: TcxGridDBColumn;
    ColDetalRuleSql: TcxGridDBColumn;
    ColDetailRuleParameters: TcxGridDBColumn;
    ColDetailShowColumns: TcxGridDBColumn;
    ColDetailEditColumns: TcxGridDBColumn;
    ColDetailActive: TcxGridDBColumn;
    ColDetailRuleButtonText: TcxGridDBColumn;
    ColDtailUpdateBillingSql: TcxGridDBColumn;
    ColDetailUpdateLocateSql: TcxGridDBColumn;
    dsLookupCust: TDataSource;
    qryLookupCust: TADOQuery;
    ColMasterCustomerName: TcxGridDBColumn;
    CustLookupView: TcxGridDBTableView;
    CustLookupViewCustomerID: TcxGridDBColumn;
    CustLookupViewCustName: TcxGridDBColumn;
    Detailrule_id: TIntegerField;
    Detailrule_execution_sequence: TIntegerField;
    Detailrule_header_description: TStringField;
    Detailrule_detail_description: TMemoField;
    Detailrule_sql: TMemoField;
    Detailrule_parameters: TStringField;
    Detailshow_columns: TMemoField;
    Detailedit_columns: TMemoField;
    Detailactive: TBooleanField;
    Detailmodified_by: TStringField;
    Detailmodified_date: TDateTimeField;
    Detailrule_button_text: TStringField;
    Detailupdate_billing_sql: TMemoField;
    Detailupdate_locate_sql: TMemoField;
    chkboxBillingRulesCustSearch: TCheckBox;
    procedure ActiveCustomerFilterClick(Sender: TObject);
    procedure MasterGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DetailGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure MasterBeforePost(DataSet: TDataSet);
    procedure DetailBeforePost(DataSet: TDataSet);
    procedure DetailNewRecord(DataSet: TDataSet);
    procedure chkboxBillingRulesCustSearchClick(Sender: TObject);
    procedure MasterAfterCancel(DataSet: TDataSet);
    procedure DetailAfterCancel(DataSet: TDataSet);
    procedure MasterBeforeInsert(DataSet: TDataSet);
    procedure DetailBeforeInsert(DataSet: TDataSet);
    procedure MasterBeforeEdit(DataSet: TDataSet);
    procedure DetailBeforeEdit(DataSet: TDataSet);
    procedure MasterAfterPost(DataSet: TDataSet);
    procedure DetailAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    BillInserted, BillDetInserted: Boolean;
    BillRulesDetailArray: TFieldValuesArray;
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  BillingRulesCustomerForm: TBillingRulesCustomerForm;

implementation

{$R *.dfm}
uses
 OdDbUtils, OdMiscUtils;

procedure TBillingRulesCustomerForm.ActivatingNow;
begin
  inherited;
  BillInserted := False;
  BillDetInserted := False;
end;

procedure TBillingRulesCustomerForm.ActiveCustomerFilterClick(Sender: TObject);
  var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Master.FieldByName('rule_id').AsInteger;
  Master.Close;
  if ActiveCustomerFilter.Checked then
    Master.CommandText := 'select * from billing_rules_customer where active = 1 order by customer_id, rule_id' else
      Master.CommandText := 'select * from billing_rules_customer order by customer_id, rule_id';
  Master.Open;
  Master.Locate('rule_id', FSelectedID, []);
end;

procedure TBillingRulesCustomerForm.DetailGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColDetailActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TBillingRulesCustomerForm.DetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  Detail.FieldByName('Rule_ID').AsVariant := NULL;
end;

procedure TBillingRulesCustomerForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TBillingRulesCustomerForm.LeavingNow;
begin
  inherited;
   Master.Close;
   Detail.Close;
end;

procedure TBillingRulesCustomerForm.MasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  BillInserted := False;
end;

procedure TBillingRulesCustomerForm.MasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'billing_rules_customer', 'customer_id', BillInserted);
end;

procedure TBillingRulesCustomerForm.DetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,BillRulesDetailArray, 'billing_rules_detail', 'rule_id', BillDetInserted);
end;

procedure TBillingRulesCustomerForm.DetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  BillDetInserted := False;
end;

procedure TBillingRulesCustomerForm.MasterBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TBillingRulesCustomerForm.DetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, BillRulesDetailArray);
end;

procedure TBillingRulesCustomerForm.MasterBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  BillInserted := True;
end;

procedure TBillingRulesCustomerForm.DetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  BillDetInserted := True;
end;

procedure TBillingRulesCustomerForm.MasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  Master.FieldByName('modified_by').AsString := AdminDm.CurrentUser;
end;

procedure TBillingRulesCustomerForm.DetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  Detail.FieldByName('modified_by').AsString := AdminDm.CurrentUser;
end;

procedure TBillingRulesCustomerForm.MasterGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
 var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColMasterActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TBillingRulesCustomerForm.OpenDataSets;
begin
  qryLookupCust.Open;
  inherited;
end;

procedure TBillingRulesCustomerForm.CloseDataSets;
begin
  inherited;
  qryLookupCust.Close;
end;

procedure TBillingRulesCustomerForm.chkboxBillingRulesCustSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxBillingRulesCustSearch.Checked then
    MasterGridView.Controller.ShowFindPanel
  else
    MasterGridView.Controller.HideFindPanel;
end;

procedure TBillingRulesCustomerForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveCustomerFilter.Enabled := True;
  chkboxBillingRulesCustSearch.Enabled := True;
end;

end.
