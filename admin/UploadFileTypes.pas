unit UploadFileTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, Grids, DBGrids, StdCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxSpinEdit, cxCheckBox, cxCalendar, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TUploadFileTypesForm = class(TBaseCxListForm)
    ColUploadFileTypeID: TcxGridDBColumn;
    ColExtension: TcxGridDBColumn;
    ColMaxKilobytes: TcxGridDBColumn;
    ColSizeErrorMessage: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    cbFileTypesSearch: TCheckBox;
    procedure DataAfterOpen(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure cbFileTypesSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    UFTInserted: Boolean;
    procedure ExtensionSetText(Sender: TField; const Text: string);
  public
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  OdMiscUtils, OdDbUtils, JclStrings, AdminDmu;

{$R *.dfm}

procedure TUploadFileTypesForm.cbFileTypesSearchClick(Sender: TObject);
begin
  inherited;
  if cbFileTypesSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TUploadFileTypesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  UFTInserted := False;
end;

procedure TUploadFileTypesForm.DataAfterOpen(DataSet: TDataSet);
begin
  DataSet.FieldByName('extension').OnSetText := ExtensionSetText;
end;

procedure TUploadFileTypesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'upload_file_type', 'upload_file_type_id', UFTInserted);
end;

procedure TUploadFileTypesForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TUploadFileTypesForm.ActivatingNow;
begin
  inherited;
   UFTInserted := False;
end;

procedure TUploadFileTypesForm.ExtensionSetText(Sender: TField; const Text: string);
begin
  Sender.AsString := Trim(StrRemoveChars(Text, ['.', '*', ' ']));
end;

procedure TUploadFileTypesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TUploadFileTypesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbFileTypesSearch.Enabled := True;
end;

procedure TUploadFileTypesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
     //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TUploadFileTypesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  UFTInserted := True;
end;

procedure TUploadFileTypesForm.DataBeforePost(DataSet: TDataSet);
begin
  if DataSet.FieldByName('active').IsNull then
    DataSet.FieldByName('active').AsBoolean := True;
end;

end.
