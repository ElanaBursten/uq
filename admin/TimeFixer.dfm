inherited TimeFixerForm: TTimeFixerForm
  Left = 417
  Top = 193
  Caption = 'Fix New Jersey Times'
  ClientHeight = 300
  ClientWidth = 427
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 41
    Width = 427
    Height = 259
    Align = alClient
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Go'
      TabOrder = 0
      OnClick = Button1Click
    end
    object TestParsing: TButton
      Left = 120
      Top = 8
      Width = 81
      Height = 25
      Caption = 'Test Parsing'
      TabOrder = 1
      OnClick = TestParsingClick
    end
    object StopButton: TButton
      Left = 272
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 2
      OnClick = StopButtonClick
    end
  end
  object Tix: TADODataSet
    CacheSize = 50
    Connection = AdminDM.Conn
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    CommandText = 
      'select'#13#10' ticket_id, transmit_date, image'#13#10' from ticket'#13#10' where t' +
      'icket_format='#39'NewJersey'#39#13#10'  and datepart(hh, transmit_date)='#39'0'#39
    Parameters = <>
    Left = 24
    Top = 96
  end
  object Updater: TADOCommand
    CommandText = 
      'update ticket set transmit_date=:transmit_date'#13#10' where ticket_id' +
      ' = :ticket_id'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'transmit_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'ticket_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 64
    Top = 96
  end
end
