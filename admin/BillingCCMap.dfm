inherited BillingCCMapForm: TBillingCCMapForm
  Caption = 'Billing CC Map'
  ClientHeight = 462
  ClientWidth = 906
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 906
    object chkboxBillingccSearch: TCheckBox
      Left = 23
      Top = 15
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxBillingccSearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 906
    Height = 421
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      object ColCol_Center: TcxGridDBColumn
        Caption = 'call center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'cleint code'
        DataBinding.FieldName = 'client_code'
        DataBinding.IsNullValueType = True
      end
      object ColWorkState: TcxGridDBColumn
        Caption = 'work state'
        DataBinding.FieldName = 'work_state'
        DataBinding.IsNullValueType = True
        Width = 62
      end
      object ColWorkCounty: TcxGridDBColumn
        Caption = 'work county'
        DataBinding.FieldName = 'work_county'
        DataBinding.IsNullValueType = True
        Width = 170
      end
      object ColWorkCity: TcxGridDBColumn
        Caption = 'work city'
        DataBinding.FieldName = 'work_city'
        DataBinding.IsNullValueType = True
        Width = 252
      end
      object ColBillingCC: TcxGridDBColumn
        Caption = 'billing cc'
        DataBinding.FieldName = 'billing_cc'
        DataBinding.IsNullValueType = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from billing_cc_map'
  end
  object dxSkinController1: TdxSkinController
    Left = 464
    Top = 208
  end
end
