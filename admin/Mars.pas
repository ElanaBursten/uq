unit Mars;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TMarsForm = class(TBaseCxListForm)
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    MarsFilter: TCheckBox;
    ColTreeEmpID: TcxGridDBColumn;
    ColNewStatus: TcxGridDBColumn;
    ColINsertDate: TcxGridDBColumn;
    ColInsertedBy: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    procedure MarsFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure ActivatingNow; override;
    function IsReadOnly: Boolean; override;
  end;

var
  MarsForm: TMarsForm;

implementation

uses OdMiscUtils;

{$R *.dfm}


procedure TMarsForm.MarsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('mars_id').AsInteger;
  Data.Close;
  if MarsFilter.Checked then
    Data.CommandText := 'select * from mars where active = 1' else
      Data.CommandText := 'select * from mars';
  Data.Open;
  Data.Locate('mars_id', FSelectedID, []);
end;

procedure TMarsForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TMarsForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TMarsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

function TMarsForm.IsReadOnly: Boolean;
begin
  Result := false;
end;

procedure TMarsForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TMarsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
 inherited;
 MarsFilter.Enabled := True;
end;

end.
