unit EmployeeIncentivePay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxMaskEdit,
  cxDBLookupComboBox, cxMemo, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, cxDropDownEdit,
  cxCurrencyEdit, StdCtrls, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxSkinBasic, dxScrollbarAnnotations, dxSkinsCore;

type
  TEmployeeIncentivePayForm = class(TBaseCxListForm)
    GridViewEmployeeLookup: TcxGridDBColumn;
    GridViewIncentiveTypeLookup: TcxGridDBColumn;
    GridViewrate: TcxGridDBColumn;
    GridViewactive: TcxGridDBColumn;
    EmployeeRef: TADODataSet;
    IncPayTypeRef: TADODataSet;
    IncPayTypeRefDS: TDataSource;
    EmployeeRefDS: TDataSource;
    EmployeeLookupView: TcxGridDBTableView;
    EmployeeViewLookupColumn: TcxGridDBColumn;
    IncentivePayTypeLookupView: TcxGridDBTableView;
    IncentivePayTypeLookupViewColumn: TcxGridDBColumn;
    GridViewemp_id: TcxGridDBColumn;
    GridViewEmpNumber: TcxGridDBColumn;
    InstrPanel: TPanel;
    InfoLabelStat: TLabel;
    Panel1: TPanel;
    cbEmpIncentiveSearch: TCheckBox;
    ActiveEmpIncentiveFilter: TCheckBox;
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure cbEmpIncentiveSearchClick(Sender: TObject);
    procedure ActiveEmpIncentiveFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    EIPInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdMiscUtils, OdDbUtils;

{$R *.dfm}

{ TProfitCenterForm }

procedure TEmployeeIncentivePayForm.ActiveEmpIncentiveFilterClick(
  Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('incentive_id').AsInteger;
  Data.Close;
  if ActiveEmpIncentiveFilter.Checked then
    Data.CommandText := 'select * from employee_pay_incentive where active = 1' else
      Data.CommandText := 'select * from employee_pay_incentive';
  Data.Open;
  Data.Locate('incentive_id', FSelectedID, []);
end;

procedure TEmployeeIncentivePayForm.cbEmpIncentiveSearchClick(Sender: TObject);
begin
  inherited;
  if cbEmpIncentiveSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TEmployeeIncentivePayForm.CloseDataSets;
begin
  inherited;
  IncPayTypeRef.Close;
  EmployeeRef.Close;
end;

procedure TEmployeeIncentivePayForm.OpenDataSets;
begin
  IncPayTypeRef.Open;
  EmployeeRef.Open;
  inherited;
end;

procedure TEmployeeIncentivePayForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbEmpIncentiveSearch.Enabled := True;
  ActiveEmpIncentiveFilter.Enabled := True;
end;

procedure TEmployeeIncentivePayForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  EIPInserted := False;
end;

procedure TEmployeeIncentivePayForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'employee_pay_incentive', 'incentive_id', EIPInserted);
end;

procedure TEmployeeIncentivePayForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TEmployeeIncentivePayForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  EIPInserted := True;
end;

procedure TEmployeeIncentivePayForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  AddLookupField('IncentiveTypeLookup', Data, 'incentive_type', IncPayTypeRef, 'code', 'description');
    Data.FieldByName('IncentiveTypeLookup').LookupCache := True;
  AddLookupField('EmployeeLookup', Data, 'emp_id', EmployeeRef, 'emp_id', 'short_name');
    Data.FieldByName('EmployeeLookup').LookupCache := True;
  AddLookupField('EmployeeNumLookup', Data, 'emp_id', EmployeeRef, 'emp_id', 'emp_number');
    Data.FieldByName('EmployeeNumLookup').LookupCache := True;
end;

procedure TEmployeeIncentivePayForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
  DataSet.FieldByName('added_date').AsDateTime := Now;
end;

procedure TEmployeeIncentivePayForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TEmployeeIncentivePayForm.ActivatingNow;
begin
  inherited;
  EIPInserted := False;
end;

procedure TEmployeeIncentivePayForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[GridviewActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TEmployeeIncentivePayForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

end.

