inherited LocatingCompanyForm: TLocatingCompanyForm
  Left = 267
  Caption = 'Locating Companies'
  ClientHeight = 468
  ClientWidth = 778
  Font.Charset = DEFAULT_CHARSET
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 778
    object NoteLabel: TLabel
      Left = 8
      Top = 4
      Width = 473
      Height = 29
      AutoSize = False
      Caption = 
        'The locating companies are the entities within the company that ' +
        'can send invoices to customers.  The address defined here is the' +
        ' payment remit address printed on the invoices.'
      WordWrap = True
    end
    object cbLocatingCompanySearch: TCheckBox
      Left = 578
      Top = 15
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbLocatingCompanySearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 778
    Height = 427
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'company_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColName: TcxGridDBColumn
        Caption = 'Company Name'
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 122
      end
      object ColAddressStreet: TcxGridDBColumn
        Caption = 'Address'
        DataBinding.FieldName = 'address_street'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 177
      end
      object ColAddressCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'address_city'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 92
      end
      object ColAddressState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'address_state'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 36
      end
      object ColAddressZip: TcxGridDBColumn
        Caption = 'Zip'
        DataBinding.FieldName = 'address_zip'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 71
      end
      object ColPhone: TcxGridDBColumn
        Caption = 'Phone'
        DataBinding.FieldName = 'phone'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 68
      end
      object ColLogoFilename: TcxGridDBColumn
        Caption = 'Logo File Name'
        DataBinding.FieldName = 'logo_filename'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 94
      end
      object ColBillingFooter: TcxGridDBColumn
        Caption = 'Billing Footer'
        DataBinding.FieldName = 'billing_footer'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 104
      end
      object ColFloatHolidays: TcxGridDBColumn
        Caption = 'Max Floating Holidays'
        DataBinding.FieldName = 'floating_holidays_per_year'
        Width = 116
      end
      object ColCompanyID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'company_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 41
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from locating_company'
  end
end
