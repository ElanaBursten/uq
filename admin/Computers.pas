unit Computers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, odMiscUtils,odHourGlass,
  cxCustomData,cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxCalendar, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxDBExtLookupComboBox, cxLookAndFeels, cxLookAndFeelPainters,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxStyles,
  cxDBLookupComboBox, OddbUtils, cxTextEdit, dxDateRanges;

type
  TComputersForm = class(TBaseCxListForm)
    NoteLabel: TLabel;
    RetireReason: TADODataSet;
    RetireReasonDS: TDataSource;
    ActiveComputersFilter: TCheckBox;
    chkboxSearch: TCheckBox;
    ColComputerInfoID: TcxGridDBColumn;
    ColComputerSerial: TcxGridDBColumn;
    ColComputerName: TcxGridDBColumn;
    ColInsertDate: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    ColWindowsUser: TcxGridDBColumn;
    ColDomainLogin: TcxGridDBColumn;
    ColOSPlatform: TcxGridDBColumn;
    ColOSMajorVersion: TcxGridDBColumn;
    ColOSMinorVersion: TcxGridDBColumn;
    ColOSServicePack: TcxGridDBColumn;
    ColServiceTag: TcxGridDBColumn;
    ColComputerModel: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColRetireReason: TcxGridDBColumn;
    ColSyncID: TcxGridDBColumn;
    ReasonView: TcxGridDBTableView;
    ColReasonCode: TcxGridDBColumn;
    ColReasonDescrption: TcxGridDBColumn;
    EmployeeDS: TDataSource;
    Employees: TADODataSet;
    EmployeeView: TcxGridDBTableView;
    EmpidCol: TcxGridDBColumn;
    EmpNameCol: TcxGridDBColumn;
    ColEmpID: TcxGridDBColumn;
    ActiveEmployeesFilter: TCheckBox;
    ColEmpActive: TcxGridDBColumn;
    procedure ActiveComputersFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure chkboxSearchClick(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    CompInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;

implementation

{$R *.dfm}
uses AdminDMu;

{ TComputersForm }

procedure TComputersForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
  CompInserted := False;
end;

procedure TComputersForm.chkboxSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TComputersForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) and
      (VarToInt(GridRec.Values[ColEmpID.Index]) > 0) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TComputersForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TComputersForm.CloseDataSets;
begin
  RetireReason.Close;
  Employees.Close;
  inherited;
end;

procedure TComputersForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CompInserted := False;
end;

procedure TComputersForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'computer_info', 'computer_info_id', CompInserted);
end;

procedure TComputersForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TComputersForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  CompInserted := True;
end;

procedure TComputersForm.DataBeforePost(DataSet: TDataSet);
begin
  if Data.FieldByName('emp_id').IsNull then
   raise Exception.Create('An emp_id is required to save a computer record');
end;

procedure TComputersForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TComputersForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TComputersForm.OpenDataSets;
var
  Cursor: IInterface;
  WherePos: Integer;
begin
  Cursor := ShowHourGlass;
  RetireReason.Open;
  Employees.Open;
  inherited;
end;

procedure TComputersForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveComputersFilter.Enabled := True;
  ActiveEmployeesFilter.Enabled := True;
  chkboxSearch.Enabled := True;
end;

procedure TComputersForm.ActiveComputersFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: String;
  SelectSQL: String;
  WhereClause: String;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('computer_info_id').AsString;
  Data.Close;
  SelectSQL :=  'select e.active as active_emp, c.* from computer_info c left join employee e on c.emp_id = e.emp_id';

  WhereClause := '';
  if ActiveComputersFilter.Checked and not ActiveEmployeesFilter.Checked then
  WhereClause :=  ' where c.active = 1'
  else if ActiveEmployeesFilter.Checked and not ActiveComputersFilter.Checked then
  WhereClause :=  ' where e.active = 1'
  else if ActiveEmployeesFilter.Checked and ActiveComputersFilter.Checked then
    WhereClause := ' where c.active = 1 and e.active = 1';

  Data.CommandText :=  SelectSQL + WhereClause;
  Data.Open;
  Data.Locate('computer_info_id', FSelectedID, []);
end;

end.
