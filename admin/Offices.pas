unit Offices;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, Grids, DBGrids, ExtCtrls, BaseList, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxDropDownEdit, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TOfficesForm = class(TBaseCxListForm)
    ColOfficeID: TcxGridDBColumn;
    ColOfficeName: TcxGridDBColumn;
    ColProfitCenter: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColCompanyName: TcxGridDBColumn;
    ColAddress1: TcxGridDBColumn;
    ColAddress2: TcxGridDBColumn;
    ColCity: TcxGridDBColumn;
    ColState: TcxGridDBColumn;
    ColZipcode: TcxGridDBColumn;
    ColPhone: TcxGridDBColumn;
    ColFax: TcxGridDBColumn;
    chkboxOfficesSearch: TCheckBox;
    ActiveOfficesFilter: TCheckBox;
    procedure ActiveOfficesFilterClick(Sender: TObject);
    procedure chkboxOfficesSearchClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    OfficeInserted: Boolean;
  public
    procedure PopulateControls; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  AdminDMu, OdCxUtils, OdHourglass, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TOfficesForm }

procedure TOfficesForm.ActiveOfficesFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: String;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('office_id').AsString;
  Data.Close;
  if ActiveOfficesFilter.Checked then
    Data.CommandText := 'select * from office where active = 1 order by office_name' else
      Data.CommandText := 'select * from office order by office_name';
  Data.Open;
  Data.Locate('office_id', FSelectedID, []);
end;

procedure TOfficesForm.chkboxOfficesSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxOfficesSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TOfficesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  OfficeInserted := False;
end;

procedure TOfficesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset, ATableArray, 'office', 'office_id', OfficeInserted);
end;

procedure TOfficesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TOfficesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  OfficeInserted := True;
end;

procedure TOfficesForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
   DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TOfficesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TOfficesForm.ActivatingNow;
begin
  inherited;
  OfficeInserted := False;
end;

procedure TOfficesForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TOfficesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TOfficesForm.PopulateControls;
begin
  inherited;
  AdminDM.GetProfitCenterList(CxComboBoxItems(ColProfitCenter));
end;

procedure TOfficesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxOfficesSearch.Enabled := True;
  ActiveOfficesFilter.Enabled := True;
end;

end.
