unit MainFU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, ComCtrls, OdEmbeddable, ImgList, StdActns,
  ActnList, ToolWin, Buttons, AdminDMu, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxInplaceContainer,
  cxTextEdit, System.Actions,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore,
  System.ImageList, dxScrollbarAnnotations, cxGridDBTableView,
  Vcl.AppEvnts, cxContainer, cxEdit, dxSkinBasic, cxFilter, cxTLdxBarBuiltInMenu;

type
  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    File1: TMenuItem;
    FileExitItem: TMenuItem;
    Edit1: TMenuItem;
    CutItem: TMenuItem;
    CopyItem: TMenuItem;
    PasteItem: TMenuItem;
    Help1: TMenuItem;
    HelpAboutItem: TMenuItem;
    ActionList: TActionList;
    FileNew1: TAction;
    FileOpen1: TAction;
    FileSave1: TAction;
    FileSaveAs1: TAction;
    FileSend1: TAction;
    FileExit1: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    HelpAbout1: TAction;
    SBar: TStatusBar;
    RightPanel: TPanel;
    ContentPanel: TPanel;
    N1: TMenuItem;
    LeftPanel: TPanel;
    ContentLabelPanel: TPanel;
    Splitter: TSplitter;
    RefreshAction: TAction;
    Refresh1: TMenuItem;
    RefreshButton: TButton;
    ScreenList: TcxTreeList;
    ScreenListColumn1: TcxTreeListColumn;
    EditButton: TButton;
    EditAction: TAction;
    RestoreLayout: TMenuItem;
    IdleTimer: TTimer;
    ImageList: TImageList;
    IdleProgressBar: TProgressBar;
    procedure FileExit1Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RefreshActionExecute(Sender: TObject);
    procedure SBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure ScreenListSelectionChanged(Sender: TObject);
    procedure EditActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure IdleTimerTimer(Sender: TObject);
  public
    procedure ShowEmployee(ID: Integer);
    procedure ShowUser(ID: Integer);
    procedure ShowQtrMapPage(Lat, Long: string);
    procedure CancelGridViewEdits;
  private
    AppTimeOut: Integer;
    EmbeddedForms: TStringList;
    FActiveEmbeddedForm: TEmbeddableForm;
    FDBWasDisconnected: Boolean;
    function GoToForm(FormID: string): TEmbeddableForm;
    function ActivateForm(FormID: string): TEmbeddableForm;
    procedure AdjustCaption;
    procedure LeaveCurrentForm;
    procedure UpdateDBConnectionStatusInfo(ConnectionStatus: TConnectionStatus);
    function GetIdleTime: DWORD;
  end;

var
  MainForm: TMainForm;

implementation

uses  OdVclUtils, IniFiles, OdMiscUtils, OdHourglass, AdminFormDefs,
  Locator, User, QtrMinPageMap;

{$R *.DFM}

procedure TMainForm.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
begin
  Caption := AppName;
  AdminDM.OnUpdateDBConnectionStatusInfo := UpdateDBConnectionStatusInfo;
  IniFile := AdminDM.CreateINIFile;
  try
    RestoreFormState(Self, IniFile, 'UI', 'Main');
    AppTimeOut := IniFile.ReadInteger('AppTimeOut', 'Timeout', 600);
    if AppTimeOut >= 20 then
      AppTimeOut := AppTimeout- 20;
  finally
    FreeAndNil(IniFile);
  end;
  FDBWasDisconnected := False;
end;

// Update the status connection information
procedure TMainForm.UpdateDBConnectionStatusInfo(ConnectionStatus: TConnectionStatus);
begin
  if ConnectionStatus = csConnected then begin
    SBar.Panels[3].Text := ConnectedToDBMessage;
    if FDBWasDisconnected then
      RefreshAction.Execute;
  end
  else
    SBar.Panels[3].Text := DisconnectedFromDBMessage;

  FDBWasDisconnected := ConnectionStatus <> csConnected
end;

procedure TMainForm.FormShow(Sender: TObject);
var
  Holder: TAdminFormDef;
  I, J: Integer;
  AllowedList: TStringList;

  VersionString: string;
  Error: Boolean;

  GroupNode: TcxTreeListNode;
begin
  AllowedList := TStringList.Create;
  ScreenList.BeginUpdate;
  try
    if AdminDM.UserHasRestrictedAccess then
      AdminDM.GetAllowedFormList(AllowedList);

    for I := 0 to Defs.Count-1 do begin
      Holder := Defs.Items[I] as TAdminFormDef;

      if AllowedList.Count>0 then
        if AllowedList.IndexOf(Holder.DisplayName)<0 then
          Continue;

      GroupNode := nil;
      for J := 0 to ScreenList.Root.Count-1 do
        if ScreenList.Root.Items[J].Values[0] = Holder.Group then
          GroupNode := ScreenList.Root.Items[J];

      if not Assigned(GroupNode) then begin
        GroupNode := ScreenList.Root.AddChild;
        GroupNode.Values[0] := Holder.Group;
      end;

      GroupNode.AddChild.Values[0] := Holder.DisplayName;
    end;
  finally
    AllowedList.Free;
    ScreenList.EndUpdate;
  end;

  SBar.Panels[0].Text := 'User: ' + AdminDM.UserName;
  SBar.Panels[1].Text := 'Version: ' + AppVersion;
  SBar.Panels[2].Text := 'Server: ' + AdminDM.ServerString;
  SBar.Panels[3].Text := ConnectedToDBMessage;
  Error := not GetFileVersionNumberString(Application.ExeName, VersionString);
  if not Error then
    Error := VersionString <> AppVersion;
  if Error then
    MessageDlg('The application EXE/DOF versions are mismatched: '+VersionString+'/'+AppVersion, mtError, [mbOK], 0);

end;

// **************** Action handlers
// All user action code in this unit should be in action handlers, not in
// toolbutton handlers, to allow for changing to a different toolbar component
// later, if needed.

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
  Close;
end;

// **********************************************************************
// Embedded form management code needs to moved in to the embedded form
// utility module and base class... right now it is copy-and-pasted to each
// project that uses it.  Ugh.  -Kyle

procedure TMainForm.AdjustCaption;
begin
  if FActiveEmbeddedForm<>nil then
    ContentLabelPanel.Caption := '   ' + FActiveEmbeddedForm.Caption;
end;

procedure TMainForm.EditActionExecute(Sender: TObject);
begin
  if Assigned(FActiveEmbeddedForm) then
    FActiveEmbeddedForm.EditNow;
end;

procedure TMainForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  if Assigned(FActiveEmbeddedForm) then
    EditAction.Enabled := FActiveEmbeddedForm.IsReadOnly;
end;

function TMainForm.ActivateForm(FormID: string): TEmbeddableForm;
var
  Form: TEmbeddableForm;
  Index: Integer;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  // This method actives the named form, and also caches them so that only
  // one instance per name is created.  This supports a form class being
  // instantiated more than once under different names;

  if EmbeddedForms = nil then
    EmbeddedForms := TStringList.Create;

  Index := EmbeddedForms.IndexOf(FormID);
  if Index >= 0 then begin
    Form := TEmbeddableForm(EmbeddedForms.Objects[Index]);
  end else begin
    Form := CreateEmbeddedForm(LookupAdminFormClass(FormID), Self, ContentPanel);
    EmbeddedForms.AddObject(FormID, Form);
  end;

  if (Form <> FActiveEmbeddedForm) or (not Form.Visible) then begin
    LeaveCurrentForm;

    // new active form
    FActiveEmbeddedForm := Form;
    FActiveEmbeddedForm.Show;
    // This is just a hack to ensure the embedded alClient form actually
    // resizes when the parent window was resized since the last time this
    // embedded form.  I can't find another way to force a realign that works.
    Form.Width := Form.Width + 1;

  end;

  AdjustCaption;
  Form.ActivatingNow;
  EditButton.Visible := Form.IsReadOnly;
  Result := Form;

   if not IdleTimer.Enabled then
      IdleTimer.Enabled := True;
end;

procedure TMainForm.LeaveCurrentForm;
var
  Index: Integer;
begin
  if Assigned(FActiveEmbeddedForm) then begin
    FActiveEmbeddedForm.LeavingNow;
    FActiveEmbeddedForm.Hide;
  end;

  // Hide any others that have become visible
  if Assigned(EmbeddedForms) then
    for Index := 0 to EmbeddedForms.Count-1 do    // hide all others
      TForm( EmbeddedForms.Objects[Index] ).Hide;
end;

procedure TMainForm.ScreenListSelectionChanged(Sender: TObject);
begin
  if ScreenList.SelectionCount > 0 then
    if ScreenList.Selections[0].Level > 0 then
      ActivateForm(ScreenList.Selections[0].Values[0]);
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  IniFile: TIniFile;
begin
  LeaveCurrentForm;

  IniFile := AdminDM.CreateIniFile;
  try
    SaveFormState(Self, IniFile, 'UI', 'Main');
  finally
    FreeAndNil(IniFile);
  end;
end;

procedure TMainForm.RefreshActionExecute(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if Assigned(FActiveEmbeddedForm) then begin
    FActiveEmbeddedForm.RefreshNow;
    if EditButton.Visible then
      FActiveEmbeddedForm.LockNow;
  end;
end;

function TMainForm.GoToForm(FormID: string): TEmbeddableForm;
begin
  ScreenList.FindNodeByText(FormID, ScreenList.Columns[0], nil, False, True, True, tlfmExact).Selected := True;
  Application.ProcessMessages;
  Result := FActiveEmbeddedForm;
end;

procedure TMainForm.ShowEmployee(ID: Integer);
begin
  GoToForm('Employees');
  (FActiveEmbeddedForm as TLocatorForm).ShowEmployee(ID);
end;

procedure TMainForm.ShowUser(ID: Integer);
begin
  GoToForm('Users');
  (FActiveEmbeddedForm as TUserForm).ShowUser(ID);
end;

procedure TMainForm.ShowQtrMapPage(Lat, Long : string);
begin
  GoToForm('Qtr-Min Routing - Pages');
  (FActiveEmbeddedForm as TQtrMinPageMapForm).GotoCoords(Lat, Long);
end;

procedure TMainForm.SBarDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
  with SBar.Canvas do
  begin
    if FDBWasDisconnected then
      Font.Color := clRed
    else
      Font.Color := SBar.Font.Color;
    TextOut(Rect.left+3, Rect.Top+3, Panel.Text );
  end;
end;

function TMainForm.GetIdleTime:DWORD;
var
  last: TLastInputInfo;
begin
  last.cbSize := Sizeof(last);
  GetLastInputInfo(last);
  Result := (GetTickCount64 - last.dwTime)div 1000;
end;

procedure TMainForm.IdleTimerTimer(Sender: TObject);
var
  Count: Integer;
begin
  if (GetIdleTime >= AppTimeOut) then
  begin
    Windows.Beep(300,500);
    IdleProgressBar.Min := 0;
    IdleProgressBar.Max := 100;
    IdleProgressBar.Visible := True;
    IdleProgressBar.Position := 0;
    for Count := 1 to 100 do
    begin
      IdleProgressBar.Position := Count;
      Sleep(200);
      if GetIdleTime < AppTimeOut then
      begin
       IdleProgressBar.Visible := False;
       Exit;
      end
    end;
    CancelGridViewEdits;
    Close;
  end;
end;

procedure TMainForm.CancelGridViewEdits;
var
  I: Integer;
  DBGridView: TcxGridDBTableView;
begin
  inherited;
  if Assigned(FActiveEmbeddedForm) then
  begin
    for I := 0 to FActiveEmbeddedForm.ComponentCount-1 do
    begin
      if FActiveEmbeddedForm.Components[I] is TcxGridDBTableView then
      begin
        DBGridview := TcxGridDBTableView(FActiveEmbeddedForm.Components[I]);
        If (dceEdit in DBGridView.DataController.EditState) or
        (dceInsert in DBGridView.DataController.EditState) then
        DBGridView.DataController.Cancel;
      end;
    end;
  end;
end;

end.
