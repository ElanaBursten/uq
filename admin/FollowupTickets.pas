unit FollowupTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxDropDownEdit,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;
type
  TFollowupTicketsForm = class(TBaseCxListForm)
    ColTicketType: TcxGridDBColumn;
    ColCallCenter: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    cbFollowupSearch: TCheckBox;
    ActiveFollowupFilter: TCheckBox;
    procedure cbFollowupSearchClick(Sender: TObject);
    procedure ActiveFollowupFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    futInserted: Boolean;
  public
    procedure PopulateCallCenterList;
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdMiscUtils, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TFollowupTicketsForm }

procedure TFollowupTicketsForm.ActiveFollowupFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('type_id').AsInteger;
  Data.Close;
  if ActiveFollowupFilter.Checked then
    Data.CommandText := 'select * from followup_ticket_type where active = 1' else
      Data.CommandText := 'select * from followup_ticket_type';
  Data.Open;
  Data.Locate('type_id', FSelectedID, []);
end;

procedure TFollowupTicketsForm.cbFollowupSearchClick(Sender: TObject);
begin
  inherited;
  if cbFollowupSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TFollowupTicketsForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  FutInserted := False;
end;

procedure TFollowupTicketsForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'followup_ticket_type', 'type_id', futInserted);
end;

procedure TFollowupTicketsForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray)
end;

procedure TFollowupTicketsForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  FutInserted := True;
end;

procedure TFollowupTicketsForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TFollowupTicketsForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TFollowupTicketsForm.ActivatingNow;
begin
  inherited;
  futInserted := False;
end;

procedure TFollowupTicketsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[Colactive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TFollowupTicketsForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TFollowupTicketsForm.PopulateCallCenterList;
begin
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter));
  CxComboBoxItems(ColCallCenter).Insert(0, '*');
end;

procedure TFollowupTicketsForm.PopulateDropdowns;
begin
  inherited;
  PopulateCallCenterList;
end;

procedure TFollowupTicketsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbFollowupSearch.Enabled := True;
  ActiveFollowupFilter.Enabled := True;
end;

end.
