unit Locator;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Mask,
  DBCtrls, BetterADODataSet, ComCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCheckBox,
  cxTextEdit, cxDropDownEdit, cxCalendar, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxDBExtLookupComboBox, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxNavigator, dxCore, cxDateUtils, Rights, System.Actions, uADSI,
  Vcl.ActnList, cxButtonEdit, dxColorEdit, dxDateRanges, cxCalc, Vcl.Menus, cxButtons, ADOInt,
  System.ImageList, Vcl.ImgList, dxScrollbarAnnotations, TermEmp, ComputerAssets,
  dxAlertWindow, AdminDmu, dxSkinBasic, dxSkinsCore, dxCoreGraphics, cxDBEdit,
  cxImageComboBox;

  const
  UM_MYMESSAGE = WM_USER + 1;
    //post message for screen paint in beforepost event

type
  TLocatorForm = class(TBaseCxListForm)
    Panel6: TPanel;
    EmpUserDS: TDataSource;
    TypeRef: TADODataSet;
    TypeRefDS: TDataSource;
    PC: TPageControl;
    UserSheet: TTabSheet;
    AssignedAreaSheet: TTabSheet;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit4: TDBText;
    DBEdit3: TDBText;
    EditLoginID: TDBEdit;
    AssignedAreasGrid: TDBGrid;
    AssignedAreasDS: TDataSource;
    InfoSheet: TTabSheet;
    InfoLabelStat: TLabel;
    TimeRuleRef: TADODataSet;
    TimeRuleRefDS: TDataSource;
    RightsSheet: TTabSheet;
    emp_rightsDS: TDataSource;
    UpdateRightCommand: TADOCommand;
    InsertRightCommand: TADOCommand;
    SaveUserButton: TButton;
    Label6: TLabel;
    Splitter1: TSplitter;
    NotificationEmailSheet: TTabSheet;
    NotificationEmailDS: TDataSource;
    CrewNumDS: TDataSource;
    CrewNumRef: TADODataSet;
    CompanyRef: TADODataSet;
    CompanyRefDS: TDataSource;
    ChangePasswordButton: TButton;
    PasswordHistorySheet: TTabSheet;
    PasswordHistoryGrid: TDBGrid;
    PasswordHistoryDS: TDataSource;
    GroupsSheet: TTabSheet;
    EmpGroups: TADODataSet;
    EmpGroupsDS: TDataSource;
    RemoveEmpGroupCommand: TADOCommand;
    AddEmpGroupCommand: TADOCommand;
    DBGrid1: TDBGrid;
    AddEmpGroupButton: TButton;
    RemoveEmpGroupButton: TButton;
    ColEmpID: TcxGridDBColumn;
    ColReportTo: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColCanReceiveTickets: TcxGridDBColumn;
    ColTypeCode: TcxGridDBColumn;
    ColEmpNumber: TcxGridDBColumn;
    ColShortName: TcxGridDBColumn;
    ColFirstName: TcxGridDBColumn;
    ColMiddleInit: TcxGridDBColumn;
    ColLastName: TcxGridDBColumn;
    ColTimeRule: TcxGridDBColumn;
    ColDialupUser: TcxGridDBColumn;
    ColPCCode: TcxGridDBColumn;
    ColPayrollPCCode: TcxGridDBColumn;
    ColCrewNum: TcxGridDBColumn;
    ColAutoClose: TcxGridDBColumn;
    ColCompanyLookup: TcxGridDBColumn;
    ColTicketViewLimit: TcxGridDBColumn;
    ColHireDate: TcxGridDBColumn;
    EmpTypeLookupView: TcxGridDBTableView;
    EmpTypeLookupViewColCode: TcxGridDBColumn;
    EmpTypeLookupViewColDescription: TcxGridDBColumn;
    EmpTypeLookupViewColRefID: TcxGridDBColumn;
    TimeRuleLookupView: TcxGridDBTableView;
    TimeRuleLookupViewColCode: TcxGridDBColumn;
    TimeRuleLookupViewColDescription: TcxGridDBColumn;
    TimeRuleLookupViewColRefID: TcxGridDBColumn;
    CrewNumLookupView: TcxGridDBTableView;
    CrewNumLookupViewColRefID: TcxGridDBColumn;
    CrewNumLookupViewColCode: TcxGridDBColumn;
    CrewNumLookupViewColDescription: TcxGridDBColumn;
    CompanyLookupView: TcxGridDBTableView;
    CompanyLookupViewColName: TcxGridDBColumn;
    ColShowFutureTickets: TcxGridDBColumn;
    TicketGoalsDS: TDataSource;
    TicketGoalsSheet: TTabSheet;
    TicketGoalsGridView: TcxGridDBTableView;
    TicketGoalsGridLevel1: TcxGridLevel;
    TicketGoalsGrid: TcxGrid;
    ColEffectiveDate: TcxGridDBColumn;
    ColTicketCount: TcxGridDBColumn;
    TicketGoalEdit: TEdit;
    TicketGoalLabel: TLabel;
    AddGoalButton: TButton;
    EffectiveDateLabel: TLabel;
    TicketGoalDateEdit: TcxDateEdit;
    ColWorkGoalActive: TcxGridDBColumn;
    ColContactPhone: TcxGridDBColumn;
    ColLocalUTC: TcxGridDBColumn;
    ActionList: TActionList;
    GrantAction: TAction;
    RevokeAction: TAction;
    EffectiveUserRightsDS: TDataSource;
    Splitter2: TSplitter;
    EffectiveRightsGrid: TcxGrid;
    EffectiveRightsGridDBTableView1: TcxGridDBTableView;
    colEffectiveRight: TcxGridDBColumn;
    colEffectiveGroup: TcxGridDBColumn;
    EffectiveRightsGridLevel1: TcxGridLevel;
    RightsFrame: TRightsFrame;
    SpacerPanel: TPanel;
    colEffectiveRightId: TcxGridDBColumn;
    ColAdUserName: TcxGridDBColumn;
    ColTestComLogin: TcxGridDBColumn;
    ColADPLogin: TcxGridDBColumn;
    lblInstructions: TLabel;  //QMANTWO-351
    Label1: TLabel;    //QMANTWO-351
    Label2: TLabel;    //QMANTWO-351
    Label3: TLabel;   //QMANTWO-351
    Label4: TLabel;
    editEmail: TDBEdit;
    Label5: TLabel;
    localLastLogIn: TLabel;
    ActiveLocatorsFilter: TCheckBox;
    Panel1: TPanel;
    EmpShortName: TDBText;
    EmpNum: TDBText;
    EmpNumLabel: TLabel; //QMANTWO-351
    ColHomeLat: TcxGridDBColumn;
    ColHomeLng: TcxGridDBColumn;
    ColAltLat: TcxGridDBColumn;
    ColAltLng: TcxGridDBColumn;
    ColEmpTermDate: TcxGridDBColumn;
    EmpUser: TADODataSet;
    EmpUseruid: TAutoIncField;
    EmpUsergrp_id: TIntegerField;
    EmpUseremp_id: TIntegerField;
    EmpUserfirst_name: TStringField;
    EmpUserlast_name: TStringField;
    EmpUserlogin_id: TStringField;
    EmpUserpassword: TStringField;
    EmpUserchg_pwd: TBooleanField;
    EmpUserlast_login: TDateTimeField;
    EmpUseractive_ind: TBooleanField;
    EmpUseremail_address: TStringField;
    AssignedAreas: TADODataSet;
    EmpRights: TADODataSet;
    PasswordHistory: TADODataSet;
    TicketGoals: TADODataSet;
    EffectiveUserRights: TADODataSet;
    ColLastLogin: TcxGridDBColumn;
    ColPlatUpdateDate: TcxGridDBColumn;
    Dataemp_id: TAutoIncField;
    Datatype_id: TIntegerField;
    Datastatus_id: TIntegerField;
    Datatimesheet_id: TIntegerField;
    Dataemp_number: TStringField;
    Datashort_name: TStringField;
    Datafirst_name: TStringField;
    Datalast_name: TStringField;
    Datamiddle_init: TStringField;
    Datareport_to: TIntegerField;
    Datacreate_date: TDateTimeField;
    Datacreate_uid: TIntegerField;
    Datamodified_date: TDateTimeField;
    Datamodified_uid: TIntegerField;
    Datacharge_cov: TBooleanField;
    Dataactive: TBooleanField;
    Datacan_receive_tickets: TBooleanField;
    Datatimerule_id: TIntegerField;
    Datadialup_user: TStringField;
    Datacompany_car: TStringField;
    Datarepr_pc_code: TStringField;
    Datapayroll_pc_code: TStringField;
    Datacrew_num: TStringField;
    Dataautoclose: TBooleanField;
    Datacompany_id: TIntegerField;
    Datarights_modified_date: TDateTimeField;
    Dataticket_view_limit: TIntegerField;
    Datahire_date: TDateTimeField;
    Datashow_future_tickets: TBooleanField;
    Dataincentive_pay: TBooleanField;
    Datacontact_phone: TStringField;
    Datalocal_utc_bias: TIntegerField;
    Dataad_username: TStringField;
    Datatest_com_login: TStringField;
    Dataadp_login: TStringField;
    Datahome_lat: TFMTBCDField;
    Datahome_lng: TFMTBCDField;
    Dataalt_lat: TFMTBCDField;
    Dataalt_lng: TFMTBCDField;
    Datatermination_date: TDateTimeField;
    Dataplat_exp_date: TDateTimeField;
    Datalast_login: TDateTimeField;
    ColPlatAge: TcxGridDBColumn;
    Dataplat_age: TIntegerField;
    EmpStatusRef: TADODataSet;
    EmpStatusDS: TDataSource;
    EmpStatusLookupView: TcxGridDBTableView;
    EmpStatusLookupViewColDescription: TcxGridDBColumn;
    EmpStatusLookupViewColRefID: TcxGridDBColumn;
    ColEmpStatus: TcxGridDBColumn;
    ColPhoneCompany: TcxGridDBColumn;
    PhoneCoRef: TADODataSet;
    PhoneCoRefDS: TDataSource;
    PhoneCoLookupView: TcxGridDBTableView;
    Dataphoneco_ref: TIntegerField;
    PhoneCoLookupViewColCode: TcxGridDBColumn;
    PhoneCoLookupViewColDescription: TcxGridDBColumn;
    PhoneCoLookupViewColRefID: TcxGridDBColumn;
    ColChargeCov: TcxGridDBColumn;
    ColCompanyCar: TcxGridDBColumn;
    ColChangeBy: TcxGridDBColumn;
    Datachangeby: TStringField;
    ColModifiedDate: TcxGridDBColumn;
    Action1: TAction;
    btnUserLink: TcxButton;
    ColAtlasNumber: TcxGridDBColumn;
    Dataatlas_number: TStringField;
    TomorrowImage: TImageList;  //QM 181
    TermEmpInfo: TADODataSet;
    TmpQuery: TADOQuery;
    btnEmployeeInfo: TButton;
    ColFirstTaskReminder: TcxGridDBColumn;
    Datafirst_task_reminder: TStringField;
    ColTerminated: TcxGridDBColumn;
    Dataterminated: TBooleanField;
    popupLimitation: TPopupMenu;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    Paste1: TMenuItem;
    dbPassword: TcxDBCheckBox;
    NotificationEmail: TADODataSet;
    NotifcationGrid: TcxGrid;
    NotifcationGridView: TcxGridDBTableView;
    ColNotifType: TcxGridDBColumn;
    ColNotifDestination: TcxGridDBColumn;
    NotifcationGridLevel: TcxGridLevel;
    procedure DataBeforeDelete(DataSet: TDataSet);
    procedure EmpUserNewRecord(DataSet: TDataSet);
    procedure EditLoginIDEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PCChange(Sender: TObject);
    procedure DSDataChange(Sender: TObject; Field: TField);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure SaveUserButtonClick(Sender: TObject);
    procedure EmpUserDSStateChange(Sender: TObject);
    procedure EmpUserBeforePost(DataSet: TDataSet);
    procedure DataBeforeScroll(DataSet: TDataSet);
    procedure DataAfterScroll(DataSet: TDataSet);
    procedure NotificationEmailNewRecord(DataSet: TDataSet);
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure DataAfterOpen(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure ChangePasswordButtonClick(Sender: TObject);
    procedure EmpRightsAfterScroll(DataSet: TDataSet);
    procedure EmpGroupsAfterScroll(DataSet: TDataSet);
    procedure AddEmpGroupButtonClick(Sender: TObject);
    procedure RemoveEmpGroupButtonClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TicketGoalsBeforeDelete(DataSet: TDataSet);
    procedure TicketGoalsNewRecord(DataSet: TDataSet);
    procedure AddGoalButtonClick(Sender: TObject);
    procedure GrantActionExecute(Sender: TObject);
    procedure RevokeActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure RightsFrameRightsGridDBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure editEmailEnter(Sender: TObject);
    procedure ActiveLocatorsFilterClick(Sender: TObject);
    procedure btnUserLinkClick(Sender: TObject);
    procedure DataAfterEdit(DataSet: TDataSet);
    procedure GridViewColumnPosChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure FormCreate(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure GridViewNavigatorButtonsButtonClick(Sender: TObject;
      AButtonIndex: Integer; var ADone: Boolean);
    procedure btnEmployeeInfoClick(Sender: TObject);
    procedure ColTerminatedPropertiesEditValueChanged(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Cut1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure NotificationEmailBeforeEdit(DataSet: TDataSet);
    procedure NotificationEmailAfterPost(DataSet: TDataSet);
    procedure NotificationEmailBeforeInsert(DataSet: TDataSet);
    procedure EmpUserBeforeEdit(DataSet: TDataSet);
    procedure EmpUserAfterPost(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure GetEmailTypeList(List: TStrings);
    procedure GridEmailNotifExit(Sender: TObject);
  private
    RightID: Integer;
    IsDST : boolean;
    LayoutChanged: Boolean;
    ActiveFlag: Boolean;     //QM-181
    UPdateTomorrow: Boolean;
    RestoreItem: TMenuItem;  //QM-78
    TomActive: Byte;        //QM-181
    LastEmpid: Integer;
    LastAtlasNbr: String;
    LastCompanyID: Integer;
    LastReportTo: Integer;
    LastLocalUTC: Integer;
    LastCanReceiveTickets: Boolean;
    LastShowFutureTickets: Boolean;
    NotificationEmailsArray: TFieldValuesArray;
    AUserArray: TFieldValuesArray;
    PriorAtlas: String;
    NEInserted: Boolean;
    EmpInserted: Boolean;
    procedure ShowUserData;
    procedure ShowNotificationData;
    procedure ShowPasswordHistory;
    procedure ShowTicketGoals;
    procedure ValidateEmpHierarchy;
    procedure SetupControls;
    procedure LoadEffectiveRights;
    procedure SelectEffectiveRight(RightId: Integer = -1);
    procedure ShowEmpGroups;
    procedure SetLastLogInDiff;
    function UTCToSystemTime(UTC: TDateTime): TDateTime;
    procedure RestoreClick(Sender: TObject);
    procedure AddTomorrowBucket;
    procedure TomorrowActiveState(DataSet: TDataSet);
    procedure umMyMessage(var message : TMessage); message UM_MYMESSAGE;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ShowEmployee(ID: Integer);
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;

implementation


uses OdDbUtils, ChangePassword, OdMiscUtils, QMConst, OdExceptions,
  OdAdoUtils, OdCxUtils, OdIsoDates,  OdHourglass, MainFU, StrUtils, System.DateUtils;

{$R *.dfm}

procedure TLocatorForm.DataBeforeDelete(DataSet: TDataSet);
var
  s: String;
begin

  if MessageDlg('Delete this locator?  This will delete any area-assignment to this '+#13+#10+'locator (but not the area itself), and delete the locator''s '+#13+#10+'user-login.',
      mtConfirmation, [mbYes,mbNo], 0) = mrYes then
   begin
    // NOT inherited;
    Data.Properties['Unique Table'].Value:='employee';
    AdminDM.DeleteLocatorRelatedRecords(DataSet.FieldByName('emp_id').AsInteger);
    s := 'Employee ' +  DataSet.FieldByName('emp_id').AsString + ' deleted';
    AdminDM.AdminChangeTable('employee', s, now);
  end else
    Abort;
end;

procedure TLocatorForm.EmpUserAfterPost(DataSet: TDataSet);
var
 s:String;
 Ains: Boolean;
begin
  inherited;
  AIns := False;
  AdminDM.TrackAdminChanges(DataSet, AUserArray, 'users', 'uid', AIns);
end;

procedure TLocatorForm.EmpUserBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, AUserArray); //QM-823
end;

procedure TLocatorForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  PriorAtlas := DataSet.FieldByName('atlas_number').AsString;
  ActiveFlag := Dataset.FieldByName('active').AsBoolean;   //QM-200
   // get the current values before editing.
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-823

end;

procedure TLocatorForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  EmpInserted := True;
end;

procedure TLocatorForm.EmpUserNewRecord(DataSet: TDataSet);
begin
  if Data.FieldByName('emp_id').IsNull then
    raise Exception.Create('There must be an employee record to add user information');

  DataSet.FieldByName('chg_pwd').AsBoolean := False;
  DataSet.FieldByName('active_ind').AsBoolean := True;
  DataSet.FieldByName('emp_id').AsInteger := Data.FieldByName('emp_id').AsInteger;
end;

procedure TLocatorForm.EditLoginIDEnter(Sender: TObject);
begin
  PostDataSet(Data); // Make sure we have an emp_id
  OpenDataSet(EmpUser);
  if EmpUser.IsEmpty then begin
    EmpUser.Append;
    EmpUser.FieldByName('login_id').AsString := Data.FieldByName('emp_number').AsString;
  end;
end;

procedure TLocatorForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TLocatorForm.CloseDataSets;
begin
  inherited;
  EmpUser.Close;
  PhoneCoRef.Close;
  TypeRef.Close;
  TimeRuleRef.Close;
  CrewNumRef.Close;
  EmpStatusRef.Close;
  CompanyRef.Close;
  EmpGroups.Close;
  EmpRights.Close;
  NotificationEmail.Close;
end;

procedure TLocatorForm.ColTerminatedPropertiesEditValueChanged(Sender: TObject);
//BP QM-600
var
  col: TcxGridDBColumn;
begin
  inherited;
  col := GridView.GetColumnByFieldName('termination_date');
  if col <> nil then
    col.Focused := True;
    if Data.FieldByName('terminated').AsBoolean = False then
        Data.FieldByName('termination_date').Value := null else
    GridView.Controller.EditingController.ShowEdit(ColEmpTermDate);
end;

procedure TLocatorForm.Copy1Click(Sender: TObject);
begin
  inherited;
  RightsFrame.EditLimitation.CopyToClipboard;
end;

procedure TLocatorForm.Cut1Click(Sender: TObject);
begin
  inherited;
  RightsFrame.EditLimitation.CutToClipboard;
end;

procedure TLocatorForm.Paste1Click(Sender: TObject);
begin
  inherited;
  RightsFrame.EditLimitation.PasteFromClipboard;
end;

procedure TLocatorForm.OpenDataSets;
begin
  TypeRef.Open;
  PhoneCoRef.Open;
  TimeRuleRef.Open;
  CrewNumRef.Open;
  EmpStatusRef.Open;
  CompanyRef.Open;
  inherited;
  //This was throwing an error that appears to have been swallowed.  Need EmpID established here.
  ShowEmpGroups;
  PCChange(nil);
end;

procedure TLocatorForm.FormCreate(Sender: TObject);
begin
  inherited;   //QM-78
  DefaultLayoutStorage(colStore, Self, GridView, AdminDM.IniFileLayoutName);
  GridView.RestoreFromIniFile(AdminDM.IniFileLayoutName);
  RestoreItem := MainForm.MainMenu.Items[1].Find('Restore Layout');
end;

procedure TLocatorForm.FormShow(Sender: TObject);
begin
  inherited;
  PC.ActivePageIndex := 0;
  SetupControls;
  LayoutChanged := False;
  Data.Properties['Update Criteria'].value :=adCriteriaKey;
  Data.Properties['Unique Table'].Value:='employee';
end;

procedure TLocatorForm.SetupControls;
begin
  if AdminDM.UserHasRestrictedAccess then begin
    GridView.OptionsData.Editing := False;
    GridView.OptionsBehavior.ImmediateEditor := False;
    AddEmpGroupButton.Enabled := False;
    RemoveEmpGroupButton.Enabled := False;
    RightsFrame.GrantButton.Enabled := False;
    RightsFrame.RevokeButton.Enabled := False;
  end else begin
    GridView.OptionsData.Editing := True;
    GridView.OptionsBehavior.ImmediateEditor := True;
    AddEmpGroupButton.Enabled := True;
    RemoveEmpGroupButton.Enabled := True;
    RightsFrame.GrantButton.Enabled := True;
    RightsFrame.RevokeButton.Enabled := True;
  end;
end;

procedure TLocatorForm.PCChange(Sender: TObject);
begin
  AssignedAreas.Active := PC.ActivePage = AssignedAreaSheet;
  EmpRights.Active := PC.ActivePage = RightsSheet;
  ShowUserData;
  ShowNotificationData;
  ShowPasswordHistory;
  ShowTicketGoals;
  LoadEffectiveRights;
  if PC.ActivePage = UserSheet then SetLastLogInDiff;
 // ShowEmpGroups;
end;

procedure TLocatorForm.GetEmailTypeList(List: TStrings);
begin
  Assert(Assigned(List));
  List.Clear;
  List.Add ('time_warning');
  List.Add ('client_error');
  List.Add ('damage_approval');
  List.Add ('damage_unassigned');
  List.Add ('termination_notice');
  List.Add ('termination_warning');
  List.Add ('emergency_requeue');
  List.Add ('emergency_alert');
end;

procedure TLocatorForm.GrantActionExecute(Sender: TObject);
var
  RightId: Integer;
  EmpRightId: Integer;
  s: String;
begin
  inherited;
  RightId := RightsFrame.RightsGridDBTableView1.DataController.DataSet.FieldByName('right_id').AsInteger;

  EmpRightId :=  RightsFrame.RightsGridDBTableView1.DataController.DataSet.FieldByName('emp_right_id').AsInteger;

  RightsFrame.SetEmployeeRight('Y', RightsFrame.EditLimitation.Text, Data,
    InsertRightCommand, UpdateRightCommand);
   With RightsFrame.RightsGridDBTableView1.DataController.DataSet do
   begin
      s := ' [employee_right_id ' + FieldbyName('emp_right_id').AsString + '] ' +
      ' Right Granted to emp_id ' + FieldByName('emp_id').AsString +
      ', right_id: ' + FieldByName('right_id').AsString +
      ', effective right: ' + FieldByName('right_description').AsString +
      ', limitation: ' + RightsFrame.EditLimitation.Text;
       AdminDM.AdminChangeTable('employee_right', s, now); //QM-823
   end;

  LoadEffectiveRights;
  SelectEffectiveRight(RightId);
end;

procedure TLocatorForm.RevokeActionExecute(Sender: TObject);
var
  s: String;
  EmpRightId: Integer;
begin
  inherited;
  EmpRightId :=  RightsFrame.RightsGridDBTableView1.DataController.DataSet.FieldByName('emp_right_id').AsInteger;

  RightsFrame.SetEmployeeRight('N', '', Data, InsertRightCommand, UpdateRightCommand);
  With RightsFrame.RightsGridDBTableView1.DataController.DataSet do
   begin
      s := ' [employee_right_id ' + FieldbyName('emp_right_id').AsString + '] ' +
      ' Right Revoked from emp_id ' + FieldByName('emp_id').AsString +
      ', right_id: ' + FieldByName('right_id').AsString +
      ', effective right: ' + FieldByName('right_description').AsString;
      AdminDM.AdminChangeTable('employee_right', s, now); //QM-823
   end;
  LoadEffectiveRights;
end;

procedure TLocatorForm.GridEmailNotifExit(Sender: TObject);
begin
  inherited;
   PostDataSet(NotificationEmail);
end;

procedure TLocatorForm.GridViewColumnPosChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);   //QM-78
begin
  inherited;
  LayoutChanged := True;
end;

procedure TLocatorForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
  AValue: String;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then
  begin
    AValue := VarToStr(GridRec.Values[ColAtlasNumber.Index]);
    if VarToBoolean(GridRec.Values[ColTerminated.Index]) then
      ACanvas.Brush.Color := clRed
    else
    If AValue.Contains('CVOR') then
      ACanvas.Brush.Color := clWebOrangeRed
    else
    if not VarToBoolean(GridRec.Values[ColActive.Index]) and
      (VarToInt(GridRec.Values[ColEmpID.Index]) > 0) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;


procedure TLocatorForm.RightsFrameRightsGridDBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var
  RecValue: String;
begin
  inherited;
  if (AFocusedRecord <> nil) and (AFocusedRecord.Level > 0) then
  begin
    RecValue := AFocusedRecord.Values[2];
    SelectEffectiveRight(StrToInt(RecValue));
  end;
end;

procedure TLocatorForm.DSDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Field=nil then begin
    ShowUserData;
    ShowNotificationData;
    ShowPasswordHistory;
    ShowTicketGoals;
    LoadEffectiveRights;
    ShowEmpGroups;
  end;
  UserSheet.Enabled := not (Data.FieldByName('type_id').AsInteger = 1245);
end;

procedure TLocatorForm.editEmailEnter(Sender: TObject);
begin
  inherited;
  EditLoginIDEnter(Sender);
end;

procedure TLocatorForm.ShowUserData;
begin
  EmpUser.Close;
  EmpUser.Parameters[0].Value := Data.FieldByName('emp_id').Value;

//  if PC.ActivePage = UserSheet then  //neutered by SR
//  begin
//    EmpUser.Open;
//    SetLastLogInDiff;
//  end;
end;

procedure TLocatorForm.LeavingNow;
begin
  inherited;
  if LayoutChanged then
  begin
    GridView.StoreToIniFile(AdminDM.IniFileLayoutName, False);
    LayoutChanged := False;
  end;
  if RestoreItem <> nil then
  begin
    RestoreItem.Visible := False;
    RestoreItem.OnClick := nil;
  end;
  CloseDataSets;
end;

procedure TLocatorForm.LoadEffectiveRights;
begin
  EffectiveUserRights.Close;
  if PC.ActivePage = RightsSheet then begin
    EffectiveUserRights.Parameters[0].Value := Data.FieldByName('emp_id').Value;
    EffectiveUserRights.Open;
  end;
end;

procedure TLocatorForm.SelectEffectiveRight(RightId: Integer = -1);
var
  I: Integer;
begin
  if PC.ActivePage = RightsSheet then begin
    if RightId > -1 then
      with EffectiveRightsGridDBTableView1 do begin
        for I := 0 to DataController.RecordCount - 1 do
          if DataController.Values[I,0] = IntToStr(RightId) then begin
            ViewData.Rows[i].Focused := True;
            Exit;
          end;
        if DataController.RecordCount > 0 then
          ViewData.Rows[0].Focused := True;
      end;
  end;
end;

procedure TLocatorForm.TicketGoalsBeforeDelete(DataSet: TDataSet);
begin
  if MessageDlg('Are you sure you want to inactivate this work goal?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
    TBaseCxListForm(Self).DataBeforeDelete(DataSet)
  else
    Abort;
end;

procedure TLocatorForm.TicketGoalsNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('emp_id').AsInteger := Data.FieldByName('emp_id').AsInteger;
end;

procedure TLocatorForm.ShowNotificationData;
var
  empid: Integer;
begin
  PostDataSet(NotificationEmail);
  NotificationEmail.Close;
  if PC.ActivePage = NotificationEmailSheet then begin
    empid := Data.FieldByName('emp_id').Value;
    NotificationEmail.Parameters[0].Value := empid;
    NotificationEmail.Open;
  end;
end;

procedure TLocatorForm.ShowPasswordHistory;
begin
  PasswordHistory.Close;
  if PC.ActivePage = PasswordHistorySheet then begin
    PasswordHistory.Parameters[0].Value := Data.FieldByName('emp_id').Value;
    PasswordHistory.Open;
  end;
end;

procedure TLocatorForm.ShowTicketGoals;
begin
  TicketGoals.Close;
  if PC.ActivePage = TicketGoalsSheet then begin
    TicketGoals.Parameters[0].Value := Data.FieldByName('emp_id').Value;
    TicketGoals.Open;
  end;
end;

procedure TLocatorForm.DataAfterPost(DataSet: TDataSet);
var
  SQL: String;
  s: String;
const                                                    //QM-181
  UpdateTomorrowSQL = 'update employee set active = %d, company_id  = %d, ' +
    'report_to = %d, local_utc_bias = %d, atlas_number = %s, can_receive_tickets = %d, ' +
    'show_future_tickets = %d where type_id = 1245 and try_convert(int, dialup_user) = %d';

 function BoolToInt (b: Boolean): Integer;
 begin
   if b then result := 1 else result := 0;
 end;
begin
  inherited;
   if UpdateTomorrow then
   begin
    try
      SQL := Format( UpdateTomorrowSQL,
      [TomActive, LastCompanyID, LastReportTo, LastLocalUTC, quotedstr(LastAtlasNbr),
      BooltoInt(LastCanReceiveTickets), BooltoInt(LastShowFutureTickets), LastEmpid]);
      ExecuteQuery(Data.Connection, SQL);
    except
      raise;
    end;
   end;
   AdminDM.TrackAdminChanges(DataSet, ATableArray, 'employee', 'emp_id', EmpInserted);
   GridView.DataController.Refresh;
   ShowUserData;
end;

procedure TLocatorForm.SaveUserButtonClick(Sender: TObject);
begin
  PostDataSet(EmpUser);
end;

procedure TLocatorForm.EmpUserDSStateChange(Sender: TObject);
begin
  SaveUserButton.Enabled := EditingDataSet(EmpUser);
end;

procedure TLocatorForm.ShowEmpGroups;
begin
  if EmpGroups.Active then
    EmpGroups.Close;
  EmpGroups.Parameters.ParamByName('emp_id').Value := Data.FieldByName('emp_id').AsInteger;
  EmpGroups.Open;
end;

procedure TLocatorForm.ShowEmployee(ID: Integer);
var
  MsgStr: String;
begin
  If not Data.Locate('emp_id', ID, []) then
  begin
    MsgStr := 'Emp ID #' + InttoStr(ID) +
     ' not found in Employee view.';
    if ActiveLocatorsFilter.Checked then
       MsgStr := MsgStr + #13#10 + 'Check the inactive fields';
    MessageDlg(MsgStr, mtWarning, [mbOK], 0);
  end;
end;

procedure TLocatorForm.EmpUserBeforePost(DataSet: TDataSet);
begin
  Assert(not DataSet.FieldByName('emp_id').IsNull, 'An emp_id is required to save a user record');
end;

procedure TLocatorForm.DataBeforeScroll(DataSet: TDataSet);
begin
  inherited;
  if EmpRights.Active then
    RightID := EmpRights.FieldByName('right_id').AsInteger;
end;

procedure TLocatorForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
  DataSet.FieldByName('modified_date').AsDateTime := now;
end;

procedure TLocatorForm.DataAfterScroll(DataSet: TDataSet);
begin
  inherited;
  UserSheet.Enabled := Data.FieldByName('type_id').AsInteger <> 1245; //QM-181
  GridView.NavigatorButtons.Refresh.Enabled :=       //QM-181
  (GridView.DataController.EditState <> [dceInsert]) and
    (Data.FieldByName('type_id').AsInteger <> 1245) and
    (Data.FieldByName('Active').AsBoolean = True);
  if (EmpRights.Active) and (RightID > 0) and (not EmpRights.IsEmpty) then
    EmpRights.Locate('right_id', RightID, []);
end;

procedure TLocatorForm.NotificationEmailAfterPost(DataSet: TDataSet);
var
  s:String;
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, NotificationEmailsArray,
     'notification_email_user', 'emp_id', NEInserted);
end;

procedure TLocatorForm.NotificationEmailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    AdminDM.StoreFieldValues(DataSet, NotificationEmailsArray); //QM-823
end;

procedure TLocatorForm.NotificationEmailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  NEInserted := True; //QM-823
end;

procedure TLocatorForm.NotificationEmailNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('emp_id').AsInteger := Data.FieldByName('emp_id').AsInteger;
end;

procedure TLocatorForm.DataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  AddLookupField('TypeCode', Data, 'type_id', TypeRef, 'ref_id', 'code');
  DataSet.FieldByname('TypeCode').LookupCache := True;
  AddLookupField('TimeRule', Data, 'timerule_id', TimeRuleRef, 'ref_id', 'code');
  DataSet.FieldByName('TimeRule').LookupCache := True;
  AddLookupField('CompanyLookup', Data, 'company_id', CompanyRef, 'company_id', 'name');
  DataSet.FieldByname('CompanyLookup').LookupCache := True;
end;

procedure TLocatorForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  EmpInserted := False;
end;

procedure TLocatorForm.DataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('modified_date').AsDateTime := now;
end;

procedure TLocatorForm.DataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Data.FieldByName('emp_id').Required := True;
  Data.FieldByName('short_name').Required := True;
end;

procedure TLocatorForm.TomorrowActiveState(DataSet: TDataSet);
var  //QM-200
  AtlasNumber: String;
  DialUpUser: String;
  EmpID: Integer;
  UserMessage: String;
  acount: integer;
  P: Pchar;
  const
    CheckEmpActiveFlag = 'select active from employee where emp_id = :dialup_user_empid';
    CheckTomExists = 'select active from employee where try_convert(int, dialup_user) = :emp_id and type_id = 1245';
    CheckForDupTomorrow = 'select count(*) N from employee ' +
      'where dialup_user = :dialup_user and type_id = :type_id and active = 1';
begin
  try
    TomActive := 1;
    EmpID := DataSet.FieldByName('emp_id').AsInteger;
    LastEmpID := EmpID;

    UpdateTomorrow := False;
    if (DataSet.FieldByName('type_id').AsInteger <> 1245) then
    begin;
      TmpQuery.SQL.Text := CheckTomExists; //QM- 342  BP
      TmpQuery.Parameters.ParamByName('emp_id').Value := Dataset.FieldByName('emp_id').AsInteger; //QM- 342  BP
      TmpQuery.Open; //QM- 342  BP
      if not TmpQuery.EOF then
      begin
        If (TmpQuery.FieldByName('Active').AsBoolean = True) then
          UpdateTomorrow := True;
      end;
      TmpQuery.Close;
    end;

    if DataSet.FieldByName('Active').AsBoolean <> ActiveFlag then
    begin
      if DataSet.FieldByName('type_id').AsInteger = 1245 then
      begin
        DialupUser := DataSet.FieldByName('dialup_user').AsString;
        if DialUpUser = '' then
        begin
          DataSet.FieldByName('active').AsBoolean := False;

          P:=PChar('There is no dialup user entry for this tomorrow bucket');
          PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
          raise EOdDataEntryError.Create('');
        end;

        TmpQuery.SQL.Text := CheckEmpActiveFlag;

        TmpQuery.Parameters.ParamByName('dialup_user_empid').Value := StrtoIntDef(DataSet.FieldByName('dialup_user').AsString, 0);
        TmpQuery.Open;
        if not TmpQuery.EOF then
        begin
          if not TmpQuery.FieldByName('active').AsBoolean and DataSet.FieldByName('active').AsBoolean then
          begin
            TmpQuery.Close;
            DataSet.FieldByName('active').AsBoolean := False; //ActiveFlag;
            P:=PChar('Employee must be Active');
            PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
            raise EOdDataEntryError.Create('');
          end else
          if TmpQuery.FieldByName('active').AsBoolean and DataSet.FieldByName('active').AsBoolean then
          begin
            try
              TmpQuery.Close;
              TmpQuery.SQL.Text :=  CheckForDupTomorrow;
              TmpQuery.Parameters.ParamByName('dialup_user').Value := DialupUser;
              TmpQuery.Parameters.ParamByName('type_id').Value := 1245;
              TmpQuery.Open;
              if TmpQuery.FieldByName('N').AsInteger > 0 then
              begin
                DataSet.FieldByName('active').AsBoolean := False;
                P:=PChar('Cannot have more than one active tomorrow bucket for an employee');
                PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
                raise EOdDataEntryError.Create('');
              end;
            finally
              TmpQuery.Close;
            end;
          end;
        end else
        begin
          DataSet.FieldByName('active').AsBoolean := False;
          P:=PChar('dialup user Emp ID for this tomorrow bucket not found');
                PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
          raise EOdDataEntryError.Create('');
        end;
      end else
      begin
        if not Dataset.FieldByName('active').AsBoolean then
        begin
          With TermEmpInfo, TermEmpInfo.Parameters do
          begin
            ParamByName('woempid').Value := EmpID;
            ParamByName('tempid').Value := EmpID;
            ParamByName('dempid').Value := EmpID;
            ParamByName('aempid').Value := EmpID;
            Open;
            if FieldbyName('total').AsInteger > 0 then
            begin
              UserMessage := 'Cannot deactivate account. Terminated employee has ' + slinebreak;
              acount := FieldbyName('wocount').AsInteger;
              if acount >0 then
                UserMessage := UserMessage + slinebreak + 'open work orders: ' + InttoStr(acount);
              acount := FieldbyName('tcount').AsInteger;
              if acount > 0  then
                UserMessage := UserMessage + slinebreak + 'open tickets: ' + InttoStr(acount);
              acount := FieldByName('dcount').AsInteger;
              if acount > 0  then
                UserMessage := UserMessage + slinebreak + 'open damages: ' + InttoStr(acount);
              acount := FieldByName('acount').AsInteger;
              if acount > 0 then
                UserMessage := UserMessage + slinebreak + 'assigned areas: ' + InttoStr(acount);
              Data.FieldByName('active').AsBoolean := true;

              P:=PChar(UserMessage);
                PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
              Abort;
            end;
            if (FieldbyName('total').AsInteger = 0) and UpdateTomorrow then
              If MessageDlg('Deactivate Tomorrow Bucket?', mtConfirmation, [mbYes, mbNo], mrNo) = mrYes then
                Tomactive := 0;
            Close;
          end;
        end;
      end;
    end;
    If TmpQuery.Active then TmpQuery.Close;
  except
    On E:Exception do
    begin
      TmpQuery.Close;
      TermEmpInfo.Close;
      raise;
    end;
  end;
end;

procedure TLocatorForm.umMyMessage(var Message: TMessage);
var P : PChar;
begin
  P:=PChar(Message.wParam);
  MessageDlg(String(P), mtError, [mbOk], 0);
end;

procedure TLocatorForm.DataBeforePost(DataSet: TDataSet);
var
  AtlasNumber: String;
  ErrMsg: String;
  Query: TDataset;
  DialUpUser: Integer;
  EmpActive: Boolean;
  UserMessage: String;
  acount: integer;
  ADataSet: TADODataset;
  P: Pchar;
  oldvalue, newvalue: variant;
  s: String;
const
  ReportToSQL = 'Select emp_id from employee where active = 1 and report_to = ';
  TicketViewMin = 0;
  TicketViewMax = 50;
begin
  inherited;
  try
    //BP QM-600
    OldValue := DataSet.FieldbyName('terminated').oldValue;
    NewValue := DataSet.FieldbyName('terminated').NewValue;
    if ((OldValue = Null) or not OldValue) and
    ((NewValue <> Null) and NewValue) then
    begin
      ADataSet := CreateDatasetWithQuery(Data.Connection, ReportToSQL + Data.FieldByName('emp_id').AsString);
      ADataSet.Open;
      if not ADataSet.EOF then
      begin
        ADataSet.Close;
        ADataSet.Free;
        DataSet.FieldByName('terminated').AsBoolean := False;
        DataSet.Cancel;
        P:=PChar('This user has people reporting to him/her');
        PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
           ADataSet.Close;
        Abort;
      end;
      ADataSet.Close;
      ADataSet.Free;
    end;
    //BP QM-600
    if DataSet.FieldByName('terminated').AsBoolean then
    begin
      If DataSet.FieldByName('termination_date').IsNull then
      begin
        P:=PChar('A termination date is required');
        PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
        raise EOdDataEntryError.Create('');
      end;
      Exit;
    end;

    If DataSet.FieldByName('company_id').IsNull then
    begin
      P:=PChar('Company is required to save a user record');
      PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
      raise EOdDataEntryError.Create('');
      Exit;
    end;

    DataSet.FieldByName('changeby').AsString := AdminDM.CurrentUser;
    if DataSet.FieldByName('type_id').AsString <> '1245' then
    begin
      AtlasNumber := DataSet.FieldbyName('atlas_number').AsString;
      if String.IsNullOrWhiteSpace(AtlasNumber) then
      begin
        P:=PChar('Atlas Number must not be empty');
        PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
        raise EOdDataEntryError.Create('');
      end;
      if AtlasNumber = PriorAtlas then
      begin
        P:=PChar('Atlas Number must not equal previous entry');
        PostMessage(Handle, UM_MYMESSAGE, Integer(P),0);
        raise EOdDataEntryError.Create('');
      end;

      if (DataSet.FieldByName('ticket_view_limit').AsInteger < TicketViewMin) or
        (DataSet.FieldByName('ticket_view_limit').AsInteger > TicketViewMax) then
      begin
        MessageDlg(Format('The ticket view limit must be between %d and %d.', [TicketViewMin, TicketViewMax]), mtError, [mbOk], 0);
        GridView.DataController.Refresh;
         raise EOdDataEntryError.Create('');
      end;
      ValidateEmpHierarchy;
      if IsEmpty(Data.FieldByName('payroll_pc_code').AsString) then
        Data.FieldByName('payroll_pc_code').Clear;
      if not IsEmpty(DataSet.FieldByName('timerule').AsString) and   //BP QM-405
        IsEmpty(DataSet.FieldByName('ad_username').AsString) then
      begin
        MessageDlg('AD Username is required for employees with a time rule', mtError, [mbOk], 0);
        GridView.DataController.Refresh;
        raise EOdDataEntryError.Create('');
      end;

      if DS.State = dsEdit then
      begin
        TomorrowActiveState(Data);  //QM-200
        if UpdateTomorrow then   //QM-181
        begin
          LastCompanyID := DataSet.FieldByName('company_id').AsInteger;
          LastReportTo := DataSet.FieldByName('report_to').AsInteger;
          LastLocalUTC := DataSet.FieldByName('local_utc_bias').AsInteger;
          LastAtlasNbr := DataSet.FieldByName('atlas_number').AsString;
          LastCanReceiveTickets  := DataSet.FieldByName('can_receive_tickets').AsBoolean;
          LastShowFutureTickets  := DataSet.FieldByName('show_future_tickets').AsBoolean;
        end;
      end;
    end;
  except
    On E:EOdDataEntryError do
    begin
      abort;  // QM-49  SR  don't know why this fixes it!!
    end else
    begin
      raise;
    end;
  end;
end;

procedure TLocatorForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.ConfigDM.GetProfitCenterList(CxComboBoxItems(ColPayrollPCCode));
  GetEmailTypeList(CxComboBoxItems(ColNotifType));
  TicketGoalDateEdit.Date := Date;
end;

procedure TLocatorForm.AddGoalButtonClick(Sender: TObject);
var
  TicketCount: Integer;
  EmpID: Integer;
  s:String;

  procedure Validate;
  const
    CheckForDupGoal = 'select count(*) N from employee_work_goal ' +
      'where emp_id=%d and effective_date=%s and active=1';
  var
    Data: TDataSet;
  begin
    if StrToIntDef(TicketGoalEdit.Text, -1) < 0 then begin
      TicketGoalEdit.SetFocus;
      raise EOdDataEntryError.Create('Ticket Count must be a positive number.');
    end;
    TicketCount := StrToInt(TicketGoalEdit.Text);

    if TicketGoalDateEdit.Text = '' then begin
      TicketGoalDateEdit.SetFocus;
      raise EOdDataEntryError.Create('Effective Date cannot be blank.');
    end;

    Data := CreateDataSetWithQuery(TicketGoals.Connection,
      Format(CheckForDupGoal, [EmpID, IsoDateToStrQuoted(TicketGoalDateEdit.Date)]));
    try
      if Data.FieldByName('N').AsInteger > 0 then
        raise EOdDataEntryError.Create('Cannot add more than one active goal for an employee and effective date.');
    finally
      FreeAndNil(Data);
    end;
  end;

begin
  EmpID := Data.FieldByName('emp_id').AsInteger;
  Validate;
  try
    TicketGoals.Insert;
    TicketGoals.FieldByName('emp_id').AsInteger := EmpID;
    TicketGoals.FieldByName('effective_date').AsDateTime := TicketGoalDateEdit.Date;
    TicketGoals.FieldByName('ticket_count').AsInteger := TicketCount;
    TicketGoals.FieldByName('active').AsBoolean := True;
    TicketGoals.FieldByName('added_date').AsDateTime := Now;
    TicketGoals.Post;
     s := s + 'Ticket Goal added to emp_id: ' + TicketGoals.FieldByName('emp_id').AsString +
      ', effective date: ' + TicketGoals.FieldByName('effective_date').AsString +
      ', ticket_count: ' + InttoStr(TicketCount) +
      ', added date: ' + TicketGoals.FieldByName('added_date').AsString;
       AdminDM.AdminChangeTable('employee_work_goal', s, now); //QM-823
  except
    CancelDataSet(TicketGoals);
    raise;
  end;
  TicketGoalEdit.Clear;
end;


procedure TLocatorForm.GridViewNavigatorButtonsButtonClick(Sender: TObject;
  AButtonIndex: Integer; var ADone: Boolean);
begin  //QM-181
  inherited;
  if (AButtonIndex = NBDI_REFRESH) then  //customize Navigator Refresh btn
  begin
    AddTomorrowBucket;
    ADone := True;
  end;
end;

procedure TLocatorForm.AddTomorrowBucket;
var  //QM-181
  empid: Integer;
  ShortName: String;
  CompanyID: String;
  AtlasNbr: String;
  ReportTo: Integer;
  LocalUTC: Integer;
  CanReceiveTickets: Boolean;
  ShowFutureTickets: Boolean;
  p: Integer;

  procedure Validate;
  const
    CheckForDupTomorrow = 'select count(*) N from employee ' +
      'where dialup_user = :dialup_user and type_id = :type_id ' +
      'and active = 1';
  var
    Query: TDataSet;
  begin
    try
      tmpQuery.SQL.Text := CheckForDupTomorrow;
      tmpQuery.Parameters.ParamByName('dialup_user').Value := EmpID;
      tmpQuery.Parameters.ParamByName('type_id').Value := 1245;
      tmpQuery.Open;
      if tmpQuery.FieldByName('N').AsInteger > 0 then
        raise EOdDataEntryError.Create('Cannot add more than one tomorrow bucket for an employee');
    finally
      tmpQuery.Close;
    end;
  end;
begin
  ShortName := Data.FieldByName('short_name').AsString;
  If MessageDlg('Add Tomorrow Bucket for ' + ShortName +'?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then Exit;
  empid := Data.FieldByName('emp_id').AsInteger;
  Validate;
  CompanyID := Data.FieldByName('company_id').AsString;
  ReportTo := Data.FieldByName('report_to').AsInteger;
  LocalUTC := Data.FieldByName('local_utc_bias').AsInteger;
  CanReceiveTickets := Data.FieldByName('can_receive_tickets').AsBoolean;
  ShowFutureTickets := Data.FieldByName('show_future_tickets').AsBoolean;
  AtlasNbr := Data.FieldByName('atlas_number').AsString;
  p:=LastDelimiter(' ',ShortName);
  ShortName:=Copy(ShortName,1,p-1) + Copy(ShortName,p+1,1) + '.TOM';
  try
    Data.Insert;
    Data.FieldByName('dialup_user').AsInteger := empid;
    Data.FieldByName('short_name').AsString := ShortName;
    Data.FieldByName('report_to').AsInteger := ReportTo;
    Data.FieldByName('company_id').AsString := CompanyID;
    Data.FieldByName('atlas_number').AsString := AtlasNbr;
    Data.FieldByName('local_utc_bias').AsInteger := LocalUTC;
    Data.FieldbyName('can_receive_tickets').AsBoolean := CanReceiveTickets;
    Data.FieldbyName('show_future_tickets').AsBoolean := ShowFutureTickets;
    Data.FieldByName('active').AsBoolean := True;
    Data.FieldByName('type_id').AsInteger := 1245;
    Data.FieldByName('crew_num').AsString := '9999';
    Data.FieldByName('charge_cov').AsBoolean := False;
    Data.FieldByName('incentive_pay').AsBoolean := False;
    Data.FieldByName('changeby').AsString := AdminDM.CurrentUser;
    Data.BeforePost := nil;
    Data.Post;
    Data.BeforePost := DataBeforePost;
    RefreshNow;
    Data.Locate('dialup_user', empid, []);
  except
    CancelDataSet(Data);
    raise;
  end;
end;

procedure TLocatorForm.btnEmployeeInfoClick(Sender: TObject);
begin
  inherited;
  DisplayTermEmpInfo(Data.FieldByName('emp_id').AsInteger);
end;

procedure TLocatorForm.btnUserLinkClick(Sender: TObject);
begin
  inherited;
  //GridView.Controller.HideFindPanel;
  MainForm.ShowUser(Data.FieldByName('emp_id').AsInteger);
end;

procedure TLocatorForm.ChangePasswordButtonClick(Sender: TObject);
begin
  if IsEmpty(EmpUser.FieldByName('login_id').AsString) then
    raise EOdDataEntryError.Create('Please set the login ID before setting the password');
  PostDataSet(EmpUser);
  ChangeUserPassword(EmpUser.FieldByName('uid').AsInteger, dbPassword, Data);
end;

procedure TLocatorForm.EmpRightsAfterScroll(DataSet: TDataSet);
var
  MinWidth: Integer;
begin
  if NotEmpty(DataSet.FieldByName('modifier_desc').AsString) then
    RightsFrame.LimitationHintLabel.Caption := DataSet.FieldByName('modifier_desc').AsString
  else
    RightsFrame.LimitationHintLabel.Caption := RightLimitationUndefined;

  if NotEmpty(DataSet.FieldByName('limitation').AsString) then
    RightsFrame.EditLimitation.Text := DataSet.FieldByName('limitation').AsString
  else
    RightsFrame.EditLimitation.Text := '';
end;

procedure TLocatorForm.EmpGroupsAfterScroll(DataSet: TDataSet);
begin
  if AdminDM.UserHasRestrictedAccess then Exit;
  AddEmpGroupButton.Enabled := (EmpGroups.FieldByName('employee_in_group').AsString <> 'Y') and
                               (not IsReadOnly);
  RemoveEmpGroupButton.Enabled := (EmpGroups.FieldByName('employee_in_group').AsString = 'Y') and
                                  (not EmpGroups.FieldByName('is_default').AsBoolean) and
                                  (not IsReadOnly) ;
end;

procedure TLocatorForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
  RightsRow: TcxCustomGridRow;
begin
  inherited;
  if AdminDM.UserHasRestrictedAccess then Exit;

  RightsRow :=  RightsFrame.RightsGridDBTableView1.Controller.FocusedRow;
  RightsFrame.GrantButton.Enabled := (RightsRow <> nil) and (RightsRow.IsData) and (not MainForm.EditButton.Enabled);
  RightsFrame.RevokeButton.Enabled := (RightsRow <> nil) and (RightsRow.IsData) and (not MainForm.EditButton.Enabled);
end;

procedure TLocatorForm.AddEmpGroupButtonClick(Sender: TObject);
var
  s: String;
begin
  AddEmpGroupCommand.Parameters.ParamByName('emp_id').Value := Data.FieldByName('emp_id').AsInteger;
  AddEmpGroupCommand.Parameters.ParamByName('group_id').Value := EmpGroups.FieldByName('group_id').AsInteger;
  AddEmpGroupCommand.Execute;

  s := s + 'emp_id: ' + Data.FieldByName('emp_id').AsString +  ' added to Employee Group: ' +
     EmpGroups.FieldByName('group_id').AsString;
  AdminDM.AdminChangeTable('employee_group', s, now); //QM-823

  EmpGroups.Requery;
end;

procedure TLocatorForm.ActivatingNow;
begin
  inherited;
  if RestoreItem <> nil then  //QM-78
  begin
    RestoreItem.Visible := True;
    RestoreItem.OnClick :=  RestoreClick;
  end;
  NEInserted := False;
  EmpInserted := False;
end;

procedure TLocatorForm.ActiveLocatorsFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: String;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('emp_id').AsString;
  Data.Close;
  if ActiveLocatorsFilter.Checked then
    Data.CommandText :=
    'select e.*, usr.last_login ' +
    'from employee e left join users usr ' +
    'on e.emp_id = usr.emp_id ' +
    'where e.active = 1 ' +
    'order by e.short_name'
  else
    Data.CommandText :=
    'select e.*, usr.last_login ' +
    'from employee e left join users usr ' +
    'on e.emp_id = usr.emp_id ' +
    'order by e.short_name';
  Data.Open;
  Data.Locate('emp_id', FSelectedID, []);
end;

procedure TLocatorForm.RemoveEmpGroupButtonClick(Sender: TObject);
var
 s: String;
begin
  RemoveEmpGroupCommand.Parameters.ParamByName('emp_group_id').Value := EmpGroups.FieldByName('emp_group_id').AsInteger;
  RemoveEmpGroupCommand.Execute;

  s := s + 'emp_id: ' + Data.FieldByName('emp_id').AsString +  ' removed from Employee Group: ' +
     EmpGroups.FieldByName('group_id').AsString;
  AdminDM.AdminChangeTable('employee_group', s, now); //QM-823

  EmpGroups.Requery;
end;

procedure TLocatorForm.RestoreClick(Sender: TObject);  //QM-78
begin
  DefaultLayoutStorage(colRestore, Self, GridView, AdminDM.IniFileLayoutName);
  LayoutChanged := True;
end;

procedure TLocatorForm.ValidateEmpHierarchy;
const
  CheckForLoop = 'select count(*) N from dbo.get_hier(%d, 0) where h_emp_id = %d';
  CheckForNoEmp = 'select count(*) N from employee where emp_id = %d';
  CheckForMultiRoot = 'select count(*) N from employee where emp_id <> %d and ' +
    'report_to is null and active = 1';
  NestingError= 'Employees can not report to people who already work under them (%d works under under %d).';
var
  EmpID: Integer;
  ReportToID: Integer;
  Hier: TADODataSet;
begin
  EmpID := Data.FieldByName('emp_id').AsInteger;
  ReportToID := Data.FieldByName('report_to').AsInteger;

  if (EmpID > 0) and (EmpID = ReportToID) then
  begin
    MessageDlg('Employees can not report to themselves.', mtError, [mbOk], 0);
    raise EOdDataEntryError.Create('');
  end;
  Hier := CreateDataSetWithQuery(AdminDM.Conn, Format(CheckForLoop, [EmpID, ReportToID]));
  try
    if Hier.FieldByName('N').AsInteger > 0 then
    begin
      MessageDlg(Format(NestingError, [ReportToID, EmpID]), mtError, [mbOk], 0);
      raise EOdDataEntryError.Create('');
    end;

    if (ReportToID = 0) and Data.FieldByName('active').AsBoolean then begin
      Hier.Close;
      Hier.CommandText := Format(CheckForMultiRoot, [EmpID]);
      Hier.Open;
      if Hier.FieldByName('N').AsInteger > 0 then
      begin
        MessageDlg('Only one active employee can have a blank report to manager.', mtError, [mbOk], 0);
        raise EOdDataEntryError.Create('');
      end;
    end;

    if ReportToID <> 0 then begin
      Hier.Close;
      Hier.CommandText := Format(CheckForNoEmp, [ReportToID]);
      Hier.Open;
      if Hier.FieldByName('N').AsInteger = 0 then
      begin
        MessageDlg(Format('Manager %d does not exist in the employee table.', [ReportToID]), mtError, [mbOk], 0);
        raise EOdDataEntryError.Create('');
      end;
    end;
  finally
    FreeAndNil(Hier);
  end;
end;

procedure TLocatorForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  PasswordHistoryGrid.ReadOnly := True;

  // TicketGoalsGrid edit and insert is always off
  TicketGoalsGridView.OptionsData.Appending := False;
  TicketGoalsGridView.OptionsData.Inserting := False;
  TicketGoalsGridView.OptionsData.Editing := False;

  //Don't Disable the Filter
  ActiveLocatorsFilter.Enabled := True;
  btnUserLink.Enabled := True;
  btnEmployeeInfo.Enabled := True;
//  BtnEmployeeInfo.Enabled := (not Data.FieldbyName('emp_id').IsNull)    //qm-374  sr
//     and (not Data.FieldByName('Termination_date').IsNull);
end;


procedure TLocatorForm.SetLastLogInDiff;
var
  Year, Month, Day, Hour, Min, Sec, mSec : Word;
  mins, hrs: Integer; // QMANTWO-352
  utcDateTime, localDateTime: TDateTime;
  function MinutesToStrEx(const Minutes: Cardinal; strLocal:string ): string; // QMANTWO-352
  var
    D, H, M: Integer;
    L : string;
  begin
    L := strLocal;
    M := Minutes;
    H := M div 60;
    M := M mod 60;
    D := H div 24;
    H := H mod 24;
    if D > 0 then
    begin
      if (H <> 0) or (M <> 0) then
        Result := Format('%d days %d hours and %d minutes ago (%s local)', [D, H, M, L])
      else
        Result := Format('%d days ago (%s local)', [D, L]);
    end
    else if H > 0 then
    begin
      if M > 0 then
        Result := Format('%d hours and %d minutes ago (%s local)', [H, M, L])
      else
        Result := Format('%d minutes ago (%s local)', [M, L]);
    end
    else
      Result := Format('%d minutes ago (%s local)', [M, L]);
  end;

begin
  utcDateTime := EmpUser.FieldByName('last_login').AsDateTime;
  localDateTime := UTCToSystemTime(utcDateTime);
  mins := MinutesBetween(localDateTime, Now());
//  if IsDST then
//  begin
//    decodedatetime(localDateTime, Year, Month, Day, Hour, Min, Sec, mSec);
//    Hour := Hour-1;
//    localDateTime := encodedatetime(Year, Month, Day, Hour, Min, Sec, mSec);
//    mins := MinutesBetween(localDateTime, Now());
//  end;
  localLastLogIn.Caption := MinutesToStrEx(mins, DateTimeToStr(localDateTime));
  localLastLogIn.Refresh;
end;


function TLocatorForm.UTCToSystemTime(UTC: TDateTime): TDateTime; //QMANTWO-352
var
  TimeZoneInf: TIME_ZONE_INFORMATION; //QMANTWO-352
  UTCTime, LocalTime: TSystemTime;
  r : integer;
begin
  r := GetTimeZoneInformation(TimeZoneInf);
  IsDST :=  (r = TIME_ZONE_ID_DAYLIGHT);
  if r > 0 then
  begin

    DatetimetoSystemTime(UTC, UTCTime);
    if SystemTimeToTzSpecificLocalTime(@TimeZoneInf, UTCTime, LocalTime) then
    begin
      result := SystemTimeToDateTime(LocalTime);
    end
    else
      result := UTC;
  end
  else
    result := UTC;
end;

end.
