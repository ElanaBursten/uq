inherited UserForm: TUserForm
  Left = 361
  Top = 212
  Caption = 'Users'
  ClientHeight = 509
  ClientWidth = 950
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 353
    Width = 950
    Height = 9
    Cursor = crVSplit
    Align = alBottom
    Color = clBtnFace
    ParentColor = False
  end
  inherited TopPanel: TPanel
    Width = 950
    Height = 29
    object chkboxUserSearch: TCheckBox
      Left = 26
      Top = 4
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxUserSearchClick
    end
    object ActiveUsersFilter: TCheckBox
      Left = 165
      Top = 4
      Width = 136
      Height = 17
      Caption = 'Active Users Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveUsersFilterClick
    end
    object btnEmployeeLink: TcxButton
      Left = 308
      Top = 2
      Width = 112
      Height = 20
      Caption = 'Find in Employees'
      Colors.Normal = clMoneyGreen
      TabOrder = 2
      OnClick = btnEmployeeLinkClick
    end
  end
  inherited Grid: TcxGrid
    Top = 29
    Width = 950
    Height = 324
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.KeyFieldNames = 'uid'
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = ColLoginID
      OptionsCustomize.ColumnFiltering = True
      OptionsData.Appending = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      object ColLoginID: TcxGridDBColumn
        Caption = 'Login ID'
        DataBinding.FieldName = 'login_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 74
      end
      object ColPassword: TcxGridDBColumn
        Caption = 'Password'
        DataBinding.FieldName = 'password'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 58
      end
      object ColFirstName: TcxGridDBColumn
        Caption = 'First Name'
        DataBinding.FieldName = 'first_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 84
      end
      object ColLastName: TcxGridDBColumn
        Caption = 'Last Name'
        DataBinding.FieldName = 'last_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 98
      end
      object ColEmailAddress: TcxGridDBColumn
        Caption = 'Email'
        DataBinding.FieldName = 'email_address'
        PropertiesClassName = 'TcxMaskEditProperties'
      end
      object ColChgPwd: TcxGridDBColumn
        Caption = 'chg pwd'
        DataBinding.FieldName = 'chg_pwd'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 46
      end
      object ColChgPwdDate: TcxGridDBColumn
        Caption = 'Expiration'
        DataBinding.FieldName = 'chg_pwd_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 77
      end
      object ColGrpID: TcxGridDBColumn
        Caption = 'Group'
        DataBinding.FieldName = 'grp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 54
      end
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Focusing = False
        Options.FilteringPopupIncrementalFiltering = True
        Width = 66
      end
      object ColActiveInd: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active_ind'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 38
      end
      object ColUID: TcxGridDBColumn
        Caption = 'UID'
        DataBinding.FieldName = 'uid'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 56
      end
      object ColCanViewHistory: TcxGridDBColumn
        Caption = 'See Hist'
        DataBinding.FieldName = 'can_view_history'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 55
      end
      object ColCanViewNotes: TcxGridDBColumn
        Caption = 'See Notes'
        DataBinding.FieldName = 'can_view_notes'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 55
      end
      object ColETL: TcxGridDBColumn
        Caption = 'ETL Reports'
        DataBinding.FieldName = 'etl_access'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Width = 68
      end
      object ColAcctDisabledDate: TcxGridDBColumn
        Caption = 'Acct. Disabled Date'
        DataBinding.FieldName = 'Account_Disabled_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Properties.Nullstring = '0001-1-1'
        Properties.SaveTime = False
        Properties.ShowTime = False
        Width = 101
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 362
    Width = 950
    Height = 147
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      950
      147)
    object Label1: TLabel
      Left = 372
      Top = 7
      Width = 241
      Height = 25
      AutoSize = False
      Caption = 'Selected user has access to these clients online:'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 10
      Top = 7
      Width = 115
      Height = 13
      Caption = 'All clients in the system:'
    end
    object AddButton: TButton
      Left = 306
      Top = 80
      Width = 54
      Height = 25
      Caption = 'Add >>'
      TabOrder = 0
      OnClick = AddButtonClick
    end
    object ChangePassword: TButton
      Left = 823
      Top = 6
      Width = 110
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Change Password'
      TabOrder = 1
      OnClick = ChangePasswordClick
    end
    object AccessClientsGrid: TcxGrid
      Left = 370
      Top = 27
      Width = 282
      Height = 114
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 2
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object AccessClientsGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = UserClientsDS
        DataController.Filter.Active = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.MRUItemsList = False
        Filtering.ColumnMRUItemsList = False
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.IncSearch = True
        OptionsBehavior.IncSearchItem = ColLoginID
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object AccessClientsGridViewcall_center: TcxGridDBColumn
          Caption = 'Call Center'
          DataBinding.FieldName = 'call_center'
          DataBinding.IsNullValueType = True
          Width = 166
        end
        object AccessClientsGridViewoc_code: TcxGridDBColumn
          Caption = 'OC Code'
          DataBinding.FieldName = 'oc_code'
          DataBinding.IsNullValueType = True
          Width = 98
        end
      end
      object AccessClientsGridLevel1: TcxGridLevel
        GridView = AccessClientsGridView
      end
    end
    object AllClientsGrid: TcxGrid
      Left = 12
      Top = 27
      Width = 288
      Height = 114
      Anchors = [akLeft, akTop, akBottom]
      TabOrder = 3
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object AllClientsGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmManual
        FindPanel.HighlightSearchResults = False
        FindPanel.ShowCloseButton = False
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = ClientsDS
        DataController.Filter.Active = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.MRUItemsList = False
        Filtering.ColumnMRUItemsList = False
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.IncSearch = True
        OptionsBehavior.IncSearchItem = ColLoginID
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.Appending = True
        OptionsData.Deleting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object AllClientsGridViewColcall_center: TcxGridDBColumn
          Caption = 'Call Center'
          DataBinding.FieldName = 'call_center'
          DataBinding.IsNullValueType = True
          SortIndex = 0
          SortOrder = soAscending
          Width = 157
        end
        object AllClientsGridViewColoc_code: TcxGridDBColumn
          Caption = 'OC Code'
          DataBinding.FieldName = 'oc_code'
          DataBinding.IsNullValueType = True
          Width = 113
        end
      end
      object AllClientsGridLevel1: TcxGridLevel
        GridView = AllClientsGridView
      end
    end
    object chkboxClientSearch: TCheckBox
      Left = 199
      Top = 6
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 4
      OnClick = chkboxClientSearchClick
    end
    object btnEmployeeInfo: TButton
      Left = 823
      Top = 88
      Width = 110
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Employee Workload'
      TabOrder = 5
      OnClick = btnEmployeeInfoClick
    end
    object dbPassword: TcxDBCheckBox
      Left = 875
      Top = 37
      Anchors = [akTop, akRight]
      Caption = 'chg pwd'
      DataBinding.DataField = 'chg_pwd'
      DataBinding.DataSource = DS
      Properties.NullStyle = nssUnchecked
      Properties.ReadOnly = True
      Style.TransparentBorder = False
      TabOrder = 6
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterOpen = DataAfterOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from users where active_ind = 1'#13#10'order by login_id'
    object Datauid: TAutoIncField
      FieldName = 'uid'
      ReadOnly = True
    end
    object Datagrp_id: TIntegerField
      FieldName = 'grp_id'
    end
    object Dataemp_id: TIntegerField
      FieldName = 'emp_id'
    end
    object Datafirst_name: TStringField
      FieldName = 'first_name'
    end
    object Datalast_name: TStringField
      FieldName = 'last_name'
      Size = 30
    end
    object Datalogin_id: TStringField
      FieldName = 'login_id'
    end
    object Datapassword: TStringField
      FieldName = 'password'
    end
    object Datachg_pwd: TBooleanField
      FieldName = 'chg_pwd'
    end
    object Datalast_login: TDateTimeField
      FieldName = 'last_login'
    end
    object Dataactive_ind: TBooleanField
      FieldName = 'active_ind'
    end
    object Datacan_view_notes: TBooleanField
      FieldName = 'can_view_notes'
    end
    object Datacan_view_history: TBooleanField
      FieldName = 'can_view_history'
    end
    object Datachg_pwd_date: TDateTimeField
      FieldName = 'chg_pwd_date'
    end
    object Dataemail_address: TStringField
      DisplayWidth = 30
      FieldName = 'email_address'
      Size = 80
    end
    object Dataetl_access: TBooleanField
      FieldName = 'etl_access'
    end
    object DataAccount_Disabled_date: TDateTimeField
      FieldName = 'Account_Disabled_date'
    end
  end
  inherited DS: TDataSource
    OnDataChange = DSDataChange
  end
  object UserClients: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = UserClientsBeforeInsert
    AfterPost = UserClientsAfterPost
    BeforeDelete = UserClientsBeforeDelete
    AfterDelete = UserClientsAfterDelete
    OnNewRecord = UserClientsNewRecord
    CommandText = 
      'select users_client.*, '#13#10'client.oc_code, client.call_center'#13#10' fr' +
      'om users_client'#13#10' inner join client on users_client.client_id=cl' +
      'ient.client_id'#13#10'where users_client.uid=:uid'#13#10'order by client.cal' +
      'l_center, client.oc_code'
    DataSource = DS
    Parameters = <
      item
        Name = 'uid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 510
      end>
    Left = 56
    Top = 160
  end
  object UserClientsDS: TDataSource
    DataSet = UserClients
    Left = 120
    Top = 160
  end
  object Clients: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from client where active=1'
    Parameters = <>
    Left = 56
    Top = 208
  end
  object ClientsDS: TDataSource
    DataSet = Clients
    Left = 112
    Top = 208
  end
  object TermEmpInfo: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select t1.wocount as wocount,t2.tcount as tcount,'#13#10't3.dcount as ' +
      'dcount, t4.acount as acount,'#13#10' t1.wocount + t2.tcount + t3.dcoun' +
      't + t4.acount as total from '#13#10'('#13#10'select count(*) as wocount from' +
      ' work_order wo'#13#10'where wo.assigned_to_id = :woempid'#13#10'and wo.close' +
      'd = 0'#13#10'    and wo.active = 1 )  as t1,'#13#10'('#9#13#10'select count(*) as t' +
      'count from ticket'#13#10'where ticket_id in ('#13#10'   select ticket_id fro' +
      'm locate'#13#10'    where assigned_to = :tempid'#13#10'    and active = 1'#13#10' ' +
      '   and closed = 0)  ) as t2,'#13#10'( '#13#10'select count(*)  as dcount fro' +
      'm damage d where'#13#10'd.investigator_id = :dempid'#13#10'    and d.damage_' +
      'type in ('#39'INCOMING'#39', '#39'PENDING'#39')'#13#10'    and d.active = 1 ) as t3, '#13 +
      #10'('#13#10'select count(*) as acount from area '#13#10'  where locator_id = :' +
      'aempid ) as t4'
    Parameters = <
      item
        Name = 'woempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'tempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'dempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'aempid'
        Size = -1
        Value = Null
      end>
    Prepared = True
    Left = 185
    Top = 120
  end
  object ColorDialog1: TColorDialog
    Color = 6634959
    CustomColors.Strings = (
      'ColorA=653DCF'
      'ColorB=FFFFFFFF'
      'ColorC=FFFFFFFF'
      'ColorD=FFFFFFFF'
      'ColorE=FFFFFFFF'
      'ColorF=FFFFFFFF'
      'ColorG=FFFFFFFF'
      'ColorH=FFFFFFFF'
      'ColorI=653DCF'
      'ColorJ=FFFFFFFF'
      'ColorK=FFFFFFFF'
      'ColorL=FFFFFFFF'
      'ColorM=FFFFFFFF'
      'ColorN=FFFFFFFF'
      'ColorO=FFFFFFFF'
      'ColorP=FFFFFFFF')
    Left = 840
    Top = 192
  end
end
