inherited HighProfileGridForm: THighProfileGridForm
  Left = 247
  Top = 202
  Caption = 'High Profile Grid'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 37
    object Label1: TLabel
      Left = 8
      Top = 11
      Width = 55
      Height = 13
      Caption = 'Call center:'
    end
    object CallCenterFilter: TComboBox
      Left = 72
      Top = 8
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      TabOrder = 0
      OnSelect = CallCenterFilterSelect
    end
    object cbHPGridSearch: TCheckBox
      Left = 250
      Top = 12
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbHPGridSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 37
    Height = 363
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'call_center'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.NoDataToDisplayInfoText = '<No grids for selected center. Press Insert to add one.>'
      OptionsView.GroupByBox = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 146
      end
      object ColClientCode: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Properties.ImmediateDropDownWhenKeyPressed = False
        Width = 215
      end
      object ColGrid: TcxGridDBColumn
        Caption = 'Grid'
        DataBinding.FieldName = 'grid'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 167
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from hp_grid order by call_center, client_code, grid'
  end
  inherited DS: TDataSource
    Left = 112
  end
end
