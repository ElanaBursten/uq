unit BaseMap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, Grids, DB, ADODB;

type
  TBaseMapForm = class(TEmbeddableForm)
    GetAreaRepr: TADODataSet;
  private
    FGrid: TStringGrid;
  protected
    procedure ClearGrid;
    property Grid: TStringGrid read FGrid write FGrid;
    function CellContentsFor(area_id: Integer): string;
  end;

implementation

uses AdminDMu;

{$R *.dfm}

{ TBaseMapForm }

function TBaseMapForm.CellContentsFor(area_id: Integer): string;
begin
  if not GetAreaRepr.Active then
    GetAreaRepr.Open;

  if GetAreaRepr.Locate('area_id', area_id, []) then
    Result := GetAreaRepr.FieldByName('area_name').AsString + '/' +
      GetAreaRepr.FieldByName('short_name').AsString;
end;

procedure TBaseMapForm.ClearGrid;
var
  R, C: Integer;
begin
  Grid.Enabled := False;
  for R := 0 to Grid.RowCount-1 do
    for C := 0 to Grid.ColCount-1 do
      Grid.Cells[C, R] := '';
end;


end.

