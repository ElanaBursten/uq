inherited LocatorForm: TLocatorForm
  Left = 393
  Top = 232
  Caption = 'Employees'
  ClientHeight = 462
  ClientWidth = 1309
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 293
    Width = 1309
    Height = 9
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 1309
    Height = 40
    Alignment = taLeftJustify
    object lblInstructions: TLabel
      Left = 4
      Top = 24
      Width = 440
      Height = 13
      Caption = 
        ' Uncheck Locator'#39's Active Flag instead of deleting them (ctrl-de' +
        'l permanently deletes them).'
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ActiveLocatorsFilter: TCheckBox
      Left = 130
      Top = 3
      Width = 136
      Height = 16
      Caption = 'Active Locators Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveLocatorsFilterClick
    end
    object btnUserLink: TcxButton
      Left = 635
      Top = 10
      Width = 89
      Height = 21
      Caption = 'Find in Users'
      Colors.Normal = clGradientActiveCaption
      TabOrder = 1
      OnClick = btnUserLinkClick
    end
    object btnEmployeeInfo: TButton
      Left = 893
      Top = 8
      Width = 110
      Height = 25
      Caption = 'Employee Workload'
      TabOrder = 2
      OnClick = btnEmployeeInfoClick
    end
  end
  inherited Grid: TcxGrid
    Top = 40
    Width = 1309
    Height = 253
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.OnButtonClick = GridViewNavigatorButtonsButtonClick
      Navigator.Buttons.Images = TomorrowImage
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Hint = 'Add Tomorrow bucket'
      Navigator.Buttons.Refresh.ImageIndex = 0
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.AutoDataSetFilter = True
      DataController.KeyFieldNames = 'emp_id'
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      OptionsBehavior.NavigatorHints = True
      OptionsCustomize.ColumnFiltering = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      OnColumnPosChanged = GridViewColumnPosChanged
      OnColumnSizeChanged = GridViewColumnPosChanged
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 47
      end
      object ColReportTo: TcxGridDBColumn
        Caption = 'Rpt To'
        DataBinding.FieldName = 'report_to'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 52
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 52
      end
      object ColAtlasNumber: TcxGridDBColumn
        Caption = 'Atlas Nbr'
        DataBinding.FieldName = 'atlas_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Width = 61
      end
      object ColPlatAge: TcxGridDBColumn
        Caption = 'Plat Age'
        DataBinding.FieldName = 'plat_age'
        Width = 56
      end
      object ColPlatUpdateDate: TcxGridDBColumn
        Caption = 'Plat Update Date'
        DataBinding.FieldName = 'plat_update_date'
        Width = 89
      end
      object ColEmpStatus: TcxGridDBColumn
        Caption = 'Emp Status'
        DataBinding.FieldName = 'status_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = EmpStatusLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = EmpStatusLookupViewColDescription
        Width = 60
      end
      object ColCanReceiveTickets: TcxGridDBColumn
        Caption = 'Rcv Tkts'
        DataBinding.FieldName = 'can_receive_tickets'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 59
      end
      object ColTypeCode: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'type_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = EmpTypeLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = EmpTypeLookupViewColCode
        Width = 40
      end
      object ColEmpNumber: TcxGridDBColumn
        Caption = 'Emp #'
        DataBinding.FieldName = 'emp_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 57
      end
      object ColShortName: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 74
      end
      object ColFirstName: TcxGridDBColumn
        Caption = 'First Name'
        DataBinding.FieldName = 'first_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 66
      end
      object ColMiddleInit: TcxGridDBColumn
        Caption = 'Md Intl'
        DataBinding.FieldName = 'middle_init'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 33
      end
      object ColLastName: TcxGridDBColumn
        Caption = 'Last Name'
        DataBinding.FieldName = 'last_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 72
      end
      object ColContactPhone: TcxGridDBColumn
        Caption = 'Contact Phone'
        DataBinding.FieldName = 'contact_phone'
        Width = 84
      end
      object ColLastLogin: TcxGridDBColumn
        Caption = 'Last Login'
        DataBinding.FieldName = 'last_login'
        Options.Editing = False
        Width = 65
      end
      object ColPhoneCompany: TcxGridDBColumn
        Caption = 'Phone Co'
        DataBinding.FieldName = 'phoneco_ref'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = PhoneCoLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = PhoneCoLookupViewColCode
        Width = 56
      end
      object ColTimeRule: TcxGridDBColumn
        Caption = 'Time Rule'
        DataBinding.FieldName = 'TimeRule'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = TimeRuleLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = TimeRuleLookupViewColCode
        Width = 65
      end
      object ColChargeCov: TcxGridDBColumn
        Caption = 'Chg COV'
        DataBinding.FieldName = 'charge_cov'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.MultiLine = True
        Properties.NullStyle = nssUnchecked
        Width = 72
      end
      object ColCompanyCar: TcxGridDBColumn
        Caption = 'COV'
        DataBinding.FieldName = 'company_car'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'COV'
        Properties.ValueUnchecked = 'NONE'
        Width = 35
      end
      object ColIncentivePay: TcxGridDBColumn
        Caption = 'Incentive Pay'
        DataBinding.FieldName = 'incentive_pay'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Width = 72
      end
      object ColDialupUser: TcxGridDBColumn
        Caption = 'Dialup User'
        DataBinding.FieldName = 'dialup_user'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Width = 61
      end
      object ColPCCode: TcxGridDBColumn
        Caption = 'Ctrl PC'
        DataBinding.FieldName = 'repr_pc_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Visible = False
        Options.Filtering = False
        Width = 53
      end
      object ColPayrollPCCode: TcxGridDBColumn
        Caption = 'Payroll PC'
        DataBinding.FieldName = 'payroll_pc_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ClearKey = 46
        Properties.DropDownListStyle = lsEditFixedList
        Properties.DropDownRows = 14
        Properties.MaxLength = 0
        Width = 55
      end
      object ColCrewNum: TcxGridDBColumn
        Caption = 'Crew'
        DataBinding.FieldName = 'crew_num'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = CrewNumLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = CrewNumLookupViewColCode
        Width = 41
      end
      object ColAutoClose: TcxGridDBColumn
        Caption = 'Autoclose'
        DataBinding.FieldName = 'autoclose'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Width = 54
      end
      object ColCompanyLookup: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'CompanyLookup'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.View = CompanyLookupView
        Properties.KeyFieldNames = 'company_id'
        Properties.ListFieldItem = CompanyLookupViewColName
        Width = 70
      end
      object ColTicketViewLimit: TcxGridDBColumn
        Caption = 'Ticket Limit'
        DataBinding.FieldName = 'ticket_view_limit'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        HeaderAlignmentHorz = taRightJustify
        Width = 58
      end
      object ColHireDate: TcxGridDBColumn
        Caption = 'Hire Date'
        DataBinding.FieldName = 'hire_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Width = 50
      end
      object ColShowFutureTickets: TcxGridDBColumn
        Caption = 'Show Future Work'
        DataBinding.FieldName = 'show_future_tickets'
        Width = 93
      end
      object ColLocalUTC: TcxGridDBColumn
        Caption = 'Local UTC'
        DataBinding.FieldName = 'local_utc_bias'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 63
      end
      object ColTestComLogin: TcxGridDBColumn
        Caption = 'Test Com Login'
        DataBinding.FieldName = 'test_com_login'
        Width = 81
      end
      object ColAdUserName: TcxGridDBColumn
        Caption = 'AD Username'
        DataBinding.FieldName = 'ad_username'
        Width = 53
      end
      object ColADPLogin: TcxGridDBColumn
        Caption = 'ADP Login'
        DataBinding.FieldName = 'adp_login'
        Width = 56
      end
      object ColHomeLat: TcxGridDBColumn
        Caption = 'Home Lat'
        DataBinding.FieldName = 'home_lat'
        Width = 55
      end
      object ColHomeLng: TcxGridDBColumn
        Caption = 'Home Lng'
        DataBinding.FieldName = 'home_lng'
        Width = 59
      end
      object ColAltLat: TcxGridDBColumn
        Caption = 'Alt Lat'
        DataBinding.FieldName = 'alt_lat'
        Width = 40
      end
      object ColAltLng: TcxGridDBColumn
        Caption = 'Alt Lng'
        DataBinding.FieldName = 'alt_lng'
        Width = 38
      end
      object ColChangeBy: TcxGridDBColumn
        Caption = 'Change By'
        DataBinding.FieldName = 'changeby'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = False
        Options.Editing = False
        Width = 61
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.AutoSelect = False
        Properties.InputKind = ikStandard
        Properties.ShowTime = False
        Options.Editing = False
        Width = 70
      end
      object ColFirstTaskReminder: TcxGridDBColumn
        Caption = 'First Task Reminder'
        DataBinding.FieldName = 'first_task_reminder'
        Width = 56
      end
      object ColTerminated: TcxGridDBColumn
        Caption = 'Terminated'
        DataBinding.FieldName = 'terminated'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.OnEditValueChanged = ColTerminatedPropertiesEditValueChanged
        Width = 64
      end
      object ColEmpTermDate: TcxGridDBColumn
        Caption = 'Termination Date'
        DataBinding.FieldName = 'termination_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.ImmediateDropDownWhenActivated = True
        Properties.InputKind = ikRegExpr
        Width = 94
      end
    end
    object EmpTypeLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = TypeRefDS
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object EmpTypeLookupViewColCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 74
      end
      object EmpTypeLookupViewColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 185
      end
      object EmpTypeLookupViewColRefID: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Width = 61
      end
    end
    object TimeRuleLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = TimeRuleRefDS
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object TimeRuleLookupViewColCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 83
      end
      object TimeRuleLookupViewColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 165
      end
      object TimeRuleLookupViewColRefID: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 42
      end
    end
    object CrewNumLookupView: TcxGridDBTableView [3]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = CrewNumDS
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object CrewNumLookupViewColCode: TcxGridDBColumn
        Caption = 'Crew Code'
        DataBinding.FieldName = 'code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 130
      end
      object CrewNumLookupViewColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 252
      end
      object CrewNumLookupViewColRefID: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 42
      end
    end
    object CompanyLookupView: TcxGridDBTableView [4]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = CompanyRefDS
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'company_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object CompanyLookupViewColName: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 139
      end
    end
    object EmpStatusLookupView: TcxGridDBTableView [5]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmpStatusDS
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object EmpStatusLookupViewColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
      end
      object EmpStatusLookupViewColRefID: TcxGridDBColumn
        Caption = 'Ref ID'
        DataBinding.FieldName = 'ref_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Visible = False
      end
    end
    object PhoneCoLookupView: TcxGridDBTableView [6]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = PhoneCoRefDS
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object PhoneCoLookupViewColCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        DataBinding.IsNullValueType = True
      end
      object PhoneCoLookupViewColDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        DataBinding.IsNullValueType = True
      end
      object PhoneCoLookupViewColRefID: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        DataBinding.IsNullValueType = True
        Visible = False
      end
    end
  end
  object Panel6: TPanel [3]
    Left = 0
    Top = 302
    Width = 1309
    Height = 160
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object PC: TPageControl
      Left = 0
      Top = 16
      Width = 1309
      Height = 144
      ActivePage = NotificationEmailSheet
      Align = alClient
      TabOrder = 0
      OnChange = PCChange
      object InfoSheet: TTabSheet
        Caption = 'Explanation'
        ImageIndex = 2
        object InfoLabelStat: TLabel
          Left = -4
          Top = 3
          Width = 369
          Height = 113
          AutoSize = False
          Caption = 
            'The Status ID field is not used by QManager.  It is used by the ' +
            'web-based system that shares the QManager database.  You should ' +
            'not set the Payroll PC (Profit Center) field unless the employe'#39 +
            's profit payroll center is not determined by the hierarchy.'
          WordWrap = True
        end
      end
      object UserSheet: TTabSheet
        Caption = 'User Setup'
        object Label11: TLabel
          Left = 61
          Top = 49
          Width = 43
          Height = 13
          Caption = 'Login ID:'
          FocusControl = EditLoginID
        end
        object Label12: TLabel
          Left = 54
          Top = 78
          Width = 50
          Height = 13
          Caption = 'Password:'
        end
        object Label13: TLabel
          Left = 52
          Top = 105
          Width = 52
          Height = 13
          Caption = 'Last Login:'
          Visible = False
        end
        object Label14: TLabel
          Left = 64
          Top = 26
          Width = 40
          Height = 13
          Caption = 'User ID:'
        end
        object Label15: TLabel
          Left = 9
          Top = 9
          Width = 179
          Height = 13
          Caption = 'User information for selected locator:'
        end
        object DBEdit4: TDBText
          Left = 121
          Top = 26
          Width = 164
          Height = 18
          Color = clBtnFace
          DataField = 'uid'
          DataSource = EmpUserDS
          ParentColor = False
        end
        object DBEdit3: TDBText
          Left = 121
          Top = 105
          Width = 109
          Height = 19
          Color = clBtnFace
          DataField = 'last_login'
          DataSource = EmpUserDS
          ParentColor = False
          Visible = False
        end
        object Label6: TLabel
          Left = 312
          Top = 55
          Width = 209
          Height = 33
          AutoSize = False
          Caption = 
            'Login ID changes are not automatically saved, you must click the' +
            ' Save button.'
          WordWrap = True
        end
        object Label1: TLabel
          Left = 76
          Top = 129
          Width = 28
          Height = 13
          Caption = 'Email:'
        end
        object Label2: TLabel
          Left = 312
          Top = 129
          Width = 169
          Height = 13
          Caption = '* Techs use utiliquestops.com'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 312
          Top = 144
          Width = 191
          Height = 13
          Caption = '* Management use utiliquest.com'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 312
          Top = 158
          Width = 190
          Height = 13
          Caption = '* LocatingInc use locatinginc.com'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 232
          Top = 105
          Width = 20
          Height = 13
          Caption = 'UTC'
          Visible = False
        end
        object localLastLogIn: TLabel
          Left = 267
          Top = 105
          Width = 358
          Height = 19
          AutoSize = False
          Visible = False
        end
        object EditLoginID: TDBEdit
          Left = 121
          Top = 46
          Width = 164
          Height = 21
          DataField = 'login_id'
          DataSource = EmpUserDS
          TabOrder = 0
          OnEnter = EditLoginIDEnter
        end
        object SaveUserButton: TButton
          Left = 312
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Save'
          Enabled = False
          TabOrder = 1
          OnClick = SaveUserButtonClick
        end
        object ChangePasswordButton: TButton
          Left = 120
          Top = 73
          Width = 165
          Height = 25
          Caption = 'Change Password'
          TabOrder = 2
          OnClick = ChangePasswordButtonClick
        end
        object editEmail: TDBEdit
          Left = 121
          Top = 130
          Width = 164
          Height = 21
          TabStop = False
          DataField = 'email_address'
          DataSource = EmpUserDS
          ReadOnly = True
          TabOrder = 3
          OnEnter = editEmailEnter
        end
        object dbPassword: TcxDBCheckBox
          Left = 315
          Top = 80
          Caption = 'chg pwd'
          DataBinding.DataField = 'chg_pwd'
          DataBinding.DataSource = DS
          Properties.NullStyle = nssUnchecked
          Properties.ReadOnly = True
          Style.TransparentBorder = False
          TabOrder = 4
        end
      end
      object AssignedAreaSheet: TTabSheet
        Caption = 'Assigned Areas'
        ImageIndex = 1
        object AssignedAreasGrid: TDBGrid
          Left = 0
          Top = 0
          Width = 1301
          Height = 116
          Align = alClient
          DataSource = AssignedAreasDS
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'area_name'
              Title.Caption = 'Area'
              Width = 137
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'map_name'
              Title.Caption = 'Map'
              Width = 178
              Visible = True
            end>
        end
      end
      object RightsSheet: TTabSheet
        Caption = 'User Rights'
        ImageIndex = 3
        object Splitter2: TSplitter
          Left = 361
          Top = 0
          Width = 7
          Height = 116
        end
        object EffectiveRightsGrid: TcxGrid
          Left = 0
          Top = 0
          Width = 361
          Height = 116
          Align = alLeft
          TabOrder = 0
          object EffectiveRightsGridDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            DataController.DataSource = EffectiveUserRightsDS
            DataController.Filter.Active = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.GroupByBox = False
            object colEffectiveRightId: TcxGridDBColumn
              DataBinding.FieldName = 'right_id'
              DataBinding.IsNullValueType = True
              Visible = False
            end
            object colEffectiveRight: TcxGridDBColumn
              Caption = 'Effective Right'
              DataBinding.FieldName = 'right_description'
              DataBinding.IsNullValueType = True
              MinWidth = 150
              Options.Editing = False
              Options.Sorting = False
              Width = 183
            end
            object colEffectiveGroup: TcxGridDBColumn
              Caption = 'Granted by'
              DataBinding.FieldName = 'group_name'
              DataBinding.IsNullValueType = True
              Options.Editing = False
              Options.Sorting = False
              Width = 172
            end
          end
          object EffectiveRightsGridLevel1: TcxGridLevel
            GridView = EffectiveRightsGridDBTableView1
          end
        end
        inline RightsFrame: TRightsFrame
          Left = 383
          Top = 0
          Width = 918
          Height = 116
          Align = alClient
          TabOrder = 1
          inherited RightHeaderLabel: TLabel
            Left = 449
          end
          inherited ModifierLabel: TLabel
            Left = 538
          end
          inherited LimitationHintLabel: TLabel
            Left = 538
          end
          inherited GrantButton: TButton
            Left = 449
            Top = 43
            Action = GrantAction
          end
          inherited EditLimitation: TEdit
            Left = 530
            Top = 46
            Hint = 'right click to copy'
            PopupMenu = popupLimitation
          end
          inherited RevokeButton: TButton
            Left = 449
            Action = RevokeAction
          end
          inherited RightsGrid: TcxGrid
            Width = 438
            Height = 116
            inherited RightsGridDBTableView1: TcxGridDBTableView
              OnFocusedRecordChanged = RightsFrameRightsGridDBTableView1FocusedRecordChanged
              DataController.DataSource = emp_rightsDS
              DataController.Filter.Active = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              inherited ColGroupRightsGridCategory: TcxGridDBColumn
                DataBinding.IsNullValueType = True
                IsCaptionAssigned = True
              end
              inherited ColGroupRightsGridRight: TcxGridDBColumn
                DataBinding.IsNullValueType = True
                Width = 186
              end
              inherited ColGroupRightsGridID: TcxGridDBColumn
                DataBinding.IsNullValueType = True
              end
              inherited ColGroupRightsGridAllowed: TcxGridDBColumn
                DataBinding.IsNullValueType = True
                Width = 28
              end
              inherited ColGroupRightsGridLimitation: TcxGridDBColumn
                DataBinding.IsNullValueType = True
                Width = 205
              end
            end
          end
        end
        object SpacerPanel: TPanel
          Left = 368
          Top = 0
          Width = 15
          Height = 116
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
        end
      end
      object GroupsSheet: TTabSheet
        Caption = 'Employee Groups'
        ImageIndex = 6
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 283
          Height = 116
          Align = alLeft
          DataSource = EmpGroupsDS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'group_name'
              ReadOnly = True
              Title.Caption = 'Group'
              Width = 135
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'employee_in_group'
              ReadOnly = True
              Title.Caption = 'Employee in Group'
              Width = 104
              Visible = True
            end>
        end
        object AddEmpGroupButton: TButton
          Left = 289
          Top = 9
          Width = 160
          Height = 25
          Caption = 'Add Employee to Group'
          TabOrder = 1
          OnClick = AddEmpGroupButtonClick
        end
        object RemoveEmpGroupButton: TButton
          Left = 294
          Top = 40
          Width = 160
          Height = 25
          Caption = 'Remove Employee from Group'
          TabOrder = 2
          OnClick = RemoveEmpGroupButtonClick
        end
      end
      object NotificationEmailSheet: TTabSheet
        Caption = 'Notification Emails'
        ImageIndex = 4
        object NotifcationGrid: TcxGrid
          Left = 0
          Top = 0
          Width = 1301
          Height = 116
          Align = alClient
          TabOrder = 0
          object NotifcationGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Visible = True
            ScrollbarAnnotations.CustomAnnotations = <>
            DataController.DataSource = NotificationEmailDS
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.GroupByBox = False
            object ColNotifType: TcxGridDBColumn
              Caption = 'Type'
              DataBinding.FieldName = 'email_type'
              DataBinding.IsNullValueType = True
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.DropDownListStyle = lsEditFixedList
              Properties.ReadOnly = False
              Width = 140
            end
            object ColNotifDestination: TcxGridDBColumn
              Caption = 'Email Destination'
              DataBinding.FieldName = 'email_destination'
              DataBinding.IsNullValueType = True
              Width = 733
            end
          end
          object NotifcationGridLevel: TcxGridLevel
            GridView = NotifcationGridView
          end
        end
      end
      object PasswordHistorySheet: TTabSheet
        Caption = 'Password History'
        ImageIndex = 5
        object PasswordHistoryGrid: TDBGrid
          Left = 0
          Top = 0
          Width = 1301
          Height = 116
          Align = alClient
          DataSource = PasswordHistoryDS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'when_changed'
              Title.Caption = 'When'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ChangedByName'
              Title.Caption = 'Changed by User'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'changed_by'
              Title.Caption = 'Changed by Admin'
              Visible = True
            end>
        end
      end
      object TicketGoalsSheet: TTabSheet
        Caption = 'Ticket Goals'
        ImageIndex = 7
        object TicketGoalLabel: TLabel
          Left = 284
          Top = 37
          Width = 64
          Height = 13
          Caption = 'Ticket Count:'
        end
        object EffectiveDateLabel: TLabel
          Left = 284
          Top = 9
          Width = 73
          Height = 13
          Caption = 'Effective Date:'
        end
        object TicketGoalsGrid: TcxGrid
          Left = 0
          Top = 0
          Width = 278
          Height = 116
          Align = alLeft
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = True
          object TicketGoalsGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            DataController.DataSource = TicketGoalsDS
            DataController.KeyFieldNames = 'work_goal_id'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.ImmediateEditor = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.GroupByBox = False
            object ColEffectiveDate: TcxGridDBColumn
              Caption = 'Effective Date'
              DataBinding.FieldName = 'effective_date'
              DataBinding.IsNullValueType = True
              Options.Editing = False
              Options.Focusing = False
              Width = 98
            end
            object ColTicketCount: TcxGridDBColumn
              Caption = 'Ticket Count'
              DataBinding.FieldName = 'ticket_count'
              DataBinding.IsNullValueType = True
              HeaderAlignmentHorz = taRightJustify
              Options.Editing = False
              Options.Focusing = False
              Width = 87
            end
            object ColWorkGoalActive: TcxGridDBColumn
              Caption = 'Active'
              DataBinding.FieldName = 'active'
              DataBinding.IsNullValueType = True
              Options.Editing = False
              Width = 74
            end
          end
          object TicketGoalsGridLevel1: TcxGridLevel
            GridView = TicketGoalsGridView
          end
        end
        object TicketGoalEdit: TEdit
          Left = 362
          Top = 34
          Width = 77
          Height = 21
          TabOrder = 2
        end
        object AddGoalButton: TButton
          Left = 363
          Top = 61
          Width = 75
          Height = 25
          Caption = 'Add Goal'
          TabOrder = 3
          OnClick = AddGoalButtonClick
        end
        object TicketGoalDateEdit: TcxDateEdit
          Left = 363
          Top = 7
          Properties.SaveTime = False
          Properties.ShowTime = False
          TabOrder = 1
          Width = 106
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1309
      Height = 16
      Align = alTop
      BevelEdges = []
      BevelOuter = bvNone
      TabOrder = 1
      object EmpShortName: TDBText
        Left = 157
        Top = 0
        Width = 94
        Height = 14
        AutoSize = True
        DataField = 'short_name'
        DataSource = DS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EmpNum: TDBText
        Left = 43
        Top = 0
        Width = 53
        Height = 14
        AutoSize = True
        DataField = 'emp_number'
        DataSource = DS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EmpNumLabel: TLabel
        Left = 4
        Top = 1
        Width = 35
        Height = 13
        Caption = 'Emp #:'
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    AfterOpen = DataAfterOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterEdit = DataAfterEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    BeforeScroll = DataBeforeScroll
    AfterScroll = DataAfterScroll
    OnNewRecord = DataNewRecord
    CommandText = 
      'select e.*, usr.last_login, usr.chg_pwd'#13#10'from employee e '#13#10'left ' +
      'join users usr '#13#10'on e.emp_id = usr.emp_id '#13#10'where e.active = 1 '#13 +
      #10'order by e.short_name'
    FieldDefs = <
      item
        Name = 'emp_id'
        Attributes = [faReadonly, faFixed]
        DataType = ftAutoInc
      end
      item
        Name = 'type_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'status_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'timesheet_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'emp_number'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'short_name'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'first_name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'last_name'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'middle_init'
        Attributes = [faFixed]
        DataType = ftFixedChar
        Size = 1
      end
      item
        Name = 'report_to'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'create_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'create_uid'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'modified_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'modified_uid'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'charge_cov'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'active'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'can_receive_tickets'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'timerule_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'dialup_user'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'company_car'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'repr_pc_code'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'payroll_pc_code'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'crew_num'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'autoclose'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'company_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'rights_modified_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'ticket_view_limit'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'hire_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'show_future_tickets'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'incentive_pay'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'contact_phone'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'local_utc_bias'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'ad_username'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'test_com_login'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'adp_login'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'home_lat'
        Attributes = [faFixed]
        DataType = ftFMTBcd
        Precision = 9
        Size = 6
      end
      item
        Name = 'home_lng'
        Attributes = [faFixed]
        DataType = ftFMTBcd
        Precision = 9
        Size = 6
      end
      item
        Name = 'alt_lat'
        Attributes = [faFixed]
        DataType = ftFMTBcd
        Precision = 9
        Size = 6
      end
      item
        Name = 'alt_lng'
        Attributes = [faFixed]
        DataType = ftFMTBcd
        Precision = 9
        Size = 6
      end
      item
        Name = 'termination_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'plat_update_date'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'plat_age'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'changeby'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'phoneco_ref'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'atlas_number'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'first_task_reminder'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'terminated'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'last_login'
        Attributes = [faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'computer_model'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'asset_tag'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'aircard_phone_no'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'aircard_phone_esn'
        DataType = ftString
        Size = 20
      end>
    StoreDefs = True
    Left = 24
    Top = 104
    object Dataemp_id: TAutoIncField
      FieldName = 'emp_id'
      ReadOnly = True
    end
    object Datatype_id: TIntegerField
      FieldName = 'type_id'
    end
    object Datastatus_id: TIntegerField
      FieldName = 'status_id'
    end
    object Datatimesheet_id: TIntegerField
      FieldName = 'timesheet_id'
    end
    object Dataemp_number: TStringField
      FieldName = 'emp_number'
      Size = 15
    end
    object Dataatlas_number: TStringField
      FieldName = 'atlas_number'
    end
    object Datashort_name: TStringField
      FieldName = 'short_name'
      Size = 30
    end
    object Datafirst_name: TStringField
      FieldName = 'first_name'
    end
    object Datalast_name: TStringField
      FieldName = 'last_name'
      Size = 30
    end
    object Datamiddle_init: TStringField
      FieldName = 'middle_init'
      FixedChar = True
      Size = 1
    end
    object Datareport_to: TIntegerField
      FieldName = 'report_to'
    end
    object Datacreate_date: TDateTimeField
      FieldName = 'create_date'
    end
    object Datacreate_uid: TIntegerField
      FieldName = 'create_uid'
    end
    object Datamodified_uid: TIntegerField
      FieldName = 'modified_uid'
    end
    object Datacharge_cov: TBooleanField
      FieldName = 'charge_cov'
    end
    object Dataactive: TBooleanField
      FieldName = 'active'
    end
    object Datacan_receive_tickets: TBooleanField
      FieldName = 'can_receive_tickets'
    end
    object Datatimerule_id: TIntegerField
      FieldName = 'timerule_id'
    end
    object Datadialup_user: TStringField
      FieldName = 'dialup_user'
    end
    object Datacompany_car: TStringField
      FieldName = 'company_car'
      Size = 10
    end
    object Datarepr_pc_code: TStringField
      FieldName = 'repr_pc_code'
      Size = 15
    end
    object Datapayroll_pc_code: TStringField
      FieldName = 'payroll_pc_code'
      Size = 15
    end
    object Datacrew_num: TStringField
      FieldName = 'crew_num'
      Size = 5
    end
    object Dataautoclose: TBooleanField
      FieldName = 'autoclose'
    end
    object Datacompany_id: TIntegerField
      FieldName = 'company_id'
    end
    object Datarights_modified_date: TDateTimeField
      FieldName = 'rights_modified_date'
    end
    object Dataticket_view_limit: TIntegerField
      FieldName = 'ticket_view_limit'
    end
    object Datahire_date: TDateTimeField
      FieldName = 'hire_date'
    end
    object Datashow_future_tickets: TBooleanField
      FieldName = 'show_future_tickets'
    end
    object Dataincentive_pay: TBooleanField
      FieldName = 'incentive_pay'
    end
    object Datacontact_phone: TStringField
      FieldName = 'contact_phone'
    end
    object Datalocal_utc_bias: TIntegerField
      FieldName = 'local_utc_bias'
    end
    object Dataad_username: TStringField
      FieldName = 'ad_username'
      Size = 64
    end
    object Datatest_com_login: TStringField
      FieldName = 'test_com_login'
    end
    object Dataadp_login: TStringField
      FieldName = 'adp_login'
    end
    object Datahome_lat: TFMTBCDField
      FieldName = 'home_lat'
      Precision = 9
      Size = 6
    end
    object Datahome_lng: TFMTBCDField
      FieldName = 'home_lng'
      Precision = 9
      Size = 6
    end
    object Dataalt_lat: TFMTBCDField
      FieldName = 'alt_lat'
      Precision = 9
      Size = 6
    end
    object Dataalt_lng: TFMTBCDField
      FieldName = 'alt_lng'
      Precision = 9
      Size = 6
    end
    object Dataplat_exp_date: TDateTimeField
      FieldName = 'plat_update_date'
    end
    object Datalast_login: TDateTimeField
      FieldName = 'last_login'
    end
    object Dataplat_age: TIntegerField
      FieldName = 'plat_age'
      ReadOnly = True
    end
    object Dataphoneco_ref: TIntegerField
      FieldName = 'phoneco_ref'
    end
    object Datachangeby: TStringField
      FieldName = 'changeby'
      Size = 40
    end
    object Datamodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object Datafirst_task_reminder: TStringField
      FieldName = 'first_task_reminder'
      Size = 8
    end
    object Dataterminated: TBooleanField
      FieldName = 'terminated'
    end
    object Datatermination_date: TDateTimeField
      FieldName = 'termination_date'
    end
  end
  inherited DS: TDataSource
    OnDataChange = DSDataChange
    Left = 88
    Top = 104
  end
  object EmpUserDS: TDataSource
    DataSet = EmpUser
    OnStateChange = EmpUserDSStateChange
    Left = 136
    Top = 184
  end
  object TypeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference'#13#10' where type='#39'emptype'#39#13#10' order by ref_id'
    Parameters = <>
    Left = 264
    Top = 104
  end
  object TypeRefDS: TDataSource
    DataSet = TypeRef
    Left = 264
    Top = 152
  end
  object AssignedAreasDS: TDataSource
    AutoEdit = False
    DataSet = AssignedAreas
    Left = 480
    Top = 336
  end
  object TimeRuleRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select * from reference'#13#10' where type='#39'timerule'#39#13#10' order by ref_i' +
      'd'
    Parameters = <>
    Left = 408
    Top = 104
  end
  object TimeRuleRefDS: TDataSource
    DataSet = TimeRuleRef
    Left = 400
    Top = 152
  end
  object emp_rightsDS: TDataSource
    DataSet = EmpRights
    Left = 608
    Top = 88
  end
  object UpdateRightCommand: TADOCommand
    CommandText = 
      'update employee_right'#13#10' set allowed=:allowed,'#13#10'  limitation=:lim' +
      'itation'#13#10' where emp_right_id=:id'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'allowed'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'limitation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 200
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 784
    Top = 128
  end
  object InsertRightCommand: TADOCommand
    CommandText = 
      'insert into employee_right (emp_id, right_id, allowed, limitatio' +
      'n)'#13#10' values (:emp_id, :right_id, :allowed, :limitation )'#13#10
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'right_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'allowed'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'limitation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 200
        Value = Null
      end>
    Left = 544
    Top = 176
  end
  object NotificationEmailDS: TDataSource
    DataSet = NotificationEmail
    Left = 369
    Top = 201
  end
  object CrewNumDS: TDataSource
    DataSet = CrewNumRef
    Left = 472
    Top = 152
  end
  object CrewNumRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select * from reference'#13#10' where type='#39'crewnum'#39' and active_ind = ' +
      '1'#13#10' order by sortby, code'
    Parameters = <>
    Left = 472
    Top = 104
  end
  object CompanyRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select company_id, name'#13#10'from locating_company'#13#10'order by name'
    Parameters = <>
    Left = 208
    Top = 104
  end
  object CompanyRefDS: TDataSource
    DataSet = CompanyRef
    Left = 208
    Top = 152
  end
  object PasswordHistoryDS: TDataSource
    AutoEdit = False
    DataSet = PasswordHistory
    Left = 529
    Top = 345
  end
  object EmpGroups: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterScroll = EmpGroupsAfterScroll
    CommandText = 
      'select eg.emp_group_id, gd.group_id, gd.group_name, eg.emp_id, g' +
      'd.is_default,'#13#10'  case '#13#10'    when eg.group_id = gd.group_id then ' +
      #39'Y'#39#13#10'    when gd.is_default = 1 then '#39'Y'#39#13#10'    else '#39'-'#39#13#10'  end em' +
      'ployee_in_group'#13#10'from group_definition gd'#13#10'  left join employee_' +
      'group eg on eg.group_id = gd.group_id and eg.emp_id = :emp_id'#13#10'w' +
      'here gd.active = 1 '#13#10
    DataSource = DS
    Parameters = <
      item
        Name = 'emp_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 688
    Top = 96
  end
  object EmpGroupsDS: TDataSource
    DataSet = EmpGroups
    Left = 880
    Top = 96
  end
  object RemoveEmpGroupCommand: TADOCommand
    CommandText = 'delete from employee_group where emp_group_id = :emp_group_id'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'emp_group_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 664
    Top = 160
  end
  object AddEmpGroupCommand: TADOCommand
    CommandText = 
      'insert into employee_group (group_id, emp_id) values (:group_id,' +
      ' :emp_id)'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'group_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 880
    Top = 160
  end
  object TicketGoalsDS: TDataSource
    AutoEdit = False
    DataSet = TicketGoals
    Left = 609
    Top = 393
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 1192
    object GrantAction: TAction
      Caption = '&Grant'
      OnExecute = GrantActionExecute
    end
    object RevokeAction: TAction
      Caption = '&Revoke'
      OnExecute = RevokeActionExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
  end
  object EffectiveUserRightsDS: TDataSource
    DataSet = EffectiveUserRights
    Left = 96
    Top = 144
  end
  object EmpUser: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeEdit = EmpUserBeforeEdit
    BeforePost = EmpUserBeforePost
    AfterPost = EmpUserAfterPost
    OnNewRecord = EmpUserNewRecord
    CommandText = 'select * from users '#13#10'where emp_id=:emp_id'
    EnableBCD = False
    IndexFieldNames = 'emp_id'
    MasterFields = 'emp_id'
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 144
    Top = 104
    object EmpUseruid: TAutoIncField
      DisplayWidth = 10
      FieldName = 'uid'
      ReadOnly = True
    end
    object EmpUsergrp_id: TIntegerField
      DisplayWidth = 10
      FieldName = 'grp_id'
    end
    object EmpUseremp_id: TIntegerField
      DisplayWidth = 10
      FieldName = 'emp_id'
    end
    object EmpUserfirst_name: TStringField
      DisplayWidth = 20
      FieldName = 'first_name'
    end
    object EmpUserlast_name: TStringField
      DisplayWidth = 30
      FieldName = 'last_name'
      Size = 30
    end
    object EmpUserlogin_id: TStringField
      DisplayWidth = 20
      FieldName = 'login_id'
    end
    object EmpUserpassword: TStringField
      DisplayWidth = 20
      FieldName = 'password'
    end
    object EmpUserchg_pwd: TBooleanField
      DisplayWidth = 5
      FieldName = 'chg_pwd'
    end
    object EmpUserlast_login: TDateTimeField
      DisplayWidth = 18
      FieldName = 'last_login'
    end
    object EmpUseractive_ind: TBooleanField
      DisplayWidth = 5
      FieldName = 'active_ind'
    end
    object EmpUseremail_address: TStringField
      DisplayWidth = 80
      FieldName = 'email_address'
      Size = 80
    end
  end
  object AssignedAreas: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      'select area.area_name, map.map_name '#13#10' from area'#13#10'  inner join m' +
      'ap on area.map_id=map.map_id'#13#10'where area.locator_id=:emp_id'
    DataSource = DS
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 597
    Top = 313
  end
  object EmpRights: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterScroll = EmpRightsAfterScroll
    CommandText = 
      'select SUBSTRING(rd.right_description, 1, CHARINDEX('#39' - '#39', rd.ri' +
      'ght_description, 1)) as right_category,'#13#10' rd.right_description, ' +
      'rd.right_id, rd.modifier_desc, '#13#10'   er.emp_right_id, er.allowed,' +
      ' er.limitation, er.emp_id'#13#10' from right_definition rd'#13#10'  left joi' +
      'n employee_right er'#13#10'    on rd.right_id = er.right_id and er.emp' +
      '_id=:emp_id'#13#10' where rd.right_id not in (select value from config' +
      'uration_data where name like '#39'HiddenRightID%'#39')'#13#10' order by right_' +
      'category, rd.right_description, rd.right_id'#13#10
    DataSource = DS
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 536
    Top = 96
  end
  object PasswordHistory: TADODataSet
    AutoCalcFields = False
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select uph.*, u2.last_name + '#39', '#39' + u2.first_name as ChangedByNa' +
      'me, u.emp_id from user_password_history uph join users u on (uph' +
      '.uid = u.uid) left outer join users u2 on (uph.changed_by_uid = ' +
      'u2.uid) where (u.emp_id = :emp_id) order by when_changed desc'
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 425
    Top = 385
  end
  object TicketGoals: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = TicketGoalsBeforeDelete
    OnNewRecord = TicketGoalsNewRecord
    CommandText = 
      'select work_goal_id,emp_id, effective_date, ticket_count, added_' +
      'date, active from employee_work_goal where emp_id = :emp_id'
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 672
    Top = 369
  end
  object EffectiveUserRights: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select er.right_id, rd.right_description, grpdef.group_name from' +
      ' dbo.get_effective_rights(:emp_id) er '#13#10'join right_definition rd' +
      ' on er.right_id = rd.right_id '#13#10'left join group_definition grpde' +
      'f on grpdef.group_id = er.group_id '#13#10'where allowed = '#39'Y'#39' order b' +
      'y rd.right_description'
    EnableBCD = False
    Parameters = <
      item
        Name = 'emp_id'
        Size = -1
        Value = Null
      end>
    Left = 32
    Top = 160
  end
  object EmpStatusRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select * from reference'#13#10' where type='#39'empstat'#39' and active_ind = ' +
      '1'#13#10' order by sortby, code'
    Parameters = <>
    Left = 960
    Top = 104
  end
  object EmpStatusDS: TDataSource
    DataSet = EmpStatusRef
    Left = 984
    Top = 160
  end
  object PhoneCoRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference'#13#10' where type='#39'phoneco'#39#13#10' order by ref_id'
    Parameters = <>
    Left = 328
    Top = 104
  end
  object PhoneCoRefDS: TDataSource
    DataSet = PhoneCoRef
    Left = 328
    Top = 152
  end
  object TomorrowImage: TImageList
    Left = 704
    Top = 338
    Bitmap = {
      494C010101000800340010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000034343473777777AF686868A46B6B
      6BA4696969A36C6C6CA46C6C6CA5717171A7757575A9727272AB757575AC7777
      77AF797979B1777777B1868686BE353535740000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008D8D8DC6D3D3D3FFE1E1E1FDC3C3
      C3FFD5D5D5FFC8C8C8FFE3E3E3FFCDCDCDFFD0D0D0FFFFFFFFFFFDFDFDFFFDFD
      FDFFFDFDFDFFF4F4F4FDFFFFFFFF8C8C8CC80000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000828282BFC3C3C3FFC1C1C1F9B7B7
      B7FBC7C7C7FBB6B6B6FBD0D0D0FBBDBDBDFBC4C4C4FBE1E1E1FCDBDBDBFCDCDC
      DCFCD6D6D6FCD4D4D4FADDDDDDFF7E7E7EC10000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000858585C0BCBCBDFFBBBBBBFDC0C0
      C0FFD5D5D6FFBABABBFFD6D6D6FFC0C0C0FFCECECFFFC5C5C5FFB5B5B5FFCCCC
      CCFFB0B0B0FFCACACAFDADADADFF7E7E7EC10000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000818181BECCCCCAFFCDCDCCFDB9B9
      B8FFD2D2D2FFBBBBBAFFD5D5D5FFC2C2C1FFCCCCCCFFD3D3D3FFD3D3D3FFCDCD
      CDFFBBBBBBFFD2D2D2FDBEBEBEFF7A7A7ABE0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007E7E7EBCC7C7CAFFCECED0FDBDBD
      C1FFD4D4D5FFB4B4B8FFD2D2D4FFBCBCC0FFCBCBCDFFCCCCCCFFC9C9CAFFCCCC
      CCFFBABABBFFCECECFFDBABABBFF777777BC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007F7F7ABBA9A9E3FFCFCFDDFDABAB
      E1FFD6D6E5FFAEAED9FFCCCCDFFFB1B1D7FFC5C5E3FFC1C1B7FFB8B8B6FFD3D3
      D1FFC5C5C1FFCDCDCBFDC4C4BFFF757575BA0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000767675B9F0EFF3FFDDDCDDFDE4E3
      E6FFE5E4E2FFB8B7D6FFD3D2DDFFB8B7D1FFCBC9D8FFBDBCCCFFBEBDCEFFC8C6
      D0FFB5B3CBFFC6C5CCFDB9B8D3FF737370B80000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000076787AB8FFFFFFFFEFF4F7FDF8FD
      FFFFFBFFFBFFC3C8F4FFE1E6F8FFC9CEF2FFDEE3F9FFD1D7F3FFCDD2F4FFD9DF
      F2FFBBC0ECFFD6DAEBFDBEC3F4FF727473B60000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000624E39B7D6A775FFC59767FDBF8E
      63FFBC8B63FFC59362FFC18F62FFC49262FFC18F61FFC39264FFC49569FFC293
      68FFC59668FFBC8D63FDCD9A69FF554030B40000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000613E15B5D78C33FFC98C42FDC880
      2FFFBE7223FFBA722DFFB9712AFFBB732EFFBB742FFFB96D24FFB86310FFB965
      13FFB86516FFB56517FCC26B17FF522F0FB10000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000674517B2E8BA7CFFD9D3C9FADCC8
      ABFCDBC3A1FCDED1BFFCE2DDD6FCDED4C9FCDCD1C2FCD7BC9CFCC88B44FCCC90
      49FCD1872FFCCC852FF9DE9032FF5A3C18AE0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000075531DB6FFBF55FFF0B963FDF6BE
      69FFF6C06DFFF6BD66FFF7C06AFFF5BC64FFF5BB64FFF3B356FFEFAA47FFF0A9
      43FFF5A432FFEFA031FCFFAD34FF6A4918B20000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000042341C7DA77E3AC7967031BC9972
      31BE997130BE997232BE997130BE997232BE997232BE997333BE997334BE9972
      33BE987134BE966F32BDA77B36C8392C17760000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object TermEmpInfo: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select t1.wocount as wocount,t2.tcount as tcount,'#13#10't3.dcount as ' +
      'dcount, t4.acount as acount,'#13#10' t1.wocount + t2.tcount + t3.dcoun' +
      't + t4.acount as total from '#13#10'('#13#10'select count(*) as wocount from' +
      ' work_order wo'#13#10'where wo.assigned_to_id = :woempid'#13#10'and wo.close' +
      'd = 0'#13#10'    and wo.active = 1 )  as t1,'#13#10'('#9#13#10'select count(*) as t' +
      'count from ticket'#13#10'where ticket_id in ('#13#10'   select ticket_id fro' +
      'm locate'#13#10'    where assigned_to = :tempid'#13#10'    and active = 1'#13#10' ' +
      '   and closed = 0)  ) as t2,'#13#10'( '#13#10'select count(*)  as dcount fro' +
      'm damage d where'#13#10'd.investigator_id = :dempid'#13#10'    and d.damage_' +
      'type in ('#39'INCOMING'#39', '#39'PENDING'#39')'#13#10'    and d.active = 1 ) as t3, '#13 +
      #10'('#13#10'select count(*) as acount from area '#13#10'  where locator_id = :' +
      'aempid ) as t4'
    Parameters = <
      item
        Name = 'woempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'tempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'dempid'
        Size = -1
        Value = Null
      end
      item
        Name = 'aempid'
        Size = -1
        Value = Null
      end>
    Prepared = True
    Left = 785
    Top = 304
  end
  object TmpQuery: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'dialup_user'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'select active from employee where dialup_user = :dialup_user and' +
        ' active = 1')
    Left = 760
    Top = 362
  end
  object popupLimitation: TPopupMenu
    Left = 1032
    Top = 323
    object Copy1: TMenuItem
      Caption = 'Copy'
      OnClick = Copy1Click
    end
    object Cut1: TMenuItem
      Caption = 'Cut'
      OnClick = Cut1Click
    end
    object Paste1: TMenuItem
      Caption = 'Paste'
      OnClick = Paste1Click
    end
  end
  object NotificationEmail: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = NotificationEmailBeforeInsert
    BeforeEdit = NotificationEmailBeforeEdit
    BeforePost = EmpUserBeforePost
    AfterPost = NotificationEmailAfterPost
    OnNewRecord = NotificationEmailNewRecord
    CommandText = 'select * from notification_email_user where emp_id = :empid'
    Parameters = <
      item
        Name = 'empid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 272
    Top = 200
  end
end
