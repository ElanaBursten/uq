unit BillingRulesVirtualGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,  AdminDmu,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxDBExtLookupComboBox, Vcl.StdCtrls, dxSkinsCore, dxSkinBasic, dxCore, dxSkinsForm;

type
  TBillingRulesVirtualGroupForm = class(TBaseCxListForm)
    ColCustomerID: TcxGridDBColumn;
    ColCustomerGroupName: TcxGridDBColumn;
    ColCustomerGroupID: TcxGridDBColumn;
    ColCustomerName: TcxGridDBColumn;
    dsLookupCust: TDataSource;
    qryLookupCust: TADOQuery;
    CustLookupView: TcxGridDBTableView;
    CustLookupViewCustomerID: TcxGridDBColumn;
    CustLookupViewCustName: TcxGridDBColumn;
    chkboxVirtualGroupSearch: TCheckBox;
    dxSkinController1: TdxSkinController;
    procedure chkboxVirtualGroupSearchClick(Sender: TObject);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    VirtualBillInserted: Boolean;
  public
    { Public declarations }
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  BillingRulesVirtualGroupForm: TBillingRulesVirtualGroupForm;

implementation

{$R *.dfm}

{ TBillingRulesVirtualGroupForm }


procedure TBillingRulesVirtualGroupForm.chkboxVirtualGroupSearchClick(
  Sender: TObject);
begin
  inherited;
  if chkboxVirtualGroupSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TBillingRulesVirtualGroupForm.CloseDataSets;
begin
  inherited;
  qryLookupCust.Close;
end;

procedure TBillingRulesVirtualGroupForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  VirtualBillInserted := False;
end;

procedure TBillingRulesVirtualGroupForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'billing_rules_virtual_group', 'customer_id', VirtualBillInserted);
end;

procedure TBillingRulesVirtualGroupForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TBillingRulesVirtualGroupForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  VirtualBillInserted := True;
end;

procedure TBillingRulesVirtualGroupForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TBillingRulesVirtualGroupForm.ActivatingNow;
begin
  inherited;
  VirtualBillInserted := False;
end;

procedure TBillingRulesVirtualGroupForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TBillingRulesVirtualGroupForm.OpenDataSets;
begin
  qryLookupCust.Open;
  inherited;
end;

procedure TBillingRulesVirtualGroupForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxVirtualGroupSearch.Enabled := True;
end;

end.
