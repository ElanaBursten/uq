unit RespondTo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, cxDBExtLookupComboBox,
  cxDropDownEdit, odHourGlass, odMiscUtils, odcxUtils, AdminDmu;

type
  TRespondToForm = class(TBaseCxListForm)
    ActiveRespondToFilter: TCheckBox;
    chkboxRespondToSearch: TCheckBox;
    ColRespondTo: TcxGridDBColumn;
    ColURL: TcxGridDBColumn;
    ColUserName: TcxGridDBColumn;
    ColPassword: TcxGridDBColumn;
    ColRespondToNote: TcxGridDBColumn;
    ColAPIKey: TcxGridDBColumn;
    ColIncludeNotes: TcxGridDBColumn;
    ColRealLoctaorName: TcxGridDBColumn;
    ColOneCallCenter: TcxGridDBColumn;
    ColIncludeEPRLink: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColAllVersions: TcxGridDBColumn;
    ColProcedureName: TcxGridDBColumn;
    ColResponoderName: TcxGridDBColumn;
    ColResponderComment: TcxGridDBColumn;
    procedure ActiveRespondToFilterClick(Sender: TObject);
    procedure chkboxRespondToSearchClick(Sender: TObject);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PopulateDropDowns; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

var
  RespondToForm: TRespondToForm;

implementation

{$R *.dfm}

procedure TRespondToForm.ActiveRespondToFilterClick(Sender: TObject);
var
  Cursor: IInterface;
  FSelectedID: Integer;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('respond_to_id').AsInteger;
  Data.Close;
  if ActiveRespondToFilter.Checked then
    Data.CommandText := 'Select * from respond_to where active = 1' else
      Data.CommandText := 'select * from respond_to';
  Data.Open;
  Data.Locate('respond_to_id', FSelectedID, []);
end;

procedure TRespondToForm.chkboxRespondToSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxRespondToSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TRespondToForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  if Trim(Data.FieldByName('respond_to').AsString) = '' then
    raise Exception.Create('Respond To field is required to save a respondto record');
end;

procedure TRespondToForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TRespondToForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TRespondToForm.PopulateDropDowns;
begin
  inherited;
  AdminDM.GetCallCenterList(CxComboBoxItems(ColOneCallCenter));
end;

procedure TRespondToForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TRespondToForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TRespondToForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveRespondToFilter.Enabled := True;
  chkboxRespondToSearch.Enabled := True;
end;

end.
