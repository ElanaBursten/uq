inherited EvalParserForm: TEvalParserForm
  Caption = 'Eval Parser'
  ClientHeight = 423
  ClientWidth = 650
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 335
    Width = 650
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 650
    Height = 29
    object lblFileCount: TLabel
      Left = 400
      Top = 11
      Width = 89
      Height = 13
      Caption = 'Parser File Count: '
    end
  end
  inherited Grid: TcxGrid
    Top = 29
    Width = 650
    Height = 306
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ColHistoryID: TcxGridDBColumn
        Caption = 'history id'
        DataBinding.FieldName = 'import_history_id'
        Width = 70
      end
      object ColSourceFile: TcxGridDBColumn
        Caption = 'source file'
        DataBinding.FieldName = 'source_file'
        Width = 168
      end
      object ColMessage: TcxGridDBColumn
        Caption = 'message text'
        DataBinding.FieldName = 'message_text'
        Width = 263
      end
      object ColStatusCode: TcxGridDBColumn
        DataBinding.FieldName = 'status_code'
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 343
    Width = 650
    Height = 80
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 106
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandText = 
      'Use [UQView]'#13#10'select import_history_id, source_file, message_tex' +
      't, status_code'#13#10'from Eval_Import_History'#13#10'where cast(import_date' +
      ' as date) ='#13#10'dateadd(day, datediff(day, 0, GETDATE()), 0) and st' +
      'atus_code = 1'#13#10
    Left = 40
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Use [QM]'#13#10'select hal_module, emails  from hal '#13#10'where hal_module' +
      ' = '#39'eval_parser'#39
    Parameters = <>
    Left = 452
    Top = 104
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 516
    Top = 104
  end
end
