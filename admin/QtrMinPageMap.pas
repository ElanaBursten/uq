unit QtrMinPageMap;
{QMANTWO-548- This unit has been removed from QManager Admin. It was replaced
  Qtr-Min Editor by XQtrix.}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseMap, Grids, StdCtrls, ExtCtrls, DB, ADODB, Buttons, GridAxis,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxMaskEdit, cxDBLookupComboBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TQtrMinPageMapForm = class(TBaseMapForm)
    TheGrid: TStringGrid;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EditLong: TEdit;
    EditLat: TEdit;
    ShowPageButton: TButton;
    GridData: TADODataSet;
    area: TADODataSet;
    areamap_name: TStringField;
    areastate: TStringField;
    areacounty: TStringField;
    areaarea_id: TAutoIncField;
    areaarea_name: TStringField;
    arealocator_id: TIntegerField;
    areaEmpName: TStringField;
    areaDS: TDataSource;
    locator: TADODataSet;
    locatorDs: TDataSource;
    AssignAreaButton: TBitBtn;
    Label4: TLabel;
    EditCallCenter: TComboBox;
    AreaGrid: TcxGrid;
    AreaGridLevel: TcxGridLevel;
    AreaGridView: TcxGridDBTableView;
    AreaGridmap_name: TcxGridDBColumn;
    AreaGridstate: TcxGridDBColumn;
    AreaGridcounty: TcxGridDBColumn;
    AreaGridarea_id: TcxGridDBColumn;
    AreaGridarea_name: TcxGridDBColumn;
    AreaGridlocator_id: TcxGridDBColumn;
    AreaGridEmpName: TcxGridDBColumn;
    procedure ShowPageButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AssignAreaButtonClick(Sender: TObject);
    procedure TheGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    ColAxis, RowAxis: TAxis;
    procedure PopulateGrid;
    procedure SetColumnLabels;
    procedure SetRowLabels;
    procedure RefreshGridData;
    procedure SetParams;
    procedure SetCellToArea(Col, Row: Integer; Area_id: Integer);
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure GotoCoords(Lat, Long : string);
  end;

implementation

{$R *.dfm}

uses
  OdHourGlass, AdminDMu;

procedure TQtrMinPageMapForm.ShowPageButtonClick(Sender: TObject);
begin
  inherited;
  PopulateGrid;
end;

procedure TQtrMinPageMapForm.PopulateGrid;
begin
  ClearGrid;
  SetRowLabels;
  SetColumnLabels;
  RefreshGridData;
  Grid.Enabled := True;
end;

procedure TQtrMinPageMapForm.FormCreate(Sender: TObject);
begin
  inherited;
  Grid := TheGrid;
end;

procedure TQtrMinPageMapForm.SetColumnLabels;
var
  x: Integer;
begin
  FreeAndNil(ColAxis);
  ColAxis := TQtrAxis.CreateSize(EditLong.Text, 14);

  Grid.ColCount := ColAxis.Size + 1;
  for x := 1 to Grid.ColCount-1 do begin
    Grid.Cells[x, 0] := ColAxis.IndexToCode(x);
  end;
end;


procedure TQtrMinPageMapForm.SetRowLabels;
var
  y: Integer;
begin
  FreeAndNil(RowAxis);
  RowAxis := TQtrAxis.CreateSize(EditLat.Text,8);

  Grid.RowCount := RowAxis.Size + 1;
  for y := 1 to Grid.RowCount-1 do begin
    Grid.Cells[0, y] := RowAxis.IndexToCode(y);
  end;

  Grid.ColWidths[0] := 50;
end;

procedure TQtrMinPageMapForm.RefreshGridData;
var
  R,C: Integer;
begin
  GridData.Close;
  SetParams;
  GridData.Open;
  while not GridData.Eof do begin
    R := RowAxis.CodeToIndex(GridData.FieldByName('qtrmin_lat').AsString);
    C := ColAxis.CodeToIndex(GridData.FieldByName('qtrmin_long').AsString);
    if (R>0) and (R<Grid.RowCount) and (C>0) and (C<Grid.ColCount) then
      Grid.Cells[C, R] := CellContentsFor(GridData.FieldByName('area_id').AsInteger);

    GridData.Next;
  end;
end;

procedure TQtrMinPageMapForm.SetParams;
var
  CAxis, RAxis: TQtrAxis;
  LongCodeMin, LongCodeMax: string;
begin
  RAxis := TQtrAxis.CreateSize(EditLat.Text,8);
  CAxis := TQtrAxis.CreateSize(EditLong.Text, 14);

  RAxis.BufferEdges;
  CAxis.BufferEdges;

  LongCodeMin := CAxis.IndexToCode(CAxis.Size);
  LongCodeMax := CAxis.IndexToCode(1);

  GridData.Parameters.ParamValues['lat1'] := RAxis.IndexToCode(RAxis.Size);
  GridData.Parameters.ParamValues['lat2'] := RAxis.IndexToCode(1);
  GridData.Parameters.ParamValues['long1'] := LongCodeMin;
  GridData.Parameters.ParamValues['long2'] := LongCodeMax;
  GridData.Parameters.ParamValues['callcenter'] := EditCallCenter.Text;

  FreeAndNil(CAxis);
  FreeAndNil(RAxis);
end;

procedure TQtrMinPageMapForm.AssignAreaButtonClick(Sender: TObject);
var
  row, col: Integer;
begin
  for Col := Grid.Selection.Left to Grid.Selection.Right do
    for Row := Grid.Selection.Top to Grid.Selection.Bottom do
      SetCellToArea(Col, Row, Area.FieldByName('Area_id').AsInteger);

  GridData.UpdateBatch;
end;


procedure TQtrMinPageMapForm.SetCellToArea(Col, Row, Area_id: Integer);
var
  x_part, y_part: string;
  CallCenter : string;
begin
  x_part := ColAxis.IndexToCode(Col);
  y_part := RowAxis.IndexToCode(Row);
  CallCenter := EditCallCenter.Text;

  if GridData.Locate('qtrmin_long;qtrmin_lat;call_center', VarArrayOf([x_part, y_part, CallCenter]), []) then begin
    GridData.Edit;
  end else begin
    GridData.Append;
    GridData.FieldByName('qtrmin_long').AsString := x_part;
    GridData.FieldByName('qtrmin_lat').AsString := y_part;
    GridData.FieldByName('call_center').AsString := CallCenter;
  end;

  GridData.FieldByName('area_id').AsInteger := Area_id;
  GridData.Post;    // doesn't go to the server, we are using batch updates

  Grid.Cells[Col, Row] := CellContentsFor(area_id);
end;


procedure TQtrMinPageMapForm.TheGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  S, Area, Locator: string;
  p: Integer;
begin
  if (ARow>0) and (ACol>0) then begin
    with (Sender as TStringGrid).Canvas do begin
      S := Grid.Cells[ACol,ARow];
      p := Pos('/' , S);
      if p>=0 then begin
        Area := Copy(S,1,p-1);
        Locator := Copy(S, p+1, 50);

        Brush.Style := bsSolid;
        if gdSelected in State then begin
          Brush.Color := clYellow;  {blue}
        end else begin
          Brush.Color := clWhite;
        end;
        FillRect(Rect);

        Pen.Style := psSolid;
        Pen.Color := clBlack;

        Brush.Style := bsClear;
        Font.Color := clBlack;
        Font.Style := [fsBold];
        TextRect(Rect, Rect.Left + 2, Rect.Top+2, Area);

        Font.Color := clBlue;
        Font.Style := [ ];
        TextRect(Rect, Rect.Left + 6, Rect.Top+14, Locator);
      end;
    end;
  end;
end;


procedure TQtrMinPageMapForm.CloseDataSets;
begin
  inherited;
  GetAreaRepr.Close;
  Area.Close;
  Locator.Close;
end;

procedure TQtrMinPageMapForm.OpenDataSets;
begin
  inherited;
  Area.Open;
  Locator.Open;
end;

procedure TQtrMinPageMapForm.FormShow(Sender: TObject);
begin
  inherited;
  OpenDataSets;
  ClearGrid;
  AdminDM.ConfigDM.GetCallCenterList(EditCallCenter.Items, True);
  EditCallCenter.Text := '';
end;

procedure TQtrMinPageMapForm.GotoCoords(Lat, Long: string);
begin
  EditLong.Text := Long;
  EditLat.Text := Lat;
  ShowPageButtonClick(Self);
end;

end.

