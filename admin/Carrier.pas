unit Carrier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCurrencyEdit,
  cxCalendar, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, BaseCxList,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TCarrierForm = class(TBaseCxListForm)
    ColName: TcxGridDBColumn;
    ColDeductible: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColCarrierId: TcxGridDBColumn;
    chkboxCarrierSearch: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure LeavingNow; override;
    procedure chkboxCarrierSearchClick(Sender: TObject);
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    CarrierInserted: Boolean;
  public
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses OdDbUtils, OdCxUtils;

{$R *.dfm}

procedure TCarrierForm.ActivatingNow;
begin
  inherited;
  CarrierInserted := False;
end;

procedure TCarrierForm.chkboxCarrierSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxCarrierSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TCarrierForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TCarrierForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CarrierInserted := False;
end;

procedure TCarrierForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'carrier', 'carrier_id', CarrierInserted);
end;

procedure TCarrierForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TCarrierForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  CarrierInserted := True;
end;

procedure TCarrierForm.FormShow(Sender: TObject);
begin
  inherited;
  FocusFirstGridRow(Grid, True);
end;

procedure TCarrierForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TCarrierForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxCarrierSearch.Enabled := True;
end;

end.
