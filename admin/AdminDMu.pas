unit AdminDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, IniFiles, Forms, Dialogs,
  OdMiscUtils, OdIsoDates, ConfigDMu, ExtCtrls, uADSI;

const
  AppName = 'Q Manager Admin';
  {$I ..\version.inc} // AppVersion = 'x.x.x.x';
//comment this
  ErrorConnFailure00        = 0;
  ErrorConnFailure17        = 17;
  ConnectedToDBMessage      = 'Connected to the database';
  DisconnectedFromDBMessage = 'Disconnected from the database';

type
  TConnectionStatus = (csConnected, csDisconnected);
  TDefaultLayout = (defStore, defRestore);
  TUpdateDBConnectionStatusInfo = procedure (ConnectionStatus: TConnectionStatus) of object;
  TFieldValuesArray = Array Of String;

  TAdminDM = class(TDataModule)
    Conn: TADOConnection;
    DeleteAreaAssignment: TADOCommand;
    DeleteUser: TADOCommand;
    Locator: TADODataSet;
    ConnectionCheck: TTimer;
    InsertAdminChanged: TADOCommand;
    AdminRestriction: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ConnAfterConnect(Sender: TObject);
    procedure ConnectionCheckTimer(Sender: TObject);
    procedure ConnBeforeConnect(Sender: TObject);
    procedure LocatorAfterCancel(DataSet: TDataSet);
    procedure LocatorBeforeInsert(DataSet: TDataSet);
    procedure LocatorBeforeEdit(DataSet: TDataSet);
  private
    LocArray: TFieldValuesArray;
    FOnUpdateDBConnectionStatusInfo: TUpdateDBConnectionStatusInfo;
    FConnectionStatus: TConnectionStatus;
    FAllowedFormList: TStringList;
    FOriginalConnString: string;
    function GetConnectionStatus: TConnectionStatus;
  public
    Server, DB, Username, Password, ConnString: string;
    ConfigDM: TConfigDM;
    LocInserted: Boolean;
    Trusted: Boolean;
    UserHasRestrictedAccess: Boolean;
    CurrentUser: String;
    ADSI: TADSI;
    function IniFileName: string;
    function IniFileLayoutName: string;
    function ConnectWithConnString: Boolean;
    procedure Connect;
    procedure ConnectFromIni;
    procedure DeleteLocatorRelatedRecords(EmpID: Integer);
    function ServerString: string;
    function CreateIniFile: TIniFile;
    function GetIniSetting(const Section, SettingName: string; Default: string = ''): string;
    procedure GetCallCenterList(List: TStrings; IncludeBlank: Boolean = False; IncludeStar: Boolean = False);
    procedure GetProfitCenterList(List: TStrings; IncludeBlank: Boolean = False);
    procedure GetClientList(List: TStrings);
    procedure GetStateList(List: TStrings);
    procedure GetStatusList(List: TStrings; IncludeStar: Boolean = False);
    procedure GetAuditMethodList(List : TStrings);
    procedure GetMarkTypeList(List: TStrings);
    procedure GetUnitConversionList(List: TStrings);
    function GetAdminRestriction(FormList: TStringlist; const UserName: string): Boolean;
    procedure GetAllowedFormList(List: TStrings);
    procedure GetDocTypeList(List: TStrings);
    procedure GetResponderTypeList(List: TStrings);
    procedure GetResponderKindList(List: TStrings);
    procedure GetCurrentUserName;
    procedure AdminChangeTable(TableName, ChangeMade: String; DateTimeChanged: TDateTime);   //QM-823
    function GetModifiedFields(DataSet: TDataSet; IDField: String; var aVarArray: TFieldValuesArray): String;  //QM-823
    function GetInsertedFields(DataSet: TDataSet; IDField: String): String;
    procedure TrackAdminChanges(DataSet: TDataset; AArray: TFieldValuesArray;
          TableName: String; IDField: String; var Inserted: Boolean);
    procedure StoreFieldValues(DataSet: TDataSet; var aVarArray: TFieldValuesArray);   //QM-823
    property ConnectionStatus: TConnectionStatus read FConnectionStatus;
    property OnUpdateDBConnectionStatusInfo: TUpdateDBConnectionStatusInfo Read FOnUpdateDBConnectionStatusInfo Write FOnUpdateDBConnectionStatusInfo;
  end;

var
  AdminDM: TAdminDM;
  ATableArray : TFieldValuesArray;

implementation

uses
  QMConst, DateUtils, Variants, OdDbUtils, OdAdoUtils;

{$R *.dfm}

{ TDM }

function TAdminDM.ConnectWithConnString: Boolean;
begin
  //If a connection string is present in the ini file, use that instead of showing the login form.
  Result := False;
  if NotEmpty(GetIniSetting('Database', 'ConnectionString', '')) then begin
    try
      ConnectAdoConnectionWithIni(Conn, IniFileName);
      UserHasRestrictedAccess := GetAdminRestriction(FAllowedFormList, UserName);
    except
      on E: Exception do
        MessageDlg(E.Message + CRLF + CRLF + 'Switching to the Login dialog.', mtError, [mbOK], 0);
    end;
    Result := Conn.Connected;
  end;
end;

procedure TAdminDM.Connect;
begin
  Conn.KeepConnection := True;
  ConnectAdoConnection(Conn, Server, DB, Trusted, Username, Password);
  UserHasRestrictedAccess := GetAdminRestriction(FAllowedFormList, UserName);
end;

procedure TAdminDM.ConnectFromIni;
begin
  ConnectAdoConnectionWithIni(Conn, IniFileName);
end;

function TAdminDM.CreateIniFile: TIniFile;
begin
  Result := TIniFile.Create(IniFileName);
end;

procedure TAdminDM.DeleteLocatorRelatedRecords(EmpID: Integer);
begin
  try
    DeleteAreaAssignment.Parameters[0].Value := EmpID;
    DeleteAreaAssignment.Execute;

    DeleteUser.Parameters[0].Value := EmpID;
    DeleteUser.Execute;
  except
  end;
end;

function TAdminDM.ServerString: string;
begin
  Result := ConnStringDescription(FOriginalConnString, Conn.DefaultDatabase);
end;

function TAdminDM.IniFileLayoutName: string;
begin
  Result := ChangeFileExt(ParamStr(0), '') + 'Layout.ini';
end;

function TAdminDM.IniFileName: string;
begin
  // The INI file is always in the same diretory as the exe
  Result := ChangeFileExt(ParamStr(0), '.ini');
end;

procedure TAdminDM.LocatorAfterCancel(DataSet: TDataSet);
begin
  LocInserted := False;
end;

procedure TAdminDM.LocatorBeforeEdit(DataSet: TDataSet);
begin
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, LocArray);
end;

procedure TAdminDM.LocatorBeforeInsert(DataSet: TDataSet);
begin
  LocInserted := True;
end;

procedure TAdminDM.DataModuleCreate(Sender: TObject);
begin
  // This should produce a halting error if a programmer accidentally relies
  // on this Connection working with the design time settings.
  Conn.ConnectionString := 'not initialized yet';
  ConfigDM := TConfigDM.Create(nil);
  ConfigDM.SetConnection(Conn);
  FAllowedFormList := TStringList.Create;
  ADSI := TADSI.Create(Self);
  GetCurrentUserName;
end;

function TAdminDM.GetIniSetting(const Section, SettingName: string; Default: string): string;
var
  Ini: TIniFile;
begin
  Ini := CreateIniFile;
  try
    Result := Ini.ReadString(Section, SettingName, Default);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TAdminDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ConfigDM);
  FreeAndNil(FAllowedFormList);
  FreeAndNil(ADSI);
end;

procedure TAdminDM.GetCallCenterList(List: TStrings; IncludeBlank: Boolean; IncludeStar: Boolean);
begin
  ConfigDM.GetCallCenterList(List, IncludeBlank, IncludeStar);
end;

procedure TAdminDM.GetProfitCenterList(List: TStrings; IncludeBlank: Boolean);
begin
  ConfigDM.GetProfitCenterList(List, IncludeBlank);
end;

procedure TAdminDM.GetResponderKindList(List: TStrings);
begin
  ConfigDM.GetReferenceValues('respkind', True, List);
end;

procedure TAdminDM.GetResponderTypeList(List: TStrings);
begin
  ConfigDM.GetReferenceValues('resptype', True, List);
end;

procedure TAdminDM.GetClientList(List: TStrings);
begin
  ConfigDM.GetClientList(List);
end;

procedure TAdminDM.GetStateList(List: TStrings);
begin
  ConfigDM.GetStateList(List);
end;

procedure TAdminDM.GetStatusList(List: TStrings; IncludeStar: Boolean);
begin
  ConfigDM.GetStatusList(List, IncludeStar);
end;

procedure TAdminDM.GetAuditMethodList(List : TStrings);
begin
  ConfigDM.GetAuditMethodList(List);
end;

procedure TAdminDM.GetMarkTypeList(List: TStrings);
begin
  ConfigDM.GetMarkTypeList(List);
end;

// Close the connection if it is in fact disconnected.
// Execute event UpdateDBConnectionStatusInfo to refresh status bar in MainFU
// This might be better in a thread, so the UI does not block
procedure TAdminDM.ConnectionCheckTimer(Sender: TObject);
begin
  ConnectionCheck.Enabled := False;
  try
    FConnectionStatus := GetConnectionStatus;
    Conn.Connected := (FConnectionStatus = csConnected);
    if Assigned(FOnUpdateDBConnectionStatusInfo) then
      FOnUpdateDBConnectionStatusInfo(FConnectionStatus);
  finally
    ConnectionCheck.Enabled := True;
  end;
end;

procedure TAdminDM.ConnAfterConnect(Sender: TObject);
begin
  ConnectionCheck.Enabled := True;
end;

procedure TAdminDM.ConnBeforeConnect(Sender: TObject);
begin
  FOriginalConnString := Conn.ConnectionString;
end;

// Check the connection and return its status.  This is basically a ping to the server DB.
function TAdminDM.GetConnectionStatus: TConnectionStatus;
var
  i: Integer;
begin
  Result := csConnected;
  try
    Conn.Execute('select 1', i, [eoExecuteNoRecords]);
   except
     if (Conn.Errors[0].NativeError in [ErrorConnFailure00, ErrorConnFailure17]) then
       Result := csDisconnected;
   end;
end;

Procedure TAdminDM.GetCurrentUserName;
var
  uName: String;
begin
//  TThread.CreateAnonymousThread(
//  procedure()
//  begin
    uName := ADSI.CurrentUserName;
//    TThread.Synchronize (TThread.CurrentThread,
//    procedure()
//    begin
//      CurrentUser := uName;
//    end)
//  end).Start;
  CurrentUser:= uName;  //qm-879 sr
  UserName:= uName;   //qm-879 sr
//  showmessage('CurrentUser '+uName);
end;

procedure TAdminDM.GetUnitConversionList(List: TStrings);
begin
  ConfigDM.GetUnitConversionList(List);
end;

function TAdminDM.GetAdminRestriction(FormList: TStringlist; const UserName: string): Boolean;
var
  S: string;
begin
  Assert(Assigned(FormList), 'FormList is not assigned');

  // Restrictions are applied if a row with non-blank allowed_forms exists for the user
  AdminRestriction.Parameters.ParamValues['login_id'] :=  UserName;
  AdminRestriction.Open;
  try
    S := AdminRestriction.FieldByName('allowed_forms').AsString;
    // This is for backward compatibility with the restrictions, which used to use
    // some form names with a "Billing - " or "Damage " prefix.
    FormList.CommaText := StringReplace(S, 'Damage ', '', [rFReplaceAll]);
    Result := FormList.Count > 0;
  finally
    AdminRestriction.Close;
  end;
end;

procedure TAdminDM.GetAllowedFormList(List: TStrings);
begin
  Assert(Assigned(List), 'No List assigned in GetAllowedFormList');
  Assert(Assigned(FAllowedFormList), 'No AllowFormList assigned in GetAllowedFormList');
  List.Clear;
  List.AddStrings(FAllowedFormList);
end;

procedure TAdminDM.GetDocTypeList(List: TStrings);
begin
  ConfigDM.GetReferenceValues('doctype', True, List);
end;

procedure TAdminDM.StoreFieldValues(DataSet: TDataSet; var aVarArray: TFieldValuesArray);
var
  I: Integer;
begin
   SetLength(aVarArray, DataSet.Fieldcount);
   for I := 0 to (DataSet.Fieldcount-1) do
      aVarArray[I] := vartoStr(DataSet.Fields[I].Value);
end;

function TAdminDM.GetInsertedFields(DataSet:TDataSet; IDField: String):String;
var
 I: Integer;
 FieldVal, FieldName: String;
 IDValue: String;
begin
   Result := '' ;
   for I := 0 to (DataSet.Fieldcount-1) do
   begin
     FieldName := DataSet.Fields[I].FieldName;
     If FieldName.Contains('modified_date') then
       Continue;
     FieldVal := VarToStr(DataSet.Fields[I].Value);
     if FieldName = IDField then
     begin
       IDValue := FieldVal;
       Continue;
     end;
     if FieldVal = '' then
       Continue;
      Result := Result + FieldName + ' ' + FieldVal + ', ';
   end;
   If Result <> '' then
    begin
      Delete(Result, Result.Length-1, 2);
      Result := 'Fields Added: ' + '[' + IDField + ' ' + IDValue + '] '  + Result;
    end;
end;

function TAdminDM.GetModifiedFields(DataSet:TDataSet; IDField: String; var aVarArray : TFieldValuesArray):String;
var
 I: Integer;
  FieldVal, FieldName: String;
  IDValue: String;
begin
   Result := '' ;
   for I := 0 to (DataSet.Fieldcount-1) do
   begin
     If DataSet.Fields[I].FieldName.Contains('modified_date') then
       Continue;

     FieldVal := VarToStr(DataSet.Fields[I].Value);
     FieldName := DataSet.Fields[I].FieldName;
     if FieldName = IDField then
     begin
       IDValue := FieldVal;
       Continue;
     end;
     If (aVarArray[I] <> FieldVal) then
       Result := Result + FieldName + ' from ' + avarArray[I] + ' to ' + FieldVal + ', ';
   end;
    If Result <> '' then
    begin
      Delete(Result, Result.Length-1, 2);
      Result := '[' + IDField + ' ' + IDValue + ']' + '  Fields changed: ' + Result;
    end;
end;

procedure TAdminDM.AdminChangeTable(TableName, ChangeMade:String; DateTimeChanged: TDateTime);
begin
  try
    With InsertAdminChanged do
    begin
      Parameters.ParamByName('changedby').Value := CurrentUser;
      Parameters.ParamByName('tablename').Value := TableName;
      Parameters.ParamByName('changemade').Value := ChangeMade;
      Parameters.ParamByName('datetimechanged').Value := DateTimeChanged;
      Execute;
    end;
  except
    On E:Exception do
     showmessage(E.Message);
  end;
end;

procedure TAdminDM.TrackAdminChanges(DataSet: TDataset; AArray: TFieldValuesArray;
 TableName: String; IDField: String; var Inserted: Boolean);
var
 s: String;
begin
  if not Inserted then
  begin
    s := GetModifiedFields(DataSet, IDField, AArray); //QM-823
    If (s <> '') then
      AdminChangeTable(TableName, s, now); //QM-823
    SetLength(AArray, 0);
  end else
  begin
    s := GetInsertedFields(DataSet, IDField);
    If (s <> '') then
      AdminDM.AdminChangeTable(TableName, s, now); //QM-823
    Inserted := False;
  end;
end;
end.
