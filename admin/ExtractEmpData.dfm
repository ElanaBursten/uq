object ExtractEmpForm: TExtractEmpForm
  Left = 0
  Top = 0
  Caption = 'Extract Employee Data'
  ClientHeight = 324
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ExtractEmpMemo: TMemo
    Left = 0
    Top = 54
    Width = 445
    Height = 270
    Align = alClient
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 445
    Height = 54
    Align = alTop
    TabOrder = 1
    object btnExtractEmpData: TButton
      Left = 24
      Top = 14
      Width = 137
      Height = 25
      Caption = 'Extract Emp Data'
      TabOrder = 0
      OnClick = btnExtractEmpDataClick
    end
  end
  object qryEmpData: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'emp_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select emp_number, short_name,  loc.name as company, '
      'r.code, ad_username, active from employee e '
      'right join reference r on ref_id = type_id '
      'right join locating_company loc on e.company_id = loc.company_id'
      'where e.emp_id = :emp_id')
    Left = 288
  end
end
