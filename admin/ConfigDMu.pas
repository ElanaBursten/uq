unit ConfigDMu;

interface

uses
  SysUtils, Classes, DB, ADODB, OdMiscUtils;

type
  TConfigDM = class(TDataModule)
    ValueGroup: TADODataSet;
    CenterGroup: TADODataSet;
    TermGroup: TADODataSet;
    ValueGroupDetail: TADODataSet;
    CenterGroupDetail: TADODataSet;
    TermGroupDetail: TADODataSet;
    Holiday: TADODataSet;
    ProfitCenters: TADODataSet;
    ConfigurationData: TADODataSet;
    Reference: TADODataSet;
    CallCenters: TADODataSet;
    AuditMethods: TADODataSet;
    DycomPeriods: TADODataSet;
    Clients: TADODataSet;
    Statuses: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    Conn: TADOConnection;
    CenterGroupCache: TStringList;
    TermGroupCache: TStringList;
    function GetGroupFromCache(Cache: TStringList; const GroupName: string; Required: Boolean = True): TStringArray;
    procedure GoToValueGroup(const GroupName: string);
    procedure OpenCenterGroups;
    procedure ClearGroupCache(Cache: TStringList);
    procedure FillGroupCache(Master, Detail: TDataSet; const IDField, MasterName, DetailField: string; Cache: TStringList);
  public
    function CreateDataSetForSQL(const SQL: string): TDataSet;
    procedure GetBillingCallCenterGroupList(List: TStrings);
    procedure GetCallCenterList(List: TStrings; IncludeBlank: Boolean = False; IncludeStar: Boolean = False);
    procedure GetBillingValueGroupList(List: TStrings; IncludeBlank: Boolean);
    function GetCenterGroup(const Group: string; Required: Boolean = True): TStringArray;
    function GetCenterGroups(Groups: array of string; Required: Boolean = True): TStringArray;
    function GetCenterGroupNameFromID(const GroupID: Integer): string;
    function GetValueGroupNameFromID(const ValueGroupID: Integer): string;
    function GetCenterGroupString(const Group: string; Required: Boolean = True): string;
    function GetConfigurationDataValue(const Name: string; Default: string): string;
    function GetReferenceValues(const RefType: string; Code: Boolean; List: TStrings): Boolean;
    procedure GetStateList(List: TStrings);
    procedure GetAuditMethodList(List : TStrings);
    function GetTermGroup(const Group: string; Required: Boolean): TStringArray;
    procedure GetValueMatchTypeList(List: TStrings);
    function IsHoliday(Date: TDateTime; const CallCenter: string; const ForBilling: Boolean=True): Boolean;
    function MatchesValueGroup(const Value, GroupName: string): Boolean;
    function MatchesCenterGroup(const Center, GroupName: string; Required: Boolean = True): Boolean;
    procedure RefreshGroups;
    procedure GetProfitCenterList(List: TStrings; IncludeBlank: Boolean = False);
    procedure SetConnection(Connection: TADOConnection);
    function GetCenterGroupID(const Group: string): Integer;
    procedure GetMarkTypeList(List: TStrings);
    procedure GetUnitConversionList(List: TStrings);
    procedure GetClientList(List: TStrings);
    procedure GetStatusList(List: TStrings; const IncludeStar: Boolean);
  end;

implementation

uses
  QMConst, OdDbUtils, DateUtils, Variants, OdAdoUtils;

type
  TValueGroup = class(TObject)
    FMatchTypes: TStringList;
    FMatchValues: TStringList;
  public
    constructor Create(DataSet: TDataSet);
    destructor Destroy; override;
    function Matches(const Value: string): Boolean;
  end;

  TGroupCacheItem = class(TObject)
    ID: Integer;
    Name: string;
    Members: TStringArray;
  end;

{$R *.dfm}

{ TConfigDM }

procedure TConfigDM.GetProfitCenterList(List: TStrings; IncludeBlank: Boolean);
begin
  ProfitCenters.Open;
  if IncludeBlank then
    GetDataSetFieldValueList(List, ProfitCenters, 'pc_code', ' ')
  else
    GetDataSetFieldValueList(List, ProfitCenters, 'pc_code', '');
end;

procedure TConfigDM.GetAuditMethodList(List : TStrings);
begin
  AuditMethods.Open;
  GetDataSetFieldValueList(List, AuditMethods, 'name', ' ');
end;

procedure TConfigDM.FillGroupCache(Master, Detail: TDataSet; const IDField, MasterName, DetailField: string; Cache: TStringList);
var
  GroupID: Integer;
  CacheItem: TGroupCacheItem;
  MasterIDField: TField;
  DetailIDField: TField;
  MasterNameField: TField;
  DetailNameField: TField;
begin
  Assert(Assigned(Master));
  Assert(Assigned(Detail));
  Assert(Assigned(Cache));

  OpenDataSet(Master);
  OpenDataSet(Detail);
  Master.First;
  Detail.First;
  Cache.Sorted := False;
  ClearGroupCache(Cache);
  MasterIDField := Master.FieldByName(IDField);
  DetailIDField := Detail.FieldByName(IDField);
  DetailNameField := Detail.FieldByName(DetailField);
  MasterNameField := Master.FieldByName(MasterName);
  while not Master.Eof do begin
    GroupID := MasterIDField.AsInteger;
    CacheItem := TGroupCacheItem.Create;  // ClearGroupCache frees these
    CacheItem.ID := GroupID;
    CacheItem.Name := MasterNameField.AsString;
    Cache.AddObject(CacheItem.Name, CacheItem);
    // !! The detail tables must be ordered on the GroupID field !!
    if Detail.Locate(IDField, GroupID, []) then
      while (DetailIDField.AsInteger = GroupID) and (not Detail.Eof) do
      begin
        SetLength(CacheItem.Members, Length(CacheItem.Members) + 1);
        CacheItem.Members[Length(CacheItem.Members) - 1] := DetailNameField.AsString;
        Detail.Next;
      end;
    Master.Next;
  end;
  Cache.Sorted := True;
end;

procedure TConfigDM.GetValueMatchTypeList(List: TStrings);
begin
  Assert(Assigned(List));
  List.Clear;
  List.Add(MatchTypeIs);
  List.Add(MatchTypeStarts);
  List.Add(MatchTypeEnds);
  List.Add(MatchTypeContains);
end;

procedure TConfigDM.GetStateList(List: TStrings);
begin
  GetReferenceValues('state', True, List);
end;

function TConfigDM.GetCenterGroup(const Group: string; Required: Boolean): TStringArray;
begin
  if not (CenterGroup.Active and CenterGroupDetail.Active) then
    FillGroupCache(CenterGroup, CenterGroupDetail, 'center_group_id', 'group_code', 'call_center', CenterGroupCache);
  Result := GetGroupFromCache(CenterGroupCache, Group, Required);
end;

function TConfigDM.GetCenterGroups(Groups: array of string; Required: Boolean = True): TStringArray;
var
  Group: TStringArray;
  i, j: Integer;
begin
  SetLength(Result, 0);
  for i := 0 to Length(Groups) - 1 do begin
    Group := GetCenterGroup(Groups[i], Required);
    for j := 0 to Length(Group) do begin
      SetLength(Result, Length(Result) + 1);
      Result[Length(Result) - 1] := Group[i];
    end;
  end;
end;

function TConfigDM.GetTermGroup(const Group: string; Required: Boolean): TStringArray;
begin
  if not (TermGroup.Active and TermGroupDetail.Active) then
    FillGroupCache(TermGroup, TermGroupDetail, 'term_group_id', 'group_code', 'term', TermGroupCache);
  Result := GetGroupFromCache(TermGroupCache, Group, Required);
end;

function TConfigDM.IsHoliday(Date: TDateTime; const CallCenter: string; const ForBilling: Boolean): Boolean;
begin
  Date := StartOfTheDay(Date);
  if not Holiday.Active then
    Holiday.Open;
  Result := Holiday.Locate('cc_code;holiday_date;billing_holiday', VarArrayOf(['*', Date, ForBilling]), [loCaseInsensitive]) or
    Holiday.Locate('cc_code;holiday_date;billing_holiday', VarArrayOf([CallCenter, Date, ForBilling]), [loCaseInsensitive]);
end;

function TConfigDM.MatchesValueGroup(const Value, GroupName: string): Boolean;
var
  Group: TValueGroup;
begin
  GoToValueGroup(GroupName);
  Group := TValueGroup.Create(ValueGroupDetail);
  try
    Result := Group.Matches(Value);
  finally
    FreeAndNil(Group);
  end;
end;

procedure TConfigDM.GoToValueGroup(const GroupName: string);
begin
  OpenDataSet(ValueGroup);
  OpenDataSet(ValueGroupDetail);

  ValueGroup.Filter := 'group_code=' + QuotedStr(GroupName);
  ValueGroup.Filtered := True;
  if ValueGroup.Eof then
    raise Exception.CreateFmt('Value group code %s not found', [GroupName]);
  Assert(SameText(ValueGroup.FieldByName('group_code').AsString, GroupName));

  ValueGroupDetail.Filtered := False;
  ValueGroupDetail.Filter := 'value_group_id=' + ValueGroup.FieldByName('value_group_id').AsString;
  ValueGroupDetail.Filtered := True;
  if ValueGroupDetail.Eof then
    raise Exception.CreateFmt('Empty value group %s', [GroupName]);
  Assert(ValueGroup.FieldByName('value_group_id').AsInteger = ValueGroupDetail.FieldByName('value_group_id').AsInteger);
end;

procedure TConfigDM.RefreshGroups;
begin
  TermGroup.Close;
  TermGroupDetail.Close;
  ValueGroup.Close;
  ValueGroupDetail.Close;
  CenterGroup.Close;
  CenterGroupDetail.Close;
  ClearGroupCache(TermGroupCache);
  ClearGroupCache(CenterGroupCache);
end;

procedure TConfigDM.GetBillingCallCenterGroupList(List: TStrings);
var
  Name: string;
  ID: Integer;
begin
  Assert(Assigned(List));
  List.Clear;
  OpenCenterGroups;
  CenterGroup.First;
  while not CenterGroup.Eof do begin
    if CenterGroup.FieldByName('show_for_billing').AsBoolean then begin
      Name := CenterGroup.FieldByName('group_code').AsString;
      ID := CenterGroup.FieldByName('center_group_id').AsInteger;
      List.AddObject(Name, TObject(ID))
    end;
    CenterGroup.Next;
  end;
end;

procedure TConfigDM.GetBillingValueGroupList(List: TStrings; IncludeBlank: Boolean);
var
  Name: string;
  ID: Integer;
begin
  Assert(Assigned(List));
  ValueGroup.Open;
  try
    List.Clear;
    ValueGroup.First;
    while not ValueGroup.Eof do begin
      Name := ValueGroup.FieldByName('group_code').AsString;
      ID := ValueGroup.FieldByName('value_group_id').AsInteger;
      List.AddObject(Name, TObject(ID));
      ValueGroup.Next;
    end;
  finally
    ValueGroup.Close;
  end;
  if IncludeBlank then
    List.Insert(0, '');
end;

function TConfigDM.GetCenterGroupString(const Group: string; Required: Boolean): string;
var
  Centers: TStringArray;
begin
  Centers := GetCenterGroup(Group, Required);
  Result := StringArrayToDelimitedString(Centers, '', ',');
end;

function TConfigDM.GetCenterGroupID(const Group: string): Integer;
begin
  OpenCenterGroups;
  if CenterGroup.Locate('group_code', Group, []) then
    Result := CenterGroup.FieldByName('center_group_id').AsInteger
  else
    raise Exception.CreateFmt('Center group %s not found to obtain the group ID', [Group]);
end;

function TConfigDM.GetCenterGroupNameFromID(const GroupID: Integer): string;
begin
  OpenCenterGroups;
  if CenterGroup.Locate('center_group_id', GroupID, []) then
    Result := CenterGroup.FieldByName('group_code').AsString
  else
    raise Exception.CreateFmt('Center group id %d not found', [GroupID]);
end;

function TConfigDM.GetValueGroupNameFromID(const ValueGroupID: Integer): string;
begin
  OpenDataSet(ValueGroup);
  ValueGroup.Filtered := False;
  if ValueGroup.Locate('value_group_id', ValueGroupID, []) then
    Result := ValueGroup.FieldByName('group_code').AsString
  else
    raise Exception.CreateFmt('Value group id %d not found', [ValueGroupID]);
end;

procedure TConfigDM.OpenCenterGroups;
begin
  GetCenterGroupString('JustOpenTheDatasets', False);
end;

function TConfigDM.GetConfigurationDataValue(const Name: string; Default: string): string;
begin
  Result := Default;
  ConfigurationData.Close;
  ConfigurationData.Parameters.ParamValues['name'] := Name;
  ConfigurationData.Open;
  if ConfigurationData.IsEmpty or (Trim(ConfigurationData.FieldByName('value').AsString) = '') then
    Result := Default
  else
    Result := ConfigurationData.FieldByName('value').AsString;
end;

function TConfigDM.GetReferenceValues(const RefType: string; Code: Boolean; List: TStrings): Boolean;
begin
  List.Clear;

  Reference.Parameters.ParamByName('type').Value := RefType;
  Reference.Open;
  try
    while not Reference.Eof do begin
      if Code then
        List.Add(Reference.FieldByName('code').AsString)
      else
        List.Add(Reference.FieldByName('description').AsString);

      Reference.Next
    end;
  finally
    Reference.Close;
  end;

  Result := List.Count > 0
end;

procedure TConfigDM.GetCallCenterList(List: TStrings; IncludeBlank: Boolean; IncludeStar: Boolean);
begin
  Assert(Assigned(List));
  CallCenters.Open;
  try
    List.Clear;
    while not CallCenters.Eof do begin
      List.Add(CallCenters.FieldByName('cc_code').DisplayText);
      CallCenters.Next;
    end;
  finally
    CallCenters.Close;
  end;
  if IncludeStar then
    List.Insert(0, '*');
  if IncludeBlank then
    List.Insert(0, '');
end;

procedure TConfigDM.GetMarkTypeList(List: TStrings);
begin
  GetReferenceValues('marktype', True, List);
end;

procedure TConfigDM.GetUnitConversionList(List: TStrings);
const
  Select = 'select bu.unit_conversion_id, ' +
    'CASE WHEN bu.first_unit_factor < 1 THEN ' +
    '''Bill 1 regardless of length'' ' +
    'WHEN bu.rest_units_factor IS Null THEN ' +
    '''Bill 1 every '' + cast(bu.first_unit_factor as varchar(10)) + '' '' + bu.unit_type ' +
    'ELSE  ''Bill 1 first '' + cast(bu.first_unit_factor as varchar(10)) + '' '' + bu.unit_type + ' +
    '''; 1 each additional '' + cast(bu.rest_units_factor as varchar(10)) + '' '' + bu.unit_type ' +
    'END as Description ' +
    'from billing_unit_conversion bu where bu.active=1 order by Description';
var
  UnitConversion: TDataSet;
begin
  Assert(Assigned(List));
  UnitConversion := CreateDataSetForSQL(Select);
  try
    List.Clear;
    while not UnitConversion.Eof do begin
      List.AddObject(UnitConversion.FieldByName('Description').AsString, TObject(UnitConversion.FieldByName('unit_conversion_id').AsInteger));
      UnitConversion.Next;
    end;
  finally
    FreeAndNil(UnitConversion);
  end;
end;

procedure TConfigDM.SetConnection(Connection: TADOConnection);
begin
  Conn := Connection;
  SetConnections(Conn, Self);
end;

function TConfigDM.MatchesCenterGroup(const Center, GroupName: string; Required: Boolean): Boolean;
begin
  Result := StringInArray(Center, GetCenterGroup(GroupName, Required));
end;

function TConfigDM.CreateDataSetForSQL(const SQL: string): TDataSet;
var
  Query: TADOQuery;
begin
  Query := TADOQuery.Create(nil);
  try
    Query.Connection := Conn;
    Query.SQL.Text := SQL;
    Query.Open;
  except
    FreeAndNil(Query);
    raise;
  end;
  Result := Query;
end;

procedure TConfigDM.ClearGroupCache(Cache: TStringList);
begin
  Assert(Assigned(Cache));
  FreeStringListObjects(Cache);
  Cache.Clear;
end;

function TConfigDM.GetGroupFromCache(Cache: TStringList; const GroupName: string; Required: Boolean): TStringArray;
var
  i: Integer;
begin
  Assert(Assigned(Cache));
  i := Cache.IndexOf(GroupName);
  if i < 0 then begin
    if Required then
      raise Exception.CreateFmt('Group %s not found', [GroupName]);
    SetLength(Result, 0);
  end
  else
    Result := (Cache.Objects[i] as TGroupCacheItem).Members;
end;

procedure TConfigDM.DataModuleCreate(Sender: TObject);
begin
  CenterGroupCache := TStringList.Create;
  TermGroupCache := TStringList.Create;
end;

procedure TConfigDM.DataModuleDestroy(Sender: TObject);
begin
  ClearGroupCache(CenterGroupCache);
  ClearGroupCache(TermGroupCache);
  FreeAndNil(CenterGroupCache);
  FreeAndNil(TermGroupCache);
end;

procedure TConfigDM.GetClientList(List: TStrings);
var
  ClientCode: string;
begin
  Assert(Assigned(List));
  Clients.Close;
  Clients.Open;
  List.BeginUpdate;
  try
    List.Clear;
    while not Clients.Eof do begin
      ClientCode := Clients.FieldByName('oc_code').DisplayText;
      List.Add(ClientCode);
      Clients.Next;
    end;
  finally
    List.EndUpdate;
  end;
  Clients.Close;
end;

procedure TConfigDM.GetStatusList(List: TStrings; const IncludeStar: Boolean);
begin
  Assert(Assigned(List));
  Statuses.Close;
  Statuses.Open;
  List.Clear;
  if IncludeStar then
    List.Add('*');
  while not Statuses.Eof do begin
    List.Add(Statuses.FieldByName('status').DisplayText);
    Statuses.Next;
  end;
  Statuses.Close;
end;

{ TValueGroup }

constructor TValueGroup.Create(DataSet: TDataSet);
begin
  FMatchTypes := TStringList.Create;
  FMatchValues := TStringList.Create;
  DataSet.First;
  while not DataSet.Eof do begin
    FMatchValues.Add(DataSet.FieldByName('match_value').AsString);
    FMatchTypes.Add(DataSet.FieldByName('match_type').AsString);
    DataSet.Next;
  end;
end;

destructor TValueGroup.Destroy;
begin
  FreeAndNil(FMatchTypes);
  FreeAndNil(FMatchValues);
  inherited;
end;

function TValueGroup.Matches(const Value: string): Boolean;
var
  i: Integer;
  MatchValue: string;
  MatchType: string;
begin
  Assert(FMatchTypes.Count = FMatchValues.Count);
  Result := False;
  for i := 0 to FMatchValues.Count - 1 do begin
    MatchValue := FMatchValues[i];
    MatchType := FMatchTypes[i];

    if MatchType = MatchTypeIs then
      Result := SameText(Value, MatchValue)
    else if MatchType = MatchTypeStarts then
      Result := StrBeginsWith(MatchValue, Value)
    else if MatchType = MatchTypeEnds then
      Result := StrEndsWith(MatchValue, Value)
    else if MatchType = MatchTypeContains then
      Result := StrContainsText(MatchValue, Value);
    if Result then
      Break;
  end;
end;

end.

