object AdminDM: TAdminDM
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 364
  Width = 265
  object Conn: TADOConnection
    ConnectionTimeout = 10
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = ConnAfterConnect
    BeforeConnect = ConnBeforeConnect
    Left = 40
    Top = 12
  end
  object DeleteAreaAssignment: TADOCommand
    CommandText = 'update area set locator_id=NULL where locator_id=:id'
    Connection = Conn
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 120
    Top = 12
  end
  object DeleteUser: TADOCommand
    CommandText = 'delete from users where emp_id=:id'
    Connection = Conn
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 120
    Top = 60
  end
  object Locator: TADODataSet
    Connection = Conn
    CursorType = ctStatic
    BeforeInsert = LocatorBeforeInsert
    BeforeEdit = LocatorBeforeEdit
    AfterCancel = LocatorAfterCancel
    CommandText = 'select * from employee'#39'#13#10'#39'order by'#39'#13#10'#39'short_name'
    Parameters = <>
    Left = 40
    Top = 144
  end
  object ConnectionCheck: TTimer
    Enabled = False
    Interval = 20000
    OnTimer = ConnectionCheckTimer
    Left = 192
    Top = 224
  end
  object InsertAdminChanged: TADOCommand
    CommandText = 
      'Insert into adminchangetracker  (changedby, tablename, changemad' +
      'e, datetimechanged) '#13#10'values (:changedby, :tablename, :changemad' +
      'e, :datetimechanged)'
    Connection = Conn
    Parameters = <
      item
        Name = 'changedby'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'tablename'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'changemade'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'datetimechanged'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    Left = 128
    Top = 136
  end
  object AdminRestriction: TADODataSet
    Connection = Conn
    CommandText = 'select * from admin_restriction where login_id = :login_id'
    Parameters = <
      item
        Name = 'login_id'
        Size = -1
        Value = Null
      end>
    Left = 56
    Top = 312
  end
end
