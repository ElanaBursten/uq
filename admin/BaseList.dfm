inherited BaseListForm: TBaseListForm
  Caption = 'BaseListForm'
  Font.Charset = ANSI_CHARSET
  OnShow = FormShow
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 548
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Grid: TDBGrid
    Left = 0
    Top = 41
    Width = 548
    Height = 359
    Align = alClient
    DataSource = DS
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Data: TADODataSet
    BeforeDelete = DataBeforeDelete
    Parameters = <>
    Left = 56
    Top = 112
  end
  object DS: TDataSource
    DataSet = Data
    Left = 96
    Top = 112
  end
end
