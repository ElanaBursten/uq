inherited FaxQueueRulesForm: TFaxQueueRulesForm
  Caption = 'Locate Notification Faxes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 23
    inherited chkboxEmailSearch: TCheckBox
      Left = 18
      Top = 5
    end
  end
  inherited Grid: TcxGrid
    Top = 23
    Height = 377
    inherited GridView: TcxGridDBTableView
      DataController.KeyFieldNames = 'fqr_id'
      object ColSendType: TcxGridDBColumn
        Caption = 'Fax System'
        DataBinding.FieldName = 'send_type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          'EFAX'
          'FAXMAKER')
        Width = 88
      end
    end
  end
  inherited Data: TADODataSet
    OnNewRecord = DataNewRecord
    CommandText = 'select * from fax_queue_rules'#13#10'order by call_center'
  end
end
