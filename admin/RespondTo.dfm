inherited RespondToForm: TRespondToForm
  Caption = 'Respond To'
  ClientHeight = 482
  ClientWidth = 820
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 820
    Height = 27
    object ActiveRespondToFilter: TCheckBox
      Left = 160
      Top = 7
      Width = 161
      Height = 13
      Caption = 'Active RespondTo Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveRespondToFilterClick
    end
    object chkboxRespondToSearch: TCheckBox
      Left = 15
      Top = 7
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = chkboxRespondToSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 27
    Width = 820
    Height = 455
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      OptionsView.GroupByBox = True
      object ColRespondTo: TcxGridDBColumn
        Caption = 'Respond To'
        DataBinding.FieldName = 'respond_to'
        Width = 83
      end
      object ColUserName: TcxGridDBColumn
        Caption = 'User Name'
        DataBinding.FieldName = 'user_name'
        Width = 80
      end
      object ColPassword: TcxGridDBColumn
        Caption = 'Password'
        DataBinding.FieldName = 'password'
        Width = 75
      end
      object ColOneCallCenter: TcxGridDBColumn
        Caption = 'One Call Center'
        DataBinding.FieldName = 'one_call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 87
      end
      object ColURL: TcxGridDBColumn
        DataBinding.FieldName = 'url'
        Width = 62
      end
      object ColAPIKey: TcxGridDBColumn
        Caption = 'API Key'
        DataBinding.FieldName = 'api_key'
        Width = 92
      end
      object ColRespondToNote: TcxGridDBColumn
        Caption = 'Respond To Note'
        DataBinding.FieldName = 'respond_to_note'
        Width = 119
      end
      object ColAllVersions: TcxGridDBColumn
        Caption = 'All Versions'
        DataBinding.FieldName = 'all_versions'
        Width = 65
      end
      object ColIncludeNotes: TcxGridDBColumn
        Caption = 'Include Notes'
        DataBinding.FieldName = 'include_notes'
        Width = 78
      end
      object ColRealLoctaorName: TcxGridDBColumn
        Caption = 'Real Locator Name'
        DataBinding.FieldName = 'real_locator_name'
        Width = 95
      end
      object ColIncludeEPRLink: TcxGridDBColumn
        Caption = 'Include EPR Link'
        DataBinding.FieldName = 'include_epr_link'
        Width = 96
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Width = 51
      end
      object ColProcedureName: TcxGridDBColumn
        Caption = 'Procedure Name'
        DataBinding.FieldName = 'ProcedureName'
        Width = 96
      end
      object ColResponoderName: TcxGridDBColumn
        Caption = 'Responder Name'
        DataBinding.FieldName = 'ResponderName'
        Width = 96
      end
      object ColResponderComment: TcxGridDBColumn
        Caption = 'Responder Comment'
        DataBinding.FieldName = 'ResponderComment'
        Width = 120
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforePost = DataBeforePost
    OnNewRecord = DataNewRecord
    CommandText = 'Select * from respond_to where active=1'
  end
end
