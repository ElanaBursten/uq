inherited HALCallCenterForm: THALCallCenterForm
  Caption = 'Call Centers HAL'
  ClientHeight = 583
  ClientWidth = 833
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 504
    Width = 833
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 833
    object lblCallCenter: TLabel
      Left = 169
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Call Center'
    end
    object Label1: TLabel
      Left = 537
      Top = 17
      Width = 110
      Height = 13
      Caption = '#days from ticket date'
    end
    object lblThreshhold: TLabel
      Left = 396
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Threshhold'
    end
    object btnCallCenter: TButton
      Left = 32
      Top = 10
      Width = 108
      Height = 25
      Caption = 'Check Call Center'
      TabOrder = 0
      OnClick = btnCallCenterClick
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 656
      Top = 13
      Width = 39
      Height = 22
      Hint = 'number of excluded directories'
      MaxValue = 9
      MinValue = 1
      TabOrder = 1
      Value = 7
    end
    object edtThreshhold: TEdit
      Left = 455
      Top = 13
      Width = 57
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      Text = '700'
    end
    object cbCallCenters: TComboBox
      Left = 237
      Top = 12
      Width = 117
      Height = 21
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  inherited Grid: TDBGrid
    Width = 833
    Height = 463
    ReadOnly = True
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 512
    Width = 833
    Height = 71
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 90
      end
      object ColDaysback: TcxGridDBColumn
        Caption = 'Days Back'
        DataBinding.FieldName = 'daysback'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColDaysbackPropertiesEditValueChanged
        Width = 62
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
  end
  object CallCenters: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'Select cc_code from call_center where active = 1'
    Parameters = <>
    Left = 176
    Top = 112
  end
  object dsCallCenters: TDataSource
    DataSet = CallCenters
    Left = 272
    Top = 112
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from hal where hal_module = '#39'call_center'#39
    Parameters = <>
    Left = 352
    Top = 112
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 416
    Top = 104
  end
end
