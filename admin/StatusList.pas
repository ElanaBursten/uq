unit StatusList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseMasterDetail, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  System.Actions, Vcl.ActnList, Data.Win.ADODB, Vcl.StdCtrls, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxDBExtLookupComboBox, dxScrollbarAnnotations;

type
  TStatusListForm = class(TBaseMasterDetailForm)
    MarkTypes: TADODataSet;
    MasterGridViewstatus: TcxGridDBColumn;
    MasterGridViewstatus_name: TcxGridDBColumn;
    MasterGridViewbillable: TcxGridDBColumn;
    MasterGridViewcomplete: TcxGridDBColumn;
    MasterGridViewallow_locate_units: TcxGridDBColumn;
    MasterGridViewmark_type_required: TcxGridDBColumn;
    MasterGridViewdefault_mark_type: TcxGridDBColumn;
    MasterGridViewmeet_only: TcxGridDBColumn;
    MasterGridViewactive: TcxGridDBColumn;
    MasterGridViewmodified_date: TcxGridDBColumn;
    DetailGridViewClientName: TcxGridDBColumn;
    DetailGridViewActive: TcxGridDBColumn;
    DetailGridViewCallCenter: TcxGridDBColumn;
    DetailGridViewStatus: TcxGridDBColumn;
    DetailGridViewCustomerID: TcxGridDBColumn;
    DetailGridViewUtility: TcxGridDBColumn;
    UtilityLookupView: TcxGridDBTableView;
    UtilityLookup: TADODataSet;
    UtilityLookupSource: TDataSource;
    UtilityLookupViewCode: TcxGridDBColumn;
    UtilityLookupViewDescription: TcxGridDBColumn;
    Splitter2: TSplitter;
    DetailGridViewResponder: TcxGridDBColumn;
    DetailGridViewDamageCo: TcxGridDBColumn;
    DamageCoLookup: TADODataSet;
    DamageCoLookupSource: TDataSource;
    DamageCompanyLookupView: TcxGridDBTableView;
    DamageCompanyLookupViewDamageCo: TcxGridDBColumn;
    DamageCompanyLookupViewRefID: TcxGridDBColumn;
    DamageCompanyLookupViewActiveID: TcxGridDBColumn;
    ActiveStatusesFilter: TCheckBox;
    chkboxStatusSearch: TCheckBox;
    ActiveClientsFilter: TCheckBox;
    chkboxClientSearch: TCheckBox;
    procedure ActiveStatusesFilterClick(Sender: TObject);
    procedure chkboxStatusSearchClick(Sender: TObject);
    procedure MasterBeforePost(DataSet: TDataSet);
    procedure MasterNewRecord(DataSet: TDataSet);
    procedure MasterGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure MasterDeleteActionExecute(Sender: TObject);
    procedure DetailBeforeOpen(DataSet: TDataSet);
    procedure ActiveClientsFilterClick(Sender: TObject);
    procedure chkboxClientSearchClick(Sender: TObject);
    procedure DetailGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DetailGridViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MasterAfterCancel(DataSet: TDataSet);
    procedure MasterBeforeInsert(DataSet: TDataSet);
    procedure MasterBeforeEdit(DataSet: TDataSet);
    procedure MasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    SLInserted: Boolean;
  public
    { Public declarations }
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  StatusListForm: TStatusListForm;

implementation

uses AdminDMu, OdCxUtils, OdDbUtils, OdMiscUtils;

{$R *.dfm}

procedure TStatusListForm.ActiveStatusesFilterClick(Sender: TObject);
var
  FSelectedStatus: String;
begin
  inherited;
  FSelectedStatus := Master.FieldByName('status').AsString;
  Master.Close;
  if ActiveStatusesFilter.Checked then
    Master.CommandText := 'select * from statuslist where active = 1 order by status ' else
      Master.CommandText := 'select * from statuslist order by status';
  Master.Open;
  Master.Locate('status', FSelectedStatus, []);
end;

procedure TStatusListForm.chkboxClientSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxClientSearch.Checked then
    DetailGridView.Controller.ShowFindPanel
  else
    DetailGridView.Controller.HideFindPanel;
end;

procedure TStatusListForm.ActiveClientsFilterClick(Sender: TObject);
var
  FSelectedStatus: String;
  SQLText: String;
begin
  inherited;
  FSelectedStatus := Detail.FieldByName('client_id').AsString;
  SQLText := 'select c.*,  a.status from client c join ' +
    '(select distinct status, sg_id from status_group_item) a ' +
    'on c.status_group_id = a.sg_id';
  Detail.Close;
  if ActiveClientsFilter.Checked then
    Detail.CommandText := SQLText + ' where c.active = 1' else
    Detail.CommandText := SQLText;
  Detail.Open;
  Detail.Locate('client_id', FSelectedStatus, []);
end;

procedure TStatusListForm.chkboxStatusSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxStatusSearch.Checked then
    MasterGridView.Controller.ShowFindPanel
  else
    MasterGridView.Controller.HideFindPanel;
end;

procedure TStatusListForm.MasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  SLInserted := False;
end;

procedure TStatusListForm.MasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'statuslist', 'status', SLInserted)
end;

procedure TStatusListForm.MasterBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TStatusListForm.MasterBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  SLInserted := True;
end;

procedure TStatusListForm.MasterBeforePost(DataSet: TDataSet);
begin
  inherited;
    if Dataset.FieldByName('default_mark_type').AsString = '' then
    Dataset.FieldByName('default_mark_type').Clear;
  if Dataset.FieldByName('mark_type_required').AsBoolean and Dataset.FieldByName('default_mark_type').IsNull then begin
    MasterGridView.GetColumnByFieldName('default_mark_type').FocusWithSelection;
    raise Exception.Create('Please select a default mark type before saving this status');
  end;
end;

procedure TStatusListForm.MasterDeleteActionExecute(Sender: TObject);
begin
  EditDataSet(Master);
  Master.FieldByName('active').Value := False;
  PostDataSet(Master);
end;

procedure TStatusListForm.MasterGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[MasterGridViewactive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TStatusListForm.MasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  Dataset.FieldByName('active').AsBoolean := True;
end;

procedure TStatusListForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  ActiveClientsFilter.Enabled := True;
  chkboxClientSearch.Enabled := True;
  ActiveStatusesFilter.Enabled := True;
  chkboxStatusSearch.Enabled := True;
end;

procedure TStatusListForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetMarkTypeList(CxComboBoxItems(MasterGridViewdefault_mark_type));
  CxComboBoxItems(MasterGridViewdefault_mark_type).Insert(0, '');
end;

procedure TStatusListForm.LeavingNow;
begin
  inherited;
  Master.Close;
  Detail.Close;
end;

procedure TStatusListForm.OpenDataSets;
begin
  inherited;
  UtilityLookup.Open;
  DamageCoLookup.Open;
end;

procedure TStatusListForm.CloseDataSets;
begin
  inherited;
  UtilityLookup.Close;
  DamageCoLookup.Close;
end;

procedure TStatusListForm.DetailBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('utility_name', Detail, 'utility_type', UtilityLookup, 'code', 'description');
  Detail.FieldByname('utility_name').LookupCache := True;
   AddLookupField('DamageCompany', Detail, 'ref_id', DamageCoLookup, 'ref_id', 'DamageCompany');
  Detail.FieldByname('DamageCompany').LookupCache := True;
end;

procedure TStatusListForm.DetailGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[DetailGridViewactive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TStatusListForm.DetailGridViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
   if( Key = VK_INSERT) or (Key = VK_DELETE) then
    Key := 0;
end;

procedure TStatusListForm.EditNow;
begin
  inherited;
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TStatusListForm.ActivatingNow;
begin
  inherited;
  SLInserted := False;
end;

end.
