; Use Inno Setup 5.0.8+
#define Version GetEnv("VERSION")
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager Admin
AppVerName=UtiliQuest Q Manager Admin {#Version}
AppID=UtiliQuestQManagerAdmin
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\Q Manager
DefaultGroupName=UtiliQuest
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/                                                     
AppVersion={#Version}{#GitCommitIDWithDash}
AppMutex=UtiliQuestQManagerAdmin
DisableProgramGroupPage=yes
OutputDir=..\build
OutputBaseFilename=JustQManagerAdminSetup-{#Version}{#GitCommitIDWithDash}
PrivilegesRequired=none
OutputManifestFile=JustQManagerAdminSetup-Manifest.txt

[Files]
Source: QManagerAdmin.exe; DestDir: {app}; Flags: ignoreversion; Components: QMAdmin; 
Source: QManagerAdmin.ini; DestDir: {app}; DestName: QManagerAdmin.ini; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 
Source: mySavedQueries.xml; DestDir: {app}; DestName: mySavedQueries.xml; Flags: ignoreversion uninsneveruninstall; Components: QMAdmin; 
Source: FrontEndsDev.HTML; DestDir: {app}; DestName: FrontEndsDev.HTML; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 
Source: FrontEndsProd.HTML; DestDir: {app}; DestName: FrontEndsProd.HTML; Flags: onlyifdoesntexist uninsneveruninstall; Components: QMAdmin; 



[Icons]
Name: "{group}\Q Manager Admin";   Filename: "{app}\QManagerAdmin.exe"; Components: QMAdmin


[Run]
Filename: "{app}\QManagerAdmin.exe"; Description: "Start Q Manager Admin Now"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent unchecked; Components: QMAdmin

Filename: "{dotnet20}\regasm.exe"; Parameters: "{sys}\eFaxDeveloper.dll /tlb:{sys}\eFaxDeveloper.tlb /u"; Flags: nowait

[Components]
Name: QMAdmin; Description: "Q Manager Admin"; Flags: checkablealone; Types: full
