unit TableViewer2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, OdEmbeddable, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, cxTextEdit,
  cxMemo,AdminDMu, Data.DB, Data.Win.ADODB, cxMaskEdit, cxDropDownEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ComCtrls, Datasnap.Provider,
  Datasnap.DBClient, SaveQueryDialog, Vcl.Buttons, OdDBUtils, dbCommon,
  cxDataControllerConditionalFormattingRulesManagerDialog, midaslib,
  dxSkinsCore, dxDateRanges,
  dxScrollbarAnnotations, dxSkinBasic, dxCore, dxSkinsForm;

type
  TDataSetWrapper = class(TDataSet);
  TMsgType = (mtErrorMsg, mtCompleteMsg, mtCompleteCommandMsg, mtActionCancelledMsg);
  TTableViewerForm2 = class(TEmbeddableForm)
    Splitter1: TSplitter;
    ADOCommand: TADOCommand;
    QueryDS: TDataSource;
    QueryATable: TADODataSet;
    SavedQueriesCDS: TClientDataSet;
    TopPanel: TPanel;
    QryMemo: TcxMemo;
    ButtonPanel: TPanel;
    BottomPageControl: TPageControl;
    QueryResultsTab: TTabSheet;
    TableGrid: TcxGrid;
    GridView: TcxGridDBTableView;
    GridLevel: TcxGridLevel;
    StoredQueriesTab: TTabSheet;
    CDSView: TcxGridDBTableView;
    CDSGridLevel1: TcxGridLevel;
    CDSGrid: TcxGrid;
    SaveQueriesDS: TDataSource;
    TableNameComboBox: TcxComboBox;
    InsertCB: TCheckBox;
    Label1: TLabel;
    CDSViewShortDesc: TcxGridDBColumn;
    CDSViewSQLText: TcxGridDBColumn;
    CDSViewModDate: TcxGridDBColumn;
    BtnPanel: TPanel;
    SaveButton: TcxButton;
    ClearBtn: TcxButton;
    RunBtn: TcxButton;
    LeftBtn: TSpeedButton;
    RightBtn: TSpeedButton;
    MessagesTab: TTabSheet;
    MessagesMemo: TcxMemo;
    CountLabel: TLabel;
    CountStaticLabel: TLabel;
    Label2: TLabel;
    LineCountLabel: TLabel;
    ADOConnection1: TADOConnection;
    SaveTSVXML: TcxButton;
    SaveTSVXMLDlg: TSaveDialog;
    btnExcelExport: TcxButton;
    RoutinesTab: TTabSheet;
    Panel1: TPanel;
    btnSPList: TcxButton;
    btnTriggerList: TcxButton;
    btnSPdef: TcxButton;
    Panel3: TPanel;
    SPGrid: TcxGrid;
    SPGridView: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Splitter2: TSplitter;
    Panel2: TPanel;
    SPTextGrid: TcxGrid;
    SPTextGridView: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    qrySPList: TADOQuery;
    SPListDS: TDataSource;
    qrySPDef: TADOQuery;
    DSSPDef: TDataSource;
    btnSaveDef: TcxButton;
    btnClearSP: TcxButton;
    dxSkinController1: TdxSkinController;
    procedure RunBtnClick(Sender: TObject);
    procedure TableNameComboBoxPropertiesChange(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure LeftBtnClick(Sender: TObject);
    procedure RightBtnClick(Sender: TObject);
    procedure CDSViewDblClick(Sender: TObject);
    procedure QryMemoPropertiesChange(Sender: TObject);
    procedure SaveTSVXMLClick(Sender: TObject);
    procedure btnExcelExportClick(Sender: TObject);
    procedure btnTriggerListClick(Sender: TObject);
    procedure btnSPListClick(Sender: TObject);
    procedure btnSPdefClick(Sender: TObject);
    procedure btnSaveDefClick(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
    procedure btnClearSPClick(Sender: TObject);
  private
    procedure LoadTableNames;
    function LoadAndRunQuery(CommandText: string): string;
    function LoadAndRunCommand(CommandText: string): string;
    procedure LoadandRunSPTriggerQuery(SQLText: string);
    procedure LoadandRunSPDefQuery(SQLText: String);
    procedure TableNameChange;
    procedure ShowQueryMessage(MsgType: TMsgType; MsgText: string; RecAffected: integer = -1);
    procedure CancelAsyncQuery(qry:TADODataSet);
    procedure CreateInsertStatement;
    function OpenDataFile: boolean;
    function PreviouslySaved: boolean;
    procedure SetCursor(ACursor: TCursor);
    procedure SaveTSVFile(DataSet: TADODataset; var AFile: TextFile);
    function DataSetToXML(DataSet: TDataSet): String;
    procedure ExportToExcel(Query: TDataSet);
  public
    constructor Create(Owner: TComponent); override;
    procedure ActivatingNow; override;
    procedure OpenDataSets; override;
    procedure PopulateControls; override;
    procedure SetControlsReadOnly(const IsReadOnly: Boolean; Parent: TComponent); override;
    procedure RunCommand;
    procedure SaveQuery;
    procedure LoadFromSaved;
  end;

const
  DataFile = 'mySavedQueries.xml';
  ValidStr = 'SUCCESSFULLY COMPLETED';

var
  TableViewerForm2: TTableViewerForm2;

implementation
uses
  OdMiscUtils, odHOurGlass, odADOUtils, CodeLookupList, ComObj, ActiveX, Excel2000;
{$R *.dfm}


procedure TTableViewerForm2.ActivatingNow;
begin
  BottomPageControl.ActivePage := QueryResultsTab;
end;

procedure TTableViewerForm2.CancelAsyncQuery(qry:TADODataSet);
var
  fCommand: _Command;
begin
  if not Assigned(qry.RecordSet) then exit;
  if stFetching in qry.RecordsetState then begin
    fCommand := _Command(qry.RecordSet.ActiveCommand);
    fCommand.Cancel;
    if Assigned(qry.RecordSet ) then qry.RecordSet.Cancel;
    TDataSetWrapper(qry).SetState(dsInactive);
    TDataSetWrapper(qry).CloseCursor;
    Qry.Connection.RollbackTrans;
    Qry.Connection.Close;
  end;
  if Assigned(qry.RecordSet) then qry.RecordSet.Cancel;
end;

procedure TTableViewerForm2.CDSViewDblClick(Sender: TObject);
begin
  inherited;
  LoadFromSaved;
end;

procedure TTableViewerForm2.ClearBtnClick(Sender: TObject);
begin
  inherited;
  QryMemo.Clear;
  MessagesMemo.Clear;
  GridView.ClearItems;
end;

function TTableViewerForm2.OpenDataFile: boolean;
begin
  SavedQueriesCDS.FileName := ExtractFilePath(Application.ExeName) + DataFile;
  if FileExists(SavedQueriesCDS.FileName) then
    SavedQueriesCDS.Open
  else
    SavedQueriesCDS.CreateDataSet;
  Result := True;
end;


constructor TTableViewerForm2.Create(Owner: TComponent);
begin
  inherited;
  PopulateControls;
  QryMemo.Lines.Clear;

  OpenDataFile;
end;

procedure TTableViewerForm2.CreateInsertStatement;
var
  TblName : string;
begin
  //Skeleton Only - Not complete
  TblName := TableNameComboBox.Text;
  QueryATable.CommandText := 'INSERT INTO ' + TblName;
  QryMemo.Lines.Add(QueryATable.CommandText);
end;

procedure TTableViewerForm2.btnSPListClick(Sender: TObject);
const
  //SPSQL = 'SELECT PROCS.name as [Stored Proc Name] FROM [sys].[procedures] as PROCS';
   SPSQL = 'select routine_name, routine_type as type from information_schema.routines ' +
   'where routine_schema not in (''sys'', ''information_schema'') ' +
   'order by routine_name';
begin
  inherited;
  SPTextGridView.ClearItems;
  LoadandRunSPTriggerQuery(SPSQL);
end;

procedure TTableViewerForm2.btnTriggerListClick(Sender: TObject);
const
  TriggerSQL = 'SELECT ' +
       'TAB.name as [Table Name], ' +
       'TRIG.name as [Trigger Name], ' +
       'TRIG.is_disabled as [Is Disabled] ' +
       'FROM [sys].[triggers] as TRIG '+
       'inner join sys.tables as TAB  ' +
       'on TRIG.parent_id = TAB.object_id  ';
begin
  inherited;
  SPTextGridView.ClearItems;
  LoadandRunSPTriggerQuery(TriggerSQL);
end;

procedure TTableViewerForm2.btnSPdefClick(Sender: TObject);
const
  TextDef = 'Exec sp_helptext N%s';
var
  SQLText:String;
  AField: TField;
begin
  inherited;
  if qrySPList.EOF then
  begin
    ShowMessage('No query results');
    Exit;
  end;

  AField := qrySPList.FindField('Trigger Name');
  if Assigned(AField) then
    SQLText := Format(TextDef, [QuotedStr(qrySPList.FieldByName('Trigger Name').AsString)])
  else
  begin
    AField := qrySPList.FindField('routine_name');
    if Assigned(AField) then
      SQLText := Format(TextDef, [QuotedStr(qrySPList.FieldByName('routine_name').AsString)]);
  end;
  if Assigned(AField) then
    LoadandRunSPDefQuery(SQLText);
end;

procedure TTableViewerForm2.btnClearSPClick(Sender: TObject);
begin
  inherited;
  SPGridview.ClearItems;
  SPTextGridView.ClearItems;
  qrySPList.Close;
  qrySPDef.Close;
end;

procedure TTableViewerForm2.btnSaveDefClick(Sender: TObject);
 var
 F: TStrings;
 SaveDialog : TSaveDialog;
begin
  inherited;
  if not qrySPDef.Active or qrySPDef.IsEmpty then
  begin
    ShowMessage('No query results');
    Exit;
  end;
  try
   SaveDialog :=  TSaveDialog.Create(self);
   SaveDialog. Options := saveDialog. Options + [ofOverwritePrompt];
   SaveDialog.Filter := 'sql Files(*.sql)|*sql';
   SaveDialog.Title := 'Save Query Results to sql';
   SaveDialog.DefaultExt := 'sql';
   SaveDialog.InitialDir :='';
   if SaveDialog.Execute then
   begin
     With qrySPDef do
     begin
       F:= TStringlist.create;
       First;
       While not EOF do
       begin
         F.Text := F.Text + (FieldByName ('Text').AsString);
         Next;
       end;
     end;
     F.SaveToFile (SaveDialog.FileName);
   end;
  finally
    F.Free;
    SaveDialog.Free;
  end;
end;

procedure TTableViewerForm2.LeftBtnClick(Sender: TObject);
begin
  inherited;
  SavedQueriesCDS.Prior;
  LoadFromSaved;
end;

function TTableViewerForm2.LoadAndRunCommand(CommandText: string) : string;
var
  RecAffected: integer;
begin
  //Test for Drop or Delete - To Add Later

  ADOCommand.CommandText := CommandText;
  SetCursor(crHourGlass);
  try
    try
      ADOCommand.Execute(RecAffected,EmptyParam);
      Result := ValidStr;
      ShowQueryMessage(mtCompleteCommandMsg, CommandText, RecAffected);
    except on e:exception do
      Result := e.Message;
    end;
  finally
    SetCursor(crDefault);
  end;
end;


function TTableViewerForm2.LoadAndRunQuery(CommandText: string): string;
var
  i : integer;
  Column: TcxGridDBColumn;
begin
  if QueryATable.Active then QueryATable.Close;
  TableGrid.BeginUpdate;
  SetCursor(crHourGlass);
  try
    try
      QueryATable.CommandText := CommandText;
      QueryATable.Open;
      ShowQueryMessage(mtCompleteMsg, CommandText, QueryATable.RecordCount);
      Result := ValidStr;
      except on e:exception do begin
        Result := e.Message; //just return the message for now
        exit;
      end;
    end;
    for i := 0 to QueryATable.FieldCount - 1 do
      if QueryATable.Fields[i].DisplayWidth > 35 then
        QueryATable.Fields[i].DisplayWidth := 35;

    GridView.ClearItems;
    GridView.DataController.CreateAllItems;
    for i := 0 to GridView.ColumnCount - 1 do begin
      Column := GridView.Columns[i];
      if Column.DataBinding.FieldName = 'active' then
        Column.Width := 38
      else if StrContainsText('_date', Column.DataBinding.FieldName) then
        Column.Width := 128;
    end;
  finally
    TableGrid.EndUpdate;
    SetCursor(crDefault);
  end;
end;

procedure TTableViewerForm2.LoadandRunSPDefQuery(SQLText: string);
begin
  if qrySPDef.Active then qrySPDef.Close;
  SPTextGrid.BeginUpdate;
  SetCursor(crHourGlass);
  try
    qrySPDef.SQL.Text := SQLText;
    qrySPDef.Open;
    if qrySPDef.EOF then
      raise Exception.Create('Stored Proc list is empty');
    SPTextGridView.ClearItems;
    SPTextGridView.DataController.CreateAllItems;
    qrySPDef.First;
  finally
    SPTextGrid.EndUpdate;
    SetCursor(crDefault);
  end;
end;

procedure TTableViewerForm2.LoadandRunSPTriggerQuery(SQLText: string);
var
  i : integer;
  Column: TcxGridDBColumn;
begin
  if qrySPList.Active then qrySPList.Close;
  SPGrid.BeginUpdate;
  SetCursor(crHourGlass);
  try
    qrySPList.SQL.Text := SQLText;
    qrySPList.Open;
    ShowQueryMessage(mtCompleteMsg, SQLText, qrySPList.RecordCount);
    for i := 0 to qrySPList.FieldCount - 1 do
    begin
      if(qrySPList.Fields[I].FieldName = 'Is Disabled') then
         qrySPList.Fields[I].DisplayWidth := 10 else
      if qrySPList.Fields[i].DisplayWidth > 35 then
        qrySPList.Fields[i].DisplayWidth := 35;
    end;
    SPGridView.ClearItems;
    SPGridView.DataController.CreateAllItems;
    qrySPList.First;
  finally
    SPGrid.EndUpdate;
    SetCursor(crDefault);
  end;
end;


procedure TTableViewerForm2.LoadFromSaved;
begin
  QryMemo.Clear;
  QryMemo.Text := SavedQueriesCDS.FieldByName('SQLText').Value;
end;

procedure TTableViewerForm2.LoadTableNames;
begin
  AdminDM.Conn.GetTableNames(TableNameComboBox.Properties.Items, False);
  AdminDM.Conn.GetTableNamesExt(TableNameComboBox.Properties.Items,False,False);
end;


procedure TTableViewerForm2.OpenDataSets;
begin
  SavedQueriesCDS.Active := True;
end;


procedure TTableViewerForm2.PopulateControls;
begin
  LoadTableNames;
end;


procedure TTableViewerForm2.QryMemoPropertiesChange(Sender: TObject);
begin
  inherited;
  CountLabel.Caption := IntToStr(Length(QryMemo.Text));
  LineCountLabel.Caption := IntToStr(QryMemo.Lines.Count);
end;

procedure TTableViewerForm2.RightBtnClick(Sender: TObject);
begin
  inherited;
    SavedQueriesCDS.Next;
    LoadFromSaved;
end;

procedure TTableViewerForm2.RunBtnClick(Sender: TObject);
begin
  inherited;
  RunCommand;
end;


procedure TTableViewerForm2.RunCommand;
var
  CommandStr: string;
  QryMsg, ComMsg: string;
begin
 CommandStr := QryMemo.Text;
 BottomPageControl.ActivePage := QueryResultsTab;

  QryMsg := LoadAndRunQuery(CommandStr);
  if QryMsg <> ValidStr then begin
    ComMsg := LoadAndRunCommand(CommandStr);
    if ComMsg <> ValidStr then
      ShowQueryMessage(mtErrorMsg, 'Invalid Query/Command. ' + ComMsg);
  end
  else
    ShowQueryMessage(mtCompleteMsg, QryMsg);
end;


procedure TTableViewerForm2.SaveButtonClick(Sender: TObject);
begin
  inherited;
  SaveQuery;
end;

procedure TTableViewerForm2.SaveQuery;
begin
 SaveQueryDlg := TSaveQueryDlg.Create(Self);
 try
   If PreviouslySaved then
     Exit;

   SaveQueryDlg.SQL.Text := QryMemo.Text;
   if SaveQueryDlg.ShowModal = mrOK then begin
     SavedQueriesCDS.Insert;
     SavedQueriesCDS.FieldByName('SQLText').AsString := QryMemo.Text;
     SavedQueriesCDS.FieldByName('ModDate').AsDateTime := Now;
     SavedQueriesCDS.FieldByName('ShortDesc').AsString := SaveQueryDlg.EdtShortDesc.Text;
     SavedQueriesCDS.Post;
     BottomPageControl.ActivePage := StoredQueriesTab;
   end
   else begin
     ShowQueryMessage(mtActionCancelledMsg, 'Query was not saved.');
   end;
 finally
   FreeAndNil(SaveQueryDlg);
 end;
end;

procedure TTableViewerForm2.SaveTSVXMLClick(Sender: TObject);
var
  AFile: TextFile;
  ResultsPath : string;
begin
  if (not QueryATable.Active) or (QueryATable.IsEmpty) then
  begin
    ShowQueryMessage(mtErrorMsg, 'Query results unavailable');
    Exit;
  end;
  SaveTSVXMLDlg.Filter := 'Tsv Files(*.tsv)|*.tsv|Xml files(*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx';
  SaveTSVXMLDlg.Title := 'Save Query Results to TSV/XML/XLSX';
  SaveTSVXMLDlg.DefaultExt := 'tsv';
  SaveTSVXMLDlg.InitialDir :='';
  if SaveTSVXMLDlg.Execute then
  begin
    ResultsPath := SaveTSVXMLDlg.FileName;
    AssignFile(AFile, ResultsPath);
    ReWrite(AFile);
    case SaveTSVXMLDlg.FilterIndex of
     1: SaveTSVFile(QueryATable, AFile);
     2: Write(AFile, DataSettoXML(QueryATable));
    end;
    CloseFile(AFile);
  end;
end;

procedure TTableViewerForm2.btnExcelExportClick(Sender: TObject);
begin
  inherited;
  ExporttoExcel(QueryATable);
end;

procedure TTableViewerForm2.ExportToExcel( Query: TDataSet);

var
  Cursor: IInterface;
  x: integer;
  Range, Worksheet, Excel, Data: Variant;
  sValue: WideString;

procedure KillExcel(var App: Variant);
var
  ProcID: DWORD;
  hProc: THandle;
  hW: HWND;
begin
  hW := App.Application.Hwnd;
  // close with usual methods
  App.DisplayAlerts := False;
  App.Workbooks.Close;
  App.Quit;
  App := Unassigned;
  // close with WinApi
  if not IsWindow(hW) then Exit; // already closed?
  GetWindowThreadProcessId(hW, ProcID);
  hProc := OpenProcess(PROCESS_TERMINATE, False, ProcID);
  TerminateProcess(hProc, 0);
end;

begin
  if (not QueryATable.Active) or (QueryATable.IsEmpty) then
  begin
    ShowQueryMessage(mtErrorMsg, 'Query results unavailable');
    Exit;
  end;

  try
    try
      try
       Excel := CreateOleObject( 'Excel.Application' );
      except
       on E: Exception do
         raise Exception.Create( 'Error starting Excel. Details: ' + E.Message );
      end;
      cursor := ShowHourGlass;

      Excel.Workbooks.Add;
      WorkSheet := Excel.Sheets.Add;
     // translate data into a VarArray
      Data := VarArrayCreate( [0, Query.RecordCount, 0, Query.FieldCount-1], varVariant);

      for x := 0 to Query.FieldCount - 1 do
        Data[0, x] := Query.Fields[x].FieldName;

      System.Variants.NullStrictConvert := False;
      Query.First;
      while not Query.Eof do begin
        for x := 0 to Query.FieldCount - 1 do
        begin
          sValue := Query.Fields[x].Value; //Use Widestring to prevent type errors in Excel
          Data[Query.RecNo, x] := sValue;
        end;
        Query.Next;
      end;

      Range := WorkSheet.Range[ WorkSheet.Cells[1,1], WorkSheet.Cells[Query.RecordCount + 1, Query.FieldCount] ];
     {copy data from allocated variant array}
      Range.Value := Data;
      Excel.Visible := true;
    except
      On E:Exception do
      begin
        KillExcel(Excel); //Force Excel COM Object to be released
        ShowQueryMessage(mtErrorMsg, E.Message);
      end;
    end;
  finally
     Data := Unassigned;
     Range := Unassigned;
     WorkSheet := Unassigned;
     Excel := Unassigned;
  end;
end;

procedure TTableViewerForm2.SaveTSVFile(DataSet: TADODataset; var AFile: TextFile);
var
  I: integer;
  lineText: String;
const
  tab = #9;
begin
  lineText := '';
  for I := 0 to DataSet.FieldCount - 1 do
    lineText := lineText + DataSet.Fields[i].FieldName + tab;
  WriteLn(AFile, lineText); //creates the header line
  DataSet.First;
  while not DataSet.eof do
  begin
    lineText := '';
    for i := 0 to DataSet.FieldCount - 1 do
      lineText := lineText + DataSet.Fields[i].AsString + tab;
      Writeln(AFile, lineText); //add the data
    DataSet.Next;
  end;
end;



function TTableViewerForm2.DataSetToXML(DataSet: TDataSet): String;
const
  ROOT = 'RESULTS';
  tab = #9;
var
  I: integer;
function MakeTag(TagName, value: String): string;
begin
  result := '<' + TagName + '>' + value + '</' + TagName + '>';
end;

begin
  result := '';
  result := result + '<' + ROOT + '>';
  DataSet.First;
  while not DataSet.eof do
  begin
    result := result + '<result>'; //each record is a result
  for I := 0 to DataSet.Fields.Count - 1 do
    result := result + MakeTag(DataSet.Fields[I].DisplayName,
    DataSet.Fields[I].Text);
    result := result + '</result >';
    DataSet.Next;
  end;
  result := result + '</' + ROOT + '>'; //the entire XML are the Results
end;


procedure TTableViewerForm2.SetControlsReadOnly(const IsReadOnly: Boolean; Parent: TComponent);
begin
  //Leave the other controls alone...
  GridView.OptionsData.Appending := not IsReadOnly;
  GridView.OptionsData.Inserting := not IsReadOnly;
  GridView.OptionsData.Deleting := not IsReadOnly;
  GridView.OptionsData.Editing := not IsReadOnly;
  GridView.OptionsSelection.CellSelect := not IsReadOnly;
end;


procedure TTableViewerForm2.SetCursor(ACursor: TCursor);
begin
  Self.Cursor := ACursor;
  QryMemo.Cursor := ACursor;
  TableGrid.Cursor := ACursor;
  CDSGrid.Cursor := ACursor;
end;

procedure TTableViewerForm2.ShowQueryMessage(MsgType: TMsgType; MsgText: string; RecAffected: integer);
const
  SEPLINE = '-------------------------------------------';
var
  CommandStr: string;
begin
  CommandStr := QryMemo.Text;
  MessagesMemo.Style.Font.Color := clMaroon;
  MessagesMemo.Properties.ReadOnly := True;
  MessagesMemo.Clear;
  if MsgType = mtErrorMsg then begin
    MessagesMemo.Lines.Add('ERROR: Please check your query statement.');
    MessagesMemo.Lines.Add('    ' + MsgText);
    MessagesMemo.Lines.Add('');
    MessagesMemo.Lines.Add(SEPLINE);
    MessagesMemo.Lines.Add('Command Statement: ');
    MessagesMemo.Lines.Add('    ' + CommandStr);
    BottomPageControl.ActivePage := MessagesTab;
  end
  else if MsgType = mtActionCancelledMsg then begin
    MessagesMemo.Lines.Add('ACTION CANCELLED:');
    MessagesMemo.Lines.Add('    ' + MsgText);
    BottomPageControl.ActivePage := MessagesTab;
  end
  else if MsgType = mtCompleteCommandMsg then begin
    if (RecAffected > -1) then
      MessagesMemo.Lines.Add('COMPLETED: ' + IntToStr(RecAffected) + ' record(s) affected.')
    else
      MessagesMemo.Lines.Add('COMPLETED');
    MessagesMemo.Lines.Add(' ');
    MessagesMemo.Lines.Add(SEPLINE);
    MessagesMemo.Lines.Add('Command Statement: ');
    MessagesMemo.Lines.Add(CommandStr);
    BottomPageControl.ActivePage := MessagesTab;
  end
  else if MsgType = mtCompleteMsg then begin
    if (RecAffected > -1) then
      MessagesMemo.Lines.Add('COMPLETED: ' + IntToStr(RecAffected) + ' record(s) affected.')
    else
      MessagesMemo.Lines.Add('COMPLETED:');
    MessagesMemo.Lines.Add(SEPLINE);
    MessagesMemo.Lines.Add(CommandStr);
  end
  else
    QryMemo.Lines.Add(MsgText);
end;


procedure TTableViewerForm2.StopBtnClick(Sender: TObject);
begin
  inherited;
  CancelASyncQuery(QueryATable);
end;


procedure TTableViewerForm2.TableNameChange;
var
  Sel: string;
  NewCommand : string;
begin
 if QueryATable.Active then QueryATable.Close;

 Sel := Trim(TableNameComboBox.Text);
 if insertCB.Checked then
   CreateInsertStatement
 else if (Sel = '') or (Sel = '--') then
    QueryATable.Close
  else begin
    TableGrid.BeginUpdate;
    try
      QryMemo.Lines.Clear;
      NewCommand := 'SELECT top 100 * FROM ' + TableNameComboBox.Text;

      QueryATable.CommandText := NewCommand;
      QryMemo.Lines.Add(NewCommand);
    finally
      TableGrid.EndUpdate;
    end;
  end;
end;


procedure TTableViewerForm2.TableNameComboBoxPropertiesChange(Sender: TObject);
begin
  TableNameChange;
  RunCommand;
  LockNow;
end;

function TTableViewerForm2.PreviouslySaved: boolean;
begin
  Result := False;
  SavedQueriesCDS.DisableControls;
  try
    SavedQueriesCDS.First;
    while not SavedQueriesCDS.Eof do begin
      if TRIM(SavedQueriesCDS.FieldByName('SQLText').Value) =
        TRIM(QryMemo.Text) then begin
          Result := True;
          Break;
        end;
      SavedQueriesCDS.Next;
    end;
  finally
    SavedQueriesCDS.EnableControls;
  end;
  if Result then
    ShowQueryMessage(mtActionCancelledMsg, 'Query is already saved in record: ' + IntToStr(SavedQueriesCDS.RecNo));
end;

end.

