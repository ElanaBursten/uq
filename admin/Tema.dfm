inherited TemaForm: TTemaForm
  Caption = 'Tema Forms'
  ClientHeight = 500
  ClientWidth = 1004
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 208
    Width = 1004
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 1004
    Height = 25
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 1004
    Height = 183
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Delete.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmAlways
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsView.GroupByBox = True
      object ColFormName: TcxGridDBColumn
        Caption = 'Form Name'
        DataBinding.FieldName = 'form_name'
        DataBinding.IsNullValueType = True
        Width = 151
      end
      object ColFormDescription: TcxGridDBColumn
        Caption = 'Form Description'
        DataBinding.FieldName = 'form_description'
        DataBinding.IsNullValueType = True
        Width = 259
      end
      object ColCalCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        DataBinding.IsNullValueType = True
      end
      object Coludf00: TcxGridDBColumn
        DataBinding.FieldName = 'udf00'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf01: TcxGridDBColumn
        DataBinding.FieldName = 'udf01'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf02: TcxGridDBColumn
        DataBinding.FieldName = 'udf02'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf03: TcxGridDBColumn
        DataBinding.FieldName = 'udf03'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf04: TcxGridDBColumn
        DataBinding.FieldName = 'udf04'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf05: TcxGridDBColumn
        DataBinding.FieldName = 'udf05'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf06: TcxGridDBColumn
        DataBinding.FieldName = 'udf06'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf07: TcxGridDBColumn
        DataBinding.FieldName = 'udf07'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf08: TcxGridDBColumn
        DataBinding.FieldName = 'udf08'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf09: TcxGridDBColumn
        DataBinding.FieldName = 'udf09'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf10: TcxGridDBColumn
        DataBinding.FieldName = 'udf10'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf11: TcxGridDBColumn
        DataBinding.FieldName = 'udf11'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf12: TcxGridDBColumn
        DataBinding.FieldName = 'udf12'
        DataBinding.IsNullValueType = True
        Width = 120
      end
      object Coludf13: TcxGridDBColumn
        DataBinding.FieldName = 'udf13'
        DataBinding.IsNullValueType = True
        Width = 120
      end
    end
  end
  object PC: TPageControl [3]
    Left = 0
    Top = 216
    Width = 1004
    Height = 284
    ActivePage = TemaHeaderSheet
    Align = alBottom
    TabOrder = 2
    object TemaDetailsSheet: TTabSheet
      Caption = 'Tema Details'
      ImageIndex = 3
      object DetailGrid: TcxGrid
        Left = 0
        Top = 0
        Width = 996
        Height = 256
        Align = alClient
        TabOrder = 0
        OnExit = DetailGridExit
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object DetailGridView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Delete.Visible = False
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmAlways
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataModeController.SmartRefresh = True
          DataController.DataSource = dsTemaDetails
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsCustomize.ColumnFiltering = False
          OptionsData.Appending = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          object ColDetailudf00: TcxGridDBColumn
            DataBinding.FieldName = 'udf00'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf01: TcxGridDBColumn
            DataBinding.FieldName = 'udf01'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf02: TcxGridDBColumn
            DataBinding.FieldName = 'udf02'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf03: TcxGridDBColumn
            DataBinding.FieldName = 'udf03'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf04: TcxGridDBColumn
            DataBinding.FieldName = 'udf04'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf05: TcxGridDBColumn
            DataBinding.FieldName = 'udf05'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf06: TcxGridDBColumn
            DataBinding.FieldName = 'udf06'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf07: TcxGridDBColumn
            DataBinding.FieldName = 'udf07'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf08: TcxGridDBColumn
            DataBinding.FieldName = 'udf08'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf09: TcxGridDBColumn
            DataBinding.FieldName = 'udf09'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf10: TcxGridDBColumn
            DataBinding.FieldName = 'udf10'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf11: TcxGridDBColumn
            DataBinding.FieldName = 'udf11'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf12: TcxGridDBColumn
            DataBinding.FieldName = 'udf12'
            DataBinding.IsNullValueType = True
            Width = 120
          end
          object ColDetailudf13: TcxGridDBColumn
            DataBinding.FieldName = 'udf13'
            DataBinding.IsNullValueType = True
            Width = 120
          end
        end
        object DetailGridLevel: TcxGridLevel
          GridView = DetailGridView
        end
      end
    end
    object TemaHeaderSheet: TTabSheet
      Caption = 'Tema Header'
      ImageIndex = 4
      object HeaderGrid: TcxGrid
        Left = 0
        Top = 0
        Width = 996
        Height = 256
        Align = alClient
        TabOrder = 0
        OnExit = HeaderGridExit
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object HeaderGridView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Delete.Visible = False
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmAlways
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dsTemaHeader
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          object cxGridColFormName: TcxGridDBColumn
            Caption = 'Form Name'
            DataBinding.FieldName = 'form_name'
            DataBinding.IsNullValueType = True
            Width = 151
          end
          object ColTicketID: TcxGridDBColumn
            Caption = 'Ticket ID'
            DataBinding.FieldName = 'ticket_id'
            DataBinding.IsNullValueType = True
            Width = 89
          end
          object ColEmpID: TcxGridDBColumn
            Caption = 'Emp ID'
            DataBinding.FieldName = 'emp_id'
            DataBinding.IsNullValueType = True
            Width = 72
          end
          object ColInsertDate: TcxGridDBColumn
            Caption = 'Insert Date'
            DataBinding.FieldName = 'insert_date'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.ReadOnly = True
            Options.Editing = False
            Width = 144
          end
        end
        object HeaderGridLevel: TcxGridLevel
          GridView = HeaderGridView
        end
      end
    end
    object TemaFieldTypeSheet: TTabSheet
      Caption = 'Tema Field Type'
      ImageIndex = 5
      object FieldTypeGrid: TcxGrid
        Left = 0
        Top = 0
        Width = 996
        Height = 256
        Align = alClient
        TabOrder = 0
        OnExit = FieldTypeGridExit
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = True
        object FieldTypeGridView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Delete.Visible = False
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dsTemaFieldType
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          object ColFieldTypeFormName: TcxGridDBColumn
            Caption = 'Form Name'
            DataBinding.FieldName = 'form_name'
            DataBinding.IsNullValueType = True
            Width = 126
          end
          object ColFieldTypeudf00: TcxGridDBColumn
            DataBinding.FieldName = 'udf00'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf01: TcxGridDBColumn
            DataBinding.FieldName = 'udf01'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf02: TcxGridDBColumn
            DataBinding.FieldName = 'udf02'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf03: TcxGridDBColumn
            DataBinding.FieldName = 'udf03'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf04: TcxGridDBColumn
            DataBinding.FieldName = 'udf04'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf05: TcxGridDBColumn
            DataBinding.FieldName = 'udf05'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf06: TcxGridDBColumn
            DataBinding.FieldName = 'udf06'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf07: TcxGridDBColumn
            DataBinding.FieldName = 'udf07'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf08: TcxGridDBColumn
            DataBinding.FieldName = 'udf08'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf09: TcxGridDBColumn
            DataBinding.FieldName = 'udf09'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf10: TcxGridDBColumn
            DataBinding.FieldName = 'udf10'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf11: TcxGridDBColumn
            DataBinding.FieldName = 'udf11'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf12: TcxGridDBColumn
            DataBinding.FieldName = 'udf12'
            DataBinding.IsNullValueType = True
          end
          object ColFieldTypeudf13: TcxGridDBColumn
            DataBinding.FieldName = 'udf13'
            DataBinding.IsNullValueType = True
          end
        end
        object FieldTypeGridLevel: TcxGridLevel
          GridView = FieldTypeGridView
        end
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from tema_form'
  end
  object TemaDetails: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = TemaDetailsBeforeInsert
    BeforeEdit = TemaDetailsBeforeEdit
    AfterPost = TemaDetailsAfterPost
    AfterCancel = TemaDetailsAfterCancel
    CommandText = 'select * from tema_details'
    Parameters = <>
    Left = 184
    Top = 128
  end
  object dsTemaDetails: TDataSource
    DataSet = TemaDetails
    Left = 272
    Top = 128
  end
  object TemaHeader: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = TemaHeaderBeforeInsert
    BeforeEdit = TemaHeaderBeforeEdit
    AfterPost = TemaHeaderAfterPost
    AfterCancel = TemaHeaderAfterCancel
    CommandText = 'select * from tema_header'
    Parameters = <>
    Left = 184
    Top = 176
  end
  object dsTemaHeader: TDataSource
    DataSet = TemaHeader
    Left = 272
    Top = 176
  end
  object TemaFieldType: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = TemaFieldTypeBeforeInsert
    BeforeEdit = TemaFieldTypeBeforeEdit
    AfterPost = TemaFieldTypeAfterPost
    AfterCancel = TemaFieldTypeAfterCancel
    CommandText = 'select * from tema_fieldtype'
    Parameters = <>
    Left = 184
    Top = 76
  end
  object dsTemaFieldType: TDataSource
    DataSet = TemaFieldType
    Left = 272
    Top = 77
  end
end
