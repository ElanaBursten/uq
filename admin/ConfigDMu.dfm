object ConfigDM: TConfigDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 290
  Width = 409
  object ValueGroup: TADODataSet
    CommandText = 'select *'#13#10'from value_group'
    Parameters = <>
    Left = 24
    Top = 64
  end
  object CenterGroup: TADODataSet
    CommandText = 'select * from center_group'#13#10'where active=1'#13#10'order by group_code'
    Parameters = <>
    Left = 24
    Top = 112
  end
  object TermGroup: TADODataSet
    CommandText = 'select *'#13#10'from term_group'#13#10'where active = 1'
    Parameters = <>
    Left = 24
    Top = 16
  end
  object ValueGroupDetail: TADODataSet
    CommandText = 'select *'#13#10'from value_group_detail'
    Parameters = <>
    Left = 116
    Top = 64
  end
  object CenterGroupDetail: TADODataSet
    CommandText = 
      'select * from center_group_detail'#13#10'where active=1'#13#10'order by cent' +
      'er_group_id, call_center /* Must be id sorted */'
    Parameters = <>
    Left = 116
    Top = 112
  end
  object TermGroupDetail: TADODataSet
    CommandText = 
      'select * from term_group_detail'#13#10'where active = 1'#13#10'order by term' +
      '_group_id  /* Must be id sorted */'
    Parameters = <>
    Left = 116
    Top = 16
  end
  object Holiday: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from  holiday'
    Parameters = <>
    Left = 208
    Top = 16
  end
  object ProfitCenters: TADODataSet
    CommandText = 'select pc_code'#13#10'from profit_center'#13#10'order by pc_code'
    Parameters = <>
    Left = 300
    Top = 16
  end
  object ConfigurationData: TADODataSet
    CommandText = 'select value from configuration_data where name = :name'
    Parameters = <
      item
        Name = 'name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end>
    Left = 300
    Top = 72
  end
  object Reference: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from reference where type=:type order by code'
    Parameters = <
      item
        Name = 'type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    Left = 208
    Top = 72
  end
  object CallCenters: TADODataSet
    CursorType = ctStatic
    CommandText = 'select cc_code from call_center order by 1'
    Parameters = <>
    Left = 24
    Top = 172
  end
  object AuditMethods: TADODataSet
    CommandText = 'select name from audit_methods'#13#10'order by name'
    Parameters = <>
    Left = 304
    Top = 136
  end
  object DycomPeriods: TADODataSet
    CursorType = ctStatic
    CommandText = 
      'select * from dycom_period'#13#10'--where ending >= DateAdd(yyyy, -1, ' +
      'getdate())'#13#10'--  and ending <= DateAdd(yyyy, 1, getdate())'#13#10#13#10
    Parameters = <>
    Left = 120
    Top = 172
  end
  object Clients: TADODataSet
    CursorType = ctStatic
    CommandText = 'select distinct oc_code'#13#10'from client'#13#10'order by oc_code'
    Parameters = <>
    Left = 208
    Top = 124
  end
  object Statuses: TADODataSet
    CursorType = ctStatic
    CommandText = 'select * from statuslist'
    Parameters = <>
    Left = 208
    Top = 178
  end
end
