unit CustomerTerms;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, Buttons, ExtCtrls, DB, ADODB, StdCtrls, AdminDMu,
  DBCtrls, Mask, ComCtrls, Grids, DBGrids, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxDropDownEdit,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxTextEdit,
  cxContainer, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxDBEdit,
  cxLookAndFeels, cxLookAndFeelPainters,dxCore, cxNavigator, cxLabel, cxDBLabel,
  BaseCxList, dxSkinsCore, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBLookupComboBox,
  cxMemo, Vcl.DBActns, System.Actions, Vcl.ActnList, dxDateRanges, dxSkinTheBezier,
  dxSkinOffice2019Colorful, dxScrollbarAnnotations, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxSkinBasic, dxBarBuiltInMenu, dxSkinsForm;

type
  TCustomerTermForm = class(TEmbeddableForm)
    CustomerPanel: TPanel;
    TermPanel: TPanel;
    HorizontalSplitter: TSplitter;
    TermListPanel: TPanel;
    CustomerListPanel: TPanel;
    TermHeaderPanel: TPanel;
    AllTermsButton: TSpeedButton;
    CustomerTermsButton: TSpeedButton;
    CustomerHeaderPanel: TPanel;
    CustomersLabel: TLabel;
    Customer: TADODataSet;
    CustomerSource: TDataSource;
    TermSource: TDataSource;
    OrphanedTermsButton: TSpeedButton;
    OfficeLookup: TADODataSet;
    OfficeLookupSource: TDataSource;
    UtilityLookup: TADODataSet;
    UtilityLookupSource: TDataSource;
    SGLookup: TADODataSet;
    SGLookupDS: TDataSource;
    Company: TADODataSet;
    CompanyDS: TDataSource;
    UnitConversionLookup: TADODataSet;
    UnitConversionDS: TDataSource;
    ProfitCenterSource: TDataSource;
    ProfitCenter: TADODataSet;
    NotificationEmailDS: TDataSource;
    CustomerGrid: TcxGrid;
    CustomerGridView: TcxGridDBTableView;
    ColCustID: TcxGridDBColumn;
    ColCustName: TcxGridDBColumn;
    ColCustNumber: TcxGridDBColumn;
    ColCustDescription: TcxGridDBColumn;
    ColCustState: TcxGridDBColumn;
    ColCustActive: TcxGridDBColumn;
    CustomerGridLevel: TcxGridLevel;
    TermGrid: TcxGrid;
    TermGridView: TcxGridDBTableView;
    TermGridViewClientID: TcxGridDBColumn;
    TermGridViewOCCode: TcxGridDBColumn;
    TermGridViewClientName: TcxGridDBColumn;
    TermGridViewActive: TcxGridDBColumn;
    TermGridViewCallCenter: TcxGridDBColumn;
    TermGridViewCustomerID: TcxGridDBColumn;
    TermGridViewUtilityName: TcxGridDBColumn;
    OfficeLookupView: TcxGridDBTableView;
    OfficeLookupViewOfficeID: TcxGridDBColumn;
    OfficeLookupViewOfficeName: TcxGridDBColumn;
    OfficeLookupViewProfitCenter: TcxGridDBColumn;
    UtilityLookupView: TcxGridDBTableView;
    UtilityLookupViewDescription: TcxGridDBColumn;
    StatusGroupLookupView: TcxGridDBTableView;
    StatusGroupLookupViewSGName: TcxGridDBColumn;
    BillingUnitConversionLookupView: TcxGridDBTableView;
    BillingUnitConversionLookupViewDescription: TcxGridDBColumn;
    BillingUnitConversionLookupViewUnitConversionID: TcxGridDBColumn;
    TermGridLevel: TcxGridLevel;
    DamageCoLookupSource: TDataSource;
    DamageCoLookup: TADODataSet;
    TermGridViewDamageCompany: TcxGridDBColumn;
    DamageCompanyLookupView: TcxGridDBTableView;
    DamageCompanyLookupViewDamageCo: TcxGridDBColumn;
    TermGridViewRefID: TcxGridDBColumn;
    DamageCompanyLookupViewRefID: TcxGridDBColumn;
    DamageCompanyLookupViewActiveID: TcxGridDBColumn;
    NotificationEmail: TADOTable;
    ClientInfoDS: TDataSource;
    ClientInfo: TADOTable;
    Term: TADODataSet;
    EmpTrainerSource: TDataSource;
    EmpTrainer: TADODataSet;
    EmpEngineer: TADODataSet;
    ColCustNumberPC: TcxGridDBColumn;
    Splitter1: TSplitter;
    EmpEngineerSource: TDataSource;
    CenterActions: TActionList;
    AddAction: TDataSetInsert;
    SaveAction: TDataSetPost;
    CancelAction: TDataSetCancel;
    InsertRightCommand: TADOCommand;
    ChildClientSP: TADOStoredProc;
    chkboxCustSearch: TCheckBox;
    Label40: TLabel;
    ColActive: TcxGridDBColumn;
    ActiveCustomersFilter: TCheckBox;
    Label41: TLabel;
    chkboxClientSearch: TCheckBox;
    ActiveClientsFilter: TCheckBox;
    ResponderLookup: TADODataSet;
    ResponderLookupDS: TDataSource;
    ResponderLookupView: TcxGridDBTableView;
    ResponderLookupViewCode: TcxGridDBColumn;
    ResponderLookupViewRefID: TcxGridDBColumn;
    TermGridViewResponder: TcxGridDBColumn;
    EditCustomerPanel: TScrollBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label27: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    CustDesc: TDBEdit;
    CustName: TDBEdit;
    CustNum: TDBEdit;
    CustPhone: TDBEdit;
    CustStreet: TDBEdit;
    CustZip: TDBEdit;
    CustState: TDBComboBox;
    CustCity: TDBEdit;
    LocatingCompany: TDBLookupComboBox;
    CustStreet2: TDBEdit;
    CustContactName: TDBEdit;
    CustContactEmail: TDBEdit;
    CustAttention: TDBEdit;
    CustomerProfitCenter: TDBLookupComboBox;
    CustContractNum: TDBEdit;
    CustomerNumberPC: TDBEdit;
    CustDamageCap: TDBEdit;
    ReverseDetailButton: TSpeedButton;
    VerticalSplitter: TSplitter;
    HPRanking: TADOQuery;
    HPRankingDS: TDataSource;
    cxGridPopupMenu1: TcxGridPopupMenu;
    RankLookup: TADOQuery;
    RankLookupDS: TDataSource;
    ResetRank: TADOQuery;
    SmallintField1: TSmallintField;
    qryDupRank: TADOQuery;
    HPRefid_Lookup: TADOQuery;
    HPRefidDS: TDataSource;
    qryDupHPRefid: TADOQuery;
    RankLookuprank: TSmallintField;
    UpdateCode: TADOQuery;
    SmallintField2: TSmallintField;
    ClientRespondTo: TADODataSet;
    ClientRespondToDS: TDataSource;
    TermDetailPageControl: TPageControl;
    TermInfoSheet: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    ClientName: TDBEdit;
    ClientOCCode: TDBEdit;
    ClientActive: TDBCheckBox;
    ClientGridEmail: TDBEdit;
    ClientAllowEditLocates: TDBCheckBox;
    ClientCallCenter: TcxDBComboBox;
    ClientUpdateCallCenter: TcxDBComboBox;
    ClientOffice: TcxDBExtLookupComboBox;
    AssignButton: TButton;
    ClientCancelButton: TButton;
    UtilityType: TcxDBExtLookupComboBox;
    ClientStatusGroupName: TcxDBExtLookupComboBox;
    CheckAlert: TDBCheckBox;
    ClientRequireUnits: TDBCheckBox;
    ClientAttachmentURL: TDBEdit;
    ClientBillingUnitRules: TcxDBComboBox;
    DamageCompany: TcxDBExtLookupComboBox;
    ProjectClientButton: TButton;
    ckboxAuditReq: TDBCheckBox;
    NotificationEmailSheet: TTabSheet;
    Label16: TLabel;
    NotificationEmailGrid: TcxGrid;
    NotificationEmailView: TcxGridDBTableView;
    NotificationEmailViewNotificationType: TcxGridDBColumn;
    NotificationEmailViewEmailAddress: TcxGridDBColumn;
    NotificationEmailGridLevel: TcxGridLevel;
    TabSheet1: TTabSheet;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    memoClientRules: TDBMemo;
    memoConcerns: TDBMemo;
    memoPlatInfo: TDBMemo;
    edtPlatFile: TDBEdit;
    edtTrainLink: TDBEdit;
    edtPlatsLink: TDBEdit;
    cbTrainer: TDBLookupComboBox;
    cbFieldEngineer: TDBLookupComboBox;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    btnHPRanking: TButton;
    Client_Code: TDBEdit;
    Client_Name: TDBEdit;
    ActiveRankingFilter: TCheckBox;
    HPRankGrid: TcxGrid;
    HPRankView: TcxGridDBTableView;
    HPRankViewDescription: TcxGridDBColumn;
    HPRankViewUtilityCode: TcxGridDBColumn;
    HPRankViewCode: TcxGridDBColumn;
    HPRankViewRank: TcxGridDBColumn;
    HPRankViewActive: TcxGridDBColumn;
    HPRankLookupView: TcxGridDBTableView;
    HPRankLookupViewRank: TcxGridDBColumn;
    RefIDLookupView: TcxGridDBTableView;
    RefIDLookupViewCode: TcxGridDBColumn;
    RefIDLookupViewDescription: TcxGridDBColumn;
    HPRankGridLevel: TcxGridLevel;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    cxGrid2: TcxGrid;
    ClientRespondToView: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    CRTViewActive: TcxGridDBColumn;
    CRTViewClientRepondTo: TcxGridDBColumn;
    chkboxClientRespondToSearch: TCheckBox;
    CustActiveCheckbox: TDBCheckBox;
    PeriodTypeCombo: TDBComboBox;
    Label39: TLabel;
    DycomCustNumber: TDBEdit; //qm-653 sr
    ColDycomCustNumber: TcxGridDBColumn; 
    lblDyCustName: TLabel;
    DycomCustName: TDBEdit;
    TermGridViewBigFoot: TcxGridDBColumn;
    dxSkinController1: TdxSkinController;  //qm-135  BP
    procedure FormCreate(Sender: TObject);
    procedure AllTermsButtonClick(Sender: TObject);
    procedure CustomerAfterScroll(DataSet: TDataSet);
    procedure TermAfterInsert(DataSet: TDataSet);
    procedure TermBeforeOpen(DataSet: TDataSet);
    procedure AssignButtonClick(Sender: TObject);
    procedure ClientCancelButtonClick(Sender: TObject);
    procedure CustomerGridViewDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure CustomerGridViewDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure TermBeforeInsert(DataSet: TDataSet);
    procedure TermBeforePost(DataSet: TDataSet);
    procedure CustomerBeforePost(DataSet: TDataSet);
    procedure CustomerBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure NotificationEmailNewRecord(DataSet: TDataSet);
    procedure CustomerGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TermGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CustomerSourceDataChange(Sender: TObject; Field: TField);
    procedure DamageCompanyLookupViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ClientInfoNewRecord(DataSet: TDataSet);
    procedure TermAfterPost(DataSet: TDataSet);
    procedure ProjectClientButtonClick(Sender: TObject);
    procedure chkboxCustSearchClick(Sender: TObject);
    procedure ActiveCustomersFilterClick(Sender: TObject);
    procedure CustomerNewRecord(DataSet: TDataSet);
    procedure ActiveClientsFilterClick(Sender: TObject);
    procedure chkboxClientSearchClick(Sender: TObject);
    procedure TermNewRecord(DataSet: TDataSet);
    procedure DatasetBeforeDelete(DataSet: TDataSet);
    procedure NotificationEmailGridExit(Sender: TObject);
    procedure ReverseDetailButtonClick(Sender: TObject);
    procedure TermAfterScroll(DataSet: TDataSet);

    procedure btnHPRankingClick(Sender: TObject);             //BP 313
    procedure HPRankViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TermSourceDataChange(Sender: TObject; Field: TField);
    procedure Client_NameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HPRankingNewRecord(DataSet: TDataSet);
    procedure ActiveRankingFilterClick(Sender: TObject);
    procedure HPRankingAfterPost(DataSet: TDataSet);
    procedure HPRankingBeforePost(DataSet: TDataSet);
    procedure HPRankingBeforeEdit(DataSet: TDataSet);
    procedure chkboxClientRespondToSearchClick(Sender: TObject);
    procedure ClientRespondToViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CustomerBeforeEdit(DataSet: TDataSet);
    procedure CustomerAfterPost(DataSet: TDataSet);
    procedure CustomerBeforeInsert(DataSet: TDataSet);
    procedure CustomerAfterCancel(DataSet: TDataSet);
    procedure TermBeforeEdit(DataSet: TDataSet);
    procedure TermAfterCancel(DataSet: TDataSet);
    procedure NotificationEmailAfterCancel(DataSet: TDataSet);
    procedure NotificationEmailAfterPost(DataSet: TDataSet);
    procedure NotificationEmailBeforeEdit(DataSet: TDataSet);
    procedure ClientInfoBeforeEdit(DataSet: TDataSet);
    procedure ClientInfoAfterPost(DataSet: TDataSet);
    procedure HPRankingAfterCancel(DataSet: TDataSet);
    procedure HPRankingBeforeInsert(DataSet: TDataSet);
    procedure CustomerGridViewNavigatorButtonsButtonClick(Sender: TObject;
      AButtonIndex: Integer; var ADone: Boolean);
    procedure NotificationEmailBeforeInsert(DataSet: TDataSet);
    procedure TabSheet1Exit(Sender: TObject);
    procedure ClientRespondToBeforeEdit(DataSet: TDataSet);
    procedure ClientRespondToAfterPost(DataSet: TDataSet);
    procedure TermGridViewNavigatorButtonsButtonClick(Sender: TObject;
      AButtonIndex: Integer; var ADone: Boolean);
  private
    HPRankID, HPRank, HPRefID: Integer;    //BP 313
    HPActive, ZeroRank: Boolean;
    CustInserted: Boolean;
    TermInserted: Boolean;
    NEInserted: Boolean;
    HPRankingInserted: Boolean;
    ATermArray: TFieldValuesArray;
    NotificationEmailsArray: TFieldValuesArray;
    ClientInfoArray: TFieldValuesArray;
    HPRankingArray: TFieldValuesArray;
    CrtArray: TFieldValuesArray;
    function CurrentCustomerID: Integer;
    procedure AssignSelectedTermToCustomer(CustomerID: Integer);
    function GetIdAt(X, Y: Integer): Integer;
    procedure TermUnitConversionOnGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure TermUnitConversionOnSetText(Sender: TField; const Text: string);
    procedure EnableClientInfoControls(Enabled: Boolean);
    function HasRecords(DataSet: TDataSet): Boolean;
    procedure UpdateCustomerFilter;
    function CurrentTermCustomerID: Integer;
    procedure RefreshRankData;
  public
    procedure PopulateDropdowns; override;
    procedure UpdateTermFilter;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;

implementation

uses
  OdDbUtils, OdMiscUtils, odHourGlass, BillPeriod, OdVclUtils, OdCxUtils;

{$R *.dfm}

procedure TCustomerTermForm.PopulateDropdowns;
begin
  inherited;
  ClientOffice.DataBinding.DataSource := TermSource;
  AdminDM.GetStateList(CxComboBoxItems(ColCustState));
  AdminDM.GetCallCenterList(CxComboBoxItems(TermGridViewCallCenter));
  ClientCallCenter.Properties.Items.Assign(CxComboBoxItems(TermGridViewCallCenter));
  ClientUpdateCallCenter.Properties.Items.Assign(CxComboBoxItems(TermGridViewCallCenter));
  CustState.Items.Assign(CxComboBoxItems(ColCustState));
  AdminDM.GetUnitConversionList(ClientBillingUnitRules.Properties.Items);
  UpdateTermFilter;
end;

procedure TCustomerTermForm.ProjectClientButtonClick(Sender: TObject);
var
  FClientID: Integer;
begin
  inherited;
  if not Term.FieldByName('parent_client_id').IsNull then
  begin
    ShowMessage('child client can not be a parent');
    Exit;
  end;

  try
    FClientID :=  Term.FieldByName('client_id').AsInteger;
    ChildClientSP.Parameters.ParamValues['@oc_code'] := Term.FieldByName('oc_code').AsString;
    ChildClientSP.ExecProc;
    Term.Requery;
    Term.Locate('client_id', FClientID,[]);
  except
     On E: Exception do
       ShowMessage(E.Message);
  end;
end;


procedure TCustomerTermForm.ReverseDetailButtonClick(Sender: TObject);
begin
  inherited;
   if ReverseDetailButton.Down then
  begin
    Customer.AfterScroll := nil;
    Term.AfterScroll := TermAfterScroll;
    OrphanedTermsButton.Enabled := False;
    CustomerTermsButton.Enabled := False;
    AllTermsButton.Enabled := False;
    Term.Filter :=  '';
    Term.Filtered := True;
    Term.AfterScroll(Term);
  end else
  begin
    Term.AfterScroll := nil;
    Customer.AfterScroll := CustomerAfterScroll;
    OrphanedTermsButton.Enabled := True;
    CustomerTermsButton.Enabled := True;
    AllTermsButton.Enabled := True;
    CustomerTermsButton.Down := True;
    Customer.Filter := '';
    Customer.Filtered := True;
    Customer.AfterScroll(Customer);
  end;
end;

function TCustomerTermForm.HasRecords(DataSet: TDataSet): Boolean;
begin
  Assert(Assigned(DataSet));
  Result := DataSet.Active and (not DataSet.IsEmpty);
end;



procedure TCustomerTermForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TCustomerTermForm.FormCreate(Sender: TObject);
begin
  inherited;  //QMANTWO-779
  SortGridByFieldNames(TermGrid, ['active', 'oc_code'], [soDescending, soAscending]);
  PeriodTypeCombo.Items.Text :=
  PERIOD_WEEK+CRLF+PERIOD_WEEK_A+CRLF+PERIOD_WEEK_B+CRLF+PERIOD_MONTH+CRLF+PERIOD_MONTH_A+CRLF+PERIOD_MONTH_B+CRLF+PERIOD_FISCALMONTH+CRLF+PERIOD_HALF; //qm-640;
  Term.AfterScroll := nil;
 end;

procedure TCustomerTermForm.ActivatingNow;
begin
  OpenDataSets;
  inherited;
  SetReadOnly(True);
  CustInserted := False;
  TermInserted := False;
  NEInserted := False;
  HPRankingInserted := False;
end;

procedure TCustomerTermForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxCustSearch.Enabled := True;
  ActiveCustomersFilter.Enabled := True;
  chkboxClientSearch.Enabled := True;
  ActiveClientsFilter.Enabled := True;
  ActiveRankingFilter.Enabled := True;    //BP 313
  btnHPRanking.Enabled := True;
  chkboxClientRespondToSearch.Enabled := True;
  PeriodTypeCombo.Enabled := True;
end;

procedure TCustomerTermForm.ActiveClientsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  FSelectedID := Term.FieldByName('client_id').AsInteger;
  Term.Close;
  if ActiveClientsFilter.Checked then
    Term.CommandText := 'select * from client c where c.active = 1 order by c.active desc, c.oc_code asc' else
      Term.CommandText := 'select * from client c order by c.active desc, c.oc_code asc';
  Term.Open;
  Term.Locate('client_id', FSelectedID, []);
end;

procedure TCustomerTermForm.ActiveCustomersFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Customer.FieldByName('customer_id').AsInteger;
  Customer.Close;
  if ActiveCustomersFilter.Checked then
    Customer.CommandText := 'select * from customer where active = 1 order by customer_name' else
      Customer.CommandText := 'select * from customer order by customer_name';
  Customer.Open;
  Customer.Locate('customer_id', FSelectedID, []);
end;

procedure TCustomerTermForm.AllTermsButtonClick(Sender: TObject);
begin
  UpdateTermFilter;
end;

procedure TCustomerTermForm.CustomerAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CustInserted := False;
end;

procedure TCustomerTermForm.CustomerAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,  ATableArray, 'customer', 'customer_id', CustInserted);
end;

procedure TCustomerTermForm.CustomerBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   // get the current values before editing.
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-823
end;

procedure TCustomerTermForm.CustomerAfterScroll(DataSet: TDataSet);
begin
  if not Customer.ControlsDisabled then
    UpdateTermFilter;
end;

procedure TCustomerTermForm.UpdateTermFilter;
begin
  if CustomerTermsButton.Down then
    Term.Filter := 'customer_id = ' + IntToStr(CurrentCustomerID)
  else if OrphanedTermsButton.Down then
    Term.Filter := 'customer_id = null'
  else
    Term.Filter :=  '';

  Term.Filtered := True;
end;

procedure TCustomerTermForm.OpenDataSets;
begin
  Customer.Open;
  ProfitCenter.Open;
  Term.Open;
  OfficeLookup.Open;
  UtilityLookup.Open;
  SGLookup.Open;
  Company.Open;
  DamageCoLookup.open;
  ResponderLookup.Open;
  NotificationEmail.Open;
  ClientInfo.Open;
  EmpTrainer.Open;
  EmpEngineer.Open;
  ClientRespondTo.Open;
  inherited;
end;

procedure TCustomerTermForm.CloseDataSets;
begin
  PostDataSet(Customer);
  PostDataSet(Term);
  Customer.Close;
  ProfitCenter.Close;
  Term.Close;
  OfficeLookup.Close;
  UtilityLookup.Close;
  SGLookup.Close;
  Company.Close;
  DamageCoLookup.close;
  ResponderLookup.Close;
  ClientInfo.Close;
  NotificationEmail.Close;
  EmpTrainer.Close;
  EmpEngineer.Close;
  ClientRespondTo.Close;
  inherited;
end;

function TCustomerTermForm.CurrentCustomerID: Integer;
begin
  Assert(Customer.Active);
  if Customer.IsEmpty then
    Result := -1
  else
    Result := Customer.FieldByName('customer_id').AsInteger;
end;

procedure TCustomerTermForm.TabSheet1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(ClientInfo);
end;

procedure TCustomerTermForm.TermAfterCancel(DataSet: TDataSet);
begin
  inherited;
   TermInserted := False;
end;

procedure TCustomerTermForm.TermAfterInsert(DataSet: TDataSet);
begin
  Term.FieldByName('customer_id').AsInteger := CurrentCustomerID;
end;

procedure TCustomerTermForm.TermAfterPost(DataSet: TDataSet);
begin
  inherited;
  EnableClientInfoControls(True);
  AdminDM.TrackAdminChanges(Dataset, ATermArray, 'client', 'client_id', TermInserted);
end;

procedure TCustomerTermForm.TermBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, ATermArray); //QM-823
end;

procedure TCustomerTermForm.TermAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not Term.ControlsDisabled then
  UpdateCustomerFilter

end;

procedure TCustomerTermForm.UpdateCustomerFilter;
begin
  Customer.Filter := 'customer_id = ' + IntToStr(CurrentTermCustomerID);
  Customer.Filtered := True;
end;

function TCustomerTermForm.CurrentTermCustomerID: Integer;
begin
  Assert(Term.Active);
    if Term.IsEmpty then
    Result := -1
  else
    Result := Term.FieldByName('customer_id').AsInteger;
end;

procedure TCustomerTermForm.TermBeforeOpen(DataSet: TDataSet);
begin
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  AddLookupField('office_name', Term, 'office_id', OfficeLookup, 'office_id', 'office_name');
  Term.FieldByname('office_name').LookupCache := True;

  AddLookupField('utility_name', Term, 'utility_type', UtilityLookup, 'code', 'description');
  Term.FieldByname('utility_name').LookupCache := True;

  AddLookupField('sg_name', Term, 'status_group_id', SGLookup, 'sg_id', 'sg_name', 30);
  Term.FieldByname('sg_name').LookupCache := True;

  AddLookupField('DamageCompany', Term, 'ref_id', DamageCoLookup, 'ref_id', 'DamageCompany');
  Term.FieldByname('DamageCompany').LookupCache := True;

//  function AddLookupField(FieldName: string; DataSet: TDataSet;
//  KeyFields: string; LookupDataSet: TDataSet; LookupKeyFields: string;
//  LookupResultField: string; Size: Integer = 50; Name: string = '';
//  DisplayLabel: string = ''): TStringField;

  Term.FieldByName('unit_conversion_id').OnGetText := TermUnitConversionOnGetText;
  Term.FieldByName('unit_conversion_id').OnSetText := TermUnitConversionOnSetText;
end;

procedure TCustomerTermForm.TermUnitConversionOnGetText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  Index: Integer;
begin
  if Sender.AsInteger < 1 then
    Text := ''
  else begin
    Index := ClientBillingUnitRules.Properties.Items.IndexOfObject(TObject(Sender.AsInteger));
    if Index = -1 then
      Text := ''
    else
      Text := ClientBillingUnitRules.Properties.Items[Index];
  end;
end;

procedure TCustomerTermForm.TermUnitConversionOnSetText(Sender: TField; const Text: string);
var
  Index: Integer;
begin
  Index := ClientBillingUnitRules.Properties.Items.IndexOf(Text);
  if Index < 0 then
    Sender.Clear
  else
    Sender.AsInteger := Integer(ClientBillingUnitRules.Properties.Items.Objects[Index]);
end;

procedure TCustomerTermForm.AssignButtonClick(Sender: TObject);
begin
  AssignSelectedTermToCustomer(CurrentCustomerID);
end;

procedure TCustomerTermForm.AssignSelectedTermToCustomer(CustomerID: Integer);
begin
  if CustomerID = -1 then
    Exit;
  if Term.IsEmpty or Customer.IsEmpty then
    raise Exception.Create('Please select a customer and a term');
  if CustomerID < 1 then
    raise Exception.Create('Invalid customer ID');
  EditDataSet(Term);
  Term.FieldByName('customer_id').AsInteger := CustomerID;
  Term.Post;
end;


procedure TCustomerTermForm.chkboxCustSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxCustSearch.Checked then
    CustomerGridView.Controller.ShowFindPanel
  else
    CustomerGridView.Controller.HideFindPanel;
end;

procedure TCustomerTermForm.chkboxClientRespondToSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxClientRespondToSearch.Checked then
    ClientRespondToView.Controller.ShowFindPanel
  else
    ClientRespondToView.Controller.HideFindPanel;
end;

procedure TCustomerTermForm.chkboxClientSearchClick(Sender: TObject);
begin
  inherited;
    if chkboxClientSearch.Checked then
    TermGridView.Controller.ShowFindPanel
  else
    TermGridView.Controller.HideFindPanel;
end;

procedure TCustomerTermForm.ClientCancelButtonClick(Sender: TObject);
begin
  Term.Cancel;
end;

function TCustomerTermForm.GetIdAt(X, Y:  Integer): Integer;
var
  GridRec: TcxCustomGridRecord;
  HitTest: TcxCustomGridHitTest;
begin
  Result := -1;

  HitTest := CustomerGrid.ViewInfo.GetHitTest(X, Y);
  if HitTest is TcxGridRecordHitTest then begin
    GridRec := TcxGridRecordHitTest(HitTest).GridRecord;
    if Assigned(GridRec) and (GridRec is TcxGridDataRow) then
      Result := GridRec.Values[ColCustID.Index];
  end;
end;

procedure TCustomerTermForm.CustomerGridViewDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  AssignSelectedTermToCustomer(GetIdAt(X, Y));
end;

procedure TCustomerTermForm.CustomerGridViewDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if (Source is TcxDragControlObject) and (not Term.IsEmpty) then
    with TcxDragControlObject(Source) do
      if (Control is TcxGridSite) and (TcxGridSite(Control).GridView = TermGridView) then
        Accept := True;
end;

procedure TCustomerTermForm.CustomerGridViewNavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  inherited;
  if AButtonIndex in [NBDI_INSERT,NBDI_APPEND] then begin
    ADone := True;
    EditCustomerPanel.SetFocus; //panel where edit controls reside
    Customer.Insert;
  end;
end;

procedure TCustomerTermForm.TermGridViewNavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  inherited;
  if AButtonIndex in [NBDI_INSERT,NBDI_APPEND] then begin
    ADone := True;
    TermInfoSheet.SetFocus; //tabsheet where edit controls reside
    Term.Insert;
  end;
end;

procedure TCustomerTermForm.CustomerSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  SetFieldDisplayFormat(Customer, 'contract_review_due_date', 'dddd mmmm d, yyyy');
end;

procedure TCustomerTermForm.DamageCompanyLookupViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;     //    if not VarToBoolean(GridRec.Values[DamageCompanyLookupViewActiveID.Index]) then
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[DamageCompanyLookupViewActiveID.Index]) then
      ACanvas.Brush.Color := $00DDDDDD;
  end;
end;

procedure TCustomerTermForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
  begin
    TermSource.OnDataChange := nil;
    inherited;
    TermSource.OnDataChange := TermSourceDataChange;
  end;
end;

procedure TCustomerTermForm.EnableClientInfoControls(Enabled: Boolean);
begin
  MemoClientRules.Enabled := Enabled;
  MemoConcerns.Enabled := Enabled;
  MemoPlatInfo.Enabled := Enabled;
  edtPlatFile.Enabled := Enabled;
  edtTrainLink.Enabled := Enabled;
  edtPlatsLink.Enabled := Enabled;
  cbTrainer.Enabled := Enabled;
  cbFieldEngineer.Enabled := Enabled;
end;

procedure TCustomerTermForm.DatasetBeforeDelete(DataSet: TDataSet);
var
  Field: TField;
begin
  inherited;
  Field := DataSet.FindField('Active');
  if Assigned(Field) then begin
    DataSet.Edit;
    Field.AsBoolean := False;
    DataSet.Post;
  end;
  Abort;
end;

procedure TCustomerTermForm.TermBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  PostDataSet(Customer);
  EnableClientInfoControls(False);
  TermInserted := True;
end;

procedure TCustomerTermForm.TermBeforePost(DataSet: TDataSet);
var
  s: String;
begin
  inherited;
  //if InsertingDataSet(Term) and Term.FieldByName('active').IsNull then
   // Term.FieldByName('active').AsBoolean := True;
  // Remove leading and trailing whitespace from term id
  Term.FieldByName('oc_code').AsString := Trim(Term.FieldByName('oc_code').AsString);
end;

procedure TCustomerTermForm.CustomerBeforePost(DataSet: TDataSet);
var
 s: String;
begin
  inherited;
  if Customer.FieldByName('locating_company').IsNull then
    raise Exception.Create('Locating Company is a required field.  Use Escape to cancel.');
  if IsEmpty(Customer.FieldByName('contract').AsString) then
    raise Exception.Create('Contract # is a required field.  Use Escape to cancel.');
end;

procedure TCustomerTermForm.NotificationEmailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  NEInserted := False;
end;

procedure TCustomerTermForm.NotificationEmailAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, NotificationEmailsArray, 'notification_email_by_client',
     'notification_email_id', NEInserted);
end;

procedure TCustomerTermForm.NotificationEmailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, NotificationEmailsArray); //QM-823
end;

procedure TCustomerTermForm.NotificationEmailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  NEInserted := True;
end;

procedure TCustomerTermForm.NotificationEmailGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(NotificationEmail);
end;

procedure TCustomerTermForm.CustomerGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TCustomerTermForm.TermGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
 GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[TermGridViewActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TCustomerTermForm.CustomerBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  CustInserted := True;
end;

procedure TCustomerTermForm.CustomerBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  AddLookupField('profit_center_name', Customer, 'pc_code', ProfitCenter, 'pc_code', 'pc_name');
  Customer.FieldByname('profit_center_name').LookupCache := True;
end;

procedure TCustomerTermForm.FormShow(Sender: TObject);
begin
  TermDetailPageControl.ActivePageIndex := 0;
end;

procedure TCustomerTermForm.CustomerNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TCustomerTermForm.TermNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').Value := True;
end;

procedure TCustomerTermForm.NotificationEmailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('client_id').AsInteger := Term.FieldByName('client_id').AsInteger;
end;

procedure TCustomerTermForm.ClientInfoAfterPost(DataSet: TDataSet);
var
 AIns: Boolean;
begin
  inherited;
  AIns := False;
  AdminDM.TrackAdminChanges(DataSet,ClientInfoArray, 'client_info', 'client_info_id', AIns);
end;

procedure TCustomerTermForm.ClientInfoBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, ClientInfoArray); //QM-823
end;

procedure TCustomerTermForm.ClientInfoNewRecord(DataSet: TDataSet);
begin
  inherited;
   DataSet.FieldByName('client_id').AsInteger := Term.FieldByName('client_id').AsInteger;
end;

procedure TCustomerTermForm.ClientRespondToAfterPost(DataSet: TDataSet);
var
  AIns: Boolean;
begin
  inherited;
  AIns := False;
  AdminDM.TrackAdminChanges(DataSet, crtArray, 'client_respond_to', 'crt_id', Ains);;
end;

procedure TCustomerTermForm.ClientRespondToBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, crtArray); //QM-823
end;

procedure TCustomerTermForm.ClientRespondToViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
 GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[CRTViewActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TCustomerTermForm.Client_NameKeyDown(Sender: TObject; var Key: Word; //BP 313
  Shift: TShiftState);
begin
  inherited;
  Key := 0;
end;

procedure TCustomerTermForm.TermSourceDataChange(Sender: TObject; //BP 313
  Field: TField);
begin
  inherited;
  if HPRanking.Active then
  begin
    RefreshRankData;
    HPRanking.Close;
    RankLookup.Close;
    HPRefid_Lookup.Close;

  end;
end;

procedure TCustomerTermForm.HPRankingBeforeEdit(DataSet: TDataSet);  //BP 313
begin
  inherited;
  HPRefID := HPRanking.FieldByName('hp_ref_id').AsInteger;
  HPRank := HPRanking.FieldByName('rank').AsInteger;
  HPActive := HPRanking.FieldByName('active').AsBoolean;
  AdminDM.StoreFieldValues(DataSet, HPRankingArray); //QM-823
end;

procedure TCustomerTermForm.HPRankingBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  HPRankingInserted := True;
end;

procedure TCustomerTermForm.HPRankingBeforePost(DataSet: TDataSet);  //BP 313
var
  s: String;
begin
  inherited;
  try
  If (HPRankingDS.State = dsInsert) or (HPRefID <> HPRanking.FieldByName('hp_ref_id').AsInteger) then
  begin
    With qryDupHPRefid.Parameters do
    begin
      ParamByName('hprefid').Value := HPRanking.FieldByName('hp_ref_id').AsInteger;
      ParamByName('client_id').Value := Term.FieldByName('client_id').AsInteger;
    end;
    qryDupHPRefid.Open;
    if not qryDupHPRefid.EOF then
    begin
      qryDupHPRefid.Close;
      raise Exception.Create(HPRefid_Lookup.FieldByName('description').AsString + ': Duplicate entry not allowed');
    end;
    qryDupHPRefid.Close;
  end;

  HPRankID := HPRanking.FieldByName('hp_util_id').AsInteger;
  ZeroRank := (HPRankingDS.State = dsEdit) and ((HPActive <> HPRanking.FieldByName('active').AsBoolean) and
  (HPRanking.FieldByName('active').AsBoolean = False));

  if (HPRankingDS.State = dsInsert) or (HPRank <> HPRanking.FieldByName('rank').AsInteger) then
  begin
    With qryDupRank.Parameters do
    begin
      ParamByName('client_id').Value := Term.FieldByName('client_id').AsInteger;
      ParamByName('rank').Value := HPRanking.FieldByName('rank').AsInteger;
    end;
    qryDupRank.Open;
    if not qryDupRank.EOF then
    begin
      HPRankID := qryDupRank.FieldByName('hp_util_id').AsInteger;
      ZeroRank := True;
    end;
    qryDupRank.Close;
  end;
  HPRanking.FieldByName('client_id').AsInteger := Term.FieldByName('client_id').AsInteger;
 except
    RefreshRankData;
    raise;
  end;
end;

procedure TCustomerTermForm.HPRankingAfterCancel(DataSet: TDataSet);
begin
  inherited;
  HPRankingInserted := False;
end;

procedure TCustomerTermForm.HPRankingAfterPost(DataSet: TDataSet); //BP 313
begin
  inherited;
  if ZeroRank then
  begin
    ResetRank.Parameters.ParamByName('utilid').Value := HPRankID;
    ResetRank.ExecSQL;
  end;
  RefreshRankData;
  AdminDM.TrackAdminChanges(DataSet, HPRankingArray, 'hp_utils_rank', 'hp_util_id', HPRankingInserted);
end;

procedure TCustomerTermForm.HPRankingNewRecord(DataSet: TDataSet); //BP 313
begin
  inherited;
  HPRanking.FieldByName('utility_code').AsString :=
    Term.FieldByName('utility_type').AsString;
  HPRanking.FieldByName('client_id').AsInteger :=
    Term.FieldByName('client_id').AsInteger;
end;

procedure TCustomerTermForm.HPRankViewCustomDrawCell(  //BP 313
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.FillRect(AViewInfo.Bounds, RootLookAndFeel.Painter.DefaultInactiveColor );
  ACanvas.Font.Color := RootLookAndFeel.Painter.DefaultContentTextColor;
end;

procedure TCustomerTermForm.ActiveRankingFilterClick(Sender: TObject); //BP 313
var
  FSelectedID: Integer;
begin
  FSelectedID :=  HPRanking.FieldByName('hp_util_id').AsInteger;
  HPRanking.Close;
  if ActiveRankingFilter.Checked then
    HPRanking.SQL.Text := 'Select HPR.hp_util_id, HPR.hp_ref_id, HPR.client_id, HPR.utility_code, ' +
    'HPR.rank, R.code, HPR.active, R.[description] from reference r  ' +
    'join hp_utils_rank HPR on (r.ref_id =HPR.hp_ref_id ) ' +
    'where r.type = ''hpmulti'' and HPR.utility_code = :utility_code ' +
    'and HPR.client_id = :client_id and R.active_ind = 1 ' +
    'and HPR.active = 1 order by rank'
   else
    HPRanking.SQL.Text := 'Select HPR.hp_util_id, HPR.hp_ref_id, HPR.client_id, HPR.utility_code,' +
    'HPR.rank, R.code, HPR.active, R.[description] from reference r '  +
    'join hp_utils_rank HPR on (r.ref_id =HPR.hp_ref_id ) '  +
    'where r.type = ''hpmulti'' and HPR.utility_code = :utility_code ' +
    'and HPR.client_id = :client_id and R.active_ind = 1 order by rank';
  RefreshRankData;
  HPRanking.Locate('hp_util_id', FSelectedID, []);
end;

procedure TCustomerTermForm.btnHPRankingClick(Sender: TObject); //BP 313
begin
 RefreshRankData;
end;

procedure TCustomerTermForm.RefreshRankData; //BP 313
var
  clientid: Integer;
  utilcode: String;
begin
  inherited;
  RankLookup.Active := False;
  HPRefid_lookup.Active := False;
  HPRanking.Active :=False;
  clientid :=  Term.FieldByName('client_id').AsInteger;
  Utilcode := Term.FieldByName('utility_type').AsString;
  HPRanking.Parameters.ParamByName('client_id').Value := clientid;
  HPRanking.Parameters.ParamByName('utility_code').Value := utilcode;
  RankLookup.Parameters.ParamByName('client_id').Value := clientid;
  RankLookup.Parameters.ParamByName('utility_code').Value := utilcode;
  RankLookup.Active := True;
  HPRefid_lookup.Active := True;
  HPRanking.Active :=True;
end;


end.

