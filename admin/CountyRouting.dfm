inherited CountyRoutingForm: TCountyRoutingForm
  Left = 210
  Top = 135
  Caption = 'State / County / City Routing'
  ClientHeight = 475
  ClientWidth = 1166
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 574
    Top = 41
    Width = 5
    Height = 434
    ParentCustomHint = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1166
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 846
      Top = 8
      Width = 188
      Height = 31
      AutoSize = False
      Caption = 
        'State/County/Munic. assignments are global, they do not require ' +
        'a map.'
      WordWrap = True
    end
    object Label8: TLabel
      Left = 157
      Top = 16
      Width = 57
      Height = 13
      Caption = 'Filter State:'
    end
    object btnSetCountytoArea: TBitBtn
      Left = 414
      Top = 8
      Width = 158
      Height = 25
      Caption = 'Assign Area to County Area'
      TabOrder = 0
      OnClick = btnSetCountytoAreaClick
    end
    object CopyClipButton: TButton
      Left = 722
      Top = 9
      Width = 106
      Height = 25
      Caption = 'Copy to Clipboard'
      TabOrder = 1
      OnClick = CopyClipButtonClick
    end
    object cbCountyAreaSearch: TCheckBox
      Left = 22
      Top = 16
      Width = 97
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 2
      OnClick = cbCountyAreaSearchClick
    end
    object chkboxAreaSearch: TCheckBox
      Left = 1061
      Top = 11
      Width = 97
      Height = 21
      Caption = 'Show Find Panel'
      TabOrder = 3
      OnClick = chkboxAreaSearchClick
    end
    object cbStateFilter: TComboBox
      Left = 220
      Top = 11
      Width = 65
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      TabOrder = 4
      OnChange = cbStateFilterChange
    end
  end
  object AreaGrid: TcxGrid
    Left = 579
    Top = 41
    Width = 587
    Height = 434
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object AreaGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = True
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = AreaDS
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'area_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object AreaColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 55
      end
      object AreaGridViewColumn1: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Width = 113
      end
      object AreaColMapName: TcxGridDBColumn
        Caption = 'Map Name'
        DataBinding.FieldName = 'map_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 30
        Properties.ReadOnly = True
        SortIndex = 0
        SortOrder = soAscending
        Width = 142
      end
      object AreaColAreaName: TcxGridDBColumn
        Caption = 'Area Name'
        DataBinding.FieldName = 'area_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 40
        Properties.ReadOnly = True
        Width = 142
      end
      object AreaColEmpName: TcxGridDBColumn
        Caption = 'Emp Name'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 137
      end
    end
    object AreaGridLevel: TcxGridLevel
      GridView = AreaGridView
    end
  end
  object CountyGrid: TcxGrid
    Left = 0
    Top = 41
    Width = 574
    Height = 434
    Align = alLeft
    TabOrder = 2
    OnExit = CountyGridExit
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object CountyGridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = CountyAreaDS
      DataController.Filter.Active = True
      DataController.Filter.SupportedLike = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Appending = True
      OptionsData.DeletingConfirmation = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object CountyGridViewstate: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = False
        Width = 45
      end
      object CountyGridViewcounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = False
        Properties.UseLeftAlignmentOnEditing = False
        Width = 102
      end
      object CountyGridViewmunic: TcxGridDBColumn
        Caption = 'Municipality'
        DataBinding.FieldName = 'munic'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = False
        Width = 105
      end
      object CountyGridViewarea_name: TcxGridDBColumn
        Caption = 'Area'
        DataBinding.FieldName = 'area_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 117
      end
      object CountyGridViewshort_name: TcxGridDBColumn
        Caption = 'Locator'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 111
      end
      object CountyGridViewcall_center: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Items.Strings = (
          'NOTHING XGH')
        Width = 79
      end
    end
    object CountyGridLevel: TcxGridLevel
      GridView = CountyGridView
    end
  end
  object CountyArea: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = CountyAreaBeforeInsert
    BeforeEdit = CountyAreaBeforeEdit
    BeforePost = CountyAreaBeforePost
    AfterPost = CountyAreaAfterPost
    AfterCancel = CountyAreaAfterCancel
    BeforeDelete = CountyAreaBeforeDelete
    OnNewRecord = CountyAreaNewRecord
    CommandText = 
      'select county_area.*,'#13#10' area.area_name,'#13#10' employee.short_name,'#13#10 +
      ' area.locator_id '#13#10' from county_area'#13#10' left join area on county_' +
      'area.area_id=area.area_id'#13#10' left join employee on area.locator_i' +
      'd=employee.emp_id'#13#10'order by state, county, munic'#13#10#13#10
    Parameters = <>
    Left = 40
    Top = 176
  end
  object CountyAreaDS: TDataSource
    DataSet = CountyArea
    Left = 40
    Top = 224
  end
  object AreaDS: TDataSource
    DataSet = AreaData
    Left = 120
    Top = 224
  end
  object AreaData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select m.map_name, a.map_id, m.state, m.county, a.area_id, '#13#10'a.a' +
      'rea_name, e.short_name, a.locator_id  '#13#10'from area a inner join m' +
      'ap m on a.map_id=m.map_id '#13#10'left join employee e on a.locator_id' +
      ' = e.emp_id '#13#10'order by a.area_name'
    Parameters = <>
    Left = 120
    Top = 176
    object AreaDatamap_name: TStringField
      FieldName = 'map_name'
      Size = 30
    end
    object AreaDatamap_id: TIntegerField
      FieldName = 'map_id'
    end
    object AreaDatastate: TStringField
      FieldName = 'state'
      Size = 5
    end
    object AreaDatacounty: TStringField
      FieldName = 'county'
      Size = 50
    end
    object AreaDataarea_id: TAutoIncField
      FieldName = 'area_id'
      ReadOnly = True
    end
    object AreaDataarea_name: TStringField
      FieldName = 'area_name'
      Size = 40
    end
    object AreaDatashort_name: TStringField
      FieldName = 'short_name'
      Size = 30
    end
    object AreaDatalocator_id: TIntegerField
      FieldName = 'locator_id'
    end
  end
  object qryLastIns: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <>
    SQL.Strings = (
      'SELECT MAX(County_area_id) from county_area')
    Left = 128
    Top = 112
  end
  object StateList: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'select state from county_area'#13#10'group by state'#13#10'order by state'#13#10
    Parameters = <>
    Left = 40
    Top = 280
  end
  object DelCountyArea: TADOCommand
    CommandText = 'Delete from  county_area where county_area_id = :ca_id'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'ca_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 40
    Top = 112
  end
end
