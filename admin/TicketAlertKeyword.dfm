inherited TicketAlertKeywordForm: TTicketAlertKeywordForm
  Left = 284
  Top = 214
  Caption = 'Ticket Alert Keywords'
  ClientHeight = 445
  ClientWidth = 703
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 421
    Top = 26
    Height = 419
  end
  inherited HeaderPanel: TPanel
    Width = 703
    Height = 26
    object ActiveTicketAlertFilter: TCheckBox
      Left = 16
      Top = 6
      Width = 155
      Height = 14
      Caption = 'Active Ticket Alert Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ActiveTicketAlertFilterClick
    end
  end
  inherited MasterPanel: TPanel
    Top = 26
    Width = 421
    Height = 419
    inherited MasterGrid: TcxGrid
      Width = 405
      Height = 369
      inherited MasterGridView: TcxGridDBTableView
        Navigator.Buttons.Insert.Visible = False
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        OnCustomDrawCell = MasterGridViewCustomDrawCell
        DataController.Filter.Active = True
        DataController.KeyFieldNames = 'alert_ticket_type_id'
        OptionsData.Deleting = False
        object ColMasterAlertTicketTypeID: TcxGridDBColumn
          DataBinding.FieldName = 'alert_ticket_type_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Visible = False
          Options.Filtering = False
          Width = 49
        end
        object ColMasterCallCenter: TcxGridDBColumn
          Caption = 'Call Center'
          DataBinding.FieldName = 'call_center'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownRows = 7
          Properties.MaxLength = 0
          Width = 68
        end
        object ColMasterTicketType: TcxGridDBColumn
          Caption = 'Ticket Type'
          DataBinding.FieldName = 'ticket_type'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Width = 290
        end
        object ColMasterActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taLeftJustify
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = 'True'
          Properties.ValueGrayed = ''
          Properties.ValueUnchecked = 'False'
          MinWidth = 16
          Options.Filtering = False
          Width = 43
        end
      end
    end
  end
  inherited DetailPanel: TPanel
    Left = 430
    Top = 26
    Width = 273
    Height = 419
    inherited DetailGrid: TcxGrid
      Width = 260
      Height = 369
      inherited DetailGridView: TcxGridDBTableView
        Navigator.Buttons.Insert.Visible = False
        DataController.Filter.Active = True
        DataController.KeyFieldNames = 'alert_ticket_keyword_id'
        OptionsSelection.HideSelection = True
        OptionsView.GroupByBox = False
        object ColDetailAlertTicketKeywordID: TcxGridDBColumn
          DataBinding.FieldName = 'alert_ticket_keyword_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Visible = False
          Options.Filtering = False
          Width = 118
        end
        object ColDetailAlertTicketTypeID: TcxGridDBColumn
          DataBinding.FieldName = 'alert_ticket_type_id'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Visible = False
          Options.Filtering = False
          Width = 98
        end
        object ColDetailKeyword: TcxGridDBColumn
          Caption = 'Alert Keyword'
          DataBinding.FieldName = 'keyword'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Width = 261
        end
      end
    end
    inherited DetailNewButton: TButton
      Left = 9
    end
  end
  inherited Master: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = MasterBeforeInsert
    BeforeEdit = MasterBeforeEdit
    AfterPost = MasterAfterPost
    AfterCancel = MasterAfterCancel
    OnNewRecord = MasterNewRecord
    CommandText = 'select * from alert_ticket_type where active = 1'
    Left = 24
    Top = 152
  end
  inherited Detail: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DetailBeforeInsert
    BeforeEdit = DetailBeforeEdit
    AfterPost = DetailAfterPost
    AfterCancel = DetailAfterCancel
    CommandText = 'select * from alert_ticket_keyword'
    IndexFieldNames = 'alert_ticket_type_id'
    MasterFields = 'alert_ticket_type_id'
    Left = 192
    Top = 148
  end
  inherited MasterSource: TDataSource
    Left = 76
    Top = 152
  end
  inherited DetailSource: TDataSource
    Left = 244
    Top = 140
  end
  inherited ActionList: TActionList
    Left = 396
  end
end
