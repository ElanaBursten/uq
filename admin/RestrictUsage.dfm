inherited RestrictUsageForm: TRestrictUsageForm
  Left = 429
  Top = 231
  Caption = 'Restrict Usage Times'
  ClientHeight = 466
  ClientWidth = 658
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 218
    Width = 658
    Height = 7
    Cursor = crVSplit
    Align = alTop
  end
  inherited TopPanel: TPanel
    Width = 658
    object TimeLimitInstructionLabel: TLabel
      Left = 7
      Top = 8
      Width = 369
      Height = 26
      Caption = 
        'Setup times when users granted the Limited Hours right can use Q' +
        ' Manager. To group related periods together, give them the same ' +
        'Group Name.'
      WordWrap = True
    end
  end
  inherited Grid: TcxGrid
    Width = 658
    Height = 177
    Align = alTop
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'right_restriction_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object Gridright_restriction_id: TcxGridDBColumn
        Caption = 'Restriction ID'
        DataBinding.FieldName = 'right_restriction_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 32
      end
      object Gridright_id: TcxGridDBColumn
        Caption = 'Right ID'
        DataBinding.FieldName = 'right_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 32
      end
      object Gridgroup_name: TcxGridDBColumn
        Caption = 'Group Name'
        DataBinding.FieldName = 'group_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 129
      end
      object Gridrestriction_type: TcxGridDBColumn
        Caption = 'Period'
        DataBinding.FieldName = 'restriction_type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 7
        Width = 111
      end
      object Gridstart_value: TcxGridDBColumn
        Caption = 'Start Time'
        DataBinding.FieldName = 'start_value'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.EditMask = '!90:00;1;_'
        Width = 125
      end
      object Gridend_value: TcxGridDBColumn
        Caption = 'EndTime'
        DataBinding.FieldName = 'end_value'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.EditMask = '!90:00;1;_'
        Width = 125
      end
      object Gridactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 45
      end
    end
  end
  object BottomPanel: TPanel [3]
    Left = 0
    Top = 225
    Width = 658
    Height = 31
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object WarningMessageInstructionLabel: TLabel
      Left = 7
      Top = 8
      Width = 352
      Height = 13
      Caption = 
        'Setup messages displayed to the user as their session time nears' +
        ' an end.'
    end
  end
  object MessageGrid: TcxGrid [4]
    Left = 0
    Top = 256
    Width = 658
    Height = 210
    Align = alClient
    TabOrder = 3
    OnExit = MessageGridExit
    LookAndFeel.NativeStyle = True
    object MessageGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = RestrictedUseMessageDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'rum_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.Appending = True
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.DataRowHeight = 34
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object MessageGridrum_id: TcxGridDBColumn
        DataBinding.FieldName = 'rum_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 37
      end
      object MessageGridcountdown_minutes: TcxGridDBColumn
        Caption = 'Minutes Left'
        DataBinding.FieldName = 'countdown_minutes'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 89
      end
      object MessageGridmessage_text: TcxGridDBColumn
        Caption = 'Message Text'
        DataBinding.FieldName = 'message_text'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.ScrollBars = ssVertical
        Width = 419
      end
      object MessageGridactive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 45
      end
    end
    object MessageGridLevel: TcxGridLevel
      GridView = MessageGridView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from right_restriction where right_id=:right_id '
    Parameters = <
      item
        Name = 'right_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
  object RestrictedUseMessage: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = RestrictedUseMessageBeforeInsert
    BeforeEdit = RestrictedUseMessageBeforeEdit
    AfterPost = RestrictedUseMessageAfterPost
    AfterCancel = RestrictedUseMessageAfterCancel
    BeforeDelete = DataBeforeDelete
    OnNewRecord = RestrictedUseMessageNewRecord
    CommandText = 'select * from restricted_use_message'#13#10'order by countdown_minutes'
    Parameters = <>
    Left = 64
    Top = 280
  end
  object RestrictedUseMessageDS: TDataSource
    DataSet = RestrictedUseMessage
    Left = 144
    Top = 296
  end
end
