unit EventViewer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxCalendar,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Data.Win.ADODB,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Vcl.ExtCtrls, AdminDMu,
  Vcl.StdCtrls, cxMemo, cxTextEdit;

type
  TEventViewerForm = class(TBaseCxListForm)
    BottomPanel: TPanel;
    ColEventID: TcxGridDBColumn;
    ColAggrType: TcxGridDBColumn;
    ColAggrID: TcxGridDBColumn;
    ColVerNum: TcxGridDBColumn;
    ColEventData: TcxGridDBColumn;
    ColEventAdded: TcxGridDBColumn;
    EventDetailsMemo: TMemo;
    Splitter: TSplitter;
    StartingEventIdLabel: TLabel;
    EndingEventIDLabel: TLabel;
    StartingEventID: TEdit;
    EndingEventID: TEdit;
    SearchButton: TButton;
    MaxLabel: TLabel;
    GridSummPanel: TPanel;
    EventCntLabel: TLabel;
    procedure GridViewFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure SearchButtonClick(Sender: TObject);
  private
    function DecodeEventData(const EventID: Integer; const EventData: string): string;
  public
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure RefreshNow; override;
  end;

implementation

uses Thrift.Collections, QMThriftUtils, QMLogic2ServiceLib;

{$R *.dfm}

procedure TEventViewerForm.GridViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if Assigned(AFocusedRecord) and AFocusedRecord.IsData then
    EventDetailsMemo.Lines.Text := DecodeEventData(
      AFocusedRecord.Values[ColEventID.Index],
      AFocusedRecord.Values[ColEventData.Index])
  else
    EventDetailsMemo.Lines.Text := 'Select an event in the list to see its details.';
end;

procedure TEventViewerForm.OpenDataSets;
begin
  // Don't call inherited. We don't want to open Data initially.
end;

procedure TEventViewerForm.RefreshNow;
begin
  SearchButton.Click;
end;

procedure TEventViewerForm.SearchButtonClick(Sender: TObject);
begin
  Data.DisableControls;
  try
    Data.Close;
    Data.CommandText := 'select top 5000 * from event_log where event_id >= ' +
      StartingEventID.Text + ' and event_id <= ' + EndingEventID.Text;
    Data.Open;
    EventCntLabel.Caption := 'Count: ' + IntToStr(Data.RecordCount);
  finally
    Data.EnableControls;
  end;
end;

procedure TEventViewerForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  // Don't call inherited. This screen is always read only.
end;

function TEventViewerForm.DecodeEventData(const EventID: Integer;
  const EventData: string): string;

  function LocateListToString(Locates: IThriftList<ILocateSummary>): string;
  var
    I: Integer;
  begin
    Result := '[';
    for I := 0 to Locates.Count-1 do begin
      Result := Result + Locates[I].ToString;
      if I < Locates.Count-1 then
        Result := Result + ','#10;
    end;
    Result := Result + ']';
  end;

var
  Ev: IEvent;
  SB: TStringBuilder;
begin
  Ev := TEventImpl.Create;
  try
    DeserializeFromJSON(EventData, Ev);
    // Ev.ToString is not a good enough event description. Add the EventID and
    // replace generic Locate property text with a detailed list of locates.
    SB := TStringBuilder.Create(Ev.ToString);
    try
      SB.Replace('EventID: ', 'EventID: ' + IntToStr(EventID));
      SB.Replace(',', ',' + #10);
      if Ev.Union.__isset_TicketReceived then
        SB.Replace('TThriftListImpl<QMLogic2ServiceLib.ILocateSummary>',
          LocateListToString(Ev.Union.TicketReceived.LocateList));
      Result := SB.ToString;
    finally
      FreeAndNil(SB);
    end;
  except
    on E: Exception do begin
      Result := 'Unable to deserialize Thrift JSON string [' +
        EventData + ']. Error: ' + E.Message;
    end;
  end;
end;

end.
