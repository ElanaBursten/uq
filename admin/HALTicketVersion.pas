unit HALTicketVersion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, AdminDmu, cxTextEdit, Vcl.StdCtrls,
  Vcl.Samples.Spin, OdDbUtils, MainFu;

type
  TTicketVersionForm = class(TBaseCxListForm)
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Splitter1: TSplitter;
    Label1: TLabel;
    SpinEditCount: TSpinEdit;
    Label2: TLabel;
    SpinEditDateAdd: TSpinEdit;
    btnDateAdd: TButton;
    chkboxTicketVersionsSearch: TCheckBox;
    GridViewticket_id: TcxGridDBColumn;
    GridViewhowmany: TcxGridDBColumn;
    procedure btnDateAddClick(Sender: TObject);
    procedure chkboxTicketVersionsSearchClick(Sender: TObject);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  TicketVersionForm: TTicketVersionForm;

implementation

{$R *.dfm}

{ TTicketVersionForm }

procedure TTicketVersionForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  SetReadOnly(True);
end;

procedure TTicketVersionForm.btnDateAddClick(Sender: TObject);
begin
  inherited;
  CloseDataSets;
  OpenDataSets;
end;

procedure TTicketVersionForm.chkboxTicketVersionsSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxTicketVersionsSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TTicketVersionForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TTicketVersionForm.ColEmailsPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TTicketVersionForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TTicketVersionForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function TTicketVersionForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TTicketVersionForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TTicketVersionForm.OpenDataSets;
begin
  HalConfig.Open;
  Data.Parameters.ParamByName('startdateoffset').Value := - SpinEditDateAdd.Value;
  Data.Parameters.ParamByName('count').Value := SpinEditCount.Value-1;
  inherited;
end;

procedure TTicketVersionForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  chkboxTicketVersionsSearch.Enabled := True;
  btnDateAdd.Enabled := True;
end;

end.
