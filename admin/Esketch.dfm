object frmESketchCodes: TfrmESketchCodes
  Left = 0
  Top = 0
  Caption = 'Output eSketch Code'
  ClientHeight = 370
  ClientWidth = 771
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 24
    Top = 8
    Width = 721
    Height = 283
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsEsketchCodes
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1DBTableView1oc_code: TcxGridDBColumn
        DataBinding.FieldName = 'oc_code'
        DataBinding.IsNullValueType = True
      end
      object cxGrid1DBTableView1customer_description: TcxGridDBColumn
        DataBinding.FieldName = 'customer_description'
        DataBinding.IsNullValueType = True
        Width = 254
      end
      object cxGrid1DBTableView1utility_type: TcxGridDBColumn
        DataBinding.FieldName = 'utility_type'
        DataBinding.IsNullValueType = True
      end
      object cxGrid1DBTableView1customer_name: TcxGridDBColumn
        DataBinding.FieldName = 'customer_name'
        DataBinding.IsNullValueType = True
      end
      object cxGrid1DBTableView1active: TcxGridDBColumn
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object btnExport: TButton
    Left = 296
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Export'
    TabOrder = 1
    OnClick = btnExportClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 351
    Width = 771
    Height = 19
    Panels = <
      item
        Text = 'Filename'
        Width = 60
      end
      item
        Width = 200
      end
      item
        Text = 'Count'
        Width = 60
      end
      item
        Width = 50
      end>
  end
  object qryEsketchCodes: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      ' USE QM'
      ' SELECT DISTINCT'
      '  oc_code'
      '  ,customer.customer_description'
      '  ,client.utility_type'
      '  ,customer.customer_name'
      '  ,client.active'
      ' FROM client'
      
        '  INNER JOIN customer on client.customer_id = customer.customer_' +
        'id'
      
        '   WHERE(client.active = 1 Or (client.active = 0 And client.modi' +
        'fied_date >= getdate() - 30))'
      '    AND customer.customer_name <> '#39'A  Code that is Homeless'#39
      'and  oc_code <> '#39#39
      'and  customer.customer_description <> '#39#39
      'and customer.customer_description like '#39'%|%'#39
      'and  client.utility_type <> '#39#39
      'and  customer.customer_name <> '#39#39
      ' ORDER BY oc_code')
    Left = 56
    Top = 24
  end
  object dsEsketchCodes: TDataSource
    DataSet = qryEsketchCodes
    Left = 56
    Top = 80
  end
  object SaveQMTextFileDlg: TSaveTextFileDialog
    DefaultExt = '*.txt'
    InitialDir = 'C:\QM\Logs'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 72
    Top = 304
  end
end
