unit Reference;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, Grids, DBGrids, ExtCtrls, BaseList,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxDropDownEdit, cxMaskEdit, cxTextEdit, cxSpinEdit, cxCheckBox,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TReferenceForm = class(TBaseCxListForm)
    TypeList: TADODataSet;
    Dataref_id: TAutoIncField;
    Datatype: TStringField;
    Datacode: TStringField;
    Datadescription: TStringField;
    Datasortby: TIntegerField;
    Dataactive_ind: TBooleanField;
    Datamodified_date: TDateTimeField;
    Datamodifier: TStringField;
    DataCodeLength: TIntegerField;
    Gridtype: TcxGridDBColumn;
    Gridcode: TcxGridDBColumn;
    GridCodeLength: TcxGridDBColumn;
    Griddescription: TcxGridDBColumn;
    Gridsortby: TcxGridDBColumn;
    Gridmodifier: TcxGridDBColumn;
    Gridactive_ind: TcxGridDBColumn;
    Gridref_id: TcxGridDBColumn;
    chkboxReferenceSearch: TCheckBox;
    ActiveReferencesFilter: TCheckBox;
    procedure DataBeforePost(DataSet: TDataSet);
    procedure DataCalcFields(DataSet: TDataSet);
    procedure ActiveReferencesFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure chkboxReferenceSearchClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    RefInserted: Boolean;
    procedure PopulateTypeList;
  public
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdCxUtils, OdDbUtils, OdMiscUtils;

{$R *.dfm}

{ TReferenceForm }

procedure TReferenceForm.PopulateTypeList;
begin
  CxComboBoxItems(Gridtype).Clear;
  TypeList.Close;
  TypeList.Open;
  while not TypeList.Eof do begin    //qm-395 BP
    If not TypeList.FieldByName('type').IsNull and     //qm-395 BP
      (Trim(TypeList.FieldValues['type']) <> '') then       //qm-395 BP
        CxComboBoxItems(Gridtype).Add(TypeList.FieldValues['type']);    //qm-395 BP
    TypeList.Next;
  end;
  TypeList.Close;
end;

procedure TReferenceForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkboxReferenceSearch.Enabled := True;
  ActiveReferencesFilter.Enabled := True;
end;

procedure TReferenceForm.ActiveReferencesFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('ref_id').AsInteger;
  Data.Close;
  if ActiveReferencesFilter.Checked then
    Data.CommandText := 'select * from reference where active_ind = 1 order by type, code, description' else
      Data.CommandText := 'select * from reference order by type, code, description';
  Data.Open;
  Data.Locate('ref_id', FSelectedID, []);
end;

procedure TReferenceForm.chkboxReferenceSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxReferenceSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TReferenceForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  RefInserted := False;
end;

procedure TReferenceForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'reference', 'ref_id', RefInserted);
end;

procedure TReferenceForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TReferenceForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  RefInserted := True;
end;

procedure TReferenceForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  {if Data.FieldByName('type').AsString = '' then
    raise Exception.Create('Type is a required field.  Use Escape to cancel.');  }
  if (Data.FieldByName('code').AsString = '') and (Data.FieldByName('description').AsString = '') then
    raise Exception.Create('Either Code or Description must have a value.  Use Escape to cancel.');
end;

procedure TReferenceForm.DataCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('CodeLength').AsInteger :=
    Length(DataSet.FieldByName('Code').AsString);
end;

procedure TReferenceForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldValues['active_ind'] := True;  //qm-395 BP
end;

procedure TReferenceForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TReferenceForm.ActivatingNow;
begin
  inherited;
  RefInserted := False;
end;

procedure TReferenceForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[Gridactive_ind.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TReferenceForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TReferenceForm.PopulateDropdowns;
begin
  inherited;
  PopulateTypeList;
end;

end.
