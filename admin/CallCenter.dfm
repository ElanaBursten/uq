inherited CallCenterForm: TCallCenterForm
  Left = 228
  Top = 176
  Caption = 'Call Centers'
  ClientHeight = 441
  ClientWidth = 862
  Font.Charset = DEFAULT_CHARSET
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 262
    Width = 862
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 862
    Height = 27
    object cbCallCenterSearch: TCheckBox
      Left = 26
      Top = 4
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbCallCenterSearchClick
    end
    object ActiveCallCentersFilter: TCheckBox
      Left = 165
      Top = 4
      Width = 156
      Height = 17
      Caption = 'Active Call Centers Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveCallCentersFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 27
    Width = 862
    Height = 235
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'cc_code'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoSortByDisplayText]
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object ColCallCenterCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'cc_code'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        SortIndex = 0
        SortOrder = soAscending
        Width = 104
      end
      object ColCCName: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'cc_name'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 198
      end
      object ColModifiedDate: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 139
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 65
      end
      object ColAdminEmail: TcxGridDBColumn
        DataBinding.FieldName = 'admin_email'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 189
      end
      object ColMessageEmail: TcxGridDBColumn
        DataBinding.FieldName = 'message_email'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 189
      end
      object ColWarningEmail: TcxGridDBColumn
        DataBinding.FieldName = 'warning_email'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 191
      end
      object ColEmergencyEmail: TcxGridDBColumn
        DataBinding.FieldName = 'emergency_email'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 189
      end
      object ColSummaryEmail: TcxGridDBColumn
        DataBinding.FieldName = 'summary_email'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 191
      end
      object ColAuditMethod: TcxGridDBColumn
        Caption = 'Audit Method'
        DataBinding.FieldName = 'audit_method'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 10
        Width = 139
      end
      object ColAllowedWork: TcxGridDBColumn
        Caption = 'Allowed Work'
        DataBinding.FieldName = 'allowed_work'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'ticket'
          'work_order')
        Width = 100
      end
      object ColUsePrerouting: TcxGridDBColumn
        Caption = 'Use Pre-Routing'
        DataBinding.FieldName = 'use_prerouting'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 117
      end
      object ColTrackArrivals: TcxGridDBColumn
        Caption = 'Track Arrivals'
        DataBinding.FieldName = 'track_arrivals'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.DisplayChecked = '1'
        Properties.DisplayUnchecked = '0'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = 'False'
        MinWidth = 16
        Width = 90
      end
      object ColTimeZone: TcxGridDBColumn
        Caption = 'Time Zone'
        DataBinding.FieldName = 'time_zone_description'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'description'
        Properties.ListColumns = <
          item
            Caption = 'Time Zone'
            FieldName = 'description'
          end>
        Width = 164
      end
    end
  end
  object PC: TPageControl [3]
    Left = 0
    Top = 270
    Width = 862
    Height = 171
    ActivePage = ContactSheet
    Align = alBottom
    TabOrder = 2
    OnChange = PCChange
    object EmailSheet: TTabSheet
      Caption = 'Email Configuration'
      ImageIndex = 1
      DesignSize = (
        854
        143)
      object Label1: TLabel
        Left = 2
        Top = 125
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Responder Email:'
      end
      object Label2: TLabel
        Left = 3
        Top = 148
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Noclient Email:'
      end
      object Label3: TLabel
        Left = 2
        Top = 77
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Emergency Email:'
      end
      object Label4: TLabel
        Left = 2
        Top = 101
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Summary Email:'
      end
      object Label5: TLabel
        Left = 2
        Top = 29
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Message Email:'
      end
      object Label6: TLabel
        Left = 1
        Top = 53
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Warning Email:'
      end
      object Label7: TLabel
        Left = 2
        Top = 4
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Admin Email:'
      end
      object Label11: TLabel
        Left = 2
        Top = 171
        Width = 100
        Height = 13
        AutoSize = False
        Caption = 'Emergency Requeue:'
      end
      object DBEdit1: TDBEdit
        Left = 107
        Top = 122
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'responder_email'
        DataSource = DS
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 108
        Top = 145
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'noclient_email'
        DataSource = DS
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 107
        Top = 98
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'summary_email'
        DataSource = DS
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 107
        Top = 73
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'emergency_email'
        DataSource = DS
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 107
        Top = 50
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'warning_email'
        DataSource = DS
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 107
        Top = 26
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'message_email'
        DataSource = DS
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 107
        Top = 2
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'admin_email'
        DataSource = DS
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 108
        Top = 168
        Width = 747
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        DataField = 'emergency_requeue_email'
        DataSource = DS
        TabOrder = 7
      end
    end
    object HighProfileSheet: TTabSheet
      Caption = 'High Profile Reasons'
      ImageIndex = 1
      object Label8: TLabel
        Left = 8
        Top = 8
        Width = 228
        Height = 89
        AutoSize = False
        Caption = 
          'Select the high profile reasons to make available for this cente' +
          'r.  If none are selected, the old, one-reason-only mechanism wil' +
          'l be used. To adjust the list of reasons here, adjust the '#39'hpmul' +
          'ti'#39' reference data'
        WordWrap = True
      end
      object hpchecklist: TcxCheckListBox
        Left = 248
        Top = 0
        Width = 217
        Height = 106
        Items = <>
        TabOrder = 0
        OnClickCheck = HpCheckListClickCheck
      end
    end
    object ContactSheet: TTabSheet
      Caption = 'Contact Information'
      ImageIndex = 2
      object ContactNotesLabel: TLabel
        Left = 1
        Top = 104
        Width = 32
        Height = 13
        Caption = 'Notes:'
      end
      object ContactGrid: TDBGrid
        Left = 1
        Top = 1
        Width = 625
        Height = 97
        DataSource = ContactsDS
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnExit = ContactGridExit
        Columns = <
          item
            Expanded = False
            FieldName = 'name'
            Title.Caption = 'Name'
            Width = 178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'phone'
            Title.Caption = 'Phone Number'
            Width = 203
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'email'
            Title.Caption = 'Email Address'
            Width = 197
            Visible = True
          end>
      end
      object ContactNotesMemo: TDBMemo
        Left = 35
        Top = 104
        Width = 377
        Height = 57
        DataField = 'notes'
        DataSource = DS
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object AddButton: TButton
        Left = 632
        Top = 4
        Width = 75
        Height = 25
        Action = AddAction
        TabOrder = 2
      end
      object SaveButton: TButton
        Left = 632
        Top = 36
        Width = 75
        Height = 25
        Action = SaveAction
        TabOrder = 3
      end
      object CancelButton: TButton
        Left = 632
        Top = 68
        Width = 75
        Height = 25
        Action = CancelAction
        TabOrder = 4
      end
    end
    object XMLTicketFormatSheet: TTabSheet
      Caption = 'XML Ticket Format'
      ImageIndex = 3
      object Label9: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 848
        Height = 26
        Align = alTop
        AutoSize = False
        Caption = 
          'Enter the XSLT stylesheet definition to specify how XML tickets ' +
          'received from the center should be displayed on screens and repo' +
          'rts. If left blank, the raw XML ticket is displayed. '
        WordWrap = True
      end
      object TicketXsltMemo: TDBMemo
        Left = 0
        Top = 32
        Width = 854
        Height = 111
        Align = alClient
        DataField = 'xml_ticket_format'
        DataSource = DS
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object StatusTranslationsSheet: TTabSheet
      Caption = 'Status Code Translations'
      ImageIndex = 4
      object StatusTranslationsGrid: TcxGrid
        Left = 0
        Top = 0
        Width = 854
        Height = 143
        Align = alClient
        TabOrder = 0
        OnExit = StatusTranslationsGridExit
        object StatusTranslationsGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          Navigator.Visible = True
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = StatusTranslationsDS
          DataController.Filter.MaxValueListCount = 10
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'status_translation_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.MRUItemsList = False
          Filtering.ColumnMRUItemsList = False
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object ColStatusTranslationID: TcxGridDBColumn
            DataBinding.FieldName = 'status_translation_id'
            DataBinding.IsNullValueType = True
            Visible = False
          end
          object ColCCCode: TcxGridDBColumn
            DataBinding.FieldName = 'cc_code'
            DataBinding.IsNullValueType = True
            Visible = False
          end
          object ColStatus: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'status'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsEditFixedList
            Width = 67
          end
          object ColOutgoingStatus: TcxGridDBColumn
            Caption = 'Outgoing Status'
            DataBinding.FieldName = 'outgoing_status'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 135
          end
          object ColTranslationActive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taLeftJustify
            Properties.NullStyle = nssUnchecked
            Properties.ValueChecked = 'True'
            Properties.ValueGrayed = ''
            Properties.ValueUnchecked = 'False'
            MinWidth = 16
            Width = 65
          end
          object ColResponderType: TcxGridDBColumn
            Caption = 'Responder Type'
            DataBinding.FieldName = 'responder_type'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsEditFixedList
            Width = 95
          end
          object ColResponderKind: TcxGridDBColumn
            Caption = 'Responder Kind'
            DataBinding.FieldName = 'responder_kind'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsEditFixedList
            Width = 94
          end
          object ColExplanationCode: TcxGridDBColumn
            Caption = 'Explanation Code'
            DataBinding.FieldName = 'explanation_code'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxTextEditProperties'
            Width = 119
          end
        end
        object StatusTranslationsGridLevel1: TcxGridLevel
          GridView = StatusTranslationsGridDBTableView1
        end
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    AfterScroll = DataAfterScroll
    OnNewRecord = DataNewRecord
    CommandText = 'select * from call_center where active = 1'#13#10
    Left = 32
    Top = 88
  end
  inherited DS: TDataSource
    Left = 72
    Top = 88
  end
  object Reasons: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      '  select reference.code + '#39' - '#39' + reference.description +'#13#10'   ca' +
      'se when chp.active=0 then '#39' (inactive)'#39' else '#39#39' end as reason,'#13#10 +
      '   reference.ref_id as hp_id, chp.active'#13#10'  from reference'#13#10'   l' +
      'eft join call_center_hp chp'#13#10'     on reference.ref_id = chp.hp_i' +
      'd and chp.call_center=:cc'#13#10'  where reference.type='#39'hpmulti'#39#13#10'  o' +
      'rder by reference.sortby'
    Parameters = <
      item
        Name = 'cc'
        Size = -1
        Value = Null
      end>
    Left = 288
    Top = 176
  end
  object UpdateReasons: TADOStoredProc
    Connection = AdminDM.Conn
    ProcedureName = 'update_call_center_hp;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CallCenter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Reasons'
        Attributes = [paNullable]
        DataType = ftString
        Size = 400
        Value = Null
      end>
    Left = 352
    Top = 176
  end
  object ContactsDS: TDataSource
    DataSet = Contacts
    Left = 272
    Top = 24
  end
  object Contacts: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = ContactsBeforeInsert
    BeforeEdit = ContactsBeforeEdit
    BeforePost = DetailsBeforePost
    AfterPost = ContactsAfterPost
    AfterCancel = ContactsAfterCancel
    CommandText = 'select * from call_center_contact'
    Parameters = <>
    Left = 200
    Top = 24
  end
  object CenterActions: TActionList
    Left = 328
    Top = 96
    object AddAction: TDataSetInsert
      Category = 'Dataset'
      Caption = 'Add'
      Enabled = False
      Hint = 'Insert'
      ImageIndex = 4
      DataSource = ContactsDS
    end
    object SaveAction: TDataSetPost
      Category = 'Dataset'
      Caption = 'Save'
      Enabled = False
      Hint = 'Post'
      ImageIndex = 7
      DataSource = ContactsDS
    end
    object CancelAction: TDataSetCancel
      Category = 'Dataset'
      Caption = 'Cancel'
      Enabled = False
      Hint = 'Cancel'
      ImageIndex = 8
      DataSource = ContactsDS
    end
  end
  object TZData: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'SELECT ref_id, code, active_ind,'#13#10'  CASE WHEN active_ind = 0 THE' +
      'N description + '#39' (inactive)'#39' '#13#10'  ELSE description END AS descri' +
      'ption'#13#10'FROM reference '#13#10'WHERE type='#39'timezone'#39#13#10'ORDER BY sortby'
    Parameters = <>
    Left = 328
    Top = 24
  end
  object StatusTranslations: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = StatusTranslationsBeforeInsert
    AfterInsert = StatusTranslationsAfterInsert
    BeforeEdit = StatusTranslationsBeforeEdit
    BeforePost = DetailsBeforePost
    AfterPost = StatusTranslationsAfterPost
    AfterCancel = StatusTranslationsAfterCancel
    BeforeDelete = DataBeforeDelete
    CommandText = 'select * from status_translation'
    Parameters = <>
    Left = 144
    Top = 96
  end
  object StatusTranslationsDS: TDataSource
    DataSet = StatusTranslations
    Left = 224
    Top = 88
  end
end
