inherited EmployeeGroupsForm: TEmployeeGroupsForm
  Left = 371
  Top = 114
  Caption = 'Employee Groups'
  ClientHeight = 590
  ClientWidth = 998
  PixelsPerInch = 96
  TextHeight = 13
  object RightsSplitter: TSplitter [0]
    Left = 0
    Top = 441
    Width = 998
    Height = 9
    Cursor = crVSplit
    Align = alTop
  end
  inherited TopPanel: TPanel
    Width = 998
    Height = 27
    object cbEmpGroupsSearch: TCheckBox
      Left = 26
      Top = 8
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbEmpGroupsSearchClick
    end
    object ActiveEmpGroupsFilter: TCheckBox
      Left = 150
      Top = 7
      Width = 163
      Height = 14
      Caption = 'Active Emp Groups Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveEmpGroupsFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 27
    Width = 998
    Height = 414
    Align = alTop
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'group_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColGroupName: TcxGridDBColumn
        Caption = 'Group'
        DataBinding.FieldName = 'group_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        SortIndex = 0
        SortOrder = soAscending
        Width = 219
      end
      object ColPriority: TcxGridDBColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        HeaderAlignmentHorz = taRightJustify
        Options.ShowEditButtons = isebNever
        Width = 56
      end
      object ColIsDefault: TcxGridDBColumn
        Caption = 'Apply to Everyone'
        DataBinding.FieldName = 'is_default'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Width = 103
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 59
      end
      object ColGroupID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'group_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 53
      end
    end
  end
  object PC: TPageControl [3]
    Left = 0
    Top = 450
    Width = 998
    Height = 140
    ActivePage = RightsSheet
    Align = alClient
    TabOrder = 2
    object RightsSheet: TTabSheet
      Caption = 'Group Rights'
      ImageIndex = 3
      inline RightsFrame: TRightsFrame
        Left = 0
        Top = 0
        Width = 990
        Height = 112
        Align = alClient
        TabOrder = 0
        inherited LimitationHintLabel: TLabel
          Left = 537
          Top = 84
        end
        inherited GrantButton: TButton
          Top = 44
          Action = GrantAction
        end
        inherited RevokeButton: TButton
          Top = 89
          Action = RevokeAction
        end
        inherited RightsGrid: TcxGrid
          Height = 112
          inherited RightsGridDBTableView1: TcxGridDBTableView
            DataController.DataSource = GroupRightsDS
            DataController.Filter.Active = True
            inherited ColGroupRightsGridCategory: TcxGridDBColumn
              Options.Filtering = True
              Width = 21
              IsCaptionAssigned = True
            end
            inherited ColGroupRightsGridRight: TcxGridDBColumn
              Options.Filtering = True
            end
            inherited ColGroupRightsGridAllowed: TcxGridDBColumn
              Options.Filtering = True
              Width = 41
            end
            inherited ColGroupRightsGridLimitation: TcxGridDBColumn
              Options.Filtering = True
              Width = 193
            end
          end
        end
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 
      'select * from group_definition where active = 1  order by group_' +
      'name'
    Left = 16
    Top = 72
  end
  inherited DS: TDataSource
    Left = 16
    Top = 120
  end
  object GroupRightsDS: TDataSource
    DataSet = GroupRights
    Left = 160
    Top = 120
  end
  object InsertRightCommand: TADOCommand
    CommandText = 
      'insert into group_right (group_id, right_id, allowed, limitation' +
      ')'#13#10' values (:group_id, :right_id, :allowed, :limitation )'#13#10
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'group_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'right_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'allowed'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'limitation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 200
        Value = Null
      end>
    Left = 160
    Top = 216
  end
  object UpdateRightCommand: TADOCommand
    CommandText = 
      'update group_right'#13#10' set allowed=:allowed,'#13#10'  limitation=:limita' +
      'tion'#13#10' where group_right_id=:id'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'allowed'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'limitation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 200
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 160
    Top = 168
  end
  object TypeRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference'#13#10' where type='#39'emptype'#39#13#10' order by ref_id'
    Parameters = <>
    Left = 80
    Top = 72
  end
  object TypeRefDS: TDataSource
    DataSet = TypeRef
    Left = 80
    Top = 120
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 784
    Top = 104
    object GrantAction: TAction
      Caption = '&Grant'
      OnExecute = GrantActionExecute
    end
    object RevokeAction: TAction
      Caption = '&Revoke'
      OnExecute = RevokeActionExecute
    end
  end
  object GroupRights: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    AfterScroll = GroupRightsAfterScroll
    CommandText = 
      'select SUBSTRING(rd.right_description, 1, CHARINDEX('#39' - '#39', rd.ri' +
      'ght_description, 1)) as right_category, '#13#10'  rd.right_description' +
      ', rd.right_id, rd.modifier_desc, '#13#10'   gr.group_right_id, gr.allo' +
      'wed, gr.limitation, gr.group_id'#13#10' from right_definition rd'#13#10'  le' +
      'ft join group_right gr'#13#10'    on rd.right_id = gr.right_id  and gr' +
      '.group_id = :group_id'#13#10' where rd.right_id not in (select value f' +
      'rom configuration_data where name like '#39'HiddenGroupID%'#39')'#13#10' order' +
      ' by right_category, rd.right_description, gr.group_id'#13#10#13#10
    DataSource = DS
    EnableBCD = False
    Parameters = <
      item
        Name = 'group_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    Left = 160
    Top = 72
  end
end
