inherited RoutingListForm: TRoutingListForm
  Left = 315
  Top = 163
  Caption = 'Routing List'
  ClientHeight = 517
  ClientWidth = 768
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 768
    Height = 35
    object Button1: TButton
      Left = 0
      Top = 4
      Width = 233
      Height = 25
      Caption = 'Assign Selected Routing to Selected Area'
      TabOrder = 0
      OnClick = Button1Click
    end
    object cbRoutingListSearch: TCheckBox
      Left = 277
      Top = 12
      Width = 99
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 1
      OnClick = cbRoutingListSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 35
    Width = 768
    Height = 258
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Hint = 'Use only for bad entries.  Otherwise set Inactive'
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'routing_list_id'
      Filtering.ColumnFilteredItemsList = True
      OptionsBehavior.NavigatorHints = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        SortIndex = 0
        SortOrder = soAscending
        Width = 88
      end
      object ColFieldName: TcxGridDBColumn
        Caption = 'Field Name'
        DataBinding.FieldName = 'field_name'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        SortIndex = 1
        SortOrder = soAscending
        Width = 146
      end
      object ColFieldValue: TcxGridDBColumn
        Caption = 'Field Value'
        DataBinding.FieldName = 'field_value'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 277
      end
      object ColAreaID: TcxGridDBColumn
        Caption = 'Area'
        DataBinding.FieldName = 'area_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 20
      end
      object ColRoutingListID: TcxGridDBColumn
        DataBinding.FieldName = 'routing_list_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Visible = False
        Options.Filtering = False
        Width = 55
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Emp Name'
        DataBinding.FieldName = 'short_name'
        Options.Editing = False
        Width = 158
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 293
    Width = 768
    Height = 224
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TPanel
      Left = 0
      Top = 0
      Width = 768
      Height = 23
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '    Areas:'
      ParentColor = True
      TabOrder = 0
      object chkboxAreaSearch: TCheckBox
        Left = 109
        Top = 5
        Width = 99
        Height = 12
        Caption = 'Show Find Panel'
        TabOrder = 0
        OnClick = chkboxAreaSearchClick
      end
    end
    object AreaGrid: TcxGrid
      Left = 0
      Top = 23
      Width = 768
      Height = 201
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
      object AreaGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Enabled = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Visible = True
        FilterBox.CustomizeDialog = False
        FilterBox.Position = fpTop
        FindPanel.DisplayMode = fpdmManual
        FindPanel.ShowCloseButton = False
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = AreaDS
        DataController.Filter.Active = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.MRUItemsList = False
        Filtering.ColumnFilteredItemsList = True
        Filtering.ColumnMRUItemsList = False
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object AreaGridViewmap_name: TcxGridDBColumn
          Caption = 'Map Name'
          DataBinding.FieldName = 'map_name'
        end
        object AreaGridViewstate: TcxGridDBColumn
          Caption = 'State'
          DataBinding.FieldName = 'state'
        end
        object AreaGridViewcounty: TcxGridDBColumn
          Caption = 'County'
          DataBinding.FieldName = 'county'
        end
        object AreaGridViewarea_id: TcxGridDBColumn
          Caption = 'Area ID'
          DataBinding.FieldName = 'area_id'
          HeaderAlignmentHorz = taRightJustify
        end
        object AreaGridViewarea_name: TcxGridDBColumn
          Caption = 'Area Name'
          DataBinding.FieldName = 'area_name'
        end
        object AreaGridViewlocator_id: TcxGridDBColumn
          Caption = 'Locator ID'
          DataBinding.FieldName = 'locator_id'
          HeaderAlignmentHorz = taRightJustify
        end
      end
      object AreaGridLevel1: TcxGridLevel
        GridView = AreaGridView
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 
      'select  rl.*,  emp.short_name '#13#10'from routing_list rl'#13#10'left join ' +
      'area ar on ar.area_id = rl.area_id '#13#10'left join employee emp on '#13 +
      #10'emp.emp_id = ar.locator_id'#13#10'order by rl.call_center, rl.field_n' +
      'ame, rl.field_value'
    FieldDefs = <
      item
        Name = 'routing_list_id'
        Attributes = [faReadonly, faFixed]
        DataType = ftAutoInc
      end
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'field_name'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'field_value'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'area_id'
        Attributes = [faFixed]
        DataType = ftInteger
      end
      item
        Name = 'active'
        Attributes = [faFixed]
        DataType = ftBoolean
      end
      item
        Name = 'short_name'
        DataType = ftString
        Size = 30
      end>
    StoreDefs = True
    Left = 40
    Top = 232
    object Datarouting_list_id: TAutoIncField
      FieldName = 'routing_list_id'
      ReadOnly = True
    end
    object Datacall_center: TStringField
      FieldName = 'call_center'
    end
    object Datafield_name: TStringField
      FieldName = 'field_name'
      Size = 25
    end
    object Datafield_value: TStringField
      FieldName = 'field_value'
      Size = 80
    end
    object Dataarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object DataAreaLookup: TStringField
      FieldKind = fkLookup
      FieldName = 'AreaLookup'
      LookupDataSet = Area
      LookupKeyFields = 'area_id'
      LookupResultField = 'area_name'
      KeyFields = 'area_id'
      Lookup = True
    end
    object Datashort_name: TStringField
      FieldName = 'short_name'
      Size = 30
    end
    object Dataactive: TBooleanField
      FieldName = 'active'
    end
  end
  inherited DS: TDataSource
    Left = 80
    Top = 232
  end
  object Area: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select map.map_name, map.state, map.county, '#13#10' area.area_id, are' +
      'a.area_name,'#13#10' area.locator_id'#13#10' from area'#13#10'  inner join map on ' +
      'area.map_id=map.map_id'#13#10'order by area.area_name'
    Parameters = <>
    Left = 120
    Top = 232
    object Areamap_name: TStringField
      FieldName = 'map_name'
      Size = 30
    end
    object Areastate: TStringField
      FieldName = 'state'
      Size = 5
    end
    object Areacounty: TStringField
      FieldName = 'county'
      Size = 50
    end
    object Areaarea_id: TAutoIncField
      FieldName = 'area_id'
      ReadOnly = True
    end
    object Areaarea_name: TStringField
      FieldName = 'area_name'
      Size = 40
    end
    object Arealocator_id: TIntegerField
      FieldName = 'locator_id'
    end
  end
  object AreaDS: TDataSource
    DataSet = Area
    Left = 160
    Top = 232
  end
end
