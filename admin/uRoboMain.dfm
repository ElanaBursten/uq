inherited RoboCopyForm: TRoboCopyForm
  Caption = 'RoboCopy'
  ClientHeight = 643
  ClientWidth = 997
  KeyPreview = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 532
    Width = 997
    Height = 7
    Cursor = crVSplit
    Align = alBottom
  end
  object Splitter1: TSplitter
    Left = 0
    Top = 161
    Width = 997
    Height = 7
    Cursor = crVSplit
    Align = alTop
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 997
    Height = 161
    Align = alTop
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 1
      Top = 25
      Width = 995
      Height = 135
      Align = alClient
      TabOrder = 0
      object CustLocatorView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Insert.Enabled = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Append.Enabled = False
        Navigator.Buttons.Edit.Enabled = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Enabled = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmManual
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCustomDrawCell = CustLocatorViewCustomDrawCell
        DataController.DataSource = dsCustomer
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object CustNameCol: TcxGridDBColumn
          Caption = 'Customer Name'
          DataBinding.FieldName = 'customer_name'
          Width = 159
        end
        object ColEmpName: TcxGridDBColumn
          Caption = 'Emp Name'
          DataBinding.FieldName = 'Name'
          Width = 137
        end
        object ColShortName: TcxGridDBColumn
          Caption = 'Short Name'
          DataBinding.FieldName = 'short_name'
          Width = 106
        end
        object ColRoboSource: TcxGridDBColumn
          DataBinding.FieldName = 'RoBoSource'
          Width = 148
        end
        object ColRoboDest: TcxGridDBColumn
          DataBinding.FieldName = 'RoBoDest'
          Width = 123
        end
        object ColRoboOptions: TcxGridDBColumn
          DataBinding.FieldName = 'RoBoOptions'
          Width = 319
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = CustLocatorView
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 995
      Height = 24
      Align = alTop
      TabOrder = 1
      object chkboxSearch: TCheckBox
        Left = 731
        Top = 1
        Width = 97
        Height = 17
        Caption = 'Show Find Panel'
        TabOrder = 0
        OnClick = chkboxSearchClick
      end
      object btnDupRow: TButton
        Left = 28
        Top = 0
        Width = 135
        Height = 22
        Caption = 'Copy row arguments'
        Default = True
        TabOrder = 1
        OnClick = btnDupRowClick
      end
    end
  end
  object pnlBot: TPanel
    Left = 0
    Top = 539
    Width = 997
    Height = 104
    Align = alBottom
    TabOrder = 1
    object btnCopy: TcxButton
      Left = 754
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Test Copy'
      TabOrder = 0
      OnClick = btnCopyClick
    end
    object btnInsertCopyArrs: TcxButton
      Left = 16
      Top = 53
      Width = 123
      Height = 25
      Caption = 'Insert Copy Values'
      TabOrder = 1
      OnClick = btnInsertCopyArrsClick
    end
    object arrTextEdit: TcxTextEdit
      Left = 83
      Top = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Width = 774
    end
    object cxLabel1: TcxLabel
      Left = 17
      Top = 8
      Caption = 'Arguments:'
      Transparent = True
    end
    object btnUpdateCopyArr: TcxButton
      Left = 342
      Top = 53
      Width = 150
      Height = 25
      Caption = 'Update Copy Values'
      TabOrder = 4
      OnClick = btnUpdateCopyArrClick
    end
    object cboLookupEmpID: TcxLookupComboBox
      Left = 149
      Top = 52
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'emp_id'
      Properties.ListColumns = <
        item
          FieldName = 'Emp  Name'
        end
        item
          FieldName = 'Short Name'
        end>
      Properties.ListSource = dsLookupEmp
      Properties.OnInitPopup = cboLookupEmpIDPropertiesInitPopup
      TabOrder = 5
      Width = 163
    end
    object cboLookupCustID: TcxLookupComboBox
      Left = 149
      Top = 79
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'customer_id'
      Properties.ListColumns = <
        item
          FieldName = 'Cust  Name'
        end>
      Properties.ListSource = dsLookupCust
      TabOrder = 6
      Width = 163
    end
    object cboLookupEmpID2: TcxDBLookupComboBox
      Left = 498
      Top = 54
      DataBinding.DataField = 'loc_emp_id'
      DataBinding.DataSource = dsCustomer
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'emp_id'
      Properties.ListColumns = <
        item
          FieldName = 'Emp  Name'
        end
        item
          FieldName = 'Short Name'
        end>
      Properties.ListOptions.CaseInsensitive = True
      Properties.ListSource = dsLookupEmp
      Properties.OnInitPopup = cboLookupEmpIDPropertiesInitPopup
      TabOrder = 7
      Width = 163
    end
    object cboLookupCustID2: TcxDBLookupComboBox
      Left = 498
      Top = 81
      DataBinding.DataField = 'customer_id'
      DataBinding.DataSource = dsCustomer
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'customer_id'
      Properties.ListColumns = <
        item
          FieldName = 'Cust  Name'
        end>
      Properties.ListSource = dsLookupCust
      TabOrder = 8
      Width = 163
    end
    object cbCopiedArgs: TCheckBox
      Left = 14
      Top = 30
      Width = 129
      Height = 17
      Caption = 'Use Copied Arguments'
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 9
      OnClick = cbCopiedArgsClick
    end
  end
  object MidPanel: TPanel
    Left = 0
    Top = 168
    Width = 997
    Height = 364
    Align = alClient
    TabOrder = 2
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 995
      Height = 362
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Settings'
        ImageIndex = 4
        object SourceDir: TcxTextEdit
          Left = 151
          Top = 15
          Properties.ReadOnly = False
          TabOrder = 0
          OnKeyPress = SourceDirKeyPress
          Width = 642
        end
        object DestDir: TcxTextEdit
          Left = 151
          Top = 42
          Properties.ReadOnly = False
          TabOrder = 1
          OnKeyPress = SourceDirKeyPress
          Width = 642
        end
        object logfolder: TcxTextEdit
          Left = 151
          Top = 85
          Properties.ReadOnly = False
          TabOrder = 4
          Text = 'C:\Plats\log'
          OnKeyPress = SourceDirKeyPress
          Width = 642
        end
        object btnFindSource: TcxButton
          Left = 799
          Top = 11
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 2
          OnClick = btnFindSourceClick
        end
        object btnFindDest: TcxButton
          Left = 799
          Top = 42
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 3
          OnClick = btnFindDestClick
        end
        object btnLogFolder: TcxButton
          Left = 799
          Top = 82
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 5
          OnClick = btnLogFolderClick
        end
        object Label1: TcxLabel
          Left = 16
          Top = 16
          Caption = '<Source>'
          Transparent = True
        end
        object Label2: TcxLabel
          Left = 16
          Top = 48
          Caption = '<Destination>'
          Transparent = True
        end
        object Label3: TcxLabel
          Left = 16
          Top = 88
          Caption = '<Log Folder>'
          Transparent = True
        end
        object cxLabel3: TcxLabel
          Left = 18
          Top = 253
          Caption = 
            'To update or insert to the customer_locator table use Insert Cop' +
            'y Values or Update Copy Values buttons.  Copy parameters, custom' +
            'er and locator selected in the lookups will be inserted or used ' +
            'for updating the row selected in the grid. '
          Style.LookAndFeel.Kind = lfStandard
          Style.LookAndFeel.NativeStyle = True
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.Color = clDefault
          StyleDisabled.LookAndFeel.Kind = lfStandard
          StyleDisabled.LookAndFeel.NativeStyle = True
          StyleDisabled.TextStyle = []
          StyleFocused.LookAndFeel.Kind = lfStandard
          StyleFocused.LookAndFeel.NativeStyle = True
          StyleHot.Color = clMedGray
          StyleHot.LookAndFeel.Kind = lfStandard
          StyleHot.LookAndFeel.NativeStyle = True
          StyleHot.TextColor = clHotLight
          Properties.WordWrap = True
          Transparent = True
          Width = 487
        end
        object cxLabel2: TcxLabel
          Left = 18
          Top = 297
          Caption = 
            'Copy parameters added or deleted are displayed in the Arguments ' +
            'field'
          StyleHot.Color = clMedGray
          StyleHot.TextColor = clHotLight
          Properties.WordWrap = True
          Transparent = True
          Width = 347
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Copy options'
        object cxCheckComboBox1: TcxCheckComboBox
          Left = 137
          Top = 307
          Hint = 
            ' (copyflags : D=Data, A=Attributes, T=Timestamps).  (S=Security=' +
            'NTFS ACLs, O=Owner info, U=aUditing info).'
          ParentShowHint = False
          Properties.Delimiter = ','
          Properties.Items = <
            item
              Description = 'Data'
              ShortDescription = 'D'
            end
            item
              Description = 'Attributes'
              ShortDescription = 'A'
            end
            item
              Description = 'Timestamps'
              ShortDescription = 'T'
            end
            item
              Description = 'Security=NTFS ACLs'
              ShortDescription = 'S'
            end
            item
              Description = 'Owner info'
              ShortDescription = 'O'
            end
            item
              Description = 'aUditing info'
              ShortDescription = 'U'
            end>
          Properties.ReadOnly = False
          Properties.OnPopup = cxCheckComboBox1PropertiesPopup
          ShowHint = True
          TabOrder = 28
          Width = 96
        end
        object CheckBox1: TcxCheckBox
          Left = 24
          Top = 10
          Hint = 'copy Subdirectories, but not empty ones.'
          Caption = '/S'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 0
          OnClick = CheckBox1Click
        end
        object CheckBox2: TcxCheckBox
          Left = 24
          Top = 48
          Hint = 'copy subdirectories, including Empty ones.'
          Caption = '/E'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 1
          OnClick = CheckBox2Click
        end
        object CheckBox3: TcxCheckBox
          Left = 24
          Top = 86
          Hint = 'only copy the top n LEVels of the source directory tree.'
          Caption = '/LEV:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 2
          OnClick = CheckBox3Click
        end
        object CheckBox4: TcxCheckBox
          Left = 24
          Top = 124
          Hint = 'copy files in restartable mode.'
          Caption = '/Z'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 3
          OnClick = CheckBox4Click
        end
        object CheckBox5: TcxCheckBox
          Left = 24
          Top = 162
          Hint = 'copy files in Backup mode.'
          Caption = '/B'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 4
          OnClick = CheckBox5Click
        end
        object CheckBox6: TcxCheckBox
          Left = 24
          Top = 200
          Hint = 'use restartable mode; if access denied use Backup mode.'
          Caption = '/ZB'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 5
          OnClick = CheckBox6Click
        end
        object CheckBox7: TcxCheckBox
          Left = 24
          Top = 310
          Hint = 'what to COPY (default is /COPY:DAT).  '
          Caption = '/COPY:copyflag[s]'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 6
          OnClick = CheckBox7Click
        end
        object CheckBox8: TcxCheckBox
          Left = 24
          Top = 238
          Hint = 'copy files with SECurity (equivalent to /COPY:DATS).'
          Caption = '/SEC'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 7
          OnClick = CheckBox8Click
        end
        object CheckBox9: TcxCheckBox
          Left = 24
          Top = 276
          Hint = 'COPY ALL file info (equivalent to /COPY:DATSOU).'
          Caption = '/COPYALL'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 8
          OnClick = CheckBox9Click
        end
        object CheckBox10: TcxCheckBox
          Left = 257
          Top = 10
          Hint = 'COPY NO file info (useful with /PURGE).'
          Caption = '/NOCOPY'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 9
          OnClick = CheckBox10Click
        end
        object CheckBox11: TcxCheckBox
          Left = 257
          Top = 48
          Hint = 'delete dest files/dirs that no longer exist in source.'
          Caption = '/PURGE '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 10
          OnClick = CheckBox11Click
        end
        object CheckBox12: TcxCheckBox
          Left = 257
          Top = 86
          Hint = 'MIRror a directory tree (equivalent to /E plus /PURGE).'
          Caption = '/MIR '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 11
          OnClick = CheckBox12Click
        end
        object CheckBox13: TcxCheckBox
          Left = 257
          Top = 124
          Hint = 'MOVe files (delete from source after copying).'
          Caption = '/MOV '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 12
          OnClick = CheckBox13Click
        end
        object CheckBox14: TcxCheckBox
          Left = 257
          Top = 162
          Hint = 'MOVE files AND dirs (delete from source after copying).'
          Caption = '/MOVE '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 13
          OnClick = CheckBox14Click
        end
        object CheckBox15: TcxCheckBox
          Left = 257
          Top = 200
          Hint = 'add the given Attributes to copied files.'
          Caption = '/A+:[RASHCNET]'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 14
          OnClick = CheckBox15Click
        end
        object CheckBox16: TcxCheckBox
          Left = 257
          Top = 238
          Hint = 'remove the given Attributes from copied files.'
          Caption = '/A-:[RASCHNET]'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 15
          OnClick = CheckBox16Click
        end
        object CheckBox17: TcxCheckBox
          Left = 257
          Top = 276
          Hint = 'CREATE directory tree and zero-length files only.'
          Caption = '/CREATE '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 16
          OnClick = CheckBox17Click
        end
        object CheckBox18: TcxCheckBox
          Left = 504
          Top = 10
          Hint = 'create destination files using 8.3 FAT file names only.'
          Caption = '/FAT '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 17
          OnClick = CheckBox18Click
        end
        object CheckBox19: TcxCheckBox
          Left = 504
          Top = 48
          Hint = 'assume FAT File Times (2-second granularity).'
          Caption = '/FFT '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 18
          OnClick = CheckBox19Click
        end
        object CheckBox20: TcxCheckBox
          Left = 504
          Top = 86
          Hint = 'turn off very long path (> 256 characters) support.'
          Caption = '/256 '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 19
          OnClick = CheckBox20Click
        end
        object CheckBox21: TcxCheckBox
          Left = 504
          Top = 124
          Hint = 'MONitor source; run again when more than n changes seen.'
          Caption = '/MON:n '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 20
          OnClick = CheckBox21Click
        end
        object CheckBox22: TcxCheckBox
          Left = 504
          Top = 162
          Hint = 'MOnitor source; run again in m minutes Time, if changed.'
          Caption = '/MOT:m '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 21
          OnClick = CheckBox22Click
        end
        object CheckBox23: TcxCheckBox
          Left = 504
          Top = 200
          Hint = 'Run Hours - times when new copies may be started.'
          Caption = '/RH:hhmm-hhmm'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 22
          OnClick = CheckBox23Click
        end
        object CheckBox24: TcxCheckBox
          Left = 504
          Top = 238
          Hint = 'check run hours on a Per File (not per pass) basis.'
          Caption = '/PF '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 23
          OnClick = CheckBox24Click
        end
        object CheckBox25: TcxCheckBox
          Left = 504
          Top = 282
          Hint = 'Inter-Packet Gap (ms), to free bandwidth on slow lines.'
          Caption = '/IPG:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 24
          OnClick = CheckBox25Click
        end
        object edtLev: TcxTextEdit
          Left = 80
          Top = 84
          ParentShowHint = False
          ShowHint = True
          TabOrder = 25
          Width = 41
        end
        object edtnChanges: TcxTextEdit
          Left = 560
          Top = 122
          ParentShowHint = False
          ShowHint = True
          TabOrder = 26
          Width = 41
        end
        object edtnMins: TcxTextEdit
          Left = 560
          Top = 160
          ParentShowHint = False
          ShowHint = True
          TabOrder = 27
          Width = 41
        end
        object edtMSGap: TcxTextEdit
          Left = 559
          Top = 274
          Hint = 'Inter-Packet Gap (ms), to free bandwidth on slow lines.'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 29
          Width = 41
        end
        object cxCheckComboBox2: TcxCheckComboBox
          Left = 365
          Top = 199
          Hint = 
            ' (copyflags : D=Data, A=Attributes, T=Timestamps).  (S=Security=' +
            'NTFS ACLs, O=Owner info, U=aUditing info).'
          ParentShowHint = False
          Properties.Delimiter = ','
          Properties.Items = <
            item
              Description = 'Read only'
              ShortDescription = 'R'
            end
            item
              Description = 'Archive'
              ShortDescription = 'A'
            end
            item
              Description = 'System'
              ShortDescription = 'S'
            end
            item
              Description = 'Hidden'
              ShortDescription = 'H'
            end
            item
              Description = 'Compressed'
              ShortDescription = 'C'
            end
            item
              Description = 'Not Content Indexed'
              ShortDescription = 'N'
            end
            item
              Description = 'Encrypted'
              ShortDescription = 'E'
            end
            item
              Description = 'Temporary'
              ShortDescription = 'T'
            end>
          Properties.ReadOnly = False
          Properties.OnPopup = cxCheckComboBox2PropertiesPopup
          ShowHint = True
          TabOrder = 30
          Width = 96
        end
        object cxCheckComboBox3: TcxCheckComboBox
          Left = 365
          Top = 234
          Hint = 
            ' (copyflags : D=Data, A=Attributes, T=Timestamps).  (S=Security=' +
            'NTFS ACLs, O=Owner info, U=aUditing info).'
          ParentShowHint = False
          Properties.Delimiter = ','
          Properties.Items = <
            item
              Description = 'Read only'
              ShortDescription = 'R'
            end
            item
              Description = 'Archive'
              ShortDescription = 'A'
            end
            item
              Description = 'System'
              ShortDescription = 'S'
            end
            item
              Description = 'Hidden'
              ShortDescription = 'H'
            end
            item
              Description = 'Compressed'
              ShortDescription = 'C'
            end
            item
              Description = 'Not Content Indexed'
              ShortDescription = 'N'
            end
            item
              Description = 'Encrypted'
              ShortDescription = 'E'
            end
            item
              Description = 'Temporary'
              ShortDescription = 'T'
            end>
          Properties.ReadOnly = False
          Properties.OnPopup = cxCheckComboBox3PropertiesPopup
          ShowHint = True
          TabOrder = 31
          Width = 96
        end
        object edtStartTime: TcxTimeEdit
          Left = 616
          Top = 200
          Properties.SpinButtons.Visible = False
          Properties.TimeFormat = tfHourMin
          TabOrder = 32
          Width = 73
        end
        object edtStopTime: TcxTimeEdit
          Left = 695
          Top = 200
          Properties.SpinButtons.Visible = False
          Properties.TimeFormat = tfHourMin
          TabOrder = 33
          Width = 73
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'File Options'
        ImageIndex = 1
        object cxCheckBox1: TcxCheckBox
          Left = 28
          Top = 32
          Hint = 'Copies only files for which the Archive attribute is set.'
          Caption = '/A '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 0
          OnClick = cxCheckBox1Click
        end
        object cxCheckBox2: TcxCheckBox
          Left = 28
          Top = 100
          Hint = 
            'Copies only files for which the Archive attribute is set, and re' +
            'sets the Archive attribute.'
          Caption = '/M '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 1
          OnClick = cxCheckBox2Click
        end
        object cxCheckBox3: TcxCheckBox
          Left = 28
          Top = 168
          Hint = 
            'Includes only files for which any of the specified attributes ar' +
            'e set. The valid values for this option are:'
          Caption = '/IA:[RASHCNETO]'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 2
          OnClick = cxCheckBox3Click
        end
        object cxCheckBox4: TcxCheckBox
          Left = 28
          Top = 236
          Hint = 
            'Excludes files for which any of the specified attributes are set' +
            '. The valid values for this option are:'
          Caption = '/XA:[RASHCNETO]'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 3
          OnClick = cxCheckBox4Click
        end
        object cxCheckBox5: TcxCheckBox
          Left = 263
          Top = 100
          Hint = 
            'Excludes files that match the specified names or paths. Wildcard' +
            ' characters (* and ?) are supported.'
          Caption = '/XF'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 4
          OnClick = cxCheckBox5Click
        end
        object cxCheckBox6: TcxCheckBox
          Left = 263
          Top = 34
          Hint = 'Excludes directories that match the specified names and paths.'
          Caption = '/XD'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 5
          OnClick = cxCheckBox6Click
        end
        object cxCheckBox7: TcxCheckBox
          Left = 30
          Top = 292
          Hint = 
            'Excludes existing files with the same timestamp, but different f' +
            'ile sizes.'
          Caption = '/XC '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 6
          OnClick = cxCheckBox7Click
        end
        object cxCheckBox8: TcxCheckBox
          Left = 263
          Top = 165
          Hint = 
            'Excludes existing files newer than the copy in the source direct' +
            'ory.'
          Caption = '/XN '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 7
          OnClick = cxCheckBox8Click
        end
        object cxCheckBox9: TcxCheckBox
          Left = 263
          Top = 233
          Hint = 
            'Excludes existing files older than the copy in the source direct' +
            'ory.'
          Caption = '/XO '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 8
          OnClick = cxCheckBox9Click
        end
        object cxCheckBox10: TcxCheckBox
          Left = 265
          Top = 301
          Hint = 
            'Excludes extra files and directories present in the destination ' +
            'but not the source. Excluding extra files will not delete files ' +
            'from the destination.'
          Caption = '/XX '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 9
          OnClick = cxCheckBox10Click
        end
        object cxCheckBox11: TcxCheckBox
          Left = 536
          Top = 32
          Hint = 
            'Excludes "lonely" files and directories present in the source bu' +
            't not the destination. Excluding lonely files prevents any new f' +
            'iles from being added to the destination.'
          Caption = '/XL '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 10
          OnClick = cxCheckBox11Click
        end
        object cxCheckBox12: TcxCheckBox
          Left = 536
          Top = 100
          Hint = 
            'Includes the same files. Same files are identical in name, size,' +
            ' times, and all attributes.'
          Caption = '/IS '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 11
          OnClick = cxCheckBox12Click
        end
        object cxCheckBox13: TcxCheckBox
          Left = 536
          Top = 168
          Hint = 
            'Includes "tweaked" files. Tweaked files have the same name, size' +
            ', and times, but different attributes.'
          Caption = '/IT '
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 12
          OnClick = cxCheckBox13Click
        end
        object cxCheckBox14: TcxCheckBox
          Left = 536
          Top = 236
          Hint = 
            'Specifies the maximum file size (to exclude files bigger than n ' +
            'bytes).'
          Caption = '/MAX:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 13
          OnClick = cxCheckBox14Click
        end
        object cxCheckBox15: TcxCheckBox
          Left = 536
          Top = 304
          Hint = 
            'Specifies the minimum file size (to exclude files smaller than n' +
            ' bytes).'
          Caption = ' /MIN:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 14
          OnClick = cxCheckBox15Click
        end
        object cxCheckBox16: TcxCheckBox
          Left = 680
          Top = 32
          Hint = 
            'Specifies the maximum file age (to exclude files older than n da' +
            'ys or date).'
          Caption = '/MAXAGE:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 15
          OnClick = cxCheckBox16Click
        end
        object cxCheckBox17: TcxCheckBox
          Left = 679
          Top = 100
          Hint = 
            'Specifies the minimum file age (exclude files newer than n days ' +
            'or date).'
          Caption = '/MINAGE:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 16
          OnClick = cxCheckBox17Click
        end
        object cxCheckBox18: TcxCheckBox
          Left = 680
          Top = 168
          Hint = 
            'Specifies the maximum last access date (excludes files unused si' +
            'nce n).'
          Caption = '/MAXLAD:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 17
          OnClick = cxCheckBox18Click
        end
        object cxCheckBox19: TcxCheckBox
          Left = 680
          Top = 236
          Hint = 
            'Specifies the minimum last access date (excludes files used sinc' +
            'e n) If n is less than 1900, n specifies the number of days. Oth' +
            'erwise, n specifies a date in the format YYYYMMDD.'
          Caption = '/MINLAD:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 18
          OnClick = cxCheckBox19Click
        end
        object cxCheckBox20: TcxCheckBox
          Left = 680
          Top = 300
          Hint = 
            'Excludes junction points, which are normally included by default' +
            '.'
          Caption = '/XJ'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 19
          OnClick = cxCheckBox20Click
        end
        object edtMaxAge: TcxTextEdit
          Left = 759
          Top = 28
          Hint = 'exclude files older than n days'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 20
          Width = 41
        end
        object edtMinAge: TcxTextEdit
          Left = 759
          Top = 96
          Hint = 'exclude files newer than n days'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 21
          Width = 41
        end
        object edtMaxDays: TcxTextEdit
          Left = 759
          Top = 164
          Hint = 'exclude files unused since n'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 22
          Width = 41
        end
        object edtMinDays: TcxTextEdit
          Left = 759
          Top = 234
          Hint = 'exclude files used since n'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 23
          Width = 41
        end
        object edtMaxBytes: TcxTextEdit
          Left = 588
          Top = 234
          ParentShowHint = False
          ShowHint = True
          TabOrder = 24
          Width = 41
        end
        object edtMinBytes: TcxTextEdit
          Left = 589
          Top = 298
          ParentShowHint = False
          ShowHint = True
          TabOrder = 25
          Width = 41
        end
        object cxCheckComboBox4: TcxCheckComboBox
          Left = 140
          Top = 164
          Hint = 
            ' (copyflags : D=Data, A=Attributes, T=Timestamps).  (S=Security=' +
            'NTFS ACLs, O=Owner info, U=aUditing info).'
          ParentShowHint = False
          Properties.Delimiter = ','
          Properties.Items = <
            item
              Description = 'Read only'
              ShortDescription = 'R'
            end
            item
              Description = 'Archive'
              ShortDescription = 'A'
            end
            item
              Description = 'System'
              ShortDescription = 'S'
            end
            item
              Description = 'Hidden'
              ShortDescription = 'H'
            end
            item
              Description = 'Compressed'
              ShortDescription = 'C'
            end
            item
              Description = 'Not Content Indexed'
              ShortDescription = 'N'
            end
            item
              Description = 'Encrypted'
              ShortDescription = 'E'
            end
            item
              Description = 'Temporary'
              ShortDescription = 'T'
            end
            item
              Description = 'Offline'
              ShortDescription = 'O'
            end>
          Properties.ReadOnly = False
          Properties.OnPopup = cxCheckComboBox4PropertiesPopup
          ShowHint = True
          TabOrder = 26
          Width = 96
        end
        object cxCheckComboBox5: TcxCheckComboBox
          Left = 142
          Top = 234
          Hint = 
            ' (copyflags : D=Data, A=Attributes, T=Timestamps).  (S=Security=' +
            'NTFS ACLs, O=Owner info, U=aUditing info).'
          ParentShowHint = False
          Properties.Delimiter = ','
          Properties.Items = <
            item
              Description = 'Read only'
              ShortDescription = 'R'
            end
            item
              Description = 'Archive'
              ShortDescription = 'A'
            end
            item
              Description = 'System'
              ShortDescription = 'S'
            end
            item
              Description = 'Hidden'
              ShortDescription = 'H'
            end
            item
              Description = 'Compressed'
              ShortDescription = 'C'
            end
            item
              Description = 'Not Content Indexed'
              ShortDescription = 'N'
            end
            item
              Description = 'Encrypted'
              ShortDescription = 'E'
            end
            item
              Description = 'Temporary'
              ShortDescription = 'T'
            end
            item
              Description = 'Offline'
              ShortDescription = 'O'
            end>
          Properties.ReadOnly = False
          Properties.OnPopup = cxCheckComboBox5PropertiesPopup
          ShowHint = True
          TabOrder = 27
          Width = 96
        end
        object SpinEdit1: TSpinEdit
          Left = 354
          Top = 34
          Width = 34
          Height = 22
          Hint = 'number of excluded directories'
          MaxValue = 9
          MinValue = 1
          TabOrder = 28
          Value = 1
        end
        object cxLabel4: TcxLabel
          Left = 304
          Top = 35
          Caption = '# of Dirs'
          StyleHot.Color = clMedGray
          StyleHot.TextColor = clHotLight
          Properties.LabelEffect = cxleExtrude
          Transparent = True
        end
        object btnDirExclusions: TButton
          Left = 393
          Top = 32
          Width = 73
          Height = 25
          Caption = 'Directories'
          TabOrder = 30
          OnClick = btnDirExclusionsClick
        end
        object cxLabel5: TcxLabel
          Left = 300
          Top = 99
          Caption = '# of File Exclusions'
          StyleHot.Color = clMedGray
          StyleHot.TextColor = clHotLight
          Properties.LabelEffect = cxleExtrude
          Transparent = True
        end
        object SpinEdit2: TSpinEdit
          Left = 402
          Top = 97
          Width = 34
          Height = 22
          Hint = 'number of excluded directories'
          MaxValue = 9
          MinValue = 1
          TabOrder = 32
          Value = 1
        end
        object BtnFileExclusions: TButton
          Left = 440
          Top = 94
          Width = 73
          Height = 25
          Caption = 'Files'
          TabOrder = 33
          OnClick = BtnFileExclusionsClick
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Retry Options'
        ImageIndex = 2
        object cxCheckBox21: TcxCheckBox
          Left = 40
          Top = 40
          Hint = 'number of Retries on failed copies: default 1 million.'
          Caption = '/R:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 0
          OnClick = cxCheckBox21Click
        end
        object cxCheckBox22: TcxCheckBox
          Left = 40
          Top = 96
          Hint = 'Wait time between retries: default is 30 seconds.'
          Caption = '/W:n'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 1
          OnClick = cxCheckBox22Click
        end
        object cxCheckBox23: TcxCheckBox
          Left = 158
          Top = 40
          Hint = 'Save /R:n and /W:n in the Registry as default settings.'
          Caption = '/REG'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 2
          OnClick = cxCheckBox23Click
        end
        object cxCheckBox24: TcxCheckBox
          Left = 158
          Top = 96
          Hint = 'wait for sharenames To Be Defined (retry error 67).'
          Caption = '/TBD'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 3
          OnClick = cxCheckBox24Click
        end
        object edtRchanges: TcxTextEdit
          Left = 85
          Top = 37
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Width = 41
        end
        object edtWchanges: TcxTextEdit
          Left = 88
          Top = 93
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Width = 41
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Logging Options'
        ImageIndex = 3
        object cxCheckBox25: TcxCheckBox
          Left = 56
          Top = 24
          Hint = 'List only - don'#39't copy, timestamp or delete any files.'
          Caption = '/L'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 0
          OnClick = cxCheckBox25Click
        end
        object cxCheckBox26: TcxCheckBox
          Left = 56
          Top = 68
          Hint = 'report all eXtra files, not just those selected.'
          Caption = '/X'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 1
          OnClick = cxCheckBox26Click
        end
        object cxCheckBox27: TcxCheckBox
          Left = 56
          Top = 112
          Hint = 'produce Verbose output, showing skipped files.'
          Caption = '/V'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 2
          OnClick = cxCheckBox27Click
        end
        object cxCheckBox28: TcxCheckBox
          Left = 56
          Top = 156
          Hint = 'include source file Time Stamps in the output.'
          Caption = '/TS'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 3
          OnClick = cxCheckBox28Click
        end
        object cxCheckBox29: TcxCheckBox
          Left = 56
          Top = 200
          Hint = 'include Full Pathname of files in the output.'
          Caption = '/FP'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 4
          OnClick = cxCheckBox29Click
        end
        object cxCheckBox30: TcxCheckBox
          Left = 208
          Top = 24
          Hint = 'No Size - don'#39't log file sizes.'
          Caption = '/NS'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 5
          OnClick = cxCheckBox30Click
        end
        object cxCheckBox31: TcxCheckBox
          Left = 208
          Top = 68
          Hint = 'No Class - don'#39't log file classes.'
          Caption = '/NC'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 6
          OnClick = cxCheckBox31Click
        end
        object cxCheckBox32: TcxCheckBox
          Left = 208
          Top = 112
          Hint = 'No File List - don'#39't log file names.'
          Caption = '/NFL'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 7
          OnClick = cxCheckBox32Click
        end
        object cxCheckBox33: TcxCheckBox
          Left = 208
          Top = 156
          Hint = 'No Directory List - don'#39't log directory names.'
          Caption = '/NDL'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 8
          OnClick = cxCheckBox33Click
        end
        object cxCheckBox34: TcxCheckBox
          Left = 208
          Top = 200
          Hint = 'No Progress - don'#39't display % copied.'
          Caption = '/NP'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 9
          OnClick = cxCheckBox34Click
        end
        object cxCheckBox35: TcxCheckBox
          Left = 208
          Top = 240
          Hint = 'show Estimated Time of Arrival of copied files.'
          Caption = '/ETA'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 10
          OnClick = cxCheckBox35Click
        end
        object cxCheckBox36: TcxCheckBox
          Left = 424
          Top = 24
          Hint = 'output status to LOG file (overwrite existing log).'
          Caption = '/LOG:'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 11
          OnClick = cxCheckBox36Click
        end
        object cxCheckBox37: TcxCheckBox
          Left = 424
          Top = 68
          Hint = 'output status to LOG file (append to existing log).'
          Caption = '/LOG+:'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 12
          OnClick = cxCheckBox37Click
        end
        object cxCheckBox38: TcxCheckBox
          Left = 424
          Top = 112
          Hint = 'output to console window, as well as the log file.'
          Caption = '/TEE'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 13
          OnClick = cxCheckBox38Click
        end
        object cxCheckBox39: TcxCheckBox
          Left = 424
          Top = 156
          Hint = 'No Job Header.'
          Caption = '/NJH'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 14
          OnClick = cxCheckBox39Click
        end
        object cxCheckBox40: TcxCheckBox
          Left = 424
          Top = 200
          Hint = 'No Job Summary.'
          Caption = '/NJS'
          ParentShowHint = False
          ShowHint = True
          Style.TransparentBorder = False
          TabOrder = 15
          OnClick = cxCheckBox40Click
        end
        object edtstatuslogoverwrite: TcxTextEdit
          Left = 493
          Top = 24
          ParentShowHint = False
          ShowHint = True
          TabOrder = 16
          OnKeyPress = SourceDirKeyPress
          Width = 140
        end
        object edtstatuslogappend: TcxTextEdit
          Left = 493
          Top = 65
          ParentShowHint = False
          ShowHint = True
          TabOrder = 17
          OnKeyPress = SourceDirKeyPress
          Width = 140
        end
      end
    end
  end
  object dsCustomer: TDataSource
    DataSet = qryCustomer
    OnDataChange = dsCustomerDataChange
    Left = 856
    Top = 229
  end
  object qryCustomer: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = qryCustomerBeforeInsert
    BeforeDelete = qryCustomerBeforeDelete
    Parameters = <>
    SQL.Strings = (
      
        'SELECT cl.customer_locator_id, cl.loc_emp_id, cl.customer_id, c.' +
        'customer_name, '
      'e.last_name + '#39', '#39' + e.first_name as Name, e.short_name,'
      ' RoBoSource, RoBoDest, RoBoOptions, RoBoFiles, RoBoLogs '
      'FROM customer_locator cl'
      'join customer c on c.customer_id = cl.customer_id'
      'join employee e on e.emp_id=cl.loc_emp_id'
      'where  IsNull(e.last_name, '#39#39') <> '#39#39' '
      'and e.active = 1 and type_id <> 1245 '
      'and c.active = 1'
      'order by last_name'
      ''
      ''
      ''
      '')
    Left = 920
    Top = 229
  end
  object insCustomer: TADOQuery
    Connection = AdminDM.Conn
    AfterInsert = insCustomerAfterInsert
    Parameters = <
      item
        Name = 'customer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'loc_emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'robosource'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end
      item
        Name = 'robodest'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end
      item
        Name = 'robooptions'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO customer_locator'
      '           (customer_id,'
      '           loc_emp_id,'
      '           robosource,'
      '           robodest,'
      '           robooptions,'
      '           active)'
      '     VALUES'
      '           (:customer_id,'
      '            :loc_emp_id,'
      '            :robosource,'
      '            :robodest, '
      '            :robooptions, '
      '            1) '
      'select ID = scope_identity()')
    Left = 832
    Top = 405
  end
  object updCustomer: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'loc_emp_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'robosource'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end
      item
        Name = 'robodest'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end
      item
        Name = 'robooptions'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 256
        Value = Null
      end
      item
        Name = 'customer_locator_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE customer_locator'
      ' SET '
      '   loc_emp_id = :loc_emp_id,'
      '   customer_id = :customer_id,'
      '   robosource  = :robosource,'
      '   robodest  = :robodest,'
      '   robooptions  = :robooptions'
      ' WHERE  customer_locator_id = :customer_locator_id'
      ''
      '')
    Left = 912
    Top = 405
  end
  object dsLookupEmp: TDataSource
    DataSet = qryLookupEmp
    Left = 832
    Top = 289
  end
  object qryLookupEmp: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'select emp_id, short_name  as [Short Name], '
      ' last_name + '#39', '#39' + first_name as [Emp  Name]'
      'from employee '
      'where  IsNull(last_name, '#39#39') <> '#39#39' '
      'and active = 1 and type_id <> 1245 '
      'order by last_name')
    Left = 832
    Top = 345
  end
  object qryLookupCust: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select customer_id, customer_name as [Cust  Name] from customer'
      'where active = 1 '
      ' order by customer_name')
    Left = 920
    Top = 345
  end
  object dsLookupCust: TDataSource
    DataSet = qryLookupCust
    Left = 912
    Top = 289
  end
  object dxSkinController1: TdxSkinController
    Left = 909
    Top = 465
  end
end
