inherited UnRoutingForm: TUnRoutingForm
  Left = 270
  Top = 206
  Caption = 'Unrouting of Unassigned'
  ClientWidth = 558
  Font.Charset = ANSI_CHARSET
  OnShow = FormShow
  TextHeight = 13
  object LocatorsGrid: TDBGrid
    Left = 0
    Top = 31
    Width = 558
    Height = 329
    Align = alClient
    DataSource = LocatorsDS
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'emp_id'
        Title.Alignment = taRightJustify
        Title.Caption = 'Emp ID'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'short_name'
        Title.Caption = 'Name'
        Width = 144
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'N'
        Title.Alignment = taRightJustify
        Title.Caption = 'Locates'
        Width = 225
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 558
    Height = 31
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 108
      Height = 13
      Caption = '"Unassigned" locators:'
    end
    object FilterRecordsButton: TButton
      Left = 215
      Top = 6
      Width = 76
      Height = 19
      Caption = 'Filter Name'
      TabOrder = 0
      OnClick = FilterRecordsButtonClick
    end
    object SearchEdit: TEdit
      Left = 297
      Top = 5
      Width = 117
      Height = 21
      TabOrder = 1
    end
    object ClearFilterButton: TButton
      Left = 419
      Top = 7
      Width = 66
      Height = 19
      Caption = 'Clear'
      TabOrder = 2
      OnClick = ClearFilterButtonClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 360
    Width = 558
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TLabel
      Left = 294
      Top = 8
      Width = 261
      Height = 33
      AutoSize = False
      Caption = 
        'This will un-route the open locates for the selected locator, ca' +
        'using the router to re-route them shortly.'
      WordWrap = True
    end
    object btnUnroute: TButton
      Left = 8
      Top = 8
      Width = 120
      Height = 25
      Caption = 'Unroute All Locates'
      TabOrder = 0
      OnClick = btnUnrouteClick
    end
    object btnUnrouteTop500: TButton
      Left = 144
      Top = 8
      Width = 120
      Height = 25
      Caption = 'Unroute 500 Locates'
      TabOrder = 1
      OnClick = btnUnrouteTop500Click
    end
  end
  object Locators: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'declare @unassigned table ('#13#10'  emp_id integer not null,'#13#10'  short' +
      '_name varchar (50)'#13#10')'#13#10#13#10'insert into @unassigned'#13#10'select emp_id,' +
      ' short_name'#13#10'from employee'#13#10'where employee.short_name like '#39'%una' +
      'ssigned%'#39#13#10#13#10'select emp_id, short_name, count(*) as N'#13#10' FROM @un' +
      'assigned una'#13#10' LEFT JOIN locate (NOLOCK) ON emp_id=assigned_to'#13#10 +
      ' GROUP BY emp_id, short_name'#13#10' order by short_name'#13#10
    CommandTimeout = 60
    Parameters = <>
    Left = 112
    Top = 72
  end
  object LocatorsDS: TDataSource
    DataSet = Locators
    Left = 160
    Top = 72
  end
  object Unroute: TADOCommand
    CommandText = 
      'set nocount on'#13#10#13#10'declare @locate_ids table (locate_id int not n' +
      'ull)'#13#10#13#10'/* Transaction handling is in the code */'#13#10#13#10'insert into' +
      ' @locate_ids'#13#10'select locate_id'#13#10' from locate'#13#10'where locate.assig' +
      'ned_to=:id'#13#10#13#10'delete from assignment where locate_id in (select ' +
      'locate_id from @locate_ids)'#13#10#13#10'update locate SET status='#39'-P'#39', cl' +
      'osed=0, assigned_to=null where locate_id in (select locate_id fr' +
      'om @locate_ids)'#13#10
    CommandTimeout = 150
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 256
    Top = 72
  end
  object Unroute500: TADOCommand
    CommandText = 
      'set nocount on'#13#10#13#10'declare @locate_ids table (locate_id int not n' +
      'ull)'#13#10#13#10'/* Transaction handling is in the code */'#13#10#13#10'insert into' +
      ' @locate_ids'#13#10'select top 500 locate_id'#13#10' from locate'#13#10'where loca' +
      'te.assigned_to=:id'#13#10#13#10'delete from assignment where locate_id in ' +
      '(select locate_id from @locate_ids)'#13#10#13#10'update locate SET status=' +
      #39'-P'#39', closed=0, assigned_to=null where locate_id in (select loca' +
      'te_id from @locate_ids)'#13#10
    CommandTimeout = 150
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 320
    Top = 72
  end
end
