inherited TicketAckMatchForm: TTicketAckMatchForm
  Left = 268
  Top = 149
  Caption = 'Emergency Type Tickets'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 24
    object chkboxTicketSearch: TCheckBox
      Left = 25
      Top = 3
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxTicketSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 24
    Height = 376
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColTicketAckMatchID: TcxGridDBColumn
        Caption = 'Ticket Ack Match ID'
        DataBinding.FieldName = 'ticket_ack_match_id'
        Visible = False
      end
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Width = 132
      end
      object ColMatchString: TcxGridDBColumn
        Caption = 'Ticket Type Match'
        DataBinding.FieldName = 'match_string'
        Width = 193
      end
      object ColWorkPriority: TcxGridDBColumn
        Caption = 'Work Priority'
        DataBinding.FieldName = 'work_priority'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownListStyle = lsFixedList
        Properties.View = WorkPriorityLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = WorkPriorityLookupViewdescription
        Width = 120
      end
    end
    object WorkPriorityLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = WorkPriorityRefDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object WorkPriorityLookupViewcode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 74
      end
      object WorkPriorityLookupViewdescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 176
      end
      object WorkPriorityLookupViewref_id: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 59
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = DataBeforeOpen
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 
      'select * from ticket_ack_match'#13#10'order by call_center, match_stri' +
      'ng'#13#10
  end
  inherited DS: TDataSource
    Left = 152
    Top = 120
  end
  object WorkPriorityRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeDelete = DataBeforeDelete
    CommandText = 
      'select * from reference where type = '#39'tkpriority'#39' and active_ind' +
      ' = 1'#13#10#13#10'union'#13#10#13#10'select null, '#39#39', '#39#39', '#39#39', null, null, '#39#39', null'
    Parameters = <>
    Left = 56
    Top = 173
  end
  object WorkPriorityRefDS: TDataSource
    DataSet = WorkPriorityRef
    Left = 152
    Top = 176
  end
end
