inherited CustomerTermForm: TCustomerTermForm
  Left = 206
  Top = 154
  Width = 1232
  AutoScroll = True
  Caption = 'Customers / Terms'
  Position = poDesigned
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HorizontalSplitter: TSplitter
    Left = 427
    Top = 0
    Width = 10
    Height = 400
    Beveled = True
  end
  object CustomerPanel: TPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 400
    Align = alLeft
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 347
      Width = 425
      Height = 7
      Cursor = crVSplit
      Align = alTop
      Color = cl3DLight
      ParentColor = False
    end
    object CustomerListPanel: TPanel
      Left = 1
      Top = 1
      Width = 425
      Height = 346
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 8
      TabOrder = 0
      object CustomerHeaderPanel: TPanel
        Left = 8
        Top = 8
        Width = 409
        Height = 31
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 0
        object CustomersLabel: TLabel
          Left = 0
          Top = 0
          Width = 409
          Height = 25
          Align = alTop
          AutoSize = False
          Caption = '   Customers'
          Color = clGray
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = False
          Layout = tlCenter
        end
        object Label40: TLabel
          Left = 154
          Top = 6
          Width = 78
          Height = 13
          Caption = 'Show Find Panel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSkyBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label41: TLabel
          Left = 291
          Top = 6
          Width = 109
          Height = 13
          Caption = 'Active Customers Only'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSkyBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object chkboxCustSearch: TCheckBox
          Left = 134
          Top = 2
          Width = 16
          Height = 24
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          OnClick = chkboxCustSearchClick
        end
        object ActiveCustomersFilter: TCheckBox
          Left = 266
          Top = 5
          Width = 19
          Height = 17
          Checked = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 1
          OnClick = ActiveCustomersFilterClick
        end
      end
      object CustomerGrid: TcxGrid
        Left = 8
        Top = 39
        Width = 409
        Height = 299
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        LookAndFeel.ScrollbarMode = sbmTouch
        object CustomerGridView: TcxGridDBTableView
          OnDragDrop = CustomerGridViewDragDrop
          OnDragOver = CustomerGridViewDragOver
          Navigator.Buttons.OnButtonClick = CustomerGridViewNavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.InfoPanel.Width = 6
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmManual
          FindPanel.ShowCloseButton = False
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = CustomerGridViewCustomDrawCell
          DataController.DataSource = CustomerSource
          DataController.Filter.MaxValueListCount = 1000
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'customer_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsData.Appending = True
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object ColCustID: TcxGridDBColumn
            Caption = 'Cust ID'
            DataBinding.FieldName = 'customer_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 43
          end
          object ColCustName: TcxGridDBColumn
            Caption = 'Name'
            DataBinding.FieldName = 'customer_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Width = 181
          end
          object ColCustNumber: TcxGridDBColumn
            Caption = 'Number'
            DataBinding.FieldName = 'customer_number'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Width = 106
          end
          object ColCustDescription: TcxGridDBColumn
            DataBinding.FieldName = 'customer_description'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Visible = False
            Options.Filtering = False
            Width = 991
          end
          object ColCustState: TcxGridDBColumn
            Caption = 'State'
            DataBinding.FieldName = 'state'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownRows = 15
            Properties.MaxLength = 0
            Width = 41
          end
          object ColCustActive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taLeftJustify
            Properties.NullStyle = nssUnchecked
            Properties.ValueChecked = 'True'
            Properties.ValueGrayed = ''
            Properties.ValueUnchecked = 'False'
            Visible = False
            MinWidth = 16
            Options.Filtering = False
            Width = 100
          end
          object ColCustNumberPC: TcxGridDBColumn
            Caption = 'Number  PC'
            DataBinding.FieldName = 'customer_number_pc'
            DataBinding.IsNullValueType = True
            Width = 84
          end
          object ColDycomCustNumber: TcxGridDBColumn
            Caption = 'Dycom Cust Nbr'
            DataBinding.FieldName = 'dycom_customer_number'
            DataBinding.IsNullValueType = True
            Width = 84
          end
          object ColActive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxCheckBoxProperties'
            Options.Filtering = False
            Width = 45
          end
        end
        object CustomerGridLevel: TcxGridLevel
          GridView = CustomerGridView
        end
      end
    end
    object EditCustomerPanel: TScrollBox
      Left = 1
      Top = 354
      Width = 425
      Height = 45
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      ParentBackground = True
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 32
        Top = 76
        Width = 57
        Height = 13
        Caption = 'Description:'
        Transparent = True
      end
      object Label2: TLabel
        Left = 60
        Top = 5
        Width = 31
        Height = 13
        Caption = 'Name:'
        Transparent = True
      end
      object Label3: TLabel
        Left = 31
        Top = 100
        Width = 61
        Height = 13
        Caption = 'Customer #:'
        Transparent = True
      end
      object Label4: TLabel
        Left = 47
        Top = 126
        Width = 45
        Height = 13
        Caption = 'Phone #:'
        Transparent = True
      end
      object Label5: TLabel
        Left = 233
        Top = 197
        Width = 30
        Height = 13
        Caption = 'State:'
        Transparent = True
      end
      object Label6: TLabel
        Left = 57
        Top = 149
        Width = 34
        Height = 13
        Caption = 'Street:'
        Transparent = True
      end
      object Label7: TLabel
        Left = 68
        Top = 198
        Width = 23
        Height = 13
        Caption = 'City:'
        Transparent = True
      end
      object Label8: TLabel
        Left = 73
        Top = 222
        Width = 18
        Height = 13
        Caption = 'Zip:'
        Transparent = True
      end
      object Label18: TLabel
        Left = 42
        Top = 318
        Width = 49
        Height = 13
        Caption = 'Company:'
        Transparent = True
      end
      object Label20: TLabel
        Left = 48
        Top = 174
        Width = 43
        Height = 13
        Caption = 'Street 2:'
        Transparent = True
      end
      object Label21: TLabel
        Left = 19
        Top = 245
        Width = 72
        Height = 13
        Caption = 'Contact Name:'
        Transparent = True
      end
      object Label22: TLabel
        Left = 25
        Top = 269
        Width = 69
        Height = 13
        Caption = 'Contact Email:'
        Transparent = True
      end
      object Label23: TLabel
        Left = 42
        Top = 294
        Width = 49
        Height = 13
        Caption = 'Attention:'
        Transparent = True
      end
      object Label24: TLabel
        Left = 29
        Top = 342
        Width = 63
        Height = 13
        Caption = 'Billing Period:'
        Transparent = True
      end
      object Label25: TLabel
        Left = 27
        Top = 368
        Width = 66
        Height = 13
        Caption = 'Profit Center:'
        Transparent = True
      end
      object Label27: TLabel
        Left = 34
        Top = 28
        Width = 57
        Height = 13
        Caption = 'Contract #:'
        Transparent = True
      end
      object Label37: TLabel
        Left = 7
        Top = 390
        Width = 86
        Height = 13
        Caption = 'Customer Nbr PC:'
        Transparent = True
      end
      object Label38: TLabel
        Left = 26
        Top = 52
        Width = 65
        Height = 13
        Caption = 'Damage Cap:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label39: TLabel
        Left = 10
        Top = 414
        Width = 81
        Height = 13
        Caption = 'Dycom Cust Nbr:'
        Transparent = True
      end
      object lblDyCustName: TLabel
        Left = 0
        Top = 437
        Width = 91
        Height = 13
        Caption = 'Dycom Cust Name:'
        Transparent = True
      end
      object CustDesc: TDBEdit
        Left = 102
        Top = 73
        Width = 232
        Height = 21
        DataField = 'customer_description'
        DataSource = CustomerSource
        TabOrder = 2
      end
      object CustName: TDBEdit
        Left = 102
        Top = 2
        Width = 232
        Height = 21
        DataField = 'customer_name'
        DataSource = CustomerSource
        TabOrder = 0
      end
      object CustNum: TDBEdit
        Left = 102
        Top = 97
        Width = 169
        Height = 21
        DataField = 'customer_number'
        DataSource = CustomerSource
        TabOrder = 3
      end
      object CustPhone: TDBEdit
        Left = 102
        Top = 122
        Width = 121
        Height = 21
        DataField = 'phone'
        DataSource = CustomerSource
        TabOrder = 4
      end
      object CustStreet: TDBEdit
        Left = 102
        Top = 146
        Width = 233
        Height = 21
        DataField = 'street'
        DataSource = CustomerSource
        TabOrder = 5
      end
      object CustZip: TDBEdit
        Left = 102
        Top = 219
        Width = 121
        Height = 21
        DataField = 'zip'
        DataSource = CustomerSource
        TabOrder = 6
      end
      object CustState: TDBComboBox
        Left = 269
        Top = 195
        Width = 67
        Height = 21
        DataField = 'state'
        DataSource = CustomerSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 8
      end
      object CustCity: TDBEdit
        Left = 102
        Top = 195
        Width = 121
        Height = 21
        DataField = 'city'
        DataSource = CustomerSource
        TabOrder = 9
      end
      object LocatingCompany: TDBLookupComboBox
        Left = 98
        Top = 314
        Width = 233
        Height = 21
        DataField = 'locating_company'
        DataSource = CustomerSource
        KeyField = 'company_id'
        ListField = 'name'
        ListSource = CompanyDS
        TabOrder = 10
      end
      object CustStreet2: TDBEdit
        Left = 101
        Top = 171
        Width = 233
        Height = 21
        DataField = 'street_2'
        DataSource = CustomerSource
        TabOrder = 7
      end
      object CustContactName: TDBEdit
        Left = 101
        Top = 243
        Width = 233
        Height = 21
        DataField = 'contact_name'
        DataSource = CustomerSource
        TabOrder = 11
      end
      object CustContactEmail: TDBEdit
        Left = 100
        Top = 266
        Width = 233
        Height = 21
        DataField = 'contact_email'
        DataSource = CustomerSource
        TabOrder = 12
      end
      object CustAttention: TDBEdit
        Left = 100
        Top = 290
        Width = 233
        Height = 21
        DataField = 'attention'
        DataSource = CustomerSource
        TabOrder = 13
      end
      object CustomerProfitCenter: TDBLookupComboBox
        Left = 99
        Top = 364
        Width = 145
        Height = 21
        DataField = 'pc_code'
        DataSource = CustomerSource
        DropDownRows = 15
        KeyField = 'pc_code'
        ListField = 'pc_name'
        ListSource = ProfitCenterSource
        TabOrder = 14
      end
      object CustContractNum: TDBEdit
        Left = 102
        Top = 25
        Width = 121
        Height = 21
        DataField = 'contract'
        DataSource = CustomerSource
        TabOrder = 1
      end
      object CustomerNumberPC: TDBEdit
        Left = 98
        Top = 387
        Width = 233
        Height = 21
        DataField = 'customer_number_pc'
        DataSource = CustomerSource
        TabOrder = 15
      end
      object CustDamageCap: TDBEdit
        Left = 102
        Top = 49
        Width = 121
        Height = 21
        DataField = 'damage_cap'
        DataSource = CustomerSource
        TabOrder = 16
      end
      object CustActiveCheckbox: TDBCheckBox
        Left = 268
        Top = 339
        Width = 65
        Height = 17
        Caption = 'Active'
        DataField = 'active'
        DataSource = CustomerSource
        TabOrder = 17
      end
      object PeriodTypeCombo: TDBComboBox
        Left = 100
        Top = 339
        Width = 145
        Height = 21
        Style = csDropDownList
        AutoDropDown = True
        DataField = 'period_type'
        DataSource = CustomerSource
        TabOrder = 18
      end
      object DycomCustNumber: TDBEdit
        Left = 97
        Top = 411
        Width = 233
        Height = 21
        DataField = 'dycom_customer_number'
        DataSource = CustomerSource
        TabOrder = 19
      end
      object DycomCustName: TDBEdit
        Left = 97
        Top = 434
        Width = 233
        Height = 21
        DataField = 'dycom_customer_name'
        DataSource = CustomerSource
        TabOrder = 20
      end
    end
  end
  object TermPanel: TPanel
    Left = 437
    Top = 0
    Width = 779
    Height = 400
    Align = alClient
    TabOrder = 1
    object TermListPanel: TPanel
      Left = 1
      Top = 1
      Width = 777
      Height = 398
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 8
      TabOrder = 0
      object VerticalSplitter: TSplitter
        Left = 8
        Top = 223
        Width = 761
        Height = 5
        Cursor = crVSplit
        Align = alBottom
      end
      object TermHeaderPanel: TPanel
        Left = 8
        Top = 8
        Width = 761
        Height = 34
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object AllTermsButton: TSpeedButton
          Tag = 1
          Left = 116
          Top = 1
          Width = 113
          Height = 23
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'All Terms'
          OnClick = AllTermsButtonClick
        end
        object CustomerTermsButton: TSpeedButton
          Tag = 1
          Left = 0
          Top = 1
          Width = 113
          Height = 23
          AllowAllUp = True
          GroupIndex = 1
          Down = True
          Caption = 'Customer Terms'
          OnClick = AllTermsButtonClick
        end
        object OrphanedTermsButton: TSpeedButton
          Tag = 1
          Left = 232
          Top = 2
          Width = 113
          Height = 23
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'Orphaned Terms'
          OnClick = AllTermsButtonClick
        end
        object ReverseDetailButton: TSpeedButton
          Tag = 1
          Left = 349
          Top = 2
          Width = 99
          Height = 23
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'Reverse Detail'
          OnClick = ReverseDetailButtonClick
        end
        object chkboxClientSearch: TCheckBox
          Left = 468
          Top = -1
          Width = 101
          Height = 16
          Caption = 'Show Find Panel'
          TabOrder = 0
          OnClick = chkboxClientSearchClick
        end
        object ActiveClientsFilter: TCheckBox
          Left = 468
          Top = 15
          Width = 128
          Height = 17
          Caption = 'Active Clients Only'
          Checked = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 1
          OnClick = ActiveClientsFilterClick
        end
      end
      object TermGrid: TcxGrid
        Left = 8
        Top = 42
        Width = 761
        Height = 181
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        object TermGridView: TcxGridDBTableView
          DragMode = dmAutomatic
          Navigator.Buttons.OnButtonClick = TermGridViewNavigatorButtonsButtonClick
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = True
          Navigator.Buttons.PriorPage.Visible = True
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.Next.Visible = True
          Navigator.Buttons.NextPage.Visible = True
          Navigator.Buttons.Last.Visible = True
          Navigator.Buttons.Append.Visible = True
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = True
          Navigator.Buttons.GotoBookmark.Visible = True
          Navigator.Buttons.Filter.Visible = True
          Navigator.Visible = True
          FindPanel.DisplayMode = fpdmManual
          FindPanel.ShowCloseButton = False
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = TermGridViewCustomDrawCell
          DataController.DataSource = TermSource
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'client_id'
          DataController.Options = [dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object TermGridViewClientID: TcxGridDBColumn
            Caption = 'Client ID'
            DataBinding.FieldName = 'client_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 49
          end
          object TermGridViewOCCode: TcxGridDBColumn
            Caption = 'Term'
            DataBinding.FieldName = 'oc_code'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            SortIndex = 1
            SortOrder = soAscending
            Width = 82
          end
          object TermGridViewClientName: TcxGridDBColumn
            Caption = 'Name'
            DataBinding.FieldName = 'client_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Width = 96
          end
          object TermGridViewActive: TcxGridDBColumn
            Caption = 'Active'
            DataBinding.FieldName = 'active'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taLeftJustify
            Properties.NullStyle = nssUnchecked
            Properties.ValueChecked = 'True'
            Properties.ValueGrayed = ''
            Properties.ValueUnchecked = 'False'
            MinWidth = 16
            Options.Filtering = False
            SortIndex = 0
            SortOrder = soDescending
            Width = 16
          end
          object TermGridViewCallCenter: TcxGridDBColumn
            Caption = 'Call Center'
            DataBinding.FieldName = 'call_center'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownRows = 15
            Width = 106
          end
          object TermGridViewCustomerID: TcxGridDBColumn
            Caption = 'Cust ID'
            DataBinding.FieldName = 'Customer_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Visible = False
            Options.Filtering = False
            Width = 80
          end
          object TermGridViewUtilityName: TcxGridDBColumn
            Caption = 'Utility'
            DataBinding.FieldName = 'utility_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.DropDownAutoSize = True
            Properties.View = UtilityLookupView
            Properties.KeyFieldNames = 'description'
            Properties.ListFieldItem = UtilityLookupViewDescription
            MinWidth = 126
            Options.HorzSizing = False
            Width = 126
          end
          object TermGridViewRefID: TcxGridDBColumn
            DataBinding.FieldName = 'ref_id'
            DataBinding.IsNullValueType = True
            Visible = False
            Options.Grouping = False
            Width = 30
          end
          object TermGridViewResponder: TcxGridDBColumn
            Caption = 'Responder'
            DataBinding.FieldName = 'RefID_Responder'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.DropDownAutoSize = True
            Properties.View = ResponderLookupView
            Properties.KeyFieldNames = 'ref_id'
            Properties.ListFieldItem = ResponderLookupViewCode
            Options.Grouping = False
            Width = 91
          end
          object TermGridViewDamageCompany: TcxGridDBColumn
            Caption = 'Damage Co'
            DataBinding.FieldName = 'DamageCompany'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.DropDownAutoSize = True
            Properties.View = DamageCompanyLookupView
            Properties.KeyFieldNames = 'ref_id'
            Properties.ListFieldItem = DamageCompanyLookupViewDamageCo
            MinWidth = 126
            Options.FilteringPopupIncrementalFiltering = True
            Width = 136
          end
          object TermGridViewBigFoot: TcxGridDBColumn
            Caption = 'BigFoot'
            DataBinding.FieldName = 'bigfoot'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taLeftJustify
            Properties.NullStyle = nssUnchecked
            Properties.UseAlignmentWhenInplace = True
            SortIndex = 2
            SortOrder = soDescending
            Width = 58
          end
        end
        object OfficeLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = OfficeLookupSource
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'office_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object OfficeLookupViewOfficeID: TcxGridDBColumn
            Caption = 'Office ID'
            DataBinding.FieldName = 'office_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Visible = False
            Options.Filtering = False
            Width = 114
          end
          object OfficeLookupViewOfficeName: TcxGridDBColumn
            Caption = 'Office'
            DataBinding.FieldName = 'office_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Options.Filtering = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 150
          end
          object OfficeLookupViewProfitCenter: TcxGridDBColumn
            Caption = 'Profit Center'
            DataBinding.FieldName = 'profit_center'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 85
          end
        end
        object UtilityLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = UtilityLookupSource
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'description'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GridLines = glNone
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.Header = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object UtilityLookupViewDescription: TcxGridDBColumn
            DataBinding.FieldName = 'description'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Options.Filtering = False
            Width = 120
          end
        end
        object StatusGroupLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = SGLookupDS
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'sg_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GridLines = glNone
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.Header = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object StatusGroupLookupViewSGName: TcxGridDBColumn
            Caption = 'Status Group'
            DataBinding.FieldName = 'sg_name'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.ReadOnly = True
            MinWidth = 150
            Options.Filtering = False
            Width = 150
          end
        end
        object BillingUnitConversionLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = UnitConversionDS
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'unit_conversion_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.Header = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object BillingUnitConversionLookupViewDescription: TcxGridDBColumn
            DataBinding.FieldName = 'Description'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Options.Filtering = False
            Width = 59
          end
          object BillingUnitConversionLookupViewUnitConversionID: TcxGridDBColumn
            DataBinding.FieldName = 'unit_conversion_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Visible = False
            Options.Filtering = False
            Width = 96
          end
        end
        object DamageCompanyLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          OnCustomDrawCell = DamageCompanyLookupViewCustomDrawCell
          DataController.DataSource = DamageCoLookupSource
          DataController.DetailKeyFieldNames = 'ref_id'
          DataController.KeyFieldNames = 'ref_id'
          DataController.Options = [dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = DamageCompanyLookupViewDamageCo
          OptionsCustomize.ColumnGrouping = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GridLines = glNone
          OptionsView.GroupByBox = False
          OptionsView.Header = False
          object DamageCompanyLookupViewDamageCo: TcxGridDBColumn
            DataBinding.FieldName = 'DamageCompany'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Width = 176
          end
          object DamageCompanyLookupViewRefID: TcxGridDBColumn
            DataBinding.FieldName = 'ref_id'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Visible = False
          end
          object DamageCompanyLookupViewActiveID: TcxGridDBColumn
            DataBinding.FieldName = 'active_ind'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
            Visible = False
          end
        end
        object ResponderLookupView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = ResponderLookupDS
          DataController.DetailKeyFieldNames = 'ref_id'
          DataController.KeyFieldNames = 'ref_id'
          DataController.Options = [dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = ResponderLookupViewCode
          OptionsCustomize.ColumnGrouping = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          OptionsView.Header = False
          object ResponderLookupViewCode: TcxGridDBColumn
            DataBinding.FieldName = 'code'
            DataBinding.IsNullValueType = True
            PropertiesClassName = 'TcxMaskEditProperties'
          end
          object ResponderLookupViewRefID: TcxGridDBColumn
            DataBinding.FieldName = 'ref_id'
            DataBinding.IsNullValueType = True
            Visible = False
          end
        end
        object TermGridLevel: TcxGridLevel
          GridView = TermGridView
        end
      end
      object TermDetailPageControl: TPageControl
        Left = 8
        Top = 228
        Width = 761
        Height = 162
        ActivePage = TermInfoSheet
        Align = alBottom
        TabOrder = 2
        object TermInfoSheet: TTabSheet
          Caption = 'Term Information'
          object Label9: TLabel
            Left = 3
            Top = 11
            Width = 31
            Height = 13
            Caption = 'Name:'
            FocusControl = ClientName
          end
          object Label10: TLabel
            Left = 3
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Term:'
            FocusControl = ClientOCCode
          end
          object Label11: TLabel
            Left = 3
            Top = 59
            Width = 33
            Height = 13
            Caption = 'Office:'
          end
          object Label12: TLabel
            Left = 272
            Top = 59
            Width = 55
            Height = 13
            Caption = 'Call center:'
          end
          object Label13: TLabel
            Left = 3
            Top = 83
            Width = 73
            Height = 13
            Caption = 'Update center:'
            WordWrap = True
          end
          object Label14: TLabel
            Left = 3
            Top = 131
            Width = 50
            Height = 13
            Caption = 'Grid email:'
            FocusControl = ClientGridEmail
          end
          object Label15: TLabel
            Left = 272
            Top = 35
            Width = 31
            Height = 13
            Caption = 'Utility:'
          end
          object Label17: TLabel
            Left = 272
            Top = 83
            Width = 55
            Height = 13
            Caption = 'Status Grp:'
          end
          object Label19: TLabel
            Left = 3
            Top = 108
            Width = 72
            Height = 13
            Caption = 'Billing unit rule:'
            WordWrap = True
          end
          object Label26: TLabel
            Left = 3
            Top = 154
            Width = 82
            Height = 13
            Caption = 'Attachment URL:'
            FocusControl = ClientAttachmentURL
          end
          object Label28: TLabel
            Left = 271
            Top = 104
            Width = 59
            Height = 13
            Caption = 'Damage Co:'
          end
          object ClientName: TDBEdit
            Left = 85
            Top = 6
            Width = 572
            Height = 21
            DataField = 'client_name'
            DataSource = TermSource
            TabOrder = 0
          end
          object ClientOCCode: TDBEdit
            Left = 85
            Top = 28
            Width = 178
            Height = 21
            DataField = 'oc_code'
            DataSource = TermSource
            TabOrder = 1
          end
          object ClientActive: TDBCheckBox
            Left = 85
            Top = 172
            Width = 59
            Height = 17
            Caption = 'Active'
            DataField = 'active'
            DataSource = TermSource
            ReadOnly = True
            TabOrder = 11
          end
          object ClientGridEmail: TDBEdit
            Left = 85
            Top = 127
            Width = 476
            Height = 21
            DataField = 'grid_email'
            DataSource = TermSource
            TabOrder = 9
          end
          object ClientAllowEditLocates: TDBCheckBox
            Left = 152
            Top = 172
            Width = 115
            Height = 17
            Caption = 'Allow edit locates'
            DataField = 'allow_edit_locates'
            DataSource = TermSource
            ReadOnly = True
            TabOrder = 12
          end
          object ClientCallCenter: TcxDBComboBox
            Left = 336
            Top = 55
            DataBinding.DataField = 'call_center'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownRows = 15
            ShowHint = False
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            StyleReadOnly.LookAndFeel.Kind = lfFlat
            StyleReadOnly.LookAndFeel.NativeStyle = True
            TabOrder = 4
            Width = 178
          end
          object ClientUpdateCallCenter: TcxDBComboBox
            Left = 85
            Top = 79
            Hint = 'Del key to clear'
            DataBinding.DataField = 'update_call_center'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 46
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownRows = 15
            Properties.Revertable = True
            ShowHint = True
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            StyleReadOnly.LookAndFeel.Kind = lfFlat
            StyleReadOnly.LookAndFeel.NativeStyle = True
            TabOrder = 5
            Width = 178
          end
          object ClientOffice: TcxDBExtLookupComboBox
            Left = 85
            Top = 55
            Hint = 'Del key to clear'
            DataBinding.DataField = 'office_id'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.View = OfficeLookupView
            Properties.KeyFieldNames = 'office_id'
            Properties.ListFieldItem = OfficeLookupViewOfficeName
            ShowHint = True
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 3
            Width = 178
          end
          object AssignButton: TButton
            Left = 538
            Top = 33
            Width = 120
            Height = 25
            Caption = 'Assign to Customer'
            TabOrder = 14
            OnClick = AssignButtonClick
          end
          object ClientCancelButton: TButton
            Left = 538
            Top = 58
            Width = 120
            Height = 25
            Caption = 'Cancel Changes'
            TabOrder = 15
            OnClick = ClientCancelButtonClick
          end
          object UtilityType: TcxDBExtLookupComboBox
            Left = 336
            Top = 30
            Hint = 'Del key to clear'
            DataBinding.DataField = 'utility_name'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.View = UtilityLookupView
            Properties.KeyFieldNames = 'description'
            Properties.ListFieldItem = UtilityLookupViewDescription
            ShowHint = True
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 2
            Width = 178
          end
          object ClientStatusGroupName: TcxDBExtLookupComboBox
            Left = 336
            Top = 79
            Hint = 'Del key to clear'
            DataBinding.DataField = 'sg_name'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownListStyle = lsFixedList
            Properties.View = StatusGroupLookupView
            Properties.KeyFieldNames = 'sg_id'
            Properties.ListFieldItem = StatusGroupLookupViewSGName
            ShowHint = True
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 6
            Width = 178
          end
          object CheckAlert: TDBCheckBox
            Left = 273
            Top = 172
            Width = 65
            Height = 17
            Caption = 'Alert'
            DataField = 'alert'
            DataSource = TermSource
            ReadOnly = True
            TabOrder = 13
          end
          object ClientRequireUnits: TDBCheckBox
            Left = 344
            Top = 171
            Width = 126
            Height = 17
            Caption = 'Require locate length'
            DataField = 'require_locate_units'
            DataSource = TermSource
            ReadOnly = True
            TabOrder = 8
          end
          object ClientAttachmentURL: TDBEdit
            Left = 85
            Top = 150
            Width = 476
            Height = 21
            DataField = 'attachment_url'
            DataSource = TermSource
            TabOrder = 10
          end
          object ClientBillingUnitRules: TcxDBComboBox
            Left = 85
            Top = 104
            Hint = 'Del key to clear'
            DataBinding.DataField = 'unit_conversion_id'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 46
            Properties.DropDownListStyle = lsFixedList
            ShowHint = True
            Style.BorderStyle = ebsFlat
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            StyleReadOnly.LookAndFeel.Kind = lfFlat
            StyleReadOnly.LookAndFeel.NativeStyle = True
            TabOrder = 7
            Width = 178
          end
          object DamageCompany: TcxDBExtLookupComboBox
            Left = 336
            Top = 103
            Hint = 'grey is inactive -BkSp  key to clear'
            DataBinding.DataField = 'DamageCompany'
            DataBinding.DataSource = TermSource
            ParentShowHint = False
            Properties.ClearKey = 8
            Properties.DropDownAutoSize = True
            Properties.View = DamageCompanyLookupView
            Properties.KeyFieldNames = 'ref_id'
            Properties.ListFieldItem = DamageCompanyLookupViewDamageCo
            ShowHint = True
            Style.LookAndFeel.Kind = lfFlat
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 16
            Width = 178
          end
          object ProjectClientButton: TButton
            Left = 538
            Top = 83
            Width = 120
            Height = 25
            Caption = 'Add Project Client'
            TabOrder = 17
            OnClick = ProjectClientButtonClick
          end
          object ckboxAuditReq: TDBCheckBox
            Left = 581
            Top = 131
            Width = 76
            Height = 17
            Caption = 'Audit Req'#39'd'
            DataField = 'audit_req'
            DataSource = TermSource
            TabOrder = 18
          end
        end
        object NotificationEmailSheet: TTabSheet
          Caption = 'Notification Emails'
          ImageIndex = 1
          object Label16: TLabel
            Left = 0
            Top = 0
            Width = 753
            Height = 20
            Align = alTop
            AutoSize = False
            Caption = 'Press in the Insert key to add an email address for this term'
          end
          object NotificationEmailGrid: TcxGrid
            Left = 0
            Top = 20
            Width = 753
            Height = 114
            Align = alClient
            TabOrder = 0
            OnExit = NotificationEmailGridExit
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object NotificationEmailView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.First.Enabled = False
              Navigator.Buttons.First.Visible = False
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.Prior.Visible = True
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Last.Enabled = False
              Navigator.Buttons.Last.Visible = False
              Navigator.Buttons.Append.Visible = True
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Edit.Visible = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Enabled = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Buttons.Filter.Visible = False
              Navigator.Visible = True
              ScrollbarAnnotations.CustomAnnotations = <>
              DataController.DataSource = NotificationEmailDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'notification_email_id'
              DataController.Options = [dcoAssignGroupingValues, dcoSaveExpanding]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnAddValueItems = False
              Filtering.ColumnMRUItemsList = False
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              object NotificationEmailViewNotificationType: TcxGridDBColumn
                Caption = 'Notification Type'
                DataBinding.FieldName = 'notification_type'
                DataBinding.IsNullValueType = True
                PropertiesClassName = 'TcxComboBoxProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.DropDownRows = 7
                Properties.IncrementalSearch = False
                Properties.Items.Strings = (
                  'emergency'
                  'highprofile'
                  'epr')
                Options.Filtering = False
                Options.IncSearch = False
                Options.FilteringPopup = False
                Options.Sorting = False
                Width = 124
              end
              object NotificationEmailViewEmailAddress: TcxGridDBColumn
                Caption = 'Email Address'
                DataBinding.FieldName = 'email_address'
                DataBinding.IsNullValueType = True
                PropertiesClassName = 'TcxTextEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Options.Filtering = False
                Width = 290
              end
            end
            object NotificationEmailGridLevel: TcxGridLevel
              GridView = NotificationEmailView
            end
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Client Information'
          ImageIndex = 2
          OnExit = TabSheet1Exit
          object Label29: TLabel
            Left = 8
            Top = 117
            Width = 42
            Height = 13
            Caption = 'Plats File'
          end
          object Label30: TLabel
            Left = 8
            Top = 144
            Width = 59
            Height = 13
            Caption = 'Training Link'
          end
          object Label31: TLabel
            Left = 8
            Top = 168
            Width = 44
            Height = 13
            Caption = 'Plats Link'
          end
          object Label32: TLabel
            Left = 3
            Top = 0
            Width = 56
            Height = 13
            Caption = 'Client Rules'
          end
          object Label33: TLabel
            Left = 310
            Top = 0
            Width = 85
            Height = 13
            Caption = 'Marking Concerns'
          end
          object Label34: TLabel
            Left = 615
            Top = 3
            Width = 77
            Height = 13
            Caption = 'Plat Information'
          end
          object Label35: TLabel
            Left = 568
            Top = 112
            Width = 34
            Height = 13
            Caption = 'Trainer'
          end
          object Label36: TLabel
            Left = 760
            Top = 112
            Width = 67
            Height = 13
            Caption = 'Field Engineer'
          end
          object memoClientRules: TDBMemo
            Left = 5
            Top = 19
            Width = 299
            Height = 89
            DataField = 'client_rules'
            DataSource = ClientInfoDS
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object memoConcerns: TDBMemo
            Left = 310
            Top = 19
            Width = 299
            Height = 89
            DataField = 'marking_concerns'
            DataSource = ClientInfoDS
            ScrollBars = ssVertical
            TabOrder = 1
          end
          object memoPlatInfo: TDBMemo
            Left = 615
            Top = 19
            Width = 299
            Height = 89
            DataField = 'plat_info'
            DataSource = ClientInfoDS
            ScrollBars = ssVertical
            TabOrder = 2
          end
          object edtPlatFile: TDBEdit
            Left = 72
            Top = 114
            Width = 417
            Height = 21
            DataField = 'plat_file'
            DataSource = ClientInfoDS
            TabOrder = 3
          end
          object edtTrainLink: TDBEdit
            Left = 73
            Top = 140
            Width = 417
            Height = 21
            DataField = 'training_link'
            DataSource = ClientInfoDS
            TabOrder = 4
          end
          object edtPlatsLink: TDBEdit
            Left = 72
            Top = 167
            Width = 417
            Height = 21
            DataField = 'plats_online_link'
            DataSource = ClientInfoDS
            TabOrder = 5
          end
          object cbTrainer: TDBLookupComboBox
            Left = 543
            Top = 131
            Width = 170
            Height = 21
            DataField = 'trainer_emp_id'
            DataSource = ClientInfoDS
            KeyField = 'emp_id'
            ListField = 'EmpName'
            ListSource = EmpTrainerSource
            TabOrder = 6
          end
          object cbFieldEngineer: TDBLookupComboBox
            Left = 744
            Top = 131
            Width = 170
            Height = 21
            DataField = 'field_engineer_emp_id'
            DataSource = ClientInfoDS
            KeyField = 'emp_id'
            ListField = 'EmpName'
            ListSource = EmpEngineerSource
            TabOrder = 7
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'HP Ranking'
          ImageIndex = 3
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 753
            Height = 35
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object btnHPRanking: TButton
              Left = 2
              Top = 4
              Width = 117
              Height = 23
              Caption = 'Edit HP Ranking'
              TabOrder = 0
              OnClick = btnHPRankingClick
            end
            object Client_Code: TDBEdit
              Left = 316
              Top = 7
              Width = 146
              Height = 21
              DataField = 'oc_code'
              DataSource = TermSource
              ReadOnly = True
              TabOrder = 1
              OnKeyDown = Client_NameKeyDown
            end
            object Client_Name: TDBEdit
              Left = 136
              Top = 6
              Width = 168
              Height = 21
              DataField = 'client_name'
              DataSource = TermSource
              ReadOnly = True
              TabOrder = 2
              OnKeyDown = Client_NameKeyDown
            end
            object ActiveRankingFilter: TCheckBox
              Left = 484
              Top = 10
              Width = 107
              Height = 17
              Caption = 'Active Rankings'
              Checked = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              State = cbChecked
              TabOrder = 3
              OnClick = ActiveRankingFilterClick
            end
          end
          object HPRankGrid: TcxGrid
            Left = 0
            Top = 35
            Width = 753
            Height = 99
            Align = alClient
            TabOrder = 1
            object HPRankView: TcxGridDBTableView
              Navigator.Buttons.ConfirmDelete = True
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Last.Enabled = False
              Navigator.Buttons.Append.Visible = True
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Visible = True
              ScrollbarAnnotations.CustomAnnotations = <>
              OnCustomDrawCell = HPRankViewCustomDrawCell
              DataController.DataSource = HPRankingDS
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              EditForm.ItemHotTrack = True
              OptionsBehavior.IncSearch = True
              OptionsBehavior.IncSearchItem = HPRankViewDescription
              OptionsSelection.HideFocusRectOnExit = False
              OptionsView.GroupByBox = False
              object HPRankViewDescription: TcxGridDBColumn
                Caption = 'Description'
                DataBinding.FieldName = 'hp_ref_id'
                DataBinding.IsNullValueType = True
                PropertiesClassName = 'TcxExtLookupComboBoxProperties'
                Properties.DropDownAutoSize = True
                Properties.DropDownSizeable = True
                Properties.View = RefIDLookupView
                Properties.KeyFieldNames = 'ref_id'
                Properties.ListFieldItem = RefIDLookupViewDescription
                Options.GroupFooters = False
                Options.Grouping = False
                Options.Sorting = False
                Width = 205
              end
              object HPRankViewUtilityCode: TcxGridDBColumn
                Caption = 'Utility Type'
                DataBinding.FieldName = 'utility_code'
                DataBinding.IsNullValueType = True
                Options.Filtering = False
                Options.FilteringWithFindPanel = False
                Options.Focusing = False
                Options.FilteringAddValueItems = False
                Options.FilteringFilteredItemsList = False
                Options.FilteringFilteredItemsListShowFilteredItemsOnly = False
                Options.FilteringMRUItemsList = False
                Options.FilteringPopup = False
                Options.FilteringPopupMultiSelect = False
                Options.GroupFooters = False
                Options.Grouping = False
                Options.Sorting = False
                Width = 96
              end
              object HPRankViewCode: TcxGridDBColumn
                Caption = 'Code'
                DataBinding.FieldName = 'code'
                DataBinding.IsNullValueType = True
                Options.Editing = False
                Options.ExpressionEditing = False
                Options.GroupFooters = False
                Options.Grouping = False
                Width = 82
              end
              object HPRankViewRank: TcxGridDBColumn
                Caption = 'Rank'
                DataBinding.FieldName = 'rank'
                DataBinding.IsNullValueType = True
                PropertiesClassName = 'TcxExtLookupComboBoxProperties'
                Properties.Alignment.Horz = taCenter
                Properties.DropDownAutoSize = True
                Properties.DropDownListStyle = lsEditList
                Properties.DropDownSizeable = True
                Properties.View = HPRankLookupView
                Properties.KeyFieldNames = 'rank'
                Properties.ListFieldItem = HPRankLookupViewRank
                MinWidth = 15
                Options.FilteringWithFindPanel = False
                Options.FilteringPopupMultiSelect = False
                Options.GroupFooters = False
                Options.Grouping = False
                Width = 71
              end
              object HPRankViewActive: TcxGridDBColumn
                Caption = 'Active'
                DataBinding.FieldName = 'active'
                DataBinding.IsNullValueType = True
                Width = 52
              end
            end
            object HPRankLookupView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              ScrollbarAnnotations.CustomAnnotations = <>
              DataController.DataSource = RankLookupDS
              DataController.KeyFieldNames = 'rank'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsSelection.CellSelect = False
              OptionsView.GridLines = glHorizontal
              OptionsView.GroupByBox = False
              OptionsView.Header = False
              object HPRankLookupViewRank: TcxGridDBColumn
                DataBinding.FieldName = 'rank'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Options.Filtering = False
                Options.FilteringWithFindPanel = False
                Options.FilteringAddValueItems = False
                Options.FilteringFilteredItemsList = False
                Options.FilteringFilteredItemsListShowFilteredItemsOnly = False
                Options.FilteringMRUItemsList = False
                Options.FilteringPopup = False
                Options.FilteringPopupMultiSelect = False
                Options.ExpressionEditing = False
                Options.GroupFooters = False
                Options.Grouping = False
              end
            end
            object RefIDLookupView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              ScrollbarAnnotations.CustomAnnotations = <>
              DataController.DataSource = HPRefidDS
              DataController.KeyFieldNames = 'ref_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsCustomize.ColumnGrouping = False
              OptionsData.DeletingConfirmation = False
              OptionsSelection.MultiSelect = True
              OptionsSelection.CellMultiSelect = True
              OptionsSelection.InvertSelect = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GroupByBox = False
              object RefIDLookupViewCode: TcxGridDBColumn
                Caption = 'Code '
                DataBinding.FieldName = 'code'
                DataBinding.IsNullValueType = True
                MinWidth = 35
                Options.FilteringPopupIncrementalFiltering = True
                Width = 100
              end
              object RefIDLookupViewDescription: TcxGridDBColumn
                Caption = 'Description'
                DataBinding.FieldName = 'description'
                DataBinding.IsNullValueType = True
                MinWidth = 200
              end
            end
            object HPRankGridLevel: TcxGridLevel
              GridView = HPRankView
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Client Respond To'
          ImageIndex = 4
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 753
            Height = 25
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object chkboxClientRespondToSearch: TCheckBox
              Left = 20
              Top = 3
              Width = 101
              Height = 16
              Caption = 'Show Find Panel'
              TabOrder = 0
              OnClick = chkboxClientRespondToSearchClick
            end
          end
          object cxGrid2: TcxGrid
            Left = 0
            Top = 25
            Width = 753
            Height = 109
            Align = alClient
            TabOrder = 1
            object ClientRespondToView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.First.Visible = False
              Navigator.Buttons.PriorPage.Visible = False
              Navigator.Buttons.NextPage.Visible = False
              Navigator.Buttons.Insert.Enabled = False
              Navigator.Buttons.Append.Enabled = False
              Navigator.Buttons.Append.Visible = False
              Navigator.Buttons.Delete.Enabled = False
              Navigator.Buttons.Delete.Visible = False
              Navigator.Buttons.Cancel.Visible = False
              Navigator.Buttons.Refresh.Enabled = False
              Navigator.Buttons.Refresh.Visible = False
              Navigator.Buttons.SaveBookmark.Enabled = False
              Navigator.Buttons.SaveBookmark.Visible = False
              Navigator.Buttons.GotoBookmark.Enabled = False
              Navigator.Buttons.GotoBookmark.Visible = False
              Navigator.Buttons.Filter.Visible = False
              Navigator.Visible = True
              FindPanel.DisplayMode = fpdmManual
              ScrollbarAnnotations.CustomAnnotations = <>
              OnCustomDrawCell = ClientRespondToViewCustomDrawCell
              DataController.DataSource = ClientRespondToDS
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              EditForm.ItemHotTrack = True
              OptionsBehavior.IncSearch = True
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsView.GroupByBox = False
              object CRTViewClientRepondTo: TcxGridDBColumn
                Caption = 'Client Respond To'
                DataBinding.FieldName = 'client_respond_to'
                DataBinding.IsNullValueType = True
                PropertiesClassName = 'TcxMaskEditProperties'
                Options.Editing = False
                Width = 112
              end
              object CRTViewActive: TcxGridDBColumn
                Caption = 'Active'
                DataBinding.FieldName = 'active'
                DataBinding.IsNullValueType = True
                Width = 52
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = ClientRespondToView
            end
          end
        end
      end
    end
  end
  object Customer: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = CustomerBeforeOpen
    BeforeInsert = CustomerBeforeInsert
    BeforeEdit = CustomerBeforeEdit
    BeforePost = CustomerBeforePost
    AfterPost = CustomerAfterPost
    AfterCancel = CustomerAfterCancel
    BeforeDelete = DatasetBeforeDelete
    AfterScroll = CustomerAfterScroll
    OnNewRecord = CustomerNewRecord
    CommandText = 'select * from customer where active = 1 order by customer_name'
    EnableBCD = False
    ParamCheck = False
    Parameters = <>
    Left = 24
    Top = 96
  end
  object CustomerSource: TDataSource
    DataSet = Customer
    OnDataChange = CustomerSourceDataChange
    Left = 24
    Top = 152
  end
  object TermSource: TDataSource
    DataSet = Term
    OnDataChange = TermSourceDataChange
    Left = 88
    Top = 136
  end
  object OfficeLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select office_id, office_name, profit_center from office'
    Parameters = <>
    Left = 224
    Top = 104
  end
  object OfficeLookupSource: TDataSource
    DataSet = OfficeLookup
    Left = 288
    Top = 104
  end
  object UtilityLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from reference where type='#39'utility'#39
    Parameters = <>
    Left = 464
    Top = 112
  end
  object UtilityLookupSource: TDataSource
    DataSet = UtilityLookup
    Left = 448
    Top = 168
  end
  object SGLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from status_group order by sg_name'
    Parameters = <>
    Left = 232
    Top = 152
  end
  object SGLookupDS: TDataSource
    DataSet = SGLookup
    Left = 136
    Top = 192
  end
  object Company: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select company_id, name'#13#10'from locating_company'
    Parameters = <>
    Left = 90
    Top = 185
  end
  object CompanyDS: TDataSource
    DataSet = Company
    Left = 362
    Top = 105
  end
  object UnitConversionLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select bu.unit_conversion_id, CASE'#13#10'  WHEN bu.rest_units_factor ' +
      'IS  Null THEN '#39'Bill 1 every '#39' + cast(bu.first_unit_factor as var' +
      'char(10)) + '#39' '#39' + bu.unit_type'#13#10'  ELSE  '#39'Bill 1 first '#39' + cast(b' +
      'u.first_unit_factor as varchar(10)) + '#39' '#39' + bu.unit_type + '#39'; 1 ' +
      'each additional '#39' + cast(bu.rest_units_factor as varchar(10)) + ' +
      #39' '#39' + bu.unit_type'#13#10'END as Description'#13#10'from billing_unit_conver' +
      'sion bu where bu.active=1 '#13#10'order by Description'
    Parameters = <>
    Left = 536
    Top = 104
  end
  object UnitConversionDS: TDataSource
    DataSet = UnitConversionLookup
    Left = 600
    Top = 112
  end
  object ProfitCenterSource: TDataSource
    DataSet = ProfitCenter
    Left = 216
    Top = 192
  end
  object ProfitCenter: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from profit_center'
    Parameters = <>
    Left = 288
    Top = 160
  end
  object NotificationEmailDS: TDataSource
    DataSet = NotificationEmail
    Left = 346
    Top = 193
  end
  object DamageCoLookupSource: TDataSource
    DataSet = DamageCoLookup
    Left = 673
    Top = 105
  end
  object DamageCoLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, description as DamageCompany, active_ind  '#13#10'from ' +
      'reference'#13#10'where type = '#39'dmgco'#39#13#10'order by description '
    Parameters = <>
    Prepared = True
    Left = 545
    Top = 169
  end
  object NotificationEmail: TADOTable
    Connection = AdminDM.Conn
    BeforeInsert = NotificationEmailBeforeInsert
    BeforeEdit = NotificationEmailBeforeEdit
    AfterPost = NotificationEmailAfterPost
    AfterCancel = NotificationEmailAfterCancel
    OnNewRecord = NotificationEmailNewRecord
    IndexFieldNames = 'client_id'
    MasterFields = 'client_id'
    MasterSource = TermSource
    TableName = 'notification_email_by_client'
    Left = 354
    Top = 153
  end
  object ClientInfoDS: TDataSource
    DataSet = ClientInfo
    Left = 328
    Top = 96
  end
  object ClientInfo: TADOTable
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeEdit = ClientInfoBeforeEdit
    AfterPost = ClientInfoAfterPost
    OnNewRecord = ClientInfoNewRecord
    IndexFieldNames = 'client_id'
    MasterFields = 'client_id'
    MasterSource = TermSource
    TableName = 'client_info'
    Left = 24
    Top = 200
  end
  object Term: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeOpen = TermBeforeOpen
    BeforeInsert = TermBeforeInsert
    AfterInsert = TermAfterInsert
    BeforeEdit = TermBeforeEdit
    BeforePost = TermBeforePost
    AfterPost = TermAfterPost
    AfterCancel = TermAfterCancel
    BeforeDelete = DatasetBeforeDelete
    AfterScroll = TermAfterScroll
    OnNewRecord = TermNewRecord
    CommandText = 
      'select * from client c where c.active = 1'#13#10'order by c.active des' +
      'c, c.oc_code asc'
    EnableBCD = False
    ParamCheck = False
    Parameters = <>
    Left = 88
    Top = 88
  end
  object EmpTrainerSource: TDataSource
    DataSet = EmpTrainer
    Left = 800
    Top = 104
  end
  object EmpTrainer: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select emp_id, (last_name+'#39', '#39'+first_name) as EmpName'#13#10'from empl' +
      'oyee where ISNULL(LTRIM(RTRIM(last_name)),'#39#39')<>'#39#39' '#13#10'order by las' +
      't_name'#13#10
    Parameters = <>
    Left = 888
    Top = 104
  end
  object EmpEngineer: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select emp_id, (last_name+'#39', '#39'+first_name) as EmpName'#13#10'from empl' +
      'oyee where ISNULL(LTRIM(RTRIM(last_name)),'#39#39')<>'#39#39' '#13#10'order by las' +
      't_name'#13#10
    Parameters = <>
    Left = 736
    Top = 32
  end
  object EmpEngineerSource: TDataSource
    DataSet = EmpEngineer
    Left = 752
    Top = 104
  end
  object CenterActions: TActionList
    Left = 648
    Top = 32
    object AddAction: TDataSetInsert
      Category = 'Dataset'
      Caption = 'Add'
      Enabled = False
      Hint = 'Insert'
      ImageIndex = 4
    end
    object SaveAction: TDataSetPost
      Category = 'Dataset'
      Caption = 'Save'
      Enabled = False
      Hint = 'Post'
      ImageIndex = 7
    end
    object CancelAction: TDataSetCancel
      Category = 'Dataset'
      Caption = 'Cancel'
      Enabled = False
      Hint = 'Cancel'
      ImageIndex = 8
    end
  end
  object InsertRightCommand: TADOCommand
    CommandText = 
      'Insert into client'#13#10'SELECT [client_name],[oc_code],[office_id],[' +
      'modified_date],[active],[call_center],[update_call_center],'#13#10'[gr' +
      'id_email], [allow_edit_locates], [customer_id], [utility_type],[' +
      'status_group_id], [unit_conversion_id], [alert], '#13#10'[require_loca' +
      'te_units], [attachment_url], [qh_id],[ref_id], [parent_client_id' +
      ']'#13#10' FROM [client]'#13#10'where [oc_code]  = :oc_code'
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'oc_code'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    Left = 488
    Top = 40
  end
  object ChildClientSP: TADOStoredProc
    Connection = AdminDM.Conn
    CursorType = ctStatic
    ProcedureName = 'AddChildClient'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Value = Null
      end
      item
        Name = '@oc_code'
        Size = 10
        Value = Null
      end>
    Left = 568
    Top = 24
  end
  object ResponderLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select 0 as sort, '#13#10'convert(int, null) as ref_id,'#13#10' '#39#39' as code'#13#10 +
      'union all'#13#10'select 1 as sort,'#13#10' ref_id,'#13#10' code from reference ref' +
      #13#10' where ref.type='#39'responder'#39' and ref.active_ind = 1'#13#10' order by ' +
      'sort,  code'
    Parameters = <>
    Left = 144
    Top = 96
  end
  object ResponderLookupDS: TDataSource
    DataSet = RankLookup
    Left = 152
    Top = 152
  end
  object HPRanking: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = HPRankingBeforeInsert
    BeforeEdit = HPRankingBeforeEdit
    BeforePost = HPRankingBeforePost
    AfterPost = HPRankingAfterPost
    AfterCancel = HPRankingAfterCancel
    BeforeDelete = DatasetBeforeDelete
    OnNewRecord = HPRankingNewRecord
    Parameters = <
      item
        Name = 'utility_code'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'Select HPR.hp_util_id, HPR.hp_ref_id,  HPR.client_id,HPR.utility' +
        '_code, '
      'HPR.rank, R.code, HPR.active, R.[description] '
      'from reference r'
      'join hp_utils_rank HPR on (r.ref_id =HPR.hp_ref_id )  '
      'where r.type = '#39'hpmulti'#39
      ' and HPR.utility_code = :utility_code'
      'and HPR.client_id = :client_id'
      ' and R.active_ind = 1 '
      ' and HPR.active = 1 '
      'order by rank')
    Left = 773
    Top = 120
  end
  object HPRankingDS: TDataSource
    DataSet = HPRanking
    Left = 722
    Top = 153
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 626
    Top = 168
  end
  object RankLookup: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'utility_code'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'client_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select HPR.rank'
      'from reference r'
      'join hp_utils_rank HPR on (r.ref_id =HPR.hp_ref_id )  '
      'where r.type = '#39'hpmulti'#39
      ' and HPR.utility_code = :utility_code'
      'and HPR.client_id = :client_id'
      ' and R.active_ind = 1 '
      ' and HPR.active = 1 '
      ' and HPR.rank > 0'
      'order by rank')
    Left = 525
    Top = 184
    object RankLookuprank: TSmallintField
      FieldName = 'rank'
    end
  end
  object RankLookupDS: TDataSource
    AutoEdit = False
    DataSet = RankLookup
    Left = 602
    Top = 217
  end
  object ResetRank: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'utilid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Update hp_utils_rank set rank = 0 where'
      'hp_util_id = :utilid'
      '')
    Left = 461
    Top = 232
    object SmallintField1: TSmallintField
      FieldName = 'rank'
    end
  end
  object qryDupRank: TADOQuery
    Connection = AdminDM.Conn
    Parameters = <
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'rank'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'Select hp_util_id from hp_utils_rank '
      'where  client_id = :client_id '
      ' and rank = :rank')
    Left = 664
    Top = 152
  end
  object HPRefid_Lookup: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select ref_id, description, code'
      'from reference'
      'where type = '#39'hpmulti'#39
      'and active_ind = 1')
    Left = 677
    Top = 216
  end
  object HPRefidDS: TDataSource
    DataSet = HPRefid_Lookup
    Left = 762
    Top = 265
  end
  object qryDupHPRefid: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'client_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'hprefid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select hp_ref_id from hp_utils_rank '
      'where  client_id = :client_id and hp_ref_id = :hprefid')
    Left = 741
    Top = 208
  end
  object UpdateCode: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'utilid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Update hp_utils_rank set code = :code  where'
      'hp_util_id = :utilid'
      '')
    Left = 949
    Top = 112
    object SmallintField2: TSmallintField
      FieldName = 'rank'
    end
  end
  object ClientRespondTo: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeEdit = ClientRespondToBeforeEdit
    AfterPost = ClientRespondToAfterPost
    CommandText = 'Select * from client_respond_to'#13#10' where client_id = :client_id'
    DataSource = TermSource
    Parameters = <
      item
        Name = 'client_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 845
    Top = 128
  end
  object ClientRespondToDS: TDataSource
    DataSet = ClientRespondTo
    Left = 866
    Top = 161
  end
  object dxSkinController2: TdxSkinController
    SkinName = 'DevExpressStyle'
    Left = 185
    Top = 81
  end
end
