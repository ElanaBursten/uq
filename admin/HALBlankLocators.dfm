inherited BlankLocatorsForm: TBlankLocatorsForm
  Caption = 'Blank Locators'
  ClientHeight = 478
  ClientWidth = 642
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 385
    Width = 642
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 642
    Height = 38
    object Label1: TLabel
      Left = 152
      Top = 13
      Width = 145
      Height = 13
      Caption = '#days from blank locator date'
    end
    object chkboxBlankLocatorSearch: TCheckBox
      Left = 15
      Top = 12
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxBlankLocatorSearchClick
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 307
      Top = 9
      Width = 45
      Height = 22
      MaxValue = 9
      MinValue = 1
      TabOrder = 1
      Value = 5
    end
    object btnDateAdd: TButton
      Left = 360
      Top = 8
      Width = 80
      Height = 21
      Caption = 'Requery'
      TabOrder = 2
      OnClick = btnDateAddClick
    end
  end
  inherited Grid: TcxGrid
    Top = 38
    Width = 642
    Height = 347
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Enabled = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = True
      object GridViewlocate_id: TcxGridDBColumn
        Caption = 'locate id'
        DataBinding.FieldName = 'locate_id'
        Width = 65
      end
      object GridViewticket_id: TcxGridDBColumn
        Caption = 'ticket id'
        DataBinding.FieldName = 'ticket_id'
      end
      object GridViewclient_code: TcxGridDBColumn
        Caption = 'client code'
        DataBinding.FieldName = 'client_code'
        Width = 76
      end
      object GridViewstatus: TcxGridDBColumn
        DataBinding.FieldName = 'status'
        Width = 55
      end
      object GridViewassigned_to: TcxGridDBColumn
        Caption = 'assigned to'
        DataBinding.FieldName = 'assigned_to'
        Width = 90
      end
      object GridViewmodified_date: TcxGridDBColumn
        Caption = 'modified date'
        DataBinding.FieldName = 'modified_date'
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 393
    Width = 642
    Height = 85
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
      end
      object ColDaysback: TcxGridDBColumn
        Caption = 'Days Back'
        DataBinding.FieldName = 'daysback'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColDaysbackPropertiesEditValueChanged
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select locate_id,ticket_id,client_code,status,'#13#10'assigned_to,modi' +
      'fied_date '#13#10'from locate with(nolock)'#13#10'where status not in ('#39'-N'#39',' +
      #39'-P'#39')'#13#10'and closed = 0'#13#10'And active = 1'#13#10'and assigned_to is null'#13#10 +
      'and modified_date >= getdate() - :daysback'
    Parameters = <
      item
        Name = 'daysback'
        DataType = ftInteger
        Size = 3
        Value = 5
      end>
    object Datalocate_id: TAutoIncField
      FieldName = 'locate_id'
      ReadOnly = True
    end
    object Dataticket_id: TIntegerField
      FieldName = 'ticket_id'
    end
    object Dataclient_code: TStringField
      FieldName = 'client_code'
      Size = 10
    end
    object Datastatus: TStringField
      FieldName = 'status'
      Size = 5
    end
    object Dataassigned_to: TIntegerField
      FieldName = 'assigned_to'
    end
    object Datamodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select * from hal where hal_module = '#39'blank_locators'#39
    Parameters = <>
    Left = 336
    Top = 88
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 392
    Top = 88
  end
end
