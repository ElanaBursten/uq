inherited BaseCxListForm: TBaseCxListForm
  Caption = 'BaseCxListForm'
  ClientHeight = 401
  Font.Charset = ANSI_CHARSET
  OnShow = FormShow
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Grid: TcxGrid
    Left = 0
    Top = 41
    Width = 544
    Height = 360
    Align = alClient
    TabOrder = 1
    OnExit = GridExit
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object GridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataModeController.SmartRefresh = True
      DataController.DataSource = DS
      DataController.Filter.MaxValueListCount = 10
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.MRUItemsList = False
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Appending = True
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.NoDataToDisplayInfoText = '<No data to display. Press Insert to add.>'
      OptionsView.GroupByBox = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object Data: TADODataSet
    BeforeDelete = DataBeforeDelete
    Parameters = <>
    Left = 56
    Top = 112
  end
  object DS: TDataSource
    DataSet = Data
    Left = 96
    Top = 112
  end
  object dxSkinController2: TdxSkinController
    SkinName = 'DevExpressStyle'
    Left = 392
    Top = 184
  end
end
