inherited DamageDueDateRulesForm: TDamageDueDateRulesForm
  Caption = 'Damage Due Date Rules'
  ClientWidth = 578
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 578
    Height = 25
    object cbDamageDueDateSearch: TCheckBox
      Left = 26
      Top = 6
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
    end
    object ActiveDueDateFilter: TCheckBox
      Left = 148
      Top = 6
      Width = 140
      Height = 12
      Caption = 'Active Dates Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveDueDateFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 578
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.KeyFieldNames = 'rule_id'
      object ColPc_Code: TcxGridDBColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'pc_code'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownRows = 20
        Width = 147
      end
      object ColUtilityCoDamaged: TcxGridDBColumn
        Caption = 'Utility Co. Damaged'
        DataBinding.FieldName = 'utility_co_damaged'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownRows = 20
        Width = 193
      end
      object ColCalcMethod: TcxGridDBColumn
        Caption = 'Calculation Method'
        DataBinding.FieldName = 'calc_Method'
        PropertiesClassName = 'TcxComboBoxProperties'
        Width = 120
      end
      object ColDays: TcxGridDBColumn
        Caption = 'Days'
        DataBinding.FieldName = 'days'
        HeaderAlignmentHorz = taRightJustify
        Width = 65
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 68
      end
      object Colrule_id: TcxGridDBColumn
        DataBinding.FieldName = 'rule_id'
        Visible = False
      end
      object GridViewColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'modified_date'
        Visible = False
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorLocation = clUseServer
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from damage_due_date_rule where active = 1'
  end
  object ProfitCenters: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'select pc_code'#13#10'from profit_center'#13#10'order by pc_code'
    Parameters = <>
    Left = 56
    Top = 168
  end
  object UtilityCompanys: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 
      'select ref_id, description from reference where type='#39'dmgco'#39' and' +
      ' active_ind=1 order by description'
    Parameters = <>
    Left = 56
    Top = 224
  end
  object CalcMethods: TADODataSet
    Connection = AdminDM.Conn
    CommandText = 'Select code from reference where type='#39'dddrule'#39' and active_ind=1'
    Parameters = <>
    Left = 56
    Top = 280
  end
end
