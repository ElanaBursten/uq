inherited OfficesForm: TOfficesForm
  Left = 335
  Top = 260
  Caption = 'Offices'
  ClientHeight = 464
  ClientWidth = 1090
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 1090
    Height = 28
    object chkboxOfficesSearch: TCheckBox
      Left = 29
      Top = 7
      Width = 99
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxOfficesSearchClick
    end
    object ActiveOfficesFilter: TCheckBox
      Left = 160
      Top = 6
      Width = 129
      Height = 16
      Caption = 'Active Offices Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ActiveOfficesFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 28
    Width = 1090
    Height = 436
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'office_id'
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColOfficeID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'office_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 34
      end
      object ColOfficeName: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'office_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 197
      end
      object ColProfitCenter: TcxGridDBColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'profit_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 15
        Width = 73
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 48
      end
      object ColCompanyName: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'company_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 91
      end
      object ColAddress1: TcxGridDBColumn
        Caption = 'Address1'
        DataBinding.FieldName = 'address1'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 152
      end
      object ColAddress2: TcxGridDBColumn
        Caption = 'Address2'
        DataBinding.FieldName = 'address2'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 100
      end
      object ColCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 81
      end
      object ColState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 37
      end
      object ColZipcode: TcxGridDBColumn
        Caption = 'Zip'
        DataBinding.FieldName = 'zipcode'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 78
      end
      object ColPhone: TcxGridDBColumn
        Caption = 'Phone'
        DataBinding.FieldName = 'phone'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 90
      end
      object ColFax: TcxGridDBColumn
        Caption = 'Fax'
        DataBinding.FieldName = 'fax'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 93
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    OnNewRecord = DataNewRecord
    CommandText = 'select * from office where active = 1'#13#10'order by office_name'
  end
  inherited DS: TDataSource
    Top = 108
  end
end
