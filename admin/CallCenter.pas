unit CallCenter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseList, DB, ADODB, Grids, DBGrids, ExtCtrls, BaseCxList,
  StdCtrls, Mask, DBCtrls, ComCtrls, CheckLst, DBActns, ActnList, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxCalendar, cxCheckBox, cxDropDownEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxDBLookupComboBox,
  OdDbUtils, cxNavigator, System.Actions, cxTextEdit, dxSkinsCore,
  cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxScrollbarAnnotations, cxContainer, cxCustomListBox,
  cxCheckListBox, AdminDmu, dxSkinBasic, dxCore, dxSkinsForm;

type
  TCallCenterForm = class(TBaseCxListForm)
    PC: TPageControl;
    EmailSheet: TTabSheet;
    HighProfileSheet: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Reasons: TADODataSet;
    UpdateReasons: TADOStoredProc;
    Label8: TLabel;
    ContactSheet: TTabSheet;
    ContactsDS: TDataSource;
    Contacts: TADODataSet;
    ContactGrid: TDBGrid;
    ContactNotesLabel: TLabel;
    ContactNotesMemo: TDBMemo;
    AddButton: TButton;
    SaveButton: TButton;
    CancelButton: TButton;
    CenterActions: TActionList;
    AddAction: TDataSetInsert;
    SaveAction: TDataSetPost;
    CancelAction: TDataSetCancel;
    XMLTicketFormatSheet: TTabSheet;
    TicketXsltMemo: TDBMemo;
    Label9: TLabel;
    ColCallCenterCode: TcxGridDBColumn;
    ColCCName: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColAdminEmail: TcxGridDBColumn;
    ColMessageEmail: TcxGridDBColumn;
    ColWarningEmail: TcxGridDBColumn;
    ColEmergencyEmail: TcxGridDBColumn;
    ColSummaryEmail: TcxGridDBColumn;
    ColAuditMethod: TcxGridDBColumn;
    ColUsePrerouting: TcxGridDBColumn;
    ColTrackArrivals: TcxGridDBColumn;
    ColTimeZone: TcxGridDBColumn;
    TZData: TADODataSet;
    StatusTranslations: TADODataSet;
    StatusTranslationsDS: TDataSource;
    StatusTranslationsSheet: TTabSheet;
    StatusTranslationsGridDBTableView1: TcxGridDBTableView;
    StatusTranslationsGridLevel1: TcxGridLevel;
    StatusTranslationsGrid: TcxGrid;
    ColStatusTranslationID: TcxGridDBColumn;
    ColCCCode: TcxGridDBColumn;
    ColStatus: TcxGridDBColumn;
    ColOutgoingStatus: TcxGridDBColumn;
    ColTranslationActive: TcxGridDBColumn;
    ColExplanationCode: TcxGridDBColumn;
    ColResponderType: TcxGridDBColumn;
    ColResponderKind: TcxGridDBColumn;
    ColAllowedWork: TcxGridDBColumn;
    cbCallCenterSearch: TCheckBox;
    ActiveCallCentersFilter: TCheckBox;
    Splitter1: TSplitter;
    hpchecklist: TcxCheckListBox;
    Label11: TLabel;
    DBEdit8: TDBEdit; //QM-1054 emergency_requeue_email
    procedure DataAfterScroll(DataSet: TDataSet);
    procedure HpCheckListClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure PCChange(Sender: TObject);
    procedure StatusTranslationsAfterInsert(DataSet: TDataSet);
    procedure cbCallCenterSearchClick(Sender: TObject);
    procedure ActiveCallCentersFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure ContactGridExit(Sender: TObject);
    procedure StatusTranslationsGridExit(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure ContactsBeforeEdit(DataSet: TDataSet);
    procedure StatusTranslationsBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure StatusTranslationsAfterPost(DataSet: TDataSet);
    procedure ContactsAfterPost(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure StatusTranslationsAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DetailsBeforePost(DataSet: TDataSet);
    procedure StatusTranslationsBeforeInsert(DataSet: TDataSet);
    procedure ContactsBeforeInsert(DataSet: TDataSet);
    procedure ContactsAfterCancel(DataSet: TDataSet);
  private
    FUpdating: Boolean;
    CCInserted: Boolean;
    ContactInserted: Boolean;
    StatusTransInserted: Boolean;
    ContactsArray: TFieldValuesArray;
    StatusTransArray: TFieldValuesArray;
    procedure PopulateList;
    procedure LoadContacts;
    procedure LoadStatusTranslations;
  public
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses OdCxUtils, OdHourglass, OdMiscUtils;

{$R *.dfm}

procedure TCallCenterForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
   DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TCallCenterForm.DataAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not Data.ControlsDisabled then begin
    if PC.ActivePage = HighProfileSheet then
      PopulateList;
    LoadContacts;
    LoadStatusTranslations;
  end;
end;

procedure TCallCenterForm.PopulateList;
var
 AItem: TcxCheckListBoxItem;
begin
  FUpdating := True;
  Reasons.Parameters.ParamValues['cc'] :=
    Data.FieldByName('cc_code').AsString;
  Reasons.Open;
  try
    HpCheckList.Items.Clear;
    HpCheckList.Items.BeginUpdate;
    try
      while not Reasons.EOF do begin
        AItem := HpCheckList.Items.Add;
      AItem.Text := Reasons.Fields[0].AsString;
      AItem.ItemObject := TObject(Reasons.Fields[1].AsInteger);
      if Reasons.FieldByName('active').AsBoolean then
          HpCheckList.Items[HpCheckList.Items.Count-1].Checked := True;
      HpCheckList.Items[HpCheckList.Items.Count-1].Enabled := not IsReadOnly;
      Reasons.Next;
      end;
    finally
      HpCheckList.Items.EndUpdate;
    end;
  finally
    Reasons.Close;
    FUpdating := False;
  end;
end;

function GetSelectedList(CLBox: TcxCheckListBox): string;
var
  I: Integer;
  Codes: string;
  Code: string;
begin
  for I := 0 to CLBox.Items.Count - 1 do begin
    if CLBox.Items[I].Checked = true then begin
      Code := IntToStr(Integer(CLBox.Items.Objects[I]));
      if Codes <>'' then
        Codes := Codes + ',';
      Codes := Codes + Code;
    end;
  end;
  Result := Codes;
end;

procedure TCallCenterForm.HpCheckListClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
var
  ChangeDesc : String;
  PrevState, NewState: String;
begin
  if FUpdating then
    Exit;

  if Data.State in dsEditModes then
    Data.Post;

  UpdateReasons.Parameters.ParamValues['@CallCenter'] := Data.FieldByName('cc_code').AsString;
  UpdateReasons.Parameters.ParamValues['@Reasons'] := GetSelectedList(HpCheckList);
  UpdateReasons.ExecProc;

  //QM-831 change tracking for hp_reasons
  if APrevState <> ANewState then
  begin
    PrevState := 'Inactive';
    NewState := 'Active';
    if APrevState = cbsChecked then
    begin
      PrevState := 'Active';
      NewState := 'Inactive';
    end;
    ChangeDesc := 'CallCenter ' + Data.FieldByName('cc_code').AsString +
     '    hp_id ' + InttoStr(Integer(hpCheckList.Items.Objects[Aindex])) + ': "' +
     hpCheckList.Items[AIndex].Text + '" changed from ' + PrevState + ' to ' + NewState;
    AdminDM.AdminChangeTable('call_center_hp', ChangeDesc, now);
  end;
end;

procedure TCallCenterForm.PCChange(Sender: TObject);
begin
  inherited;
  if PC.ActivePage = HighProfileSheet then
    PopulateList;
end;

procedure TCallCenterForm.PopulateDropdowns;
begin
  //populate time_zone dropdown
  AddLookupField('time_zone_description', Data, 'time_zone', TZData, 'ref_id', 'description',35);

  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  if not Data.Active then
    Data.FieldByName('time_zone_description').LookupCache := True;

  // populate audit_method dropdown
  CxComboBoxItems(ColAuditMethod).Clear;
  CxComboBoxItems(ColAuditMethod).Add('');
  AdminDM.GetAuditMethodList(CxComboBoxItems(ColAuditMethod));
  SetCxComboBoxDropdownRows(ColAuditMethod);

  AdminDM.GetStatusList(CxComboBoxItems(ColStatus));
  AdminDM.GetResponderTypeList(CxComboBoxItems(ColResponderType));
  AdminDM.GetResponderKindList(CxComboBoxItems(ColResponderKind));
end;

procedure TCallCenterForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TCallCenterForm.OpenDataSets;
begin
  inherited;
  TZData.Open;
  Contacts.Open;
  StatusTranslations.Open;
end;

procedure TCallCenterForm.ActivatingNow;
begin
  inherited;
  CCInserted := False;
  StatusTransInserted := False;
  ContactInserted := False;
end;

procedure TCallCenterForm.ActiveCallCentersFilterClick(Sender: TObject);
var
  FSelectedID: String;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('cc_code').AsString;
  Data.Close;
  if ActiveCallCentersFilter.Checked then
    Data.CommandText := 'select * from call_center where active = 1 order by cc_code' else
      Data.CommandText := 'select * from call_center order by cc_code';
  Data.Open;
  Data.Locate('cc_code', FSelectedID, []);
end;

procedure TCallCenterForm.cbCallCenterSearchClick(Sender: TObject);
begin
  inherited;
    if cbCallCenterSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TCallCenterForm.CloseDataSets;
begin
  inherited;
  StatusTranslations.Close;
  Contacts.Close;
  TZData.Close;
end;

procedure TCallCenterForm.ContactGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(Contacts);
end;

procedure TCallCenterForm.StatusTranslationsGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(StatusTranslations);
end;

procedure TCallCenterForm.LoadContacts;
begin
  Contacts.Close;
  Contacts.CommandText := 'select * from call_center_contact where cc_code = ''' + Data.FieldByName('cc_code').AsString + '''';
  Contacts.Open;
end;

procedure TCallCenterForm.LoadStatusTranslations;
begin
  StatusTranslations.Close;
{  StatusTranslations.CommandText := 'select status_translation_id, cc_code, ' +
    'status, outgoing_status, active, explanation_code, responder_type, responder_kind ' +
    'from status_translation where cc_code = '''
    + Data.FieldByName('cc_code').AsString + ''''; }
  StatusTranslations.CommandText := 'select * from status_translation where cc_code = ''' +
    Data.FieldByName('cc_code').AsString  + '''';
  StatusTranslations.Open;
end;

procedure TCallCenterForm.DetailsBeforePost(DataSet: TDataSet);
var
  Center: string;
begin
  inherited;
  Center := Data.FieldByName('cc_code').AsString;
  if Trim(Center) = '' then
    raise Exception.Create('Please fill in a call center code first');
  DataSet.FieldByName('cc_code').AsString := Center;
  EditDataSet(Data);
  Data.FieldByName('modified_date').AsDateTime := Now;
  Data.Post;
end;

procedure TCallCenterForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
     inherited;
end;

procedure TCallCenterForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TCallCenterForm.StatusTranslationsAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TCallCenterForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
   // get the current values before editing
  AdminDM.StoreFieldValues(DataSet, ATableArray); //QM-83
end;

procedure TCallCenterForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
   CCInserted := True;
end;

procedure TCallCenterForm.StatusTranslationsBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, StatusTransArray); //QM-847
end;

procedure TCallCenterForm.StatusTranslationsBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  StatusTransInserted := True;
end;

procedure TCallCenterForm.ContactsBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  AdminDM.StoreFieldValues(DataSet, ContactsArray); //QM-847
end;

procedure TCallCenterForm.ContactsBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ContactInserted := True;
end;

procedure TCallCenterForm.StatusTranslationsAfterCancel(DataSet: TDataSet);
begin
  inherited;
  StatusTransInserted := False;
end;

procedure TCallCenterForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CCInserted := False;
end;

procedure TCallCenterForm.ContactsAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ContactInserted := False;
end;

procedure TCallCenterForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, ATableArray, 'call_center', 'cc_code', CCInserted);
end;

procedure TCallCenterForm.StatusTranslationsAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Data, StatusTransArray, 'status_translation', 'status_translation_id', StatusTransInserted);
end;

procedure TCallCenterForm.ContactsAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(DataSet, ContactsArray, 'call_center_contact', 'contact_id', ContactInserted);
end;

procedure TCallCenterForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  HpCheckList.Enabled := True;
  PopulateList;
  ActiveCallCentersFilter.Enabled := True;
  cbCallCenterSearch.Enabled := True;
end;
end.

