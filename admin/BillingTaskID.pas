unit BillingTaskID;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, AdminDmu, Vcl.ExtCtrls, Vcl.StdCtrls, MainFU;

type
  TBillingTaskIDForm = class(TBaseCxListForm)
    ColEmpID: TcxGridDBColumn;
    ColCustNumber: TcxGridDBColumn;
    ColCustName: TcxGridDBColumn;
    ColTaskID: TcxGridDBColumn;
    ColpcCode: TcxGridDBColumn;
    ColpcName: TcxGridDBColumn;
    ColEmpNumber: TcxGridDBColumn;
    ColEmpName: TcxGridDBColumn;
    ColShortName: TcxGridDBColumn;
    ColEmpManID: TcxGridDBColumn;
    ColEmpManNumber: TcxGridDBColumn;
    ColEmpManName: TcxGridDBColumn;
    ColEmpManShortName: TcxGridDBColumn;
    cbTaskidSearch: TCheckBox;
    droptemp: TADOQuery;
    ColSolomonpc: TcxGridDBColumn;
    procedure cbTaskidSearchClick(Sender: TObject);
  private
    { Private declarations }
    procedure LeavingNow; override;
    function IsReadOnly: Boolean; override;
     procedure SetReadOnly(const IsReadOnly: Boolean); override;
  public
    { Public declarations }
  end;

var
  BillingTaskIDForm: TBillingTaskIDForm;

implementation

{$R *.dfm}

{ TBillingTaskIDForm }


procedure TBillingTaskIDForm.cbTaskidSearchClick(Sender: TObject);
begin
   inherited;
    if cbTaskidSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;

end;

function TBillingTaskIDForm.IsReadOnly: Boolean;
begin
   Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TBillingTaskIDForm.LeavingNow;
begin
  inherited;
  dropTemp.ExecSQL;
end;



procedure TBillingTaskIDForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbTaskidSearch.Enabled := True;
end;

end.
