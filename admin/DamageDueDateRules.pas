unit DamageDueDateRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxCheckBox, cxDropDownEdit, OdDbUtils, OdCxUtils,
  cxDBLookupComboBox, cxNavigator, AdminDMu,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  TDamageDueDateRulesForm = class(TBaseCxListForm)
    ColPc_Code: TcxGridDBColumn;
    ColUtilityCoDamaged: TcxGridDBColumn;
    ColCalcMethod: TcxGridDBColumn;
    ColDays: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    Colrule_id: TcxGridDBColumn;
    GridViewColumn1: TcxGridDBColumn;
    ProfitCenters: TADODataSet;
    UtilityCompanys: TADODataSet;
    CalcMethods: TADODataSet;
    cbDamageDueDateSearch: TCheckBox;
    ActiveDueDateFilter: TCheckBox;
    procedure ActiveDueDateFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    DamageddInserted: Boolean;
    procedure LoadLookUpLists;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure RefreshNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  DamageDueDateRulesForm: TDamageDueDateRulesForm;

implementation

uses OdMiscUtils;

{$R *.dfm}

{ TDamageDueDateForm }

procedure TDamageDueDateRulesForm.RefreshNow;
begin
  ProfitCenters.Requery;
  UtilityCompanys.Requery;
  CalcMethods.Requery;
  LoadLookupLists;
end;

procedure TDamageDueDateRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbDamageDueDateSearch.Enabled := True;
  ActiveDueDateFilter.Enabled := True;
end;

procedure TDamageDueDateRulesForm.OpenDataSets;
begin
  inherited;
  UtilityCompanys.Open;
  ProfitCenters.Open;
  CalcMethods.Open;
  LoadLookupLists;
end;

procedure TDamageDueDateRulesForm.CloseDataSets;
begin
  inherited;
  UtilityCompanys.Close;
  ProfitCenters.Close;
  CalcMethods.Close;
end;

procedure TDamageDueDateRulesForm.ActiveDueDateFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('rule_id').AsInteger;
  Data.Close;
  if ActiveDueDateFilter.Checked then
    Data.CommandText := 'select * from damage_due_date_rule where active = 1' else
      Data.CommandText := 'select * from damage_due_date_rule';
  Data.Open;
  Data.Locate('rule_id', FSelectedID, []);
end;

procedure TDamageDueDateRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DamageddInserted := False;
end;

procedure TDamageDueDateRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'damage_due_date_rule', 'rule_id', DamageddInserted);
end;

procedure TDamageDueDateRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TDamageDueDateRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  DamageddInserted := True;
end;

procedure TDamageDueDateRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
   DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TDamageDueDateRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TDamageDueDateRulesForm.ActivatingNow;
begin
  inherited;
  DamageddInserted := False;
end;

procedure TDamageDueDateRulesForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TDamageDueDateRulesForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TDamageDueDateRulesForm.LoadLookUpLists;
begin
  GetDataSetFieldValueList(CxComboBoxItems(ColUtilityCoDamaged), UtilityCompanys, 'description', '*');
  GetDataSetFieldValueList(CxComboBoxItems(ColPc_Code), ProfitCenters, 'pc_code', '*');
  GetDataSetFieldValueList(CxComboBoxItems(ColCalcMethod), CalcMethods, 'code', '');
end;

end.
