inherited HALEPRsForm: THALEPRsForm
  Caption = 'EPRs'
  ClientHeight = 513
  ClientWidth = 732
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 392
    Width = 732
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 732
    Height = 34
  end
  inherited Grid: TcxGrid
    Top = 34
    Width = 732
    Height = 358
    inherited GridView: TcxGridDBTableView
      OptionsData.Appending = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ColCallCenter: TcxGridDBColumn
        Caption = 'call center'
        DataBinding.FieldName = 'call_center'
        Options.Editing = False
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Options.Editing = False
      end
      object ColOldest: TcxGridDBColumn
        DataBinding.FieldName = 'oldest'
        Options.Editing = False
      end
      object ColNewest: TcxGridDBColumn
        DataBinding.FieldName = 'newest'
        Options.Editing = False
      end
      object ColCount: TcxGridDBColumn
        DataBinding.FieldName = 'count'
        Options.Editing = False
      end
      object ColAgeInDays: TcxGridDBColumn
        Caption = 'age in days'
        DataBinding.FieldName = 'AgeInDays'
        Options.Editing = False
      end
      object ColAgeInHours: TcxGridDBColumn
        Caption = 'age in hours'
        DataBinding.FieldName = 'AgeInHours'
        Options.Editing = False
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 400
    Width = 732
    Height = 113
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Hal Module'
        DataBinding.FieldName = 'hal_module'
        Options.Editing = False
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 662
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select call_center, status, min(insert_date) as oldest, max(inse' +
      'rt_date) as newest, count(*) as count,'#13#10' datediff(d,min(insert_d' +
      'ate),getdate()) as AgeInDays,datediff(hh,min(insert_date),getdat' +
      'e()) as AgeInHours '#13#10'from epr_response group by call_Center, sta' +
      'tus'
    FieldDefs = <
      item
        Name = 'call_center'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'status'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'oldest'
        Attributes = [faReadonly, faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'newest'
        Attributes = [faReadonly, faFixed]
        DataType = ftDateTime
      end
      item
        Name = 'count'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end
      item
        Name = 'AgeInDays'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end
      item
        Name = 'AgeInHours'
        Attributes = [faReadonly, faFixed]
        DataType = ftInteger
      end>
    StoreDefs = True
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select hal_module, emails  from hal where hal_module = '#39'epr_resp' +
      'onse'#39
    Parameters = <>
    Left = 560
    Top = 80
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 624
    Top = 80
  end
end
