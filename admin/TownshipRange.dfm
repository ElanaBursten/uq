inherited TownshipRangeForm: TTownshipRangeForm
  Caption = 'Township Range Routing Setup'
  ClientWidth = 738
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 738
    Height = 60
    object Label1: TLabel
      Left = 8
      Top = 33
      Width = 49
      Height = 13
      Caption = 'Township:'
    end
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Baseline:'
    end
    object Label3: TLabel
      Left = 264
      Top = 5
      Width = 265
      Height = 45
      AutoSize = False
      Caption = 
        'We don'#39't yet know what the actual baselines are called and which' +
        ' are used; please use the state abbreviation in the meantime - C' +
        'O, AL, etc.'
      WordWrap = True
    end
    object TownshipPrefix: TEdit
      Left = 72
      Top = 32
      Width = 81
      Height = 21
      TabOrder = 1
    end
    object SearchBtn: TButton
      Left = 168
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Show'
      TabOrder = 2
      OnClick = SearchBtnClick
    end
    object EditBaseCode: TEdit
      Left = 72
      Top = 8
      Width = 81
      Height = 21
      TabOrder = 0
    end
    object chkboxTownshipSearch: TCheckBox
      Left = 579
      Top = 40
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 3
      OnClick = chkboxTownshipSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 60
    Width = 738
    Height = 340
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      OptionsView.Indicator = True
      object ColBaseCode: TcxGridDBColumn
        Caption = 'Baseline/State'
        DataBinding.FieldName = 'basecode'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 105
      end
      object ColTownship: TcxGridDBColumn
        Caption = 'Township'
        DataBinding.FieldName = 'township'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 151
      end
      object ColRange: TcxGridDBColumn
        Caption = 'Range'
        DataBinding.FieldName = 'range'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 79
      end
      object ColSection: TcxGridDBColumn
        Caption = 'Section'
        DataBinding.FieldName = 'section'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 125
      end
      object ColArea: TcxGridDBColumn
        DataBinding.FieldName = 'Area'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownRows = 18
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.KeyFieldNames = 'area_id'
        Properties.ListColumns = <
          item
            FieldName = 'area_name'
          end>
        Properties.ListOptions.ShowHeader = False
        Width = 135
      end
      object ColCallCenter: TcxGridDBColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'call_center'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 18
        Width = 122
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 
      'select basecode, township, range, section, area_id, call_center ' +
      #13#10'from map_trs'#13#10'where basecode = :base'#13#10' and township LIKE :pref' +
      'ix'#13#10'ORDER BY basecode, call_center, township, range, section'
    Parameters = <
      item
        Name = 'base'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = '1%'
      end>
    Top = 120
    object Databasecode: TStringField
      FieldName = 'basecode'
      Required = True
      Size = 10
    end
    object Datatownship: TStringField
      FieldName = 'township'
      Required = True
      Size = 5
    end
    object Datarange: TStringField
      FieldName = 'range'
      Required = True
      Size = 6
    end
    object Datasection: TStringField
      FieldName = 'section'
      Required = True
      Size = 6
    end
    object Dataarea_id: TIntegerField
      FieldName = 'area_id'
    end
    object DataArea: TStringField
      FieldKind = fkLookup
      FieldName = 'Area'
      LookupDataSet = Area
      LookupKeyFields = 'area_id'
      LookupResultField = 'area_name'
      KeyFields = 'area_id'
      Size = 40
      Lookup = True
    end
    object Datacall_center: TStringField
      FieldName = 'call_center'
    end
  end
  object Area: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select map.map_name, map.state, map.county, '#13#10' area.area_id, are' +
      'a.area_name,'#13#10' area.locator_id'#13#10' from area'#13#10'  inner join map on ' +
      'area.map_id=map.map_id'#13#10'order by area.area_name'
    Parameters = <>
    Left = 144
    Top = 112
    object Areamap_name: TStringField
      FieldName = 'map_name'
      Size = 30
    end
    object Areastate: TStringField
      FieldName = 'state'
      Size = 5
    end
    object Areacounty: TStringField
      FieldName = 'county'
      Size = 50
    end
    object Areaarea_id: TAutoIncField
      FieldName = 'area_id'
      ReadOnly = True
    end
    object Areaarea_name: TStringField
      FieldName = 'area_name'
      Size = 40
    end
    object Arealocator_id: TIntegerField
      FieldName = 'locator_id'
    end
  end
  object AreaDS: TDataSource
    DataSet = Area
    Left = 120
    Top = 160
  end
end
