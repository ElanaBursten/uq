unit Hierarchy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, Grids, DBGrids, Buttons, OdEmbeddable,
  DBCtrls, ComCtrls, Menus, ImgList, cxGraphics, cxCustomData, cxStyles, cxTL,
  cxControls, cxInplaceContainer, cxTLData, cxDBTL, cxMaskEdit, cxCheckBox,
  cxLookAndFeels, cxLookAndFeelPainters, cxTextEdit,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore,
  System.ImageList, StrUtils, dxScrollbarAnnotations, dxSkinBasic, cxFilter, cxTLdxBarBuiltInMenu;

type
  THierarchyForm = class(TEmbeddableForm)
    Panel1: TPanel;
    TreeDataSet: TADODataSet;
    TreeDataSource: TDataSource;
    Splitter: TSplitter;
    SearchResultsPanel: TPanel;
    GridDataSource: TDataSource;
    GridDataSet: TADODataSet;
    TypeRef: TADODataSet;
    TypeRefDS: TDataSource;
    SearchResultGrid: TDBGrid;
    CriteriaPanel: TPanel;
    FindUpdateButton: TButton;
    ClearButton: TButton;
    EmployeeSearchEdit: TEdit;
    FirstNameSearchEdit: TEdit;
    LastNameSearchEdit: TEdit;
    ShortNameSearchEdit: TEdit;
    TypeComboBox: TComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ChangeLogSheet: TTabSheet;
    Panel4: TPanel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EditButton: TButton;
    ChangeLogMemo: TMemo;
    ShowActiveCheckBox: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    DBText7: TDBText;
    DBText8: TDBText;
    Label7: TLabel;
    DBText9: TDBText;
    Label8: TLabel;
    Label9: TLabel;
    DBText10: TDBText;
    NotificationEmailData: TADODataSet;
    NotificationEmailDataDS: TDataSource;
    TreePopup: TPopupMenu;
    ExpandAll: TMenuItem;
    ContractAll: TMenuItem;
    DBText11: TDBText;
    Label10: TLabel;
    ProfitCenterCombo: TComboBox;
    SetProfitCenter: TButton;
    SetReprPC: TADOCommand;
    Label11: TLabel;
    ProblemsSheet: TTabSheet;
    ProblemsGrid: TDBGrid;
    ProblemsDS: TDataSource;
    ProblemsSP: TADOStoredProc;
    Panel2: TPanel;
    ProblemEditAboveButton: TButton;
    ProblemEditEmpScreenButton: TButton;
    Label12: TLabel;
    ImageList: TImageList;
    ExpandNodeMenuItem: TMenuItem;
    ContractNodeMenuItem: TMenuItem;
    SplitterBottom: TSplitter;
    NotificationEmailGrid: TDBGrid;
    ADNameSearchEdit: TEdit;
    ADPSearchEdit: TEdit;
    Panel5: TPanel;
    TreeList: TcxDBTreeList;
    EmployeeColumn: TcxDBTreeListColumn;
    ActiveColumn: TcxDBTreeListColumn;
    EmpNumColumn: TcxDBTreeListColumn;
    TypeColumn: TcxDBTreeListColumn;
    ReprPCColumn: TcxDBTreeListColumn;
    PayrollPCCodeColumn: TcxDBTreeListColumn;
    Label16: TLabel;
    Label17: TLabel;
    Panel3: TPanel;
    cbEmpTreeSearch: TCheckBox;
    TomQuery: TADOQuery;
    procedure FindUpdateButtonClick(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SearchResultGridDblClick(Sender: TObject);
    procedure EmployeeSearchEditKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ShowActiveCheckBoxClick(Sender: TObject);
    procedure EditButtonClick(Sender: TObject);
    procedure TreeDataSetBeforePost(DataSet: TDataSet);
    procedure ExpandAllClick(Sender: TObject);
    procedure ContractAllClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeDataSetBeforeOpen(DataSet: TDataSet);
    procedure SetProfitCenterClick(Sender: TObject);
    procedure GridDataSetBeforeOpen(DataSet: TDataSet);
    procedure ExpandAllButtonClick(Sender: TObject);
    procedure ProblemsSPAfterOpen(DataSet: TDataSet);
    procedure ProblemsGridDblClick(Sender: TObject);
    procedure ProblemEditAboveButtonClick(Sender: TObject);
    procedure ProblemEditEmpScreenButtonClick(Sender: TObject);
    procedure TreeListFocusedNodeChanged(Sender: TcxCustomTreeList;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure TreeListCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
    procedure ReprPCColumnGetDisplayText(Sender: TcxTreeListColumn;
      ANode: TcxTreeListNode; var Value: string);
    procedure TreeListDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ExpandNodeMenuItemClick(Sender: TObject);
    procedure ContractNodeMenuItemClick(Sender: TObject);
    procedure cbEmpTreeSearchClick(Sender: TObject);
    procedure TreeListFindPanelVisibilityChanged(Sender: TObject;
      const AVisible: Boolean);
    procedure TreeListFilterNode(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var Accept: Boolean);
    procedure TreeListMoveTo(Sender: TcxCustomTreeList;
      AttachNode: TcxTreeListNode; AttachMode: TcxTreeListNodeAttachMode;
      Nodes: TList; var IsCopy, Done: Boolean);
    procedure TreeDataSetAfterPost(DataSet: TDataSet);
    procedure TreeDataSetBeforeEdit(DataSet: TDataSet);
    procedure TreeListBeginDragNode(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var Allow: Boolean);
  private
    FSelectedEmp: Integer;
    AllowDrag: Boolean;
    SelNode:  TcxTreeListNode;
    AdUserName: String;
    ShortName: String;
    function GetEffectivePC(Node: TcxTreeListNode): string;
    function GetDirectPC(Node: TcxTreeListNode): string;
    procedure SetupControls;
    procedure MsgDlgNoSystemSound(DlgText: String; var Cancel: Boolean);
  public
    function MakeSQL(EmpID: Integer): string;
    procedure PopulateTypeComboBox;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure RefreshNow; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure EditNow; override;
  end;


implementation

{$R *.dfm}

uses AdminDMu, MainFU, OdHourglass, OdMiscUtils, OdADOUtils, OdDbUtils;

type THackGrid = class(TDBGrid);

procedure THierarchyForm.OpenDataSets;
var
  Cursor: IInterface;
  WherePos: Integer;
begin
  Cursor := ShowHourGlass;
  TypeRef.Open;

  TreeDataSet.Close;
  WherePos := Pos('where', string(TreeDataSet.CommandText));
  if WherePos > 0 then
    TreeDataSet.CommandText := Copy(TreeDataSet.CommandText, 1, WherePos - 1);

  if ShowActiveCheckBox.Checked then
    TreeDataSet.CommandText := TreeDataSet.CommandText + 'where active = 1';
  TreeDataSet.Open;
  TreeDataSet.Locate('emp_id', FSelectedEmp, []);

  TreeDataSource.Enabled := True;    // avoid the TreeList finding the record before the locate

  ProblemsSP.Open;
  inherited;
end;

procedure THierarchyForm.CloseDataSets;
begin
  inherited;
  FSelectedEmp := TreeDataSet.FieldByName('emp_id').AsInteger;
  TypeRef.Open;
  TreeDataSource.Enabled := False;
  TreeDataSet.Close;
  GridDataSet.Close;
  NotificationEmailData.Close;
  ProblemsSP.Close;
end;

procedure THierarchyForm.PopulateTypeComboBox;
var
  TypeCode: string;
  RefId: Integer;
begin
  TypeComboBox.Clear;
  TypeRef.First;
  while not TypeRef.Eof do begin
    TypeCode := TypeRef.FieldByName('code').AsString;
    RefId := TypeRef.FieldByName('ref_id').AsInteger;
    TypeComboBox.AddItem(TypeCode, TObject(RefId));
    TypeRef.Next;
  end;
end;

procedure THierarchyForm.ProblemsSPAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.IsEmpty then
    ProblemsSheet.ImageIndex := -1
  else
    ProblemsSheet.ImageIndex := 0;
end;

function THierarchyForm.MakeSQL(EmpID: Integer): string;
Type
 TDynArray = Array of Integer;
var
  SQL: string;
  RefId: Integer;
  TypeArray: TDynArray;
  I: Integer;
  OrderBy: TStringList;

function FindMatchText(Strings: TStrings; const SubStr: string): TDynArray;
var
 I: Integer;
 Len: Integer;
begin
  Len := 0;
  for I := 0 to Strings.Count-1 do
  begin
    if ContainsText(Strings[I], SubStr) then
    begin
      Inc(Len);
      SetLength(Result, Len);
      Result[Len-1] := I;
    end;
  end;
  if Len = 0 then
  begin
    SetLength(Result, 1);
    Result[0] := -1;
  end;
end;

begin
  SQL := Format('select h_emp_id, h_short_name, h_first_name, h_last_name, h_emp_number, h_adp_login, ' +
  'h_ad_username, h_type_id, h_report_level, h_report_to from get_report_hier5(%d,0) where 1=1 ', [EmpID]);
  try
   OrderBy := TStringList.Create;
   if EmployeeSearchEdit.Text > '' then
   begin
     OrderBy.Add('h_emp_number');
     SQL := SQL + Format('and h_emp_number like %s', [QuotedStr(EmployeeSearchEdit.Text + '%')]);
   end;

   if FirstNameSearchEdit.Text > '' then
   begin
     OrderBy.Add('h_first_name');
     SQL := SQL + Format('and h_first_name like %s ', [QuotedStr(FirstNameSearchEdit.Text + '%')]);
   end;

   if LastNameSearchEdit.Text > '' then
   begin
     OrderBy.Add('h_last_name');
     SQL := SQL + Format('and h_last_name like %s ', [QuotedStr(LastNameSearchEdit.Text + '%')]);
   end;

   if ShortNameSearchEdit.Text > '' then
     SQL := SQL + Format('and h_short_name like %s ', [QuotedStr(ShortNameSearchEdit.Text + '%')]);

   if ADNameSearchEdit.Text > '' then
   begin
     Orderby.Add('h_ad_username');
     SQL := SQL + Format('and h_ad_username like %s ', [QuotedStr(ADNameSearchEdit.Text + '%')]);
   end;

   if ADPSearchEdit.Text > '' then
   begin
     Orderby.Add('h_adp_login');
     SQL := SQL + Format('and h_adp_login like %s ', [QuotedStr(ADPSearchEdit.Text + '%')]);
   end;

   if (TypeComboBox.Text > '') then
   begin
     Orderby.Add('h_type_id');
     If TypeComboBox.ItemIndex = -1 then
     begin
       TypeArray := FindMatchText(TypeCombobox.Items,TypeComboBox.Text);
       if TypeArray[0] = -1 then
         SQL := SQL + Format('and h_type_id = %d ', [-1])
       else
       begin
         for I := Low(TypeArray) to High(TypeArray) do
         begin
           RefId := Integer(TypeComboBox.Items.Objects[TypeArray[I]]);
           if I=0 then
           begin
             SQL := SQL + Format('and (h_type_id = %d ', [RefId]);
             continue;
           end;
           SQL := SQL + Format('or h_type_id = %d ', [RefId]);
         end;
         SQL := SQL + ') ';
         SetLength(TypeArray, 0);
       end;
     end else
     begin
       RefId := Integer(TypeComboBox.Items.Objects[TypeComboBox.ItemIndex]);
       SQL := SQL + Format('and h_type_id = %d ', [RefID]);
     end;
   end;

   if ShowActiveCheckBox.Checked then
     SQL := SQL + 'and h_active=1 ';

   SQL := SQL + 'order by h_short_name';
   if OrderBy.Count > 0 then
   begin
     SQL := SQL + ',';
     SQL := SQL + OrderBy.CommaText;
   end;
   Result := SQL;
  finally
    OrderBy.Free;
  end;
end;

procedure THierarchyForm.FindUpdateButtonClick(Sender: TObject);
var
  CurrentID: Integer;
begin
  CurrentID := TreeDataSet.FieldByName('emp_id').AsInteger;

  GridDataSet.Close;
  GridDataSet.CommandText := MakeSQL(CurrentID);
  GridDataSet.Open;
end;

procedure THierarchyForm.ActivatingNow;
begin
  inherited;
  TreeList.FullCollapse;
end;


procedure THierarchyForm.ClearButtonClick(Sender: TObject);
begin
  TypeComboBox.ClearSelection;
  TypeComboBox.Text := '';
  EmployeeSearchEdit.Text := '';
  FirstNameSearchEdit.Text := '';
  LastNameSearchEdit.Text := '';
  ShortNameSearchEdit.Text := '';
  ADPSearchEdit.Text := '';
  ADNameSearchEdit.Text := '';

  GridDataSet.Close;
  GridDataSet.CommandText := 'select * from get_hier3(0,0)';
  GridDataSet.Open;
end;

procedure THierarchyForm.FormShow(Sender: TObject);
begin
  OpenDataSets;
  SetupControls;
end;

procedure THierarchyForm.SetupControls;
begin
  TreeList.OptionsBehavior.DragExpand := True;
  TreeList.OptionsBehavior.Sorting := True;
  TreeList.OptionsBehavior.GoToNextCellOnTab := True;
  TreeList.OptionsBehavior.FocusCellOnCycle := True;
  if AdminDM.UserHasRestrictedAccess then begin
    TreeList.DragMode := dmManual;
    TreeList.OptionsData.Editing := False;
    TreeList.OptionsBehavior.ImmediateEditor := False;
    SetProfitCenter.Enabled := False;
    ProblemEditEmpScreenButton.Enabled := False;
    ProblemEditAboveButton.Enabled := False;
  end else begin
    TreeList.DragMode := dmAutomatic;
    TreeList.OptionsData.Editing := True;
    TreeList.OptionsBehavior.ImmediateEditor := True;
    SetProfitCenter.Enabled := True;
    ProblemEditEmpScreenButton.Enabled := True;
    ProblemEditAboveButton.Enabled := True;
  end;
end;

procedure THierarchyForm.SearchResultGridDblClick(Sender: TObject);
var
  EmpID: Longint;
  Node: TcxTreeListNode;
  ParentNode: TcxTreeListNode;
  NodeText: Variant;
begin
  //qm-225 changed to prevent full tree from opening on search BP
  //qm-345 prevent expanding children of found node
  if TreeList.Selections[0] = nil then  Exit;
  ParentNode := Treelist.Selections[0];
  EmpID := GridDataSet.FieldByName('h_emp_id').AsInteger;
  TreeDataSet.Locate('emp_id', EmpID, []);
  NodeText := TreeList.ColumnByName('EmployeeColumn').Value;
  TreeList.FullCollapse;
  ParentNode.Expand(False);
  Node := TreeList.FindNodeByText(NodeText,TreeList.ColumnByName('EmployeeColumn'));
  TreeList.FocusedNode := Node;
end;

procedure THierarchyForm.EmployeeSearchEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then FindUpdateButtonClick(Sender);
end;

procedure THierarchyForm.SpeedButton1Click(Sender: TObject);
begin
  TypeComboBox.ClearSelection;
  TypeComboBox.Text := '';
end;

procedure THierarchyForm.ShowActiveCheckBoxClick(Sender: TObject);
begin
  CloseDataSets;
  OpenDataSets;
end;

procedure THierarchyForm.EditButtonClick(Sender: TObject);
begin
  MainForm.ShowEmployee(TreeDataSet.FieldByName('emp_id').AsInteger);
end;

procedure THierarchyForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure THierarchyForm.TreeDataSetBeforePost(DataSet: TDataSet);
var
  ReportToField: TField;
  EmpID: Integer;
  OldReportTo, NewReportTo: Integer;
  CheckTomExists: String;
  UpdateTomorrowSQL: String;
  Line: string;
begin
  inherited;
  ReportToField := DataSet.FieldByName('report_to');
  EmpID := DataSet.FieldByName('emp_id').AsInteger;

  OldReportTo := 0;
  if VarIsNumeric(ReportToField.OldValue) then
    OldReportTo := ReportToField.OldValue;

  NewReportTo := 0;
  if VarIsNumeric(ReportToField.NewValue) then
    NewReportTo := ReportToField.NewValue;

  ChangeLogSheet.TabVisible := True;
  Line := Format('Locator ID %d, formerly reported to %d, now reports to %d',
     [EmpID, OldReportTo, NewReportTo] );
    ChangeLogMemo.Lines.Add(Line);

  //QM-473 B.P.
  if NewReportTo > 0 then
  begin
    try
      CheckTomExists := 'select active from employee where try_convert(int, dialup_user) = ' + InttoStr(EmpID) + ' and type_id = 1245';
      TomQuery.SQL.Text := CheckTomExists;
      TomQuery.Open;
      if not TomQuery.EOF then
      begin
        UpdateTomorrowSQL := Format('Update employee set report_to = %d where ' +
         'try_convert(int, dialup_user) = %d and type_id = ' + QuotedStr('1245'), [NewReportTo, EmpID]);
        ExecuteQuery(TreeDataSet.Connection, UpdateTomorrowSQL);
      end;
      TomQuery.Close;
    except
      On E:Exception do
      begin
        TomQuery.Close;
        raise;
      end;
    end;
  end;
end;

procedure THierarchyForm.PopulateDropdowns;
begin
  inherited;
  PopulateTypeComboBox;
end;

procedure THierarchyForm.ExpandAllClick(Sender: TObject);
begin
  TreeList.FullExpand;
end;

procedure THierarchyForm.ContractAllClick(Sender: TObject);
begin
  TreeList.FullCollapse;
end;

procedure THierarchyForm.ExpandNodeMenuItemClick(Sender: TObject);
begin
  if TreeList.SelectionCount > 0 then
    TreeList.Selections[0].Expand(True);
end;

procedure THierarchyForm.ContractNodeMenuItemClick(Sender: TObject);
begin
  if TreeList.SelectionCount > 0 then
    TreeList.Selections[0].Collapse(True);
end;

procedure THierarchyForm.TreeListBeginDragNode(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Allow: Boolean);
begin
  inherited;
  AdUserName := TreeDataSet.FieldByName('ad_username').AsString;
  Shortname :=  TreeDataSet.FieldByName('short_name').AsString;
  SelNode := Treelist.FocusedNode;
end;

procedure THierarchyForm.TreeListCustomDrawDataCell(
  Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
var
  DirectPC: string;
  EffectivePC: string;
  ParentPC: string;
  ReprPC: string;
begin
  if AViewInfo.Column.Name <> 'ReprPCColumn' then
    Exit;

  ReprPC := VarToString(AViewInfo.DisplayValue);
  if IsEmpty(ReprPC) then begin
    ReprPC := GetEffectivePC(AViewInfo.Node);
    if IsEmpty(ReprPC) then
      ReprPC := '?';
  end;

  DirectPC := GetDirectPC(AViewInfo.Node);
  EffectivePC := GetEffectivePC(AViewInfo.Node);
  ParentPC := GetEffectivePC(AViewInfo.Node.Parent);

  // If I am specified, and am the same as my parent, that is a problem,
  // so show bold red
  if NotEmpty(DirectPC) and (DirectPC = ParentPC) then begin
    ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
    ACanvas.Font.Color := clRed;
  end else begin
    // Otherwise, highlight in bold to show I am different
    if Assigned(AViewInfo.Node.Parent) and (ParentPC <> ReprPC) and (ReprPC <> '?') then
      ACanvas.Font.Style := ACanvas.Font.Style + [fsBold]
    else
      ACanvas.Font.Color := clLtGray;
  end;
  ADone := False;
end;

procedure THierarchyForm.TreeListDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Sender = TreeList) and AllowDrag;
end;

procedure THierarchyForm.TreeListFindPanelVisibilityChanged(Sender: TObject;
  const AVisible: Boolean);
begin
  inherited;
  if not AVisible then
  begin
    Panel3.Visible := True;
    cbEmpTreeSearch.Checked := False;
  end;
end;

procedure THierarchyForm.cbEmpTreeSearchClick(Sender: TObject);
begin
  inherited;
  if cbEmpTreeSearch.Checked then
  begin
    Panel3.Visible := False;
    TreeList.ShowFindPanel;
  end;
end;

procedure THierarchyForm.TreeListFilterNode(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Accept: Boolean);
begin
  inherited;
  ANode.Expanded := True;
end;

procedure THierarchyForm.TreeListFocusedNodeChanged(
  Sender: TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  NotificationEmailData.Close;
  NotificationEmailData.Parameters[0].Value := TreeDataSet.FieldByName('emp_id').Value;
  NotificationEmailData.Open;
end;

procedure THierarchyForm.MsgDlgNoSystemSound(DlgText: String; var Cancel: Boolean);
const
  ico_Confirmation = MakeIntResource (102);
Var
  hRes: THandle;
  eVar: Array [1..512] of Char;
  MBParams: TMsgBoxParams;
begin
  ExpandEnvironmentStrings(PChar('%SystemRoot%\system32\user32.dll'),@eVar,512);
  hRes := LoadLibrary(Pchar(@eVar));
  If hRes > 0 then
  try
    With MBParams do
    begin
      cbSize             := SizeOf(MBParams);
      hwndOwner          := Application.MainForm.Handle;
      hInstance          := hRes;
      lpszIcon           := ico_confirmation;
      lpszText           := pchar(DlgText);
      lpszCaption        := PChar('');
      dwStyle            := MB_USERICON or MB_OKCANCEL;
      dwContextHelpId    := 0;
      lpfnMsgBoxCallback := nil;
      dwLanguageId       := LANG_NEUTRAL;
    end;
    Case Integer(MessageBoxIndirect(MBParams)) of
      IDOK :;
      IDCANCEL : Cancel := true;
    End;
  finally
    FreeLibrary(hRes);
  end;
end;

procedure THierarchyForm.TreeListMoveTo(Sender: TcxCustomTreeList;
  AttachNode: TcxTreeListNode; AttachMode: TcxTreeListNodeAttachMode;
  Nodes: TList; var IsCopy, Done: Boolean);
begin
  inherited;
  MsgDlgNoSystemSound('Move to ' + AttachNode.Texts[0] + '?', Done);
  if Done then Exit;

  If (AdUserName <> '') and (GetDirectPC(SelNode) = '') then
  begin
    if GetEffectivePC(AttachNode) = '' then
    MessageDlg('Warning: Profit Center is missing for this moved employee: ' +
      'adUserName ' + adUserName + ', Shortname ' + ShortName, mtWarning, [mbYes], 0);
  end;
end;

function THierarchyForm.GetDirectPC(Node: TcxTreeListNode): string;
begin
  if Node <> nil then
    Result := VarToString(Node.Values[ReprPCColumn.ItemIndex]);
end;

function THierarchyForm.GetEffectivePC(Node: TcxTreeListNode): string;
begin
  Result := GetDirectPC(Node);
  if IsEmpty(Result) and Assigned(Node) and Assigned(Node.Parent) then
    Result := GetEffectivePC(Node.Parent)
end;

procedure THierarchyForm.FormCreate(Sender: TObject);
begin
  inherited;
  ChangeLogSheet.TabVisible := False;
  AdminDM.GetProfitCenterList(ProfitCenterCombo.Items);
  FSelectedEmp := 2676;
  PageControl1.ActivePageIndex := 0;

  // Workaround for Delphi bug: missing vertical scrdoneollbar on TDBGrids. See http://qc.embarcadero.com/wc/qcmain.aspx?d=7527
  THackGrid(NotificationEmailGrid).ScrollBars := ssNone;
end;

procedure THierarchyForm.TreeDataSetAfterPost(DataSet: TDataSet);
var
 AIns: Boolean;
begin
  inherited;      //QM 847 track reportto changes
  AIns := False;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'employee', 'emp_id', AIns);
end;

procedure THierarchyForm.TreeDataSetBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure THierarchyForm.TreeDataSetBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('TypeLookup', TreeDataSet, 'type_id', TypeRef, 'ref_id', 'code');
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  TreeDataSet.FieldByName('TypeLookup').LookupCache := True;
end;

procedure THierarchyForm.RefreshNow;
begin
  inherited;
  AdminDM.GetProfitCenterList(ProfitCenterCombo.Items, True);
  TreeDataSet.Locate('emp_id', FSelectedEmp, []);
end;

procedure THierarchyForm.ReprPCColumnGetDisplayText(Sender: TcxTreeListColumn;
  ANode: TcxTreeListNode; var Value: string);
begin
  if IsEmpty(Value) then begin
    Value := GetEffectivePC(ANode);
    if IsEmpty(Value) then
      Value := '?';
  end;
end;

procedure THierarchyForm.SetProfitCenterClick(Sender: TObject);
var
  FSelectedID: Integer;
  s: String;

  procedure SetCurrentValue(PCValue: Variant);
  begin
    TreeDataSet.BeforePost := nil;
    EditDataSet(TreeDataSet);
    TreeDataSet.FieldByName('repr_pc_code').Value := PCValue;
    PostDataSet(TreeDataSet);
    TreeDataSet.BeforePost := TreeDataSetBeforePost;
  end;

begin
  inherited;
  Assert(TreeDataSet.Active and (not TreeDataSet.IsEmpty));
  if NotEmpty(ProfitCenterCombo.Text) then begin
    ChangeLogMemo.Lines.Add('Clearing all current settings but this one for PC: ' + ProfitCenterCombo.Text);
    SetReprPC.Parameters.ParamValues['PCCode'] := ProfitCenterCombo.Text;
    SetReprPC.Parameters.ParamValues['PCCode2'] := ProfitCenterCombo.Text;
    SetReprPC.Parameters.ParamValues['id'] := TreeDataSet.FieldByName('emp_id').AsInteger;
    SetReprPC.Execute;
    FSelectedEmp :=  TreeDataSet.FieldByName('emp_id').AsInteger;
    ShowMessage('You need to refresh the screen, to see the full result of this change.');

    s := 'repr_pc_code ' + ProfitCenterCombo.Text + ' set for emp_id ' + inttostr(FSelectedEmp);
    AdminDM.AdminChangeTable('employee', s, now); //QM-823
  end
  else
    SetCurrentValue(Null);
end;

procedure THierarchyForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbempTreeSearch.Enabled := True;
  FindUpdateButton.Enabled := True;
  ClearButton.Enabled := True;
  ShowActiveCheckBox.Enabled :=True;
  EditButton.Enabled := True;
  ProblemEditEmpScreenButton.Enabled := True;
  ProblemEditAboveButton.Enabled := True;
  EmployeeSearchEdit.Enabled := True;
  FirstNameSearchEdit.Enabled := True;
  LastNameSearchEdit.Enabled := True;
  ShortNameSearchEdit.Enabled := True;
  ADNameSearchEdit.Enabled := True;
  ADPSearchEdit.Enabled := True;
  TypeComboBox.Enabled := True;
  AllowDrag := not IsReadOnly;
end;

procedure THierarchyForm.GridDataSetBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddLookupField('TypeLookup', GridDataSet, 'h_type_id', TypeRef, 'ref_id', 'code');
  //EB QMANTWO-548  Set These fields to cache to minimize the delay (on non-keyed fields)
  GridDataSet.FieldByName('TypeLookup').LookupCache := True;
end;

procedure THierarchyForm.ExpandAllButtonClick(Sender: TObject);
begin
  if TreeList.SelectionCount > 0 then
    TreeList.Selections[0].Expand(True);
end;

procedure THierarchyForm.ProblemsGridDblClick(Sender: TObject);
var
  EmpID: Longint;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  EmpID := ProblemsSP.FieldByName('emp_id').AsInteger;
  TreeDataSet.Locate('emp_id', EmpID, []);
end;

procedure THierarchyForm.ProblemEditAboveButtonClick(Sender: TObject);
begin
  ProblemsGridDblClick(nil);
end;

procedure THierarchyForm.ProblemEditEmpScreenButtonClick(Sender: TObject);
begin
  MainForm.ShowEmployee(ProblemsSP.FieldByName('emp_id').AsInteger);
end;

end.

