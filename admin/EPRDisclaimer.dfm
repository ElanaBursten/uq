inherited EPRDisclaimerForm: TEPRDisclaimerForm
  Caption = 'EPR Disclaimer Editor'
  ClientWidth = 687
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 687
    Height = 25
    object chkboxDisclaimerSearch: TCheckBox
      Left = 24
      Top = 5
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxDisclaimerSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 25
    Width = 687
    Height = 375
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Enabled = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Enabled = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Enabled = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Enabled = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Enabled = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      object GridViewdisclaimer_id: TcxGridDBColumn
        Caption = 'Disclaimer ID'
        DataBinding.FieldName = 'disclaimer_id'
        Options.Editing = False
        Width = 72
      end
      object GridViewdisclaimer_text: TcxGridDBColumn
        Caption = 'Disclaimer Text'
        DataBinding.FieldName = 'disclaimer_text'
        PropertiesClassName = 'TcxMemoProperties'
        Width = 597
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from disclaimer'
    Left = 40
    Top = 248
  end
  inherited DS: TDataSource
    Left = 112
    Top = 248
  end
end
