inherited MissingAttachmentsForm: TMissingAttachmentsForm
  Caption = 'Missing Attachments'
  ClientHeight = 551
  ClientWidth = 733
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 463
    Width = 733
    Height = 8
    Cursor = crVSplit
    Align = alBottom
  end
  inherited TopPanel: TPanel
    Width = 733
    object Label1: TLabel
      Left = 158
      Top = 15
      Width = 103
      Height = 13
      Caption = '#days to attach date'
    end
    object SpinEditDateAdd: TSpinEdit
      Left = 267
      Top = 12
      Width = 39
      Height = 22
      MaxValue = 9
      MinValue = 1
      TabOrder = 0
      Value = 5
    end
    object btnDateAdd: TButton
      Left = 313
      Top = 11
      Width = 80
      Height = 22
      Caption = 'Requery'
      TabOrder = 1
      OnClick = btnDateAddClick
    end
    object chkboxMissingAttachmentsSearch: TCheckBox
      Left = 31
      Top = 15
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 2
      OnClick = chkboxMissingAttachmentsSearchClick
    end
  end
  inherited Grid: TcxGrid
    Width = 733
    Height = 422
    inherited GridView: TcxGridDBTableView
      object GridViewshort_name: TcxGridDBColumn
        DataBinding.FieldName = 'short_name'
        Width = 71
      end
      object GridViewCOLUMN1: TcxGridDBColumn
        Caption = 'Contact Phone'
        DataBinding.FieldName = 'COLUMN1'
        Width = 117
      end
      object GridViewPending: TcxGridDBColumn
        DataBinding.FieldName = 'Pending'
      end
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 471
    Width = 733
    Height = 80
    Align = alBottom
    TabOrder = 2
    OnExit = cxGrid1Exit
    object GridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      ScrollbarAnnotations.CustomAnnotations = <>
      OnEditKeyDown = GridDBTableViewEditKeyDown
      DataController.DataSource = dsHalConfig
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.ClearPersistentSelectionOnOutsideClick = True
      OptionsView.GroupByBox = False
      object ColModule: TcxGridDBColumn
        Caption = 'Module'
        DataBinding.FieldName = 'hal_module'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Width = 106
      end
      object ColEmails: TcxGridDBColumn
        Caption = 'Emails'
        DataBinding.FieldName = 'emails'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.OnEditValueChanged = ColEmailsPropertiesEditValueChanged
        Width = 869
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select short_name, coalesce(e.contact_phone, '#39'No Phone'#39'), '#13#10'coun' +
      't(attach_date) as '#39'Pending'#39#13#10'from attachment a with (nolock)'#13#10'jo' +
      'in employee e on e.emp_id = a.attached_by'#13#10'where cast(attach_dat' +
      'e as date) = '#13#10'   cast(dateadd(d,:startdateoffset, getdate()) as' +
      ' date) and upload_date is null'#13#10'group by short_name, e.contact_p' +
      'hone'#13#10'order by short_name'
    Parameters = <
      item
        Name = 'startdateoffset'
        DataType = ftInteger
        Size = 1
        Value = 7
      end>
    Left = 48
    Top = 160
  end
  inherited DS: TDataSource
    Top = 160
  end
  object HalConfig: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select hal_module, emails  from hal where hal_module = '#39'missing_' +
      'attachments'#39
    Parameters = <>
    Left = 396
    Top = 168
  end
  object dsHalConfig: TDataSource
    DataSet = HalConfig
    Left = 460
    Top = 152
  end
end
