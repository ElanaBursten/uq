object SaveQueryDlg: TSaveQueryDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Save Query'
  ClientHeight = 278
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 227
    Height = 14
    Caption = 'Name or Short Description for this Query:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object EdtShortDesc: TEdit
    Left = 8
    Top = 28
    Width = 360
    Height = 21
    MaxLength = 50
    TabOrder = 0
  end
  object SQL: TMemo
    Left = 8
    Top = 55
    Width = 360
    Height = 185
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = clMenu
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'SQL')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object SaveBtn: TButton
    Left = 216
    Top = 245
    Width = 75
    Height = 25
    Caption = 'Save'
    ModalResult = 1
    TabOrder = 2
  end
  object CancelBtn: TButton
    Left = 293
    Top = 246
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
