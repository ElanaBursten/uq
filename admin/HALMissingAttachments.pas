unit HALMissingAttachments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, cxTextEdit, Vcl.StdCtrls,
  Vcl.Samples.Spin, OdDbUtils, MainFu, AdminDmu;

type
  TMissingAttachmentsForm = class(TBaseCxListForm)
    Label1: TLabel;
    SpinEditDateAdd: TSpinEdit;
    btnDateAdd: TButton;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    GridDBTableView: TcxGridDBTableView;
    ColModule: TcxGridDBColumn;
    ColEmails: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    chkboxMissingAttachmentsSearch: TCheckBox;
    HalConfig: TADODataSet;
    dsHalConfig: TDataSource;
    GridViewshort_name: TcxGridDBColumn;
    GridViewCOLUMN1: TcxGridDBColumn;
    GridViewPending: TcxGridDBColumn;
    procedure chkboxMissingAttachmentsSearchClick(Sender: TObject);
    procedure btnDateAddClick(Sender: TObject);
    procedure GridDBTableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ColEmailsPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    function IsReadOnly: Boolean; override;
  end;

var
  MissingAttachmentsForm: TMissingAttachmentsForm;

implementation

{$R *.dfm}

procedure TMissingAttachmentsForm.ActivatingNow;
begin
  inherited;
  HalConfig.Open;
  SetReadOnly(True);
end;

procedure TMissingAttachmentsForm.btnDateAddClick(Sender: TObject);
begin
  inherited;
  CloseDataSets;
  OpenDataSets;
end;

procedure TMissingAttachmentsForm.chkboxMissingAttachmentsSearchClick(
  Sender: TObject);
begin
   inherited;
    if chkboxMissingAttachmentsSearch.Checked then
    Gridview.Controller.ShowFindPanel
  else
    Gridview.Controller.HideFindPanel;
end;

procedure TMissingAttachmentsForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TMissingAttachmentsForm.OpenDataSets;
begin
  HalConfig.Open;
  Data.Parameters.ParamByName('startdateoffset').Value := SpinEditDateAdd.Value;
  inherited;
end;

procedure TMissingAttachmentsForm.CloseDataSets;
begin
  inherited;
  HalConfig.Close;
end;

procedure TMissingAttachmentsForm.ColEmailsPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if Trim(TcxCustomEdit(Sender).EditValue) = '' then
  begin
    showmessage('emails is a required field');
    HalConfig.Cancel;
  end;
end;

procedure TMissingAttachmentsForm.cxGrid1Exit(Sender: TObject);
begin
  inherited;
  PostDataSet(HalConfig);
end;

procedure TMissingAttachmentsForm.GridDBTableViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  If not (dceEdit in gridDBTableView.DataController.EditState) then
  begin
  if (Key = VK_INSERT) or
    ((Key = VK_DELETE) or (Key = VK_DOWN)) or (Shift = []) or (Shift = [ssCtrl]) then
   Key := 0
  end;
end;

function TMissingAttachmentsForm.IsReadOnly: Boolean;
begin
  Result := False;
  MainFu.MainForm.RefreshButton.Visible := False;
end;

procedure TMissingAttachmentsForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  chkboxMissingAttachmentsSearch.Enabled := True;
  btnDateAdd.Enabled := True;
end;

end.
