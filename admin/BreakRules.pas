unit BreakRules;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, ActnList, StdCtrls, AdminDMu,
  OdExceptions, QMConst, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxDropDownEdit, cxBlobEdit,
  cxTextEdit, cxSpinEdit, cxCalendar, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TBreakRulesForm = class(TBaseCxListForm)
    Datarule_id: TAutoIncField;
    Datapc_code: TStringField;
    Databreak_needed_after: TIntegerField;
    Databreak_length: TIntegerField;
    Datarule_type: TStringField;
    Datarule_message: TStringField;
    Datamodified_date: TDateTimeField;
    Dataactive: TBooleanField;
    Datamessage_frequency: TIntegerField;
    BottomPanel: TPanel;
    InstructionLabel: TLabel;
    Databutton_text: TStringField;
    ColRuleId: TcxGridDBColumn;
    ColPCCode: TcxGridDBColumn;
    ColBreakLength: TcxGridDBColumn;
    ColRuleType: TcxGridDBColumn;
    ColBreakNeededAfter: TcxGridDBColumn;
    ColRuleMessage: TcxGridDBColumn;
    ColButtonText: TcxGridDBColumn;
    ColMessageFrequency: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    cbBReakRulesSearch: TCheckBox;
    ActiveBreakRulesFilter: TCheckBox;
    procedure Databreak_needed_afterGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure Databreak_needed_afterSetText(Sender: TField;
      const Text: string);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataBeforePost(DataSet: TDataSet);
    procedure cbBReakRulesSearchClick(Sender: TObject);
    procedure ActiveBreakRulesFilterClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    BreakRulesInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure LeavingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure OpenDataSets; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses OdCxUtils, OdDbUtils, OdMiscUtils;

{$R *.dfm}

procedure TBreakRulesForm.PopulateDropdowns;
begin
  inherited;
  AdminDM.GetProfitCenterList(CxComboBoxItems(ColPCCode), False);

  CxComboBoxItems(ColRuleType).Clear;
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeBreakAfter);
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeBreakEvery);
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeSubmit);
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeSubmitDisagree);
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeManagerAlteredTime);
  CxComboBoxItems(ColRuleType).Add(BreakRuleTypeLunch);
end;

procedure TBreakRulesForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbBreakRulesSearch.Enabled := True;
  ActiveBreakRulesFilter.Enabled := True;
end;

procedure TBreakRulesForm.Databreak_needed_afterGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
var
  BreakHours, BreakMinutes: Integer;
begin
  BreakHours := Sender.AsInteger div 60;
  BreakMinutes := Sender.AsInteger mod 60;
  Text := FormatDateTime('hh:mm', EncodeTime(BreakHours, BreakMinutes, 0, 0));
end;

procedure TBreakRulesForm.Databreak_needed_afterSetText(Sender: TField;
  const Text: string);
var
  BreakHours, BreakMinutes, Sec, MiliSec: Word;
begin
  DecodeTime(StrToTime(Text), BreakHours, BreakMinutes, Sec, MiliSec);
  Sender.Value := BreakHours * 60 + BreakMinutes;
end;

procedure TBreakRulesForm.DataNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('active').Value := True;
  DataSet.FieldByName('modified_date').Value := Now;
end;

procedure TBreakRulesForm.GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TBreakRulesForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TBreakRulesForm.OpenDataSets;
begin
  inherited;
  GridView.DataController.FocusedRowIndex := 0;
end;

procedure TBreakRulesForm.ActivatingNow;
begin
  inherited;
  BreakRulesInserted := False;
end;

procedure TBreakRulesForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TBreakRulesForm.ActiveBreakRulesFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
begin
  inherited;
  FSelectedID := Data.FieldByName('rule_id').AsInteger;
  Data.Close;
  if ActiveBreakRulesFilter.Checked then
    Data.CommandText := 'select * from break_rules where active = 1' else
      Data.CommandText := 'select * from break_rules';
  Data.Open;
  Data.Locate('rule_id', FSelectedID, []);
end;

procedure TBreakRulesForm.cbBReakRulesSearchClick(Sender: TObject);
begin
  inherited;
  if cbBreakRulesSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TBreakRulesForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  BreakRulesInserted := False;
end;

procedure TBreakRulesForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'break_rules', 'rule_id', BreakRulesInserted);
end;

procedure TBreakRulesForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TBreakRulesForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  BreakRulesInserted := True;
end;

procedure TBreakRulesForm.DataBeforePost(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('rule_type').AsString = BreakRuleTypeSubmit then begin
    if not DataSet.FieldByName('break_length').IsNull then
      DataSet.FieldByName('break_length').Clear;
    if not DataSet.FieldByName('break_needed_after').IsNull then
      DataSet.FieldByName('break_needed_after').Clear;
    if DataSet.FieldByName('button_text').AsString = '' then
      raise EOdDataEntryError.Create('Please supply text to appear on the button ' +
        'of the dialog box. For multiple buttons, separate each caption with a comma.');
  end;

  if (DataSet.FieldByName('rule_type').AsString = BreakRuleTypeBreakEvery) or
    (DataSet.FieldByName('rule_type').AsString = BreakRuleTypeBreakAfter) then begin
    if not DataSet.FieldByName('message_frequency').IsNull then
      DataSet.FieldByName('message_frequency').Clear;
    if not DataSet.FieldByName('button_text').IsNull then
      DataSet.FieldByName('button_text').Clear;
    if DataSet.FieldByName('break_length').AsInteger < 1 then
      raise EOdDataEntryError.Create('A break length value is required for this type of rule');
    if DataSet.FieldByName('break_needed_after').AsInteger < 1 then
      raise EOdDataEntryError.Create('A time for break value is required for this type of rule');
  end;

  if (DataSet.FieldByName('rule_type').AsString = BreakRuleTypeSubmitDisagree) or
    (DataSet.FieldByName('rule_type').AsString = BreakRuleTypeManagerAlteredTime) then begin
    if not DataSet.FieldByName('break_length').IsNull then
      DataSet.FieldByName('break_length').Clear;
    if not DataSet.FieldByName('break_needed_after').IsNull then
      DataSet.FieldByName('break_needed_after').Clear;
    if DataSet.FieldByName('button_text').AsString = '' then
      raise EOdDataEntryError.Create('Please supply text to appear on the button ' +
        'of the dialog box. For multiple buttons, separate each caption with a comma.');
  end;

  if (DataSet.FieldByName('rule_type').AsString = BreakRuleTypeLunch) then begin
    if DataSet.FieldByName('break_length').AsInteger < 1 then
      raise EOdDataEntryError.Create('A break length value is required for this type of rule');
  end;

  //TODO: Need to add some add'l validation checks
  {
    invalid if more than one AFTER/EVERY rule per PC
    invalid if more than one SUBMIT rule per PC
    invalid if more than one DISAGREE rule per PC
  }
end;

end.
