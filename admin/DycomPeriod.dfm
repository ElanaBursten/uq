inherited DycomPeriodForm: TDycomPeriodForm
  Caption = 'Dycom Period'
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Height = 24
    object cbDycomPeriodSearch: TCheckBox
      Left = 19
      Top = 5
      Width = 97
      Height = 12
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cbDycomPeriodSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 24
    Height = 376
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      OptionsCustomize.ColumnFiltering = True
      OptionsView.GroupByBox = True
      object ColPeriod: TcxGridDBColumn
        Caption = 'Period'
        DataBinding.FieldName = 'period'
        SortIndex = 0
        SortOrder = soAscending
        Width = 69
      end
      object ColShortPeriod: TcxGridDBColumn
        Caption = 'Short Period'
        DataBinding.FieldName = 'short_period'
        Options.Editing = False
        Options.Focusing = False
        Width = 101
      end
      object ColStartingDate: TcxGridDBColumn
        Caption = 'Starting Date'
        DataBinding.FieldName = 'starting'
        Width = 125
      end
      object ColEndingDate: TcxGridDBColumn
        Caption = 'Ending Date'
        DataBinding.FieldName = 'ending'
        Width = 125
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    BeforePost = DataBeforePost
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from dycom_period order by period'
  end
end
