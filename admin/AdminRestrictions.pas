 //BP QM-371   1005

unit AdminRestrictions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseCxList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ExtCtrls, Vcl.StdCtrls, dxSkinsCore,
  dxSkinBasic, cxDropDownEdit, cxContainer, cxTextEdit,
  cxMaskEdit, cxPropertiesStore, cxDatautils, Vcl.Buttons, dxCore, dxSkinsForm;

type
  TAdminRestrictionForm = class(TBaseCxListForm)
    ColAllowedForms: TcxGridDBColumn;
    ColLoginID: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ActiveRestrictionsFilter: TCheckBox;
    cbActiveRestrictionsSearch: TCheckBox;
    pnlAdiminRestriction: TPanel;
    cboAddForms: TcxComboBox;
    memoAllowedForms: TMemo;
    lblAdd: TLabel;
    cboDeleteForms: TcxComboBox;
    lblDelete: TLabel;
    btnClose: TBitBtn;
    procedure ActiveRestrictionsFilterClick(Sender: TObject);
    procedure cbActiveRestrictionsSearchClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cboAddFormsPropertiesCloseUp(Sender: TObject);
    procedure ColAllowedFormsPropertiesInitPopup(Sender: TObject);
    procedure ColAllowedFormsPropertiesCloseUp(Sender: TObject);
    procedure cboDeleteFormsPropertiesCloseUp(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure DataNewRecord(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
  private
    AddList: TStringList;
    AllowedFormsValue: String;
    ARInserted: Boolean;
    procedure GetFormsList(List: TStrings);
    { Private declarations }
  public
    { Public declarations }
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure PopulateDropdowns; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

var
  AdminRestrictionForm: TAdminRestrictionForm;

implementation

uses AdminDMu, AdminFormDefs, OdHourglass, odMiscUtils, OdCxUtils;

{$R *.dfm}

procedure TAdminRestrictionForm.ActiveRestrictionsFilterClick(Sender: TObject);
var
  FSelectedID: Integer;
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  FSelectedID := Data.FieldByName('admin_restriction_id').AsInteger;
  Data.Close;
  if ActiveRestrictionsFilter.Checked then
    Data.CommandText := 'select * from admin_restriction where active = 1 order by login_id' else
      Data.CommandText := 'select * from admin_restriction order by login_id';
  Data.Open;
  Data.Locate('admin_restriction_id', FSelectedID, []);
end;

procedure TAdminRestrictionForm.btnCloseClick(Sender: TObject);
begin
  inherited;
  TcxPopupEdit(GridView.Controller.EditingController.Edit).DroppedDown := False;
end;

procedure TAdminRestrictionForm.cbActiveRestrictionsSearchClick(Sender: TObject);
begin
  inherited;
    if cbActiveRestrictionsSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TAdminRestrictionForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  GridRec: TcxCustomGridRecord;
begin
  GridRec := AViewInfo.RecordViewInfo.GridRecord;
  if GridRec is TcxGridDataRow then begin
    if not VarToBoolean(GridRec.Values[ColActive.Index]) then
    begin
      ACanvas.Font.Color := clInactiveCaptionText;
      ACanvas.Brush.Color := clWebLightYellow; //3DLight; //$00DDDDDD
    end
    else begin
      ACanvas.Brush.Color := clMoneyGreen; //$00DDDDDD;
      ACanvas.Font.Color := clWindowText //clBlack;
    end;
  end;
end;

procedure TAdminRestrictionForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbActiveRestrictionsSearch.Enabled := True;
  ActiveRestrictionsFilter.Enabled := True;
end;

procedure TAdminRestrictionForm.PopulateDropdowns;
begin
  inherited;
  cboAddForms.Properties.Sorted := True;
  cboDeleteForms.Properties.Sorted := True;
  cboAddForms.Clear;
  GetFormsList(cboAddForms.Properties.Items);
end;

procedure TAdminRestrictionForm.GetFormsList(List: TStrings);
var
  I: Integer;
  Holder: TAdminFormDef;
begin
  List.Clear;
  if Assigned(Defs) then
  for I := 0 to Defs.Count-1 do
  begin
    Holder := Defs.Items[I] as TAdminFormDef;
    List.Add(Holder.DisplayName);
  end;
end;

procedure TAdminRestrictionForm.cboAddFormsPropertiesCloseUp(Sender: TObject);
var
  Line: String;
begin
  inherited;
  Line := cboAddForms.Text;
  if Line = '' then Exit;
  If Pos(#32, cboAddForms.Text) > 0 then
  begin
   Line := '"' + Line + '"';
   AddList.Add(Line);
  end else
    AddList.Add(Line);
  cboDeleteForms.Properties.Items.Add(Line);
  memoAllowedForms.Lines.Clear;
  memoAllowedForms.Lines.Assign(Addlist);
end;

procedure TAdminRestrictionForm.cboDeleteFormsPropertiesCloseUp(Sender: TObject);
var
Index: Integer;
begin
  inherited;
  if cboDeleteForms.Text = '' then Exit;
  Index := cboDeleteForms.Properties.Items.IndexOf(cboDeleteForms.Text);
  if Index <> -1 then
  begin
    cboDeleteForms.Properties.Items.Delete(Index);
    cboDeleteForms.Text := '';
    AddList.Delete(Index);
    memoAllowedForms.Lines.Clear;
    memoAllowedForms.Lines.Assign(AddList);
    AllowedFormsValue := AddList.DelimitedText;
      if Trim(AllowedFormsValue) <> ''  then
    gridview.DataController.SetEditValue(Gridview.Controller.FocusedColumnIndex, AllowedFormsValue, evsValue)
  end;
end;

procedure TAdminRestrictionForm.ColAllowedFormsPropertiesCloseUp(
  Sender: TObject);
begin
  inherited;
  AllowedFormsValue := AddList.DelimitedText;
  AddList.Free;
  memoAllowedForms.Lines.Clear;
  if Trim(AllowedFormsValue) <> ''  then
    gridview.DataController.SetEditValue(Gridview.Controller.FocusedColumnIndex, AllowedFormsValue, evsValue)
end;

procedure TAdminRestrictionForm.ColAllowedFormsPropertiesInitPopup(
  Sender: TObject);
var
  CheckValue: Variant;
begin
  inherited;
  AddList := TStringList.Create;
  AddList.Sorted := True;
  AddList.Duplicates := dupIgnore;
  AddList.StrictDelimiter := True;
  AddList.QuoteChar := #0;
  AddList.Delimiter := ',';

  CheckValue := GridView.Controller.EditingController.Edit.Editingvalue;
  if trim(vartostr(CheckValue)) = '' then Exit;
  AddList.DelimitedText := vartostr(CheckValue);
  cboDeleteForms.Properties.Items.Assign(AddList);
  memoAllowedForms.Lines.Assign(Addlist);
end;

procedure TAdminRestrictionForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ARInserted := False;
end;

procedure TAdminRestrictionForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  cboAddForms.Text := '';
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'admin_restriction', 'admin_restriction_id', ARInserted);
end;

procedure TAdminRestrictionForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
     //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TAdminRestrictionForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ARInserted := True;
end;

procedure TAdminRestrictionForm.DataNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('active').AsBoolean := True;
end;

procedure TAdminRestrictionForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TAdminRestrictionForm.ActivatingNow;
begin
  inherited;
  ARInserted := False;
end;

end.
