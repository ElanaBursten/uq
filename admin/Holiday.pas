unit Holiday;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxDropDownEdit, cxCalendar,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxCheckBox, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Vcl.StdCtrls,
  dxDateRanges, dxScrollbarAnnotations;

type
  THolidayForm = class(TBaseCxListForm)
    ColCallCenter: TcxGridDBColumn;
    ColHolidayDate: TcxGridDBColumn;
    ColBillingHoliday: TcxGridDBColumn;
    chkboxHolidaySearch: TCheckBox;
    procedure chkboxHolidaySearchClick(Sender: TObject);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
  private
    HolidayInserted: Boolean;
  public
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

procedure THolidayForm.chkboxHolidaySearchClick(Sender: TObject);
begin
  inherited;
  if chkboxHolidaySearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure THolidayForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  HolidayInserted := False;
end;

procedure THolidayForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset,ATableArray, 'holiday', 'cc_code', HolidayInserted);
end;

procedure THolidayForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
    //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure THolidayForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  HolidayInserted := True;
end;

procedure THolidayForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure THolidayForm.ActivatingNow;
begin
  inherited;
  HolidayInserted := False;
end;

procedure THolidayForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure THolidayForm.PopulateDropdowns;
begin
  AdminDM.GetCallCenterList(CxComboBoxItems(ColCallCenter), False, True);
end;

procedure THolidayForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  chkBoxHolidaySearch.Enabled := True;
end;

end.

