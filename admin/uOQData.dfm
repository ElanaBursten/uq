inherited OQDataForm: TOQDataForm
  Caption = 'OQ Qualification'
  ClientHeight = 496
  ClientWidth = 814
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 814
    Height = 29
    object cboqDataSearch: TCheckBox
      Left = 34
      Top = 6
      Width = 97
      Height = 17
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = cboqDataSearchClick
    end
    object oqDataFilter: TCheckBox
      Left = 173
      Top = 6
      Width = 156
      Height = 17
      Caption = 'Active Qualifcations Only'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = oqDataFilterClick
    end
  end
  inherited Grid: TcxGrid
    Top = 29
    Width = 814
    Height = 467
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Edit.Enabled = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OnCustomDrawCell = GridViewCustomDrawCell
      OptionsView.GroupByBox = True
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxMaskEditProperties'
      end
      object ColEmpName: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = EmpLookupView
        Properties.KeyFieldNames = 'emp_id'
        Properties.ListFieldItem = EmpNameCol
        Properties.ReadOnly = False
        Width = 162
      end
      object CustLookupCol: TcxGridDBColumn
        Caption = 'Customer'
        DataBinding.FieldName = 'ref_id'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownSizeable = True
        Properties.View = CustLookupView
        Properties.KeyFieldNames = 'ref_id'
        Properties.ListFieldItem = CustCol
        Width = 165
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.View = StatusLookupView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = StatusCol
        Width = 82
      end
      object ColQualifiedDate: TcxGridDBColumn
        Caption = 'Qualified Date'
        DataBinding.FieldName = 'qualified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 145
      end
      object ColExpiredDate: TcxGridDBColumn
        Caption = 'Expired Date'
        DataBinding.FieldName = 'expired_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 149
      end
      object ColActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
      end
    end
    object EmpLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Enabled = False
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Edit.Enabled = False
      Navigator.Buttons.Post.Enabled = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Buttons.SaveBookmark.Enabled = False
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.FocusViewOnApplyFilter = True
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = EmpLookupDS
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object EmpIDCol: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        PropertiesClassName = 'TcxTextEditProperties'
      end
      object EmpNameCol: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        PropertiesClassName = 'TcxTextEditProperties'
      end
    end
    object StatusLookupView: TcxGridDBTableView [2]
      Navigator.Buttons.CustomButtons = <>
      FindPanel.Behavior = fcbSearch
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = StatusRefDS
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnGrouping = False
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      object StatusCol: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'code'
        PropertiesClassName = 'TcxTextEditProperties'
        Width = 75
      end
    end
    object CustLookupView: TcxGridDBTableView [3]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = CustRefDS
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnGrouping = False
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      object CustCol: TcxGridDBColumn
        DataBinding.FieldName = 'description'
        PropertiesClassName = 'TcxTextEditProperties'
        Options.Grouping = False
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from oq_data where active = 1'
  end
  object StatusRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 'select code from reference'#13#10' where type='#39'oq_data'#39' order by code'
    Parameters = <>
    Left = 408
    Top = 112
  end
  object EmpLookup: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'Select emp_id, '#13#10'case'#13#10'  when (last_name is not NULL) and (first' +
      '_name is not NULL) then'#13#10'       concat(last_name, '#39', '#39', first_na' +
      'me)'#13#10' when last_name is not NULL then'#13#10'         last_name'#13#10'  els' +
      'e NULL'#13#10'end as Name'#13#10'from employee where  active = 1 and  type_i' +
      'd <> 1245 order by Name'
    Parameters = <>
    Left = 288
    Top = 112
  end
  object StatusRefDS: TDataSource
    DataSet = StatusRef
    Left = 400
    Top = 168
  end
  object EmpLookupDS: TDataSource
    DataSet = EmpLookup
    Left = 296
    Top = 168
  end
  object CustRef: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    CommandText = 
      'select ref_id, description from reference where type = '#39'dmgco'#39' a' +
      'nd active_ind = 1'
    Parameters = <>
    Left = 480
    Top = 112
  end
  object CustRefDS: TDataSource
    DataSet = CustRef
    Left = 480
    Top = 168
  end
end
