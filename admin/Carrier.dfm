inherited CarrierForm: TCarrierForm
  Caption = 'Carriers'
  ClientWidth = 550
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 550
    Height = 24
    object chkboxCarrierSearch: TCheckBox
      Left = 12
      Top = 5
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxCarrierSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 24
    Width = 550
    Height = 376
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      FindPanel.ShowCloseButton = False
      DataController.Filter.MaxValueListCount = 0
      DataController.Filter.Active = True
      DataController.KeyFieldNames = 'carrier_id'
      OptionsCustomize.ColumnFiltering = True
      object ColName: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 257
      end
      object ColDeductible: TcxGridDBColumn
        Caption = 'Deductible'
        DataBinding.FieldName = 'deductible'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        HeaderAlignmentHorz = taRightJustify
        Width = 162
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'modified date'
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        Width = 151
      end
      object ColCarrierId: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'carrier_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Width = 126
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from carrier'#13#10'order by carrier_id'#13#10
  end
end
