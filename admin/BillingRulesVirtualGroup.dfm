inherited BillingRulesVirtualGroupForm: TBillingRulesVirtualGroupForm
  Caption = 'Billing Rules Virtual Group'
  ClientWidth = 631
  TextHeight = 13
  inherited TopPanel: TPanel
    Width = 631
    Height = 30
    object chkboxVirtualGroupSearch: TCheckBox
      Left = 47
      Top = 9
      Width = 97
      Height = 14
      Caption = 'Show Find Panel'
      TabOrder = 0
      OnClick = chkboxVirtualGroupSearchClick
    end
  end
  inherited Grid: TcxGrid
    Top = 30
    Width = 631
    Height = 370
    inherited GridView: TcxGridDBTableView
      Navigator.Buttons.Append.Visible = True
      Navigator.Visible = True
      FindPanel.DisplayMode = fpdmManual
      OptionsCustomize.GroupBySorting = True
      OptionsView.GroupByBox = True
      object ColCustomerID: TcxGridDBColumn
        Caption = 'Customer ID'
        DataBinding.FieldName = 'customer_id'
        DataBinding.IsNullValueType = True
        Width = 74
      end
      object ColCustomerName: TcxGridDBColumn
        Caption = 'Customer Name'
        DataBinding.FieldName = 'customer_id'
        DataBinding.IsNullValueType = True
        PropertiesClassName = 'TcxExtLookupComboBoxProperties'
        Properties.View = CustLookupView
        Properties.KeyFieldNames = 'customer_id'
        Properties.ListFieldItem = CustLookupViewCustName
        Width = 246
      end
      object ColCustomerGroupID: TcxGridDBColumn
        Caption = 'Customer Group ID'
        DataBinding.FieldName = 'customer_group_id'
        DataBinding.IsNullValueType = True
        Width = 103
      end
      object ColCustomerGroupName: TcxGridDBColumn
        Caption = 'Customer Group Name'
        DataBinding.FieldName = 'customer_group_name'
        DataBinding.IsNullValueType = True
        Width = 160
      end
    end
    object CustLookupView: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsLookupCust
      DataController.KeyFieldNames = 'customer_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object CustLookupViewCustomerID: TcxGridDBColumn
        DataBinding.FieldName = 'customer_id'
        DataBinding.IsNullValueType = True
      end
      object CustLookupViewCustName: TcxGridDBColumn
        DataBinding.FieldName = 'Cust  Name'
        DataBinding.IsNullValueType = True
      end
    end
  end
  inherited Data: TADODataSet
    Connection = AdminDM.Conn
    CursorType = ctStatic
    BeforeInsert = DataBeforeInsert
    BeforeEdit = DataBeforeEdit
    AfterPost = DataAfterPost
    AfterCancel = DataAfterCancel
    CommandText = 'select * from billing_rules_virtual_group'
  end
  object dsLookupCust: TDataSource
    DataSet = qryLookupCust
    Left = 192
    Top = 113
  end
  object qryLookupCust: TADOQuery
    Connection = AdminDM.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select customer_id, customer_name as [Cust  Name] from customer'
      'where active = 1 '
      ' order by customer_name')
    Left = 264
    Top = 113
  end
  object dxSkinController1: TdxSkinController
    Left = 464
    Top = 208
  end
end
