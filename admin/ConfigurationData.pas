unit ConfigurationData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, AdminDmu,
  Dialogs, BaseList, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Vcl.DBCtrls;

type
  TConfigurationDataForm = class(TBaseListForm)
    Dataname: TStringField;
    Datavalue: TStringField;
    Datadescription: TMemoField;
    SearchEdit: TEdit;
    FilterRecordsButton: TButton;
    ClearFilterButton: TButton;
    Datasync: TBooleanField;
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
//    procedure SelectUpdateFileButtonClick(Sender: TObject);
    procedure GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridDblClick(Sender: TObject);
    procedure FilterRecordsButtonClick(Sender: TObject);
    procedure ClearFilterButtonClick(Sender: TObject);
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure GridExit(Sender: TObject);
    procedure SearchEditChange(Sender: TObject);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
  private
    ConfigInserted: Boolean;
    procedure UpdateValue(Name, Value: string);
  public
   procedure LeavingNow; override;
   procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses
  OdSecureHash, DBMemoEditor;

{$R *.dfm}

procedure TConfigurationDataForm.UpdateValue(Name, Value: string);
begin
  if not Data.Locate('name', Name, []) then
    raise Exception.Create(Name + ' record not found.');
  Data.Edit;
  Data.FieldByName('value').AsString := Value;
  Data.Post;
end;

procedure TConfigurationDataForm.ActivatingNow;
begin
  inherited;
  ConfigInserted := False;
end;

procedure TConfigurationDataForm.ClearFilterButtonClick(Sender: TObject);
begin
  inherited;
  Data.Filtered := False;
  Data.Filter := '';
  Data.First;
  ClearFilterButton.Enabled := False;
  FilterRecordsButton.Enabled := True;
  SearchEdit.Text := '';
end;

procedure TConfigurationDataForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  ConfigInserted := False;
end;

procedure TConfigurationDataForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;  //QM 847 track reportto changes
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'configuration_data', 'name', ConfigInserted);
end;

procedure TConfigurationDataForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TConfigurationDataForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  ConfigInserted := True;
end;

procedure TConfigurationDataForm.EditNow;
begin
   If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
     inherited;
end;

procedure TConfigurationDataForm.FilterRecordsButtonClick(Sender: TObject);
begin
  inherited;
  if SearchEdit.Text <> '' then
  begin
    Data.Filter := 'Name like ''%' + SearchEdit.Text + '%'' ';
    Data.Filtered := True;
    Data.First;
    FilterRecordsButton.Enabled := False;
    ClearFilterButton.Enabled := True;
  end;
end;

procedure TConfigurationDataForm.GridDblClick(Sender: TObject);   //QMANTWO-173
var
  MemoEditorForm : TMemoEditorForm;
begin
  inherited;
  begin
    if Grid.SelectedField = Datadescription then
    begin
      MemoEditorForm := TMemoEditorForm.Create(nil);
      with MemoEditorForm do
        try
          DBMemoEditor.Text := Datadescription.AsString;
          showmodal;
          Data.Edit;
          if MemoEditorForm.ModalResult = mrOk  then
          Datadescription.AsString := DBMemoEditor.Text;
        finally
          freeandnil(MemoEditorForm);
        end;
    end;
  end;
end;

procedure TConfigurationDataForm.GridExit(Sender: TObject);
begin
  inherited;
  if Data.State in DSEditModes then
    Data.Post;
end;

procedure TConfigurationDataForm.GridKeyDown(Sender: TObject; var Key: Word;   //QMANTWO-173
  Shift: TShiftState);
var
  MemoEditorForm : TMemoEditorForm;
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if Grid.SelectedField = Datadescription then
    begin
      MemoEditorForm := TMemoEditorForm.Create(nil);
      with MemoEditorForm do
        try
          DBMemoEditor.Text := Datadescription.AsString;
          showmodal;
          Data.Edit;
          if MemoEditorForm.ModalResult = mrOk  then
          Datadescription.AsString := DBMemoEditor.Text;
        finally
          freeandnil(MemoEditorForm);
        end;
    end; //if Grid.SelectedField
  end;  //if Key = VK_RETURN then
end;

procedure TConfigurationDataForm.LeavingNow;
begin
  inherited;
  Data.Close;
end;

procedure TConfigurationDataForm.SearchEditChange(Sender: TObject);
begin
  inherited;
  ClearFilterButton.Enabled := SearchEdit.Text <> '';
  FilterRecordsButton.Enabled := True;
  if (SearchEdit.Text = '') then
    ClearFilterButton.Click;
end;

//procedure TConfigurationDataForm.SelectUpdateFileButtonClick(Sender: TObject);      never used  SR
//var
//  OpenDialog: TOpenDialog;
//  HashString: string;
//begin
//  inherited;
//
//  OpenDialog := TOpenDialog.Create(nil);
//  try
//    OpenDialog.InitialDir := GetCurrentDir;
//    OpenDialog.Options := [ofFileMustExist];
//    OpenDialog.Filter := '*.vbs, *.vbe, *.js, *.jse|*.vbs;*.vbe;*.js;*.jse';
//    OpenDialog.FilterIndex := 1;
//
//    if OpenDialog.Execute then begin
//      HashString := HashFile(OpenDialog.FileName);
//
//      Data.Connection.BeginTrans;
//      try
//        UpdateValue('UpdateFileName', ExtractFileName(OpenDialog.FileName));
//        UpdateValue('UpdateFileHash', HashString);
//        Data.Connection.CommitTrans;
//      except
//        on E: Exception do begin
//          Data.Connection.RollbackTrans;
//          E.Message := E.Message + ' You may need to apply ' +
//            'alter_schema_2599.sql to your database.';
//          raise;
//        end;
//      end;
//    end;
//  finally
//    FreeAndNil(OpenDialog);
//  end;
//end;

procedure TConfigurationDataForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  FilterRecordsButton.Enabled := True;
  SearchEdit.ReadOnly := False;
  DBNavigator1.Enabled:= not(IsReadOnly);
end;

end.
