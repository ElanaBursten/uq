unit BaseList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, Grids, DBGrids, DB, ADODB, ExtCtrls;

type
  TBaseListForm = class(TEmbeddableForm)
    TopPanel: TPanel;
    Data: TADODataSet;
    DS: TDataSource;
    Grid: TDBGrid;
    procedure FormShow(Sender: TObject);
    procedure DataBeforeDelete(DataSet: TDataSet);
  public
    procedure LeavingNow; override;
    procedure CloseDataSets; override;
    procedure OpenDataSets; override;
    procedure ActivatingNow; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TBaseListForm }

procedure TBaseListForm.LeavingNow;
begin
  inherited;
  if Data.State in dsEditModes then
    Data.Post;
end;

procedure TBaseListForm.FormShow(Sender: TObject);
begin
  inherited;
  if TopPanel.ControlCount = 0 then
    TopPanel.Hide;
  OpenDataSets;
end;

procedure TBaseListForm.DataBeforeDelete(DataSet: TDataSet);
var
  Field: TField;
begin
  inherited;
  Field := DataSet.FindField('Active');
  if Field<>nil then begin
    DataSet.Edit;
    Field.AsBoolean := False;
    DataSet.Post;
    Abort;
  end;
end;

procedure TBaseListForm.CloseDataSets;
begin
  inherited;
  Data.Close;
end;

procedure TBaseListForm.OpenDataSets;
begin
  inherited;
  Data.Open;
end;

procedure TBaseListForm.ActivatingNow;
begin
  inherited;
  SetReadOnly(True);
end;

procedure TBaseListForm.SetReadOnly(const IsReadOnly: Boolean);
var
  BM: TBookmark;
begin
  inherited;
  BM := Data.GetBookmark;
  RefreshNow;
  Data.GotoBookmark(BM);
  if Grid.CanFocus then
    Grid.SetFocus;
end;

end.

