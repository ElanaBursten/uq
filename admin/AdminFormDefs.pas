unit AdminFormDefs;

interface

uses Classes, Contnrs, OdVclUtils, IniFiles, OdEmbeddable, ExtractEmpData{, Esketch};

type
  TAdminFormClass = class of TEmbeddableForm;

  TAdminFormDef = class
    DisplayName: string;
    Group: string;
    FormClass: TAdminFormClass;
  end;

  function LookupAdminFormClass(DisplayName: string): TAdminFormClass;

var
  Defs: TObjectList;

implementation

uses
  SysUtils,
  MapSetup, StatusList, Locator, Offices, Reference, CallCenter, TimeFixer, User, CountyRouting,
  EmployeeIncentivePay, ProfitCenter, TownshipRange, UnRouting, FollowupTickets, AssetTypes,
  UploadLocations, TicketAckMatch, Hierarchy, DamageDefEst, Holiday, RoutingList, Carrier,
  LocatingCompany, uFrontEnds, CustomerTerms, StatusGroups, ConfigurationData, FaxQueueRules,
  EmailQueueRules, HighProfileGrid, BreakRules, UploadFileTypes, RestrictUsage, EmployeeGroups,
  HighProfileAddress, Computers, TicketAlertKeyword, HighlightKeywordRules, RequiredDocuments,
  DamagePCRules, DamageDueDateRules, DycomPeriod, EPRDisclaimer, WorkOrderWorkType, {EventViewer,}
  TicketInfo, EPRNotificationConfig, TableViewer2, EPRQueueRules, PlatMain, AuditDueDateRules,
  AdminRestrictions, uRoboMain, uOQData, ClientRespondTo, RespondTo, uMainStatusEdit, HALAttachments,
  HALAssignedTickets, HALCallCenters, HALActiveUsers, HALEPRs, HALBlankLocators, HALTicketVersion,
  HALFireCracker, HALTKAttachCV, HALParserLogErrors,HALErrorLogs, Rabbitmq, StatusPlus, BillingCCMap,
  Tema, BillingRulesCustomer, BillingTaskID, BillingRulesVirtualGroup,TSEApprovalQueue, TSEApprovalQueueHist;

var
  Group: string;

function LookupAdminFormClass(DisplayName: string): TAdminFormClass;
var
  Holder: TAdminFormDef;
  I: Integer;
begin
  for I := 0 to Defs.Count-1 do begin
    Holder := Defs.Items[I] as TAdminFormDef;
    if Holder.DisplayName = DisplayName then begin
      Result := Holder.FormClass;
      Exit;
    end;
  end;

  raise Exception.Create('ERROR: Don''t know how to create form: ' + DisplayName);
end;

procedure RegisterAdmin(DisplayName: string; ImplClass: TAdminFormClass);
var
  Holder: TAdminFormDef;
begin
  if not Assigned(Defs) then
    Defs := TObjectList.Create;

  Holder := TAdminFormDef.Create;
  Holder.DisplayName := DisplayName;
  Holder.FormClass := ImplClass;
  Holder.Group := Group;
  Defs.Add(Holder);
end;

function AdminFormCompare(Item1, Item2: Pointer): Integer;
begin
  Result := CompareStr(TAdminFormDef(Item1).DisplayName,
                       TAdminFormDef(Item2).DisplayName);
end;

initialization
  Group := 'Tickets and Work Orders';
  RegisterAdmin('Call Centers', TCallCenterForm);
  RegisterAdmin('Statuses', TStatusListForm);
  RegisterAdmin('Status Groups', TStatusGroupForm);
  RegisterAdmin('Status Plus', TStatusPlusForm);
  RegisterAdmin('Status Edits Exclusions',TfrmMainStatusEdit);
  RegisterAdmin('Customers / Terms', TCustomerTermForm);
  RegisterAdmin('Followup Tickets', TFollowupTicketsForm);
  RegisterAdmin('Emergency Ticket Types', TTicketAckMatchForm);
  RegisterAdmin('Locate Notification Faxes', TFaxQueueRulesForm);
  RegisterAdmin('Locate Notification Emails', TEmailQueueRulesForm);
  RegisterAdmin('High Profile Grids', THighProfileGridForm);
  RegisterAdmin('High Profile Addresses', THighProfileAddressForm);
  RegisterAdmin('Ticket Alert Keywords', TTicketAlertKeywordForm);
  RegisterAdmin('Highlight Keyword Rules', THighlightKeywordRulesForm);
  RegisterAdmin('Work Order Work Types', TWorkOrderWorkTypeForm);
  RegisterAdmin('Ticket Info Questions', TTicketInfoForm);
  RegisterAdmin('Respond To', TRespondToForm);
  RegisterAdmin('Client Respond To', TfrmClientRespondTo);


  Group := 'Routing';
  RegisterAdmin('Map Setup', TMapSetupForm);
  RegisterAdmin('County Routing', TCountyRoutingForm);
  //QMANTWO-548 Deprecated screens
  //RegisterAdmin('Qtr-Min Routing - Overview', TQtrMinForm);
  //RegisterAdmin('Qtr-Min Routing - Pages', TQtrMinPageMapForm);
  RegisterAdmin('Township-Range Routing', TTownshipRangeForm);
  RegisterAdmin('Fix New Jersey Times', TTimeFixerForm);
  RegisterAdmin('Unrouting of Unassigned', TUnRoutingForm);
  RegisterAdmin('Routing List', TRoutingListForm);

  Group := 'Users / Organization';
  RegisterAdmin('Locating Companies', TLocatingCompanyForm);
  RegisterAdmin('Profit Centers', TProfitCenterForm);
  RegisterAdmin('Offices', TOfficesForm);
  RegisterAdmin('Employees', TLocatorForm);
  RegisterAdmin('Users', TUserForm);
  RegisterAdmin('Employee Groups', TEmployeeGroupsForm);
  RegisterAdmin('Employee Hierarchy', THierarchyForm);
  RegisterAdmin('Break Rules', TBreakRulesForm);
  RegisterAdmin('Employee Incentive Pay', TEmployeeIncentivePayForm);
  RegisterAdmin('OQ Qualification', TOQDataForm);    //QM 447 BP

  Group := 'Damages';
  RegisterAdmin('Default Estimates', TDamageDefEstForm);
  RegisterAdmin('Profit Center Rules', TDamagePCRulesForm);
  RegisterAdmin('Due Date Rules', TDamageDueDateRulesForm);
  RegisterAdmin('Required Documents', TRequiredDocumentsForm);
  RegisterAdmin('Carriers', TCarrierForm);

  Group := 'Misc';
  RegisterAdmin('Reference / Lookup', TReferenceForm);
  RegisterAdmin('Asset Types', TAssetTypeForm);
  RegisterAdmin('Upload Locations', TUploadLocationsForm);
  RegisterAdmin('Holidays', THolidayForm);
  RegisterAdmin('Configuration Data', TConfigurationDataForm);
  RegisterAdmin('Upload File Types', TUploadFileTypesForm);
  RegisterAdmin('Restrict Usage Times', TRestrictUsageForm);
  RegisterAdmin('Computers', TComputersForm);
  RegisterAdmin('Dycom Periods', TDycomPeriodForm);
 // RegisterAdmin('Export eSketch Codes', TfrmESketchCodes);   //QM-203  SR
  RegisterAdmin('Table Viewer', TTableViewerForm2);
  RegisterAdmin('Extract Emp Data', TExtractEmpForm);     //QM-225
  RegisterAdmin('Admin Restrictions', TAdminRestrictionForm);
  RegisterAdmin('Rabbit MQ Admin', TRabbitmqForm);

  Group := 'EPR Admin';
  RegisterAdmin('EPR Notification Config', TEPRNotificationConfigForm);
  RegisterAdmin('EPR Queue Rules', TEPRQueueRulesForm);
  RegisterAdmin('EPR Disclaimer', TEPRDisclaimerForm);

  Group := 'Audit';
  RegisterAdmin('Audit Due Date Rules', TAuditDueDateRulesForm);

  Group := 'Plats';
  RegisterAdmin('Plat Manager', TPlatManagerForm);     //QM-148 BP
  RegisterAdmin('RoboCopy', TRoboCopyForm); //QM-418 BP

  Group := 'Monitor';
  RegisterAdmin('Front Ends', TMonitorForm);

  Group := 'HAL';
  RegisterAdmin('Missing Attachments', TAttachmentsForm); //QM-531 BP
  RegisterAdmin('Assigned Tickets', TAssignedTicketsForm); //QM-531 BP
  RegisterAdmin('Call Centers HAL', THALCallCenterForm); //QM-531 BP
  RegisterAdmin('Active Users', TActiveUsersForm); //QM-531 BP
  RegisterAdmin('EPRs', THALEPRsForm); //QM-531 BP
  RegisterAdmin('Blank Locators', TBlankLocatorsForm); //QM-531 BP
  RegisterAdmin('Ticket Version', TTicketVersionForm); //QM-531 BP
  RegisterAdmin('FireCracker', TFireCrackerForm); //QM-531 BP
  RegisterAdmin('TKAttachCV', TTKAttachCVForm); //QM-531 BP
  RegisterAdmin('Parser Log Errors', TParserLogErrorsForm); //QM-531 BP
  RegisterAdmin('Error Logs', TErrorLogsForm); //QM-531 BP
  RegisterAdmin('TSE Approval Queue', TApprovalQueueForm); //QM 831 BP
  RegisterAdmin('TSE Approval Queue History', TApprovalQueueHistForm); //QM 831 BP

  Group := 'Billing';
  RegisterAdmin('Billing CC Map', TBillingCCMapForm); //QM-531 BP
  RegisterAdmin('Billing Rules Customer', TBillingRulesCustomerForm); //QM-531 BP
  RegisterAdmin('Billing Rules Virtual Group', TBillingRulesVirtualGroupForm);
  RegisterAdmin('Billing Task ID', TBillingTaskIDForm);

  Group := 'Tema';
  RegisterAdmin('Tema Forms', TTemaForm);
 finalization
  FreeAndNil(Defs);

end.
