unit CountyRouting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxMaskEdit, cxDBLookupComboBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxDropDownEdit, cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog,
  dxDateRanges, dxScrollbarAnnotations, dxSkinBasic, dxSkinsCore, AdminDmu;

type
  TCountyRoutingForm = class(TEmbeddableForm)
    Panel1: TPanel;
    Label3: TLabel;
    btnSetCountytoArea: TBitBtn;
    CopyClipButton: TButton;
    CountyArea: TADODataSet;
    CountyAreaDS: TDataSource;
    AreaDS: TDataSource;
    Splitter1: TSplitter;
    AreaGridLevel: TcxGridLevel;
    AreaGrid: TcxGrid;
    AreaGridView: TcxGridDBTableView;
    AreaColMapName: TcxGridDBColumn;
    AreaColAreaName: TcxGridDBColumn;
    AreaColEmpName: TcxGridDBColumn;
    CountyGridView: TcxGridDBTableView;
    CountyGridLevel: TcxGridLevel;
    CountyGrid: TcxGrid;
    CountyGridViewstate: TcxGridDBColumn;
    CountyGridViewcounty: TcxGridDBColumn;
    CountyGridViewmunic: TcxGridDBColumn;
    CountyGridViewcall_center: TcxGridDBColumn;
    CountyGridViewarea_name: TcxGridDBColumn;
    CountyGridViewshort_name: TcxGridDBColumn;
    AreaColState: TcxGridDBColumn;
    AreaData: TADODataSet;
    cbCountyAreaSearch: TCheckBox;
    chkboxAreaSearch: TCheckBox;
    AreaGridViewColumn1: TcxGridDBColumn;
    AreaDatamap_name: TStringField;
    AreaDatamap_id: TIntegerField;
    AreaDatastate: TStringField;
    AreaDatacounty: TStringField;
    AreaDataarea_id: TAutoIncField;
    AreaDataarea_name: TStringField;
    AreaDatashort_name: TStringField;
    AreaDatalocator_id: TIntegerField;
    qryLastIns: TADOQuery;
    StateList: TADODataSet;
    Label8: TLabel;
    cbStateFilter: TComboBox;
    DelCountyArea: TADOCommand;
    procedure btnSetCountytoAreaClick(Sender: TObject);
    procedure CopyClipButtonClick(Sender: TObject);
    procedure cbCountyAreaSearchClick(Sender: TObject);
    procedure chkboxAreaSearchClick(Sender: TObject);
    procedure CountyGridExit(Sender: TObject);
    procedure cbStateFilterChange(Sender: TObject);
    procedure CountyAreaNewRecord(DataSet: TDataSet);
    procedure CountyAreaBeforePost(DataSet: TDataSet);
    procedure CountyAreaAfterPost(DataSet: TDataSet);
    procedure CountyAreaBeforeDelete(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CountyAreaAfterCancel(DataSet: TDataSet);
    procedure CountyAreaBeforeInsert(DataSet: TDataSet);
    procedure CountyAreaBeforeEdit(DataSet: TDataSet);
    procedure AreaGridViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    CountyAreaInserted: Boolean;
    AEdit: Boolean;
    procedure PopulateStateList;
    procedure ResetFilter;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure EditNow; override;
  end;

implementation

uses OdHourglass, OdDbUtils, Clipbrd, OdMiscUtils;

{$R *.dfm}

procedure TCountyRoutingForm.AreaGridViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if( Key = VK_INSERT) or (Key = VK_DELETE) then
    Key := 0;
end;

procedure TCountyRoutingForm.btnSetCountytoAreaClick(Sender: TObject);
var
  ID: Integer;
begin
  with CountyArea do begin
    SetLength(ATableArray, 0);
    AdminDM.StoreFieldValues(CountyArea, ATableArray);
    Edit;
    FieldByName('area_id').AsInteger := areaData.FieldByName('area_id').AsInteger;
    FieldByName('map_id').AsInteger :=  areaData.FieldByName('map_id').AsInteger;
    FieldByName('state').AsString := areaData.FieldByName('state').AsString;
    FieldByName('county').AsString := areaData.FieldByName('county').AsString;
    FieldByName('munic').AsString := '';
    Post;

    ID := FieldbyName('county_area_id').AsInteger;
    Close;
    Open;
    Locate('county_area_id', ID, []);
  end;
end;

procedure TCountyRoutingForm.PopulateStateList;
begin
  StateList.Open;
  cbStateFilter.Items.Clear;
  cbStateFilter.Items.Add('ALL');
  while not StateList.EOF do begin
    cbStateFilter.Items.Add(StateList.Fields[0].AsString);
    StateList.Next;
  end;
  StateList.Close;
  cbStateFilter.ItemIndex := 0;
  ResetFilter;
end;

procedure TCountyRoutingForm.ResetFilter;
begin
  CountyGridView.DataController.Filter.Clear;
  AreaGridView.DataController.Filter.Clear;
  if cbStateFilter.Text <> 'ALL' then
  begin
    AreaGridView.DataController.Filter.Root.AddItem(AreaColState, foEqual, cbStateFilter.Text, cbStateFilter.Text);
    CountyGridView.DataController.Filter.Root.AddItem(CountyGridViewState, foEqual, cbStateFilter.Text, cbStateFilter.Text);
  end;
end;

procedure TCountyRoutingForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  cbCountyAreaSearch.Enabled := True;
  chkboxAreaSearch.Enabled := True;
end;

procedure TCountyRoutingForm.CountyAreaAfterCancel(DataSet: TDataSet);
begin
  inherited;
  CountyAreaInserted := False;
end;

procedure TCountyRoutingForm.CountyAreaAfterPost(DataSet: TDataSet);
var
  ID:Integer;
begin
  inherited;
   AdminDM.TrackAdminChanges(Dataset,ATableArray, 'county_area', 'county_area_id', CountyAreaInserted);
   if AEdit then
   begin
     AEdit := False;
     Exit;
   end;
   CountyArea.Close;
   CountyArea.Open;
   qryLastIns.Open;
   ID := qryLastIns.Fields.Fields[0].AsInteger;
   qryLastIns.Close;
   CountyArea.Locate('county_area_id', ID, []);
end;

procedure TCountyRoutingForm.CountyAreaBeforeDelete(DataSet: TDataSet);
var
 s:String;
begin
  inherited;
  With DelCountyArea.Parameters do
    ParamByName('ca_id').Value :=
      CountyArea.FieldByName('county_area_id').AsInteger;
  DelCountyArea.Execute;
    s := 'county_area ' +  DataSet.FieldByName('county_area_id').AsString +
    ', map_id ' + DataSet.FieldByName('map_id').AsString +
    ', state ' + DataSet.FieldByName('state').AsString  +
    ', county ' + DataSet.FieldByName('county').AsString  +
    ' deleted';
  AdminDM.AdminChangeTable('employee', s, now);
  CountyArea.Close;
  CountyArea.Open;
  Abort;
end;

procedure TCountyRoutingForm.CountyAreaBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TCountyRoutingForm.CountyAreaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  CountyAreaInserted := True;
end;

procedure TCountyRoutingForm.CountyAreaBeforePost(DataSet: TDataSet);
begin
  inherited;
  if CountyAreaDS.State = dsEdit then
  begin
    AEdit := True;
    Exit;
  end;

  With DataSet do
  begin
    FieldByName('area_id').AsInteger := areaData.FieldByName('area_id').AsInteger;
    FieldByName('map_id').AsInteger :=  areaData.FieldByName('map_id').AsInteger;
  end;
end;

procedure TCountyRoutingForm.CountyAreaNewRecord(DataSet: TDataSet);
begin
 inherited;
  With DataSet do
  begin
    FieldByName('state').AsString := areaData.FieldByName('state').AsString;
    FieldByName('county').AsString := areaData.FieldByName('county').AsString;
  end;
end;

procedure TCountyRoutingForm.CountyGridExit(Sender: TObject);
begin
  inherited;
  PostDataSet(CountyArea);
end;

procedure TCountyRoutingForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TCountyRoutingForm.FormCreate(Sender: TObject);
begin
  inherited;
  AEdit := False;
end;

procedure TCountyRoutingForm.CopyClipButtonClick(Sender: TObject);
var
  s: TStringStream;
begin
  s := TStringStream.Create;
  Screen.Cursor := crHourGlass;
  try
    s.SetSize(100000);  // start with it large to avoid reallocations
    CountyArea.First;
    SaveDelimToStream(etANSI, CountyArea, s, #9, True, nil, nil, nil);
    CountyArea.First;
    Clipboard.AsText := s.DataString;
    ShowMessage('The data can now be pasted in to Excel.');
  finally
    FreeAndNil(s);
    Screen.Cursor := crDefault;
  end;
end;

procedure TCountyRoutingForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TCountyRoutingForm.cbStateFilterChange(Sender: TObject);
begin
  inherited;
  ResetFilter;
end;

procedure TCountyRoutingForm.cbCountyAreaSearchClick(Sender: TObject);
begin
  inherited;
  if cbCountyAreaSearch.Checked then
    CountyGridView.Controller.ShowFindPanel
  else
    CountyGridView.Controller.HideFindPanel;
end;

procedure TCountyRoutingForm.chkboxAreaSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxAreaSearch.Checked then
    AreaGridView.Controller.ShowFindPanel
  else
    AreaGridView.Controller.HideFindPanel;
end;

procedure TCountyRoutingForm.CloseDataSets;
begin
  inherited;
  CountyArea.Close;
  AreaData.Close;
  StateList.Close;
end;

procedure TCountyRoutingForm.OpenDataSets;
begin
  inherited;
  cbStateFilter.Visible := false;
  PopulateStateList;
  cbStateFilter.Visible := true;
  CountyArea.Open;
  AreaData.Open;
  AdminDM.ConfigDM.GetCallCenterList(TcxComboBoxProperties(CountyGridViewcall_center.Properties).Items);
end;

procedure TCountyRoutingForm.ActivatingNow;
begin
  OpenDataSets;
  SetReadOnly(True);
  AreaGridView.DataController.FocusedRowIndex := 0;
  CountyAreaInserted := False;
  inherited;
end;

end.

