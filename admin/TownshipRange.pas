unit TownshipRange;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseCxList, DB, ADODB, ExtCtrls, StdCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit,
  cxMaskEdit, cxDBLookupComboBox, cxDropDownEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxDateRanges,
  dxScrollbarAnnotations;

type
  TTownshipRangeForm = class(TBaseCxListForm)
    Area: TADODataSet;
    Areamap_name: TStringField;
    Areastate: TStringField;
    Areacounty: TStringField;
    Areaarea_id: TAutoIncField;
    Areaarea_name: TStringField;
    Arealocator_id: TIntegerField;
    AreaDS: TDataSource;
    Datatownship: TStringField;
    Datarange: TStringField;
    Datasection: TStringField;
    Dataarea_id: TIntegerField;
    DataArea: TStringField;
    Label1: TLabel;
    TownshipPrefix: TEdit;
    SearchBtn: TButton;
    Label2: TLabel;
    EditBaseCode: TEdit;
    Label3: TLabel;
    Databasecode: TStringField;
    Datacall_center: TStringField;
    ColBaseCode: TcxGridDBColumn;
    ColTownship: TcxGridDBColumn;
    ColRange: TcxGridDBColumn;
    ColSection: TcxGridDBColumn;
    ColArea: TcxGridDBColumn;
    ColCallCenter: TcxGridDBColumn;
    chkboxTownshipSearch: TCheckBox;
    procedure SearchBtnClick(Sender: TObject);
    procedure chkboxTownshipSearchClick(Sender: TObject);
    procedure DataAfterCancel(DataSet: TDataSet);
    procedure DataBeforeInsert(DataSet: TDataSet);
    procedure DataBeforeEdit(DataSet: TDataSet);
    procedure DataAfterPost(DataSet: TDataSet);
  private
    trInserted: Boolean;
  public
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
    procedure PopulateDropdowns; override;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure LeavingNow; override;
    procedure EditNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses AdminDMu, OdDbUtils, OdCxUtils;

{$R *.dfm}

{ TTownshipRangeForm }

procedure TTownshipRangeForm.chkboxTownshipSearchClick(Sender: TObject);
begin
  inherited;
  if chkboxTownshipSearch.Checked then
    GridView.Controller.ShowFindPanel
  else
    GridView.Controller.HideFindPanel;
end;

procedure TTownshipRangeForm.CloseDataSets;
begin
  inherited;
  Area.Close;
end;

procedure TTownshipRangeForm.DataAfterCancel(DataSet: TDataSet);
begin
  inherited;
  trInserted := False;
end;

procedure TTownshipRangeForm.DataAfterPost(DataSet: TDataSet);
begin
  inherited;
  AdminDM.TrackAdminChanges(Dataset, ATableArray, 'map_trs', 'basecode', trInserted);
end;

procedure TTownshipRangeForm.DataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  //QM 847 track reportto changes
  AdminDM.StoreFieldValues(DataSet, ATableArray);
end;

procedure TTownshipRangeForm.DataBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  trInserted := True;
end;

procedure TTownshipRangeForm.EditNow;
begin
  If MessageDlg('Warning: the changes you are about to make are being audited', mtWarning, [mbOK, mbAbort], 0) = mrOK then
    inherited;
end;

procedure TTownshipRangeForm.ActivatingNow;
begin
  inherited;
  trInserted := False;
end;

procedure TTownshipRangeForm.LeavingNow;
begin
  inherited;
  CloseDataSets;
end;

procedure TTownshipRangeForm.OpenDataSets;
begin
  inherited;
  Area.Open;
end;

procedure TTownshipRangeForm.SearchBtnClick(Sender: TObject);
begin
  inherited;
  Data.Close;
  Data.Parameters.ParamValues['base'] := EditBaseCode.Text;
  Data.Parameters.ParamValues['prefix'] := TownshipPrefix.Text + '%';
  Data.Open;
end;

procedure TTownshipRangeForm.PopulateDropdowns;
begin
  inherited;
  // Populate call center dropdown
  AdminDM.ConfigDM.GetCallCenterList(CxComboBoxItems(ColCallCenter), False,
    True);
end;

procedure TTownshipRangeForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  SearchBtn.Enabled := True;
  chkboxTownshipSearch.Enabled := True;
  EditBaseCode.ReadOnly := False;
  TownshipPrefix.ReadOnly := False;
end;

end.

