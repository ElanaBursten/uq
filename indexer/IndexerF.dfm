object Form1: TForm1
  Left = 726
  Top = 241
  Width = 488
  Height = 291
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Terminal'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    480
    264)
  PixelsPerInch = 96
  TextHeight = 8
  object QueryMemo: TMemo
    Left = 0
    Top = 180
    Width = 480
    Height = 84
    Anchors = [akLeft, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Terminal'
    Font.Style = []
    Lines.Strings = (
      'create table ticket_word_lookup ('
      '    ticket_id integer not null,'
      '    field_id not null,'
      '    word varchar(20) not null'
      ')')
    ParentFont = False
    TabOrder = 0
  end
  object Button1: TButton
    Left = 432
    Top = 184
    Width = 43
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Exec'
    TabOrder = 1
  end
  object StatusMemo: TMemo
    Left = 0
    Top = 0
    Width = 480
    Height = 175
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Terminal'
    Font.Style = []
    Lines.Strings = (
      'StatusMemo')
    ParentFont = False
    TabOrder = 2
  end
  object GoButton: TButton
    Left = 432
    Top = 158
    Width = 43
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Go!'
    TabOrder = 3
    OnClick = GoButtonClick
  end
  object Tickets: TBetterADODataSet
    CacheSize = 100
    Connection = CONN
    CommandText = 
      'select top 1000 ticket_id, work_address_street  from ticket wher' +
      'e ticket_id > coalesce((select max(ticket_id) from ticket_word_l' +
      'ookup), 0) order by ticket_id'
    Parameters = <>
    IndexDefs = <>
    Left = 384
    Top = 8
  end
  object CONN: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=doggy183;Persist Security Info=True' +
      ';User ID=sa;Initial Catalog=QMTesting;Data Source=testingdb'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 416
    Top = 8
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 8
    Top = 8
  end
  object AddWord: TADOCommand
    CommandText = 
      'INSERT INTO ticket_word_lookup (ticket_id, word) VALUES (:ticket' +
      '_id, :word)'#13#10
    Connection = CONN
    Parameters = <
      item
        Name = 'ticket_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'word'
        Size = -1
        Value = Null
      end>
    Left = 416
    Top = 40
  end
end
