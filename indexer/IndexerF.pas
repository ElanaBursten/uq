unit IndexerF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ADODB, DB, BetterADODataSet, ExtCtrls;

type
  TForm1 = class(TForm)
    Tickets: TBetterADODataSet;
    CONN: TADOConnection;
    QueryMemo: TMemo;
    Button1: TButton;
    StatusMemo: TMemo;
    Timer1: TTimer;
    GoButton: TButton;
    AddWord: TADOCommand;
    procedure FormCreate(Sender: TObject);
    procedure GoButtonClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    FProcecessed: Integer;
    procedure SetProcecessed(const Value: Integer);
    procedure ProcessRecords;
    { Private declarations }
  public
    { Public declarations }
    CurrentTicket : Integer;
    property Procecessed : Integer read FProcecessed write SetProcecessed;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Conn.Connected := True;
end;

procedure TForm1.ProcessRecords;

  procedure ProcessRecord;

    procedure AddAWord(Ticket: Integer; Word : string);
    begin
      AddWord.Parameters.ParamByName('ticket_id').Value := Ticket;
      AddWord.Parameters.ParamByName('word').Value := Word;
      AddWord.Execute;
    end;

    var
      WordText : string;

    procedure AddAWord2(Ticket: Integer; Word : string);
    begin
      if Length(WordText) = 0 then
        WordText := 'INSERT INTO ticket_word_lookup (ticket_id, word) VALUES (' + IntToStr(Ticket) + ',' + Word + ','
      else
        WordText := WordText + IntToStr(Ticket) + ',' + Word + ',';
    end;

    procedure AddWords;
    begin
      StatusMemo.Lines.Add(WordText);
    end;

  var
    StreetName : string;
    Words      : TStringList;

    I, J,
    TicketID   : Integer;

  begin
    WordText := '';
    TicketId := Tickets.FieldByName('ticket_id').AsInteger;
    StreetName := Tickets.FieldByName('work_address_street').AsString;
    StatusMemo.Lines.Add(format('Adding (%d) %s', [TicketID, Streetname]));
    Words := TStringList.Create;
    Words.CommaText := StringReplace(StreetName, ' ', ',', [rfReplaceAll]);

    Procecessed := Procecessed + 1;

    for I := 0 to Words.Count - 1 do
      try
        if Length(Trim(Words[I])) > 0 then
          AddAWord2(TicketID, Trim(Words[I]));

        AddWords;
      except
        on E: Exception do StatusMemo.Lines.Add('Exception ' + E.Message);
      end;
  end;

var
  Recno : Integer;
begin
  Tickets.Open;

  CONN.BeginTrans;

  if Tickets.RecordCount = 0 then begin  // We're done !! !(Yeah right!) or it failed (most likely ;)..
    Timer1.Enabled := False;
    GoButton.Enabled := False;
    Conn.Close;
    Exit;
  end;

  for Recno := 1 to Tickets.RecordCount do begin
    ProcessRecord;
    Tickets.Next;
    if (Recno mod 5) = 0 then begin
      Application.ProcessMessages;
      sleep(100);
    end;
  end;

  CONN.CommitTrans;

  Tickets.Close;
end;

procedure TForm1.GoButtonClick(Sender: TObject);
begin
//  Timer1.Enabled := True;
ProcessRecords;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  ProcessRecords;
end;

procedure TForm1.SetProcecessed(const Value: Integer);
begin
  FProcecessed := Value;
  Caption := 'Processed ' + IntToStr(Value);
end;


end.



(* SQL code to do mostly the same thing

// CREATE LOOKUP TABLE

create table ticket_word_lookup (
    ticket_id integer not null,
    word varchar(20) not null
)

drop table




declare @integers table (
    id integer not null
)

declare @i int select @i = 1
while @I <= 100 begin
  insert into @integers(ID) values (@I)
  set @I = @I+1
end

declare @temp_tickets table (
  ticket_id integer null,
  work_address varchar(60) NULL
)

insert into
  @temp_tickets (ticket_id, work_address)
    select top 1000 ticket_id, work_address_street
     from ticket
     where ticket_id >
         (select max(ticket_id) from ticket_word_lookup) order by ticket_id

insert into
  ticket_word_lookup (ticket_id, word)
    select ticket_id, NullIf(SubString(' ' + work_address + ' ' , ID , CharIndex(' ' , ' ' + work_address + ' ' , ID) - ID) , '') AS Word
    from @temp_tickets, @integers
    where id <= Len(' ' + work_address + ' ')
          AND SubString(' ' + work_address + ' ' , ID - 1, 1) = ' '
          AND CharIndex(' ' , ' ' + work_address + ' ' , ID) - ID > 0
*)
