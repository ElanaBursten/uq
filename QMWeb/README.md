# QM Tablet

## Bower Setup
1. Install NodeJS (10.17 or newer) and NPM: http://nodejs.org/
2. You may need to add C:\Users\usernamehere\AppData\Roaming\npm to your PATH.
3. Install bower: npm install -g bower
4. Install JS deps via Bower: bower install

## Gulp Setup
1. Install NodeJS (10.7 or newer) and NPM: http://nodejs.org/
2. You may need to add C:\Users\usernamehere\AppData\Roaming\npm to your PATH.
3. Install gulpjs: npm install -g gulp
4. Install NPM dependencies: npm install

## Installing Google Go
1. Install Google Go: http://golang.org/doc/install
2. You may need to add C:\Go\bin to your path

## Google Go Server Setup
1. You will need to up a place to download dependencies for go.
   Run 'go help gopath' from a cmd for instruction on how to set up the directories.
2. Run the command from a cmd: go get github.com/gorilla/mux
3. Run the command from a cmd: go get github.com/gorilla/sessions

### To start the server:
1. cd to the server/ directory.
2. Run the command: 
```
go run main.go
```
3. If you would like to run using the development files, run: 
```
go run main.go dev
```
4. By default, the server runs on port 8765
5. To change the port via the command line, use 
```
export PORT=":<portnumber>"
```
6. To change the port via a config file, you will need to add QMWebDemoServerConf.json
## Development
In order to have the SCSS/Jade files automatically compile when edited, you must run gulp in development mode:
```
gulp dev
```
In order to build the files as they will be in production (minified/optimized), run gulp's build task:
```
gulp build
```
In order to ensure your code follows good JavaScript conventions, you must run JSHint:
```
gulp jshint
```
In order to run unit tests on your code, you can run Karma:
```
gulp karma
```
In order to run end-to-end tests, first make sure you have downloaded the dependencies for Protractor:
```
gulp webdriver_update
```
Then, run Protractor:
```
gulp protractor
```
(Note: you must have first run ```gulp build``` to make sure the proper files have been created.)

Running ```gulp prod``` will build the files as well as running JSHint, Karma, and Protractor.

## Making an Installer
1. Install Inno Setup (http://www.jrsoftware.org/isdl.php)
2. Install MSBuild Community Tasks (https://code.google.com/p/msbuildtasks)
3. Run GoBuild.bat to make a new installer.
