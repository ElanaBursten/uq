"use strict";

angular.module("common.services.errorHandler", [
    "common.directives.alert",
    "common.directives.accordion"
]
    )
    .factory("errorHandler", ["$injector", function ($injector) {
        return {
            queue: [],
            limitQueue: function () {
                var pingService = $injector.get("pingService");
                pingService.showOverlay = false;
                if (this.queue.length >= 3) {
                    this.queue.shift();
                }
            },
            sendToServer: function (type, data, datetime) {
                var qmHttp = $injector.get("qmHttp"),
                    postData = {
                        type: type,
                        env: navigator.userAgent,
                        datetime: datetime,
                        data: data
                    };
                qmHttp.post("/error", postData);
            },
            addHTTPError: function (status, method, msg, url, datetime) {
                this.limitQueue();
                this.queue.push({
                    title: "An HTTP error occurred (" + status + ")",
                    type: "error",
                    msgs: [
                        {
                            label: "Message: ",
                            data: msg
                        },
                        {
                            label: "HTTP Method: ",
                            data: method
                        },
                        {
                            label: "URL: ",
                            data: url
                        },
                        {
                            label: "Time: ",
                            data: datetime
                        }
                    ]
                });
                var obj = {method: method, msg: msg, url: url};
                this.sendToServer("HTTP", obj, datetime);
            },
            addTimeoutError: function (method, url, datetime) {
                this.limitQueue();
                this.queue.push({
                    title: "There was a problem connecting to the server!",
                    type: "error",
                    msgs: [
                        {
                            label: "HTTP Method: ",
                            data: method
                        },
                        {
                            label: "URL: ",
                            data: url
                        },
                        {
                            label: "Time: ",
                            data: datetime
                        }
                    ]
                });
                var obj = {method: method, url: url};
                this.sendToServer("Timeout", obj, datetime);
            },
            addAngularError: function (name, msg, stack, route, datetime) {
                this.limitQueue();
                this.queue.push({
                    title: "A client error occurred!",
                    type: "error",
                    msgs: [
                        {
                            label: "Type: ",
                            data: name
                        },
                        {
                            label: "Message: ",
                            data: msg
                        },
                        {
                            label: "Route: ",
                            data: route
                        },
                        {
                            label: "Time: ",
                            data: datetime
                        }
                    ]
                });
                var obj = {name: name, msg: msg, stack: stack, route: route};
                this.sendToServer("Angular", obj, datetime);
            }
        };
    }])
    .directive("errorAlerts", function () {
        return {
            restrict: "A",
            templateUrl: "/services/errorAlerts.tmplt.html",
            controller: [
                "$scope",
                "errorHandler",
                function ($scope, errorHandler) {
                    $scope.$watch(function () {
                        return errorHandler.queue;
                    }, function (n) {
                        $scope.queue = n;
                    }, true);

                    $scope.removeAlert = function (i) {
                        errorHandler.queue.splice(i, 1);
                    };
                }
            ]
        };
    });