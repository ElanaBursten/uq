angular.module('services.workers', [])
    .factory('WorkerFactory', function ($window) {
        var workers = {
            compress: function () {
                onmessage = function (message) {
                    var string;

                    if (message.data.method === 'compress') {
                        string = JSON.stringify(message.data.payload);
                        postMessage({
                            id: message.data.id,
                            payload: LZString.compressToUTF16(string)
                        });
                    } else if (message.data.method === 'decompress') {
                        string = LZString.decompressFromUTF16(message.data.payload);
                        postMessage({
                            id: message.data.id,
                            payload: JSON.parse(string)
                        });
                    }

                    if (message.data.initUrl) {
                        importScripts(message.data.initUrl + '/lib/lz-string/libs/lz-string-1.3.3.js');
                    }
                };
            }
        };

        return {
            create: function (name) {
                if (!workers[name]) {
                    return undefined;
                }

                var worker = new Worker(
                    URL.createObjectURL(
                        new Blob(['(' + workers[name] + ')()'], {type: 'application/javascript'})
                    )
                );
                worker.postMessage({initUrl: $window.location.origin});
                return worker;
            }
        };
    });
