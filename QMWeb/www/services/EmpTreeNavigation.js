angular.module("services.EmpTreeNavigation", ["common.services.qmHttp", "services.timeService", "services.sync"])
    .factory("EmpTreeNavigationService", function (qmHttp, $q, timeService, $location, syncService) {
        var obj = {
            rootNode: null, // should be the large graph
            childrenNodes: [], // smaller graphs on side
            breadcrumbs: []
        },
            addGraphURL = function (n) {
                var time, lineURL, barURL;
                time = timeService.getMoment().add('hours', -6).format('YYYY-MM-DD');
                lineURL = qmHttp.baseUrl +
                    "/metric/total/EMP/" + n.node_id +
                    "/TCLEMP/MIN/" + time;
                n.lineGraphFragment = [lineURL];
                time = timeService.getMoment().add('days', -30).format('YYYY-MM-DD');
                barURL = qmHttp.baseUrl +
                    "/metric/total/EMP/" + n.node_id +
                    "/TCLEMP/DAY/" + time;
                n.barGraphFragment = [barURL];
                return n;
            };
        obj.setBreadCrumbs = function (current) {
            // make a copy of the existing ancestors array using .slice
            var crumbs = current.ancestors.slice(0);
            crumbs.push({
                "id": current.node_id,
                "name": current.short_name
            });
            obj.breadcrumbs = crumbs;
        };
        obj.search = function (identifier) {
            return syncService.search(identifier).then(function (data) {
                var i;
                if (data.length === 1) {
                    $location.url('/managedash/?e=' + data[0].emp_id);
                    return;
                }
                obj.rootNode = {short_name: "Search Results: " + data.length, searchResult: true};
                obj.breadcrumbs = {};
                for (i = 0; i < data.length; i++) {
                    //nodes.push(addGraphURL(data.data[i].node));
                    data[i].node_id = data[i].emp_id;
                }

                obj.childrenNodes = data;
                return obj;
            });
        };
        obj.goToNode = function (identifier) {
            return qmHttp.get("/nodestat/" + identifier).success(function (data) {
                var subs = data.child;
                obj.rootNode = addGraphURL(data.node);
                obj.setBreadCrumbs(data.node);

                obj.childrenNodes.length = 0;
                _.each(subs, function (el) {
                    if (el.due) {
                        obj.childrenNodes.push(addGraphURL(el));
                    }
                });
                return obj;
            });
        };
        return obj;
    })
    .directive("breadCrumbs", function () {
        return {
            scope: {
                breadStack: "=crumbs",
                goTo: "&"
            },
            template:   "<div class='breadcrumbs'>" +
                "<span ng-repeat='b in breadStack'>" +
                "<span class='separator' ng-hide='$first'>/</span>" +
                "<span class='ng-class: {current: $last, " +
                "clicker: !$last};' ng-bind='b.name' " +
                "ng-click='goTo({node:b.id})' id='{{b.id}}'>/</span>" +
                "</span></div>",
            replace: true
        };
    });