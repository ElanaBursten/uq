"use strict";

angular.module("common.services.authentication", [
    "common.services.qmHttp",
    "common.services.errorHandler"
]
    )
    .run(
        function (qmHttp, pingService) {
            qmHttp.get("/ping")
                .success(function (data) {
                    pingService.showOverlay = false;
                });
        }
    )
    .config(function ($httpProvider) {
        var interceptor = [
            '$q',
            "$location",
            "errorHandler",
            "$injector",
            function ($q, $location, errorHandler, $injector) {
                return function (promise) {
                    return promise.then(function (response) {
                        return response;
                    }, function (response) {
                        var qmHttp = $injector.get("qmHttp"),
                            config = response.config;
                        if (config.url === qmHttp.baseUrl + "/error") {
                            return response;
                        }
                        if (response.status === 0) {
                            errorHandler.addTimeoutError(config.method,
                                config.url, new Date());
                        } else if (response.status === 401 ||
                                response.status === 403) {
                            window.location = "login?redirectTo=" +
                                $location.path().substring(1);
                        } else {
                            errorHandler.addHTTPError(response.status,
                                config.method, response.data,
                                config.url, new Date());
                        }
                        return $q.reject(response);
                    });
                };
            }
        ];
        $httpProvider.responseInterceptors.push(interceptor);
    })
    .factory('pingService',
        function (qmHttp, $interval) {
            return {
                didInit: false,
                init: function () {
                    if (this.didInit) {
                        return;
                    }

                    function ping() {
                        qmHttp.get("/ping");
                    }

                    $interval(ping, 4 * 60 * 1000);
                    this.didInit = true;
                },
                showOverlay: true
            };
        }
        )
    .directive("qmOverlay",
        function (pingService) {
            return {
                restrict: "A",
                link: function (scope, el) {
                    scope.$watch(function () {
                        return pingService.showOverlay;
                    }, function (n, o) {
                        if (n === o) {
                            return;
                        }
                        if (!n) {
                            el.delay(1250).fadeOut(500);
                        }
                    });
                }
            };
        }
        );