angular.module("services.syncGraphs", [])
    .service('syncGraphs', [function () {
        return {
            plotFamilies: {},
            getMaxYAxisVal: function (syncFamily) {
                if (!syncFamily.$max) {
                    syncFamily.$max = 0;
                }
                var i, l;
                // only loop through the last plot in the list (l), because if
                // it's in the list, it's already been processed for the max val
                for (i = 0, l = syncFamily.plots.length - 1; i < syncFamily.plots[l].data[0].length; i++) {
                    syncFamily.$max = Math.max(syncFamily.$max, syncFamily.plots[l].data[0][i][1]);
                }

                return Math.round((syncFamily.$max + 1) / 2) * 2;
            }
        };
    }]);
