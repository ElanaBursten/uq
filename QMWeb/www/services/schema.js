var syncSchema = {
    "version" : "1.0",

    "tables" : [
        {
            "name" : "row",
            "indexes" : [
                {
                    "fields" : [
                        "SyncDateTime"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "SyncDateTime",
                    "type" : "ftDateTime"
                }
            ]
        },
        {
            "name" : "addin_info",
            "indexes" : [
                {
                    "fields" : [
                        "addin_info_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "foreign_type_id",
                    "fields" : [
                        "foreign_type", "foreign_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "addin_info_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "foreign_type",
                    "type" : "ftWord"
                }, {
                    "name" : "foreign_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "latitude",
                    "type" : "ftFloat"
                }, {
                    "name" : "longitude",
                    "type" : "ftFloat"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "area",
            "indexes" : [
                {
                    "fields" : [
                        "area_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "area_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "area_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "map_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "asset",
            "indexes" : [
                {
                    "fields" : [
                        "asset_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "asset_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "asset_code",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "asset_number",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "condition",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "solomon_profit_center",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "serial_num",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "cost",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "asset_assignment",
            "indexes" : [
                {
                    "fields" : [
                        "assignment_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "asset_id",
                    "fields" : [
                        "asset_id"
                    ]
                }, {
                    "name" : "emp_id",
                    "fields" : [
                        "emp_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "assignment_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "asset_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "asset_type",
            "indexes" : [
                {
                    "fields" : [
                        "asset_code"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "asset_code",
                    "fields" : [
                        "asset_code"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "asset_code",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "assignment",
            "indexes" : [
                {
                    "fields" : [
                        "assignment_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "assignment_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "downloaded_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_by",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "workload_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "attachment",
            "indexes" : [
                {
                    "fields" : [
                        "attachment_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "foreign_id",
                    "fields" : [
                        "foreign_id"
                    ]
                }, {
                    "name" : "foreign_type",
                    "fields" : [
                        "foreign_type"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "attachment_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "filename",
                    "type" : "ftString",
                    "size" : 64
                }, {
                    "name" : "foreign_type",
                    "type" : "ftWord"
                }, {
                    "name" : "foreign_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "orig_filename",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "extension",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "orig_file_mod_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "attach_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "upload_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "size",
                    "type" : "ftInteger"
                }, {
                    "name" : "attached_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "source",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "file_hash",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "background_upload",
                    "type" : "ftBoolean"
                }, {
                    "name" : "doc_type",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "upload_machine_name",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "upload_try_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "need_to_upload",
                    "type" : "ftBoolean"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_adjustment",
            "indexes" : [
                {
                    "fields" : [
                        "adjustment_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "adjustment_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "adjustment_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "customer_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "amount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "last_modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "which_invoice",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "gl_code",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_detail",
            "indexes" : [
                {
                    "fields" : [
                        "billing_detail_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "billing_detail_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "bill_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "billing_cc",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "ticket_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "work_county",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "work_city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_address_number",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_number_2",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_street",
                    "type" : "ftString",
                    "size" : 45
                }, {
                    "name" : "ticket_type",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "qty_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "closed_how",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "closed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "bill_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "bucket",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "price",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "con_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_done_for",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "work_type",
                    "type" : "ftString",
                    "size" : 90
                }, {
                    "name" : "work_lat",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "work_long",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "rate",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "work_cross",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "call_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "initial_status_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "emp_number",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "short_name",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "work_description",
                    "type" : "ftMemo"
                }, {
                    "name" : "area_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "transmit_count",
                    "type" : "ftInteger"
                }, {
                    "name" : "hours",
                    "type" : "ftBCD",
                    "size" : 3
                }, {
                    "name" : "line_item_text",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "revision",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "map_ref",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "locate_hours_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "qty_charged",
                    "type" : "ftInteger"
                }, {
                    "name" : "which_invoice",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "tax_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "tax_rate",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "tax_amount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "raw_units",
                    "type" : "ftInteger"
                }, {
                    "name" : "update_of",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "high_profile",
                    "type" : "ftBoolean"
                }, {
                    "name" : "plat",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "initial_arrival_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "units_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_remarks",
                    "type" : "ftMemo"
                }, {
                    "name" : "locate_added_by",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "rate_value_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "additional_line_item_text",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_gl",
            "indexes" : [
                {
                    "fields" : [
                        "billing_gl_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "billing_gl_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "charge_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "period_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "gl_code",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_header",
            "indexes" : [
                {
                    "fields" : [
                        "bill_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "bill_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "bill_run_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "bill_start_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "bill_end_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "n_tickets",
                    "type" : "ftInteger"
                }, {
                    "name" : "n_locates",
                    "type" : "ftInteger"
                }, {
                    "name" : "totalamount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "committed",
                    "type" : "ftBoolean"
                }, {
                    "name" : "committed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "center_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "committed_by_emp",
                    "type" : "ftInteger"
                }, {
                    "name" : "period_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "app_version",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_invoice",
            "indexes" : [
                {
                    "fields" : [
                        "invoice_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "billing_header_id",
                    "fields" : [
                        "billing_header_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "invoice_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "billing_header_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "customer_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "end_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "invoice_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "total_amount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "revision",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "which_invoice",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_output_config",
            "indexes" : [
                {
                    "fields" : [
                        "output_config_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "customer_id",
                    "fields" : [
                        "customer_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "output_config_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "customer_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "output_template",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "contract",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "payment_terms",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "cust_po_num",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "output_fields",
                    "type" : "ftMemo"
                }, {
                    "name" : "billing_period",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "flat_fee",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "center_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "SolomonOffice",
                    "type" : "ftString",
                    "size" : 3
                }, {
                    "name" : "SolomonPeriodToPost",
                    "type" : "ftString",
                    "size" : 6
                }, {
                    "name" : "which_invoice",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "sales_tax_rate",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "group_by_price",
                    "type" : "ftBoolean"
                }, {
                    "name" : "omit_zero_amount",
                    "type" : "ftBoolean"
                }, {
                    "name" : "customer_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "transmission_billing",
                    "type" : "ftBoolean"
                }, {
                    "name" : "sales_tax_included",
                    "type" : "ftBoolean"
                }, {
                    "name" : "transmission_bulk",
                    "type" : "ftBoolean"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "plat_map_prefix",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "consol_line_item",
                    "type" : "ftBoolean"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "billing_unit_conversion",
            "indexes" : [
                {
                    "fields" : [
                        "unit_conversion_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "unit_conversion_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "unit_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "first_unit_factor",
                    "type" : "ftInteger"
                }, {
                    "name" : "rest_units_factor",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "break_rules",
            "indexes" : [
                {
                    "fields" : [
                        "rule_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "break_needed_after",
                    "type" : "ftInteger"
                }, {
                    "name" : "break_length",
                    "type" : "ftInteger"
                }, {
                    "name" : "rule_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "rule_message",
                    "type" : "ftMemo"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "message_frequency",
                    "type" : "ftInteger"
                }, {
                    "name" : "button_text",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "break_rules_response",
            "indexes" : [
                {
                    "fields" : [
                        "response_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "response_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "response_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "response",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "contact_phone",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "message_dest_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "call_center",
            "indexes" : [
                {
                    "fields" : [
                        "cc_code"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "cc_code",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "cc_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "admin_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "emergency_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "summary_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "message_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "warning_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "uses_wp",
                    "type" : "ftBoolean"
                }, {
                    "name" : "responder_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "noclient_email",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "audit_method",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "use_prerouting",
                    "type" : "ftBoolean"
                }, {
                    "name" : "notes",
                    "type" : "ftMemo"
                }, {
                    "name" : "track_arrivals",
                    "type" : "ftBoolean"
                }, {
                    "name" : "xml_ticket_format",
                    "type" : "ftMemo"
                }, {
                    "name" : "time_zone",
                    "type" : "ftInteger"
                }, {
                    "name" : "alert_phone_no",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "call_center_hp",
            "indexes" : [
                {
                    "fields" : [
                        "call_center_hp_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "call_center_hp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "hp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "carrier",
            "indexes" : [
                {
                    "fields" : [
                        "carrier_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "carrier_id",
                    "fields" : [
                        "carrier_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "carrier_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "deductible",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "center_group",
            "indexes" : [
                {
                    "fields" : [
                        "center_group_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "center_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "group_code",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "show_for_billing",
                    "type" : "ftBoolean"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "center_group_detail",
            "indexes" : [
                {
                    "fields" : [
                        "center_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "center_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "center_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "center_ticket_summary",
            "indexes" : [
                {
                    "fields" : [
                        "pc_code"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "tickets_open",
                    "type" : "ftInteger"
                }, {
                    "name" : "tickets_due_today",
                    "type" : "ftInteger"
                }, {
                    "name" : "tickets_due_tomorrow",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "client",
            "indexes" : [
                {
                    "fields" : [
                        "client_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "client_name",
                    "fields" : [
                        "client_name"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "client_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "oc_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "office_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "update_call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "grid_email",
                    "type" : "ftMemo"
                }, {
                    "name" : "allow_edit_locates",
                    "type" : "ftBoolean"
                }, {
                    "name" : "customer_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "utility_type",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "status_group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "unit_conversion_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "alert",
                    "type" : "ftBoolean"
                }, {
                    "name" : "require_locate_units",
                    "type" : "ftBoolean"
                }, {
                    "name" : "attachment_url",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "configuration_data",
            "indexes" : [
                {
                    "fields" : [
                        "name"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "name",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "value",
                    "type" : "ftMemo"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "editable",
                    "type" : "ftBoolean"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "customer",
            "indexes" : [
                {
                    "fields" : [
                        "customer_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "customer_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "customer_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "customer_description",
                    "type" : "ftString",
                    "size" : 150
                }, {
                    "name" : "customer_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "city",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "zip",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "street",
                    "type" : "ftString",
                    "size" : 35
                }, {
                    "name" : "phone",
                    "type" : "ftString",
                    "size" : 16
                }, {
                    "name" : "locating_company",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "street_2",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "attention",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "contact_name",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "contact_email",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "period_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "contract",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage",
            "indexes" : [
                {
                    "fields" : [
                        "damage_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "office_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_inv_num",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "damage_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "damage_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "uq_notified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "notified_by_person",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "notified_by_company",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "notified_by_phone",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "client_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "client_claim_id",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "date_mailed",
                    "type" : "ftDateTime"
                }, {
                    "name" : "date_faxed",
                    "type" : "ftDateTime"
                }, {
                    "name" : "sent_to",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "size_type",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "location",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "page",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "grid",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "county",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "utility_co_damaged",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "diagram_number",
                    "type" : "ftInteger"
                }, {
                    "name" : "remarks",
                    "type" : "ftMemo"
                }, {
                    "name" : "investigator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "investigator_arrival",
                    "type" : "ftDateTime"
                }, {
                    "name" : "investigator_departure",
                    "type" : "ftDateTime"
                }, {
                    "name" : "investigator_est_damage_time",
                    "type" : "ftDateTime"
                }, {
                    "name" : "investigator_narrative",
                    "type" : "ftMemo"
                }, {
                    "name" : "excavator_company",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "excavator_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "excavation_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "locate_marks_present",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "locate_requested",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "site_mark_state",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "site_sewer_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_water_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_catv_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_gas_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_power_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_tel_marked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_other_marked",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "site_pictures",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "site_marks_measurement",
                    "type" : "ftBCD",
                    "size" : 3
                }, {
                    "name" : "site_buried_under",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "site_clarity_of_marks",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "site_hand_dig",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_mechanized_equip",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_paint_present",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_flags_present",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_exc_boring",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_exc_grading",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_exc_open_trench",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_img_35mm",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_img_digital",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_img_video",
                    "type" : "ftBoolean"
                }, {
                    "name" : "disc_repair_techs_were",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_repairs_were",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_repair_person",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_repair_contact",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_repair_comment",
                    "type" : "ftMemo"
                }, {
                    "name" : "disc_exc_were",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_exc_person",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_exc_contact",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_exc_comment",
                    "type" : "ftMemo"
                }, {
                    "name" : "disc_other1_person",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_other1_contact",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_other1_comment",
                    "type" : "ftMemo"
                }, {
                    "name" : "disc_other2_person",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_other2_contact",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "disc_other2_comment",
                    "type" : "ftMemo"
                }, {
                    "name" : "exc_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "exc_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "exc_resp_other_desc",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "exc_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "exc_resp_response",
                    "type" : "ftMemo"
                }, {
                    "name" : "uq_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "uq_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "uq_resp_other_desc",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "uq_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "spc_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "spc_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "spc_resp_other_desc",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "spc_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "profit_center",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "utilistar_id",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "closed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "uq_resp_ess_step",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "facility_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "facility_size",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "locator_experience",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "locate_equipment",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "was_project",
                    "type" : "ftBoolean"
                }, {
                    "name" : "claim_status",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "claim_status_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "invoice_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "facility_material",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "accrual_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "invoice_code_initial_value",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "uq_damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "estimate_locked",
                    "type" : "ftBoolean"
                }, {
                    "name" : "site_marked_in_white",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "site_tracer_wire_intact",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "site_pot_holed",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "site_nearest_fac_measure",
                    "type" : "ftBCD",
                    "size" : 3
                }, {
                    "name" : "excavator_doing_repairs",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "offset_visible",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "offset_qty",
                    "type" : "ftBCD",
                    "size" : 3
                }, {
                    "name" : "claim_coordinator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "estimate_agreed",
                    "type" : "ftBoolean"
                }, {
                    "name" : "approved_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "approved_datetime",
                    "type" : "ftDateTime"
                }, {
                    "name" : "reason_changed",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_priority_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "user_changed_pc",
                    "type" : "ftBoolean"
                }, {
                    "name" : "marks_within_tolerance",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_bill",
            "indexes" : [
                {
                    "fields" : [
                        "damage_bill_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "damage_id",
                    "fields" : [
                        "damage_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "damage_bill_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "billing_period_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "billable",
                    "type" : "ftBoolean"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "invoiced",
                    "type" : "ftBoolean"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_default_est",
            "indexes" : [
                {
                    "fields" : [
                        "ddest_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "ddest_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "facility_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "facility_size",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "facility_material",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "profit_center",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "default_amount",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "utility_co_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_estimate",
            "indexes" : [
                {
                    "fields" : [
                        "estimate_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "damage_id",
                    "fields" : [
                        "damage_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "estimate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "third_party",
                    "type" : "ftBoolean"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "amount",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "company",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "contract",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "phone",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "utilistar_id",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "estimate_type",
                    "type" : "ftInteger"
                }, {
                    "name" : "third_party_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "litigation_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_history",
            "indexes" : [
                {
                    "fields" : [
                        "damage_id", "modified_date"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "damage_id",
                    "fields" : [
                        "damage_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "profit_center",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "exc_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "exc_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "uq_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "uq_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "spc_resp_code",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "spc_resp_type",
                    "type" : "ftString",
                    "size" : 4
                }, {
                    "name" : "exc_resp_other_desc",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "exc_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "exc_resp_response",
                    "type" : "ftMemo"
                }, {
                    "name" : "uq_resp_other_desc",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "uq_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "uq_resp_ess_step",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "spc_resp_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "reason_changed",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_invoice",
            "indexes" : [
                {
                    "fields" : [
                        "invoice_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "invoice_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "estimate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "invoice_num",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "amount",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "company",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "received_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "damage_city",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "damage_state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "damage_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "satisfied",
                    "type" : "ftBoolean"
                }, {
                    "name" : "satisfied_by_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "satisfied_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "approved_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "approved_amount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "paid_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "paid_amount",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "invoice_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "cust_invoice_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "packet_request_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "packet_recv_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "invoice_approved",
                    "type" : "ftBoolean"
                }, {
                    "name" : "litigation_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "third_party_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "payment_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "invoice_type",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_litigation",
            "indexes" : [
                {
                    "fields" : [
                        "litigation_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "damage_id",
                    "fields" : [
                        "damage_id"
                    ]
                }, {
                    "name" : "carrier_id",
                    "fields" : [
                        "carrier_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "litigation_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "file_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "plaintiff",
                    "type" : "ftString",
                    "size" : 45
                }, {
                    "name" : "carrier_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "attorney",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "party",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "demand",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "accrual",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "settlement",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "closed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "comment",
                    "type" : "ftMemo"
                }, {
                    "name" : "defendant",
                    "type" : "ftString",
                    "size" : 45
                }, {
                    "name" : "venue",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "venue_state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "service_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "summons_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "carrier_notified",
                    "type" : "ftBoolean"
                }, {
                    "name" : "claim_status",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_profit_center_rule",
            "indexes" : [
                {
                    "fields" : [
                        "rule_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "county",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "damage_third_party",
            "indexes" : [
                {
                    "fields" : [
                        "third_party_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "third_party_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "claimant",
                    "type" : "ftString",
                    "size" : 45
                }, {
                    "name" : "address1",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "address2",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "city",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "zipcode",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "phone",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "claim_desc",
                    "type" : "ftMemo"
                }, {
                    "name" : "uq_notified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "carrier_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "carrier_notified",
                    "type" : "ftBoolean"
                }, {
                    "name" : "demand",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "claim_status",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "document",
            "indexes" : [
                {
                    "fields" : [
                        "doc_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "doc_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "foreign_type",
                    "type" : "ftInteger"
                }, {
                    "name" : "foreign_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "required_doc_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "comment",
                    "type" : "ftString",
                    "size" : 250
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "editable_areas",
            "indexes" : [
                {
                    "fields" : [
                        "area_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "map_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "map_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "county",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "ticket_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "area_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "area_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "email_queue_result",
            "indexes" : [
                {
                    "fields" : [
                        "eqr_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "eqr_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "email_address",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "sent_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "error_details",
                    "type" : "ftMemo"
                }, {
                    "name" : "ls_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "emp_group_right",
            "indexes" : [
                {
                    "fields" : [
                        "emp_id", "right_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "group_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "allowed",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "limitation",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "emp_hier",
            "indexes" : [
                {
                    "fields" : [
                        "emp_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "lft_idx",
                    "fields" : [
                        "lft"
                    ],
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "emp_id",
                    "type" : "ftInteger",
                    "attributes" : "faRequired"
                }, {
                    "name" : "lft",
                    "type" : "ftInteger",
                    "attributes" : "faRequired"
                }, {
                    "name" : "rght",
                    "type" : "ftInteger",
                    "attributes" : "faRequired"
                }, {
                    "name" : "depth",
                    "type" : "ftInteger",
                    "attributes" : "faRequired"
                }, {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }
            ]
        },

        {
            "name" : "emp_workload",
            "indexes" : [
                {
                    "fields" : [
                        "emp_tree_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "emp_tree_id",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "report_to",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "emp_number",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "short_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "sort",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_view_limit",
                    "type" : "ftInteger"
                }, {
                    "name" : "show_future_tickets",
                    "type" : "ftBoolean"
                }, {
                    "name" : "under_me",
                    "type" : "ftBoolean"
                }, {
                    "name" : "is_manager",
                    "type" : "ftBoolean"
                }, {
                    "name" : "tickets_tot",
                    "type" : "ftInteger"
                }, {
                    "name" : "locates_tot",
                    "type" : "ftInteger"
                }, {
                    "name" : "damages_tot",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_tot",
                    "type" : "ftInteger"
                }, {
                    "name" : "node_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "work_status_today",
                    "type" : "ftString",
                    "size" : 3
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "employee",
            "indexes" : [
                {
                    "fields" : [
                        "emp_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "type_id",
                    "fields" : [
                        "type_id"
                    ]
                }, {
                    "name" : "report_to",
                    "fields" : [
                        "report_to"
                    ]
                }, {
                    "name" : "repr_pc_code",
                    "fields" : [
                        "repr_pc_code"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "timesheet_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_number",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "short_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "first_name",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "last_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "middle_init",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "report_to",
                    "type" : "ftInteger"
                }, {
                    "name" : "create_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "create_uid",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_uid",
                    "type" : "ftInteger"
                }, {
                    "name" : "charge_cov",
                    "type" : "ftBoolean"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "can_receive_tickets",
                    "type" : "ftBoolean"
                }, {
                    "name" : "timerule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "dialup_user",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "company_car",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "repr_pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "payroll_pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "crew_num",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "autoclose",
                    "type" : "ftBoolean"
                }, {
                    "name" : "company_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "rights_modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "ticket_view_limit",
                    "type" : "ftInteger"
                }, {
                    "name" : "hire_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "show_future_tickets",
                    "type" : "ftBoolean"
                }, {
                    "name" : "incentive_pay",
                    "type" : "ftBoolean"
                }, {
                    "name" : "under_me_timesheets",
                    "type" : "ftBoolean"
                }, {
                    "name" : "under_me_reports",
                    "type" : "ftBoolean"
                }, {
                    "name" : "under_me",
                    "type" : "ftBoolean"
                }, {
                    "name" : "effective_pc",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "effective_payroll_pc",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "employee_activity",
            "indexes" : [
                {
                    "fields" : [
                        "emp_activity_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "emp_activity_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "activity_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "activity_type",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "details",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "extra_details",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "employee_right",
            "indexes" : [
                {
                    "fields" : [
                        "emp_right_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "emp_right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "allowed",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "limitation",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "error_report",
            "indexes" : [
                {
                    "fields" : [
                        "error_report_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "error_report_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "occurred_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "report_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "severity",
                    "type" : "ftInteger"
                }, {
                    "name" : "error_text",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "details",
                    "type" : "ftMemo"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "exported_damages_history",
            "indexes" : [
                {
                    "fields" : [
                        "damage_id", "short_period"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "short_period",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "estimate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "project",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "task",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "claim_number",
                    "type" : "ftInteger"
                }, {
                    "name" : "invoice_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "liability_amount",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "change_in_liability_amount",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "followup_ticket_type",
            "indexes" : [
                {
                    "fields" : [
                        "type_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "type_id",
                    "fields" : [
                        "type_id"
                    ]
                }, {
                    "name" : "call_center",
                    "fields" : [
                        "call_center"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "ticket_type",
                    "type" : "ftString",
                    "size" : 38
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "gps_position",
            "indexes" : [
                {
                    "fields" : [
                        "gps_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "gps_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "latitude",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "longitude",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "hdop_feet",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "highlight_rule",
            "indexes" : [
                {
                    "fields" : [
                        "highlight_rule_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "highlight_rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "field_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "word_regex",
                    "type" : "ftMemo"
                }, {
                    "name" : "regex_modifier",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "which_match",
                    "type" : "ftInteger"
                }, {
                    "name" : "highlight_bold",
                    "type" : "ftBoolean"
                }, {
                    "name" : "highlight_color",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "jobsite_arrival",
            "indexes" : [
                {
                    "fields" : [
                        "arrival_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "arrival_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "arrival_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "entry_method",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "deleted_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "deleted_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "location_status",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locate",
            "indexes" : [
                {
                    "fields" : [
                        "locate_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }, {
                    "name" : "closed",
                    "fields" : [
                        "closed"
                    ]
                }, {
                    "name" : "locator_id",
                    "fields" : [
                        "locator_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "client_code",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "high_profile",
                    "type" : "ftBoolean"
                }, {
                    "name" : "qty_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "price",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "closed",
                    "type" : "ftBoolean"
                }, {
                    "name" : "closed_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "closed_how",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "closed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "invoiced",
                    "type" : "ftBoolean"
                }, {
                    "name" : "assigned_to",
                    "type" : "ftInteger"
                }, {
                    "name" : "regular_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "overtime_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "added_by",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "watch_and_protect",
                    "type" : "ftBoolean"
                }, {
                    "name" : "high_profile_reason",
                    "type" : "ftInteger"
                }, {
                    "name" : "seq_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "assigned_to_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "mark_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "alert",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "entry_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "gps_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status_changed_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locator_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "StatusAsAssignedUser",
                    "type" : "ftInteger"
                }, {
                    "name" : "length_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "length_total",
                    "type" : "ftInteger"
                }, {
                    "name" : "workload_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locate_facility",
            "indexes" : [
                {
                    "fields" : [
                        "locate_facility_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "locate_facility_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "facility_type",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "facility_material",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "facility_size",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "facility_pressure",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "offset",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "deleted_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "deleted_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locate_hours",
            "indexes" : [
                {
                    "fields" : [
                        "locate_hours_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "locate_hours_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "regular_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "overtime_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "entry_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "units_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "hours_invoiced",
                    "type" : "ftBoolean"
                }, {
                    "name" : "units_invoiced",
                    "type" : "ftBoolean"
                }, {
                    "name" : "unit_conversion_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locate_plat",
            "indexes" : [
                {
                    "fields" : [
                        "locate_plat_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "locate_plat_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "plat",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locate_status",
            "indexes" : [
                {
                    "fields" : [
                        "ls_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "ls_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "high_profile",
                    "type" : "ftBoolean"
                }, {
                    "name" : "qty_marked",
                    "type" : "ftInteger"
                }, {
                    "name" : "statused_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "statused_how",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "regular_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "overtime_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "watch_and_protect",
                    "type" : "ftBoolean"
                }, {
                    "name" : "high_profile_reason",
                    "type" : "ftInteger"
                }, {
                    "name" : "mark_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "entry_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "gps_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status_changed_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "locating_company",
            "indexes" : [
                {
                    "fields" : [
                        "company_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "company_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "name",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "logo_filename",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "address_street",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "address_city",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "address_state",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "address_zip",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "phone",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "billing_footer",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "floating_holidays_per_year",
                    "type" : "ftWord"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "payroll_company_code",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "message",
            "indexes" : [
                {
                    "fields" : [
                        "message_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "message_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "message_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "from_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "destination",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "subject",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "body",
                    "type" : "ftMemo"
                }, {
                    "name" : "sent_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "expiration_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "show_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "message_dest",
            "indexes" : [
                {
                    "fields" : [
                        "message_dest_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ack_date",
                    "fields" : [
                        "ack_date"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "message_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "message_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "from_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "destination",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "subject",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "body",
                    "type" : "ftMemo"
                }, {
                    "name" : "sent_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "show_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "expiration_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "message_dest_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ack_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "read_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "message_recip",
            "indexes" : [
                {
                    "fields" : [
                        "message_dest_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "message_id",
                    "fields" : [
                        "message_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "message_dest_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "message_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ack_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "read_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "rule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "tse_entry_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "notes",
            "indexes" : [
                {
                    "fields" : [
                        "notes_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "foreign_type_id",
                    "fields" : [
                        "foreign_type", "foreign_id"
                    ]
                }, {
                    "name" : "DeltaStatus",
                    "fields" : [
                        "DeltaStatus"
                    ]
                }, {
                    "name" : "damage_id",
                    "fields" : [
                        "damage_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "notes_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "foreign_type",
                    "type" : "ftWord"
                }, {
                    "name" : "foreign_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "entry_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "uid",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "note",
                    "type" : "ftMemo"
                }, {
                    "name" : "damage_notes_type",
                    "type" : "ftInteger"
                }, {
                    "name" : "damage_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "office",
            "indexes" : [
                {
                    "fields" : [
                        "office_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "office_name",
                    "fields" : [
                        "office_name"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "office_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "office_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "profit_center",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "hours_to_respond",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "company_name",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "address1",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "address2",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "city",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "zipcode",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "phone",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "fax",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "open_locate",
            "indexes" : [
                {
                    "fields" : [
                        "locate_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "profit_center",
            "indexes" : [
                {
                    "fields" : [
                        "pc_code"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "pc_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "gl_code",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "timesheet_num",
                    "type" : "ftString",
                    "size" : 25
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "company_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "billing_contact",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "adp_code",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "pc_city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "manager_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "reference",
            "indexes" : [
                {
                    "fields" : [
                        "ref_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "default",
                    "fields" : [
                        "type", "sortby"
                    ]
                }, {
                    "name" : "code",
                    "fields" : [
                        "code"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "ref_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "sortby",
                    "type" : "ftInteger"
                }, {
                    "name" : "active_ind",
                    "type" : "ftBoolean"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modifier",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "report_log",
            "indexes" : [
                {
                    "fields" : [
                        "request_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "request_id",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "report_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "request_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "update_count",
                    "type" : "ftInteger"
                }, {
                    "name" : "next_update",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "required_document",
            "indexes" : [
                {
                    "fields" : [
                        "required_doc_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "required_doc_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "foreign_type",
                    "type" : "ftInteger"
                }, {
                    "name" : "name",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "require_for_locate",
                    "type" : "ftBoolean"
                }, {
                    "name" : "require_for_no_locate",
                    "type" : "ftBoolean"
                }, {
                    "name" : "require_for_min_estimate",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "user_selectable",
                    "type" : "ftBoolean"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "required_document_type",
            "indexes" : [
                {
                    "fields" : [
                        "rdt_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "rdt_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "required_doc_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "doc_type",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "response_log",
            "indexes" : [
                {
                    "fields" : [
                        "response_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "locate_id",
                    "fields" : [
                        "locate_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "response_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "locate_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "response_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "call_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "response_sent",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "success",
                    "type" : "ftBoolean"
                }, {
                    "name" : "reply",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "restricted_use_exemption",
            "indexes" : [
                {
                    "fields" : [
                        "rue_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "rue_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "effective_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "expire_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "granted_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "revoked_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "revoked_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "restricted_use_message",
            "indexes" : [
                {
                    "fields" : [
                        "rum_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "rum_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "countdown_minutes",
                    "type" : "ftInteger"
                }, {
                    "name" : "message_text",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "right_definition",
            "indexes" : [
                {
                    "fields" : [
                        "right_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "right_description",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "right_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "entity_data",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "parent_right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modifier_desc",
                    "type" : "ftString",
                    "size" : 150
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "right_restriction",
            "indexes" : [
                {
                    "fields" : [
                        "right_restriction_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "right_restriction_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "group_name",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "restriction_type",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "start_value",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "end_value",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "status_group",
            "indexes" : [
                {
                    "fields" : [
                        "sg_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "sg_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "sg_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "right_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "status_group_item",
            "indexes" : [
                {
                    "fields" : [
                        "sg_item_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "sg_item_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "sg_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "statuslist",
            "indexes" : [
                {
                    "fields" : [
                        "status"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "status_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "billable",
                    "type" : "ftBoolean"
                }, {
                    "name" : "complete",
                    "type" : "ftBoolean"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "mark_type_required",
                    "type" : "ftBoolean"
                }, {
                    "name" : "default_mark_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "allow_locate_units",
                    "type" : "ftBoolean"
                }, {
                    "name" : "meet_only",
                    "type" : "ftBoolean"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "syncstatus",
            "indexes" : [
                {
                    "fields" : [
                        "TableName"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "TableName",
                    "type" : "ftString",
                    "size" : 25,
                    "attributes" : "faRequired"
                }, {
                    "name" : "PrimaryKey",
                    "type" : "ftString",
                    "size" : 45
                }, {
                    "name" : "LastSyncDown",
                    "type" : "ftString",
                    "size" : 25
                }
            ]
        },

        {
            "name" : "task_schedule",
            "indexes" : [
                {
                    "fields" : [
                        "task_schedule_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }, {
                    "name" : "est_start_date",
                    "fields" : [
                        "est_start_date"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "task_schedule_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "est_start_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "performed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "ticket",
            "indexes" : [
                {
                    "fields" : [
                        "ticket_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "kind",
                    "fields" : [
                        "kind"
                    ]
                }, {
                    "name" : "due_date",
                    "fields" : [
                        "due_date"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "parsed_ok",
                    "type" : "ftBoolean"
                }, {
                    "name" : "ticket_format",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "kind",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "map_page",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "revision",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_description",
                    "type" : "ftMemo"
                }, {
                    "name" : "work_state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "work_county",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_address_number",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_number_2",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_street",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "work_cross",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "work_subdivision",
                    "type" : "ftString",
                    "size" : 70
                }, {
                    "name" : "work_type",
                    "type" : "ftString",
                    "size" : 90
                }, {
                    "name" : "work_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_notc",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_remarks",
                    "type" : "ftMemo"
                }, {
                    "name" : "priority",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "legal_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "legal_good_thru",
                    "type" : "ftDateTime"
                }, {
                    "name" : "legal_restake",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "respond_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "duration",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "company",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "con_type",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "con_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "con_address",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "con_city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "con_state",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "con_zip",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "call_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "caller",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "caller_contact",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "caller_phone",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_cellular",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_fax",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_altcontact",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_altphone",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_email",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "operator",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "channel",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_lat",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "work_long",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "image",
                    "type" : "ftMemo"
                }, {
                    "name" : "parse_errors",
                    "type" : "ftMemo"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "ticket_type",
                    "type" : "ftString",
                    "size" : 38
                }, {
                    "name" : "legal_due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "parent_ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "do_not_mark_before",
                    "type" : "ftDateTime"
                }, {
                    "name" : "route_area_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "watch_and_protect",
                    "type" : "ftBoolean"
                }, {
                    "name" : "service_area_code",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "explosives",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "serial_number",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "map_ref",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "followup_type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "do_not_respond_before",
                    "type" : "ftDateTime"
                }, {
                    "name" : "recv_manager_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ward",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "source",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "update_of",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "alert",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "work_priority_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "do_not_route_until",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_extent",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "geocode_precision",
                    "type" : "ftInteger"
                }, {
                    "name" : "route_area_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "area_changed",
                    "type" : "ftBoolean"
                }, {
                    "name" : "area_changed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "followup_transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "followup_due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "ticket_ack",
            "indexes" : [
                {
                    "fields" : [
                        "ticket_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "ticket_ack_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_version_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "has_ack",
                    "type" : "ftBoolean"
                }, {
                    "name" : "ack_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "ack_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "locator_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "ticket_info",
            "indexes" : [
                {
                    "fields" : [
                        "ticket_info_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "ticket_info_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "info_type",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "info",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "added_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "ticket_version",
            "indexes" : [
                {
                    "fields" : [
                        "ticket_version_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "ticket_id",
                    "fields" : [
                        "ticket_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "ticket_version_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_revision",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "ticket_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "ticket_type",
                    "type" : "ftString",
                    "size" : 38
                }, {
                    "name" : "transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "processed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "arrival_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "serial_number",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "image",
                    "type" : "ftMemo"
                }, {
                    "name" : "filename",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "ticket_format",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "source",
                    "type" : "ftString",
                    "size" : 12
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "timesheet_entry",
            "indexes" : [
                {
                    "fields" : [
                        "entry_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "work_emp_id_work_date_status",
                    "fields" : [
                        "work_emp_id", "work_date", "status"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "entry_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "entry_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "entry_date_local",
                    "type" : "ftDateTime"
                }, {
                    "name" : "entry_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "approve_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "approve_date_local",
                    "type" : "ftDateTime"
                }, {
                    "name" : "approve_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "final_approve_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "final_approve_date_local",
                    "type" : "ftDateTime"
                }, {
                    "name" : "final_approve_by",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_start1",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop1",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start2",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop2",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start3",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop3",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start4",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop4",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start5",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop5",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start1",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop1",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start2",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop2",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start3",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop3",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start4",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop4",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start5",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop5",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start6",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop6",
                    "type" : "ftDateTime"
                }, {
                    "name" : "miles_start1",
                    "type" : "ftInteger"
                }, {
                    "name" : "miles_stop1",
                    "type" : "ftInteger"
                }, {
                    "name" : "vehicle_use",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "reg_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "ot_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "dt_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "callout_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "vac_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "leave_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "br_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "hol_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "jury_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "floating_holiday",
                    "type" : "ftBoolean"
                }, {
                    "name" : "rule_ack_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "rule_id_ack",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_pc_code",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "reason_changed",
                    "type" : "ftInteger"
                }, {
                    "name" : "lunch_start",
                    "type" : "ftDateTime"
                }, {
                    "name" : "source",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_start6",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop6",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start7",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop7",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start8",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop8",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start9",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop9",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start10",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop10",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start11",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop11",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start12",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop12",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start13",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop13",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start14",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop14",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start15",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop15",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_start16",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_stop16",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start7",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop7",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start8",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop8",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start9",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop9",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start10",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop10",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start11",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop11",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start12",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop12",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start13",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop13",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start14",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop14",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start15",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop15",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_start16",
                    "type" : "ftDateTime"
                }, {
                    "name" : "callout_stop16",
                    "type" : "ftDateTime"
                }, {
                    "name" : "pto_hours",
                    "type" : "ftBCD",
                    "size" : 2
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "upload_file_type",
            "indexes" : [
                {
                    "fields" : [
                        "upload_file_type_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "upload_file_type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "extension",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "max_kilobytes",
                    "type" : "ftInteger"
                }, {
                    "name" : "size_error_message",
                    "type" : "ftString",
                    "size" : 200
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "upload_location",
            "indexes" : [
                {
                    "fields" : [
                        "location_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "location_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "description",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "server",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "password",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "username",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "directory",
                    "type" : "ftString",
                    "size" : 150
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "group_name",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "port",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "users",
            "indexes" : [
                {
                    "fields" : [
                        "uid"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "uid",
                    "type" : "ftInteger"
                }, {
                    "name" : "grp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "emp_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "first_name",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "last_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "login_id",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "password",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "chg_pwd",
                    "type" : "ftBoolean"
                }, {
                    "name" : "last_login",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active_ind",
                    "type" : "ftBoolean"
                }, {
                    "name" : "can_view_notes",
                    "type" : "ftBoolean"
                }, {
                    "name" : "can_view_history",
                    "type" : "ftBoolean"
                }, {
                    "name" : "chg_pwd_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "email_address",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "etl_access",
                    "type" : "ftBoolean"
                }, {
                    "name" : "api_key",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "widget",
            "indexes" : [
                {
                    "fields" : [
                        "widget_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "widget_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "owner_name",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "length",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "wo_assignment",
            "indexes" : [
                {
                    "fields" : [
                        "assignment_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "assigned_to_id",
                    "fields" : [
                        "assigned_to_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "assignment_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "assigned_to_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "added_by",
                    "type" : "ftString",
                    "size" : 8
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "wo_response_log",
            "indexes" : [
                {
                    "fields" : [
                        "wo_response_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "wo_id",
                    "fields" : [
                        "wo_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "wo_response_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "response_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "wo_source",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "response_sent",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "success",
                    "type" : "ftBoolean"
                }, {
                    "name" : "reply",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "wo_status_history",
            "indexes" : [
                {
                    "fields" : [
                        "wo_status_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "wo_status_id",
                    "fields" : [
                        "wo_status_id"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "wo_status_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "status_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "statused_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "statused_how",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "insert_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "cga_reason",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order",
            "indexes" : [
                {
                    "fields" : [
                        "wo_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "kind",
                    "fields" : [
                        "kind"
                    ]
                }, {
                    "name" : "due_date",
                    "fields" : [
                        "due_date"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "assigned_to_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "client_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "parsed_ok",
                    "type" : "ftBoolean"
                }, {
                    "name" : "wo_source",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "kind",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "status",
                    "type" : "ftString",
                    "size" : 5
                }, {
                    "name" : "closed",
                    "type" : "ftBoolean"
                }, {
                    "name" : "map_page",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "map_ref",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "closed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "status_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "cancelled_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "work_type",
                    "type" : "ftString",
                    "size" : 90
                }, {
                    "name" : "work_description",
                    "type" : "ftMemo"
                }, {
                    "name" : "work_address_number",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_number_2",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_address_street",
                    "type" : "ftString",
                    "size" : 60
                }, {
                    "name" : "work_cross",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "work_county",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_city",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "work_state",
                    "type" : "ftString",
                    "size" : 2
                }, {
                    "name" : "work_zip",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_lat",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "work_long",
                    "type" : "ftBCD",
                    "size" : 6
                }, {
                    "name" : "caller_name",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "caller_contact",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "caller_phone",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_cellular",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_fax",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_altcontact",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_altphone",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "caller_email",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "client_wo_number",
                    "type" : "ftString",
                    "size" : 40
                }, {
                    "name" : "image",
                    "type" : "ftMemo"
                }, {
                    "name" : "parse_errors",
                    "type" : "ftMemo"
                }, {
                    "name" : "update_of_wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "job_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "client_order_num",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "client_master_order_num",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "wire_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "work_center",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "central_office",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "serving_terminal",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "circuit_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "f2_cable",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "terminal_port",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "f2_pair",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "state_hwy_row",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "road_bore_count",
                    "type" : "ftInteger"
                }, {
                    "name" : "driveway_bore_count",
                    "type" : "ftInteger"
                }, {
                    "name" : "call_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "source_sent_attachment",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "map_x_coord",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "map_y_coord",
                    "type" : "ftBCD",
                    "size" : 4
                }, {
                    "name" : "statused_how",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "statused_by_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order_inspection",
            "indexes" : [
                {
                    "fields" : [
                        "wo_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "account_number",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "premise_id",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "meter_number",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "compliance_due_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "actual_meter_number",
                    "type" : "ftString",
                    "size" : 80
                }, {
                    "name" : "actual_meter_location",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "building_type",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "cga_visits",
                    "type" : "ftInteger"
                }, {
                    "name" : "map_status",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "mercury_regulator",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "vent_clearance_dist",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "vent_ignition_dist",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "alert_first_name",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "alert_last_name",
                    "type" : "ftString",
                    "size" : 30
                }, {
                    "name" : "alert_order_number",
                    "type" : "ftString",
                    "size" : 50
                }, {
                    "name" : "inspection_change_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "cga_reason",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "invoiced",
                    "type" : "ftBoolean"
                }, {
                    "name" : "gas_light",
                    "type" : "ftString",
                    "size" : 15
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order_remedy",
            "indexes" : [
                {
                    "fields" : [
                        "wo_remedy_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "wo_remedy_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order_ticket",
            "indexes" : [
                {
                    "fields" : [
                        "wo_id", "ticket_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "ticket_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order_version",
            "indexes" : [
                {
                    "fields" : [
                        "wo_version_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }, {
                    "name" : "wo_id",
                    "fields" : [
                        "wo_id"
                    ]
                }, {
                    "name" : "transmit_date",
                    "fields" : [
                        "transmit_date"
                    ]
                }
            ],
            "fields" : [
                {
                    "name" : "wo_version_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "wo_revision",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "wo_number",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "work_type",
                    "type" : "ftString",
                    "size" : 90
                }, {
                    "name" : "transmit_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "processed_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "arrival_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "image",
                    "type" : "ftMemo"
                }, {
                    "name" : "filename",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "wo_source",
                    "type" : "ftString",
                    "size" : 20
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        },

        {
            "name" : "work_order_work_type",
            "indexes" : [
                {
                    "fields" : [
                        "work_type_id"
                    ],
                    "primary" : true,
                    "unique" : true
                }
            ],
            "fields" : [
                {
                    "name" : "work_type_id",
                    "type" : "ftInteger"
                }, {
                    "name" : "work_type",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "work_description",
                    "type" : "ftString",
                    "size" : 100
                }, {
                    "name" : "flag_color",
                    "type" : "ftString",
                    "size" : 10
                }, {
                    "name" : "alert",
                    "type" : "ftBoolean"
                }, {
                    "name" : "active",
                    "type" : "ftBoolean",
                    "attributes" : "faRequired"
                }, {
                    "name" : "modified_date",
                    "type" : "ftDateTime"
                }, {
                    "name" : "DeltaStatus",
                    "type" : "ftString",
                    "size" : 1
                }, {
                    "name" : "LocalKey",
                    "type" : "ftDateTime"
                }, {
                    "name" : "LocalStringKey",
                    "type" : "ftString",
                    "size" : 20
                }
            ]
        }

    ]
};