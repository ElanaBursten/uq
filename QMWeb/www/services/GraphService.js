angular.module("services.GraphService", ["services.syncGraphs"])
    .factory("GraphService", function (syncGraphs) {
        var obj = {};
        obj.graphHeight = 75;
        obj.setHeight = function (el, heightClass) {
            if (heightClass === undefined || heightClass === "") {
                el.height(this.graphHeight);
            } else {
                el.addClass(heightClass);
            }
        };
        obj.mapGraphData = function (data) {
            var map = [];
            _.each(data, function (element, index) {
                map.push([index, element]);
            });
            return map;
        };
        obj.plotGraph = function (el, data, opts) {
            if (!el || !data || !opts) {
                throw "Error plotting graph: Incorrect parameters.";
            }
            if (el && (el.width() > 0 && el.height() > 0)) {
                var plot = ({el: el, data: data, opts: opts});
                $.plot(el, data, opts);
                return plot;
            }
            return null;
        };
        obj.onSourceUrlChange = function (newUrls, scope) {
            if (newUrls.length <= 0) { // cleared all URLs, empty graph
                clearTimeout(scope.timeout);
                scope.displays = [];
                scope.datas = [];
                scope.plot = scope.plotGraph(scope.graph, [[]], scope.getGraphOptions());
                scope.numberOfSources = 0;
            } else if (newUrls.length > scope.numberOfSources) { // added a URL
                var newURL = newUrls.slice(scope.numberOfSources, newUrls.length);
                if (!angular.isArray(newURL)) {
                    newURL = [newURL];
                }
                scope.draw(newURL);
                scope.numberOfSources++;
            } else if (newUrls.length < scope.numberOfSources) { // removed a URL
                scope.displays.pop();
                scope.datas.pop();
                scope.plot = scope.plotGraph(scope.graph, scope.displays, scope.getGraphOptions());
                scope.numberOfSources--;
            }
        };

        // returns the maxY value for the axis,
        // so the directive which calls this can redraw the graph
        obj.syncGraphs = function (n, scope) {
            if (n && scope.syncFamily && scope.syncData) {
                if (!scope.syncData[scope.syncFamily]) {
                    scope.syncData[scope.syncFamily] = {plots: []};
                }
                scope.syncData[scope.syncFamily].plots.push(n);
                return syncGraphs.getMaxYAxisVal(scope.syncData[scope.syncFamily]);
            }
            return null;
        };
        return obj;
    });