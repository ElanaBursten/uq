angular.module("common.services.qmHttp", [])
    .factory("qmHttp", ["$http", function ($http) {
        return {
            baseUrl: "//" + window.location.host + "/api/v1",
            get: function (url, config, useBaseUrl) {
                var b = useBaseUrl || true,
                    c = jQuery.extend({},
                        {withCredentials: true, timeout: 15000}, config || {});
                return $http.get((b ? this.baseUrl : "") + url, c);
            },
            post: function (url, data, config, useBaseUrl) {
                var b = useBaseUrl || true,
                    d = data || {},
                    c = jQuery.extend({},
                        {withCredentials: true, timeout: 15000}, config || {});
                return $http.post((b ? this.baseUrl : "") + url, d, c);
            }
        };
    }]);