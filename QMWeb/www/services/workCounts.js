angular.module('services.workCounts', ['services.timeService', 'services.sync'])
    .factory('workCounts', function (syncService, timeService) {
        var obj = {},
            allFilter,
            emergencyFilter,
            newFilter,
            ongoingFilter,
            damageFilter,
            workOrderFilter;

        obj.ticketFilters = [];
        obj.workOrderFilters = [];
        obj.damageFilters = [];

        allFilter = {
            name: 'All Tickets',
            updateFunction: function () {
                var i, tickets = [], ans = [];
                for (i = 0; syncService.data.locate && i < syncService.data.locate.arr.length; i++) {
                    if (syncService.data.locate.arr[i].assigned_to === syncService.logged_in_user_emp_id) {
                        tickets.push(syncService.data.locate.arr[i].ticket_id);
                    }
                }

                tickets = _.uniq(tickets);
                for (i = 0; i < tickets.length; i++) {
                    ans.push(syncService.getTicket(tickets[i]));
                }
                allFilter.arr = ans;
            },
            arr: []
        };
        obj.ticketFilters.push(allFilter);

        emergencyFilter = {
            name: 'Emergency Tickets',
            updateFunction: function () {
                emergencyFilter.arr = _.filter(allFilter.arr, function (el) {
                    return el.kind === "EMERGENCY";
                });
            },
            arr: []
        };
        obj.ticketFilters.push(emergencyFilter);

        newFilter = {
            name: 'New Tickets',
            updateFunction: function () {
                newFilter.arr = _.filter(allFilter.arr, function (el) {
                    var diff = moment(el.transmit_date).diff(timeService.getMoment(), 'days', true);
                    return diff >= -1 && diff <= 0;
                });
            },
            arr: []
        };
        obj.ticketFilters.push(newFilter);

        ongoingFilter = {
            name: 'Ongoing Tickets',
            updateFunction: function () {
                ongoingFilter.arr = _.filter(allFilter.arr, function (el) {
                    return el.kind === "Ongoing";
                });
            },
            arr: []
        };
        obj.ticketFilters.push(ongoingFilter);

        damageFilter = {
            name: 'All Damages',
            updateFunction: function () {
                if (!syncService.data.damage) {
                    damageFilter.arr = [];
                    return;
                }
                damageFilter.arr = _.filter(syncService.data.damage.arr, function (el) {
                    return el.investigator_id === syncService.logged_in_user_emp_id ||
                            el.locator_id === syncService.logged_in_user_emp_id;
                });
            },
            arr: []
        };
        obj.damageFilters.push(damageFilter);

        workOrderFilter = {
            name: 'All Work Orders',
            updateFunction: function () {
                if (!syncService.data.work_order) {
                    workOrderFilter.arr = [];
                    return;
                }
                workOrderFilter.arr = _.filter(syncService.data.work_order.arr, function (el) {
                    return el.assigned_to_id === syncService.logged_in_user_emp_id;
                });
            },
            arr: []
        };
        obj.workOrderFilters.push(workOrderFilter);

        function update() {
            var i;
            for (i = 0; i < obj.ticketFilters.length; i++) {
                obj.ticketFilters[i].updateFunction();
            }
            for (i = 0; i < obj.workOrderFilters.length; i++) {
                obj.workOrderFilters[i].updateFunction();
            }
            for (i = 0; i < obj.damageFilters.length; i++) {
                obj.damageFilters[i].updateFunction();
            }
        }
        syncService.updateFunctions.push(update);

        obj.filterTicketsBy = function (filterName) {
            var i;
            for (i = 0; i < obj.ticketFilters.length; i++) {
                if (obj.ticketFilters[i].name === filterName) {
                    return obj.ticketFilters[i].arr;
                }
            }

            if (filterName === 'All') {
                return obj.ticketFilters[0].arr;
            }

            return [];
        };

        obj.filterDamagesBy = function (filterName) {
            var i;
            for (i = 0; i < obj.damageFilters.length; i++) {
                if (obj.damageFilters[i].name === filterName) {
                    return obj.damageFilters[i].arr;
                }
            }
            if (filterName === 'All') {
                return obj.damageFilters[0].arr;
            }
            return [];
        };

        obj.filterWorkOrdersBy = function (filterName) {
            var i;
            for (i = 0; i < obj.workOrderFilters.length; i++) {
                if (obj.workOrderFilters[i].name === filterName) {
                    return obj.workOrderFilters[i].arr;
                }
            }

            if (filterName === 'All') {
                return obj.workOrderFilters[0].arr;
            }

            return [];
        };

        obj.filterBy = function (filterName) {
            var i;
            for (i = 0; i < obj.ticketFilters.length; i++) {
                if (obj.ticketFilters[i].name === filterName) {
                    return obj.ticketFilters[i].arr;
                }
            }
            for (i = 0; i < obj.damageFilters.length; i++) {
                if (obj.damageFilters[i].name === filterName) {
                    return obj.damageFilters[i].arr;
                }
            }
            for (i = 0; i < obj.workOrderFilters.length; i++) {
                if (obj.workOrderFilters[i].name === filterName) {
                    return obj.workOrderFilters[i].arr;
                }
            }
            return [];
        };

        obj.availableFilters = _.union(['All'],
                _.pluck(_.union(obj.ticketFilters, obj.damageFilters, obj.workOrderFilters), 'name'));

        return obj;
    });