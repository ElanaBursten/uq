angular.module('services.compressionWorker', ['services.workers'])
    .factory('compressionWorker', function ($q, WorkerFactory, $rootScope) {
        var obj = {},
            n = 0,
            lzString = WorkerFactory.create('compress');

        function handleMessages(method, data) {
            var d = $q.defer(),
                id = "result" + n++;

            lzString.postMessage({
                method: method,
                id: id,
                payload: data
            });

            function listener(message) {
                if (message.data.id === id) {
                    $rootScope.$apply(function () {
                        d.resolve(message.data.payload);
                        lzString.removeEventListener("message", listener);
                    });
                }
            }
            lzString.addEventListener("message", listener);

            return d.promise;
        }

        obj.compress = function (data) {
            return handleMessages("compress", data);
        };

        obj.decompress = function (data) {
            return handleMessages("decompress", data);
        };

        return obj;
    });