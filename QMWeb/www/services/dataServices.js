angular.module("services.dataServices", ['services.sync'])
    .factory("EmployeeService", function (WorkService) {
        var obj = {
            visibleEmployees : [],
            selectedEmployee : null
        };
        obj.setSelectedEmployee = function (emp) {
            obj.selectedEmployee = emp;
            WorkService.clearList();
            WorkService.showTicketsForSubordinates = true;
        };
        return obj;
    })
    .service('favoritesDataService', function () {
        function FavoriteNode(emp) {
            this.node_id = emp.node_id;
            this.short_name = emp.short_name;
            this.image = emp.image;
        }
        var favoritesList = [],
            data = {
                getFavorites: function () {
                    return favoritesList;
                },
                addFavorite: function (employee) {
                    if (!_.find(favoritesList, function (el) {
                            return el.node_id === employee.node_id;
                        })) {
                        favoritesList.push(new FavoriteNode(employee));
                    }
                },
                removeFavorite: function (employee) {
                    favoritesList = _.reject(favoritesList, function (fav) {
                        return fav.node_id === employee.node_id;
                    });
                },
                isFavorite: function (employee) {
                    return (undefined !== _.find(favoritesList, function (fav) {
                        return fav && employee && fav.node_id === employee.node_id;
                    }));
                },
                clearList: function () {
                    favoritesList.length = 0;
                }
            };
        return data;
    })
    .factory("WorkService", function (qmHttp, $q) {
        var obj = {
            visibleWork : [],
            selectedWork : [],
            allWork: [],
            showTicketsForSubordinates: true,
            hasSubordinates: false,
            emergencyMode: false
        };
        obj.getWork = function (identifier, employNode) {
            return obj.getWorkHelper(identifier, employNode).then(function (work) {
                obj.allWork = work;
                obj.selectedWork.length = 0;
                obj.getVisible();
                return work;
            });
        };
        obj.getWorkHelper = function (identifier, employNode) {
            var subs;
            return qmHttp.get("/nodestat/" + identifier).then(function (response) {
                var data = response.data,
                    promises = [],
                    j;
                // Get promises for child work
                subs = data.child;
                if (obj.showTicketsForSubordinates && subs) {
                    for (j in subs) {
                        if (subs.hasOwnProperty(j) && subs[j].node_id) {
                            promises = promises.concat(obj.getWorkHelper(subs[j].node_id, employNode));
                        }
                    }
                } else if (identifier === employNode) {
                    obj.showTicketsForSubordinates = false;
                }
                // Get promise for parent work
                if (data.node) {
                    promises.push(qmHttp.get("/employee/" + data.node.node_id + "/work/")
                        .then(function (response) { return response.data.work; }));
                }
                return $q.all(promises);
            }).then(function (responses) {
                obj.hasSubordinates = subs ? true : false;
                var i, work = [];
                for (i = 0; i < responses.length; i++) {
                    work = work.concat(responses[i]);
                }
                return work;
            });
        };
        obj.toggleEmergency = function () {
            obj.emergencyMode = !obj.emergencyMode;
            obj.getVisible();
        };
        obj.getVisible = function () {
            var work;
            if (obj.emergencyMode) {
                work = _.filter(obj.allWork, function (item) {
                    return item.emergency;
                });
                obj.visibleWork = work;
            } else {
                obj.visibleWork = obj.allWork;
            }
        };
        obj.toggleSelected = function (selected) {
            if (!this.isSelected(selected)) {
                obj.selectedWork = obj.selectedWork.concat([selected]);
            } else {
                obj.selectedWork = _.reject(obj.selectedWork, function (work) {
                    return work.ticket_id === selected.ticket_id;
                });
            }
        };
        obj.isSelected = function (work) {
            return (undefined !== _.find(obj.selectedWork, function (selected) {
                return work.ticket_id === selected.ticket_id;
            }));
        };
        obj.clearList = function () {
            this.selectedWork.length = 0;
        };
        return obj;
    })
    .service("NotificationService", function () {
        var demoData = [
            {message: "Call center is not sending tickets", time: "1 min", warning: false, notificationId: 0},
            {message: "Fax server is offline", time: "7 min", warning: true, notificationId: 1},
            {message: "Disaster recovery site is online", time: "8 min", warning: false, notificationId: 2},
            {message: "Call center has 16 errors on tickets", time: "16 min", warning: false, notificationId: 3},
            {message: "Server down for maintenance", time: "36 min", warning: false, notificationId: 4},
            {message: "Office closed tomorrow", time: "1 hour", warning: false, notificationId: 5}
        ],
            outOfOrderList = [],
            max = 5,
            list;
        function getId() {
            if (outOfOrderList.length > 0) {
                var id = _.min(outOfOrderList);
                outOfOrderList = _.without(outOfOrderList, id);
                return id;
            }
            max++;
            return max;
        }
        function freeId(id) {
            outOfOrderList.push(id);
        }
        list = demoData;
        return {
            notificationList: list,
            addNotification: function (obj) {
                var notificationId = getId();
                obj.notificationId = notificationId;
                list.push(obj);
                return notificationId;
            },
            removeNotification: function (obj) {
                freeId(obj.notificationId);
                list = _.without(list, obj);
            }
        };
    })
    .service("WorkQueueService", function () {
        var demoData = [
            {title: "Call Centers above Projected Volume", percent: 80, queueId: 0},
            {title: "Locators on Target with Ticket Volume", percent: 47, queueId: 1},
            {title: "This Week's Damage Quota", percent: 32, queueId: 2},
            {title: "Forecasted Percent of Closed Tickets", percent: 63, queueId: 3},
            {title: "Work Center Efficiency", percent: 80, queueId: 4}
        ],
            outOfOrderList = [],
            max = 4,
            list;
        function getId() {
            if (outOfOrderList.length > 0) {
                var id = _.min(outOfOrderList);
                outOfOrderList = _.without(outOfOrderList, id);
                return id;
            }
            max++;
            return max;
        }
        function freeId(id) {
            outOfOrderList.push(id);
        }
        list = demoData;
        return {
            workItems: list,
            addWork: function (obj) {
                var queueId = getId();
                obj.queueId = queueId;
                list.push(obj);
                return queueId;
            },
            setPercentDone: function (queueId, percent) {
                var item;
                for (item = 0; item < demoData.length; item++) {
                    if (demoData[item].queueId === queueId) {
                        demoData[item].percent = percent;
                    }
                }
            },
            removeItem: function (obj) {
                freeId(obj.queueId);
                list = _.without(list, obj);
            }
        };
    })
    .service("MessageService", function () {
        var demoData = [
            {
                avatarLink: "../../img/kc.jpeg",
                header: {from: "Kyle Cordes", time: "6 min"},
                message: "Please get me that report ASAP!",
                messageId: 0
            },
            {
                avatarLink: "../../img/sp.jpeg",
                header: {from: "Sam Pepose", time: "56 min"},
                message: "Do not forget our meeting at 3pm.",
                messageId: 1
            },
            {
                avatarLink: "../../img/mm.jpeg",
                header: {from: "Michael McNeil", time: "3 hours"},
                message: "Bring your tablet to the conference!",
                messageId: 2
            },
            {
                avatarLink: "../../img/ps.jpeg",
                header: {from: "Paul Spears", time: "yesterday"},
                message: "Annual Damages Report attached",
                messageId: 3
            }
        ],
            outOfOrderList = [],
            max = 3,
            list;
        function getId() {
            if (outOfOrderList.length > 0) {
                var id = _.min(outOfOrderList);
                outOfOrderList = _.without(outOfOrderList, id);
                return id;
            }
            max++;
            return max;
        }
        function freeId(id) {
            outOfOrderList.push(id);
        }
        list = demoData;
        return {
            messageList: list,
            addMessage: function (obj) {
                var messageId = getId();
                obj.messageId = messageId;
                list.push(obj);
                return messageId;
            },
            removeMessage: function (obj) {
                freeId(obj.messageId);
                list = _.without(list, obj);
            }
        };
    })
    .service("WorkTypeService", function () {
        var map = {
            "Work Order": "workorder",
            "Ticket": "ticket",
            "Damage": "damage"
        };
        return {
            getType: function (work) {
                return map[work];
            }
        };
    })
    .service('Formatter', function () {
        var ticket = {};
        ticket.maxLength = 30;
        ticket.cutoff = function (raw) {
            if (raw && raw.length > ticket.maxLength) {
                var fixed = raw.slice(0, ticket.maxLength) + "...";
                return fixed;
            }
            return raw;
        };
        //other types of metadata to be formatted

        return {ticket: ticket};
    })
    .factory('ticketService', function (qmHttp, syncService) {
        return {
            getTicket: function (ticketId) {
                return qmHttp.get("/work/ticket/" + ticketId).then(
                    function (response) {
                        var t = response.data.ticket;
                        if (t) {
                            return t;
                        }
                        throw "Server returned undefined ticket";
                    }
                );
            }
        };
    });