angular.module('services.sync', ['common.services.qmHttp', 'services.compressionWorker'])
    .factory('syncService', function (qmHttp, compressionWorker, $q) {
        var obj = {},
            lastSyncDateRequestFormat = 'YYYY-MM-DDTHH:mm:ss.SSS';
        obj.data = {};
        obj.failedSyncs = 0;
        obj.syncCount = 0;
        obj.logged_in_user_emp_id = -1;
        obj.updateFunctions = [];

        function store() {
            obj.compressing = true;
            return compressionWorker.compress(obj.data)
                .then(function (compressedData) {
                    localStorage.setItem('sync', compressedData);
                    obj.compressing = false;
                });
        }

        obj.load = function () {
            obj.loading = true;
            return $q.when(localStorage.getItem('sync') || "")
                .then(compressionWorker.decompress)
                .then(function (retrievedData) {
                    if (retrievedData) {
                        obj.data = retrievedData;
                    }
                    obj.loading = false;
                })
                .then(obj.update);
        };

        obj.pkfn = function (tableName) {
            var tableSchema = _.findWhere(syncSchema.tables, {name: tableName}),
                keyField,
                f;
            if (!tableSchema || tableSchema.indexes.length === 0) {
                return null;
            }
            keyField = tableSchema.indexes[0].fields[0];

            f = function (line) {
                return line[keyField];
            };
            f.key = keyField;
            return f;
        };

        obj.objToArr = function (thing) {
            var ans = [], item;
            for (item in thing) {
                if (thing.hasOwnProperty(item)) {
                    ans.push(thing[item]);
                }
            }
            return ans;
        };

        obj.search = function (query) {
            return obj.updatePromise.then(function () {
                var table_data = obj.data.employee.arr,
                    result = [],
                    i;
                for (i = 0; i < table_data.length; i++) {
                    if (table_data[i].short_name &&
                            table_data[i].short_name.toUpperCase()
                            .indexOf(query.toUpperCase()) > -1) {
                        result.push(table_data[i]);
                    }
                }
                return result;
            });
        };

        obj.parse = function (raw, initial) {
            var table_data = raw.table_data,
                result = initial || {},
                i,
                table,
                keyFn,
                keyValue,
                rowUpdate;
            for (i = 0; i < table_data.length; i++) {
                rowUpdate = table_data[i];
                result[rowUpdate.tname] = result[rowUpdate.tname] || {};
                table = result[rowUpdate.tname];
                keyFn = obj.pkfn(rowUpdate.tname);
                if (keyFn) {
                    keyValue = keyFn(rowUpdate);
                    table[keyFn.key] = table[keyFn.key] || {};
                    table[keyFn.key][keyValue] = _.extend({}, table[keyFn.key][keyValue], rowUpdate);
                }
            }

            for (table in result) {
                if (result.hasOwnProperty(table) && obj.pkfn(table)) {
                    result[table].arr = obj.objToArr(result[table][obj.pkfn(table).key]);
                }
            }
            //TODO Come up with a less fragile way to get the sync date
            obj.lastSync = moment(table_data[0].SyncDateTime, 'YYYY-MM-DDTHH:mm:ss.SSS');

            return result;
        };

        function getTicketString() {
            var ans = '';
            if (obj.data && obj.data.ticket) {
                _.each(obj.data.ticket.arr, function (el) {
                    ans += el.ticket_id + ',';
                });
            }
            return ans.slice(0, ans.length - 1);
        }

        function getDamageString() {
            var ans = '';
            if (obj.data && obj.data.damage) {
                _.each(obj.data.damage.arr, function (el) {
                    ans += el.damage_id + ',';
                });
            }
            return ans.slice(0, ans.length - 1);
        }

        function getWorkOrderString() {
            var ans = '';
            if (obj.data && obj.data.work_order) {
                _.each(obj.data.work_order.arr, function (el) {
                    ans += el.wo_id + ',';
                });
            }
            return ans.slice(0, ans.length - 1);
        }

        obj.update = function () {
            var updatePromise,
                lastSyncString,
                ticketsString = getTicketString(),
                damageString = getDamageString(),
                workOrderString = getWorkOrderString();
            obj.syncing = true;
            if (obj.lastSync) {
                lastSyncString = obj.lastSync.format(lastSyncDateRequestFormat);
            } else {
                lastSyncString = '';
            }
            // TODO Add code to figure out the real value of this
            obj.logged_in_user_emp_id = 3512;

            updatePromise = qmHttp.get('/sync/' + obj.logged_in_user_emp_id +
                    '?last_sync=' + lastSyncString +
                    '&tickets=' + ticketsString +
                    '&damages=' + damageString +
                    '&wos=' + workOrderString).success(function (raw) {
                var i;
                obj.data = obj.parse(raw, obj.data);
                for (i = 0; i < obj.updateFunctions.length; i++) {
                    obj.updateFunctions[i]();
                }
                obj.syncCount++;
                obj.failedSyncs = 0;
                store();
            }).error(function (err) {
                obj.failedSyncs++;
            }).then(function (ignored) {
                return obj.data;
            }).finally(function () {
                obj.syncing = false;
            });
            obj.updatePromise = updatePromise;
            return updatePromise;
        };

        function getNotes(t) {
            var data = obj.data,
                notes = _.filter(data.notes.arr, function (el) {
                    return el.foreign_id === t.ticket_id && el.foreign_type === "1";
                }),
                i,
                emp_id,
                filter = function (el) {
                    return el.foreign_id === t.locate[i].locate_id && el.foreign_type === "5";
                };
            for (i = 0; i < t.locate.length; i++) {
                notes = notes.concat(_.filter(data.notes.arr, filter));
            }
            for (i = 0; i < notes.length; i++) {
                emp_id = data.users.uid[notes[i].uid].emp_id;
                notes[i].employee = data.employee.emp_id[emp_id];
            }

            return notes;
        }

        obj.getTicket = function (tid) {
            if (!obj.data.ticket) {
                return false;
            }
            var data = obj.data,
                t = data.ticket.ticket_id[tid],
                i,
                j,
                loc,
                lf;
            if (t) {
                if (data.locate) {
                    t.locate = _.filter(data.locate.arr, function (el) {
                        return el.ticket_id === t.ticket_id;
                    });
                }
                if (data.notes) {
                    t.note = getNotes(t);
                }
                t.attachment = [];
                if (data.attachment) {
                    for (i = 0; i < data.attachment.arr.length; i++) {
                        if (data.attachment.arr[i].foreign_id === t.ticket_id &&
                                data.attachment.arr[i].foreign_type === "1") {
                            t.attachment.push(data.attachment.arr[i]);
                        } else if (data.attachment.arr[i].foreign_type === "5" &&
                                _.findWhere(t.locate, {locate_id: data.attachment.arr[i].foreign_id})) {
                            t.attachment.push(data.attachment.arr[i]);
                        }
                    }
                }
                t.ticket_version = [];
                if (data.ticket_version) {
                    for (i = 0; i < data.ticket_version.arr.length; i++) {
                        if (data.ticket_version.arr[i].ticket_id === t.ticket_id) {
                            t.ticket_version.push(data.ticket_version.arr[i]);
                        }
                    }
                }
                if (data.locate_facility) {
                    for (i = 0; i < t.locate.length; i++) {
                        loc = t.locate[i];
                        loc.facility = [];
                        for (j = 0; j < data.locate_facility.arr.length; j++) {
                            lf = data.locate_facility.arr[j];
                            if (loc.locate_id === lf.locate_id) {
                                loc.facility.push(lf);
                            }
                        }
                    }
                }
                if (data.locate_facility) {
                    for (i = 0; i < t.locate.length; i++) {
                        loc = t.locate[i];
                        loc.facility = [];
                        for (j = 0; j < data.locate_facility.arr.length; j++) {
                            lf = data.locate_facility.arr[j];
                            if (loc.locate_id === lf.locate_id) {
                                loc.facility.push(lf);
                            }
                        }
                    }
                }
            }
            return t;
        };
        return obj;
    });