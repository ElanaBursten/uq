angular.module("services.footerData", ["common.services.qmHttp", "services.timeService"])
    .service('footerDataService', function (qmHttp, timeService) {
        var data = {};

        data.dataURLs = localStorage.getItem("dataURLs") === null ?
                [qmHttp.baseUrl + "/metric/total/EMP/2676/TCLEMP/MIN/" +
                    timeService.getMoment().format('YYYY-MM-DD')] :
                    JSON.parse(localStorage.getItem("dataURLs"));
        data.addTimeSeriesURL = function (url) {
            if (data.dataURLs.indexOf(url) < 0) {
                data.dataURLs.push(url);
                localStorage.setItem("dataURLs",
                    JSON.stringify(data.dataURLs));
            }
        };

        data.removeTimeSeriesURL = function () {
            data.dataURLs.pop();
            localStorage.setItem("dataURLs",
                JSON.stringify(data.dataURLs));
        };

        return data;
    });