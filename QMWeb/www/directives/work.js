angular.module("directives.work", [
    'services.dataServices', 'directives.map', 'directives.googleMap'
]
    )
    .directive("work", function () {
        return {
            templateUrl: '/directives/work.tmplt.html',
            controller: 'WorkCtrl'
        };
    })
    .controller("WorkCtrl",
        function ($scope, qmHttp, WorkService, EmployeeService) {
            $scope.work = WorkService.visibleWork;
            $scope.select = WorkService.toggleSelected;
            $scope.selectedEmployee = EmployeeService.selectedEmployee;
            $scope.hasSubordinates = WorkService.hasSubordinates;
            $scope.showTicketsForSubordinates = WorkService.showTicketsForSubordinates;
            $scope.$watch(
                function () {
                    return EmployeeService.selectedEmployee;
                },
                function (n, o) {
                    if (n === o) {
                        return;
                    }
                    $scope.selectedEmployee = n;
                    if ($scope.selectedEmployee) {
                        WorkService.getWork(n.node_id, $scope.selectedEmployee.node_id).then(function () {
                            $scope.hasSubordinates = WorkService.hasSubordinates;
                            $scope.showTicketsForSubordinates = WorkService.showTicketsForSubordinates;
                        });
                    }
                }
            );
            $scope.customFilter = function (data) {
                if ($scope.search !== undefined) {
                    var pos,
                        mssg,
                        des = $scope.search.des,
                        text = $scope.search.text;
                    if (text !== undefined) {
                        for (pos in data) {
                            if (data.hasOwnProperty(pos)) {
                                mssg = String(data[pos]).toLowerCase();
                                if (des === pos || des === undefined || des === '') {
                                    if (mssg.substring(0, text.length) === text.toLowerCase()) {
                                        return true;
                                    }
                                }

                            }
                        }
                        return false;
                    }
                    return true;
                }
                return true;
            };
            $scope.isSelected = WorkService.isSelected;
            $scope.toggleEmergency = WorkService.toggleEmergency;
            $scope.emergencyMode = WorkService.emergencyMode;
            // Work sorting
            $scope.dateDue = "details['Date Due']";
            $scope.workSort = "Acknowledged";
            $scope.sortDescending = false;
            $scope.setWorkSort = function (category, descend) {
                $scope.workSort = category;
                $scope.sortDescending = descend;
            };
            $scope.longId = function (ID) {
                return ID && ID.length > 13;
            };

            $scope.extraLongId = function (ID) {
                return ID && ID.length > 16;
            };
            // Ng-class
            $scope.typeClassMap = function (type) {
                var types = {
                    "Work Order" : "workOrder",
                    "Ticket" : "ticket",
                    "Damage" : "damage"
                };
                return types[type] || "Other";
            };
            $scope.urgencyClassMap = {
                "" : "urgencyLow",
                "!": "urgencyNormal",
                "! !" : "urgencyMedium",
                "! ! !" : "urgencyHigh"
            };
            $scope.$watch(
                function () {
                    return WorkService.visibleWork;
                },
                function (n, o) {
                    if (n !== o) {
                        $scope.work = n;
                    }
                },
                true
            );
            // Buttons
            $scope.showMode = 'tiles';                         // LIST - MAP buttons        (Tasks - Right)
            $scope.showTicketsGrouped = false;              // GROUP - TOGETHER buttons  (Tasks - Right)

            // Work sorting
            $scope.dateDue = "details['Date Due']";
            // Return boolean for ng-class
            $scope.isWorkOrder = function (type) { return (type === "Work Order"); };
            $scope.isTicket    = function (type) { return (type === "Ticket");     };
            $scope.isDamage    = function (type) { return (type === "Damage");     };
            $scope.isOther     = function (type) { return ((type !== "Work Order") &&
                    (type !== "Ticket") && (type !== "Damage")); };
            $scope.urgencyLow = function (urgency) { return (urgency === ""); };
            $scope.urgencyNormal = function (urgency) { return (urgency === "1"); };
            $scope.urgencyMedium = function (urgency) { return (urgency === "2"); };
            $scope.urgencyHigh = function (urgency) { return (urgency === "3"); };
            $scope.$watch('showTicketsForSubordinates', function (n, o) {
                if (n === o) {return; }
                WorkService.showTicketsForSubordinates = $scope.showTicketsForSubordinates;
                WorkService.getWork($scope.selectedEmployee.node_id, $scope.selectedEmployee.node_id)
                    .then(function () {
                        $scope.hasSubordinates = WorkService.hasSubordinates;
                        $scope.showTicketsForSubordinates = WorkService.showTicketsForSubordinates;
                    });
            });
        });
