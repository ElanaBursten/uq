"use strict";

angular.module("directives.map", [])
    .controller("MapCtrl", function ($scope, WorkService, EmployeeService, MAP_CREDENTIALS) {
        // Updates map view based on selected work
        $scope.updateMapView = function () {
            var locations = [], i;
            $scope.getPins();
            if (WorkService.selectedWork.length === 0 && WorkService.visibleWork.length !== 0) {
                for (i = 0; i < WorkService.visibleWork.length; i++) {
                    locations.push(new Microsoft.Maps.Location(
                        WorkService.visibleWork[i].work_lat,
                        WorkService.visibleWork[i].work_long
                    ));
                }
                $scope.map.setView({
                    bounds: new Microsoft.Maps.LocationRect.fromLocations(locations),
                    padding: 50
                });
            } else if (WorkService.selectedWork.length !== 0) {
                for (i = 0; i < WorkService.selectedWork.length; i++) {
                    locations.push(new Microsoft.Maps.Location(
                        WorkService.selectedWork[i].work_lat,
                        WorkService.selectedWork[i].work_long
                    ));
                }
                $scope.map.setView({
                    bounds: new Microsoft.Maps.LocationRect.fromLocations(locations),
                    padding: 50
                });
            } else {
                new Microsoft.Maps.GeoLocationProvider($scope.map).getCurrentPosition();
            }
        };

        $scope.getPins = function () {
            var work, i;
            for (i = 0; i < WorkService.visibleWork.length; i++) {
                work = WorkService.visibleWork[i];
                if (work.work_lat && work.work_long) {
                    $scope.createPin(work);
                }
            }
        };

        // Creates a pin and infobox for a given work and adds it to the map entities
        $scope.createPin = function (work) {
            var loc = new Microsoft.Maps.Location(work.work_lat, work.work_long),
                pin = new Microsoft.Maps.Pushpin(loc),
                pinInfobox = new Microsoft.Maps.Infobox(pin.getLocation(),
                    {
                        title: work.ticket_id,
                        description: work.company,
                        height: 75,
                        width: 200,
                        visible: WorkService.isSelected(work),
                        offset: new Microsoft.Maps.Point(0, 5),
                        showCloseButton: false
                    }
                    );

            Microsoft.Maps.Events.addHandler(pin, 'click', function (e) {
                this.setOptions({ visible: true });
                WorkService.toggleSelected(work);
                $scope.$apply();
            }.bind(pinInfobox));
            Microsoft.Maps.Events.addHandler(pinInfobox, 'click', function (e) {
                this.setOptions({ visible: false });
                WorkService.toggleSelected(work);
                $scope.$apply();
            }.bind(pinInfobox));

            $scope.map.entities.push(pin);
            $scope.map.entities.push(pinInfobox);
            return pin;
        };

        $scope.clearMapEntities = function () {
            $scope.map.entities.clear();
        };

        if (window.Microsoft !== undefined) {
            $scope.mapOptions = {
                credentials: MAP_CREDENTIALS,
                center: new Microsoft.Maps.Location(34.344025, -80.721211),
                mapTypeId: Microsoft.Maps.MapTypeId.road,
                zoom: 9,
                enableSearchLogo: false
            };

            $scope.$watch(function () { return WorkService.visibleWork; }, function () {
                $scope.clearMapEntities();
                $scope.updateMapView();
            }, true);
            $scope.$watch("emergencyMode", function () {
                $scope.clearMapEntities();
                $scope.updateMapView();
            }, true);
        }
    })
    .directive("workMap", function () {
        return {
            controller: "MapCtrl",
            link: function (scope, element) {
                if (window.Microsoft !== undefined) {
                    scope.map = new Microsoft.Maps.Map(element.get(0), scope.mapOptions);
                } else {
                    element.html("<p>Unable to connect to Bing, refresh this page to try again</p>");
                    element.css({"text-align": "center"});
                }
            }
        };
    });