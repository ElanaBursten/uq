angular.module("directives.googleMap", ["google-maps"])
    .controller("GoogleMapCtrl", function ($scope, WorkService) {
        $scope.map = {
            center: {
                latitude: 34.344025,
                longitude: -80.721211
            },
            zoom: 8,
            control: {}
        };

        $scope.models = {
            work: []
        };

        $scope.$watch(function () { return WorkService.visibleWork; }, function () {
            $scope.models.work = WorkService.visibleWork;
            angular.forEach($scope.models.work, function (item) {
                item.latitude = item.work_lat;
                item.longitude = item.work_long;
                item.showWindow = WorkService.isSelected(item);
                item.toggleSelected = function () {
                    WorkService.toggleSelected(this.model);
                    this.showWindow = WorkService.isSelected(this.model);
                    // Refreshes the map after the digest cycle ends. The map must be refreshed after toggling
                    // a marker because the work detail box appears below the map when one work item is
                    // selected, changing its size. However, the map needs to be refreshed AFTER the
                    // work detail box opens, which only happens after angular applies the selection changes.
                    $scope.$$postDigest(function () {
                        $scope.map.control.refresh();
                    });
                };
            });
        });
    })
    .directive("gMap", function () {
        return {
            template: function () {
                if (window.google && !angular.equals(window.google.maps.Size(), {})) {
                    return "<div ng-include='\"/directives/googleMap.tmplt.html\"'>";
                }
                return "Unable to connect to Google.";
            },
            controller: "GoogleMapCtrl"
        };
    });