'use strict';

angular.module('common.directives.template', [])
    .directive("qmUniform", function () {
        return {
            restrict: "A",
            replace: true,
            link: function (scope, el) {
                el.not('[data-no-uniform="true"],#uniform-is-ajax').uniform();
            }
        };
    });