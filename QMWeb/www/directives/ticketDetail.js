angular.module('directives.ticketDetail', ['services.dataServices'])
    .directive('ticketDetail', function () {
        return {
            templateUrl: '/directives/ticketDetail.tmplt.html',
            controller: 'TicketDetailCtrl'
        };
    })
    .controller('TicketDetailCtrl', function ($scope, Formatter) {
        $scope.locates = {isOpen: true};
        $scope.$watch(function () {
            return $scope.data;
        }, function (n) {
            var possibleExtras, extras, obj;
            possibleExtras = ['work_address_number', 'work_address_number_2', 'work_address_street'];
            extras = -1;
            for (obj = 0; obj < possibleExtras.length; obj++) {
                if ($scope.data && $scope.data.ticket && $scope.data.ticket[possibleExtras[obj]] &&
                        $scope.data.ticket[possibleExtras[obj]] !== '') {
                    extras++;
                }
            }
            $scope.registerExtraLine(extras);
        });

        $scope.metadata = [
            {display: 'Status', fields: ['status']},
            {display: 'Street', fields: ['work_address_number', 'work_address_number_2', 'work_address_street']},
            {display: 'City', fields: ['work_city']},
            {display: 'County', fields: ['work_county']},
            {display: 'Grid', fields: ['map_page']},
            {display: 'Work For', fields: ['company']},
            {display: 'Description', fields: ['work_description']},
            {display: 'Contact', fields: ['caller_contact']},
            {display: 'Phone', fields: ['caller_phone']},
            {display: 'Alt. Contact', fields: ['caller_altcontact']},
            {display: 'Alt. Phone', fields: ['caller_altphone']},
            {display: 'Do Not Mark', fields: ['do_not_mark_before']},
            {display: 'Area', fields: ['route_area_name']},
            //{display: 'Est. Start', fields: ['']},
            //{display: 'Contractor', fields: []},
            {display: 'Work Type', fields: ['work_type']}
        ];

        $scope.parse = function (fieldArray) {
            var fullDisplayField, x;
            fullDisplayField = "";
            for (x = 0; x < fieldArray.length; x++) {
                if ($scope.data && $scope.data.ticket && $scope.data.ticket[fieldArray[x]]) {
                    fullDisplayField += $scope.data.ticket[fieldArray[x]] + " ";
                }
            }
            return fullDisplayField;
        };

        $scope.tables = {
            'Locates': [
                {
                    column: 'Client',
                    field: 'client_code',
                    width: 60,
                    align: 'left'
                },
                {
                    column: 'Alr',
                    field: 'Alert2',
                    width: 20,
                    align: 'left'
                },
                {
                    column: 'Status',
                    field: 'status',
                    width: 39,
                    align: 'left'
                },
                {
                    column: 'Qty',
                    field: 'qty_marked',
                    width: 36,
                    align: 'right'
                },
                {
                    column: 'Locator',
                    field: 'short_name',
                    width: 97,
                    align: 'left'
                },
                {
                    column: 'Closed Date',
                    field: 'close_date',
                    width: 111,
                    align: 'left'
                }
            ]
            /*'Responses': [
                {
                    column: 'Client',
                    field: 'client_code',
                    width: 65,
                    align: 'left'
                },
                {
                    column: 'Status',
                    field: 'status',
                    width: 39,
                    align: 'left'
                },
                {
                    column: 'Response Date',
                    field: 'response_date',
                    width: 86,
                    align: 'left'
                },
                {
                    column: 'Sent',
                    field: 'response_sent',
                    width: 103,
                    align: 'left'
                },
                {
                    column: 'Reply',
                    field: 'reply',
                    width: 100,
                    align: 'left'
                }
            ],
            'Notes': [
                {
                    column: 'Date',
                    field: 'entry_date',
                    width: 91,
                    align: 'left'
                },
                {
                    column: 'Note',
                    field: 'Note',
                    width: 221,
                    align: 'left'
                }
            ],
            'Attachments': [
                //column: 'Attachment ID'
                {
                    column: 'File Name',
                    field: 'orig_filename',
                    width: 95,
                    type: 'str',
                    align: 'left'
                },
                //column: 'Size'
                {
                    column: 'Attach Date',
                    field: 'attach_date',
                    width: 99,
                    type: 'date',
                    align: 'left'
                },
                {
                    column: 'Upload Date',
                    field: 'upload_date',
                    width: 88,
                    type: 'date',
                    align: 'left'
                },
                {
                    column: 'Doc Type',
                    field: 'doc_type',
                    width: 50,
                    type: 'str',
                    align: 'left'
                },
                {
                    column: 'Comment',
                    field: 'comment',
                    width: 61,
                    type: 'str',
                    align: 'left'
                },
                {
                    column: 'Generated File Name',
                    field: 'filename',
                    width: 137,
                    type: 'str',
                    align: 'left'
                },
                //foreign_type
                //foreign_id
                {
                    column: 'Active',
                    field: 'active',
                    width: 41,
                    type: 'chk',
                    align: 'left'
                },
                {
                    column: 'Attached By',
                    field: 'short_name',
                    width: 115,
                    type: 'str',
                    align: 'left'
                }
                //Attached By ID
                //Source
                //Upload Machine Name
            ]*/
        };
        $scope.hasValue = function (field) {
            if ($scope.data && $scope.data.ticket && $scope.data.ticket[field] &&
                    $scope.data.ticket[field] !== "") {
                return true;
            }
            return false;
        };
    });