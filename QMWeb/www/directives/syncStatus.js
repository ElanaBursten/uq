angular.module('directives.syncStatus', ['services.sync'])
    .directive('syncStatus', function () {
        return {
            templateUrl: '/directives/syncStatus.tmplt.html',
            controller: 'syncStatusController'
        };
    })
    .controller('syncStatusController', function ($scope, syncService) {
        $scope.getStatus = function () {
            if (syncService.loading) {
                return 'Loading...';
            }
            if (syncService.compressing) {
                return 'Compressing...';
            }
            if (syncService.syncing) {
                return 'Syncing...';
            }
            if (syncService.lastSync) {
                return 'The last sync was ' + syncService.lastSync.fromNow();
            }
            return 'Not synced yet';
        };

        $scope.sync = function () {
            syncService.update();
        };
    });