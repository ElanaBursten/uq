angular.module("directives.employees", [
    'services.dataServices',
    'services.EmpTreeNavigation'
]
    )
    .directive("employees", function () {
        return {
            templateUrl: '/directives/employees.tmplt.html',
            controller: 'EmployeesCtrl'
        };
    })
    .controller("EmployeesCtrl",
        function ($scope, $routeParams, $location, qmHttp,
                EmployeeService, EmpTreeNavigationService, favoritesDataService) {
            $scope.selectEmployee = EmployeeService.setSelectedEmployee;
            $scope.addFavorite = favoritesDataService.addFavorite;
            $scope.removeFavorite = favoritesDataService.removeFavorite;
            $scope.isFavorite = favoritesDataService.isFavorite;
            $scope.employee = {};
            $scope.employee.goTo = function (nodeID) {
                $location.url('/manage/?e=' + nodeID);
            };
            $scope.isSelectedEmployee = function (emp) {
                return EmployeeService.selectedEmployee && emp &&
                    (emp.node_id ===
                        EmployeeService.selectedEmployee.node_id);
            };
            $scope.longName = function (emp) {
                return emp.short_name.length > 14;
            };
            $scope.extraLongName = function (emp) {
                return emp.short_name.length > 18;
            };
            var identifier = "";
            if ($routeParams.e) {
                identifier = $routeParams.e;
            }
            EmpTreeNavigationService.goToNode(identifier).then(
                function () {
                    $scope.node = EmpTreeNavigationService;
                    $scope.selectEmployee(EmpTreeNavigationService.rootNode);
                    $scope.breadcrumbs = $scope.node.breadcrumbs;
                }
            );
            $scope.data = favoritesDataService;
            $scope.goDownLevel = function (emp) {
                $location.url("/manage?e=" + emp.node_id);
            };
            $scope.goUpLevel = function (emp) {
                if (emp.ancestors) {
                    $location.url("/manage?e=" + emp.ancestors[emp.ancestors.length - 1].id);
                }
            };

            $scope.showContact = function (contact) {
                contact.showContactInfo = !contact.showContactInfo;
            };

            // Buttons
            // LIST - TREE buttons       (People - Left)
            $scope.showTree = false;
        });
