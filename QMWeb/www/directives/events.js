"use strict";

angular.module("common.directives.events", [])
    .directive("qmAutoFocus", function () {
        return {
            restrict: "A",
            link: function (scope, el, attrs) {
                el.focus();
            }
        };
    })
    .directive('qmFocus', ['$parse', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.qmFocus);
            element.bind('focus', function (event) {
                scope.$apply(function () {
                    fn(scope, {$event: event});
                });
            });
        };
    }])
    .directive('qmBlur', ['$parse', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.qmBlur);
            element.bind('blur', function (event) {
                scope.$apply(function () {
                    fn(scope, {$event: event});
                });
            });
        };
    }])
    .directive('contentFullHeight', ["$window", function ($window) {
        //TODO: Clean this up...
        return function (scope, el) {
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return { 'h': w.height(), 'w': w.width() };
            };
            scope.widthFunctions = function (h, w, el) {
                if (h) {
                    if ($("#body").height() > h) {
                        $("#content").css("min-height", $("#body").height());
                    } else {
                        $("#content").css("min-height", h - 43);
                    }
                }

                if (w <= 767) { // Phone mode
                    if (h) {
                        if ($("#body").height() > h) {
                            $("#content").css("min-height",
                                $("#body").height());

                        } else {
                            $("#content").css("min-height", h - 124);
                        }
                    }
                } else if (w < 980 && w > 767) { // Icon tablet mode
                    if ($(".main-menu-span").hasClass("span2")) {
                        $(".main-menu-span").removeClass("span2");
                        $(".main-menu-span").addClass("span1");
                    }

                    if (el.hasClass("span10")) {
                        el.removeClass("span10");
                        el.addClass("span11");
                    }
                } else {
                    if ($(".main-menu-span").hasClass("span1")) {
                        $(".main-menu-span").removeClass("span1");
                        $(".main-menu-span").addClass("span2");
                    }

                    if (el.hasClass("span11")) {
                        el.removeClass("span11");
                        el.addClass("span10");
                    }
                }
            };
            scope.$watch(scope.getWindowDimensions,
                function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        scope.widthFunctions(newValue.h, newValue.w, el);
                    }
                }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
            scope.widthFunctions(scope.getWindowDimensions().h,
                scope.getWindowDimensions().w, el);
        };
    }]);