angular.module("directives.graphs.timeSeriesBar", [
    'services.timeService',
    'services.GraphService'
]
    )
    .directive("timeSeriesBarGraph",
        function ($http, timeService, GraphService) {
            return {
                restrict: "A",
                scope: {
                    sourceUrls : "=",
                    graphTitle: "@",
                    syncData: "=",
                    syncFamily: "@",
                    graphOptions: "=",
                    graphClass: "@",
                    visibleDays: "@"
                },
                templateUrl: "/directives/graphs/graph.tmplt.html",
                link: function (scope, el, attrs) {
                    var TOTAL_VISIBLE_DAYS = scope.visibleDays || 30,
                        setTicks = function () {
                            var tickArray = [],
                                i,
                                moment,
                                increment = 1,
                                BAR_WIDTH = 8,
                                LABEL_WIDTH = 37,
                                width = el.width() - BAR_WIDTH;

                            while ((LABEL_WIDTH * (TOTAL_VISIBLE_DAYS / increment)) > width) {
                                increment++;
                            }

                            for (i = 0; i <= TOTAL_VISIBLE_DAYS; i += increment) {
                                moment = timeService.getMoment().add('days', -TOTAL_VISIBLE_DAYS + i);
                                if (!timeService.getMoment().add('days', -TOTAL_VISIBLE_DAYS + i - increment)
                                        .isSame(moment, 'month') || i === 0) {
                                    tickArray.push([i, moment.format('MMM D')]);
                                } else {
                                    tickArray.push([i, moment.format('D')]);
                                }
                            }
                            return tickArray;
                        },
                        ticks = setTicks(),
                        lastMonth = function () {
                            var current = timeService.getMoment().add('days', 0),
                                previous = timeService.getMoment().add('days', -1),
                                dayCounter = -2,
                                i;
                                //goes to correct date after first else

                            for (i = TOTAL_VISIBLE_DAYS; i > 0; i--) {
                                if (previous.format('M') !== current.format('M')) {
                                    return i;
                                }
                                current = previous;
                                previous = timeService.getMoment().add('days', dayCounter);
                                dayCounter--;
                            }
                            //no shade if all days are within the same month
                            return 0;
                        };

                    el.on("resize", function () {
                        if (scope.plot) {
                            scope.plot.opts.xaxis.ticks = setTicks();
                            scope.plot = GraphService.plotGraph(scope.graph, scope.displays, scope.plot.opts);
                        }
                    });

                    scope.graph = el.find(".graph");
                    scope.thisMonth = moment().format('MMM');
                    scope.lastMonth = moment().add('months', -1).format('MMM');
                    scope.datas = [];
                    scope.displays = [];
                    scope.numberOfSources = 0;
                    scope.getGraphOptions = function () {
                        var defaultOptions = {
                                series: { shadowSize: 1 },
                                bars: {
                                    show: true,
                                    barWidth: 0.5,
                                    align: "center"
                                },
                                yaxis: {
                                    min: 0,
                                    minTickSize: 1,
                                    tickDecimals: false
                                },
                                xaxis: {
                                    tickSize: 1,
                                    show: true,
                                    ticks: ticks
                                },
                                colors: ["#7890a8"], //gray-blue
                                grid: {    tickColor: "#dddddd",
                                    backgroundColor: "#FFFFFF",
                                    borderWidth: 0,
                                    hoverable: true,
                                    markings: [
                                        {
                                            color: "#e3e3e3",
                                            xaxis: { from: -0.5, to: lastMonth() }
                                        },
                                        {
                                            color: '#dddddd',
                                            lineWidth: 1,
                                            xaxis: { from: -1, to: -1 }
                                        },
                                        {
                                            color: '#dddddd',
                                            lineWidth: 1,
                                            xaxis: {from: TOTAL_VISIBLE_DAYS, to: TOTAL_VISIBLE_DAYS}
                                        }
                                    ]
                                    },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "Tickets: %y",
                                    shifts: {
                                        x: 10,
                                        y: 10
                                    },
                                    defaultTheme: false
                                }
                            };
                        return jQuery.extend({}, defaultOptions, scope.graphOptions);
                    };
                    scope.draw = function (urls) {
                        var onSuccess = function (data) {
                                var struct = GraphService.mapGraphData(data.Data);
                                scope.datas.push(struct);
                                scope.displays.push(struct.slice(data.Data));
                                scope.plot = GraphService.plotGraph(scope.graph, scope.displays,
                                    scope.getGraphOptions());
                            },
                            onError = function (data, status) {
                                scope.displays.pop();
                                scope.datas.pop();
                            };
                        _.each(urls, function (element) {
                            $http.get(element, {withCredentials: true})
                                .success(onSuccess)
                                .error(onError);
                        });
                    };

                    GraphService.setHeight(scope.graph, scope.graphClass);

                    scope.$watch("sourceUrls",
                        function (urls, old) {
                            if (!urls) {
                                return;
                            }
                            GraphService.onSourceUrlChange(urls, scope);
                        }, true
                        );

                    if (scope.syncData && scope.syncFamily) {
                        scope.$watch("plot", function (n) {
                            var maxY = GraphService.syncGraphs(n, scope);
                            _.each(scope.plots, function (element) {
                                element.opts.yaxis.max = maxY;
                                $.plot(element.el, element.data, element.opts);
                            });
                        });
                    }
                }
            };
        }
        );
