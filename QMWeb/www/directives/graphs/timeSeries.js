angular.module("directives.graphs.timeSeries", [
    'services.timeService',
    'services.GraphService'
]
    )
    .directive("timeSeriesGraph",
        function ($http, timeService, GraphService) {
            return {
                restrict: "A",
                scope: {
                    sourceUrls : "=",
                    graphTitle: "@",
                    syncData: "=",
                    syncFamily: "@",
                    graphOptions: "=",
                    graphClass: "@",
                    visibleHours: "@",
                    graphColor: "@"
                },
                templateUrl: "/directives/graphs/graph.tmplt.html",
                link: function (scope, el, attrs) {
                    var RENDER_TIMEOUT = 1000,
                        TOTAL_VISIBLE_HOURS = scope.visibleHours || 12,
                        TOTAL_VISIBLE_SECONDS = 180,
                        getTime = {
                            Hour : function () {
                                var cd = timeService.getMoment().toDate();
                                return cd.getHours();
                            },
                            Minute : function () {
                                var cd = timeService.getMoment().toDate();
                                return getTime.Hour() * 60 + cd.getMinutes();
                            },
                            Second : function () {
                                var cd = timeService.getMoment().toDate();
                                return getTime.Minute() * 60 + cd.getSeconds();
                            }
                        },
                        timeMetadata = {
                            "Hour" : {
                                // Range of data is from midnight until NOW
                                range : [0, getTime.Hour()],
                                per_hour : 1,
                                // We can't display more data then the present time, so max x-value is NOW
                                max: getTime.Hour(),
                                // We only want to display a max of TOTAL_VISIBLE_HOURS,
                                // so min is that # of hours in the past
                                min: getTime.Hour() - TOTAL_VISIBLE_HOURS
                            },
                            "Minute": {
                                range: [0, getTime.Minute()],
                                per_hour : 60,
                                max: (getTime.Hour() + 1) * 60,
                                min: (getTime.Hour() - (TOTAL_VISIBLE_HOURS - 1)) * 60
                            },
                            "Second" : {
                                // We only care about the start range (TOTAL VISIBLE SECS into the past)
                                // The end range does not matter, because this range is dynamic
                                range: [getTime.Second() - TOTAL_VISIBLE_SECONDS, null],
                                per_hour: 3600,
                                max: getTime.Second(),
                                min: getTime.Second() - TOTAL_VISIBLE_SECONDS
                            }
                        },
                        secToHHMMMeridian = function (sec) {
                            var hours   = Math.floor(sec / 3600),
                                minutes = Math.floor(
                                    (sec - (hours * 3600)) / 60
                                ),
                                meridian = " AM";

                            if (hours   < 10) {hours   = "0" + hours; }
                            if (minutes < 10) {minutes = "0" + minutes; }

                            if (hours === 12) {
                                meridian = " PM";
                            } else if (hours > 12) {
                                hours = (hours - 12);
                                meridian = " PM";
                            } else if (hours === 0) {
                                hours = 12;
                                meridian = " AM";
                            }
                            return hours + ':' + minutes + meridian;
                        },
                        secTicks = function () {
                            var obj = [],
                                i;
                            for (i = timeMetadata.Second.range[0];
                                        i < scope.secondsDataIndex +
                                        TOTAL_VISIBLE_SECONDS +
                                        timeMetadata.Second.range[0]; i += 60) {
                                obj.push([i, secToHHMMMeridian(i)]);
                            }
                            return obj;
                        },
                        ticks = function () {
                            if (!scope.gran) {
                                return {};
                            }
                            if (scope.gran === "Second") {
                                return secTicks();
                            }
                            var obj = [],
                                ptsPerHr = timeMetadata[scope.gran].per_hour,
                                i = 0,
                                num,
                                hour,
                                minute,
                                meridiem,
                                max = timeMetadata[scope.gran].max,
                                min = timeMetadata[scope.gran].min,
                                increment = 0,
                                BAR_WIDTH = 8,
                                LABEL_WIDTH = 34,
                                width = el.width() - BAR_WIDTH,
                                incrementTicks = [ptsPerHr * 4,
                                                  ptsPerHr * 3,
                                                  ptsPerHr * 2,
                                                  ptsPerHr,
                                                  ptsPerHr / 2,
                                                  ptsPerHr / 4];
                            incrementTicks.reverse();

                            while ((LABEL_WIDTH * ((max - min) / incrementTicks[increment])) > width &&
                                    increment < incrementTicks.length) {
                                increment++;
                            }

                            for (i = min; i < max; i += incrementTicks[increment]) {
                                num = i / ptsPerHr;

                                if (num >= 12) {
                                    num -= 12;
                                    meridiem = "PM";
                                } else if (num < 0) {
                                    num += 12;
                                    meridiem = "PM";
                                } else {
                                    meridiem = "AM";
                                }

                                if (num >= 1) {
                                    hour = Math.floor(num);
                                } else {
                                    hour = "12";
                                }

                                if (Math.floor(num) !== num) {
                                    minute = ":" + Math.round(60 * (num - Math.floor(num)));
                                    meridiem = "";
                                } else {
                                    minute = "";
                                }

                                obj.push([i, hour + minute + meridiem]);
                            }

                            return obj;
                        };

                    el.on("resize", function () {
                        if (scope.plot) {
                            scope.plot.opts.xaxis.ticks = ticks();
                            scope.plot = GraphService.plotGraph(scope.graph, scope.displays, scope.plot.opts);
                        }
                    });

                    // datas stores all sets that are currently loaded.
                    // All parts of these sets may not necessarily be visible.
                    scope.datas = [];
                    // displays stores all sets that are currently displayed on the graph
                    scope.displays = [];
                    // secondsDataIndex refers to the last index of the total seconds data shown
                    // this starts at TOTAL_VISIBLE_SECONDS because that's how much data we show to start
                    scope.secondsDataIndex = TOTAL_VISIBLE_SECONDS;
                    // timeout is the actual JS timeout handling the seconds graph movement
                    // we keep a reference so we can cancel it if needed
                    scope.timeout = null;
                    scope.gran = undefined;
                    scope.numberOfSources = 0;
                    scope.graph = el.find(".graph");

                    scope.reRenderSecondsGraph = function () {
                        // loop through all visible data sets,
                        // we may be displaying multiple seconds data sets at a time
                        _.each(scope.displays, function (element) {
                            element.shift();
                            element.push(element[timeMetadata.Second.range[0] + scope.secondsDataIndex]);
                        });
                        scope.secondsDataIndex++;
                        scope.plot = GraphService.plotGraph(scope.graph, scope.displays, scope.getGraphOptions());
                        clearTimeout(scope.timeout);
                        scope.timeout = setTimeout(scope.reRenderSecondsGraph, RENDER_TIMEOUT);
                    };

                    scope.getGraphOptions = function () {
                        var defaultOptions = {
                            series: { shadowSize: 1 },
                            lines: {
                                lineWidth: 1,
                                fill: true,
                                fillColor: {
                                    colors: [
                                        { opacity: 0.3 },
                                        { opacity: 0.3 }
                                    ]
                                }
                            },
                            yaxis: {
                                min: 0,
                                minTickSize: 1,
                                tickDecimals: false
                            },
                            xaxis: {
                                tickSize: 1,
                                show: true,
                                ticks: ticks()
                            },
                            //Flot has an inline dependency on jquery.colorhelpers.js, so we can use it on green too
                            colors: [$.color.parse(scope.graphColor || "#498d5b").scale('rgb', 1.25).toString()],
                            grid: {
                                tickColor: "#dddddd",
                                borderWidth: 0
                            }
                        };
                        return jQuery.extend({}, defaultOptions, scope.options);
                    };

                    scope.draw = function (urls) {
                        var onSuccess = function (data) {
                                scope.gran = data.Granularity;

                                var struct = GraphService.mapGraphData(data.Data),
                                    opts = scope.getGraphOptions();
                                scope.datas.push(struct);

                                if (scope.gran === "Second") {
                                    scope.displays.push(struct.slice(timeMetadata.Second.range[0],
                                        timeMetadata.Second.range[0] + TOTAL_VISIBLE_SECONDS
                                        ));
                                    clearTimeout(scope.timeout);
                                    scope.timeout = setTimeout(scope.reRenderSecondsGraph, RENDER_TIMEOUT);
                                } else {
                                    // We only care about the data within the visible range
                                    scope.displays.push(struct.slice(timeMetadata[scope.gran].range[0],
                                        timeMetadata[scope.gran].range[1]
                                        ));
                                }
                                opts.xaxis.min = timeMetadata[scope.gran].min;
                                opts.xaxis.max = timeMetadata[scope.gran].max;
                                scope.plot = GraphService.plotGraph(scope.graph, scope.displays, opts);
                            },
                            onError = function (data, status) {
                                scope.displays.pop();
                                scope.datas.pop();
                            };
                        _.each(urls, function (element) {
                            $http.get(element, {withCredentials: true})
                                .success(onSuccess)
                                .error(onError);
                        });
                    };

                    GraphService.setHeight(scope.graph, scope.graphClass);

                    scope.$watch("sourceUrls",
                        function (urls, old) {
                            if (!urls) {
                                return;
                            }
                            GraphService.onSourceUrlChange(urls, scope);
                        }, true
                        );

                    if (scope.syncData && scope.syncFamily) {
                        scope.$watch("plot", function (n) {
                            var maxY = GraphService.syncGraphs(n, scope);
                            _.each(scope.plots, function (element) {
                                element.opts.yaxis.max = maxY;
                                $.plot(element.el, element.data, element.opts);
                            });
                        });
                    }
                }
            };
        }
        );
