angular.module('directives.workDescription', [])
    .directive('workDescription', function () {
        return {
            scope: {
                ticket: '='
            },
            templateUrl: '/directives/workDescription.tmplt.html'
        };
    });