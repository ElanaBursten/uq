angular.module('directives.expandingText', ['services.dataServices'])
    .controller('eTextCtrl', function ($scope, Formatter) {
        var ticketFormatter = Formatter.ticket;
        $scope.isShort = true;
        $scope.$watch("full", function (n) {
            $scope.short = ticketFormatter.cutoff(n);
        });
        $scope.short = 'Hello...';
        $scope.full = 'Hello World!';
        $scope.toggle = function () {
            $scope.isShort = !$scope.isShort;
        };
    })
    .directive('etext', function () {
        return {
            scope: true,
            templateUrl: '/directives/etext.tmplt.html',
            controller: 'eTextCtrl',
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {
                        scope.$watch(function () {return iAttrs.etext; }, function (n) {
                            scope.full = n;
                        });
                        scope.full = iAttrs.etext;
                    }
                };
            }
        };
    });