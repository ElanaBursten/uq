angular.module('directives.ticketDropdown', ['services.sync', 'services.workCounts'])
    .directive('ticketDropdown', function () {
        return {
            restrict: 'A',
            templateUrl: '/directives/workCount.tmplt.html',
            controller: 'WorkCountCtrl',
            scope: {
                filter: '@',
                label: '=',
                icon: '='
            }
        };
    })
    .controller('WorkCountCtrl', function ($scope, syncService, $location, workCounts, $rootScope) {
        $scope.navigate = function () {
            if ($rootScope.options.demoMode) {
                $location.url('/mywork?f=' + $scope.filter);
            }
        };
        $scope.$watch(function () {return syncService.syncCount; }, function (o, n) {
            $scope.length = workCounts.filterBy($scope.filter).length;
        });
    });