angular.module('directives.workQueue', [])
    .directive('workqueue', function () {
        return {
            restrict: 'A',
            scope: true,
            controller: 'WorkQueueCtrl',
            templateUrl: '/directives/workQueue.tmplt.html'
        };
    })
    .controller('WorkQueueCtrl', function ($scope, WorkQueueService) {
        $scope.count = 0;
        $scope.workQueueList = [];
        function listChanged(newList) {
            $scope.count = newList.length;
            $scope.workQueueList = newList;
        }
        $scope.$watch(function () {return WorkQueueService.workItems; }, listChanged);
        $scope.viewAll = function () {
            return;
        };
    });