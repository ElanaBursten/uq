angular.module("directives.duePieChart", [])
    .directive("duePieChart", function () {
        return {
            scope: {
                due: "=dueData",
                title: "@",
                graphStyle: "@"
            },
            link: function (sc, el, attrs) {
                sc.graphData = {
                    label: {}
                };
                var colors = {
                    late: $.color.parse("#c80404").scale('rgb', 0.85).toString(), //red
                    soon: "#f0a818", //gold
                    today: "#498d5b", //green
                    tomorrow: "#7890a8", //gray-blue
                    nextday: "#304878", //medium-blue
                    later: "#181848" //dark-blue
                },
                    options = {
                        series: {
                            pie: {
                                innerRadius: 0.6,
                                show: true,
                                label: false
                            }
                        },
                        legend: {
                            show: true,
                            labelFormatter: function (label, obj) {
                                var index = label.lastIndexOf(" ");
                                return label.substring(0, index) + "</br>" +
                                    "<span style='color:" + obj.color + ";'>" + label.substring(index + 1) + "</span>";
                            },
                            noColumns: 1,
                            container: el.children('.legend')
                        },
                        grid: {
                            hoverable: true,
                            clickable: true
                        },
                        colors: [
                            colors.late,
                            colors.soon,
                            colors.today,
                            colors.tomorrow,
                            colors.nextday,
                            colors.later
                        ]
                    },
                    total,
                    graph = el.find(".graph"),
                    draw = function () {
                        var processSlice = function (tag, showAs) {
                                if (!sc.due) {
                                    return {};
                                }

                                var amount = sc.due[tag] ? sc.due[tag].N : 0,
                                    amountFormatted = numeral(amount).format('0,0'),
                                    labText = (showAs || tag.substring(2));
                                return {
                                    label: labText + ' ' + amountFormatted,
                                    labelText: labText,
                                    labelNum: amountFormatted,
                                    data: amount
                                };
                            },
                            i = 0,
                            sliceTypes = [
                                ["1-Late", null],
                                ["2-Soon", null],
                                ["3-Today", null],
                                ["4-Tomorrow", null],
                                ["5-NextDay", "Next Day"],
                                ["6-Later", null]
                            ],
                            slices = [];

                        for (i = 0; i < sliceTypes.length; i++) {
                            slices.push(processSlice(sliceTypes[i][0], sliceTypes[i][1]));
                        }

                        total = 0;
                        for (i = 0; i < slices.length; i++) {
                            total += slices[i].data;
                        }

                        sc.graphData.label.text = "Total";
                        sc.graphData.label.num = numeral(total).format('0,0');

                        options.series.pie.radius = 0.9;
                        if (graph && (graph.width() > 0 && graph.height() > 0)) {
                            sc.plot = $.plot(graph, slices, options);
                            sc.plot.draw();
                        }
                    };

                graph.bind("plothover", function (event, pos, obj) {
                    sc.$apply(function () {
                        var lastIndex;
                        if (obj) {
                            lastIndex = obj.series.label.lastIndexOf(" ");
                            sc.graphData.label.text = obj.series.label.slice(0, lastIndex);
                            sc.graphData.label.num = obj.series.label.slice(lastIndex);
                        } else {
                            sc.graphData.label.text = "Total";
                            sc.graphData.label.num = numeral(total).format('0,0');
                        }
                    });
                });

                sc.$watch(
                    function () {
                        return sc.due;
                    },
                    function (n) {
                        draw();
                    }
                );
                draw();
            },
            transclude: true,
            templateUrl: "/directives/duePieChart.tmplt.html",
            replace: true
        };
    });
