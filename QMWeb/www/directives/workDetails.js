angular.module("directives.workDetails", [
    'services.dataServices',
    'directives.expandingText'
]
    )
    .directive("workDetails", function () {
        return {
            templateUrl: '/directives/workDetails.tmplt.html',
            controller: 'WorkDetailsCtrl'
        };
    })
    .controller("WorkDetailsCtrl",
        function ($scope, WorkService, Formatter, WorkTypeService, qmHttp) {
            $scope.workType = '';
            $scope.WorkService = WorkService;
            var extraLines = 0,
                smallSizeOrig = 170,
                extraLineHeight = 10,
                getImportantDetails = function (details, items) {
                    var importantDetails = {},
                        i,
                        d;
                    for (i in items) {
                        if (items.hasOwnProperty(i)) {
                            for (d in details) {
                                if (details.hasOwnProperty(d)) {
                                    if (items[i] === d) {
                                        importantDetails[d] = details[d];
                                    }
                                }
                            }
                        }
                    }
                    return importantDetails;
                };
            $scope.paneHeight = function (inner) {
                if ($scope.expandedDetails) {
                    return {'height': '350px'};
                }
                var val = smallSizeOrig + (extraLines * extraLineHeight);
                if (inner) {
                    val += 5;
                }
                return {'height': val + 'px'};
            };
            $scope.registerExtraLine = function (val) {
                extraLines = val;
            };
            $scope.acknowledgeWork = function () {
                WorkService.selectedWork[0].Acknowledged = true;
            };
            $scope.canAcknowledge = function () {
                if (WorkService.selectedWork[0] &&
                        !WorkService.selectedWork[0].Acknowledged &&
                        WorkService.selectedWork[0].emergency) {
                    return true;
                }
                return false;
            };

            $scope.$watch(function () {return WorkService.selectedWork; },
                function (n) {
                    if (WorkService.selectedWork[0]) {
                        $scope.headerWorkType = WorkService.selectedWork[0].Type;
                        $scope.headerWorkId = WorkService.selectedWork[0].ticket_id;
                        $scope.workType = WorkTypeService.getType(WorkService.selectedWork[0].Type);
                    } else {
                        $scope.workType = '';
                        $scope.headerWorkType = "";
                        $scope.headerWorkId = "";
                    }
                    extraLines = 0;
                });
            $scope.expandedDetails = false;
            $scope.toggleDetailsSize = function () {
                $scope.expandedDetails = !$scope.expandedDetails;
            };

            $scope.getTicketMetadata = function (key) {
                return metadata.ticket[key];
            };

            $scope.$watch(function () {
                return WorkService.selectedWork;
            }, function (n) {
                $scope.selectedWork = n[0];
                if ($scope.selectedWork) {
                    //Note: to make work have needed fields, priority, caller, legal_due_date was copied from the
                    // tickets folder.  When work is selected, these values may change if the tickets change.
                    if ($scope.selectedWork.Type === "Ticket") {
                        qmHttp.get("/work/ticket/" + $scope.selectedWork.ticket_id).success(function (data) {
                            $scope.data = data;
                            var details = data.ticket,
                                importantItems = $scope.selectedWork.important;
                            $scope.selectedWork.ticket = details;
                            $scope.selectedWork.importantDetails =
                                getImportantDetails(details, importantItems);
                        });
                    }
                    if ($scope.selectedWork.Type === "Damage") {
                        //TODO add Damage handling here
                        return;
                    }
                    if ($scope.selectedWork.Type === "Work Order") {
                        //TODO add Work Order handling here
                        return;
                    }
                } else {
                    $scope.data = {};
                }
            });
        }
        );
