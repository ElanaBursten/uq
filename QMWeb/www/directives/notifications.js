angular.module('directives.notifications', [])
    .directive('notifications', function () {
        return {
            restrict: 'A',
            scope: true,
            templateUrl: "/directives/notifications.tmplt.html",
            controller: "NotificationsCtrl"
        };
    })
    .controller('NotificationsCtrl', function ($scope, NotificationService) {
        function listUpdated(newList) {
            var warning = 0,
                total = 0,
                item;
            for (item = 0; item < newList.length; item++) {
                if (newList[item].warning) {
                    warning++;
                }
                total++;
            }
            $scope.count = {warning: warning, total: total};
            $scope.itemList = newList;
        }
        $scope.$watch(function () {
            return NotificationService.notificationList;
        }, listUpdated);
        $scope.count = {
            warning: 0,
            total: 0
        };
        $scope.itemList = [];

        $scope.viewAll = function () {
            return;
        };
    });