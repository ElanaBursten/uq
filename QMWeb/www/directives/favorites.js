angular.module("directives.favorites", ["services.dataServices"])
    .directive("favorites", function () {
        return {
            templateUrl: '/directives/favorites.tmplt.html',
            controller: 'FavoritesCtrl'
        };
    })
    .controller("FavoritesCtrl", function ($scope, favoritesDataService, EmployeeService) {
        $scope.favorites = {};
        $scope.favorites.expanded = false;
        $scope.favorites.selectFavorite = EmployeeService.setSelectedEmployee;
        $scope.favorites.isFavoriteSelected = function (emp) {
            return (EmployeeService.selectedEmployee &&
                (emp.node_id ===
                    EmployeeService.selectedEmployee.node_id));
        };
        $scope.favorites.clearFavorites = favoritesDataService.clearList;
        $scope.favorites.getFavorites = favoritesDataService.getFavorites;
    });