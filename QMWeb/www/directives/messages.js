angular.module('directives.messages', [])
    .directive('messages', function () {
        return {
            restrict: 'A',
            templateUrl: '/directives/messages.tmplt.html',
            controller: 'MessageCtrl'
        };
    })
    .controller('MessageCtrl', function ($scope, MessageService) {
        function listUpdated(newList) {
            var total = 0,
                item;
            for (item = 0; item < newList.length; item++) {
                total++;
            }
            $scope.count = total;
            $scope.itemList = newList;
        }
        $scope.$watch(function () {
            return MessageService.messageList;
        }, listUpdated);
        $scope.count = 0;
        $scope.itemList = [];
        $scope.viewAll = function () {
            return;
        };
    });