'use strict';

angular.module('app', [
    "config",
    'screens',
    'common.services.authentication',
    "common.services.qmHttp",
    "common.services.errorHandler",
    "directives.notifications",
    "directives.workQueue",
    "directives.messages",
    "ngRoute",
    "templates",
    "app.sidebar",
    "directives.syncStatus",
    "services.sync",
    "directives.ticketDropdown"
]
    )
    .config(
        function ($routeProvider, $locationProvider) {
            $locationProvider.html5Mode(true);
            $routeProvider.when('/', { redirectTo: "/managedash" });
        }
    )
    .directive("logoutButton", function (qmHttp, $location) {
        return {
            link: function (scope, el) {
                el.click(function () {
                    qmHttp.post("/logout", {})
                        .success(function () {
                            $location.hash('');
                            window.location = "/login";
                        });
                });
            }
        };
    })
    .controller("MainCtrl",
        function ($scope, $rootScope, pingService, syncService) {
            syncService.load();

            $rootScope.header = {
                title: "",
                isCollapsed: true
            };

            $scope.toggleSidebar = function () {
                $scope.$broadcast("toggleSidebar");
            };
            $scope.closeTabletSidebar = function () {
                $scope.$broadcast("closeTabletSidebar");
            };
            $scope.fullBodyWidth = true;
            pingService.init();
        })
    .factory('$exceptionHandler',
        function ($injector, errorHandler, $log) {
            return function (e, cause) {
                var $location = $injector.get("$location");
                errorHandler.addAngularError(e.name, e.message,
                    e.stack, $location.path(), new Date());
                $log.error.apply($log, arguments);
            };
        }
        );
