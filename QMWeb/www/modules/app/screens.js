angular.module('screens',
    [
        'mystats',
        'settings',
        'filler',
        'overview',
        'manage',
        'ticketDetail',
        'myWork',
        'options'
    ]
    );