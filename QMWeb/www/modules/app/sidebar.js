angular.module('app.sidebar', [])
    .directive('sidebarElement', function () {
        return {
            replace: true,
            templateUrl: "/modules/app/sidebarElement.tmplt.html",
            scope: {
                href: "@",
                title: "@",
                icon: "@"
            },
            controller: ["$scope", "$location", "$window", function ($scope, $location, $window) {
                $scope.routeMatch = function () {
                    return $location.path() === $scope.href;
                };
                $scope.go = function () {
                    if (angular.element($window).width() < 980) { //tablet mode
                        $scope.$emit("toggleSidebar");
                    }
                    $location.path($scope.href);
                };
            }]
        };
    })
    .directive('sidebar', function () {
        return {
            replace: true,
            templateUrl: "/modules/app/sidebar.tmplt.html",
            link: function (scope, el) {
                //TODO: SFP - Clean this up...at least it works...

                var processSidebar, makeSnapper, toggleSidebar, closeTabletSidebar;
                makeSnapper = function () {
                    var type = String + scope.tabletSidebar;
                    if (scope.snapperType !== type) {
                        scope.snapperType = type;
                        scope.snapper = new Snap({
                            element: angular.element(".main-content")[0],
                            maxPosition: scope.tabletSidebar ? 0 : 200,
                            touchToDrag: false,
                            tapToClose: false
                        });
                        if (scope.open) {
                            scope.snapper.open('left');
                        } else {
                            scope.snapper.close();
                        }
                    }
                };

                processSidebar = function () {
                    scope.tabletSidebar = angular.element(window).width() < 992;
                    makeSnapper();
                    if (scope.tabletSidebar) {
                        scope.fullBodyWidth = true;
                        angular.element(el).width(70);
                    } else {
                        scope.fullBodyWidth = !scope.open;
                    }
                    if (!scope.$$phase) {
                        scope.$apply();
                    }
                };

                toggleSidebar = function () {
                    if (scope.open) {
                        scope.snapper.close();
                        scope.open = false;
                    } else {
                        scope.snapper.open("left");
                        scope.open = true;
                    }
                    processSidebar();
                };

                closeTabletSidebar = function () {
                    if (scope.open && scope.tabletSidebar) {
                        scope.open = false;
                        scope.snapper.close();
                    }
                };

                scope.open = false;
                processSidebar();

                scope.$on("toggleSidebar", toggleSidebar);
                scope.$on("closeTabletSidebar", closeTabletSidebar);

                angular.element(window).on("resize", function () {
                    processSidebar();
                    scope.$apply();
                });
            }
        };
    });
