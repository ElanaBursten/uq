window.onload = function () {
    setTimeout(function () {
        var overlay = document.getElementById("overlay"),
            error;
        // If el exists and is visible
        if (overlay && overlay.offsetWidth > 0 && overlay.offsetHeight > 0) {
            overlay.parentNode.removeChild(overlay);
            error = document.getElementById("fatal-error");
            error.style.display = "block";
        }
    }, 15000);
};