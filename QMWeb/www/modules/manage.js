'use strict';

angular.module('manage', [
    'common.directives.events',
    'directives.graphs.timeSeries',
    'services.footerData',
    "common.services.qmHttp",
    'ui.bootstrap',
    'directives.favorites',
    'directives.employees',
    'directives.work',
    'directives.workDetails',
    'directives.ticketDetail',
    'ngRoute'
]
    )
    .config(function ($routeProvider) {
        $routeProvider.when('/manage', {
            templateUrl: '/modules/manage.tmplt.html',
            controller: 'ManageWorkCtrl'
        });
    })
    .controller('ManageWorkCtrl',
        function ($scope, qmHttp, footerDataService) {
            $scope.header.title = "Manage Work";

            $scope.urls = footerDataService.dataURLs;
            $scope.$watch(
                function () {return footerDataService.dataURLs; },
                function (n, o) {
                    if (n === o) {
                        return;
                    }
                    $scope.urls = n;
                },
                true
            );
        });