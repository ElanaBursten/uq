
'use strict';

angular.module('mystats', [
    'common.directives.events',
    'directives.graphs.timeSeries',
    "common.services.qmHttp",
    "services.syncGraphs",
    "services.timeService",
    "ngRoute"
]
    )
    .config(function ($routeProvider) {
        $routeProvider.when('/mystats', {
            templateUrl: '/modules/myStats/myStats.tmplt.html',
            controller: 'MyStatsCtrl'
        });
    })
    .controller('MyStatsCtrl',
        function ($scope, qmHttp, syncGraphs, timeService) {
            $scope.header.title = "My Stats";
            var currDate = timeService.getMoment().format('YYYY-MM-DD');
            $scope.graphs = [
                {
                    label: "STS/UQ Atlanta Total Tickets Closed",
                    url: qmHttp.baseUrl +
                        '/metric/total/EMP/2676/TCLEMP/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                },
                {
                    label: "OCC2 VUPS (FXL) Tickets Closed",
                    url: qmHttp.baseUrl +
                        '/metric/single/CC/OCC2/TCLOSE/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                },
                {
                    label: "3003 Utilities Protection Center (Eden) Tickets Closed",
                    url: qmHttp.baseUrl +
                        '/metric/single/CC/3003/TCLOSE/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                },
                {
                    label: "New Jersey Tickets Closed",
                    url: qmHttp.baseUrl +
                        '/metric/single/CC/NewJersey/TCLOSE/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                },
                {
                    label: "Tennessee One Call FNV1 Tickets Received",
                    url: qmHttp.baseUrl +
                        '/metric/single/CC/FNV1/TREC/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                },
                {
                    label: "Tennessee One Call FNV1 Tickets Closed",
                    url: qmHttp.baseUrl +
                        '/metric/single/CC/FNV1/TCLOSE/MIN/' + currDate,
                    graphOptions: {yaxis: {max: 5}}
                }
            ];
            $scope.plotFamilies = syncGraphs.plotFamilies;
        }
        );
