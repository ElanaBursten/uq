angular.module('ticketDetail', [
    'ngRoute',
    'services.dataServices',
    'directives.workDescription'
]
     )
     .config(function ($routeProvider) {
        $routeProvider.when('/ticketdetail', {
            templateUrl: '/modules/ticketDetail.tmplt.html',
            controller: 'ticketDetailCtrl',
            reloadOnSearch: false
        });
    })
    .controller('ticketDetailCtrl', function ($scope, $location, $routeParams,
                                               ticketService, syncService, $anchorScroll) {
        $scope.header.title = "Ticket Details";
        $scope.ticketStatus = "Loading Ticket...";
        if ($routeParams.t) {
            $scope.ticketId = $routeParams.t;
        } else {
            throw "No ticket ID";
        }

        var ticket = syncService.getTicket($scope.ticketId);
        if (ticket) {
            $scope.ticket = ticket;
            $scope.ticketStatus = "";
        } else {
            ticketService.getTicket($scope.ticketId).then(
                function (ticket) {
                    $scope.ticketStatus = "";
                    $scope.ticket = ticket;
                },
                function () {
                    $scope.ticketStatus = 'Ticket not found';
                }
            );
        }

//TODO: Reroute page if user syncs in background

        $scope.arrived = false;

        $scope.arrive = function () {
            $scope.arrived = true;
        };

        $scope.scroll = function (sectionId) {
            $location.hash(sectionId);
            $anchorScroll();
        };
    });