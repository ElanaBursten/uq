'use strict';

angular.module('options', ['ngRoute', 'version'])
    .config(function ($routeProvider) {
        $routeProvider.when('/options', {
            templateUrl: '/modules/options.tmplt.html',
            controller: 'OptionsCtrl'
        });
    })
    .controller('OptionsCtrl',
        function ($scope, VersionService) {
            $scope.header.title = "Options";
            $scope.version = VersionService.version;
        });
