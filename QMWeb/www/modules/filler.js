'use strict';

angular.module('filler', [
    'directives.graphs.timeSeries',
    'ngRoute'
]
    )
    .config(function ($routeProvider) {
        $routeProvider.when('/messages', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/findwo', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/newticket', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/oldticket', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/findticket', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/damage', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/reports', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/approval', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/assets', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/billing', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/allocation', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
        $routeProvider.when('/areaeditor', {
            templateUrl: '/modules/filler.tmplt.html',
            controller: 'FillerCtrl'
        });
    })
    .controller('FillerCtrl', function ($scope, footerDataService) {
        $scope.message = "This is a placeholder for a feature we have in mind.";
        $scope.header.title = "Placeholder";
        $scope.urls = footerDataService.dataURLs;
    });