'use strict';

angular.module('overview', [
    'common.directives.events',
    'directives.graphs.timeSeries',
    'directives.graphs.timeSeriesBar',
    'services.footerData',
    'services.syncGraphs',
    "directives.duePieChart",
    "common.services.qmHttp",
    'services.EmpTreeNavigation',
    'ngRoute'
]
    )
    .config(function ($routeProvider) {
        $routeProvider.when('/managedash', {
            templateUrl: '/modules/overview.tmplt.html',
            controller: 'OverviewCtrl'
        });
    })
    .controller('OverviewCtrl',
        function ($scope, $routeParams, $location, qmHttp, footerDataService, syncGraphs, EmpTreeNavigationService) {
            $scope.header.title = "Overview Dashboard";

            $scope.goTo = function (type, node) {
                $location.url('/managedash/?' + type + '=' + node);
            };

            $scope.plotFamilies = syncGraphs.plotFamilies;

            // graph along the bottom
            $scope.urls = footerDataService.dataURLs;
            $scope.$watch(function () {
                return footerDataService.dataURLs;
            }, function (n, o) {
                if (n === o) {
                    return;
                }
                $scope.urls = n;
            }, true);

            $scope.delayDrillDown = function (nodeId, elem) {
                // TODO: Animation here
                $scope.goTo('e', nodeId);
            };

            var set = function (data) {
                $scope.data = EmpTreeNavigationService;
            };

            if ($routeParams.e) {
                EmpTreeNavigationService.goToNode($routeParams.e).then(set);
            } else if ($routeParams.q) {
                EmpTreeNavigationService.search($routeParams.q).then(set);
            } else {
                EmpTreeNavigationService.goToNode('').then(set);
            }

        }
        );
