function uniqueValues(arr, fields, pred) {
    var cats = [],
        key,
        x,
        str = "",
        i,
        j,
        sortFieldName,
        cmp;

    for (i = 0; i < arr.length; i++) {
        if (pred) {
            if (!pred(arr[i])) {
                continue;
            }
        }

        key = [];
        for (j = 0; j < fields.length; j++) {
            key.push(arr[i][fields[j]]);
        }
        x = key.join('|');
        if (cats.indexOf(x) < 0) {
            cats.push(x);
        }
    }

    if (fields.length > 1) {
        for (i = 0; i < cats.length; i++) {
            str = cats[i].split("|");
            cats[i] = {};
            for (j = 0; j < fields.length; j++) {
                cats[i][fields[j]] = str[j];
            }
        }

        sortFieldName = fields[fields.length - 1];
        cmp = function (a, b) {
            var ka = a[sortFieldName].toLowerCase(),
                kb = b[sortFieldName].toLowerCase();
            if (ka < kb) {
                return -1;
            }
            if (ka > kb) {
                return 1;
            }
            return 0;
        };
        cats.sort(cmp);
    } else {
        // They are strings, trivial sort.
        cats.sort();
    }
    return cats;
}

var searchObject = function (data, key, val) {
    var foundIt = false,
        i = 0,
        len;
    for (i = 0, len = data.length; i < len && !foundIt; i++) {
        if (data[i][key] === val) {
            foundIt = true;
        }
    }
    return foundIt;
};

var g; // hackery for easy REPL development

angular.module('settings', [
    'services.footerData',
    "common.services.qmHttp",
    "ngRoute",
    "services.timeService"
]
    )
    .config(function ($routeProvider) {
        $routeProvider.when('/settings', {
            templateUrl: '/modules/settings/settings.tmplt.html',
            controller: 'SettingsCtrl'
        });
    })
    .controller('SettingsCtrl', [
        "$scope",
        "qmHttp",
        "footerDataService",
        function ($scope, qmHttp, footerDataService, timeService) {
            $scope.timeSeries = {
                selected: {
                },
                urls: footerDataService.dataURLs
            };

            $scope.init = function () {
                qmHttp.get("/metric")
                    .success(function (data) {
                        // divvy it up here, not "on the fly"
                        $scope.timeSeries.combinations = data;
                        g = data;
                        $scope.timeSeries.entity_categories =
                            uniqueValues(data, ["ent_category"]);
                    });
            };

            var isValid = function (v) {
                //return false;
                return v !== null && v !== undefined &&
                    v.replace(/ /g, "") !== "";
            },
                getError = function () {
                    var x = $scope.timeSeries.selected,
                        y = "Problem with ";
                    if (!isValid(x.ent_cat)) {
                        return y + "Entity Category";
                    }
                    if (!isValid(x.ent_code)) {
                        return y + "Entity";
                    }
                    if (!isValid(x.metric_code)) {
                        return y + "Metric";
                    }
                    if (!isValid(x.gran_code)) {
                        return y + "Granularity";
                    }
                    return true;
                };

            $scope.addGraphURL = function () {
                var timeSeries = $scope.timeSeries.selected,
                    error = getError(),
                    date = "";
                if (!angular.isString(error)) {
                    $scope.timeSeries.error = "";
                    date = timeService.getMoment().format('YYYY-MM-DD');
                    footerDataService.addTimeSeriesURL(
                        qmHttp.baseUrl + "/metric/single/" + timeSeries.ent_cat +
                            "/" + timeSeries.ent_code + "/" + timeSeries.metric_code +
                            "/" + timeSeries.gran_code + "/" + date
                    );
                } else {
                    $scope.timeSeries.error = error;
                }
            };

            $scope.removeGraphURL = function () {
                footerDataService.removeTimeSeriesURL();
            };

            $scope.$watch("timeSeries.selected.ent_cat", function (n, o) {
                if (n === o) {
                    return;
                }
                $scope.timeSeries.entities = uniqueValues(g,
                        ["ent_code", "ent_name"], function (v) {
                        return v.ent_category ===
                            $scope.timeSeries.selected.ent_cat;
                    }
                    );

                if ($scope.timeSeries.entities &&
                        !searchObject($scope.timeSeries.entities, "ent_code",
                            $scope.timeSeries.selected.ent_code)) {
                    $scope.timeSeries.selected.metric_code = undefined;
                }
                if ($scope.timeSeries.metrics &&
                        !searchObject($scope.timeSeries.metrics, "metric_code",
                            $scope.timeSeries.selected.metric_code)) {
                    $scope.timeSeries.selected.metric_code = undefined;
                }
                if ($scope.timeSeries.granularity &&
                        !searchObject($scope.timeSeries.granularity,
                            "gran_code",
                            $scope.timeSeries.selected.gran_code)) {
                    $scope.timeSeries.selected.gran_code = undefined;
                }
            }, true);

            $scope.$watch("timeSeries.selected.ent_code", function (n, o) {
                if (n === o) {
                    return;
                }
                $scope.timeSeries.metrics = uniqueValues(g,
                    ["metric_code", "metric_name"], function (v) {
                        return v.ent_category ===
                                $scope.timeSeries.selected.ent_cat &&
                            v.ent_code ===
                                $scope.timeSeries.selected.ent_code;
                    });

                if ($scope.timeSeries.metrics &&
                        !searchObject($scope.timeSeries.metrics, "metric_code",
                            $scope.timeSeries.selected.metric_code)) {
                    $scope.timeSeries.selected.metric_code = undefined;
                }
                if ($scope.timeSeries.granularity &&
                        !searchObject($scope.timeSeries.granularity,
                            "gran_code",
                            $scope.timeSeries.selected.gran_code)) {
                    $scope.timeSeries.selected.gran_code = undefined;
                }
            }, true);

            $scope.$watch("timeSeries.selected.metric_code", function (n, o) {
                if (n === o) {
                    return;
                }
                $scope.timeSeries.granularity = uniqueValues(g,
                    ["gran_code", "gran_name"],
                    function (v) {
                        return v.ent_category ===
                                $scope.timeSeries.selected.ent_cat &&
                            v.ent_code ===
                                $scope.timeSeries.selected.ent_code &&
                            v.metric_code ===
                                $scope.timeSeries.selected.metric_code;
                    });

                if ($scope.timeSeries.granularity &&
                        !searchObject($scope.timeSeries.granularity,
                            "gran_code",
                            $scope.timeSeries.selected.gran_code)) {
                    $scope.timeSeries.selected.gran_code = undefined;
                }
            }, true);

            $scope.$watch(function () {
                return footerDataService.dataURLs;
            }, function (n, o) {
                if (n === o) {
                    return;
                }
                $scope.timeSeries.urls = n;
            }, true);
        }
    ]);

