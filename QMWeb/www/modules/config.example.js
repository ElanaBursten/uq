angular.module("config", [])
    .run(function (qmHttp, timeService, $rootScope) {
        qmHttp.baseUrl = "//" + window.location.host + "/api/v1";
        timeService.getMoment = function () {
            // Use the below line to show a different time, you can chain the
            // add calls together to modify the time in depth
            //return moment().add('hours', -10).add('days', -10);
            return moment(); //Use this line to show the current time
        };
        $rootScope.options = {
            demoMode : false,
            demoEnabled : false
        };
    })
    .constant("MAP_CREDENTIALS", "Agjzc3l2N6CNJ-Zhrc40ASa7XU3NjXme8yq6FKzMnV5fWEo08Stuj0acSEQ1ACGi");