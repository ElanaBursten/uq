angular.module('myWork', ['ngRoute', 'services.sync', 'directives.workDescription', 'services.workCounts'])
    .config(function ($routeProvider) {
        $routeProvider.when('/mywork', {
            templateUrl: '/modules/myWork.tmplt.html',
            controller: 'MyWorkCtrl'
        });
    })
    .controller('MyWorkCtrl', function ($scope, qmHttp, $location, workCounts, $routeParams, syncService) {
        $scope.header.title = "My Work";
        $scope.filters = workCounts.availableFilters;

        $scope.$watch('$routeParams.f', function () {
            if (_.contains($scope.filters, $routeParams.f)) {
                $scope.filter = $routeParams.f;
            } else {
                $scope.filter = $scope.filters[0];
            }
        });

        $scope.$watch('filter', function () {
            $location.url('/mywork?f=' + $scope.filter);
        });

        $scope.viewDetails = function (ticket) {
            $location.url('/ticketdetail?t=' + ticket);
        };

        $scope.$watch(
            function () {
                if (syncService.data.ticket) {
                    return syncService.syncCount;
                }
                return 0;
            },
            function (n, o) {
                $scope.tickets = workCounts.filterTicketsBy($scope.filter);
                $scope.damages = workCounts.filterDamagesBy($scope.filter);
                $scope.workOrders = workCounts.filterWorkOrdersBy($scope.filter);
            }
        );
    });