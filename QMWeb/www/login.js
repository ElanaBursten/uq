$(function () {
    $("#username").focus(function () {
        $(this).parent(".input-prepend").addClass("input-prepend-focus");
    });
    $("#username").focus();

    $("#username").focusout(function () {
        $(this).parent(".input-prepend").removeClass("input-prepend-focus");
    });

    $("#password").focus(function () {
        $(this).parent(".input-prepend").addClass("input-prepend-focus");
    });

    $("#password").focusout(function () {
        $(this).parent(".input-prepend").removeClass("input-prepend-focus");
    });

    var prmstr = window.location.search.substr(1).split("&"),
        params = {},
        tmparr,
        i = 0;

    for (i = 0; i < prmstr.length; i++) {
        tmparr = prmstr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    if (params.failed) {
        $("#error").text("Incorrect login!").show();
    }
});