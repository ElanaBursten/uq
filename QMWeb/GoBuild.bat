REM GIT_COMMIT, GIT_BRANCH, and BUILD_NUMBER variables are set by Jenkins. These are defaults for testing.

set BUILD_NUMBER=1
set GIT_COMMIT=devtestnota4reallongcommitidforourbuilds
set GIT_BRANCH=origin/develop

msbuild GoQMWeb.msbuild
pause