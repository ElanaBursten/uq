exports.config = {
  // seleniumServerJar: './node_modules/protractor/selenium/selenium-server-standalone-2.44.0.jar',

  allScriptsTimeout: 11000,

  specs: [
    'test/e2e/*.js'
  ],

  capabilities: {
    'browserName': 'firefox'
  },

  chromeOnly: false,

  baseUrl: 'http://localhost:8765/',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 300000,
    isVerbose: true
  }
};
