var gulp = require('gulp'),
    gutil = require('gulp-util'),
    jade = require('gulp-jade'),
    useref = require('gulp-useref'),
    minifyCss = require('gulp-minify-css'),
    minifyHtml = require('gulp-minify-html'),
    sass = require('gulp-sass'),
    templateCache = require('gulp-angular-templatecache'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    streamqueue = require('streamqueue'),
    rename = require("gulp-rename"),
    fs = require('fs'),
    del = require('del'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    assets = require('gulp-assets'),
    replace = require('gulp-replace'),
    jshint = require('gulp-jshint'),
    jslint = require('gulp-jslint-simple'),
    stylish = require('jshint-stylish'),
    filter = require('gulp-filter'),
    karma = require('gulp-karma'),
    protractor = require('gulp-protractor').protractor,
    webdriver_update = require('gulp-protractor').webdriver_update,
    childProcess = require('child_process'),
    runSequence = require('run-sequence'),
    async = require('async');

var IN = "www/",
    OUT = "www-build/";

var jslintOpts = {
    "browser": true,
    "maxlen": 120,
    "white": false,
    "vars": false,
    "unparam": true,
    "sloppy": true,
    "plusplus": true,
    "indent": 4,
    "stupid": true,
    "forin": false,
    "eqeq": false,
    "devel": true,
    "continue": true,
    "node": true,
    "regexp": true,
    "todo": true,
    "nomen": true,
    "predef": [
        "jQuery", "angular", "moment",
        "$", "Snap", "Microsoft",
        "numeral", "_",
        "metadata", "syncSchema",
        "onmessage", "postMessage", "LZString", "importScripts",
        "Worker", "Blob", "URL",
        "describe", "it", "beforeEach", "afterEach", "expect",
        "inject", "browser", "by", "element", "jasmine"
    ]
}

gulp.task('clean', function (done) {
    del([OUT + "*"], done);
});

gulp.task('copy-dev-config', function () {
    if (!fs.existsSync(IN + "modules/config.js")) {
        return gulp.src(IN + "modules/config.example.js")
            .pipe(rename("modules/config.js"))
            .pipe(gulp.dest(IN));
    }
});

gulp.task('copy-prod-config', function () {
    return gulp.src(IN + "modules/config.example.js")
        .pipe(ngAnnotate())
        .pipe(gulp.dest(OUT))
        .pipe(rename("config.js"))
        .pipe(gulp.dest(OUT));
});

gulp.task('static-files', function () {
    var exts = [
        "**/*.png",
        "**/*.gif",
        "**/*.json",
        "**/*.jpeg",
        "**/*.jpg",
        "**/*.svg",
        "**/*.eot",
        "**/*.ttf",
        "**/*.woff",
        "!lib/**"];
    return gulp.src(exts, {cwd: IN}).pipe(gulp.dest(OUT));
});

gulp.task('copy-compass-lib', function () {
    return gulp.src(IN + 'lib/compass-vanilla/compass/stylesheets/**/*')
        .pipe(gulp.dest(OUT + 'lib/compass-vanilla/compass/stylesheets'));
});

gulp.task('copy-map-mock-workaround', function () {
    return gulp.src(IN + 'lib/google-maps-mock/google-maps-mock.js')
        .pipe(rename('google-maps-mock.js'))
        .pipe(gulp.dest(OUT));
});

gulp.task('scss-string-replace', ['copy-compass-lib'], function () {
    return gulp.src('lib/compass-vanilla/compass/stylesheets/compass/**/*.scss', {cwd: OUT})
        .pipe(replace('$mult-bgs: -compass-list-size($backgrounds) > 1;', '$mult-bgs: length($backgrounds) > 1;'))
        .pipe(replace('@warn "$inset expected to be true or the inset keyword.', '//'))
        .pipe(replace('@warn "$relative-font-sizing is true but', '//'))
        .pipe(gulp.dest(OUT + 'lib/compass-vanilla/compass/stylesheets/compass/'));
});

gulp.task('css', ['scss-string-replace'], function () {
    gulp.start('css-ie');
    var vendorStream, appStream, ieFilter;
    ieFilter = filter('!style-ie.css');
    vendorStream = gulp.src(IN + 'CSS/**/*.css').pipe(ieFilter);
    appStream = gulp.src(IN + 'CSS/style.scss')
        .pipe(sass({includePaths: [OUT + 'lib/compass-vanilla/compass/stylesheets',
                IN + 'lib/bootstrap-sass-official/assets/stylesheets']}));

    return streamqueue({objectMode: true}, vendorStream, appStream)
        .pipe(concat('styles.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest(OUT + 'CSS/'));
});

gulp.task('css-ie', function () {
    return gulp.src(IN + 'CSS/style-ie.css')
        .pipe(minifyCss())
        .pipe(gulp.dest(OUT + 'CSS/'));
});

gulp.task('html', function () {
    return gulp.src(["main.jade", "login.jade"], {cwd: IN})
        .pipe(jade({pretty: true}))
        .pipe(useref())
        .pipe(minifyHtml({
            empty: true,
            quotes: true,
            conditionals: true,
            comments: true
        }))
        .pipe(gulp.dest(OUT));
});

gulp.task('js', function () {
    gulp.start('js-login');
    var templateStream, appStream, configFilter,
    build = '.' + process.env.BUILD_NUMBER;
    if (build == undefined){
        build = '.devtest';
    }
    configFilter = filter("!config.js");
    /** Minimize the client-page JS **/
    // cache the templates
    templateStream = gulp.src("**/*.tmplt.jade", {cwd: IN})
        .pipe(jade())
        .pipe(templateCache({
            root: "/"
        }));

    // add annotations to modules (array-notation)
    appStream = gulp.src("main.jade", {cwd: IN})
        .pipe(jade({pretty: true}))
        .pipe(assets.js())
        .pipe(configFilter)
        .pipe(ngAnnotate());
    return streamqueue({objectMode: true}, appStream, templateStream)
        // concat the two streams into a file
        .pipe(concat("client.min.js"))
        .pipe(uglify())
        .pipe(replace('return{version:"DEVTEST"}',
                'return{version:"' + fs.readFileSync('version.txt', {encoding: 'utf8'}) + build + '"}'))
        .pipe(gulp.dest(OUT));
});

gulp.task('js-login', function () {
    return gulp.src(IN + "login.jade")
        .pipe(jade({pretty: true}))
        .pipe(assets.js())
        .pipe(concat('login.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(OUT));
});

gulp.task('worker-libs', function () {
    var libs = [
        "**/lib/lz-string/libs/lz-string-1.3.3.js"
    ];

    return gulp.src(libs, {cwd: IN})
        .pipe(gulp.dest(OUT));
});

gulp.task('lint', function () {
    return gulp.src(["www/**/*.js", "test/**/*.js", "!www/lib/**"])
        .pipe(jslint.run(jslintOpts))
        .pipe(jslint.report({
            reporter: stylish,
            emitErrorAtEnd: true
        }))
        .pipe(jshint())
        .pipe(jshint.reporter("jshint-stylish"))
        .pipe(jshint.reporter("fail"));
});

gulp.task('dev-scss', ['scss-string-replace'], function () {
    return watch({glob: IN + 'CSS/style.scss', name: "SCSS", verbose: true})
        .pipe(plumber())
        .pipe(sass({includePaths: [OUT + 'lib/compass-vanilla/compass/stylesheets',
                IN + 'lib/bootstrap-sass-official/assets/stylesheets'],
                errLogToConsole: true}))
        .pipe(gulp.dest(OUT + "CSS/"));
});

gulp.task('dev-css', function () {
    return watch({glob: IN + "**/*.css", name: "CSS", verbose: true})
        .pipe(gulp.dest(OUT));
});

gulp.task('dev-jade', function () {
    return watch({glob: IN + "**/*.jade", name: "Jade", verbose: true})
        .pipe(plumber())
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(OUT));
});

gulp.task('dev-js', function () {
    var libFilter = filter("!lib/**");
    return watch({glob: IN + "**/*.js", name: "JS", verbose: true})
        .pipe(libFilter)
        .pipe(jslint.run(jslintOpts))
        .pipe(jslint.report({
            reporter: stylish
        }))
        .pipe(jshint())
        .pipe(jshint.reporter("jshint-stylish"))
        .pipe(libFilter.restore())
        .pipe(gulp.dest(OUT));
});

gulp.task('karma', function () {
    return gulp.src('fake.file') // Give a fake file so Karma uses the files in karma.conf.js
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function (err) { // Exit gulp if tests fail
            throw err;
        });
});

gulp.task('webdriver_update', webdriver_update);

gulp.task('protractor', function (cb) {
    async.series(
        [
            function (callback) {
                if (!fs.existsSync('server/main.exe')) {
                    childProcess.exec('go build main.go', {cwd: 'server'}, function () {
                        callback();
                    });
                } else {
                    callback();
                }
            },
            function (callback) {
                var server = childProcess.execFile('main.exe', [], {cwd: 'server'});
                gulp.src('fake.file')
                    .pipe(protractor({
                        configFile: 'protractor.conf.js'
                    }))
                    .on('end', function () {
                        server.kill();
                        callback();
                    });
            }
        ],
        function () {
            cb();
        }
    );
});

gulp.task('build', ['clean'], function (cb) {
    runSequence(
        [
            'copy-prod-config', 'static-files', 'css',
            'js', 'html', 'copy-map-mock-workaround', 'worker-libs'
        ],
        cb
    );
});

gulp.task('prod', ['lint'], function (cb) {
    runSequence(
        ['build', 'karma'],
        // 'protractor', Does not pass on build server with Firefox.
        cb
    );
});

gulp.task('dev', ['clean'], function (cb) {
    runSequence(
        [
            'copy-dev-config', 'static-files', 'dev-scss',
            'dev-css', 'dev-jade', 'dev-js', 'copy-map-mock-workaround'
        ],
        cb
    );
});