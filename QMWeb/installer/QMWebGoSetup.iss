#define MyAppName "Q Manager Web Demo Server"
#define VerFile FileOpen("../version.txt")
#define Build GetEnv("BUILD_NUMBER")
#define MyAppVersion FileRead(VerFile)
#expr FileClose(VerFile)
#undef VerFile

#define MyAppPublisher "UtiliQuest, LLC"
#define MyAppURL "http://www.utiliquest.com/"
#define MyAppExeName "QMWebDemoServer.exe"
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7)
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppId={{3EB9CEBD-ACE3-40B6-BEDB-603701FA2B1D}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputDir=..\build-output\
OutputBaseFilename=QMWeb-Demo-Setup-{#MyAppVersion}.{#Build}{#GitCommitIDWithDash}
OutputManifestFile=QMWeb-Demo-Setup-Manifest.txt
Compression=lzma
SolidCompression=yes
AppCopyright=Copyright 2014 by UtiliQuest LLC

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "..\www-build\*"; DestDir: "{app}\www"; Flags: ignoreversion recursesubdirs createallsubdirs; Excludes: config.js
Source: "..\www-build\config.example.js"; DestDir: "{app}\www"; DestName: config.js; Flags: onlyifdoesntexist uninsneveruninstall
Source: "..\server\staticJSON\*"; DestDir: "{app}\server\staticJSON"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "..\server\QMWebDemoServer.exe"; DestDir: "{app}\server"; Flags: ignoreversion
Source: "..\server\QMWebDemoServerConf.example.json"; DestDir: "{app}\server"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\server\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
