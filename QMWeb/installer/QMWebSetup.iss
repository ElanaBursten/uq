#define MyAppName "Q Manager Web"
#define VerFile FileOpen("../version.txt")
#define Build GetEnv("BUILD_NUMBER")
#define MyAppVersion FileRead(VerFile)
#expr FileClose(VerFile)
#undef VerFile

#define MyAppPublisher "UtiliQuest, LLC"
#define MyAppURL "http://www.utiliquest.com/"
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7)
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-DevTest"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2014 by UtiliQuest LLC
AppName={#MyAppName} 
AppVerName={#MyAppName} {#MyAppVersion} 
UninstallDisplayName = {#MyAppName} {#MyAppVersion} {code:GetVirtualDirNameFromInstallDir|''}
;AppId must be unique for each instance (aka VirtualDir) for the uninstaller to work correctly.
AppID={#MyAppName} {code:GetVirtualDirAlias|''}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppMutex=UtiliQuestQManagerWebContent
AppVersion={#MyAppVersion}{#GitCommitIDWithDash}
DefaultDirName={pf}\UtiliQuest\Q Manager Server
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputDir=..\build-output\
OutputBaseFilename=QMWeb-Setup-{#MyAppVersion}.{#Build}{#GitCommitIDWithDash}
OutputManifestFile=QMWeb-Setup-Manifest.txt
Compression=lzma/max
SolidCompression=yes
SetupLogging=yes
UsePreviousLanguage=no
DirExistsWarning=no

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "..\www-build\*"; DestDir: "{app}\{code:GetVirtualDirAlias|''}\www"; Flags: ignoreversion recursesubdirs createallsubdirs; Excludes: config.js; 
Source: "..\www-build\config.example.js"; DestDir: "{app}\{code:GetVirtualDirAlias|''}\www"; DestName: config.js; Flags: onlyifdoesntexist uninsneveruninstall; 

[Code]
var
  VirtualDirAliasPage: TInputQueryWizardPage;

procedure CreateCustomPages;
var
  DefaultVirDir: string;
begin
  VirtualDirAliasPage := CreateInputQueryPage(wpSelectDir, 'Select Server Instance',  
    'Q Manager Server Alias', 'Please specify the name of the virtual directory ' + 
    'alias containing the QMLogic service that will host the web content, then click Next.');
  // Add items (False means it's not a password edit)
  VirtualDirAliasPage.Add('Alias:', False);
  DefaultVirDir := GetPreviousData('Alias', '');
  if DefaultVirDir = '' then
    DefaultVirDir := 'QMServerApp'; 
  VirtualDirAliasPage.Values[0] := DefaultVirDir;
end;

procedure InitializeWizard();
begin
  CreateCustomPages;
end;

{ Warns user if they try to install into a folder that does not contain QMLogic2. }
function NextButtonClick(CurPageID: Integer): Boolean;
var
  QML2: string;
begin
  Result := True;
  case CurPageID of
    wpReady: begin
      QML2 := ExpandConstant('{app}') + '\' + VirtualDirAliasPage.Values[0] + 
        '\QMLogic2_' + VirtualDirAliasPage.Values[0] + '.exe'; 
      if not FileExists(QML2) then begin
        MsgBox('Setup detected no QMLogic2 in the selected dir.' #10#13 +
          '[' + QML2 + ']' #10#13 +
          'Please go back and select a valid QM Server folder and alias name.', 
          mbError, MB_OK);
        Result := False;
      end;
    end;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo,
  MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: string): string;
var
  S: string;
begin
  S := MemoTypeInfo + NewLine;
  S := S + MemoComponentsInfo + NewLine;
  S := S + 'QM Server Alias: ' + NewLine;
  S := S + Space + VirtualDirAliasPage.Values[0] + NewLine;
  S := S + 'QM Web Content Path: ' + NewLine; 
  S := S + Space + ExpandConstant('{app}') + '\' + VirtualDirAliasPage.Values[0] + '\www' + NewLine;
  S := S + MemoGroupInfo;
  Result := S;
end;

procedure RegisterPreviousData(PreviousDataKey: Integer);
begin
  SetPreviousData(PreviousDataKey, 'Alias', VirtualDirAliasPage.Values[0]);
end;

function GetVirtualDirAlias(DummyParam: string): string;
begin
  if Assigned(VirtualDirAliasPage) then 
    Result := VirtualDirAliasPage.Values[0]
  else //it has not been collected from the custom page yet
    Result := '';
end;



// TODO: These "common" functions should be in a shared file instead of in each installer.
function RemoveTrailingBackslash(Path: string): string;
begin
  Result := Path;
  if Result[Length(Result)] = '\' then 
    Delete(Result, Length(Result), 1); 
end;

function ExtractPathFromFilename(FileName: string): string;
begin
  Result := FileName;
  while Result[Length(Result)] <> '\' do //remove exe name from string to next backslash
    Delete(Result, Length(Result), 1);
end;

function GetVirtualDirNameFromInstallDir(DummyParam: string): string;
begin
  Result := ExpandConstant('{app}'); 
  Result := ExtractFileName(RemoveTrailingBackslash(Result));
end;
