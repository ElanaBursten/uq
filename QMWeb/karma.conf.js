module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'www/lib/jquery/dist/jquery.js',
      'www/lib/jquery-migrate/jquery-migrate.js',
      'www/lib/jquery.uniform/jquery.uniform.js',
      'www/lib/flot/jquery.flot.js',
      'www/lib/flot/jquery.flot.resize.js',
      'www/lib/flot/jquery.flot.pie.js',
      'www/lib/flot.tooltip/js/jquery.flot.tooltip.js',

      'www/lib/angular/angular.js',
      'www/lib/angular-route/angular-route.js',
      'www/lib/angular-mocks/angular-mocks.js',
      'www/lib/moment/moment.js',
      'www/lib/numeral/numeral.js',
      'www/lib/snapjs/snap.js',
      'www/lib/underscore/underscore.js',
      'www/lib/angular-ui-bootstrap-bower/ui-bootstrap.js',
      'www/lib/google-maps-mock/google-maps-mock.js',
      'www/lib/angular-google-maps/dist/angular-google-maps.js',
      {pattern: 'www/lib/lz-string/libs/lz-string-1.3.3.js', watched: false, included: false, served: true},

      'www/modules/*.js',
      'www/modules/**/*.js',
      'www/directives/*.js',
      'www/directives/**/*.js',
      'www/services/*.js',

      {pattern: 'server/staticJSON/**/*.json', watched: true, included: false, served: true},
      'test/unit/**/*.js'
    ],
    exclude: [
    ],
    proxies: {
      '/lib/lz-string/libs/lz-string-1.3.3.js': '/base/www/lib/lz-string/libs/lz-string-1.3.3.js'
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,

    // Use Firefox, it works across plats and on build server?
    browsers: ['Firefox'], // 'Chrome', 'IE', 'Firefox'
    plugins : [
               'karma-chrome-launcher',
               'karma-firefox-launcher',
               //'karma-IE-launcher',
               'karma-jasmine'
               ],
    captureTimeout: 60000,
    browserNoActivityTimeout: 30000,
    singleRun: false
  });
};
