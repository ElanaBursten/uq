package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var store = sessions.NewCookieStore([]byte("something-very-secret"))
var baseUrl = "/api/v1"
var dateFormatter = "2006-01-02"

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func getBasePath() string {
	www_check, www_err := exists("../www/")
	www_build_check, www_build_err := exists("../www-build/")

	path := ""

	if www_build_check {
		path = "../www-build/"
	} else if www_check {
		path = "../www/"
	} else {
		fmt.Println(www_err)
		fmt.Println(www_build_err)
	}
	return path
}

var basePath = getBasePath()

func addAngularRoutes(r *mux.Router, path ...string) {
	for p := range path {
		r.HandleFunc("/"+path[p], AngularRouteHandler).Methods("GET")
	}
}

func AngularRouteHandler(w http.ResponseWriter, hr *http.Request) {
	path := basePath + "main.html"
	file, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("Error reading file: " + path)
		return
	}
	if hasAuth(hr, w) {
		fmt.Fprint(w, string(file))
		return
	}
	http.Redirect(w, hr, "/login", 301)
}

func apiCall(r *mux.Router, url, json, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		if !needsAuth || hasAuth(hr, w) {
			file, err := ioutil.ReadFile("staticJSON/" + json)
			if err != nil {
				fmt.Println("Error reading JSON file: " + json)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprint(w, string(file))
		}
		return
	}).Methods(method)
}

func matchSearchString(searchString string) []string {
	var match []string
	names := []string{"jacob smith", "thurman smith", "ansco", "billy bones", "bnl", "john doe", "jane doe", "willy stewart", "sts/uq atlanta", "corporate office - atlanta", "mark redfern"}
	ids := []string{"314", "804", "16483", "111", "222", "333", "444", "2263", "2676", "4513", "5728"}
	count := 0
	for i := 0; i < len(names); i++ {
		if strings.Contains(names[i], searchString) {
			match = append(match, ids[i])
			count = count + 1
		}
	}
	if count == 0 {
		return []string{"0"}
		count = 1
	}
	match = match[:count]
	return match
}

func nodestatApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		if !needsAuth || hasAuth(hr, w) {
			if strings.Contains(hr.URL.String(), "?q=") {
				var data = "{\"data\":["
				var splitSearchString = strings.SplitAfter(hr.URL.String(), "?q=")
				var searchString = strings.ToLower(splitSearchString[len(splitSearchString)-1])
				match := matchSearchString(searchString)
				for i := 0; i < len(match); i++ {
					var filename = "staticJSON/" + match[i] + ".json"
					file, err := ioutil.ReadFile(filename)
					if err != nil {
						fmt.Println("Error reading JSON file: " + filename)
						return
					} else {
						data += string(file) + ","
					}
				}
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprint(w, string(data)[:len(data)-1]+"]}")
			} else {
				var splitSearchString = strings.SplitAfter(hr.URL.Path, "/nodestat/")
				var searchString = strings.ToLower(splitSearchString[len(splitSearchString)-1])
				if searchString == "" {
					searchString = "2676"
				}
				var filename = "staticJSON/" + searchString + ".json"
				file, err := ioutil.ReadFile(filename)
				if err != nil {
					fmt.Println("Error reading JSON file: " + filename)
					return
				}
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprint(w, string(file))
			}
		}
		return
	}).Methods(method)
}

func randomMinuteJSONApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		//Use this to simulate server lag
		//time.Sleep(5000 * time.Millisecond)
		if !needsAuth || hasAuth(hr, w) {
			w.Header().Set("Content-Type", "application/json")
			vars := mux.Vars(hr)
			passedDate := vars["date"]
			parsedDate, _ := time.Parse(dateFormatter, passedDate)
			fmt.Fprint(w, randomMinuteJSONGenerator(parsedDate))
		}
		return
	}).Methods(method)
}

func randomDayJSONApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		//Use this to simulate server lag
		//time.Sleep(5000 * time.Millisecond)
		if !needsAuth || hasAuth(hr, w) {
			w.Header().Set("Content-Type", "application/json")
			vars := mux.Vars(hr)
			passedDate := vars["date"]
			parsedDate, _ := time.Parse(dateFormatter, passedDate)
			fmt.Fprint(w, randomDayJSONGenerator(parsedDate))
		}
		return
	}).Methods(method)
}

func randomDayJSONGenerator(startDate time.Time) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var TOTALPOINTS = int(time.Since(startDate).Hours() / 24)
	var ans = "{ \"Category\": \"CC\",\n" +
		"  \"Entity\": \"3003 Utilities Protection Center (Eden)\"," +
		"  \"Metric\": \"Emergencies Received\"," +
		"  \"MetricType\": \"flow\"," +
		"  \"Granularity\": \"Day\"," +
		"  \"DateStart\": \"" + startDate.Format(dateFormatter) + "\"," +
		"  \"DateEnd\": \"" + time.Now().Format(dateFormatter) + "\"," +
		"  \"Data\": [0"

	for i := 0; i < TOTALPOINTS-1; i++ {
		var num = r.Intn(30) - 15
		if num < 0 {
			num = 0
		}
		ans += "," + strconv.Itoa(num)
	}

	ans += "]}"

	return ans
}

func randomMinuteJSONGenerator(startDate time.Time) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var TOTALPOINTS = int(time.Since(startDate).Minutes())
	var ans = "{ \"Category\": \"CC\",\n" +
		"  \"Entity\": \"3003 Utilities Protection Center (Eden)\"," +
		"  \"Metric\": \"Emergencies Received\"," +
		"  \"MetricType\": \"flow\"," +
		"  \"Granularity\": \"Minute\"," +
		"  \"DateStart\": \"" + startDate.Format(dateFormatter) + "\"," +
		"  \"DateEnd\": \"" + time.Now().Format(dateFormatter) + "\"," +
		"  \"Data\": [0"

	for i := 0; i < TOTALPOINTS-1; i++ {
		var num = r.Intn(30) - 15
		if num < 0 {
			num = 0
		}
		ans += "," + strconv.Itoa(num)
	}
	ans += "]}"

	return ans
}

func hasAuth(req *http.Request, writer http.ResponseWriter) bool {
	session, _ := store.Get(req, "session-name")
	if session.Values["loggedIn"] == true {
		return true
	}
	return false
}

func taskListApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		var splitTicketStringFirst = strings.SplitAfter(hr.URL.Path, "/employee/")
		var ticketWithWork = splitTicketStringFirst[len(splitTicketStringFirst)-1]
		var splitTicketStringSecond = strings.Split(ticketWithWork, "/")
		var ticket = splitTicketStringSecond[0]

		if !needsAuth || hasAuth(hr, w) {
			var filename = "staticJSON/taskList/" + ticket + "_task_list.json"
			file, err := ioutil.ReadFile(filename)
			if err != nil {
				fmt.Println("Error reading JSON file: " + filename)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprint(w, string(file))
		}
		return
	}).Methods(method)
}

func damageApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		//var splitTicketString = strings.SplitAfter(hr.URL.Path, "/work/");
		//var ticket = splitTicketString[len(splitTicketString)-1];

		//Place holder

		return
	}).Methods(method)
}

func workOrderApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		//var splitTicketString = strings.SplitAfter(hr.URL.Path, "/work/");
		//var ticket = splitTicketString[len(splitTicketString)-1];

		//Place holder

		return
	}).Methods(method)
}

func ticketApiCall(r *mux.Router, url, method string, needsAuth bool) {
	r.HandleFunc(url, func(w http.ResponseWriter, hr *http.Request) {
		var splitTicketString = strings.SplitAfter(hr.URL.Path, "/ticket/")
		var ticket = splitTicketString[len(splitTicketString)-1]

		if !needsAuth || hasAuth(hr, w) {
			var filename = "staticJSON/tickets/" + ticket + ".json"
			file, err := ioutil.ReadFile(filename)
			if err != nil {
				fmt.Println("Error reading JSON file: " + filename)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprint(w, string(file))
		}
		return
	}).Methods(method)
}

func main() {
	r := mux.NewRouter()

	// Session lasts for a day
	store.Options.MaxAge = 3600 * 24

	// Angular Routes
	addAngularRoutes(r,
		"",
		"mystats",
		"mywork",
		"help",
		"managedash",
		"manage",
		"messages",
		"newticket",
		"ticketdetail",
		"oldticket",
		"findticket",
		"findwo",
		"damage",
		"reports",
		"approval",
		"assets",
		"billing",
		"areaeditor",
		"options",
		"sync",
		"settings",
		"allocation")

	// API Calls
	apiCall(r, "/testAuth", "empty.json", "GET", true)

	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/TREC/MIN/{date:.*}", "MIN.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/TREC/HOUR/{date:.*}", "HOUR.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/TREC/SEC/{date:.*}", "SEC.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/EREC/MIN/{date:.*}", "MIN.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/EREC/HOUR/{date:.*}", "HOUR.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/{entity:.*}/EREC/SEC/{date:.*}", "SEC.json", "GET", true)

	//scrolling time series graph requests gets caught by this
	apiCall(r, baseUrl+"/metric/total/EMP/{entity:.*}/TCLEMP/SEC/{date:.*}", "SEC.json", "GET", true)

	randomMinuteJSONApiCall(r, baseUrl+"/metric/total/EMP/{entity:.*}/TCLEMP/MIN/{date:.*}", "GET", true)
	apiCall(r, baseUrl+"/metric/total/EMP/{entity:.*}/TCLEMP/HOUR/{date:.*}", "TCLEMP-rollup-HOUR.json", "GET", true)
	apiCall(r, baseUrl+"/metric/total/EMP/{entity:.*}/RESPQ/HOUR/{date:.*}", "TCLEMP-rollup-HOUR.json", "GET", true)
	randomDayJSONApiCall(r, baseUrl+"/metric/total/EMP/{entity:.*}/TCLEMP/DAY/{date}", "GET", true)

	nodestatApiCall(r, baseUrl+"/nodestat/{entity:.*}", "GET", true)
	nodestatApiCall(r, baseUrl+"/nodestat", "GET", true)

	randomMinuteJSONApiCall(r, baseUrl+"/metric/single/CC/NewJersey/TCLOSE/MIN/{date:.*}", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/1421/TREC/MIN/2014-01-01", "1421-TREC-MIN-2014-01-01.json", "GET", true)
	randomMinuteJSONApiCall(r, baseUrl+"/metric/single/CC/FNV1/TCLOSE/MIN/{date:.*}", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/1421/TCLOSE/MIN/2014-01-01", "1421-TCLOSE-MIN-2014-01-01.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/OCC2/TCLOSE/MIN/{date:.*}", "OCC2-TCLOSE-MIN-2014-01-01.json", "GET", true)
	apiCall(r, baseUrl+"/metric/single/CC/3003/TCLOSE/MIN/{date:.*}", "3003-TCLOSE-MIN-2014-01-01.json", "GET", true)

	apiCall(r, baseUrl+"/metric", "metric.json", "GET", true)
	apiCall(r, baseUrl+"/hierarchy", "hierarchy.json", "GET", true)
	apiCall(r, baseUrl+"/sync/{emp:.*}", "sync/3512-sync.json", "GET", true)

	apiCall(r, baseUrl+"/ping", "empty.json", "GET", true)

	apiCall(r, baseUrl+"/error", "empty.json", "POST", false)

	// TODO: In QMLogic2 tic, dmg, and wo Urls do not have the "/work" part. They are like: baseUrl+"/ticket/{ticket:.*}"
	ticketApiCall(r, baseUrl+"/work/ticket/{ticket:.*}", "GET", true)
	damageApiCall(r, baseUrl+"/work/damage/{damage:.*}", "GET", true)
	workOrderApiCall(r, baseUrl+"/work/workOrder/{workOrder:.*}", "GET", true)
	taskListApiCall(r, baseUrl+"/employee/{node_id:.*}/work/", "GET", true)

	r.HandleFunc("/login", func(w http.ResponseWriter, hr *http.Request) {
		session, _ := store.Get(hr, "session-name")
		formBody, err := ioutil.ReadAll(hr.Body)
		if err != nil {
			fmt.Println("Error Reading Credentials")
			return
		}
		if string(formBody) == "username=admin&password=quDragon73" {
			session.Values["loggedIn"] = true
			session.Save(hr, w)
			redirect := hr.FormValue("redirectTo")
			http.Redirect(w, hr, "/"+redirect, 301)
			return
		}
		http.Redirect(w, hr, "/login?failed=1", 301)
	}).Methods("POST")

	r.HandleFunc("/login", func(w http.ResponseWriter, hr *http.Request) {
		path := basePath + "login.html"
		file, err := ioutil.ReadFile(path)
		if err != nil {
			fmt.Println("Error reading file.")
			return
		}
		fmt.Fprint(w, string(file))
		return
	}).Methods("GET")

	r.HandleFunc(baseUrl+"/logout", func(w http.ResponseWriter, hr *http.Request) {
		session, _ := store.Get(hr, "session-name")
		session.Values["loggedIn"] = false
		session.Save(hr, w)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, string(`{}`))
		return
	}).Methods("POST")

	r.PathPrefix("/").Handler(http.FileServer(http.Dir(basePath))) //Root is down here b/c router needs to be set-up from specific to general

	type Configuration struct {
		Port string
	}

	file, _ := os.Open("QMWebDemoServerConf.json")
	decoder := json.NewDecoder(file)
	configuration := &Configuration{}
	decoder.Decode(&configuration)

	port := os.Getenv("PORT")
	addr := os.Getenv("IP")
	if port == "" {
		if configuration.Port != "" {
			fmt.Println("Running on port :" + configuration.Port)
			port = configuration.Port
		} else {

			fmt.Println("No config file or $PORT set, Running on port 8765")
			port = "8765"
		}
	} else {
		fmt.Println("Running on port " + port)
	}
	log.Fatal(http.ListenAndServe(addr+": "+port, r))
}
