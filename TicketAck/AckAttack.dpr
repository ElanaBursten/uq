program AckAttack;
{$R 'QMVersion.res' '..\QMVersion.rc'}
//{$R '..\QMIcon.res'}
{
  Run params
    configXML := ParamStr(1);
    callCenter := ParamStr(2);
   'GUI' or 'RUN' := ParamStr(3);
}
{$R *.res}
uses
  Vcl.Forms,
  SysUtils,
  uMainParse in 'uMainParse.pas' {Form1},
  GlobalSU in 'GlobalSU.pas';

 var
   MyInstanceName: string;
begin
  Application.Initialize;
  MyInstanceName := ExtractFileName(Application.ExeName) + '_' + ParamStr(2);
  if (ParamStr(3) <> 'GUI') and
    (ParamStr(3) <> 'RUN')then
    begin
      Application.Terminate;
      Exit;
    end;

  if CreateSingleInstance(MyInstanceName) then
    begin
      ReportMemoryLeaksOnShutdown := DebugHook <> 0;
      Application.ShowMainForm := (ParamStr(3) = 'GUI');
      Application.MainFormOnTaskBar := true;
      Application.CreateForm(TForm1, Form1);
      if ParamStr(3) = 'RUN' then
      begin
        //Form1.btnRetrieveClick(nil);
        Form1.GetEmailsandDelete(False, True)
      end else
        Application.Run;
    end
  else Application.Terminate;
end.
