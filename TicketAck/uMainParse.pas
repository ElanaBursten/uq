unit uMainParse;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, System.Win.ComObj, Winapi.MSXML,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,

  IdMessage, IdIMAP4, IdBaseComponent, IdComponent, IdTCPConnection, IdText,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, Vcl.ExtCtrls,
  Vcl.ComCtrls, Data.DB, Data.Win.ADODB, Vcl.Buttons, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdAntiFreezeBase,
  IdAntiFreeze;

type
  TLogType = (ltError, ltInfo, ltNotice, ltWarning, ltMissing);
  EBadAckData = class(Exception);

type
  TLogResults = record
    LogType: TLogType;
    MethodName: String[40];
    Status: String[200];
    ExcepMsg: String;
    DataStream: String;
  end;

type
  TForm1 = class(TForm)
    IMAPClient: TIdIMAP4;
    pnlMenu: TPanel;
    pnlButtons: TPanel;
    btnRetrieve: TButton;
    Splitter2: TSplitter;
    pnlMail: TPanel;
    pnlHeader: TPanel;
    Memo1: TMemo;
    pnlBody: TPanel;
    Memo2: TMemo;
    Splitter1: TSplitter;
    btnDelete: TButton;
    StatusBar1: TStatusBar;
    cbExpunge: TCheckBox;
    ADOConn: TADOConnection;
    updRespLog: TADOQuery;
    delRespActWait: TADOQuery;
    btnRestore: TButton;
    btnHotStop: TBitBtn;
    ProgressBar1: TProgressBar;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    pnlLabels: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lblOfMail: TLabel;
    Label10: TLabel;
    qryWaiting: TADOQuery;
    procedure btnRetrieveClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure btnHotStopClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Memo1DblClick(Sender: TObject);
    procedure Memo2DblClick(Sender: TObject);
    procedure IMAPClientStatus(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure Button1Click(Sender: TObject);
    procedure IdSSLIOHandlerSocketOpenSSL1Status(ASender: TObject;
      const AStatus: TIdStatus; const AStatusText: string);
    procedure Memo1Change(Sender: TObject);
    procedure Memo2Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    LogResult: TLogResults;
    flogDir: wideString;
    fbodyTextDir: wideString;
    fadoDatabase: wideString;
    fadoPassword: wideString;
    fadoLogin: wideString;
    fadoHost: wideString;
    fcallCenter: wideString;
    fconfigXML: tFilename;
    fSafeStop: boolean;
    fmailPort: integer;
    fmailPW: wideString;
    fmailHost: wideString;
    fmailUsername: wideString;
    RL_CallCenter:string;   //QM-495
    connectionStr:string;
    procedure clearLogRecord;
    function ParseXML(xmlFile: tFilename): boolean;
    function LoadParams: boolean;
    function ConfigDB: boolean;
    function ConfigEMail: boolean;
    function RemoveDups(aList: TStrings): TStrings;
    function GetTextView(Msg: TIdMessage): wideString;
    function GetAppVersionStr: string;
    function ProcessINI: boolean;
    { Private declarations }
  public
    MailBoxName: string;
    GetTextViewIndex: integer;
    ackList: TStringList;
    procedure WriteLog(LogResult: TLogResults);
    // procedure GetGmailBodyTextParts;
    function GetEmailsandDelete(GUI: boolean; Expunge: boolean = False)
      : boolean;
    property SafeStop: boolean read fSafeStop write fSafeStop;
    property callCenter: wideString read fcallCenter;
    property adoHost: wideString read fadoHost;
    property adoDatabase: wideString read fadoDatabase;
    property adoLogin: wideString read fadoLogin;
    property adoPassword: wideString read fadoPassword;
    property mailHost: wideString read fmailHost;
    property mailPort: integer read fmailPort;
    property mailUsername: wideString read fmailUsername;
    property mailPW: wideString read fmailPW;
    property logDir: wideString read flogDir;
    property bodyTextDir: wideString read fbodyTextDir;
    property configXML: tFilename read fconfigXML;
    { Public declarations }
  end;

procedure ScrollToLastLine(Memo: TMemo);

var
  Form1: TForm1;
  dupList: TStringList;

  // Provider=SQLNCLI11.1;Persist Security Info=False;User ID=QMParserUTL;password=ihjidojo3;Initial Catalog="";Data Source=SSDS-UTQ-QM-02;Initial File Name="";Server SPN=""

implementation

uses dateUtils, System.RegularExpressions, INIFiles;
{$R *.dfm}

procedure TForm1.btnDeleteClick(Sender: TObject);
var
  ToDelete, anIndx: integer;
  TheUID: string;
  TheFlags: TIdMessageFlagsSet;
  Confirmed: array of integer;
begin
  StatusBar1.Panels[0].Text := TimeToStr(Time);

  ToDelete := 0;
  try
    IMAPClient.Connect();
    try
      if IMAPClient.ConnectionState <> csNonAuthenticated then
      begin
        if IMAPClient.SelectMailBox(MailBoxName) then
        begin
          for anIndx := 1 to IMAPClient.MailBox.TotalMsgs do
          begin
            StatusBar1.Panels[8].Text := 'Deleting';
            ProgressBar1.Max := IMAPClient.MailBox.TotalMsgs;
            IMAPClient.GetUID(anIndx, TheUID);
            IMAPClient.UIDRetrieveFlags(TheUID, TheFlags);
            if mfDeleted in TheFlags then
            begin
              SetLength(Confirmed, Succ(ToDelete));
              Confirmed[ToDelete] := anIndx;
              Inc(ToDelete);
            end;
            application.ProcessMessages;
            ProgressBar1.StepIt;
            StatusBar1.Panels[1].Text := IntToStr(anIndx);
            StatusBar1.Panels[2].Text := IntToStr(IMAPClient.MailBox.TotalMsgs);
            StatusBar1.Refresh;
            ProgressBar1.Refresh;
          end; // for anIndx := 1 to IMAP.MailBox.TotalMsgs
          if ToDelete > 0 then
            if not IMAPClient.DeleteMsgs(Confirmed) then
            begin
              StatusBar1.Panels[8].Text := 'Del failed';
              Memo1.Lines.add('Failed to delete messages.');
            end
            else
              StatusBar1.Panels[8].Text := 'Del done';
        end; // if IMAPClient.SelectMailBox
        application.ProcessMessages;
      end;
    finally
      if cbExpunge.Checked then
      begin
        if IMAPClient.ExpungeMailBox then
          Memo2.Lines.add('That mail be gone forever');
      end
      else
        showMessage('the Delete flag has been set for all mail in Inbox');
      IMAPClient.Disconnect;
      cbExpunge.Checked := False;
    end;
  except
    on e: Exception do
    begin
      Memo2.Lines.add(e.message);
    end;
  end;
  StatusBar1.Panels[7].Text := TimeToStr(Time);
  StatusBar1.Refresh;
  application.Terminate;
end;

procedure TForm1.btnHotStopClick(Sender: TObject);
begin
  SafeStop := not(SafeStop);
  if SafeStop then
    btnHotStop.Font.Color := clRed
  else
    btnHotStop.Font.Color := clGreen;
end;

procedure TForm1.btnRestoreClick(Sender: TObject);
var
  TheUID: string;
  MsgIndex: integer;
  TheFlags: TIdMessageFlagsSet;
  TotalMsgs: integer;
begin
  // new stuff
  IMAPClient.Connect();
  try
    if IMAPClient.ConnectionState <> csNonAuthenticated then
    begin
      if IMAPClient.SelectMailBox(MailBoxName) then
      begin
        TotalMsgs := IMAPClient.MailBox.TotalMsgs;
        for MsgIndex := 1 to TotalMsgs do
        begin
          StatusBar1.Panels[8].Text := 'Restoring';
          ProgressBar1.Max := TotalMsgs;
          IMAPClient.GetUID(MsgIndex, TheUID);
          IMAPClient.UIDStoreFlags(TheUID, sdReplace,
            TheFlags - [mfFlagged, mfSeen, mfDeleted]);
          ProgressBar1.StepIt;
          ProgressBar1.Refresh;
          StatusBar1.Panels[1].Text := IntToStr(MsgIndex);
          StatusBar1.Panels[2].Text := IntToStr(TotalMsgs);
          StatusBar1.Refresh;
        end; // for anIndx := 1 to IMAP.MailBox.TotalMsgs
        StatusBar1.Panels[8].Text := 'Restore Done';
      end; // if IMAPClient.SelectMailBox
    end;

  finally
    IMAPClient.Disconnect;
  end;
  Memo1.Lines.add(IntToStr(TotalMsgs) + ' restored from Delete status.');
end;

procedure TForm1.btnRetrieveClick(Sender: TObject);
begin
  StatusBar1.Panels[8].Text := 'Ack''ing';
  btnHotStop.Enabled := true;
  GetEmailsandDelete(true, False);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  IMAPClient.Connect();
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dupList.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
begin
  ProcessINI;
  flogDir := 'c:\Logs';
  MailBoxName := 'INBOX';
  if ParamStr(3) = 'RUN' then
  begin
    LoadParams;
    ParseXML(configXML);
    ProcessINI;
    ConfigDB;
    ConfigEMail;
  end
  else if ParamStr(3) = 'GUI' then
  begin
    btnHotStop.Enabled := False;
    btnRetrieve.Enabled := False;
    for i := 0 to paramcount do
      Memo1.Lines.add('Param' + IntToStr(i) + ': ' + ParamStr(i));

    if LoadParams then
      Memo1.Lines.add('LoadParam succeeded');

    if ParseXML(configXML) then
      Memo1.Lines.add('ParseXML(configXML) succeeded')
    else
    begin
      Memo1.Lines.add('ParseXML(configXML) failed');
      exit;
    end;

    if ConfigDB then
      Memo1.Lines.add('ConfigDB succeeded ')
    else
    begin
      Memo1.Lines.add('ConfigDB failed');
      exit;
    end;

    if ConfigEMail then
      Memo1.Lines.add('ConfigEMail succeeded ');

    Memo1.Lines.add('******************************* Get Ready to Ack & Roll!!!! *******************************');
    btnRetrieve.Enabled := true;
    self.Caption := self.Caption + '     ' + ' Acking ' + callCenter + ' in the Free World';
    self.Caption := self.Caption + '     ' + GetAppVersionStr;
  end;
  dupList := TStringList.Create;
  dupList.Sorted := true;
  dupList.Duplicates := dupIgnore;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  // application.ScaleForPPI(96 * self.Monitor.PixelsPerInch / 115);
end;

function TForm1.GetEmailsandDelete(GUI: boolean;
  Expunge: boolean = False): boolean;
var
  MsgIndex, i: integer;
  MsgObject: TIdMessage;
  TheUID: string;
  fatalError: boolean;
  TheFlags: TIdMessageFlagsSet;
  s, sSerialNum, sAck: string;
  msgStatus: string;
  failed: boolean;
  ToDelete: integer;
  Confirmed: array of integer;
  iTotalMsgs: integer;
  totalInList: integer;
  ackList: TStringList;
  iQty: integer;
const
  FILE_EXT = '.TXT';

  function ExtractNumbers(const s: string): TArray<string>;
  var
    regex: TRegEx;
    match: TMatch;
    matches: TMatchCollection;
    i: integer;
  begin
    Result := nil;
    i := 0;
    regex := TRegEx.Create('\d+');
    matches := regex.matches(s);
    if matches.Count > 0 then
    begin
      SetLength(Result, matches.Count);
      for match in matches do
      begin
        Result[i] := match.Value;
        Inc(i);
      end;
    end;
  end;

begin
  LogResult.LogType := ltNotice;
  LogResult.MethodName:='Start of New Run--Get Mail and Process';
  WriteLog(LogResult);
  ToDelete := 0;
  fatalError := False;
  if GUI then
  begin
    Memo1.Clear;
    Memo2.Clear;
  end;

  IMAPClient.Connect;

  try
    ackList := TStringList.Create;
    ackList.Duplicates := dupIgnore;
    ackList.Sorted := true;
    if IMAPClient.SelectMailBox(MailBoxName) then
    begin
      iTotalMsgs := IMAPClient.MailBox.TotalMsgs;
      if GUI then
      begin
        StatusBar1.Panels[0].Text := TimeToStr(Time);
        StatusBar1.Panels[2].Text := IntToStr(iTotalMsgs);
      end;
      for MsgIndex := 1 to iTotalMsgs do
      begin
        MsgObject := TIdMessage.Create(nil);
        StatusBar1.Panels[1].Text :=  IntToStr(MsgIndex);
        failed := False;
        ToDelete := 0;
        try
          IMAPClient.Retrieve(MsgIndex, MsgObject);
          if GUI then
            MsgObject.Headers.ConvertToStdValues(Memo1.Lines);

          IMAPClient.GetUID(MsgIndex, TheUID);
          IMAPClient.UIDRetrieveFlags(TheUID, TheFlags);

          if mfDeleted in TheFlags then
          begin
            FreeAndNil(MsgObject);
            continue; // flagged for delete so get the next one
          end;

          SetLength(Confirmed, Succ(ToDelete));
          Confirmed[ToDelete] := MsgIndex;
          Inc(ToDelete);
          ackList.Clear;
          ackList.Delimiter := #124;
          ackList.StrictDelimiter := true;
          ackList.DelimitedText := GetTextView(MsgObject);
          totalInList := ackList.Count;
          if GUI then
          begin
            Memo2.Lines.AddStrings(ackList);
            StatusBar1.Panels[4].Text := IntToStr(totalInList);
            StatusBar1.Refresh;
          end;
          // ADOConn.BeginTrans;
          for i := 0 to totalInList - 1 do
          begin
            StatusBar1.Panels[3].Text := IntToStr(i);
            StatusBar1.Panels[5].Text := TimeToStr(Time);
            failed:=False;
            sAck := 'Ack';
            s := Trim(ackList[i]);
            if s = '' then
              continue;

            sSerialNum := ExtractNumbers(s)[0];
            updRespLog.Parameters.ParamByName('ack').Value := sAck;
            updRespLog.Parameters.ParamByName('serialNo').Value := sSerialNum;
            updRespLog.Parameters.ParamByName('CallCenter').Value := RL_CallCenter;//QM-495
            if updRespLog.ExecSQL > 0 then
            begin
              delRespActWait.Parameters.ParamByName('serialNo').Value :=
                sSerialNum;
              delRespActWait.ExecSQL;
            end
            else
            begin
              failed:=True;
              IMAPClient.UIDStoreFlags(TheUID, sdAdd, TheFlags + [mfFlagged]);
              try
                iQty := 0;
                qryWaiting.Parameters.ParamByName('RefNumber').Value:=sSerialNum;
                qryWaiting.Open;
                iQty := qryWaiting.FieldByName('Qty').AsInteger;
                If iQty = 0 then
                begin
                  LogResult.MethodName := 'Processing Mail';
                  LogResult.Status := 'Failed to update ';
                  LogResult.ExcepMsg :=  sSerialNum+' is Not in response_ack_waiting';
                end
                else
                LogResult.ExcepMsg :=  sSerialNum+' in response_ack_waiting';
                LogResult.LogType := ltError;
                if GUI then
                begin
                  Memo2.Lines.add(LogResult.MethodName + ' failed');
                  Memo2.Lines.add(LogResult.Status);
                  Memo2.Lines.add(LogResult.ExcepMsg);
                  Memo2.Lines.add(LogResult.DataStream);
                end;
                WriteLog(LogResult);
              finally
                qryWaiting.Close;
              end;
            end;
            StatusBar1.Panels[6].Text := TimeToStr(Time);
            application.ProcessMessages;
          end; // for i := 0 to ListBox1.Count - 1 do

          if not failed then
          begin
            // if ADOConn.InTransaction then
            // ADOConn.CommitTrans;
            LogResult.MethodName := 'Processing Mail, saving copy';
            LogResult.Status := 'Comitted all transactions';
            LogResult.DataStream := 'serialNumbers: ' + ackList.CommaText;
            LogResult.LogType := ltInfo;
            WriteLog(LogResult);
            if GUI then
            begin
              Memo2.Lines.add('Processed ' + sSerialNum + ' Ack');
              Memo2.Lines.add('Processed ' + IntToStr(i) + ' of ' +
                IntToStr(totalInList));
            end;
            MsgObject.Body.SaveToFile(bodyTextDir + '\' + callCenter +
              FormatDateTime('yyyy-mm-dd hh-mm-ss', MsgObject.Date) + FILE_EXT);

            if ToDelete > 0 then
              IMAPClient.DeleteMsgs(Confirmed);
            if Expunge then
              IMAPClient.ExpungeMailBox; // Uncomment this for Expunge testing;

          end
          else
          begin
            // if ADOConn.InTransaction then
            // ADOConn.RollbackTrans;
            LogResult.MethodName := 'Processing Mail';
            LogResult.Status := 'mail body empty-nothing to do!!';
            LogResult.DataStream := 'Mail Timestamp: ' +
              DateTimeToStr(MsgObject.Date);
            LogResult.LogType := ltInfo;
            WriteLog(LogResult);
          end;

          if SafeStop then
          begin
            Memo2.Lines.add('Halted due to Hot Stop request');
            exit;
          end;
        finally
          // write textBody to log
          IMAPClient.UIDStoreFlags(TheUID, sdAdd, TheFlags + [mfDeleted]);
          MsgObject.Free;
        end; // Try-Finally
      end; // 1 to IMAPClient.MailBox.TotalMsgs
    end;
  finally
    IMAPClient.Disconnect;
    ackList.Free;
    if GUI then
    begin
      btnHotStop.Enabled := False;
      StatusBar1.Panels[8].Text := 'Ack Completed';
      StatusBar1.Panels[7].Text := TimeToStr(Time);
      if SafeStop then
        StatusBar1.Panels[8].Text := 'Hot Stopped!!!';
      StatusBar1.Refresh;
    end;
  end; // try-finally
end;

function TForm1.GetTextView(Msg: TIdMessage): wideString;
var
  s, t, ContentType, BodyText, temp: string;
  i: integer;
begin
  GetTextViewIndex := -1;
  { No attachments, just show body }
  Msg.MessageParts.CountParts;
  if Msg.MessageParts.Count = 0 then
  begin
    Result := Msg.Body.Text;
    exit
  end;
  Msg.Headers.CaseSensitive := False;
  Msg.Headers.UnfoldLines := true;
  // Set variables to zero
  s := '';
  t := '';
  i := 0;
  // Go through to find first Text & HTML Attachment
  while (length(s) = 0) and (i < Msg.MessageParts.Count) do
  begin
    // Only look at text, not file attachments
    if Msg.MessageParts.Items[i] is TIdText then
    begin
      { Not multipart, get first attachment }
      if (pos('multipart', LowerCase(Msg.Headers.Values['Content-Type'])) = 0)
      then
      begin
        // Set text as first attachment with text (spaces don't count)
        while (i < Msg.MessageParts.Count) and (length(s) = 0) do
          if (Msg.MessageParts.Items[i] IS TIdText) and
            (length(Trim(TIdText(Msg.MessageParts.Items[i]).Body.Text)) > 0)
          then
          begin
            t := TIdText(Msg.MessageParts.Items[i]).Body.Text;
            temp := LowerCase(copy(t, 1, 450));
            if (pos('<html', temp) > 0) or (pos('<body', temp) > 0) then
              s := TIdText(Msg.MessageParts.Items[i]).Body.Text;
          end
          else
            Inc(i);
      end
      else { Is multipart/???, get both parts, display HTML }
      begin { Now get the first HTML text message attachment }
        while (i < Msg.MessageParts.Count) and (length(s) = 0) do
        begin
          { Attachment must be text type and not blank }
          if (Msg.MessageParts.Items[i] is TIdText) and
            (length(Trim(TIdText(Msg.MessageParts.Items[i]).Body.Text)) > 0)
          then
          begin
            ContentType := LowerCase(TIdText(Msg.MessageParts.Items[i])
              .ContentType);
            BodyText := TIdText(Msg.MessageParts.Items[i]).Body.Text;
            { Get first plain text }
            if (ContentType = 'text/plain') and (length(t) = 0) then
              t := BodyText;
            { Get first HTML attachment }
            if (ContentType = 'text/html') and (length(s) = 0) then
            begin
              s := BodyText;
            end;
            { Look for first html document }
            if (ContentType = 'multipart/alternative') OR
              (ContentType = 'multipart/mixed') OR
              (ContentType = 'multipart/related') then
            begin
              // Copy first 450 chars for speed
              temp := LowerCase(copy(BodyText, 1, 450));
              { test to see if it is HTML }
              if (length(s) = 0) then
              begin
                // is it HTML
                if (pos('<html', temp) > 0) or (pos('<body', temp) > 0) then
                  s := BodyText
                else { No, it is text }
                  if (length(t) = 0) then
                    t := BodyText;
              end;
              // multipart/related = all related parts inside one attatchments (html, images, etc)
              if (ContentType = 'multipart/related') and (t <> '') then
              begin
                s := '';
              end;
            end;
          end;
          { if s not set then next record }
          if length(s) = 0 then
            Inc(i);
        end;
      end;
    end;
    { if s not set then next record }
    if length(s) = 0 then
      Inc(i);
  end; // while
  { No HTML attachment found, show the plain text }
  if length(s) = 0 then
    s := t;
  if length(s) = 0 then
    s := '- - - Error, no multipart message found - - -';
  // Set the text view index if a part was used (for Forward email)
  if i < Msg.MessageParts.Count then
    GetTextViewIndex := i;
  s := StringReplace(s, #$D#$A, #124, [rfReplaceAll]);
  s := StringReplace(s, #44, '', [rfReplaceAll]);
  Result := s;
end;

function TForm1.RemoveDups(aList: TStrings): TStrings;
var
  i: integer;
begin
  try
    dupList.Clear;
    for i := 0 to aList.Count - 1 do
    begin
      if aList[i] <> '' then
        dupList.add(aList[i]);
    end;
  finally
    Result := dupList;
  end;
end;

procedure TForm1.IdSSLIOHandlerSocketOpenSSL1Status(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
  self.Caption :='SocketOpenSSL1Status '+ AStatusText;
  Memo1.Lines.add(AStatusText);
  Memo1.Refresh;
end;

procedure TForm1.IMAPClientStatus(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  self.Caption :='IMAPClientStatus '+ AStatusText;
  Memo1.Lines.add(AStatusText);
  Memo1.Refresh;
end;

function TForm1.ParseXML(xmlFile: tFilename): boolean;
var
  XMLDOMDocument: IXMLDOMDocument;
  XMLDOMNodeList: IXMLDOMNodeList;
  XMLDOMNode: IXMLDOMNode;
  aXMLDOMNode: IXMLDOMNode;
  mailNode: IXMLDOMNode;
  logNode: IXMLDOMNode;
  adoNode: IXMLDOMNode;
  xml: TStringList;
  i: integer;
  s: string;
begin // bodyTextDir  logDir
  xml := TStringList.Create;
  Result := False;
  try
    xml.LoadFromFile(xmlFile);
    XMLDOMDocument := CoDOMDocument.Create;
    try
      try
        XMLDOMDocument.loadXML(xml.Text);
        if (XMLDOMDocument.parseError.ErrorCode <> 0) then
          raise Exception.CreateFmt('Error in Xml Data %s',
            [XMLDOMDocument.parseError]);

        XMLDOMNode := XMLDOMDocument.selectSingleNode('//ticketAcker');
        logNode := XMLDOMDocument.selectSingleNode('//ticketAcker/logdir');

        if XMLDOMNode <> nil then
        begin
          XMLDOMNodeList := XMLDOMNode.selectNodes('.//process');

          if XMLDOMNodeList <> nil then
          begin
            for i := 0 to XMLDOMNodeList.length - 1 do
            begin
              aXMLDOMNode := XMLDOMNodeList.item[i];
              if aXMLDOMNode.attributes.getNamedItem('format').Text = callCenter
              then
              begin
                fbodyTextDir := aXMLDOMNode.attributes.getNamedItem('Ack').Text;

                mailNode := aXMLDOMNode.selectSingleNode('.//mailBox');

                fmailHost := mailNode.attributes.getNamedItem('host').Text;
                fmailPort :=
                  StrToInt(mailNode.attributes.getNamedItem('port').Text);
                fmailUsername := mailNode.attributes.getNamedItem
                  ('username').Text;
                fmailPW := mailNode.attributes.getNamedItem('password').Text;
                Result := true;
              end;
            end;
          end;
        end;
      except
        on e: Exception do
        begin
          LogResult.MethodName := 'ParseXML';
          LogResult.ExcepMsg := e.message;
          LogResult.DataStream := XMLDOMDocument.Text;
          LogResult.LogType := ltError;
          WriteLog(LogResult);
          Result := False;
          exit;
        end;
      end;

    finally
      XMLDOMDocument := nil;
    end;
  finally
    xml.Free;
  end;
end;

function TForm1.ProcessINI: boolean;
var
  AckAttAckINI: TIniFile;
begin
  try
    AckAttAckINI := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
    connectionStr := AckAttAckINI.ReadString('Database', 'ConnectionString', '');

    LogResult.LogType := ltInfo;
    LogResult.MethodName := 'ProcessINI';
    WriteLog(LogResult);

  finally
    AckAttAckINI.Free;
  end;
end;

function TForm1.ConfigDB: boolean;
begin
  if ADOConn.Connected then
    ADOConn.Close;
  try
    with ADOConn do
    begin
      ADOConn.ConnectionString := connectionStr;
      ADOConn.Open();
      Result := ADOConn.Connected;
      LogResult.MethodName := 'ConfigDB';
      LogResult.DataStream := 'ConnectionString: ' + connectionStr;
      LogResult.LogType := ltInfo;
      if ParamStr(3) = 'GUI' then
        Memo1.Lines.add('Connected with ' + ADOConn.ConnectionString);
    end;
    assert(ADOConn.Connected, 'Failed to connect to database '+ ADOConn.ConnectionString);

  except
    on e: Exception do
    begin
      Memo1.Lines.add('Failed to connect to database '+ ADOConn.ConnectionString);
      if e is EAssertionFailed then
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := e.message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end
      else
      begin
        LogResult.MethodName := 'ConfigDB';
        LogResult.ExcepMsg := e.message;
        LogResult.DataStream := ADOConn.ConnectionString;
        LogResult.LogType := ltError;
        WriteLog(LogResult);
      end;
    end;
  end;
end;

function TForm1.ConfigEMail: boolean;
begin
  Result := true;
  with IMAPClient do
  begin
    Host := mailHost;
    Port := mailPort;
    Username := mailUsername;
    Password := mailPW;

    LogResult.MethodName := 'ConfigEMail';
    LogResult.DataStream := 'Host: ' + Host + ' Port: ' + IntToStr(Port) +
      ' UserName: ' + Username + ' Password:' + mailPW;
    LogResult.LogType := ltInfo;
    WriteLog(LogResult);
  end;
end;

procedure TForm1.WriteLog(LogResult: TLogResults);
var
  myFile: TextFile;
  LogName: string;
  Leader: string;
  EntryType: String;
const
  CALL_CENTER_LOG = 'ATTACHER_%s';
  MISS_ACK_LOG = 'MISSING_ACK_%s';
  PRE_Pend = '[yyyy-mm-dd hh:nn:ss] ';

  FILE_EXT = '.TXT';
begin
  Leader := FormatDateTime(PRE_Pend, now);
  if LogResult.LogType <> ltMissing then

    LogName := logDir + '\' + callCenter + '-' + format(CALL_CENTER_LOG,
      [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT
  else
    LogName := logDir + '\' + callCenter + '-' + format(MISS_ACK_LOG,
      [FormatDateTime('yyyy-mm-dd', Date)]) + FILE_EXT;
  if ParamStr(3) = 'GUI' then
    Memo1.Lines.add('Log path ' + LogName);

  // callCenter
  case LogResult.LogType of
    ltError:
      begin
        EntryType := '**************** ERROR ****************';
      end;
    ltInfo:
      begin
        EntryType := '****************  INFO  ****************';
      end;
    ltNotice:
      begin
        EntryType := '**************** NOTICE ****************';
      end;
    ltWarning:
      begin
        EntryType := '**************** WARNING ****************';
      end;
  end;

  if FileExists(LogName) then
  begin
    AssignFile(myFile, LogName);
    Append(myFile);
  end
  else
  begin
    AssignFile(myFile, LogName);
    Rewrite(myFile);
  end;
  WriteLn(myFile, EntryType);
  if LogResult.MethodName <> '' then
  begin
    WriteLn(myFile, Leader + 'Method Name/Line Num : ' + LogResult.MethodName);
    if ParamStr(3) = 'GUI' then
      Memo1.Lines.add(Leader + 'Method Name/Line Num : ' +
        LogResult.MethodName);
  end;
  if LogResult.ExcepMsg <> '' then
  begin
    WriteLn(myFile, Leader + 'Exception : ' + LogResult.ExcepMsg);
    if ParamStr(3) = 'GUI' then
      Memo1.Lines.add(Leader + 'Exception : ' + LogResult.ExcepMsg);
  end;
  if LogResult.Status <> '' then
  begin
    WriteLn(myFile, Leader + 'Status : ' + LogResult.Status);
    if ParamStr(3) = 'GUI' then
      Memo1.Lines.add(Leader + 'Status : ' + LogResult.Status);
  end;
  if LogResult.DataStream <> '' then
  begin
    WriteLn(myFile, Leader + 'Data : ' + LogResult.DataStream);
    if ParamStr(3) = 'GUI' then
      Memo1.Lines.add(Leader + 'Data : ' + LogResult.DataStream);
  end;

  CloseFile(myFile);
  clearLogRecord;
end;

procedure TForm1.clearLogRecord;
begin
  LogResult.MethodName := '';
  LogResult.ExcepMsg := '';
  LogResult.DataStream := '';
  LogResult.Status := '';
end;

function TForm1.LoadParams: boolean;
begin
  try
    fconfigXML := '';
    fcallCenter := '';
    fconfigXML := ParamStr(1);
    fcallCenter := ParamStr(2);
    if fcallCenter = 'SCA6 'then    //QM-495
    RL_CallCenter :='SCA1';        //QM-495

    if fcallCenter = 'NCA2 'then  //QM-495
    RL_CallCenter :='NCA1';     //QM-495

    if fconfigXML = '' then
    begin
      LogResult.MethodName := 'LoadParams';
      LogResult.ExcepMsg := 'Fatal Error - Show Stopper';
      LogResult.DataStream := 'Could not find Config.XML file';
      LogResult.LogType := ltError;
      if ParamStr(3) = 'GUI' then
        Memo1.Lines.add(LogResult.DataStream);
    end;

    if fcallCenter = '' then
    begin
      LogResult.MethodName := 'LoadParams';
      LogResult.ExcepMsg := 'Fatal Error - Show Stopper';
      LogResult.DataStream := 'Callcenter named not passed in.';
      LogResult.LogType := ltError;
      Memo1.Lines.add(LogResult.DataStream);
    end;
  except
    on e: Exception do
    begin
      LogResult.MethodName := 'LoadParams';
      LogResult.ExcepMsg := e.message;
      LogResult.Status := 'Exception thrown';
      LogResult.DataStream := 'Could not process command line parameters';
      LogResult.LogType := ltError;
      if ParamStr(3) = 'GUI' then
        Memo1.Lines.add(LogResult.DataStream);
      Result := False;
      exit;
    end;
  end;

  LogResult.MethodName := 'LoadParams';
  LogResult.Status := 'process callcenter and config.xml';
  LogResult.DataStream := 'callCenter: ' + callCenter + '  configXML: ' +
    configXML;
  LogResult.LogType := ltInfo;
  if ParamStr(3) = 'GUI' then
    Memo1.Lines.add(LogResult.DataStream);
  WriteLog(LogResult);
  Result := true;
  application.ProcessMessages;
end;

procedure TForm1.Memo1Change(Sender: TObject);
begin
  ScrollToLastLine(Memo1);
end;

procedure TForm1.Memo1DblClick(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm1.Memo2Change(Sender: TObject);
begin
  ScrollToLastLine(Memo2);
end;

procedure TForm1.Memo2DblClick(Sender: TObject);
begin
  Memo2.Clear;
end;

function TForm1.GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
  begin
    showMessage(SysErrorMessage(GetLastError));
    RaiseLastOSError;
  end;

  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi,
    // major
    LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
    LongRec(FixedPtr.dwFileVersionLS).Hi, // release
    LongRec(FixedPtr.dwFileVersionLS).Lo]) // build
end;

procedure ScrollToLastLine(Memo: TMemo);
begin
  SendMessage(Memo.Handle, EM_LINESCROLL, 0, Memo.Lines.Count);
end;

end.
