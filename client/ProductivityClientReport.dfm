inherited ProductivityClientReportForm: TProductivityClientReportForm
  Caption = 'ProductivityClientReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 95
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 8
    Width = 130
    Height = 13
    Caption = 'Locate Status Date Range:'
  end
  object Label6: TLabel [2]
    Left = 0
    Top = 122
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  inline DateRange: TOdRangeSelectFrame [3]
    Left = 43
    Top = 27
    Width = 260
    Height = 59
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 15
      Top = 11
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Left = 144
      Top = 11
    end
    inherited DatesLabel: TLabel
      Left = 15
      Top = 37
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 53
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 53
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 165
    end
  end
  object ManagersComboBox: TComboBox
    Left = 96
    Top = 92
    Width = 179
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox
    Left = 96
    Top = 119
    Width = 199
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
end
