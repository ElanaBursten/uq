unit BillableDamagesReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, DBCtrls, OdRangeSelect;

type
  TBillableDamagesReportForm = class(TReportBaseForm)
    ProfitCenter: TComboBox;
    ProfitCenterLabel: TLabel;
    BillingDateRange: TOdRangeSelectFrame;
    Label1: TLabel;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu;

{ TDamageDefaultEstimateReportForm }

procedure TBillableDamagesReportForm.InitReportControls;
begin
  inherited;
  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
  BillingDateRange.SelectDateRange(rsLastWeek);
end;

procedure TBillableDamagesReportForm.ValidateParams;
begin
  inherited;
  SetReportID('BillableDamages');

  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParamDate('BillingFromDate', BillingDateRange.FromDate);
  SetParamDate('BillingToDate', BillingDateRange.ToDate);
end;

end.
