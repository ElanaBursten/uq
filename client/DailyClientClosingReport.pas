unit DailyClientClosingReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect;

type
  TDailyClientLocatesReportForm = class(TReportBaseForm)
    CallCenterLabel: TLabel;
    ClientCodeLabel: TLabel;
    EditClientCode: TEdit;
    CallCenterComboBox: TComboBox;
    LocatesClosed: TComboBox;
    LocatesClosedLabel: TLabel;
    PrintImagesBox: TCheckBox;
    SyncDate: TOdRangeSelectFrame;
    TransmitDate: TOdRangeSelectFrame;
    SyncDateRangeLabel: TLabel;
    TransmitDateRangeLabel: TLabel;
    procedure SyncDateDatesComboBoxChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdExceptions, QMCOnst;

{$R *.dfm}

{ TDailyClientClosingReportForm }

procedure TDailyClientLocatesReportForm.ValidateParams;
begin
  inherited;
  if EditClientCode.Text = '' then begin
    EditClientCode.SetFocus;
    raise EOdEntryRequired.Create('Please select a client.');
  end;
  if (SyncDate.FromDate = 0) and (TransmitDate.FromDate = 0) then
    raise EOdEntryRequired.Create('Please select at least one date range');

  SetReportID('DailyClientClosing');
  SetParamDate('date', SyncDate.FromDate);
  SetParamDate('sync_to_date', SyncDate.ToDate);
  SetParamDate('xmit_from_date', TransmitDate.FromDate);
  SetParamDate('xmit_to_date', TransmitDate.ToDate);
  SetParam('call_center', FCallCenterList.GetCode(CallCenterComboBox.Text));
  SetParam('client_code', EditClientCode.Text);
  SetParamInt('locate_closed', LocatesClosed.ItemIndex);
  SetParamBoolean('images', PrintImagesBox.Checked);
end;

procedure TDailyClientLocatesReportForm.InitReportControls;
var
  i: TOpenClosedCriteria;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  CallCenterComboBox.ItemIndex := 0;
  SyncDate.NoDateRange;
  TransmitDate.NoDateRange;
  for i := Low(TOpenClosedCriteria) to High(TOpenClosedCriteria) do
    LocatesClosed.Items.Add(OpenClosedDescription[i]);
  LocatesClosed.ItemIndex := Ord(ocClosed);
  PrintImagesBox.Checked := False;
end;

procedure TDailyClientLocatesReportForm.SyncDateDatesComboBoxChange(Sender: TObject);
begin
  inherited;
  SyncDate.DatesComboBoxChange(Sender);
end;

end.
