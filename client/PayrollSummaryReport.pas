unit PayrollSummaryReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, ComCtrls, dxCore, cxDateUtils;

type
  TPayrollSummaryReportForm = class(TReportBaseForm)
    PeriodLabel: TLabel;
    ManagerLabel: TLabel;
    ChooseDate: TcxDateEdit;
    PCCombo: TComboBox;
    procedure ChooseDateChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdMiscUtils;

{ TPayrollSummaryReportForm }

procedure TPayrollSummaryReportForm.InitReportControls;
begin
  inherited;
  DM.ReportableTimesheetProfitCenterList(PCCombo.Items);
  ChooseDate.Date := ThisSaturday;
end;

procedure TPayrollSummaryReportForm.ValidateParams;
var
  EndDate: TDateTime;
begin
  inherited;
  RequireComboBox(PCCombo, 'Please select a payroll timesheet number.');
  SetReportID('PayrollSummary');
  SetParam('pc', DM.GetPCFromTimesheetComboText(PCCombo.Text));
  EndDate := ChooseDate.Date;
  SetParamDate('end_date', EndDate);
end;

procedure TPayrollSummaryReportForm.ChooseDateChange(Sender: TObject);
begin
  if ChooseDate.Date <> SaturdayIze(ChooseDate.Date) then
    ChooseDate.Date := SaturdayIze(ChooseDate.Date);
end;

end.

