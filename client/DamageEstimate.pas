unit DamageEstimate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, StdCtrls, ExtCtrls, Grids, DBGrids;

type
  TEstimateFrame = class(TFrame)
    EstimatesGrid: TDBGrid;
    NewEstimatePanel: TPanel;
    EstimateAmountLabel: TLabel;
    EstimateCommentLabel: TLabel;
    NewEstimateButton: TButton;
    EstimateAmount: TEdit;
    EstimateComment: TEdit;
    EstimateSource: TDataSource;
    Panel1: TPanel;
    ViewAllck: TCheckBox;
    Label2: TLabel;
    EstimateCompanyLabel: TLabel;
    AccrualAmountLabel: TLabel;
    AccrualAmount: TLabel;
    procedure NewEstimateButtonClick(Sender: TObject);
    procedure ViewAllckClick(Sender: TObject);
  private
    FEstimateType: Integer;
    FDamageID: Integer;
    FOtherID: Integer;
    FAfterAddEstimate: TNotifyEvent;
    procedure FilterEstimates(const EstimateType: Integer);
    procedure AddEstimate(const EstimateAmount: Double; const EstimateComment: string;
      const EstimateType: Integer);
    procedure ValidateData;
    procedure ViewAllEstimates;
    procedure ConfigureGridColumns;
    procedure SetAccrualAmount;
    function GetCurrentEstimateAmount: Currency;
  public
    procedure RefreshNow;
    procedure Initialize(const EstimateType, DamageID, OtherID: Integer);
    property AfterAddEstimate: TNotifyEvent read FAfterAddEstimate write FAfterAddEstimate;
    property CurrentEstimateAmount: Currency read GetCurrentEstimateAmount;
  end;

implementation

uses OdVclUtils, OdMiscUtils, OdDbUtils, OdExceptions, QMConst,
  DMu, LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDmu;

{$R *.dfm}

procedure TEstimateFrame.FilterEstimates(const EstimateType: Integer);
begin
  Assert(Assigned(EstimateSource.Dataset));
  EstimateSource.DataSet.Filter := 'damage_id=' + IntToStr(FDamageID);
  if (EstimateType > qmestAll) and (not ViewAllck.Checked) then
    EstimateSource.DataSet.Filter := EstimateSource.DataSet.Filter
      + ' and estimate_type=' + IntToStr(EstimateType);

  EstimateSource.DataSet.Filtered := True;
  ConfigureGridColumns;
end;

procedure TEstimateFrame.ViewAllEstimates;
begin
  FilterEstimates(qmestAll);
end;

procedure TEstimateFrame.NewEstimateButtonClick(Sender: TObject);
var
  Est: TDataSet;
begin
  Est := EstimateSource.DataSet;
  Assert(Assigned(Est));

  EstimateAmount.Text := Trim(EstimateAmount.Text);
  if StrToFloatDef(EstimateAmount.Text, MaxInt) = MaxInt then
    raise EOdEntryRequired.Create('Please enter a valid estimate amount');
  if StrToFloatDef(EstimateAmount.Text, 0) = 0 then
    raise EOdEntryRequired.Create('Zero is not allowed for an estimate amount');

  AddEstimate(StrToFloat(EstimateAmount.Text), EstimateComment.Text, FEstimateType);
  EstimateAmount.Clear;
  EstimateComment.Clear;
end;

procedure TEstimateFrame.AddEstimate(const EstimateAmount: Double;
  const EstimateComment: string; const EstimateType: Integer);
var
  Est: TDataSet;
begin
  Est := EstimateSource.DataSet;
  Assert(Assigned(Est));
  Assert(FDamageID > 0);

  Est.Open;
  Est.Insert;
  Est.FieldByName('modified_date').AsDateTime := Now;
  Est.FieldByName('added_date').AsDateTime := RoundSQLServerTimeMS(Now);
  Est.FieldByName('emp_id').AsInteger := DM.EmpID;
  Est.FieldByName('damage_id').AsInteger := FDamageID;
  Est.FieldByName('amount').AsFloat := EstimateAmount;
  Est.FieldByName('comment').AsString := EstimateComment;
  Est.FieldByName('estimate_type').asInteger := EstimateType;
  Est.FieldByName('third_party').AsBoolean := (EstimateType=qmest3rdParty);
  if EstimateType = qmest3rdParty then
    Est.FieldByName('third_party_id').asInteger := FOtherID;
  if EstimateType = qmestLitigation then
    Est.FieldByName('litigation_id').asInteger := FOtherID;
  if EstimateType in [qmest3rdParty, qmestLitigation] then
    Est.FieldByName('company').AsString := EstimateCompanyLabel.Caption;
  try
    // Manual call necessary since the global BeforePost overrides here
    LDamageDM.DamageEstimateBeforePost(Est);
    Est.Post;
    DM.FlushDatabaseBuffers;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddEstimate, '');
    if Assigned(FAfterAddEstimate) then
      FAfterAddEstimate(Self);
  except
    Est.Cancel;
    raise;
  end;
  RefreshNow;
end;

procedure TEstimateFrame.Initialize(const EstimateType, DamageID, OtherID: Integer);
begin
  FEstimateType := EstimateType;
  FDamageID := DamageID;
  if FEstimateType > qmestDamage then
    FOtherID := OtherID
  else
    FOtherID := 0;

  RefreshNow;
end;

procedure TEstimateFrame.ValidateData;
begin
  if (FEstimateType < qmestDamage) or (FEstimateType > qmestLitigation) then
    raise Exception.Create('Invalid estimate type');
end;

procedure TEstimateFrame.ConfigureGridColumns;
var
  i: Integer;
  col: TColumn;
begin
  for i:=0 to EstimatesGrid.Columns.Count-1 do begin
    if ViewAllck.Checked then
      EstimatesGrid.Columns.Items[i].Visible := True
    else begin
      col := EstimatesGrid.Columns.Items[i];
      if SameText('estimate_type_desc', col.FieldName) then
        col.Visible := False;
      if SameText('company', col.FieldName) then
        col.Visible := not (FEstimateType = qmestDamage);
      if SameText('contract', col.FieldName) then
        col.Visible := (FEstimateType = qmest3rdParty);
    end;
  end;
end;

procedure TEstimateFrame.RefreshNow;
var
  EnableAdd: Boolean;
begin
  ValidateData;
  ViewAllCk.Enabled := PermissionsDM.EmployeeHasLitigationRights;
  EnableAdd := (FDamageID > 0) and PermissionsDM.CanI(RightDamagesAddEstimate);
  if EnableAdd and (FEstimateType in [qmestLitigation, qmest3rdParty]) then
    EnableAdd := FOtherID > 0;
  if EnableAdd then
    EnableAdd := Self.Parent.Enabled;

  SetEnabledOnControlAndChildren(NewEstimatePanel, EnableAdd);
  LDamageDM.DamageEstimate.Close;
  LDamageDM.DamageEstimate.Open;
  FilterEstimates(FEstimateType);
  SetAccrualAmount;
end;

procedure TEstimateFrame.ViewAllckClick(Sender: TObject);
begin
  if ViewAllCk.Checked then
    ViewAllEstimates
  else
    FilterEstimates(FEstimateType);
end;

procedure TEstimateFrame.SetAccrualAmount;
var
  Accrual: Double;
begin
  Accrual := LDamageDM.GetDamageAccrualAmount(FDamageID,
    LDamageDM.Damage.FieldByName('invoice_code').AsString);
  AccrualAmount.Caption := FormatFloat('0.00', Accrual);
end;

function TEstimateFrame.GetCurrentEstimateAmount: Currency;
const
  SelectCurrentEst = 'select * from damage_estimate ' +
    'where damage_id = %d and estimate_type = %d and active ' +
    'order by added_date desc top 1';
var
  EstData: TDataSet;
begin
  Result := 0.00;
  if not Assigned(EstimateSource.DataSet) then
    Exit;

  EstData := DM.Engine.OpenQuery(Format(SelectCurrentEst, [FDamageID, FEstimateType]));
  try
    if not EstData.IsEmpty then
      Result := EstData.FieldByName('amount').AsCurrency;
  finally
    FreeAndNil(EstData);
  end;
end;

end.

