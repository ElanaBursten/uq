object frmCallOutDialog: TfrmCallOutDialog
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'CallOut'
  ClientHeight = 173
  ClientWidth = 269
  Color = clMoneyGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblStart: TLabel
    Left = 146
    Top = 55
    Width = 65
    Height = 13
    AutoSize = False
    Caption = 'start'
  end
  object lblStop: TLabel
    Left = 146
    Top = 82
    Width = 65
    Height = 13
    AutoSize = False
    Caption = 'stop'
  end
  object Label1: TLabel
    Left = 34
    Top = 8
    Width = 201
    Height = 32
    Caption = 'NOTICE: You are about to add time to yesterday'#39's timesheet.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 34
    Top = 152
    Width = 200
    Height = 13
    Caption = 'Changes are not visible until after a sync.'
  end
  object coStart: TcxTimeEdit
    Left = 51
    Top = 52
    EditValue = 0d
    Properties.TimeFormat = tfHourMin
    TabOrder = 0
    OnExit = coStartExit
    Width = 89
  end
  object coStop: TcxTimeEdit
    Left = 51
    Top = 79
    EditValue = 0d
    Properties.TimeFormat = tfHourMin
    TabOrder = 1
    Width = 89
  end
  object BitBtn1: TBitBtn
    Left = 48
    Top = 116
    Width = 75
    Height = 25
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 129
    Top = 116
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkCancel
  end
end
