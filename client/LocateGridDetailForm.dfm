object LocateGridDetail: TLocateGridDetail
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'LocateGridDetail'
  ClientHeight = 109
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LocGrid: TcxGrid
    Left = 0
    Top = 0
    Width = 645
    Height = 109
    Align = alClient
    BevelInner = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LocGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = LocSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsView.CellEndEllipsis = True
      OptionsView.GroupByBox = False
    end
    object LocGridLevel1: TcxGridLevel
      GridView = LocGridView
    end
  end
  object LocSource: TDataSource
    AutoEdit = False
    DataSet = LocateSearch
    Left = 408
    Top = 8
  end
  object LocateSearch: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    Exclusive = True
    Left = 376
    Top = 8
  end
end
