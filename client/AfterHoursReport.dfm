inherited AfterHoursReportForm: TAfterHoursReportForm
  Left = 340
  Top = 228
  Caption = 'Productivity After Hours Report'
  ClientHeight = 364
  ClientWidth = 569
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 0
    Top = 8
    Width = 130
    Height = 13
    Caption = 'Locate Status Date Range:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 86
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label6: TLabel [2]
    Left = 0
    Top = 116
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  inline RangeSelect: TOdRangeSelectFrame [3]
    Left = 34
    Top = 22
    Width = 271
    Height = 63
    TabOrder = 0
    inherited ToLabel: TLabel
      Left = 145
    end
    inherited DatesComboBox: TComboBox
      Left = 55
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 55
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 166
      Width = 88
    end
  end
  object ManagersComboBox: TComboBox
    Left = 89
    Top = 85
    Width = 180
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox
    Left = 89
    Top = 113
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
end
