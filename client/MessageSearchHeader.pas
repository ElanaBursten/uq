unit MessageSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls, OdRangeSelect, DB, OdHourGlass,
  OdIsoDates, UQUtils, QMServerLibrary_Intf, DMu, cxGridDBTableView, cxMaskEdit;

type
  TMessageSearchCriteria = class(TSearchCriteria)
    AddedDateLabel: TLabel;
    SentDateRange: TOdRangeSelectFrame;
    Label1: TLabel;
    ShowDateRange: TOdRangeSelectFrame;
    MessageNumber: TEdit;
    Destination: TEdit;
    Subject: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    RecipientName: TEdit;
  private
    ShowingRecipients: Boolean;
    function CreateSearchParams: NVPairList;
  public
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure BeforeItemSelected(ID: Variant); override;
    procedure InitializeSearchCriteria; override;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); override;
  end;

implementation

{$R *.dfm}

{ TMessageSearchCriteria }

procedure TMessageSearchCriteria.DefineColumns;
begin
  inherited;
  ShowingRecipients := Length(RecipientName.Text) > 0;
  with Defs do begin
    SetDefaultTable('message_dest');
    AddColumn('', 0, 'message_id', ftUnknown, '', 0, True, False);
    AddColumn('Message #', 160, 'message_number');
    AddColumn('Sender Name', 125, 'SenderName');  //QM-22    SR
    AddColumn('Destination', 165, 'destination');
    AddColumn('Subject', 150, 'subject');
    AddColumn('Sent Date', 150, 'sent_date', ftDateTime);
    AddColumn('Show Date', 75, 'show_date', ftDateTime);
    AddColumn('Sent', 60, 'sent_to_count', ftInteger, '', 0, False, not ShowingRecipients);
    AddColumn('Ack''ed', 60, 'ack_count', ftInteger, '', 0, False, not ShowingRecipients);
    AddColumn('Recipient', 150, 'recipient_name', ftUnknown, '', 0, False, ShowingRecipients);
    AddColumn('Ack Date', 100, 'ack_date', ftDateTime, '', 0, False, ShowingRecipients);
    if DM.UQState.DeveloperMode then
      AddColumn('Active', 40, 'active', ftBoolean);
  end;
end;

function TMessageSearchCriteria.GetXmlString: string;
var
  Cursor: IInterface;
  Params: NVPairList;
begin
  Cursor := ShowHourGlass;
  Params := CreateSearchParams;
  try
    DM.CallingServiceName('MessageSearch');
    Result := DM.Engine.Service.MessageSearch(DM.Engine.SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

function TMessageSearchCriteria.CreateSearchParams: NVPairList;
begin
  Result := NVPairList.Create;
  AddParam(Result, 'message_number', MessageNumber.Text);
  AddParam(Result, 'destination', Destination.Text);
  AddParam(Result, 'subject', Subject.Text);
  AddParam(Result, 'recipient_name', RecipientName.Text);
  AddParam(Result, 'show_date_start', IsoDateTimeToStr(ShowDateRange.FromDate));
  AddParam(Result, 'show_date_end', IsoDateTimeToStr(ShowDateRange.ToDate));
  AddParam(Result, 'sent_date_start', IsoDateTimeToStr(SentDateRange.FromDate));
  AddParam(Result, 'sent_date_end', IsoDateTimeToStr(SentDateRange.ToDate));
  AddParam(Result, 'show_inactive', Ord(DM.UQState.DeveloperMode));
end;

procedure TMessageSearchCriteria.CustomizeGrid(GridView: TcxGridDBTableView);
var
  ColSentToCount: TcxGridDBColumn;
  ColAckCount: TcxGridDBColumn;
begin
  ColSentToCount := GridView.GetColumnByFieldName('sent_to_count');
  if Assigned(ColSentToCount) then
  begin
    ColSentToCount.HeaderAlignmentHorz := taRightJustify;
    ColSentToCount.PropertiesClass := TcxMaskEditProperties;
    ColSentToCount.Properties.Alignment.Horz := taRightJustify;
  end;
  ColAckCount := GridView.GetColumnByFieldName('ack_count');
  if Assigned(ColAckCount) then
  begin
    ColAckCount.HeaderAlignmentHorz := taRightJustify;
    ColAckCount.PropertiesClass := TcxMaskEditProperties;
    ColAckCount.Properties.Alignment.Horz := taRightJustify;
  end;
end;

procedure TMessageSearchCriteria.BeforeItemSelected(ID: Variant);
begin
  inherited;
  if not ShowingRecipients then
    DM.Engine.GetMessageRecipFromServer(ID)
  else
    Abort; // nothing more to drill down to
end;

procedure TMessageSearchCriteria.InitializeSearchCriteria;
begin
  inherited;
  ShowDateRange.SelectDateRange(rsThisWeek);
end;

end.
