unit TicketSearchHeader;

interface                                                              

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, ExtCtrls, StdCtrls, OdRangeSelect, 
  Buttons, cxGrid, cxGridTableView, cxGridCustomTableView, cxGridDBTableView,
  ComCtrls, cxButtonEdit, cxDropDownEdit, cxEdit, ActnList, LocateGridDetailForm,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxTextEdit, cxMaskEdit, cxStyles, cxClasses, cxCustomData, cxFilter; //QM-376
CONST
  UM_RESET_CLIENT = WM_USER + $109;
  DebugExpandedHeight = 275;
  RegularHeight = 250;
type
  TTicketSearchCriteria = class(TSearchCriteria)
    TicketNumber: TEdit;
    City: TEdit;
    TicketType: TEdit;
    WorkDoneFor: TEdit;
    Company: TEdit;
    DueDateFrame: TOdRangeSelectFrame;
    County: TEdit;
    WorkType: TEdit;
    RecvDateFrame: TOdRangeSelectFrame;
    CallCenter: TEdit;
    Attachments: TComboBox;
    ApproveButton: TButton;
    ManualTickets: TComboBox;
    Priority: TComboBox;
    LocatePanel: TPanel;
    SearchPanel: TPanel;
    Label12: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    AttachmentsLabel: TLabel;
    CountyLabel: TLabel;
    WorkTypeLabel: TLabel;
    cbUtilityCo: TcxComboBox;
    ClientCode: TcxComboBox;
    cxStyleRepository1: TcxStyleRepository;
    cxInActiveStyle: TcxStyle;
    cxActiveStyle: TcxStyle;
    State: TEdit;
    Street: TEdit;
    edtTicketID: TEdit;
    lblTicketID: TLabel;
    edtLocateID: TEdit;
    lblLocateID: TLabel;  //QMANTWO-422
    procedure ApproveButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbUtilityCoPropertiesCloseUp(Sender: TObject);
    procedure cbUtilityCoPropertiesDrawItem(AControl: TcxCustomComboBox;
      ACanvas: TcxCanvas; AIndex: Integer; const ARect: TRect;
      AState: TOwnerDrawState);
    procedure FormCreate(Sender: TObject);
    procedure DueDateFrameToDateEditPropertiesCloseUp(Sender: TObject); //QM-84
    procedure DueDateFrameFromDateEditPropertiesCloseUp(Sender: TObject);  //QM-84
    procedure RecvDateFrameToDateEditPropertiesCloseUp(Sender: TObject);   //QM-84
    procedure RecvDateFrameFromDateEditPropertiesCloseUp(Sender: TObject); //QM-84
    procedure ClientCodePropertiesDrawItem(AControl: TcxCustomComboBox;
      ACanvas: TcxCanvas; AIndex: Integer; const ARect: TRect;
      AState: TOwnerDrawState);
  private
    FTicketID: Integer;
    FLocateGrid: TLocateGridDetail;  //QMANTWO-422
    FRightSearchTicketsLimitation: string;
    ReferenceCache: TStringList;
    UtilityCoInt:integer;  //QMANTWO-810
    procedure ColWorkPriorityGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure ColWorkPriorityGetFilterValues(Sender: TcxCustomGridTableItem; AValueList: TcxDataFilterValueList);
    procedure SetClientLookup;
    procedure SetUtilCoLookup;
    procedure CleanUpcbUtilityCoObjects;
  public
    LocateCol: TcxGridDBColumn;
    property LocateGrid: TLocateGridDetail read fLocateGrid write fLocateGrid;   //QMANTWO-422
    procedure GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow); override;
    function GetXmlString: string; override;
    procedure BeforeItemSelected(ID: Variant); override;
    procedure DefineColumns; override;
    procedure Showing; override;
    procedure LogSearchResultForDebugging(Data: string);
    procedure BeforeItemPrint(ID: Variant); override;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); override;
    procedure InitializeSearchCriteria; override;
    procedure UpdateRecordCount(Num, MaxNum: Integer); override;
  end;

  TUtilityCoObj = class(TObject)    //QMANTWO-810
    private
    Active_Ind:Boolean;
    Ref_ID:integer;
  end;

  TClientObj = class(TObject)  //QM-82
    private
    Active: Boolean;
    ref_id: Integer;
  end;

implementation

{$R *.dfm}

uses DB, DMu, OdExceptions, QMConst, QMServerLibrary_Intf, OdHourGlass,
  OdVclUtils, LocalPermissionsDMu, LocalEmployeeDMu;

{ TTicketSearchCriteria }

function TTicketSearchCriteria.GetXmlString: string;
var
  Cursor: IInterface;
  PriorityString: String;
  DebugID: integer;
  NumStr: string;
begin
  Cursor := ShowHourGlass;
  if (Trim(TicketNumber.Text) = '') and RecvDateFrame.IsEmpty then
    raise EOdCriteriaRequiredException.Create('You must specify a received-date range unless searching by ticket number.');

  if Priority.Text <> TicketPriorityAny then
    PriorityString := DM.GetRefCodeForDisplay('tkpriority', Priority.Text);

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeFindTicket, '');

  NumStr := Trim(edtTicketID.Text);
  if (NumStr <> '') then begin   //QM-305 EB Add Debug Ticket ID Search
    try
      If (TryStrToInt(NumStr, DebugID)) then begin
//      DebugID := StrToInt(Trim(edtTicketID.Text));
        if DebugID > 0 then begin
          DM.CallingServiceName('DebugTicketIDSearch');
          Result := DM.Engine.Service.DebugTicketIDSearch(DM.Engine.SecurityInfo, DebugID);
        end
      end
      else begin
        ShowMessage('Invalid Ticket ID (should be numeric)');
        Exit;
      end;
    except
      EodCriteriaRequiredException.Create('Ticket ID is not valid.');
    end;
  end

  else if (Trim(edtLocateID.Text)) <> '' then begin   //QM-305 EB Add Debug Ticket ID Search
    NumStr := Trim(edtLocateID.Text);
    Try
      if (TryStrToInt(NumStr, DebugID)) then begin
        if DebugID > 0 then begin
          DM.CallingServiceName('DebugLocateIDSearch');
          Result := DM.Engine.Service.DebugLocateIDSearch(DM.Engine.SecurityInfo, DebugID);
        end;
      end
      else begin
        ShowMessage('Invalid Locate ID (must be numeric)');
        Exit;
      end;
    except
      EodCriteriaRequiredException.Create('Locate ID is not valid.');
    end;
  end

  else begin
    DM.CallingServiceName('TicketSearch6');
    Result := DM.Engine.Service.TicketSearch6(
      DM.Engine.SecurityInfo,
      Trim(TicketNumber.Text),
      Street.Text,
      City.Text,
      State.Text,
      County.Text,
      WorkType.Text,
      TicketType.Text,
      WorkDoneFor.Text,
      Company.Text,
      DueDateFrame.FromDate,
      DueDateFrame.ToDate,
      RecvDateFrame.FromDate,
      RecvDateFrame.ToDate,
      CallCenter.Text,
      ClientCode.Text,
      GetComboObjectInteger(Attachments),
      GetComboObjectInteger(ManualTickets),
      PriorityString,
      UtilityCoInt        //QMANTWO-810
      );
  end;

  if DM.UQState.DeveloperMode then
    LogSearchResultForDebugging(Result);
end;

procedure TTicketSearchCriteria.CleanUpcbUtilityCoObjects;   //QMANTWO-810
var
  I: integer;
begin
  for I := cbUtilityCo.Properties.Items.Count - 1 downto 0 do
    cbUtilityCo.Properties.Items.Objects[I].Free;
  for I:=ClientCode.Properties.Items.Count-1 downto 0 do   //QM-82
    ClientCode.Properties.Items.Objects[I].Free;
end;

procedure TTicketSearchCriteria.ClientCodePropertiesDrawItem(
  AControl: TcxCustomComboBox; ACanvas: TcxCanvas; AIndex: Integer;
  const ARect: TRect; AState: TOwnerDrawState);
begin
  inherited;
  if AIndex <> 0 then
  begin
    if TClientObj(AControl.Properties.Items.objects[AIndex]).Active = False then
    begin
      ACanvas.Brush.Color := clLtGray;
      ACanvas.FillRect(ARect);
    end;
    ACanvas.TextOut(ARect.Left, ARect.Top, AControl.Properties.Items[AIndex]);
  end;

end;

procedure TTicketSearchCriteria.BeforeItemSelected(ID: Variant);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  DM.UpdateTicketInCache(ID);
end;

procedure TTicketSearchCriteria.cbUtilityCoPropertiesCloseUp(Sender: TObject);
var       //QMANTWO-810
  IocCode:integer;
  ocCode:string;
begin
  inherited;
  UtilityCoInt:= 0;
  if cbUtilityCo.ItemIndex > 0 then  
  UtilityCoInt:= TUtilityCoObj(cbUtilityCo.Properties.Items.Objects[cbUtilityCo.ItemIndex]).ref_id;
  if UtilityCoInt >0 then
  begin
    IocCode := clientCode.ItemIndex;
    if IocCode>-1 then
      ocCode := clientCode.Properties.Items[IocCode];
    dm.qryClient.Filter := 'Ref_id=' + IntToStr(UtilityCoInt);
    dm.qryClient.Filtered := True;
    SetClientLookup;
    if IocCode > -1 then
    begin
      clientCode.ItemIndex:=clientCode.Properties.Items.IndexOf(ocCode);
    end;
  end
  else
  begin
    dm.qryClient.Filter := '';
    dm.qryClient.Filtered := False;
    SetClientLookup;
  end;
end;

procedure TTicketSearchCriteria.cbUtilityCoPropertiesDrawItem(
  AControl: TcxCustomComboBox; ACanvas: TcxCanvas; AIndex: Integer;
  const ARect: TRect; AState: TOwnerDrawState);   //QMANTWO-810
begin
  inherited;
  if AIndex <> 0 then
  begin
    if TUtilityCoObj(AControl.Properties.Items.objects[AIndex]).Active_Ind = False then
    begin
      ACanvas.Brush.Color := clLtGray;
      ACanvas.FillRect(ARect);
    end;
    ACanvas.TextOut(ARect.Left, ARect.Top, AControl.Properties.Items[AIndex]);
  end;
end;

procedure TTicketSearchCriteria.DefineColumns;
begin
  inherited;
  Assert(Assigned(Defs));
  with Defs do begin
    SetDefaultTable('ticket');
    AddColumn('Ticket ID', 0, 'ticket_id', ftUnknown, '', 0, True, False);
    AddColumn('Type', 75, 'ticket_type');
    AddColumn('Ticket #', 90, 'ticket_number');
    AddColumn('Revision', 45, 'revision'); //QMANTWO-605
    AddColumn('Due Date', 85, 'due_date');
    AddColumn('State', 40, 'work_state');
    AddColumn('City', 100, 'work_city');
    AddColumn('Number', 45, 'work_address_number');
    AddColumn('Street', 120, 'work_address_street');
    AddColumn('Work Done For', 110, 'company');
    AddColumn('Company', 110, 'con_name');
    AddColumn('County', 60, 'work_county');
    AddColumn('Work Type', 75, 'work_type');
    AddColumn('Kind', 60, 'kind');
    AddColumn('Attachments', 60, 'attachments');
    AddColumn('Status', 65, 'status');
    AddColumn('Priority', 40, 'work_priority');
    AddColumn('Followup Type', 80, 'followup_type');
    if DM.UQState.DeveloperMode then
      AddColumn('Parent Tkt ID', 80, 'parent_ticket_id');
  end;

end;

procedure TTicketSearchCriteria.DueDateFrameFromDateEditPropertiesCloseUp(
  Sender: TObject);
begin   //From always has to be less or equal to To
  inherited;
  If DueDateFrame.FromDateEdit.Date > DueDateFrame.ToDateEdit.Date then   //QM-84 SR
  DueDateFrame.ToDateEdit.Date:= DueDateFrame.FromDateEdit.Date;
end;

procedure TTicketSearchCriteria.DueDateFrameToDateEditPropertiesCloseUp(
  Sender: TObject);
begin   //From always has to be less or equal to To
  inherited;
  If DueDateFrame.FromDateEdit.Date > DueDateFrame.ToDateEdit.Date  then   //QM-84 SR
  DueDateFrame.FromDateEdit.Date := DueDateFrame.ToDateEdit.Date;
end;

procedure TTicketSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  FRightSearchTicketsLimitation := PermissionsDM.GetLimitation(RightSearchTickets);
  ApproveButton.Visible := PermissionsDM.CanI(RightTicketsApproveManual);
  PrintButton.Visible := PrintButton.Visible and DM.CanIPrintTickets;

  if not Assigned(ReferenceCache) then
    ReferenceCache := TStringList.Create
  else
    ReferenceCache.Clear;

  try
    DM.Reference.Filter := 'type = ''tkpriority''';
    DM.Reference.Filtered := True;
    DM.Reference.First;
    while not DM.Reference.EOF do begin
      ReferenceCache.Add(DM.Reference.FieldByName('sortby').AsString + '=' + DM.Reference.FieldByName('description').AsString);
      DM.Reference.Next;
    end;
  finally
    DM.Reference.Filter := '';
    DM.Reference.Filtered := False;
  end;

  DM.GetAttachmentCriteria(Attachments.Items);
  ManualTickets.Items.Clear;
  ManualTickets.Items.AddObject('', TObject(Ord(mtNone)));
  ManualTickets.Items.AddObject('Manual Only', TObject(Ord(mtManual)));
  ManualTickets.Items.AddObject('Needs Approval', TObject(Ord(mtNeedsApproval)));
  ManualTickets.Items.AddObject('Has Approval', TObject(Ord(mtApproved)));
  DM.GetRefDisplayList('tkpriority', Priority.Items);
  Priority.Items.Add(TicketPriorityAny);
  RestoreCriteria;
end;

procedure TTicketSearchCriteria.SetUtilCoLookup;  //QMANTWO-810
var
  MyObject:TUtilityCoObj;
begin
  cbUtilityCo.Properties.Items.clear;
  cbUtilityCo.Properties.Items.AddObject('', TObject(Ord(mtNone)));  //QMANTWO-810
  with dm.qryUtilityCo do
  begin
    try
      Open;
      while not(Eof) do
      begin
        MyObject:=TUtilityCoObj.Create;
        MyObject.ref_id:= FieldByName('ref_id').AsInteger;
        MyObject.Active_Ind:= FieldByName('Active_ind').AsBoolean;
        cbUtilityCo.Properties.Items.AddObject(FieldByName('description').asString,  MyObject);
        next;
      end;
    finally
      close;
    end;
  end;
end;

procedure TTicketSearchCriteria.SetClientLookup;  //QMANTWO-810  82
var
  MyObject:TClientObj;   //QM-82
begin
  ClientCode.Properties.Items.clear;  //QMANTWO-810
  with dm.qryClient do
  begin
    try
      close;
      Open;
      First;
      while not(Eof) do
      begin
        MyObject:=TClientObj.Create;
        MyObject.ref_id:= FieldByName('ref_id').AsInteger;
        MyObject.Active:= FieldByName('Active').AsBoolean;
        ClientCode.Properties.Items.AddObject(FieldByName('oc_code').asString,  MyObject);  //QM-82
        next;
      end;
    finally
      close;
    end;
  end;

end;

procedure TTicketSearchCriteria.UpdateRecordCount(Num, MaxNum: Integer);
begin
  inherited;  {Run parent method as default}
end;

procedure TTicketSearchCriteria.LogSearchResultForDebugging(Data: string);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create('rawdump.xml', fmCreate);
  try
    FS.Write(Data[1], Length(Data));
  finally
    FreeAndNil(FS);
  end;
end;

procedure TTicketSearchCriteria.RecvDateFrameFromDateEditPropertiesCloseUp(
  Sender: TObject);
begin     //From always has to be less or equal to To
  inherited;
  If RecvDateFrame.FromDateEdit.Date > RecvDateFrame.ToDateEdit.Date then   //SR
  RecvDateFrame.ToDateEdit.Date:= RecvDateFrame.FromDateEdit.Date;
end;

procedure TTicketSearchCriteria.RecvDateFrameToDateEditPropertiesCloseUp(
  Sender: TObject);
begin     //From always has to be less or equal to To
  inherited;
  If RecvDateFrame.FromDateEdit.Date > RecvDateFrame.ToDateEdit.Date  then   //SR
  RecvDateFrame.FromDateEdit.Date := RecvDateFrame.ToDateEdit.Date;
end;

procedure TTicketSearchCriteria.GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow);
var
  Col: TcxGridDBColumn;
  Status: string;
begin
  inherited;
  if Row = nil then begin
    FTicketID := -1;
    ApproveButton.Enabled := False;
    PrintButton.Enabled := False;
  end
  else begin
    Col := GridView.GetColumnByFieldName('status');
    Status := VarToStr(Row.Values[Col.Index]);
    ApproveButton.Enabled := (Status = TicketStatusManual);
    PrintButton.Enabled := True;

    Col := GridView.GetColumnByFieldName('ticket_id');
    FTicketID := Row.Values[Col.Index];
  end;
end;

procedure TTicketSearchCriteria.ApproveButtonClick(Sender: TObject);
begin
  inherited;
  Assert(FTicketID > 0);
  DM.SetTicketAcknowledged(FTicketID);
  ApproveButton.Enabled := False;
end;

procedure TTicketSearchCriteria.BeforeItemPrint(ID: Variant);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  DM.UpdateTicketInCache(ID);
end;

procedure TTicketSearchCriteria.ColWorkPriorityGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := ReferenceCache.Values[AText];
end;

procedure TTicketSearchCriteria.ColWorkPriorityGetFilterValues(
  Sender: TcxCustomGridTableItem; AValueList: TcxDataFilterValueList);     //QM-376 EB
var
  i, j : integer;
  PriorityList: TStringList;
  ACodeString: string;
  ASortCode: integer;
  ASortCodeLIst: TstringList;
begin
  PriorityList := TStringList.Create;
  ASortCodeList:= TStringList.Create;
  ASortCodeList.Sorted := True;
  try
    DM.GetWorkPriorityRefList(PriorityList);

    for j := 0 to AValueList.Count - 1 do
      ASortCodeList.Add(VarToStr(AValueList[j].Value));

    AValueList.Clear;
    j := -1;
    for i := 0 to PriorityList.Count - 1 do begin
      ACodeString := PriorityList.Names[i];
      If (TryStrToInt(PriorityList.ValueFromIndex[i], ASortCode)) then begin
        if ASortCodeList.Find(IntToStr(ASortCode), j) then
          AValueList.Add(fviValue, ASortCode, ACodeString, False);
      end;
    end;
  finally
    FreeAndNil(PriorityList);
    FreeAndNil(ASortCodeList);
  end;
end;

procedure TTicketSearchCriteria.FormCreate(Sender: TObject);
var
  ShowIDSearchFields : boolean;
begin
  inherited;
  SetUtilCoLookup;  //QMANTWO-810   QM-32

  if DM.UQState.DeveloperMode then begin
    ShowIDSearchFields := True;  //QM-305 EB
    Self.Height := DebugExpandedHeight;

  end
  else begin
    ShowIDSearchFields := False;
    Self.Height := RegularHeight;
  end;

  lblTicketID.Visible := ShowIDSearchFields;
  lblLocateID.Visible := ShowIDSearchFields;
  edtTicketID.Visible := ShowIDSearchFields;
  edtLocateID.Visible := ShowIDSearchFields;
end;

procedure TTicketSearchCriteria.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(ReferenceCache);
  FreeAndNil(fLocateGrid);  //QMANTWO-422
  CleanUpcbUtilityCoObjects;   //QMANTWO-810
end;

procedure TTicketSearchCriteria.CustomizeGrid(GridView: TcxGridDBTableView);
var
  Col: TcxGridDBColumn;
begin
  Col := GridView.GetColumnByFieldName('work_priority');
  if Assigned(Col) then
    Col.OnGetDisplayText := ColWorkPriorityGetDisplayText;
    Col.Options.SortByDisplayText := isbtOn;
    Col.OnGetFilterValues := ColWorkPriorityGetFilterValues;
//    GridView.Filter.OnGetValueList
  If not Assigned(fLocateGrid) then  //QMANTWO-422
    fLocateGrid := TLocateGridDetail.CreateForGrid(Self, Self.LocatePanel);
end;



procedure TTicketSearchCriteria.InitializeSearchCriteria;
var
  P: Integer;
begin
  inherited;
  RecvDateFrame.SelectDateRange(rsThisWeek);
  Attachments.ItemIndex := 0;
  ManualTickets.ItemIndex := 0;
  Priority.ItemIndex := Priority.Items.Count - 1;

  cbUtilityCo.ItemIndex := -1;  //QM-83  SR
  cbUtilityCoPropertiesCloseUp(nil);  //QM-83  SR

  CallCenter.Enabled := True;
  ClientCode.Enabled := True;
  if FRightSearchTicketsLimitation <> '' then begin
    P := Pos('/', FRightSearchTicketsLimitation);
    if P > 0 then begin
      CallCenter.Text := Copy(FRightSearchTicketsLimitation, 1, P - 1);
      ClientCode.Text := Copy(FRightSearchTicketsLimitation, P + 1, 20);
      ClientCode.Enabled := False;
    end else
      CallCenter.Text := FRightSearchTicketsLimitation;
    CallCenter.Enabled := False;
  end;
  edtTicketID.Text := '';
  edtLocateID.Text := '';
end;


end.

