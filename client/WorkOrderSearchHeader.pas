unit WorkOrderSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, ExtCtrls, StdCtrls, OdRangeSelect, 
  Buttons, cxGrid, cxGridTableView, cxGridCustomTableView, cxGridDBTableView, cxMaskedit;

type
  TWorkOrderSearchCriteria = class(TSearchCriteria)
    TicketNumber: TEdit;
    Street: TEdit;
    City: TEdit;
    State: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ClientName: TEdit;
    ClientNameLabel: TLabel;
    Company: TEdit;
    Label8: TLabel;
    DueDateFrame: TOdRangeSelectFrame;
    County: TEdit;
    CountyLabel: TLabel;
    TransmitDateFrame: TOdRangeSelectFrame;
    WOSourceLabel: TLabel;
    WOSource: TEdit;
    Label11: TLabel;
    Kind: TComboBox;
    UQDamageIDLabel: TLabel;
    WorkOrderNumber: TEdit;
    TicketsLabel: TLabel;
    Tickets: TComboBox;
    procedure FormDestroy(Sender: TObject);
  private
    FWorkOrderID: Integer;
    ReferenceCache: TStringList;
  public
    procedure GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow); override;
    function GetXmlString: string; override;
    procedure BeforeItemSelected(ID: Variant); override;
    procedure DefineColumns; override;
    procedure Showing; override;
    procedure LogSearchResultForDebugging(Data: string);
    procedure BeforeItemPrint(ID: Variant); override;
    procedure InitializeSearchCriteria; override;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); override;
  end;

implementation

{$R *.dfm}

uses DB, DMu, OdExceptions, QMConst, QMServerLibrary_Intf, OdHourGlass,
  OdVclUtils, LocalEmployeeDMu;

{ TWorkOrderSearchCriteria }

function TWorkOrderSearchCriteria.GetXmlString: string;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;

  if ((Trim(WorkOrderNumber.Text) = '') and (Trim(TicketNumber.Text) = '')) and TransmitDateFrame.IsEmpty then
    raise EOdCriteriaRequiredException.Create('You must specify a transmit date range unless searching by work order or ticket number.');

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeFindWorkOrder, '');

  DM.CallingServiceName('WorkOrderSearch');
  Result := DM.Engine.Service.WorkOrderSearch(
    DM.Engine.SecurityInfo,
      TicketNumber.Text,
      WorkOrderNumber.Text,
      Street.Text,
      City.Text,
      State.Text,
      County.Text,
      WOSource.Text,
      ClientName.Text,
      Company.Text,
      DueDateFrame.FromDate,
      DueDateFrame.ToDate,
      TransmitDateFrame.FromDate,
      TransmitDateFrame.ToDate,
      Kind.Text,   //TODO - probably we should put values in ref table for DONE, NORMAL, PRIORITY rather than hard-coding in control. Maybe type could be kind and we could reuse for ticket, etc if they are the same?
      GetComboObjectInteger(Tickets)
    );

  if DM.UQState.DeveloperMode then
    LogSearchResultForDebugging(Result);
end;

procedure TWorkOrderSearchCriteria.BeforeItemSelected(ID: Variant);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  DM.UpdateWorkOrderInCache(ID);
end;

procedure TWorkOrderSearchCriteria.CustomizeGrid(GridView: TcxGridDBTableView);
var
  ColTickets : TcxGridDBColumn;
begin
  ColTickets := GridView.GetColumnByFieldName('tickets');
  if Assigned(ColTickets) then
  begin
    ColTickets.HeaderAlignmentHorz := taRightJustify;
    ColTickets.PropertiesClass := TcxMaskEditProperties;
    ColTickets.Properties.Alignment.Horz := taRightJustify;
  end;
end;

procedure TWorkOrderSearchCriteria.DefineColumns;
begin
  inherited;
  Assert(Assigned(Defs));
  with Defs do begin
    SetDefaultTable('work_order');
    AddColumn('Work Order ID', 0, 'wo_id', ftUnknown, '', 0, True, False);
    AddColumn('Work Order #', 110, 'wo_number');
//todo implement - leave out for ticket number for now; will need to revisit ConvertDataSetToXML which culls out duplicate wo_id's (so only one ticket number is shown for a work order when more exist)
//  AddColumn('Ticket Number #', 110, 'ticket_number');
    AddColumn('Number', 45, 'work_address_number');
    AddColumn('Street', 120, 'work_address_street');
    AddColumn('City', 100, 'work_city');
    AddColumn('State', 40, 'work_state');
    AddColumn('County', 60, 'work_county');
    AddColumn('WO Source', 100, 'wo_source');
    AddColumn('Client name', 100, 'client_name');
    AddColumn('Company', 110, 'caller_name');
    AddColumn('Due Date', 85, 'due_date');
    AddColumn('Transmit Date', 85, 'transmit_date');
    AddColumn('Kind', 60, 'kind');
    AddColumn('Status', 65, 'status');
    AddColumn('Tickets', 60, 'tickets');
  end;
end;

procedure TWorkOrderSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  PrintButton.Visible := False;
//todo implement  PrintButton.Visible := PrintButton.Visible and DM.CanIPrintTickets;}
  DM.GetWOTicketCriteria(Tickets.Items);
  RestoreCriteria;
end;

procedure TWorkOrderSearchCriteria.LogSearchResultForDebugging(Data: string);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create('rawdump.xml', fmCreate);
  try
    FS.Write(Data[1], Length(Data));
  finally
    FreeAndNil(FS);
  end;
end;

procedure TWorkOrderSearchCriteria.GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow);
var
  Col: TcxGridDBColumn;
  Status: string;
begin
  inherited;
  if Row = nil then begin
    FWorkOrderID := -1;
//todo    PrintButton.Enabled := False;
  end
  else begin
    Col := GridView.GetColumnByFieldName('status');
    Status := VarToStr(Row.Values[Col.Index]);
//todo    PrintButton.Enabled := True;

    Col := GridView.GetColumnByFieldName('wo_id');
    FWorkOrderID := Row.Values[Col.Index];
  end;
end;

procedure TWorkOrderSearchCriteria.BeforeItemPrint(ID: Variant);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  DM.UpdateWorkOrderInCache(ID);
end;

procedure TWorkOrderSearchCriteria.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(ReferenceCache);
end;

procedure TWorkOrderSearchCriteria.InitializeSearchCriteria;
begin
  inherited;
  Tickets.ItemIndex := 0;
  TransmitDateFrame.SelectDateRange(rsThisWeek);
end;

end.


