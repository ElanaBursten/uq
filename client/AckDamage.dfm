object AckDamageDialog: TAckDamageDialog
  Left = 298
  Top = 220
  BorderStyle = bsDialog
  Caption = 'Acknowledge Damage Ticket'
  ClientHeight = 168
  ClientWidth = 383
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 365
    Height = 113
    Shape = bsFrame
  end
  object InvestigatorLabel: TLabel
    Left = 27
    Top = 84
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Damage Investigator'
  end
  object Label1: TLabel
    Left = 86
    Top = 60
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager'
  end
  object OKButton: TButton
    Left = 110
    Top = 133
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 3
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 198
    Top = 133
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object CreateDamageCheckbox: TCheckBox
    Left = 26
    Top = 24
    Width = 169
    Height = 17
    Caption = 'Create damage investigation'
    TabOrder = 0
    OnClick = CreateDamageCheckboxClick
  end
  object ManagerComboBox: TComboBox
    Left = 144
    Top = 56
    Width = 217
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnChange = ManagerComboBoxChange
  end
  object EmployeeComboBox: TComboBox
    Left = 144
    Top = 80
    Width = 217
    Height = 21
    ItemHeight = 13
    TabOrder = 2
  end
end
