unit DigAlertNote;  //qm-578 sr
{QM-578
Special Note for BG&E Hi Profile/Critical facilities  }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TfrmDigAlertNoteDialog = class(TForm)
    Button1: TButton;
    BGE_NOTE: TRichEdit;
    btnCancel: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure storeNote(alertNote:string);
  end;

var
  frmDigAlertNoteDialog: TfrmDigAlertNoteDialog;

implementation
uses LocateStatusQuestions;

{$R *.dfm}

procedure TfrmDigAlertNoteDialog.Button1Click(Sender: TObject);
begin
  storeNote(BGE_NOTE.text);
end;

procedure TfrmDigAlertNoteDialog.storeNote(alertNote: string);
begin
  LocateStatusQuestions.DigAlertNoteTxt :=alertNote;
end;

end.
