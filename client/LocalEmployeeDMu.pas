unit LocalEmployeeDMu;

interface

uses
  SysUtils, Classes, DB, dbisamtb,
  DMu, SyncEngineU, QMConst, OdMiscUtils, StdCtrls, OdIsoDates;

type
  TEmpWorkType = record  {QM-294  EB Using this so that we don't keep running queries in Tree}
    TomorrowBucketRefID: integer;
  end;

  TFirstTaskReminder = class(Tobject)  //QM-494 First Task Reminder
    fEmpID: integer;
    fReminderStr: string;
    fReminderDate: TDatetime;  {Today + Reminder time}
    fLastChecked: TDateTime;
    fUseIniOnly: boolean;
    fEstStartDate: TDateTime;
    fRequired: boolean;
    fShown: boolean;
    fDebug: boolean;
    public
     Constructor Create(pEmpID: integer; HitINIOnly: Boolean);
     Destructor Destroy;
     function Remindercheck(HitIniOnly: Boolean): boolean;
     function HasReminder: boolean;
     function NagFlag: boolean;
     function TimeRemaining: TDateTime;
     function HasEstStartDate: boolean;
     property EmpID: integer read fEmpID;
     property ReminderStr: string read fReminderStr;
     property LastChecked: TDateTime read fLastChecked;
     property Shown: boolean read fShown write fShown;
     property ReminderDate: TDateTime read fReminderDate;
     property EstStartDate: TDateTime read fEstStartDate write fEstStartDate;
     property Required: boolean read fRequired write fRequired;
     property Debug: boolean read fDebug;
  end;

  TEmployeeDM = class(TDataModule)
    EmpHier: TDBISAMTable;
    ManagersUnderEmp: TDBISAMQuery;
    GetEmployeeQuery: TDBISAMQuery;
    GetManagerID: TDBISAMQuery;
    EmployeeLookup: TDBISAMTable;
    Employees: TDBISAMQuery;
    ManagersWithInactiveEmps: TDBISAMQuery;
    UserLookup: TDBISAMQuery;
    EmpTypes: TDBISAMQuery;
    ActivityLookup: TDBISAMQuery;
    EmpIsManagerQuery: TDBISAMQuery;
    FirstTaskLookup: TDBISAMQuery;
    qryEmployeeBalance: TDBISAMQuery;
    qryAbandonedCallOut: TDBISAMQuery;
  private
    fEmpWorkType: TEmpWorkType;
    FEffectivePCCache: TStringList;
    procedure EmployeeNestHandler(const ID, Left, Right, Depth, Tag: Integer);
    procedure RefreshEffectivePCCache;
    function IsManagersWithInactiveEmps(EmpID: Integer): Boolean;
  public
  //qm-579
    InsertDate        :TDateTime;
    EmploymentStatus  :string;
    FirstName         :string;
    LastName          :string;
    SickOption        :string;
    SickBalance       :double;
    PtoOption         :string;
    PtoBalance        :double;
    PtoAccrual        :double;
    WeeksRemaining    :double;
    AccrualToEOY      :double;
    EstBalAtEOY       :double;
    property EmpWorkType: TEmpWorkType read fEmpWorkType;

   function GetEmployeeShortName(EmpID: Integer): string;
    function GetEmployeeNumber(EmpID: Integer): string;
    function GetEmployeeEstStartTime(EmpID: Integer): TDateTime;
    function GetUserShortName: string;
    procedure GetEmployeeFullName(EmpID: Integer; var FirstName, LastName: string);
    function GetUserEmployeeNumber: string;
    function GetLoginName: string;
    function GetPasswordHash: string;

    function GetEmployeeTypeID(const EmpID: Integer): Integer;
    function GetEmpIDForPayrollProfitCenter(const ProfitCenter: string): Integer;

    function EmployeeHasCompanyVehicle(EmpID: Integer): Boolean;
    function GetEmployeePhone(EmpID: Integer): string;
    function GetLastPlatUpdate(EmpID: Integer): TDateTime;

    {Emp Activity}
    procedure AddEmployeeActivityEvent(const ActivityType, Details: string; EventDateTime: TDateTime=0; const ExtraDetails: string='');
    function HasRecordedEmployeeActivity(const ActivityType: string; EventDate: TDateTime=0): Boolean;
    function AlreadyHasActivityToday(const ActivityType: string; const EmpID: integer): Boolean;

    {Employee Lists}
    procedure EmployeeList(List: TStrings; ManagerID: Integer; EmployeeStatus: Integer = StatusActive);
    procedure FullEmployeeList(List: TStrings);
    procedure EmployeeReportList(List: TStrings; ReportID: string = RightRunReportForManager);
    procedure GetListForEmployeeDisplayMode(List: TStrings; Mode: TEmployeeSelectMode; ManagerID: Integer = -1);
    procedure LoadEmployeeListForManagerIDs(List: TStrings; ManagerIDs: TIntegerArray; ManagersOnly: Boolean);
    function GetIDListForEmployeeSelectMode(Mode: TEmployeeSelectMode; ManagerID: Integer): string;
    procedure EmpTypeList(List: TStrings; IncludeBlank: Boolean = False);
    procedure InitEmployeeStatusCombo(EmployeeStatusCombo: TCustomComboBox);

    {Mgr Lists}
    procedure ManagerList(List: TStrings; ReportID: string = RightRunReportForManager);
    procedure ManagerListTimeSheets(List: TStrings);
    function EffectiveManagerIDsForRight(const RightName: string): TIntegerArray;
    function EffectiveManagerIDsForReports(ReportID: string = RightRunReportForManager): TIntegerArray;
    function EffectiveManagerIDsForTimeSheets(Right: string = RightTimesheetsApprove): TIntegerArray;
    procedure ManagerListMessages(List: TStrings);

    {Mgr ID}
    function GetMyManagerID: Integer;
    function GetManagerIDForEmp(EmpID: Integer): Integer;
    function GetManagerIDListUnderManagerID(ManagerList: TStrings): string; overload;
    function GetManagerIDListUnderManagerID(ManagerId: Integer): string; overload;
    function EmpIsManager(EmpID: Integer): Boolean;

    procedure LoadCallCenterEmployees(LocatorID: Integer; List: TStrings);
    procedure UpdateHierarchyCache;

    {Employee Plat}
    procedure UpdateEmployee_PlatUpdate(FileDateTime: TDatetime);
    procedure GetPlatDateAndAge(EmpID: Integer; var RetPlatDate: TDateTime; var RetPlatAge: Integer);

    {First Task}     //QM-494 First Task Reminder EB
    function GetCurrentFirstTaskTime(EmpID: integer): string; //QM-494 First Task Reminder EB
    procedure UpdateFirstTaskReminder(EmpID: integer; ReminderTime: string); //QM-494 First Task Reminder EB

    {Employee Work Types - That we need to repeatedly check}
    procedure GetEmpWorkTypes;

    {OQ/PLUS WhiteList}
    function GetOQ(EmpID: integer): string;   //QM-444 EB
    function GetOQListForManager(MgrID: integer): string; //QM-444 EB Part 2
	  function GetPLUS(EmpID: integer): string;  //QM-585 EB Plus Whitelist

  	{Employee Balance (sick and PTO)}   //QM-579
    function GetEmployeeBalance(EmpID: integer):boolean;
    procedure ClearEmpBalRecord;
    function LoadEmployeeSickPTOfromServer(pEmpID: integer): boolean;

    {Employee PLUS White list}   //QM-585 EB
    function AddEmpToPlusWhiteList(pEmpID: integer; PlusWLID: integer): boolean; //QM-585 EB Plus WHiteList
    function RemoveEmpFromPlusWhiteList(pEmpID: integer; PlusWLID: integer): boolean;  //QM-585 EB Plus WHiteList
    function GetPLUSForManager(MgrID: integer): string; //QM-585 EB Plus WhiteList
    function GetEmpPlusMgrList(MgrID: integer): string; //qm-585 EB Plus Whitelist Part 3

    {Employee Timesheet_Entry - will be moving the rest from DMu} //QM-597 EB Abandoned Callout
    function HasLocalTimeSheetEntriesToday(pEmpID: integer): boolean;

    {MARS - Bulk Status}
    function GetMARSEmpList(MgrID: integer): string;  {QM-671 EB MARS}
    function AddMARSRequest(pLocatorID: integer; pNewStatus:string; pInsertedBy: integer): boolean;  {QM-671 EB MARS}
    function RemoveMARSRequest(pMARSID: integer): boolean; {QM-671 EB MARS}

    {Route Order Optimization}          //QM-702 EB
    function AddROOptimizationRequest(pLocatorID: integer; pInsertedBy: integer; pNextTicketID: integer): boolean; {QM-702 RO Optimization EB}
  end;

var
  EmployeeDM: TEmployeeDM;

implementation

{$R *.dfm}
uses
   OdDBISAMUtils, OdDbUtils, Variants, JclStrings,
   uNester, LocalPermissionsDMu, OdHourGlass, DateUtils, Dialogs; //QM-579 EB

{ TDataModule1 }

procedure TEmployeeDM.AddEmployeeActivityEvent(const ActivityType,
  Details: string; EventDateTime: TDateTime; const ExtraDetails: string);
var
  Dataset: TDataset;
  ActivityDate: TDateTime;
begin
  if EventDateTime = 0 then
    ActivityDate := RoundSQLServerTimeMS(Now)
  else
    ActivityDate := RoundSQLServerTimeMS(EventDateTime);

  DataSet := DM.Engine.OpenWholeTable('employee_activity');
  try
    if not DataSet.Locate('emp_id;activity_date;activity_type;details;extra_details',
      VarArrayOf([DM.EmpID, ActivityDate, ActivityType, Details, ExtraDetails]), []) then begin
      DM.Engine.LinkEventsAutoInc(Dataset);
      DataSet.Insert;
      DataSet.FieldByName('emp_id').AsInteger := DM.EmpID;
      DataSet.FieldByName('activity_date').AsDateTime := ActivityDate;
      DataSet.FieldByName('activity_type').AsString := ActivityType;
      DataSet.FieldByName('details').AsString := Details;
      DataSet.FieldByName('extra_details').AsString := ExtraDetails;
      DataSet.FieldByName('lat').AsFloat := DM.CurrentGPSLatitude;    //...sr  qmantwo-203
      DataSet.FieldByName('lng').AsFloat := DM.CurrentGPSLongitude;   //...sr  qmantwo-203
      DataSet.Post;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;




function TEmployeeDM.AlreadyHasActivityToday(const ActivityType: string; const EmpID: integer): Boolean;
begin
  Result := False;
  if ActivityType <> '' then begin
    DM.CallingServiceName('HasActivityToday');
    Result := DM.Engine.Service.HasActivityToday(DM.Engine.SecurityInfo, ActivityType, EmpID);
  end;
end;


function TEmployeeDM.EffectiveManagerIDsForReports(
  ReportID: string): TIntegerArray;
var
  RestrictionString: string;
  Managers: TStringList;
  i: Integer;
begin
  SetLength(Result, 1);
  if (not IsEmpty(DM.CurrentReportID)) and (DM.CurrentReportID <> ReportID) then
    ReportID := DM.CurrentReportID;
  if PermissionsDM.CanI(ReportID) or PermissionsDM.CanI(RightRunReportForManager) then begin

    RestrictionString := Trim(PermissionsDM.GetLimitation(ReportID));

    if ((RestrictionString = '') or (RestrictionString = '-')) and (ReportID <> RightRunReportForManager) then
      if PermissionsDM.CanI(RightRunReportForManager) then
        RestrictionString := Trim(PermissionsDM.GetLimitation(RightRunReportForManager));

    if (RestrictionString = '') or (RestrictionString = '-') then begin
      GetManagerID.ParamByName('EmpID').AsInteger := DM.EmpID;
      GetManagerID.Open;
      try
        if GetManagerID.Eof or (GetManagerID.Fields[0].AsInteger = 0) then
          Result[0] := DM.EmpID
        else
          Result[0] := GetManagerID.Fields[0].AsInteger;
      finally
        GetManagerID.Close;
      end;
    end else begin
      Managers := TStringList.Create;
      try
        Managers.CommaText := RestrictionString;
        SetLength(Result, Managers.Count);

        for i := 0 to Managers.Count - 1 do
          Result[i] := StrToIntDef(Managers[i], 0);
      finally
        FreeAndNil(Managers);
      end;
    end;
  end else
    Result[0] := DM.EmpID;
end;

function TEmployeeDM.EffectiveManagerIDsForRight(
  const RightName: string): TIntegerArray;
var
  RestrictionString: string;
  Managers: TStringList;
  i: Integer;
begin
  SetLength(Result, 1);
  if PermissionsDM.CanI(RightName) then begin
    RestrictionString := Trim(PermissionsDM.GetLimitation(RightName));

    if (RestrictionString = '') or (RestrictionString = '-') then begin
      GetManagerID.ParamByName('EmpID').AsInteger := DM.EmpID;
      GetManagerID.Open;
      try
        if GetManagerID.Eof or (GetManagerID.Fields[0].AsInteger = 0) then
          Result[0] := DM.EmpID
        else
          Result[0] := GetManagerID.Fields[0].AsInteger;
      finally
        GetManagerID.Close;
      end;
    end else begin
      Managers := TStringList.Create;
      try
        Managers.CommaText := RestrictionString;
        SetLength(Result, Managers.Count);

        for i := 0 to Managers.Count - 1 do
          Result[i] := StrToIntDef(Managers[i], 0);
      finally
        FreeAndNil(Managers);
      end;
    end;
  end else
    Result[0] := DM.EmpID;
end;


function TEmployeeDM.EffectiveManagerIDsForTimeSheets(
  Right: string): TIntegerArray;
var
  RestrictionString: string;
  Managers: TStringList;
  i: Integer;
begin
  SetLength(Result, 1);
  if PermissionsDM.CanI(RightTimesheetsApprove) then begin

    RestrictionString := PermissionsDM.GetLimitation(Right);

    if (RestrictionString = '') or (RestrictionString = '-') then begin
      Result[0] := StrToIntDef(RestrictionString, 0);
      if Result[0] = 0 then
        Result[0] := DM.EmpID
    end else begin
      Managers := TStringList.Create;
      try
        Managers.CommaText := RestrictionString;
        SetLength(Result, Managers.Count);

        for i := 0 to Managers.Count - 1 do
          Result[i] := StrToIntDef(Managers[i], 0);
      finally
        FreeAndNil(Managers);
      end;
    end;
  end else
    Result[0] := DM.EmpID;
end;

function TEmployeeDM.EmpIsManager(EmpID: Integer): Boolean;
begin
  EmpIsManagerQuery.ParamByName('emp_id').AsInteger := EmpID;
  EmpIsManagerQuery.Open;
  EmpIsManagerQuery.First;
  if (EmpIsManagerQuery.RecordCount > 0) or ( not EmpIsManagerQuery.Eof) then
    Result := True
  else
    Result := False;
end;

function TEmployeeDM.EmployeeHasCompanyVehicle(EmpID: Integer): Boolean;
const
  SQL = 'select company_car from employee where emp_id = %d';
var
  DataSet: TDataSet;
begin
  Assert(EmpID > 0);
  Result := False;
  DataSet := DM.Engine.OpenQuery(Format(SQL, [EmpID]));
  try
    if DataSet.Eof then
      raise Exception.CreateFmt('Employee %d not found to determine vehicle usage', [EmpID]);
    Result := DataSet.FieldByName('company_car').AsString = CompanyCarCOV;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TEmployeeDM.EmployeeList(List: TStrings; ManagerID,
  EmployeeStatus: Integer);
var
  Name: string;
  i: Integer;
  Cursor: IInterface;
  StatusToInclude: Boolean; // Active = True, Inactive = False
  IncludeLocator: Boolean;
  LocType: integer;  //QM-856 EB Remove TOM Bucket from send

begin
  Cursor := ShowHourGlass;
  Assert(Assigned(List));
  List.Clear;
  List.BeginUpdate;
  try
    GetEmpWorkTypes;

    Employees.ParamByName('report_to').AsInteger := ManagerID;
    Employees.Open;
    StatusToInclude := EmployeeStatus = StatusActive;
    while not Employees.Eof do begin
      LocType := Employees.FieldByName('type_id').AsInteger;     //QN-856 EB Remove TOM Bucket from send
      IncludeLocator := EmployeeStatus = StatusAll;
      IncludeLocator := IncludeLocator or (Employees.FieldByName('active').AsBoolean = StatusToInclude);
      IncludeLocator := IncludeLocator and (LocType <> fEmpWorkType.TomorrowBucketRefID);  //QM-856 EB REmove TOM Bucket from send
      // If the request is for inactive employees only, their managers are also included so the list looks normal
      IncludeLocator := IncludeLocator or ((EmployeeStatus = StatusInactive) and IsManagersWithInactiveEmps(Employees.FieldByName('emp_id').AsInteger));
      if IncludeLocator then begin
        Name := Employees.FieldByName('indented').AsString;
        i := List.Add(Name);
        List.Objects[i] := Pointer(Employees.FieldByName('emp_id').AsInteger);
      end;
      Employees.Next;
    end;
  finally
    List.EndUpdate;
    Employees.Close;
    if ManagersWithInactiveEmps.Active then
      ManagersWithInactiveEmps.Close;
  end;
end;

procedure TEmployeeDM.EmployeeNestHandler(const ID, Left, Right, Depth,
  Tag: Integer);
var
  PCCode: Variant;
begin
  PCCode := Null;
  if (Tag <> NULLNODEID) then
    PCCode := FEffectivePCCache.Strings[Tag];
  EmpHier.AppendRecord([ID, Left, Right, Depth, PCCode]);
end;

procedure TEmployeeDM.EmployeeReportList(List: TStrings; ReportID: string);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  LoadEmployeeListForManagerIDs(List, EffectiveManagerIDsForReports(ReportID), False);
end;

procedure TEmployeeDM.EmpTypeList(List: TStrings; IncludeBlank: Boolean);
begin
  Assert(Assigned(List));
  List.Clear;
  if IncludeBlank then
    List.Add('');
  RefreshDataSet(EmpTypes);
  while not EmpTypes.Eof do begin
    List.AddObject(EmpTypes.FieldByName('description').AsString,
      TObject(EmpTypes.FieldByName('ref_id').AsInteger));
    EmpTypes.Next;
  end;
  EmpTypes.Close;
end;



procedure TEmployeeDM.FullEmployeeList(List: TStrings);
const
  SQL = 'select emp_id, short_name from employee order by short_name';
var
  DataSet: TDataSet;
begin
  Assert(Assigned(List));
  DataSet := DM.Engine.OpenQuery(SQL);
  try
    List.BeginUpdate;
    try
      while not DataSet.Eof do begin
        List.AddObject(DataSet.FieldByName('short_name').AsString,
                       TObject(DataSet.FieldByName('emp_id').AsInteger));
        DataSet.Next;
      end;
    finally
      List.EndUpdate;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;



function TEmployeeDM.GetEmpIDForPayrollProfitCenter(
  const ProfitCenter: string): Integer;
const
  SQL = 'select emp_id from employee where repr_pc_code = ';
var
  DataSet: TDataSet;
begin
  Assert(ProfitCenter <> '');
  Result := -1;
  DataSet := DM.Engine.OpenQuery(SQL + QuotedStr(ProfitCenter));
  try
    if DataSet.Eof then
      raise Exception.CreateFmt('No employee is confgured to manage the profit center %s', [ProfitCenter]);
    Result := DataSet.Fields[0].AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TEmployeeDM.GetEmployeeBalance(EmpID: integer):boolean; //qm-579  sr
begin
  qryEmployeeBalance.close;
  try
    result := false;
    qryEmployeeBalance.ParamByName('EmpID').value := EmpID;
    qryEmployeeBalance.Open;
    if not qryEmployeeBalance.eof then result := true;
    
    with qryEmployeeBalance do
    begin
      EmploymentStatus := FieldByName('Employment_Status').AsString;
      InsertDate       := FieldByName('insert_date').AsDateTime;

      FirstName        := FieldByName('First_Name').AsString;
      LastName         := FieldByName('Last_Name').AsString;
      SickOption       := FieldByName('SICK_Option').AsString;
      SickBalance      := FieldByName('SICK_Balance').AsFloat;

      PtoOption        := FieldByName('PTO_Option').AsString;
      PtoBalance       := FieldByName('PTO_Balance').AsFloat;
      PtoAccrual       := FieldByName('PTO_Accrual').AsFloat;

      WeeksRemaining   := FieldByName('WeeksRemaining').AsFloat;
      AccrualToEOY     := FieldByName('Accrual_to_EOY').AsFloat;
      EstBalAtEOY      := FieldByName('Est_Bal_at_EOY').AsFloat;
  end;
  finally
    qryEmployeeBalance.close;
  end;
end;

procedure TEmployeeDM.ClearEmpBalRecord;
begin
//  with EmpBalType do
//  begin
    EmploymentStatus := '';
    InsertDate       := 0.0;

    SickOption       := '';
//    SickBalance      := 0.0;

    PtoOption        := '';
    PtoBalance       := 0.0;
    PtoAccrual       := 0.0;

    WeeksRemaining   := 0.0;
    AccrualToEOY     := 0.0;
    EstBalAtEOY      := 0.0;
//  End;
end;

function TEmployeeDM.GetEmployeeEstStartTime(EmpID: Integer): TDateTime;
begin

end;

procedure TEmployeeDM.GetEmployeeFullName(EmpID: Integer; var FirstName,
  LastName: string);
begin
  FirstName := '';
  LastName := '';
  GetEmployeeQuery.ParamByName('emp_id').AsInteger := EmpID;
  try
    GetEmployeeQuery.Open;
    if GetEmployeeQuery.IsEmpty then
      raise Exception.CreateFmt('Employee %d not found to determine full name', [EmpID]);
    FirstName := GetEmployeeQuery.FieldByName('first_name').AsString;
    LastName := GetEmployeeQuery.FieldByName('last_name').AsString;
  finally
    GetEmployeeQuery.Close;
  end;
end;

function TEmployeeDM.GetEmployeeNumber(EmpID: Integer): string;
begin
  EmployeeLookup.Open;
  if EmployeeLookup.Locate('emp_id', EmpID, []) then
    Result := EmployeeLookup.FieldByName('emp_number').AsString
  else
    Result := '';
end;

function TEmployeeDM.GetEmployeePhone(EmpID: Integer): string;
begin
  GetEmployeeQuery.ParamByName('emp_id').AsInteger := EmpID;
  try
    GetEmployeeQuery.Open;
    Result := GetEmployeeQuery.FieldByName('contact_phone').AsString;
  finally
    GetEmployeeQuery.Close;
  end;
end;

function TEmployeeDM.GetLastPlatUpdate(EmpID: Integer): TDateTime;
begin
  GetEmployeeQuery.ParamByName('emp_id').AsInteger := EmpID;
  try
    GetEmployeeQuery.Open;
    Result := GetEmployeeQuery.FieldByName('plat_update_date').AsDateTime;
  finally
    GetEmployeeQuery.Close;
  end;
end;

function TEmployeeDM.GetEmployeeShortName(EmpID: Integer): string;
begin
  GetEmployeeQuery.ParamByName('emp_id').AsInteger := EmpID;
  try
    GetEmployeeQuery.Open;
    Result := GetEmployeeQuery.FieldByName('short_name').AsString;
  finally
    GetEmployeeQuery.Close;
  end;
end;

function TEmployeeDM.GetEmployeeTypeID(const EmpID: Integer): Integer;
const
  SelectEmployeeType = 'select type_id as N from employee where emp_id=';
begin
  Result := DM.Engine.RunSQLReturnN(SelectEmployeeType + IntToStr(EmpID));
end;



function TEmployeeDM.GetEmpPlusMgrList(MgrID: integer): string;
begin
  DM.CallingServiceName('GetEmpPlusMgrList');
  Result := DM.Engine.Service.GetEmpPlusMgrList(Dm.Engine.SecurityInfo, MgrID);

end;

procedure TEmployeeDM.GetEmpWorkTypes;   //QM-294 EB
  procedure ClearEmpWorkTypes;
  begin
     fEmpWorkType.TomorrowBucketRefID := 0;
  end;

var
  typecode: string;
begin
  ClearEmpWorkTypes;
  RefreshDataSet(EmpTypes);
  EmpTypes.First;
  while not EmpTypes.EOF do begin
    typecode := EmpTypes.FieldByName('code').AsString;
    if typecode = 'UQTOM' then
      fEmpWorkType.TomorrowBucketRefID := EmpTypes.FieldByName('ref_id').AsInteger;
    EmpTypes.Next;
  end;
end;



function TEmployeeDM.GetIDListForEmployeeSelectMode(Mode: TEmployeeSelectMode;
  ManagerID: Integer): string;
var
  i: Integer;
  List: TStrings;
begin
  Result := '';
  if Mode = esAll then
    Exit;
  List := TStringList.Create;
  try
    GetListForEmployeeDisplayMode(List, Mode, ManagerID);
    for i := 0 to List.Count - 1 do begin
      if (Result = '') or ((Length(Result) > 1) and (Result[Length(Result)] = '(')) then
        Result := Result + IntToStr(Integer(List.Objects[i]))
      else
        Result := Result +','+ IntToStr(Integer(List.Objects[i]));
      //if (i > 0) and (i mod 100 = 0) then
      //  Result := Result + ') or emp_id in ('
    end;
  finally
    FreeAndNil(List);
  end;
end;



procedure TEmployeeDM.GetListForEmployeeDisplayMode(List: TStrings;
  Mode: TEmployeeSelectMode; ManagerID: Integer);
begin
  case Mode of
    // All employees of all types/statuses
    esAll: FullEmployeeList(List);
    // Managers you are allowed run reports for and all subordinate managers
    esReportManangers: ManagerList(List, RightRunReportForManager);
    // Managers you are allowed run reports for and all subordinate employees of any type/status
    esReportEmployees: EmployeeReportList(List, RightRunReportForManager);
    // Managers you are allowed approve timesheets for and all subordinate managers
    esTimesheetEmployees: ManagerListTimeSheets(List);
    // The entire employee hierarchy under a given manager
    esAllEmployeesUnderManager: EmployeeList(List, ManagerID);
    //Entire ACTIVE employee hierachy under a given manager
    esAllActiveEmployeesUnderManager: EmployeeList(List, ManagerID, 1);
    else raise Exception.Create('Unknown employee list mode');
  end;
end;

function TEmployeeDM.GetLoginName: string;
begin
  Result := DM.UQState.UserName;
end;

function TEmployeeDM.GetManagerIDForEmp(EmpID: Integer): Integer;
begin
  Assert(EmpID > 0);
  GetManagerID.ParamByName('EmpID').AsInteger := EmpID;
  GetManagerID.Open;
  try
    if GetManagerID.Eof or (GetManagerID.Fields[0].AsInteger = 0) then
      Result := EmpID
    else
      Result := GetManagerID.Fields[0].AsInteger;
  finally
    GetManagerID.Close;
  end;
end;

function TEmployeeDM.GetManagerIDListUnderManagerID(ManagerId: Integer): string;
var
  lManagerList: TStrings;
begin
  lManagerList := TStringList.Create;
  try
    lManagerList.Add(IntToStr(ManagerId));
    Result := GetManagerIDListUnderManagerID(lManagerList);
  finally
    lManagerList.Free;
  end;
end;

function TEmployeeDM.GetMARSEmpList(MgrID: integer): string; //QM-671 MARS Bulk Status
begin
  DM.CallingServiceName('GetMARSEmpList');
  Result := DM.Engine.Service.GetMARSEmpList(DM.Engine.SecurityInfo, MgrID);
end;

function TEmployeeDM.GetMyManagerID: Integer;
begin
  Result := GetManagerIDForEmp(DM.EmpID);
end;

function TEmployeeDM.GetOQ(EmpID: integer): string;  //QM-444 EB     //QM-485 EB Can we repurpose this?
begin
  DM.CallingServiceName('GetOQDataForEmployee');
  Result := DM.Engine.Service.GetOQDataForEmployee(DM.Engine.SecurityInfo, EmpID);
end;

function TEmployeeDM.GetOQListForManager(MgrID: integer): string;
begin
   DM.CallingServiceName('GetOQDataForManager');
  Result := DM.Engine.Service.GetOQDataForManager(Dm.Engine.SecurityInfo, MgrID, 1);
end;


function TEmployeeDM.AddEmpToPlusWhiteList(pEmpID, PlusWLID: integer): boolean;  //QM-585 EB Plus Whitelist
begin
  DM.CallingServiceName('AddEmpToPlusWhiteList');
  DM.Engine.Service.AddEmpToPlusWhiteList(DM.Engine.SecurityInfo, pEmpID, PlusWLId, DM.EmpID, 'Gas');
end;

function TEmployeeDM.AddMARSRequest(pLocatorID: integer; pNewStatus: string;
  pInsertedBy: integer): boolean;
begin
  DM.CallingServiceName('AddMARSRequest');   //QM-671 MARS EB Bulk Status
  Result := DM.Engine.Service.AddMARSRequest(DM.Engine.SecurityInfo, pLocatorID, pNewStatus, pInsertedBy);
end;


function TEmployeeDM.AddROOptimizationRequest(pLocatorID, pInsertedBy, pNextTicketID: integer): boolean;  //QM-702 Route Optimization EB
begin
  DM.CallingServiceName('AddROOptimizationRequest');
  Result := DM.Engine.Service.AddROOptimizationRequest(DM.Engine.SecurityInfo, pLocatorID, pInsertedBy, pNextTicketID)
end;

function TEmployeeDM.RemoveMARSRequest(pMARSID: integer): boolean;   {QM-671 EB MARS}
begin
  DM.CallingServiceName('RemoveMARSRequest');
  Result := DM.Engine.Service.RemoveMARSRequest(DM.Engine.SecurityInfo, pMARSID);
end;



function TEmployeeDM.RemoveEmpFromPlusWhiteList(pEmpID, PlusWLID: integer): boolean;  //QM-585 EB Plus Whitelist
begin
  DM.CallingServiceName('RemoveEmpFromPlusWhiteList');
  DM.Engine.Service.RemoveEmpFromPlusWhiteList(DM.Engine.SecurityInfo, pEmpID, PlusWLId, DM.EmpID);
end;



function TEmployeeDM.GetPLUS(EmpID: integer): string;  //QM-585 PLUS
begin
  DM.CallingServiceName('GetWMEmpPlusUtilByEmp');
  Result := DM.Engine.Service.GetWMEmpPlusUtilByEmp(DM.Engine.SecurityInfo, EmpID);

end;

function TEmployeeDM.GetPLUSForManager(MgrID: integer): string;
begin
  DM.CallingServiceName('GetPLUSForManager');
  Result := DM.Engine.Service.GetPLUSForManager(Dm.Engine.SecurityInfo, MgrID);
end;

function TEmployeeDM.GetManagerIDListUnderManagerID(
  ManagerList: TStrings): string;
var
  List: TStringList;
  IDList: string;
  i: Integer;
  ManagerIDsArray: TIntegerArray;
begin
  List := TStringList.Create;
  try
    SetLength(ManagerIDsArray, ManagerList.Count);
    for i := 0 to ManagerList.Count - 1 do
      ManagerIDsArray[i] := StrToInt(ManagerList[i]);
    LoadEmployeeListForManagerIDs(List, ManagerIDsArray, True);
    for i := 0 to List.Count - 1 do
      IDList := IDList + IntToStr(Integer(List.Objects[i])) + ',';
    if Length(IdList) > 0 then
      IDList[Length(IdList)] := ' '
    else
      IDList := '0';
  finally
    FreeAndNil(List);
  end;
  Result := IDList;
end;

function TEmployeeDM.GetPasswordHash: string;
begin
  Result := DM.UQState.Password;
end;

procedure TEmployeeDM.GetPlatDateAndAge(EmpID: Integer;
  var RetPlatDate: TDateTime; var RetPlatAge: Integer);
begin
  GetEmployeeQuery.ParamByName('emp_id').AsInteger := EmpID;
  try
    GetEmployeeQuery.Open;
    RetPlatDate := GetEmployeeQuery.FieldByName('plat_update_date').AsDateTime;
    RetPlatAge :=  GetEmployeeQuery.FieldByName('plat_age').AsInteger;
  finally
    GetEmployeeQuery.Close;
  end;
end;



function TEmployeeDM.HasLocalTimeSheetEntriesToday(pEmpID: integer): boolean;
const
  SQL = 'select entry_id from timesheet_entry where work_emp_id = %d and work_date >= ''%s''';
var
  DataSet: TDataSet;

begin
  Result := False;
  DataSet := DM.Engine.OpenQuery(Format(SQL, [pEmpID, IsoDateToStr(Today)]));
  try
    if not DataSet.EOF then
      Result := True;
    DataSet.Close;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TEmployeeDM.HasRecordedEmployeeActivity(const ActivityType: string;
  EventDate: TDateTime): Boolean;
var
  ActivityDate: TDateTime;
begin
  Result := False;
  if EventDate = 0 then
    ActivityDate := RoundSQLServerTimeMS(Date)
  else
    ActivityDate := RoundSQLServerTimeMS(EventDate);

  ActivityLookup.ParamByName('activity_type').AsString := ActivityType;
  ActivityLookup.ParamByName('activity_date').AsDateTime := ActivityDate;
  try
   ActivityLookup.Open;
   ActivityLookup.First;
   If not ActivityLookup.Eof then
     Result := True;
  finally
    ActivityLookup.Close;
  end;
end;

function TEmployeeDM.GetUserEmployeeNumber: string;
begin
  Result := GetEmployeeNumber(DM.EmpID);
end;

function TEmployeeDM.GetUserShortName: string;
begin
  Result := GetEmployeeShortName(DM.UQState.EmpID);
end;

procedure TEmployeeDM.InitEmployeeStatusCombo(
  EmployeeStatusCombo: TCustomComboBox);
begin
  Assert(EmployeeStatusCombo <> nil);
  DM.PopulateActiveStatusList(EmployeeStatusCombo.Items);
  if EmployeeStatusCombo.Items.Count > 0 then
    EmployeeStatusCombo.ItemIndex := 0;
end;


function TEmployeeDM.IsManagersWithInactiveEmps(EmpID: Integer): Boolean;
begin
  if not ManagersWithInactiveEmps.Active then
    ManagersWithInactiveEmps.Open;
  Result := ManagersWithInactiveEmps.Locate('emp_id', EmpID, []);
end;

procedure TEmployeeDM.LoadCallCenterEmployees(LocatorID: Integer;
  List: TStrings);
begin
  if not EmpHier.Active then
    EmpHier.Open;
  EmpHier.First;
  List.Clear;
  while not EmpHier.EOF do begin
    List.AddObject(EmpHier.FieldByName('short_name').AsString, TObject(EmpHier.FieldByName('emp_id').AsInteger));
    EmpHier.Next;
  end;
end;

procedure TEmployeeDM.LoadEmployeeListForManagerIDs(List: TStrings;
  ManagerIDs: TIntegerArray; ManagersOnly: Boolean);
const
  SQL =
  'select HierLoc.depth - HierMgr.depth as level, HierLoc.emp_id, '+
  '  e.short_name, e.active, HierLoc.lft, e.type_id, '+
  '  Substring(''                              '' from 1 for ((HierLoc.depth - HierMgr.depth)*2)) + e.short_name as indented '+
  'from emp_hier HierLoc, emp_hier HierMgr '+
  '  inner join employee e on (HierLoc.emp_id = e.emp_id) '+
  '  inner join reference r on (r.ref_id = e.type_id) ' +
  'where HierLoc.lft between HierMgr.lft and HierMgr.rght '+
  '  and HierMgr.emp_id = :EmpID and (HierLoc.emp_id = HierMgr.emp_id or (%s)) '+
  'order by HierLoc.lft'; // This includes inactive employees, so we can report on them
var
  i: Integer;
  Manager: Integer;
  Name: string;
  EmpTypes: string;
begin
  Assert(Assigned(List));
  List.Clear;
  List.BeginUpdate;
  try
    for Manager := Low(ManagerIDs) to High(ManagerIDs) do
      try
        if ManagersOnly then
          EmpTypes := 'r.modifier like ''%MGR%''' // Corp, SD, Dir, Mgr, SS, Sup
        else
          EmpTypes := '1=1';
        ManagersUnderEmp.SQL.Text := Format(SQL, [EmpTypes]);
        ManagersUnderEmp.ParamByName('EmpID').AsInteger := ManagerIDs[Manager];
        ManagersUnderEmp.Open;
        while not ManagersUnderEmp.Eof do begin
          Name := ManagersUnderEmp.FieldByName('indented').AsString;
          i := List.Add(Name);
          List.Objects[i] := Pointer(ManagersUnderEmp.FieldByName('emp_id').AsInteger);
          ManagersUnderEmp.Next;
        end;
      finally
        ManagersUnderEmp.Close;
      end;
  finally
    List.EndUpdate;
  end;
end;


function TEmployeeDM.LoadEmployeeSickPTOfromServer(pEmpID: integer): boolean;  //QM-579 EB
var
  BalanceStr: string;
begin
  BalanceStr := '';
  //ShowMessage(IntToStr(pEmpID));  //Debug
  DM.CallingServiceName('GetSickleavePTOBalances');
  BalanceStr := DM.Engine.Service.GetSickleavePTOBalances(DM.Engine.SecurityInfo, pEmpID);
  //ShowMessage(BalanceStr); //Debug
  DM.Engine.SyncTablesFromXmlString(BalanceStr);

end;

procedure TEmployeeDM.ManagerList(List: TStrings; ReportID: string);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  LoadEmployeeListForManagerIDs(List, EffectiveManagerIDsForReports(ReportID), True);
end;

procedure TEmployeeDM.ManagerListMessages(List: TStrings);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  LoadEmployeeListForManagerIDs(List, EffectiveManagerIDsForRight(RightMessagesSend), True);
end;

procedure TEmployeeDM.ManagerListTimeSheets(List: TStrings);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  LoadEmployeeListForManagerIDs(List, EffectiveManagerIDsForTimeSheets, True);
end;

procedure TEmployeeDM.RefreshEffectivePCCache;
const
  Select = 'select distinct repr_pc_code from employee ' +
    'where repr_pc_code is not null order by repr_pc_code';
var
  Data: TDataSet;
begin
  if not Assigned(FEffectivePCCache) then
    FEffectivePCCache := TStringList.Create;

  Data := DM.Engine.OpenQuery(Select);
  try
    DM.GetListFromDataSet(Data, FEffectivePCCache, 'repr_pc_code', '', False);
  finally
    FreeAndNil(Data);
  end;
end;




procedure TEmployeeDM.UpdateEmployee_PlatUpdate(FileDateTime: TDatetime);
const
  SQL = 'update employee set plat_update_date=%s where emp_id= %d';
begin
  DM.CallingServiceName('UpdateEmployeePlatsFromHistory');
//  UpdateDateStr := DateTimeToStr(FileDateTime);
  DM.Engine.Service.UpdateEmployeePlatsFromHistory(DM.Engine.SecurityInfo, DM.EmpID, FileDateTime);
  DM.Engine.ExecQuery(Format(SQL, [DateToDBISAMDate(FileDateTime), DM.EmpID ]));
end;




procedure TEmployeeDM.UpdateHierarchyCache;
{
 This uses the TNester object developed by Kyle Cordes to visualize the employee
 hierarchy as nested sets. A description of the nested sets approach to hierarchy
 modeling can be found here: http://www.intelligententerprise.com/001020/celko1_1.jhtml

 Joins to the emp_hier table are used to navigate branches of the hierarchy.
 The UQ org looks something like this where the coordinates in the parentheses would represent each employee's lft & rght values:

       STS-Utiliquest
          (1, 12)
            /\
           /  \
     B.West  M.Bell
    (2, 9)   (10, 11)
      /\
     /  \
D.Hood  G.Mosier
(3, 4)  (5, 8)
          /
         /
      142 Manager
      (6, 7)
}
var
  Employees: TDataSet;
  N: TNester;
  ReportsTo: Integer;
  PCIndex: Integer;
begin
  Employees := nil;
  N := TNester.Create(False);
  RefreshEffectivePCCache;
  try
    // Order by short_name here so ordering by emp_hier.lft later comes out in name order, too.
    Employees := DM.Engine.OpenQuery('select emp_id, report_to, short_name, repr_pc_code from employee order by short_name');
    while not Employees.Eof do begin
      ReportsTo := Employees.FieldByName('report_to').AsInteger;
      if ReportsTo = 0 then
        ReportsTo := NULLNODEID;

      PCIndex := FEffectivePCCache.IndexOf(Employees.FieldByName('repr_pc_code').AsString);
      if PCIndex > -1 then
        N.AddNode(Employees.FieldByName('emp_id').AsInteger, ReportsTo, PCIndex)
      else
        N.AddNode(Employees.FieldByName('emp_id').AsInteger, ReportsTo);
      Employees.Next;
    end;
    Employees.Close;

    DM.StartTransaction;
    try
      EmpHier.Open;
      EmpHier.LockTable;
      try
        DM.Engine.EmptyTable('emp_hier');
        N.Convert(EmployeeNestHandler);
      finally
        EmpHier.UnlockTable;
        CloseDataSet(EmpHier);
      end;
     DM.Commit;
    except
      DM.Rollback;
      raise;
    end;
  finally
    FreeAndNil(Employees);
    FreeAndNil(N);
    FreeAndNil(FEffectivePCCache);
  end;
end;

function TEmployeeDM.GetCurrentFirstTaskTime(EmpID: integer): string; //QM-494 First Task Reminder EB
begin
  DM.CallingServiceName('GetCurrentFirstTaskTime');
  Result := DM.Engine.Service.GetFirstTaskReminder(DM.Engine.SecurityInfo, EmpID);
end;


procedure TEmployeeDM.UpdateFirstTaskReminder(EmpID: integer; ReminderTime: string); //QM-494 First Task Reminder EB
begin
  DM.CallingServiceName('UpdateFirstTaskReminder');
  DM.Engine.Service.UpdateFirstTaskReminder(DM.Engine.SecurityInfo, EmpID, ReminderTime);
end;

{ TFirstTaskReminder }
function TFirstTaskReminder.NagFlag: boolean;
begin
  if (Now >= fReminderDate) and (EstStartDate < (Date + 1)) then
    Result := True
  else begin
    Result := False;
  end;
end;

constructor TFirstTaskReminder.Create(pEmpID: integer; HitINIOnly: boolean);  //QM-494 First Task
begin
  fEmpID := pEmpID;
  fReminderStr := '';
  fReminderDate := Today - 1;  {init as yesterday}
  fLastChecked := Today - 1;
  fEstStartDate:= Today - 1;
  fUseIniOnly := HitIniOnly;

  if PermissionsDM.CanI(RightTicketsViewFirstTaskReminder) then begin
    fRequired := True;  {Note: we will turn this off if there is no valid time stored}
    ReminderCheck(HitIniOnly);
  end
  else
    fRequired := False;  {ignore if user does not have permission}

end;



destructor TFirstTaskReminder.Destroy;
begin

end;

function TFirstTaskReminder.HasEstStartDate: boolean;
var
  DBEstStartDate: TDatetime;
begin
  {check local DBISAM first}
  EmployeeDM.FirstTaskLookup.ParamByName('EMPID').AsInteger := fEmpID;
  EmployeeDM.FirstTaskLookup.Open;
  EmployeeDM.FirstTaskLookup.First;
  if not EmployeeDM.FirstTaskLookup.Eof then
    DBEstStartDate := EmployeeDM.FirstTaskLookup.FieldByName('est_start_date').AsDateTime;

    if (EstStartDate > (Date + 1)) then begin
      fEstStartDate := DBEstStartDate;
      Result := True;
    end;

end;

function TFirstTaskReminder.HasReminder: boolean;
begin
  if (fReminderStr <> BlankTime) and (fReminderStr <> '') then
    Result := True
  else Result := False

end;



function TFirstTaskReminder.Remindercheck(HitINIOnly:boolean): boolean;
begin
  Result := False;
  if Self.Required then begin
    {If recent sync, just use what is in INI file}
    if (DM.UQState.FTR.Reminder > '') and (DM.UQState.FTR.Reminder <> BlankTime) and
       (DM.UQState.FTR.LastUpdated > Today) and HitIniOnly then
      fReminderStr := DM.UQState.FTR.Reminder
    else if fUseIniOnly then
      fReminderStr := DM.UQState.FTR.Reminder
    else begin
      DM.CallingServiceName('GetCurrentFirstTaskTime');
      fReminderStr := DM.Engine.Service.GetFirstTaskReminder(DM.Engine.SecurityInfo, fEmpID);
      if (fReminderStr = '') or (fReminderStr = BlankTime) then
        fRequired := False;
      DM.UQState.UpdateFirstTaskIni(fReminderStr, Self.Required);
    end;
    Self.fDebug := DM.UQState.FTR.Debug;
  end;

  if HasReminder then begin
    fReminderDate := Date + StrToDatetime(fReminderStr);
    if fReminderDate <= Now then
      Result := True;
  end
  else
   fReminderDate:= Date - 1;  {Outdated}

  fLastChecked := Now;
end;

function TFirstTaskReminder.TimeRemaining: TDateTime;
begin
  Result := Now - fReminderDate;
end;

end.
