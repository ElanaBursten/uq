object PotentialHazardDialog: TPotentialHazardDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Potential Hazard'
  ClientHeight = 262
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object PotentialHazardLabel1: TLabel
    Left = 8
    Top = 8
    Width = 335
    Height = 13
    Caption = 
      'There have been one or more potentially hazardous conditions not' +
      'ed.'
  end
  object Label1: TLabel
    Left = 8
    Top = 152
    Width = 356
    Height = 26
    Caption = 
      'Call the WGL Hotline at (703) 750-1000. Record the first and las' +
      't name of the person you spoke with and the confirmation number ' +
      'they gave you.'
    WordWrap = True
  end
  object FirstNameLabel: TLabel
    Left = 8
    Top = 195
    Width = 55
    Height = 13
    Caption = 'First Name:'
  end
  object LastNameLabel: TLabel
    Left = 126
    Top = 194
    Width = 54
    Height = 13
    Caption = 'Last Name:'
  end
  object ConfirmationNumberLabel: TLabel
    Left = 255
    Top = 194
    Width = 105
    Height = 13
    Caption = 'Confirmation Number:'
  end
  object OKButton: TButton
    Left = 294
    Top = 231
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKButtonClick
  end
  object FirstNameEdit: TEdit
    Left = 8
    Top = 207
    Width = 112
    Height = 21
    TabOrder = 1
    OnChange = DataChange
  end
  object LastNameEdit: TEdit
    Left = 126
    Top = 207
    Width = 123
    Height = 21
    TabOrder = 2
    OnChange = DataChange
  end
  object ConfirmationNumberEdit: TEdit
    Left = 255
    Top = 207
    Width = 114
    Height = 21
    TabOrder = 3
    OnChange = DataChange
  end
  object HazardMemo: TMemo
    Left = 8
    Top = 24
    Width = 352
    Height = 122
    TabStop = False
    Lines.Strings = (
      'HazardMemo')
    ParentColor = True
    ReadOnly = True
    TabOrder = 4
  end
end
