inherited SearchForm: TSearchForm
  BorderIcons = [biSystemMenu]
  Caption = 'Search'
  ClientHeight = 503
  ClientWidth = 687
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 6
    Top = 186
    Width = 673
    Height = 294
    TabOrder = 0
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object GridView: TcxGridDBTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnFocusedRecordChanged = GridViewFocusedRecordChanged
      DataController.DataSource = SearchSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.HideSelection = True
      OptionsSelection.InvertSelect = False
      OptionsView.GridLineColor = clBtnFace
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object NothingFoundPanel: TPanel
    Left = 20
    Top = 220
    Width = 257
    Height = 53
    Caption = 'No Matching Items Were Found'
    TabOrder = 1
    Visible = False
  end
  object SearchTable: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    Exclusive = True
    Left = 24
    Top = 8
  end
  object SearchSource: TDataSource
    AutoEdit = False
    DataSet = SearchTable
    Left = 24
    Top = 56
  end
  object SchemaTable: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    Left = 80
    Top = 8
  end
  object SaveDlg: TSaveDialog
    DefaultExt = 'tsv'
    Filter = 'Tab Separated Files (*.tsv)|*.tsv|Text files (*.txt)|*.txt'
    Title = 'Save Tab-Delimited Data'
    Left = 80
    Top = 56
  end
end
