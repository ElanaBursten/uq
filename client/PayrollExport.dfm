object PayrollExportForm: TPayrollExportForm
  Left = 384
  Top = 375
  BorderStyle = bsDialog
  Caption = 'Payroll Export'
  ClientHeight = 129
  ClientWidth = 288
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    288
    129)
  PixelsPerInch = 96
  TextHeight = 13
  object WeekLabel: TLabel
    Left = 14
    Top = 12
    Width = 66
    Height = 13
    Caption = 'Week Ending:'
  end
  object ProfitCenter: TLabel
    Left = 14
    Top = 36
    Width = 99
    Height = 13
    Caption = 'Payroll Timesheet #:'
  end
  object NoteLabel: TLabel
    Left = 14
    Top = 60
    Width = 263
    Height = 35
    AutoSize = False
    Caption = 
      'Note: All of the timesheets for the profit center must be approv' +
      'ed and final approved before exporting.'
    WordWrap = True
  end
  object ProfitCenterCombo: TComboBox
    Left = 115
    Top = 33
    Width = 166
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object ExportButton: TButton
    Left = 59
    Top = 96
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Export'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelButton: TButton
    Left = 154
    Top = 96
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object ExportDate: TcxDateEdit
    Left = 115
    Top = 9
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = ExportDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 166
  end
end
