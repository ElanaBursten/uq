inherited MessageSearchCriteria: TMessageSearchCriteria
  Caption = 'Messages'
  ClientHeight = 244
  Font.Charset = ANSI_CHARSET
  PixelsPerInch = 96
  TextHeight = 13
  inherited RecordsLabel: TLabel
    Top = 219
  end
  object AddedDateLabel: TLabel [1]
    Left = 320
    Top = 82
    Width = 48
    Height = 13
    Caption = 'Sent Date'
  end
  object Label1: TLabel [2]
    Left = 320
    Top = 18
    Width = 69
    Height = 13
    Caption = 'Show On Date'
  end
  object Label2: TLabel [3]
    Left = 6
    Top = 22
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Message Number:'
  end
  object Label3: TLabel [4]
    Left = 6
    Top = 52
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Destination:'
  end
  object Label4: TLabel [5]
    Left = 6
    Top = 81
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Subject:'
  end
  object Label5: TLabel [6]
    Left = 6
    Top = 108
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Recipient Name:'
  end
  inherited SearchButton: TButton
    Top = 213
    TabOrder = 6
  end
  inherited CancelButton: TButton
    Left = 504
    Top = 213
    TabOrder = 7
  end
  inherited CopyButton: TButton
    Top = 213
    TabOrder = 8
  end
  inherited ExportButton: TButton
    Top = 213
    TabOrder = 9
  end
  inherited PrintButton: TButton
    TabOrder = 10
  end
  inherited ClearButton: TButton
    Left = 425
    Top = 213
    TabOrder = 11
  end
  inline SentDateRange: TOdRangeSelectFrame
    Left = 397
    Top = 70
    Width = 248
    Height = 63
    TabOrder = 5
  end
  inline ShowDateRange: TOdRangeSelectFrame
    Left = 397
    Top = 6
    Width = 248
    Height = 63
    TabOrder = 4
  end
  object MessageNumber: TEdit
    Left = 97
    Top = 18
    Width = 193
    Height = 21
    TabOrder = 0
  end
  object Destination: TEdit
    Left = 97
    Top = 48
    Width = 193
    Height = 21
    TabOrder = 1
  end
  object Subject: TEdit
    Left = 97
    Top = 78
    Width = 193
    Height = 21
    TabOrder = 2
  end
  object RecipientName: TEdit
    Left = 97
    Top = 105
    Width = 193
    Height = 21
    TabOrder = 3
  end
end
