unit TicketActivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst, OdRangeSelect;

type
  TTicketActivityReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    Label1: TLabel;
    ActivityDateRange: TOdRangeSelectFrame;
    Label2: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu, LocalEmployeeDMu;

procedure TTicketActivityReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
  ActivityDateRange.SelectDateRange(rsYesterday);
end;

procedure TTicketActivityReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TicketActivity');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamDate('StartDate', ActivityDateRange.FromDate, True);
  SetParamDate('EndDate', ActivityDateRange.ToDate, True);
  SetParamIntCombo('EmployeeStatus', EmployeeStatusCombo, True);
end;

end.
