inherited MessageMainForm: TMessageMainForm
  Left = 461
  Top = 184
  Caption = 'Messages'
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MessageTabControl: TTabControl
    Left = 0
    Top = 0
    Width = 548
    Height = 400
    Align = alClient
    TabOrder = 0
    Tabs.Strings = (
      'Review Messages'
      'Send Message')
    TabIndex = 0
    OnChange = MessageTabControlChange
  end
end
