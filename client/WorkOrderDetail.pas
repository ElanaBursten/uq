unit WorkOrderDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, DBCtrls, AttachmentsAddin, PlatAddin,
  Attachments, ComCtrls, ExtCtrls, DB, DMu, DBISAMTb,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, 
  cxStyles, cxDataStorage, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, BaseNotes, cxNavigator,
  WorkOrderDetailBase, WorkOrderDetailWGL, WorkOrderDetailLAM01,
  ActnList, CodeLookupList, WorkOrderDetailOHM, WorkOrderDetailINR,
  cxCustomData, cxFilter, cxData;

type
  TWorkOrderDetailForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    TicketNum: TLabel;
    DoneButton: TButton;
    PrintButton: TButton;
    WorkOrderPageControl: TPageControl;
    DetailsTab: TTabSheet;
    FunctionKeyPanel: TPanel;
    F5Label: TLabel;
    F6Label: TLabel;
    F7Label: TLabel;
    F8Label: TLabel;
    F9Label: TLabel;
    ImageTab: TTabSheet;
    NotesTab: TTabSheet;
    HistoryTab: TTabSheet;
    HistPanel: TPanel;
    HistoryPageControl: TPageControl;
    WorkOrderStatusTab: TTabSheet;
    AssignmentsTab: TTabSheet;
    AttachmentsTab: TTabSheet;
    AttachmentsFrame: TAttachmentsFrame;
    WorkOrderDS: TDataSource;
    StatusSource: TDataSource;
    WorkOrderNotesFrame: TBaseNotesFrame;
    WorkOrderMemo: TMemo;
    WorkOrderStatusGrid: TcxGrid;
    WorkOrderStatusHistoryDS: TDataSource;
    WorkOrderStatusGridView: TcxGridDBTableView;
    WOStatusColWOStatusID: TcxGridDBColumn;
    WOStatusColWOID: TcxGridDBColumn;
    WOStatusColStatusDate: TcxGridDBColumn;
    WOStatusColStatus: TcxGridDBColumn;
    WOStatusColStatusedByID: TcxGridDBColumn;
    WOColStatusStatusedHow: TcxGridDBColumn;
    WOStatusColInsertDate: TcxGridDBColumn;
    WorkOrderStatusGridLevel: TcxGridLevel;
    WOStatusColShortName: TcxGridDBColumn;
    WorkOrderStatusHistory: TDBISAMTable;
    WOAssignmentsGrid: TcxGrid;
    WOAssignmentsGridView: TcxGridDBTableView;
    WOAColAssignmentID: TcxGridDBColumn;
    WOAColWOID: TcxGridDBColumn;
    WOAColAssignedToID: TcxGridDBColumn;
    WOAColInsertDate: TcxGridDBColumn;
    WOAColAddedBy: TcxGridDBColumn;
    WOAColModifiedDate: TcxGridDBColumn;
    WOAColActive: TcxGridDBColumn;
    WOAssignmentsGridLevel: TcxGridLevel;
    WOAssignmentDS: TDataSource;
    WorkOrderAssignmentHistory: TDBISAMTable;
    WOAColAssignedToShortName: TcxGridDBColumn;
    ResponsesTab: TTabSheet;
    ResponsesGrid: TcxGrid;
    ResponsesGridView: TcxGridDBTableView;
    WORespColWOResponseID: TcxGridDBColumn;
    WORespColWOID: TcxGridDBColumn;
    WORespColResponseDate: TcxGridDBColumn;
    WORespColWOSource: TcxGridDBColumn;
    WORespColStatus: TcxGridDBColumn;
    WORespColResponseSent: TcxGridDBColumn;
    WORespColSuccess: TcxGridDBColumn;
    WORespColReply: TcxGridDBColumn;
    ResponsesGridLevel: TcxGridLevel;
    WorkOrderResponseLog: TDBISAMTable;
    WOResponseLogDS: TDataSource;
    WOAColCallCenter: TcxGridDBColumn;
    TicketsDS: TDataSource;
    Tickets: TDBISAMQuery;
    WOVersionsTab: TTabSheet;
    VersionsPanel: TPanel;
    VersionSpacerPanel: TPanel;
    WOVersionImageMemo: TDBMemo;
    WOVersionsGrid: TcxGrid;
    WOVersionsGridView: TcxGridDBTableView;
    ColWOVerCallCenter: TcxGridDBColumn;
    ColWOVerActive: TcxGridDBColumn;
    ColWOVerVersionID: TcxGridDBColumn;
    ColWOVerTicketRevision: TcxGridDBColumn;
    ColWOVerTicketType: TcxGridDBColumn;
    ColWOVerTransmitDate: TcxGridDBColumn;
    ColWOVerArrivalDate: TcxGridDBColumn;
    ColWOVerProcessedDate: TcxGridDBColumn;
    WOVersionsGridLevel: TcxGridLevel;
    WOVersionsDS: TDataSource;
    WorkOrderVersions: TDBISAMQuery;
    WODetailsLAM01: TWOLAM01Frame;
    WODetailsWGL: TWOWGLFrame;
    ActionList: TActionList;
    DoneAction: TAction;
    PrintAction: TAction;
    WOStatusCGAReasonDescription: TcxGridDBColumn;
    RestrictionPanel: TPanel;
    NoteSubTypeCombo: TComboBox;
    Label17: TLabel;
    WODetailsOHM: TWOOHMFrame;
    WODetailsINR: TWOINRFrame;
    Kind: TDBText;
    WorkOrderNumber: TDBText;  //QMANTWO-387
    procedure WorkOrderDSDataChange(Sender: TObject; Field: TField);
    procedure DoneActionExecute(Sender: TObject);
    procedure WorkOrderPageControlChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AttachmentsFrameAddButtonClick(Sender: TObject);
    procedure ManageAddinButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PrintActionExecute(Sender: TObject);
    procedure WOAColAddedByGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure WorkOrderVersionsCalcFields(DataSet: TDataSet);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure NoteSubTypeComboChange(Sender: TObject);
    procedure WorkOrderNotesFrameAddNoteButtonClick(Sender: TObject);
    procedure WODetailsOHMcmbxCurbValueFoundPropertiesChange(Sender: TObject);
    procedure ItemDblClick(Sender: TObject);
  private
    FNoteSubTypeList: TCodeLookupList;  //Associate object with the combobox
    FOnDoneWithWorkOrder: TNotifyEvent;
    FOnLeavingAndSaveWorkOrder: TNotifyEvent;
    FOnWorkOrderPrint: TItemSelectedEvent;
    FAttachmentAddin: TAttachmentsAddinManager;
    FWorkOrderHistoryDownloaded: Boolean;
    FPlatAddin: TPlatAddinManager;

    function WorkOrderIsClosed: Boolean;
    procedure EnableEditing(Enabled: Boolean);
    procedure SaveWorkOrder;
    function WorkOrder: TDataSet;
    procedure PrepareWorkOrderAddins;
    procedure ShowWorkOrderImage;
    function HaveWorkOrder: Boolean;
    procedure ShowWorkOrderHistory;
    procedure SetupDataSets;
    procedure SetupImageMemoDisplay(Memo: TCustomMemo; const WorkOrderIsXml: Boolean);
    procedure LoadDetailsFrame(Source: string);
    procedure TabSetup(Source: string);
    procedure SetWorkOrderSource(Source: string);
    procedure PopulateNoteSubType(NoteType: Integer);
    procedure ShowNotesAsReadOnly(Value: boolean);

  protected
    FWorkOrderID: Integer;
    FActiveWOFrame: TWODetailBaseFrame;
    FWorkOrderSource: string;
  public
    IsLoaded: Boolean;
    procedure SetReadOnly(const IsReadOnly: Boolean); override;
    procedure DoneWithWorkOrder;
    procedure GotoWorkOrder(ID: Integer; ComingFromManagementForm: Boolean);
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure ClearSelection;

    property OnDoneWithWorkOrder: TNotifyEvent read FOnDoneWithWorkOrder write FOnDoneWithWorkOrder;
    property OnLeavingAndSaveWorkOrder: TNotifyEvent read FOnLeavingAndSaveWorkOrder write FOnLeavingAndSaveWorkOrder;
    property OnWorkOrderPrint: TItemSelectedEvent read FOnWorkOrderPrint write FOnWorkOrderPrint;
    property WorkOrderSource: string read FWorkOrderSource write SetWorkOrderSource;
    property ActiveWOFrame: TWODetailBaseFrame read FActiveWOFrame write FActiveWOFrame;
  end;

var
  WorkOrderDetailForm: TWorkOrderDetailForm;

implementation

uses OdHourGlass, QMConst, OdDBUtils, OdMiscUtils, OdExceptions,
  SharedImages, DBISAMLb, TimeFileMonitor, StrUtils,
  LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

procedure TWorkOrderDetailForm.FormCreate(Sender: TObject);
begin
  inherited;
  IsLoaded := False;
  FNoteSubTypeList := TCodeLookupList.Create(NoteSubTypeCombo.Items);
  WorkOrderDS.DataSet := DM.WorkOrder;
  WODetailsLAM01.WorkOrderDS := WorkOrderDS;
  WODetailsLAM01.TicketsDS := TicketsDS;
  WODetailsWGL.WorkOrderDS := WorkOrderDS;
  ActiveWOFrame := WODetailsLAM01; //Set to WO Details screen for now
  SetupDataSets;
  PopulateNoteSubType(qmftWorkOrder);
  DM.OutstandingWOAttachments := False;  //QMANTWO-815 EB
end;

procedure TWorkOrderDetailForm.SetupDataSets;
begin
  AddLookupField('short_name', WorkOrderStatusHistory, 'statused_by_id',
    EmployeeDM.EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
  AddLookupField('cga_reason_desc', WorkOrderStatusHistory, 'cga_reason',
    DM.WorkOrderCGAReasonLookup, 'code', 'description', 100, 'CGAReasonDescField');

  AddLookupField('assigned_to_short_name', WorkOrderAssignmentHistory, 'assigned_to_id',
    EmployeeDM.EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'AssignedToShortNameField');
end;

procedure TWorkOrderDetailForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  DoneAction.Enabled := DoneAction.Enabled or AttachmentsFrame.FilesWereAttached;
  if (not IsReadOnly) and (ActiveWOFrame = WODetailsWGL) then
    WODetailsWGL.CheckCGAReason;
//  else  //QMANTWO-393
//  if (not IsReadOnly) and (ActiveWOFrame = WODetailsOHM) then //QMANTWO-393
//    WODetailsOHM.CheckCGAReason;  //QMANTWO-393
end;

procedure TWorkOrderDetailForm.ActivatingNow;
begin
  inherited;
  WorkOrderPageControl.ActivePage := DetailsTab;
  HistoryPageControl.ActivePage := WorkOrderStatusTab;

  HistoryTab.TabVisible := PermissionsDM.CanI(RightViewWorkOrderHistory) or PermissionsDM.IsWorkOrderManager;
  //WorkOrderNotesFrame.NoteSubType := '700';
end;

function TWorkOrderDetailForm.WorkOrder: TDataSet;
begin
  Result := WorkOrderDS.DataSet;
  Assert(Assigned(Result));
end;

function TWorkOrderDetailForm.WorkOrderIsClosed: Boolean;
begin
  Result := DM.WorkOrder.FieldByName('closed').AsBoolean;
    ActiveWOFrame.SetWorkOrderClosed(Result);
//    DoneAction.Enabled := Result;      //QMANTWO-393
  if (ActiveWOFrame = WODetailsOHM) or (ActiveWOFrame = WODetailsINR) then
    Result := false;    //QMANTWO-393  //QMANTWO-434 EB Do we close WO
end;

procedure TWorkOrderDetailForm.WorkOrderNotesFrameAddNoteButtonClick(
  Sender: TObject);
begin
  inherited;
  WorkOrderNotesFrame.NoteSubType := FNoteSubTypeList.GetCode(NoteSubTypeCombo.Text);
  WorkOrderNotesFrame.SubTypeText := NoteSubTypeCombo.Text;
  WorkOrderNotesFrame.AddNoteButtonClick(self);
end;

procedure TWorkOrderDetailForm.EnableEditing(Enabled: Boolean);
begin
  SetReadOnly(not Enabled);
  if (ActiveWOFrame = WODetailsWGL) then
    WODetailsWGL.CheckEditingSettings
  else if (ActiveWOFrame = WODetailsINR) then
    WODetailsINR.EnableEditing(True);  //EB QMANTWO-434
end;

procedure TWorkOrderDetailForm.SetReadOnly(const IsReadOnly: Boolean);
begin
  inherited;
  SetControlsReadOnly(IsReadOnly, ActiveWOFrame);
  SetControlsReadOnly(IsReadOnly, WorkOrderNotesFrame.HeaderPanel);
  AttachmentsFrame.ReadOnly := IsReadOnly;
  AttachmentsFrame.AllowAddOnly := PermissionsDM.CanI(RightAttachmentsAddToClosedWorkOrder);
  ActiveWOFrame.PlatAddInButton.enabled := True;  //QMANTWO-231     QMANTWO-260
  PrintButton.Enabled := True;
end;

procedure TWorkOrderDetailForm.GotoWorkOrder(ID: Integer; ComingFromManagementForm: Boolean);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;

  FWorkOrderID := ID;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeViewWorkOrder, IntToStr(ID));

  DM.GoToWorkOrder(ID);
  WorkOrderSource := DM.GetWOSource;
  LoadDetailsFrame(WorkOrderSource);
  TabSetup(WorkOrderSource); //QMANTWO-459 INR Notes
  WOStatusCGAReasonDescription.Visible := (WorkOrderSource = WOSourceWGL);

  ActiveWOFrame.RefreshStatusList;
  ActiveWOFrame.Load(ID);

  ActiveWOFrame.StatusedHow := 'Detail';
  if ComingFromManagementForm then
    ActiveWOFrame.StatusedHow := 'Work Man';

  ShowWorkOrderImage;

  AttachmentsFrame.Initialize(qmftWorkOrder, ID);
  WorkOrderNotesFrame.Initialize(qmftWorkOrder, ID, DM.WorkOrderNotesData);

  // Highlight the notes tab if we have any notes for this work order
  if DM.WorkOrderNotesData.IsEmpty then
    NotesTab.ImageIndex := -1
  else
    NotesTab.ImageIndex := ImageIndexNotes;

  FWorkOrderHistoryDownloaded := False;

  PrepareWorkOrderAddins;
  EnableEditing(not WorkOrderIsClosed);  //QMANTWO-393 controls readonly setting


end;

procedure TWorkOrderDetailForm.ShowWorkOrderImage;
var
  WorkOrderIsXml: Boolean;
begin
  WorkOrderMemo.Text := DM.GetWorkOrderImage(DM.WorkOrder, WorkOrderIsXml);
  WorkOrderMemo.WordWrap := WorkOrderIsXml;
  if WorkOrderIsXML then
    WorkOrderMemo.ScrollBars := ssVertical
  else
    WorkOrderMemo.ScrollBars := ssBoth;
end;



procedure TWorkOrderDetailForm.WorkOrderDSDataChange(Sender: TObject; Field: TField); //todo check that this is firing.
begin
  inherited;
  if Field = nil then begin
    ActiveWOFrame.CurrentStatusOfSelectedWorkOrder := WorkOrder.FieldByName('Status').AsString;
    ActiveWOFrame.RetrieveStatus;
  end;
end;

procedure TWorkOrderDetailForm.DoneActionExecute(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  DoneWithWorkOrder;
end;

procedure TWorkOrderDetailForm.DoneWithWorkOrder;
begin
  if PermissionsDM.CanI(RightAutoAttach) then
    DM.AddAutoAttachments(qmftWorkOrder, FWorkOrderID);

  SaveWorkOrder; //save whether closed or not
  if Assigned(FOnDoneWithWorkOrder) then    //QMANTWO-393
    FOnDoneWithWorkOrder(Self);
end;

procedure TWorkOrderDetailForm.SaveWorkOrder;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;

  Assert(Assigned(WorkOrderDS.DataSet));

  if ActiveWOFrame.NeedToSave then
    ActiveWOFrame.SaveWorkOrderDetails;

  WorkOrderNotesFrame.SaveCachedNotes;
  WorkOrder.Refresh;

  if WorkOrder.FieldByName('closed').AsBoolean = True then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWorkOrderClosed, WorkOrder.FieldByName('wo_id').AsString);
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWorkOrderSaved, WorkOrder.FieldByName('wo_id').AsString);

  DM.FlushDatabaseBuffers;
  EnableEditing(not WorkOrderIsClosed);

  DM.OutstandingWOAttachments := False;  //QMANTWO-815 EB
end;

procedure TWorkOrderDetailForm.WorkOrderPageControlChange(Sender: TObject);
begin
  inherited;
  if WorkOrderPageControl.ActivePage = HistoryTab then begin
    if FWorkOrderID = 0 then
      Exit;
    ShowWorkOrderHistory;
  end;
  AttachmentsFrame.UpdateCameraStatus;
end;

procedure TWorkOrderDetailForm.ShowNotesAsReadOnly(Value: Boolean);
begin
  RestrictionPanel.Visible := Not Value;
  WorkOrderNotesFrame.HeaderPanel.Visible := Not Value;
  NotesTab.Enabled := Not Value;
end;

procedure TWorkOrderDetailForm.ShowWorkOrderHistory;

  procedure FilterOnWorkOrderId(Table: TDBISAMTable);
  begin
    Table.Filter := 'wo_id = ' + IntToStr(FWorkOrderID);
    Table.Filtered := True;
    Table.Open;
  end;

  procedure OpenWorkOrderVersionsDataset;
  const
    SQLMaxArrivalDate =
      'select max(arrival_date) as arrival_date from work_order_version where wo_id = %d';
    SQLWorkOrderVersions =
      'select wv.*, cc.cc_name as call_center, if(wv.arrival_date = ''%s'' then ''*'' else '' '') as active '+
      'from work_order_version wv left join call_center cc on wv.wo_source = cc.cc_code where wv.wo_id = %d';
  var
    DataSet: TDataSet;
  begin
    DataSet := DM.Engine.OpenQuery(Format(SQLMaxArrivalDate, [FWorkOrderID]));
    try
      WorkOrderVersions.SQL.Text := Format(SQLWorkOrderVersions, [AnsiDateTimeToStr(Dataset.FieldByName('arrival_date').AsDateTime, False), FWorkOrderID]);
      AddCalculatedField('image_display', WorkOrderVersions, TStringField, 4000);
      WorkOrderVersions.Open;
    finally
      Dataset.Close;
      Dataset.Free;
    end;
  end;

var
  Cursor: IInterface;
begin
  if FWorkOrderHistoryDownloaded then
    Exit;

  Cursor := ShowHourGlass;
  WorkOrderStatusHistory.Close;
  WorkOrderAssignmentHistory.Close;
  WorkOrderResponseLog.Close;
  WorkOrderVersions.Close;

  if FWorkOrderID > 0 then
    DM.GetWorkOrderHistory(FWorkOrderID);

  FilterOnWorkOrderID(WorkOrderStatusHistory);
  FilterOnWorkOrderID(WorkOrderAssignmentHistory);
  FilterOnWorkOrderID(WorkOrderResponseLog);
  OpenWorkOrderVersionsDataset;

  FWorkOrderHistoryDownloaded := True;
end;

procedure TWorkOrderDetailForm.AttachmentsFrameAddButtonClick(
  Sender: TObject);
begin
  inherited;
  DM.OutstandingWOAttachments := True;  //QMANTWO-815 EB
  AttachmentsFrame.AddAttachmentsActionExecute(Sender);
end;

procedure TWorkOrderDetailForm.ClearSelection;
begin
  ClearSelectionsDBText(HeaderPanel);
  //ActiveWOFrame.ClearSelection;
end;

procedure TWorkOrderDetailForm.PopulateNoteSubType(NoteType: Integer);
begin
  DM.NotesSubTypeList(FNoteSubTypeList, NoteType);
end;

procedure TWorkOrderDetailForm.PrepareWorkOrderAddins;
begin
  if not Assigned(FAttachmentAddin) then
    FAttachmentAddin := TAttachmentsAddinManager.Create(DM,
      AttachmentsFrame.AttachmentAddinButton, ManageAddinButtonClick);

  Assert(Assigned(FAttachmentAddin), 'Attachment addin manager is not assigned');
  FAttachmentAddin.PrepareWorkOrderAttachmentsAddin(HaveWorkOrder);

  if not Assigned(FPlatAddin) then
    FPlatAddin := TPlatAddinManager.Create(DM, ActiveWOFrame.PlatAddinButton, ManageAddinButtonClick);
  ActiveWOFrame.PlatAddinButton.OnClick := ManageAddinButtonClick;//Necessary to programmatically hook events in inherited frames or they won't fire
  FPlatAddin.PrepareForWorkOrder(HaveWorkOrder);
end;

procedure TWorkOrderDetailForm.ManageAddinButtonClick(Sender: TObject);
var
  Cursor: IInterface;
  WorkOrder: TDataSet;
begin
  if not HaveWorkOrder then
    raise EOdDataEntryError.Create('Please select a work order.');

  Cursor := ShowHourGlass;
  WorkOrder := WorkOrderDS.DataSet;
  Assert(Assigned(WorkOrder));

  if Sender = AttachmentsFrame.AttachmentAddinButton then begin
    if FAttachmentAddin.ExecuteForWorkOrder(WorkOrderDS.DataSet,
      AttachmentsFrame.SelectedAttachmentID, qmftWorkOrder,
      AttachmentsFrame.SelectedAttachmentFilename) then
      AttachmentsFrame.RefreshNow;
  end else if Sender = ActiveWOFrame.PlatAddinButton then begin
    FPlatAddin.ExecuteForWorkOrder(WorkOrder);
  end;
end;

procedure TWorkOrderDetailForm.NoteSubTypeComboChange(Sender: TObject);
begin
  inherited;
    WorkOrderNotesFrame.NoteSubType := FNoteSubTypeList.GetCode(NoteSubTypeCombo.Text);
    WorkOrderNotesFrame.SubTypeText := NoteSubTypeCombo.Text;
end;

function TWorkOrderDetailForm.HaveWorkOrder: Boolean;
begin
  Result := Assigned(DM) and Assigned(DM.WorkOrder) and DM.WorkOrder.Active and
    (not DM.WorkOrder.IsEmpty);
end;

procedure TWorkOrderDetailForm.ItemDblClick(Sender: TObject);
begin
  ClearSelection;
  ActiveWOFrame.ClearSelection;
  if Sender is TDBText then
    CopyDBText(Sender as TDBText)
  else if Sender is TDBMemo then
    CopyDBMemo(Sender as TDBMemo)
  else if Sender is TLabel then
    CopyLabelText(Sender as TLabel)
  else if Sender is TDBEdit then
    CopyDBEdit(Sender as TDBEdit);
end;

procedure TWorkOrderDetailForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FAttachmentAddin);
end;

procedure TWorkOrderDetailForm.PrintActionExecute(Sender: TObject);
begin
  inherited;
  if Assigned(OnWorkOrderPrint) then
    OnWorkOrderPrint(Self, FWorkOrderID);
end;

procedure TWorkOrderDetailForm.LeavingNow;
const
  BlankLine = '-';
begin
  inherited;
  if (not TimeFileMonitorDM.NavigatingToTimesheetForImport) and
    (ActiveWOFrame.NeedToSave or WorkOrderNotesFrame.NotesInCache or
    (WorkOrderNotesFrame.NoteText.Lines.Count > 0)) then begin
    case MessageDlg('Do you want to save the current work order changes before leaving this screen? Use Cancel to stay on this screen.', mtConfirmation, [mbYes,mbNo,mbCancel],0) of
      mrYes: begin
        SaveWorkOrder;
        if Assigned(FOnLeavingAndSaveWorkOrder) then
          FOnLeavingAndSaveWorkOrder(Self);
      end;
      mrNo: ActiveWOFrame.CancelWorkOrder;
      mrCancel: Abort;
    end;
    NoteSubTypeCombo.ClearSelection;
  end;

  DM.OutstandingWOAttachments := False;
  if NotEmpty(WorkOrderNotesFrame.NoteText.Text) then
    WorkOrderNotesFrame.NoteText.Clear;

  WorkOrderStatusHistory.Close;
  WorkOrderAssignmentHistory.Close;
  WorkOrderResponseLog.Close;
  //Free the Addin viewer here in case a different kind of work order is needed next time the form is used.
  FreeAndNil(FPlatAddin);
end;

procedure TWorkOrderDetailForm.LoadDetailsFrame(Source : string);
begin
  WODetailsWGL.Visible := False;
  WODetailsLAM01.Visible := False;
  WODetailsOHM.Visible := False; //QMANTWO-393
  WODetailsINR.Visible := False; //WMANTWO-434 EB

  if Source = 'LAM01' then
     ActiveWOFrame := WODetailsLAM01
  else if Source = 'WGL' then
    ActiveWOFrame := WODetailsWGL
  else if Source = 'OHM' then begin
    ActiveWOFrame := WODetailsOHM;  //QMANTWO-393
    If RightStr(trim(DM.WorkOrder.FieldByName('wo_number').asString),1) = 'P' then
      WODetailsOHM.OHMtype := 'Premise'
    else If RightStr(trim(DM.WorkOrder.FieldByName('wo_number').asString),1) = 'G' then
      WODetailsOHM.OHMtype := 'GasMain';
  end
  else if Source = 'INR' then begin
    ActiveWOFrame := WODetailsINR; //QMANTWO-434 EB

  end;

  if Source <>'LAM01' then begin     //QMANTWO-446 EB This can be removed after everyone is upgraded. It is only a sync warning
    if DM.CheckNoWOFilter then
      MessageDlg('Error: Inspection Items missing. Please sync before proceeding.', mtError, [mbOK],0);
  end;

  ActiveWOFrame.WorkOrderSourceStr := Source; //QMANTWO-436 EB
  ActiveWOFrame.Visible := True;
  ActiveWOFrame.WorkOrderDS:= WorkOrderDS;
  if Source = 'LAM01' then
    WODetailsLAM01.TicketsDS := TicketsDS;
  ActiveWOFrame.Setup;

  IsLoaded := True;
end;

procedure TWorkOrderDetailForm.TabSetup(Source: string);
begin
  if (Source = 'INR') or (Source = 'WGL') then   //QMANTWO-506 EB - Makes Notes tab read-only
    ShowNotesAsReadOnly(True)    //QMANTWO-459 EB - This will only set visibility
  else  //QMANTWO-459 EB
    ShowNotesAsReadOnly(False);
end;

procedure TWorkOrderDetailForm.WOAColAddedByGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: String);
begin
  inherited;
  if AText = '' then
    AText := AppName
  else if StrToIntDef(AText, -1) > 1 then
    AText := EmployeeDM.GetEmployeeShortName(StrToInt(AText));
end;

procedure TWorkOrderDetailForm.WODetailsOHMcmbxCurbValueFoundPropertiesChange(      //QMANTWO-393
  Sender: TObject);
begin
  inherited;
  if IsLoaded and (ActiveWOFrame = WODetailsOHM) then begin
    WODetailsOHM.CurbValueChanged := True;
  end;
end;

procedure TWorkOrderDetailForm.WorkOrderVersionsCalcFields(DataSet: TDataSet);
var
  WorkOrderIsXml: Boolean;
begin
  inherited;
  DataSet.FieldByName('image_display').Value := DM.GetWorkOrderImage(DataSet, WorkOrderIsXml);
  SetupImageMemoDisplay(WOVersionImageMemo, WorkOrderIsXml);
end;

procedure TWorkOrderDetailForm.SetupImageMemoDisplay(Memo: TCustomMemo; const WorkOrderIsXml: Boolean);
begin
  if Memo is TMemo then begin
    TMemo(Memo).WordWrap := WorkOrderIsXml;
    if WorkOrderIsXml then
      TMemo(Memo).ScrollBars := ssVertical
    else
      TMemo(Memo).ScrollBars := ssBoth;
  end else if Memo is TDBMemo then begin
    TDBMemo(Memo).WordWrap := WorkOrderIsXml;
    if WorkOrderIsXml then
      TDBMemo(Memo).ScrollBars := ssVertical
    else
      TDBMemo(Memo).ScrollBars := ssBoth;
  end;
end;

procedure TWorkOrderDetailForm.SetWorkOrderSource(Source: string);
begin
   fWorkOrderSource := DM.WorkOrder.FieldByName('wo_source').AsString;
end;

end.
