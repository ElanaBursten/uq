unit PlusMgrList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, 
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, StdCtrls, ExtCtrls, DBClient,
  DMu, LocalEmployeeDMu, ActnList, MSXML2_TLB, OdMSXMLUtils, OdMiscUtils, OdIsoDates,
  Menus, cxCheckBox, cxTextEdit, cxFilter, cxData;

type
  TPlusMgrListForm = class(TEmbeddableForm)
    Headerpnl: TPanel;
    ReportingToLbl: TLabel;
    MgrLbl: TLabel;
    PLUSAllowedOnlyCBx: TCheckBox;
    PlusGrid: TcxGrid;
    PlusView: TcxGridDBTableView;
    ColEmpID: TcxGridDBColumn;
    ColEmpNumber: TcxGridDBColumn;
    ColEmpShortName: TcxGridDBColumn;
    ColFirstName: TcxGridDBColumn;
    ColLastName: TcxGridDBColumn;
    ColModDate: TcxGridDBColumn;
    ColAllowed: TcxGridDBColumn;
    ColPlusCode: TcxGridDBColumn;
    ColPlusDesc: TcxGridDBColumn;
    PlusGridLevel: TcxGridLevel;
    ColModifiedBy: TcxGridDBColumn;
    EmpPlusSource: TDataSource;
    PlusPopup: TPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure PlusViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure PLUSAllowedOnlyCBxClick(Sender: TObject);
    procedure AddWhiteListActionExecute(Sender: TObject);
    procedure RemoveFromWhitelistActionExecute(Sender: TObject);
  private
    fHasPlus: boolean;
    fListLoaded: boolean;
    ManagerID: integer;
    UsingDefMgr: boolean;
    SelectedEmpID: integer;
    SelectedEmp: string;
    procedure Init;
    procedure ClearList;
  public
    PlusListDS: TClientDataset;
    procedure LoadEmpPlusList;
    procedure CreatePlusTable;

    function PopulatePLUSDataSetfromDOM(Doc: IXMLDomDocument; TargetNode: Integer = 0): integer;  //QM-585 Emp Plus Part 3 EB
    procedure ActivatingNow; override;
    function LoadEmpPlusMgrRights: boolean;
    procedure Refresh;
  end;

  const
    MgrNode = '//PLUS_MgrList';

var
  PlusMgrListForm: TPlusMgrListForm;

implementation
uses
  QMConst;

{$R *.dfm}

procedure TPlusMgrListForm.ActivatingNow;
var
  CanDebug: Boolean;
begin
  inherited;
  fListLoaded := FALSE;
  fHasPlus:= FALSE;
  if UsingDefMgr then begin  {Tells us whether to load people under the logged in user}
    ManagerID := DM.EmpID;
  end;

  LoadEmpPlusList;
  
  {Open up the background fields so that they can be viewed in Debug}
  if DM.UQState.DeveloperMode then
    CanDebug := True
  else
    CanDebug := False;

  ColEmpID.VisibleForCustomization := CanDebug;

end;




procedure TPlusMgrListForm.ClearList;
begin
  PlusListDS.EmptyDataSet;
  flistLoaded := False;
end;

procedure TPlusMgrListForm.CreatePlusTable;
begin
  PlusListDS := TClientDataSet.Create(Application);
  PlusListDS.FieldDefs.Add('emp_id', ftInteger);
  PlusListDS.FieldDefs.Add('short_name', ftString, 30);
  PlusListDS.FieldDefs.Add('first_name', ftString, 30);
  PlusListDS.FieldDefs.Add('last_name', ftString, 30);
  PlusListDS.FieldDefs.Add('emp_number', ftString, 20);
  PlusListDS.FieldDefs.Add('tg_id', ftInteger);
  PlusListDS.FieldDefs.Add('tg_active', ftBoolean);
  PlusListDS.FieldDefs.Add('PLUS_code', ftString, 100);
  PlusListDS.FieldDefs.Add('PLUS_desc', ftString, 100);
  PlusListDS.FieldDefs.Add('modified_date', ftDateTime);
  PlusListDS.FieldDefs.Add('modified_by', ftString, 100);

  PlusListDS.CreateDataSet;

end;

procedure TPlusMgrListForm.FormCreate(Sender: TObject);
begin
  inherited;
  Init;
end;


procedure TPlusMgrListForm.Init;
begin
  UsingDefMgr := True;
  MgrLbl.Caption := EmployeeDM.GetEmployeeShortName(DM.EmpID);
  CreatePlusTable;
  LoadEmpPlusMgrRights;
end;

procedure TPlusMgrListForm.LoadEmpPlusList;
var
  Doc: IXMLDomDocument;
  RecCount: integer;
begin
  Doc := ParseXml(EmployeeDM.GetEmpPlusMgrList(ManagerID));
  RecCount := PopulatePLUSDataSetfromDOM(Doc, ManagerID);
  MgrLbl.Caption := EmployeeDM.GetEmployeeShortName(ManagerID);
  PlusGrid.SetFocus;
  fListLoaded := True;
end;



function TPlusMgrListForm.LoadEmpPlusMgrRights: boolean;
      procedure AddMenuItem(Caption: string; TermGroup: integer; ToAdd:boolean);
      var
        NewMenuItemAdd: TMenuItem;
        MenuCount: integer;
      begin
        NewMenuItemAdd := TMenuItem.Create(PlusPopup);
        MenuCount := PlusPopup.Items.Count;
        if Caption ='-' then begin
          NewMenuItemAdd.Caption := Caption;
          PlusPopup.Items.Add(NewMenuItemAdd);
        end
        else begin
          PlusPopup.Items.Add(NewMenuItemAdd);
          if ToAdd then
            NewMenuItemAdd.OnClick := AddWhiteListActionExecute
          else
           NewMenuItemAdd.OnClick := RemoveFromWhiteListActionExecute;

          NewMenuItemAdd.Caption := Caption;
          NewMenuItemAdd.Tag := TermGroup;
        end;
      end;
var
  PlusStr: string;
  WhiteList: Tstringlist;
  i, outTag: integer;
begin
  PlusStr := EmployeeDM.GetPLUSForManager(DM.EmpID);
  if PlusStr > '' then begin
    Try
      fHasPlus := True;
      WhiteList := TStringList.Create;
      WhiteList.StrictDelimiter := True;
      WhiteList.Delimiter := '^';

      WhiteList.DelimitedText := PlusStr;
      for i := 0 to WhiteList.Count - 1 do begin
        if not tryStrToInt(WhiteList.Names[i], outtag) then
          outtag := 0;
        AddMenuItem('Add to ' + WhiteList.ValueFromIndex[i], outtag, True );
        AddMenuItem('Remove from ' + WhiteList.ValueFromIndex[i], outtag, False);
        AddMenuItem('-', 0, False);
      end;

    Finally
      freeandnil(WhiteList);
    End;

  end;
  result := True;
end;

procedure TPlusMgrListForm.PlusViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);

begin
  inherited;
  {These are applied after any style settings in Grid}
    if not AViewInfo.Selected then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[ColAllowed.Index]) then begin
        If (AViewInfo.GridRecord.Values[ColAllowed.Index] = TRUE) then begin
            ACanvas.Brush.Color := clMoneyGreen;
        end
        else if (not VarIsNull(AViewInfo.GridRecord.Values[ColPlusCode.Index])) and
                             (AViewInfo.GridRecord.Values[ColPlusCode.Index] <> '-') and
                             (AViewInfo.GridRecord.Values[ColAllowed.Index] = FALSE) then
          ACanvas.Brush.Color := clLightRed
        else
          ACanvas.Brush.Color :=clCream
      end;
    end

    else {Selected Row}
      if not VarIsNull(AViewInfo.GridRecord.Values[ColAllowed.Index]) then
        If (AViewInfo.GridRecord.Values[ColPlusCode.Index] <> '-') and
           (AViewInfo.GridRecord.Values[ColAllowed.Index] = FALSE) then
            ACanvas.Brush.Color := clMaroon;
end;


function TPlusMgrListForm.PopulatePLUSDataSetfromDOM(Doc: IXMLDomDocument;
  TargetNode: Integer): integer;
var
  i: integer;
  Nodes: IXMLDOMNodeList;
  Elem: IXMLDOMElement;

  {Copying from WorkManageUtils - could refactor later}
  function AttrText(const AttrName: string): string;
  var
    AttrNode: IXMLDomNode;
  begin
    AttrNode := Elem.attributes.getNamedItem(AttrName);
    if AttrNode <> nil then
      Result := AttrNode.text
    else
      Result := '-';
  end;

  function AttrInt(const AttrName: string): Integer;
  var
    AttrStr: string;
  begin
    AttrStr := AttrText(AttrName);
    try
      Result := StrToInt(AttrStr);
    except
      on E: Exception do begin
        E.Message := Format('Invalid Integer text "%s" for attribute "%s"', [AttrStr, AttrName]);
        raise;
      end;
    end;
  end;

  function AttrDateTime(const AttrName: string): Variant;
  var
    AttrStr: string;
  begin
    Result := Null;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and IsoIsValidDateTime(AttrStr) then
      Result := IsoStrToDateTime(AttrStr);
  end;

  function AttrFloat(const AttrName: string): Double;
  var
    AttrStr: string;
  begin
    Result := 0.0;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and (AttrStr <> NULL) then
      Result := StrToFloat(AttrStr);
  end;
{BEGIN}
begin
  PlusListDS.DisableControls;
  while not PlusListDS.IsEmpty do
    PlusListDS.Delete;

  try
    Nodes := Doc.SelectNodes(MgrNode);
    {Process the records}
    for i := 0 to Nodes.Length-1 do begin
      Elem := Nodes[i] as IXMLDOMElement;

      PlusListDS.Open;
      PlusListDS.Append;
      {----}
      PlusListDS.FieldByName('emp_id').asInteger := AttrInt('emp_id');
      PlusListDS.FieldByName('short_name').AsString := AttrText('short_name');
      PlusListDS.FieldByName('first_name').AsString := AttrText('first_name');
      PlusListDS.FieldByName('last_name').AsString := AttrText('last_name');
      PlusListDS.FieldByName('emp_number').AsString := AttrText('emp_number');
      if AttrText('tg_id') <> '-' then
        PlusListDS.FieldByName('tg_id').AsInteger:= AttrInt('tg_id');
      if (AttrText('tg_active') <> '-') and (AttrInt('tg_active') > 0) then
        PlusListDS.FieldByName('tg_active').AsBoolean := True
      else
        PlusListDS.FieldByName('tg_active').AsBoolean := False;

      PlusListDS.FieldByName('PLUS_code').Asstring := AttrText('PLUS_code');

      PlusListDS.FieldByName('PLUS_desc').Asstring := AttrText('PLUS_desc');
      if AttrText('modified_date') <> '-' then
        PlusListDS.FieldByName('modified_date').AsDateTime := AttrDateTime('modified_date');
      PlusListDS.FieldByName('modified_by').AsString := AttrText('modified_by');

      {----}
      PlusListDS.Post;
    end;
  finally                        
    PlusListDS.Open;
    EmpPlusSource.DataSet := PlusListDS;
    PlusListDS.First;
    PlusListDS.EnableControls;
  end;
end;

procedure TPlusMgrListForm.Refresh;
var
  RecNum: integer;
begin
 try
   RecNum := PlusView.DataController.FocusedRecordIndex;
 except
   RecNum:= 0;
 end;
 Clearlist;
 LoadEmpPlusList;
 try
   if RecNum > 0 then
     PlusView.DataController.FocusedRecordIndex := RecNum;
 except
   {Swallow}
 end;
end;



procedure TPlusMgrListForm.AddWhiteListActionExecute(Sender: TObject);
var
  SelWhiteList: integer;
begin
  inherited;

  SelectedEmpID := PlusListDS.FieldByName('emp_id').AsInteger;
  SelectedEmp := EmployeeDM.GetEmployeeShortName(SelectedEmpID);

  With Sender as TMenuItem do begin
    SelWhiteList := tag;
    ShowMessage('Adding ' +  SelectedEmp + ' to ' +  inttostr(tag));
  end;
  EmployeeDM.AddEmptoPlusWhiteList(SelectedEmpID, SelWhiteList); //QM-585 EB PLUS Whitelist

  Refresh;
end;



procedure TPlusMgrListForm.RemoveFromWhitelistActionExecute(Sender: TObject);
var
  SelWhiteList: integer;

begin
  inherited;
  SelectedEmpID := PlusListDS.FieldByName('emp_id').AsInteger;
  SelectedEmp := EmployeeDM.GetEmployeeShortName(SelectedEmpID); //QM-585 EB PLUS Whitelist

  With Sender as TMenuItem do begin
    SelWhiteList := tag;
    ShowMessage('Removing ' +  SelectedEmp + ' from ' +  inttostr(tag));
  end;
  EmployeeDM.RemoveEmpFromPlusWhiteList(SelectedEmpID, SelWhiteList); //QM-585 EB PLUS Whitelist

  Refresh;
end;

procedure TPlusMgrListForm.PLUSAllowedOnlyCBxClick(Sender: TObject);
begin
  inherited;
  if PlusAllowedOnlyCBx.Checked then begin
    PlusListDS.Filter := 'tg_active = TRUE';
    PlusListDS.Filtered := True;
  end
  else begin
    PlusListDS.Filter := '';
    PlusListDS.Filtered := False;
  end;
end;



end.
