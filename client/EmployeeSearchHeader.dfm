inherited EmployeeSearchCriteria: TEmployeeSearchCriteria
  Caption = 'Employee Search'
  ClientHeight = 112
  ClientWidth = 613
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 34
    Top = 16
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'First Name'
  end
  object Label2: TLabel [1]
    Left = 33
    Top = 40
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Last Name'
  end
  object Label3: TLabel [2]
    Left = 28
    Top = 64
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Short Name'
  end
  object Label4: TLabel [3]
    Left = 28
    Top = 88
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Employee #'
  end
  inherited RecordsLabel: TLabel
    Left = 288
    Top = 60
  end
  inherited SearchButton: TButton
    Left = 288
    Top = 80
  end
  inherited CancelButton: TButton
    Left = 528
    Top = 80
  end
  inherited CopyButton: TButton
    Left = 368
    Top = 80
    TabOrder = 9
  end
  inherited ExportButton: TButton
    Left = 448
    Top = 80
    TabOrder = 6
  end
  inherited PrintButton: TButton
    TabOrder = 10
  end
  object FirstName: TEdit
    Left = 92
    Top = 12
    Width = 161
    Height = 21
    TabOrder = 2
  end
  object LastName: TEdit
    Left = 92
    Top = 36
    Width = 161
    Height = 21
    TabOrder = 3
  end
  object ShortName: TEdit
    Left = 92
    Top = 60
    Width = 161
    Height = 21
    TabOrder = 4
  end
  object EmployeeNumber: TEdit
    Left = 92
    Top = 85
    Width = 160
    Height = 21
    TabOrder = 7
  end
  object CheckBox1: TCheckBox
    Left = 288
    Top = 10
    Width = 305
    Height = 17
    Caption = 'Show managers only'
    TabOrder = 8
  end
end
