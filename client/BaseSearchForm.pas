unit BaseSearchForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ComCtrls, SearchHeader, MSXML2_TLB, ExtCtrls,
  DMu, DB, DBISAMTb, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridLevel, cxGrid, OdXMLToDataSet, OdDBISAMUtils, cxNavigator,
  Menus, ActnList, cxCustomData, cxFilter, cxData;

type
  TSearchForm = class(TEmbeddableForm)
    NothingFoundPanel: TPanel;
    SearchTable: TDBISAMTable;
    SearchSource: TDataSource;
    SchemaTable: TDBISAMTable;
    SaveDlg: TSaveDialog;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    GridView: TcxGridDBTableView;
    procedure SearchButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure ExportButtonClick(Sender: TObject);
    procedure CopyButtonClick(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PrintButtonClick(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    procedure GridViewFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    fExplicitRowSelect: boolean;
    procedure ReloadLocateGrid;
    procedure SetCriteria(ACriteria: TSearchCriteria);
    procedure SetCriteriaClass(const AClass: TSearchCriteriaClass);
    procedure SetRecordCount(Dom: IXMLDOMDocument);
    procedure DoGridNodeChanged;
    procedure SetOnItemPrint(const Value: TItemSelectedEvent);
  protected
    FCriteria: TSearchCriteria;
    FCriteriaClass: TSearchCriteriaClass;
    FShowTicketLocates: Boolean;
    FLocalSearch: Boolean;
    FAllowCancel: Boolean;
    DOM: IXMLDOMDocument;
    FOnItemSelected: TItemSelectedEvent;
    FHeaderWidth: Integer;
    FOnItemPrint: TItemSelectedEvent;
    FBlockGridUpdates: Boolean;
    procedure DoItemSelected; virtual;
    procedure SizeFormToFitCriteria;
    procedure EmptySearchTable;
    procedure DoItemPrint; virtual;
  public
    property LocalSearch: Boolean read FLocalSearch write FLocalSearch;
    property AllowCancel: Boolean read FAllowCancel write FAllowCancel;
    property OnItemSelected: TItemSelectedEvent read FOnItemSelected write FOnItemSelected;
    property Criteria: TSearchCriteria read FCriteria write SetCriteria;
    property CriteriaClass: TSearchCriteriaClass read FCriteriaClass write SetCriteriaClass;
    property OnItemPrint: TItemSelectedEvent read FOnItemPrint write SetOnItemPrint;
    property ShowTicketLocates: boolean read fShowticketLocates write fShowTicketLocates;
    constructor CreateWithCriteria(AOwner: TComponent; ACriteria: TSearchCriteriaClass);
    destructor Destroy; override;
    function GetSelectedFieldValue(const FieldName: string): Variant;

  end;

implementation

{$R *.dfm}

uses OdHourGlass, OdExceptions, OdMiscUtils, Math,
  OdCxXmlUtils, OdDbUtils, Clipbrd, OdMSXMLUtils, OdCxUtils,
  LocateGridDetailForm, TicketSearchHeader; //QMANTWO-422

{ TSearchForm }


constructor TSearchForm.CreateWithCriteria(AOwner: TComponent; ACriteria: TSearchCriteriaClass);
begin
  Assert(Assigned(ACriteria));
  Create(Owner);
  CriteriaClass := ACriteria;
end;

procedure TSearchForm.SearchButtonClick(Sender: TObject);
var
  DOM: IXMLDOMDocument;
  Cursor: IInterface;
  HaveMatches: Boolean;
begin
  fExplicitRowSelect := False; //QMANTWO-422 New Search
  Assert(Assigned(Criteria));
  EmptySearchTable;
  Cursor := ShowHourGlass;
  try
    Criteria.DefineColumns;
    DOM := ParseXml(Criteria.GetXmlString);
    SetRecordCount(DOM);

    Grid.BeginUpdate;
    SearchTable.DisableControls;
    FBlockGridUpdates := True;
    try
      ConvertXMLToDataSet(DOM, Criteria.Defs, SearchTable, Criteria.CustomizeDataSet);
      FHeaderWidth := CreateGridColumnsFromDef(GridView, SearchTable, Criteria.Defs);
      SortGridByFirstVisibleColumn(Grid);
    finally
      SearchTable.EnableControls;
      Grid.EndUpdate;
      FBlockGridUpdates := False;
    end;
    HaveMatches := SearchTable.RecordCount > 0;
    NothingFoundPanel.Left := Grid.Left + 25;
    NothingFoundPanel.Top := Grid.Top + 40;
    NothingFoundPanel.Visible := not HaveMatches;
    FocusFirstGridRow(Grid);
    if SearchTable.RecordCount = 1 then // if only 1 result was returned, FocusFirstGridRow() doesn't fire OnFocusedRecordChanged
      DoGridNodeChanged;
    Criteria.ExportButton.Enabled := HaveMatches;
    Criteria.CopyButton.Enabled := HaveMatches;
    Criteria.CustomizeGrid(GridView);
    fExplicitRowSelect := True;  //QMANTWO-422
    ReloadLocateGrid;
    if Grid.CanFocus then
      Grid.SetFocus;
  except
    on E: EOdCriteriaRequiredException do begin
      ShowExceptionError(E);  // Catch it here, don't log it
    end
  end;
end;

procedure TSearchForm.CancelButtonClick(Sender: TObject);
begin
  Assert(Assigned(Criteria));
  ModalResult := mrCancel;
end;

destructor TSearchForm.Destroy;
begin
  DOM := nil;
  inherited;
end;

procedure TSearchForm.GridDblClick(Sender: TObject);
begin
  if HaveFocusedGridViewRow(GridView) then begin
    ModalResult := mrOK;
    DoItemSelected;
  end
  else
    ModalResult := mrCancel;
end;

function TSearchForm.GetSelectedFieldValue(const FieldName: string): Variant;
var
  Field: TField;
begin
  Assert(not SearchTable.IsEmpty);
  Assert(not (Trim(FieldName) = ''));
  Field := SearchTable.FindField(FieldName);
  if Assigned(Field) then
    Result := Field.Value
  else
    Result := Null;
end;

procedure TSearchForm.FormShow(Sender: TObject);
begin
  inherited;
  Criteria.CancelButton.Visible := IsModal;
  Criteria.Showing;
  Realign;
//  fExplicitRowSelect := False; //QMANTWO-422
end;

procedure TSearchForm.DoItemSelected;
var
  ID: Variant;
begin
  ID := GetSelectedFieldValue(Criteria.Defs.GetPrimaryKeyFieldName);
  Criteria.BeforeItemSelected(ID);
  if Assigned(FOnItemSelected) then
    FOnItemSelected(Self, ID);
end;

procedure TSearchForm.SetCriteria(ACriteria: TSearchCriteria);
begin
  FCriteria := ACriteria;
end;

procedure TSearchForm.SetCriteriaClass(const AClass: TSearchCriteriaClass);
begin
  if Assigned(FCriteriaClass) then
    Exit;
  Grid.BeginUpdate;
  try
    FCriteriaClass := AClass;
    FCriteria := CreateEmbeddedForm(AClass, Self, Self) as TSearchCriteria;
    Grid.Align := alClient;
    Criteria.Align := alTop;
    Criteria.Visible := True;
    Caption := Criteria.Caption;
    Realign;
    Criteria.SearchButton.OnClick := SearchButtonClick;
    Criteria.CancelButton.OnClick := CancelButtonClick;
    Criteria.ExportButton.OnClick := ExportButtonClick;
    Criteria.CopyButton.OnClick := CopyButtonClick;
    Criteria.PrintButton.OnClick := PrintButtonClick;
    Criteria.ClearButton.OnClick := ClearButtonClick;
    SizeFormToFitCriteria;
  finally
    Grid.EndUpdate;
  end;
end;

procedure TSearchForm.SizeFormToFitCriteria;
var
  i: Integer;
  Right: Integer;
  MaxRight: Integer;
  Ctrl: TControl;
begin
  MaxRight := Criteria.Width;
  for i := 0 to Criteria.ControlCount - 1 do begin
    Ctrl := Criteria.Controls[i];
    Right := Ctrl.Left + Ctrl.Width;
    MaxRight := Max(MaxRight, Right + 10);
  end;
  ClientWidth := MaxRight;
end;



procedure TSearchForm.EmptySearchTable;
begin
  SearchTable.Close;
  if SearchTable.Exists then
    SearchTable.DeleteTable;
  If ShowTicketLocates then     //QMANTWO-422  Need to blank out the panel if it is Ticket Search criteria
    TTicketSearchCriteria(Criteria).LocatePanel.Visible := False;
end;

procedure TSearchForm.ExportButtonClick(Sender: TObject);
var
  m: TMemoryStream;
  Fields: TStringList;
begin
  if SaveDlg.Execute then begin
    m := TMemoryStream.Create;
    Screen.Cursor := crHourGlass;
    try
      m.SetSize(100000);  // Start with it large to avoid reallocations
      Fields := TStringList.Create;
      try
        Fields.Text := SearchTable.FieldList.Text; // D2007 FieldList is a TWideStringList
        SearchTable.First;
        SaveDelimToStream(SearchTable, m, #9, True, Fields);
        SearchTable.First;
      finally
        FreeAndNil(Fields);
      end;
      m.SetSize(m.Position);
      m.SaveToFile(SaveDlg.FileName);
      ShowMessage('Data saved to ' + SaveDlg.FileName);
    finally
      FreeAndNil(m);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TSearchForm.CopyButtonClick(Sender: TObject);
var
  m: TMemoryStream;
  Buf: string;
  Fields: TStringList;
begin
  m := TMemoryStream.Create;
  Screen.Cursor := crHourGlass;
  try
    Buf := #0;
    m.SetSize(100000);  // Start with it large to avoid reallocations
    Fields := TStringList.Create;
    try
      Fields.Text := SearchTable.FieldList.Text;
      SearchTable.First;
      SaveDelimToStream(SearchTable, m, #9, True, Fields);
      SearchTable.First;
    finally
      FreeAndNil(Fields);
    end;
    m.Write(Buf[1], 1);
    m.SetSize(m.Position);
    Clipboard.SetTextBuf(m.Memory);
    ShowMessage('The data can now be pasted into Excel.');
  finally
    FreeAndNil(m);
    Screen.Cursor := crDefault;
  end;
end;

procedure TSearchForm.SetRecordCount(Dom: IXMLDOMDocument);

  function GetIntAttr(Node: IXMLDOMNode; Attr: string): Integer;
  var
    Attribute: IXMLDOMNode;
  begin
    Result := -1;
    Attribute := Node.attributes.getNamedItem(Attr);
    if Assigned(Attribute) then
      Result := StrToIntDef(Attribute.text, -1);
  end;

var
  Matching: Integer;
  Max: Integer;
  Node: IXmlDomNode;
begin
  // <rows MaxRows="1000" ActualRows="59" TotalRows="59">
  FCriteria.RecordsLabel.Visible := False;
  Node := Dom.selectSingleNode('//rows');
  if Assigned(Node) then begin
    Matching := GetIntAttr(Node, 'ActualRows');
    Max := GetIntAttr(Node, 'MaxRows');
    FCriteria.UpdateRecordCount(Matching, Max);
  end
end;


procedure TSearchForm.GridViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  ReloadLocateGrid;
end;


procedure TSearchForm.DoGridNodeChanged;
begin
  Assert(Assigned(Criteria));
  Criteria.GridNodeChanged(GridView, GridView.Controller.FocusedRow);
end;

procedure TSearchForm.PrintButtonClick(Sender: TObject);
begin
  if HaveFocusedGridViewRow(GridView) then begin
    ModalResult := mrOK;
    DoItemPrint;
  end;
end;

procedure TSearchForm.ReloadLocateGrid;
var
  ID : Integer;
  TicketNum : string;
  PrimKeyFieldName: string;
begin
  if not FBlockGridUpdates then
    DoGridNodeChanged;
  if ShowTicketLocates and fExplicitRowSelect then begin
    try
      PrimKeyFieldName := Criteria.Defs.GetPrimaryKeyFieldName;
      ID := GetSelectedFieldValue(PrimKeyFieldName);
      if ID > 0 then begin
        TicketNum := GetSelectedFieldValue('ticket_number');
        TTicketSearchCriteria(Criteria).LocateGrid.LoadLocateGrid(ID, TicketNum);
        TTicketSearchCriteria(Criteria).LocatePanel.Visible := True;
        TTicketSearchCriteria(Criteria).LocateGrid.Align := alClient;
        TTicketSearchCriteria(Criteria).LocateGrid.Visible := True;
      end
      else begin
        TTicketSearchCriteria(Criteria).LocateGrid.Visible := False;
        TTicketSearchCriteria(Criteria).LocatePanel.Visible := False;
      end;
    except
      TTicketSearchCriteria(Criteria).LocateGrid.Visible := False;
      TTicketSearchCriteria(Criteria).LocatePanel.Visible := False;
    end;
  end;
end;

procedure TSearchForm.ClearButtonClick(Sender: TObject);
begin
  Criteria.InitializeSearchCriteria;
end;

procedure TSearchForm.SetOnItemPrint(const Value: TItemSelectedEvent);
begin
  FOnItemPrint := Value;
  Criteria.PrintButton.Visible := Assigned(FOnItemPrint);
end;

procedure TSearchForm.DoItemPrint;
var
  ID: Integer;
begin
  ID := GetSelectedFieldValue(Criteria.Defs.GetPrimaryKeyFieldName);
  Criteria.BeforeItemPrint(ID);
  if Assigned(FOnItemPrint) then
    FOnItemPrint(Self, ID);
end;

end.


