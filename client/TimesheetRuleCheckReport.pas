unit TimesheetRuleCheckReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, Spin,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TTimesheetRuleCheckReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    ManagersComboBox: TComboBox;
    ChooseDate: TcxDateEdit;
    DepthLabel: TLabel;
    Depth: TSpinEdit;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure ChooseDateChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdMiscUtils, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TTimesheetRuleCheckReportForm }

procedure TTimesheetRuleCheckReportForm.InitReportControls;
begin
  inherited;
  ChooseDate.Date := ThisSaturday;
  SetUpManagerList(ManagersComboBox);

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TTimesheetRuleCheckReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimeRuleCheck');
  SetParamDate('end_date', ChooseDate.Date);
  SetParamIntCombo('emp_id', ManagersComboBox, True);
  SetParamInt('HierarchyDepth', Depth.Value);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TTimesheetRuleCheckReportForm.ChooseDateChange(Sender: TObject);
begin
  inherited;
  if ChooseDate.Date <> SaturdayIze(ChooseDate.Date) then
    ChooseDate.Date := SaturdayIze(ChooseDate.Date);
end;

end.
