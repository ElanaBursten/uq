unit PotentialHazard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TPotentialHazardDialog = class(TForm)
    PotentialHazardLabel1: TLabel;
    Label1: TLabel;
    OKButton: TButton;
    FirstNameEdit: TEdit;
    LastNameEdit: TEdit;
    ConfirmationNumberEdit: TEdit;
    FirstNameLabel: TLabel;
    LastNameLabel: TLabel;
    ConfirmationNumberLabel: TLabel;
    HazardMemo: TMemo;
    procedure OKButtonClick(Sender: TObject);
    procedure DataChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
  public
    class function GetPotentialHazardData(const CheckedHazards: string; var FirstName, LastName, ConfirmationNumber, AlertPhoneNo: string): Boolean;
  end;


implementation
uses OdMiscUtils, OdExceptions;

{$R *.dfm}


{ TPotentialHazardDialog }

procedure TPotentialHazardDialog.DataChange(Sender: TObject);
begin
  OKButton.Enabled := NotEmpty(FirstNameEdit.Text) and NotEmpty(LastNameEdit.Text) and NotEmpty(ConfirmationNumberEdit.Text);
end;

procedure TPotentialHazardDialog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := ModalResult = mrOk;
end;

class function TPotentialHazardDialog.GetPotentialHazardData(const CheckedHazards: string; var FirstName, LastName, ConfirmationNumber, AlertPhoneNo: string): Boolean;
var
  Dialog: TPotentialHazardDialog;
begin
  if AlertPhoneNo = '' then
    AlertPhoneNo := '(703) 750-1000';

  Dialog := TPotentialHazardDialog.Create(nil);
  try
    Dialog.Label1.Caption := 'Call the WGL Hotline at ' + AlertPhoneNo + '. Record the first and last name of the person you spoke with and the confirmation number they gave you.';
    Dialog.HazardMemo.Text := CheckedHazards;
    Dialog.FirstNameEdit.Text := FirstName;
    Dialog.LastNameEdit.Text := LastName;
    Dialog.ConfirmationNumberEdit.Text := ConfirmationNumber;
    Result := Dialog.ShowModal = mrOk;
    if Result then begin
      FirstName := Dialog.FirstNameEdit.Text;
      LastName := Dialog.LastNameEdit.Text;
      ConfirmationNumber := Dialog.ConfirmationNumberEdit.Text;
      if IsEmpty(FirstName) or IsEmpty(LastName) or IsEmpty(ConfirmationNumber) then
        Result := False;
    end
  finally
    FreeAndNil(Dialog);
  end;
end;


procedure TPotentialHazardDialog.OKButtonClick(Sender: TObject);
begin
  if IsEmpty(FirstNameEdit.Text) or IsEmpty(LastNameEdit.Text) or IsEmpty(ConfirmationNumberEdit.Text) then
    raise EOdDataEntryError.Create('First Name, Last Name and Confirmation Number are required.')
  else
    ModalResult := mrOk;
end;

end.
