unit ResponderStatusTranslationsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst;

type
  TResponderStatusTranslationsReportForm = class(TReportBaseForm)
    lblResponderActive: TLabel;
    ActiveStatusCombo: TComboBox;
    Label3: TLabel;
    CallCenterList: TCheckListBox;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;


implementation
uses DMu, OdVclUtils, OdExceptions, OdMiscUtils;

{$R *.dfm}

procedure TResponderStatusTranslationsReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  DM.PopulateActiveStatusList(ActiveStatusCombo.Items);
  ActiveStatusCombo.ItemIndex := 0;
end;

procedure TResponderStatusTranslationsReportForm.SelectAllButtonClick(
  Sender: TObject);
begin
  SetCheckListBox(CallCenterList, True);
end;

procedure TResponderStatusTranslationsReportForm.SelectNoneButtonClick(
  Sender: TObject);
begin
  SetCheckListBox(CallCenterList, False);
end;

procedure TResponderStatusTranslationsReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('ResponderStatusTranslations');
  SetParamInt('ActiveFlag', GetComboObjectInteger(ActiveStatusCombo));

  CheckedItems := GetCheckedItemCodesString(CallCenterList);
  if IsEmpty(CheckedItems) then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one call center for the report.');
  end;

  if IsAllCheckListBox(CallCenterList, True) then
    CheckedItems := '*';
  SetParam('CallCenters', CheckedItems);
end;

end.
