unit LoadReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls;

type
  TLoadReportForm = class(TReportBaseForm)
    Label2: TLabel;
    ManagersComboBox: TComboBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdVclUtils, LocalEmployeeDMu;

{ TLoadReportForm }

procedure TLoadReportForm.ValidateParams;
begin
  inherited;
  SetReportID('LocatorLoad');
  SetParamIntCombo('manager_id', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TLoadReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  if ManagersComboBox.Items.Count > 1 then
    ManagersComboBox.ItemIndex := 0;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.




