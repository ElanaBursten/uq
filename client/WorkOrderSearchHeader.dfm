inherited WorkOrderSearchCriteria: TWorkOrderSearchCriteria
  Left = 422
  Top = 224
  Caption = 'Work Order Search'
  ClientHeight = 208
  ClientWidth = 631
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 13
    Top = 38
    Width = 72
    Height = 13
    Caption = 'Ticket Number:'
  end
  object Label2: TLabel [1]
    Left = 13
    Top = 62
    Width = 34
    Height = 13
    Caption = 'Street:'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 86
    Width = 23
    Height = 13
    Caption = 'City:'
  end
  object Label4: TLabel [3]
    Left = 13
    Top = 110
    Width = 30
    Height = 13
    Caption = 'State:'
  end
  object ClientNameLabel: TLabel [4]
    Left = 234
    Top = 14
    Width = 61
    Height = 13
    Caption = 'Client Name:'
  end
  object Label8: TLabel [5]
    Left = 234
    Top = 38
    Width = 49
    Height = 13
    Caption = 'Company:'
  end
  object CountyLabel: TLabel [6]
    Left = 13
    Top = 134
    Width = 39
    Height = 13
    Caption = 'County:'
  end
  inherited RecordsLabel: TLabel
    Left = 316
    Top = 182
    Width = 113
  end
  object WOSourceLabel: TLabel [8]
    Left = 13
    Top = 158
    Width = 58
    Height = 13
    Caption = 'WO Source:'
  end
  object Label11: TLabel [9]
    Left = 234
    Top = 158
    Width = 24
    Height = 13
    Caption = 'Kind:'
  end
  object UQDamageIDLabel: TLabel [10]
    Left = 13
    Top = 14
    Width = 71
    Height = 13
    Caption = 'Work Order #:'
  end
  object TicketsLabel: TLabel [11]
    Left = 13
    Top = 182
    Width = 37
    Height = 13
    Caption = 'Tickets:'
  end
  inherited SearchButton: TButton
    Left = 540
    Top = 16
    TabOrder = 13
  end
  object TicketNumber: TEdit [13]
    Left = 92
    Top = 34
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Street: TEdit [14]
    Left = 92
    Top = 58
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object City: TEdit [15]
    Left = 92
    Top = 82
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object State: TEdit [16]
    Left = 92
    Top = 106
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
  end
  object ClientName: TEdit [17]
    Left = 316
    Top = 10
    Width = 197
    Height = 21
    TabOrder = 8
  end
  object Company: TEdit [18]
    Left = 316
    Top = 34
    Width = 197
    Height = 21
    TabOrder = 9
  end
  inline DueDateFrame: TOdRangeSelectFrame [19]
    Left = 217
    Top = 55
    Width = 299
    Height = 52
    TabOrder = 10
    inherited FromLabel: TLabel
      Left = 17
      Top = 7
      Width = 76
      Caption = 'Due Date From:'
    end
    inherited ToLabel: TLabel
      Left = 190
      Top = 7
      Alignment = taRightJustify
    end
    inherited DatesLabel: TLabel
      Left = 17
      Top = 31

    end
    inherited DatesComboBox: TComboBox
      Left = 99
      Top = 27
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 99
      Top = 3
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 210
      Top = 3
    end
  end
  object County: TEdit [20]
    Left = 92
    Top = 130
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 5
  end
  inline TransmitDateFrame: TOdRangeSelectFrame [21]
    Left = 217
    Top = 103
    Width = 300
    Height = 50
    TabOrder = 11
    inherited FromLabel: TLabel
      Left = 17
      Top = 7
      Width = 75
      Caption = 'Rcv Date From:'
    end
    inherited ToLabel: TLabel
      Left = 190
      Top = 7
      Alignment = taRightJustify
    end
    inherited DatesLabel: TLabel
      Left = 17
      Top = 31
    end
    inherited DatesComboBox: TComboBox
      Left = 99
      Top = 27
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 99
      Top = 3
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 210
      Top = 3
    end
  end
  inherited CancelButton: TButton [22]
    Left = 540
    Top = 145
    TabOrder = 17
  end
  inherited CopyButton: TButton [23]
    Left = 540
    Top = 47
    TabOrder = 14
  end
  inherited ExportButton: TButton [24]
    Left = 540
    Top = 79
    TabOrder = 15
  end
  inherited PrintButton: TButton
    Left = 540
    Top = 112
    Enabled = True
    TabOrder = 16
  end
  inherited ClearButton: TButton
    Left = 540
    Top = 178
    TabOrder = 18
  end
  object WOSource: TEdit
    Left = 92
    Top = 154
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 6
  end
  object Kind: TComboBox
    Left = 316
    Top = 154
    Width = 198
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 12
    Items.Strings = (
      ''
      'DONE'
      'NORMAL'
      'PRIORITY')
  end
  object WorkOrderNumber: TEdit
    Left = 92
    Top = 10
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Tickets: TComboBox
    Left = 92
    Top = 178
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    Items.Strings = (
      '')
  end
end
