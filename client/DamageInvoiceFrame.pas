unit DamageInvoiceFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Invoice, StdCtrls, ActnList, DB, DBISAMTb, DBCtrls, Mask, OdComboBoxController, QMConst,
  OdVclUtils, ExtCtrls,
  OdHourGlass, OdMiscUtils, OdDbUtils, OdExceptions, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxButtonEdit, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxDBData, cxCurrencyEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxNavigator;

type
  TDamageInvoiceFrame = class(TInvoiceFrame)
    InvoiceGrid: TcxGrid;
    InvoiceLevel: TcxGridLevel;
    InvoiceView: TcxGridDBTableView;
    ColInvoiceType: TcxGridDBColumn;
    ColInvoiceID: TcxGridDBColumn;
    ColInvoiceNum: TcxGridDBColumn;
    ColReceivedDate: TcxGridDBColumn;
    ColClaimantID: TcxGridDBColumn;
    ColClaimant: TcxGridDBColumn;
    ColCompany: TcxGridDBColumn;
    ColAmount: TcxGridDBColumn;
    ColPaidDate: TcxGridDBColumn;
    ColPaidAmount: TcxGridDBColumn;
    FooterPanel: TPanel;
    AccrualCodeLabel: TLabel;
    AccrualCode: TComboBox;
    InvoiceBalance: TLabel;
    ViewAllCk: TCheckBox;
    ClaimantLabel: TLabel;
    Claimant: TComboBox;
    PaymentCodeLabel: TLabel;
    PaymentCode: TDBComboBox;
    procedure ViewAllCkClick(Sender: TObject);
    procedure InsertActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
  protected
    AccrualCodeController: TComboController;
    procedure FilterInvoices(InvoiceType: Integer); override;
  private
    procedure SetupAccrualCodeController(DamageSource: TDataSource);
    procedure UpdateInvoiceTotalDisplay;
    procedure ConfigureGridColumns;
    procedure ViewAllInvoices;
  public
    procedure RefreshNow;
    procedure Initialize(DamageSource: TDataSource; InvoiceType, DamageID, OtherID: Integer);
    destructor Destroy; override;
  end;

implementation

uses DMu, LocalPermissionsDmu, LocalEmployeeDMu, LocalDamageDMu;

{$R *.dfm}

{ TDamageInvoiceFrame }

destructor TDamageInvoiceFrame.Destroy;
begin
  FreeAndNil(AccrualCodeController);
  inherited;
end;

procedure TDamageInvoiceFrame.Initialize(DamageSource: TDataSource; InvoiceType,
  DamageID, OtherID: Integer);
begin
  FInvoiceType := InvoiceType;
  FDamageID := DamageID;
  FOtherID := OtherID;
  if InvoiceType = qminvDamage then
    SetupAccrualCodeController(DamageSource);
  if (InvoiceType = qminv3rdParty) or (InvoiceType = qminvLitigation) then
    LDamageDM.GetDamageClaimantList(Claimant.Items, DamageID, InvoiceType);
  DM.GetRefDisplayList('paycode', PaymentCode.Items);
  ClaimantLabel.Visible := InvoiceType <> qminvDamage;
  Claimant.Visible := ClaimantLabel.Visible;
  PaymentCodeLabel.Visible := InvoiceType = qminvLitigation;
  PaymentCode.Visible := PaymentCodeLabel.Visible;
  RefreshNow;
end;

procedure TDamageInvoiceFrame.RefreshNow;
var
  EnableAdd: Boolean;
begin
  ViewAllCk.Enabled := PermissionsDM.EmployeeHasLitigationRights;
  EnableAdd := (FDamageID > 0);
  if EnableAdd and (FInvoiceType = qminv3rdParty) then
    EnableAdd := FOtherID > 0;
  if EnableAdd and (FInvoiceType = qminvLitigation) then
    EnableAdd := FOtherID > 0;

  FrameEnabled := EnableAdd;
  Invoice.Close;
  Invoice.Open;
  UpdateInvoiceTotalDisplay;
  FilterInvoices(FInvoiceType);
end;

procedure TDamageInvoiceFrame.FilterInvoices(InvoiceType: Integer);
begin
  inherited;
  ConfigureGridColumns;
end;

procedure TDamageInvoiceFrame.ConfigureGridColumns;
begin
  ColInvoiceType.Visible := FInvoiceType <> qminvDamage;
  ColClaimant.Visible := FInvoiceType <> qminvDamage;
end;

procedure TDamageInvoiceFrame.UpdateInvoiceTotalDisplay;
begin
  //TODO: Need to do this
end;

procedure TDamageInvoiceFrame.SetupAccrualCodeController(DamageSource: TDataSource);
var
  Codes, Values, Allowed: TStringList;
begin
  Codes := TStringList.Create;
  Values := TStringList.Create;
  Allowed := TStringList.Create;
  try
    DM.GetRefCodeList('invcode', Codes);
    DM.GetRefDisplayList('invcode', Values);
    PermissionsDM.GetLimitationList(RightDamagesSetInvoiceCode, Allowed);

    if Allowed.Count = 0 then begin
      Allowed.Add(DamageInvoiceCodeExpect);
      Allowed.Add(DamageInvoiceCodeNopay);
    end;

    FreeAndNil(AccrualCodeController);
    AccrualCodeController := TComboController.Create(AccrualCode, DamageSource, 'invoice_code',  Codes, Allowed, Values);
  finally
    FreeAndNil(Codes);
    FreeAndNil(Values);
    FreeAndNil(Allowed);
  end;
  SizeComboDropdownToItems(AccrualCode);
end;

procedure TDamageInvoiceFrame.ViewAllCkClick(Sender: TObject);
begin
  if ViewAllCk.Checked then
    ViewAllInvoices
  else
    FilterInvoices(FInvoiceType);
end;

procedure TDamageInvoiceFrame.ViewAllInvoices;
begin
  FilterInvoices(qminvAll);
end;

procedure TDamageInvoiceFrame.InsertActionExecute(Sender: TObject);
begin
  inherited;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddInvoice, '');
end;

procedure TDamageInvoiceFrame.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  AccrualCode.Enabled := Self.Parent.Enabled and (not ReadOnly) and FrameEnabled;
  AccrualCodeLabel.Enabled := AccrualCode.Enabled;
end;

end.
