unit Attachments;

interface

uses
  Controls, StdCtrls, Classes, ExtCtrls, Windows, Forms, DB, DBISAMTb, ActnList,
  Dialogs, ExtDlgs, SysUtils, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, 
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCalendar,
  cxDropDownEdit, cxCheckBox, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxNavigator, cxFilter, cxData;

type
  TAttachmentsFrame = class(TFrame)
    FooterPanel: TPanel;
    RemoveButton: TButton;
    AddButton: TButton;
    AttachmentsPanel: TPanel;
    Attachment: TDBISAMQuery;
    AttachmentSource: TDataSource;
    Actions: TActionList;
    AddAttachmentsAction: TAction;
    RemoveAttachmentAction: TAction;
    AttachmentListLevel: TcxGridLevel;
    AttachmentList: TcxGrid;
    AttachmentListDBTableView: TcxGridDBTableView;
    ColAttachmentID: TcxGridDBColumn;
    ColFileName: TcxGridDBColumn;
    ColOrigFileName: TcxGridDBColumn;
    ColAttachDate: TcxGridDBColumn;
    ColComment: TcxGridDBColumn;
    ColSize: TcxGridDBColumn;
    ColUploadDate: TcxGridDBColumn;
    DownloadButton: TButton;
    DownloadAction: TAction;
    DownloadAllAction: TAction;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenPictureDialog;
    ColForeignType: TcxGridDBColumn;
    ColForeignID: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColAttachedBy: TcxGridDBColumn;
    EmployeeLookup: TDBISAMTable;
    ColShortName: TcxGridDBColumn;
    SelectAllButton: TButton;
    SelectAllAction: TAction;
    ActivateCameraButton: TButton;
    AttachFromCamera: TAction;
    ColSource: TcxGridDBColumn;
    AttachmentAddinButton: TButton;
    DocumentTypeLookup: TDBISAMTable;
    ColDocumentType: TcxGridDBColumn;
    ColUploadMachineName: TcxGridDBColumn;
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure AddAttachmentsActionExecute(Sender: TObject);
    procedure RemoveAttachmentActionExecute(Sender: TObject);
    procedure ColSizeGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure AttachmentListExit(Sender: TObject);
    procedure DownloadActionExecute(Sender: TObject);
    procedure AttachmentBeforeOpen(DataSet: TDataSet);
    procedure SelectAllActionExecute(Sender: TObject);
    procedure AttachFromCameraExecute(Sender: TObject);
    procedure AttachmentAfterOpen(DataSet: TDataSet);
    procedure AttachmentAfterPost(DataSet: TDataSet);
  private
    FForeignType: Integer;
    FForeignID: Integer;
    FUploadGroup: string;
    FLinkedType: Integer;
    FLinkedID: Integer;
    FFilesWereAttached: Boolean;
    FAttachingFilesNow: Boolean;
    FAllowRemove: Boolean;
    FAllowAddOnly: Boolean;
    FAfterAttachmentChange: TNotifyEvent;
    FUseDocumentType: Boolean;
    FReadOnly: Boolean;
    procedure ValidateData;
    procedure GetCurrentFileData(var OriginalFile, ServerFile, ForeignType: string; var ForeignID: Integer; var UploadDate: TDateTime);
    procedure ForceSelection;
    function GetSelectedAttachmentID: Integer;
    function GetSelectedForeignType: Integer;
    function GetSelectedAttachmentFilename: string;
    procedure AttachmentDataChanged;
    function SelectedAttachmentCount: Integer;
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  public
    property FilesWereAttached: Boolean read FFilesWereAttached write FFilesWereAttached;
    property SelectedAttachmentID: Integer read GetSelectedAttachmentID;
    property SelectedForeignType: Integer read GetSelectedForeignType;
    property SelectedAttachmentFilename: string read GetSelectedAttachmentFilename;
    property AfterAttachmentChange: TNotifyEvent read FAfterAttachmentChange write FAfterAttachmentChange;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly;
    property AllowAddOnly: Boolean read FAllowAddOnly write FAllowAddOnly;
    procedure AfterDoPhotoAttachment(Sender: TObject);
    procedure OnPhotoAttachment(FileName: TFileName);
    procedure Initialize(const ForeignType, ForeignID: Integer;
      UseDocumentType: Boolean = False; LinkedType: Integer = -1; LinkedID: Integer = -1);
    procedure DoWIAAttachment;
    procedure DoDefaultAttachment(Path: string);
    procedure UpdateCameraStatus;
    procedure RefreshNow;
  end;

implementation

{$R *.dfm}

uses DMu, UQUtils, OdDbUtils, OdHourglass, QMConst, OdMiscUtils, OdUqInternet,
  OdWiaManager, OdStillImage, OdCxUtils, Graphics, 
  LocalPermissionsDMu;

procedure TAttachmentsFrame.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
var
  HaveRecords: Boolean;
  HaveUploadGroup: Boolean;
  HaveTypeMatch: Boolean;
begin
  HaveRecords := Attachment.Active and (Attachment.RecordCount > 0);
  HaveUploadGroup := FUploadGroup <> '';
  HaveTypeMatch := False;
  if Attachment.Active then  // Added to prevent exception when attachment is not active
    HaveTypeMatch := Attachment.FieldByName('foreign_type').AsInteger = FForeignType;

  RemoveAttachmentAction.Enabled := HaveRecords and HaveUploadGroup and HaveTypeMatch and FAllowRemove and (not ReadOnly);
  DownloadAction.Enabled := HaveRecords;
  DownloadAllAction.Enabled := HaveRecords;
  AddAttachmentsAction.Enabled := HaveUploadGroup and ((not ReadOnly) or AllowAddOnly);
  SelectAllAction.Enabled := HaveRecords;
end;

procedure TAttachmentsFrame.AddAttachmentsActionExecute(Sender: TObject);
var
  i: Integer;
  FileName: string;
  OldDirectory: string;
begin
  if FAttachingFilesNow then
    Exit;
  FAttachingFilesNow := True;
  DM.StopTimeImportTimer;
  try
    ValidateData;
    FFilesWereAttached := False;
    OldDirectory := GetCurrentDir;
    try
      if OpenDialog.Execute then begin
        for i := 0 to OpenDialog.Files.Count - 1 do begin
          FileName := OpenDialog.Files[i];
          if FileExists(FileName) then
            DM.AddAttachmentToRecord(FForeignType, FForeignID, FileName, ManualAttachmentSource);
        end;
        FFilesWereAttached := True;
        AttachmentDataChanged;
      end;
    finally
      SetCurrentDir(OldDirectory);
    end;
    RefreshNow;
  finally
    FAttachingFilesNow := False;
    DM.StartTimeImportTimer;
  end;
end;

procedure TAttachmentsFrame.Initialize(const ForeignType, ForeignID: Integer;
   UseDocumentType: Boolean; LinkedType: Integer; LinkedID: Integer);
begin
  FForeignType := ForeignType;
  FForeignID := ForeignID;
  FLinkedType := LinkedType;
  FLinkedID := LinkedID;
  FUseDocumentType := UseDocumentType;
  FFilesWereAttached := False;
  ValidateData;
  RefreshNow;
  OpenDialog.Filter := SaveDialog.Filter;
  ColActive.Visible := DM.UQState.DeveloperMode;
  ColActive.Properties.ReadOnly := (not DM.UQState.DeveloperMode) or (not PermissionsDM.CanI(RightRemoveAttachments));
  ColAttachedBy.Visible := DM.UQState.DeveloperMode;
  ColSource.Visible := DM.UQState.DeveloperMode;
  ColDocumentType.Visible := FUseDocumentType;
  DM.GetRefDisplayList('doctype', CxComboBoxItems(ColDocumentType));
  SetCxComboBoxDropDownRows(ColDocumentType);
  ColUploadMachineName.Visible := DM.UQState.DeveloperMode;

  // InitialDir is set to MyComputer
  OpenDialog.InitialDir := MyComputerCLSID;
//  imgQR.Picture := nil;
//  imgQR.Width := 0;//StrToIntDef(INIReadSetting('AWSAPI','QRCode','0'),0);

//  if imgQR.Width > 0 then
//  begin
//    tmp := GetWindowsTempFileName(True, '', 'qr');
//    tmp := tmp + '.png';
//    emp_id := DM.UQState.EmpID;// EmployeeLookup.FieldByName('emp_id').asinteger; 
//    url := format(  
//      // http://10.18.63.24:3579/attachment/qrcode?size=400&msg=appID=1
//      // ,attachmentapiurl=http://10.18.63.24:3579
//      //,foreignid=99338418
//      //,foreigntypeid=1
//      //,empid=21913
//      //,mode=login
//      //,caption=Please%20scan%20barcode
//      '%sattachment/qrcode?size=400&msg=appID=1' 
//        +',attachmentapiurl=%s' 
//        +',foreignid=%d'
//        +',foreigntypeid=%d'
//        +',empid=%d'
//        +',mode=%s'
//        +',caption=Attachments'
//        +',prodAPI=%s' 
//      ,[
//        INIReadSetting('AWSAPI','URL',GAttachmentURL,GAttachmentURL)
//        , INIReadSetting('AWSAPI','URL',GAttachmentURL,GAttachmentURL)
//        , FForeignID 
//        , FForeignType
//        , emp_id
//        , 'camera' 
//        , INIReadSetting('AWSAPI','ProdAPI',GProdAPI,GProdAPI)
//    ]);
//    DeleteURLCacheEntry(PChar(url));
//    if URLDownloadToFile(nil, 
//      PChar(url)
//      , PChar(tmp)
//      , 0
//      , nil ) = S_OK then
//    begin
//      imgQR.Picture.LoadFromFile(tmp)
//    end
//  end  
end;

procedure TAttachmentsFrame.ValidateData;
begin
  if FForeignType <= 0 then
    raise Exception.Create('Invalid foreign type');
end;

procedure TAttachmentsFrame.RefreshNow;
const
  SQL = 'select * from attachment where (foreign_type = :ForeignType and foreign_id = :ForeignID %s) ';
  LinkedAttachmentsClause = ' or (foreign_type = :LinkedType and foreign_id = :LinkedID %s) ';
var
  ActiveClause: string;
begin
  ValidateData;
  if DM.UQState.DeveloperMode then
    ActiveClause := ''
  else
    ActiveClause := ' and active ';

  if FLinkedID <= 0 then
    Attachment.SQL.Text := Format(SQL, [ActiveClause])
  else
    Attachment.SQL.Text := Format(SQL, [ActiveClause]) + Format(LinkedAttachmentsClause, [ActiveClause]);

  Attachment.ParamByName('ForeignType').AsInteger := FForeignType;
  Attachment.ParamByName('ForeignID').AsInteger := FForeignID;
  if FLinkedID > 0 then begin
    Attachment.ParamByName('LinkedType').AsInteger := FLinkedType;
    Attachment.ParamByName('LinkedID').AsInteger := FLinkedID;
  end;
  RefreshDataSet(Attachment);
  DM.Engine.LinkEventsAutoInc(Attachment);
  FUploadGroup := PermissionsDM.GetLimitation(RightUploadGroup);
  FAllowRemove := PermissionsDM.CanI(RightRemoveAttachments);
end;

procedure TAttachmentsFrame.RemoveAttachmentActionExecute(Sender: TObject);
var
  i: Integer;
begin
  ForceSelection;
  if not YesNoDialogFmt('Are you sure you want to remove the selected %s?',
    [AddSIfNot1(SelectedAttachmentCount, 'attachment')]) then
    Exit;

  AttachmentListDBTableView.DataController.BeginLocate;
  try
    for i := 0 to SelectedAttachmentCount - 1 do begin
      AttachmentListDBTableView.DataController.FocusedRowIndex := AttachmentListDBTableView.DataController.GetSelectedRowIndex(i);
      try
        Attachment.Delete;
      except
        on E: EAbort do begin
        // Ignore Aborts from setting Active=False and continue "deleting"
        end;
      end;
      AttachmentDataChanged;
    end;
  finally
    AttachmentListDBTableView.DataController.EndLocate;
    RefreshNow;
  end;
end;

procedure TAttachmentsFrame.ColSizeGetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: String);
begin
  AText := GetKBStringForBytes(StrToIntDef(AText, 0));
end;

procedure TAttachmentsFrame.AttachmentListExit(Sender: TObject);
begin
  UpdateAndPostDataSet(Attachment);
end;

procedure TAttachmentsFrame.DownloadActionExecute(Sender: TObject);
var
  OriginalFileName: string;
  FTPFileName: string;
  Cursor: IInterface;
  AttachmentID: Integer;
  DestFile: string;
  ForeignType: string;
  ForeignID: Integer;
  i: Integer;
  UploadDate: TDateTime;
begin
  ForceSelection;
  Cursor := ShowHourGlass;
  for i := 0 to SelectedAttachmentCount - 1 do begin
    AttachmentListDBTableView.DataController.FocusedRowIndex := AttachmentListDBTableView.DataController.GetSelectedRowIndex(i);
    AttachmentList.Refresh;
    GetCurrentFileData(OriginalFileName, FTPFileName, ForeignType, ForeignID, UploadDate);
    if (FTPFileName <> '') and (UploadDate > 0) then begin
      AttachmentID := Attachment.FieldByName('attachment_id').AsInteger;
      if AttachmentID > 0 then begin
        DestFile := DM.DownloadAttachment(AttachmentID, i + 1, SelectedAttachmentCount);
        if FileExists(DestFile) then
          OdShellExecute(DestFile)
        else
          raise Exception.CreateFmt('Attachment file %s not found', [DestFile]);
      end
      else
        ShowMessageFmt('%s has not been uploaded, so it can not be downloaded', [OriginalFileName]);
    end;
  end;
end;

procedure TAttachmentsFrame.GetCurrentFileData(var OriginalFile, ServerFile, ForeignType: string; var ForeignID: Integer; var UploadDate: TDateTime);
var
  GridRecord: TcxCustomGridRecord;
begin
  GridRecord := AttachmentListDBTableView.Controller.FocusedRecord;
  if (Assigned(GridRecord) and (GridRecord is TcxGridDataRow) ) then begin
    OriginalFile := GridRecord.Values[ColOrigFileName.Index];
    ServerFile := GridRecord.Values[ColFileName.Index];
    ForeignType := DM.GetForeignTypeDirFromForeignType(GridRecord.Values[ColForeignType.Index]);
    ForeignID := GridRecord.Values[ColForeignID.Index];
    UploadDate := VarToDateTime(GridRecord.Values[ColUploadDate.Index]);
  end;
end;

procedure TAttachmentsFrame.AfterDoPhotoAttachment(Sender: TObject);
begin
  RefreshNow;
end;

procedure TAttachmentsFrame.OnPhotoAttachment(FileName: TFileName);
begin
  ValidateData;
  if FileExists(FileName) then begin
    DM.AddAttachmentToRecord(FForeignType, FForeignID, FileName, PhotoAttachmentSource);
    RefreshDataSet(Attachment);
    FFilesWereAttached := True;
  end;
end;

procedure TAttachmentsFrame.DoWIAAttachment;
var
  Cursor: IInterface;
  QMTempFolder: string;
begin
  if not Assigned(WiaManager) then
    Exit;
  if FAttachingFilesNow then
    Exit;
  Cursor := ShowHourGlass;
  QMTempFolder := GetWindowsTempPath + 'QManager\';
  OdForceDirectories(QMTempFolder);
  FAttachingFilesNow := True;
  try
    FFilesWereAttached := False;
    WiaManager.AfterFileTransfer := OnPhotoAttachment;
    WiaManager.TransferFilesFromDevice(QMTempFolder);
  finally
    FAttachingFilesNow := False;
  end;
end;

procedure TAttachmentsFrame.DoDefaultAttachment(Path: string);
begin
  OpenDialog.InitialDir := Path;
  AddAttachmentsAction.Execute;
end;

procedure TAttachmentsFrame.AttachmentBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('short_name', Attachment, 'attached_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
end;

procedure TAttachmentsFrame.SelectAllActionExecute(Sender: TObject);
begin
  AttachmentListDBTableView.Controller.SelectAll;
end;

procedure TAttachmentsFrame.ForceSelection;
begin
  if (not Attachment.IsEmpty) and (SelectedAttachmentCount < 1) then begin
    AttachmentListDBTableView.Controller.FocusedRecord.Selected := True;
    Assert(SelectedAttachmentCount > 0);
  end;
end;

procedure TAttachmentsFrame.AttachFromCameraExecute(Sender: TObject);
begin
  if (Owner is TForm) then
    PostMessage(TForm(Owner).Handle, WM_STI_LAUNCH, 0, 0);
end;

procedure TAttachmentsFrame.UpdateCameraStatus;
begin
  if Assigned(WiaManager) then
    AttachFromCamera.Enabled := WiaManager.HasDeviceConnected and (not FReadOnly)
  else
    AttachFromCamera.Enabled := False;
end;

function TAttachmentsFrame.GetSelectedAttachmentID: Integer;
begin
  if (SelectedAttachmentCount = 1) then
    Result := Attachment.FieldByName('attachment_id').AsInteger
  else
    Result := 0;
end;

function TAttachmentsFrame.GetSelectedForeignType: Integer;
begin
  if (SelectedAttachmentCount = 1) then
    Result := Attachment.FieldByName('foreign_type').AsInteger
  else
    Result := 0;
end;

function TAttachmentsFrame.GetSelectedAttachmentFilename: string;
begin
  if (SelectedAttachmentCount = 1) then
    Result := Attachment.FieldByName('orig_filename').AsString
  else
    Result := '';
end;

procedure TAttachmentsFrame.AttachmentDataChanged;
begin
  if Assigned(FAfterAttachmentChange) then
    FAfterAttachmentChange(Self);
end;

procedure TAttachmentsFrame.AttachmentAfterOpen(DataSet: TDataSet);
begin
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('doc_type'), 'doctype');
end;

procedure TAttachmentsFrame.AttachmentAfterPost(DataSet: TDataSet);
begin
  AttachmentDataChanged;
end;

function TAttachmentsFrame.SelectedAttachmentCount: Integer;
begin
  Result := AttachmentListDBTableView.Controller.SelectedRecordCount;
end;

procedure TAttachmentsFrame.SetReadOnly(const Value: Boolean);
begin
  ColComment.Properties.ReadOnly := Value;
  ColDocumentType.Properties.ReadOnly := Value;
  FReadOnly := Value;
end;

function TAttachmentsFrame.GetReadOnly: Boolean;
begin
  Result := FReadOnly;
end;

end.
