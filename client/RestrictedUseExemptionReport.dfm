inherited RestrictedUseExemptionReportForm: TRestrictedUseExemptionReportForm
  Left = 361
  Top = 135
  Caption = 'RestrictedUseExemptionReportForm'
  ClientHeight = 233
  ClientWidth = 511
  PixelsPerInch = 96
  TextHeight = 13
  object GrantedDateRangeLabel: TLabel [0]
    Left = 0
    Top = 6
    Width = 103
    Height = 13
    Caption = 'Granted Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object RevokedDateRangeLabel: TLabel [1]
    Left = 255
    Top = 6
    Width = 106
    Height = 13
    Caption = 'Revoked Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inline GrantedDateRange: TOdRangeSelectFrame [2]
    Left = 4
    Top = 20
    Width = 244
    Height = 63
    TabOrder = 0
    inherited ToLabel: TLabel
      Top = 11
    end
  end
  inline RevokedDateRange: TOdRangeSelectFrame
    Left = 260
    Top = 20
    Width = 253
    Height = 63
    TabOrder = 1
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
  end
  object IncludeExemptionsForGroup: TRadioGroup
    Left = 4
    Top = 89
    Width = 499
    Height = 96
    Caption = 'Include Exemptions For '
    ItemIndex = 0
    Items.Strings = (
      'Any Worker'
      'Specific Worker:'
      'Anyone Under:')
    TabOrder = 2
    OnClick = UpdateVisibility
  end
  inline SpecificWorkerBox: TEmployeeSelect
    Left = 124
    Top = 127
    Width = 205
    Height = 30
    TabOrder = 3
    inherited EmployeeCombo: TComboBox
      Left = 0
      Top = 4
      Width = 166
    end
  end
  object WorkersUnderManagerBox: TComboBox
    Left = 124
    Top = 158
    Width = 166
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = '<Choose Manager>'
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 456
    Top = 180
  end
end
