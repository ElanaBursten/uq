inherited MessageAckForm: TMessageAckForm
  Caption = 'Messages'
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonPanel: TPanel
    Left = 0
    Top = 359
    Width = 548
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object AckButton: TButton
      Left = 214
      Top = 6
      Width = 147
      Height = 27
      Caption = '&Acknowledge Messages'
      TabOrder = 0
      Visible = False
      OnClick = AckButtonClick
    end
    object Memo1: TMemo
      Left = 23
      Top = 6
      Width = 185
      Height = 27
      TabOrder = 1
      Visible = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 548
    Height = 359
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    Caption = 'Panel2'
    TabOrder = 1
    object WebBrowser1: TWebBrowser
      Left = 8
      Top = 8
      Width = 532
      Height = 343
      Align = alClient
      TabOrder = 0
      OnBeforeNavigate2 = WebBrowser1BeforeNavigate2
      ControlData = {
        4C000000FC360000732300000000000000000000000000000000000000000000
        000000004C000000000000000000000001000000E0D057007335CF11AE690800
        2B2E126208000000000000004C0000000114020000000000C000000000000046
        8000000000000000000000000000000000000000000000000000000000000000
        00000000000000000100000000000000000000000000000000000000}
    end
  end
  object Messages: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from message_dest m'
      'left outer join break_rules b on m.rule_id = b.rule_id'
      'where m.ack_date is null and m.show_date <= current_timestamp '
      'and m.expiration_date > current_timestamp'
      'and m.active = True'
      'order by m.show_date')
    Params = <>
    Left = 168
    Top = 88
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = Timer1Timer
    Left = 48
    Top = 48
  end
end
