object frmStreetMap: TfrmStreetMap
  Left = 427
  Top = 268
  Caption = 'Last Known Location'
  ClientHeight = 626
  ClientWidth = 848
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelHeader: TPanel
    Left = 0
    Top = 0
    Width = 848
    Height = 40
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Left = 292
      Top = 10
      Width = 15
      Height = 13
      Caption = 'Lat'
    end
    object Label3: TLabel
      Left = 393
      Top = 10
      Width = 23
      Height = 13
      Caption = 'Long'
    end
    object CheckBoxTraffic: TCheckBox
      Left = 15
      Top = 8
      Width = 58
      Height = 17
      Caption = 'Traffic'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = CheckBoxTrafficClick
    end
    object CheckBoxStreeView: TCheckBox
      Left = 82
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Street View'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = CheckBoxStreeViewClick
    end
    object BitBtn1: TBitBtn
      Left = 760
      Top = 2
      Width = 75
      Height = 25
      TabOrder = 2
      OnClick = BitBtn1Click
      Kind = bkClose
    end
    object edtLat: TEdit
      Left = 312
      Top = 6
      Width = 57
      Height = 21
      TabOrder = 3
      Text = '34.5'
    end
    object edtLng: TEdit
      Left = 422
      Top = 6
      Width = 57
      Height = 21
      TabOrder = 4
      Text = '-84.0'
    end
    object Button2: TButton
      Left = 211
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Go To'
      TabOrder = 5
      OnClick = ButtonGotoLocationClick
    end
  end
  object WVWindowParent1: TWVWindowParent
    Left = 0
    Top = 40
    Width = 848
    Height = 567
    Align = alClient
    TabOrder = 1
    DoubleBuffered = False
    Browser = WVBrowser1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 607
    Width = 848
    Height = 19
    Panels = <
      item
        Width = 400
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 400
      end>
  end
  object WVBrowser1: TWVBrowser
    TargetCompatibleBrowserVersion = '104.0.1293.44'
    AllowSingleSignOnUsingOSPrimaryAccount = False
    OnInitializationError = WVBrowser1InitializationError
    OnAfterCreated = WVBrowser1AfterCreated
    OnNavigationCompleted = WVBrowser1NavigationCompleted
    Left = 56
    Top = 64
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 48
    Top = 128
  end
end
