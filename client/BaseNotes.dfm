object BaseNotesFrame: TBaseNotesFrame
  Left = 0
  Top = 0
  Width = 637
  Height = 300
  TabOrder = 0
  object NotesGrid: TcxGrid
    Left = 0
    Top = 137
    Width = 637
    Height = 153
    Align = alClient
    PopupMenu = NotesGridPopupMenu
    TabOrder = 2
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = False
    object NotesGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = NotesGridViewCustomDrawCell
      DataController.DataSource = NotesSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'notes_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.EditAutoHeightBorderColor = clHotLight
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.RowSeparatorColor = clBlack
      OptionsView.RowSeparatorWidth = 1
      Preview.Column = ColNotesNote
      Preview.MaxLineCount = 1000
      Preview.Visible = True
      Styles.Selection = SharedDevExStyleData.SmallGridHighlight
      Styles.Header = SharedDevExStyleData.NavyHeader
      object ColNotesID: TcxGridDBColumn
        Caption = 'Notes ID'
        DataBinding.FieldName = 'notes_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object ColNotesForeignType: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'foreign_type'
        Visible = False
      end
      object ColNotesForeignID: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'foreign_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object ColNotesEntryDate: TcxGridDBColumn
        Caption = 'Entry Date'
        DataBinding.FieldName = 'entry_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        OnCustomDrawCell = ColNotesEntryDateCustomDrawCell
        Options.Filtering = False
        Width = 115
      end
      object ColNotesAddedByName: TcxGridDBColumn
        Caption = 'Entered By'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 159
      end
      object ColNotesUID: TcxGridDBColumn
        Caption = 'UID'
        DataBinding.FieldName = 'uid'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object ColNotesActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Visible = False
        MinWidth = 16
        Options.Filtering = False
        Width = 40
      end
      object ColRestriction: TcxGridDBColumn
        DataBinding.FieldName = 'Restriction'
        Width = 257
      end
      object ColNotesNote: TcxGridDBColumn
        Caption = 'Note'
        DataBinding.FieldName = 'note'
      end
      object ColNotesSource: TcxGridDBColumn
        Caption = 'Notes For'
        DataBinding.FieldName = 'notes_source'
        Visible = False
        VisibleForCustomization = False
      end
    end
    object NotesGridLevel: TcxGridLevel
      GridView = NotesGridView
    end
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 637
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object NoteLabel: TLabel
      Left = 5
      Top = 8
      Width = 26
      Height = 26
      Caption = 'Note Text:'
      WordWrap = True
    end
    object NoteText: TMemo
      Left = 37
      Top = 8
      Width = 483
      Height = 123
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object AddNoteButton: TButton
      Left = 526
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Add Note'
      TabOrder = 1
      OnClick = AddNoteButtonClick
    end
  end
  object FooterPanel: TPanel
    Left = 0
    Top = 290
    Width = 637
    Height = 10
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
  end
  object NotesSource: TDataSource
    OnDataChange = NotesSourceDataChange
    Left = 208
    Top = 193
  end
  object NotesGridPopupMenu: TPopupMenu
    Left = 8
    Top = 256
    object CopyNote1: TMenuItem
      Action = CopyNoteAction
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object MakeNotePrivate1: TMenuItem
      Action = ChangeToPrivateAction
    end
    object MakeNotePublic1: TMenuItem
      Action = ChangeToPublicAction
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object DeleteNote1: TMenuItem
      Action = DeleteNoteAction
    end
  end
  object NotesGridActionList: TActionList
    Left = 40
    Top = 256
    object CopyNoteAction: TAction
      Caption = 'Copy Note'
      OnExecute = CopyNoteActionExecute
    end
    object DeleteNoteAction: TAction
      Caption = 'Delete Note'
      OnExecute = DeleteNoteActionExecute
    end
    object ChangeToPrivateAction: TAction
      Caption = 'Make Note Private'
      OnExecute = ChangeToPrivateActionExecute
    end
    object ChangeToPublicAction: TAction
      Caption = 'Make Note Public'
      OnExecute = ChangeToPublicActionExecute
    end
    object UndoAction: TAction
      Caption = 'Undo'
    end
  end
end
