object FirstTaskReminderForm: TFirstTaskReminderForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Set First Task Reminder'
  ClientHeight = 87
  ClientWidth = 184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblEmployee: TLabel
    Left = 8
    Top = 8
    Width = 72
    Height = 14
    Caption = 'lblEmployee'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ReminderTimeEdit: TcxTimeEdit
    Left = 8
    Top = 28
    EditValue = 0d
    ParentFont = False
    Properties.TimeFormat = tfHourMin
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Width = 168
  end
  object btnSet: TcxButton
    Left = 64
    Top = 58
    Width = 53
    Height = 25
    Caption = 'Set'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnSetClick
  end
  object btnCancel: TcxButton
    Left = 123
    Top = 58
    Width = 53
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
