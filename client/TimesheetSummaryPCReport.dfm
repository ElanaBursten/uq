inherited TimesheetSummaryPCReportForm: TTimesheetSummaryPCReportForm
  Caption = 'TimesheetSummaryPCReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 89
    Height = 13
    Caption = 'Work Date Range:'
  end
  object Label2: TLabel [1]
    Left = 1
    Top = 171
    Width = 82
    Height = 13
    Caption = 'Employee Types:'
  end
  inline OdRangeSelectFrame1: TOdRangeSelectFrame [2]
    Left = 26
    Top = 22
    Width = 248
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
  object CheckBox1: TCheckBox
    Left = 34
    Top = 95
    Width = 241
    Height = 17
    Caption = 'Include Callout hours'
    TabOrder = 1
  end
  object CheckBox2: TCheckBox
    Left = 34
    Top = 119
    Width = 241
    Height = 17
    Caption = 'Approved hours only'
    TabOrder = 2
  end
  object CheckBox3: TCheckBox
    Left = 34
    Top = 143
    Width = 241
    Height = 17
    Caption = 'Final approved hours only'
    TabOrder = 3
  end
  object CheckListBox1: TCheckListBox
    Left = 97
    Top = 171
    Width = 171
    Height = 217
    ItemHeight = 13
    TabOrder = 4
  end
end
