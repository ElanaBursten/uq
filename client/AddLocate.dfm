object AddLocateForm: TAddLocateForm
  Left = 240
  Top = 331
  BorderStyle = bsDialog
  Caption = 'Add a Locate'
  ClientHeight = 177
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  OnShow = FormShow
  DesignSize = (
    456
    177)
  PixelsPerInch = 96
  TextHeight = 13
  object c: TGroupBox
    Left = 14
    Top = 8
    Width = 434
    Height = 130
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Locate Information '
    TabOrder = 0
    DesignSize = (
      434
      130)
    object Label1: TLabel
      Left = 23
      Top = 20
      Width = 31
      Height = 13
      Caption = 'Client:'
    end
    object Label2: TLabel
      Left = 23
      Top = 47
      Width = 35
      Height = 13
      Caption = 'Status:'
    end
    object lblLocator: TLabel
      Left = 23
      Top = 74
      Width = 40
      Height = 13
      Caption = 'Locator:'
    end
    object ClientCombo: TComboBox
      Left = 93
      Top = 16
      Width = 333
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 0
      OnChange = ClientComboChange
    end
    object HighProfileCheckBox: TCheckBox
      Left = 23
      Top = 104
      Width = 85
      Height = 17
      Anchors = [akLeft, akBottom]
      Caption = 'High Profile'
      TabOrder = 1
    end
    object LocatorComboBox: TcxComboBox
      Left = 93
      Top = 70
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      TabOrder = 2
      OnClick = LocatorComboBoxClick
      Width = 333
    end
    object StatusCombo: TcxComboBox
      Left = 93
      Top = 43
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      TabOrder = 3
      OnClick = LocatorComboBoxClick
      Width = 60
    end
  end
  object CancelBtn: TButton
    Left = 217
    Top = 144
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object OKBtn: TButton
    Left = 136
    Top = 144
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
  end
end
