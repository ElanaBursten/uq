unit StateMgr;

interface

uses Graphics, IniFiles, QMConst, Classes;
type
  TDeviceRec = record   //QM-164 EB Config/Plats
    AirCardDeviceID: string;  //Cell Device ID
    AirCardBrand: string;
    AirCardPhone: string;
    SimCardDeviceID: string;
    Subscriber: string;
    LastUpdated: TDateTime;
  end;

  TFTR = record   //QM-494 First Task Reminder EB
    Reminder: string;
    LastUpdated: TDateTime;
    Required: boolean;
    Debug: boolean;
  end;

type
  TUQState = class(TObject)
  private
    FShadeColor: TColor;
    FUserName: string;
    FDisplayName: string;
    FPassword: string;
    FPreviousUser: string;
    FServerURL: string;
    FPreviousServerURL: string;
    FWebUpdateUrl: string;
    FBaseHelpURL: string;
    FRememberLoginData: Boolean;
    FLastLocalSyncTime: TDateTime;
    fLastTicketDataClean: TDateTime;  //QM-986 EB Reset Ticket Data on First Sync
    FDueTicketColor: TColor;
    FDueHoursTicketColor: TColor;
    FCriticalTicketColor: TColor;
    FDoNotMarkTicketColor: TColor;
    FDue7TomorrowColor: TColor;
    FHighestGeocodePrecisionColor: TColor;
    FAcceptableGeocodePrecisionColor: TColor;
    FZeroGeocodeMatchesColor: TColor;
    FShowColorPrefs: Boolean;
    FActivityReportLastDateTime: string;
    FActivityReportLastOffice: Integer;
    FActivityReportLastManager: Integer;
    FAllowMultipleInstances: Boolean;
    FWebUpdateLastDateTime: string;
    FEmpID: Integer;
    FUserID: Integer;
    FPreviousLoginHash: string;
    FIniFileName: string;
    FDebugConfigFileName: string;
    FUserType: string;
    FDeveloperMode: Boolean;
    fDeveloperOverrideMode: Boolean;  //QM-579 EB  - Infamous Debug Mode
//    fDeveloperSyncWait1: Integer;     //QM-614 EB
//    fDeveloperSyncWait2: Integer;     //QM-614 EB
    FTMSplitterPos: Integer;
    FScanAndPatchLevel: Integer;
    FLastUploadLocation: Integer;
    FLastAttachmentSaveDir: string;
    FServerUrls: TStringList;
    FPrevUserNames: TStringList;
    FEncryptedPassword: Boolean;
    FAllowRememberedLogin: Boolean;
    FPlainPassword: string;
    FTimeSubmitMessageLastDate: string;
    FPowerOnLastDateTime: string;
    FPowerOffLastDateTime: string;
    FPowerOnFirstDateTimeToday: string;
    FNewServerURL: string;
    FDriveLettersForAttachment: string;
    FLastAttachmentDir: string;
    FSaveSyncXML: Boolean;
    FAppLocal: Boolean;
    FLastWindowsUnlockDateTime: string;
    FDebugUploadThread: Boolean;
    FProfitCenter: string;
    FNotesFixDateTime: string;
    FLastUpdateFileHash: string;
    FRegisteredForCamera: Boolean;
    FAPIKey: string;
    FQMLogic2URL: string;
    FQtrMinEnabled: Boolean;
    FScreenShotPath: string;
    fGoogleAPIStr: string;

    {QMANTWO-607 EB - Debug variables}
    fDebugStatus: string; // Debug status message to record.   //QM-805 EB Debug Ini Refactoring
    fSecondaryDebugOn: Boolean;  //Turns on/off entire QManagerDebugConfig.ini   //QM-805 EB Debug Ini Refactoring
    fDebugDeviceOverride: Boolean; //QM-805 EB Debug Ini Refactoring
    fDebugADIgnore: boolean;
    fDebugZScalerIgnore: boolean; //QM-876 EB Maint
    fDebugLocalDomain: string;
    fDebugWorkMgmtDebug: boolean;
    fDebugPlatsAltLocation: string;  //QM-164
    fDebugUseAltPlatsLoc: boolean;  //QM-164
    fOverride_AirCardDeviceID: string;  //QM-805 EB Debug Refactoring
    fOverride_AirCardBrand: string;  //QM-805 EB Debug Refactoring
    fOverride_AirCardPhone: string;  //QM-805 EB Debug Refactoring
    fOverride_SimCardDeviceID: string;  //QM-805 EB Debug Refactoring
    fOverride_Subscriber: string;  //QM-805 EB Debug Refactoring

    {QM-147 EB Powershell and Time}
    fUsePowershellTime: boolean;

    {QM-164 EB Device Information}
    fDeviceRec: TDeviceRec;
    fPrevDeviceRec: TDeviceRec;

    {QM-444 Part 2 and other grid coloring EB}
    fIsQualified: TColor;
    fIsNotQualified: TColor;
    fIsAboutToExpire: TColor;

    {QM-494 First Task}
    fFTR: TFTR;


    {QM-494 First Task}
    procedure ReadFirstTask(TheIniFile: TIniFile);

    {QM-164 EB Device Information}
    procedure ReadDeviceRec(TheIniFile: TIniFile);
    procedure UpdateDeviceRec(TheIniFile: TIniFile);

    procedure LoadFromIni;
    procedure LoadDebugConfig(DCIniFile: TIniFile);
    procedure Initialize;
    procedure CorrectDueColor;
    procedure SetPassword(const Value: string);
    procedure SetupDamageLiabililtyDescriptions;
    function GetServerURL: string;
    function GetQMLogic2URL: string;

  public
    DamageLiabilityDescription: array[TDamageLiability] of string;
    function CompanyShortName: string;
    property UserName: string read FUserName write FUserName;
    property DisplayName: string read FDisplayName write FDisplayName;
    property Password: string read FPassword write SetPassword;
    property PreviousUser: string read FPreviousUser write FPreviousUser;
    property RememberLoginData: Boolean read FRememberLoginData write FRememberLoginData;
    property ServerURL: string read GetServerURL write FServerURL;
    property PreviousServerURL: string read FPreviousServerURL write FPreviousServerURL;
    property WebUpdateURL: string read FwebUpdateUrl write FWebUpdateUrl;
    property BaseHelpURL: string read FBaseHelpURL write FBaseHelpURL;
    property ShadeColor: TColor read FShadeColor write FShadeColor;
    property LastLocalSyncTime: TDateTime read FLastLocalSyncTime write FLastLocalSyncTime;
    property LastTicketDataClean: TDateTime read fLastTicketDataClean  write fLastTicketDataClean ;  //QM-986 EB Reset Ticket Data
    property DueTicketColor: TColor read FDueTicketColor write FDueTicketColor;
    property DueHoursTicketColor: TColor read FDueHoursTicketColor write FDueHoursTicketColor;
    property CriticalTicketColor: TColor read FCriticalTicketColor write FCriticalTicketColor;
    property DoNotMarkTicketColor: TColor read FDoNotMarkTicketColor write FDoNotMarkTicketColor;
    property Due7TomorrowColor: TColor read FDue7TomorrowColor write FDue7TomorrowColor;
    property HighestGeocodePrecisionColor: TColor read
      FHighestGeocodePrecisionColor write FHighestGeocodePrecisionColor;
    property AcceptableGeocodePrecisionColor: TColor read
      FAcceptableGeocodePrecisionColor write FAcceptableGeocodePrecisionColor;
    property ZeroGeocodeMatchesColor: TColor read FZeroGeocodeMatchesColor write
      FZeroGeocodeMatchesColor;
    property ShowColorPrefs: Boolean read FShowColorPrefs write FShowColorPrefs;
    property ActivityReportLastDateTime: string read FActivityReportLastDateTime write FActivityReportLastDateTime;
    property ActivityReportLastOffice: Integer read FActivityReportLastOffice write FActivityReportLastOffice;
    property ActivityReportLastManager: Integer read FActivityReportLastManager write FActivityReportLastManager;
    property AllowMultipleInstances: Boolean read FAllowMultipleInstances write FAllowMultipleInstances;
    property WebUpdateLastDateTime: string read FWebUpdateLastDateTime write FWebUpdateLastDateTime;
    property EmpID: Integer read FEmpID write FEmpID;
    property UserID: Integer read FUserID write FUserID;
    property PreviousLoginHash: string read FPreviousLoginHash write FPreviousLoginHash;
    property UserType: string read FUserType write FUserType;
    property DeveloperMode: Boolean read FDeveloperMode write FDeveloperMode;
    property DeveloperOverrideMode: Boolean read fDeveloperOverrideMode write fDeveloperOverrideMode;  //QM-579 EB

//    property DeveloperSyncWait1: Integer read fDeveloperSyncWait1 write fDeveloperSyncWait1; //QM-614 EB
//    property DeveloperSyncWait2: Integer read fDeveloperSyncWait2 write fDeveloperSyncWait2; //QM-614 EB
    property TMSplitterPos: Integer read FTMSplitterPos write FTMSplitterPos;
    property ScanAndPatchLevel: Integer read FScanAndPatchLevel write FScanAndPatchLevel;
    property LastUploadLocation: Integer read FLastUploadLocation write FLastUploadLocation;
    property LastAttachmentSaveDir: string read FLastAttachmentSaveDir write FLastAttachmentSaveDir;
    property ServerUrls: TStringList read FServerUrls write FServerUrls;
    property PrevUserNames: TStringList read FPrevUserNames write FPrevUserNames;  //EB QMANTWO-607
    property EncryptedPassword: Boolean read FEncryptedPassword write FEncryptedPassword;
    property AllowRememberedLogin: Boolean read FAllowRememberedLogin write FAllowRememberedLogin;
    property PlainPassword: string read FPlainPassword write FPlainPassword;
    property TimeSubmitMessageLastDate: string read FTimeSubmitMessageLastDate write FTimeSubmitMessageLastDate;
    property PowerOnLastDateTime: string read FPowerOnLastDateTime write FPowerOnLastDateTime;
    property PowerOffLastDateTime: string read FPowerOffLastDateTime write FPowerOffLastDateTime;
    property PowerOnFirstDateTimeToday: string read FPowerOnFirstDateTimeToday write FPowerOnFirstDateTimeToday;

    property NewServerURL: string read FNewServerURL write FNewServerURL;
    property DriveLettersForAttachment: string read FDriveLettersForAttachment write FDriveLettersForAttachment;
    property LastAttachmentDir: string read FLastAttachmentDir write FLastAttachmentDir;
    property SaveSyncXML: Boolean read FSaveSyncXML write FSaveSyncXML;
    property AppLocal: Boolean read FAppLocal write FAppLocal;
    property LastWindowsUnlockDateTime: string read FLastWindowsUnlockDateTime write FLastWindowsUnlockDateTime;
    property DebugUploadThread: Boolean read FDebugUploadThread write FDebugUploadThread;
    property ProfitCenter: string read FProfitCenter write FProfitCenter;
    property NotesFixDateTime: string read FNotesFixDateTime write FNotesFixDateTime;
    property LastUpdateFileHash: string read FLastUpdateFileHash write FLastUpdateFileHash;
    property RegisteredForCamera: Boolean read FRegisteredForCamera write FRegisteredForCamera;
    property APIKey: string read FAPIKey write FAPIKey;
    property QMLogic2URL: string read GetQMLogic2URL write FQMLogic2URL;
    property QtrMinEnabled: boolean read FQtrMinEnabled write FQtrMinEnabled;
    property ScreenShotPath: string read FScreenShotPath write FScreenShotPath;
    property GoogleAPIStr: string read FGoogleAPIStr write FGoogleAPIStr;

    {QMANTWO-607 EB - Debug properties}
    property DebugStatus: string read fDebugStatus write fDebugStatus; //QM-805 EB Debug ini refactoring
    property SecondaryDebugOn: boolean read fSecondaryDebugOn write fSecondaryDebugOn; //QM-805 EB Debug ini refactoring
    property DebugDeviceOverride: Boolean read fDebugDeviceOverride write fDebugDeviceOverride; //QM-805 EB Debug ini refactoring
    property DebugADIgnore: boolean read fDebugADIgnore write fDebugADIgnore;
    property DebugZScalerIgnore: boolean read fDebugZScalerIgnore write fDebugZScalerIgnore; //QM-876 Maintenance
    property DebugLocalDomain: string read fDebugLocalDomain write fDebugLocalDomain;
    property DebugWorkMgmtDebug: boolean read fDebugWorkMgmtDebug write fDebugWorkMgmtDebug;
    property DebugPlatsAltLocation: string read fDebugPlatsAltLocation write fDebugPlatsAltLocation;  //QM-164
    property DebugUseAltPlatsLoc: boolean read fDebugUseAltPlatsLoc write fDebugUseAltPlatsLoc; //QM-164
    property Override_AirCardDeviceID: string read fOverride_AirCardDeviceID write fOverride_AirCardDeviceID; //QM-805 EB Debug Refactoring
    property Override_AirCardBrand: string read fOverride_AirCardBrand write fOverride_AirCardBrand; //QM-805 EB Debug Refactoring
    property Override_AirCardPhone: string read fOverride_AirCardPhone write fOverride_AirCardPhone; //QM-805 EB Debug Refactoring
    property Override_SimCardDeviceID: string read fOverride_SimCardDeviceID write fOverride_SimCardDeviceID; //QM-805 EB Debug Refactoring
    property Override_Subscriber: string read fOverride_Subscriber write fOverride_Subscriber; //QM-805 EB Debug Refactoring

    {QM-147 EB - Time Access properties}
    property UsePowershellTime: Boolean read fUsePowershellTime write fUsePowershellTime;

    {QM-164 EB - Record Device Information (Config/Plats)}
    property DeviceRec: TDeviceRec read fDeviceRec write fDeviceRec;
    property PrevDeviceRec: TDeviceRec read fPrevDeviceRec write fPrevDeviceRec;


    {QM-494 EB - First Task}
    property FTR: TFTR read fFTR write fFTR;

    {QM-444 Part 2 and other grid coloring EB}
    property IsQualified: TColor read fIsQualified write fIsQualified;
    property IsAboutToExpire: TColor read fIsAboutToExpire write fIsAboutToExpire;
    property IsNotQualified: TColor read fIsNotQualified write fIsNotQualified;


    function BrandLocatorsInc: Boolean;
    procedure SaveToIni;
    property IniFileName: string read FIniFileName;
    property DebugConfigFileName: string read FDebugConfigFileName;
    constructor Create; virtual;
    constructor CreateFromIniFile(IniFileName: string); virtual;
    function CreateIniFile: TIniFile;
    function CreateDebugConfigIni(OrigIniFileName: string): TIniFile;
    function RemoveRememberedLogin: Boolean;
    function GetScreenShotPath: string;
    procedure UpdateFirstTaskIni(ReminderStr: String; Required: boolean);  //QM-494 First Task EB

    
    destructor Destroy; override;
  end;

implementation

uses SysUtils, OdMiscUtils, PasswordRules;

const
  clLightRed = 10987499;

  {Ini Sections}
  PrefsSection = 'Preferences';
  ServerSection = 'Server';
  QMLogic2Section = 'Server2';
  DevServerSection = 'DevServers';

  //  AllServersSection = 'AllServers';
  UserSection = 'User';
  SystemSection = 'System';
  ReportsSection = 'Reports';
  UISection = 'UI';
  QtrMinSection = 'QtrMin';
  OtherAppsSection = 'OtherApps';
  TimeAdjustSection = 'TimeAdjust';

  {QMANTWO-607 Debug and Config Sections}
  DebugSection = 'Debug_Settings';
  ConfigSection = 'Config';
  AllServersSection = 'AllServers';
  PreviousUserNamesSection = 'PreviousUserNames';
  AlternateSection = 'Alternate';


  {QM-164 Recording the Device Information}
  DeviceSection = 'Device';


  {Ini Itens}
  ServerURLIdent = 'URL';
  PreviousServerURLIdent = 'PreviousURL';
  WebUpdateURLIdent = 'WebUpdateURL';
  UserNameIdent = 'UserName';
  DisplayNameIdent = 'DisplayName';
  PasswordIdent = 'Password';
  RememberLoginDataIdent = 'RememberLoginData';
  ShadeColorIdent = 'ShadeColor';
  DueTicketColorIdent = 'DueTicketColor';
  DueHoursTicketColorIdent = 'DueHoursTicketColor';
  CriticalTicketColorIdent = 'CriticalTicketColor';
  DoNotMarkTicketColorIdent = 'DoNotMarkTicketColor';
  Due7TomorrowColorIndent = 'Due7TomorrowColor';
  HighestGeocodePrecisionColorIdent = 'HighestGeocodePrecisionColor';
  AcceptableGeocodePrecisionColorIdent = 'AcceptableGeocodePrecisionColor';
  ZeroGeocodeMatchesColorIdent = 'ZeroGeocodeMatchesColor';
  BaseHelpURLIdent = 'BaseHelpURL';
  LastLocalSyncTimeIdent = 'LastLocalSyncTime';
  LastTicketDataCleanIndent = 'LastTicketDataClean'; //QM-986 EB Reset Ticket Data on first Sync
  ShowColorPrefsIdent = 'ShowColorPrefs';
  ActivityReportLastDateTimeIdent = 'ActivityReportLastDateTime';
  ActivityReportLastOfficeIdent = 'ActivityReportLastOffice';
  ActivityReportLastManagerIdent = 'ActivityReportLastManager';
  AllowMultipleInstancesIdent = 'AllowMultipleInstances';
  WebUpdateLastDateTimeIdent = 'WebUpdateLastDateTime';
  EmpIDIdent = 'EmpID';
  UserIDIdent = 'UserID';
  PreviousLoginHashIdent = 'PreviousLoginHash';
  UserTypeIdent = 'UserType';
  EnableDamagesIdent = 'EnableDamages';
  EnableLitigationIdent = 'EnableLitigation';
  DeveloperModeIdent = 'Debug';
  DeveloperOverrideModeIdent = 'DevOverride';  //QM-579 EB

//  DeveloperSyncWait1Indent = 'DebugSyncWait1';  //QM-614 EB Only for debug
//  DeveloperSyncWait2Indent = 'DebugSyncWait2';
  URLVersionIdent = 'URLVersion';
  TMSplitterPosIdent = 'TMSplitterPos';
  ScanAndPatchLevelIdent = 'ScanAndPatchLevel';
  LastUploadLocationIdent = 'LastUploadLocation';
  LastAttachmentSaveDirIdent = 'LastAttachmentSaveDir';
  EncryptedPasswordIdent = 'EncryptedPassword';
  AllowRememberedLoginIdent = 'AllowRememberedLogin';
  TimeSubmitMessageLastDateIdent = 'TimeSubmitMessageLastDate';
  PowerOnLastDateTimeIdent = 'PowerOnLastDateTime';
  PowerOffLastDateTimeIdent = 'PowerOffLastDateTime';
  PowerOnFirstDateTimeTodayIdent = 'PowerOnFirstDateTimeToday';
  DriveLettersForAttachmentIdent = 'DriveLettersForAttachment';
  LastAttachmentDirIdent = 'LastAttachmentDir';
  SaveSyncXMLIdent = 'SaveSyncXML';
  AppLocalIdent = 'AppLocal';
  LastWindowsUnlockDateTimeIdent = 'LastWindowsUnlockDateTime';
  DebugUploadThreadIdent = 'DebugUploadThread';
  ProfitCenterIdent = 'ProfitCenter';
  NotesFixDateTimeIdent = 'NotesFixDateTime';
  LastUpdateFileHashIdent = 'LastUpdateFileHash';
  RegisteredForCameraIdent = 'RegisteredForCamera';
  APIKeyIdent = 'APIKey';
  QtrMinEnabledIdent = 'EnableQtrMin';
  ScreenShotIndent = 'ScreenShotPath';
  GoogleAPIIndent = 'GoogleAPIKey';

  {Debug Values only - Settings for development and testing (QManagerDebugConfig.Ini}
  DebugADIndent = 'ADIgnore';  //QMANTWO-607 EB
  DebugZScalerIndent = 'ZScalerIgnore'; //QM-876 EB Maint
  StatusIndent = 'Status';  //QMANTWO-607 EB
  DebugAirCardIndent = 'AirCardOn';
  DebugLocDomainIndent = 'LocalDomain';
  DebugWorkMgmtDebugIndent = 'WorkManagementSelect';   //QM-11 EB Hide from standard debug
  DebugPlatsAltLocationIndent = 'PlatsAltLocation'; //QM-164
  DebugUseAltPlatsLocIndent = 'UseAltPlatsLoc'; //QM-164
  DebugDeviceOverrideIndent='DeviceOverride';  //QM-805 EB Refactor Debug INI
  SecondaryDebugOnIndent='SecondaryDebugOn';   //QM-805 EB Refactor Debug INI
  Override_AirCardDeviceIDIndent = 'Override_AirCardDeviceID'; //QM-805 EB Refactor Debug INI
  Override_AirCardBrandIndent = 'Override_AirCardBrand'; //QM-805 EB Refactor Debug INI
  Override_AirCardPhoneIndent =  'Override_AirCardPhone';  //QM-805 EB Refactor Debug INI
  Override_SimCardDeviceIDIndent = 'Override_SimCardDeviceID'; //QM-805 EB Refactor Debug INI
  Override_SubscriberIndent = 'Override_Subscriber'; //QM-805 EB Refactor Debug INI

  {Time Adjust - QM-147 EB Powershell}
  UsePowershellIndent = 'UsePowershellStartTime';

  {Device values - QM-164}
    Device_AirCardDeviceIDIndent = 'Device_AirCardDeviceID';
    Device_AirCardBrandIndent = 'Device_AirCardBrand';
    Device_AirCardPhoneIndent = 'Device_AirCardPhone';
    Device_SimCardDeviceIDIndent = 'Device_SimCardDeviceID';
    Device_SubscriberIndent = 'Device_Subscriber';
    Device_LastUpdatedIndent = 'Device_LastUpdated';

  {First Task - QM-494}
    FirstTaskReminderIndent = 'ftr';
    FirstTaskReminder_lastupdateIndent = 'ftr_lastupdate';
    FirstTaskReminder_requiredIndent = 'ftr_req';
    FirstTaskReminder_debugIndent = 'ftr_debug';


{ TUQState }

function TUQState.BrandLocatorsInc: Boolean;
begin
  Result := StrContainsText('locqm', FServerURL);
end;

function TUQState.CompanyShortName: string;
begin
  if BrandLocatorsInc then
    Result := 'Loc. Inc.'
  else
    Result := 'UQ/STS';
end;

procedure TUQState.CorrectDueColor;
begin
  if DueTicketColor = 255 then
    DueTicketColor := 8454143;
end;

constructor TUQState.Create;
begin
  inherited;
  // When created this way, the State object is not persisted
  Initialize;
end;

function TUQState.CreateDebugConfigIni(OrigIniFileName: string): TIniFile;
var
  DebugConfigIniName: string;
begin
  {QMANTWO-607 EB
   Craete the DebugConfig File name from the standard Ini name: OrigIniFileName}
   DebugConfigIniName := ChangeFileExt(OrigIniFileName, '');  //OrigIniFileName;
   DebugConfigIniName := DebugConfigIniName + 'DebugConfig.Ini';
//   if FileExists(DebugConfigIniName) then
     Result := TIniFile.Create(DebugConfigIniName);

end;

constructor TUQState.CreateFromIniFile(IniFileName: string);
begin
  inherited;
  // When created this way, the State object is persisted (when told to)
  FIniFileName := IniFileName;
  Initialize;
  LoadFromIni;
  SetupDamageLiabililtyDescriptions;
end;

function TUQState.CreateIniFile: TIniFile;
begin
  Result := TIniFile.Create(FIniFileName);
end;

destructor TUQState.Destroy;
begin
  FreeAndNil(FServerUrls);
  FreeAndNil(FPrevUserNames);  //QMANTWO-607 EB
  inherited;
end;

procedure TUQState.Initialize;
begin
  ShadeColor := $00EBEBEB; // Light silver
  DueTicketColor := $0080FFFF; // Light yellow
  DueHoursTicketColor := $00FDCCE6; // Light purple
  CriticalTicketColor := $009B9DE3; // Light red
  DoNotMarkTicketColor := $00A6D2FF; // Light orange
  HighestGeocodePrecisionColor := $0096FF96; // green
  AcceptableGeocodePrecisionColor := $0000E8E8; // yellow
  ZeroGeocodeMatchesColor := $00FAFAFA; // white, same as background
  Due7TomorrowColor := $0080FFFF; //Light yellow

  {QM-444}
  IsAboutToExpire := $0080FFFF; // Light yellow
  IsQualified := clMoneyGreen; //$0096FF96; // green
  IsNotQualified := $009B9DE3; // Light red

  TMSplitterPos := 235;

  FServerUrls := TStringList.Create;
  FPrevUserNames := TStringList.Create;

  fFTR.Reminder := '';
  fFTR.LastUpdated := Now - 10;
end;

procedure TUQState.LoadDebugConfig(DCIniFile: TIniFile);
begin
  if FileExists(DCIniFile.FileName) then begin
    {Override Settings}
    DebugDeviceOverride := DCINIFile.ReadBool(DeviceSection, DebugDeviceOverrideIndent, False);  //QM-805 EB Debug Refactor
    SecondaryDebugOn := DCINIFile.ReadBool(ConfigSection, SecondaryDebugOnIndent, False); //QM-805 EB Debug Refactor
    {Other Debug Settings}
    DebugADIgnore := DCINIFile.ReadBool(DebugSection, DebugADIndent, False);  //QMANTWO-607 EB)
    DebugLocalDomain := DCINIFile.ReadString(DebugSection, DebugLocDomainIndent, DebugLocalDomain);
    DebugWorkMgmtDebug := DCINIFile.ReadBool(DebugSection, DebugWorkMgmtDebugIndent, False);  //QM-11 EB Move Debug to QManagerDebugConfig.ini
    DebugPlatsAltLocation := DCINIFile.ReadString(AlternateSection, DebugPlatsAltLocationIndent, DebugPlatsAltLocation);  //QM-164 EB
    DebugUseAltPlatsLoc := DCINIFile.ReadBool(AlternateSection, DebugUseAltPlatsLocIndent, False);     //QM-164 EB
    {Debug Override Settings}
    Override_AirCardDeviceID := DCINIFile.ReadString(DeviceSection, Override_AirCardDeviceIDIndent, '--'); //QM-805 EB Debug Refactoring
    Override_AirCardBrand := DCINIFile.ReadString(DeviceSection, Override_AirCardBrandIndent, '--'); //QM-805 EB Debug Refactoring
    Override_AirCardPhone := DCINIFile.ReadString(DeviceSection, Override_AirCardPhoneIndent, '--'); //QM-805 EB Debug Refactoring
    Override_SimCardDeviceID := DCINIFile.ReadString(DeviceSection, Override_SimCardDeviceIDIndent, '--'); //QM-805 EB Debug Refactoring
    Override_Subscriber := DCINIFile.ReadString(DeviceSection, Override_SubscriberIndent, '--');  //QM-805 EB Debug Refactoring

    {Read Servers}
    DCIniFile.ReadSectionValues(AllServersSection, FServerUrls);
    DCIniFile.ReadSectionValues(PreviousUserNamesSection, FPrevUserNames);

    {ZScaler Bypass for Dev}
    DebugZScalerIgnore := DCINIFile.ReadBool(DebugSection, DebugZScalerIndent, False);   //QM-876 EB Maint
  end

  else begin {Create one}
    DCIniFile.WriteString(ConfigSection, StatusIndent, 'New' );
    DCIniFile.WriteBool(ConfigSection, SecondaryDebugONIndent, FALSE);    //QM-805 EB Debug Refactor: Explicitly turh off this file
  end;
end;

procedure TUQState.LoadFromIni;
var
  IniFile: TIniFile;
  DebugConfigIni: TIniFile;
  s: string;
  Buf: array[0..80] of char;
  BufSize: Integer;
  HexString: string;
  i: Integer;
  HexEncoded: Boolean;
begin
  if not FileExists(IniFileName) then
    Exit;

  IniFile := TIniFile.Create(IniFileName);
  DebugConfigIni := CreateDebugConfigIni(IniFileName);  {QMANTWO-607 EB}

  try
    // do this before reading the [User] section's keys
    AllowRememberedLogin := IniFile.ReadBool(UserSection, AllowRememberedLoginIdent, AllowRememberedLogin);
    if not AllowRememberedLogin then
      RemoveRememberedLogin;

    ServerURL := INIFile.ReadString(ServerSection, ServerURLIdent, ServerURL);
    PreviousServerURL := INIFile.ReadString(ServerSection, PreviousServerURLIdent, ServerURL);
    WebUpdateURL :=  INIFile.ReadString(ServerSection, WebUpdateURLIdent, WebUpdateURL);
    UserName := IniFile.ReadString(UserSection, UserNameIdent, UserName);
    DisplayName := IniFile.ReadString(UserSection, DisplayNameIdent, DisplayName);
    APIKey := IniFile.ReadString(UserSection, APIKeyIdent, APIKey);
    EncryptedPassword := IniFile.ReadBool(UserSection, EncryptedPasswordIdent, EncryptedPassword);
    s := IniFile.ReadString(UserSection, PasswordIdent, Password);
    RememberLoginData := IniFile.ReadBool(UserSection, RememberLoginDataIdent, RememberLoginData);
    if EncryptedPassword then
    begin
      // Don't use the string if it's not a hex-encoded string
      HexEncoded := True;
      for i := 1 to Length(s) do begin
        if (not (((s[i] >= '0') and (s[i] <= '9')) or ((s[i] >='A') and (s[i] <= 'F'))))
           or (Odd(Length(s)))  then begin
          HexEncoded := False;
          Password := '';
          Break;
        end;
      end;

      if HexEncoded then begin
        BufSize := SizeOf(Buf);
        FillChar(Buf, SizeOf(Buf), 0);
        HexToBin(pChar(s), Buf, BufSize);
        HexString := string(Buf);
        Password := SuperCipher(HexString);
      end;
    end else
      Password := s;
    PreviousUser := UserName;
    if RememberLoginData then
      PlainPassword := Password
    else
      PlainPassword := '';
    ActivityReportLastDateTime := IniFile.ReadString(ReportsSection, ActivityReportLastDateTimeIdent, ActivityReportLastDateTime);
    ActivityReportLastOffice := IniFile.ReadInteger(ReportsSection, ActivityReportLastOfficeIdent, ActivityReportLastOffice);
    ActivityReportLastManager := IniFile.ReadInteger(ReportsSection, ActivityReportLastManagerIdent, ActivityReportLastManager);
    ShadeColor := IniFile.ReadInteger(PrefsSection, ShadeColorIdent, ShadeColor);
    DueTicketColor := IniFile.ReadInteger(PrefsSection, DueTicketColorIdent, DueTicketColor);
    DueHoursTicketColor := IniFile.ReadInteger(PrefsSection, DueHoursTicketColorIdent, DueHoursTicketColor);
    CriticalTicketColor := IniFile.ReadInteger(PrefsSection, CriticalTicketColorIdent, CriticalTicketColor);
    DoNotMarkTicketColor := IniFile.ReadInteger(PrefsSection, DoNotMarkTicketColorIdent, DoNotMarkTicketColor);
    Due7TomorrowColor := IniFile.ReadInteger(PrefsSection, Due7TomorrowColorIndent, Due7TomorrowColor);
    HighestGeocodePrecisionColor := IniFile.ReadInteger(PrefsSection,
      HighestGeocodePrecisionColorIdent, HighestGeocodePrecisionColor);
    AcceptableGeocodePrecisionColor := IniFile.ReadInteger(PrefsSection,
      AcceptableGeocodePrecisionColorIdent, AcceptableGeocodePrecisionColor);
    ZeroGeocodeMatchesColor := IniFile.ReadInteger(PrefsSection,
      ZeroGeocodeMatchesColorIdent, ZeroGeocodeMatchesColor);
    BaseHelpURL := IniFile.ReadString(SystemSection, BaseHelpURLIdent, BaseHelpURL);
    LastLocalSyncTime:=  IniFile.ReadDateTime(SystemSection, LastLocalSyncTimeIdent, LastLocalSyncTime);
    LastTicketDataClean:= IniFile.ReadDateTime(SystemSection, LastTicketDataCleanIndent, LastTicketDataClean);  //QM-986 EB Reset Ticket Data on First Sync
    ShowColorPrefs := IniFile.ReadBool(PrefsSection, ShowColorPrefsIdent, ShowColorPrefs);
    AllowMultipleInstances := IniFile.ReadBool(PrefsSection, AllowMultipleInstancesIdent, AllowMultipleInstances);
    WebUpdateLastDateTime := IniFile.ReadString(SystemSection, WebUpdateLastDateTimeIdent, WebUpdateLastDateTime);
    EmpID := IniFile.ReadInteger(UserSection, EmpIDIdent, 0);
    UserID := IniFile.ReadInteger(UserSection, UserIDIdent, 0);
    PreviousLoginHash := IniFile.ReadString(UserSection, PreviousLoginHashIdent, PreviousLoginHash);
    UserType := IniFile.ReadString(UserSection, UserTypeIdent, UserType);
    DeveloperMode := IniFile.ReadBool(UserSection, DeveloperModeIdent, DeveloperMode);
    DeveloperOverrideMode := IniFile.ReadBool(UserSection, DeveloperOverrideModeIdent, DeveloperOverrideMode);

//    DeveloperSyncWait1 := IniFile.ReadInteger(UserSection, DeveloperSyncWait1Indent, 0);  //QM-614 Eb Debug for Sync
//    DeveloperSyncWait2:= IniFile.ReadInteger(UserSection, DeveloperSyncWait2Indent, 0) ;  //QM-614 EB
    TMSplitterPos := IniFile.ReadInteger(UISection, TMSplitterPosIdent, TMSplitterPos);
    ScanAndPatchLevel := IniFile.ReadInteger(SystemSection, ScanAndPatchLevelIdent, 0);
    LastUploadLocation := IniFile.ReadInteger(UISection, LastUploadLocationIdent, -1);
    LastAttachmentSaveDir := IniFile.ReadString(UISection, LastAttachmentSaveDirIdent, '');
    TimeSubmitMessageLastDate := IniFile.ReadString(UserSection, TimeSubmitMessageLastDateIdent, TimeSubmitMessageLastDate);
    PowerOnLastDateTime := IniFile.ReadString(UserSection, PowerOnLastDateTimeIdent, PowerOnLastDateTime);
    PowerOffLastDateTime := IniFile.ReadString(UserSection, PowerOffLastDateTimeIdent, PowerOffLastDateTime);
    PowerOnFirstDateTimeToday := IniFile.ReadString(UserSection,PowerOnFirstDateTimeTodayIdent,PowerOnFirstDateTimeToday);
    DriveLettersForAttachment := IniFile.ReadString(PrefsSection, DriveLettersForAttachmentIdent, '');
    LastAttachmentDir := IniFile.ReadString(PrefsSection, LastAttachmentDirIdent, '');
    SaveSyncXML := IniFile.ReadBool(UserSection, SaveSyncXMLIdent, SaveSyncXML);
    AppLocal := IniFile.ReadBool(UserSection, AppLocalIdent, AppLocal);
    LastWindowsUnlockDateTime := IniFile.ReadString(UserSection, LastWindowsUnlockDateTimeIdent, LastWindowsUnlockDateTime);
    DebugUploadThread := IniFile.ReadBool(SystemSection, DebugUploadThreadIdent, False);
    ProfitCenter := IniFile.ReadString(UserSection, ProfitCenterIdent, ProfitCenter);
    NotesFixDateTime := IniFile.ReadString(UserSection, NotesFixDateTimeIdent, NotesFixDateTime);
    LastUpdateFileHash := IniFile.ReadString(UserSection, LastUpdateFileHashIdent, LastUpdateFileHash);
    RegisteredForCamera := IniFile.ReadBool(SystemSection, RegisteredForCameraIdent, False);
    QMLogic2URL := INIFile.ReadString(QMLogic2Section, ServerURLIdent, QMLogic2URL);
    QtrMinEnabled :=  INIFile.ReadBool(QtrMinSection, QtrMinEnabledIdent, QtrMinEnabled);
    ScreenShotPath := INIFile.ReadString(OtherAppsSection, ScreenshotIndent, ScreenShotPath);
    GoogleAPIStr := INIFile.ReadString(OtherAppsSection, GoogleAPIIndent, 'AIzaSyBUUmU6wnXyfE-j12-e-hsCwfVnve6PRPM');

    UsePowershellTime := INIFile.ReadBool(TimeAdjustSection, UsePowershellIndent, UsePowershellTime);     //QM-147 EB Powershell - may also be set in permissions

    ReadDeviceRec(IniFile); {QM-164 EB Config/Plats}
    {Make sure Debug flags are initially set}
    DebugADIgnore := False; //QM-876 EB Cleanup
    DebugZScalerIgnore := False;  //QM-876 EB Cleanup
    DebugLocalDomain := '';
    DebugDeviceOverride := False;   //QM-805 EB Refactor Debug Ini file
    SecondaryDebugOn := FALSE;      //QM-805 EB Refactor Debug Ini file

    {QM-494 First Task}
    ReadFirstTask(IniFile);


    {EB - All Debug Settings rely on the Debug=1: two step check}
    if DeveloperMode then begin
      LoadDebugConfig(DebugConfigIni);
	  IniFile.ReadSectionValues(DevServerSection, FServerUrls); //Larry's change?
 //     DebugAD := INIFile.ReadBool(DebugSection, DebugADIndent, DebugAD);  //QMANTWO-607 EB
    end;

    CorrectDueColor;
  finally
    FreeAndNil(IniFile);
  end;
end;

procedure TUQState.SaveToIni;
var
  IniFile: TIniFile;
  Buf: array[0..80] of char;
  HexString: string;
  EncryptedPasswordString: string;
begin
  if IniFileName = '' then
    Exit;

  IniFile := TIniFile.Create(IniFileName);
  try
    if NotEmpty(NewServerURL) then
      IniFile.WriteString(ServerSection, ServerURLIdent, NewServerURL)
    else
      IniFile.WriteString(ServerSection, ServerURLIdent, ServerURL);
    IniFile.WriteString(ServerSection, PreviousServerURLIdent, PreviousServerURL);
    IniFile.WriteString(ServerSection, WebUpdateURLIdent, WebUpdateURL);
    IniFile.WriteBool(UserSection, RememberLoginDataIdent, RememberLoginData);
    IniFile.WriteString(UserSection, UserNameIdent, UserName);
    IniFile.WriteString(UserSection, DisplayNameIdent, DisplayName);
    IniFile.WriteString(UserSection, APIKeyIdent, APIKey);

    if RememberLoginData then begin
      EncryptedPassword := True; // From this point forward, the password will always be stored encrypted
      Fillchar(Buf, SizeOf(Buf), 0);
      EncryptedPasswordString := SuperCipher(PlainPassword);
      BinToHex(pChar(EncryptedPasswordString), Buf, Length(EncryptedPasswordString)); // Make sure all chars in encrypted password are stored properly
      HexString := string(Buf);
      IniFile.WriteString(UserSection, PasswordIdent, HexString);
      IniFile.WriteBool(UserSection, EncryptedPasswordIdent, EncryptedPassword);
    end
    else
      IniFile.WriteString(UserSection, PasswordIdent, '');

    IniFile.WriteString(ReportsSection, ActivityReportLastDateTimeIdent, ActivityReportLastDateTime);
    IniFile.WriteInteger(ReportsSection, ActivityReportLastOfficeIdent, ActivityReportLastOffice);
    IniFile.WriteInteger(ReportsSection, ActivityReportLastManagerIdent, ActivityReportLastManager);

    IniFile.WriteInteger(PrefsSection, ShadeColorIdent, ShadeColor);
    IniFile.WriteInteger(PrefsSection, DueTicketColorIdent, DueTicketColor);
    IniFile.WriteInteger(PrefsSection, DueHoursTicketColorIdent, DueHoursTicketColor);
    IniFile.WriteInteger(PrefsSection, CriticalTicketColorIdent, CriticalTicketColor);
    IniFile.WriteInteger(PrefsSection, DoNotMarkTicketColorIdent, DoNotMarkTicketColor);
    IniFile.WriteInteger(PrefsSection, Due7TomorrowColorIndent, Due7TomorrowColor);
    IniFile.WriteInteger(PrefsSection, HighestGeocodePrecisionColorIdent,
      HighestGeocodePrecisionColor);
    IniFile.WriteInteger(PrefsSection, AcceptableGeocodePrecisionColorIdent,
      AcceptableGeocodePrecisionColor);
    IniFile.WriteInteger(PrefsSection, ZeroGeocodeMatchesColorIdent,
      ZeroGeocodeMatchesColor);
    IniFile.WriteString(SystemSection, BaseHelpURLIdent, BaseHelpURL);
    IniFile.WriteDateTime(SystemSection, LastLocalSyncTimeIdent, LastLocalSyncTime);
    IniFile.WriteDateTime(SystemSection, LastTicketDataCleanIndent, LastTicketDataClean);  //QM-986 EB Reset Ticket Data on first Sync
    IniFile.WriteBool(PrefsSection, ShowColorPrefsIdent, ShowColorPrefs);
    IniFile.WriteBool(PrefsSection, AllowMultipleInstancesIdent, AllowMultipleInstances);
    IniFile.WriteString(SystemSection, WebUpdateLastDateTimeIdent, WebUpdateLastDateTime);
    IniFile.WriteInteger(UserSection, EmpIDIdent, EmpID);
    IniFile.WriteInteger(UserSection, UserIDIdent, UserID);
    IniFile.WriteString(UserSection, PreviousLoginHashIdent, PreviousLoginHash);
    IniFile.WriteString(UserSection, UserTypeIdent, UserType);
    IniFile.WriteInteger(UISection, TMSplitterPosIdent, TMSplitterPos);
    IniFile.WriteInteger(SystemSection, ScanAndPatchLevelIdent, ScanAndPatchLevel);
    IniFile.WriteInteger(UISection, LastUploadLocationIdent, LastUploadLocation);
    IniFile.WriteString(UISection, LastAttachmentSaveDirIdent, LastAttachmentSaveDir);
    IniFile.WriteString(UserSection, TimeSubmitMessageLastDateIdent, TimeSubmitMessageLastDate);
    IniFile.WriteString(UserSection, PowerOnLastDateTimeIdent, PowerOnLastDateTime);
    IniFile.WriteString(UserSection, PowerOffLastDateTimeIdent, PowerOffLastDateTime);
    IniFile.WriteString(UserSection, PowerOnFirstDateTimeTodayIdent, PowerOnFirstDateTimeToday);
    IniFile.WriteString(PrefsSection, DriveLettersForAttachmentIdent, DriveLettersForAttachment);
    IniFile.WriteString(PrefsSection, LastAttachmentDirIdent, LastAttachmentDir);
    IniFile.WriteString(UserSection, LastWindowsUnlockDateTimeIdent, LastWindowsUnlockDateTime);
    IniFile.WriteString(UserSection, ProfitCenterIdent, ProfitCenter);
    IniFile.WriteString(UserSection, NotesFixDateTimeIdent, NotesFixDateTime);
    IniFile.WriteString(UserSection, LastUpdateFileHashIdent, LastUpdateFileHash);
    IniFile.WriteBool(SystemSection, RegisteredForCameraIdent, RegisteredForCamera);

    UpdateDeviceRec(IniFile); {QM-164 EB - Config/Plats}
  finally
    FreeAndNil(IniFile);
  end;
end;

procedure TUQState.SetPassword(const Value: string);
begin
  if (RememberLoginData) or (Value = '') or (PasswordIsHashed(Value)) then
    FPassword := Value
  else
    raise Exception.Create('Invalid password hash');
end;

procedure TUQState.SetupDamageLiabililtyDescriptions;
begin
  DamageLiabilityDescription[dlUQHas] := CompanyShortName + ' has liability';
  DamageLiabilityDescription[dlUQNotHave] := CompanyShortName + ' has no liability';
  DamageLiabilityDescription[dlOnlyUQ] := 'Only ' + CompanyShortName + ' is liable';
  DamageLiabilityDescription[dlUQAndExcavator] := CompanyShortName + ' and excavator are liable';
  DamageLiabilityDescription[dlNeither] := 'Neither ' + CompanyShortName +  ' nor excavator are liable';
  DamageLiabilityDescription[dlAny] := 'ANY/ALL liability';
end;

procedure TUQState.ReadDeviceRec(TheIniFile: TIniFile);  {QM-164 EB Config/Plats}
begin
  {Backup previous values if there are any}

  if assigned(TheIniFile) then begin
    fDeviceRec.AirCardDeviceID := TheIniFile.ReadString(DeviceSection, Device_AirCardDeviceIDIndent, fDeviceRec.AirCardDeviceID);
    fDeviceRec.AirCardBrand := TheIniFile.ReadString(DeviceSection, Device_AirCardBrandIndent, fDeviceRec.AirCardBrand);
    fDeviceRec.AirCardPhone := TheIniFile.ReadString(DeviceSection, Device_AirCardPhoneIndent, fDeviceRec.AirCardPhone);
    fDeviceRec.SimCardDeviceID := TheIniFile.ReadString(DeviceSection, Device_SimCardDeviceIDIndent, fDeviceRec.SimCardDeviceID);
    fDeviceRec.Subscriber := TheIniFile.ReadString(DeviceSection, Device_SubscriberIndent, fDeviceRec.Subscriber);
    fDeviceRec.LastUpdated := TheIniFile.ReadDateTime(DeviceSection, Device_LastUpdatedIndent, fDeviceRec.LastUpdated);
  end;
end;




procedure TUQState.UpdateDeviceRec(TheIniFile: TIniFile);
begin
  {QM-164 EB //QM-805 removed the requirement that there are values}
  if assigned(TheIniFile) then begin
    TheIniFile.WriteString(DeviceSection, Device_AirCardDeviceIDIndent, fDeviceRec.AirCardDeviceID);
    TheIniFile.WriteString(DeviceSection, Device_AirCardBrandIndent, fDeviceRec.AirCardBrand);
    TheIniFile.WriteString(DeviceSection, Device_AirCardPhoneIndent, fDeviceRec.AirCardPhone);
    TheIniFile.WriteString(DeviceSection, Device_SimCardDeviceIDIndent, fDeviceRec.SimCardDeviceID);
    TheIniFile.WriteString(DeviceSection, Device_SubscriberIndent, fDeviceRec.Subscriber);
    TheIniFile.WriteDateTime(DeviceSection, Device_LastUpdatedIndent, fDeviceRec.LastUpdated);
  end;
end;

procedure TUQState.ReadFirstTask(TheIniFile: TIniFile); //QM-494 First Task EB
begin
  if assigned(TheIniFile) then begin
    fFTR.Reminder := TheIniFile.ReadString(UserSection, FirstTaskReminderIndent, fFTR.Reminder);
    fFTR.LastUpdated := TheIniFile.ReadDateTime(UserSEction, FirstTaskReminder_lastupdateIndent, fFTR.LastUpdated);
    fFTR.Required := TheIniFile.ReadBool(UserSection, FirstTaskReminder_requiredIndent, fFTR.Required);
    fFTR.Debug := TheIniFile.ReadBool(UserSection, FirstTaskReminder_debugIndent, False);  {This has to be manually set}
  end;
end;

procedure TUQState.UpdateFirstTaskIni(ReminderStr: String; Required: boolean);  //QM-494 First Task EB
var
  TheIniFile: TiniFile;
begin
  TheIniFile := TIniFile.Create(IniFileName);
  try
    {If it is not required, the ReminderStr is just set to ''}
    TheIniFile.WriteBool(UserSection, FirstTaskReminder_requiredIndent, Required);
    TheIniFile.WriteString(UserSection, FirstTaskReminderIndent, ReminderStr);
    TheIniFile.WriteDateTime(UserSection, FirstTaskReminder_lastupdateIndent, Now);
  finally
    FreeAndNil(TheIniFile);
  end;
end;

function TUQState.RemoveRememberedLogin: Boolean;
const
  DeleteKeyArray: array[0..1] of string = (
    PasswordIdent,
    RememberLoginDataIdent);
var
  IniFile: TIniFile;
  I: Integer;
begin
  Result := False;
  IniFile := TIniFile.Create(IniFileName);
  try
    for I := Low(DeleteKeyArray) to High(DeleteKeyArray) do begin
      if IniFile.ValueExists(UserSection, DeleteKeyArray[I]) then begin
        IniFile.DeleteKey(UserSection, DeleteKeyArray[I]);
        Result := True;
      end;
    end;
  finally
    FreeAndNil(IniFile);
  end;
end;

function TUQState.GetScreenShotPath: string;
begin
  Result := ScreenShotPath;
end;

function TUQState.GetServerURL: string;
begin
  Result := RemoveSlash(FServerURL);
end;

function TUQState.GetQMLogic2URL: string;
begin
  Result := RemoveSlash(FQMLogic2URL);
end;

end.
