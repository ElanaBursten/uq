unit DMHttpServer;

interface

uses
  SysUtils, Classes, IdHTTPServer, IdContext, IdBaseComponent, 
  IdCustomHTTPServer, IdSocketHandle, ExtCtrls, AppEvnts, Windows,
  Messages, QMConst, OdIdleTimer;

type
  TDMHttp = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure IdHTTPServerCommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
  private
    IdHTTPServer: TIdHTTPServer;
    FIdleTimer: TOdIdleTimer;
    FBinding: TIdSocketHandle;
    function GetIsIdle: Boolean;
  public
    constructor Create(IdleTimer: TOdIdleTimer); reintroduce;
    procedure ActivateHttpServer;
    procedure StopHttpServer;
    property IsIdle: boolean read GetIsIdle;
  end;

var
  DMHttp: TDMHttp;

implementation

{$R *.dfm}
uses
  DMu, TimeClockImport, JclFileUtils, LocalPermissionsDMu;

constructor TDMHttp.Create(IdleTimer: TOdIdleTimer);
begin
  inherited Create(nil);
  Assert(Assigned(IdleTimer), 'IdleTimer must be created before creating the http server');
  FIdleTimer := IdleTimer;
end;

procedure TDMHttp.DataModuleCreate(Sender: TObject);
begin
  FBinding := nil;
  IdHTTPServer := TIdHTTPServer.Create(Self);
  IdHttpServer.OnCommandGet := IdHTTPServerCommandGet;
end;

procedure TDMHttp.DataModuleDestroy(Sender: TObject);
begin
  StopHttpServer;
  FreeAndNil(FBinding);
  FreeAndNil(IdHTTPServer);
end;

procedure TDMHttp.ActivateHttpServer;
begin
  Assert(Assigned(IdHttpServer), 'IdHttpServer must be created first');
  StopHttpServer;
  if Assigned(FBinding) then
    FreeAndNil(FBinding);

  FBinding := IdHttpServer.Bindings.Add;
  FBinding.IP := '127.0.0.1';
  FBinding.Port := StrToIntDef(DM.GetConfigurationDataValue('QMClientHttpPort'), 9236);
  IdHttpServer.Active := PermissionsDM.CanI(RightTimesheetsClockImport);
end;

procedure TDMHttp.StopHttpServer;
begin
  if Assigned(IdHttpServer) and IdHttpServer.Active then
    IdHttpServer.Active := False;
end;

procedure TDMHttp.IdHTTPServerCommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
const
  XML = '<?xml version="1.0" encoding="UTF-8"?><QManager><IsIdle>%s</IsIdle></QManager>';
var
  Status: string;
  SavedTimeImportFileName: string;
begin
  AResponseInfo.ResponseNo := 404;
  Status := 'The request was not recognized. Try /IsIdle or /TimeImport instead.';
  AResponseInfo.ContentText := 'The request was not recognized. Try /IsIdle or /TimeImport instead.';

  if SameText(ARequestInfo.Document, '/IsIdle') then begin
    Status := BoolToStr(IsIdle, True);
    AResponseInfo.ContentText := Format(XML, [Status]);
  end
  else if SameText(ARequestInfo.Document, '/TimeImport') then begin
    if SaveHTTPPostedTimeImportFile(ARequestInfo.PostStream, SavedTimeImportFileName) then begin
      AResponseInfo.ResponseNo := 200;
      AResponseInfo.ContentText := Format('http://localhost:%d/TimeImportStatus?%s',
        [FBinding.Port, PathExtractFileNameNoExt(SavedTimeImportFileName)]);
    end
    else begin
      AResponseInfo.ResponseNo := 500;
      AResponseInfo.ContentText := 'Could not process time import XML. Please check the header' +
        ' content type is set to text/xml and that the XML is formatted correctly.';
    end;
  end else if SameText(ARequestInfo.Document, '/TimeImportStatus') then begin
    AResponseInfo.ContentText := GetTimeImportStatus(ARequestInfo.QueryParams);
    if Copy(AResponseInfo.ContentText, 1, 3) = '404' then
      AResponseInfo.ResponseNo := 404
    else
      AResponseInfo.ResponseNo := 200;
  end;

end;

function TDMHttp.GetIsIdle: Boolean;
var
  EditedDatasetName: string;
begin
  Result := (not DM.AnyDatasetInEditMode(EditedDatasetName)) and
    FIdleTimer.IsApplicationIdle and (not DM.eSketchIsRunning);
end;

end.
