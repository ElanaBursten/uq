inherited BillingExceptionReportForm: TBillingExceptionReportForm
  Left = 389
  Top = 364
  Caption = 'Billing Exception'
  DesignSize = (
    548
    400)
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 0
    Top = 87
    Width = 65
    Height = 20
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  object Label4: TLabel [1]
    Left = 360
    Top = 87
    Width = 28
    Height = 13
    Caption = 'Units:'
  end
  object Label1: TLabel [2]
    Left = 394
    Top = 111
    Width = 103
    Height = 66
    Caption = 
      '"Units" is the minimum number of units to look for in duplicate ' +
      'locates billed.'
    WordWrap = True
  end
  object DateRangeLabel: TLabel [3]
    Left = 0
    Top = 8
    Width = 65
    Height = 20
    AutoSize = False
    Caption = 'Date Range:'
    WordWrap = True
  end
  object CallCenterList: TCheckListBox [4]
    Left = 74
    Top = 84
    Width = 275
    Height = 309
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object Units: TEdit [5]
    Left = 394
    Top = 84
    Width = 77
    Height = 21
    TabOrder = 1
    OnKeyPress = UnitsKeyPress
  end
  inline RangeSelect: TOdRangeSelectFrame [6]
    Left = 2
    Top = 21
    Width = 287
    Height = 63
    TabOrder = 2
    inherited ToLabel: TLabel
      Left = 168
    end
    inherited DatesComboBox: TComboBox
      Left = 72
      Width = 205
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 72
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 189
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 381
  end
end
