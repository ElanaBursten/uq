inherited DamageThirdPartySearchCriteria: TDamageThirdPartySearchCriteria
  Left = 253
  Top = 208
  Caption = 'Damage Third Party Search'
  ClientHeight = 194
  ClientWidth = 645
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 34
    Top = 29
    Width = 57
    Height = 13
    Caption = 'Damage ID:'
  end
  inherited RecordsLabel: TLabel
    Left = 264
    Top = 146
    Width = 113
  end
  object Label3: TLabel [2]
    Left = 34
    Top = 53
    Width = 71
    Height = 12
    Caption = 'Third Party ID:'
  end
  object Label5: TLabel [3]
    Left = 34
    Top = 77
    Width = 37
    Height = 13
    Caption = 'Carrier:'
  end
  object Label2: TLabel [4]
    Left = 34
    Top = 6
    Width = 45
    Height = 13
    Caption = 'Claimant:'
  end
  object ProfitCenterLabel: TLabel [5]
    Left = 34
    Top = 102
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object ClaimStatusLabel: TLabel [6]
    Left = 34
    Top = 123
    Width = 63
    Height = 13
    Caption = 'Claim Status:'
  end
  object UtilityCoDamagedLabel: TLabel [7]
    Left = 301
    Top = 6
    Width = 104
    Height = 19
    Caption = 'Utility Co. Damaged:'
    WordWrap = True
  end
  object AddressLabel: TLabel [8]
    Left = 301
    Top = 29
    Width = 43
    Height = 13
    Caption = 'Address:'
  end
  object CityLabel: TLabel [9]
    Left = 301
    Top = 53
    Width = 23
    Height = 13
    Caption = 'City:'
  end
  object StateLabel: TLabel [10]
    Left = 301
    Top = 77
    Width = 30
    Height = 13
    Caption = 'State:'
  end
  inherited SearchButton: TButton
    Left = 264
    Top = 166
    TabOrder = 11
  end
  inherited CancelButton: TButton
    Left = 584
    Top = 166
    TabOrder = 14
  end
  inherited CopyButton: TButton
    Left = 344
    Top = 166
    TabOrder = 12
  end
  inherited ExportButton: TButton
    Left = 424
    Top = 166
    TabOrder = 13
  end
  inherited PrintButton: TButton
    TabOrder = 15
  end
  inherited ClearButton: TButton
    Left = 504
    Top = 166
    TabOrder = 16
  end
  object UQDamageID: TEdit
    Left = 109
    Top = 26
    Width = 143
    Height = 21
    TabOrder = 1
  end
  object ThirdPartyID: TEdit
    Left = 109
    Top = 50
    Width = 143
    Height = 21
    TabOrder = 2
  end
  object CarrierSearch: TComboBox
    Left = 109
    Top = 74
    Width = 162
    Height = 21
    Style = csDropDownList
    DropDownCount = 15
    ItemHeight = 13
    TabOrder = 3
  end
  object Claimant: TEdit
    Left = 109
    Top = 3
    Width = 143
    Height = 21
    TabOrder = 0
  end
  object ProfitCenter: TComboBox
    Left = 109
    Top = 99
    Width = 162
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 4
  end
  object ClaimStatus: TComboBox
    Left = 109
    Top = 122
    Width = 162
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 5
  end
  object UtilityCoDamaged: TEdit
    Left = 409
    Top = 3
    Width = 197
    Height = 21
    TabOrder = 6
  end
  object Address: TEdit
    Left = 409
    Top = 26
    Width = 197
    Height = 21
    TabOrder = 7
  end
  object City: TEdit
    Left = 409
    Top = 50
    Width = 198
    Height = 21
    TabOrder = 8
  end
  object State: TComboBox
    Left = 409
    Top = 74
    Width = 198
    Height = 21
    Style = csDropDownList
    CharCase = ecUpperCase
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 9
  end
  inline NotifiedDate: TOdRangeSelectFrame
    Left = 297
    Top = 97
    Width = 310
    Height = 47
    TabOrder = 10
    inherited FromLabel: TLabel
      Left = 4
      Top = 5
      Width = 103
      Height = 18
      Caption = 'Notified Date Range:'
      WordWrap = True
    end
    inherited ToLabel: TLabel
      Left = 203
      Top = 6
    end
    inherited DatesLabel: TLabel
      Left = 4
      Top = 28
    end
    inherited DatesComboBox: TComboBox
      Left = 112
      Top = 25
      Width = 198
      DropDownCount = 14
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 112
      Top = 2
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 224
      Top = 2
    end
  end
end
