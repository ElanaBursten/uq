inherited DamageListReportForm: TDamageListReportForm
  Left = 155
  Top = 96
  Caption = 'DamageListReportForm'
  ClientHeight = 580
  ClientWidth = 770
  PixelsPerInch = 96
  TextHeight = 13
  object ExcavatorLabel: TLabel [0]
    Left = 249
    Top = 277
    Width = 53
    Height = 13
    Caption = 'Excavator:'
  end
  object TypeLabel: TLabel [1]
    Left = 0
    Top = 323
    Width = 73
    Height = 13
    Caption = 'Invest. Status:'
  end
  object ProfitCenterLabel: TLabel [2]
    Left = 0
    Top = 237
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object UtilityCoDamagedLabel: TLabel [3]
    Left = 249
    Top = 317
    Width = 86
    Height = 29
    AutoSize = False
    Caption = 'Utility Company Damaged:'
    WordWrap = True
  end
  object Label2: TLabel [4]
    Left = 249
    Top = 300
    Width = 94
    Height = 19
    AutoSize = False
    Caption = 'Work To Be Done:'
    WordWrap = True
  end
  object AttachmentsLabel: TLabel [5]
    Left = 493
    Top = 277
    Width = 65
    Height = 13
    Caption = 'Attachments:'
  end
  object InvoicesLabel: TLabel [6]
    Left = 492
    Top = 300
    Width = 44
    Height = 13
    Caption = 'Invoices:'
  end
  object CityLabel: TLabel [7]
    Left = 250
    Top = 36
    Width = 23
    Height = 13
    Caption = 'City:'
  end
  object LocationLabel: TLabel [8]
    Left = 250
    Top = 11
    Width = 44
    Height = 13
    Caption = 'Location:'
  end
  object StateLabel: TLabel [9]
    Left = 435
    Top = 36
    Width = 18
    Height = 13
    Caption = 'St.:'
  end
  object Label3: TLabel [10]
    Left = 0
    Top = 264
    Width = 84
    Height = 35
    AutoSize = False
    Caption = 'Only include responsbilities:'
    WordWrap = True
  end
  object FromLabel: TLabel [11]
    Left = 0
    Top = 11
    Width = 103
    Height = 13
    Caption = 'Damage Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [12]
    Left = 0
    Top = 85
    Width = 83
    Height = 13
    Caption = 'Due Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [13]
    Left = 0
    Top = 160
    Width = 93
    Height = 13
    Caption = 'Notify Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [14]
    Left = 0
    Top = 300
    Width = 63
    Height = 13
    Caption = 'Claim Status:'
  end
  object Label7: TLabel [15]
    Left = 250
    Top = 62
    Width = 84
    Height = 41
    AutoSize = False
    Caption = 'Include Invoice Codes:'
    WordWrap = True
  end
  object Label6: TLabel [16]
    Left = 492
    Top = 323
    Width = 45
    Height = 13
    Caption = 'Estimate:'
  end
  object Excavator: TEdit [17]
    Left = 339
    Top = 274
    Width = 148
    Height = 21
    TabOrder = 12
  end
  object DamageType: TComboBox [18]
    Left = 100
    Top = 320
    Width = 144
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 17
    Items.Strings = (
      ''
      'Approved'
      'Pending Approval'
      'Incoming'
      'Pending')
  end
  object ProfitCenter: TComboBox [19]
    Left = 100
    Top = 234
    Width = 144
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 3
  end
  object UtilityCoDamaged: TEdit [20]
    Left = 339
    Top = 320
    Width = 148
    Height = 21
    TabOrder = 14
  end
  object WorkToBeDone: TEdit [21]
    Left = 339
    Top = 297
    Width = 148
    Height = 21
    TabOrder = 13
  end
  object Attachments: TComboBox [22]
    Left = 567
    Top = 274
    Width = 148
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 15
  end
  object Invoices: TComboBox [23]
    Left = 567
    Top = 297
    Width = 148
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 16
  end
  object City: TEdit [24]
    Left = 339
    Top = 33
    Width = 89
    Height = 21
    TabOrder = 9
  end
  object Location: TEdit [25]
    Left = 339
    Top = 8
    Width = 169
    Height = 21
    TabOrder = 8
  end
  object State: TComboBox [26]
    Left = 459
    Top = 33
    Width = 49
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 10
  end
  inline DueDate: TOdRangeSelectFrame [27]
    Left = 1
    Top = 99
    Width = 251
    Height = 63
    TabOrder = 1
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
  object UQResp: TCheckBox
    Left = 100
    Top = 259
    Width = 140
    Height = 19
    Caption = 'CompanyShortName'
    TabOrder = 4
  end
  object EXResp: TCheckBox
    Left = 100
    Top = 275
    Width = 84
    Height = 19
    Caption = 'Excavator'
    TabOrder = 5
  end
  object SpecialResp: TCheckBox
    Left = 180
    Top = 275
    Width = 60
    Height = 19
    Caption = 'Special'
    TabOrder = 6
  end
  inline DamageDate: TOdRangeSelectFrame
    Left = 1
    Top = 25
    Width = 251
    Height = 62
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
  inline NotifyDate: TOdRangeSelectFrame
    Left = 1
    Top = 173
    Width = 251
    Height = 61
    TabOrder = 2
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
  object ClaimStatusBox: TComboBox
    Left = 100
    Top = 296
    Width = 144
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 7
    Items.Strings = (
      'All'
      'Open'
      'Closed')
  end
  object InvoiceCodeList: TCheckListBox
    Left = 339
    Top = 59
    Width = 169
    Height = 120
    ItemHeight = 13
    TabOrder = 11
  end
  object SelectAllButton: TButton
    Left = 516
    Top = 59
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 18
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton
    Left = 516
    Top = 91
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 19
    OnClick = ClearAllButtonClick
  end
  object EstimateCombo: TComboBox
    Left = 567
    Top = 320
    Width = 148
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 20
    Items.Strings = (
      'Initial'
      'Current')
  end
  object GroupBoxInvestigator: TGroupBox
    Left = 339
    Top = 179
    Width = 342
    Height = 93
    Caption = 'Investigator/Locator'
    TabOrder = 21
    object Label8: TLabel
      Left = 11
      Top = 13
      Width = 46
      Height = 13
      Caption = 'Manager:'
    end
    object LabelLocInv: TLabel
      Left = 11
      Top = 51
      Width = 180
      Height = 13
      Caption = 'Please select Locator or Investigator:'
    end
    object CheckBoxAll: TCheckBox
      Left = 235
      Top = 31
      Width = 97
      Height = 17
      Caption = 'All Employees'
      TabOrder = 0
      OnClick = CheckBoxAllClick
    end
    object InvestigatorLocatorComboBox: TComboBox
      Left = 10
      Top = 65
      Width = 198
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 1
    end
    object RadioButtonInvestigator: TRadioButton
      Left = 235
      Top = 56
      Width = 80
      Height = 17
      Caption = 'Investigator'
      TabOrder = 2
      OnClick = RadioButtonInvestigatorClick
    end
    object RadioButtonLocator: TRadioButton
      Left = 235
      Top = 72
      Width = 80
      Height = 17
      Caption = 'Locator'
      TabOrder = 3
      OnClick = RadioButtonInvestigatorClick
    end
    object ManagersComboBox: TComboBox
      Left = 10
      Top = 29
      Width = 198
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 4
      OnChange = ManagersComboBoxChange
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 31
    Top = 348
  end
end
