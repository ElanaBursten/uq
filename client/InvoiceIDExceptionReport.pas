unit InvoiceIDExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TInvoiceIDExceptionReportForm = class(TReportBaseForm)
    Label1: TLabel;
    InvoiceDate: TOdRangeSelectFrame;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

{ TInvoiceIDExceptionReportForm }

procedure TInvoiceIDExceptionReportForm.InitReportControls;
begin
  inherited;
  InvoiceDate.SelectDateRange(OdRangeSelect.rsLast3Months);
end;

procedure TInvoiceIDExceptionReportForm.ValidateParams;
begin
  inherited;
  SetReportID('InvoiceIDException');
  SetParamDate('StartDate', InvoiceDate.FromDate);
  SetParamDate('EndDate', InvoiceDate.ToDate);
end;

end.
