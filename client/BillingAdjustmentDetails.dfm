inherited BillingAdjustmentDetailsForm: TBillingAdjustmentDetailsForm
  Left = 861
  Top = 256
  Caption = 'Billing Adjustment Details'
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object CustomerCodeLabel: TLabel
    Left = 8
    Top = 47
    Width = 50
    Height = 13
    Caption = 'Customer:'
  end
  object TypeLabel: TLabel
    Left = 8
    Top = 96
    Width = 28
    Height = 13
    Caption = 'Type:'
  end
  object DescriptionLabel: TLabel
    Left = 8
    Top = 174
    Width = 57
    Height = 13
    Caption = 'Description:'
  end
  object AmountLabel: TLabel
    Left = 8
    Top = 149
    Width = 41
    Height = 13
    Caption = 'Amount:'
  end
  object AdjDateLabel: TLabel
    Left = 8
    Top = 122
    Width = 85
    Height = 13
    Caption = 'Adjustment Date:'
  end
  object AddedDateLabel: TLabel
    Left = 309
    Top = 45
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Added Date:'
  end
  object AddedDateText: TDBText
    Left = 380
    Top = 45
    Width = 76
    Height = 13
    AutoSize = True
    DataField = 'added_date'
    DataSource = AdjustmentSource
  end
  object WhichInvoiceLabel: TLabel
    Left = 8
    Top = 70
    Width = 83
    Height = 16
    Caption = 'Which Invoice:'
    WordWrap = True
  end
  object GLCodeLabel: TLabel
    Left = 8
    Top = 200
    Width = 44
    Height = 13
    Caption = 'GL Code:'
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 0
    Width = 548
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object SaveButton: TButton
      Left = 5
      Top = 6
      Width = 86
      Height = 25
      Action = SaveAction
      TabOrder = 0
    end
    object CancelButton: TButton
      Left = 95
      Top = 6
      Width = 86
      Height = 25
      Action = CancelAction
      Cancel = True
      TabOrder = 1
    end
    object AddButton: TButton
      Left = 185
      Top = 6
      Width = 99
      Height = 25
      Action = NewAdjustmentAction
      Cancel = True
      TabOrder = 2
    end
    object DeleteButton: TButton
      Left = 289
      Top = 6
      Width = 99
      Height = 25
      Action = DeleteAdjustmentAction
      Cancel = True
      TabOrder = 3
    end
  end
  object AdjustmentType: TDBComboBox
    Left = 97
    Top = 92
    Width = 120
    Height = 21
    Style = csDropDownList
    DataField = 'type'
    DataSource = AdjustmentSource
    ItemHeight = 13
    TabOrder = 2
    OnChange = AdjustmentTypeChange
  end
  object AdjustmentAmount: TDBEdit
    Left = 97
    Top = 145
    Width = 120
    Height = 21
    DataField = 'amount'
    DataSource = AdjustmentSource
    TabOrder = 3
  end
  object AdjustmentDescription: TDBEdit
    Left = 97
    Top = 171
    Width = 379
    Height = 21
    DataField = 'description'
    DataSource = AdjustmentSource
    TabOrder = 4
  end
  object WhichInvoice: TDBComboBox
    Left = 97
    Top = 67
    Width = 120
    Height = 21
    Style = csDropDownList
    DataField = 'which_invoice'
    DataSource = AdjustmentSource
    ItemHeight = 13
    TabOrder = 1
  end
  object GLCodeLookup: TcxDBExtLookupComboBox
    Left = 97
    Top = 197
    DataBinding.DataField = 'gl_code'
    DataBinding.DataSource = AdjustmentSource
    Properties.DropDownHeight = 300
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 350
    Properties.View = GLCodesView
    Properties.KeyFieldNames = 'gl_code'
    Properties.ListFieldItem = ColGLLookupGLCode
    TabOrder = 5
    Width = 200
  end
  object CustomerID: TcxDBExtLookupComboBox
    Left = 97
    Top = 43
    DataBinding.DataField = 'customer_name'
    DataBinding.DataSource = AdjustmentSource
    Properties.DropDownHeight = 300
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 450
    Properties.View = CustomerLookupView
    Properties.KeyFieldNames = 'customer_id'
    Properties.ListFieldItem = ColCustLookupCustomerName
    Properties.OnChange = CustomerIDPropertiesChange
    TabOrder = 6
    Width = 200
  end
  object AdjustmentDate: TcxDBDateEdit
    Left = 97
    Top = 118
    DataBinding.DataField = 'adjustment_date'
    DataBinding.DataSource = AdjustmentSource
    Properties.ButtonGlyph.Data = {
      82030000424D820300000000000076000000280000000F0000000D0000000100
      2000000000000C03000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000808080008080800080808000808000008080000080808000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
      0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
      0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
      0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
      8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
      00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
      0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000808080008080800080808000808000008080000080808000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
      0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
      0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080008080000080800000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080800000}
    TabOrder = 7
    Width = 120
  end
  object AdjustmentSource: TDataSource
    DataSet = BillingAdjustment
    Left = 296
    Top = 80
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 492
    Top = 52
    object CancelAction: TAction
      Caption = '&Cancel'
      ShortCut = 27
      OnExecute = CancelActionExecute
    end
    object SaveAction: TAction
      Caption = '&Save'
      ShortCut = 16467
      OnExecute = SaveActionExecute
    end
    object NewAdjustmentAction: TAction
      Caption = '&New Adjustment'
      OnExecute = NewAdjustmentActionExecute
    end
    object DeleteAdjustmentAction: TAction
      Caption = '&Delete Adjustment'
      OnExecute = DeleteAdjustmentActionExecute
    end
  end
  object BillingAdjustment: TDBISAMTable
    BeforeOpen = BillingAdjustmentBeforeOpen
    AfterOpen = BillingAdjustmentAfterOpen
    OnNewRecord = BillingAdjustmentNewRecord
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'billing_adjustment'
    Left = 328
    Top = 80
  end
  object CustomerSource: TDataSource
    DataSet = DM.Customers
    Left = 296
    Top = 110
  end
  object GLCodes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select gl_code, description'
      'from billing_gl'
      'where active')
    Params = <>
    Left = 361
    Top = 80
  end
  object GLSource: TDataSource
    DataSet = GLCodes
    Left = 361
    Top = 110
  end
  object ViewRepo: TcxGridViewRepository
    Left = 336
    Top = 200
    object GLCodesView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = GLSource
      DataController.KeyFieldNames = 'gl_code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ScrollBars = ssVertical
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColGLLookupGLCode: TcxGridDBBandedColumn
        Caption = 'GL Code'
        DataBinding.FieldName = 'gl_code'
        Width = 48
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColGLLookupDescription: TcxGridDBBandedColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
    end
    object CustomerLookupView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = CustomerSource
      DataController.KeyFieldNames = 'customer_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ScrollBars = ssVertical
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColCustLookupCustomerID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'customer_id'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColCustLookupCustomerName: TcxGridDBBandedColumn
        Caption = 'Customer Name'
        DataBinding.FieldName = 'customer_name'
        Width = 81
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ColCustLookupCustomerNumber: TcxGridDBBandedColumn
        Caption = 'Customer #'
        DataBinding.FieldName = 'customer_number'
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ColCustLookupCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Width = 22
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ColCustLookupState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Width = 30
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ColCustLookupPCCode: TcxGridDBBandedColumn
        Caption = 'Profit Ctr'
        DataBinding.FieldName = 'pc_code'
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
    end
  end
end
