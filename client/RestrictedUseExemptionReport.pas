unit RestrictedUseExemptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect, EmployeeSelectFrame;

type
  TRestrictedUseExemptionReportForm = class(TReportBaseForm)
    GrantedDateRange: TOdRangeSelectFrame;
    RevokedDateRange: TOdRangeSelectFrame;
    GrantedDateRangeLabel: TLabel;
    RevokedDateRangeLabel: TLabel;
    IncludeExemptionsForGroup: TRadioGroup;
    SpecificWorkerBox: TEmployeeSelect;
    WorkersUnderManagerBox: TComboBox;
    procedure UpdateVisibility(Sender: TObject);
    procedure InitReportControls; override;
  protected
    procedure ValidateParams; override;
  end;

implementation

uses DMu, EmployeeSearch, QMConst, LocalPermissionsDMu;

{$R *.dfm}

{ TRestrictedUseExemptionForm }

procedure TRestrictedUseExemptionReportForm.InitReportControls;
begin
  inherited;

  SetUpManagerList(WorkersUnderManagerBox);
  GrantedDateRange.SelectDateRange(rsThisWeek);
  RevokedDateRange.SelectDateRange(rsCustom);

  SpecificWorkerBox.Initialize(esAllEmployeesUnderManager, -1, StrToIntDef(PermissionsDM.GetLimitation(ReportID), DM.EmpID));
  AddEmployeeSearchButtonForCombo(WorkersUnderManagerBox, esReportManangers);

  UpdateVisibility(Self);
end;

procedure TRestrictedUseExemptionReportForm.ValidateParams;
begin
  inherited;
  SetReportID('RestrictedUseExemption');
  SetParamDate('GrantedStart', GrantedDateRange.FromDate, True);
  SetParamDate('GrantedEnd', GrantedDateRange.ToDate, True);
  SetParamDate('RevokedStart', RevokedDateRange.FromDate, False);
  SetParamDate('RevokedEnd', RevokedDateRange.ToDate, False);

  SetParamInt('ExemptionsFor', IncludeExemptionsForGroup.ItemIndex);
  if IncludeExemptionsForGroup.ItemIndex = 1 then
    SetParamIntCombo('ExemptionsForID', SpecificWorkerBox.EmployeeCombo);
  if IncludeExemptionsForGroup.ItemIndex = 2 then begin
    RequireComboBox(WorkersUnderManagerBox, 'Please select a manager to include exemptions for');
    SetParamIntCombo('ExemptionsForID', WorkersUnderManagerBox);
  end;
end;

procedure TRestrictedUseExemptionReportForm.UpdateVisibility(Sender: TObject);
begin
  SpecificWorkerBox.Enabled := (IncludeExemptionsForGroup.ItemIndex = 1);
  WorkersUnderManagerBox.Enabled := (IncludeExemptionsForGroup.ItemIndex = 2);
end;

end.

