unit SimpleLinkDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OKCANCL1, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit;

type
  TDlgSimpleLink = class(TOKBottomDlg)
    lblLink: TLabel;
    Label1: TLabel;
    EdtLink: TcxTextEdit;
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    Mylink: string;
  end;

var
  DlgSimpleLink: TDlgSimpleLink;

implementation

{$R *.dfm}

procedure TDlgSimpleLink.FormShow(Sender: TObject);     {QM-1020 EB Fix Mesasge links}
begin
  inherited;
  EdtLink.SetFocus;  
end;

procedure TDlgSimpleLink.OKBtnClick(Sender: TObject);
begin
  inherited;
  MyLink := EdtLink.Text;

end;

end.

