inherited frmMARSBulkStatus: TfrmMARSBulkStatus
  Caption = 'MARS Bulk Status'
  ClientWidth = 962
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 962
    Height = 26
    Align = alTop
    BevelEdges = []
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      962
      26)
    object lblManager: TLabel
      Left = 10
      Top = 4
      Width = 62
      Height = 16
      Caption = 'Manager:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCount: TLabel
      Left = 310
      Top = 4
      Width = 38
      Height = 16
      Caption = 'Count:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object btnRefresh: TButton
      Left = 884
      Top = 1
      Width = 75
      Height = 22
      Anchors = [akTop, akRight]
      Caption = 'Refresh'
      TabOrder = 0
      OnClick = btnRefreshClick
    end
  end
  object StatusGrid: TcxGrid
    Left = 0
    Top = 26
    Width = 962
    Height = 374
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = False
    object StatusGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = StatusGridViewCustomDrawCell
      OnFocusedRecordChanged = StatusGridViewFocusedRecordChanged
      DataController.DataSource = MarsSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      OptionsView.RowSeparatorColor = clGray
      Styles.Selection = SharedDevExStyleData.GridSelectionStyle
      Styles.Header = SharedDevExStyleData.BoldBlueFont
      object CheckBoxCol: TcxGridDBColumn
        DataBinding.FieldName = 'mCheckbox'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DisplayGrayed = 'False'
        Properties.NullStyle = nssUnchecked
        Properties.OnChange = CheckBoxColPropertiesChange
        Visible = False
        VisibleForCustomization = False
        Width = 22
        IsCaptionAssigned = True
      end
      object EmpIDCol: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        Visible = False
        Options.Editing = False
        VisibleForCustomization = False
        Width = 74
      end
      object ShortNameCol: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'short_name'
        Options.Editing = False
        Width = 153
      end
      object EmpNumCol: TcxGridDBColumn
        Caption = 'Emp Number'
        DataBinding.FieldName = 'emp_number'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 81
      end
      object NewStatusCol: TcxGridDBColumn
        Caption = 'New Status'
        DataBinding.FieldName = 'new_status'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'new_status'
        Properties.ListColumns = <
          item
            FieldName = 'new_status'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = DSNewStatus
        HeaderAlignmentHorz = taCenter
        Width = 47
      end
      object InsertDateCol: TcxGridDBColumn
        Caption = 'Insert Date'
        DataBinding.FieldName = 'insert_date'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 145
      end
      object InsertedByIDCol: TcxGridDBColumn
        Caption = 'Inserted By ID'
        DataBinding.FieldName = 'inserted_by'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForCustomization = False
        Width = 53
      end
      object InsertedByNameCol: TcxGridDBColumn
        Caption = 'Inserted By'
        DataBinding.FieldName = 'inserted_by_name'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 141
      end
      object MarsActiveCol: TcxGridDBColumn
        Caption = 'Mars Active'
        DataBinding.FieldName = 'mars_active'
        Visible = False
        Options.Editing = False
        VisibleForCustomization = False
      end
      object ProcessStatusCol: TcxGridDBColumn
        Caption = 'IP'
        DataBinding.FieldName = 'inProgress'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForCustomization = False
        Width = 33
      end
      object ButtonCol: TcxGridDBColumn
        Caption = 'Action'
        DataBinding.ValueType = 'Boolean'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.HideSelection = False
        Properties.ViewStyle = vsButtonsOnly
        OnGetProperties = ButtonColGetProperties
        HeaderAlignmentHorz = taCenter
        Options.ShowEditButtons = isebAlways
        Styles.Content = SharedDevExStyleData.BoldBlueFont
        Width = 119
      end
      object RowMessageCol: TcxGridDBColumn
        Caption = 'Process Message'
        DataBinding.FieldName = 'RowMessage'
        Width = 161
      end
    end
    object StatusGridLevel: TcxGridLevel
      GridView = StatusGridView
    end
  end
  object MarsSource: TDataSource
    Left = 584
    Top = 65534
  end
  object DSNewStatus: TDataSource
    Left = 330
    Top = 55
  end
  object cxEditRepository1: TcxEditRepository
    Left = 260
    Top = 135
    object RepoButtonSubmit: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Action = actionSubmit
          Kind = bkText
        end>
      Properties.HideSelection = False
      Properties.Images = SharedImagesDM.Images
      Properties.ViewStyle = vsButtonsOnly
    end
    object RepoButtonSTOP: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Action = actionStop
          Kind = bkText
        end>
      Properties.HideSelection = False
      Properties.Images = SharedImagesDM.Images
      Properties.ViewStyle = vsButtonsOnly
    end
    object RepoButtonSubmitFreeze: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Caption = 'SUBMIT'
          Default = True
          Enabled = False
          Kind = bkText
        end>
      Properties.ViewStyle = vsButtonsOnly
    end
    object RepoButtonSTOPFreeze: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Caption = 'STOP'
          Default = True
          Enabled = False
          Kind = bkText
        end>
      Properties.ViewStyle = vsButtonsOnly
    end
  end
  object ActionList1: TActionList
    Images = SharedImagesDM.Images
    Left = 320
    Top = 135
    object actionStop: TAction
      Caption = 'STOP'
      ImageIndex = 39
      OnExecute = actionStopExecute
    end
    object actionSubmit: TAction
      Caption = 'SUBMIT'
      ImageIndex = 15
      OnExecute = actionSubmitExecute
    end
  end
end
