unit PayrollExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, SharedDevExStyles, ComCtrls, dxCore, cxDateUtils;

type
  TPayrollExportResult = record
    DoSearch: Boolean;
    ProfitCenter: string;
    FinalApproval: Boolean;
    WeekEnding: TDateTime;
    ExceptionMsg: string;
  end;

  TPayrollExportForm = class(TForm)
    ProfitCenterCombo: TComboBox;
    ExportDate: TcxDateEdit;
    ExportButton: TButton;
    CancelButton: TButton;
    WeekLabel: TLabel;
    ProfitCenter: TLabel;
    NoteLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ExportDateChange(Sender: TObject);
  private
    function SelectedProfitCenter: string;
  protected
    procedure Loaded; override;
  public
    class function Execute: TPayrollExportResult;
  end;

implementation

uses
  OdMiscUtils, DateUtils, DMu, OdHourglass, Terminology;

{$R *.dfm}

{ TPayrollExportForm }

class function TPayrollExportForm.Execute: TPayrollExportResult;
var
  Form: TPayrollExportForm;
  Msg: string;
  Center: string;
  Cursor: IInterface;
begin
  Result.DoSearch := False;
  Form := TPayrollExportForm.Create(nil);
  try
    if (Form.ShowModal = mrOK) then begin
      Center := Form.SelectedProfitCenter;
      Assert(Center <> '');
      Assert(DayOfTheWeek(Form.ExportDate.Date) = DaySaturday);
      Cursor := ShowHourGlass;
      try
        DM.CallingServiceName('ExportTimesheets');
        Msg := DM.Engine.Service.ExportTimesheets(DM.Engine.SecurityInfo, Center, Form.ExportDate.Date);
      except
        on E: Exception do
        begin
          Result.DoSearch := True;

          if StrContains('pending approval', E.Message) then
            Result.FinalApproval := False
          else if StrContains('pending final approval', E.Message) then
            Result.FinalApproval := True
          else
            raise;
          Result.ExceptionMsg := E.Message;
          Result.ProfitCenter := Form.SelectedProfitCenter;
          Result.WeekEnding := Form.ExportDate.Date;
        end;
      end;
      if Msg <> '' then
        ShowMessage(Msg);
    end;
  finally
    FreeAndNil(Form);
  end;
end;

procedure TPayrollExportForm.FormCreate(Sender: TObject);
begin
  ExportDate.Date := SaturdayIze(Now - 6);
  DM.TimesheetProfitCenterList(ProfitCenterCombo.Items);
  if ProfitCenterCombo.Items.Count > 0 then
    ProfitCenterCombo.ItemIndex := 0;
  ExportButton.Enabled := ProfitCenterCombo.Items.Count > 0;
  //We are assigning the glyph manually because the dfm's may produce different
  //binary image data based on the bit settings of the individual developer's monitor.
  SharedDevExStyleData.SetGlyph(ExportDate, 0);
end;

procedure TPayrollExportForm.ExportDateChange(Sender: TObject);
begin
  if DayOfTheWeek(ExportDate.Date) <> daySaturday then
    ExportDate.Date := SaturdayIze(ExportDate.Date);
end;

function TPayrollExportForm.SelectedProfitCenter: string;
begin
  Assert(ProfitCenterCombo.ItemIndex >= 0);
  Result := DM.GetPCFromTimesheetComboText(ProfitCenterCombo.Text);
end;

procedure TPayrollExportForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

end.
