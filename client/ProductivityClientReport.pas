unit ProductivityClientReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TProductivityClientReportForm = class(TReportBaseForm)
    DateRange: TOdRangeSelectFrame;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    Label2: TLabel;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdVclUtils, LocalEmployeeDmu;

{$R *.dfm}

{ TProductivityClientReportForm }

procedure TProductivityClientReportForm.InitReportControls;
begin
  inherited;
  DateRange.SelectDateRange(rsYesterday);
  SetUpManagerList(ManagersComboBox);
  SelectComboBoxItemFromObjects(ManagersComboBox, DM.EmpID);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TProductivityClientReportForm.ValidateParams;
begin
  inherited;
  SetReportID('ProductivityClient');
  SetParamDate('DateFrom', DateRange.FromDate);
  SetParamDate('DateTo', DateRange.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

end.
