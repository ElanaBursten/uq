unit MyAssets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, DBGrids, DB, DBISAMTb, ExtCtrls, Grids;

type
  TMyAssetsForm = class(TEmbeddableForm)
    AssetSource: TDataSource;
    Assets: TDBISAMQuery;
    AssetGrid: TDBGrid;
    NoAssetsPanel: TPanel;
  public
    procedure ActivatingNow; override;
    procedure RefreshNow; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdDBUtils;

{ TMyAssetsForm }

procedure TMyAssetsForm.ActivatingNow;
begin
  inherited;
  RefreshNow;
end;

procedure TMyAssetsForm.RefreshNow;
begin
  inherited;
  Assets.ParamByName('EmpID').AsInteger := DM.EmpID;
  RefreshDataSet(Assets);
  AssetGrid.Visible := not Assets.Eof;
end;

end.
