unit Invoice;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, DB, DBISAMTb, StdCtrls, DBCtrls, Mask, DMu, QMConst, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxButtonEdit;

type
  TInvoiceFrame = class(TFrame)
    InvoiceData: TDBISAMTable;
    InvoiceSource: TDataSource;
    ActionList: TActionList;
    SaveAction: TAction;
    CancelAction: TAction;
    GoToDamageAction: TAction;
    InsertAction: TAction;
    EditInvoicePanel: TPanel;
    InvoiceGroup: TGroupBox;
    AmountLabel: TLabel;
    InvoiceCompanyLabel: TLabel;
    InvoiceNumberLabel: TLabel;
    ReceivedDateLabel: TLabel;
    CommentsLabel: TLabel;
    InvoiceDateLabel: TLabel;
    Amount: TDBEdit;
    InvoiceCompany: TDBEdit;
    InvoiceNumber: TDBEdit;
    ReceivedDate: TcxDBDateEdit;
    Comments: TDBEdit;
    InvoiceDate: TcxDBDateEdit;
    PaymentGroup: TGroupBox;
    ApprovedDateLabel: TLabel;
    AmountApprovedLabel: TLabel;
    PaidDateLabel: TLabel;
    AmountPaidLabel: TLabel;
    SatisfiedLabel: TLabel;
    InvoiceApprovedLabel: TLabel;
    AmountApproved: TDBEdit;
    ApprovedDate: TcxDBDateEdit;
    AmountPaid: TDBEdit;
    PaidDate: TcxDBDateEdit;
    Satisfied: TDBCheckBox;
    InvoiceApproved: TDBCheckBox;
    DamageGroup: TGroupBox;
    DamageIDLabel: TLabel;
    DamageStateLabel: TLabel;
    DamageCityLabel: TLabel;
    DamageDateLabel: TLabel;
    DamageCity: TDBEdit;
    UQDamageID: TcxDBButtonEdit;
    DamageDate: TcxDBDateEdit;
    DamageState: TDBComboBox;
    GoToDamage: TButton;
    PacketGroup: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    PacketRequested: TcxDBDateEdit;
    PacketReceived: TcxDBDateEdit;
    SaveButton: TButton;
    CancelButton: TButton;
    InsertButton: TButton;
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CancelActionExecute(Sender: TObject);
    procedure UQDamageIDButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure InvoiceDataAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure GoToDamageActionExecute(Sender: TObject);
    procedure InvoiceDataAfterInsert(DataSet: TDataSet);
    procedure SatisfiedClick(Sender: TObject);
    procedure SaveActionExecute(Sender: TObject);
    procedure InsertActionExecute(Sender: TObject);
    procedure InvoiceBeforePost(DataSet: TDataSet);
    procedure InvoiceAfterPost(DataSet: TDataSet);
    procedure InvoiceAfterScroll(DataSet: TDataSet);
  private
    FOnFinished: TNotifyEvent;
    FInsertMode: Boolean;
    FOnGoToDamage: TItemSelectedEvent;
    FWasSatisfied: Boolean;
    FEmbeddedInDamage: Boolean;
    FReadOnly: Boolean;
    FPostTimestamp: TDateTime;
    FFrameEnabled: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    procedure SetInsertMode(const Value: Boolean);
    procedure CheckSatisfiedConditions;
    procedure SetupEditing(Edit: Boolean);
    procedure SetEmbeddedInDamage(const Value: Boolean);
    procedure CheckDateMinimums;
    procedure GetDamageData(DamageID: Integer);
    procedure AddInvoiceLookupFields;
    procedure SetFrameEnabled(const Value: Boolean);
  protected
    FDamageID: Integer;
    FOtherID: Integer;
    FInvoiceType: Integer;
    property InsertMode: Boolean read FInsertMode write SetInsertMode;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
    procedure SetWasSatisfied;
    procedure FilterInvoices(InvoiceType: Integer); virtual;
  public
    property OnFinished: TNotifyEvent read FOnFinished write FOnFinished;
    property OnGoToDamage: TItemSelectedEvent read FOnGoToDamage write FOnGoToDamage;
    property EmbeddedInDamage: Boolean read FEmbeddedInDamage write SetEmbeddedInDamage;
    property FrameEnabled: Boolean read FFrameEnabled write SetFrameEnabled;
    procedure AddNewInvoice; virtual;
    procedure GoToDamageInvoice(InvoiceID: Integer); overload;
    procedure GoToDamageInvoice(DataSet: TDataSet; DamageID: Integer); overload;
    procedure GoToDamageInvoice(DataSet: TDataSet; DamageID, ThirdPartyID, LitigationID: Integer); overload;
    procedure Cancel;
    procedure Save;
    function Editing: Boolean;
    function Invoice: TDataSet;
    procedure UpdateActions;

    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses
  OdHourglass, BaseSearchForm, DamageSearchHeader, OdDbUtils, OdExceptions,
  OdVclUtils, OdCxUtils, OdMiscUtils, LocalPermissionsDMu, LocalDamageDMu;

{$R *.dfm}

procedure TInvoiceFrame.AddNewInvoice;
begin
  InsertMode := True;
  if not EmbeddedInDamage then
    InvoiceSource.DataSet := InvoiceData;
  Invoice.Close;
  Invoice.AfterOpen := InvoiceDataAfterOpen;
  Invoice.Open;
  Invoice.Insert;
  Application.ProcessMessages;
  Invoice.FieldByName('satisfied').AsBoolean := False;
  Invoice.FieldByName('invoice_approved').AsBoolean := False;
  SetWasSatisfied;
end;

procedure TInvoiceFrame.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  SaveAction.Enabled := EditingDataSet(Invoice);

  GoToDamageAction.Enabled := Assigned(OnGoToDamage) and (UQDamageID.Text <> '') and (Invoice.State <> dsInsert);
  GoToDamageAction.Visible := not EmbeddedInDamage;

  if EditingDataSet(Invoice) or EmbeddedInDamage then begin
    CancelAction.Caption := 'Cancel';
    CancelAction.Enabled := EditingDataSet(Invoice);
  end else begin
    CancelAction.Caption := 'Done';
  end;
  InsertAction.Visible := EmbeddedInDamage;
  ReadOnly := Invoice.IsEmpty or not PermissionsDM.CanI(RightDamagesSetInvoiceCode) or not FrameEnabled;

  //Rule-based edits enabling
  if FrameEnabled then begin
    InvoiceApproved.Enabled := PermissionsDM.CanI(RightDamagesApproveInvoice);
    if (InvoiceGroup.Enabled <> ((InvoiceApproved.Checked=False) and (ReadOnly=False)) ) then
      SetEnabledOnControlAndChildren(InvoiceGroup, (InvoiceApproved.Checked=False) and (ReadOnly=False));
    AmountApproved.Enabled := (InvoiceApproved.Checked=False) and (ReadOnly=False);
    ApprovedDate.Enabled := (InvoiceApproved.Checked=False) and (ReadOnly=False);
    InsertAction.Enabled := PermissionsDM.CanI(RightDamagesSetInvoiceCode) and (not EditingDataSet(Invoice));
    if InsertButton.Enabled then
      EditInvoicePanel.Enabled := True; //need panel enabled in order for button to be enabled/clickable
    SaveAction.Enabled := SaveAction.Enabled and (ReadOnly= False);
  end;
end;

procedure TInvoiceFrame.SetInsertMode(const Value: Boolean);
begin
  FInsertMode := Value;
end;

procedure TInvoiceFrame.SaveActionExecute(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  PostDataSet(Invoice);
  if InsertMode then begin
    InsertMode := False;
    Invoice.Close;
    if Assigned(FOnFinished) then
      FOnFinished(Self);
  end;
end;

procedure TInvoiceFrame.CancelActionExecute(Sender: TObject);
begin
  if EditingDataSet(Invoice) then
    Invoice.Cancel;
  if Assigned(FOnFinished) then
    FOnFinished(Self);
end;

procedure TInvoiceFrame.UQDamageIDButtonClick(Sender: TObject; AbsoluteIndex: Integer);
var
  SearchForm: TSearchForm;
  DamageID: string;
begin
  SearchForm := TSearchForm.CreateWithCriteria(nil, TDamageSearchCriteria);
  try
    SearchForm.ShowModal;
    if SearchForm.ModalResult = mrOK then begin
      DamageID := SearchForm.GetSelectedFieldValue('damage_id');
      if DamageID <> '' then begin
        Invoice.Edit;
        Invoice.FieldByName('damage_id').AsInteger := StrToInt(DamageID);
        Invoice.FieldByName('uq_damage_id').AsInteger := SearchForm.GetSelectedFieldValue('uq_damage_id');
      end;
    end;
  finally
    FreeAndNil(SearchForm);
  end;
end;

procedure TInvoiceFrame.InvoiceDataAfterOpen(DataSet: TDataSet);
const
  InvoiceRequired: array[0..4] of string =
    ('company', 'invoice_num', 'amount', 'cust_invoice_date', 'received_date');
begin
  OpenDataset(LDamageDM.DamageLookup);
  SetRequiredFields(DataSet, InvoiceRequired);
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('payment_code'), 'paycode');
  DM.Engine.LinkEventsAutoInc(DataSet);
  DataSet.BeforePost := InvoiceBeforePost;
  DataSet.AfterPost := InvoiceAfterPost;
  DataSet.AfterScroll := InvoiceAfterScroll;
end;

procedure TInvoiceFrame.FormShow(Sender: TObject);
begin
  if InsertMode then
    if InvoiceNumber.CanFocus then
      InvoiceNumber.SetFocus;
end;

procedure TInvoiceFrame.GoToDamageActionExecute(Sender: TObject);
begin
  // The damage data is always downloaded at this point (either by being
  // embedded in the damage screen or by calling GoToDamageInvoice)
  if Assigned(OnGoToDamage) then
    OnGoToDamage(Self, Invoice.FieldByName('damage_id').AsInteger);
end;

procedure TInvoiceFrame.InvoiceDataAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('received_date').AsDateTime := Trunc(Now);
end;

// This is only called when showing a stand-alone invoice from the invoice
// search screen.  The invoice may not be attached to a damage.
procedure TInvoiceFrame.GoToDamageInvoice(InvoiceID: Integer);
var
  DamageID: Integer;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  DamageID := LDamageDM.GetDamageIDForInvoiceID(InvoiceID);
  if DamageID > 0 then
    GetDamageData(DamageID);
  AddInvoiceLookupFields;
  InsertMode := False;
  InvoiceSource.DataSet := InvoiceData;
  RefreshDataSet(Invoice);
  if not Invoice.Locate('invoice_id', InvoiceID, []) then
    raise Exception.Create('Unable to find invoice id: ' + IntToStr(InvoiceID));
  SetWasSatisfied;
end;

procedure TInvoiceFrame.GoToDamageInvoice(DataSet: TDataSet; DamageID: Integer);
begin
  Assert(Assigned(DataSet));
  InvoiceSource.DataSet := DataSet;
  FDamageID := DamageID;
  InsertMode := False;
  InvoiceData.Close;
  InvoiceDataAfterOpen(DataSet);
  SetWasSatisfied;
  FilterInvoices(qminvDamage);
end;

procedure TInvoiceFrame.GoToDamageInvoice(DataSet: TDataSet; DamageID,
  ThirdPartyID, LitigationID: Integer);
begin
  Assert(Assigned(DataSet));
  InvoiceSource.DataSet := DataSet;
  FDamageID := DamageID;
  InsertMode := False;
  InvoiceData.Close;
  InvoiceDataAfterOpen(DataSet);
  SetWasSatisfied;
  if ThirdPartyID > 0 then
    FilterInvoices(qminv3rdParty)
  else if LitigationID > 0 then
    FilterInvoices(qminvLitigation)
  else
    FilterInvoices(qminvDamage);
end;

procedure TInvoiceFrame.CheckSatisfiedConditions;
const
  EstQuery = 'The most recent estimate for this damage is %f.'+#13#10+
             'Would you like to add a new estimate for the paid amount of '+#13+#10+'%f?';
var
  PaidAmount: Double;
  LatestEstimate: Double;
  DamageId: Integer;
  Comment: string;
begin
  // TODO: Does this need third_party_id & litigation_id (potentially null) values?
  DamageID := Invoice.FieldByName('damage_id').AsInteger;
  if DamageID < 1 then
    raise EOdEntryRequired.Create('An invoice can not be satisfied without a valid damage ID');
  PaidAmount := Invoice.FieldByName('paid_amount').AsFloat;
  if PaidAmount < 0.01 then
    raise EOdEntryRequired.Create('An invoice can not be satisfied without a paid amount');
  if not EmbeddedInDamage then
    LDamageDM.UpdateDamageInCache(DamageID);
  LatestEstimate := LDamageDM.GetCurrentDamageEstimateAmount(DamageID);
  if (Abs(LatestEstimate - PaidAmount) > 0.00999) then begin
    if MessageDlg(Format(EstQuery, [LatestEstimate, PaidAmount]), mtConfirmation, [mbYes, mbCancel], 0) <> mrYes then
      Abort
    else begin
      Comment := Invoice.FieldByName('comment').AsString;
      if IsEmpty(Comment) then
        Comment  := 'Automatic Estimate';
      LDamageDM.AddNewEstimateForDamage(DamageID, PaidAmount,
        Invoice.FieldByName('company').AsString, Comment);
    end;
  end;
end;

procedure TInvoiceFrame.SatisfiedClick(Sender: TObject);
begin
  with (Sender as TDBCheckBox) do
    if State = cbGrayed then
      State := cbUnchecked;
end;

procedure TInvoiceFrame.Cancel;
begin
  CancelAction.Execute;
end;

procedure TInvoiceFrame.Save;
begin
  SaveAction.Execute;
end;

constructor TInvoiceFrame.Create(AOwner: TComponent);
begin
  inherited;
  DM.GetRefDisplayList('state', DamageState.Items);
end;

procedure TInvoiceFrame.SetupEditing(Edit: Boolean);
begin
  SetEnabledOnControlAndChildren(InvoiceGroup, Edit);
  SetEnabledOnControlAndChildren(PaymentGroup, Edit);
  SetEnabledOnControlAndChildren(DamageGroup, Edit);
  SetEnabledOnControlAndChildren(PacketGroup, Edit);
end;

procedure TInvoiceFrame.SetEmbeddedInDamage(const Value: Boolean);
begin
  FEmbeddedInDamage := Value;
end;

procedure TInvoiceFrame.SetReadOnly(const Value: Boolean);
{ Used internally by ActionListUpdate to determine control enabling; is
  subordinate to Frame enabling.}
begin
  if (Value <> FReadOnly) then begin
    FReadOnly := Value;
    SetupEditing(not FReadOnly);
  end;
end;

procedure TInvoiceFrame.SetFrameEnabled(const Value: Boolean);
{ Descendants may use this property to disable everything on the EditInvoicePanel
  when needed. }
begin
  if (not Self.Parent.Enabled) then
    FFrameEnabled := False
  else
    FFrameEnabled := Value;
  SetEnabledOnControlAndChildren(EditInvoicePanel, FFrameEnabled);
  SetReadOnly(not FFrameEnabled);
  //Button actions need to reflect current Frame enabled state
  SaveAction.Enabled := SaveButton.Enabled;
  CancelAction.Enabled := CancelButton.Enabled;
  InsertAction.Enabled := InsertButton.Enabled;
end;

procedure TInvoiceFrame.InsertActionExecute(Sender: TObject);
begin
  AddNewInvoice;
  Invoice.FieldByName('invoice_type').AsInteger := FInvoiceType;
  if EmbeddedInDamage and (FDamageID > 0) then
    Invoice.FieldByName('damage_id').AsInteger := FDamageID;
  if EmbeddedInDamage and (FOtherID > 0) then
    if FInvoiceType = qminv3rdParty then
      Invoice.FieldByName('third_party_id').AsInteger := FOtherID
    else if FInvoiceType = qminvLitigation then
      Invoice.FieldByName('litigation_id').AsInteger := FOtherID;
end;

function TInvoiceFrame.Invoice: TDataSet;
begin
  Result := InvoiceSource.DataSet;
end;

function TInvoiceFrame.Editing: Boolean;
begin
  Result := EditingDataSet(Invoice);
end;

procedure TInvoiceFrame.InvoiceBeforePost(DataSet: TDataSet);
begin
  CheckDateMinimums;
  FPostTimestamp := Now;
  Invoice.FieldByName('LocalKey').AsDateTime := FPostTimestamp;
  if (not FWasSatisfied) and (Invoice.FieldByName('satisfied').AsBoolean) then begin
    CheckSatisfiedConditions;
    Invoice.FieldByName('satisfied_by_emp_id').AsInteger := DM.UQState.EmpID;
    Invoice.FieldByName('satisfied_date').AsDateTime := Now;
  end;
  DM.Engine.BeforePost(DataSet);
end;

procedure TInvoiceFrame.InvoiceAfterPost(DataSet: TDataSet);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  FWasSatisfied := Invoice.FieldByName('satisfied').AsBoolean;
  DM.SendChangesToServer;
  Invoice.Refresh;
end;

procedure TInvoiceFrame.SetWasSatisfied;
begin
  if Invoice.Eof then
    FWasSatisfied := False
  else
    FWasSatisfied := Invoice.FieldByName('satisfied').AsBoolean;
end;

procedure TInvoiceFrame.InvoiceAfterScroll(DataSet: TDataSet);
begin
  SetWasSatisfied;
end;

procedure TInvoiceFrame.UpdateActions;
var
  Dummy: Boolean;
begin
  ActionListUpdate(nil, Dummy);
end;

procedure TInvoiceFrame.FilterInvoices(InvoiceType: Integer);
begin
  if InvoiceType > qminvAll then
    Invoice.Filter := 'invoice_type = ' + IntToStr(InvoiceType)
  else
    Invoice.Filter := '';
  Invoice.Filtered := True;
  Invoice.Open;
end;

procedure TInvoiceFrame.CheckDateMinimums;
begin
  VerifyMinimumSQLServerDate(Invoice.FieldByName('cust_invoice_date'));
  VerifyMinimumSQLServerDate(Invoice.FieldByName('received_date'));
  VerifyMinimumSQLServerDate(Invoice.FieldByName('approved_date'));
  VerifyMinimumSQLServerDate(Invoice.FieldByName('paid_date'));
  VerifyMinimumSQLServerDate(Invoice.FieldByName('packet_request_date'));
  VerifyMinimumSQLServerDate(Invoice.FieldByName('packet_recv_date'));
end;

procedure TInvoiceFrame.AddInvoiceLookupFields;
begin
  AddLookupField('uq_damage_id', Invoice, 'damage_id', LDamageDM.DamageLookup, 'damage_id', 'uq_damage_id');
  AddLookupField('invoice_type_desc', Invoice, 'invoice_type', DM.EstimateTypeLookup,
    'code', 'description', 10, 'InvoiceTypeLookup', 'InvoiceType');
end;

procedure TInvoiceFrame.GetDamageData(DamageID: Integer);
begin
  LDamageDM.UpdateDamageInCache(DamageID);
end;

end.
