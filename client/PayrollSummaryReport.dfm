inherited PayrollSummaryReportForm: TPayrollSummaryReportForm
  Caption = 'PayrollSummaryReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object PeriodLabel: TLabel [0]
    Left = 0
    Top = 11
    Width = 78
    Height = 18
    AutoSize = False
    Caption = 'Week Ending:'
    WordWrap = True
  end
  object ManagerLabel: TLabel [1]
    Left = 0
    Top = 43
    Width = 99
    Height = 13
    Caption = 'Payroll Timesheet #:'
  end
  object PCCombo: TComboBox [2]
    Left = 106
    Top = 40
    Width = 150
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object ChooseDate: TcxDateEdit [3]
    Left = 106
    Top = 8
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = ChooseDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 150
  end
end
