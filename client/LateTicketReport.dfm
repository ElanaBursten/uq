inherited LateTicketReportForm: TLateTicketReportForm
  Left = 447
  Top = 383
  Caption = 'LateTicketReportForm'
  ClientHeight = 309
  ClientWidth = 375
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 83
    Height = 13
    Caption = 'Due Date Range:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 87
    Width = 46
    Height = 13
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'Manager:'
  end
  object Label6: TLabel [2]
    Left = 0
    Top = 116
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object Label4: TLabel [3]
    Left = 0
    Top = 170
    Width = 65
    Height = 20
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  inline RangeSelect: TOdRangeSelectFrame [4]
    Left = 47
    Top = 21
    Width = 267
    Height = 60
    TabOrder = 0
    inherited ToLabel: TLabel
      Left = 148
    end
    inherited DatesComboBox: TComboBox
      Width = 216
    end
    inherited FromDateEdit: TcxDateEdit
      Width = 89
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 170
      Width = 90
    end
  end
  object ManagersComboBox: TComboBox
    Left = 91
    Top = 84
    Width = 198
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox
    Left = 91
    Top = 113
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object CallCenterList: TCheckListBox
    Left = 91
    Top = 165
    Width = 275
    Height = 141
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
  end
  object ArrivalModeCheckBox: TCheckBox
    Left = 91
    Top = 142
    Width = 217
    Height = 17
    Caption = 'Use Arrival Date to determine lateness'
    TabOrder = 4
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 336
    Top = 240
  end
end
