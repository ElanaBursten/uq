unit LocatePlats;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TLocatePlatForm = class(TForm)
    PlatEntryPanel: TPanel;
    LocatePlatLabel: TLabel;
    PlatsMemo: TMemo;
    HowToLabel: TLabel;
    OKButton: TButton;
    CancelButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FLocateID: Integer;
    FOriginalPlats: TStringList;
    procedure ShowLocatePlatsAsMemo;
    procedure SaveOriginalPlats;
    procedure SetClient(const Value: string);
  protected
    procedure Loaded; override;
  public
    property LocateID: Integer read FLocateID write FLocateID;
    property Client: string write SetClient;
    function SaveLocatePlats(aLocateID: Integer): Boolean;
  end;

function EditLocatePlats(const aLocateID: Integer; const aClient: string): Boolean;

implementation

uses DMu, Terminology;

{$R *.dfm}

function TLocatePlatForm.SaveLocatePlats(aLocateID: Integer): Boolean;
var
  index: Integer;
  plat: string;
  platId: Integer;
  FCurrentPlats: TStringList;
begin
  Result := False;
  if SameText(FOriginalPlats.Text, PlatsMemo.Text) then
    Exit;

  //remove duplicate strings from the memo to prevent duplicate locate_plat recs
  FCurrentPlats := TStringList.create;
  try
    FCurrentPlats.Sorted := True;
    FCurrentPlats.Duplicates := dupIgnore;
    FCurrentPlats.Assign(PlatsMemo.Lines); //strip duplicates
    PlatsMemo.Lines := FCurrentPlats;
  finally
    FCurrentPlats.Free;
  end;

  with DM.LocatePlatData do begin
    Open;
    DisableControls;
    Filter := 'active and locate_id=' + IntToStr(aLocateID);
    Filtered := True;
    try
      // inactivate all plats that were removed
      for index := 0 to FOriginalPlats.Count - 1 do begin
        if PlatsMemo.Lines.IndexOf(FOriginalPlats.strings[index]) < 0 then begin
          platId := Integer(FOriginalPlats.Objects[index]);
          if Locate('locate_plat_id', platId, []) then begin
            Edit;
            FieldByName('active').Value := False;
            FieldByName('modified_by').AsInteger := DM.UQState.EmpID;
            FieldByName('modified_date').AsDateTime := Now;
            Post;
          end;
        end;
      end;

      // add the specified ones not already there
      for index := 0 to PlatsMemo.Lines.Count - 1 do begin
        plat := Trim(PlatsMemo.Lines[index]);
        if (plat <> '') and (FOriginalPlats.IndexOf(plat) < 0) then begin
          Append;
          FieldByName('locate_id').AsInteger := aLocateID;
          FieldByName('plat').AsString := plat;
          FieldByName('added_by').AsInteger := DM.UQState.EmpID;
          FieldByName('insert_date').AsDateTime := Now;
          Post;
          result := True;
        end;
      end;
    finally
      Filtered := False;
      Filter := '';
      EnableControls;
    end;
  end;
end;

procedure TLocatePlatForm.FormShow(Sender: TObject);
begin
  SaveOriginalPlats;
  ShowLocatePlatsAsMemo;
end;

procedure TLocatePlatForm.SetClient(const Value: string);
begin
  LocatePlatLabel.Caption := LocatePlatLabel.Caption + ' ' + Value;
end;

procedure TLocatePlatForm.ShowLocatePlatsAsMemo;
begin
  PlatsMemo.Clear;
  PlatsMemo.Lines.AddStrings(FOriginalPlats);
end;

procedure TLocatePlatForm.FormCreate(Sender: TObject);
begin
  FOriginalPlats := TStringList.Create;
end;

procedure TLocatePlatForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FOriginalPlats);
end;

function EditLocatePlats(const aLocateID: Integer; const aClient: string): Boolean;
begin
  Result := False;
  with TLocatePlatForm.Create(nil) do
    try
      LocateID := aLocateID;
      Client := aClient;
      if ShowModal = mrOK then
        Result := SaveLocatePlats(LocateID);
    finally
      Free;
    end;
end;

procedure TLocatePlatForm.SaveOriginalPlats;
begin
  Assert(Assigned(FOriginalPlats));
  DM.GetLocatePlatList(LocateID, FOriginalPlats);
end;

procedure TLocatePlatForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '') then
    AppTerminology.ReplaceVCL(Self);
end;

end.
