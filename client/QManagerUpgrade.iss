[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager
AppVerName=UtiliQuest Q Manager
AppID=UtiliQuestQManager
Compression=bzip/9
DefaultDirName={pf}\UtiliQuest\Q Manager
DefaultGroupName=UtiliQuest
AppPublisher=UtiliQuest, Inc.
AppPublisherURL=http://www.utiliquest.com/
AppVersion=0.7.19.5
AppMutex=UtiliQuestQManager
DisableProgramGroupPage=yes
OutputBaseFilename=QManagerUpgrade-0.7.19.5b
AdminPrivilegesRequired=no
;WizardDebug=yes

[Files]
Source: "QManager.exe";        DestDir: "{app}"; CopyMode: alwaysoverwrite
Source: "DefaultData\*.*";     DestDir: "{app}\Data"; CopyMode: alwaysoverwrite

[Icons]
Name: "{group}\Q Manager"; Filename: "{app}\QManager.exe"

;[Registry]
;Root: HKCU; Subkey: "Software\UtiliQuest\Data"; ValueType: STRING; ValueName: "TestString"; ValueData: "string"; Flags: uninsdeletevalue

[Dirs]
Name: "{app}\Data"

[UninstallDelete]
Type: files; Name: "{app}\Data\*.*"


