unit SearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, DB, DMu, OdDBISAMUtils, Buttons, DBISAMTb,
  cxGridTableView, cxGridDBTableView, cxTextEdit, QMServerLibrary_Intf;

type
  TSearchCriteria = class(TEmbeddableForm)
    SearchButton: TButton;
    CancelButton: TButton;
    CopyButton: TButton;
    ExportButton: TButton;
    RecordsLabel: TLabel;
    PrintButton: TButton;
    ClearButton: TButton;
  protected
    SavedComboChoices: TStringList;
    function CreateNVPairListFromCriteria: NVPairList;
    procedure SaveCriteria;
    procedure RestoreCriteria;
  public
    Defs: TColumnList;
    procedure BeforeItemSelected(ID: Variant); virtual;
    function GetXmlString: string; virtual; abstract;
    procedure DefineColumns; virtual;
    procedure Showing; virtual;
    procedure GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow); virtual;
    procedure CustomizeDataSet(DataSet: TDBISAMTable; Event: TCustomizeEvent); virtual;
    procedure BeforeItemPrint(ID: Variant); virtual;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); virtual;
    procedure InitializeSearchCriteria; virtual;
    procedure UpdateRecordCount(Num, MaxNum: Integer); virtual;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

  TSearchCriteriaClass = class of TSearchCriteria;

implementation

uses
  OdRangeSelect, OdMiscUtils, OdIsoDates, OdVclUtils;

{$R *.dfm}

{ TSearchCriteria }

procedure TSearchCriteria.BeforeItemSelected(ID: Variant);
begin
  // Override to download data to show the selected item, etc.
end;

// Create params automatically from all the compatible components on the
// form specified by a column info record.  The param value will get
// "guessed" for you (hopefully correctly).  Tweak this to add new component
// types or recognize new component use cases.
function TSearchCriteria.CreateNVPairListFromCriteria: NVPairList;

  procedure AddPair(const Name, Value: string);
  var
    Pair: NVPair;
  begin
    Pair := Result.Add;
    Pair.Name := Name;
    Pair.Value := Value;
  end;

var
  i: Integer;
  Control: TControl;
  ParamName: string;
  Edit: TCustomEdit;
  Combo: TComboBox;
  Range: TOdRangeSelectFrame;
  ColInfo: TColumnInfo;
begin
  Result := NVPairList.Create;
  for i := 0 to Defs.ColumnCount - 1 do begin
    Defs.GetColumnInfo(ColInfo, i);
    if Assigned(ColInfo.Control) then begin
      Control := ColInfo.Control;
      ParamName := ColInfo.ParamName;
      if Control is TEdit then begin
        Edit := Control as TCustomEdit;
        if Trim(Edit.Text) <> '' then
          AddPair(ParamName, Edit.Text);
      end
      else if Control is TOdRangeSelectFrame then begin
        Range := Control as TOdRangeSelectFrame;
        if not Range.IsEmpty then begin
          AddPair(ParamName + '_start', IsoDateTimeToStr(Range.FromDate));
          AddPair(ParamName + '_end',   IsoDateTimeToStr(Range.ToDate));
        end;
      end
      else if Control is TComboBox then begin
        Combo := Control as TComboBox;
        if Trim(Combo.Text) <> '' then begin
          if Combo.Style = csDropdown then
            AddPair(ParamName, Combo.Text)
          else if Combo.Style = csDropdownList then begin
            if Combo.ItemIndex >= 0 then begin
              if AllObjectsAreAssigned(Combo.Items, 1) then
                AddPair(ParamName, IntToStr(GetComboObjectInteger(Combo)))
              else
                AddPair(ParamName, Combo.Text);
              // TODO: Add reference type translation here...
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TSearchCriteria.CustomizeDataSet(DataSet: TDBISAMTable; Event: TCustomizeEvent);
begin
  // Override this to create lookup/calculated/new fields, assign
  // advanced field properties, or change field values
end;

procedure TSearchCriteria.DefineColumns;
begin
  Defs := TColumnList.Create;
end;

procedure TSearchCriteria.GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow);
begin
  // React to the selected grid node changing here
end;

procedure TSearchCriteria.Showing;
begin
  // Called right before the search form is shown
  // HERE, NOT the Create, is the place to setup dynamic controls such as
  // comboboxes.
end;

procedure TSearchCriteria.UpdateRecordCount(Num, MaxNum: Integer);
begin
  {Default Record Count}
  if (MaxNum > -1) and (Num > -1) then begin
      RecordsLabel.Visible := True;
      RecordsLabel.Caption := Format('%d found (%d max)', [Num, MaxNum]);
  end;
  Self.Refresh;
end;

procedure TSearchCriteria.BeforeItemPrint(ID: Variant);
begin
  // override to do any pre-print processing
end;

procedure TSearchCriteria.CustomizeGrid(GridView: TcxGridDBTableView);
begin
  // override to do any Grid customization
end;

procedure TSearchCriteria.InitializeSearchCriteria;
var
  I: Integer;
begin
  for I:= 0 to ControlCount -1 do begin
    if (Controls[I] is TEdit) then
      TEdit(Controls[I]).Text := '';
    if (Controls[I] is TcxCustomTextEdit) then
      TcxCustomTextEdit(Controls[I]).Text := '';
    if (Controls[I] is TComboBox) then begin
      TComboBox(Controls[I]).ItemIndex := -1;
      TComboBOx(Controls[I]).Text := '';
    end;
    if (Controls[I] is TOdRangeSelectFrame) then
      TOdRangeSelectFrame(Controls[I]).NoDateRange;
    if (Controls[I] is TCheckBox) then
      TCheckBox(Controls[I]).Checked := False;
    { Initialize any other controls that are added to search forms here }
  end;
end;

constructor TSearchCriteria.Create(Owner: TComponent);
begin
  inherited;
  SavedComboChoices := TStringList.Create;

  //Need to populate the controls before we can initialize them
  Showing;
  InitializeSearchCriteria;
end;

destructor TSearchCriteria.Destroy;
begin
  FreeAndNil(SavedComboChoices);
  inherited;
end;

procedure TSearchCriteria.SaveCriteria;
var
  I: Integer;
begin
  // Populate SavedSearch pair list with all user settings
  // note that we ONLY need to handle things that get
  // repopulated (comboboxes)

  SavedComboChoices.Clear;
  //Saves the choices in a name/value pair TStringList
  for I:= 0 to ControlCount -1 do begin
    if (Controls[I] is TComboBox)then
      SavedComboChoices.Values[Controls[I].Name] := TComboBox(Controls[I]).Text;
  end;
end;

procedure TSearchCriteria.RestoreCriteria;
var
  I: Integer;
  CurrentIndex: Integer;
  CurrentValue: String;
begin
  // restore control settings (selected values) from SavedSearch
  for I:=0 to ControlCount -1 do begin
    if(Controls[I] is TComboBox) then begin
      CurrentValue := SavedComboChoices.Values[TComboBox(Controls[I]).Name];
      //if the option is no longer available the control is set to -1
      CurrentIndex := TComboBox(Controls[I]).Items.IndexOf(CurrentValue);
      TComboBox(Controls[I]).ItemIndex := CurrentIndex;

      //indexof doesn't handle nil string comparisons
      if (CurrentValue = '') and (TComboBox(Controls[I]).Items[0] = '')then
        TComboBox(Controls[I]).ItemIndex := 0;
    end;
  end;
end;

end.


