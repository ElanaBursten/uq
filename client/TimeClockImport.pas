unit TimeClockImport;

interface

uses TimeClockEntry, DB, Classes;
{$WARN SYMBOL_PLATFORM OFF}


function ImportTimeClock(WorkDate: TDateTime; TimeClockTranslator: TTimeClockTranslator; DayEditable: Boolean): Boolean;
function GetTimeImportFolderName: string;
procedure CleanTimeImportFolders(const DaysOld: Integer);
procedure SetupTimeImportFolders;
//This function used for client-side Tempo HTTP API:
function SaveHTTPPostedTimeImportFile(const PostedFileStream: TStream; var SavedFileName: string): Boolean;
function GetTimeImportStatus(const FileName: string): string;

implementation

uses SysUtils, Dialogs, JclFileUtils, OdMiscUtils,
  DMu, QMConst, TimeFileMonitor, MSXML2_TLB, OdMSXMLUtils, ActiveX, OdIsoDates, OdLog,
  ApplicationFiles, LocalPermissionsDMu, LocalEmployeeDMu;

procedure SetupTimeImportFolders;
var
  TimeImportFolder: string;
  FailedImportFolder: string;
  ProcessedImportFolder: string;
begin
  TimeImportFolder := GetTimeImportFolderName;
  if not ForceDirectories(TimeImportFolder) then
    raise Exception.CreateFmt('Unable to create time import folder %s.', [TimeImportFolder]);

  ProcessedImportFolder := IncludeTrailingPathDelimiter(TimeImportFolder + 'Processed');
  if not ForceDirectories(ProcessedImportFolder) then
    raise Exception.CreateFmt('Unable to create time import processed folder %s.',
      [ProcessedImportFolder]);

  FailedImportFolder := IncludeTrailingPathDelimiter(TimeImportFolder + 'Failed');
  if not ForceDirectories(FailedImportFolder) then
    raise Exception.CreateFmt('Unable to create time import failed folder %s.',
      [FailedImportFolder]);
end;

function ImportTimeClock(WorkDate: TDateTime;
  TimeClockTranslator: TTimeClockTranslator; DayEditable: Boolean): Boolean;
const
  MsgCouldNotImport = 'The time files listed below could not be imported and were ' +
          'moved to the Time Import\Failed folder: ';
var
  TimeImportFolder: string;
  FailedImportFolder: string;
  ProcessedImportFolder: string;
  FileList: TStringList;
  ErrorList: TStringList;
  i: Integer;
  FileName: string;
  MovedOk: Boolean;
  FilePattern: string;
  LoggedInEmpNumber: string;
begin
  Result := False;

  if not PermissionsDM.CanI(RightTimesheetsClockImport) then
    Exit;

  Assert(Assigned(TimeClockTranslator), 'ClockDataTranslator is unassigned in ImportTimeClock');

  TimeImportFolder := GetTimeImportFolderName;
  ProcessedImportFolder := IncludeTrailingPathDelimiter(TimeImportFolder + 'Processed');
  FailedImportFolder := IncludeTrailingPathDelimiter(TimeImportFolder + 'Failed');

  LoggedInEmpNumber := EmployeeDM.GetUserEmployeeNumber;
  ErrorList := nil;
  FileList := TStringList.Create;
  try
    ErrorList := TStringList.Create;
    FilePattern := TimeImportFolder + FormatDateTime('yyyy-mm-dd_', WorkDate) + '*.xml';
    AdvBuildFileList(FilePattern, faArchive, FileList, amSubSetOf);
    // Assumes the files are named so they are processed in chronological order
    for i := 0 to FileList.Count-1 do begin
      try
        FileName := FileList[i];
        if IsFileInUse(TimeImportFolder + FileName) then begin//file is still being written by Tempo; skip it
          Continue;
        end;
        if not DayEditable then
          raise Exception.Create('Cannot import time because the day is no longer editable.');
        TimeClockTranslator.AddTimeFromXMLFile(TimeImportFolder + FileName, LoggedInEmpNumber);
        MovedOk := FileMove(TimeImportFolder + FileName, ProcessedImportFolder + FileName, True);
        if not MovedOk then
          ErrorList.Add(FileName + ' - could not be moved to ' + ProcessedImportFolder);
        Result := True; // at least one file was imported successfully
        TimeFileMonitorDM.TimeFilesWereImported := True;
      except
        on E: Exception do begin
          MovedOk := FileMove(TimeImportFolder + FileName, FailedImportFolder + FileName, True);
          if MovedOk and (not FileExists(TimeImportFolder + FileName)) then
            ErrorList.Add(FileName + ' - ' + E.Message)
          else
            ErrorList.Add(FileName + ' - [not moved] ' + E.Message);
        end;
      end;
    end;

    if ErrorList.Count > 0 then begin
      DM.AddToQMLog(MsgCouldNotImport + ErrorList.Text);
      DM.ShowTransientMessage(MsgCouldNotImport + ErrorList.Text)
    end
    else
      if FileList.Count > 0 then
        EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTimeEntryImport, 'Time Entry Import', Now);
  finally
    FreeAndNil(FileList);
    FreeAndNil(ErrorList);
  end;
end;

function GetTimeImportFolderName: string;
begin
 {QMANTWO-624 AppLocal - This folder has been moved under the specific server name (in the event we add another server)}
  Result := IncludeTrailingPathDelimiter((GetClientServerSpecificLocalDir) + '\Time Import');
end;


procedure CleanTimeImportFolders(const DaysOld: Integer);
var
  Path: string;
begin
  try
    Path := IncludeTrailingPathDelimiter(GetTimeImportFolderName) + 'Processed';
    OdDeleteFile(Path, '*.*', DaysOld, False, False);
  except on E: Exception do
    MessageDlg('Unable to clean out processed time import folder: ' + E.Message, mtError, [mbOK], 0);
  end;

  try
    Path := IncludeTrailingPathDelimiter(GetTimeImportFolderName) + 'Failed';
    OdDeleteFile(Path, '*.*', DaysOld, False, False);
  except on E: Exception do
    MessageDlg('Unable to clean out failed time import folder: ' + E.Message, mtError, [mbOK], 0);
  end;
end;

function SaveHTTPPostedTimeImportFile(const PostedFileStream: TStream; var SavedFileName: string): Boolean;
var
  TimeImportFolder: string;
  TimeImportFileXML: IXMLDOMDocument;
  ClockEntry: IXMLDOMNode;
  ActivityTime: TDateTime;
  ActivityDateTimeStr: string;
  TimeImportFileName: string;
begin
  Result := False;
  SavedFileName := '';
  ActivityDateTimeStr := '';
  TimeImportFolder := GetTimeImportFolderName;
  CoInitialize(nil);
  try
    //Load the XML file that is received via HTTP post into an IXMLDOMDocument
    TimeImportFileXML := CoDOMDocument.Create;
    if TimeImportFileXML.load(StreamToOleVariant(PostedFileStream)) then begin
      //Now read the time entry so we can build the file name
      ClockEntry := TimeImportFileXML.selectSingleNode('/QManager/ClockEntry');
      GetAttributeTextFromNode(ClockEntry, 'Time', ActivityDateTimeStr);
      ActivityTime := IsoStrToDateTime(Copy(ActivityDateTimeStr, 1, 23));
      TimeImportFileName := FormatDateTime('yyyy-mm-dd_hh-mm-ss',ActivityTime) + '.xml';
      //Save the file into the TimeImportFolder for processing as usual
      TimeImportFileXML.save(TimeImportFolder + TimeImportFileName);
      SavedFileName := TimeImportFileName;
      Result := FileExists(TimeImportFolder + TimeImportFileName);
    end;
  finally
    CoUninitialize;
  end;
end;

{QMANTWO-624 AppLocal - This folder has been moved to the server name (in the event we ever add more servers)}
function GetTimeImportStatus(const FileName: string): string;
  function GetErrorMessage(TimeImportFileName: string): string;
  var
    i: Integer;
    LogContents: TStringList;
  begin
    LogContents := TStringList.Create;
    try
      LogContents.LoadFromFile(OdLog.GetLog.FileName);
      for i := LogContents.Count - 1 downto 0 do  //read file backwards to get most recent error
        if Pos(TimeImportFileName, LogContents[i]) <> 0 then begin
          Result := LogContents[i];
          Exit;
        end;
    finally
      FreeAndNil(LogContents);
    end;
  end;
var
  TimeImportFolder, TimeImportFileName: string;
begin
  TimeImportFolder := IncludeTrailingPathDelimiter(GetTimeImportFolderName);
  TimeImportFileName := StringReplace(FileName, '%20', ' ', [rfReplaceAll]) + '.xml';
  if FileExists(TimeImportFolder + TimeImportFileName) then
    Result := 'Pending'
  else if FileExists(TimeImportFolder + 'Processed\' + TimeImportFileName) then
    Result := 'Success'
  else if FileExists(TimeImportFolder + 'Failed\' + TimeImportFileName) then
    Result := 'Error - ' + GetErrorMessage(TimeImportFileName)
  else
    Result := '404 - Not Found';
end;


end.
