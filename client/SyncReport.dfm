inherited SyncReportForm: TSyncReportForm
  Left = 341
  Top = 195
  Caption = 'SyncReportForm'
  ClientHeight = 188
  ClientWidth = 349
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox [0]
    Left = 0
    Top = 8
    Width = 336
    Height = 84
    Caption = ' Date Range '
    TabOrder = 0
    inline RangeSelect: TOdRangeSelectFrame
      Left = 2
      Top = 14
      Width = 327
      Height = 63
      TabOrder = 0
      inherited FromLabel: TLabel
        Left = 22
      end
      inherited ToLabel: TLabel
        Left = 205
      end
      inherited DatesLabel: TLabel
        Left = 22
      end
      inherited DatesComboBox: TComboBox
        Left = 115
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 114
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 226
      end
    end
  end
  object GroupBox2: TGroupBox [1]
    Left = 0
    Top = 96
    Width = 336
    Height = 84
    Caption = ' Office '
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 22
      Width = 46
      Height = 13
      Caption = 'Manager:'
    end
    object Label6: TLabel
      Left = 24
      Top = 49
      Width = 84
      Height = 13
      Caption = 'Employee Status:'
    end
    object ManagersComboBox: TComboBox
      Left = 117
      Top = 19
      Width = 179
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 0
    end
    object EmployeeStatusCombo: TComboBox
      Left = 117
      Top = 46
      Width = 179
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
  end
end
