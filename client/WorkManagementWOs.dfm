inherited WorkMgtWOsFrame: TWorkMgtWOsFrame
  inherited SummaryPanel: TPanel
    Caption = 'Work Orders'
    inherited ExpandLabel: TLabel
      Height = 19
    end
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Work Orders'
    end
    inherited BaseGrid: TcxGrid
      Enabled = False
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = WorkOrderView
      end
    end
    inherited PanelDisplaySelectedItem: TPanel
      object Label29: TLabel
        Left = 5
        Top = 17
        Width = 71
        Height = 13
        Caption = 'Work Order #:'
      end
      object Label30: TLabel
        Left = 5
        Top = 36
        Width = 35
        Height = 13
        Caption = 'Status:'
      end
      object Label31: TLabel
        Left = 302
        Top = 55
        Width = 92
        Height = 13
        Caption = 'Master Order Num:'
      end
      object Label32: TLabel
        Left = 5
        Top = 55
        Width = 56
        Height = 13
        Caption = 'Work Type:'
      end
      object Label33: TLabel
        Left = 5
        Top = 92
        Width = 43
        Height = 13
        Caption = 'Address:'
      end
      object Label34: TLabel
        Left = 302
        Top = 36
        Width = 86
        Height = 13
        Caption = 'Client Order Num:'
      end
      object WOSumWONumber: TDBText
        Left = 82
        Top = 17
        Width = 107
        Height = 13
        AutoSize = True
        Color = clActiveCaption
        DataField = 'wo_number'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object WOSumStatus: TDBText
        Left = 85
        Top = 36
        Width = 81
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'status'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object WOSumAddress: TDBText
        Left = 85
        Top = 92
        Width = 90
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_address_number'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumClientWONumber: TDBText
        Left = 399
        Top = 36
        Width = 139
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'client_wo_number'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label35: TLabel
        Left = 5
        Top = 150
        Width = 48
        Height = 13
        Caption = 'WO Date:'
      end
      object WOSumTransmitDate: TDBText
        Left = 85
        Top = 150
        Width = 122
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'transmit_date'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label36: TLabel
        Left = 5
        Top = 186
        Width = 49
        Height = 13
        Caption = 'Due Date:'
      end
      object WOSumDueDate: TDBText
        Left = 85
        Top = 186
        Width = 93
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'due_date'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WoSumWorkType: TDBText
        Left = 85
        Top = 55
        Width = 101
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_type'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumClientMON: TDBText
        Left = 399
        Top = 55
        Width = 101
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'client_master_order_num'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumWireCenter: TDBText
        Left = 399
        Top = 73
        Width = 108
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'wire_center'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label37: TLabel
        Left = 302
        Top = 73
        Width = 86
        Height = 13
        Caption = 'Wire Center Num:'
      end
      object WOSumCity: TDBText
        Left = 85
        Top = 111
        Width = 66
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_city'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label38: TLabel
        Left = 5
        Top = 130
        Width = 75
        Height = 13
        Caption = 'Contact Phone:'
      end
      object WOSumContactPhone: TDBText
        Left = 85
        Top = 130
        Width = 123
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'caller_phone'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label39: TLabel
        Left = 5
        Top = 168
        Width = 57
        Height = 13
        Caption = 'Description:'
      end
      object WOSumJobNum: TDBText
        Left = 399
        Top = 17
        Width = 89
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'job_number'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label40: TLabel
        Left = 302
        Top = 17
        Width = 61
        Height = 13
        Caption = 'Job Number:'
      end
      object Label41: TLabel
        Left = 5
        Top = 205
        Width = 62
        Height = 13
        Caption = 'Closed Date:'
      end
      object WOSumClosedDate: TDBText
        Left = 85
        Top = 205
        Width = 108
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'closed_date'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label42: TLabel
        Left = 302
        Top = 92
        Width = 65
        Height = 13
        Caption = 'Work Center:'
      end
      object WOSumWorkCenter: TDBText
        Left = 399
        Top = 92
        Width = 112
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_center'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label43: TLabel
        Left = 5
        Top = 73
        Width = 61
        Height = 13
        Caption = 'Caller Name:'
      end
      object WOSumCallerName: TDBText
        Left = 85
        Top = 74
        Width = 108
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'caller_contact'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label44: TLabel
        Left = 302
        Top = 111
        Width = 71
        Height = 13
        Caption = 'Central Office:'
      end
      object WOSumCentralOffice: TDBText
        Left = 399
        Top = 111
        Width = 117
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'central_office'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label45: TLabel
        Left = 302
        Top = 130
        Width = 83
        Height = 13
        Caption = 'Serving Terminal:'
      end
      object WOSumServingTerm: TDBText
        Left = 399
        Top = 130
        Width = 117
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'serving_terminal'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label46: TLabel
        Left = 548
        Top = 17
        Width = 48
        Height = 13
        Caption = 'Circuit ID:'
      end
      object WOSumCircuitNum: TDBText
        Left = 659
        Top = 17
        Width = 105
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'circuit_number'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label47: TLabel
        Left = 548
        Top = 36
        Width = 46
        Height = 13
        Caption = 'F2 Cable:'
      end
      object WOSumF2Cable: TDBText
        Left = 659
        Top = 36
        Width = 88
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'f2_cable'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumTermPort: TDBText
        Left = 659
        Top = 55
        Width = 98
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'terminal_port'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label48: TLabel
        Left = 548
        Top = 55
        Width = 67
        Height = 13
        Caption = 'Terminal Port:'
      end
      object Label49: TLabel
        Left = 548
        Top = 73
        Width = 37
        Height = 13
        Caption = 'F2 Pair:'
      end
      object WOSumF2Pair: TDBText
        Left = 659
        Top = 73
        Width = 79
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'f2_pair'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label50: TLabel
        Left = 548
        Top = 92
        Width = 99
        Height = 13
        Caption = 'Within St Hwy ROW:'
      end
      object Label51: TLabel
        Left = 548
        Top = 111
        Width = 86
        Height = 13
        Caption = 'Road Bore Count:'
      end
      object WOSumRoadBores: TDBText
        Left = 659
        Top = 111
        Width = 105
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'road_bore_count'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label52: TLabel
        Left = 548
        Top = 130
        Width = 106
        Height = 13
        Caption = 'Driveway Bore Count:'
      end
      object WOSumDrivewayBores: TDBText
        Left = 659
        Top = 130
        Width = 129
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'driveway_bore_count'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumStateHwyROW: TDBText
        Left = 659
        Top = 92
        Width = 126
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'state_hwy_row'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumWorkState: TDBText
        Left = 214
        Top = 111
        Width = 39
        Height = 13
        DataField = 'work_state'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object WOSumWorkStreet: TDBText
        Left = 159
        Top = 92
        Width = 110
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_address_street'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object WOSumWorkDesc: TDBMemo
        Left = 85
        Top = 168
        Width = 211
        Height = 13
        BorderStyle = bsNone
        Color = clBtnFace
        DataField = 'work_description'
        DataSource = WorkOrderSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnDblClick = ItemDblClick
      end
      object RefreshWorkOrderButton: TButton
        Left = 5
        Top = 228
        Width = 126
        Height = 27
        Caption = 'Refresh'
        TabOrder = 1
        OnClick = RefreshWorkOrderButtonClick
      end
      object SendMessageGroupBox: TGroupBox
        Left = 293
        Top = 150
        Width = 405
        Height = 83
        Caption = 'CGA Commitment Message '
        TabOrder = 2
        Visible = False
        DesignSize = (
          405
          83)
        object WOAssignedToID: TDBText
          Left = 318
          Top = 0
          Width = 94
          Height = 17
          DataField = 'Assigned_To_ID'
          DataSource = WorkOrderSource
          Visible = False
        end
        object MessageLabel: TLabel
          Left = 9
          Top = 29
          Width = 86
          Height = 13
          Caption = 'Message Number:'
        end
        object MessageStatus: TLabel
          Left = 9
          Top = 51
          Width = 76
          Height = 13
          Caption = 'Message Status'
        end
        object SendMessageButton: TButton
          Left = 9
          Top = 54
          Width = 88
          Height = 22
          Action = SendMessageAction
          Enabled = False
          TabOrder = 0
        end
        object MessageNumberEdit: TEdit
          Left = 101
          Top = 26
          Width = 139
          Height = 21
          TabStop = False
          ParentColor = True
          ReadOnly = True
          TabOrder = 1
        end
        object CGAMessage: TMemo
          Left = 9
          Top = 15
          Width = 390
          Height = 35
          Anchors = [akLeft, akTop, akRight, akBottom]
          ScrollBars = ssVertical
          TabOrder = 2
        end
      end
    end
  end
  inherited ActionListFrame: TActionList
    OnUpdate = ActionListFrameUpdate
    object SendMessageAction: TAction
      Caption = 'Send Message'
      OnExecute = SendMessageActionExecute
    end
  end
  inherited ListTable: TDBISAMTable
    AfterOpen = ItemListTableAfterOpen
    TableName = 'TMWorkOrderList'
  end
  inherited DetailTable: TDBISAMTable
    TableName = 'work_order'
  end
  object WOGridViewRepository: TcxGridViewRepository
    Left = 523
    Top = 120
    object WorkOrderView: TcxGridDBTableView
      DragMode = dmAutomatic
      OnDblClick = ViewDblClick
      OnMouseUp = WorkOrderViewMouseUp
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = True
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = True
      Navigator.Buttons.GotoBookmark.Visible = True
      Navigator.Buttons.Filter.Visible = True
      OnCellClick = WorkOrderViewCellClick
      OnCustomDrawCell = WorkOrderViewCustomDrawCell
      OnSelectionChanged = WorkOrderViewSelectionChanged
      DataController.DataSource = WorkOrderListSource
      DataController.KeyFieldNames = 'wo_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.NoDataToDisplayInfoText = '<No work orders for selected locator>'
      OptionsView.GridLines = glNone
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Inactive = UnfocusedGridSelStyle
      Styles.Selection = GridSelectionStyle
      OnCustomDrawIndicatorCell = WorkOrderViewCustomDrawIndicatorCell
      object ColWOWorkOrderNumber: TcxGridDBColumn
        Caption = 'Work Order #'
        DataBinding.FieldName = 'wo_number'
        Options.Editing = False
        Width = 106
      end
      object ColWOKind: TcxGridDBColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Width = 88
      end
      object ColWOStatusCode: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Visible = False
        Options.Editing = False
        Width = 51
      end
      object ColWOWorkOrderStatusDescription: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status_description'
        Width = 92
      end
      object ColWODueDate: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.ShowEditButtons = isebNever
        Width = 112
      end
      object ColWOWorkOrderDate: TcxGridDBColumn
        Caption = 'Order Date'
        DataBinding.FieldName = 'transmit_date'
        Options.Editing = False
        Options.ShowEditButtons = isebNever
        Width = 112
      end
      object ColWOWorkAddressNumber: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'work_address_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Width = 38
      end
      object ColWOStreet: TcxGridDBColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Width = 165
      end
      object ColWOCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Options.Editing = False
        Width = 110
      end
      object ColWOCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Width = 108
      end
      object ColWOState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'work_state'
        Width = 38
      end
      object ColWOMapPage: TcxGridDBColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 61
      end
      object ColWOMapRef: TcxGridDBColumn
        Caption = 'Map Grid'
        DataBinding.FieldName = 'map_ref'
        Width = 56
      end
      object ColWOClient: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'oc_code'
        Width = 111
      end
      object ColWOWorkOrderStatus: TcxGridDBColumn
        Caption = 'Status Byte'
        DataBinding.FieldName = 'work_order_status'
        Visible = False
      end
      object ColWOID: TcxGridDBColumn
        Caption = 'WO ID'
        DataBinding.FieldName = 'wo_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        VisibleForCustomization = False
      end
    end
  end
  object WorkOrderListSource: TDataSource
    DataSet = ListTable
    Left = 40
    Top = 92
  end
  object WorkOrder: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'work_order'
    Left = 696
    Top = 404
  end
  object WorkOrderSource: TDataSource
    DataSet = DetailTable
    Left = 725
    Top = 348
  end
  object cxStyleRepoTickets: TcxStyleRepository
    PixelsPerInch = 96
    object BottomGridStyle: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object GridSelectionStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clHotLight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object UnfocusedGridSelStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 11829830
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = 14745599
    end
    object IndicatorStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16776176
      TextColor = clBlue
    end
  end
end
