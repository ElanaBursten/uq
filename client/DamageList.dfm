object DamageListForm: TDamageListForm
  Left = 262
  Top = 168
  Caption = 'Damages'
  ClientHeight = 348
  ClientWidth = 569
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SearchDamageButton: TButton
    Left = 112
    Top = 8
    Width = 97
    Height = 25
    Action = SearchDamageAction
    TabOrder = 1
  end
  object NewButton: TButton
    Left = 8
    Top = 8
    Width = 97
    Height = 25
    Action = NewDamageAction
    TabOrder = 0
  end
  object SearchInvoiceButton: TButton
    Left = 216
    Top = 8
    Width = 97
    Height = 25
    Action = SearchInvoiceAction
    TabOrder = 2
  end
  object SearchLitigationButton: TButton
    Left = 320
    Top = 8
    Width = 97
    Height = 25
    Action = SearchLitigationAction
    TabOrder = 3
  end
  object SearchThirdPartyButton: TButton
    Left = 424
    Top = 8
    Width = 97
    Height = 25
    Action = SearchThirdPartyAction
    TabOrder = 4
  end
  object ActionList: TActionList
    Left = 464
    Top = 224
    object NewDamageAction: TAction
      Caption = 'New Damage'
      OnExecute = NewDamageActionExecute
    end
    object SearchDamageAction: TAction
      Caption = 'Search Damage'
      OnExecute = SearchDamageActionExecute
    end
    object SearchLitigationAction: TAction
      Caption = 'Search Litigation'
      OnExecute = SearchLitigationActionExecute
    end
    object SearchThirdPartyAction: TAction
      Caption = 'Search 3rd Party'
      OnExecute = SearchThirdPartyActionExecute
    end
    object SearchInvoiceAction: TAction
      Caption = 'Search Invoice'
      OnExecute = SearchInvoiceActionExecute
    end
  end
end
