object EstimateFrame: TEstimateFrame
  Left = 0
  Top = 0
  Width = 670
  Height = 354
  TabOrder = 0
  object EstimatesGrid: TDBGrid
    Left = 0
    Top = 81
    Width = 670
    Height = 247
    Align = alClient
    DataSource = EstimateSource
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'estimate_type_desc'
        Title.Caption = 'Type'
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'company'
        Title.Caption = 'Claimant'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'modified_date'
        Title.Caption = 'Date'
        Width = 147
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'employee_name'
        Title.Caption = 'Employee'
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'amount'
        Title.Caption = 'Amount'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contract'
        Title.Caption = 'Contract'
        Width = 83
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comment'
        Title.Caption = 'Comment'
        Width = 219
        Visible = True
      end>
  end
  object NewEstimatePanel: TPanel
    Left = 0
    Top = 0
    Width = 670
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 1
    object EstimateAmountLabel: TLabel
      Left = 1
      Top = 11
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estimate Amount'
    end
    object EstimateCommentLabel: TLabel
      Left = 262
      Top = 11
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comment'
    end
    object Label2: TLabel
      Left = 41
      Top = 58
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Claimant'
      Visible = False
    end
    object EstimateCompanyLabel: TLabel
      Left = 96
      Top = 58
      Width = 215
      Height = 13
      AutoSize = False
      Caption = ' '
      Visible = False
    end
    object AccrualAmountLabel: TLabel
      Left = 9
      Top = 35
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'Accrual Amount'
    end
    object AccrualAmount: TLabel
      Left = 95
      Top = 35
      Width = 22
      Height = 13
      Caption = '0.00'
    end
    object NewEstimateButton: TButton
      Left = 324
      Top = 53
      Width = 109
      Height = 25
      Caption = 'Add New Estimate'
      TabOrder = 2
      OnClick = NewEstimateButtonClick
    end
    object EstimateAmount: TEdit
      Left = 95
      Top = 7
      Width = 157
      Height = 21
      TabOrder = 0
    end
    object EstimateComment: TEdit
      Left = 323
      Top = 7
      Width = 307
      Height = 21
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 328
    Width = 670
    Height = 26
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 2
    object ViewAllck: TCheckBox
      Left = 4
      Top = 5
      Width = 129
      Height = 17
      Caption = 'View All Estimates'
      TabOrder = 0
      OnClick = ViewAllckClick
    end
  end
  object EstimateSource: TDataSource
    AutoEdit = False
    Left = 18
    Top = 98
  end
end
