unit DamageLiabilityChangesReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect;

type
  TDamageLiabilityChangesReportForm = class(TReportBaseForm)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ActivityDate: TOdRangeSelectFrame;
    DamageDate: TOdRangeSelectFrame;
    NotifiedDate: TOdRangeSelectFrame;
    Label1: TLabel;
    Label2: TLabel;
    State: TComboBox;
    ProfitCenter: TComboBox;
    LiabilityLabel: TLabel;
    LiabilityStart: TComboBox;
    LiabilityEnd: TComboBox;
    Label6: TLabel;
    Mode: TComboBox;
    Label7: TLabel;
    IncludeNoUQCheckBox: TCheckBox;
    IncludeNoUQCheckBoxLabel: TLabel;
    Label8: TLabel;
    EditMinChange: TEdit;
    Label9: TLabel;
    ExcludeInitialEstimateCheckBox: TCheckBox;
    IncludeThirdParty: TCheckBox;
    IncludeLitigation: TCheckBox;
    procedure ModeChange(Sender: TObject);
    procedure EditMinChangeKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  private
    procedure SetupLiabilityCombo(Combo: TComboBox);
    procedure ResetLiabilityCombos;
    function FinancialMode: Boolean;
  end;

implementation

uses
  DMu, OdExceptions, QMConst;

{$R *.dfm}

{ TDamageEstimateChangesReportForm }

procedure TDamageLiabilityChangesReportForm.ValidateParams;
begin
  inherited;
  if Length(State.Text) = 1 then begin
    State.SetFocus;
    raise EOdEntryRequired.Create('Please enter a valid state abbreviation.');
  end;

  SetReportID('DamageLiabilityChanges');
  SetParamDate('DateFrom', ActivityDate.FromDate);
  SetParamDate('DateTo', ActivityDate.ToDate);

  if not DamageDate.IsEmpty then begin
    SetParamDate('DamageDateFrom', DamageDate.FromDate);
    SetParamDate('DamageDateTo', DamageDate.ToDate);
  end;
  if not NotifiedDate.IsEmpty then begin
    SetParamDate('NotifiedDateFrom', NotifiedDate.FromDate);
    SetParamDate('NotifiedDateTo', NotifiedDate.ToDate);
  end;

  SetParam('State', State.Text);
  SetParam('ProfitCenter', ProfitCenter.Text);

  // The report doesn't really need to know this, but we'll send it
  // so the report system can log it.
  SetParam('Mode', Mode.Text);

  SetParamInt('LiabilityStart', LiabilityStart.ItemIndex);
  SetParamInt('LiabilityEnd', LiabilityEnd.ItemIndex);

  if IncludeNoUQCheckBox.Checked then
    SetParamInt('IncludeZeroUQLiab', 1)
  else
    SetParamInt('IncludeZeroUQLiab', 0);

  if EditMinChange.Text = '' then
    SetParamInt('MinChange', 0)
  else
    SetParamInt('MinChange', StrToInt(EditMinChange.Text));

  if ExcludeInitialEstimateCheckBox.Checked then
    SetParamInt('ExcludeInitialEst', 1)
  else
    SetParamInt('ExcludeInitialEst', 0);
  SetParamBoolean('IncludeThirdParty', IncludeThirdParty.Checked);
  SetParamBoolean('IncludeLitigation', IncludeLitigation.Checked);
end;

procedure TDamageLiabilityChangesReportForm.InitReportControls;
begin
  inherited;
  IncludeNoUQCheckBoxLabel.Caption := 'Include damages with no ' + DM.UQState.CompanyShortName +
    ' liability at the start or end of the range. These can be safely omitted for ' +
    DM.UQState.CompanyShortName + ' liability reporting';
  Mode.Items.Clear;
  Mode.Items.Add(DM.UQState.CompanyShortName + ' Liability Change Reporting');
  Mode.Items.Add('Misc. Change Reporting');
  Mode.ItemIndex := 0;
  ModeChange(Self);

  ActivityDate.SelectDateRange(rsPreviousMonth);
  DamageDate.NoDateRange;
  NotifiedDate.NoDateRange;
  DM.GetRefDisplayList('state', State.Items);
  DM.ProfitCenterList(ProfitCenter.Items);

  ResetLiabilityCombos;
end;

procedure TDamageLiabilityChangesReportForm.SetupLiabilityCombo(
  Combo: TComboBox);
var
  i: TDamageLiability;
begin
  Combo.Items.Clear;
  for i := Low(TDamageLiability) to High(TDamageLiability) do
    Combo.Items.Add(DM.UQState.DamageLiabilityDescription[i]);
  Combo.ItemIndex := Combo.Items.Count-1;
end;

procedure TDamageLiabilityChangesReportForm.ResetLiabilityCombos;
begin
  SetupLiabilityCombo(LiabilityStart);
  SetupLiabilityCombo(LiabilityEnd);
  EditMinChange.Text := '';
end;

function TDamageLiabilityChangesReportForm.FinancialMode: Boolean;
begin
  Result :=  Mode.ItemIndex = 0;
end;

procedure TDamageLiabilityChangesReportForm.ModeChange(Sender: TObject);
begin
  inherited;
  if FinancialMode then
    ResetLiabilityCombos;

  LiabilityStart.Enabled := not FinancialMode;
  LiabilityEnd.Enabled := not FinancialMode;

  IncludeNoUQCheckBox.Checked := not FinancialMode;
  IncludeNoUQCheckBox.Enabled := not FinancialMode;
  IncludeNoUQCheckBoxLabel.Enabled := not FinancialMode;

  EditMinChange.Enabled := not FinancialMode;

  ExcludeInitialEstimateCheckBox.Enabled := not FinancialMode;
end;

procedure TDamageLiabilityChangesReportForm.EditMinChangeKeyPress(
  Sender: TObject; var Key: Char);
begin
  // only allow digits
  if not (Key in ['0'..'9', #13, #8, #9]) then Key := #0;
end;

end.

