object frmWaze: TfrmWaze
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = '              Waze Locater'
  ClientHeight = 344
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object wazePaintBox: TPaintBox
    Left = 0
    Top = 0
    Width = 300
    Height = 297
    Align = alTop
    OnClick = wazePaintBoxClick
    OnPaint = wazePaintBoxPaint
  end
  object Panel1: TPanel
    Left = 0
    Top = 297
    Width = 300
    Height = 50
    Align = alTop
    TabOrder = 0
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 298
      Height = 48
      Align = alClient
      Color = clMoneyGreen
      ReadOnly = True
      TabOrder = 0
      OnClick = Memo1Click
    end
  end
end
