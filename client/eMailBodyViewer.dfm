object frmEmailBody: TfrmEmailBody
  Left = 0
  Top = 0
  ActiveControl = mailBodyMemo
  AlphaBlendValue = 120
  BorderStyle = bsSizeToolWin
  Caption = '                       EMail reader'
  ClientHeight = 284
  ClientWidth = 378
  Color = clAqua
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mailBodyMemo: TMemo
    Left = 0
    Top = 0
    Width = 378
    Height = 243
    Align = alTop
    Color = clCream
    Lines.Strings = (
      '')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WantReturns = False
  end
  object btnClose: TBitBtn
    Left = 156
    Top = 254
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkClose
  end
end
