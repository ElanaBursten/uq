inherited SearchCriteria: TSearchCriteria
  Align = alTop
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Search'
  ClientHeight = 149
  ClientWidth = 667
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object RecordsLabel: TLabel
    Left = 70
    Top = 124
    Width = 113
    Height = 13
    Caption = '1000 found (1000 max)'
    Visible = False
  end
  object SearchButton: TButton
    Left = 186
    Top = 118
    Width = 75
    Height = 25
    Caption = '&Search'
    Default = True
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 505
    Top = 118
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object CopyButton: TButton
    Left = 266
    Top = 118
    Width = 75
    Height = 25
    Caption = 'C&opy'
    Enabled = False
    TabOrder = 2
  end
  object ExportButton: TButton
    Left = 346
    Top = 118
    Width = 75
    Height = 25
    Caption = '&Export'
    Enabled = False
    ModalResult = 4
    TabOrder = 3
  end
  object PrintButton: TButton
    Left = 425
    Top = 118
    Width = 75
    Height = 25
    Caption = '&Print'
    Enabled = False
    TabOrder = 4
    Visible = False
  end
  object ClearButton: TButton
    Left = 584
    Top = 118
    Width = 75
    Height = 25
    Caption = 'C&lear'
    TabOrder = 5
  end
end
