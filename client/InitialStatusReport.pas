unit InitialStatusReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls, OdRangeSelect, CheckLst;

type
  TInitialStatusReportForm = class(TReportBaseForm)
    RangeSelect: TOdRangeSelectFrame;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    Label3: TLabel;
    Arrival: TCheckBox;
    CallCenterList: TCheckListBox;
    Label4: TLabel;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdVclUtils, OdExceptions, ODMiscUtils;

{ TInitialStatusReportForm }

procedure TInitialStatusReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  SetUpManagerList(ManagersComboBox);
end;

procedure TInitialStatusReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('InitialStatus');
  SetParamDate('StartDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);

  CheckedItems := GetCheckedItemCodesString(CallCenterList);
  if IsEmpty(CheckedItems) then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one call center for the report.');
  end;

  SetParam('CallCenter', CheckedItems);
  SetParamIntCombo('ManagerId', ManagersComboBox);
  SetParamBoolean('Arrival', Arrival.Checked);
end;

end.

