unit DamageInvoiceForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, DB, DBISAMTb, Mask, DBCtrls, ActnList, DMu,
  Invoice;

type
  TDamageInvoiceForm = class(TEmbeddableForm)
    InvoiceFrame: TInvoiceFrame;
    DummyList: TActionList;
    DummyAction: TAction;
    DummyActionButton: TButton;
    procedure DummyActionExecute(Sender: TObject);
    procedure DummyListUpdate(Action: TBasicAction; var Handled: Boolean);
  private
    function GetOnFinished: TNotifyEvent;
    procedure SetOnFinished(const Value: TNotifyEvent);
    function GetOnGoToDamage: TItemSelectedEvent;
    procedure SetOnGoToDamage(const Value: TItemSelectedEvent);
  public
    property OnFinished: TNotifyEvent read GetOnFinished write SetOnFinished;
    property OnGoToDamage: TItemSelectedEvent read GetOnGoToDamage write SetOnGoToDamage;
    procedure AddNewInvoice;
    procedure GoToDamageInvoice(InvoiceID: Integer);
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
  end;

implementation

uses BaseSearchForm, DamageSearchHeader, OdDBUtils, OdExceptions, OdHourglass;

{$R *.dfm}

procedure TDamageInvoiceForm.AddNewInvoice;
begin
  InvoiceFrame.AddNewInvoice;
end;

procedure TDamageInvoiceForm.LeavingNow;
var
  Answer: Word;
begin
  inherited;
  if InvoiceFrame.Editing then begin
    Answer := MessageDlg('Save current invoice?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    case Answer of
      mrYes: InvoiceFrame.SaveAction.Execute;
      mrNo: InvoiceFrame.Invoice.Cancel;
      mrCancel: begin
        InvoiceFrame.Invoice.Edit;
        Abort;
      end;
    end;
  end;
end;

procedure TDamageInvoiceForm.ActivatingNow;
begin
  inherited;
  InvoiceFrame.FrameEnabled := True;
end;

procedure TDamageInvoiceForm.GoToDamageInvoice(InvoiceID: Integer);
begin
  InvoiceFrame.GoToDamageInvoice(InvoiceID);
end;

function TDamageInvoiceForm.GetOnFinished: TNotifyEvent;
begin
  Result := InvoiceFrame.OnFinished;
end;

procedure TDamageInvoiceForm.SetOnFinished(const Value: TNotifyEvent);
begin
  InvoiceFrame.OnFinished := Value;
end;

function TDamageInvoiceForm.GetOnGoToDamage: TItemSelectedEvent;
begin
  Result := InvoiceFrame.OnGoToDamage;
end;

procedure TDamageInvoiceForm.SetOnGoToDamage(const Value: TItemSelectedEvent);
begin
  InvoiceFrame.OnGoToDamage := Value;
end;

procedure TDamageInvoiceForm.DummyActionExecute(Sender: TObject);
begin
  inherited;
  ShowMessage('Nothing');
end;

procedure TDamageInvoiceForm.DummyListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  inherited;
  InvoiceFrame.UpdateActions;
end;

end.
