object StatFrame: TStatFrame
  Left = 0
  Top = 0
  Width = 451
  Height = 34
  Align = alTop
  AutoSize = True
  Color = clCaptionText
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object pnlQuestion: TPanel
    Left = 0
    Top = 0
    Width = 409
    Height = 34
    Align = alLeft
    ParentBackground = False
    TabOrder = 0
    object edtQuestion: TMemo
      Left = 1
      Top = 1
      Width = 407
      Height = 32
      TabStop = False
      Align = alClient
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 0
    end
  end
  object pnlAnswer: TPanel
    Left = 409
    Top = 0
    Width = 42
    Height = 34
    Align = alClient
    ParentBackground = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 1
      Top = 1
      Height = 32
    end
    object CountLbl: TLabel
      Left = -13
      Top = 1
      Width = 18
      Height = 32
      Align = alRight
      AutoSize = False
      Caption = '444'
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object btnClearIt: TButton
      Left = 5
      Top = 1
      Width = 36
      Height = 32
      Align = alRight
      Caption = 'Clear'
      TabOrder = 0
      TabStop = False
      Visible = False
      OnClick = btnClearItClick
    end
  end
end
