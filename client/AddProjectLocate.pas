unit AddProjectLocate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  StdCtrls, cxContainer, cxEdit, ExtCtrls,
  CheckLst, DB, dbisamtb, cxTextEdit, cxMaskEdit, cxDropDownEdit, 
  cxDBLookupComboBox, cxMemo;

type
  TLocateClientData = record
    TicketID: Integer;
    LocateID: Integer;
    ClientID: Integer;
      ClientName: string;
      OCCode: string;
      OfficeID: integer;
      CallCenter: string;
      AllowEditLocates: Boolean;  //EB TO DO check and see how this might affect us
    Status:   string;
    HighProf: Boolean;
    AssignedToID: Integer;
    ParentClient: string;  //Set this temporarily so that the user can see it.
    Active: boolean;
    ShortNote: string;
  end;

  TAddProjectLocateForm = class(TForm)
    NamePanel: TPanel;
    Panel2: TPanel;
    ClientQry: TDBISAMQuery;
    QryOrigLocate: TDBISAMQuery;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    ClientCombo: TComboBox;
    Label1: TLabel;
    OKBtn: TButton;
    Button1: TButton;
    LocatorComboBox: TcxComboBox;
    LocateNoEdit: TEdit;
    lblNote: TLabel;
    edtShortNote: TEdit;
    MessagePanel: TPanel;
    MessageMemo: TcxMemo;
    StatusCombo: TcxComboBox;
    procedure FormShow(Sender: TObject);
    procedure ClientComboChange(Sender: TObject);
    procedure StatusComboChange(Sender: TObject);
    procedure LocatorComboBoxClick(Sender: TObject);
  private
    fInitialized: boolean;
    fOrigLocate : TLocateClientData;
    fNewLocate : TLocateClientData;
    procedure ClearListandObjects(AList: TStringList);
    procedure PopulateStatusList;
    procedure SetChildClientList; //SetCallCenter
    procedure ClearLocateInfo(LocateData: TLocateClientData);
//    procedure SetParentLengthMarked(Value: integer);    //Previous design...but will leave in here for now
//    function IsParentLengthMarked: boolean;
  protected
    procedure EnableDisableControls;
  public
    procedure InitTicketLocate(TicketID, LocateID: Integer; MgrID: integer);
    procedure LoadLocatorList(ManagerID: integer);
    procedure ClearLocatorList;
    function SaveRecord: TLocateClientData;

  end;


  // Note: The locate is added to the memory dataset (not yet committed to disk)
function ExecuteAddProjectLocateDialog(TicketID, LocateID: Integer; MgrID: integer; pParentLocateMarkedAmt: integer): TLocateClientData;


const
 {Form Height Settings}
  HeightNoMessage = 257;
  HeightWithMessage =  317;

implementation
uses
  DMu, OdvclUtils, QMConst, LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

{ TAddProjectLocateForm }

procedure TAddProjectLocateForm.ClearListandObjects(AList: TStringList);
var
  i: Integer;
begin
  for i := 0 to (AList.Count-1) do
    AList.Objects[i].Free;
  AList.Clear;
end;

procedure TAddProjectLocateForm.ClearLocateInfo(LocateData: TLocateClientData);
begin
  LocateData.Active := False;
  LocateData.TicketID := -1;
  LocateData.LocateID := -1;
  LocateData.ClientID := -1;
  LocateData.ClientName := '';
  LocateData.OCCode := '';
  LocateData.OfficeID := -1;
  LocateData.CallCenter := '';
  LocateData.AllowEditLocates := False;
  LocateData.Status := '';
  LocateData.HighProf := False;
  LocateData.AssignedToID := -1;
  LocateData.ParentClient := '';
end;

procedure TAddProjectLocateForm.ClearLocatorList;
begin
  LocatorComboBox.Clear;
end;

procedure TAddProjectLocateForm.ClientComboChange(Sender: TObject);
begin
  PopulateStatusList;
  EnableDisableControls;
end;


procedure TAddProjectLocateForm.EnableDisableControls;
begin
  OKBtn.Enabled := (ClientCombo.ItemIndex <> -1) and
    (StatusCombo.ItemIndex <> -1) and (LocatorComboBox.ItemIndex <> -1);
//  StatusCombo.Enabled := (ClientCombo.ItemIndex <> -1);
end;

procedure TAddProjectLocateForm.FormShow(Sender: TObject);
begin
  fInitialized := False;
end;

procedure TAddProjectLocateForm.InitTicketLocate(TicketID, LocateID: Integer; MgrID: integer);   //TicketID, LocateID, LocateClient, PermissionsMgrID
begin
  ClearLocateInfo(fOrigLocate);
  ClearLocateInfo(fNewLocate);
  fOrigLocate.LocateID := LocateID;
  fOrigLocate.TicketID := TicketID;
  QryOrigLocate.Close;
  QryOrigLocate.ParamByName('locate_id').AsInteger := LocateID;
  QryOrigLocate.Open;
  if QryOrigLocate.RecordCount = 0 then begin
    fOrigLocate.Status := '';
    fOrigLocate.HighProf := False;
    fOrigLocate.ClientID := 0;
  end
  else begin
    fOrigLocate.Status       := QryOrigLocate.FieldByName('status').AsString;
    fOrigLocate.ClientID     := QryOrigLocate.FieldByName('client_id').AsInteger;
      fOrigLocate.ClientName := QryOrigLocate.FieldByName('client_name').AsString;
      fOrigLocate.OCCode     := QryOrigLocate.FieldByName('oc_code').AsString;
      fOrigLocate.OfficeID   := QryOrigLocate.FieldByName('office_id').AsInteger;
      fOrigLocate.CallCenter := QryOrigLocate.FieldByName('call_center').AsString;
      fOrigLocate.AllowEditLocates := QryOrigLocate.FieldByName('allow_edit_locates').AsBoolean;
    fOrigLocate.HighProf     := QryOrigLocate.FieldByName('high_profile').AsBoolean;
    fOrigLocate.AssignedToID := QryOrigLocate.FieldByName('assigned_to_id').AsInteger;

//    fOrigLocate.ParentLocateID := -1;
//    fNewLocate := fOrigLocate;
//    {The New locate is a child of the Orig locate}
//    fNewLocate.ParentLocateID := fOrigLocate.LocateID;

  end;
  LocateNoEdit.Text := fOrigLocate.ClientName;

  ClearLocatorList;
  LoadLocatorList(MgrID);
  SetChildClientList;
  PopulateStatusList;

  fInitialized := True;
end;
//
//function TAddProjectLocateForm.IsParentLengthMarked: boolean;
//begin
//  Result := (fParentLengthMarkedAmt > 0);
//end;

procedure TAddProjectLocateForm.LoadLocatorList(ManagerID: integer);

begin
  ClearLocatorList;
  EmployeeDM.EmployeeList(LocatorComboBox.Properties.Items, ManagerID, 1);
end;

procedure TAddProjectLocateForm.LocatorComboBoxClick(Sender: TObject);
begin
  EnableDisableControls;
end;

procedure TAddProjectLocateForm.PopulateStatusList;
begin
  {QMANTWO-616 EB Currently, only -R is a valid starting status}
  //QM-282 EB Changed status combo to devEx
  StatusCombo.Properties.Items.Clear;
  StatusCombo.Properties.Items.Add(ReadyToWorkStatus);
  StatusCombo.ItemIndex := StatusCombo.Properties.Items.IndexOf(ReadyToWorkStatus);
  StatusCombo.Enabled := False;
end;

function TAddProjectLocateForm.SaveRecord: TLocateClientData;
var
  CurLocatorID,
  CurClientID: integer;
  CurStatus: string;
  NewLocateID: integer;
begin

  //Don't try to popup the hours dialog on status change during the locate add
  DM.IgnoreSetStatus := True;
  try
    CurLocatorID := Integer(LocatorComboBox.Properties.Items.Objects[LocatorComboBox.ItemIndex]);
    CurClientID := Integer(ClientCombo.Items.Objects[ClientCombo.ItemIndex]);
    CurStatus := StatusCombo.Text;
    if fOrigLocate.ClientID >= 0 then
      with DM.LocateData do begin
        Append;
        FieldByName('locator_id').AsInteger := CurLocatorID;
        FieldByName('short_name').AsString := EmployeeDM.GetEmployeeShortName(FieldByName('locator_id').AsInteger);
        // Some code in TDM expects locate_id to be negative for new locates
        NewLocateID := DM.Engine.GenTempKey;
        FieldByName('locate_id').AsInteger := NewLocateID;
        FieldByName('ticket_id').AsInteger := fOrigLocate.TicketID;
        FieldByName('client_id').AsInteger := CurClientID;
        FieldByName('client_code').AsString := DM.GetClientCode(CurClientID);
        FieldByName('status').AsString := CurStatus;
        FieldByName('high_profile').AsBoolean := fOrigLocate.HighProf;
        FieldByName('qty_marked').AsInteger := 1;
        FieldByName('active').AsBoolean := True;
        FieldByName('added_by').AsString := IntToStr(DM.UQState.EmpID);
        FieldByName('LocalStringKey').AsString := FieldByName('locate_id').AsString;
        FieldByName('assigned_to').AsInteger := CurLocatorID;
        FieldByName('assigned_to_id').AsInteger := CurLocatorID;
        FieldByName('DeltaStatus').AsString := 'I';
        FieldByName('parent_code').AsString := fOrigLocate.OCCode;  //EB - Temporarily force this to show in grid

        {EB - Assign our NewLocate data}
        fNewLocate := fOrigLocate;
        fNewLocate.TicketID := fOrigLocate.TicketID;;
        fNewLocate.LocateID := NewLocateID;
        fNewLocate.AssignedToID := CurLocatorID;
        fNewLocate.ClientID := CurClientID;
        fNewLocate.Status := CurStatus;
        fNewLocate.ShortNote := 'Added ' + LocatorComboBox.Text + ' on ' + DateToStr(Now) +
                                ' to ' + LocateNoEdit.Text + ':  ' + edtShortNote.Text;
        fNewLocate.Active := True;

        Post;
       DM.AddLocatetoProject(fNewLocate.TicketID, fNewLocate.ClientID, fNewLocate.LocateID, fNewLocate.AssignedToID);
      end;
  finally
    DM.IgnoreSetStatus := False;
    Result := fNewLocate;
  end;

end;


procedure TAddProjectLocateForm.SetChildClientList;
begin
  DM.GetProjectClientList(fOrigLocate.CallCenter, fOrigLocate.ClientID, ClientCombo.Items, fOrigLocate.TicketID);
  SizeComboDropdownToItems(ClientCombo);
  // Exclude the -R status from the list if the user is not a manager
  DM.GetStatusStrings(StatusCombo.Properties.Items, not PermissionsDM.CanI(RightTicketsAddReadyToMarkLocate));
  EnableDisableControls;
end;

//procedure TAddProjectLocateForm.SetParentLengthMarked(Value: integer);
//const
//  ParentLengthMessage = 'This client already has a length of %d in the parent. ' +
//  'It will be overwritten by the Project child locates when they are rolled up.' +
//  'To avoid: add units to one of the child locates.';
//begin
//  fParentLengthMarkedAmt := Value;
//  if IsParentLengthMarked then begin
//    MessageMemo.Lines.Add(Format(ParentLengthMessage, [fParentLengthMarkedAmt]));
//    MessagePanel.Visible := True;
//    Self.Height := HeightWithMessage;
//  end
//  else begin
//    MessagePanel.Visible := False;
//    MessageMemo.Clear;
//    Self.Height := HeightNoMessage;
//  end;
//end;

procedure TAddProjectLocateForm.StatusComboChange(Sender: TObject);
begin
  EnableDisableControls;
end;

function ExecuteAddProjectLocateDialog(TicketID, LocateID: Integer; MgrID: integer; pParentLocateMarkedAmt: integer): TLocateClientData;
var
  AddProjectLocateForm : TAddProjectLocateForm;
begin
  AddProjectLocateForm := TAddProjectLocateForm.Create(Nil);
    try
      AddProjectLocateForm.InitTicketLocate(TicketID, LocateID, MgrID);
      AddProjectLocateForm.Height := HeightNoMessage;
 //     AddProjectLocateForm.SetParentLengthMarked(pParentLocateMarkedAmt);
      if AddProjectLocateForm.ShowModal = mrOK then
        Result := AddProjectLocateForm.SaveRecord;
    finally
      AddProjectLocateForm.Free;
    end;

end;

end.
