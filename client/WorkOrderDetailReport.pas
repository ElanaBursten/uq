unit WorkOrderDetailReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, ExtCtrls, BaseSearchForm,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit;

type
  TWorkOrderDetailReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    WorkOrderNumberEdit: TcxButtonEdit;
    CallCenterEdit: TEdit;
    IncludeNotes: TCheckBox;
    TransmitDateEdit: TEdit;
    Label3: TLabel;
    IncludeAttachments: TCheckBox;
    LabelSelectImages: TLabel;
    AttachmentList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    Label4: TLabel;
    procedure IncludeAttachmentsClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure WorkOrderNumberEditButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure UpdateAttachmentList;
    procedure UpdateWorkOrderFields;
  public
    class procedure SetWorkOrderID(WOID: Integer);
  end;

implementation

uses
  DMu, OdExceptions, QMConst, OdVclUtils, OdMiscUtils, WorkOrderSearchHeader;

var
  FWOID: Integer;
{$R *.dfm}

{ TWorkOrderDetailReportForm }

class procedure TWorkOrderDetailReportForm.SetWorkOrderID(WOID: Integer);
begin
  FWOID := WOID;
end;

procedure TWorkOrderDetailReportForm.ValidateParams;
begin
  inherited;
  if (FWOID < 1) then
    raise EOdEntryRequired.Create('You must specify a work order number');

  IncludeAttachments.Checked := not IsEmpty(GetCheckedItemString(AttachmentList, True));

  SetReportID('WorkOrderDetail');
  SetParamInt('WOID', FWOID);
  SetParamBoolean('IncludeNotes', IncludeNotes.Checked);
  if IncludeAttachments.Checked then
    SetParam('ImageList', GetCheckedItemString(AttachmentList, True));
end;

procedure TWorkOrderDetailReportForm.InitReportControls;
begin
  inherited;

  WorkOrderNumberEdit.Clear;
  CallCenterEdit.Clear;
  TransmitDateEdit.Clear;
  IncludeNotes.Checked := True;
  UpdateWorkOrderFields;
end;

procedure TWorkOrderDetailReportForm.UpdateWorkOrderFields;
var
  CloseWorkOrder: Boolean;
begin
  if FWOID > 0 then begin
    CloseWorkOrder := not DM.WorkOrder.Active; // Active when coming from Work Order Detail
    if not DM.WorkOrder.Active then begin
      DM.WorkOrder.Open;
      DM.WorkOrder.Locate('wo_id', FWOID, []);
    end;
    WorkOrderNumberEdit.Text := DM.WorkOrder.FieldByName('wo_number').AsString;
    CallCenterEdit.Text := DM.WorkOrder.FieldByName('wo_source').AsString;
    TransmitDateEdit.Text := DM.WorkOrder.FieldByName('transmit_date').AsString;
    if CloseWorkOrder then
      DM.WorkOrder.Close;
    UpdateAttachmentList;
  end;
end;

procedure TWorkOrderDetailReportForm.IncludeAttachmentsClick(Sender: TObject);
begin
  AttachmentList.Enabled := IncludeAttachments.Checked;
  SelectAllButton.Enabled := IncludeAttachments.Checked;
  ClearAllButton.Enabled := IncludeAttachments.Checked;
  LabelSelectImages.Enabled := IncludeAttachments.Checked;
  SetCheckListBox(AttachmentList, IncludeAttachments.Checked);
end;

procedure TWorkOrderDetailReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(AttachmentList, True);
end;

procedure TWorkOrderDetailReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(AttachmentList, False);
end;

procedure TWorkOrderDetailReportForm.UpdateAttachmentList;
begin
  Assert(FWOID > 0, 'FWOID is required in UpdateAttachmentList');
  DM.GetPrintableAttachmentList(qmftWorkOrder, FWOID, AttachmentList.Items);
  SetCheckListBox(AttachmentList, IncludeAttachments.Checked);
  ShowCheckListboxHorzScrollbars(AttachmentList);
end;

procedure TWorkOrderDetailReportForm.WorkOrderNumberEditButtonClick(
  Sender: TObject; AbsoluteIndex: Integer);
var
  SearchForm: TSearchForm;
  WOID: string;
begin
  SearchForm := TSearchForm.CreateWithCriteria(nil, TWorkOrderSearchCriteria);
  try
    if Trim(WorkOrderNumberEdit.Text) <> '' then
      (SearchForm.Criteria as TWorkOrderSearchCriteria).WorkOrderNumber.Text :=
        Trim(WorkOrderNumberEdit.Text);
    if SearchForm.ShowModal = mrOK then begin
      WOID := SearchForm.GetSelectedFieldValue('wo_id');
      if WOID <> '' then begin
        FWOID := StrToInt(WOID);
        DM.UpdateWorkOrderInCache(FWOID);
        UpdateWorkOrderFields;
      end;
    end;
  finally
    FreeAndNil(SearchForm);
  end;
end;

end.
