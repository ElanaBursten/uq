inherited EmailedHPTicketsReportForm: TEmailedHPTicketsReportForm
  Left = 541
  Top = 295
  Caption = 'EmailedHPTicketsReportForm'
  ClientHeight = 115
  ClientWidth = 360
  PixelsPerInch = 96
  TextHeight = 13
  object CallCenterLabel: TLabel [0]
    Left = 0
    Top = 10
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object CallCenterComboBox: TComboBox [1]
    Left = 72
    Top = 8
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
  end
end
