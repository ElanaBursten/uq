unit MessageMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ComCtrls, MessageSend, BaseSearchForm, DMu,
  QMConst, MessageDetail;

type
  TMessageMainForm = class(TEmbeddableForm)
    MessageTabControl: TTabControl;
    procedure MessageTabControlChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    VisibleForm: TEmbeddableForm;
    MessageSendForm: TMessageSendForm;
    MessageReviewForm: TSearchForm;
    MessageDetailForm: TMessageDetailForm;
    procedure PrepareSendMessageForm;
    procedure MessageReviewSearch;
    procedure SendNewMessage;
    procedure SetupFormControls;
    procedure MessageSelected(Sender: TObject; ID: Integer);
    procedure MessageDetailDone(Sender: TObject);
    procedure LeaveVisibleForm;
  public
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
  end;

implementation

uses
  MessageSearchHeader, LocalPermissionsDMu;

{$R *.dfm}

procedure TMessageMainForm.ActivatingNow;
begin
  inherited;
  SetupFormControls;
end;

procedure TMessageMainForm.SetupFormControls;
begin
  MessageTabControl.Tabs.Clear;
  if PermissionsDM.CanI(RightMessagesReview) then
    MessageTabControl.Tabs.Add('Review Messages');
  if PermissionsDM.CanI(RightMessagesSend) then
    MessageTabControl.Tabs.Add('Send Messages');
  if MessageTabControl.Tabs.Count > 0 then begin
    MessageTabControl.TabIndex := 0;
    MessageTabControlChange(Self);
  end;
end;

procedure TMessageMainForm.MessageTabControlChange(Sender: TObject);
var
  TabCaption: string;
begin
  inherited;
  if MessageTabControl.TabIndex < 0 then
    Exit;

  LeaveVisibleForm;
  TabCaption := MessageTabControl.Tabs[MessageTabControl.TabIndex];
  if SameText(TabCaption, 'Review Messages') then
    MessageReviewSearch
  else
    SendNewMessage;
end;


procedure TMessageMainForm.SendNewMessage;
begin
  PrepareSendMessageForm;
  MessageSendForm.Show;
  MessageSendForm.ActivatingNow;
  VisibleForm := MessageSendForm;
end;

procedure TMessageMainForm.MessageReviewSearch;
begin
  if not Assigned(MessageReviewForm) then begin
    MessageReviewForm := (CreateEmbeddedForm(TSearchForm, Self, MessageTabControl) as TSearchForm);
    MessageReviewForm.CriteriaClass := TMessageSearchCriteria;
    MessageReviewForm.OnItemSelected := MessageSelected;
  end;
  MessageReviewForm.Show;
  VisibleForm := MessageReviewForm;
end;

procedure TMessageMainForm.PrepareSendMessageForm;
begin
  if not Assigned(MessageSendForm) then
    MessageSendForm := CreateEmbeddedForm(TMessageSendForm, Self,
      MessageTabControl) as TMessageSendForm;

  Assert(Assigned(MessageSendForm));
end;

procedure TMessageMainForm.FormShow(Sender: TObject);
begin
  inherited;
  if Assigned(MessageReviewForm) then
    TMessageSearchCriteria(MessageReviewForm.Criteria).Showing;
end;

procedure TMessageMainForm.MessageSelected(Sender: TObject; ID: Integer);
begin
  if Assigned(MessageDetailForm) = False then
    MessageDetailForm := CreateEmbeddedForm(TMessageDetailForm, Self,
      MessageTabControl) as TMessageDetailForm;
  Assert(Assigned(MessageDetailForm));
  MessageReviewForm.LeavingNow;
  MessageReviewForm.Hide;
  MessageDetailForm.OnDone := MessageDetailDone;
  MessageDetailForm.ShowMessageDetail(ID);
  VisibleForm := MessageDetailForm;
end;

procedure TMessageMainForm.MessageDetailDone(Sender: TObject);
begin
  MessageDetailForm.LeavingNow;
  MessageDetailForm.Hide;
  MessageReviewSearch;
end;

procedure TMessageMainForm.LeavingNow;
begin
  LeaveVisibleForm;
  inherited;
end;

procedure TMessageMainForm.LeaveVisibleForm;
begin
  if Assigned(VisibleForm) then
    VisibleForm.LeavingNow;
end;

end.
