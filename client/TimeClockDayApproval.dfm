inherited TimeClockDayApprovalFrame: TTimeClockDayApprovalFrame
  Height = 735
  inherited BelowCalloutPanel: TPanel
    inherited BelowCalloutTopPanel: TPanel
      object WorkHoursLabel: TLabel
        Left = 2
        Top = 1
        Width = 38
        Height = 13
        Caption = 'Wk Tot:'
      end
      object CalloutHoursLabel: TLabel
        Left = 2
        Top = 19
        Width = 36
        Height = 13
        Caption = 'Co Tot:'
      end
      object RegularHoursLabel: TLabel
        Left = 2
        Top = 38
        Width = 23
        Height = 13
        Caption = 'Reg:'
      end
      object OvertimeHoursLabel: TLabel
        Left = 2
        Top = 57
        Width = 18
        Height = 13
        Caption = 'OT:'
      end
      object DoubletimeHoursLabel: TLabel
        Left = 2
        Top = 76
        Width = 13
        Height = 13
        Caption = 'DT'
      end
    end
    inherited LeaveVacPanel: TPanel
      object LeaveHoursLabel: TLabel [0]
        Left = 2
        Top = 6
        Width = 33
        Height = 13
        Caption = 'Leave:'
      end
      object VacationHoursLabel: TLabel [1]
        Left = 2
        Top = 26
        Width = 27
        Height = 13
        Caption = 'Vacn:'
      end
      inherited VacationHours: TcxDBCurrencyEdit
        Top = 23
      end
      inherited SickLeaveHours: TcxDBComboBox
        Top = 3
      end
    end
    inherited PTOPanel: TPanel
      object PTOHoursLabel: TLabel [0]
        Left = 2
        Top = 4
        Width = 24
        Height = 13
        Caption = 'PTO:'
      end
    end
    inherited BelowCalloutBottomPanel: TPanel
      Align = alTop
      inherited DayHours: TLabel
        Left = 92
        Top = 104
      end
      object BereavementHoursLabel: TLabel [1]
        Left = 2
        Top = 3
        Width = 38
        Height = 13
        Caption = 'Bereav:'
      end
      object HolidayLabel: TLabel [2]
        Left = 2
        Top = 22
        Width = 39
        Height = 13
        Caption = 'Holiday:'
      end
      object JuryDutyHoursLabel: TLabel [3]
        Left = 2
        Top = 41
        Width = 25
        Height = 13
        Caption = 'Jury:'
      end
      object DayHoursLabel: TLabel [4]
        Left = 2
        Top = 104
        Width = 28
        Height = 13
        Caption = 'Total:'
      end
      object EmployeeTypeLabel: TLabel [5]
        Left = 2
        Top = 82
        Width = 28
        Height = 13
        Caption = 'Type:'
      end
      object VehicleUseLabel: TLabel [6]
        Left = 2
        Top = 123
        Width = 37
        Height = 13
        Caption = 'Vehicle:'
      end
      object MilesStart1Label: TLabel [7]
        Left = 2
        Top = 144
        Width = 27
        Height = 13
        Caption = 'Miles:'
      end
      object FloatingHolidayLabel: TLabel [8]
        Left = 2
        Top = 61
        Width = 39
        Height = 13
        Caption = 'Float-H:'
      end
      object ApprovalMemoLabel: TLabel [9]
        Left = 1
        Top = 185
        Width = 27
        Height = 13
        Caption = 'Aprv:'
      end
      inherited ApprovalMemo: TMemo
        Left = 43
        Top = 186
      end
      inherited BereavementHours: TcxDBCurrencyEdit [11]
      end
      inherited HolidayHours: TcxDBCurrencyEdit [12]
      end
      inherited JuryDutyHours: TcxDBCurrencyEdit [13]
      end
      inherited MilesStart1: TcxDBCurrencyEdit [14]
      end
      inherited MilesStop1: TcxDBCurrencyEdit [15]
      end
      inherited FloatingHoliday: TDBCheckBox [16]
        Width = 46
        Visible = True
        OnClick = nil
      end
      inherited VehicleUse: TcxDBComboBox [17]
      end
      inherited EmployeeType: TComboBox [18]
        Top = 79
      end
      inherited ReasonChanged: TcxComboBox [19]
        Properties.OnChange = nil
      end
      inherited SubmitButton: TButton [20]
        Left = 52
        Top = 186
      end
    end
  end
  object LegendGroup: TGroupBox [4]
    Left = 3
    Top = 511
    Width = 131
    Height = 144
    Caption = 'Legend'
    TabOrder = 3
    object NotEnteredShape: TShape
      Left = 8
      Top = 16
      Width = 13
      Height = 13
    end
    object NotSubmittedShape: TShape
      Left = 8
      Top = 31
      Width = 13
      Height = 13
    end
    object NotEnteredLabel: TLabel
      Left = 27
      Top = 16
      Width = 85
      Height = 13
      AutoSize = False
      Caption = 'Not entered'
    end
    object NotSubmittedLabel: TLabel
      Left = 27
      Top = 31
      Width = 85
      Height = 13
      AutoSize = False
      Caption = 'Not submitted'
    end
    object ApprovedLabel: TLabel
      Left = 27
      Top = 61
      Width = 85
      Height = 13
      AutoSize = False
      Caption = 'Approved'
    end
    object ApprovedShape: TShape
      Left = 8
      Top = 61
      Width = 13
      Height = 13
    end
    object FinalApprovedLabel: TLabel
      Left = 27
      Top = 76
      Width = 85
      Height = 13
      AutoSize = False
      Caption = 'Final approved'
      Color = clBtnFace
      ParentColor = False
    end
    object FinalApprovedShape: TShape
      Left = 8
      Top = 76
      Width = 13
      Height = 13
    end
    object SubmittedShape: TShape
      Left = 8
      Top = 46
      Width = 13
      Height = 13
    end
    object SubmittedLabel: TLabel
      Left = 27
      Top = 46
      Width = 85
      Height = 13
      AutoSize = False
      Caption = 'Submitted'
    end
    object UTAEnteredLabel: TLabel
      Left = 27
      Top = 91
      Width = 61
      Height = 13
      Caption = 'UTA entered'
    end
    object UTAEnteredShape: TShape
      Left = 8
      Top = 91
      Width = 13
      Height = 13
    end
    object RTASQEnteredLabel: TLabel
      Left = 27
      Top = 106
      Width = 75
      Height = 13
      Caption = 'RTASQ entered'
    end
    object RTASQEnteredShape: TShape
      Left = 8
      Top = 106
      Width = 13
      Height = 13
    end
    object TSEEnteredShape: TShape
      Left = 8
      Top = 122
      Width = 13
      Height = 13
    end
    object TSEEnteredLabel: TLabel
      Left = 27
      Top = 122
      Width = 59
      Height = 13
      Caption = 'TSE Entered'
    end
  end
  inherited TimeClockGrid: TcxGrid
    Height = 121
    TabOrder = 4
    inherited TimeClockGridDBTableView: TcxGridDBTableView
      OptionsView.NoDataToDisplayInfoText = ''
      OptionsView.ShowEditButtons = gsebNever
      Styles.Background = TimeClockGridContentROStyle
      Styles.Content = TimeClockGridContentROStyle
    end
  end
  inherited DayActions: TActionList
    Left = 141
    Top = 179
  end
  inherited DefaultEditStyleController: TcxDefaultEditStyleController
    PixelsPerInch = 96
  end
  inherited TimeClockStyleRepo: TcxStyleRepository
    PixelsPerInch = 96
  end
end
