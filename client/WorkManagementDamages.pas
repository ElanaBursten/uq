unit WorkManagementDamages;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableWorkMgtGridFrame, cxGraphics, cxControls, cxLookAndFeels,
  cxGridTableView, cxLookAndFeelPainters, cxStyles, cxClasses, cxGridLevel, cxGrid,
  StdCtrls, ExtCtrls, OdDbUtils,
  cxCustomData, cxDataStorage, cxEdit, DB, cxDBData, WorkManageUtils,
  cxGridCustomTableView, cxGridDBTableView, cxGridCustomView, dbisamtb, OdDBISAMUtils,
  cxNavigator, DBCtrls, ActnList, Menus, OdContainer, cxFilter,
  cxData;

type
  TWorkMgtDamagesFrame = class(TExpandableWorkMgtGridFrameBase)
    DamageGridViewRepository: TcxGridViewRepository;
    DamageView: TcxGridDBTableView;
    DamageListSource: TDataSource;
    ColDamageDamageID: TcxGridDBColumn;
    ColDamageDamageType: TcxGridDBColumn;
    ColDamageDamageDate: TcxGridDBColumn;
    ColDamageLocation: TcxGridDBColumn;                                           
    ColDamageCity: TcxGridDBColumn;
    ColDamageCounty: TcxGridDBColumn;
    ColDamageState: TcxGridDBColumn;
    ColDamageUtilityCoDamaged: TcxGridDBColumn;
    ColDamageExcavator: TcxGridDBColumn;
    ColDamageProfitCenter: TcxGridDBColumn;
    ColDamageDueDate: TcxGridDBColumn;
    ColDamageClosedDate: TcxGridDBColumn;
    ColDamageFacilityType: TcxGridDBColumn;
    ColDamageFacilitySize: TcxGridDBColumn;
    ColDamageFacilityMaterial: TcxGridDBColumn;
    ColDamageUQDamageID: TcxGridDBColumn;
    ColDamageModifiedDate: TcxGridDBColumn;
    ColDamagePriority: TcxGridDBColumn;
    Label25: TLabel;
    Label28: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DSumUQDamageID: TDBText;
    DSumDamageType: TDBText;
    DSumAddress: TDBText;
    DSumWorkPriority: TDBText;
    Label20: TLabel;
    DSumDamageDate: TDBText;
    Label23: TLabel;
    DSumDueDate: TDBText;
    DSumUtilityCo: TDBText;
    DSumFacilityType: TDBText;
    DSumFacilitySize: TDBText;
    Label24: TLabel;
    DSumCity: TDBText;
    Label26: TLabel;
    DSumCounty: TDBText;
    Label27: TLabel;
    DSumExcavator: TDBText;
    DSumState: TDBText;
    Label21: TLabel;
    DSumClosedDate: TDBText;
    Label22: TLabel;
    DSumFacilityMaterial: TDBText;
    RefreshDamageButton: TButton;
    DamagePriorityCombo: TComboBox;
    DamageSource: TDataSource;
    cxStyleRepoTickets: TcxStyleRepository;
    BottomGridStyle: TcxStyle;
    GridSelectionStyle: TcxStyle;
    UnfocusedGridSelStyle: TcxStyle;
    procedure DamageViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure DamageViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DamageListAfterOpen(DataSet: TDataSet);
    procedure RefreshDamageButtonClick(Sender: TObject);
    procedure DamagePriorityComboChange(Sender: TObject);
    procedure BaseGridEnter(Sender: TObject);
    procedure PriorityPopupPopup(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ItemDetailTableAfterOpen(DataSet: TDataSet);
    procedure ItemDetailTableCalcFields(DataSet: TDataSet);
    procedure HighPriorityActionExecute(Sender: TObject);
    procedure NormalPriorityActionExecute(Sender: TObject);
    procedure DamageViewCustomDrawIndicatorCell(Sender: TcxGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxCustomGridIndicatorItemViewInfo;
      var ADone: Boolean);
    procedure DamageViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DamageViewSelectionChanged(Sender: TcxCustomGridTableView);
  private
    FDamageColumns: TColumnList;
  protected
    function ItemIDColumn: TcxGridColumn; override;
    procedure SetGridAndViewEvents; override;
    procedure WMDestroy(var Msg: TWMDestroy); message WM_DESTROY;
 public
    PrevSelList: TIntegerList;  //QMANTWO-690 EB
    procedure RefreshCaptionCounts; override;

    procedure ReassignSelectedDamages(SelectedLocatorID, LocatorID: Integer);
    procedure CreateDamageListTable;
    function LoadDamageList(SelectedLocatorID, GetNumTicketDays: Integer;
      NodeIsUnderMe,ShowOpenWorkOnly: Boolean): Boolean;
    procedure UpdateFrameData(const SourceDataSet: TDataSet); override;
    procedure SetDamageDateFormats(DataSet: TDataSet);
    procedure ShowSelectedItem; override;
    procedure ShowDamage;
    procedure SetPriorityComboBox(PriorityCombo: TComboBox; PriorityID: Variant);
    procedure MarkPriority(PriorityRefID: Integer);
    procedure UpdateItemInCache; override;
    procedure CreateStructures;    //QMANTWO-690 EB
    procedure DestroyStructures;   //QMANTWO-690 EB
    procedure LoadSelList;         //QMANTWO-690 EB
    procedure MoveToDamageID(GoToDamageID: Integer); //QMANTWO-690 EB
  end;

var
  WorkMgtDamagesFrame: TWorkMgtDamagesFrame;

implementation

uses QMServerLibrary_Intf, DMu, QMConst, MSXML2_TLB, OdXmlToDataSet, UQUtils,
  ServiceLink, OdHourglass, OdVclUtils, OdExceptions, LocalEmployeeDmu, LocalDamageDMu;

{$R *.dfm}

{ TWorkMgtDamagesFrame }

procedure TWorkMgtDamagesFrame.RefreshCaptionCounts;
begin
  inherited;
{ If/when the Ticket Goals feature is implemented for Damages, make visible and set these labels:
  TodayCountLabelSummary.Caption :=
  TodayCountLabelDataSummary.Caption :=
  TomorrowCountLabelSummary.Caption :=
  TomorrowCountLabelDataSummary.Caption :=
  UnscheduledCountLabelSummary.Caption :=
  UnscheduledCountLabelDataSummary.Caption :=}
end;

procedure TWorkMgtDamagesFrame.UpdateItemInCache;
begin
  LDamageDM.UpdateDamageInCache(SelectedItemID);
end;

procedure TWorkMgtDamagesFrame.RefreshDamageButtonClick(Sender: TObject);
begin
  inherited;
  RefreshItem;
end;

procedure TWorkMgtDamagesFrame.SetGridAndViewEvents;
begin
  inherited;
  DamageView.OnDblClick := ViewDblClick;
  DamageView.OnFocusedRecordChanged := ViewFocusedRecordChanged;
  BaseGrid.OnEnter := BaseGridEnter;

  DamagePriorityCombo.OnChange := DamagePriorityComboChange;
  RefreshDamageButton.OnClick := RefreshDamageButtonClick;
end;

procedure TWorkMgtDamagesFrame.WMDestroy(var Msg: TWMDestroy);
begin
  if (csDestroying in ComponentState) then
    FreeAndNil(FDamageColumns); 
  inherited;
end;

procedure TWorkMgtDamagesFrame.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  DamagePriorityCombo.Enabled := HaveFocusedItem;
end;

procedure TWorkMgtDamagesFrame.BaseGridEnter(Sender: TObject);
begin
  inherited;
  GridState.InitialDisplay := False;  //QMANTWO-690 EB
end;

procedure TWorkMgtDamagesFrame.CreateDamageListTable;
begin
  FDamageColumns := TColumnList.Create;
  with FDamageColumns do begin
    SetDefaultTable('TMDamageList');
    AddColumn('DamageID', 55, 'damage_id', ftInteger, '', 0, True, False);
    AddColumn('ID', 55, 'uq_damage_id', ftInteger, '', 0);
    AddColumn('Type', 115, 'damage_type', ftString);
    AddColumn('Notified Date', 126, 'uq_notified_date', ftDateTime);
    AddColumn('Damage Date', 80, 'damage_date', ftDateTime);
    AddColumn('Due Date', 126, 'due_date', ftDateTime);
    AddColumn('Utility Co', 90, 'utility_co_damaged', ftString);
    AddColumn('Facility Type', 90, 'facility_type', ftString);
    AddColumn('Facility Size', 90, 'facility_size', ftString);
    AddColumn('Facility Material', 90, 'facility_material', ftString);
    AddColumn('Location', 120, 'location', ftString);
    AddColumn('City', 70, 'city', ftString);
    AddColumn('State', 32, 'state', ftString);
    AddColumn('County', 75, 'county', ftString);
    AddColumn('Excavator Co', 90, 'excavator_company', ftString);
    AddColumn('Ticket Number', 85, 'ticket_number', ftString);
    AddColumn('Profit Ctr', 55, 'profit_center', ftString);
    AddColumn('Investigator', 75, 'short_name', ftString);
    AddColumn('Visible', 20, 'damage_is_visible', ftBoolean);
    AddColumn('Priority', 100, 'work_priority', ftString);
    AddColumn('Sort', 20, 'work_priority_sort', ftInteger);
    AddColumn('Closed Date', 126, 'closed_date', ftDateTime);
    AddColumn('Modified Date', 126, 'modified_date', ftDateTime);
  end;
    if (DM.UQState.SecondaryDebugOn) and (DM.UQState.DebugWorkMgmtDebug) then    //QM-805 EB //QM-338 EB - Fields Available to view in Debug Mode
    ColDamageDamageID.VisibleForCustomization := True;
end;

procedure TWorkMgtDamagesFrame.CreateStructures;
begin
  PrevSelList := TIntegerList.Create;
  GridState.LastOpenedID := 0;
end;

procedure TWorkMgtDamagesFrame.DamageViewCellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  //The reason Showing[Ticket|WO|Damage]Summary checks are there is to prevent
  //excessive fetching of the ticket|damage|wo details from the server. If the
  //summary is already showing the selected row, then there is no need to sync
  //those details down again.
  if (not ShowingSummary) and (Assigned(Sender.Controller)) then 
    ViewFocusedRecordChanged(Sender, Sender.Controller.FocusedRecord, Sender.Controller.FocusedRecord, False);

  AHandled := False;
end;

procedure TWorkMgtDamagesFrame.DamageViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  Status: TDamageStatus;
  DueDate: TDateTime;
begin
  inherited;
  if not AViewInfo.Selected then begin
    if VarIsNull(AViewInfo.GridRecord.Values[ColDamageDueDate.Index]) then
      DueDate := 0
    else
      DueDate := (AViewInfo.GridRecord.Values[ColDamageDueDate.Index]);
    Status := GetDamageStatusFromValues(DueDate);
    ACanvas.Brush.Color := GetDamageColor(Status, (AViewInfo.GridRecord.Index mod 2) = 1);
  end;
  ADone := False;
end;

procedure TWorkMgtDamagesFrame.DamageViewCustomDrawIndicatorCell(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
begin
  inherited;
  {Grid Indicator highlighting}
  if (AViewInfo is TcxGridIndicatorRowItemViewInfo) then begin
     If TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Selected then begin
        ACanvas.Brush.Color := clYellow;
     end
     else if TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.focused then
       ACanvas.Brush.Color := clSkyBlue;
  end;
end;

procedure TWorkMgtDamagesFrame.DamageViewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  FocusedRec: TcxCustomGridRecord;
  ShouldBeSelected: boolean;
begin
  {Elana - Do no call this explicitly (it should only be called when the mouse is clicked on Grid)
           This is used instead of MouseDown so that it does not interfere with dragging
           tickets}  //QMANTWO-690 EB
 // inherited;
 ShouldBeSelected := False;  //QM-876 Eb Code Cleanup
 if not GridState.AmDragging then begin
   if HaveFocusedItem then begin
     FocusedRec := ListView.Controller.FocusedRecord;
     If not PrevSelList.Has(FocusedRec.Index) then
       ShouldBeSelected := True;
   end;

   if ((ssCtrl in Shift) or (ssShift in Shift)) then begin
     If (not DamageView.Controller.FocusedRecord.Selected) and ShouldBeSelected then
       DamageView.Controller.FocusedRecord.Selected := True;
   end
   else begin
     DamageView.Controller.ClearSelection;
     DamageView.Controller.FocusedRecord.Selected := True;
   end;
 end;
   GridState.CheckSelection := True;
end;

procedure TWorkMgtDamagesFrame.DamageViewSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  inherited;
  if (not GridState.InitialDisplay) and (GridState.LastDisplayedID <> GetSelectedItemID) then    //QMANTWO-690
    ShowSelectedItem;
end;

procedure TWorkMgtDamagesFrame.DestroyStructures;
begin
  FreeAndNil(PrevSelList);
end;

procedure TWorkMgtDamagesFrame.ShowDamage;
begin
  if (BaseGrid.Dragging) then
    Exit;

  DetailTable.DisableControls;
  try
    if SelectedItemID <> 0 then begin
      if HaveFocusedItem then begin
        // Hack: Use a RO call without a progress dialog to allow drag and drop without focus loss
        GetChan.ShowProgress := False;
        try
          UpdateItemInCache;
        finally
          GetChan.ShowProgress := True;
        end;
      end;

      AddCalculatedField('work_priority', DetailTable, TStringField, 20);
      DetailTable.Open;
      DetailTable.Refresh;
      DetailTable.SetRange([SelectedItemID], [SelectedItemID]);

      if HaveFocusedItem then begin
        // Update the GUI/grid in case any values just changed
        UpdateFrameData(DetailTable);
        SetPriorityComboBox(DamagePriorityCombo, DetailTable.FieldByName('work_priority_id').Value);
      end;
    end;
  finally
    DetailTable.EnableControls;
  end;
end;

procedure TWorkMgtDamagesFrame.ShowSelectedItem;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  ShowDamage;
  GridState.LastDisplayedID := GetSelectedItemID;   //QMANTWO-690  EB
  ClearSelectionsDBText; //QMANTWO-577 EB
end;

function TWorkMgtDamagesFrame.LoadDamageList(SelectedLocatorID,
  GetNumTicketDays: Integer; NodeIsUnderMe, ShowOpenWorkOnly: Boolean): Boolean;
var
  Doc: IXMLDomDocument;
begin
  FPopulatingWorkSummary := True;
  try
    if (SelectedLocatorID <= 0) or not NodeIsUnderMe then begin
      ClearListTable;
    end else begin
      Doc := GetDamageListFromServer(SelectedLocatorID, GetNumTicketDays, ShowOpenWorkOnly);

      BaseGrid.BeginUpdate;
      try
        ConvertXMLToDataSet(Doc, FDamageColumns, ListTable);
        if SelectedItemID = 0 then
          DamageView.DataController.GotoFirst
        else
          DamageView.DataController.LocateByKey(SelectedItemID);
      finally
        FBlockItemUpdates := True;
        try
          BaseGrid.EndUpdate;
        finally
          FBlockItemUpdates := False;
        end;
      end;
    end;
  finally
    FPopulatingWorkSummary := False;
    if DamageView.DataController.RecordCount > 0 then begin   //QM-338 EB
      DamageView.OptionsCustomize.ColumnsQuickCustomization := True;
      BaseGrid.Enabled := True;
    end
    else begin
      DamageView.OptionsCustomize.ColumnsQuickCustomization := False;
      BaseGrid.Enabled := False;
    end;
  end;

  //Close the detail dataset in preparation for selecting first row
  DetailTable.Close;
  Result := HasRecords(ListTable);
  if Result then begin
    SelectFirstGridRow;
    GridState.InitialDisplay := True;
  end;
end;

procedure TWorkMgtDamagesFrame.LoadSelList;
var
  SelRecord: TcxCustomGridRecord;
  i : integer;
begin
  {EB QMANTWO-690 Multi-Select Tweak}
    PrevSelList.Clear;
    for i := 0 to DamageView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := DamageView.Controller.SelectedRecords[i];
      PrevSelList.Add(SelRecord.Index);
    end;
end;

procedure TWorkMgtDamagesFrame.DamagePriorityComboChange(Sender: TObject);
begin
  inherited;
  MarkPriority(GetComboObjectInteger(DamagePriorityCombo, True));
end;

procedure TWorkMgtDamagesFrame.SetPriorityComboBox(PriorityCombo: TComboBox;
  PriorityID: Variant);
begin
  Assert(Assigned(PriorityCombo));
  DM.GetRefLookupList('tkpriority', PriorityCombo.Items, True);    //QM-933 EB SortBy
  PriorityCombo.Items.Delete(PriorityCombo.Items.IndexOf('Release for Work')); //"Release for Work" not supported for Damages at this time.

  if PriorityID = Null then
    PriorityCombo.ItemIndex := PriorityCombo.Items.IndexOf('Normal')
  else
    SelectComboBoxItemFromObjects(PriorityCombo, PriorityID);
end;

procedure TWorkMgtDamagesFrame.MarkPriority(PriorityRefID: Integer);
begin
  if SelectedItemID <> 0 then begin
    LDamageDM.SetDamagePriority(SelectedItemID, PriorityRefID);
    RefreshItem;
  end else
    raise EOdDataEntryError.Create('Please select a damage before changing the priority.');
end;

procedure TWorkMgtDamagesFrame.MoveToDamageID(GoToDamageID: Integer);
var
  i: integer;
begin
  {QMANTWO-690 EB Allows us to move back to the last opened ticket}
  for i := 0 to DamageView.ViewData.RecordCount - 1 do
  If (GoToDamageID = DamageView.ViewData.Records[i].Values[ItemIDColumn.ID]) then begin
    DamageView.ViewData.Records[i].Focused := True;
    DamageView.ViewData.Records[i].Selected := True;
    Exit;
  end;
end;

procedure TWorkMgtDamagesFrame.HighPriorityActionExecute(Sender: TObject);
begin
  inherited;
  DamagePriorityCombo.ItemIndex := DamagePriorityCombo.Items.IndexOf('High');
  MarkPriority(GetComboObjectInteger(DamagePriorityCombo, True));
end;

procedure TWorkMgtDamagesFrame.NormalPriorityActionExecute(Sender: TObject);
begin
  inherited;
  DamagePriorityCombo.ItemIndex := DamagePriorityCombo.Items.IndexOf('Normal');
  MarkPriority(GetComboObjectInteger(DamagePriorityCombo, True));
end;

procedure TWorkMgtDamagesFrame.PriorityPopupPopup(Sender: TObject);
var
  I: Integer;
begin
  inherited;

  for I := 0 to PriorityPopup.Items.Count - 1 do begin
    if Assigned(PriorityPopup.Items[I].Action) then
      (PriorityPopup.Items[I].Action as TAction).Enabled := False;
  end;

  if SelectedTreeNodeType = ntTicketActivities then
    Exit;

  if HaveFocusedItem then begin
    if (not ShowingSummary) then
      ViewFocusedRecordChanged(DamageView, DamageView.Controller.FocusedRecord,
        DamageView.Controller.FocusedRecord, False);
    HighPriorityAction.Enabled := True;
    NormalPriorityAction.Enabled := True;
  end;
end;

procedure TWorkMgtDamagesFrame.ReassignSelectedDamages(SelectedLocatorID, LocatorID: Integer);
var
  SelRecord: TcxCustomGridRecord;
  i: Integer;
  Damages: QMServerLibrary_Intf.IntegerList;
  DamageID: Integer;
begin
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeMoveTicket, IntToStr(DamageView.Controller.SelectedRecordCount) + ' moved');
  Damages := QMServerLibrary_Intf.IntegerList.Create;
  for i := 0 to DamageView.Controller.SelectedRecordCount - 1 do begin
    SelRecord := DamageView.Controller.SelectedRecords[i];
    if SelRecord.IsData then begin
      DamageID := SelRecord.Values[ItemIDColumn.Index];
      if DamageID > 0 then
        Damages.Add(DamageID);
    end;
  end;

  if Damages.Count > 0 then begin
    DM.CallingServiceName('MoveDamages');
    DM.Engine.Service.MoveDamages(DM.Engine.SecurityInfo, Damages, SelectedLocatorID, LocatorID);
  end;
end;

function TWorkMgtDamagesFrame.ItemIDColumn: TcxGridColumn;
begin
  Result := ColDamageDamageID;
end;

procedure TWorkMgtDamagesFrame.UpdateFrameData(const SourceDataSet: TDataSet);
var
  i: Integer;
  FieldName: string;
begin
  Assert(ListTable.FieldByName('damage_id').AsInteger = SourceDataSet.FieldByName('damage_id').AsInteger);

  FBlockItemUpdates := True; 
  EditDataSet(ListTable);
  for i := 0 to ListTable.Fields.Count - 1 do begin
    FieldName := ListTable.Fields[i].FieldName;
    if Assigned(SourceDataSet.FindField(FieldName)) then
      ListTable.FieldByName(FieldName).Value := SourceDataSet.FieldByName(FieldName).Value;
  end;
  ListTable.Post; // Saves to memory only
  FBlockItemUpdates := False;
end;

procedure TWorkMgtDamagesFrame.ItemDetailTableAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetDamageDateFormats(DataSet);
end;

procedure TWorkMgtDamagesFrame.ItemDetailTableCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('work_priority').AsString := LDamageDM.GetDamagePriority(DataSet.FieldByName('damage_id').AsInteger);
end;

procedure TWorkMgtDamagesFrame.DamageListAfterOpen(DataSet: TDataSet);
begin
  SetDamageDateFormats(DataSet);
end;

procedure TWorkMgtDamagesFrame.SetDamageDateFormats(DataSet: TDataSet);
begin
  SetFieldDisplayFormat(DataSet, 'due_date', 'mmm d, yyyy t');
  SetFieldDisplayFormat(DataSet, 'damage_date', 'mmm d, yyyy');
  SetFieldDisplayFormat(DataSet, 'closed_date', 'mmm d, yyyy t');
end;

end.
