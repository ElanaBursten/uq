inherited DamageInvoiceFrame: TDamageInvoiceFrame
  Height = 492
  object FooterPanel: TPanel [0]
    Left = 0
    Top = 464
    Width = 669
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
    DesignSize = (
      669
      28)
    object InvoiceBalance: TLabel
      Left = 8
      Top = 8
      Width = 121
      Height = 13
      Caption = 'Damage Invoice Balance:'
    end
    object ViewAllCk: TCheckBox
      Left = 540
      Top = 5
      Width = 129
      Height = 17
      Anchors = [akTop, akRight]
      Caption = 'View All Invoices'
      TabOrder = 0
      OnClick = ViewAllCkClick
    end
  end
  inherited EditInvoicePanel: TPanel
    Height = 317
    TabOrder = 1
    object AccrualCodeLabel: TLabel [0]
      Left = 390
      Top = 270
      Width = 67
      Height = 13
      Caption = 'Accrual Code:'
    end
    inherited InvoiceGroup: TGroupBox
      Height = 190
      inherited AmountLabel: TLabel
        Top = 66
        Width = 79
        Alignment = taLeftJustify
        Caption = 'Invoice Amount:'
      end
      inherited InvoiceCompanyLabel: TLabel
        Left = 14
        Top = 42
        Width = 87
        Alignment = taLeftJustify
        Caption = 'Invoice Company:'
      end
      inherited InvoiceNumberLabel: TLabel
        Left = 14
        Top = 17
        Width = 79
        Alignment = taLeftJustify
        Caption = 'Invoice Number:'
      end
      inherited ReceivedDateLabel: TLabel
        Left = 14
        Top = 114
        Width = 74
        Alignment = taLeftJustify
        Caption = 'Received Date:'
      end
      inherited CommentsLabel: TLabel
        Left = 14
        Top = 162
        Width = 54
        Alignment = taLeftJustify
        Caption = 'Comments:'
      end
      inherited InvoiceDateLabel: TLabel
        Left = 14
        Top = 90
        Width = 65
        Alignment = taLeftJustify
        Caption = 'Invoice Date:'
      end
      object ClaimantLabel: TLabel [6]
        Left = 14
        Top = 139
        Width = 45
        Height = 13
        Caption = 'Claimant:'
        Visible = False
      end
      inherited InvoiceNumber: TDBEdit
        Top = 13
      end
      inherited Comments: TDBEdit
        Top = 159
        TabOrder = 6
      end
      object Claimant: TComboBox
        Left = 104
        Top = 135
        Width = 156
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 5
        Visible = False
      end
    end
    inherited PaymentGroup: TGroupBox
      Height = 190
      inherited ApprovedDateLabel: TLabel
        Left = 13
        Top = 42
        Width = 77
        Alignment = taLeftJustify
        Caption = 'Approved Date:'
      end
      inherited AmountApprovedLabel: TLabel
        Left = 13
        Top = 17
        Width = 91
        Alignment = taLeftJustify
        Caption = 'Amount Approved:'
      end
      inherited PaidDateLabel: TLabel
        Left = 13
        Top = 114
        Width = 50
        Alignment = taLeftJustify
        Caption = 'Paid Date:'
      end
      inherited AmountPaidLabel: TLabel
        Left = 13
        Top = 90
        Width = 64
        Alignment = taLeftJustify
        Caption = 'Amount Paid:'
      end
      inherited SatisfiedLabel: TLabel
        Left = 13
        Top = 129
        Width = 112
        Height = 39
        Alignment = taLeftJustify
        Caption = 'Payment Completely Satisfies the Damage:'
      end
      inherited InvoiceApprovedLabel: TLabel
        Left = 13
        Top = 158
        Width = 91
        Height = 39
        Alignment = taLeftJustify
        Caption = 'Approved For Payment:'
      end
      object PaymentCodeLabel: TLabel [6]
        Left = 13
        Top = 66
        Width = 74
        Height = 13
        Caption = 'Payment Code:'
        Visible = False
      end
      inherited AmountPaid: TDBEdit
        Top = 87
        TabOrder = 3
      end
      inherited PaidDate: TcxDBDateEdit
        Top = 111
        TabOrder = 4
      end
      inherited Satisfied: TDBCheckBox
        Top = 134
        TabOrder = 5
      end
      inherited InvoiceApproved: TDBCheckBox
        Top = 161
        TabOrder = 6
      end
      object PaymentCode: TDBComboBox
        Left = 124
        Top = 63
        Width = 156
        Height = 21
        Style = csDropDownList
        DataField = 'payment_code'
        DataSource = InvoiceSource
        ItemHeight = 13
        TabOrder = 2
        Visible = False
      end
    end
    inherited DamageGroup: TGroupBox
      Top = 192
      inherited DamageIDLabel: TLabel
        Left = 14
        Width = 57
        Alignment = taLeftJustify
        Caption = 'Damage ID:'
      end
      inherited DamageStateLabel: TLabel
        Left = 14
        Top = 44
        Width = 72
        Alignment = taLeftJustify
        Caption = 'Damage State:'
      end
      inherited DamageCityLabel: TLabel
        Left = 14
        Top = 19
        Width = 65
        Alignment = taLeftJustify
        Caption = 'Damage City:'
      end
      inherited DamageDateLabel: TLabel
        Left = 14
        Top = 70
        Width = 69
        Alignment = taLeftJustify
        Caption = 'Damage Date:'
      end
    end
    inherited PacketGroup: TGroupBox
      Top = 192
      inherited Label1: TLabel
        Left = 13
        Width = 91
        Alignment = taLeftJustify
        Caption = 'Packet Requested:'
      end
      inherited Label3: TLabel
        Left = 13
        Top = 44
        Width = 83
        Alignment = taLeftJustify
        Caption = 'Packet Received:'
      end
    end
    inherited SaveButton: TButton
      Top = 290
      TabOrder = 5
    end
    inherited CancelButton: TButton
      Top = 290
      TabOrder = 6
    end
    inherited InsertButton: TButton
      Top = 290
      TabOrder = 7
    end
    object AccrualCode: TComboBox
      Left = 501
      Top = 266
      Width = 156
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
    end
  end
  object InvoiceGrid: TcxGrid [2]
    Left = 0
    Top = 317
    Width = 669
    Height = 147
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object InvoiceView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = InvoiceSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'invoice_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ColInvoiceType: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'invoice_type_desc'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 30
      end
      object ColInvoiceID: TcxGridDBColumn
        Caption = 'Invoice ID'
        DataBinding.FieldName = 'invoice_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Filtering = False
        Width = 55
      end
      object ColInvoiceNum: TcxGridDBColumn
        Caption = 'Invoice #'
        DataBinding.FieldName = 'invoice_num'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 85
      end
      object ColReceivedDate: TcxGridDBColumn
        Caption = 'Recv Date'
        DataBinding.FieldName = 'received_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Width = 65
      end
      object ColClaimantID: TcxGridDBColumn
        Caption = 'Claimant ID'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object ColClaimant: TcxGridDBColumn
        Caption = 'Claimant'
        DataBinding.FieldName = 'claimant_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 99
      end
      object ColCompany: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'company'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 138
      end
      object ColAmount: TcxGridDBColumn
        Caption = 'Amount'
        DataBinding.FieldName = 'amount'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DecimalPlaces = 2
        Properties.DisplayFormat = '$,0.00;-$,0.00'
        Properties.Nullable = False
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 70
      end
      object ColPaidDate: TcxGridDBColumn
        Caption = 'Paid Date'
        DataBinding.FieldName = 'paid_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Width = 76
      end
      object ColPaidAmount: TcxGridDBColumn
        Caption = 'Amount Paid'
        DataBinding.FieldName = 'paid_amount'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 70
      end
    end
    object InvoiceLevel: TcxGridLevel
      GridView = InvoiceView
    end
  end
end
