object frmTimeSheetOverride: TfrmTimeSheetOverride
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Override Start Time'
  ClientHeight = 215
  ClientWidth = 247
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 59
    Width = 40
    Height = 13
    Caption = 'Reason:'
  end
  object Label2: TLabel
    Left = 32
    Top = 114
    Width = 32
    Height = 13
    Caption = 'Other:'
  end
  object Label3: TLabel
    Left = 35
    Top = 10
    Width = 50
    Height = 13
    Caption = 'New Time:'
  end
  object lblLength: TLabel
    Left = 32
    Top = 152
    Width = 185
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = cl3DDkShadow
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object edtOther: TEdit
    Left = 32
    Top = 128
    Width = 185
    Height = 21
    Enabled = False
    MaxLength = 50
    TabOrder = 1
    OnKeyUp = edtOtherKeyUp
  end
  object btnClose: TBitBtn
    Left = 44
    Top = 176
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    NumGlyphs = 2
  end
  object btnCancel: TBitBtn
    Left = 125
    Top = 176
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    OnClick = btnCancelClick
    NumGlyphs = 2
  end
  object cbReason: TComboBox
    Left = 32
    Top = 72
    Width = 185
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnCloseUp = cbReasonCloseUp
  end
  object cxTimeEdit1: TcxTimeEdit
    Left = 35
    Top = 24
    EditValue = 0d
    ParentFont = False
    Properties.ClearKey = 16449
    Properties.TimeFormat = tfHourMin
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 4
    Width = 110
  end
  object orReasons: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      'select description'
      'from reference'
      'where type = '#39'timesheet'#39
      'and'
      'code = '#39'OR_REASON'#39
      'and active_ind')
    Params = <>
    Left = 184
    Top = 8
  end
end
