inherited MyTicketsForm: TMyTicketsForm
  Left = 555
  Top = 280
  Caption = 'My Work'
  ClientHeight = 567
  ClientWidth = 910
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 910
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      910
      37)
    object ViewLabel: TLabel
      Left = 18
      Top = 12
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'View'
    end
    object OpenTicketsLabel: TLabel
      Left = 469
      Top = 12
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Open Tickets'
      Visible = False
    end
    object DueTodayLabel: TLabel
      Left = 591
      Top = 12
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Due Today'
      Visible = False
    end
    object DueTomorrowLabel: TLabel
      Left = 701
      Top = 12
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Due Tomorrow'
      Visible = False
    end
    object ViewStyleCombo: TComboBox
      Left = 46
      Top = 8
      Width = 121
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = ViewStyleComboChange
    end
    object ApplyButton: TButton
      Left = 173
      Top = 6
      Width = 156
      Height = 25
      Caption = 'Apply Quick Close / Refresh'
      TabOrder = 1
      OnClick = ApplyButtonClick
    end
    object ClearButton: TButton
      Left = 330
      Top = 6
      Width = 88
      Height = 25
      Caption = 'Clear Routes'
      TabOrder = 2
      OnClick = ClearButtonClick
    end
    object OpenTicketCount: TDBEdit
      Left = 538
      Top = 8
      Width = 41
      Height = 21
      TabStop = False
      DataField = 'tickets_open'
      DataSource = CenterTicketSummarySource
      ParentColor = True
      ReadOnly = True
      TabOrder = 3
      Visible = False
    end
    object DueTodayCount: TDBEdit
      Left = 650
      Top = 8
      Width = 41
      Height = 21
      TabStop = False
      DataField = 'tickets_due_today'
      DataSource = CenterTicketSummarySource
      ParentColor = True
      ReadOnly = True
      TabOrder = 4
      Visible = False
    end
    object DueTomorrowCount: TDBEdit
      Left = 777
      Top = 8
      Width = 41
      Height = 21
      TabStop = False
      DataField = 'tickets_due_tomorrow'
      DataSource = CenterTicketSummarySource
      ParentColor = True
      ReadOnly = True
      TabOrder = 5
      Visible = False
    end
    object ExportButton: TButton
      Left = 419
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Export'
      TabOrder = 6
      OnClick = ExportButtonClick
    end
    object SelectAllButton: TButton
      Left = 738
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Select All'
      TabOrder = 7
      OnClick = SelectAllButtonClick
    end
    object SelectNoneButton: TButton
      Left = 815
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Select None'
      TabOrder = 8
      OnClick = SelectNoneButtonClick
    end
  end
  object MyWorkScrollbox: TScrollBox
    Left = 0
    Top = 37
    Width = 910
    Height = 530
    HorzScrollBar.Visible = False
    Align = alClient
    TabOrder = 1
    OnMouseWheel = MyWorkScrollboxMouseWheel
    object WorkOrdersGrid: TcxGrid
      Left = 0
      Top = 0
      Width = 889
      Height = 154
      Align = alTop
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object WorkOrdersGridLevelDetail: TcxGridLevel
        GridView = WorkOrdersGridDetailsTableView
      end
      object WorkOrdersGridLevelSummary: TcxGridLevel
        GridView = WorkOrdersGridSummaryTableView
      end
    end
    object TicketsGrid: TcxGrid
      Left = 0
      Top = 154
      Width = 889
      Height = 220
      Align = alTop
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object TicketsGridLevelDetail: TcxGridLevel
        GridView = TicketsGridDetailsTableView
      end
      object TicketsGridLevelSummary: TcxGridLevel
        GridView = TicketsGridSummaryTableView
      end
    end
    object DamagesGrid: TcxGrid
      Left = 0
      Top = 374
      Width = 889
      Height = 154
      Align = alTop
      TabOrder = 2
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object DamagesGridLevelDetail: TcxGridLevel
        GridView = DamagesGridDetailsTableView
      end
      object DamagesGridLevelSummary: TcxGridLevel
        GridView = DamagesGridSummaryTableView
      end
    end
  end
  object TicketLocates: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    IndexName = 'ticket_id'
    MasterFields = 'ticket_id'
    TableName = 'locate'
    Left = 80
    Top = 271
  end
  object OpenTickets: TDBISAMQuery
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    BeforeOpen = OpenTicketsBeforeOpen
    AfterScroll = OpenTicketsAfterScroll
    OnCalcFields = OpenTicketsCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      '/* REPLACED AT RUNTIME */'
      
        'select ticket_id, ticket_number, kind, due_date, ticket_type, co' +
        'mpany,'
      
        '  priority, work_type, work_city, work_address_street, work_coun' +
        'ty,'
      '  work_description, legal_good_thru, map_page, ticket_format, '
      
        '  work_address_number, do_not_mark_before, work_date, LocalStrin' +
        'gKey, alert,'
      
        '  coalesce(ref.sortby, 0) work_priority, coalesce(ref.descriptio' +
        'n, '#39'Normal'#39') priority_disp,'
      '  if(task.est_start_date is null, False, True) first_task,'
      '  0 export'
      
        'from ticket inner join locate on ticket.ticket_id=locate.ticket_' +
        'id'
      'left join reference ref on ticket.work_priority_id=ref.ref_id'
      
        'left join task_schedule task on ticket.ticket_id = task.ticket_i' +
        'd'
      'where locate.locator_id=:emp_id'
      '  and locate.active'
      '  and NOT locate.closed'
      ' and ((do_not_mark_before is NULL)'
      '   or (do_not_mark_before <= :last_sync))'
      
        'group by ticket_id, ticket_number, kind, due_date, ticket_type, ' +
        'company,'
      
        '  priority, work_type, work_city, work_address_street, work_coun' +
        'ty,'
      '  legal_good_thru, map_page, ticket_format, '
      '  work_address_number, do_not_mark_before, work_priority'
      'order by work_priority, kind, due_date, ticket_number')
    Params = <
      item
        DataType = ftInteger
        Name = 'emp_id'
        Value = 206
      end
      item
        DataType = ftDateTime
        Name = 'last_sync'
        Value = 0d
      end>
    Left = 48
    Top = 271
    ParamData = <
      item
        DataType = ftInteger
        Name = 'emp_id'
        Value = 206
      end
      item
        DataType = ftDateTime
        Name = 'last_sync'
        Value = 0d
      end>
  end
  object TicketSource: TDataSource
    DataSet = OpenTickets
    Left = 16
    Top = 271
  end
  object ClearRoutes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'update ticket'
      'set LocalStringKey = null')
    Params = <>
    Left = 16
    Top = 307
  end
  object SetRoute: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'update ticket'
      'set LocalStringKey = :Route'
      'where ticket_id = :TicketID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'Route'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
    Left = 48
    Top = 307
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Route'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
  end
  object CenterTicketSummary: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'center_ticket_summary'
    Left = 80
    Top = 307
  end
  object CenterTicketSummarySource: TDataSource
    DataSet = CenterTicketSummary
    Left = 112
    Top = 307
  end
  object DamageSource: TDataSource
    DataSet = OpenDamages
    Left = 22
    Top = 512
  end
  object OpenDamages: TDBISAMQuery
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'select damage_id, uq_damage_id, damage_date, damage.due_date, da' +
        'mage_type, t.ticket_number, profit_center, '
      
        '  utility_co_damaged, location, city, county, state, facility_ty' +
        'pe, facility_size, excavator_company,'
      
        '  coalesce(ref.sortby, 0) work_priority_id, coalesce(ref.descrip' +
        'tion, '#39'Normal'#39') work_priority'
      'from damage'
      '  left join reference ref on damage.work_priority_id= ref.ref_id'
      '  left join ticket t on damage.ticket_id = t.ticket_id'
      '  where investigator_id = :emp_id'
      '  and damage_type NOT in ('#39'APPROVED'#39')'
      '  and damage.active = 1'
      'order by work_priority, due_date')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 51
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object MyWorkViewRepository: TcxGridViewRepository
    Left = 304
    Top = 112
    object DamagesGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = DamagesGridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = DamageSource
      DataController.KeyFieldNames = 'damage_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'DAMAGES'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object DmgDColDamagePriority: TcxGridDBBandedColumn
        Caption = 'Priority Sortby'
        DataBinding.FieldName = 'work_priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object DmgDColWorkPriorityDesc: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_description'
        PropertiesClassName = 'TcxTextEditProperties'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object DmgDColDamageType: TcxGridDBBandedColumn
        Caption = 'Damage Type'
        DataBinding.FieldName = 'damage_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object DmgDColDamageID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'damage_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object DmgDColUQDamageID: TcxGridDBBandedColumn
        Caption = 'UQ Damage #'
        DataBinding.FieldName = 'uq_damage_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 87
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object DmgDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Width = 93
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object DmgDColDamageDate: TcxGridDBBandedColumn
        Caption = 'Damage Date'
        DataBinding.FieldName = 'damage_date'
        Options.Editing = False
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object DmgDColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object DmgDColProfitCenter: TcxGridDBBandedColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'profit_center'
        Options.Editing = False
        Options.Filtering = False
        Width = 95
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object DmgDColCompanyDamaged: TcxGridDBBandedColumn
        Caption = 'Company Damaged'
        DataBinding.FieldName = 'utility_co_damaged'
        Options.Editing = False
        Options.Filtering = False
        Width = 117
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object DmgDColFacilityType: TcxGridDBBandedColumn
        Caption = 'Facility Type'
        DataBinding.FieldName = 'facility_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 76
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object DmgDColFacilitySize: TcxGridDBBandedColumn
        Caption = 'Facility Size'
        DataBinding.FieldName = 'facility_size'
        Options.Editing = False
        Options.Filtering = False
        Width = 163
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object DmgDColExcavatorCompany: TcxGridDBBandedColumn
        Caption = 'Excavator Company'
        DataBinding.FieldName = 'excavator_company'
        Options.Editing = False
        Options.Filtering = False
        Width = 188
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object DmgDColLocation: TcxGridDBBandedColumn
        Caption = 'Location'
        DataBinding.FieldName = 'location'
        Options.Editing = False
        Options.Filtering = False
        Width = 181
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object DmgDColDetailsCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Options.Editing = False
        Options.Filtering = False
        Width = 195
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object DmgDColDetailsCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Options.Editing = False
        Options.Filtering = False
        Width = 193
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object DmgDColDetailsState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Options.Editing = False
        Options.Filtering = False
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object DmgDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = 0
        Properties.ValueUnchecked = 'False'
        Options.Filtering = False
        Options.Sorting = False
        Width = 86
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
    end
    object TicketsGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = TicketSource
      DataController.KeyFieldNames = 'ticket_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'TICKETS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object TktDColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 224
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object TktDColKind: TcxGridDBBandedColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object TktDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 174
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object TktDColWorkDate: TcxGridDBBandedColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'work_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 122
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object TktDColTicketType: TcxGridDBBandedColumn
        Caption = 'Type'
        DataBinding.FieldName = 'ticket_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object TktDColCompany: TcxGridDBBandedColumn
        Caption = 'Company'
        DataBinding.FieldName = 'company'
        Options.Editing = False
        Options.Filtering = False
        Width = 124
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object TktDColPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 79
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object TktDColWorkType: TcxGridDBBandedColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 224
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object TktDColWorkCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Options.Editing = False
        Options.Filtering = False
        Width = 174
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object TktDColStreetNum: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 122
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object TktDColStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object TktDColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object TktDColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 124
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
      object TktDColAlert: TcxGridDBBandedColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert2'
        Options.Editing = False
        Options.Filtering = False
        Styles.Content = SharedDevExStyleData.BoldRedFontStyle
        Width = 24
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object TktDColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        Options.Editing = False
        Options.Filtering = False
        Width = 457
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 2
      end
      object TktDColLegalGoodThru: TcxGridDBBandedColumn
        Caption = 'Legal Thru'
        DataBinding.FieldName = 'legal_good_thru'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object TktDColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 1
      end
      object TktDColTicketID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object TktDColLocates: TcxGridDBBandedColumn
        Caption = 'Locates'
        DataBinding.FieldName = 'locates'
        Options.Editing = False
        Options.Filtering = False
        Options.Sorting = False
        Width = 124
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 2
      end
      object TktDColQuickClose: TcxGridDBBandedColumn
        Caption = 'Quick Close'
        DataBinding.FieldName = 'quick_close'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediatePost = True
        Options.Filtering = False
        Options.Sorting = False
        Width = 115
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 2
      end
      object TktDColRoute: TcxGridDBBandedColumn
        Caption = 'Route'
        DataBinding.FieldName = 'LocalStringKey'
        Options.Filtering = False
        Width = 69
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object TktDColWorkPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        OnGetDisplayText = ColWorkPriorityGetDisplayText
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object TktDColWorkPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_disp'
        Visible = False
        Options.Filtering = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object TktDColFirstTask: TcxGridDBBandedColumn
        Caption = 'First Task'
        DataBinding.FieldName = 'first_task'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 64
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 2
      end
      object TktDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Options.Sorting = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 2
      end
      object TktDColTicketStatus: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_status'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
    end
    object WorkOrdersGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = WorkOrdersGridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = WorkOrdersSource
      DataController.KeyFieldNames = 'wo_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'WORK ORDERS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object WoDColWorkOrderID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'wo_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object WoDColWorkOrderNumber: TcxGridDBBandedColumn
        Caption = 'Work Order #'
        DataBinding.FieldName = 'wo_number'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object WoDColClientID: TcxGridDBBandedColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 132
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object WoDColWOSource: TcxGridDBBandedColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'wo_source'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object WoDColKind: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 33
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object WoDColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'work_order_status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 48
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object WoDColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object WoDColMapRef: TcxGridDBBandedColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object WoDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Options.VertSizing = False
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object WoDColWorkType: TcxGridDBBandedColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 624
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object WoDColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 275
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object WoDColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_Number'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object WoDColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 156
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object WoDColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 105
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object WoDColWorkCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 62
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object WoDColWorkState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'work_state'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 35
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object WoDColWorkZip: TcxGridDBBandedColumn
        Caption = 'Zip'
        DataBinding.FieldName = 'work_zip'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object WoDColWorkLat: TcxGridDBBandedColumn
        Caption = 'Lat'
        DataBinding.FieldName = 'work_lat'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object WoDColWorkLong: TcxGridDBBandedColumn
        Caption = 'Long'
        DataBinding.FieldName = 'work_long'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object WoDColCallerName: TcxGridDBBandedColumn
        Caption = 'Caller Name'
        DataBinding.FieldName = 'caller_name'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 97
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object WoDColCallerPhone: TcxGridDBBandedColumn
        Caption = 'Caller Phone'
        DataBinding.FieldName = 'caller_phone'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object WoDColCallerCellular: TcxGridDBBandedColumn
        Caption = 'Caller Cell'
        DataBinding.FieldName = 'caller_cellular'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object WoDColClientTransmitDate: TcxGridDBBandedColumn
        Caption = 'Transmit Date'
        DataBinding.FieldName = 'transmit_date'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Options.VertSizing = False
        Width = 93
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object WoDColJobNumber: TcxGridDBBandedColumn
        Caption = 'Job #'
        DataBinding.FieldName = 'job_number'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 49
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object WoDColWireCenter: TcxGridDBBandedColumn
        Caption = 'Wire Center'
        DataBinding.FieldName = 'wire_center'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 2
      end
      object WoDColWorkCenter: TcxGridDBBandedColumn
        Caption = 'Work Center'
        DataBinding.FieldName = 'work_center'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 2
      end
      object WoDColCentralOffice: TcxGridDBBandedColumn
        Caption = 'C.O.'
        DataBinding.FieldName = 'central_office'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 27
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 2
      end
      object WoDColServingTerminal: TcxGridDBBandedColumn
        Caption = 'Srv Term'
        DataBinding.FieldName = 'serving_terminal'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 2
      end
      object WoDColCircuitNumber: TcxGridDBBandedColumn
        Caption = 'Circuit #'
        DataBinding.FieldName = 'circuit_number'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 142
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object WoDColF2Cable: TcxGridDBBandedColumn
        Caption = 'F2 Cable'
        DataBinding.FieldName = 'f2_cable'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 47
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 2
      end
      object WoDColTerminalPort: TcxGridDBBandedColumn
        Caption = 'Term. Port'
        DataBinding.FieldName = 'terminal_port'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 2
      end
      object WoDColF2Pair: TcxGridDBBandedColumn
        Caption = 'F2 Pair'
        DataBinding.FieldName = 'f2_pair'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 56
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 2
      end
      object WoDColStateHwyROW: TcxGridDBBandedColumn
        Caption = 'St. ROW'
        DataBinding.FieldName = 'state_hwy_row'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 2
      end
      object WoDColRoadBoreCount: TcxGridDBBandedColumn
        Caption = 'Rd. Bores'
        DataBinding.FieldName = 'road_bore_count'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 2
      end
      object WoDColDrivewayBoreCount: TcxGridDBBandedColumn
        Caption = 'Dr. Bores'
        DataBinding.FieldName = 'driveway_bore_count'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 68
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 2
      end
      object WoDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
    end
    object WorkOrdersGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = WorkOrdersGridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = WorkOrdersSource
      DataController.KeyFieldNames = 'wo_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      Preview.AutoHeight = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'WORK ORDERS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object WoSColWorkOrderID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'wo_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object WoSColWorkOrderNumber: TcxGridDBBandedColumn
        Caption = 'Work Order #'
        DataBinding.FieldName = 'wo_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 118
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object WoSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 211
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object WoSColKind: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soDescending
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object WoSColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'work_order_status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object WoSColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object WoSColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object WoSColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object WoSColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object WoSColMapRef: TcxGridDBBandedColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object WoSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
    end
    object TicketsGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = TicketSource
      DataController.KeyFieldNames = 'ticket_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'TICKETS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object TktSColTicketID: TcxGridDBBandedColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object TktSColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object TktSColKind: TcxGridDBBandedColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object TktSColAlert: TcxGridDBBandedColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert2'
        Options.Editing = False
        Options.Filtering = False
        Styles.Content = SharedDevExStyleData.BoldRedFontStyle
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object TktSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 54
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object TktSColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object TktSColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 106
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object TktSColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object TktSColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Description'
        DataBinding.FieldName = 'work_description'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object TktSColLegalGoodThru: TcxGridDBBandedColumn
        Caption = 'Legal Thru'
        DataBinding.FieldName = 'legal_good_thru'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object TktSColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object TktSColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object TktSColLocates: TcxGridDBBandedColumn
        Caption = 'Locates'
        DataBinding.FieldName = 'locates'
        Options.Editing = False
        Options.Filtering = False
        Options.Sorting = False
        Width = 89
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object TktSColRoute: TcxGridDBBandedColumn
        Caption = 'Route'
        DataBinding.FieldName = 'LocalStringKey'
        Options.Filtering = False
        Width = 35
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object TktSColWorkPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_disp'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object TktSColWorkPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Options.SortByDisplayText = isbtOff
        Width = 49
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object TktSColQuickClose: TcxGridDBBandedColumn
        Caption = 'Quick Close'
        DataBinding.FieldName = 'quick_close'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediatePost = True
        Options.Filtering = False
        Options.Sorting = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object TktSColFirstTask: TcxGridDBBandedColumn
        Caption = 'First Task'
        DataBinding.FieldName = 'first_task'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object TktSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Options.Sorting = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object TktSColTicketStatus: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_status'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
    end
    object DamagesGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = DamagesGridDblClick
      OnMouseUp = ViewMouseUp
      OnMouseWheel = ViewMouseWheel
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = DamageSource
      DataController.KeyFieldNames = 'damage_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssNone
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'DAMAGES'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object DmgSColDamageType: TcxGridDBBandedColumn
        Caption = 'Damage Type'
        DataBinding.FieldName = 'damage_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object DmgSColUQDamageID: TcxGridDBBandedColumn
        Caption = 'UQ Damage #'
        DataBinding.FieldName = 'uq_damage_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object DmgSColDamageID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'damage_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object DmgSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object DmgSColDamageDate: TcxGridDBBandedColumn
        Caption = 'Damage Date'
        DataBinding.FieldName = 'damage_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 76
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object DmgSColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 46
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object DmgSColExcavatorCompany: TcxGridDBBandedColumn
        Caption = 'Excavator Company'
        DataBinding.FieldName = 'excavator_company'
        Options.Editing = False
        Options.Filtering = False
        Width = 107
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object DmgSColLocation: TcxGridDBBandedColumn
        Caption = 'Location'
        DataBinding.FieldName = 'location'
        Options.Editing = False
        Options.Filtering = False
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object DmgSColCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Options.Editing = False
        Options.Filtering = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object DmgSColCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Options.Editing = False
        Options.Filtering = False
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object DmgSColState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Options.Editing = False
        Options.Filtering = False
        Width = 30
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object DmgSColPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_description'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object DmgSColWorkPriority: TcxGridDBBandedColumn
        DataBinding.FieldName = 'work_priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object DmgSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = 0
        Properties.ValueUnchecked = 'False'
        Options.Filtering = False
        Options.Sorting = False
        Width = 38
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
    end
  end
  object OpenWorkOrders: TDBISAMQuery
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    BeforeOpen = OpenWorkOrdersBeforeOpen
    OnCalcFields = OpenWorkOrdersCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * '
      'from work_order'
      'where assigned_to_id = :emp_id'
      '  and active = 1'
      '  and closed = 0'
      'order by '
      '  kind, due_date, wo_number')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 11
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object WorkOrdersSource: TDataSource
    DataSet = OpenWorkOrders
    Left = 46
    Top = 104
  end
end
