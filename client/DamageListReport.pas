unit DamageListReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect, CheckLst, ExtCtrls;

type
  TDamageListReportForm = class(TReportBaseForm)
    ExcavatorLabel: TLabel;
    TypeLabel: TLabel;
    ProfitCenterLabel: TLabel;
    UtilityCoDamagedLabel: TLabel;
    Label2: TLabel;
    AttachmentsLabel: TLabel;
    InvoicesLabel: TLabel;
    Excavator: TEdit;
    DamageType: TComboBox;
    ProfitCenter: TComboBox;
    UtilityCoDamaged: TEdit;
    WorkToBeDone: TEdit;
    Attachments: TComboBox;
    Invoices: TComboBox;
    CityLabel: TLabel;
    LocationLabel: TLabel;
    StateLabel: TLabel;
    Label3: TLabel;
    City: TEdit;
    Location: TEdit;
    State: TComboBox;
    DueDate: TOdRangeSelectFrame;
    UQResp: TCheckBox;
    EXResp: TCheckBox;
    SpecialResp: TCheckBox;
    DamageDate: TOdRangeSelectFrame;
    NotifyDate: TOdRangeSelectFrame;
    FromLabel: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ClaimStatusBox: TComboBox;
    Label7: TLabel;
    InvoiceCodeList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    EstimateCombo: TComboBox;
    Label6: TLabel;
    GroupBoxInvestigator: TGroupBox;
    CheckBoxAll: TCheckBox;
    InvestigatorLocatorComboBox: TComboBox;
    Label8: TLabel;
    RadioButtonInvestigator: TRadioButton;
    RadioButtonLocator: TRadioButton;
    ManagersComboBox: TComboBox;
    LabelLocInv: TLabel;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure RadioButtonInvestigatorClick(Sender: TObject);
    procedure CheckBoxAllClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  private
    FInvoiceCodes: TStringList;
    FDamageClaimStatusCodes: TStringList;
    procedure SetupClaimStatusCombo;
    procedure SetVisibility;
    function ManagerSelected: Boolean;
    function SelectLocInvMode: Boolean;
    procedure RepopulateLocInvList;
  public
  end;


implementation

uses
  DMu, OdExceptions, OdVclUtils, QMConst, LocalEmployeeDmu;

{$R *.dfm}

{ TDamageListReportForm }

procedure TDamageListReportForm.InitReportControls;
begin
  inherited;
  DamageDate.SelectDateRange(rsLastWeek);
  DueDate.NoDateRange;
  NotifyDate.NoDateRange;

  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
  ProfitCenter.Items.Insert(1, '(Subtotal by PC)');
  DM.GetRefDisplayList('state', State.Items);
  DM.GetAttachmentCriteria(Attachments.Items);
  Attachments.ItemIndex := 0;
  DM.GetInvoiceCriteria(Invoices.Items);
  Invoices.ItemIndex := 0;
  UQResp.Caption := DM.UQState.CompanyShortName;

  FDamageClaimStatusCodes := TStringList.Create;
  SetupClaimStatusCombo;

  FInvoiceCodes := TStringList.Create;
  DM.GetInvoiceCodeList(InvoiceCodeList.Items, FInvoiceCodes);
  SetCheckListBox(InvoiceCodeList, True);

  EstimateCombo.ItemIndex := 1;
  SetUpManagerList(ManagersComboBox);
  SetVisibility;
end;

procedure TDamageListReportForm.SetupClaimStatusCombo;
begin
  Assert(Assigned(FDamageClaimStatusCodes));
  DM.GetRefCodeList('claimstat', FDamageClaimStatusCodes);
  FDamageClaimStatusCodes.Insert(0, 'ALL');
  FDamageClaimStatusCodes.Insert(2, 'CLOSED');

  DM.GetRefDisplayList('claimstat', ClaimStatusBox.Items);
  ClaimStatusBox.Items.Insert(0, 'All');
  ClaimStatusBox.Items.Insert(2, 'Closed');
  ClaimStatusBox.ItemIndex := 0;
end;

procedure TDamageListReportForm.ValidateParams;
var
  InvCodes: string;
  I: Integer;
begin
  inherited;
  SetReportID('DamageList');
  SetParamDate('StartDate', DamageDate.FromDate);
  SetParamDate('EndDate', DamageDate.ToDate);
  SetParamDate('StartDueDate', DueDate.FromDate);
  SetParamDate('EndDueDate', DueDate.ToDate);
  SetParamDate('StartNotifyDate', NotifyDate.FromDate);
  SetParamDate('EndNotifyDate', NotifyDate.ToDate);
  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParamBoolean('IncludeUQResp', UQResp.Checked);
  SetParamBoolean('IncludeExcvResp', ExResp.Checked);
  SetParamBoolean('IncludeSpecialResp', SpecialResp.Checked);
  SetParam('Location', Location.Text);
  SetParam('City', City.Text);
  SetParam('State', State.Text);
  SetParam('DamageType', DamageType.Text);
  SetParam('ExcavatorCompany', Excavator.Text);
  SetParam('WorkToBeDone', WorkToBeDone.Text);
  SetParam('UtilityCoDamaged', UtilityCoDamaged.Text);
  SetParamInt('Attachments', GetComboObjectInteger(Attachments));
  SetParamInt('Invoices', GetComboObjectInteger(Invoices));
  SetParam('ClaimStatus', FDamageClaimStatusCodes[ClaimStatusBox.ItemIndex]);


  if ManagerSelected then begin
    SetParamIntCombo('Manager', ManagersComboBox, True);
    if RadioButtonInvestigator.Checked then begin
      SetParamIntCombo('Investigator',InvestigatorLocatorComboBox, SelectLocInvMode);
      SetParamInt('Locator', 0);
    end
    else if RadioButtonLocator.Checked then begin
      SetParamIntCombo('Locator', InvestigatorLocatorComboBox,SelectLocInvMode);
      SetParamInt('Investigator', 0);
    end
    else
      raise EOdEntryRequired.Create('You must select Investigator or Locator.');
  end;

  SetParamInt('Estimate', EstimateCombo.ItemIndex);   //0-Initial;1-Latest;2=Both

  // TODO: Enable these once we get them working:
  //SetParam('DamageType', DamageType.Text);

  for I := 0 to InvoiceCodeList.Items.Count - 1 do begin
    if InvoiceCodeList.Checked[I] then begin
      if InvCodes = '' then begin
        InvCodes := FInvoiceCodes[I];
      end else begin
        InvCodes := InvCodes + ',' + FInvoiceCodes[I];
      end;
    end;
  end;
  if InvCodes = ''
    then raise EOdEntryRequired.Create('You must select at least one invoice code.');
  SetParam('InvCodes', InvCodes);
end;

procedure TDamageListReportForm.SelectAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(InvoiceCodeList, True);
end;

procedure TDamageListReportForm.ClearAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(InvoiceCodeList, False);
end;

procedure TDamageListReportForm.SetVisibility;
begin
  CheckBoxAll.Enabled:=ManagerSelected;
  LabelLocInv.Visible:=ManagerSelected;
  RadioButtonInvestigator.Visible:=ManagerSelected;
  RadioButtonLocator.Visible:=ManagerSelected;

  InvestigatorLocatorComboBox.Enabled:=SelectLocInvMode;
  RadioButtonInvestigator.Enabled:=ManagerSelected;
  RadioButtonLocator.Enabled:=ManagerSelected;

  InvestigatorLocatorComboBox.Enabled:=SelectLocInvMode and (RadioButtonInvestigator.Checked or RadioButtonLocator.Checked);

  if RadioButtonInvestigator.Checked then begin
    RadioButtonLocator.Checked:=False;
    LabelLocInv.Caption:='Investigator:';
  end
  else if RadioButtonLocator.Checked then begin
    RadioButtonInvestigator.Checked:=False;
    LabelLocInv.Caption:='Locator:';
  end;
end;

procedure TDamageListReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  inherited;
  SetVisibility;
  RepopulateLocInvList;
end;

function TDamageListReportForm.ManagerSelected: Boolean;
begin
  Result:=ManagersComboBox.ItemIndex<>-1;
end;

procedure TDamageListReportForm.RadioButtonInvestigatorClick(
  Sender: TObject);
begin
  inherited;
  SetVisibility;
end;

procedure TDamageListReportForm.CheckBoxAllClick(Sender: TObject);
begin
  inherited;
  SetVisibility;
  RepopulateLocInvList;
end;

function TDamageListReportForm.SelectLocInvMode: Boolean;
begin
  Result := (not CheckBoxAll.Checked);
end;

procedure TDamageListReportForm.RepopulateLocInvList;
begin
  if SelectLocInvMode then begin
    if ManagerSelected then begin
      EmployeeDM.EmployeeList(InvestigatorLocatorComboBox.Items, GetComboObjectInteger(ManagersComboBox))
    end;
  end else
    InvestigatorLocatorComboBox.Items.Clear;
end;

end.

