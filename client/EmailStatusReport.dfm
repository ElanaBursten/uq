inherited EmailStatusReportForm: TEmailStatusReportForm
  Left = 453
  Top = 508
  Caption = 'EmailStatusReportForm'
  ClientHeight = 255
  ClientWidth = 380
  PixelsPerInch = 96
  TextHeight = 13
  object CallCenterLabel: TLabel [0]
    Left = 0
    Top = 107
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 10
    Width = 145
    Height = 13
    Caption = 'Email Notification Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object CallCenterComboBox: TComboBox [2]
    Left = 74
    Top = 104
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
  end
  inline QueueDate: TOdRangeSelectFrame [3]
    Left = 30
    Top = 26
    Width = 248
    Height = 63
    TabOrder = 1
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
end
