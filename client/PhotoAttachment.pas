unit PhotoAttachment;

{$WARN UNIT_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, ODSelectImage, DMu;

type

  TOnPhotoAttachment = procedure (FileName: TFileName) of object;

  TPhotoAttachmentFrame = class(TFrame)
    PanelPhotos: TPanel;
    ScrollBox: TScrollBox;
    PanelFooter: TPanel;
    LabelCurrentDirectory: TLabel;
    LabelMessage: TLabel;
    ButtonSelAnotherDir: TButton;
    ButtonAttachAll: TButton;
    ButtonAttachSelected: TButton;
    CheckBoxShowSelOnly: TCheckBox;
    ButtonCancel: TButton;
    procedure ButtonSelAnotherDirClick(Sender: TObject);
    procedure ButtonAttachAllClick(Sender: TObject);
    procedure ButtonAttachSelectedClick(Sender: TObject);
    procedure CheckBoxShowSelOnlyClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    SourceDir: string;
    TempDir: string;
    Initialized: Boolean;
    FOnPhotoAttachment: TOnPhotoAttachment;
    FAfterDoPhotoAttachment: TNotifyEvent;
    SelectImageList: TSelectImageList;

    procedure AdjustControls(Status: Boolean);
    function GetFileList(const Path: string; FileFilter: TStrings; const Recursive: Boolean = True): TStringList;

    procedure LoadAndArrangeImages(const Path: string);
    procedure AttachSelectedImages;
    procedure DoPhotoAttachment;

    procedure AfterLoadImages(Sender: TObject);
    procedure AfterCopyImages(Sender: TObject);
    procedure OnLoadImage(Sender: TSelectImageList; const ImageNumber: Integer);
    procedure OnCopyImage(Sender: TSelectImageList; const ImageNumber: Integer);
    procedure SetOnPhotoAttachment(const Value: TOnPhotoAttachment);
    procedure SetAfterDoPhotoAttachment(const Value: TNotifyEvent);
  public
    { Public declarations }
    procedure BeforeDestruction; override;
    procedure Initialize;
    property OnPhotoAttachment: TOnPhotoAttachment read FOnPhotoAttachment write SetOnPhotoAttachment;
    property AfterDoPhotoAttachment: TNotifyEvent read FAfterDoPhotoAttachment write SetAfterDoPhotoAttachment;
  end;

implementation

{$R *.dfm}

uses Inifiles, FileCtrl, ODMiscUtils, Dialogs, QMConst;

{ TPhotoAttachmentFrame }

const
  Idle = True;
  Working = not Idle;

////////////////

procedure TPhotoAttachmentFrame.Initialize;
var
  Ini: TIniFile;
begin
  if Initialized then
    Exit;
  PanelFooter.Height := 45;
  Ini := TIniFile.Create(ExpandFileName('QManager.ini'));
  try
    SourceDir := Ini.ReadString('Preferences', 'PhotoSourceDir', '');
    TempDir := DM.GetAttachmentDownloadFolder;
    LabelCurrentDirectory.Caption := 'Current Directory: ' + SourceDir;
  finally
    FreeAndNil(Ini);
  end;
  SelectImageList := TSelectImageList.Create;
  SelectImageList.AfterLoadImages := AfterLoadImages;
  SelectImageList.OnLoadImage := OnLoadImage;
  SelectImageList.AfterCopyImages := AfterCopyImages;
  SelectImageList.OnCopyImage := OnCopyImage;
  LoadAndArrangeImages(SourceDir);
  Initialized := True;
end;

////////////////

function TPhotoAttachmentFrame.GetFileList(const Path: string; FileFilter: TStrings; const Recursive: Boolean): TStringList;
//TODO: replace this method to use the JCL function AdvBuildFileList
var
  FileList: TStringList;
  i: Integer;

  procedure RFindFile(const folder: TFileName; FileExtension: string; const Recursive: Boolean);
  var
    SearchRec: TSearchRec;
  begin
    // Locate all matching files in the current
    // folder and add their names to the list
    if FindFirst(folder + FileExtension, faAnyFile, SearchRec) = 0 then begin
      try
        repeat
          Application.ProcessMessages;
          if (SearchRec.Attr and faDirectory = 0) or (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then begin
            FileList.Add(folder + SearchRec.Name);
          end;
        until (FindNext(SearchRec) <> 0);
      except
        SysUtils.FindClose(SearchRec);
        raise;
      end;
      SysUtils.FindClose(SearchRec);
    end;
    if Recursive then begin
      // Now search the subfolders
      if FindFirst(folder + '*', faAnyFile, SearchRec) = 0 then begin
        try
          repeat
            if ((SearchRec.Attr and faDirectory) <> 0) and
              (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then
              RFindFile(folder + SearchRec.Name + '\', FileExtension, Recursive);
          until (FindNext(SearchRec) <> 0);
        except
          SysUtils.FindClose(SearchRec);
          raise;
        end;
        SysUtils.FindClose(SearchRec);
      end;
    end;
  end; // procedure RFindFile inside of FindFile

begin // function FindFile
  FileList := TStringList.Create;
  try
    for i := 0 to FileFilter.Count - 1 do
      RFindFile(IncludeTrailingPathDelimiter(Path), FileFilter[i], Recursive);
    Result := FileList;
  except
    FreeAndNil(FileList);
    raise;
  end;
end;

////////////////

procedure TPhotoAttachmentFrame.AdjustControls(Status: Boolean);
begin
  ButtonSelAnotherDir.Enabled := Status = Idle;
  ButtonAttachAll.Enabled := Status = Idle;
  ButtonAttachSelected.Enabled := Status = Idle;
  CheckBoxShowSelOnly.Enabled := Status = Idle;
  ButtonCancel.Visible := Status <> Idle;
  if (Status = Idle) then begin
    Screen.Cursor := crDefault;
    LabelMessage.Caption := '';
    LabelMessage.Visible := False;
  end
  else
    Screen.Cursor := crHourGlass;
end;

////////////////

procedure TPhotoAttachmentFrame.LoadAndArrangeImages(const Path: string);
var
  FileFilter: TStringList;
  FileList: TStrings;
  FileExtension: TStrings;
  i: Integer;
begin
  Assert(Path <> '', 'Blank photo path');
  AdjustControls(Working);
  FileExtension := nil;
  FileFilter := TStringList.Create;
  FileExtension := TStringList.Create;
  try
    LabelMessage.Caption := 'Searching for image files...';
    LabelMessage.Visible := True;
    Application.ProcessMessages;
    FileExtension.Delimiter := ' ';
    FileExtension.DelimitedText := DM.GetFileExtensionForPrintableFileType(ImageFilesModifier);
    for i := 0 to FileExtension.Count - 1 do begin
      if not StrContainsText('GIF', FileExtension[i]) then
        FileFilter.Add('*' + FileExtension[i]);
    end;

    FileList := GetFileList(SourceDir, FileFilter);
    LabelMessage.Caption := 'Generating data...';
    Application.ProcessMessages;
    SelectImageList.GenerateStructure(FileList);
    SelectImageList.ArrangeImagesOnParent(ScrollBox);
    SelectImageList.LoadImages;
  finally
    FreeAndNil(FileFilter);
    FreeAndNil(FileExtension);
    AdjustControls(Idle);
  end;
end;

////////////////

procedure TPhotoAttachmentFrame.AttachSelectedImages;
begin
  Assert(Assigned(FOnPhotoAttachment));
  AdjustControls(Working);
  try
    LabelMessage.Visible := True;
    // Copy all pictures to the working directory drive
    SelectImageList.CopyImagesToDest(TempDir);
    // Attach them to the ticket at QManager
    DoPhotoAttachment;
    // Delete selected images from the camera
    LabelMessage.Caption := 'Deleting selected images from source';
    Application.ProcessMessages;
    SelectImageList.DeleteSelectedImagesFromSource;
    CheckBoxShowSelOnly.OnClick := nil;
    CheckBoxShowSelOnly.Checked := False;
    CheckBoxShowSelOnly.OnClick := CheckBoxShowSelOnlyClick;
    SelectImageList.ArrangeImagesOnParent(ScrollBox);
    if Assigned(FAfterDoPhotoAttachment) then
      FAfterDoPhotoAttachment(Self);
  finally
    AdjustControls(Idle);
  end;
end;

////////////////

procedure TPhotoAttachmentFrame.DoPhotoAttachment;
var
  FileList: TStringList;
  i: Integer;
begin
  AdjustControls(Working);
  FileList := TStringList.Create;
  try
    SelectImageList.GetSelectedFileNames(FileList);
    for i := 0 to FileList.Count - 1 do begin
      if Assigned(FOnPhotoAttachment) then
        FOnPhotoAttachment(AddSlash(TempDir) + ExtractFileName(FileList[i]));
    end;
  finally
    FreeAndNil(FileList);
    AdjustControls(Idle);
  end;
end;

////////////////

procedure TPhotoAttachmentFrame.AfterLoadImages(Sender: TObject);
begin
  LabelMessage.Visible := False;
  LabelMessage.Caption := 'Total images: '+IntToStr(SelectImageList.Count);
  Application.ProcessMessages;
end;

////////////////

procedure TPhotoAttachmentFrame.OnLoadImage(Sender: TSelectImageList;
  const ImageNumber: Integer);
begin
  LabelMessage.Caption := 'Loading image '+IntToStr(ImageNumber)+'/'+IntToStr(Sender.Count);
  Application.ProcessMessages;
end;

////////////////

procedure TPhotoAttachmentFrame.AfterCopyImages(Sender: TObject);
begin
  LabelMessage.Visible := False;
  LabelMessage.Caption := '';
  Application.ProcessMessages;
end;

////////////////

procedure TPhotoAttachmentFrame.ButtonSelAnotherDirClick(Sender: TObject);
var
  Dir: string;
begin
  SelectDirectory('Select another directory:', '', Dir);
  if Dir <> '' then begin
    SourceDir := Dir;
    LabelCurrentDirectory.Caption := 'Current Directory: ' + SourceDir;
    SelectImageList.ClearImages;
    LoadAndArrangeImages(SourceDir);
  end;
end;

procedure TPhotoAttachmentFrame.OnCopyImage(Sender: TSelectImageList;
  const ImageNumber: Integer);
begin
  LabelMessage.Caption := 'Copying image ' + IntToStr(ImageNumber) + '/' + IntToStr(Sender.Count);
  Application.ProcessMessages;
end;

////////////////

procedure TPhotoAttachmentFrame.FrameResize(Sender: TObject);
begin
  if Assigned(SelectImageList) then
    SelectImageList.ArrangeImagesOnParent(ScrollBox, CheckBoxShowSelOnly.Checked);
end;

////////////////

procedure TPhotoAttachmentFrame.ButtonAttachAllClick(Sender: TObject);
begin
  if MessageDlg('This will delete all images from ' + SourceDir + #13#10'Confirm?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    SelectImageList.SelectAll;
    AttachSelectedImages;
  end;
end;

////////////////

procedure TPhotoAttachmentFrame.ButtonAttachSelectedClick(Sender: TObject);
begin
  if MessageDlg('This will delete the selected images from ' + SourceDir + #13#10'Confirm?', mtWarning, [mbYes, mbNo], 0) = mrYes then
    AttachSelectedImages;
end;

////////////////

procedure TPhotoAttachmentFrame.CheckBoxShowSelOnlyClick(Sender: TObject);
begin
  SelectImageList.ArrangeImagesOnParent(ScrollBox, CheckBoxShowSelOnly.Checked);
end;

////////////////

procedure TPhotoAttachmentFrame.ButtonCancelClick(Sender: TObject);
begin
  SelectImageList.CancelCurrentOperation;
end;

////////////////

procedure TPhotoAttachmentFrame.SetOnPhotoAttachment(
  const Value: TOnPhotoAttachment);
begin
  FOnPhotoAttachment := Value;
end;

////////////////

procedure TPhotoAttachmentFrame.SetAfterDoPhotoAttachment(
  const Value: TNotifyEvent);
begin
  FAfterDoPhotoAttachment := Value;
end;

////////////////

procedure TPhotoAttachmentFrame.BeforeDestruction;
begin
  inherited;
  FreeAndNil(SelectImageList);
end;

end.
