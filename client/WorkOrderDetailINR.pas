unit WorkOrderDetailINR;

interface

uses                                                     
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDBEdit, StdCtrls, DBCtrls, ExtCtrls,
  DB, cxCheckListBox, dbisamtb, WorkOrderDetailBase, 
  cxDropDownEdit, cxStyles, cxDataStorage,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxGridCustomView,
  cxGrid, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxCheckBox,
  ComCtrls, cxMemo, cxCustomData, cxFilter, cxData;

type
  TWOINRFrame = class(TWODetailBaseFrame)
    AccountNumber: TDBText;
    WorkOrderInspectionDS: TDataSource;
    ComplianceDueDate: TDBText;
    AssignedToLabel: TLabel;
    AssignedTo: TDBText;
    Label4: TLabel;
    PremiseIDLabel: TLabel;
    PremiseID: TDBText;
    CallerPhoneLabel: TLabel;
    AtlPhoneLabel: TLabel;
    Phone: TDBText;
    AltPhone: TDBText;
    ComplianceDueDateLabel: TLabel;
    AOCInspectionLabel: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    CustInspectionLabel: TLabel;
    InitialInspectionPanel: TPanel;
    InitialInspectionLabel: TLabel;
    DateOfVisitLabel: TLabel;
    CGACountLabel: TLabel;
    CompletionCodeLabel: TLabel;
    DateOfVisit: TDBText;
    CGACount: TDBText;
    CompletionCodeCombobox1: TDBLookupComboBox;
    Remedies: TDBISAMQuery;
    PotentialHazardCheckList: TcxCheckListBox;
    AOCInspectionCheckList: TcxCheckListBox;
    CustomerInspectionCheckList: TcxCheckListBox;
    WorkTypes: TDBISAMQuery;
    ViewRepo: TcxGridViewRepository;
    BuildingView: TcxGridDBBandedTableView;
    BuildingRef: TDBISAMQuery;
    BuildingRefSource: TDataSource;
    BuildingDescription: TcxGridDBBandedColumn;
    MapStatusRef: TDBISAMQuery;
    MapStatusRefSource: TDataSource;
    MeterLocationRef: TDBISAMQuery;
    MeterLocationRefSource: TDataSource;
    YnubRef: TDBISAMQuery;
    YnubRefSource: TDataSource;
    VentClearanceRef: TDBISAMQuery;
    VentClearanceRefSource: TDataSource;
    VentIgnitionRef: TDBISAMQuery;
    VentIgnitionRefSource: TDataSource;
    VentClearanceDistCombobox: TcxDBExtLookupComboBox;
    VentIgnitionDistComboBox: TcxDBExtLookupComboBox;
    InadequateVentClearance: TcxCheckBox;
    InadequateVentClearanceIg: TcxCheckBox;
    UpdateWORemedy: TDBISAMQuery;
    InsertWORemedy: TDBISAMQuery;
    CGAReasonLabel: TLabel;
    CGAReasonCombo: TcxDBExtLookupComboBox;
    CGAReasonRef: TDBISAMQuery;
    CGAReasonRefSource: TDataSource;
    CGAReasonView: TcxGridDBBandedTableView;
    CGAReasonDescription: TcxGridDBBandedColumn;
    GasLightView: TcxGridDBBandedTableView;
    GasLightDescription: TcxGridDBBandedColumn;
    GasLightRef: TDBISAMQuery;
    GasLightRefSource: TDataSource;
    WorkOrderOHMdetails: TDBISAMTable;       //REMOVE
    dsWorkOrderOHMdetails: TDataSource;
    pcDetails: TPageControl;
    tsMeter: TTabSheet;
    pnlGasMeter: TPanel;
    Label22: TLabel;
    WorkDescription: TcxDBMemo;
    btnQuestionnaire: TButton;
    LOALabel: TLabel;
    WorkCenter: TDBText;
    StatusField: TDBText;
    BtnRefresh: TButton;
    lblRefreshing: TLabel;
    procedure InadequateVentClearanceClick(Sender: TObject);
    procedure InadequateVentClearanceIgClick(Sender: TObject);
    procedure RemedyCheckListClickCheck(Sender: TObject;
      AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure ComboBoxEditing(Sender: TObject; var CanEdit: Boolean);
    procedure VentClearanceDistComboboxEditing(Sender: TObject; var CanEdit: Boolean);
    procedure btnQuestionnaireClick(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
    procedure WorkTypesFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ItemDblClick(Sender: TObject);    
  private
    CompletionCode: string;
    FLoading: Boolean;
    fINRBaseURL: string;
    procedure SaveVentClearance(WorkType: string; Checkbox: TcxCheckbox);
    procedure SetRemedyEditing;
    procedure CheckVentDistanceCheckboxChanged(WorkType: string; Checkbox: TcxCheckbox);
    procedure SaveInspectionList(var CheckList: TcxCheckListBox);
    procedure SaveAllInspectionLists;
    function GetCGAReason(Description: String): String;
    function GetINRBaseURL: string;
  protected
    function HasPotentialHazard: Boolean;
    procedure SetInheritedFrameEvents; override;
    procedure LoadInspectionLists;
    procedure AfterGotoWorkOrder; override;
  public
    procedure CheckCGAReason;
    procedure Load(ID: Integer); override;
    procedure EnableEditing(Enabled: Boolean); override;
    function ValidateWorkOrderFields: Boolean; override;
    function NeedToSave: Boolean; override;
    procedure SaveWorkOrderDetails; override;
    procedure CancelWorkOrder; override;
  end;

const
  NO_VALUE = '--';
implementation

uses DMu, OdMiscUtils, OdDbUtils, OdCxUtils, QMConst, Hashes, ShellAPI;

{$R *.dfm}

{ TWOOHMFrame }

procedure TWOINRFrame.EnableEditing(Enabled: Boolean);   //QMANTWO-434 (Everything for INR is readonly)
begin
//  inherited;
//  VentClearanceDistCombobox.Enabled := InadequateVentClearance.Checked;
//  VentIgnitionDistComboBox.Enabled := InadequateVentClearanceIg.Checked;
  CGAReasonCombo.Enabled := False; //Enabled and (CompletionCodeCombobox.Text = CGAStatus);
//  CompletionCodeCombobox.Enabled := False;
  AOCInspectionCheckList.ReadOnly := True;
  PotentialHazardCheckList.ReadOnly := True;
  CustomerInspectionCheckList.ReadOnly := True;
end;

function TWOINRFrame.HasPotentialHazard: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to PotentialHazardCheckList.Items.Count - 1 do begin
    Result := PotentialHazardCheckList.Items[i].Checked;
    if Result then
      Break;
  end;
end;

procedure TWOINRFrame.InadequateVentClearanceClick(Sender: TObject);
begin
  inherited;
  VentClearanceDistCombobox.Enabled := InadequateVentClearance.Checked;
  CheckVentDistanceCheckboxChanged(WorkTypeVC,InadequateVentClearance);
end;

procedure TWOINRFrame.InadequateVentClearanceIgClick(Sender: TObject);
begin
  inherited;
  VentIgnitionDistComboBox.Enabled := InadequateVentClearanceIg.Checked;
  CheckVentDistanceCheckboxChanged(WorkTypeVI,InadequateVentClearanceIg);
end;

procedure TWOINRFrame.ItemDblClick(Sender: TObject);
begin
  ClearSelectionsDBText(InitialInspectionPanel);  //QMANTWO-577 EB Clear local panel first
  inherited;
end;

procedure TWOINRFrame.AfterGotoWorkOrder;
begin
  inherited;
  FLoading := False;
end;

procedure TWOINRFrame.CheckVentDistanceCheckboxChanged(WorkType: string; Checkbox: TcxCheckbox);
begin
  if not FLoading then
    SetRemedyEditing;
end;

procedure TWOINRFrame.Load(ID: Integer);
var
  LoadStatus: string;
  IsClosed: boolean;
begin
  FLoading := True;
  fINRBaseURL := GetINRBaseURL;
  inherited;
  try
    tsMeter.TabVisible := False;  //Hide the actual tab
    tsMeter.Visible := True;
    LoadStatus := WorkOrderDS.DataSet.FieldByName('status').AsString;
    IsClosed := WorkOrderDS.DataSet.FieldByName('closed').AsBoolean;
    If (LoadStatus <> '') and IsClosed then begin
      StatusField.Font.Color := clWindowText;
      StatusField.Font.Style := [fsBold];
      CompletionCode := WorkOrderDS.DataSet.FieldByName('status').AsString;
    end
    else begin
      StatusField.Font.Color := clWindowFrame;
      StatusField.Font.Style := [];
    end;

    LoadInspectionLists;
    RefreshDataSet(BuildingRef);
    RefreshDataSet(MapStatusRef);
    RefreshDataSet(MeterLocationRef);
    RefreshDataSet(YnubRef);
    RefreshDataSet(VentClearanceRef);
    RefreshDataSet(VentIgnitionRef);
    RefreshDataSet(CGAReasonRef);
    RefreshDataSet(GasLightRef);
  finally
    FLoading := False;
  end;
end;


procedure TWOINRFrame.LoadInspectionLists;
var
  TargetCheckList: TcxCheckListBox;
  ListItem: TcxCheckListBoxItem;
  WorkTypeCode: string;
  IsPopulated: Boolean;
  FlagColor: string;
begin
  RefreshDataSet(WorkTypes);

  Remedies.Close;
  Remedies.ParamByName('WOID').AsInteger := WorkOrderDS.DataSet.FieldByName('wo_id').AsInteger;
  Remedies.Open;

  AOCInspectionCheckList.Clear;
  PotentialHazardCheckList.Clear;
  CustomerInspectionCheckList.Clear;

  //First use WorkTypes to populate the checkbox lists.
  WorkTypes.First;
  while not WorkTypes.EOF do begin
    FlagColor := WorkTypes.FieldByName('flag_color').AsString;
    WorkTypeCode := WorkTypes.FieldByName('work_type').AsString;
    //Use Remedies to set any checked values
    IsPopulated := Remedies.Locate('work_type',WorkTypeCode,[]);
    //Find out whick checklist to put it in
    if FlagColor = 'RED' then
      TargetChecklist := PotentialHazardChecklist
    else if FlagColor = 'ORANGE' then
      TargetChecklist := CustomerInspectionChecklist
    else
      TargetChecklist := AOCInspectionChecklist;

    if WorkTypeCode = WorkTypeVC then
      InadequateVentClearance.Checked := IsPopulated;
    if WorkTypeCode = WorkTypeVI then
      InadequateVentClearanceIg.Checked := IsPopulated;

    if (WorkTypeCode <> WorkTypeVC) and (WorkTypeCode <> WorkTypeVI) then begin
      ListItem := TargetChecklist.Items.Add;
      ListItem.Text := WorkTypes.FieldByName('work_description').AsString;
      ListItem.Tag := WorkTypes.FieldByName('work_type_id').AsInteger;
      ListItem.DisplayName := WorkTypeCode;
      ListItem.Checked := IsPopulated;
    end;
    WorkTypes.Next;
  end;

end;



function TWOINRFrame.NeedToSave: Boolean;
begin
  Result := EditingDataSet(WorkOrderDS.DataSet)
    or EditingDataSet(WorkOrderInspectionDS.DataSet);
end;

procedure TWOINRFrame.RemedyCheckListClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  inherited;
  SetRemedyEditing;
end;

procedure TWOINRFrame.ComboBoxEditing(Sender: TObject; var CanEdit: Boolean);
begin
  inherited;
  EditDataSet(WorkOrderDS.DataSet);
end;



function TWOINRFrame.GetCGAReason(Description: String): String; //todo this should be a common method for handling our blank descriptions?
begin
  if CGAReasonRef.Locate('description',Description,[]) then
    Result := CGAReasonRef.FieldByName('code').AsString
  else
    Result := 'B'; //Return default blank code
end;

function TWOINRFrame.GetINRBaseURL: string;
begin
  if fINRBaseURL = '' then
    fINRBaseURL := DM.GetConfigurationDataValue('INR_BaseURL', '');
  Result := fINRBaseURL;
end;

procedure TWOINRFrame.CheckCGAReason;
begin
 // CGAReasonCombo.Enabled := (CompletionCodeCombobox.Text = CGAStatus);
  if not CGAReasonCombo.Enabled then
    CGAReasonCombo.Clear;
end;

procedure TWOINRFrame.SaveAllInspectionLists;
begin
  SaveVentClearance(WorkTypeVC, InadequateVentClearance);
  SaveVentClearance(WorkTypeVI, InadequateVentClearanceIg);
  SaveInspectionList(AOCInspectionCheckList);
  SaveInspectionList(CustomerInspectionCheckList);
  SaveInspectionList(PotentialHazardCheckList);
end;

procedure TWOINRFrame.SaveVentClearance(WorkType: string; Checkbox: TcxCheckbox);
const
  GetWorkTypeIDSQL = 'select work_type_id as N from work_order_work_type where work_type=''%s''';
  ExistsSQL = 'select count(*) as N from work_order_remedy where wo_id=%d and work_type_id=%d';
var
  WOID, WorkTypeID: Integer;
begin
  WOID := DM.WorkOrder.FieldByName('wo_id').AsInteger;
  WorkTypeID := DM.Engine.RunSQLReturnN(Format(GetWorkTypeIDSQL,[WorkType]));

  if DM.Engine.RunSQLReturnN(Format(ExistsSQL, [WOID, WorkTypeID])) = 1 then begin//work_order_remedy record already exists
    UpdateWORemedy.Params.ParamByName('active').Value := Checkbox.Checked;
    UpdateWORemedy.Params.ParamByName('wo_id').Value := WOID;
    UpdateWORemedy.Params.ParamByName('work_type_id').Value := WorkTypeID;
    UpdateWORemedy.ExecSQL;
  end
  else if Checkbox.Checked then begin //It's checked but not in the table, so we need to insert a new record
    OpenDataSet(DM.WorkOrderRemedy);
    DM.WorkOrderRemedy.Insert;
    DM.WorkOrderRemedy.FieldByName('wo_id').AsInteger := WOID;
    DM.WorkOrderRemedy.FieldByName('work_type_id').AsInteger := WorkTypeID;
    DM.WorkOrderRemedy.FieldByName('active').AsBoolean := True;
    DM.WorkOrderRemedy.Post;
  end;
end;

procedure TWOINRFrame.SaveInspectionList(var CheckList: TcxCheckListBox);
const
  ExistsSQL = 'select count(*) as N from work_order_remedy where wo_id=%d and work_type_id=%d';
var
  i, WOID, WorkTypeID: Integer;
  Active: Boolean;
begin
  WOID := DM.WorkOrder.FieldByName('wo_id').AsInteger;
  for i := 0 to Pred(CheckList.Count) do begin
    WorkTypeID := CheckList.Items[i].Tag;
    Active := CheckList.Items[i].Checked;

    if DM.Engine.RunSQLReturnN(Format(ExistsSQL, [WOID, WorkTypeID])) = 1 then begin
      UpdateWORemedy.Params.ParamByName('active').Value := Ord(Active);
      UpdateWORemedy.Params.ParamByName('wo_id').Value := WOID;
      UpdateWORemedy.Params.ParamByName('work_type_id').Value := WorkTypeID;
      UpdateWORemedy.ExecSQL;
    end
    else if Active then begin
      OpenDataSet(DM.WorkOrderRemedy);
      DM.WorkOrderRemedy.Insert;
      DM.WorkOrderRemedy.FieldByName('wo_id').AsInteger := WOID;
      DM.WorkOrderRemedy.FieldByName('work_type_id').AsInteger := WorkTypeID;
      DM.WorkOrderRemedy.FieldByName('active').AsBoolean := True;
      DM.WorkOrderRemedy.Post;
    end;
  end;
end;


procedure TWOINRFrame.SaveWorkOrderDetails;
var
  CGAVisitCount: Integer;
  AlertFirst, AlertLast, AlertConfirmation, AlertPhoneNo: string;
begin
  inherited;

  if EditingDataSet(WorkOrderInspectionDS.DataSet) then begin
    WorkOrderInspectionDS.DataSet.UpdateRecord;  // Force an update in case the user did not leave the field
    EditDataSet(WorkOrderInspectionDS.DataSet);
  end;

  EditDataSet(WorkOrderDS.DataSet);
  ValidateWorkOrderFields;

//  StatusList.Locate('status', CompletionCodeCombobox.Text, []);
  if StatusList.FieldByName('complete').AsBoolean then begin
    WorkOrderDS.DataSet.FieldByName('closed').AsBoolean := True;
    WorkOrderDS.DataSet.FieldByName('closed_date').AsDateTime := Now;
  end;


  //Associated Inspection record
  if not EditingDataSet(DM.WorkOrderInspection) then
    EditDataSet(DM.WorkOrderInspection);

//  if CompletionCodeCombobox.Text = CGAStatus then begin
  if StatusField.Caption = CGAStatus then begin
    CGAVisitCount := DM.WorkOrderInspection.FieldByName('cga_visits').AsInteger;
    Inc(CGAVisitCount);
    DM.WorkOrderInspection.FieldByName('cga_visits').AsInteger := CGAVisitCount;
  end
  else//make sure CGA reason is cleared for non-cga status
    DM.WorkOrderInspection.FieldByName('cga_reason').AsString := GetCGAReason('');

  //Grab Alert Data
  if HasPotentialHazard then begin
    AlertPhoneNo := DM.GetCallCenterAlertPhoneNoForCallCenterID(WorkOrderDS.DataSet.FieldByName('wo_source').AsString);
    AlertFirst := DM.WorkOrderInspection.FieldByName('alert_first_name').AsString;
    AlertLast := DM.WorkOrderInspection.FieldByName('alert_last_name').AsString;
    AlertConfirmation := DM.WorkOrderInspection.FieldByName('alert_order_number').AsString;
//    TPotentialHazardDialog.GetPotentialHazardData(               //QMANTWO-423
//      PotentialHazardChecklist.CheckedItemsString(False, sLineBreak),  //QMANTWO-423
//      AlertFirst, AlertLast, AlertConfirmation, AlertPhoneNo);          //QMANTWO-423
    DM.WorkOrderInspection.FieldByName('alert_first_name').AsString := AlertFirst;
    DM.WorkOrderInspection.FieldByName('alert_last_name').AsString := AlertLast;
    DM.WorkOrderInspection.FieldByName('alert_order_number').AsString := AlertConfirmation;
  end;

  PostDataSet(DM.WorkOrderInspection);

  //Work Order Remedies
//  SaveAllInspectionLists;  //INR Inspection lists are read-only, so we are not going to save them

  if StatusList.FieldByName('complete').AsBoolean then begin
    WorkOrderDS.DataSet.FieldByName('closed').AsBoolean := True;
    WorkOrderDS.DataSet.FieldByName('closed_date').AsDateTime := Now;
  end;

  WorkOrderDS.DataSet.FieldByName('status_date').AsDateTime := Now;
  WorkOrderDS.DataSet.FieldByName('statused_how').AsString := FStatusedHow;
  WorkOrderDS.DataSet.FieldByName('statused_by_id').AsInteger := DM.EmpID;
  PostDataSet(WorkOrderDS.DataSet);
end;

procedure TWOINRFrame.btnQuestionnaireClick(Sender: TObject);
var
    HashString : string;
    WOIDStr, EmpIDStr : string;
    Pepper, LPepper, RPepper, Link: string;
    WOID: Integer;
const
    WID  = 'wid=';
    EMPID  = 'empid=';
    SOURCE = 'source=';
    QM = 'qm';
    FM   = 'FM=';
    CODE = 'code=';
    AMP  = #38;
    NoLinkErr = 'Error: Unable to find Remediation URL (INR_BaseURL)';
    LinkErr = 'Error opening link at ';
begin
  inherited;
  Pepper := DM.GetConfigurationDataValue('EPR_Pepper'); // case sensitive
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  fINRBaseURL := DM.GetConfigurationDataValue('INR_BaseURL');
  if fINRBaseURL = '' then begin
    MessageDlg(NoLinkErr, mtError, [mbOK], 0);
  end;
  WOID := WorkOrderDS.DataSet.FieldByName('wo_id').AsInteger;
  WOIDStr := IntToStr(WOID);

  EmpIDStr := IntToStr(DM.EmpID);
  HashString := LPepper +
                WOIDStr +
                EmpIDStr +
                QM +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);
  Link := fINRBaseURL +
          WID + WOIDStr + AMP +
          EMPID + EmpIDStr + AMP +
          SOURCE + QM  + AMP +
          CODE + HashString;
  try
    ShellExecute(0, 'open', PChar(Link), nil, nil, SW_SHOW);
  except
  on e:exception do
    MessageDlg(LinkErr + fINRBaseURL + ' ' + e.message, mtError, [mbOK], 0);
  end;
end;


procedure TWOINRFrame.BtnRefreshClick(Sender: TObject);
var
  WOID: integer;
begin
  inherited;
  BtnRefresh.Visible := False;
  lblRefreshing.Visible := True;
  Screen.Cursor := crHourGlass;
  try
    WOID := WorkOrderDS.DataSet.FieldByName('wo_id').AsInteger;
    DM.UpdateWorkOrderInCache(WOID);
    DM.GoToWorkOrder(WOID);
    Load(WOID);
  finally
    BtnRefresh.Visible := True;
    lblRefreshing.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TWOINRFrame.CancelWorkOrder;
begin
  inherited;
  DM.WorkOrderInspection.Cancel;
end;


function TWOINRFrame.ValidateWorkOrderFields: Boolean;
const
  MsgReq = ' is required to save work order.';
  MsgGasLampReq = ' is required to close work order.';
  MsgMustBeForCGA = ' must be set to %s when Completion Code is %s';
  MsgNoAOCObReq = ' No Inspection items are checked, please check "%s" to continue.'; //WorkTypeNOAOC
  MSGNoAOCObClear = ' Inspection items are checked, please clear the "%s" checkbox to continue.';  //WorkTypeNOAOC
  MSGCGAClearAll = ' %s Completion Code is selected so all Inspection items must be unchecked to continue. These items must be unchecked: %s';//CGAStatus
var
  AOCMessage: string;
  IsCGA: Boolean;
  IsReadyToWork: Boolean;

  function ValidateInspectionCheckboxes: Boolean;
  var
    NOAOCisChecked: Boolean;
    CheckedItemList: TOdStringList;
    ItemsAreChecked: Boolean;
    NoAOCIdx: Integer;
  begin
    //Started to add a class helper here (i.e. IndexOfDisplayName), but the underlying classes
    //do not support it as DisplayName is not used as expected (i.e. always returns Text)
    NoAOCIdx := AOCInspectionCheckList.Items.IndexOf(WorkTypeNOAOC);
    if NoAOCIdx = -1 then begin
      Result := True;
      Exit; //NOAOC work type is not present, so validation rules cannot be applied.
    end;
    NoAOCisChecked := AOCInspectionCheckList.items[NoAOCIdx].Checked;

    CheckedItemList := TOdStringList.Create;
    try
      if InadequateVentClearance.Checked then
        CheckedItemList.Add(InadequateVentClearance.Caption);
      if InadequateVentClearanceIg.Checked then
        CheckedItemList.Add(InadequateVentClearanceIg.Caption);
      AOCInspectionChecklist.AddCheckedItemsToList(CheckedItemList);
      CustomerInspectionChecklist.AddCheckedItemsToList(CheckedItemList);
      PotentialHazardChecklist.AddCheckedItemsToList(CheckedItemList);
      if NOAOCisChecked and (not IsCGA)then
        RemoveStringFromList(WorkTypeNOAOC,CheckedItemList);
      ItemsAreChecked := (CheckedItemList.Count > 0);

      if IsCGA then begin
       //if CGA completion code is selected - then NO checkboxes at all can be checked.
       Result := (not ItemsAreChecked) and (not NOAOCisChecked);
       AOCMessage := Format(MSGCGAClearAll,[CGAStatus, CheckedItemList.CommaText]);
      end
      else begin
       if not ItemsAreChecked then begin
         //if no checkbox is checked- then "No AOC Observed" must be checked.
         Result := NOAOCisChecked;
         AOCMessage := Format(MsgNoAOCObReq,[WorkTypeNOAOC]);
       end
       else begin
         //if any checkbox is checked- then "No AOC Observed" must be UNchecked/cleared.
         Result := not NOAOCisChecked;
         AOCMessage := Format(MSGNoAOCObClear,[WorkTypeNOAOC]);
       end;
      end;
    finally
      FreeAndNil(CheckedItemList);
    end;
  end;

begin
  IsReadyToWork := (StatusField.Caption = ReadyToWorkStatus);
  Result := IsReadyToWork;
  IsCGA := (StatusField.Caption = CGAStatus);  //QMANTWO-446 EB

  if (not ISReadyToWork) then begin //No need to validate -R status because responses are not generated for this status
    if (not ValidateInspectionCheckboxes) then
      RaiseErrorOnFrame(AOCInspectionLabel.Caption + AOCMessage, AOCInspectionCheckList)
    else if (InadequateVentClearance.Checked and IsEmpty(VentClearanceDistCombobox.Text)) then
      RaiseErrorOnFrame(InadequateVentClearance.Caption + MsgReq, VentClearanceDistCombobox)
    else if (InadequateVentClearanceIg.Checked and IsEmpty(VentIgnitionDistComboBox.Text)) then
      RaiseErrorOnFrame(InadequateVentClearanceIg.Caption + MsgReq, VentIgnitionDistComboBox)
    else
      Result := True;
  end;
end;

procedure TWOINRFrame.VentClearanceDistComboboxEditing(Sender: TObject; var CanEdit: Boolean);
begin
  inherited;
  SetRemedyEditing;
end;

procedure TWOINRFrame.WorkTypesFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);   //QMANTWO-446 EB
var
  RowSource: string;
begin
  inherited;
  Assert(Assigned(DataSet));
  RowSource := WorkTypes.FieldByName('wo_source').AsString;
  Accept := WorkTypeisValidForWOSource(RowSource);
end;

procedure TWOINRFrame.SetRemedyEditing;
begin
  //EditDataSet(WorkOrderDS.DataSet);
end;

procedure TWOINRFrame.SetInheritedFrameEvents;
begin
  inherited;
  //Hook any dataset, etc events; inherited frames will not fire them.
  StatusList.OnFilterRecord := StatusListFilterRecord; //todo refactor to base
  DM.Engine.LinkEventsAutoInc(DM.WorkOrderRemedy);
end;


end.
