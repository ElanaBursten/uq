unit streetMap;   //used fpor Last known location
//QM-553
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, StdCtrls, ExtCtrls, ComCtrls, 
  Buttons, uWVBrowserBase, uWVBrowser, uWVWindowParent,
  uWVTypes, uWVTypeLibrary, uWVLoader, uWVWinControl;
type
  TfrmStreetMap = class(TForm)
    PanelHeader: TPanel;
    CheckBoxTraffic: TCheckBox;
    CheckBoxStreeView: TCheckBox;
    BitBtn1: TBitBtn;
    WVWindowParent1: TWVWindowParent;
    WVBrowser1: TWVBrowser;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    edtLat: TEdit;
    Label2: TLabel;
    edtLng: TEdit;
    Label3: TLabel;
    Button2: TButton;

    procedure CheckBoxTrafficClick(Sender: TObject);

    procedure CheckBoxStreeViewClick(Sender: TObject);
    procedure ButtonClearMarkersClick(Sender: TObject);
    procedure ButtonGotoLocationClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure WVBrowser1AfterCreated(Sender: TObject);
    procedure WVBrowser1InitializationError(Sender: TObject;
      aErrorCode: HRESULT; const aErrorMessage: wvstring);
    procedure WVBrowser1NavigationCompleted(Sender: TObject;
      const aWebView: ICoreWebView2;
      const aArgs: ICoreWebView2NavigationCompletedEventArgs);
  private
    { Private declarations }

    fUtc: string;
    fLat: string;
    fLng: string;
    fLocatDT: string;
    fShortName: string;
    fMulti: Boolean;

    procedure CreateMap(Sender: TObject);
    procedure LoadMap;
  protected
    procedure WMMove(var aMessage : TWMMove); message WM_MOVE;
    procedure WMMoving(var aMessage : TMessage); message WM_MOVING;
  public
    property  LocatDT : string Read fLocatDT write fLocatDT;
    property  Lat : string read fLat write fLat;
    property  Lng : string read fLng write fLng;
    property  Utc : string read fUtc write fUtc;
    property  ShortName : string read fShortName write fShortName;
    property Multi: Boolean read fMulti write fMulti;

    constructor Create(AOwner:TComponent; aLat, aLng, aUtc, aShortName:string);reintroduce;
//    function MapAllBreadcrumbs: Boolean;
  end;

var
  frmStreetMap: TfrmStreetMap;
  GoogleAPIKey: AnsiString;
  SystemDir: string;

CONST
  GOOGLE_MAPS_HDR=         //qm-931
  '<html> '+
  '<head> '+
  '  <meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />  '+
  '  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=%s&amp;callback=initMap"></script> '+
  '  <script type="text/javascript"> ';
  GOOGLE_MAPS_BODY=         //qm-931
  'var map; '+
  'var trafficLayer; '+
  'var markersArray = []; '+
  '  '+
  'function initialize() { '+
  ' '+
  '  var latlng = new google.maps.LatLng(34.714776,-80.019213);  '+
  '  var myOptions = {  '+
  '    zoom: 15,  '+
  '    center: latlng, '+
  '    mapTypeId: google.maps.MapTypeId.ROADMAP  '+
  '  }; '+
  '  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);  '+
  '  trafficLayer = new google.maps.TrafficLayer();   '+
  '  map.set("streetViewControl", false);  '+
  '}  '+
'  '+
'  function GotoLatLng(Lat, Lng, LabelM) {  '+
'   var latlng = new google.maps.LatLng(Lat,Lng);  '+
'   map.setCenter(latlng); '+
'   PutMarker(Lat, Lng, LabelM); '+
'  } '+
'   '+
' function ClearMarkers() { '+
'  if (markersArray) {  '+
'    for (i in markersArray) { '+
'      markersArray[i].setMap(null); '+
'    }  '+
'  }  '+
'}  '+
'   '+
'  function PutMarker(Lat, Lng, Msg) {  '+
'   var latlng = new google.maps.LatLng(Lat,Lng); '+
'   var marker = new google.maps.Marker({ '+
'      position: latlng,  '+
'      map: map, '+
'      title: Msg+" ("+Lat+","+Lng+")" '+
'  }); '+
' markersArray.push(marker); '+
'  } '+
'    '+
'  function ShowAllMarkers() { '+
'  var bounds = new google.maps.LatLngBounds(); '+
'    for (var i = 0; i < markersArray.length; i++) {  '+
'      bounds.extend(markersArray[i].getPosition()); '+
'  }  '+
'   '+
'  map.fitBounds(bounds);  '+
'  }  '+
'  '+
'  '+
'  function TrafficOn()   { trafficLayer.setMap(map); }  '+
'   '+
'  function TrafficOff()  { trafficLayer.setMap(null); } '+
'  '+
'  function StreetViewOn() { map.set("streetViewControl", true); } '+
'   '+
'  function StreetViewOff() { map.set("streetViewControl", false); } '+
'   '+
'</script> '+
'</head> '+
'<body onload="initialize()"> '+
'  <div id="map_canvas" style="width:100%; height:100%"></div> '+
'</body>  '+
'</html> ';



implementation
uses DMu;   //qm-931
{$R *.dfm}


constructor TfrmStreetMap.Create(AOwner: TComponent; aLat, aLng, aUtc, aShortName: string);
begin
  inherited Create(AOwner);
  Lat  := aLat;
  Lng  := aLng;
  Utc  :=  aUtc;
  ShortName:= aShortName;
  edtLat.Text:= Lat;
  edtLng.Text:= Lng;
end;

procedure TfrmStreetMap.CreateMap(Sender: TObject);
begin
   GoogleAPIKey:=DM.GetConfigurationDataValue('GEO_Key','',True);    //qm-931
   LoadMap;   //qm-931
   ButtonGotoLocationClick(Sender);  //qm-931
end;

procedure TfrmStreetMap.LoadMap;
var
  htmlString: string;
begin
  try
    try
      htmlString:=  Format(GOOGLE_MAPS_HDR,[GoogleAPIKey]) +GOOGLE_MAPS_BODY;  //qm-931  sr
      WVBrowser1.NavigateToString(htmlString);  //qm-931  sr

    except
      {$IFDEF DEBUG}
      on e : exception do
        OutputDebugString(PWideChar('TfrmStreetMap.LoadMap error: ' + e.message + chr(0)));
      {$ENDIF}
    end;
  finally
    WVBrowser1.Refresh;   //qm-931  sr
  end;
end;

procedure TfrmStreetMap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmStreetMap.FormShow(Sender: TObject);
begin
  if GlobalWebView2Loader.InitializationError then
    showmessage(GlobalWebView2Loader.ErrorMessage)
   else
    if GlobalWebView2Loader.Initialized then
      WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
     else
      Timer1.Enabled := True;
      self.Caption := Caption+' for '+ShortName;

end;

//{QMANTWO-302 - Added}
//function TfrmStreetMap.MapAllBreadcrumbs: Boolean;
////var
////  fLat, fLng,
////  fLabel: string;
//  //fActivity: string;
//begin
//  WVBrowser1.ExecuteScript('ShowAllMarkers()');
//end;

procedure TfrmStreetMap.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if GlobalWebView2Loader.Initialized then
    WVBrowser1.CreateBrowser(WVWindowParent1.Handle)
   else
    Timer1.Enabled := True;
end;


procedure TfrmStreetMap.WMMove(var aMessage: TWMMove);
begin
  inherited;
  if (WVBrowser1 <> nil) then
    WVBrowser1.NotifyParentWindowPositionChanged;
end;

procedure TfrmStreetMap.WMMoving(var aMessage: TMessage);
begin
  inherited;
  if (WVBrowser1 <> nil) then
    WVBrowser1.NotifyParentWindowPositionChanged;
end;

procedure TfrmStreetMap.WVBrowser1AfterCreated(Sender: TObject);
begin
  WVWindowParent1.UpdateSize;
  Caption := 'Street map for Tickets for Edge';
  CreateMap(Sender);
end;

procedure TfrmStreetMap.WVBrowser1InitializationError(Sender: TObject;
  aErrorCode: HRESULT; const aErrorMessage: wvstring);
begin
  showmessage(aErrorMessage);
end;

procedure TfrmStreetMap.WVBrowser1NavigationCompleted(Sender: TObject;
  const aWebView: ICoreWebView2;
  const aArgs: ICoreWebView2NavigationCompletedEventArgs);
begin
  ButtonGotoLocationClick(Sender);
end;

procedure TfrmStreetMap.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TfrmStreetMap.ButtonClearMarkersClick(Sender: TObject);
begin
  WVBrowser1.ExecuteScript('ClearMarkers()');
end;

procedure TfrmStreetMap.ButtonGotoLocationClick(Sender: TObject);
begin
  Lat:=  edtLat.Text;
  Lng:=  edtLng.Text;
  WVBrowser1.ExecuteScript(Format('GotoLatLng(%s,%s)',[Lat,Lng]));
end;

procedure TfrmStreetMap.CheckBoxStreeViewClick(Sender: TObject);
begin
  if CheckBoxStreeView.Checked then
   WVBrowser1.ExecuteScript('StreetViewOn()')
  else
   WVBrowser1.ExecuteScript('StreetViewOff()');
end;

procedure TfrmStreetMap.CheckBoxTrafficClick(Sender: TObject);
begin
  if CheckBoxTraffic.Checked then
   WVBrowser1.ExecuteScript('TrafficOn()')
  else
   WVBrowser1.ExecuteScript('TrafficOff()');
end;

initialization
  GlobalWebView2Loader                := TWVLoader.Create(nil);
  GlobalWebView2Loader.UserDataFolder := ExtractFileDir(Application.ExeName) + '\CustomCache';
  GlobalWebView2Loader.StartWebView2;


end.
