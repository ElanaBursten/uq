unit BillingAdjustmentSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls, OdRangeSelect, OdDBISAMUtils, DB,
  ExtCtrls, QMConst, cxGridDBTableView, cxMaskedit;

type
  TBillingAdjustmentCriteria = class(TSearchCriteria)
    AdjustmentDate: TOdRangeSelectFrame;
    AddedDate: TOdRangeSelectFrame;
    CallCenters: TComboBox;
    Description: TEdit;
    Amount: TEdit;
    Customer: TComboBox;
    AdjustmentType: TComboBox;
    CallCenterLabel: TLabel;
    CustomerLabel: TLabel;
    TypeLabel: TLabel;
    DescriptionLabel: TLabel;
    AmountLabel: TLabel;
    AdjustmentDateLabel: TLabel;
    AddedDateLabel: TLabel;
    WidthPanel: TPanel;
    Label1: TLabel;
    WhichInvoice: TComboBox;
    GLCodeLabel: TLabel;
    GLCode: TEdit;
  public
    procedure BeforeItemSelected(ID: Variant); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure Showing; override;
    procedure InitializeSearchCriteria; override;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); override;
  end;

implementation

uses
  DMu, QMServerLibrary_Intf, OdVclUtils;

{$R *.dfm}

{ TBillingAdjustmentCriteria }

procedure TBillingAdjustmentCriteria.BeforeItemSelected(ID: Variant);
begin
  inherited;
  DM.Engine.GetBillingAdjustmentDataFromServer(ID);
end;

procedure TBillingAdjustmentCriteria.CustomizeGrid(
  GridView: TcxGridDBTableView);
var
  ColAmount: TcxGridDBColumn;
begin
  ColAmount := GridView.GetColumnByFieldName('amount');
  if Assigned(ColAmount) then begin
    ColAmount.HeaderAlignmentHorz := taRightJustify;
    ColAmount.PropertiesClass := TcxMaskEditProperties;
    ColAmount.Properties.Alignment.Horz := taRightJustify;
  end;
end;

procedure TBillingAdjustmentCriteria.DefineColumns;
begin
  inherited;
  Assert(Assigned(Defs));
  with Defs do begin
    SetDefaultTable('billing_adjustment');
    AddColumn('ID', 45, 'adjustment_id', ftInteger, '', 0, True, False);
    AddColumn(AdjustmentDate, 'Adj Date', 80, 'adjustment_date');
    AddColumn(CallCenters, 'center_group_id', 'Center', 50, 'call_center', ftString, 'client', 0, False, False);
    AddColumn(Customer, 'customer_id', 'Customer', 100, 'customer_name');
    AddColumn(WhichInvoice, 'Invoice', 105, 'which_invoice');
    AddColumn(Amount, 'Amount', 75, 'amount', ftFloat, '', 0, False, True, ',0.00;-,0.00; ');
    AddColumn(AdjustmentType, 'Adj Type', 60, 'type');
    AddColumn(Description, 'Description', 160, 'description');
    AddColumn(GLCode, 'GL Code', 50, 'gl_code');
    AddColumn(AddedDate, 'Added Date', 115, 'added_date');
  end;
end;

function TBillingAdjustmentCriteria.GetXmlString: string;
var
  Params: NVPairList;
begin
  Params := CreateNVPairListFromCriteria;
  try
    Result := DM.Engine.Service.BillingAdjustmentSearch(DM.Engine.SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

procedure TBillingAdjustmentCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.BillableCenterGroupList(CallCenters.Items, [RightBillingAdjustmentsView], True);
  DM.CustomerList(Customer.Items, True);
  SizeComboDropdownToItems(Customer);
  DM.GetRefDisplayList('adjtype', AdjustmentType.Items, '', True);
  DM.CustomerInvoiceList(WhichInvoice.Items);
  RestoreCriteria;
end;

procedure TBillingAdjustmentCriteria.InitializeSearchCriteria;
begin
  inherited;
  AdjustmentDate.SelectDateRange(rsCustom, Trunc(Now - 10), Trunc(Now + 10));
  AddedDate.SelectDateRange(rsCustom, Trunc(IncMonth(Now, -1)), Trunc(Now));
end;

end.
