object AttachmentsFrame: TAttachmentsFrame
  Left = 0
  Top = 0
  Width = 961
  Height = 284
  TabOrder = 0
  object AttachmentsPanel: TPanel
    Left = 0
    Top = 0
    Width = 961
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object AttachmentList: TcxGrid
      Left = 5
      Top = 5
      Width = 951
      Height = 243
      Align = alClient
      TabOrder = 0
      OnExit = AttachmentListExit
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object AttachmentListDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = AttachmentSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'attachment_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.FocusCellOnTab = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsView.ShowEditButtons = gsebForFocusedRecord
        OptionsView.GridLineColor = clBtnFace
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object ColAttachmentID: TcxGridDBColumn
          Caption = 'Attachment ID'
          DataBinding.FieldName = 'attachment_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 23
        end
        object ColOrigFileName: TcxGridDBColumn
          Caption = 'File Name'
          DataBinding.FieldName = 'orig_filename'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 156
        end
        object ColSize: TcxGridDBColumn
          Caption = 'Size (KB)'
          DataBinding.FieldName = 'size'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taRightJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          OnGetDisplayText = ColSizeGetDisplayText
          HeaderAlignmentHorz = taRightJustify
          Options.Editing = False
          Options.Filtering = False
          Width = 67
        end
        object ColAttachDate: TcxGridDBColumn
          Caption = 'Attach Date'
          DataBinding.FieldName = 'attach_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Options.Editing = False
          Options.Filtering = False
          Options.ShowEditButtons = isebNever
          Width = 156
        end
        object ColUploadDate: TcxGridDBColumn
          Caption = 'Upload Date'
          DataBinding.FieldName = 'upload_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Options.Editing = False
          Options.Filtering = False
          Options.ShowEditButtons = isebNever
          Width = 156
        end
        object ColDocumentType: TcxGridDBColumn
          Caption = 'Doc Type'
          DataBinding.FieldName = 'doc_type'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownListStyle = lsEditFixedList
          Properties.DropDownRows = 7
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Properties.Revertable = True
          Options.Filtering = False
          Options.ShowEditButtons = isebAlways
          Width = 78
        end
        object ColComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 231
        end
        object ColFileName: TcxGridDBColumn
          Caption = 'Generated File Name'
          DataBinding.FieldName = 'filename'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 158
        end
        object ColForeignType: TcxGridDBColumn
          DataBinding.FieldName = 'foreign_type'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 72
        end
        object ColForeignID: TcxGridDBColumn
          DataBinding.FieldName = 'foreign_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 59
        end
        object ColActive: TcxGridDBColumn
          Caption = 'Active'
          DataBinding.FieldName = 'active'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taLeftJustify
          Properties.NullStyle = nssUnchecked
          Properties.ReadOnly = True
          Properties.ValueChecked = 'True'
          Properties.ValueGrayed = ''
          Properties.ValueUnchecked = 'False'
          Visible = False
          MinWidth = 16
          Options.Filtering = False
          Width = 39
        end
        object ColShortName: TcxGridDBColumn
          Caption = 'Attached By'
          DataBinding.FieldName = 'short_name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 103
        end
        object ColAttachedBy: TcxGridDBColumn
          Caption = 'Attached By ID'
          DataBinding.FieldName = 'attached_by'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 72
        end
        object ColSource: TcxGridDBColumn
          Caption = 'Source'
          DataBinding.FieldName = 'source'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 56
        end
        object ColUploadMachineName: TcxGridDBColumn
          Caption = 'Upload Machine Name'
          DataBinding.FieldName = 'upload_machine_name'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 152
        end
      end
      object AttachmentListLevel: TcxGridLevel
        GridView = AttachmentListDBTableView
      end
    end
  end
  object FooterPanel: TPanel
    Left = 0
    Top = 253
    Width = 961
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object RemoveButton: TButton
      Left = 410
      Top = 1
      Width = 130
      Height = 25
      Action = RemoveAttachmentAction
      TabOrder = 3
    end
    object AddButton: TButton
      Left = 5
      Top = 1
      Width = 130
      Height = 25
      Action = AddAttachmentsAction
      TabOrder = 0
    end
    object DownloadButton: TButton
      Left = 275
      Top = 1
      Width = 130
      Height = 25
      Action = DownloadAction
      TabOrder = 2
    end
    object SelectAllButton: TButton
      Left = 545
      Top = 1
      Width = 130
      Height = 25
      Action = SelectAllAction
      TabOrder = 4
    end
    object ActivateCameraButton: TButton
      Left = 140
      Top = 1
      Width = 130
      Height = 25
      Action = AttachFromCamera
      TabOrder = 1
    end
    object AttachmentAddinButton: TButton
      Left = 680
      Top = 1
      Width = 130
      Height = 25
      Caption = '(Addin Button)'
      TabOrder = 5
      Visible = False
    end
  end
  object Attachment: TDBISAMQuery
    BeforeOpen = AttachmentBeforeOpen
    AfterOpen = AttachmentAfterOpen
    AfterPost = AttachmentAfterPost
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      '/* Overwritten in code */'
      'select * from attachment'
      'where foreign_type = :ForeignType and foreign_id = :ForeignID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ForeignType'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ForeignID'
        Value = 0
      end>
    Left = 556
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ForeignType'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ForeignID'
        Value = 0
      end>
  end
  object AttachmentSource: TDataSource
    DataSet = Attachment
    Left = 492
    Top = 32
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 436
    Top = 32
    object AddAttachmentsAction: TAction
      Caption = '&Add Attachments...'
      OnExecute = AddAttachmentsActionExecute
    end
    object RemoveAttachmentAction: TAction
      Caption = '&Remove Selected'
      OnExecute = RemoveAttachmentActionExecute
    end
    object DownloadAction: TAction
      Caption = '&Download Selected...'
      OnExecute = DownloadActionExecute
    end
    object DownloadAllAction: TAction
      Caption = 'Download All...'
    end
    object SelectAllAction: TAction
      Caption = 'Select &All'
      OnExecute = SelectAllActionExecute
    end
    object AttachFromCamera: TAction
      Caption = 'Attach from &Camera...'
      OnExecute = AttachFromCameraExecute
    end
  end
  object SaveDialog: TSaveDialog
    Filter = 
      'All Files (*.*)|*.*|Images (*.jpg, *.gif, *.bmp)|*.jpg;*.gif;*.b' +
      'mp|Word Documents (*.doc)|*.doc'
    Left = 492
    Top = 84
  end
  object OpenDialog: TOpenPictureDialog
    DefaultExt = 'jpg'
    Options = [ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 556
    Top = 84
  end
  object EmployeeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'employee'
    Left = 436
    Top = 83
  end
  object DocumentTypeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'document_type'
    Left = 396
    Top = 127
  end
end
