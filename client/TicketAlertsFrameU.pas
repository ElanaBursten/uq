unit TicketAlertsFrameU;

{QM-771 Ticket Alerts EB}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxDataStorage, cxEdit, cxNavigator,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, DMu, SharedDevExStyles,
  cxRichEdit, cxGridBandedTableView, cxGridDBBandedTableView, cxCustomData,
  cxFilter, cxData;

type
  TTicketAlertsFrame = class(TFrame)
    Panel1: TPanel;
    AlertGridView: TcxGridDBTableView;
    AlertGridLevel1: TcxGridLevel;
    AlertGrid: TcxGrid;
    AlertDS: TDataSource;
    ColAlertID: TcxGridDBColumn;
    ColAlertSource: TcxGridDBColumn;
    ColAlertTypeCode: TcxGridDBColumn;
    ColAlertTypeCodeDesc: TcxGridDBColumn;
    ColAlertDesc: TcxGridDBColumn;
    ColInsertDate: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColSortBy: TcxGridDBColumn;
    ColColor: TcxGridDBColumn;
    ColShowColor: TcxGridDBColumn;
    procedure AlertGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    fCurrentTicketID: integer;
    fLoaded: boolean;
  public
    function InitAlertsFrame(Expanded: boolean): boolean;
    procedure OpenAndGetAlerts(TicketID: integer; NeedRefresh: boolean = False);
    procedure OpenAlertsQuery(TicketID: integer);
    procedure CloseAlerts;
    function HasAlerts: boolean;
  end;

implementation

{$R *.dfm}

{ TTicketAlertsFrame }

procedure TTicketAlertsFrame.AlertGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  ShowColor: string;
begin
  If (AViewInfo.Item.Index = ColAlertDesc.Index) then
    ACanvas.Font.Color := clNavy;

  if not(VarIsNull(AViewInfo.GridRecord.Values[ColColor.Index]))and
        (AViewInfo.Item.Index = ColShowColor.Index) then begin
    ShowColor := UpperCase(AViewInfo.GridRecord.Values[ColColor.Index]);
    if ShowColor = 'RED' then
      ACanvas.Brush.Color := clRed
    else if ShowColor = 'BLUE' then
      ACanvas.Brush.Color := clSkyBlue
    else if ShowColor = 'YELLOW' then
      ACanvas.Brush.Color := clYellow
    else if ShowColor = 'GREEN' then
      ACanvas.Brush.Color := clMoneyGreen
    else if ShowColor = 'ORANGE' then
      ACanvas.Brush.Color := clWebOrange
    else if ShowColor = 'BLACK' then
      ACanvas.Brush.Color := clBlack
    else
      ACanvas.Brush.Color := clGray;
  end;
end;

procedure TTicketAlertsFrame.CloseAlerts;
begin
  DM.TicketAlertsQuery.Close;
end;

function TTicketAlertsFrame.HasAlerts: boolean;
begin
  if AlertDs.DataSet.RecordCount > 0 then
    Result := True
  else
    Result := False;
end;

function TTicketAlertsFrame.InitAlertsFrame(Expanded: boolean): boolean;
begin
  {Expanded: Shows more of the fields}
  ColAlertID.Visible := Expanded;
  ColAlertTypeCodeDesc.Visible := Expanded;
  ColModifiedDate.Visible := Expanded;
  AlertDS.Dataset := DM.TicketAlertsQuery;
  fCurrentTicketID := 0;
  fLoaded := False;
end;

procedure TTicketAlertsFrame.OpenAndGetAlerts(TicketID: integer; NeedRefresh: boolean = False);
begin
  if (TicketID <> fCurrentTicketID) or NeedRefresh then begin
    DM.GetTicketAlerts(TicketID);   //QM-771 Ticket Alerts EB
    fCurrentTicketID := TicketID;
  end;
  OpenAlertsQuery(TicketID);
end;

procedure TTicketAlertsFrame.OpenAlertsQuery(TicketID: integer);
begin
  if (fCurrentTicketID <> TicketID) then
    Exit;     {Can't open}

  if DM.TicketALertsQuery.Active then
    DM.TicketAlertsQuery.Close;
  try
    DM.TicketAlertsQuery.ParamByName('ticket_id').AsInteger := TicketID;
    DM.TicketAlertsQuery.Open;

    fLoaded := HasAlerts;
  except
    ShowMessage('An Error occurred pulling Ticket Alerts (TicketAlertsQuery)');
  end;
end;




end.
