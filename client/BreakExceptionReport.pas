unit BreakExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst, OdRangeSelect;

type
  TBreakExceptionReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    Label1: TLabel;
    WorkDateRange: TOdRangeSelectFrame;
    Label2: TLabel;
    EmployeeStatusCombo: TComboBox;
    TimeRulesCheckListBox: TCheckListBox;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    Label3: TLabel;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
  private
    function GetCheckedTimeRuleIDs: string;
    procedure CheckDefaultTimeRulesToExclude;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu, OdVclUtils, OdMiscUtils, LocalEmployeeDMu;

procedure TBreakExceptionReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  DM.EmpTimeRuleList(TimeRulesCheckListBox.Items);
  CheckDefaultTimeRulesToExclude;

  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
  WorkDateRange.SelectDateRange(rsLastWeek);
end;

procedure TBreakExceptionReportForm.ValidateParams;
begin
  inherited;
  SetReportID('PayrollExceptionBreaks');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamDate('StartDate', WorkDateRange.FromDate, True);
  SetParamDate('EndDate', WorkDateRange.ToDate, True);
  SetParamIntCombo('EmployeeStatus', EmployeeStatusCombo, True);
  SetParam('ExcludeTimeRuleIDs', GetCheckedTimeRuleIDs);
end;

procedure TBreakExceptionReportForm.CheckDefaultTimeRulesToExclude;
const
  DefaultExcludes: array[0..1] of string = ('SAL','NO RULE');
var
  I, Idx: Integer;
begin
  SetCheckListBox(TimeRulesCheckListBox, False);
  for I := Low(DefaultExcludes) to High(DefaultExcludes) do begin
    Idx := TimeRulesCheckListBox.Items.IndexOf(DefaultExcludes[I]);
    if Idx > -1 then
      TimeRulesCheckListBox.Checked[Idx] := True;
  end;
end;

function TBreakExceptionReportForm.GetCheckedTimeRuleIDs: string;
var
  I: Integer;
  Id: Integer;
begin
  Result := '';
  for I := 0 to TimeRulesCheckListBox.Items.Count - 1 do begin
    if TimeRulesCheckListBox.Checked[I] then begin
      Id := Integer(TimeRulesCheckListBox.Items.Objects[I]); // get ref id
      if IsEmpty(Result) then
        Result := IntToStr(Id)
      else
        Result := Result + ',' + IntToStr(Id);
    end;
  end;
end;

procedure TBreakExceptionReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(TimeRulesCheckListBox, True);
end;

procedure TBreakExceptionReportForm.SelectNoneButtonClick(Sender: TObject);
begin
  SetCheckListBox(TimeRulesCheckListBox, False);
end;

end.
