inherited HighLevelProductivityReportForm: THighLevelProductivityReportForm
  Caption = 'High Level Productivity'
  ClientHeight = 460
  PixelsPerInch = 96
  TextHeight = 13
  object ActivityDatesLabel: TLabel [0]
    Left = 0
    Top = 8
    Width = 71
    Height = 13
    Caption = 'Activity Dates:'
  end
  object ProfitCentersLabel: TLabel [1]
    Left = 0
    Top = 81
    Width = 71
    Height = 13
    Caption = 'Profit Centers:'
  end
  inline ActivityDateRange: TOdRangeSelectFrame [2]
    Left = 46
    Top = 19
    Width = 248
    Height = 63
    TabOrder = 0
  end
  object ProfitCenterList: TCheckListBox
    Left = 90
    Top = 81
    Width = 198
    Height = 371
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 1
  end
  object SelectAllButton: TButton
    Left = 298
    Top = 81
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 2
    OnClick = SelectAllButtonClick
  end
  object SelectNoneButton: TButton
    Left = 298
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Select None'
    TabOrder = 3
    OnClick = SelectNoneButtonClick
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 0
  end
end
