unit ClosedTicketDetailReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, ExtCtrls, BaseSearchForm,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit;

type
  TClosedTicketDetailReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    TicketNumberEdit: TcxButtonEdit;
    CallCenterEdit: TEdit;
    IncludeNotes: TCheckBox;
    TransmitDateEdit: TEdit;
    Label3: TLabel;
    IncludeAttachments: TCheckBox;
    IncludeFacilities: TCheckBox;
    LabelSelectImages: TLabel;
    AttachmentList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    Label4: TLabel;
    procedure IncludeAttachmentsClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure TicketNumberEditButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure AttachmentListClickCheck(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure UpdateAttachmentList;
    procedure UpdateTicketFields;
  public
    class procedure SetTicketID(TicketID: Integer);
  end;

implementation

uses
  DMu, OdExceptions, QMConst, OdVclUtils, OdMiscUtils, TicketSearchHeader;

var
  FTicketID: Integer;
{$R *.dfm}

{ TClosedTicketDetailReportForm }

class procedure TClosedTicketDetailReportForm.SetTicketID(TicketID: Integer);
begin
  FTicketID := TicketID;
end;

procedure TClosedTicketDetailReportForm.ValidateParams;
begin
  inherited;
  if (FTicketID < 1) then
    raise EOdEntryRequired.Create('You must specify a ticket number');

  IncludeAttachments.Checked := not IsEmpty(GetCheckedItemString(AttachmentList, True));

  SetReportID('TicketDetail');
  SetParamInt('TicketID', FTicketID);
  SetParamBoolean('IncludeNotes', IncludeNotes.Checked);
  SetParamBoolean('IncludeFacilities', IncludeFacilities.Checked);
  if IncludeAttachments.Checked then
    SetParam('ImageList', GetCheckedItemString(AttachmentList, True));
end;

procedure TClosedTicketDetailReportForm.InitReportControls;
begin
  inherited;

  TicketNumberEdit.Clear;
  CallCenterEdit.Clear;
  TransmitDateEdit.Clear;
  IncludeNotes.Checked := True;
  UpdateTicketFields;
end;

procedure TClosedTicketDetailReportForm.UpdateTicketFields;
var
  CloseTicket: Boolean;
begin
  if FTicketID > 0 then begin
    CloseTicket := not DM.Ticket.Active; // Active when coming from Ticket Detail
    if not DM.Ticket.Active then begin
      DM.Ticket.Open;
      DM.Ticket.Locate('ticket_id', FTicketID, []);
    end;
    TicketNumberEdit.Text := DM.Ticket.FieldByName('ticket_number').AsString;
    CallCenterEdit.Text := DM.Ticket.FieldByName('ticket_format').AsString;
    TransmitDateEdit.Text := DM.Ticket.FieldByName('transmit_date').AsString;
    if CloseTicket then
      DM.Ticket.Close;
    UpdateAttachmentList;
  end;
end;

procedure TClosedTicketDetailReportForm.IncludeAttachmentsClick(Sender: TObject);
begin
  AttachmentList.Enabled := IncludeAttachments.Checked;
  SelectAllButton.Enabled := IncludeAttachments.Checked;
  ClearAllButton.Enabled := IncludeAttachments.Checked;
  LabelSelectImages.Enabled := IncludeAttachments.Checked;
  SetCheckListBox(AttachmentList, IncludeAttachments.Checked);
end;

procedure TClosedTicketDetailReportForm.SelectAllButtonClick(Sender: TObject);
begin
   SetCheckListBox(AttachmentList, True, 160);    //QMANTWO-275
end;

procedure TClosedTicketDetailReportForm.AttachmentListClickCheck(
  Sender: TObject);
begin
  inherited;
  LimitCheckListBox(AttachmentList, 160);    //QMANTWO-275
end;

procedure TClosedTicketDetailReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(AttachmentList, False);
end;

procedure TClosedTicketDetailReportForm.UpdateAttachmentList;
begin
  Assert(FTicketID > 0, 'FTicketID is required in UpdateAttachmentList');
  DM.GetPrintableAttachmentList(qmftTicket, FTicketID, AttachmentList.Items);
  SetCheckListBox(AttachmentList, IncludeAttachments.Checked, 159);  //QMANTWO-275
  ShowCheckListboxHorzScrollbars(AttachmentList);
end;

procedure TClosedTicketDetailReportForm.TicketNumberEditButtonClick(
  Sender: TObject; AbsoluteIndex: Integer);
var
  SearchForm: TSearchForm;
  TicketID: string;
begin
  SearchForm := TSearchForm.CreateWithCriteria(nil, TTicketSearchCriteria);
  try
    if Trim(TicketNumberEdit.Text) <> '' then
      (SearchForm.Criteria as TTicketSearchCriteria).TicketNumber.Text := Trim(TicketNumberEdit.Text);
    if SearchForm.ShowModal = mrOK then begin
      TicketID := SearchForm.GetSelectedFieldValue('ticket_id');
      if TicketID <> '' then begin
        FTicketID := StrToInt(TicketID);
        DM.UpdateTicketInCache(FTicketID);
        UpdateTicketFields;
      end;
    end;
  finally
    FreeAndNil(SearchForm);
  end;
end;

end.
