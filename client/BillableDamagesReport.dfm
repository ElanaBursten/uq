inherited BillableDamagesReportForm: TBillableDamagesReportForm
  Left = 651
  Top = 279
  Caption = 'BillableDamagesReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object ProfitCenterLabel: TLabel [0]
    Left = 0
    Top = 94
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 8
    Width = 132
    Height = 13
    Caption = 'Damage Billing Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ProfitCenter: TComboBox [2]
    Left = 85
    Top = 91
    Width = 203
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 0
  end
  inline BillingDateRange: TOdRangeSelectFrame [3]
    Left = 4
    Top = 19
    Width = 313
    Height = 63
    TabOrder = 1
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Left = 177
      Top = 11
    end
    inherited DatesComboBox: TComboBox
      Left = 81
      Top = 35
      Width = 204
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 81
      Width = 86
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 199
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 3
  end
end
