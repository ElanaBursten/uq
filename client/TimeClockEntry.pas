unit TimeClockEntry;

interface

uses
  SysUtils, Classes, Contnrs, OdExceptions;

type
  EInvalidTimeDataError = class(EOdDataEntryError);

  TTimeSpan = record
    StartTime: TDateTime;
    StopTime: TDateTime;
  end;

  IRowWriter = interface
    procedure AddRow(EmpID: Integer; WorkDate, ActivityTime: TDateTime; const ActivityType: string);
  end;

  IDataSaver = interface
    function GetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; var WorkTimes, CalloutTimes: array of TTimeSpan; var LunchStartTime: TDateTime): Boolean;
    procedure SetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; WorkTimes, CalloutTimes: array of TTimeSpan; LunchStartTime: TDateTime);
    procedure ClearTimesheetTimes(EmpID: Integer; WorkDate: TDateTime);
    procedure ClearTimeclockTimes(EmpID: Integer; WorkDate: TDateTime);
  end;

  TStringRowWriter = class(TInterfacedObject, IRowWriter)
  private
    Buffer: string;
  public
    function AsString: string;
    procedure AddRow(EmpID: Integer; WorkDate: TDateTime; ActivityTime: TDateTime; const ActivityType: string);
  end;

  TStringDataSaver = class(TInterfacedObject, IDataSaver)
  private
    Buffer: string;
  public
    function AsString: string;
    procedure Clear;
    function GetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; var WorkTimes, CalloutTimes: array of TTimeSpan; var LunchStartTime: TDateTime): Boolean;
    procedure SetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; WorkTimes, CalloutTimes: array of TTimeSpan; LunchStartTime: TDateTime);
    procedure ClearTimesheetTimes(EmpID: Integer; WorkDate: TDateTime);
    procedure ClearTimeclockTimes(EmpID: Integer; WorkDate: TDateTime);
    constructor Create;
  end;

  TActivity = class(TObject)
  public
    ActivityTime: TDateTime;
    IsCallout: Boolean;
    IsStartTime: Boolean;
    IsLunchStart: Boolean;
    ActivityType: string;
  end;

  TTimeClockTranslator = class(TObject)
  private
    Saver: IDataSaver;
    Writer: IRowWriter;
    TimeList: TObjectList;
    FEmpID: Integer;
    FWorkDate: TDateTime;
    FIsSubmitted: Boolean;
    function GetActivityType(ATime: TActivity; IsFinalWorkTime: Boolean; const LastType: string): string;
    procedure ConvertToTimeSheet;
    procedure AddEntryToTimeList(ATime: TDateTime; ACallout, AStartTime, ALunchStart: Boolean; const AActivity: string);
    procedure EmitOutputRows;
    procedure SortTimeList;
    procedure InitializeTimeArray(var TimeArray: array of TTimeSpan);
    function AnyCalloutTimesAfter(WorkEndTime: TDateTime): Boolean;
    function GetLastActivityType: string;
  public
    procedure AddTimeEntryTimes;
    procedure AddTimeClockTime(ActivityTime: TDateTime; const ActivityType: string);
    procedure Initialize(EmpID: Integer; WorkDate: TDateTime; IsSubmitted: Boolean);
    procedure ClearTimeList;
    procedure CompleteSave;
    procedure ExportTabularForm;
    procedure ValidateFollowupActivity(const LastActivityType, ThisActivityType: string);
    procedure AddTimeFromXMLFile(const FileName, LoggedInUserEmployeeNumber: string);
    function WorkTimesUsed: Integer;
    function CallTimesUsed: Integer;

    constructor Create(DataSaver: IDataSaver; DataWriter: IRowWriter);
    destructor Destroy; override;
  end;

const
  // time clock activity categories
  WorkStart = 'Work Start';   // QMANTWO-238      WorkStart     = 'Work In';
  WorkStop = 'Work Stop';     // QMANTWO-238      WorkStop      = 'Work Out';
  LunchStart = 'Meal Out';    // QMANTWO-236      LunchStart    = 'Lunch Out';
  LunchStop  = 'Meal In';     // QMANTWO-236      LunchStop     = 'Lunch In';
  PersonalStart = 'Out';      // QMANTWO-238      PersonalStart = 'Personal Out';
  PersonalStop = 'In';        // QMANTWO-238      PersonalStop  = 'Personal In';
  CalloutStart = 'Call In';
  CalloutStop = 'Call Out';

  WorkStartActTypes: array[0..3] of string = (WorkStart, CalloutStart, LunchStop, PersonalStop);
  WorkStopActTypes:  array[0..1] of string = (WorkStop, CalloutStop);

implementation

uses OdMiscUtils, DateUtils, OdIsoDates, OdMSXMLUtils, MSXML2_TLB, QMConst;

{ TTimeClockTranslator }

procedure TTimeClockTranslator.Initialize(EmpID: Integer; WorkDate: TDateTime; IsSubmitted: Boolean);
begin
  FEmpID := EmpID;
  FWorkDate := WorkDate;
  FIsSubmitted := IsSubmitted;
  TimeList.Clear;
  Saver.ClearTimeclockTimes(FEmpID, FWorkDate);
end;

procedure TTimeClockTranslator.AddTimeClockTime(ActivityTime: TDateTime; const ActivityType: string);
var
  Act: TActivity;
begin
  Act := TActivity.Create;
  Act.ActivityTime := ActivityTime;
  Act.IsCallout := StrContainsText('call', ActivityType);  //  how do you know???
  Act.IsStartTime := (StrEndsWithText('in', ActivityType)) or (StrEndsWithText('Start', ActivityType));   // QMANTWO-238  Work in changes to Work Start
  Act.IsLunchStart := LunchStart = ActivityType;
  Act.ActivityType := ActivityType;
  TimeList.Add(Act);
end;

procedure TTimeClockTranslator.InitializeTimeArray(var TimeArray: array of TTimeSpan);
var
  I: Integer;
begin
  for I := Low(TimeArray) to High(TimeArray) do begin
    TimeArray[I].StartTime := 0;
    TimeArray[I].StopTime := 0;
  end;
end;

procedure TTimeClockTranslator.AddTimeEntryTimes;
var
  I: Integer;
  IsLunch: Boolean;
  WorkTimes: array[0..TimesheetNumWorkPeriods-1] of TTimeSpan;
  CallTimes: array[0..TimesheetNumCalloutPeriods-1] of TTimeSpan;
  LunchStartTime: TDateTime;
begin
  Assert(FEmpID <> -1, 'Initialize must be called before AddTimeEntryTimes');
  InitializeTimeArray(WorkTimes);
  InitializeTimeArray(CallTimes);

  // load the arrays with the existing timesheet_entry values
  Saver.GetTimesheetTimes(FEmpID, FWorkDate, WorkTimes, CallTimes, LunchStartTime);

  // add them to the internal TObjectList cache in the timeclock_entry format
  for I := Low(WorkTimes) to High(WorkTimes) do begin
    IsLunch := False;
    if WorkTimes[I].StartTime > 0 then begin
      if I = Low(WorkTimes) then
        AddEntryToTimeList(WorkTimes[I].StartTime, False, True, False, WorkStart)
      else if GetLastActivityType = LunchStart then
        AddEntryToTimeList(WorkTimes[I].StartTime, False, True, False, LunchStop)
      else
        AddEntryToTimeList(WorkTimes[I].StartTime, False, True, False, PersonalStop);
    end;

    if WorkTimes[I].StopTime > 0 then begin
      if not FloatEquals(LunchStartTime, 0) then
        IsLunch := FloatEquals(LunchStartTime, WorkTimes[I].StopTime);
      if IsLunch then
        AddEntryToTimeList(WorkTimes[I].StopTime, False, False, IsLunch, LunchStart)
      else
        AddEntryToTimeList(WorkTimes[I].StopTime, False, False, IsLunch, PersonalStart);
    end;
  end;
  for I := Low(CallTimes) to High(CallTimes) do begin
    if CallTimes[I].StartTime > 0 then
      AddEntryToTimeList(CallTimes[I].StartTime, True, True, False, CalloutStart);
    if CallTimes[I].StopTime > 0 then
      AddEntryToTimeList(CallTimes[I].StopTime, True, False, False, CalloutStop);
  end;
end;

procedure TTimeClockTranslator.AddEntryToTimeList(ATime: TDateTime; ACallout,
  AStartTime, ALunchStart: Boolean; const AActivity: string);
var
  Entry: TActivity;
begin
  Entry := TActivity.Create;
  Entry.ActivityTime := ATime;
  Entry.IsCallout := ACallout;
  Entry.IsStartTime := AStartTime;
  Entry.IsLunchStart := ALunchStart;
  Entry.ActivityType := AActivity;
  TimeList.Add(Entry);
end;

procedure TTimeClockTranslator.CompleteSave;
begin
  SortTimeList;
  ConvertToTimeSheet;
end;

constructor TTimeClockTranslator.Create(DataSaver: IDataSaver; DataWriter: IRowWriter);
begin
  inherited Create;
  TimeList := TObjectList.Create;

  Writer := DataWriter;
  Saver := DataSaver;
  FEmpID := -1;
  FWorkDate := 0;
end;

destructor TTimeClockTranslator.Destroy;
begin
  FreeAndNil(TimeList);
  inherited;
end;

function TTimeClockTranslator.AnyCalloutTimesAfter(WorkEndTime: TDateTime): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to TimeList.Count - 1 do
    with (TimeList.Items[i] as TActivity) do begin
      if IsCallout and (ActivityTime > WorkEndTime) then begin
        Result := True;
        Break;
      end;
    end;
end;

// figure out a TActivity's activity type
function TTimeClockTranslator.GetActivityType(ATime: TActivity; IsFinalWorkTime: Boolean; const LastType: string): string;
begin
  if ATime.IsCallout then begin    // callout time categories
    if ATime.IsStartTime then
      Result := CalloutStart
    else
      Result := CalloutStop;
    Exit;
  end;

  if ATime.IsLunchStart then begin
    Result := LunchStart;
    Exit;
  end;

  if ATime.IsStartTime then begin
    if LastType = LunchStart then
      Result := LunchStop
    else if LastType = PersonalStart then
      Result := PersonalStop
    else
      Result := WorkStart;
  end else if IsFinalWorkTime and (FIsSubmitted or (FWorkDate <> SysUtils.Date) or
    AnyCalloutTimesAfter(ATime.ActivityTime)) then
    Result := WorkStop
  else
    Result := PersonalStart;
end;

procedure TTimeClockTranslator.EmitOutputRows;
var
  i: Integer;
  LastWorkStopTimeIdx: Integer;
  IsLastWorkStopTime: Boolean;
  ActivityType, PriorType: string;
  Act: TActivity;
begin
  Assert(FEmpID <> -1, 'Initialize must be called before EmitOutputRows');
  PriorType := '';
  LastWorkStopTimeIdx := -1;
  for i := 0 to TimeList.Count-1 do begin
    Act := TimeList.Items[i] as TActivity;
    if (not Act.IsCallout) then begin
      if (not Act.IsStartTime) then
        LastWorkStopTimeIdx := i
      else
        LastWorkStopTimeIdx := -1; // reset if we find a later work_start
    end;
  end;

  for i := 0 to TimeList.Count - 1 do begin
    IsLastWorkStopTime := (i = LastWorkStopTimeIdx);
    Act := TimeList.Items[i] as TActivity;
    ActivityType := GetActivityType(Act, IsLastWorkStopTime, PriorType);
    PriorType := ActivityType;
    Writer.AddRow(FEmpID, FWorkDate, Act.ActivityTime, ActivityType);
  end;
end;

// Output TimeList as a series of activity times & types.
procedure TTimeClockTranslator.ExportTabularForm;
begin
  SortTimeList;
  EmitOutputRows;
end;

procedure TTimeClockTranslator.ValidateFollowupActivity(const LastActivityType, ThisActivityType: string);
begin
// if not TimeSheetCalloutAdd.YesterdayCallOuts then  //QM-442 SR override for Yesterday COs
// begin
  if IsEmpty(LastActivityType) or IsEmpty(ThisActivityType) then
    Exit;

  if StrEndsWithText('in', LastActivityType) then begin
    if StrEndsWithText('in', ThisActivityType) then
      raise EInvalidTimeDataError.Create('Cannot have consecutive clock in activity types.');
    if StrContainsText('callout', LastActivityType) and (not StrContainsText('callout', ThisActivityType)) then   // not sure about this
      raise EInvalidTimeDataError.Create('A Call In activity can only be followed by a Call Out activity.');
  end else if StrEndsWithText('out', LastActivityType) then begin
    if StrEndsWithText('out', ThisActivityType) or StrEndsWithText('stop', ThisActivityType) then
      raise EInvalidTimeDataError.Create('Cannot have consecutive clock out activity types.');
//    if StrContainsText('personal', LastActivityType) and (not StrContainsText('personal', ThisActivityType))
      if ((LastActivityType = 'in') and (ThisActivityType <> 'out'))  //qmantwo-236
      and (not StrContainsText('call', ThisActivityType) or not StrContainsText('meal', ThisActivityType)) then
      raise EInvalidTimeDataError.Create('An Out activity can only be followed by a In or Call In activity.'); //qmantwo-236
    if StrContainsText('meal', LastActivityType) and (not StrContainsText('meal', ThisActivityType)) then       //qmantwo-236
      raise EInvalidTimeDataError.Create('A Meal Out activity can only be followed by a Meal In activity.');    //qmantwo-236
  end;
// end;
end;

// Save the TimeList as arrays of WorkTimes & CallTimes.
procedure TTimeClockTranslator.ConvertToTimeSheet;
var
  Act: TActivity;
  i: Integer;
  CallIdx: Integer;
  WorkIdx: Integer;
  WorkTimes: array[0..TimesheetNumWorkPeriods-1] of TTimeSpan;
  CallTimes: array[0..TimesheetNumCalloutPeriods-1] of TTimeSpan;
  LunchStart: TDateTime;
  LastActivityType: string;
  LastActivityTime: TDateTime;
begin
  Assert(FEmpID <> -1, 'Initialize must be called before ConvertToTimesheet.');
  InitializeTimeArray(WorkTimes);
  InitializeTimeArray(CallTimes);
  CallIdx := 0;
  WorkIdx := 0;
  LunchStart := 0;
  LastActivityType := '';
  LastActivityTime := -1;
  for i := 0 to TimeList.Count - 1 do begin
    Act := TimeList.Items[i] as TActivity;
    ValidateFollowupActivity(LastActivityType, Act.ActivityType);
    if SameHourAndMinute(Act.ActivityTime, LastActivityTime) then
      raise EInvalidTimeDataError.CreateFmt(
        'The %s activity cannot have the same %s time as the previous %s activity.',
        [Act.ActivityType, FormatDateTime('hh:mm ampm', Act.ActivityTime), LastActivityType]);

    LastActivityType := Act.ActivityType;
    LastActivityTime := Act.ActivityTime;
    if Act.IsCallout then begin
      if CallIdx > High(CallTimes) then
        raise EInvalidTimeDataError.CreateFmt('Exceeded maximum of %d callout time ranges.', [TimesheetNumCalloutPeriods]);
      if Act.IsStartTime then
        CallTimes[CallIdx].StartTime := Act.ActivityTime
      else begin
        CallTimes[CallIdx].StopTime := Act.ActivityTime;
        Inc(CallIdx);
      end;
    end else begin
      if WorkIdx > High(WorkTimes) then
        raise EInvalidTImeDataError.CreateFmt('Exceeded maximum of %d work time ranges.', [TimesheetNumWorkPeriods]);
      if Act.IsStartTime then
        WorkTimes[WorkIdx].StartTime := Act.ActivityTime
      else begin
        WorkTimes[WorkIdx].StopTime := Act.ActivityTime;
        Inc(WorkIdx);
      end;
    end;
    if Act.IsLunchStart then
    begin
//      if not FloatEquals(0, LunchStart) then   //QMANTWO-236  need to support multiple lunches
//        raise EInvalidTimeDataError.Create('Only one meal break per day is allowed.');
      LunchStart := Act.ActivityTime;    //QMANTWO-251
    end;
  end;

  Saver.SetTimesheetTimes(FEmpID, FWorkDate, WorkTimes, CallTimes, LunchStart);
end;

function TTimeClockTranslator.GetLastActivityType: string;
begin
  Result := '';
  if TimeList.Count > 0 then
    Result := (TimeList.Items[TimeList.Count-1] as TActivity).ActivityType;
end;

procedure TTimeClockTranslator.AddTimeFromXMLFile(const FileName, LoggedInUserEmployeeNumber: string);
var
  XML: IXMLDOMDocument;
  ClockEntry: IXMLDOMNode;
  EmpNum: string;
  ActivityType: string;
  ActivityDateTimeStr: string;
  ActivityAttribute: string;
  ActivityTime: TDateTime;

  procedure Validate;
  begin
    if IsEmpty(EmpNum) then
      raise EInvalidTimeDataError.Create('Time clock import file is missing EmployeeNumber information.');
    if IsEmpty(ActivityDateTimeStr) then
      raise EInvalidTimeDataError.Create('Time clock import file is missing Time information.');
    if IsEmpty(ActivityType) then
      raise EInvalidTimeDataError.Create('Time clock import file is missing ClockType information.');
    if IsEmpty(ActivityAttribute) then
      raise EInvalidTimeDataError.Create('Time clock import file is missing ClockAttribute information.');
    if EmpNum <> LoggedInUserEmployeeNumber then
      raise EInvalidTimeDataError.CreateFmt('Only time clock entries for the current Q Manager user can be imported. ' +
        '%s time for Employee # %s was not imported.', [ActivityType, EmpNum]);
    if DateOf(ActivityTime) <> DateOf(FWorkDate) then
      raise EInvalidTimeDataError.CreateFmt('Expected the work date contained in the import file to be %s but it was %s.',
        [FormatDateTime('yyyy-mm-dd', FWorkDate), FormatDateTime('yyyy-mm-dd', ActivityTime)]);
    if ActivityTime > Now then
      raise EInvalidTimeDataError.Create('Time clock entries that occur in the future cannot be imported.');
    ValidateFollowupActivity(GetLastActivityType, ActivityType);
  end;

begin
  EmpNum := '';
  ActivityType := '';
  ActivityDateTimeStr := '';
  XML := LoadXMLFile(FileName);
  ClockEntry := XML.selectSingleNode('/QManager/ClockEntry');
  if not Assigned(ClockEntry) then
    raise Exception.Create('Time clock import file is missing ClockEntry node.');

  GetAttributeTextFromNode(ClockEntry, 'EmployeeNumber', EmpNum);
  GetAttributeTextFromNode(ClockEntry, 'Time', ActivityDateTimeStr);
  GetAttributeTextFromNode(ClockEntry, 'ClockType', ActivityType);
  GetAttributeTextFromNode(ClockEntry, 'ClockAttribute', ActivityAttribute);
  ActivityTime := IsoStrToDateTime(Copy(ActivityDateTimeStr, 1, 23)); // the incoming time string has the UTC offset appended
  Validate;
  AddTimeClockTime(TimeOf(ActivityTime), ActivityType);
  CompleteSave;
  Writer.AddRow(FEmpID, FWorkDate, ActivityTime, ActivityType);
end;

// sorts TimeList in ascending time order
function CompareTimes(Item1, Item2: Pointer): Integer;
begin
  Result := CompareTime(TActivity(Item1).ActivityTime, TActivity(Item2).ActivityTime);
end;

procedure TTimeClockTranslator.SortTimeList;
begin
  TimeList.Sort(@CompareTimes);
end;

procedure TTimeClockTranslator.ClearTimeList;
begin
  TimeList.Clear;
end;

function TTimeClockTranslator.CallTimesUsed: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to TimeList.Count - 1 do
    with TimeList.Items[I] as TActivity do begin
      if IsCallout and IsStartTime then
        Inc(Result);
    end;
end;

function TTimeClockTranslator.WorkTimesUsed: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to TimeList.Count - 1 do
    with TimeList.Items[I] as TActivity do begin
      if (not IsCallout) and IsStartTime then
        Inc(Result);
    end;
end;

{ TStringRowWriter }

procedure TStringRowWriter.AddRow(EmpID: Integer; WorkDate, ActivityTime: TDateTime; const ActivityType: string);
begin
  Buffer := Buffer + IntToStr(EmpID) + ':' + FormatDateTime('yyyymmdd', WorkDate) + ':' +
    FormatDateTime('hhnn', ActivityTime) + ':' + ActivityType + '|';
end;

function TStringRowWriter.AsString: string;
begin
  Result := Buffer;
end;

{ TStringDataSaver }

constructor TStringDataSaver.Create;
begin
  inherited;
  Clear;
end;

procedure TStringDataSaver.Clear;
begin
  Buffer := '';
end;

function TStringDataSaver.AsString: string;
begin
  Result := Buffer;
end;

procedure TStringDataSaver.ClearTimeclockTimes(EmpID: Integer; WorkDate: TDateTime);
begin
  Buffer := Buffer + 'C:clock:' + IntToStr(EmpID) + ':' + FormatDateTime('yyyymmdd', WorkDate) + '|';
end;

procedure TStringDataSaver.ClearTimesheetTimes(EmpID: Integer; WorkDate: TDateTime);
begin
  Buffer := Buffer + 'C:entry:' + IntToStr(EmpID) + ':' + FormatDateTime('yyyymmdd', WorkDate) + '|';
end;

procedure TStringDataSaver.SetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; WorkTimes,
  CalloutTimes: array of TTimeSpan; LunchStartTime: TDateTime);

  procedure AddTime(const AName: string; ATime: TDateTime; ADelim: char=':');
  begin
    if FloatEquals(ATime, 0) then
      Buffer := Buffer + AName + '=null' + ADelim
    else
      Buffer := Buffer + AName + '=' + FormatDateTime('hhnn', ATime) + ADelim;
  end;

var
  i: Integer;
begin
  Buffer := Buffer + 'I:' + IntToStr(EmpID) + ':' + FormatDateTime('yyyymmdd', WorkDate) + ':';
  for i := Low(WorkTimes) to High(WorkTimes) do begin
    AddTime('ws' + IntToStr(i+1), WorkTimes[i].StartTime);
    AddTime('we' + IntToStr(i+1), WorkTimes[i].StopTime);
  end;
  for i := Low(CalloutTimes) to High(CalloutTimes) do begin
    AddTime('cs' + IntToStr(i+1), CalloutTimes[i].StartTime);
    AddTime('ce' + IntToStr(i+1), CalloutTimes[i].StopTime);
  end;
  AddTime('ls', LunchStartTime, '|');
end;

function TStringDataSaver.GetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime;
  var WorkTimes, CalloutTimes: array of TTimeSpan;
  var LunchStartTime: TDateTime): Boolean;
var
  I: Integer;
begin
  // Should this do anything more useful?
  for I := Low(WorkTimes) to High(WorkTimes) do begin
    WorkTimes[I].StartTime := 0;
    WorkTimes[I].StopTime  := 0;
  end;
  for I := Low(CalloutTimes) to High(CalloutTimes) do begin
    CalloutTimes[I].StartTime := 0;
    CalloutTimes[I].StopTime  := 0;
  end;
  LunchStartTime := 0;
  Result := True;
end;

end.
