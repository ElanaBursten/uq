inherited TimesheetReportForm: TTimesheetReportForm
  Left = 346
  Top = 219
  Caption = 'TimesheetReportForm'
  ClientHeight = 266
  ClientWidth = 392
  PixelsPerInch = 96
  TextHeight = 13
  object PeriodLabel: TLabel [0]
    Left = 0
    Top = 11
    Width = 81
    Height = 17
    AutoSize = False
    Caption = 'Week Ending:'
    WordWrap = True
  end
  object ManagerLabel: TLabel [1]
    Left = 0
    Top = 43
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object EmployeeLabel: TLabel [2]
    Left = 0
    Top = 163
    Width = 50
    Height = 13
    Caption = 'Employee:'
  end
  object SortLabel: TLabel [3]
    Left = 0
    Top = 132
    Width = 55
    Height = 13
    Caption = 'Sort Order:'
  end
  object DepthLabel: TLabel [4]
    Left = 0
    Top = 98
    Width = 82
    Height = 13
    Caption = 'Hierarchy Depth:'
  end
  object lbIncludeEmployee: TLabel [5]
    Left = -1
    Top = 192
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [6]
    Left = 88
    Top = 40
    Width = 205
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
    OnChange = ManagersComboBoxChange
  end
  object LocatorComboBox: TComboBox [7]
    Left = 88
    Top = 160
    Width = 205
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 5
  end
  object SortCombo: TComboBox [8]
    Left = 88
    Top = 129
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    Items.Strings = (
      'short_name'
      'last_name'
      'emp_number')
  end
  object AllCheckBox: TCheckBox [9]
    Left = 88
    Top = 70
    Width = 282
    Height = 17
    Caption = 'Timesheets for all of this manager'#39's employees'
    TabOrder = 2
    OnClick = AllCheckBoxClick
  end
  object EmployeeStatusCombo: TComboBox [10]
    Left = 88
    Top = 189
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
  end
  object Depth: TSpinEdit [11]
    Left = 88
    Top = 95
    Width = 100
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 99
  end
  object ChooseDate: TcxDateEdit [12]
    Left = 88
    Top = 8
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = ChooseDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 100
  end
end
