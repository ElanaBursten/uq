inherited WorkOrderDetailReportForm: TWorkOrderDetailReportForm
  Left = 638
  Top = 239
  Caption = 'WorkOrderDetailReportForm'
  ClientHeight = 413
  ClientWidth = 501
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 100
    Height = 13
    Caption = 'Work Order Number:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 42
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object Label3: TLabel [2]
    Left = 0
    Top = 73
    Width = 71
    Height = 13
    Caption = 'Transmit Date:'
  end
  object LabelSelectImages: TLabel [3]
    Left = 102
    Top = 149
    Width = 134
    Height = 13
    Caption = 'Select attachments to print:'
  end
  object Label4: TLabel [4]
    Left = 276
    Top = 5
    Width = 123
    Height = 26
    Caption = 'Use the button to search for a work order to print.'
    WordWrap = True
  end
  object CallCenterEdit: TEdit [5]
    Left = 102
    Top = 39
    Width = 166
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 1
  end
  object IncludeNotes: TCheckBox [6]
    Left = 102
    Top = 98
    Width = 154
    Height = 17
    Caption = 'Include Work Order Notes'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object IncludeAttachments: TCheckBox [7]
    Left = 102
    Top = 122
    Width = 186
    Height = 17
    Caption = 'Include Work Order Attachments'
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = IncludeAttachmentsClick
  end
  object AttachmentList: TCheckListBox [8]
    Left = 102
    Top = 169
    Width = 285
    Height = 217
    ItemHeight = 13
    TabOrder = 5
  end
  object SelectAllButton: TButton [9]
    Left = 397
    Top = 169
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 6
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [10]
    Left = 397
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 7
    OnClick = ClearAllButtonClick
  end
  object TransmitDateEdit: TEdit [11]
    Left = 102
    Top = 70
    Width = 166
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 2
  end
  object WorkOrderNumberEdit: TcxButtonEdit [12]
    Left = 102
    Top = 8
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.ReadOnly = True
    Properties.OnButtonClick = WorkOrderNumberEditButtonClick
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 166
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 0
    Top = 318
  end
end
