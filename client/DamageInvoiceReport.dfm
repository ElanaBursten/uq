inherited DamageInvoiceReportForm: TDamageInvoiceReportForm
  Left = 575
  Top = 329
  Caption = 'DamageInvoiceReportForm'
  ClientWidth = 522
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 188
    Height = 13
    Caption = 'Damage Invoice Received Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 12
    Top = 86
    Width = 224
    Height = 13
    Caption = 'Clear the "To" date to indicate no ending date.'
    FocusControl = ReceivedDateRange
  end
  object ProfitCenterLabel: TLabel [2]
    Left = 0
    Top = 115
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object UtilityCoDamagedLabel: TLabel [3]
    Left = 0
    Top = 134
    Width = 85
    Height = 29
    AutoSize = False
    Caption = 'Invoice From Company:'
    WordWrap = True
  end
  object PaidDateRangeLabel: TLabel [4]
    Left = 256
    Top = 8
    Width = 122
    Height = 13
    Caption = 'Invoice Paid Date Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [5]
    Left = 0
    Top = 168
    Width = 73
    Height = 13
    Caption = 'Invoice Status:'
    WordWrap = True
  end
  inline ReceivedDateRange: TOdRangeSelectFrame [6]
    Left = 4
    Top = 22
    Width = 248
    Height = 63
    TabOrder = 0
  end
  object ProfitCenter: TComboBox
    Left = 87
    Top = 112
    Width = 141
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 1
  end
  object InvoiceCompany: TEdit
    Left = 87
    Top = 138
    Width = 140
    Height = 21
    TabOrder = 2
  end
  inline PaidDateRange: TOdRangeSelectFrame
    Left = 258
    Top = 21
    Width = 248
    Height = 63
    TabOrder = 3
  end
  object ShowEstimates: TCheckBox
    Left = 256
    Top = 142
    Width = 193
    Height = 14
    Caption = 'Show First and Last Estimates'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object ShowPaidAmount: TCheckBox
    Left = 256
    Top = 116
    Width = 193
    Height = 14
    Caption = 'Show Paid Amount'
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
  object InvoiceStatus: TComboBox
    Left = 87
    Top = 165
    Width = 141
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 6
    Text = 'All Invoices Received'
    OnChange = InvoiceStatusChange
    Items.Strings = (
      'All Invoices Received'
      'Received and Paid'
      'Received and Denied')
  end
end
