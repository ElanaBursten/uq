unit RouteOrderOverride;
{2/3/2023 EB QM-697 EB Resurrect Route Order - recording the reason a ticket was
                done out of order}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Dmu, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, StdCtrls, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCheckListBox, QMConst, RouteOrderMgt, Buttons;

type

  TRouteOrderOVerrideFrm = class(TForm)
    lblReason: TLabel;
    cmboReason: TcxComboBox;
    btnApply: TButton;
    lblNumberTicketsSkipped: TLabel;
    lblInstructions: TLabel;
    TicketsSkipped: TLabel;
    RouteCheckList: TcxCheckListBox;
    btnClear: TButton;
    lblProvideReasonCount: TLabel;
    btnDone: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure cmboReasonPropertiesChange(Sender: TObject);
    procedure RouteCheckListClickCheck(Sender: TObject; AIndex: Integer;
      APrevState, ANewState: TcxCheckBoxState);
    procedure btnClearClick(Sender: TObject);
    procedure btnDoneClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    fLocalEarlierRoute: TRouteOrderOverrideRec;
    fEarlierRouteArray: TRouteArray;
    fCheckedItems: integer;
    fAllChecked: boolean;
    fWaitingForAssignment: boolean;
    fParentTicketID: integer;
    fParentTicketNum: string;
    procedure FillReasonCombo;
    function CheckValidSelection: boolean;
    function ResetItem(Item: Integer): string;
  public
    procedure LoadTicketInfoRec(ATicketID: Integer; TicketNumber: string; AnEarlierRoute: TRouteOrderOverrideRec);
    procedure LoadTicketInfo(ATicketID: Integer; TicketNumber: string; TotalNumberOfTicketsSkipped: integer; EarlierRoutes: TRouteArray);
    procedure SaveRouteOrderToCache;
    procedure SaveRouteOrderNotesToCache;
    procedure CloseForm;
  end;

const
  RouteOrd = 'ROUTEORD';  //QMANTWO-775 EB ticket_info.info_type for this functionality
  Instructions = 'Ticket # %S was done out of order. Please provide reason for skipping the following ticket(s).';
var
  RouteOrderOVerrideFrm: TRouteOrderOVerrideFrm;

implementation

{$R *.dfm}

{ TRouteOrderOVerrideFrm }

procedure TRouteOrderOVerrideFrm.btnCancelClick(Sender: TObject);
begin
//qm-723   sr
end;

procedure TRouteOrderOVerrideFrm.btnClearClick(Sender: TObject);
var
  i: integer;
begin
  {clear all reason codes in our array}
  for i := 0 to maxRORec do begin
    fEarlierRouteArray[i].ReasonCode := '';
    fEarlierRouteArray[i].ReasonStr := '';
  end;

  for i := 0 to RouteCheckList.Items.Count - 1 do begin
    RouteCheckList.Items[i].Text := ResetItem(i);
    RouteCheckList.Items[i].Checked := False;
    RouteCheckList.Items[i].Enabled := True;
  end;

  btnClear.Enabled := False;
  btnApply.Enabled := False;
  btnDone.Enabled := False;

end;

procedure TRouteOrderOVerrideFrm.btnDoneClick(Sender: TObject);
begin
  if self.ModalResult<> mrCancel then
  CloseForm;
end;

procedure TRouteOrderOVerrideFrm.btnApplyClick(Sender: TObject);
var
  CurRefCode, CurRefStr: string;
  i: integer;
  PriorityRefID:integer;
begin
  CurRefCode := DM.GetRefCodeForDisplay('routeord', cmboReason.Text);
  CurRefStr := cmboReason.Text;
  for i := 0 to RouteCheckList.Items.Count - 1 do begin
    if (RouteCheckList.Items[i].Checked) and (RouteCheckList.Items[i].Enabled) then begin
      fEarlierRouteArray[i].ReasonCode := CurRefCode;
      fEarlierRouteArray[i].ReasonStr := CurRefStr;
//      PriorityRefID:=2106; //qm-720  sr    look up from ref table.
      PriorityRefID := DM.GetTicketPriorityIDByCode(TicketPrioritySkipped); //QM-720 Route Order Skipped EB
      DM.SetTicketPriority(fEarlierRouteArray[i].TicketID, PriorityRefID ); //qm-720  sr
      RouteCheckList.Items[i].Text := FloatToStr(fEarlierRouteArray[i].OrderNum) + ': ' + fEarlierRouteArray[i].TicketNum  + ' -> ' + CurRefStr;
      //RouteCheckList.Items[i].Text + ' -> ' + CurRefStr;
      RouteCheckList.Items[i].Enabled := False;
    end;
  end;
  btnApply.Enabled := False;  {Assignments have been made for now}
  fWaitingForAssignment := False;

  if fAllChecked then
    btnDone.Enabled := True;
//  btnCancel.Visible:= true;
end;

function TRouteOrderOVerrideFrm.CheckValidSelection: boolean;
var
  i: Integer;
begin
  fWaitingForAssignment := False;
  fCheckedItems := 0;
  fAllChecked := False;
  for i := 0 to (RouteCheckList.Items.Count - 1) do begin

    if (RouteCheckList.Items[i].Checked) then begin
      Inc(fCheckedItems);
      if RouteCheckList.Items[i].Enabled then
        fWaitingForAssignment := True;
    end;
  end;

  if (fCheckedItems > 0) and (cmboReason.Text <> '') then begin
    btnApply.Enabled := True;
    btnClear.Enabled := True;
    if (fCheckedItems = (RouteCheckList.Items.Count)) then begin //begin
      fAllChecked := True;
      if (fAllChecked) and (not fWaitingForAssignment) then
        btnDone.Enabled := True
      else
        btnDone.Enabled := False;
    end;
  end
  else begin
    btnApply.Enabled := False;
    btnClear.Enabled := False;
    btnDone.Enabled := False;
  end;
  Result := btnApply.Enabled;
end;

procedure TRouteOrderOVerrideFrm.CloseForm;
begin
  if fAllChecked then begin
    SaveRouteOrderToCache;
    SaveRouteOrderNotesToCache;
  end;
end;

procedure TRouteOrderOVerrideFrm.cmboReasonPropertiesChange(Sender: TObject);
begin
  CheckValidSelection;
end;

procedure TRouteOrderOVerrideFrm.FillReasonCombo;
begin
  DM.GetRefDisplayList('routeord', cmboReason.Properties.Items);
end;

procedure TRouteOrderOVerrideFrm.FormCreate(Sender: TObject);
begin
  fCheckedItems := 0;
  FillReasonCombo;
end;

procedure TRouteOrderOVerrideFrm.LoadTicketInfo(ATicketID: Integer;
  TicketNumber: string; TotalNumberOfTicketsSkipped: integer; EarlierRoutes: TRouteArray);
var
  i: integer;
  TicketItem: string;
  TID: string;
  ShortDateStr: string;
begin
  fParentTicketID := ATicketID;
  fParentTicketNum := TicketNumber;
  TID := TicketNumber + ' (' + IntToStr(ATicketID) + ')';
  lblInstructions.Caption := format(Instructions, [TID]);
  if (TotalNumberOfTicketsSkipped > 5) then
    lblProvideReasonCount.Visible := True
  else
    lblProvideReasonCount.Visible := False;

  TicketsSkipped.Caption := IntToStr(TotalNumberOfTicketsSkipped);
//  fEarlierRouteArray := copy(EarlierRoutes,0,4);

  for i := 0 to 4 do begin
    fEarlierRouteArray[i].Found := EarlierRoutes[i].Found;
    fEarlierRouteArray[i].OrderNum := EarlierRoutes[i].OrderNum;
    fEarlierRouteArray[i].TicketID := EarlierRoutes[i].TicketID;
    fEarlierRouteArray[i].TicketNum := EarlierRoutes[i].TicketNum;
    fEarlierRouteArray[i].DueDate := EarlierRoutes[i].DueDate;
    fEarlierRouteArray[i].ReasonCode := '';
    fEarlierRouteArray[i].ReasonStr := '';

    ShortDateStr := formatdatetime('m/d/yy hh:mm am/pm', fEarlierRouteArray[i].DueDate);

    if fEarlierRouteArray[i].Found then begin
      TicketItem := ResetItem(i);
      RouteCheckList.AddItem(TicketItem);
    end;
  end;
end;

procedure TRouteOrderOVerrideFrm.LoadTicketInfoRec(ATicketID: Integer; TicketNumber: string; AnEarlierRoute: TRouteOrderOverrideRec);
var
  TID: string;
begin
  TID := TicketNumber + ' (' + IntToStr(ATicketID) + ')';

  fLocalEarlierRoute := AnEarlierRoute;
end;

function TRouteOrderOVerrideFrm.ResetItem(Item: Integer): string;
var
  ShortDateStr: string;
begin
  ShortDateStr := formatdatetime('m/d/yy hh:mm am/pm', fEarlierRouteArray[Item].DueDate);
  Result := floattoStr(fEarlierRouteArray[Item].OrderNum) + ': ' + fEarlierRouteArray[Item].TicketNum  + ', Due: ' + ShortDateStr;

  {If the list has already been loaded, then reset it}
  if RouteCheckList.Items.Count > Item then
    RouteCheckList.Items[Item].Text := Result;
end;

procedure TRouteOrderOVerrideFrm.RouteCheckListClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  CheckValidSelection;
end;

procedure TRouteOrderOVerrideFrm.SaveRouteOrderNotesToCache;
var
  RouteText: string;
  i: integer;
begin
  if RouteCheckList.Items.Count > 0 then begin
    RouteText := ROPrefix + TicketsSkipped.Caption + ' ticket(s) had an earlier route order';
    DM.SaveNote(RouteText, 'RO', PrivateTicketSubType, qmftTicket, fParentTicketID, False);

    for i := 0 to RouteCheckList.Items.Count-1 do begin
      RouteText := NoteROSkipped;   //qm-747 sr
      RouteText := RouteText + ' ';
      RouteText := RouteText + ' - ' + fEarlierRouteArray[i].ReasonStr;
      RouteText := RouteText + ' By Ticket Number: ' + fParentTicketNum;  //qm-747 sr
//      showmessage('RouteText: '+RouteText+' '+ inttostr(fEarlierRouteArray[i].TicketID));
      DM.SaveNoteToServer(fEarlierRouteArray[i].TicketID, RouteText, PrivateTicketSubType, dm.EmpID); //qm-747 sr
    end;
  end;
end;

procedure TRouteOrderOVerrideFrm.SaveRouteOrderToCache;
var
  i: integer;
  RO: TRouteOrderOverrideRec;
begin
  for i := 0 to maxRORec do begin

    RO := fEarlierRouteArray[i];
    if (RO.Found) and (RO.ReasonCode <> '') then
      SetRouteOrderReason(fParentTicketID, fParentTicketNum, RO);
  end;

end;

end.
