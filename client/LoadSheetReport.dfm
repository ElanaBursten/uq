inherited LoadSheetReportForm: TLoadSheetReportForm
  Left = 283
  Top = 188
  Caption = 'Load Sheet'
  ClientHeight = 573
  ClientWidth = 454
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 1
    Top = 11
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label3: TLabel [1]
    Left = 1
    Top = 60
    Width = 45
    Height = 13
    Caption = 'Locators:'
  end
  object Label6: TLabel [2]
    Left = 0
    Top = 35
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [3]
    Left = 88
    Top = 8
    Width = 233
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 0
    TabOrder = 0
    OnChange = ManagersComboBoxChange
  end
  object LocatorListBox: TListBox [4]
    Left = 88
    Top = 56
    Width = 252
    Height = 509
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 2
  end
  object EmployeeStatusCombo: TComboBox [5]
    Left = 88
    Top = 32
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 1
    OnChange = EmployeeStatusComboChange
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
    Top = 372
  end
end
