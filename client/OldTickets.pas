unit OldTickets;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OdEmbeddable, Buttons, StdCtrls, ExtCtrls, DMu, Db,
  DBISAMTb, QMConst, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, 
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxCalendar, cxMemo,
  cxTextEdit, cxGrid, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel, cxNavigator,
  cxContainer, cxDropDownEdit, cxButtonEdit, ActnList, cxFilter,
  cxData;

type
  TOldTicketsForm = class(TEmbeddableForm)
    OldTickets: TDBISAMQuery;
    TicketLocates: TDBISAMTable;
    TicketSource: TDataSource;
    HeaderPanel: TPanel;
    RefreshButton: TButton;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    GridView: TcxGridDBTableView;
    ColTicketID: TcxGridDBColumn;
    ColTicketNumber: TcxGridDBColumn;
    ColKind: TcxGridDBColumn;
    ColDueDate: TcxGridDBColumn;
    ColTicketType: TcxGridDBColumn;
    ColWorkDescription: TcxGridDBColumn;
    ColCompany: TcxGridDBColumn;
    ColPriority: TcxGridDBColumn;
    ColWorkType: TcxGridDBColumn;
    ColWorkAddressNumber: TcxGridDBColumn;
    ColWorkAddressStreet: TcxGridDBColumn;
    ColLegalGoodThru: TcxGridDBColumn;
    ColWorkCity: TcxGridDBColumn;
    ColMapPage: TcxGridDBColumn;
    ColWorkCounty: TcxGridDBColumn;
    ColTicketFormat: TcxGridDBColumn;
    ColLocates: TcxGridDBColumn;
    BtnFind: TcxButtonEdit;
    OldTicketsActionList: TActionList;
    SearchAction: TAction;
    ClearSearchAction: TAction;
    btnWhatRow: TButton;
    procedure FormCreate(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RefreshButtonClick(Sender: TObject);
    procedure OldTicketsAfterScroll(DataSet: TDataSet);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ColLocatesGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure GridViewDataControllerGroupingChanged(Sender: TObject);
    procedure GridViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ColCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ColTicketNumberCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure BtnFindEditing(Sender: TObject; var CanEdit: Boolean);
    procedure SearchActionExecute(Sender: TObject);
    procedure ClearSearchActionExecute(Sender: TObject);
    procedure btnWhatRowClick(Sender: TObject);
  private
    FOnItemSelected: TItemSelectedEvent;
    FOnSelectTicket: TItemSelectedEvent;
    DuringSearch: Boolean;
    procedure RefreshTickets;
    procedure DoItemSelected;
    function GetTicketIDForRecord(Rec: TcxCustomGridRecord; var TicketID: Integer): Boolean;
    procedure SetDebugVisibleColumns;
    function GetFirstRecordIndexByText(AColumnIndex: Integer; SearchTxt: string): Integer;
    function FieldFind(FieldValue: string): integer;
  public
    function GetCurrentTicketID(var TicketID: Integer): Boolean;
    property OnItemSelected: TItemSelectedEvent read FOnItemSelected
                                                write FOnItemSelected;
    property OnSelectTicket: TItemSelectedEvent read FOnSelectTicket
                                                write FOnSelectTicket;
  end;

implementation

{$R *.DFM}

uses
  OdHourglass, UQUtils, LocalEmployeeDMu;

function TOldTicketsForm.FieldFind(FieldValue: string): integer;
var
  ColIndex: integer;
begin
  if BtnFind.Text = '' then
    Exit;
  
  try
    GridView.DataController.GotoFirst;
    DuringSearch := True;
    for ColIndex := 0 to GridView.ColumnCount-1 do begin

    //  Found := GridView.DataController.Search.Locate(ColIndex, FieldValue);
//      CurrentFocusedRecord := GridView.DataController.FocusedRecordIndex;
//      GridView.DataController.Search.Cancel;
//      if Found then begin
//        GridView.Controller.SelectCells(ColIndex, ColIndex, CurrentFocusedRecord, CurrentFocusedRecord);
//        GridView.Controller.FocusedColumn := ColTicketNumber;
//      end;
//      Result := -1; // Row was not found
      if GridView.Columns[ColIndex].Visible then
        GetFirstRecordIndexByText(ColIndex, BtnFind.Text);
    end;
  finally
    RefreshTickets;
 //   DuringSearch := False;
  end;
end;


function TOldTicketsForm.GetFirstRecordIndexByText(AColumnIndex: integer; SearchTxt: string): Integer;
var
  I: Integer;
  CompareToText, uSearchTxt: string;
begin
  Result := - 1;
//  for i := 0 to GridView.DataController.RowCount - 1 do begin
  for i := 0 to GridView.DataController.RecordCount - 1 do begin

//  if GridView.Cont then

    uSearchTxt := SearchTxt;

    CompareToText := GridView.DataController.DisplayTexts[i, AColumnIndex];
    if AnsiPos(uSearchTxt, CompareToText) > 0 then begin
      Result := i;
      GridView.Controller.FocusRecord(i, False);
      GridView.Controller.FocusedRecord.ViewInfo.Invalidate;
      Break;

    end;
  end;
end;




procedure TOldTicketsForm.FormCreate(Sender: TObject);
begin
  DuringSearch := False;
  RefreshTickets;
end;

procedure TOldTicketsForm.BtnFindEditing(Sender: TObject; var CanEdit: Boolean);
begin
  inherited;
  DuringSearch := False;
end;

procedure TOldTicketsForm.btnWhatRowClick(Sender: TObject);
var
  RecIdx, RowID: Integer;
begin
  inherited;
  RecIdx := GridView.Controller.FocusedRecordIndex;
  RowID := GridView.Controller.FocusedRowIndex;
  ShowMessage('RECORD: ' + IntToStr(RecIdx) + ' | ROW: ' + IntToStr(RowID));
end;

procedure TOldTicketsForm.ClearSearchActionExecute(Sender: TObject);
begin
  inherited;
  BtnFind.Clear;
  RefreshTickets;
end;

procedure TOldTicketsForm.ColCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  var
  UFindTxt, UDisplayTxt: string;
begin
  inherited;
  //ColTicketNumber.Index])
  try
  if (DuringSearch) and (BtnFind.Text <> '') then begin   //and (BtnFind.Text <> '')
    uFindTxt := Uppercase(BtnFind.Text);
    uDisplayTxt := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index];
    if AnsiPos(uFindTxt, uDisplayTxt ) > 0  then begin

      ACanvas.Brush.Color := clWebLemonChiffon;
      ACanvas.Font.Color := clBlack;
    end;
  end;
  finally
    {Swallow}
  end;
end;

procedure TOldTicketsForm.DoItemSelected;
var
  TicketID: Integer;
begin
  if GetCurrentTicketID(TicketID) then
    if Assigned(FOnItemSelected) then
      FOnItemSelected(Self, TicketID);
end;

procedure TOldTicketsForm.RefreshTickets;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeFindTicket, '');

  OldTickets.Close;
  OldTickets.Params[0].AsInteger := DM.EmpID;
  OldTickets.Open;

  TicketLocates.Close;
  TicketLocates.Open;
  GridView.ViewData.Expand(True);
end;

procedure TOldTicketsForm.SearchActionExecute(Sender: TObject);
begin
  inherited;
  FieldFind(BtnFind.Text);
end;

procedure TOldTicketsForm.SetDebugVisibleColumns;
begin
  if (DM.UQState.SecondaryDebugOn) and (DM.UQState.DebugWorkMgmtDebug) then begin  //QM-805 EB   //QM-131 EB - Fields Available to view in Debug Mode
    ColTicketID.VisibleForCustomization := True;
    ColTicketFormat.VisibleForCustomization := True;
    ColLegalGoodThru.VisibleForCustomization := True;
    ColWorkType.VisibleForCustomization := True;
    ColPriority.VisibleForCustomization := True;
    ColKind.VisibleForCustomization := True;
  end;
end;

procedure TOldTicketsForm.GridDblClick(Sender: TObject);
begin
  DoItemSelected;
end;

function TOldTicketsForm.GetCurrentTicketID(var TicketID: Integer): Boolean;
begin
  Result := GetTicketIDForRecord(GridView.Controller.FocusedRecord, TicketID);
end;

procedure TOldTicketsForm.FormShow(Sender: TObject);
begin
  if Grid.CanFocus then
    Grid.SetFocus;
end;

function TOldTicketsForm.GetTicketIDForRecord(Rec: TcxCustomGridRecord;
  var TicketID: Integer): Boolean;
begin
  Result := False;
  TicketID := -1;
  if (Rec = nil) or (Rec is TcxGridGroupRow) then
    Exit;
  TicketID := Rec.Values[ColTicketID.Index];
  Result := True;
end;

procedure TOldTicketsForm.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = Ord(' ')) or (Key = VK_RETURN) then
    DoItemSelected;
end;

procedure TOldTicketsForm.RefreshButtonClick(Sender: TObject);
begin
  RefreshTickets;
end;

procedure TOldTicketsForm.OldTicketsAfterScroll(DataSet: TDataSet);
begin
  if Assigned(FOnSelectTicket) then
    FOnSelectTicket(Self, DataSet.FieldByName('ticket_id').AsInteger);
end;

procedure TOldTicketsForm.GridViewColumnHeaderClick(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  GridView.OptionsBehavior.IncSearchItem := AColumn;
end;

procedure TOldTicketsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  if AViewInfo.Selected then
    Exit;

//  if AViewInfo.RecordViewInfo.Index mod 2 = 1 then
//    ACanvas.Brush.Color := DM.UQState.ShadeColor;
  try

  //if AnsiPos(BtnFind.Text, AViewInfo.GridRecord.Values[ColTicketNumber.Index]) > 0  then
//    ACanvas.Brush.Color := clCream;

  finally
    {Swallow}
  end;

end;

procedure TOldTicketsForm.ColLocatesGetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: String);
var
  TicketID: Integer;
begin
  if GetTicketIDForRecord(ARecord, TicketID) then
    AText := GetLocatesStringForTicket(TicketLocates, TicketID);
end;

procedure TOldTicketsForm.ColTicketNumberCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
      if AnsiPos(BtnFind.Text, AViewInfo.GridRecord.Values[AViewInfo.Item.Index]) > 0  then begin
      ACanvas.Brush.Color := clWebLemonChiffon;
      ACanvas.Font.Color := clBlack;
    end;
end;

procedure TOldTicketsForm.GridViewDataControllerGroupingChanged(Sender: TObject);
begin
  GridView.ViewData.Expand(True);
end;

end.

