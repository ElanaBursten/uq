inherited BillingMainForm: TBillingMainForm
  Left = 302
  Top = 180
  Caption = 'Billing'
  ClientHeight = 460
  ClientWidth = 775
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BillingPages: TPageControl
    Left = 0
    Top = 0
    Width = 775
    Height = 460
    ActivePage = RunTab
    Align = alClient
    TabOrder = 0
    object AdjustmentsTab: TTabSheet
      Caption = 'Adjustments'
      OnShow = AdjustmentsTabShow
      object BillingAdjustmentTabs: TTabControl
        Left = 0
        Top = 0
        Width = 767
        Height = 432
        Align = alClient
        Style = tsButtons
        TabHeight = 24
        TabOrder = 0
        Tabs.Strings = (
          'Adjustment Search'
          'New Adjustment')
        TabIndex = 0
        TabWidth = 120
        OnChange = BillingAdjustmentTabsChange
        object BillingAdjustmentSearchTab: TTabSheet
          Caption = 'Search Adjustment'
        end
        object BillingAdjustmentDetailTab: TTabSheet
          Caption = 'New Adjustment'
          ImageIndex = 1
        end
      end
    end
    object RunTab: TTabSheet
      Caption = 'Queue'
      ImageIndex = 1
      OnShow = RunTabShow
      object RunBillingHeaderPanel: TPanel
        Left = 0
        Top = 0
        Width = 767
        Height = 189
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          767
          189)
        object RefreshButton: TButton
          Left = 6
          Top = 156
          Width = 122
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Refresh Status Display'
          TabOrder = 1
          OnClick = RefreshButtonClick
        end
        object ViewLogButton: TButton
          Left = 137
          Top = 156
          Width = 122
          Height = 25
          Action = ViewBillingRunLog
          Anchors = [akLeft, akBottom]
          TabOrder = 2
        end
        object RunBox: TGroupBox
          Left = 6
          Top = 4
          Width = 723
          Height = 145
          Anchors = [akLeft, akTop, akBottom]
          Caption = 'Run Billing'
          TabOrder = 0
          DesignSize = (
            723
            145)
          object CenterGroupLabel: TLabel
            Left = 6
            Top = 18
            Width = 62
            Height = 13
            Alignment = taRightJustify
            Caption = 'Call Centers:'
          end
          object Label6: TLabel
            Left = 400
            Top = 45
            Width = 65
            Height = 13
            Caption = 'Period Dates:'
          end
          object Label2: TLabel
            Left = 400
            Top = 21
            Width = 80
            Height = 13
            AutoSize = False
            Caption = 'Period Type:'
          end
          object Label3: TLabel
            Left = 574
            Top = 42
            Width = 4
            Height = 13
            Caption = '-'
          end
          object Label4: TLabel
            Left = 400
            Top = 69
            Width = 80
            Height = 13
            AutoSize = False
            Caption = 'Look Back To:'
          end
          object PeriodStarting: TLabel
            Left = 478
            Top = 46
            Width = 56
            Height = 13
            Caption = '--------------'
          end
          object PeriodLookBackDate: TLabel
            Left = 478
            Top = 70
            Width = 56
            Height = 13
            Caption = '--------------'
          end
          object RunBillingButton: TButton
            Left = 476
            Top = 93
            Width = 122
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Queue Billing Run'
            TabOrder = 3
            OnClick = RunBillingButtonClick
          end
          object CenterGroupListBox: TCheckListBox
            Left = 8
            Top = 34
            Width = 361
            Height = 103
            Anchors = [akLeft, akTop, akBottom]
            Columns = 4
            ItemHeight = 13
            TabOrder = 0
          end
          object PeriodTypeCombo: TComboBox
            Left = 476
            Top = 18
            Width = 93
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = PeriodTypeComboChange
            Items.Strings = (
              'WEEK'
              'WEEK_A'
              'WEEK_B'
              'WEEK_C'
              'WEEK_D'
              'MONTH'
              'MONTH_A'
              'MONTH_B'
              'MONTH_C'
              'MONTH_D'
              'HALF')
          end
          object PeriodEndingDate: TcxDateEdit
            Left = 586
            Top = 41
            Properties.SaveTime = False
            Properties.ShowTime = False
            Properties.OnEditValueChanged = PeriodEndingDateChange
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 2
            Width = 116
          end
        end
        object CancelRunButton: TButton
          Left = 266
          Top = 156
          Width = 122
          Height = 25
          Action = CancelBillingRun
          Anchors = [akLeft, akBottom]
          TabOrder = 3
        end
      end
      object QueueGrid: TcxGrid
        Left = 0
        Top = 189
        Width = 767
        Height = 243
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        object QueueView: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = QueueDS
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'run_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
        end
        object QueueLevel: TcxGridLevel
          GridView = QueueView
        end
      end
    end
    object ReviewTab: TTabSheet
      Caption = 'Review'
      ImageIndex = 2
      OnShow = ReviewTabShow
    end
    object BillingLogTab: TTabSheet
      Caption = 'Billing Process Log'
      ImageIndex = 3
      TabVisible = False
      object BillingProcessLog: TMemo
        Left = 0
        Top = 0
        Width = 767
        Height = 432
        Align = alClient
        Lines.Strings = (
          'BillingProcessLog')
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
  end
  object QueueDS: TDataSource
    DataSet = QueueTable
    Left = 68
    Top = 236
  end
  object QueueTable: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    Exclusive = True
    Left = 112
    Top = 236
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 28
    Top = 236
    object ViewBillingRunLog: TAction
      Category = 'Queue'
      Caption = 'View Billing Run Log'
      OnExecute = ViewBillingRunLogExecute
    end
    object CancelBillingRun: TAction
      Category = 'Queue'
      Caption = 'Cancel Billing Run'
      OnExecute = CancelBillingRunExecute
    end
  end
end
