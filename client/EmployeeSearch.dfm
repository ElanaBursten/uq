inherited EmployeeSearchForm: TEmployeeSearchForm
  Caption = 'Employee Search'
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited HeaderPanel: TPanel
    inherited RecordsLabel: TLabel
      Top = 61
    end
    object RoleLabel: TLabel [1]
      Left = 8
      Top = 87
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Role:'
    end
    object ManagerLabel: TLabel [2]
      Left = 8
      Top = 63
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Manager:'
    end
    object EmpNumLabel: TLabel [3]
      Left = 8
      Top = 39
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Employee #:'
    end
    object NameLabel: TLabel [4]
      Left = 8
      Top = 15
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Name:'
    end
    inherited SearchButton: TButton
      Top = 82
      TabOrder = 5
    end
    inherited CancelButton: TButton
      Top = 82
      TabOrder = 6
    end
    inherited CopyButton: TButton
      Top = 82
      TabOrder = 7
    end
    inherited ExportButton: TButton
      Top = 82
      TabOrder = 8
    end
    object ManagersOnlyCheck: TCheckBox
      Left = 343
      Top = 14
      Width = 305
      Height = 17
      Caption = 'Show managers only'
      TabOrder = 4
      Visible = False
    end
    object ShortName: TEdit
      Left = 80
      Top = 12
      Width = 161
      Height = 21
      TabOrder = 0
    end
    object EmpNumber: TEdit
      Left = 80
      Top = 36
      Width = 161
      Height = 21
      TabOrder = 1
    end
    object Manager: TEdit
      Left = 80
      Top = 60
      Width = 161
      Height = 21
      TabOrder = 2
    end
    object EmpType: TComboBox
      Left = 80
      Top = 85
      Width = 161
      Height = 21
      Style = csDropDownList
      DropDownCount = 16
      ItemHeight = 0
      TabOrder = 3
    end
    object btnClear: TButton
      Left = 259
      Top = 10
      Width = 75
      Height = 23
      Caption = 'Clear'
      TabOrder = 9
      OnClick = btnClearClick
    end
  end
end
