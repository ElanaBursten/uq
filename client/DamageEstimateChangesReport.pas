unit DamageEstimateChangesReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect;

type
  TDamageEstimateChangesReportForm = class(TReportBaseForm)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ModifiedDate: TOdRangeSelectFrame;
    DamageDate: TOdRangeSelectFrame;
    NotifiedDate: TOdRangeSelectFrame;
    Label1: TLabel;
    Label2: TLabel;
    LiabilityLabel: TLabel;
    State: TComboBox;
    ProfitCenter: TComboBox;
    Liability: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdExceptions, QMConst;

{$R *.dfm}

{ TDamageEstimateChangesReportForm }

procedure TDamageEstimateChangesReportForm.ValidateParams;
begin
  inherited;
  if Length(State.Text) = 1 then begin
    State.SetFocus;
    raise EOdEntryRequired.Create('Please enter a valid state abbreviation.');
  end;

  SetReportID('DamageEstimateChanges');
  SetParamDate('DateFrom', ModifiedDate.FromDate);
  SetParamDate('DateTo', ModifiedDate.ToDate);
  if not DamageDate.IsEmpty then begin
    SetParamDate('DamageDateFrom', DamageDate.FromDate);
    SetParamDate('DamageDateTo', DamageDate.ToDate);
  end;
  if not NotifiedDate.IsEmpty then begin
    SetParamDate('NotifiedDateFrom', NotifiedDate.FromDate);
    SetParamDate('NotifiedDateTo', NotifiedDate.ToDate);
  end;
  SetParam('State', State.Text);
  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParamInt('Liability', Liability.ItemIndex);
end;

procedure TDamageEstimateChangesReportForm.InitReportControls;
var
  i: TDamageLiability;
begin
  inherited;

  ModifiedDate.SelectDateRange(rsPreviousMonth);
  DamageDate.NoDateRange;
  NotifiedDate.NoDateRange;
  DM.GetRefDisplayList('state', State.Items);
  DM.ProfitCenterList(ProfitCenter.Items);
  Liability.Items.Clear;
  for i := Low(TDamageLiability) to High(TDamageLiability) do
    Liability.Items.Add(DamageLiabilityDescription[i]);
  Liability.ItemIndex := Liability.Items.Count-1;
end;

end.
