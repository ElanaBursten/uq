inherited DamageInvoiceSearchCriteria: TDamageInvoiceSearchCriteria
  Left = 302
  Top = 238
  Caption = 'Damage Invoice Search'
  ClientHeight = 172
  ClientWidth = 619
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  PixelsPerInch = 96
  TextHeight = 13
  object CompanyLabel: TLabel [0]
    Left = 15
    Top = 36
    Width = 49
    Height = 13
    Caption = 'Company:'
  end
  object DamageCityLabel: TLabel [1]
    Left = 15
    Top = 60
    Width = 65
    Height = 13
    Caption = 'Damage City:'
  end
  object DamageStateLabel: TLabel [2]
    Left = 15
    Top = 84
    Width = 72
    Height = 13
    Caption = 'Damage State:'
  end
  object DamageIDLabel: TLabel [3]
    Left = 15
    Top = 108
    Width = 57
    Height = 13
    Caption = 'Damage ID:'
  end
  object InvoiceNumberLabel: TLabel [4]
    Left = 15
    Top = 12
    Width = 79
    Height = 13
    Caption = 'Invoice Number:'
  end
  object AmountLabel: TLabel [5]
    Left = 15
    Top = 132
    Width = 41
    Height = 13
    Caption = 'Amount:'
  end
  object CommentLabel: TLabel [6]
    Left = 296
    Top = 12
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comment:'
  end
  inherited RecordsLabel: TLabel
    Left = 99
    Top = 155
    Width = 113
  end
  inherited SearchButton: TButton
    Left = 251
    Top = 140
    TabOrder = 9
  end
  inherited CancelButton: TButton
    Left = 563
    Top = 140
    TabOrder = 12
  end
  inherited CopyButton: TButton
    Left = 329
    Top = 140
    TabOrder = 10
  end
  inherited ExportButton: TButton
    Left = 407
    Top = 140
    TabOrder = 11
  end
  inherited PrintButton: TButton
    TabOrder = 13
  end
  inherited ClearButton: TButton
    Left = 484
    Top = 140
    TabOrder = 14
  end
  object Company: TEdit
    Left = 98
    Top = 32
    Width = 148
    Height = 21
    TabOrder = 1
  end
  object DamageCity: TEdit
    Left = 98
    Top = 56
    Width = 148
    Height = 21
    TabOrder = 2
  end
  object DamageState: TComboBox
    Left = 98
    Top = 80
    Width = 148
    Height = 21
    DropDownCount = 14
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 3
    Items.Strings = (
      ''
      'Open'
      'Closed')
  end
  object UQDamageID: TEdit
    Left = 98
    Top = 104
    Width = 148
    Height = 21
    TabOrder = 4
  end
  object InvoiceNumber: TEdit
    Left = 98
    Top = 8
    Width = 148
    Height = 21
    TabOrder = 0
  end
  object Amount: TEdit
    Left = 99
    Top = 128
    Width = 146
    Height = 21
    TabOrder = 5
  end
  object Comment: TEdit
    Left = 379
    Top = 8
    Width = 198
    Height = 21
    TabOrder = 6
  end
  inline ReceivedDate: TOdRangeSelectFrame
    Left = 292
    Top = 29
    Width = 285
    Height = 48
    TabOrder = 7
    inherited FromLabel: TLabel
      Left = 4
      Top = 6
      Width = 74
      Alignment = taRightJustify
      Caption = 'Received Date:'
    end
    inherited ToLabel: TLabel
      Left = 178
      Top = 6
    end
    inherited DatesLabel: TLabel
      Left = 4
      Top = 29
    end
    inherited DatesComboBox: TComboBox
      Left = 87
      Top = 25
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 87
      Top = 2
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 199
      Top = 2
    end
  end
  inline InvoiceDate: TOdRangeSelectFrame
    Left = 292
    Top = 81
    Width = 285
    Height = 48
    TabOrder = 8
    inherited FromLabel: TLabel
      Left = 4
      Top = 3
      Width = 65
      Alignment = taRightJustify
      Caption = 'Invoice Date:'
    end
    inherited ToLabel: TLabel
      Left = 178
      Top = 6
    end
    inherited DatesLabel: TLabel
      Left = 4
      Top = 29
    end
    inherited DatesComboBox: TComboBox
      Left = 87
      Top = 25
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 87
      Top = 2
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 199
      Top = 2
    end
  end
end
