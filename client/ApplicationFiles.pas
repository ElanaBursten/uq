unit ApplicationFiles;

interface

uses
  StateMgr;

function GetClientLocalDir: string;
function GetClientDataLocalDir(const DataDir: string = 'data'): string;
function GetClientAddinsLocalDir: string;
function GetClientAttachmentsLocalDir: string;
function GetClientAttachmentsDownloadDir(ApplicationName: string): string;
function GetClientExportsLocalDir: string;
function GetClientServerSpecificLocalDir: string;
function GetClientAddinsFile: string;
procedure ConfigureDataAndExecuteApp(State: TUQState);

implementation

uses SysUtils, OdMiscUtils, ShellAPI, Classes, Forms, JclSysInfo, Windows,
  OdInternetUtil, JclStrings, QMConst, JclFileUtils;

var
  UQState: TUQState;

function GetClientDataLocalDir(const DataDir: string): string;
begin
  Assert(Assigned(UQState));
  if UQState.AppLocal then
    Result := MakePath(GetClientServerSpecificLocalDir, 'Data')
  else
    Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + DataDir;
end;

function GetClientExportsLocalDir: string;
begin
  if UQState.AppLocal then
    Result := MakePath(GetClientServerSpecificLocalDir, 'Exports')
  else
    Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'export';
end;

function GetServerDatabaseIdentifier: string;
begin
  Result := GetURLPath(UQState.ServerURL + '/');
  Result := StrRemoveLeadingChars(Result, ['\', '/']);
  Result := StrRemoveEndChars(Result, ['\', '/']);
  Result := MakeLegalFileName(Result);
  if SameText(Result, 'Test2') then
    Result := 'QM'
  else if SameText(Result, 'LocQM') then
    Result := 'Loc';
end;

function GetClientLocalDir: string;
begin
  Result := MakePath(GetLocalAppDataFolder, 'Q Manager');
end;

function GetClientServerSpecificLocalDir: string;
begin
  Result := MakePath(GetClientLocalDir, GetServerDatabaseIdentifier);

end;

function GetClientAddinsFile: string;
var
  PFAddins: string;
begin
  PFAddins := MakePath(MakePath(MakePath(GetProgramFilesFolder, 'UtiliQuest'), 'Q Manager'), AddinsIni);
  // Prefer the Program Files Addins.ini, since that is where the UQ installer puts the file
  if FileExists(PFAddins) then
    Result := PFAddins
  else
    Result := AddSlash(ExtractFilePath(ParamStr(0))) + AddinsIni;
end;

function GetClientAddinsLocalDir: string;
begin
  Result := MakePath(GetClientServerSpecificLocalDir, 'Addins');
end;

function GetClientAttachmentsLocalDir: string;
begin
  if UQState.AppLocal then
    Result := MakePath(GetClientServerSpecificLocalDir, 'Attachments')
  else
    Result := MakePath(ExtractFileDir(ParamStr(0)), 'Attachments');
end;

function GetClientAttachmentsDownloadDir(ApplicationName: string): string;
begin
  Result := AddSlash(AddSlash(GetAppdataFolder) + ApplicationName + ' Downloads');
end;

procedure ConfigureDataAndExecuteApp;
var
  UpdateExeFile: string;
  UpdateFullPath: string;

  procedure CreateLocalDirs;
    procedure ForceDir(const Dir: string);
    begin
      if not ForceDirectories(Dir) then
        raise Exception.Create('Unable to create local application directory: ' + Dir);
    end;
  begin
    ForceDir(GetClientLocalDir);
    ForceDir(GetClientServerSpecificLocalDir);
    ForceDir(GetClientDataLocalDir);
    ForceDir(GetClientAddinsLocalDir);
    ForceDir(GetClientAttachmentsLocalDir);
    ForceDir(GetClientExportsLocalDir);
  end;


  procedure CopyApplicationLocal;
  var
    OldClientDir: string;
    OldDataDir: string;
    OldAttachmentDir: string;
    OldExportDir: string;
    OldIniFile: string;
    BackupIniLocation: string;
    Flags: FILEOP_FLAGS;
  begin
    OldClientDir := ExtractFilePath(Application.ExeName);
    OldDataDir :=  MakePath(OldClientDir, 'data');
    OldAttachmentDir := MakePath(OldClientDir, 'Attachments');
    OldExportDir := MakePath(OldClientDir, 'export');
    OldIniFile := MakePath(OldClientDir, 'QManager.ini');
    Flags := FOF_FILESONLY or FOF_NOCONFIRMATION or FOF_NOCONFIRMMKDIR or FOF_NOERRORUI or FOF_SILENT {or FOF_NORECURSION or FOF_NO_UI or FOF_NOCOPYSECURITYATTRIBS};
    {QMANTWO-624 - Removed code that copied these to AppData directory
                     * bpls
                     * Ini files
                     * copy of the client application and other EXEs}

    if DirectoryExists(OldDataDir) then
      ShellCopy(MakePath(OldDataDir, '*.*'), GetClientDataLocalDir, Flags, True);

    if FileExists(OldIniFile) then begin
      BackupIniLocation := GetClientServerSpecificLocalDir + '\QManager_Backup'  + '.Ini';
      if not FileExists(BackupIniLocation) then      
        ShellCopy(OldIniFile, BackupIniLocation , Flags, True);
    end;
    
    if DirectoryExists(OldAttachmentDir) then
      ShellCopy(MakePath(OldAttachmentDir, '*.*'), GetClientAttachmentsLocalDir, Flags, True);
    if DirectoryExists(OldExportDir) then
      ShellCopy(MakePath(OldExportDir, '*.*'), GetClientExportsLocalDir, Flags, True);

    SysUtils.DeleteFile(MakePath(GetClientLocalDir, 'unins000.exe'));
    { Don't try to clear old files/settings unless we've verified some old directories exist }
    if DirectoryExists(OldDataDir) or DirectoryExists(OldExportDir) or DirectoryExists(OldAttachmentDir) then begin
      
      if CanWriteToDirectory(OldClientDir) then begin
        DeleteDirectory(OldExportDir, True);
//        DeleteDirectory(OldDataDir, True);
        MoveFile(PChar(OldDataDir), PChar(OldDataDir + '_Archived_' + FormatDateTime('YYYYMMDD', NOW))); //QMANTWO-624 EB Archive instead of delete
        DeleteDirectory(OldAttachmentDir, True);
       {QMANTWO-624 - Removed the ini copy over and replace}
      end;
    end;
  end;

begin
  UQState := State;
  if not UQState.AppLocal then
    Exit;
  UpdateExeFile := ExtractFileName(Application.ExeName);
  UpdateFullPath := MakePath(GetClientLocalDir, UpdateExeFile);
  if SameFileName(Application.ExeName, UpdateFullPath) then
    Exit;
  CreateLocalDirs;
  if not FileExists(UpdateFullPath) then
    CopyApplicationLocal;
  {QMANTWO-624 - Removed switchover to the exe in AppData folder.
                 It will continue to run from the installation folder}
end;

end.

