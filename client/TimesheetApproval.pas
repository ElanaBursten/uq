unit TimesheetApproval;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, DB, DBISAMTb, ExtCtrls, StdCtrls,
  OdRangeSelect, ActnList, OdXmlToDataSet, OdDBISAMUtils, Types,
  QMServerLibrary_Intf, EmployeeSelectFrame,
  BaseTimesheetDay, TimeClockDayApproval, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxDataStorage, cxEdit, cxDBData,
  cxTextEdit, cxMaskEdit, cxCalendar, cxCheckBox, cxTimeEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxGridBandedTableView,
  cxGridDBBandedTableView, cxNavigator, cxCustomData, cxFilter,
  cxData, TimeClockDay;

type
  TApprovalAction = (taApprove, taFinalApprove, taAll, taNone);

  TTimesheetApprovalForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    TimesheetSource: TDataSource;
    ApproveButton: TButton;
    FinalApproveButton: TButton;
    SelectAllButton: TButton;
    DateRange: TOdRangeSelectFrame;
    SearchButton: TButton;
    SearchTypeCombo: TComboBox;
    ShowLabel: TLabel;
    EmployeesLabel: TLabel;
    ActionList: TActionList;
    SearchAction: TAction;
    ApproveAction: TAction;
    FinalApproveAction: TAction;
    SelectAllAction: TAction;
    Timesheets: TDBISAMTable;
    RightPanel: TPanel;
    ExportAction: TAction;
    ExportButton: TButton;
    EmployeeSelect: TEmployeeSelect;
    TimesheetDetails: TTimeClockDayApprovalFrame;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    GridDBTableView: TcxGridDBBandedTableView;
    ColUniquePK: TcxGridDBBandedColumn;
    ColEntryID: TcxGridDBBandedColumn;
    ColWorkDate: TcxGridDBBandedColumn;
    ColApprove: TcxGridDBBandedColumn;
    ColSelectionFlag: TcxGridDBBandedColumn;
    ColShortName: TcxGridDBBandedColumn;
    ColMilesDriven: TcxGridDBBandedColumn;
    ColVehicleUse: TcxGridDBBandedColumn;
    ColWorkStart1: TcxGridDBBandedColumn;
    ColWorkStop1: TcxGridDBBandedColumn;
    ColWorkStart2: TcxGridDBBandedColumn;
    ColWorkStop2: TcxGridDBBandedColumn;
    ColWorkStart3: TcxGridDBBandedColumn;
    ColWorkStop3: TcxGridDBBandedColumn;
    ColWorkStart4: TcxGridDBBandedColumn;
    ColWorkStop4: TcxGridDBBandedColumn;
    ColCalloutStart1: TcxGridDBBandedColumn;
    ColCalloutStop1: TcxGridDBBandedColumn;
    ColCalloutStart2: TcxGridDBBandedColumn;
    ColCalloutStop2: TcxGridDBBandedColumn;
    ColRegHrs: TcxGridDBBandedColumn;
    ColOTHrs: TcxGridDBBandedColumn;
    ColCalloutHrs: TcxGridDBBandedColumn;
    ColLeaveHrs: TcxGridDBBandedColumn;
    ColHolHrs: TcxGridDBBandedColumn;
    ColDTHrs: TcxGridDBBandedColumn;
    ColVacHrs: TcxGridDBBandedColumn;
    ColHidden1: TcxGridDBBandedColumn;
    ColFloatingHoliday: TcxGridDBBandedColumn;
    ColStatus: TcxGridDBBandedColumn;
    ColApproveBy: TcxGridDBBandedColumn;
    ColFinalApproveBy: TcxGridDBBandedColumn;
    GridDBBandedTableView1: TcxGridDBBandedTableView;
    GridDBBandedTableView1Column1: TcxGridDBBandedColumn;
    ColPTO: TcxGridDBBandedColumn;
    ColPerDiem: TcxGridDBBandedColumn;
    Label1: TLabel;
    ColEntryBy: TcxGridDBBandedColumn;
    ColSource: TcxGridDBBandedColumn;  //QM-760 Timesheet RTASQ EB
    procedure FormCreate(Sender: TObject);
    procedure SearchActionExecute(Sender: TObject);
    procedure ApproveActionExecute(Sender: TObject);
    procedure FinalApproveActionExecute(Sender: TObject);
    procedure SelectAllActionExecute(Sender: TObject);
    destructor Destroy; override;
    procedure SetupSearchColumns;
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure SelectFinalApprovableActionExecute(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure ExportActionExecute(Sender: TObject);
    procedure GridDBTableViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure GridDBTableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ColApproveCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TimesheetsAfterScroll(DataSet: TDataSet);
  private
    FColumns: TColumnList;
    FHaveRecords: Boolean;
    FCanApprove: Boolean;
    FCanFinalApprove: Boolean;
    FCanSelectAll: Boolean; //QM-118 Select All Permissions Only
    FNotSubmittedColor: TColor;
    FNotEnteredColor: TColor;
    FSubmittedColor: TColor;
    FApprovedColor: TColor;
    FFinalApprovedColor: TColor;
    FSalaryUTAColor: TColor;   //QM-613 EB Timesheet UTA
    FTimeRTASQColor: TColor;   //QM-760 EB Timesheet for RTASQ
    FTimeTSEColor: TColor;     //QM-833 EB Timesheet TSE
    FLastAction: TApprovalAction;
    procedure CustomizeDataSet(DataSet: TDBISAMTable; Event: TCustomizeEvent);
    procedure ApproveSelectedItems(Final: Boolean);
    procedure SelectApprovable(Final: Boolean);
    function ColValue(ARecord: TcxGridDataRow; ACol: TcxGridColumn): Variant;
    procedure SetupSearchTypeCombo;
    function MakeSelectedList: TimesheetRecordList;
    procedure SetPermissions;
    function CanApproveCurrentTimesheet(Action: TApprovalAction): Boolean;
    function CanApproveTimesheet(Action: TApprovalAction; EntryID, EmpID, ApproveBy,
                                 FinalApproveBy, EntryBy: Integer; const Status: string; TikSource: string): Boolean;
    function GetColorForNode(Default: TColor; ARecord: TcxGridDataRow): TColor;
    procedure EnableDetailUpdates(IncludeControls: Boolean = True);
    procedure DisableDetailUpdates(IncludeControls: Boolean = True);
    procedure DoSearch(ForceEmpID: Integer = -1; ProfitCenter: string = '');
    procedure SetupTimesheetDetails;
  public
    procedure ActivatingNow; override;
  end;

implementation

uses
  DateUtils, DMu, QMConst, OdVclUtils, MSXML2_TLB, OdMSXMLUtils, OdDbUtils,
  OdHourglass, OdExceptions, OdMiscUtils, PayrollExport,
  HoursDataset, LocalPermissionsDmu, LocalEmployeeDmu;

{$R *.dfm}

procedure TTimesheetApprovalForm.FormCreate(Sender: TObject);
begin
  SetPermissions;
  FNotSubmittedColor := $00E8E8F7;
  FNotEnteredColor := $00EBEBEB;
  FSubmittedColor := clWhite;
  FApprovedColor := $00E2FFE1;
  FFinalApprovedColor := $00B0FFAE;
  FSalaryUTAColor := clWebSandyBrown;  //QM-613 EB Timesheet UTA
  FTimeRTASQColor := clWebPaleGoldenrod;  //QM-760 EB Timesheet Rtasq
  FTimeTSEColor := clWebPaleTurquoise; //QM-833 EB TSE
  FLastAction := taNone;
  FColumns := TColumnList.Create;
  DateRange.WeekBeginsSunday := True;
  DateRange.SelectDateRange(rsCustom, Today - 4, Today - 1);
  EmployeeSelect.Initialize(esTimesheetEmployees, DM.EmpID);
  SetupSearchTypeCombo;
  SetupTimesheetDetails;
  SetupSearchColumns;
end;

procedure TTimesheetApprovalForm.SetupTimesheetDetails;
begin
  with TimesheetDetails do begin
    ReadOnly := True;
    ShowApprovalData := True;
    NotEnteredShape.Brush.Color := FNotEnteredColor;
    NotSubmittedShape.Brush.Color := FNotSubmittedColor;
    SubmittedShape.Brush.Color := FSubmittedColor;
    ApprovedShape.Brush.Color := FApprovedColor;
    FinalApprovedShape.Brush.Color := FFinalApprovedColor;
    UTAEnteredShape.Brush.Color := FSalaryUTAColor;
    RTASQEnteredShape.Brush.Color := FTimeRTASQColor;  //QM-760 Timesheet RTASQ EB
    TSEEnteredShape.Brush.Color := FTimeTSEColor;  //QM-833 TSE EB
  end;
end;

procedure TTimesheetApprovalForm.TimesheetsAfterScroll(DataSet: TDataSet);
begin
  if not DataSet.ControlsDisabled then
    TimesheetDetails.DisplayDataSet(Timesheets);
end;

procedure TTimesheetApprovalForm.DoSearch(ForceEmpID: Integer; ProfitCenter: string);
var
  DOM: IXMLDOMDocument;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if ForceEmpID < 1 then
    ForceEmpID := EmployeeSelect.SelectedID;
  if DateRange.ToDate > EndOfTheDay(Today) then
    DateRange.SelectDateRange(rsCustom, DateRange.FromDate, Today);
  FLastAction := taNone;
  DM.CallingServiceName('SearchTimesheetsForProfitCenter');
  DOM := ParseXML(DM.Engine.Service.SearchTimesheetsForProfitCenter(DM.Engine.SecurityInfo, GetComboObjectInteger(SearchTypeCombo),
    DateRange.FromDate, DateRange.ToDate, ForceEmpID, 99, ProfitCenter));
  DisableDetailUpdates(False);
  try
    Timesheets.Close;
    if Assigned(DOM) then
      ConvertXMLToDataSet(DOM, FColumns, Timesheets, CustomizeDataSet);
    SetDateFieldDisplayFormats(Timesheets, 'h:mm');
    SetFieldDisplayFormat(Timesheets, 'work_date', 'dddd mmmm d, yyyy');
    Timesheets.First;
    case TTimesheetSearchType(GetComboObjectInteger(SearchTypeCombo)) of
      tsAll:  FLastAction := taAll;
      tsApproval:  FLastAction := taApprove;
      tsFinalApproval: FLastAction := taFinalApprove;
    end;
  finally
    // Calling Timesheets.EnableControls here caused infinite exception loops in DX
    EnableDetailUpdates(False);
    FHaveRecords := Timesheets.Active and (not Timesheets.IsEmpty);
  end;
  GridDBTableView.DataController.Groups.FullExpand;
end;

procedure TTimesheetApprovalForm.SearchActionExecute(Sender: TObject);
begin
  DoSearch;
end;

procedure TTimesheetApprovalForm.ApproveActionExecute(Sender: TObject);
begin
    ApproveSelectedItems(False);
end;

procedure TTimesheetApprovalForm.FinalApproveActionExecute(Sender: TObject);
begin
  ApproveSelectedItems(True);
end;

procedure TTimesheetApprovalForm.SelectAllActionExecute(Sender: TObject);
begin
  if FLastAction in [taApprove, taFinalApprove] then
    SelectApprovable(FLastAction = taFinalApprove);
end;

destructor TTimesheetApprovalForm.Destroy;
begin
  FreeAndNil(FColumns);
  inherited;
end;

procedure TTimesheetApprovalForm.SetupSearchColumns;
begin
  FColumns.SetDefaultTable('timesheet_entry');
  FColumns.AddDefaultTableColumns;
  FColumns.AddColumn('Employee', 30, 'short_name', ftString, 'employee', 30);
end;

procedure TTimesheetApprovalForm.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  SelectAllAction.Enabled := FHaveRecords and FCanSelectAll and    //QM-118 EB Select ALl Permissions
                             ( (FCanApprove and (FLastAction = taApprove))
                                or (FCanFinalApprove and (FLastAction = taFinalApprove)));
  FinalApproveAction.Enabled := SelectAllAction.Enabled and (FLastAction = taFinalApprove);
  ApproveAction.Enabled := SelectAllAction.Enabled and (FLastAction = taApprove);
  ExportAction.Enabled := PermissionsDM.CanI(RightTimesheetsExport);
end;

procedure TTimesheetApprovalForm.CustomizeDataSet(DataSet: TDBISAMTable; Event: TCustomizeEvent);

  function Field(const Name: string): TField;
  begin
    Result := DataSet.FieldByName(Name);
  end;

  function AnyFieldsHaveData(Names: array of string): Boolean;
  var
    Fld: TField;
    i: Integer;
  begin
    Result := False;
    for i := Low(Names) to High(Names) do begin
      Fld := Field(Names[i]);
      if (not Fld.IsNull) and (Fld.AsString <> '') and (Fld.AsString <> '0') then begin
        Result := True;
        Break;
      end;
    end;
  end;

const
  CheckDataFields: array[0..8] of string =('callout_start2', 'callout_start3',
    'callout_start4', 'callout_start5', 'callout_start6', 'work_start4',
    'work_start5', 'jury_hours', 'br_hours');
begin
  if Event = ceFieldDefs then begin
    DataSet.FieldDefs.Add('flag', ftString, 15);
    DataSet.FieldDefs.Add('miles_driven', ftInteger);
    DataSet.FieldDefs.Add('approve', ftBoolean);
    DataSet.FieldDefs.Add('unique_pk', ftAutoInc);
  end
  else if Event = ceDataRow then begin
    if not Field('miles_start1').IsNull then
      Field('miles_driven').AsInteger := Field('miles_stop1').AsInteger - Field('miles_start1').AsInteger;
    if AnyFieldsHaveData(CheckDataFields) then
      Field('flag').AsString := '**';
  end;
end;

procedure TTimesheetApprovalForm.ApproveSelectedItems(Final: Boolean);
var
  List: TimesheetRecordList;
  ReturnedMsg: string;
begin
  List := MakeSelectedList;
  try
    if Assigned(List) then begin
      DM.CallingServiceName('ApproveTimesheets');

      {QM-833 TSE Timesheets note:  Marking this spot.  If we have permissions...}
      ReturnedMsg := DM.Engine.Service.ApproveTimesheets(DM.Engine.SecurityInfo, Final, Now, List);
      if ReturnedMsg <> '' then
        ShowMessage(ReturnedMsg);
      Timesheets.EmptyTable;
      FHaveRecords := False;
    end;
  finally
    FreeAndNil(List);
  end;
  //MarkCheckedApproved(Final);
end;

procedure TTimesheetApprovalForm.SelectFinalApprovableActionExecute(Sender: TObject);
begin
  SelectApprovable(True);
end;

procedure TTimesheetApprovalForm.SelectApprovable(Final: Boolean);
var
  DoCheck: Boolean;
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  DisableDetailUpdates;
  try
    Timesheets.First;
    while not Timesheets.Eof do begin
      if Final then
        DoCheck := CanApproveCurrentTimesheet(taFinalApprove)
      else
        DoCheck := CanApproveCurrentTimesheet(taApprove);
      if Timesheets.FieldByName('approve').AsBoolean <> DoCheck then begin
        Timesheets.Edit;
        Timesheets.FieldByName('approve').AsBoolean := DoCheck;
        Timesheets.Post;
      end;
      Timesheets.Next;
    end;
    Timesheets.First;
  finally
    EnableDetailUpdates;
  end;
  GridDBTableView.DataController.UpdateItems(False);
  GridDBTableView.DataController.Groups.FullExpand;
end;

function TTimesheetApprovalForm.ColValue(ARecord: TcxGridDataRow; ACol: TcxGridColumn): Variant;
begin
  Assert(Assigned(ACol));
  Assert(Assigned(ARecord));
  Result := ARecord.Values[ACol.Index];
end;

procedure TTimesheetApprovalForm.SetupSearchTypeCombo;
var
  i: TTimesheetSearchType;
begin
  SearchTypeCombo.Items.Clear;
  for i := Low(TTimesheetSearchType) to High(TTimesheetSearchType) do
    SearchTypeCombo.Items.AddObject(TimesheetSearchDescription[i], TObject(i));
  if not FCanFinalApprove then
    SearchTypeCombo.Items.Delete(GetIndexOfComboInteger(SearchTypeCombo, Ord(tsFinalApproval)))
  else
    SelectComboBoxItemFromObjects(SearchTypeCombo, Ord(tsFinalApproval));
  if not FCanApprove then
    SearchTypeCombo.Items.Delete(GetIndexOfComboInteger(SearchTypeCombo, Ord(tsApproval)))
  else
    SelectComboBoxItemFromObjects(SearchTypeCombo, Ord(tsApproval));
end;

function TTimesheetApprovalForm.MakeSelectedList: TimesheetRecordList;

  function WarnForMissingEnd: Boolean;
  const
    WarningMsg = 'You have selected timesheets with missing stop period times.  Continue?';
  var
    Warn: Boolean;
  begin
    Result := False;
    try
      CheckWorkPeriodsEnd(Timesheets, 'work', TimesheetNumWorkPeriods);
      CheckWorkPeriodsEnd(Timesheets, 'callout', TimesheetNumWorkPeriods);
      Warn := False;
    except
      on EOdDataEntryError do
        Warn := True;
    end;

    if Warn then
      Result := (MessageDlg(WarningMsg, mtWarning, [mbYes, mbNo], 0) = mrNo);
  end;

const
  WarningMsg = 'You have selected timesheets that are not yet submitted.  Continue?';
var
  TSRec: TimesheetRecord;
  ActiveWarned: Boolean;
  MissingEndWarned: Boolean;
begin
  Result := TimesheetRecordList.Create;
  ActiveWarned := False;
  MissingEndWarned := False;
  PostDataSet(Timesheets);
  DisableDetailUpdates;
  try
    Timesheets.First;
    while not Timesheets.Eof do begin
      if Timesheets.FieldByName('approve').AsBoolean then begin
        TSRec := Result.Add;
        TSRec.EntryID := Timesheets.FieldByName('entry_id').AsInteger;
        TSRec.EmpID := Timesheets.FieldByName('work_emp_id').AsInteger;
        TSRec.WorkDate := Timesheets.FieldByName('work_date').AsDateTime;
        if (not ActiveWarned) and (Timesheets.FieldByName('status').AsString = TimesheetStatusActive) then
          if (MessageDlg(WarningMsg, mtWarning, [mbYes, mbNo], 0) = mrYes) then begin
            ActiveWarned := True;
          end
          else begin
            FreeAndNil(Result);
            Exit;
          end;
        if (not MissingEndWarned) and WarnForMissingEnd then begin
          FreeAndNil(Result);
          Exit;
        end
        else
          MissingEndWarned := True;
        Assert((TSRec.EmpID > 0) and (TSRec.WorkDate > 1));
      end;
      Timesheets.Next;
    end;
    Timesheets.First;
  finally
    EnableDetailUpdates;
  end;
  if Result.Count < 1 then begin
    FreeAndNil(Result);
    raise EOdEntryRequired.Create('Please check some timesheets first');
  end;
end;

procedure TTimesheetApprovalForm.SetPermissions;
begin
  FCanApprove := PermissionsDM.CanI(RightTimesheetsApprove);
  FCanFinalApprove := PermissionsDM.CanI(RightTimesheetsFinalApprove);
  FCanSelectAll := PermissionsDM.CanI(RightTimesheetsApproveSelectAll);  //QM-118 Select All Permissions
  Assert(FCanApprove or FCanFinalApprove, 'Permissions error');
end;

procedure TTimesheetApprovalForm.ActivatingNow;
begin
  inherited;
  SetPermissions;

  ColFloatingHoliday.Visible := True;
 // ColSpacer.Visible := False;
  TimesheetDetails.FloatingHolidayVisible := True;
end;

function TTimesheetApprovalForm.CanApproveCurrentTimesheet(Action: TApprovalAction): Boolean;
var
  pEntryID, pEmpID, pApproveBy, pFinalApproveBy, pEntryBy: integer;
  pStatus, pSource: string;

begin
  pSource := '';
  if (not Timesheets.Active) or Timesheets.Eof then
    Result := False
  else begin
    pEntryID :=   Timesheets.FieldByName('entry_id').AsInteger;
    pEmpID :=     Timesheets.FieldByname('work_emp_id').AsInteger;
    pApproveBy := Timesheets.FieldByName('approve_by').AsInteger;
    pFinalApproveBy := Timesheets.FieldByName('final_approve_by').AsInteger;
    pStatus :=    Timesheets.FieldByName('status').AsString;
    pEntryBy:=    Timesheets.FieldByName('entry_by').AsInteger;
    pSource :=    UpperCase(Timesheets.FieldByName('source').AsString);   //QM-760 Timesheet RTASQ EB

    Result := CanApproveTimesheet(Action, pEntryID, pEmpID, pApproveBy,
                                  pFinalapproveBy, pEntryBy, pStatus, pSource);   //QM-760 Timesheet RTASQ  EB
  end;
end;

function TTimesheetApprovalForm.CanApproveTimesheet(Action: TApprovalAction;
  EntryID, EmpID, ApproveBy, FinalApproveBy, EntryBy: Integer; const Status: string; TikSource: string): Boolean;    //QM-613 EB Timeshet UTA
var
  IsApproved: Boolean;
  IsFinalApproved: Boolean;
  IsBlank: Boolean;
  IsMyself: Boolean;
begin
  Result := False;
  if (EntryBy = TimesheetUTAEnteredBy) or (TikSource = TimesheetRtasqSource) then begin    //QM-613 EB Timesheet UTA   //QM-760 Timesheet RTASQ EB
    Exit;
  end;

  if Action in [taNone, taAll] then
    Exit;

     // QM-760 RTasq
  IsApproved := ApproveBy > 0;
  IsFinalApproved := FinalApproveBy > 0;
  IsBlank := EntryID = 0;
  IsMyself := EmpID = DM.EmpID;

  case Action of
    taApprove:
      Result := (not IsApproved) and (not IsMyself);
    taFinalApprove:
      Result := IsApproved and (not IsBlank) and (not IsFinalApproved) and (not IsMyself);
  end;
end;

function TTimesheetApprovalForm.GetColorForNode(Default: TColor; ARecord: TcxGridDataRow): TColor;
var
  CValue: Variant;
  AStr: string;
begin
  Result := Default;
  try
    // Delphi version note: with the updated RTL version, the following code
    // works fine without the VarIsNumeric() checks.

    {UTA}
    CValue := ColValue(ARecord, ColEntryBy);                  //QM-613 Timesheet UTA EB
    if (CValue = TimesheetUTAEnteredBy) then begin
      Result := FSalaryUTAColor;
      Exit;
    end;

    {RTASQ}
    CValue := ColValue(ARecord, ColSource);                   //QM-760 Timesheet RTASQ EB
    AStr := UpperCase(VarToStr(CValue));
    if (AStr = TimesheetRtasqSource) then begin
      Result := FTimeRTASQColor;
      Exit;
    end;

    {TSE}
    CValue := ColValue(ARecord, ColSource);                  //QM-833 Timesheet TSE EB
    AStr := UpperCase(VarToStr(CValue));
    if (AStr = TimesheetTSESource) then begin
      Result := FTimeTSEColor;
      Exit;
    end;


    CValue := ColValue(ARecord, ColFinalApproveBy);
    if VarIsNumeric(CValue) and (CValue > 0) then begin
      Result := FFinalApprovedColor;
      Exit;
    end;

    CValue := ColValue(ARecord, ColApproveBy);
    if VarIsNumeric(CValue) and (CValue > 0) then begin
      Result := FApprovedColor;
      Exit;
    end;

    CValue := ColValue(ARecord, ColStatus);
    if VarIsNumeric(CValue) and (CValue = TimesheetStatusSubmit) then begin
      Result := FSubmittedColor;
      Exit;
    end;

    if VarIsNumeric(CValue) and (CValue = TimesheetStatusActive) then begin
      Result := FNotSubmittedColor;
      Exit;
    end;

    CValue := ColValue(ARecord, ColEntryID);
    if CValue = Null then
      Result := FNotEnteredColor;




  except
    // If anything goes wrong (as often happens with variants), we must not
    // have it throw the ex everytime it runs the event handler.
  end;
end;

procedure TTimesheetApprovalForm.GridDblClick(Sender: TObject);
var
  Rec: TcxCustomGridRecord;
  EmpID: Integer;
  WorkDate: TDateTime;
  FinalApproved: Boolean;
  EntryBy: Integer; //QM-613 EB Timesheet UTA
  TikSource: string;  //QM-760 RTASQ Timesheet EB
begin
  inherited;

  Rec := GridDBTableView.Controller.FocusedRecord;
  if FHaveRecords and Assigned(Rec) and (Rec is TcxGridDataRow) then begin
    FinalApproved := Timesheets.FieldByName('final_approve_by').AsInteger > 0;
    EmpID := Timesheets.FieldByName('work_emp_id').AsInteger;
    WorkDate := Timesheets.FieldByName('work_date').AsDateTime;
    EntryBy :=Timesheets.FieldByName('entry_by').AsInteger;         //QM-613 EB Timesheet UTA
    TikSource := Timesheets.FieldByName('source').AsString;         //QM-760 RTASQ Timesheet EB


    if (EntryBy = TimesheetUTAEnteredBy) or (UPPERCASE(TikSource) = TimesheetRtasqSource) then                       //QM-613 EB Timesheet UTA
      raise EodNotAllowed.Create('This timesheet was imported from UTA/Rtasq and cannot be edited.');  //QM-613 Timesheet UTA

    if not PermissionsDM.CanI(RightTimesheetsUnApprove) then
      if FinalApproved then
        raise EOdNotAllowed.Create('You can not edit final approved timesheets');
    if EmpID = DM.EmpID then
      raise EOdNotAllowed.Create('Please edit your own timesheets using the Timesheet button.');
    Assert((WorkDate > 1) and(EmpID > 0));

    if DM.HavePendingTimesheetChanges then
      DM.SendTimeChangesToServer;
    if FinalApproved then
      DM.DownloadAndEditTimesheetDay(EmpID, WorkDate, teITOverride)
    else
      DM.DownloadAndEditTimesheetDay(EmpID, WorkDate, teManager);
  end;
end;

procedure TTimesheetApprovalForm.DisableDetailUpdates(IncludeControls: Boolean = True);
begin
  if IncludeControls then
    Timesheets.DisableControls;
  Timesheets.AfterScroll := nil;
end;

procedure TTimesheetApprovalForm.EnableDetailUpdates(IncludeControls: Boolean = True);
begin
  if IncludeControls then
    Timesheets.EnableControls;
  Timesheets.AfterScroll := TimesheetsAfterScroll;
  TimesheetsAfterScroll(Timesheets);
end;

procedure TTimesheetApprovalForm.ExportActionExecute(Sender: TObject);
const
  ASkMsg = 'Would you like to see the unapproved timesheets you are allowed to approve?';
var
  Search: TPayrollExportResult;
  SearchEmpID: Integer;
begin
  Search := TPayrollExportForm.Execute;
  if Search.DoSearch then begin
    if (MessageDlg(Search.ExceptionMsg + sLineBreak + AskMsg, mtError, [mbYes, mbNo], 0) = mrYes) then begin
      SearchEmpID := EmployeeDM.GetManagerIDForEmp(EmployeeDM.GetEmpIDForPayrollProfitCenter(Search.ProfitCenter));
      SelectComboBoxItemFromObjects(EmployeeSelect.EmployeeCombo, SearchEmpID);
      DateRange.SelectDateRange(rsCustom, Trunc(Search.WeekEnding - 6), Trunc(Search.WeekEnding));
      if Search.FinalApproval then
        SelectComboBoxItemFromObjects(SearchTypeCombo, Ord(tsFinalApproval))
      else
        SelectComboBoxItemFromObjects(SearchTypeCombo, Ord(tsApproval));
      DoSearch(SearchEmpID, Search.ProfitCenter);
      EmployeeSelect.SelectID(DM.EmpID);
    end;
  end;
end;

procedure TTimesheetApprovalForm.GridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  Background: TColor;
  Rec: TcxGridDataRow;
begin
  if not (AViewInfo.GridRecord is TcxGridDataRow) then
    Exit;

  Rec := AViewInfo.GridRecord as TcxGridDataRow;
  Background := ACanvas.Brush.Color;
  if (not AViewInfo.Selected) and FHaveRecords then begin
    Background := GetColorForNode(Background, Rec);
    ACanvas.Brush.Color := Background;
  end;
  if (AViewInfo.Item.Index = ColApprove.Index) and (FLastAction = taAll) then begin
    ACanvas.Brush.Color := GetColorForNode(Background, Rec);
    ACanvas.FillRect(ACanvas.Canvas.ClipRect, ACanvas.Brush.Color);
    ADone := True;
  end;
end;

procedure TTimesheetApprovalForm.GridDBTableViewEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  AAllow := CanApproveCurrentTimesheet(FLastAction);
end;

procedure TTimesheetApprovalForm.ColApproveCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;

  if FLastAction = taAll then
    ADone := True;
end;

end.
