inherited AreaEditorForm: TAreaEditorForm
  Caption = 'Area Editor'
  ClientHeight = 559
  ClientWidth = 951
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 951
    Height = 36
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      951
      36)
    object SaveChangesButton: TButton
      Left = 6
      Top = 8
      Width = 91
      Height = 25
      Caption = 'Save Changes'
      TabOrder = 0
      OnClick = SaveChangesButtonClick
    end
    object QtrMinBtn: TButton
      Left = 817
      Top = 10
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Qtr Min Map Editor '
      TabOrder = 1
      OnClick = QtrMinBtnClick
    end
  end
  object DataPanel: TPanel
    Left = 0
    Top = 36
    Width = 951
    Height = 523
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 1
    object AreaGrid: TcxGrid
      Left = 5
      Top = 5
      Width = 941
      Height = 513
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object AreaGridView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = EditableAreasSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'area_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.ShowEditButtons = gsebForFocusedRecord
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object ColMapName: TcxGridDBColumn
          Caption = 'Map Name'
          DataBinding.FieldName = 'map_name'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.ReadOnly = True
          Visible = False
          GroupIndex = 0
          Options.Editing = False
          Options.Filtering = False
          Width = 83
        end
        object ColAreaName: TcxGridDBColumn
          Caption = 'Area Name'
          DataBinding.FieldName = 'area_name'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 117
        end
        object ColShortName: TcxGridDBColumn
          Caption = 'Assigned Locator'
          DataBinding.FieldName = 'short_name'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.ViewStyle = vsHideCursor
          Properties.OnButtonClick = ColLocatorButtonClick
          Options.Filtering = False
          Width = 151
        end
        object ColLocatorID: TcxGridDBColumn
          Caption = 'Locator ID'
          DataBinding.FieldName = 'locator_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Options.Editing = False
          Options.Filtering = False
          Width = 60
        end
        object ColState: TcxGridDBColumn
          Caption = 'State'
          DataBinding.FieldName = 'state'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 52
        end
        object ColCounty: TcxGridDBColumn
          Caption = 'County'
          DataBinding.FieldName = 'county'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 108
        end
        object ColTicketCode: TcxGridDBColumn
          Caption = 'TicketCode'
          DataBinding.FieldName = 'ticket_code'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 134
        end
        object ColMapID: TcxGridDBColumn
          Caption = 'Map ID'
          DataBinding.FieldName = 'map_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 56
        end
        object ColAreaID: TcxGridDBColumn
          Caption = 'Area ID'
          DataBinding.FieldName = 'area_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 56
        end
        object QtrMinMap: TcxGridDBColumn
          Caption = 'Map'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Caption = 'Map'
              Default = True
              Kind = bkText
              LeftAlignment = True
            end>
          Properties.ReadOnly = True
          Properties.ViewStyle = vsButtonsOnly
          Properties.OnButtonClick = AreaEditorBtnPropertiesButtonClick
          Width = 44
        end
      end
      object AreaGridLevel: TcxGridLevel
        GridView = AreaGridView
      end
    end
  end
  object EditableAreasSource: TDataSource
    DataSet = EditableAreas
    Left = 32
    Top = 96
  end
  object EditableAreas: TDBISAMTable
    BeforeOpen = EditableAreasBeforeOpen
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'editable_areas'
    Left = 128
    Top = 96
  end
end
