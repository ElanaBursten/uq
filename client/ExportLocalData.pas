unit ExportLocalData;

interface

uses SysUtils, Classes, DateUtils, MSXML2_TLB, DB, OdMiscUtils,
  OdMSXMLUtils, OdDBISAMUtils, OdDbUtils, OdDataSetToXml, DMu;

type
  TXMLExporter = class
  private
    FXMLDoc: IXMLDOMDocument;
  public
    constructor Create;
    destructor Destroy; override;
    procedure ConvertDataToXMLDoc;
    procedure SaveToFile(const FileName: string);
  end;


procedure CreateQMDataXMLFile(const XMLFileName: string);

implementation

procedure CreateQMDataXMLFile;
var
  Exporter: TXMLExporter;
begin
  Exporter := TXMLExporter.Create;
  try
    Exporter.ConvertDataToXMLDoc;
    Exporter.SaveToFile(XMLFileName);
  finally
    FreeAndNil(Exporter);
  end;
end;

constructor TXMLExporter.Create;
begin
  inherited;
end;

destructor TXMLExporter.Destroy;
begin
  inherited;
end;

procedure TXMLExporter.ConvertDataToXMLDoc;
const
  SelectMyTicketIDs = 'select distinct ticket.ticket_id ' +
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'where locate.locator_id=%d ' +
    'and locate.active ' +
    'and NOT locate.closed ' +
    'and ((do_not_mark_before is NULL) ' +
    'or (do_not_mark_before <= %s)) ' +
    'and %s '; // placeholder for 'ticket.ticket_id in ()' criteria
  SelectMyTickets = 'select ticket.*, coalesce(ref.sortby, 0) work_priority, ' +
    'coalesce(ref.description, ''Normal'') priority_disp, ' +
    'if(task.est_start_date is null, 0, -1) first_task, task.est_start_date ' +
    'from ticket ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'left join task_schedule task on (task.ticket_id=ticket.ticket_id and task.active=True ' +
    'and task.est_start_date >= %s and task.est_start_date < %s) ' +
    'where %s ' +
    'order by work_priority desc, first_task, due_date, ticket_number';
  SelectLocates='select * from locate where %s';
  SelectArrivals='select * from jobsite_arrival where %s';
  SelectEmpActivity='select * from employee_activity';
  TableNameArray: array[0..3] of string = ('ticket','locate','jobsite_arrival','employee_activity');
var
  XML: string;
  MyTickets: TDataSet;
  DataSetArray: array[0..3] of TDataSet;
  TicketIDList: TStringList;
  VisibleTicketClause: string;
  i: Integer;
begin
  Assert(Assigned(DM), 'No data module assigned in TXMLExporter.ConvertDataToXML');
  Assert(Length(TableNameArray) = Length(DataSetArray), 'Mismatched array lengths in TXMLExporter.ConvertDataToXML');

  for i := Low(DataSetArray) to High(DataSetArray) do
    DataSetArray[i] := nil;

  TicketIDList := TStringList.Create;
  try
    TicketIDList.Delimiter := ',';
    DM.GetLimitedTicketIdList(TicketIDList, DM.MyTicketViewLimit);
    if TicketIDList.Count > 0 then
      VisibleTicketClause := ' ticket_id in (' + GetSQLInClauseForStrings(TicketIDList, False) + ') '
    else
      // No ticket limit, get all open tickets
      VisibleTicketClause := ' 1=1 ';

    MyTickets := DM.Engine.OpenQuery(Format(SelectMyTicketIDs, [DM.EmpID,
      DateTimeToDBISAMDateTime(DM.UQState.LastLocalSyncTime), VisibleTicketClause]));
    try
      GetDataSetFieldValueList(TicketIDList, MyTickets, 'ticket_id');
    finally
      FreeAndNil(MyTickets);
    end;

    if TicketIDList.Count > 0 then
      VisibleTicketClause := ' ticket_id in (' + GetSQLInClauseForStrings(TicketIDList, False) + ') '
    else
      // No open tickets, make empty ticket, locate, & jobsite_arrival datasets
      VisibleTicketClause := '0=1';

    DataSetArray[0] := DM.Engine.OpenQuery(Format(SelectMyTickets,
      [DateToDBISAMDate(Tomorrow), DateToDBISAMDate(Tomorrow + 3),
      VisibleTicketClause]));
    DataSetArray[1] := DM.Engine.OpenQuery(Format(SelectLocates,
      [VisibleTicketClause]));
    DataSetArray[2] := DM.Engine.OpenQuery(Format(SelectArrivals,
      [VisibleTicketClause]));
    DataSetArray[3] := DM.Engine.OpenQuery(SelectEmpActivity);
    XML := ConvertDataSetsToSQLXML(DataSetArray, TableNameArray);
    FXMLDoc := ParseXml(XML);
    FXMLDoc.async := False;
  finally
    FreeAndNil(TicketIDList);
    for i := Low(DataSetArray) to High(DataSetArray) do
      FreeAndNil(DataSetArray[i]);
  end;
end;

procedure TXMLExporter.SaveToFile(const FileName: string);
begin
  SaveDebugFile(FileName, PrettyPrintXml(FXMLDoc));
end;


end.
