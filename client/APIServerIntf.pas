{
 Start of a unit to interface with the QMLogic2 server.

 TODO: Replace JSON structures with Thrift
}

unit APIServerIntf;

interface

uses
  SysUtils, Classes, SuperObject, QMTransport;

type
  TAPIServerInterface = class
  private
    FTransport: THttpTransport;
  public
    AppVersion: string;
    constructor Create(Transport: THttpTransport);
    destructor Destroy; override;
    procedure MoveTickets(TicketIDs: TStringList; const AssignFrom, AssignTo, ChangedBy: Integer);
  end;

implementation

uses
  QMConst, OdMiscUtils;

{ TAPIServerInterface }

constructor TAPIServerInterface.Create(Transport: THttpTransport);
begin
  inherited Create();

  Assert(Assigned(Transport), 'Transport is not defined');
  FTransport := Transport;
end;

destructor TAPIServerInterface.Destroy;
begin
  inherited;
end;

// TODO: This should use a Thrift command class instead of JSON.
procedure TAPIServerInterface.MoveTickets(TicketIDs: TStringList;
  const AssignFrom, AssignTo, ChangedBy: Integer);
var
  Stream: TStringStream;
  TicketListObj, TicketObj, MainObj: ISuperObject;
  i: Integer;
begin
  Assert(Assigned(TicketIDs));
  if TicketIDs.Count = 0 then
    Exit;

  TicketListObj := SA([]);
  for i := 0 to TicketIDs.Count - 1 do begin
    TicketObj := SO;
    TicketObj.I[JsonTicketId] := TicketIDs.GetInteger(i);
    TicketObj.I[JsonFromLocator] := AssignFrom;
    TicketObj.I[JsonToLocator] := AssignTo;
    TicketObj.I[JsonChangedBy] := ChangedBy;
    TicketListObj.AsArray.Add(TicketObj);
  end;

  MainObj := SO;
  MainObj.O['tickets'] := TicketListObj;
  Stream := TStringStream.Create(MainObj.AsJSon(True, False));
  try
    FTransport.PostStream(QMLogic2BaseURL + ReassignTicketURL, Stream);
  finally
    FreeAndNil(Stream);
  end;
end;

end.
