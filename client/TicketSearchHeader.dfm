inherited TicketSearchCriteria: TTicketSearchCriteria
  Left = 296
  Top = 323
  Caption = 'Ticket Search'
  ClientHeight = 271
  ClientWidth = 827
  Font.Charset = ANSI_CHARSET
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited RecordsLabel: TLabel
    Left = 299
    Top = 228
  end
  object SearchPanel: TPanel [1]
    Left = 0
    Top = 0
    Width = 577
    Height = 271
    Align = alLeft
    BevelEdges = []
    BevelOuter = bvNone
    TabOrder = 25
    object Label12: TLabel
      Left = 215
      Top = 203
      Width = 47
      Height = 13
      Caption = 'Utility Co:'
    end
    object Label1: TLabel
      Left = 6
      Top = 11
      Width = 72
      Height = 13
      Caption = 'Ticket Number:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 36
      Width = 34
      Height = 13
      Caption = 'Street:'
    end
    object Label3: TLabel
      Left = 8
      Top = 60
      Width = 23
      Height = 13
      Caption = 'City:'
    end
    object Label4: TLabel
      Left = 8
      Top = 84
      Width = 30
      Height = 13
      Caption = 'State:'
    end
    object Label6: TLabel
      Left = 215
      Top = 12
      Width = 59
      Height = 13
      Caption = 'Ticket Type:'
    end
    object Label7: TLabel
      Left = 215
      Top = 36
      Width = 76
      Height = 13
      Caption = 'Work Done For:'
    end
    object Label8: TLabel
      Left = 215
      Top = 60
      Width = 49
      Height = 13
      Caption = 'Company:'
    end
    object Label9: TLabel
      Left = 215
      Top = 179
      Width = 56
      Height = 13
      Caption = 'Term Code:'
    end
    object Label10: TLabel
      Left = 8
      Top = 179
      Width = 74
      Height = 13
      Caption = 'Manual Tickets:'
    end
    object Label11: TLabel
      Left = 8
      Top = 203
      Width = 38
      Height = 13
      Caption = 'Priority:'
    end
    object Label5: TLabel
      Left = 8
      Top = 156
      Width = 57
      Height = 13
      Caption = 'Call Center:'
    end
    object AttachmentsLabel: TLabel
      Left = 8
      Top = 228
      Width = 65
      Height = 13
      Caption = 'Attachments:'
    end
    object CountyLabel: TLabel
      Left = 8
      Top = 108
      Width = 39
      Height = 13
      Caption = 'County:'
    end
    object WorkTypeLabel: TLabel
      Left = 8
      Top = 132
      Width = 56
      Height = 13
      Caption = 'Work Type:'
    end
    object lblTicketID: TLabel
      Left = 6
      Top = 251
      Width = 46
      Height = 13
      Caption = 'Ticket ID:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblLocateID: TLabel
      Left = 215
      Top = 250
      Width = 50
      Height = 13
      Caption = 'Locate ID:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edtTicketID: TEdit
      Left = 84
      Top = 248
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object edtLocateID: TEdit
      Left = 299
      Top = 248
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  inherited SearchButton: TButton
    Left = 504
    Top = 12
    Width = 65
    TabOrder = 17
  end
  object TicketNumber: TEdit [3]
    Left = 84
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object City: TEdit [4]
    Left = 84
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object TicketType: TEdit [5]
    Left = 299
    Top = 8
    Width = 197
    Height = 21
    TabOrder = 10
  end
  object WorkDoneFor: TEdit [6]
    Left = 299
    Top = 32
    Width = 197
    Height = 21
    TabOrder = 11
  end
  object Company: TEdit [7]
    Left = 299
    Top = 56
    Width = 197
    Height = 21
    TabOrder = 12
  end
  inline DueDateFrame: TOdRangeSelectFrame [8]
    Left = 215
    Top = 77
    Width = 284
    Height = 52
    TabOrder = 13
    inherited FromLabel: TLabel
      Left = 0
      Top = 7
      Width = 76
      Caption = 'Due Date From:'
    end
    inherited ToLabel: TLabel
      Left = 175
      Top = 7
    end
    inherited DatesLabel: TLabel
      Left = 0
      Top = 31
    end
    inherited DatesComboBox: TComboBox
      Left = 84
      Top = 27
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 84
      Top = 3
      Properties.OnChange = nil
      Properties.OnCloseUp = DueDateFrameFromDateEditPropertiesCloseUp
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 195
      Top = 3
      Properties.OnCloseUp = DueDateFrameToDateEditPropertiesCloseUp
    end
  end
  object County: TEdit [9]
    Left = 84
    Top = 104
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
  end
  object WorkType: TEdit [10]
    Left = 84
    Top = 128
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 5
  end
  inline RecvDateFrame: TOdRangeSelectFrame [11]
    Left = 211
    Top = 125
    Width = 287
    Height = 50
    TabOrder = 14
    inherited FromLabel: TLabel
      Left = 4
      Top = 7
      Width = 81
      Caption = 'Recv Date From:'
    end
    inherited ToLabel: TLabel
      Left = 179
      Top = 7
    end
    inherited DatesLabel: TLabel
      Left = 4
      Top = 31
    end
    inherited DatesComboBox: TComboBox
      Left = 88
      Top = 27
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 88
      Top = 3
      Properties.OnChange = nil
      Properties.OnCloseUp = RecvDateFrameFromDateEditPropertiesCloseUp
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 199
      Top = 3
      Properties.OnCloseUp = RecvDateFrameToDateEditPropertiesCloseUp
    end
  end
  inherited CancelButton: TButton [12]
    Left = 504
    Top = 171
    Width = 65
    TabOrder = 22
  end
  inherited CopyButton: TButton [13]
    Left = 504
    Top = 43
    Width = 65
    TabOrder = 18
  end
  inherited ExportButton: TButton [14]
    Left = 504
    Top = 75
    Width = 65
    TabOrder = 19
  end
  inherited PrintButton: TButton
    Left = 504
    Top = 138
    Width = 65
    TabOrder = 21
  end
  inherited ClearButton: TButton
    Left = 504
    Top = 204
    Width = 65
    TabOrder = 23
  end
  object CallCenter: TEdit
    Left = 84
    Top = 152
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 6
  end
  object Attachments: TComboBox
    Left = 84
    Top = 224
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 9
    Items.Strings = (
      '')
  end
  object ApproveButton: TButton
    Left = 504
    Top = 107
    Width = 65
    Height = 25
    Caption = '&Approve'
    Enabled = False
    ModalResult = 2
    TabOrder = 20
    OnClick = ApproveButtonClick
  end
  object ManualTickets: TComboBox
    Left = 84
    Top = 176
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
  end
  object Priority: TComboBox
    Left = 84
    Top = 200
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
  end
  object LocatePanel: TPanel
    Left = 577
    Top = 0
    Width = 250
    Height = 271
    Align = alClient
    BevelEdges = []
    BevelOuter = bvLowered
    TabOrder = 24
    Visible = False
  end
  object cbUtilityCo: TcxComboBox
    Left = 299
    Top = 200
    Properties.OnCloseUp = cbUtilityCoPropertiesCloseUp
    Properties.OnDrawItem = cbUtilityCoPropertiesDrawItem
    Style.BorderColor = clWindowFrame
    Style.Color = clWindow
    TabOrder = 16
    Width = 199
  end
  object ClientCode: TcxComboBox
    Left = 299
    Top = 176
    Properties.OnDrawItem = ClientCodePropertiesDrawItem
    TabOrder = 15
    Width = 199
  end
  object State: TEdit
    Left = 84
    Top = 80
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
  end
  object Street: TEdit
    Left = 84
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 656
    Top = 88
    PixelsPerInch = 96
    object cxInActiveStyle: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
    object cxActiveStyle: TcxStyle
    end
  end
end
