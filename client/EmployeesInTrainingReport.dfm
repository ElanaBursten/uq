inherited EmployeesInTrainingReportForm: TEmployeesInTrainingReportForm
  Left = 460
  Top = 204
  Caption = 'Employees in Training'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 11
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 45
    Width = 75
    Height = 13
    Caption = 'Training Period:'
  end
  object ManagerCombo: TComboBox [2]
    Left = 56
    Top = 8
    Width = 191
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object ShowDetailsCheckBox: TCheckBox [3]
    Left = 0
    Top = 128
    Width = 209
    Height = 17
    Caption = 'Show Employee Details'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  inline TrainingDateRange: TOdRangeSelectFrame [4]
    Left = 12
    Top = 60
    Width = 265
    Height = 53
    TabOrder = 2
    inherited FromLabel: TLabel
      Left = 7
      Top = 10
    end
    inherited ToLabel: TLabel
      Left = 146
      Top = 10
    end
    inherited DatesLabel: TLabel
      Left = 7
      Top = 35
    end
    inherited DatesComboBox: TComboBox
      Top = 32
      Width = 212
    end
    inherited FromDateEdit: TcxDateEdit
      Top = 7
      Width = 88
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 168
      Top = 7
      Width = 88
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
  end
end
