inherited DailySalesReportForm: TDailySalesReportForm
  Caption = 'DailySalesReportForm'
  ClientHeight = 160
  ClientWidth = 379
  PixelsPerInch = 96
  TextHeight = 13
  object ReportDateLabel: TLabel [0]
    Left = 0
    Top = 47
    Width = 61
    Height = 13
    Caption = 'Date Range:'
  end
  object ManagerLabel: TLabel [1]
    Left = 0
    Top = 12
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  inline ReportDate: TOdRangeSelectFrame [2]
    Left = 29
    Top = 66
    Width = 248
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 4
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Left = 4
      Top = 37
    end
    inherited DatesComboBox: TComboBox
      Width = 198
    end
  end
  object ManagerCombo: TComboBox
    Left = 73
    Top = 9
    Width = 218
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
end
