unit EmailedHPTicketsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls;

type
  TEmailedHPTicketsReportForm = class(TReportBaseForm)
    CallCenterLabel: TLabel;
    CallCenterComboBox: TComboBox;
  private
  public
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu;

{$R *.dfm}

{ TReportBaseForm1 }

procedure TEmailedHPTicketsReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  CallCenterComboBox.ItemIndex := 0;
end;

procedure TEmailedHPTicketsReportForm.ValidateParams;
begin
  inherited;
  SetReportID('EmailedHPTickets');
  SetParam('CallCenter', FCallCenterList.GetCode(CallCenterComboBox.Text));
end;

end.
