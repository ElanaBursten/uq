unit EmployeeSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LocalSearch, DB, StdCtrls, ExtCtrls, QMConst, OdDBISAMUtils,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxDBData, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxNavigator, cxCustomData, cxFilter, cxData, dbisamtb, cxGridLevel;

type
  TEmployeeSearchForm = class(TLocalSearchForm)
    ManagersOnlyCheck: TCheckBox;
    ShortName: TEdit;
    EmpNumber: TEdit;
    Manager: TEdit;
    RoleLabel: TLabel;
    ManagerLabel: TLabel;
    EmpNumLabel: TLabel;
    NameLabel: TLabel;
    EmpType: TComboBox;
    btnClear: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FSearchMode: TEmployeeSelectMode;
    FManagerID: Integer;
    fAlreadyLoaded: Boolean;  //QM-938 EB Fix Emp Search PART 2
    procedure SetSearchMode(const Value: TEmployeeSelectMode);
    function UserEnteredValues: boolean;  //QM-938 EB Fix Emp Search PART 2
  protected
    property SearchMode: TEmployeeSelectMode read FSearchMode write SetSearchMode;
    function GetWhereClause: string; override;
    procedure DefineColumns(Cols: TColumnList); override;
    function GetSelectSQL: string; override;
    procedure ClearAllValues;    //QM-938 EB Emp Search Changes
  public
    constructor Create(AOwner: TComponent); override;
    class function Execute(Mode: TEmployeeSelectMode; var SelectedID: Variant;
      EmployeeType: string = ''; ManagerID: Integer = -1): Boolean; reintroduce;

    function PreLoadExecute(var pEmpID: integer; pShortName, pEmpNum, pManager, pRole: string): Boolean; //QM-938 Fix the employee search critera
    procedure RefreshNow; override;
  end;

procedure AddEmployeeSearchButtonForCombo(Combo: TComboBox;
  Mode: TEmployeeSelectMode; EmployeeType: string = ''; ManagerID: Integer = -1);

implementation

uses DMu, OdDBISAMSqlBuilder, OdVclUtils, OdMiscUtils, LocalEmployeeDMu;

{$R *.dfm}

const
  SelectSQL = 'select e.short_name, e.emp_id, e.emp_number, e.active, m.short_name as manager, '+
    'r.description as role from employee e '+
    '  join reference r on e.type_id = r.ref_id '+
    '  join employee m on e.report_to = m.emp_id ';

var // "class fields"
  ClassSearchMode: TEmployeeSelectMode;
  ClassEmployeeType: string;
  ClassManagerID: Integer;

procedure TEmployeeSearchForm.btnClearClick(Sender: TObject);
begin
  inherited;
  ClearAllValues;
end;

procedure TEmployeeSearchForm.ClearAllValues;  //QM-938 EB Emp Search Changes
begin
  ShortName.Text := '';
  EmpNumber.Text := '';
  Manager.Text := '';
  EmpType.Text := '';

end;

constructor TEmployeeSearchForm.Create(AOwner: TComponent);
begin
  inherited;
  SearchMode := ClassSearchMode;
  FManagerID := ClassManagerID;

  SelectComboItem(EmpType, ClassEmployeeType, -1);
end;

procedure TEmployeeSearchForm.DefineColumns(Cols: TColumnList);
begin
  Cols.SetDefaultTable('employee');
  Cols.AddColumn('Name', 150, 'short_name');
  Cols.AddColumn('Emp ID', 65, 'emp_id', ftInteger, '', 0, True, DM.UQState.DeveloperMode);
  Cols.AddColumn('Employee #', 85, 'emp_number');
  Cols.AddColumn('Role', 100, 'role', ftString, '', 35, True);
  Cols.AddColumn('Manager', 150, 'manager');
  Cols.AddColumn('Active', 50, 'active', ftBoolean, '', 0, False, False);
end;

class function TEmployeeSearchForm.Execute(Mode: TEmployeeSelectMode; var SelectedID: Variant;
   EmployeeType: string; ManagerID: Integer): Boolean;
begin
  ClassSearchMode := Mode;
  ClassManagerID := ManagerID;
  ClassEmployeeType := EmployeeType;
  Result := inherited Execute(SelectedID);
end;

function TEmployeeSearchForm.PreLoadExecute(var pEmpID: Integer; pShortName, pEmpNum, pManager, pRole: string): boolean;
{This can replace Execute by preloading the search values}
//QM-938 Fix the employee search critera
begin
   Result := False;
  try
    {Preset incoming values}
    ShortName.Text := pShortName;
    EmpNumber.Text := pEmpNum;
    Manager.Text := pManager;
    EmpType.Text := pRole;

    {Set the new one}
    if ShowModal = mrOK then begin
      pEmpID := Self.SelectedRowID;
      Result := pEmpID <> -1;
    end;
    fAlreadyLoaded := True;   //QM-938 EB Fix Emp Search PART 2
  finally

  end;

end;

function TEmployeeSearchForm.GetSelectSQL: string;
begin
  Result := SelectSQL;
end;

function TEmployeeSearchForm.GetWhereClause: string;
var
  SQL: TOdDBISAMSqlBuilder;
  IDStr, EmpNumStr: string;
  ID: integer;
  isSingleID, SearchCriteria: boolean;  //QM-938 EB Fix Emp Search PART 2

begin
  IDStr := '';
  EmpNumStr := '';
  isSingleID := False;  //QM-938 EB Fix Emp Search PART 2
  SQL := TOdDBISAMSqlBuilder.Create;
  try
    IDStr := EmployeeDM.GetIDListForEmployeeSelectMode(SearchMode, FManagerID);
    if (IDStr <> '') and (EmpNumber.Text = '') then begin       //qm-938 EB Fix Emp Search
      isSingleID := TryStrToInt(IDStr, ID);   //QM-938 EB Fix Emp Search PART 2

      {Debug: Not used in search at this time}
      if isSingleID then   //QM-938 EB Fix Emp Search PART 2
        EmpNumStr := EmployeeDM.GetEmployeeNumber(ID)   {Get the Emp number coming in}
      else EmpNumStr := '';
     end;

    SQL.NakedBooleanCompare := True;


    SQL.AddKeywordSearch('e.short_name', ShortName.Text);
    SQL.AddKeywordSearch('m.short_name', Manager.Text);
    SQL.AddKeywordSearch('e.emp_number', EmpNumber.Text);
    SQL.AddExactStringFilter('r.description', EmpType.Text);
    //SQL.AddBooleanSearch('e.active', True);


    {Only use the incoming ID if there are no User Entered Values}
    if (IDStr <> '') and not UserEnteredValues then begin   //QM-938 EB Fix Emp Search PART 2
      if (EmpType.Text <> TOMCODE) then     //QM-938 EB Emp Search Changes
        SQL.AddWhereCondition('emp_id in (' + IDStr + ')')
    end;

    SQL.AddWhereCondition('e.active = True'); //QM-938 EB Fix Emp Search PART 2

    //if ManagersOnlyCheck.Checked then
    //  SQL.AddWhereCondition('e.emp_id in (select report_to from employee)');
    Result := SQL.WhereClause;
  finally
    FreeAndNil(SQL);
  end;
end;


procedure TEmployeeSearchForm.RefreshNow;
begin
  inherited;
  EmployeeDM.EmpTypeList(EmpType.Items, True);
end;

procedure TEmployeeSearchForm.SetSearchMode(const Value: TEmployeeSelectMode);
var
  ManagersOnly: Boolean;
begin
  FSearchMode := Value;
  ManagersOnly := SearchMode = esReportManangers;
  ManagersOnlyCheck.Enabled := not ManagersOnly;
  ManagersOnlyCheck.Checked := ManagersOnly;
end;

function TEmployeeSearchForm.UserEnteredValues: boolean;   //QM-938 EB Fix Emp Search PART 2
begin
  Result := False;
  if fAlreadyLoaded then
    if (EmpType.Text <> '') or
       (Manager.Text <> '') or
       (EmpNumber.Text <> '') or
       (ShortName.Text <> '') then
      Result := True;

end;

procedure TEmployeeSearchForm.FormCreate(Sender: TObject);
begin
  inherited;
  RefreshNow;
end;

procedure TEmployeeSearchForm.FormShow(Sender: TObject);
begin
  inherited;
  fAlreadyLoaded := True;  //QM-938 EB Fix Emp Search PART 2
end;

type
  TManagerLink = class(TComponent)
  private
    Combo: TComboBox;
    Mode: TEmployeeSelectMode;
    EmployeeType: string;
    ManagerID: Integer;
    procedure Clicked(Sender: TObject);
  end;

{ TManagerLink }

procedure TManagerLink.Clicked(Sender: TObject);
var
  SelectedID: Variant;
  SelIDStr: string;   //QM-938 EB Fix Emp Search PART 2
begin
  if not Combo.Enabled then
    Exit;

  if TEmployeeSearchForm.Execute(Mode, SelectedID, EmployeeType, ManagerID) then begin
    SelIDStr := VarToStr(SelectedID);
    if not SelectComboBoxItemFromObjects(Combo, SelectedID) then
     //QM-938 EB Fix Emp Search PART 2
      raise Exception.CreateFmt('Employee %s (%s) is not valid for this field', [EmployeeDM.GetEmployeeShortName(SelectedID), SelIDStr]);
    if Assigned(Combo.OnChange) then
      Combo.OnChange(Combo);
  end;
end;

procedure AddEmployeeSearchButtonForCombo(Combo: TComboBox;
  Mode: TEmployeeSelectMode; EmployeeType: string; ManagerID: Integer);
var
  Button: TButton;
  Link: TManagerLink;
begin
  Button := TButton.Create(Combo.Owner);
  Button.Parent := Combo.Parent;
  Button.Left := Combo.Left + Combo.Width + 2;
  Button.Top := Combo.Top;
  Button.Height := Combo.Height;
  Button.Width := 18;
  Button.Caption := '...';
  Button.TabOrder := Combo.TabOrder + 1;
  Button.Font.Assign(Combo.Font);
  Button.Visible := True;

  Link := TManagerLink.Create(Combo.Owner);
  Link.Combo := Combo;
  Link.Mode := Mode;
  Link.EmployeeType := EmployeeType;
  Link.ManagerID := ManagerID;
  Button.OnClick := Link.Clicked;
end;

end.

