unit DamageDefaultEstimateReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, DBCtrls, OdVclUtils;

type
  TDamageDefaultEstimateReportForm = class(TReportBaseForm)
    UtilityCoDamaged: TComboBox;
    UtilityCoLabel: TLabel;
    ProfitCenter: TComboBox;
    ProfitCenterLabel: TLabel;
    FacilityTypeLabel: TLabel;
    FacilityType: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu;

{ TDamageDefaultEstimateReportForm }

procedure TDamageDefaultEstimateReportForm.InitReportControls;
begin
  inherited;
  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
  DM.GetRefDisplayList('dmgco', UtilityCoDamaged.Items, '', True);
  UtilityCoDamaged.Sorted := True;
  SizeComboDropdownToItems(UtilityCoDamaged);
  DM.GetRefCodeList('factype', FacilityType.Items);
  FacilityType.Items.Insert(0, '');
end;

procedure TDamageDefaultEstimateReportForm.ValidateParams;
begin
  inherited;
  SetReportID('DamageDefaultEstimate');

  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParam('FacilityType',  FacilityType.Text);
  {TODO: should ideally pass the ref_id, not the name}
  SetParam('UtilityCo', UtilityCoDamaged.Text);
end;

end.
