unit WorkManagement;

interface

uses
  Windows, Messages, DMu, OdEmbeddable, Classes, ActnList, DBISAMTb, DB,
  ComCtrls, Buttons, StdCtrls, DBCtrls, Controls, ExtCtrls, Menus, 
  QMServerLibrary_Intf, ImgList, OdDbUtils, WorkManageUtils, cxStyles, 
  cxGraphics, cxDataStorage, cxEdit, cxDBData, 
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxLookAndFeels, streetMap,
  cxLookAndFeelPainters, OdDBISAMUtils, cxMaskEdit, cxCalendar, cxTextEdit,
  cxMemo, cxContainer, cxDropDownEdit, cxCheckBox, cxTL, cxDBTL, 
  cxLabel, cxInplaceContainer, WorkManagementDamages, WorkManagementTickets,
  Forms, BaseExpandableDataFrame, BaseExpandableWorkMgtGridFrame,
  WorkManagementWOs, cxNavigator, AppEvnts, OdContainer, cxButtons,
  dxGDIPlusClasses, cxImage, Graphics, FirstTaskSetDialog,
  cxTLdxBarBuiltInMenu, cxCustomData, cxTLData;

const
  UM_SET_TREE = WM_USER + $107;  //QM-11

var
  ActivationCount: integer;

type
  TWorkManagementForm = class(TEmbeddableForm)
    TreePanel: TPanel;
    Splitter: TSplitter;
    PanelTreeviewTitle: TPanel;
    EmpTree: TcxDBTreeList;
    RefreshButton: TButton;
    ActionList: TActionList;
    ShowEmployeeActionList: TActionList;
    ShowActiveAction: TAction;
    ShowInactiveAction: TAction;
    ShowAllAction: TAction;
    TreePopup: TPopupMenu;
    ShowAllAction1: TMenuItem;
    Action21: TMenuItem;
    ShowAllAction2: TMenuItem;
    RefreshTreeAction: TAction;
    SearchTreeAction: TAction;
    SearchButton: TButton;
    N1: TMenuItem;
    GrantUseExemptionMenuItem: TMenuItem;
    RevokeUseExemptionMenuItem: TMenuItem;
    GrantUseExemptionAction: TAction;
    RevokeUseExemptionAction: TAction;
    PanelTickets: TPanel;
    TicketForPanel: TPanel;
    ShowWhichCombo: TComboBox;
    EmpFilterCombo: TComboBox;
    TicketLimitEdit: TcxPopupEdit;
    TicketLimitPanel: TPanel;
    NoLimitLabel: TLabel;
    MaxLimitLabel: TLabel;
    TicketLimitTrackBar: TTrackBar;
    N2: TMenuItem;
    AckAllActivityAction: TAction;
    AckAllActivityMenuItem: TMenuItem;
    ActivitiesTimer: TTimer;
    PanelTop: TPanel;
    ShowFutureTicketsCheckbox: TCheckBox;
    EmpWorkloadSource: TDataSource;
    EmployeeColumn: TcxDBTreeListColumn;
    TicketCountColumn: TcxDBTreeListColumn;
    EmpNumColumn: TcxDBTreeListColumn;
    LocateCountColumn: TcxDBTreeListColumn;
    SortColumn: TcxDBTreeListColumn;
    EmpIDColumn: TcxDBTreeListColumn;
    EmpWorkload: TDBISAMTable;
    UnderMeColumn: TcxDBTreeListColumn;
    IsManagerColumn: TcxDBTreeListColumn;
    TicketViewLimitColumn: TcxDBTreeListColumn;
    ShowFutureTicketsColumn: TcxDBTreeListColumn;
    ReportToColumn: TcxDBTreeListColumn;
    DamageCountColumn: TcxDBTreeListColumn;
    WOCountColumn: TcxDBTreeListColumn;
    ShortNameColumn: TcxDBTreeListColumn;
    EmpTreeIDColumn: TcxDBTreeListColumn;
    EmpTypeColumn: TcxDBTreeListColumn;
    TicketFrame: TWorkMgtTicketsFrame;
    DamageFrame: TWorkMgtDamagesFrame;
    WorkOrderFrame: TWorkMgtWOsFrame;
    WorkStatusTodayColumn: TcxDBTreeListColumn;
    contact_phone: TDBText;
    short_name: TDBText;
    GetEmpID1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Mapallmylocators1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    SendTextMessage1: TMenuItem;
    btnEmerView: TcxButton;
    N5: TMenuItem;
    PlatsUpdated1: TMenuItem;
    PlatsAge: TMenuItem;  //QMANTWO-302
    IsActiveColumn: TcxDBTreeListColumn;
    UpdatePhoneNumber1: TMenuItem;
    EmergenciesCheckbox: TCheckBox;
    GasImage: TImage;
    oq: TLabel;
    N6: TMenuItem;
    EmpOQList: TMenuItem;
    SetFirstTaskReminderAction: TAction;
    N7: TMenuItem;
    SetFirstTaskReminder1: TMenuItem;
    N8: TMenuItem;
    AddWhitelistAction: TAction;
    RemoveFromWhiteListAction: TAction;
    procedure SelectTicketForMainForm(Sender: TObject; TicketID: Integer);
    procedure NewRecord(DataSet: TDataSet);
    procedure EmpTreeCanFocusNode(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var Allow: Boolean);
    procedure EmpTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure EmpTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TicketViewDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ShowWhichComboChange(Sender: TObject);
    procedure RefreshSelectedManagerEvent(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure TreePanelResize(Sender: TObject);
    procedure ShowActiveActionExecute(Sender: TObject);
    procedure ShowInactiveActionExecute(Sender: TObject);
    procedure ShowAllActionExecute(Sender: TObject);
    procedure EmpFilterComboChange(Sender: TObject);
    procedure RefreshTreeActionExecute(Sender: TObject);
    procedure SearchTreeActionExecute(Sender: TObject);
    procedure LocateDataCalcFields(DataSet: TDataSet);
    procedure GrantUseExemptionActionExecute(Sender: TObject);
    procedure RevokeUseExemptionActionExecute(Sender: TObject);
    procedure TreePopupPopup(Sender: TObject);
    procedure TicketLimitTrackBarChange(Sender: TObject);
    procedure cxTicketLimitEditPropertiesCloseUp(Sender: TObject);
    procedure TicketLimitEditKeyPress(Sender: TObject; var Key: Char);
    procedure NoLimitLabelClick(Sender: TObject);
    procedure MaxLimitLabelClick(Sender: TObject);
    procedure TicketLimitTrackBarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AckAllActivityActionExecute(Sender: TObject);
    procedure ActivitiesTimerTimer(Sender: TObject);
    procedure DamageViewDblClick(Sender: TObject);
    procedure ShowFutureTicketsCheckboxClick(Sender: TObject);
    procedure WorkOrderViewDblClick(Sender: TObject);
    procedure WorkOrderAfterOpen(DataSet: TDataSet);
    procedure TreeListStylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode; var AStyle: TcxStyle);
    procedure EmpTreeGetNodeImageIndex(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; AIndexType: TcxTreeListImageIndexType;
      var AIndex: TImageIndex);
    procedure EmpWorkloadAfterScroll(DataSet: TDataSet);
    procedure WorkOrderFrameClick(Sender: TObject);
    procedure TicketFrameClick(Sender: TObject);
    procedure DamageFrameClick(Sender: TObject);
    procedure WorkOrderFrameAfterExpand(Sender: TObject);
    procedure TicketFrameAfterExpand(Sender: TObject);
    procedure DamageFrameAfterExpand(Sender: TObject);
    procedure RescheduleDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure EmpTreeSelectionChanged(Sender: TObject);
    procedure PanelTreeviewTitleResize(Sender: TObject);
    procedure GetEmpID1Click(Sender: TObject);
    procedure Mapallmylocators1Click(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure SendTextMessage1Click(Sender: TObject);
    procedure btnEmerViewClick(Sender: TObject);
    procedure UpdatePhoneNumber1Click(Sender: TObject);
    procedure EmergenciesCheckboxClick(Sender: TObject);
    procedure EmpOQListClick(Sender: TObject);  //QMANTWO-302
    procedure SetFirstTaskReminderActionExecute(Sender: TObject);  //QM-494 First Task Reminder EB

    procedure AddWhitelistActionExecute(Sender: TObject);
    procedure RemoveFromWhiteListActionExecute(Sender: TObject);

  private
    SelectedLocatorID: Integer;
    SelectedLocatorName: string;
    FOnTicketItemSelected: TItemSelectedEvent;
    FOnDamageItemSelected: TItemSelectedEvent;
    FOnWorkOrderItemSelected: TItemSelectedEvent;
    FOnSelectTicket: TItemSelectedEvent;
    ManagerNode: TcxTreeListNode;  // Currently selected manager
    FUpdatingTree: Boolean;
    FSearchMode: Boolean;
    FMyIDs: TStringList;
    FSearchText: string;
    SelectedLocatorTicketLimit: Integer;
    SelectedLocatorShowFutureWork: Boolean;
    FSavingEmpWorkload: Boolean;
    FCurrentExpandedFrame: TExpandableWorkMgtGridFrameBase;
    fHasPlus: boolean; //QM-585 EB Employee Plus whitelist
    procedure ShowWorkSummaryForSelectedNode;
    procedure ShowWorkOrderSummaryForSelectedNode;
    procedure ShowTicketSummaryForSelectedNode;
    procedure ShowDamageSummaryForSelectedNode;
    procedure AddTicketFilterItems;
    function GetNumTicketDays: Integer;
    procedure PopulateHierarchy;
    function WantActiveEmps: Boolean;
    function WantInactiveEmps: Boolean;
    procedure DetermineMyIDs;
    procedure GrantOrRevokeRestrictedUseExemption(Revoke: Boolean);
    procedure AdjustTicketListColumns(NodeType: TNodeType);
    function GetMaxActivityDays: Integer;
    procedure AdjustDamageListView(const ShowDamages: Boolean);
    procedure AdjustWorkOrderListView(const ShowWorkOrders: Boolean);
    function CanManageDamages: Boolean;
    function CanManageWorkOrders: Boolean;
    function CanManageFutureTickets: Boolean;
    procedure RefreshScreenControls;
    function ShowOpenWorkOnly: Boolean;
    procedure PopulateNodeFromServer(EmpID: Integer);
    procedure FilterEmpsByActiveStatus;
    procedure RemoveTicketFromList(Sender: TObject);
    procedure RefreshFrameData;
    procedure ExpandFrame;
    procedure ClearFrameLists;
    procedure RefreshFramesShowOpen;
    function PopulateBreadCrumbData(empID: integer; var RawData: string): Boolean;
    procedure ShowBreadCrumb(Lat,Lng, Utc: string; shortName:string);
    procedure ShowBreadCrumbsList(MgrName:string);
    procedure DoEmployeeSearchInt(Reset: Boolean; ShortName:string; TicketID:integer);  //QM-11 SR

    {QM-444 OQ and QM-585 EB Plus Whitelist}
    //    procedure OQSize(Expanded: Boolean);  //QM-444 EB
    function LoadEmpPlusMgrRights: boolean; //QM-585 EB Plus Whitelist
  public
     WkManFormHandle: THandle;
    property OnTicketItemSelected: TItemSelectedEvent read FOnTicketItemSelected write FOnTicketItemSelected; //used by MainFu to activate the TicketDetail form
    property OnDamageItemSelected: TItemSelectedEvent read FOnDamageItemSelected write FOnDamageItemSelected;
    property OnWorkOrderItemSelected: TItemSelectedEvent read FOnWorkOrderItemSelected write FOnWorkOrderItemSelected;
    property OnSelectTicket: TItemSelectedEvent read FOnSelectTicket write FOnSelectTicket; //Used by MainFu to set it's FCurrentTicketID variable
   
    procedure OnSetTree(var Msg: TMessage); message UM_SET_TREE; //QM-11 SR
    procedure RefreshSelectedManager(ANode: TcxTreeListNode);
    procedure RefreshAllManagers;
    procedure DoEmployeeSearch(Reset: Boolean = True);
    procedure ActivatingNow; override;
    procedure LeavingNow; override;

  end;


implementation

uses SysUtils, Dialogs, OdVclUtils, OdHourglass, OdMiscUtils,
  OdIsoDates, QMConst, OdCxUtils, JclSysInfo, 
   StrUtils,
  SharedImages, Variants, AckAllTicketActivity, SharedDevExStyles,
  LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDMu, TextMessageBox,
  PhoneVerifyDialog;

{$R *.DFM}

procedure TWorkManagementForm.NewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('DeltaStatus').AsString := 'I';
  DM.Engine.SetActiveField(DataSet, True);
end;

procedure TWorkManagementForm.EmpTreeCanFocusNode(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Allow: Boolean);
const
  cGAS = 'GAS';
var
  LastLocatorID: Integer;
  UtilStr, UtilTypeStr: string;
  UtilDelPos: integer;
begin
  Allow := True;
  GasImage.Visible := False;  //QM-444 EB
  oq.Visible := False;

  if FUpdatingTree then
    Exit;

  Assert(Assigned(ANode));
  LastLocatorID := SelectedLocatorID;
  TicketLimitTrackBar.Position := 0;
  Mapallmylocators1.Visible := False;
  {Manager Node}
  if IsManagerNode(EmpTree, ANode) then begin
    If PermissionsDM.CanI(RightMapAllLocators) then  //QMANTWO-302 (and is ManagerNode )
     Mapallmylocators1.Visible := True;
    if ManagerNode <> ANode then begin
      ManagerNode := ANode;
      ClearFrameLists;
      RefreshSelectedManager(ManagerNode);      // Refresh newly selected data
      if ANode.HasChildren and ANode.CanExpand then
        ANode.Expand(True);
    end;
    SelectedLocatorID := EmpIDOfNode(EmpTree, ANode);
    SelectedLocatorName := EmpNameOfNode(EmpTree, ANode);
    AdjustTicketListColumns(ntOrgNode);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntOrgNode;
  end
  {Emergency Node}
  else if IsEmergencyNode(EmpTree, ANode) then begin
    SelectedLocatorID := MAGIC_NEW_EMERG;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    FCurrentExpandedFrame.ParentNodeReferenceName := ShortNameForFile(EmpTree,ManagerNode); //QM-10 EB
    AdjustTicketListColumns(ntEmergency);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntEmergency;//For Tickets, we need to know if emergency node because frame behavior depends on it.
  end
  {Ticket Activity Node}
  else if IsTicketActivityNode(EmpTree, ANode) then begin
    SelectedLocatorID := MAGIC_NEW_ACTIVITY;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntTicketActivities);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntTicketActivities;//For Tickets, we need to know if activity node because frame behavior depends on it.
    FCurrentExpandedFrame.ParentNodeReferenceName :=  ShortNameForFile(EmpTree,ManagerNode);  //QM-10 EB
  end
  {Past Due Node}
  else if IsPastDueNode(EmpTree, ANode) then begin //QM-133 Need something for past due
    SelectedLocatorID := MAGIC_PAST_DUE;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntPastDue);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntPastDue; //QM-133 EB
  end
  {Open Tickets Node}
  else if IsOpenTicketsNode(EmpTree, ANode) then begin  //QM-428 EB
    SelectedLocatorID := MAGIC_OPEN_TICKETS;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntOpen);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntOpen;
  end
  {Today Node}
  else if IsTodayNode(EmpTree, ANode) then begin  //QM-318 EB
    SelectedLocatorID := MAGIC_TODAY;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntToday);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntToday; //QM-318 EB
  end
  {Virtual Bucket Node}
  else if IsVirtualBucketNode(EmpTree, ANode) then begin
    SelectedLocatorID := MAGIC_VIRTUAL;
    Assert(Assigned(ANode.Parent));
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntVirtual);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntVirtual; //QM-486 Virtual EB
  end
  {Locator Node}
  else begin
    SelectedLocatorID := EmpIDOfNode(EmpTree, ANode);
    SelectedLocatorName := EmpNameOfNode(EmpTree, ANode);
    ManagerNode := ANode.Parent;
    AdjustTicketListColumns(ntLocator);
    FCurrentExpandedFrame.SelectedTreeNodeType := ntLocator;
    FCurrentExpandedFrame.ParentNodeReferenceName := ShortNameForFile(EmpTree,ManagerNode);   //QM-10 EB
  end;

  if (SelectedLocatorID = MAGIC_NEW_EMERG) or
     (SelectedLocatorID = MAGIC_NEW_ACTIVITY) or
     (SelectedLocatorID = MAGIC_PAST_DUE) or
     (SelectedLocatorID = MAGIC_TODAY) or            //QM-318 EB Today
     (SelectedLocatorID = MAGIC_VIRTUAL) or
     (LastLocatorID <> SelectedLocatorID) then begin
    TicketFrame.SelectedItemID := 0;
    DamageFrame.SelectedItemID := 0;
    WorkOrderFrame.SelectedItemID := 0;
  end;

  FCurrentExpandedFrame.ShowingSummary := False;
  RefreshFrameData;
  ShowWorkSummaryForSelectedNode;

  if (SelectedLocatorID = MAGIC_NEW_EMERG) or
     (SelectedLocatorID = MAGIC_NEW_ACTIVITY) then begin
    contact_Phone.Visible := False;
    short_name.Visible := False;
  end
  else begin
    contact_phone.Visible := ((contact_phone.Left + contact_phone.Width) < (SearchButton.Left - 2));
    short_name.Visible := ((short_name.Left + short_name.Width) < (SearchButton.Left - 2));

    {OQ/Emp PLUS}
    OQ.Caption := '--';
    UtilTypeStr := '';

    UtilStr := EmployeeDM.GetPLUS(SelectedLocatorID);  //QM-585 EB
    UtilDelPos := Pos(':', UtilStr);
    if UtilStr <> '--' then begin
      OQ.Caption := Copy(UtilStr, UtilDelPos+1, length(UtilStr)-1);
      UtilTypeStr := Uppercase(Copy(UtilStr, 0, UtilDelPos-1));
    end

    else begin   //QM-585 EB - If there was no PLUS Whitelist, check OQ
      OQ.Caption := EmployeeDM.GetOQ(SelectedLocatorID);  //QM-585 EB -Check to see if we can repurpose!
      if oq.Caption <> '--' then
        UtilTypeStr := cGAS;
    end;

    if (oq.Caption = '--') then {nothing found}
      gasImage.Refresh
    else begin
     if (UtilTypeStr = cGAS) then begin
       gasImage.Visible := True;
       gasImage.Refresh;
     end;
     oq.Visible := True;
   end;
  end;

end;

procedure TWorkManagementForm.ClearFrameLists;
begin
  TicketFrame.ClearListTable;
  DamageFrame.ClearListTable;
  WorkOrderFrame.ClearListTable;
end;

procedure TWorkManagementForm.EmpTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := IsEmployeeNode(EmpTree, EmpTree.GetNodeAt(X, Y))
    and (not IsTicketActivityNode(EmpTree, EmpTree.FocusedNode));
end;

procedure TWorkManagementForm.EmpTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  NewNode: TcxTreeListNode;
  NewLocatorID: Integer;
  OldManagerNode: TcxTreeListNode;
  CurrentNode: TcxTreeListNode;
begin
  NewNode := EmpTree.GetNodeAt(X, Y);
  CurrentNode := EmpTree.FocusedNode;
  if IsEmployeeNode(EmpTree, NewNode) and (Source is TcxDragControlObject) and
    (TcxDragControlObject(Source).Control is TcxGridSite)then begin
    NewLocatorID := EmpIDOfNode(EmpTree, NewNode);
    if TcxGridSite(TcxDragControlObject(Source).Control).GridView = TicketFrame.TicketView then
      TicketFrame.ReassignSelectedTickets(SelectedLocatorID, NewLocatorID)
    else if TcxGridSite(TcxDragControlObject(Source).Control).GridView = DamageFrame.DamageView then
      DamageFrame.ReassignSelectedDamages(SelectedLocatorID, NewLocatorID)
    else if TcxGridSite(TcxDragControlObject(Source).Control).GridView = WorkOrderFrame.WorkOrderView then
      WorkOrderFrame.ReassignSelectedWorkOrders(SelectedLocatorID, NewLocatorID);
    RefreshSelectedManager(ManagerNode);  // Update totals on destination
    OldManagerNode := ManagerNode;
    try
      ManagerNode := NewNode;
      while not IsManagerNode(EmpTree, ManagerNode) do
        ManagerNode := NewNode.Parent;
      RefreshSelectedManager(ManagerNode);  // Update totals on destination
    finally
      ManagerNode := OldManagerNode;
    end;
    CurrentNode.Focused := True;
    if TicketFrame.HaveFocusedItem then
      TicketFrame.ForceReload := True;   //QM-695 EB
    ShowWorkSummaryForSelectedNode;
    TicketFrame.ForceReload := False;    //QM-695 EB
  end;
  try
    TcxGridSite(TcxDragControlObject(Source).Control).GridView.Controller.Scroll(sbVertical,scTrack, WorkManagementTickets.ScrollPtr); //qm-419  sr
  except
    //swallow - just do nothing in case ticket moved
  end;

  if (TicketFrame.TicketView.DataController.RecordCount > 0) and (WorkManagementTickets.AfterMoveRec > -1) then  // QM-419 EB
    TicketFrame.MoveFocusToTicketByRow(WorkManagementTickets.AfterMoveRec);
  end;

procedure TWorkManagementForm.EmpWorkloadAfterScroll(DataSet: TDataSet);
begin
  if FUpdatingTree or FSavingEmpWorkload then
    Exit;

  if IsLocator(DataSet) then begin
    SelectedLocatorTicketLimit := DataSet.FieldByName('ticket_view_limit').AsInteger;
    TicketLimitTrackBar.Position := SelectedLocatorTicketLimit;
  end;
  if IsEmployee(DataSet) then begin
    SelectedLocatorShowFutureWork := DataSet.FieldByName('show_future_tickets').AsBoolean;
    ShowFutureTicketsCheckbox.Checked := SelectedLocatorShowFutureWork;
  end;

  if not FSearchMode then
    ShowWorkSummaryForSelectedNode;
end;

function TWorkManagementForm.ShowOpenWorkOnly: Boolean;
begin
  Result := ShowWhichCombo.ItemIndex = 0;
end;

procedure TWorkManagementForm.ShowWorkSummaryForSelectedNode;
var
  Cursor: IInterface;
begin
  if FUpdatingTree then
    Exit;

  Cursor := ShowHourGlass;
  ActivitiesTimer.Enabled := False;

  ShowWorkOrderSummaryForSelectedNode;
  ShowTicketSummaryForSelectedNode;
  ShowDamageSummaryForSelectedNode;
end;

procedure TWorkManagementForm.ShowWorkOrderSummaryForSelectedNode;
var
  NodeIsUnderMe: Boolean;
begin
  if not CanManageWorkOrders then
    Exit;
  FUpdatingTree := True;
  try
    NodeIsUnderMe := IsNodeUnderMe(EmpTree, EmpTree.FocusedNode);

    WorkOrderFrame.LoadWorkOrderList(SelectedLocatorID,
      GetNumTicketDays, NodeIsUnderMe, ShowOpenWorkOnly);
    WorkOrderFrame.GridState.InitialDisplay := True;
  finally
    FUpdatingTree := False;
  end;
end;

procedure TWorkManagementForm.ShowTicketSummaryForSelectedNode;
var
  Node: TcxTreeListNode;
  NodeIsUnderMe: Boolean;
  ManagerID: Integer;
  NodeName: string;
begin
  FUpdatingTree := True;
  try
    Node := EmpTree.FocusedNode;
    NodeIsUnderMe := IsNodeUnderMe(EmpTree, Node);
    ManagerID := ReportToOfNode(EmpTree, Node);
    NodeName := EmpWorkload.FieldByName('short_name').AsString; //QM-486 Virtual Bucket EB
    TicketFrame.ForceReload := True; //QM-751 update
    TicketFrame.LoadTicketList(SelectedLocatorID, ManagerID, GetNumTicketDays,
      GetMaxActivityDays, NodeName, NodeIsUnderMe, ShowOpenWorkOnly);
    TicketFrame.GridState.InitialDisplay := True;    //5/31
         
    TicketFrame.ExportButton.Enabled := PermissionsDM.CanI(RightWorkManagementExport);  //QM-10 GPX Export
    TicketFrame.btnBulkDueDate.Visible := PermissionsDM.CanI(RightWorkManagementBulkDueDate);  //QM-959 EB

  finally
    FUpdatingTree := False;
  end;
end;

procedure TWorkManagementForm.ShowDamageSummaryForSelectedNode;
var
  NodeIsUnderMe: Boolean;
begin
  if not CanManageDamages then
    Exit;
  FUpdatingTree := True;
  try
    NodeIsUnderMe := IsNodeUnderMe(EmpTree, EmpTree.FocusedNode);

    DamageFrame.LoadDamageList(SelectedLocatorID,
      GetNumTicketDays, NodeIsUnderMe, ShowOpenWorkOnly);
  finally
    FUpdatingTree := False;
  end;
end;

procedure TWorkManagementForm.TicketViewDblClick(Sender: TObject);
var
  TicketID: Integer;
begin
  // Show the ticket detail
  if TicketFrame.HaveFocusedItem then begin
    TicketFrame.GetCurrentItemID(TicketID);
    TicketFrame.GridState.LastOpenedID := TicketID;   //QMANTWO-690 EB

    DamageFrame.SelectedItemID := 0;
    WorkOrderFrame.SelectedItemID := 0;
    if TicketID <> 0 then
      if Assigned(FOnTicketItemSelected) then
        FOnTicketItemSelected(Self, TicketID);
  end;
end;

procedure TWorkManagementForm.DamageViewDblClick(Sender: TObject);
var
  DamageID: Integer;
begin
  inherited;
  // Show the damage detail
  if DamageFrame.HaveFocusedItem then begin
    DamageFrame.GetCurrentItemID(DamageID);
    DamageFrame.GridState.LastOpenedID := DamageID;  //QMANTWO-690 EB
    TicketFrame.SelectedItemID := 0;
    WorkOrderFrame.SelectedItemID := 0;
    if DamageID <> 0 then begin
      LDamageDM.UpdateDamageInCache(DamageID);
      if Assigned(FOnDamageItemSelected) then
        FOnDamageItemSelected(Self, DamageID);
    end;
  end;
end;

procedure TWorkManagementForm.WorkOrderViewDblClick(Sender: TObject);
var
  WorkOrderID: Integer;
begin
  if WorkOrderFrame.HaveFocusedItem then begin
    WorkOrderFrame.GetCurrentItemID(WorkOrderID);
    WorkOrderFrame.GridState.LastOpenedID := WorkOrderID;   //QMANTWO-690 EB
    TicketFrame.SelectedItemID := 0;
    DamageFrame.SelectedItemID := 0;
    if WorkOrderID <> 0 then
      if Assigned(FOnWorkOrderItemSelected) then
        FOnWorkOrderItemSelected(Self, WorkOrderID);
  end;
end;

procedure TWorkManagementForm.FormCreate(Sender: TObject);
begin
  AddTicketFilterItems;
  TreePanel.Width := DM.UQState.TMSplitterPos;
  FMyIDs := TStringList.Create;
  AdjustTicketListColumns(ntOrgNode);
  TicketFrame.CreateTicketListTable;
  DamageFrame.CreateDamageListTable;
  WorkOrderFrame.CreateWorkOrderListTable;

  FCurrentExpandedFrame := nil;

  TicketFrame.OnSelectItem := SelectTicketForMainForm;
  TicketFrame.OnViewDblClick := TicketViewDblClick;
  TicketFrame.OnRefreshSelectedManager := RefreshSelectedManagerEvent;
  TicketFrame.OnRescheduleItemDragDrop := RescheduleDragDrop;
  TicketFrame.OnAfterExpand := TicketFrameAfterExpand;
  TicketFrame.OnRemoveTicketFromList := RemoveTicketFromList;
  TicketFrame.CreateStructures;   //QMANTWO-690 EB

  DamageFrame.OnViewDblClick := DamageViewDblClick;
  DamageFrame.OnRefreshSelectedManager := RefreshSelectedManagerEvent;
  DamageFrame.OnAfterExpand := DamageFrameAfterExpand;
  DamageFrame.CreateStructures; //QMANTWO-690 EB

  WorkOrderFrame.OnViewDblClick := WorkOrderViewDblClick;
  WorkOrderFrame.OnRefreshSelectedManager := RefreshSelectedManagerEvent;
  WorkOrderFrame.OnAfterExpand := WorkOrderFrameAfterExpand;
  WorkOrderFrame.CreateStructures;  //QMANTWO-690 EB

  ActivationCount := 0;
end;

procedure TWorkManagementForm.RescheduleDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  inherited;
  Showmessage('Not yet implemented. The selected ticket ids are: ' + TicketFrame.RescheduledItemIDs);
{todo something like this:
    DM.RescheduleTicketWorkloadDates(TicketFrame.RescheduledItemIDs, Today);--todo Tomorrow, Unscheduled }
end;

procedure TWorkManagementForm.WorkOrderFrameClick(Sender: TObject);
begin
  DamageFrame.Contract(alBottom);
  TicketFrame.Contract(alBottom);
  WorkOrderFrame.Expand;
  FCurrentExpandedFrame := WorkOrderFrame;
end;

procedure TWorkManagementForm.TicketFrameClick(Sender: TObject);
begin
  WorkOrderFrame.Contract(alTop);
  DamageFrame.Contract(alBottom);
  TicketFrame.Expand;
  TicketFrame.ChangeViewEmergencies(EmergenciesCheckBox.Checked);   //QM-425 EB
  if TicketFrame.HasPrevSelectedItems(SelectedLocatorID) then
    TicketFrame.ReSelectList;
  FCurrentExpandedFrame := TicketFrame;
end;

procedure TWorkManagementForm.DamageFrameClick(Sender: TObject);
begin
  WorkOrderFrame.Contract(alTop);
  TicketFrame.Contract(alTop);
  DamageFrame.Expand;
  FCurrentExpandedFrame := DamageFrame;
end;

procedure TWorkManagementForm.WorkOrderFrameAfterExpand(Sender: TObject);
begin
  FCurrentExpandedFrame := WorkOrderFrame;
end;

procedure TWorkManagementForm.TicketFrameAfterExpand(Sender: TObject);
begin
  FCurrentExpandedFrame := TicketFrame;
end;

procedure TWorkManagementForm.DamageFrameAfterExpand(Sender: TObject);
begin
  FCurrentExpandedFrame := DamageFrame;
end;

procedure TWorkManagementForm.SelectTicketForMainForm(Sender: TObject; TicketID: Integer);
begin
  if Assigned(FOnSelectTicket) then
    FOnSelectTicket(Self, TicketID);
end;

procedure TWorkManagementForm.SendTextMessage1Click(Sender: TObject);  // QM-9 sr
var
  SelectedEmpID :integer;
  textToSend, ShortName:string;
  WorkStatus:string;
  PhoneNo:string;
  function GetNumbers(const Value: string): string;
  var
    ch: char;
    Index, Count: integer;
  begin
    SetLength(Result, Length(Value));
    Count := 0;
    for Index := 1 to length(Value) do
    begin
      ch := Value[Index];
      if (ch >= '0') and (ch <='9') then
      begin
        inc(Count);
        Result[Count] := ch;
      end;
    end;
    SetLength(Result, Count);
  end;

begin
  inherited;
  SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
  shortName := EmpWorkload.FieldByName('short_name').AsString;

  try
    frmTextMessageBox:= TfrmTextMessageBox.Create(nil);
    DM.qryEmpPhoneCo.Params.ParamByName('EmpID').Value:=SelectedEmpID;
    DM.qryEmpPhoneCo.open;
    If DM.qryEmpPhoneCo.FieldByName('TextMailAddress').IsNull then
    begin
      If (MessageDlg(shortName+' is not configured to accept text messages from QManager.  '
      +#13+#10+'Please contact the Help Desk and ask that they set '+shortName
      +#13+#10+' Phone Company in QAdmin Employee table.', mtError, [mbOK], 0)=mrOK) then exit;
    End;

    PhoneNo :=  GetNumbers(EmpWorkload.FieldByName('contact_phone').AsString);

    if Length(PhoneNo)<>10 then
    begin
      if (MessageDlg(PhoneNo+' does not appear to be normal cell phone number. '+
        'Are you sure you want to send that person a Text Message?', mtWarning, [mbYes, mbNo], 0) = mrCancel) then exit;
    end;

    WorkStatus := EmpWorkload.FieldByName('work_Status_today').AsString;
    if WorkStatus='OFF' then
    Begin
      if (MessageDlg(shortName+' does not appear to be working at this time. Are you sure you want to send that person a Text Message?', mtWarning, [mbYes, mbNo], 0) = mrCancel) then exit;
    End;



    frmTextMessageBox.ShowModal;

    if frmTextMessageBox.ModalResult =mrOk then
    begin
      textToSend:=frmTextMessageBox.textMemo.Text;
      DM.SendTextToLocator(SelectedEmpID, textToSend);
    end
    else
    ShowMessage('Text cancelled');

  finally
    DM.qryEmpPhoneCo.close;
    frmTextMessageBox.Release;
  end;
end;

procedure TWorkManagementForm.SetFirstTaskReminderActionExecute(Sender: TObject);  //QM-494 First Task Reminder EB
begin
  inherited;
  OpenFirstTaskReminderFor(SelectedLocatorID);
end;

procedure TWorkManagementForm.FormShow(Sender: TObject);
begin
  btnEmerView.visible:=false;
  btnEmerView.visible:=DM.CanViewEmergencies;  //QM-11 SR
  if (not EmpWorkload.Active) or (EmpWorkload.IsEmpty) then
    PopulateHierarchy;
  // This fixes a bug where the tree would not align to fill the client area until the form was resized
  WindowState := wsMaximized;
  ActivitiesTimer.Interval := DM.GetConfigurationDataInt('TicketActRefreshSeconds', 120) * 1000;
  TicketLimitTrackBar.Max := DM.GetConfigurationDataInt('TicketChangeViewLimitsMax',100);
  MaxLimitLabel.Caption := IntToStr(TicketLimitTrackBar.Max);

  RefreshFrameData;
  WorkOrderFrame.Setup('Work Orders: ', WorkOrderFrameClick);
  TicketFrame.Setup('Tickets: ', TicketFrameClick);
  DamageFrame.Setup('Damages: ', DamageFrameClick);

  ExpandFrame;
end;

procedure TWorkManagementForm.ExpandFrame;
begin
  //Returning to screen from work item or a different node selected:
  if Assigned(FCurrentExpandedFrame) and FCurrentExpandedFrame.Visible then begin
    if FCurrentExpandedFrame is TWorkMgtWOsFrame then
      WorkOrderFrameClick(Self)
    else if FCurrentExpandedFrame is TWorkMgtTicketsFrame then
      TicketFrameClick(Self)
    else if FCurrentExpandedFrame is TWorkMgtDamagesFrame then
      DamageFrameClick(Self);
  end
  //Initially viewing screen:
  else if WorkOrderFrame.Visible then
    WorkOrderFrameClick(Self)
  else
    TicketFrameClick(Self);

  RefreshFramesShowOpen;
end;

procedure TWorkManagementForm.FormDestroy(Sender: TObject);
const
  UM_SHUT_DOWN = WM_USER + $108;  //QM-11   SR
var
  AHandle : THandle;
begin
  FreeAndNil(FMyIDs);
  TicketFrame.DestroyStructures;     //QMANTWO-690 EB
  WorkOrderFrame.DestroyStructures;  //QMANTWO-690 EB
  DamageFrame.DestroyStructures;     //QMANTWO-690 EB
  AHandle:=FindWindow('TfrmEMDispatch', NIL);   //QM-11   SR
  if Handle <> 0 then
  PostMessage(AHandle, UM_SHUT_DOWN, 0,0);

end;

procedure TWorkManagementForm.ShowWhichComboChange(Sender: TObject);
begin
  RefreshFramesShowOpen;
  ShowWorkSummaryForSelectedNode;
end;

procedure TWorkManagementForm.RefreshFramesShowOpen;
begin
  TicketFrame.ShowOpenItemsOnly := ShowOpenWorkOnly;
  DamageFrame.ShowOpenItemsOnly := ShowOpenWorkOnly;
  WorkOrderFrame.ShowOpenItemsOnly := ShowOpenWorkOnly;
end;

procedure TWorkManagementForm.RefreshSelectedManagerEvent(Sender: TObject);
begin
  RefreshSelectedManager(EmpTree.FocusedNode);
end;

procedure TWorkManagementForm.AddTicketFilterItems;
begin
  ShowWhichCombo.Items.Clear;
  ShowWhichCombo.Items.AddInteger('Open Only', 0);
  ShowWhichCombo.Items.AddInteger('Yesterday', 1);
  ShowWhichCombo.Items.AddInteger('Last 7 Days', 7);
  ShowWhichCombo.Items.AddInteger('Last 15 Days', 15);
  ShowWhichCombo.Items.AddInteger('Last 30 Days', 30);
  ShowWhichCombo.Items.AddInteger('Last 60 Days', 60);
  ShowWhichCombo.ItemIndex := 0;
end;

procedure TWorkManagementForm.AddWhitelistActionExecute(Sender: TObject);   //QM-585 Eb PLUS Whitelist
var
  SelectedEmpID, SelWhiteList: integer;
  SelectedEmp: String;
begin
  inherited;

  SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
  SelectedEmp := EmployeeDM.GetEmployeeShortName(SelectedEmpID);


  With Sender as TMenuItem do begin
    SelWhiteList := tag;
    ShowMessage('Adding ' +  SelectedEmp + ' to ' +  inttostr(tag));
  end;
  EmployeeDM.AddEmptoPlusWhiteList(SelectedEmpID, SelWhiteList); //QM-585 EB PLUS Whitelist
end;


procedure TWorkManagementForm.RemoveFromWhiteListActionExecute(Sender: TObject); //QM-585 EB PLUS Whitelist
var
  SelectedEmpID, SelWhiteList: integer;
  SelectedEmp: String;
begin
  inherited;
  SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
  SelectedEmp := EmployeeDM.GetEmployeeShortName(SelectedEmpID); //QM-585 EB PLUS Whitelist

  With Sender as TMenuItem do begin
    SelWhiteList := tag;
    ShowMessage('Removing ' +  SelectedEmp + ' from ' +  inttostr(tag));
  end;
  EmployeeDM.RemoveEmpFromPlusWhiteList(SelectedEmpID, SelWhiteList); //QM-585 EB PLUS Whitelist
end;

function TWorkManagementForm.GetNumTicketDays: Integer;
begin
  Assert(ShowWhichCombo.ItemIndex > -1);
  Result := GetComboObjectInteger(ShowWhichCombo);
end;

procedure TWorkManagementForm.ActionListUpdate(Action: TBasicAction;var Handled: Boolean);
begin
  TicketLimitEdit.Enabled := PermissionsDM.CanI(RightTicketsChangeViewLimits) and IsLocator(EmpWorkload);
  ShowFutureTicketsCheckbox.Enabled := CanManageFutureTickets;
  ShowWhichCombo.Enabled := IsEmployee(EmpWorkload) or IsEmergency(EmpWorkload);
  EmpFilterCombo.Enabled := ShowWhichCombo.Enabled;
  AckAllActivityAction.Enabled := IsManager(EmpWorkload);
  ActivitiesTimer.Enabled := IsTicketActivity(EmpWorkload);
  EmergenciesCheckBox.Enabled := IsEmployee(EmpWorkload) or IsPastDue(EmpWorkload) or IsEmergency(EmpWorkload) or IsTodayDue(EmpWorkload);  //QM-425 EB
end;

function TWorkManagementForm.CanManageFutureTickets: Boolean;
var
  SelectedEmpID: Integer;
begin
  Result := False;
  if PermissionsDM.CanI(RightTicketsManageFutureTickets) and IsEmployee(EmpWorkload) then begin
    SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
    Result := not (SelectedEmpID = DM.EmpID); //cannot modify own
  end;
end;

// The below works because only 1 emergency/activity is acked at a time


procedure TWorkManagementForm.RemoveTicketFromList(Sender: TObject);
var
  LocsOnTicket: Integer;
begin
  Assert(TicketFrame.ListTable.RecordCount > 0);
  LocsOnTicket := TicketFrame.LocateData.RecordCount;
  TicketFrame.ListTable.Delete;
  Assert(IDOfNode(EmpTree, EmpTree.FocusedNode) = EmpWorkload.FieldByName('emp_tree_id').Value);
  FSavingEmpWorkload := True;
  EditDataSet(EmpWorkload);
  try
    EmpWorkload.FieldByName('tickets_tot').AsInteger := EmpWorkload.FieldByName('tickets_tot').AsInteger - 1;
    EmpWorkload.FieldByName('locates_tot').AsInteger := EmpWorkload.FieldByName('locates_tot').AsInteger - LocsOnTicket;
    EmpWorkload.Post;
    FSavingEmpWorkload := False;
  except
    EmpWorkload.Cancel;
    raise;
  end;
end;

procedure TWorkManagementForm.EmpTreeGetNodeImageIndex(
  Sender: TcxCustomTreeList; ANode: TcxTreeListNode;
  AIndexType: TcxTreeListImageIndexType; var AIndex: TImageIndex);
begin
  {Emergency Node}
  if IsEmergencyNode(EmpTree, ANode) then
    AIndex := ImageIndexClock
  {Ticket Activity Node}
  else if IsTicketActivityNode(EmpTree, ANode) then
    AIndex := ImageIndexGreenFlag
  {Past Due Node}
  else if ISPastDueNode(EmpTree, ANode) then   //QM-133 EB Past Due
    AIndex := ImageIndexRedFlag
  {Open Tickets - No list for this one, just counts}
  else if IsOpenTicketsNode(EmpTree, ANode) then //QM-428 EB Open Tickets
    AIndex := ImageIndexBlueFlag
  else if IsTodayNode(EmpTree, ANode) then     //QM-318 EB Today
    AIndex := ImageIndexYellowFlag
  else if IsVirtualBucketNode(EmpTree, ANode) then  //QM-486 EB Virtual Bucket
    AIndex := ImageIndexVirtualBucket
  {Assigned Nodes}
  else if Assigned(ANode) then begin
    {Manager Nodes}
    if IsManagerNode(EmpTree, ANode) then begin
      If IsInActiveEmp(EmpTree, ANode) then
        AIndex := ImageIndexInactiveMgr
      else if IsClockedInNode(EmpTree, ANode) then
        AIndex := ImageIndexManagerIn
      else if IsClockedOutNode(EmpTree, ANode) then
        AIndex := ImageIndexManagerOut
      else if IsCalledInNode(EmpTree, ANode) then
        AIndex := ImageIndexCalledIn
      else
        AIndex := ImageIndexManagerOff;
    end
    else if IsTomorrowNode(EmpTree, ANode) then
        AIndex := ImageIndexTomorrow // QM-294 EB

    //Locator Nodes
    else begin
      If IsInActiveEmp(EmpTree, ANode) then
        AIndex := ImageIndexInactiveEmp
      else if IsClockedInNode(EmpTree, ANode) then
        AIndex := ImageIndexLocatorIn
      else if IsClockedOutNode(EmpTree, ANode) then
        AIndex := ImageIndexLocatorOut
      else if IsCalledInNode(EmpTree, ANode) then
        AIndex := ImageIndexCalledIn
      else
        AIndex := ImageIndexLocatorOff;
    end;
  end;
end;

procedure TWorkManagementForm.EmpTreeSelectionChanged(Sender: TObject);
var
  lim: string;
begin
  inherited;
  TicketFrame.cxHyperLinkEdit1.Visible:= false;   //QM-461  SR    clears old URL
  contact_phone.Left := short_name.Left; // + short_name.Width + 3;
  TicketFrame.GridState.InitialDisplay := True;    //QMANTWO-564 EB  Don't let idle change to selected     5/29
  WorkOrderFrame.GridState.InitialDisplay := True; //QMANTWO-690 EB
  DamageFrame.GridState.InitialDisplay := True;    //QMANTWO-690 EB

  if (PermissionsDM.CanI(RightTicketsUseRouteOptimize)) and  //QM-702 Route Order Optimize EB then
      (SelectedLocatorID > 0) and
      (GetEmpPermissionFromServer(SelectedLocatorID, RightTicketsUseRouteOrder, lim)) then
  //    (PermissionsDM.GetEmpRightFromServer(SelectedLocatorID, RightTicketsUseRouteOrder, lim)) then
      TicketFrame.ToggleOptimizeRouteOn(True)
    else
      TicketFrame.ToggleOptimizeRouteOn(False);

end;

procedure TWorkManagementForm.TreeListStylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
  var AStyle: TcxStyle);
begin
  if IsEmergencyNode(EmpTree, ANode) then
    AStyle := SharedDevExStyleData.BoldRedFontStyle
  else if IsPastDueNode(EmpTree, ANode) then     //QM-133 EB Past Due
    AStyle := SharedDevExStyleData.BoldMaroonFont
  else if IsOpenTicketsNode(EmpTree, ANode) then
    AStyle := SharedDevExStyleData.BoldBlueFont  //QM-428 EB Open
  else if IsTodayNode(EmpTree, ANode) then       //QM-318 EB Today
    AStyle := SharedDevExStyleData.BoldFontStyle
  else if IsVirtualBucketNode(EmpTree, ANode) then  //QM-486 EB Virtual Bucket
    AStyle := SharedDevExStyleData.BoldBrownStyle
  else if IsManagerNode(EmpTree, ANode) and (FMyIDs.IndexOf(IntToStr(EmpIDOfNode(EmpTree, ANode))) > -1) then
    AStyle := SharedDevExStyleData.BoldFontStyle
  else If IsInActiveEmp(EmpTree, ANode) then
    AStyle := SharedDevExStyleData.GrayFontInactiveNode;
end;

procedure TWorkManagementForm.FormMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  // Mouse wheel messages seem to be routed to the ticket filter combobox
  // even when the form is disabled in Delphi 6.  This code fixes that bug.
  if not GetParentForm(Self).Enabled then
    Handled := True;
end;

procedure TWorkManagementForm.TreePanelResize(Sender: TObject);
begin
  DM.UQState.TMSplitterPos := TreePanel.Width;
end;

procedure TWorkManagementForm.RefreshSelectedManager(ANode: TcxTreeListNode);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if IsManagerNode(EmpTree, ANode) and IsNodeUnderMe(EmpTree, ANode) then begin
    FUpdatingTree := True;
    try
      PopulateNodeFromServer(EmpIDOfNode(EmpTree, ANode));
    finally
      FUpdatingTree := False;
    end;
  end;
end;

procedure TWorkManagementForm.RefreshAllManagers;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  ClearFrameLists;
  FUpdatingTree := True;
  try
    PopulateAllOpenManagersFromServer(EmpTree);
  finally
    FUpdatingTree := False;
  end;
  if fCurrentExpandedFrame = TicketFrame then
    TicketFrame.ForceReload := True;

  ShowWorkSummaryForSelectedNode;  // refresh my node
  TicketFrame.ForceReload := False;
end;

procedure TWorkManagementForm.WorkOrderAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetFieldDisplayFormat(DataSet, 'due_date', 'mmm d, yyyy t');
  SetFieldDisplayFormat(DataSet, 'closed_date', 'mmm d, yyyy t');
  SetFieldDisplayFormat(DataSet, 'transmit_date', 'mmm d, yyyy t');
end;

procedure TWorkManagementForm.DetermineMyIDs;
var
  RestrictionString: string;
begin
  FMyIDs.Clear;
  if PermissionsDM.CanI(RightTicketManagement) then begin
    RestrictionString := PermissionsDM.GetLimitation(RightTicketManagement);
    FMyIDs.CommaText := RestrictionString;
  end;
end;

procedure TWorkManagementForm.PanelTreeviewTitleResize(Sender: TObject);
begin
  inherited;
  contact_phone.Left := short_name.Left + short_name.Width + 3;
  contact_phone.Visible := ((contact_phone.Left + contact_phone.Width) < (SearchButton.Left - 2));
  short_name.Visible := ((short_name.Left + short_name.Width) < (SearchButton.Left - 2));
end;

procedure TWorkManagementForm.PopulateHierarchy;
begin
  FUpdatingTree := True;
  try
    CloseDataSet(EmpWorkload);
    ClearTable(EmpWorkload);
    DetermineMyIDs;
    DM.CallingServiceName('GetHierarchy2');
    DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.GetHierarchy2(DM.Engine.SecurityInfo, True, True));
    EmpWorkload.Open;
    FilterEmpsByActiveStatus;
    EmpTree.FullExpand;
    EmpTree.FocusedNode := EmpTree.LastNode;
    EmpTree.FocusedNode.MakeVisible;
    EmpTree.BestFitVisibleColumns;
    LoadEmpPlusMgrRights;  //QM-585
  finally
    FUpdatingTree := False;
  end;
end;



procedure TWorkManagementForm.ShowActiveActionExecute(Sender: TObject);
begin
  EmpFilterCombo.ItemIndex := 0;
  EmpFilterComboChange(EmpFilterCombo);
end;

procedure TWorkManagementForm.ShowInactiveActionExecute(Sender: TObject);
begin
  EmpFilterCombo.ItemIndex := 1;
  EmpFilterComboChange(EmpFilterCombo);
end;

procedure TWorkManagementForm.ShowAllActionExecute(Sender: TObject);
begin
  EmpFilterCombo.ItemIndex := 2;
  EmpFilterComboChange(EmpFilterCombo);
end;

procedure TWorkManagementForm.EmergenciesCheckboxClick(Sender: TObject);
begin
  inherited;
   if FCurrentExpandedFrame = TicketFrame then
    TicketFrame.ChangeViewEmergencies(EmergenciesCheckBox.Checked);   //QM-425 EB
end;

procedure TWorkManagementForm.EmpFilterComboChange(Sender: TObject);
begin
  RefreshSelectedManager(ManagerNode); // They care more about this one, so don't refresh everything
  if FCurrentExpandedFrame = TicketFrame then
    TicketFrame.lblAllCounts.Caption := '';  //QM-695 Open Tickets
end;

procedure TWorkManagementForm.EmpOQListClick(Sender: TObject);
var
  SelMgrID: integer;
begin
  inherited;
  {Place holder - note implemented with QM-444 Part 2 yet}
  SelMgrID := EmpIDOfNode(EmpTree,  ManagerNode);
  If (SelMgrID <> SelectedLocatorID) then
    SelMgrID := DM.EmpID;
end;

function TWorkManagementForm.WantActiveEmps: Boolean;
begin
  Result := EmpFilterCombo.ItemIndex in [0, 2];
end;

function TWorkManagementForm.WantInactiveEmps: Boolean;
begin
  Result := EmpFilterCombo.ItemIndex in [1, 2];
end;

procedure TWorkManagementForm.RefreshTreeActionExecute(Sender: TObject);
begin
  RefreshAllManagers;
end;

procedure TWorkManagementForm.DoEmployeeSearch(Reset: Boolean);
var
  Node: TcxTreeListNode;
  DoSearch: Boolean;
  Cursor: IInterface;
begin
  DoSearch := not Reset;
  Reset := (FSearchText = '') or Reset;
  if Reset or (FSearchText = '') then
    DoSearch := InputQuery('Employee Search', 'Type an employee name to search (press F3 to search for the next occurrence):', FSearchText);
  if DoSearch then begin
    Cursor := ShowHourglass;
    FSearchMode := True;
    EmpTree.BeginUpdate;
    try
      Node := FindLocatorInTreeView(FSearchText, EmpTree, FMyIDs, Reset, EmpFilterCombo.ItemIndex);
    finally
      FSearchMode := False;
      EmpTree.EndUpdate;
    end;
    if Assigned(Node) then begin
      EmpTree.FocusedNode := Node;
      EmpTree.TopVisibleNode := Node;
      ShowWorkSummaryForSelectedNode;
    end;
  end;
end;

procedure TWorkManagementForm.DoEmployeeSearchInt(Reset: Boolean; ShortName:string; TicketID:integer);  //QM-11 SR
var
  Node: TcxTreeListNode;
  Cursor: IInterface;
begin
    Cursor := ShowHourglass;
    FSearchMode := True;
    EmpTree.BeginUpdate;
    try
      Node := FindLocatorInTreeView(FSearchText, EmpTree, FMyIDs, Reset, EmpFilterCombo.ItemIndex);
    finally
      FSearchMode := False;
      EmpTree.EndUpdate;
    end;
    if Assigned(Node) then begin
      EmpTree.FocusedNode := Node;
      EmpTree.TopVisibleNode := Node;
      ShowWorkSummaryForSelectedNode;
      TicketFrameClick(nil);
      TicketFrame.MoveToTicketID(ticketID);
    end;
end;

procedure TWorkManagementForm.SearchTreeActionExecute(Sender: TObject);
begin
  DoEmployeeSearch;
end;

function TWorkManagementForm.LoadEmpPlusMgrRights: boolean;  //QM-585 EB Plus Whitelist
      procedure AddMenuItem(Caption: string; TermGroup: integer; ToAdd:boolean);
      var
        NewMenuItemAdd: TMenuItem;
        MenuCount: integer;
      begin
        NewMenuItemAdd := TMenuItem.Create(TreePopup);
        MenuCount := TreePopup.Items.Count;
        if Caption ='-' then begin
          NewMenuItemAdd.Caption := Caption;
          TreePopup.Items.Add(NewMenuItemAdd);
        end
        else begin
          TreePopup.Items.Add(NewMenuItemAdd);
          if ToAdd then
            NewMenuItemAdd.OnClick := AddWhiteListActionExecute
          else
           NewMenuItemAdd.OnClick := RemoveFromWhiteListActionExecute;

          NewMenuItemAdd.Caption := Caption;
          NewMenuItemAdd.Tag := TermGroup;
       //   NewMenuItemAdd.Enabled := False;   //QM-585 Plus Whitelist - SET THIS BACK
        end;
      end;
var
  PlusStr: string;
  WhiteList: Tstringlist;
  i, outTag: integer;
begin
  PlusStr := EmployeeDM.GetPLUSForManager(DM.EmpID);
  if PlusStr > '' then begin
    Try
      fHasPlus := True;
      WhiteList := TStringList.Create;
      WhiteList.StrictDelimiter := True;
      WhiteList.Delimiter := '^';
  //    WhiteList.LineBreak := '<|>';
      WhiteList.DelimitedText := PlusStr;
      for i := 0 to WhiteList.Count - 1 do begin
        if not tryStrToInt(WhiteList.Names[i], outtag) then
          outtag := 0;
        AddMenuItem('Add to ' + WhiteList.ValueFromIndex[i], outtag, True );
        AddMenuItem('Remove from ' + WhiteList.ValueFromIndex[i], outtag, False);
        AddMenuItem('-', 0, False);
        //        ShowMessage('Name:  ' + WhiteList.Names[i]);
//        ShowMessage('Value: ' + WhiteList.ValueFromIndex[i]);

      end;

    Finally
      freeandnil(WhiteList);
    End;

//  TreePopup.Items.Add(TMenuItem);
  end;
  result := True;
end;

procedure TWorkManagementForm.LocateDataCalcFields(DataSet: TDataSet);
var
  Alert2: string;
begin
  Alert2 := '';
  if DataSet.FieldByName('alert').AsBoolean or SameText(DataSet.FieldByName('locatealert').AsString, 'A') then
    Alert2 := '!';
  DataSet.FieldByName('alert2').AsString := Alert2;

end;

procedure TWorkManagementForm.GrantUseExemptionActionExecute(Sender: TObject);
begin
  GrantOrRevokeRestrictedUseExemption(False);
end;

procedure TWorkManagementForm.RevokeUseExemptionActionExecute(Sender: TObject);
begin
  GrantOrRevokeRestrictedUseExemption(True);
end;

procedure TWorkManagementForm.GrantOrRevokeRestrictedUseExemption(Revoke: Boolean);
var
  Msg: string;
begin
  if SelectedLocatorID <= 0 then
    Exit;

  if Revoke then
    Msg := 'Restricted use exemption has been revoked.'
  else
    Msg := 'A 48 hour restricted use exemption has been granted.';

  DM.Engine.UpdateRestrictedUseExemption(SelectedLocatorID, Revoke);
  MessageDlg(Msg, mtInformation, [mbOk], 0);
end;

procedure TWorkManagementForm.TreePopupPopup(Sender: TObject);
var
  SelectedEmpID: integer;
  PlatDate: TDateTime;
  PlatAge: Integer;
  EmpPlusGroups: TIntegerList;
begin
  GrantUseExemptionAction.Enabled := (SelectedLocatorID > 0);
  RevokeUseExemptionAction.Enabled := GrantUseExemptionAction.Enabled;
  SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;

  EmployeeDM.GetPlatDateAndAge(SelectedEmpID, PlatDate, PlatAge);
  if PlatDate > 367 then begin
    PlatsUpdated1.Visible := True;
    PlatsAge.Visible := True;
    PlatsUpdated1.Caption := 'Plats Last Updated: ' + DateTimeToStr(PlatDate);
    PlatsAge.Caption := 'Plats Age: ' + IntToStr(PlatAge) + ' days';
  end
  else begin
    PlatsUpdated1.Visible := False;
    PlatsAge.Visible := False;
  end;

  if SelectedEmpID > 0 then //QM-494 First Task EB
    SetFirstTaskReminderAction.Visible := PermissionsDM.CanI(RightTicketsSetFirstTaskReminder)
  else
    SetFirstTaskReminderAction.Visible := False;

  if SelectedEmpID > 0 then begin //QM-585 EB PLUS Whitelist
    EmpPlusGroups := TIntegerList.Create;
 //   EmployeeDM.GetPlusInfo(SelectedEmpID, EmpPlusGroups);   //QM-585 EB PLUS Whitelsit (FINISH THIS)
//    ShowHidePlus(SelectedEmpID, EmpPlusGroups);
  end;

end;

procedure TWorkManagementForm.UpdatePhoneNumber1Click(Sender: TObject);
var  //qm-388 
 SelectedEmpID:integer;
  function GetNumbers(const Value: string): string;
  var
    ch: char;
    Index, Count: integer;
  begin
    SetLength(Result, Length(Value));
    Count := 0;
    for Index := 1 to length(Value) do
    begin
      ch := Value[Index];
      if (ch >= '0') and (ch <='9') then
      begin
        inc(Count);
        Result[Count] := ch;
      end;
    end;
    SetLength(Result, Count);
  end;
begin   //qm-388  sr  contact_phone
 // inherited;
 SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
  try
      PhoneVerifyDlg:= TPhoneVerifyDlg.create(nil);
      PhoneVerifyDlg.presentNumber :=  GetNumbers(EmpWorkload.FieldByName('contact_phone').AsString);
      if PhoneVerifyDlg.showmodal =mrOk then
      begin
        FSavingEmpWorkload := True;
        EditDataSet(EmpWorkload);
        EmpWorkload.FieldByName('contact_phone').AsString := PhoneVerifyDlg.newNumber;
        EmpWorkload.Post;
        FSavingEmpWorkload := False;
        DM.updatePhoneNo(SelectedEmpID, PhoneVerifyDlg.newNumber);
      end
      else
      Showmessage('Phone number not changed');
  finally
    PhoneVerifyDlg.Release;
  end;
end;

procedure TWorkManagementForm.TicketLimitTrackBarChange(Sender: TObject);
begin
  if TicketLimitTrackBar.Position > 0 then
    TicketLimitEdit.Text := 'Ticket Limit: ' + IntToStr(TicketLimitTrackBar.Position)
  else
    TicketLimitEdit.Text := 'No Ticket Limit';
end;

procedure TWorkManagementForm.cxTicketLimitEditPropertiesCloseUp(Sender: TObject);
var
  NewLimit: Integer;
begin
  if not IsEmployee(EmpWorkload) then
    Exit;

  NewLimit := TicketLimitTrackBar.Position;
  if NewLimit <> SelectedLocatorTicketLimit then begin
    DM.SetEmployeeTicketViewLimit(SelectedLocatorID, NewLimit);
    SelectedLocatorTicketLimit := NewLimit;
    FSavingEmpWorkload := True;
    try
      EditDataset(EmpWorkLoad);
      EmpWorkLoad.FieldByName('ticket_view_limit').Value := NewLimit;
      PostDataset(EmpWorkLoad);
    finally
      FSavingEmpWorkload := False;
    end;
    ShowWorkSummaryForSelectedNode;
  end;
end;

procedure TWorkManagementForm.TicketLimitEditKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := #0;
  if not TicketLimitEdit.DroppedDown then
    TicketLimitEdit.DroppedDown := True;
end;

procedure TWorkManagementForm.NoLimitLabelClick(Sender: TObject);
begin
  TicketLimitTrackBar.Position := TicketLimitTrackBar.Min;
end;

procedure TWorkManagementForm.OnSetTree(var Msg: TMessage);   //QM-11
var
  empID:integer;
  ticketID:integer;
  shortName:string;
begin
  empID:=0;
  ticketID:=0;
  shortName := '';
  empID   := Msg.LParam;
  ticketID:= Msg.WParam;
  try
    dm.qrySearchLocator.ParamByName('empID').Value:=empID;
    dm.qrySearchLocator.Open;
    shortName:= dm.qrySearchLocator.FieldByName('short_name').AsString;

    FSearchText:= shortName;
    DoEmployeeSearchInt(True,shortName, ticketID);
  finally
    dm.qrySearchLocator.Close;
  end;
end;

//procedure TWorkManagementForm.OQSize(Expanded: Boolean);
//begin
// if Expanded then begin
//   oqMemo.Properties.ScrollBars := ssVertical;
//   oqMemo.Top := 0;
//   oqMemo.Height := 37;
//   oqMemo.Refresh;
// end
// else begin
//   oqMemo.Properties.ScrollBars := ssNone;
//   oqMemo.Top := 2;
//   oqMemo.Height := 18;
//   oqMemo.Refresh;
// end;
//
//end;

procedure TWorkManagementForm.MaxLimitLabelClick(Sender: TObject);
begin
  TicketLimitTrackBar.Position := TicketLimitTrackBar.Max;
end;

procedure TWorkManagementForm.TicketLimitTrackBarKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    TicketLimitTrackBar.Position := SelectedLocatorTicketLimit;
  if Key in [VK_RETURN, VK_ESCAPE] then begin
    Key := 0;
    TicketLimitEdit.SetFocus;
  end;
end;

procedure TWorkManagementForm.AdjustTicketListColumns(NodeType: TNodeType);
var
  ShowDamages: Boolean;
  ShowWorkOrders: Boolean;
begin
  TicketFrame.AdjustTicketListColumns(NodeType); //QM-133 Eb Past Due refactor
  ShowDamages := not (NodeType in [ntTicketActivities, ntEmergency]);
  AdjustDamageListView(ShowDamages);

  ShowWorkOrders := not (NodeType in [ntTicketActivities, ntEmergency]); 
  AdjustWorkOrderListView(ShowWorkOrders);
  ExpandFrame;
end;

procedure TWorkManagementForm.GetEmpID1Click(Sender: TObject);
var
  SelectedEmpID : integer;
  shortName : string;
  xmlRawData : string;
  begXML,endXML : integer;
  fLat, fLng, fUtc : string;
  nonXML : string;
  sl : TStringList;
begin  //QMANTWO-175
  inherited;
  xmlRawData := '';
  SelectedEmpID := EmpWorkload.FieldByName('emp_id').AsInteger;
  shortName := EmpWorkload.FieldByName('short_name').AsString;
  if PopulateBreadCrumbData(SelectedEmpID, xmlRawData) then
  begin
   begXML := Ansipos('<ROOT>', xmlRawData);
   endXML := Ansipos('/></ROOT',xmlRawData);
   nonXML :=  copy(xmlRawData,(begXML+8),((endXML-(begXML+8))));
    try
      sl := TStringList.Create;
      sl.Delimiter := ' ';
      sl.StrictDelimiter := true;
      sl.DelimitedText := nonXML;
      fLat := ReplaceStr(sl.Values['lat'],#34,'') ;
      fLng := ReplaceStr(sl.Values['long'],#34,'') ;
      fUtc := ReplaceStr(sl.Values['utc'],#34,'') ;
      ShowBreadCrumb(fLat,fLng, fUtc, shortName);
    finally
      if assigned(sl) then FreeAndNil(sl);
    end;
  end;
end;

procedure TWorkManagementForm.Mapallmylocators1Click(Sender: TObject);   //QMANTWO-302
const
  TblName = 'emp_breadcrumbs';  {DBISAM table we will save last emp locations}
var
  SelectedMgrID: integer;
  XMLRawData: string;
  MgrName: string;
  PrimaryKeys: TStringList;
  NumChanges: integer;
begin
  inherited;
  XMLRawData := '';
  PrimaryKeys := TStringList.Create;
  PrimaryKeys.Add('emp_id');
  try
    SelectedMgrID := EmpWorkload.FieldByName('emp_id').AsInteger;
    MgrName := EmpWorkload.FieldByName('short_name').AsString;
    XMLRawData := DM.GetMgrBreadCrumbsFromServer(SelectedMgrID);

    NumChanges := DM.Engine.LoadDataSetFromXml(XMLRawData, TblName, PrimaryKeys, True );
    if NumChanges > 0 then
      ShowBreadCrumbsList(MgrName)
    else
      ShowMessage('No Location Information for locators under ' + MgrName);
  finally
    FreeAndNil(PrimaryKeys);
  end;

end;

function TWorkManagementForm.PopulateBreadCrumbData(empID:integer; var RawData:string):boolean;
begin   //QMANTWO-175
  result := true;
  try
    RawData := DM.GetEmpBreadCrumbFromServer(EmpID);
  except
  on E:Exception do
  begin
     showmessage(e.message);
     result := false
  end;
  end;
end;


procedure TWorkManagementForm.ShowBreadCrumb(Lat,Lng, Utc: string; shortName:string);
var
  frmStreetMap2 : TfrmStreetMap;
begin
  if (Lat<>'') and (Lng<>'') and (Lat<>'0') and (Lng<>'0') then
  begin
    frmStreetMap2 := TfrmStreetMap.Create(Application, Lat, Lng, Utc, ShortName);
    frmStreetMap2.ShowModal;
  end
  else
    showmessage('No location information for '+ shortName);
  Lat  := '';
  Lng  := '';
end;

procedure TWorkManagementForm.ShowBreadCrumbsList(MgrName:string);
var
  frmStreetMap3: TfrmStreetMap;
  MyDate : String;
begin
  MyDate := IsoDateTimeToStr(RoundSQLServerTimeMS(Now));
  frmStreetMap3 := TfrmStreetMap.Create(Application,'34.0','-84.0',MyDate, 'Manager: ' + MgrName);
//  frmStreetMap3.MapAllBreadcrumbs;   {Call this from the timer so it has time to load}
  frmStreetMap3.Multi := True;
  frmStreetMap3.ShowModal;
end;

function TWorkManagementForm.GetMaxActivityDays: Integer;
begin
  Result := 10;
end;

procedure TWorkManagementForm.AckAllActivityActionExecute(Sender: TObject);
var
  AckCount: Integer;
begin
  if SelectedLocatorID <= 0 then
    Exit;

  AckCount := AcknowledgeActivityForManager(SelectedLocatorID, SelectedLocatorName);
  if AckCount > 0 then
    MessageDlg(Format('%d ticket activity %s acknowledged.', [AckCount,
      AddSIfNot1(AckCount, 'record')]), mtInformation, [mbOk], 0);
end;

procedure TWorkManagementForm.ActivatingNow;
begin
  inherited;
  fHasPlus:= False;  //QM-585 EB Employee Plus Whitelist
  EmployeeDM.GetEmpWorkTypes;  //QM-294 EB
  RefreshScreenControls;
  if (ActivationCount < 1) and TicketFrame.Visible then begin   //Maint  QM-338 EB Grid Settings Ini
    TicketFrame.RestoreTicketGridSettings;
    TicketFrame.fgridLoads := 0;      //QM-695 EB
    TicketFrame.ForceReload := False;   //QM-695 EB
  end;
  Inc(ActivationCount);
end;

procedure TWorkManagementForm.RefreshScreenControls;
begin
  GrantUseExemptionAction.Visible := PermissionsDM.CanI(RightRestrictUsageGrantExemption);
  RevokeUseExemptionAction.Visible := GrantUseExemptionAction.Visible;
  AckAllActivityAction.Visible := PermissionsDM.CanI(RightTicketsAcknowledgeAllActivities);
  UpdatePhoneNumber1.Visible := PermissionsDM.CanI(RightUpdatePhoneNumber);  //qm-388 sr
  DamageCountColumn.Visible := CanManageDamages;
  WOCountColumn.Visible := CanManageWorkOrders;
end;

procedure TWorkManagementForm.ActivitiesTimerTimer(Sender: TObject);
begin
  if (SelectedLocatorID = MAGIC_NEW_ACTIVITY) and (Self.Visible) and GetParentForm(Self).Enabled then
    ShowWorkSummaryForSelectedNode;
end;

procedure TWorkManagementForm.LeavingNow;
begin
  inherited;
  ActivitiesTimer.Enabled := False;
  if TicketFrame.Visible then
    TicketFrame.SaveTicketGridSettings;  //QM-338 EB Grid Settings Ini
end;

procedure TWorkManagementForm.AdjustDamageListView(const ShowDamages: Boolean);
begin
  DamageFrame.Visible := ShowDamages and CanManageDamages;
  if DamageFrame.Visible then //Initialize selected item in case permission was changed back.
    DamageFrame.SelectedItemID := 0;
end;

procedure TWorkManagementForm.AdjustWorkOrderListView(const ShowWorkOrders: Boolean);
begin
  WorkOrderFrame.Visible := ShowWorkOrders and CanManageWorkOrders;
  if WorkOrderFrame.Visible then //Initialize selected item in case permission was changed back.
    WorkOrderFrame.SelectedItemID := 0;
end;

procedure TWorkManagementForm.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
  inherited;
  {EB QMANTWO-564/690 - This is to reload the Last Selection List so that we can
                        compare contents when a new row has been focused.}
  if (FCurrentExpandedFrame = TicketFrame) and
     (TicketFrame.GridState.CheckSelection) and
     (not TicketFrame.GridState.InitialDisplay) then begin
    TicketFrame.GridState.CheckSelection := False;
    TicketFrame.LoadSelList;
  end
  else if (FCurrentExpandedFrame = WorkOrderFrame) and
          (WorkOrderFrame.GridState.CheckSelection) and
          (not WorkOrderFrame.GridState.InitialDisplay) then begin
    WorkOrderFrame.GridState.CheckSelection := False;
    WorkOrderFrame.LoadSelList;
  end
  else if (FCurrentExpandedFrame = DamageFrame) and
          (DamageFrame.GridState.CheckSelection) and
          (not DamageFrame.GridState.InitialDisplay) then begin
    DamageFrame.GridState.CheckSelection := False;
    DamageFrame.LoadSelList;
  end;
end;

procedure TWorkManagementForm.btnEmerViewClick(Sender: TObject);
var
  FileName, Params, Folder: string;  //QM-11
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
  ErrorCode: integer;
  appPath:string;
begin
  inherited;
  ErrorCode:=0;
  WaitUntilTerminated:= false;
  WaitUntilIdle := false;
  RunMinimized:= false;

  appPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
  if DM.CanViewEmergencies then begin
    FileName :=appPath+'EmerViewer.exe';
    Folder := '';
    WkManFormHandle := self.Handle;
    Params := IntToStr(WkManFormHandle)+' "'+DM.UQState.DisplayName+'" '+DM.ConnectedDB;  //QM-44   SR
    ExecuteProcess(FileName, Params, Folder, WaitUntilTerminated, WaitUntilIdle, RunMinimized, ErrorCode);
//    ShellExecute(handle, 'Open', PChar(FileName), PChar(Params), nil, SW_SHOWNORMAL);
  end;
  if ErrorCode > 0 then  ShowMessage(SysErrorMessage(GetLastError));
  
end;

function TWorkManagementForm.CanManageDamages: Boolean;
begin
  Result := PermissionsDM.CanI(RightDamagesManagement);
end;

function TWorkManagementForm.CanManageWorkOrders: Boolean;
begin
  Result := PermissionsDM.CanI(RightWorkOrdersManagement);
end;

procedure TWorkManagementForm.ShowFutureTicketsCheckboxClick(Sender: TObject);
begin
  if not IsEmployee(EmpWorkload) then
    Exit;

  if ShowFutureTicketsCheckbox.Checked <> SelectedLocatorShowFutureWork then begin
    SelectedLocatorShowFutureWork := ShowFutureTicketsCheckbox.Checked;
    DM.SetEmployeeShowFutureWork(SelectedLocatorID, SelectedLocatorShowFutureWork);
    FSavingEmpWorkload := True;
    try
      EditDataset(EmpWorkLoad);
      EmpWorkLoad.FieldByName('show_future_tickets').AsBoolean := SelectedLocatorShowFutureWork;
      PostDataset(EmpWorkLoad);
    finally
      FSavingEmpWorkload := False;
    end;
  end;
end;


procedure TWorkManagementForm.FilterEmpsByActiveStatus;
var
  EmpFilter: string;
  ID: string;
begin
  if WantActiveEmps and (not WantInactiveEmps) then
    {Hide inactive employees, but show managers }
    EmpFilter := '(active=1) or (is_manager=1)'
  else if (not WantActiveEmps) and WantInactiveEmps then
    {Show inactive employees, but show managers and non-employee nodes, too.}
    EmpFilter := '(active=0) or (is_manager=1) or (type_id<0)'
  else
    {Show everyone}
    EmpFilter := '';
  if EmpFilter <> EmpWorkload.Filter then begin
    ID := EmpWorkload.FieldByName('emp_tree_id').Value;
    EmpWorkload.Filter := EmpFilter;
    EmpWorkload.Filtered := NotEmpty(EmpWorkload.Filter);
    EmpWorkload.Locate('emp_tree_id', ID, []);
  end;
  EmpWorkload.Refresh;
end;

procedure TWorkManagementForm.PopulateNodeFromServer(EmpID: Integer);
var
  LimitationStr: string;
  VBMgrID: integer;
begin
  EmpTree.BeginUpdate;
  try
    if EmpID > 0 then begin
    {Permissions: Virtual Bucket, Today, Open & Past Due (stacked to avoid old client compatibility issues}
      If PermissionsDM.CanI(RightTicketsVirtualBucket) then begin     //QM-486 Virtual Bucket EB
        LimitationStr := PermissionsDM.GetLimitationDef(RightTicketsVirtualBucket, '0');
        try
          VBMgrID := StrToInt(LimitationStr);
          if (VBMgrID = 0) or (not EmployeeDM.EmpIsManager(VBMgrID)) then
              VBMgrID := DM.EmpID;
        except
          VBMgrID := DM.EmpID;
        end;
        DM.CallingServiceName('MultiOpenTotals6V');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6V(DM.Engine.SecurityInfo, IntToStr(EmpID), VBMgrID ));
        EmpTree.DataController.DataSet.Refresh;
      end else
	 {Permissions: Today, Open & Past Due  - note this is primarily to avoid old client compatibility issues}
      if PermissionsDM.CanI(RightTicketsTodayOpenPastDueTree) then begin  //QM-318 Today (and Past Due) EB
        DM.CallingServiceName('MultiOpenTotals6TD');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6TD(DM.Engine.SecurityInfo, IntToStr(EmpID)));
        EmpTree.DataController.DataSet.Refresh;
      end else
      {Permissions: Open & Past Due Tickets}
      if PermissionsDM.CanI(RightTicketsOpenTree) then begin //QM-428 Open Tickets EB
        DM.CallingServiceName('MultiOpenTotals6OP');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6OP(DM.Engine.SecurityInfo, IntToStr(EmpID)));
        EmpTree.DataController.DataSet.Refresh;
      end else
    {Permissions: Past Due}
      if PermissionsDM.CanI(RightTicketsPastDueTree) then begin    //QM-248 & QM-133 Past Due EB
        DM.CallingServiceName('MultiOpenTotals6PD');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6PD(DM.Engine.SecurityInfo, IntToStr(EmpID)));
        EmpTree.DataController.DataSet.Refresh;
      end
      else begin
        DM.CallingServiceName('MultiOpenTotals6');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6(DM.Engine.SecurityInfo, IntToStr(EmpID)));
        EmpTree.DataController.DataSet.Refresh;
      end;
    end;
    FilterEmpsByActiveStatus;
  finally
    EmpTree.EndUpdate;
  end;
end;

procedure TWorkManagementForm.RefreshFrameData;
begin
  WorkOrderFrame.RefreshData;
  TicketFrame.RefreshData;
  DamageFrame.RefreshData;
end;

end.
