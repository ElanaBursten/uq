inherited WorkOrderDetailForm: TWorkOrderDetailForm
  Left = 437
  Top = 216
  Caption = 'Work Order Detail'
  ClientHeight = 657
  ClientWidth = 793
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 793
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object TicketNum: TLabel
      Left = 7
      Top = 8
      Width = 71
      Height = 13
      Caption = 'Work Order #:'
      Transparent = True
    end
    object Kind: TDBText
      Left = 289
      Top = 8
      Width = 81
      Height = 13
      DataField = 'kind'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object WorkOrderNumber: TDBText
      Left = 94
      Top = 7
      Width = 113
      Height = 14
      AutoSize = True
      BiDiMode = bdLeftToRight
      DataField = 'wo_number'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object DoneButton: TButton
      Left = 547
      Top = 5
      Width = 85
      Height = 25
      Action = DoneAction
      TabOrder = 0
    end
    object PrintButton: TButton
      Left = 455
      Top = 5
      Width = 85
      Height = 25
      Action = PrintAction
      TabOrder = 1
    end
  end
  object WorkOrderPageControl: TPageControl
    Left = 0
    Top = 34
    Width = 793
    Height = 623
    ActivePage = DetailsTab
    Align = alClient
    Images = SharedImagesDM.Images
    TabOrder = 1
    OnChange = WorkOrderPageControlChange
    object ImageTab: TTabSheet
      Caption = 'Image'
      ImageIndex = -1
      object WorkOrderMemo: TMemo
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
    object DetailsTab: TTabSheet
      Caption = 'Details'
      ImageIndex = -1
      object FunctionKeyPanel: TPanel
        Left = 0
        Top = 0
        Width = 0
        Height = 594
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object F5Label: TLabel
          Left = 8
          Top = 21
          Width = 12
          Height = 13
          Caption = 'F5'
          Transparent = True
        end
        object F6Label: TLabel
          Left = 8
          Top = 38
          Width = 12
          Height = 13
          Caption = 'F6'
          Transparent = True
        end
        object F7Label: TLabel
          Left = 8
          Top = 55
          Width = 12
          Height = 13
          Caption = 'F7'
          Transparent = True
        end
        object F8Label: TLabel
          Left = 8
          Top = 72
          Width = 12
          Height = 13
          Caption = 'F8'
          Transparent = True
        end
        object F9Label: TLabel
          Left = 8
          Top = 89
          Width = 12
          Height = 13
          Caption = 'F9'
          Transparent = True
        end
      end
      inline WODetailsWGL: TWOWGLFrame
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        HorzScrollBar.Range = 700
        HorzScrollBar.Smooth = True
        HorzScrollBar.Tracking = True
        VertScrollBar.Range = 550
        VertScrollBar.Smooth = True
        VertScrollBar.Tracking = True
        Align = alClient
        TabOrder = 2
        Visible = False
        inherited DetailsPanel: TPanel
          Width = 785
          inherited TransmitDate: TDBText
            DataSource = WorkOrderDS
          end
          inherited CustomerPanel: TPanel
            Width = 785
            inherited CallerPhoneLabel: TLabel
              Left = 464
            end
            inherited AtlPhoneLabel: TLabel
              Left = 464
            end
            inherited Phone: TDBText
              Left = 527
            end
            inherited AltPhone: TDBText
              Left = 527
            end
            inherited WorkAddressStreet: TDBText
              Width = 234
            end
          end
        end
        inherited FeedDetailsPanel: TPanel
          Width = 785
          inherited MapPageLabel: TLabel
            Left = 416
            Top = 1
          end
          inherited MapPage: TDBText
            Left = 455
            Top = 1
          end
          inherited MapRef: TDBText
            Left = 634
            Top = 1
          end
          inherited MapPageGrid: TLabel
            Left = 575
            Top = 1
          end
        end
        inherited EditingPanel: TPanel
          Width = 785
          Height = 399
          inherited InitialInspectionPanel: TPanel
            Width = 785
            inherited CGACount: TDBText
              Width = 94
            end
          end
          inherited InadequateVentClearance: TcxCheckBox
          end
          inherited InadequateVentClearanceIg: TcxCheckBox
          end
        end
        inherited ButtonPanel: TPanel
          Top = 562
          Width = 785
        end
        inherited StatusList: TDBISAMQuery
          Top = 16
        end
        inherited StatusSource: TDataSource
          Left = 292
          Top = 16
        end
      end
      inline WODetailsOHM: TWOOHMFrame
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        HorzScrollBar.Range = 700
        HorzScrollBar.Smooth = True
        HorzScrollBar.Tracking = True
        VertScrollBar.Range = 550
        VertScrollBar.Smooth = True
        VertScrollBar.Tracking = True
        Align = alClient
        TabOrder = 3
        Visible = False
        inherited DetailsPanel: TPanel
          Width = 785
          inherited CustomerPanel: TPanel
            Width = 785
          end
        end
        inherited FeedDetailsPanel: TPanel
          Width = 785
          inherited pcOHM: TPageControl
            Width = 785
            inherited tsGasMain: TTabSheet
              inherited pnlGasMain: TPanel
                Ctl3D = False
                ParentCtl3D = False
              end
            end
            inherited tsPremise: TTabSheet
              inherited pnlPremise: TPanel
                Width = 777
                Ctl3D = False
                ParentCtl3D = False
                inherited dbCkBox_has_c: TcxDBCheckBox
                end
                inherited cmbxCurbValueFound: TcxComboBox
                  Properties.OnChange = WODetailsOHMcmbxCurbValueFoundPropertiesChange
                end
              end
            end
          end
        end
        inherited EditingPanel: TPanel
          Width = 785
          Height = 259
          inherited InitialInspectionPanel: TPanel
            Width = 785
            inherited InitialInspectionLabel: TLabel
              Top = -1
            end
            inherited DateOfVisitLabel: TLabel
              Top = 14
            end
            inherited CGACountLabel: TLabel
              Left = 335
              Top = 14
            end
            inherited CompletionCodeLabel: TLabel
              Left = 2
              Top = 34
            end
            inherited DateOfVisit: TDBText
              Top = 14
            end
            inherited CGACount: TDBText
              Left = 407
              Top = 14
            end
            inherited CGAReasonLabel: TLabel
              Left = 335
              Top = 34
            end
            inherited CompletionCodeCombobox: TDBLookupComboBox
              Left = 93
              Top = 31
            end
            inherited CGAReasonCombo: TcxDBExtLookupComboBox
              Left = 410
              Top = 31
            end
          end
          inherited InadequateVentClearance: TcxCheckBox
          end
          inherited InadequateVentClearanceIg: TcxCheckBox
          end
        end
        inherited ButtonPanel: TPanel
          Top = 564
          Width = 947
          Align = alNone
          inherited PlatAddinButton: TButton
            Left = 504
          end
        end
      end
      inline WODetailsINR: TWOINRFrame
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        HorzScrollBar.Range = 700
        HorzScrollBar.Smooth = True
        HorzScrollBar.Tracking = True
        VertScrollBar.Range = 550
        VertScrollBar.Smooth = True
        VertScrollBar.Tracking = True
        Align = alClient
        TabOrder = 4
        inherited DetailsPanel: TPanel
          Width = 785
          inherited CustomerPanel: TPanel
            Width = 785
          end
        end
        inherited FeedDetailsPanel: TPanel
          Width = 785
          inherited pcDetails: TPageControl
            Width = 785
            inherited tsMeter: TTabSheet
              inherited pnlGasMeter: TPanel
                Width = 777
              end
            end
          end
        end
        inherited EditingPanel: TPanel
          Width = 785
          inherited InitialInspectionPanel: TPanel
            Width = 785
            inherited DateOfVisitLabel: TLabel
              Top = 16
            end
            inherited CGACountLabel: TLabel
              Top = 31
            end
            inherited CompletionCodeLabel: TLabel
              Left = 233
              Top = 31
            end
            inherited DateOfVisit: TDBText
              Top = 16
            end
            inherited CGACount: TDBText
              Top = 31
            end
            inherited CGAReasonLabel: TLabel
              Top = 31
            end
            inherited StatusField: TDBText
              Left = 329
              Top = 31
            end
            inherited CompletionCodeCombobox1: TDBLookupComboBox
              Left = 544
              Top = 25
            end
            inherited CGAReasonCombo: TcxDBExtLookupComboBox
              Left = 487
              Top = 28
              Width = 119
            end
          end
          inherited PotentialHazardCheckList: TcxCheckListBox
            Height = 97
          end
          inherited AOCInspectionCheckList: TcxCheckListBox
            Height = 166
            Visible = True
          end
          inherited InadequateVentClearance: TcxCheckBox
          end
          inherited InadequateVentClearanceIg: TcxCheckBox
          end
        end
        inherited ButtonPanel: TPanel
          Width = 785
          Height = 54
          inherited ClosedDateLabel: TLabel
            Left = 6
            Top = 7
          end
          inherited ClosedDate: TDBText
            Left = 77
            Top = 7
          end
          inherited PlatAddinButton: TButton
            Top = 2
          end
        end
        inherited WorkTypes: TDBISAMQuery
          Left = 670
          Top = 320
        end
        inherited WorkOrderOHMdetails: TDBISAMTable
          Left = 668
          Top = 356
        end
      end
      inline WODetailsLAM01: TWOLAM01Frame
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        HorzScrollBar.Range = 700
        HorzScrollBar.Smooth = True
        HorzScrollBar.Tracking = True
        VertScrollBar.Range = 550
        VertScrollBar.Smooth = True
        VertScrollBar.Tracking = True
        Align = alClient
        TabOrder = 1
        inherited DetailsPanel: TPanel
          Width = 785
          inherited WorkLong: TDBText
            Top = 55
          end
          inherited TerminalGPSLabel: TLabel
            Top = 55
          end
        end
        inherited FeedDetailsPanel: TPanel
          Width = 785
        end
        inherited EditingPanel: TPanel
          Width = 785
          Height = 345
          inherited RelatedTicketsLabel: TLabel
            Left = 461
          end
          inherited SourceSentAttachment: TLabel
            Left = 91
          end
          inherited WorkDescription: TDBMemo
            Left = 91
            Width = 367
          end
          inherited WorkCross: TDBEdit
            Left = 91
          end
          inherited DrivewayBores: TcxDBSpinEdit
            Left = 91
          end
          inherited RoadwayBores: TcxDBSpinEdit
            Properties.OnChange = nil
            Properties.OnValidate = nil
          end
          inherited RelatedTickets: TDBGrid
            Left = 507
            TitleFont.Charset = ANSI_CHARSET
          end
          inherited StatusCombo: TDBLookupComboBox
            Left = 91
            Top = 123
          end
          inherited DBGrid1: TDBGrid
            TitleFont.Charset = ANSI_CHARSET
          end
        end
        inherited ButtonPanel: TPanel
          Top = 554
          Width = 785
        end
      end
    end
    object NotesTab: TTabSheet
      Caption = 'Notes'
      ImageIndex = -1
      inline WorkOrderNotesFrame: TBaseNotesFrame
        Left = 0
        Top = 39
        Width = 785
        Height = 517
        Align = alTop
        TabOrder = 0
        inherited NotesGrid: TcxGrid
          Width = 785
          Height = 370
          inherited NotesGridView: TcxGridDBTableView
            inherited ColNotesNote: TcxGridDBColumn
              Options.Editing = False
            end
          end
        end
        inherited HeaderPanel: TPanel
          Width = 785
          inherited NoteLabel: TLabel
            Top = 4
          end
          inherited NoteText: TMemo
            Top = 4
          end
          inherited AddNoteButton: TButton
            Top = 6
            OnClick = WorkOrderNotesFrameAddNoteButtonClick
          end
        end
        inherited FooterPanel: TPanel
          Top = 507
          Width = 785
        end
      end
      object RestrictionPanel: TPanel
        Left = 0
        Top = 0
        Width = 785
        Height = 39
        Align = alTop
        BevelEdges = []
        BevelOuter = bvNone
        TabOrder = 1
        object Label17: TLabel
          Left = 38
          Top = 5
          Width = 111
          Height = 13
          Caption = 'Restriction (Sub Type):'
          WordWrap = True
        end
        object NoteSubTypeCombo: TComboBox
          Left = 37
          Top = 18
          Width = 243
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = NoteSubTypeComboChange
          Items.Strings = (
            'Ticket'
            'Locate 1'
            'Locate 2'
            'Locate 3')
        end
      end
    end
    object HistoryTab: TTabSheet
      Caption = 'History'
      ImageIndex = -1
      object HistPanel: TPanel
        Left = 0
        Top = 0
        Width = 785
        Height = 595
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 5
        TabOrder = 0
        object HistoryPageControl: TPageControl
          Left = 5
          Top = 5
          Width = 775
          Height = 585
          ActivePage = WorkOrderStatusTab
          Align = alClient
          TabOrder = 0
          object WorkOrderStatusTab: TTabSheet
            Caption = 'Work Order Status'
            object WorkOrderStatusGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 767
              Height = 556
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object WorkOrderStatusGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                DataController.DataSource = WorkOrderStatusHistoryDS
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object WOStatusColWOStatusID: TcxGridDBColumn
                  Caption = 'WO Status ID'
                  DataBinding.FieldName = 'wo_status_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 84
                end
                object WOStatusColWOID: TcxGridDBColumn
                  Caption = 'Work Order ID'
                  DataBinding.FieldName = 'wo_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 92
                end
                object WOStatusColStatusDate: TcxGridDBColumn
                  Caption = 'Statused'
                  DataBinding.FieldName = 'status_date'
                  Options.Editing = False
                  Width = 111
                end
                object WOStatusColStatus: TcxGridDBColumn
                  Caption = 'Status'
                  DataBinding.FieldName = 'status'
                  Options.Editing = False
                  Width = 155
                end
                object WOStatusCGAReasonDescription: TcxGridDBColumn
                  Caption = 'CGA Reason'
                  DataBinding.FieldName = 'cga_reason_desc'
                  Options.Editing = False
                  Width = 175
                end
                object WOStatusColStatusedByID: TcxGridDBColumn
                  DataBinding.FieldName = 'statused_by_id'
                  Visible = False
                  Options.Editing = False
                end
                object WOStatusColShortName: TcxGridDBColumn
                  Caption = 'Statused By'
                  DataBinding.FieldName = 'short_name'
                  Options.Editing = False
                  Width = 102
                end
                object WOColStatusStatusedHow: TcxGridDBColumn
                  Caption = 'Statused How'
                  DataBinding.FieldName = 'statused_how'
                  Options.Editing = False
                  Width = 108
                end
                object WOStatusColInsertDate: TcxGridDBColumn
                  Caption = 'Inserted'
                  DataBinding.FieldName = 'insert_date'
                  Options.Editing = False
                end
              end
              object WorkOrderStatusGridLevel: TcxGridLevel
                GridView = WorkOrderStatusGridView
              end
            end
          end
          object AssignmentsTab: TTabSheet
            Caption = 'Assignments'
            ImageIndex = 1
            object WOAssignmentsGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 767
              Height = 556
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object WOAssignmentsGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                DataController.DataSource = WOAssignmentDS
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object WOAColAssignmentID: TcxGridDBColumn
                  Caption = 'Assignment ID'
                  DataBinding.FieldName = 'assignment_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 76
                end
                object WOAColWOID: TcxGridDBColumn
                  Caption = 'Work Order ID'
                  DataBinding.FieldName = 'wo_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 78
                end
                object WOAColAssignedToID: TcxGridDBColumn
                  Caption = 'Assigned To'
                  DataBinding.FieldName = 'assigned_to_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  Visible = False
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                end
                object WOAColAssignedToShortName: TcxGridDBColumn
                  Caption = 'Assigned To'
                  DataBinding.FieldName = 'assigned_to_short_name'
                  Options.Editing = False
                  Width = 133
                end
                object WOAColCallCenter: TcxGridDBColumn
                  Caption = 'Call Center'
                  DataBinding.FieldName = 'wo_call_center'
                  Width = 96
                end
                object WOAColInsertDate: TcxGridDBColumn
                  Caption = 'Insert Date'
                  DataBinding.FieldName = 'insert_date'
                  Options.Editing = False
                  Width = 92
                end
                object WOAColAddedBy: TcxGridDBColumn
                  Caption = 'Added By'
                  DataBinding.FieldName = 'added_by'
                  OnGetDisplayText = WOAColAddedByGetDisplayText
                  Options.Editing = False
                  Width = 110
                end
                object WOAColModifiedDate: TcxGridDBColumn
                  Caption = 'Modified Date'
                  DataBinding.FieldName = 'modified_date'
                  Options.Editing = False
                  Width = 99
                end
                object WOAColActive: TcxGridDBColumn
                  Caption = 'Active'
                  DataBinding.FieldName = 'active'
                  Options.Editing = False
                  Width = 86
                end
              end
              object WOAssignmentsGridLevel: TcxGridLevel
                GridView = WOAssignmentsGridView
              end
            end
          end
          object ResponsesTab: TTabSheet
            Caption = 'Responses'
            ImageIndex = 2
            object ResponsesGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 767
              Height = 556
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object ResponsesGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                DataController.DataSource = WOResponseLogDS
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object WORespColWOResponseID: TcxGridDBColumn
                  Caption = 'Response ID'
                  DataBinding.FieldName = 'wo_response_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 82
                end
                object WORespColWOID: TcxGridDBColumn
                  Caption = 'Work Order ID'
                  DataBinding.FieldName = 'wo_id'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 82
                end
                object WORespColResponseDate: TcxGridDBColumn
                  Caption = 'Response Date'
                  DataBinding.FieldName = 'response_date'
                  Visible = False
                  Options.Editing = False
                end
                object WORespColWOSource: TcxGridDBColumn
                  Caption = 'Call Center'
                  DataBinding.FieldName = 'wo_source'
                  Options.Editing = False
                  Width = 167
                end
                object WORespColStatus: TcxGridDBColumn
                  Caption = 'Status'
                  DataBinding.FieldName = 'status'
                  Options.Editing = False
                  Width = 109
                end
                object WORespColResponseSent: TcxGridDBColumn
                  Caption = 'Response Sent'
                  DataBinding.FieldName = 'response_sent'
                  Visible = False
                  Options.Editing = False
                end
                object WORespColSuccess: TcxGridDBColumn
                  Caption = 'Success'
                  DataBinding.FieldName = 'success'
                  Options.Editing = False
                  Width = 102
                end
                object WORespColReply: TcxGridDBColumn
                  Caption = 'Reply'
                  DataBinding.FieldName = 'reply'
                  Options.Editing = False
                  Width = 65
                end
              end
              object ResponsesGridLevel: TcxGridLevel
                GridView = ResponsesGridView
              end
            end
          end
          object WOVersionsTab: TTabSheet
            Caption = 'Work Order Versions'
            ImageIndex = 3
            object VersionsPanel: TPanel
              Left = 0
              Top = 0
              Width = 767
              Height = 556
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object VersionSpacerPanel: TPanel
                Left = 4
                Top = 127
                Width = 759
                Height = 8
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
              end
              object WOVersionImageMemo: TDBMemo
                Left = 4
                Top = 135
                Width = 759
                Height = 417
                Align = alClient
                DataField = 'image_display'
                DataSource = WOVersionsDS
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 1
              end
              object WOVersionsGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 759
                Height = 123
                Align = alTop
                TabOrder = 2
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object WOVersionsGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  DataController.DataSource = WOVersionsDS
                  DataController.KeyFieldNames = 'wo_version_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColWOVerCallCenter: TcxGridDBColumn
                    Caption = 'Call Center'
                    DataBinding.FieldName = 'call_center'
                    Options.Editing = False
                    Width = 156
                  end
                  object ColWOVerActive: TcxGridDBColumn
                    Caption = 'Active'
                    DataBinding.FieldName = 'active'
                    Options.Editing = False
                    Width = 37
                  end
                  object ColWOVerVersionID: TcxGridDBColumn
                    Caption = 'Version ID'
                    DataBinding.FieldName = 'wo_version_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 64
                  end
                  object ColWOVerTicketRevision: TcxGridDBColumn
                    Caption = 'Revision'
                    DataBinding.FieldName = 'wo_revision'
                    Options.Editing = False
                    Width = 65
                  end
                  object ColWOVerTicketType: TcxGridDBColumn
                    Caption = 'Type'
                    DataBinding.FieldName = 'work_type'
                    Width = 78
                  end
                  object ColWOVerTransmitDate: TcxGridDBColumn
                    Caption = 'Transmit Date'
                    DataBinding.FieldName = 'transmit_date'
                    Width = 111
                  end
                  object ColWOVerArrivalDate: TcxGridDBColumn
                    Caption = 'Arrival Date'
                    DataBinding.FieldName = 'arrival_date'
                    Options.Editing = False
                    Width = 129
                  end
                  object ColWOVerProcessedDate: TcxGridDBColumn
                    Caption = 'Processed Date'
                    DataBinding.FieldName = 'processed_date'
                    Options.Editing = False
                    Width = 156
                  end
                end
                object WOVersionsGridLevel: TcxGridLevel
                  GridView = WOVersionsGridView
                end
              end
            end
          end
        end
      end
    end
    object AttachmentsTab: TTabSheet
      Caption = 'Attachments'
      ImageIndex = -1
      inline AttachmentsFrame: TAttachmentsFrame
        Left = 0
        Top = 0
        Width = 785
        Height = 594
        Align = alClient
        TabOrder = 0
        inherited AttachmentsPanel: TPanel
          Width = 785
          Height = 563
          inherited AttachmentList: TcxGrid
            Width = 775
            Height = 553
            inherited AttachmentListDBTableView: TcxGridDBTableView
              inherited ColAttachmentID: TcxGridDBColumn
                Properties.Alignment.Horz = taRightJustify
                HeaderAlignmentHorz = taRightJustify
              end
              inherited ColForeignID: TcxGridDBColumn
                Properties.Alignment.Horz = taRightJustify
                HeaderAlignmentHorz = taRightJustify
              end
              inherited ColAttachedBy: TcxGridDBColumn
                Properties.Alignment.Horz = taRightJustify
                HeaderAlignmentHorz = taRightJustify
              end
            end
          end
        end
        inherited FooterPanel: TPanel
          Top = 563
          Width = 785
        end
      end
    end
  end
  object WorkOrderDS: TDataSource
    DataSet = DM.WorkOrder
    OnDataChange = WorkOrderDSDataChange
    Left = 366
    Top = 8
  end
  object StatusSource: TDataSource
    AutoEdit = False
    Left = 333
    Top = 8
  end
  object WorkOrderStatusHistoryDS: TDataSource
    DataSet = WorkOrderStatusHistory
    Left = 688
    Top = 8
  end
  object WorkOrderStatusHistory: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'wo_status_history'
    Left = 653
    Top = 8
  end
  object WOAssignmentDS: TDataSource
    DataSet = WorkOrderAssignmentHistory
    Left = 689
    Top = 38
  end
  object WorkOrderAssignmentHistory: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'wo_assignment'
    Left = 653
    Top = 38
  end
  object WorkOrderResponseLog: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'wo_response_log'
    Left = 653
    Top = 70
  end
  object WOResponseLogDS: TDataSource
    DataSet = WorkOrderResponseLog
    Left = 688
    Top = 72
  end
  object TicketsDS: TDataSource
    DataSet = Tickets
    Left = 301
    Top = 40
  end
  object Tickets: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select ticket_number'
      'from ticket t'
      'join work_order_ticket w on w.ticket_id = t. ticket_id'
      'where'
      '  w.wo_id = :wo_id and'
      '  w.active')
    Params = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end>
    Left = 342
    Top = 40
    ParamData = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end>
  end
  object WOVersionsDS: TDataSource
    AutoEdit = False
    DataSet = WorkOrderVersions
    Left = 588
    Top = 40
  end
  object WorkOrderVersions: TDBISAMQuery
    OnCalcFields = WorkOrderVersionsCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    Params = <>
    Left = 620
    Top = 40
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 400
    Top = 8
    object DoneAction: TAction
      Caption = 'Done (F12)'
      ShortCut = 123
      OnExecute = DoneActionExecute
    end
    object PrintAction: TAction
      Caption = 'Print Preview'
      OnExecute = PrintActionExecute
    end
  end
end
