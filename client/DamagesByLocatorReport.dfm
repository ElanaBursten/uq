inherited DamagesByLocatorReportForm: TDamagesByLocatorReportForm
  Left = 518
  Top = 347
  Caption = 'Damages By Locator Report'
  ClientHeight = 258
  ClientWidth = 387
  PixelsPerInch = 96
  TextHeight = 13
  object ProfitCenterLabel: TLabel [0]
    Left = 0
    Top = 88
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object LiabilityLabel: TLabel [1]
    Left = 0
    Top = 220
    Width = 39
    Height = 13
    Caption = 'Liability:'
  end
  object Label1: TLabel [2]
    Left = 0
    Top = 8
    Width = 103
    Height = 13
    Caption = 'Damage Date Range:'
  end
  object ManagerLabel: TLabel [3]
    Left = 0
    Top = 117
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object EmployeeStatusLabel: TLabel [4]
    Left = 0
    Top = 146
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object EmployeeLabel: TLabel [5]
    Left = 0
    Top = 193
    Width = 40
    Height = 13
    Caption = 'Locator:'
  end
  inline RangeSelect: TOdRangeSelectFrame [6]
    Left = 54
    Top = 24
    Width = 231
    Height = 58
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 5
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Left = 125
    end
    inherited DatesLabel: TLabel
      Left = 5
      Top = 35
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 41
      Top = 32
      Width = 185
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 41
      Top = 7
      Width = 79
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 145
      Top = 7
      Width = 80
    end
  end
  object ProfitCenter: TComboBox
    Left = 96
    Top = 85
    Width = 112
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object Liability: TComboBox
    Left = 96
    Top = 217
    Width = 200
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 6
  end
  object ManagersComboBox: TComboBox
    Left = 96
    Top = 114
    Width = 164
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 2
    OnChange = ManagersComboBoxChange
  end
  object EmployeeStatusCombo: TComboBox
    Left = 96
    Top = 143
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  object LocatorComboBox: TComboBox
    Left = 96
    Top = 190
    Width = 200
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 5
  end
  object AllEmployeesCheckBox: TCheckBox
    Left = 96
    Top = 169
    Width = 282
    Height = 17
    Caption = 'Activity for all of this manager'#39's employees'
    TabOrder = 4
    OnClick = AllEmployeesCheckBoxClick
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
  end
end
