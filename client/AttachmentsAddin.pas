unit AttachmentsAddin;

interface

uses Classes, StdCtrls, Windows, SysUtils, DBISAMTb, DB, MSXML2_TLB, DMu, BaseAddinManager;

type
  TAttachmentsAddinManager = class(TBaseAddinManager)
  private
    AddinInfo: TDBISAMTable;
    procedure CreateAddinInfoTable;
    procedure ApplyTicketUpdates(const Address1, Address2, Street, City,
      State, Latitude, Longitude: string);
    procedure ProcessTicketOutputFile(const FileName: string);
    procedure ProcessDamageOutputFile(const FileName: string);
    procedure ProcessWorkOrderOutputFile(const FileName: string);
    procedure ApplyDamageUpdates(const Street, County, City, State, Latitude, Longitude: string);
    procedure SaveAddinInfo(const ForeignType, ForeignID: Integer; const Latitude, Longitude: string);
    procedure SaveNewAttachments(Attachments: IXMLDOMNodeList; ForeignType, ForeignID: Integer);
    procedure SaveNewActivities(Activities: IXMLDOMNodeList);
    procedure ApplyWorkOrderUpdates(const Street, County, City, State, Latitude, Longitude: string);
  protected
    function BuildAddinCommandLine(const InputFile, OutputFile: string): string; override;
    function ChromePreferencesFilename: string;
    function ChromeDownloadDir: string;
    procedure WaitForAttachmentsInDownloadDir(const AttachmentFileMask: string);
  public
    constructor Create(DM: TDM; AddinButton: TButton; ClickHandler: TNotifyEvent); override;
    destructor Destroy; override;
    procedure PrepareTicketAttachmentsAddin(const HaveTicket: Boolean);
    procedure PrepareDamageAttachmentsAddin(const HaveDamage: Boolean);
    procedure PrepareWorkOrderAttachmentsAddin(const HaveWorkOrder: Boolean);
    function ExecuteForTicket(Ticket, Locates: TDataSet; const SelectedAttachmentID: Integer=0; const SelectedForeignType: Integer=0; const SelectedAttachmentFilename: string=''): Boolean;
    function ExecuteForDamage(DamageID: Integer; const SelectedAttachmentID: Integer=0; const SelectedForeignType: Integer=0; const SelectedAttachmentFilename: string=''): Boolean;
    function ExecuteForWorkOrder(WorkOrder: TDataSet; const SelectedAttachmentID: Integer=0; const SelectedForeignType: Integer=0; const SelectedAttachmentFilename: string=''): Boolean;
  end;

implementation

uses
  OdMSXMLUtils, OdISODates, OdDbUtils, OdMiscUtils, QMConst, Dialogs, Controls,
  JCLFileUtils, Forms, SuperObject, SuperObjectUtils, LocalEmployeeDMu;

{ TAttachmentsAddinManager }
const
  SelectDamage = 'select * from damage where damage_id = %d';
  // Relative to the chrome user data dir
  RelativeChromePreferencesFilename = 'default\preferences';
  // Relative to the user profile dir
  RelativeDownloadsDir = 'downloads';

procedure TAttachmentsAddinManager.PrepareTicketAttachmentsAddin(const HaveTicket: Boolean);
const
  SectionID  = 'TicketAttachments';
begin
  AddinButton.Visible := False and HaveTicket;
  if HaveTicket then
    PrepareAddin(SectionID);
end;

procedure TAttachmentsAddinManager.PrepareDamageAttachmentsAddin(const HaveDamage: Boolean);
const
  SectionID  = 'DamageAttachments';
begin
  AddinButton.Visible := False and HaveDamage;
  if HaveDamage then
    PrepareAddin(SectionID);
end;

procedure TAttachmentsAddinManager.PrepareWorkOrderAttachmentsAddin(const HaveWorkOrder: Boolean);
const
  SectionID  = 'WorkOrderAttachments';
begin
  AddinButton.Visible := False and HaveWorkOrder;
  if HaveWorkOrder then
    PrepareAddin(SectionID);
end;

constructor TAttachmentsAddinManager.Create(DM: TDM; AddinButton: TButton; ClickHandler: TNotifyEvent);
begin
  inherited Create(DM, AddinButton, ClickHandler);
  CreateAddinInfoTable;
end;

destructor TAttachmentsAddinManager.Destroy;
begin
  FreeAndNil(AddinInfo);
  inherited;
end;

procedure TAttachmentsAddinManager.CreateAddinInfoTable;
begin
  AddinInfo := TDBISAMTable.Create(nil);
  AddinInfo.DatabaseName := DM.Database1.DatabaseName;
  AddinInfo.SessionName := DM.Database1.SessionName;
  AddinInfo.TableName := 'addin_info';
  DM.Engine.LinkEventsAutoInc(AddinInfo);
end;

function TAttachmentsAddinManager.ChromePreferencesFilename: string;
begin
  Result := BuildFileName(BrowserUserDataDir, RelativeChromePreferencesFilename);
end;

function TAttachmentsAddinManager.ChromeDownloadDir: string;
var
  DefaultDirectory: ISuperObject;
begin
  DefaultDirectory := SuperObjectForKey(SuperObjectForKey(
    TSuperObject.ParseFile(ChromePreferencesFilename, False), 'download'),
    'default_directory');
  if DefaultDirectory = nil then
    { As of October 2013, Chrome doesn't set the download directory in the
      preferences file, until the user tries changing it. When it's not in the
      preferences file, Chrome uses the standard Windows download directory
      (e.g. C:\Users\dan\Downloads), according to my testing in Windows 7. This
      could change. }
    Result := MakePath(GetUserProfileFolder, RelativeDownloadsDir)
  else
    Result := DefaultDirectory.AsString;
end;

procedure TAttachmentsAddinManager.WaitForAttachmentsInDownloadDir(
  const AttachmentFileMask: string);
var
  FileList: TStringList;

  function WarningMessage: string;
  var
    I: Integer;
  begin
     Result := 'Waiting for the following attachments to be moved from the ' +
       'browser''s download folder:' + CRLF;

     for I := 0 to FileList.Count-1 do
       Result := Result + FileList[I] + CRLF;

     Result := Result + CRLF + 'Click ''Retry'' to recheck if the ' +
       'attachments have been moved. Click ''Abort'' to stop waiting, and ' +
       'resume Q Manager.';
  end;
begin
  FileList := TStringList.Create;
  try
    while True do begin
      FileList.Clear;
      AdvBuildFileList(BuildFileName(ChromeDownloadDir, AttachmentFileMask +
        '.zip'), faArchive, FileList, amSubSetOf);
      if (FileList.Count < 1) or (MessageDlg(WarningMessage, mtWarning,
        [mbRetry, mbAbort], 0) = mrAbort) then
        Break;
      Application.MainForm.Update;
      Sleep(1000);
    end;
  finally
    FreeAndNil(FileList);
  end;
end;

function TAttachmentsAddinManager.ExecuteForTicket(Ticket, Locates: TDataSet;
  const SelectedAttachmentID, SelectedForeignType: Integer;
  const SelectedAttachmentFilename: string): Boolean;
var
  InputFile: string;
  OutputFile: string;
  AttachmentFileMask: string;
begin
  Assert(Assigned(Locates));
  LocateData := Locates; // This has the current (even unsaved) changes in it
  TicketData := Ticket;
  try
    Assert(not TicketData.IsEmpty, 'Ticket not present');

    ActionAttachmentID := SelectedAttachmentID;
    ActionForeignType := SelectedForeignType;
    ActionAttachmentFilename := SelectedAttachmentFilename;

    if AddinType = atDefault then
      InputFile := GenerateTicketInput;

    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketAttachAddinStart, TicketData.FieldByName('ticket_id').AsString);
    OutputFile := RunAddin(InputFile);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketAttachAddinEnd, TicketData.FieldByName('ticket_id').AsString);

    if AddinType = atChromeApplication then begin
      AttachmentFileMask := DM.ESketchFileMask(TicketData.FieldByName(
        'ticket_number').AsString);
      WaitForAttachmentsInDownloadDir(AttachmentFileMask);
      DM.AddAutoAttachments(qmftTicket, TicketData.FieldByName(
        'ticket_id').AsInteger, '', AttachmentFileMask + '.@cv',
        AddinAttachmentSource, False);
      Result := True;
    end else begin
      DeleteFile(InputFile);
      Result := FileExists(OutputFile);
      if Result then begin
        ProcessTicketOutputFile(OutputFile);
        DeleteFile(OutputFile);
      end;
    end;
  finally
    TicketData := nil;
    LocateData := nil;
  end;
end;

function TAttachmentsAddinManager.ExecuteForDamage(DamageID: Integer; const SelectedAttachmentID, SelectedForeignType: Integer; const SelectedAttachmentFilename: string): Boolean;
var
  InputFile: string;
  OutputFile: string;
begin
  DamageData := DM.Engine.OpenQuery(Format(SelectDamage, [DamageID]));
  try
    Assert(not DamageData.IsEmpty, 'Damage not found in local cache');

    ActionAttachmentID := SelectedAttachmentID;
    ActionForeignType := SelectedForeignType;
    ActionAttachmentFilename := SelectedAttachmentFilename;
    InputFile := GenerateDamageInput;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAttachAddinStart, DamageData.FieldByName('damage_id').AsString);
    OutputFile := RunAddin(InputFile);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAttachAddinEnd, DamageData.FieldByName('damage_id').AsString);
    DeleteFile(InputFile);

    Result := FileExists(OutputFile);
    if Result then begin
      ProcessDamageOutputFile(OutputFile);
      DeleteFile(OutputFile);
    end;
  finally
    FreeAndNil(DamageData);
  end;
end;

function TAttachmentsAddinManager.ExecuteForWorkOrder(WorkOrder: TDataSet; const SelectedAttachmentID, SelectedForeignType: Integer; const SelectedAttachmentFilename: string): Boolean;
var
  InputFile: string;
  OutputFile: string;
begin
  Assert(not WorkOrder.IsEmpty, 'Work Order not present');

  WorkOrderData := WorkOrder;
  try
    ActionAttachmentID := SelectedAttachmentID;
    ActionForeignType := SelectedForeignType;
    ActionAttachmentFilename := SelectedAttachmentFilename;
    InputFile := GenerateWorkOrderInput;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWOAttachAddinStart, WorkOrderData.FieldByName('wo_id').AsString);
    OutputFile := RunAddin(InputFile);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWOAttachAddinEnd, WorkOrderData.FieldByName('wo_id').AsString);
    DeleteFile(InputFile);

    Result := FileExists(OutputFile);
    if Result then begin
      ProcessWorkOrderOutputFile(OutputFile);
      DeleteFile(OutputFile);
    end;
  finally
    WorkOrderData := nil;
  end;
end;

procedure TAttachmentsAddinManager.SaveAddinInfo(const ForeignType, ForeignID: Integer; const Latitude, Longitude: string);
begin
  if IsEmpty(Latitude) or IsEmpty(Longitude) then
    Exit;

  // TODO: Should this treat 0,0 as Null,Null or a real point off the W. coast of Africa?
  try
    StrToFloat(Latitude);
    StrToFloat(Longitude);
  except
    on EConvertError do
      raise Exception.CreateFmt('Latitude or longitude is not a number: (%s / %s)', [Latitude, Longitude]);
  end;

  AddinInfo.Open;
  AddinInfo.Insert;
  AddinInfo.FieldByName('foreign_type').AsInteger := ForeignType;
  AddinInfo.FieldByName('foreign_id').AsInteger := ForeignID;
  AddinInfo.FieldByName('latitude').AsString := Latitude;
  AddinInfo.FieldByName('longitude').AsString := Longitude;
  AddinInfo.FieldByName('added_by_id').AsInteger := DM.EmpID;
  AddinInfo.FieldByName('added_date').AsDateTime := RoundSQLServerTimeMS(Now);
  AddinInfo.FieldByName('active').AsBoolean := True;
  AddinInfo.Post;
  AddinInfo.Close;
end;

procedure TAttachmentsAddinManager.ApplyTicketUpdates(const Address1, Address2, Street, City, State, Latitude, Longitude: string);
begin
  // TODO: May need to save the other fields somewhere someday, too.
  DM.SetTicketModified;
  SaveAddinInfo(qmftTicket, TicketData.FieldByName('ticket_id').AsInteger, Latitude, Longitude);
end;

procedure TAttachmentsAddinManager.ProcessTicketOutputFile(const FileName: string);
var
  Output: IXMLDOMDocument;

  function GetTicketValue(const ElementName, FieldName: string): string;
  var
    Node: IXMLDOMNode;
  begin
    Result := TicketData.FieldByName(FieldName).AsString;
    Node := Output.selectSingleNode('/QManager/Ticket/' + ElementName);
    if Assigned(Node) then
      Result := Node.text;
  end;

var
  Address1: string;
  Address2: string;
  Street: string;
  City: string;
  State: string;
  Latitude: string;
  Longitude: string;
  NewAttachments: IXMLDOMNodeList;
  NewActivities: IXMLDOMNodeList;
  TicketID: Integer;
begin
  Output := LoadXMLFile(FileName);
  Address1 := GetTicketValue('Address1', 'work_address_number');
  Address2 := GetTicketValue('Address2', 'work_address_number_2');
  Street := GetTicketValue('Street', 'work_address_street');
  City := GetTicketValue('City', 'work_city');
  State := GetTicketValue('State', 'work_state');
  Latitude := GetTicketValue('Latitude', 'work_lat');
  Longitude := GetTicketValue('Longitude', 'work_long');
  ApplyTicketUpdates(Address1, Address2, Street, City, State, Latitude, Longitude);
  TicketID := TicketData.FieldByName('ticket_id').AsInteger;
  NewAttachments := Output.selectNodes('/QManager/Ticket/Attachments/NewAttachment');
  SaveNewAttachments(NewAttachments, qmftTicket, TicketID);
  NewActivities := Output.selectNodes('/QManager/Ticket/Activities/Activity');
  SaveNewActivities(NewActivities);
end;

procedure TAttachmentsAddinManager.ApplyDamageUpdates(const Street, County, City, State, Latitude, Longitude: string);
begin
  SaveAddinInfo(qmftDamage, DamageData.FieldByName('damage_id').AsInteger, Latitude, Longitude);
end;

procedure TAttachmentsAddinManager.ProcessDamageOutputFile(const FileName: string);
var
  Output: IXMLDOMDocument;
const
  DamageNodeRoot = '/QManager/Damage/';
  TicketNodeRoot = '/QManager/Ticket/'; // temporarily, damages are treated like tickets

  function GetDamageValue(const ElementName, FieldName: string): string;
  var
    Node: IXMLDOMNode;
  begin
    Result := '';
    if HaveField(DamageData, FieldName) then
      Result := DamageData.FieldByName(FieldName).AsString;
    Node := Output.selectSingleNode(DamageNodeRoot + ElementName);
    if not Assigned(Node) then
      Node := Output.selectSingleNode(TicketNodeRoot + ElementName);
    if Assigned(Node) then
      Result := Node.text;
  end;

  function SelectDamageOrTicketNodes(const Path: string): IXMLDOMNodeList;
  begin
    Result := Output.selectNodes(DamageNodeRoot + Path);
    if Result.length < 1 then
      Result := Output.selectNodes(TicketNodeRoot + Path);
  end;

var
  NewAttachments: IXMLDOMNodeList;
  NewActivities: IXMLDOMNodeList;
  DamageID: Integer;
  Street: string;
  City: string;
  County: string;
  State: string;
  Latitude: string;
  Longitude: string;

begin
  Output := LoadXMLFile(FileName);

  Street := GetDamageValue('Street', 'location');
  City := GetDamageValue('City', 'city');
  County := GetDamageValue('County', 'county');
  State := GetDamageValue('State', 'state');
  Latitude := GetDamageValue('Latitude', 'addin_lat');
  Longitude := GetDamageValue('Longitude', 'addin_long');
  ApplyDamageUpdates(Street, County, City, State, Latitude, Longitude);

  DamageID := DamageData.FieldByName('damage_id').AsInteger;
  NewAttachments := SelectDamageOrTicketNodes('Attachments/NewAttachment');
  SaveNewAttachments(NewAttachments, qmftDamage, DamageID);
  NewActivities := SelectDamageOrTicketNodes('Activities/Activity');
  SaveNewActivities(NewActivities);
end;

procedure TAttachmentsAddinManager.SaveNewAttachments(Attachments: IXMLDOMNodeList; ForeignType, ForeignID: Integer);
var
  Attachment: IXMLDOMElement;
  NewFile: Widestring;
  i: Integer;
begin
  for i := 0 to Attachments.length - 1 do begin
    Attachment := (Attachments.item[i] as IXMLDOMElement);
    if GetNodeText(Attachment, 'FileName', NewFile) then begin
      if not IsPathAbsolute(NewFile) then
        NewFile := AddSlash(DM.GetAttachmentDownloadFolder) + NewFile;
      if FileExists(NewFile) then
        DM.AddAttachmentToRecord(ForeignType, ForeignID, NewFile, AddinAttachmentSource)
      else
        raise Exception.Create(NewFile + ' does not exist');
    end
    else
      raise Exception.Create('NewAttachment node is missing the <Filename> element: ' + Attachments[i].xml);
  end;
end;

procedure TAttachmentsAddinManager.SaveNewActivities(Activities: IXMLDOMNodeList);
var
  Activity: IXMLDOMElement;
  ActivityDetails1: WideString;
  ActivityDetails2: WideString;
  ActivityTimestamp: WideString;
  ActivityDateTime: TDateTime;  i: Integer;
begin
  for i := 0 to Activities.length - 1 do begin
    Activity := (Activities.item[i] as IXMLDOMElement);

    if not GetNodeText(Activity, 'Timestamp', ActivityTimestamp) then
      raise Exception.Create('The Activity element does not contain a Timestamp child element');
    if not IsoIsValidDateTime(ActivityTimestamp) then
      raise Exception.Create('The activity timestamp is not a valid ISO date and time: ' + ActivityTimestamp);
    ActivityDateTime := IsoStrToDateTime(ActivityTimestamp);

    GetNodeText(Activity, 'Details1', ActivityDetails1); // Optional
    GetNodeText(Activity, 'Details2', ActivityDetails2); // Optional

    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeAddinActivity, ActivityDetails1, ActivityDateTime, ActivityDetails2);
  end;
end;

function TAttachmentsAddinManager.BuildAddinCommandLine(const InputFile, OutputFile: string): string;
begin
  if AddinType = atChromeApplication then
    Result := '"' + AddinFileName + '" --user-data-dir="' + BrowserUserDataDir +
      '" "' + AddinURL + '?' + GenerateTicketURLSearchPart + '"'
  else
    Result := Format('"%s" -i "%s" -o "%s" -u "%s" -p "%s" -e "%s" -d "%d"',
      [AddinFileName, InputFile, OutputFile, DM.UQState.UserName, DM.UQState.Password,
      EmployeeDM.GetUserEmployeeNumber, DM.EmpID]);
end;

procedure TAttachmentsAddinManager.ProcessWorkOrderOutputFile(const FileName: string);
var
  Output: IXMLDOMDocument;
const
  NodeRoot = '/QManager/Ticket/'; // work orders are treated like tickets

  function GetValue(const ElementName, FieldName: string): string;
  var
    Node: IXMLDOMNode;
  begin
    Result := '';
    if HaveField(WorkOrderData, FieldName) then
      Result := WorkOrderData.FieldByName(FieldName).AsString;
    Node := Output.selectSingleNode(NodeRoot + ElementName);
    if Assigned(Node) then
      Result := Node.text;
  end;

  function SelectTicketNodes(const Path: string): IXMLDOMNodeList;
  begin
    Result := Output.selectNodes(NodeRoot + Path);
  end;

var
  NewAttachments: IXMLDOMNodeList;
  NewActivities: IXMLDOMNodeList;
  WorkOrderID: Integer;
  Street: string;
  City: string;
  County: string;
  State: string;
  Latitude: string;
  Longitude: string;

begin
  Output := LoadXMLFile(FileName);

  Street := GetValue('Street', 'location');
  City := GetValue('City', 'city');
  County := GetValue('County', 'county');
  State := GetValue('State', 'state');
  Latitude := GetValue('Latitude', 'addin_lat');
  Longitude := GetValue('Longitude', 'addin_long');
  ApplyWorkOrderUpdates(Street, County, City, State, Latitude, Longitude);

  WorkOrderID := WorkOrderData.FieldByName('wo_id').AsInteger;
  NewAttachments := SelectTicketNodes('Attachments/NewAttachment');
  SaveNewAttachments(NewAttachments, qmftWorkOrder, WorkOrderID);
  NewActivities := SelectTicketNodes('Activities/Activity');
  SaveNewActivities(NewActivities);
end;

procedure TAttachmentsAddinManager.ApplyWorkOrderUpdates(const Street, County, City, State, Latitude, Longitude: string);
begin
  if not EditingDataSet(DM.WorkOrder) then
    DM.SetWorkOrderModified;
  SaveAddinInfo(qmftWorkOrder, WorkOrderData.FieldByName('wo_id').AsInteger, Latitude, Longitude);
end;

end.
