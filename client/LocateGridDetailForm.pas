unit LocateGridDetailForm;
{QMANTWO-422: Added this search form to be displayed on Search screen to
 display locates. However, it may be used as an independent form or displayed
 elsewhere as dictated (EB)}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxDataStorage, cxEdit, cxNavigator,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dbisamtb, MSXML2_TLB,
  odDBISAMUtils, OdMSXMLUtils, OdXMLToDataSet, OdCxXMLUtils, OdCXUtils, DMu,
  cxCustomData, cxFilter, cxData;

type
  TLocateGridDetail = class(TForm)
    LocGridView: TcxGridDBTableView;
    LocGridLevel1: TcxGridLevel;
    LocGrid: TcxGrid;
    LocSource: TDataSource;
    LocateSearch: TDBISAMTable;
    procedure FormShow(Sender: TObject);
  private
    fTicketID: integer;
    fColumnsDefined: Boolean;
    function GetXmlStringforLocates(TicketID: integer): string;
    procedure SetLocateRecordCount(Dom: IXMLDOMDocument);
  protected
    LocDef: TColumnList;
    FHeaderWidth: Integer;
    FBlockGridUpdates: boolean;
  public
    property TicketID: integer read fTicketID write fTicketID;
    procedure DefineColumns;
    procedure LoadLocateGrid(TicketID: integer; TicketNum: string);
    destructor Destroy; override;
    constructor Create(AOwner: TComponent); override;
    class function CreateForGrid(pOwner: TComponent; ParentWinControl: TWinControl): TLocateGridDetail;
  end;

var
  LocateGridDetail: TLocateGridDetail;

implementation

{$R *.dfm}



destructor TLocateGridDetail.Destroy;
begin
  FreeAndNil(LocDef);
  inherited;
end;

procedure TLocateGridDetail.LoadLocateGrid(TicketID: integer; TicketNum: string);

var
  LDOM: IXMLDOMDocument;
begin
    Self.Caption := 'Locates for Ticket #: ' + TicketNum;
    LDOM := ParseXml(GetXmlStringforLocates(TicketID));
    SetLocateRecordCount(LDOM);

    LocGrid.BeginUpdate;
    LocateSearch.DisableControls;
    FBlockGridUpdates := True;
    try
      DefineColumns;
      ConvertXMLToDataSet(LDOM, LocDef, LocateSearch);   //, Criteria.CustomizeDataSet
      FHeaderWidth := CreateGridColumnsFromDef(LocGridView, LocateSearch, LocDef);
      SortGridByFirstVisibleColumn(LocGrid);

    finally
     LocateSearch.EnableControls;
      LocGrid.EndUpdate;
      FBlockGridUpdates := False;
    end;
    if not Self.Showing then
        Self.Show;
end;

procedure TLocateGridDetail.DefineColumns;
const
  NotVisible=False;
begin
  if not fColumnsDefined then begin
    LocDef.AddColumn('Locator ID', 0, 'locator_id', ftInteger, '', 0, False, NotVisible);
    LocDef.AddColumn('Ticket Number', 95, 'ticket_number', ftString, '', 0, False, NotVisible );
    LocDef.AddColumn('Locate ID', 0, 'locate_id', ftInteger, '', 0, True, NotVisible);
    LocDef.AddColumn('Locator', 125, 'locator', ftString);
    LocDef.AddColumn('Status', 40, 'status', ftString);
    LocDef.AddColumn('Client Code', 55, 'client_code', ftString);
    LocDef.AddColumn('Client', 115, 'client', ftString);
    LocDef.AddColumn('High Profile', 50, 'high_profile', ftBoolean);
    LocDef.AddColumn('Qty Marked', 50, 'qty_marked', ftInteger);  // locate_status
    LocDef.AddColumn('Closed', 35, 'closed', ftBoolean);
    LocDef.AddColumn('Closed Date', 95, 'closed_date', ftDateTime);
    fColumnsDefined := True;
  end;
end;

constructor TLocateGridDetail.Create(AOwner: TComponent);
begin
  inherited;
  LocDef := TColumnList.Create;
end;

class function TLocateGridDetail.CreateForGrid(
  pOwner: TComponent; ParentWinControl: TWinControl): TLocateGridDetail;
begin
  Result := TLocateGridDetail.Create(pOwner);
  Result.Parent := ParentWinControl;
  Result.Align := alClient;
end;


procedure TLocateGridDetail.FormShow(Sender: TObject);
begin
  FocusFirstGridRow(LocGrid);
end;

function TLocateGridDetail.GetXmlStringforLocates(TicketID: integer): string;
begin
  DM.CallingServiceName('GetTicketLocateList');
  Result := DM.Engine.Service.GetTicketLocateList(DM.Engine.SecurityInfo, TicketID);
end;


procedure TLocateGridDetail.SetLocateRecordCount(Dom: IXMLDOMDocument);

  function GetIntAttr(Node: IXMLDOMNode; Attr: string): Integer;
  var
    Attribute: IXMLDOMNode;
  begin
    Result := -1;
    Attribute := Node.attributes.getNamedItem(Attr);
    if Assigned(Attribute) then
      Result := StrToIntDef(Attribute.text, -1);
  end;

var
//  Matching: Integer;
//  Max: Integer;
  Node: IXmlDomNode;
begin
  Node := Dom.selectSingleNode('//rows');
//  if Assigned(Node) then begin
//    Matching := GetIntAttr(Node, 'ActualRows');
//    Max := GetIntAttr(Node, 'MaxRows');
//  end
end;



end.
