unit TicketsDueReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, dxCore, cxDateUtils;

type
  TTicketsDueReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ManagersComboBox: TComboBox;
    DateTimeSelect: TcxDateEdit;
    PrintTicketNotesBox: TCheckBox;
    PrintRemarksBox: TCheckBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TTicketsDueReportForm }

procedure TTicketsDueReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TicketsDue');
  SetParamDate('DueDate', DateTimeSelect.Date);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamBoolean('PrintTicketNotes', PrintTicketNotesBox.Checked);
  SetParamBoolean('PrintRemarks', PrintRemarksBox.Checked);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TTicketsDueReportForm.InitReportControls;
begin
  inherited;
  DateTimeSelect.Date := Date + 1;
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.
