object PhotoAttachmentFrame: TPhotoAttachmentFrame
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  Align = alClient
  AutoSize = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  OnResize = FrameResize
  object PanelPhotos: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 259
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    BorderWidth = 5
    Caption = 'PanelPhotos'
    TabOrder = 0
    object ScrollBox: TScrollBox
      Left = 5
      Top = 5
      Width = 441
      Height = 249
      HorzScrollBar.Visible = False
      VertScrollBar.Smooth = True
      VertScrollBar.Style = ssFlat
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = clWindow
      ParentColor = False
      TabOrder = 0
    end
  end
  object PanelFooter: TPanel
    Left = 0
    Top = 259
    Width = 451
    Height = 45
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object LabelCurrentDirectory: TLabel
      Left = 8
      Top = 0
      Width = 126
      Height = 13
      Caption = 'LabelCurrentDirectory'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelMessage: TLabel
      Left = 633
      Top = 22
      Width = 82
      Height = 13
      Caption = 'LabelMessage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object ButtonSelAnotherDir: TButton
      Left = 8
      Top = 17
      Width = 100
      Height = 25
      Caption = 'Select Directory...'
      TabOrder = 0
      OnClick = ButtonSelAnotherDirClick
    end
    object ButtonAttachAll: TButton
      Left = 120
      Top = 17
      Width = 100
      Height = 25
      Caption = 'Attach All'
      TabOrder = 1
      OnClick = ButtonAttachAllClick
    end
    object ButtonAttachSelected: TButton
      Left = 232
      Top = 17
      Width = 100
      Height = 25
      Caption = 'Attach Selected'
      TabOrder = 2
      OnClick = ButtonAttachSelectedClick
    end
    object CheckBoxShowSelOnly: TCheckBox
      Left = 463
      Top = 21
      Width = 152
      Height = 17
      Caption = 'Show selected images only'
      TabOrder = 3
      OnClick = CheckBoxShowSelOnlyClick
    end
    object ButtonCancel: TButton
      Left = 345
      Top = 17
      Width = 100
      Height = 25
      Caption = 'Cancel'
      TabOrder = 4
      OnClick = ButtonCancelClick
    end
  end
end
