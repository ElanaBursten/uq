object LocateWorkForm: TLocateWorkForm
  Left = 193
  Top = 146
  BorderIcons = [biSystemMenu]
  Caption = 'Work Done on Locates'
  ClientHeight = 382
  ClientWidth = 800
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 480
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    800
    382)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 142
    Width = 94
    Height = 13
    Caption = 'Previously Entered:'
  end
  object WorkGroup: TGroupBox
    Left = 12
    Top = 7
    Width = 785
    Height = 130
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Add Units/Hours'
    TabOrder = 0
    object Label2: TLabel
      Left = 47
      Top = 19
      Width = 55
      Height = 13
      Caption = 'Work Date:'
    end
    object WarningLabel: TLabel
      Left = 336
      Top = 16
      Width = 438
      Height = 19
      AutoSize = False
      Transparent = True
      WordWrap = True
    end
    object WorkDate: TcxDateEdit
      Left = 132
      Top = 16
      EditValue = '-700000'
      Properties.DateButtons = [btnToday]
      Properties.DateOnError = deToday
      TabOrder = 0
      Width = 173
    end
    object HoursGroup: TGroupBox
      Left = 7
      Top = 41
      Width = 569
      Height = 83
      Caption = '  Hours  '
      TabOrder = 1
      object OvertimeHoursLabel: TLabel
        Left = 8
        Top = 41
        Width = 79
        Height = 13
        Caption = 'Overtime Hours:'
      end
      object RegularHoursLabel: TLabel
        Left = 8
        Top = 17
        Width = 72
        Height = 13
        Caption = 'Regular Hours:'
      end
      object Label3: TLabel
        Left = 8
        Top = 62
        Width = 73
        Height = 13
        Caption = 'Tot Hrs to add:'
      end
      object TotalHoursLabel: TLabel
        Left = 100
        Top = 62
        Width = 97
        Height = 13
        Caption = '------             -------  '
      end
      object Label5: TLabel
        Left = 132
        Top = 21
        Width = 19
        Height = 13
        Caption = 'hrs.'
      end
      object Label6: TLabel
        Left = 194
        Top = 21
        Width = 25
        Height = 13
        Caption = 'mins.'
      end
      object Label7: TLabel
        Left = 132
        Top = 44
        Width = 19
        Height = 13
        Caption = 'hrs.'
      end
      object Label8: TLabel
        Left = 194
        Top = 44
        Width = 25
        Height = 13
        Caption = 'mins.'
      end
      object CloseLocates: TCheckBox
        Left = 298
        Top = 56
        Width = 144
        Height = 17
        Caption = 'Close all affected locates'
        TabOrder = 2
      end
      object ApplyToAll: TCheckBox
        Left = 300
        Top = 11
        Width = 198
        Height = 17
        Caption = 'Apply to all OH and -R status locates:'
        TabOrder = 0
      end
      object AllLocatesMemo: TMemo
        Left = 300
        Top = 28
        Width = 229
        Height = 29
        TabStop = False
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Lines.Strings = (
          '(Verizon - VZN111, Washington Gas - '
          'WGL44, Edison Electric - EE012)')
        ParentColor = True
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object edtRegHrs: TEdit
        Left = 100
        Top = 14
        Width = 30
        Height = 21
        TabOrder = 3
        OnKeyPress = edtRegHrsKeyPress
      end
      object edtRegMins: TEdit
        Left = 163
        Top = 13
        Width = 30
        Height = 21
        TabOrder = 4
        OnKeyPress = edtRegHrsKeyPress
      end
      object edtOTHrs: TEdit
        Left = 100
        Top = 36
        Width = 30
        Height = 21
        TabOrder = 5
        OnKeyPress = edtRegHrsKeyPress
      end
      object edtOTMins: TEdit
        Left = 163
        Top = 36
        Width = 30
        Height = 21
        TabOrder = 6
        OnKeyPress = edtRegHrsKeyPress
      end
      object btnTotalHrs: TBitBtn
        Left = 220
        Top = 57
        Width = 54
        Height = 20
        Caption = 'Total Hrs'
        TabOrder = 7
        OnClick = btnTotalHrsClick
      end
    end
    object UnitsGroup: TGroupBox
      Left = 582
      Top = 41
      Width = 200
      Height = 83
      Caption = '  Units  '
      TabOrder = 2
      object UnitsLabel: TLabel
        Left = 10
        Top = 17
        Width = 66
        Height = 13
        Caption = 'Units Marked:'
      end
      object Label4: TLabel
        Left = 10
        Top = 41
        Width = 35
        Height = 13
        Caption = 'Status:'
      end
      object UnitsMarked: TcxSpinEdit
        Left = 82
        Top = 14
        AutoSize = False
        Properties.AssignedValues.MinValue = True
        Properties.MaxValue = 50000.000000000000000000
        TabOrder = 0
        Height = 21
        Width = 57
      end
      object StatusCombo: TComboBox
        Left = 58
        Top = 41
        Width = 81
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          ''
          'C'
          'M'
          'N'
          'XA')
      end
    end
  end
  object HoursGrid: TcxGrid
    Left = 12
    Top = 160
    Width = 780
    Height = 183
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object HoursGridTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnEditing = HoursGridTableViewEditing
      OnFocusedItemChanged = HoursGridTableViewFocusedItemChanged
      DataController.DataSource = LocateHoursDS
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'locate_hours_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = ColRegularHours
        end
        item
          Kind = skSum
          Column = ColOvertimeHours
        end
        item
          Kind = skSum
          Column = ColTotalHours
        end
        item
          Kind = skSum
          Column = ColUnitsMarked
        end
        item
          OnGetText = HoursGridTableViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems4GetText
          Column = ColUnitType
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.HideSelection = True
      OptionsSelection.InvertSelect = False
      OptionsView.Footer = True
      OptionsView.GridLineColor = clBtnFace
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ColClient: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_code'
        PropertiesClassName = 'TcxTextEditProperties'
        Visible = False
      end
      object ColClientName: TcxGridDBColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_name'
        PropertiesClassName = 'TcxTextEditProperties'
        SortIndex = 0
        SortOrder = soAscending
        Width = 145
      end
      object ColEmployee: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 107
      end
      object ColWorkDate: TcxGridDBColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'work_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.ReadOnly = True
        SortIndex = 1
        SortOrder = soDescending
        Width = 76
      end
      object ColEntryDate: TcxGridDBColumn
        Caption = 'Entry Date'
        DataBinding.FieldName = 'entry_date'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 100
      end
      object ColRegularHours: TcxGridDBColumn
        Caption = 'Reg Hrs'
        DataBinding.FieldName = 'regular_hours'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MinValue = True
        Properties.Increment = 0.250000000000000000
        Properties.MaxValue = 24.000000000000000000
        HeaderAlignmentHorz = taRightJustify
        Width = 51
      end
      object ColOvertimeHours: TcxGridDBColumn
        Caption = 'OT Hrs'
        DataBinding.FieldName = 'overtime_hours'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.AssignedValues.MinValue = True
        Properties.Increment = 0.250000000000000000
        Properties.MaxValue = 24.000000000000000000
        HeaderAlignmentHorz = taRightJustify
        Width = 45
      end
      object ColTotalHours: TcxGridDBColumn
        Caption = 'Tot Hrs'
        DataBinding.FieldName = 'total_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        HeaderAlignmentHorz = taRightJustify
        Width = 52
      end
      object ColStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Width = 37
      end
      object ColUnitsMarked: TcxGridDBColumn
        Caption = 'Units'
        DataBinding.FieldName = 'units_marked'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.Alignment.Horz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 30
        Width = 60
      end
      object ColUnitType: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'unit_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.ReadOnly = True
        Width = 46
      end
      object ColLocateId: TcxGridDBColumn
        DataBinding.FieldName = 'locate_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Visible = False
        HeaderAlignmentHorz = taRightJustify
        IsCaptionAssigned = True
      end
    end
    object HoursGridLevel: TcxGridLevel
      GridView = HoursGridTableView
    end
  end
  object CloseButton: TButton
    Left = 395
    Top = 350
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Close'
    ModalResult = 2
    TabOrder = 3
    OnClick = CloseButtonClick
  end
  object AddHoursButton: TButton
    Left = 282
    Top = 350
    Width = 104
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save Hours/Units'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = AddHoursButtonClick
  end
  object LocateHoursDS: TDataSource
    Left = 16
    Top = 182
  end
end
