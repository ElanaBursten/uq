unit Sync;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, Buttons, DB, DBISAMTb, Progress,
  DateUtils, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit,
  cxCalendar, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxNavigator, cxCustomData,
  cxFilter, cxData;

type
  TSyncForm = class(TEmbeddableForm)
    SyncLog: TMemo;
    HeaderPanel: TPanel;
    AttachmentsPanel: TPanel;
    AttachmentsSource: TDataSource;
    AttachmentGrid: TcxGrid;
    AttachmentLevel: TcxGridLevel;
    AttachmentView: TcxGridDBTableView;
    Splitter: TSplitter;
    AttachmentsActionPanel: TPanel;
    Attachments: TDBISAMQuery;
    ColAttachmentID: TcxGridDBColumn;
    ColFileName: TcxGridDBColumn;
    ColOrigFileName: TcxGridDBColumn;
    ColAttachDate: TcxGridDBColumn;
    ColUploadDate: TcxGridDBColumn;
    ColComment: TcxGridDBColumn;
    ColSize: TcxGridDBColumn;
    UploadButton: TButton;
    LocationCombo: TComboBox;
    LocationLabel: TLabel;
    SyncPanel: TPanel;
    NetworkCombo: TComboBox;
    TimeLabel: TLabel;
    UploadSizeTotal: TDBISAMQuery;
    UploadProgressFrame: TProgressFrame;
    LunchLockoutTimer: TTimer;
    LunchLockoutTimeLabel: TLabel;
    OverrideLunchLockButton: TButton;
    procedure UploadButtonClick(Sender: TObject);
    procedure AttachmentGridExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NetworkComboChange(Sender: TObject);
    procedure ColSizeGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure OverrideLunchLockButtonClick(Sender: TObject);
    procedure LunchLockoutTimerTimer(Sender: TObject);
  private
    FOnSyncDone: TNotifyEvent;
    FOnSyncStart: TNotifyEvent;
    FOnShowTimesheet: TNotifyEvent;
    FOnLunchLockoutExpired: TNotifyEvent;
    FCanUploadInBackground: Boolean;
    LunchUnlockTime: TDateTime;
    procedure LogSyncMessage(Sender: TObject; const Msg: string);
    procedure AddLogLine(Msg: string);
    procedure SyncNow;
    procedure UpdateAttachmentList;
    procedure SetAttachmentsVisible(Value: Boolean);
    procedure StartLogCapture;
    procedure StopLogCapture;
    procedure UpdateTimeEstimate;
    procedure PoulateUploadSpeedCombo;
    procedure EnableProgressFeedback(const Enable: Boolean);
    procedure UploadMajorProgress(Sender: TObject; const ProgressMessage: string; const PctComplete: Integer);
    procedure UploadMinorProgress(Sender: TObject; const ProgressMessage: string; const PctComplete: Integer);
    procedure SetupBackgroundUploader;
    procedure SetLunchControlsVisibleEnabled(const Enable: Boolean);
  public
    procedure CheckLunchLockout;
    procedure ActivatingNow; override;
    function RequireSync: Boolean; override;
    property OnSyncDone: TNotifyEvent read FOnSyncDone write FOnSyncDone;
    property OnSyncStart: TNotifyEvent read FOnSyncStart write FOnSyncStart;
    property OnShowTimesheet: TNotifyEvent read FOnShowTimesheet write FOnShowTimesheet;
    property OnLunchLockoutExpired: TNotifyEvent read FOnLunchLockoutExpired write FOnLunchLockoutExpired;
  end;

implementation

uses DMu, OdHourglass, OdLog, OdDBUtils, OdVclUtils, OdExceptions, QMConst,
  OdMiscUtils, UQUtils, LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

procedure TSyncForm.SyncNow;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  if Assigned(FOnSyncStart) then
    FOnSyncStart(Self);
  StartLogCapture;
  try
    try
      DM.SyncNow;
    except
      on E: Exception do begin
        OdLog.GetLog.LogException(E);
        AddLogLine('ERROR:');
        AddLogLine(Copy(E.Message, 1, 600) + ' (' + E.ClassName + ')');
      end;
    end;
  finally
    StopLogCapture;
    if Assigned(FOnSyncDone) then
      FOnSyncDone(Self);
  end;
end;

procedure TSyncForm.LogSyncMessage(Sender: TObject; const Msg: string);
begin
  AddLogLine(Msg);
end;

procedure TSyncForm.AddLogLine(Msg: string);
begin
  SyncLog.Lines.Add(FormatDateTime(LongTimeFormat, Now) + ': ' + Msg);
end;

procedure TSyncForm.ActivatingNow;
begin
  inherited;
  UploadProgressFrame.Hide;
  SetAttachmentsVisible(False);
  SyncNow;
  FCanUploadInBackground := DM.CanBackgroundUpload;
  UpdateAttachmentList;
  DM.UploadLocationList(LocationCombo.Items);
  SelectComboBoxItemFromObjects(LocationCombo, DM.UQState.LastUploadLocation);
  SetupBackgroundUploader;
  CheckLunchLockout;
end;

function TSyncForm.RequireSync: Boolean;
begin
  Result := False;
end;

procedure TSyncForm.UpdateAttachmentList;
begin
  Attachments.ParamByName('EmpID').AsInteger := DM.EmpID;
  RefreshDataSet(Attachments);
  if not FCanUploadInBackground then begin
    SetAttachmentsVisible(Attachments.RecordCount > 0);
    // Force it above the attachments list
    Splitter.Align := alNone;
    Splitter.Align := alBottom;
    UpdateTimeEstimate;
  end;
end;

procedure TSyncForm.SetAttachmentsVisible(Value: Boolean);
begin
  AttachmentsPanel.Visible := Value;
  Splitter.Visible := Value;
end;

procedure TSyncForm.UploadButtonClick(Sender: TObject);
begin
  UpdateAndPostDataSet(Attachments);
  if LocationCombo.ItemIndex < 0 then
    raise EOdEntryRequired.Create('Please select an upload location');
  StartLogCapture;
  try
    EnableProgressFeedback(True);
    DM.UploadPendingAttachments(GetComboObjectInteger(LocationCombo));
  finally
    EnableProgressFeedback(False);
    StopLogCapture;
    UpdateAttachmentList;
  end;
  DM.UQState.LastUploadLocation := GetComboObjectInteger(LocationCombo);
end;

procedure TSyncForm.AttachmentGridExit(Sender: TObject);
begin
  UpdateAndPostDataSet(Attachments);
end;

procedure TSyncForm.FormCreate(Sender: TObject);
begin
  inherited;
  DM.Engine.LinkEventsAutoInc(Attachments);
  PoulateUploadSpeedCombo;
  SetFontBold(LunchLockoutTimeLabel);
end;

procedure TSyncForm.StartLogCapture;
begin
  DM.Engine.Log := LogSyncMessage;
end;

procedure TSyncForm.StopLogCapture;
begin
  DM.Engine.Log := nil;
end;

procedure TSyncForm.UpdateTimeEstimate;
const
  EstimateLabel = 'Estimated upload time is %s when using this type of connection:';
var
  TotalBytes: Integer;
  KBPerSec: Integer;
begin
  UploadSizeTotal.ParamByName('EmpID').AsInteger := DM.EmpID;
  UploadSizeTotal.Open;
  try
    TotalBytes := UploadSizeTotal.Fields[0].AsInteger;
    KBPerSec := TransferSpeeds[NetworkCombo.ItemIndex].KBPerSec;
    TimeLabel.Caption := Format(EstimateLabel, [SecondsToHMS(Round((TotalBytes/1024)/KBPerSec) + 5)]);
  finally
    UploadSizeTotal.Close;
  end;
end;

procedure TSyncForm.PoulateUploadSpeedCombo;
var
  i: Integer;
begin
  NetworkCombo.Clear;
  for i := Low(TransferSpeeds) to High(TransferSpeeds) do
    NetworkCombo.Items.AddObject(TransferSpeeds[i].Description, TObject(i));
  NetworkCombo.ItemIndex := 0;
end;

procedure TSyncForm.NetworkComboChange(Sender: TObject);
begin
  UpdateTimeEstimate;
end;

procedure TSyncForm.ColSizeGetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: String);
begin
  AText := GetKBStringForBytes(StrToIntDef(AText, 0));
end;

procedure TSyncForm.EnableProgressFeedback(const Enable: Boolean);
begin
  if not FCanUploadInBackground then begin
    if Enable then begin
      UploadProgressFrame.Show;
      DM.AttachmentManager.OnMajorProgress := UploadMajorProgress;
      DM.AttachmentManager.OnMinorProgress := UploadMinorProgress;
    end else begin
      DM.AttachmentManager.OnMajorProgress := nil;
      DM.AttachmentManager.OnMinorProgress := nil;
      UploadProgressFrame.Hide;
    end;
  end;
end;

procedure TSyncForm.SetupBackgroundUploader;
begin
  if FCanUploadInBackground then
    DM.UploadPendingAttachments(-1)     // start now
end;

procedure TSyncForm.UploadMajorProgress(Sender: TObject; const ProgressMessage: string;
  const PctComplete: Integer);
begin
  UploadProgressFrame.ShowMajorProgress(ProgressMessage, PctComplete);
  if PctComplete >= 100 then
    EnableProgressFeedback(False);
end;

procedure TSyncForm.UploadMinorProgress(Sender: TObject; const ProgressMessage: string;
  const PctComplete: Integer);
begin
  UploadProgressFrame.ShowMinorProgress(ProgressMessage, PctComplete);
end;

procedure TSyncForm.CheckLunchLockout;
begin
  if PermissionsDM.CanI(RightWorkDuringLunch) then
    SetLunchControlsVisibleEnabled(False)
  else if DM.IsLunchLockout then begin
    SetLunchControlsVisibleEnabled(True);
  end;
end;

procedure TSyncForm.SetLunchControlsVisibleEnabled(const Enable: Boolean);
begin
  if Enabled then
    LunchUnlockTime := DM.GetLunchUnlockTime
  else
    LunchUnlockTime := 0;
  LunchLockoutTimer.Enabled := Enable;
  LunchLockoutTimeLabel.Visible := Enable;
  OverrideLunchLockButton.Visible := Enable;
end;

procedure TSyncForm.OverrideLunchLockButtonClick(Sender: TObject);
{- Confirmation message is displayed and user selects Agree to proceed.
- If the user selects Disagree the window is closed and the clock continues to count down backwards.
- Manager receives email notification that user has clocked in during their meal time.
- User will select �meal In� and begin working.}
begin
  if DM.ConfirmShortLunchBreak then begin
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeLunchBreakException, '', Now);
    SetLunchControlsVisibleEnabled(False);
    if Assigned(FOnShowTimesheet) then
      FOnShowTimesheet(Self);
  end;
end;

procedure TSyncForm.LunchLockoutTimerTimer(Sender: TObject);
var
  ShowSeconds: Boolean;
begin
  if (not SameTime(LunchUnlockTime, 0)) and (LunchUnlockTime > Time) then begin
    ShowSeconds := SecondsBetween(LunchUnlockTime, Time) < 60;
    LunchLockoutTimeLabel.Caption := 'Remaining break time: ' +   //qmantwo-237   --sr
      SecondsToTime(SecondsBetween(LunchUnlockTime, Time), ShowSeconds);
  end else begin
    SetLunchControlsVisibleEnabled(False);
    if Assigned(FOnLunchLockoutExpired) then
      FOnLunchLockoutExpired(self);
  end;
end;

end.

