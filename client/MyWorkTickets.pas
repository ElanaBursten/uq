unit MyWorkTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableDataFrame, BaseExpandableGridFrame, StdCtrls, ExtCtrls,
  cxStyles, cxGraphics, cxDataStorage,
  cxEdit, cxDBData, cxDropDownEdit, cxTextEdit, cxCheckBox, DBISAMTb, DB,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxGridLevel, DMu, UQUtils, QMConst, dxCore,
  cxNavigator, LocalEmployeeDMu, cxCustomData, cxFilter, cxData, dxSkinsCore;

const
  UM_EXPANDALL = WM_USER + $101;
  UM_SET_TO_MAP = WM_USER + $105;
  BlankTime = '00:00:00';
type
  TMyWorkTicketsFrame = class(TExpandableGridFrameBase)
    TicketSource: TDataSource;
    OpenTickets: TDBISAMQuery;
    TicketLocates: TDBISAMTable;
    ClearRoutes: TDBISAMQuery;
    SetRoute: TDBISAMQuery;
    CenterTicketSummary: TDBISAMTable;
    CenterTicketSummarySource: TDataSource;
    ClearButton: TButton;
    ApplyButton: TButton;
    MyWorkViewRepository: TcxGridViewRepository;
    TicketsGridDetailsTableView: TcxGridDBBandedTableView;
    TktDColTicketNumber: TcxGridDBBandedColumn;
    TktDColKind: TcxGridDBBandedColumn;
    TktDColDueDate: TcxGridDBBandedColumn;
    TktDColWorkDate: TcxGridDBBandedColumn;
    TktDColTicketType: TcxGridDBBandedColumn;
    TktDColCompany: TcxGridDBBandedColumn;
    TktDColPriority: TcxGridDBBandedColumn;
    TktDColWorkType: TcxGridDBBandedColumn;
    TktDColWorkCity: TcxGridDBBandedColumn;
    TktDColStreetNum: TcxGridDBBandedColumn;
    TktDColStreet: TcxGridDBBandedColumn;
    TktDColWorkAddressStreet: TcxGridDBBandedColumn;
    TktDColWorkCounty: TcxGridDBBandedColumn;
    TktDColAlert: TcxGridDBBandedColumn;
    TktDColWorkDescription: TcxGridDBBandedColumn;
    TktDColLegalGoodThru: TcxGridDBBandedColumn;
    TktDColMapPage: TcxGridDBBandedColumn;
    TktDColTicketID: TcxGridDBBandedColumn;
    TktDColLocates: TcxGridDBBandedColumn;
    TktDColQuickClose: TcxGridDBBandedColumn;
    TktDColRoute: TcxGridDBBandedColumn;
    TktDColWorkPriority: TcxGridDBBandedColumn;
    TktDColWorkPriorityDescription: TcxGridDBBandedColumn;
    TktDColFirstTask: TcxGridDBBandedColumn;
    TktDColExport: TcxGridDBBandedColumn;
    TktDColTicketStatus: TcxGridDBBandedColumn;
    TicketsGridSummaryTableView: TcxGridDBBandedTableView;
    TktSColTicketID: TcxGridDBBandedColumn;
    TktSColTicketNumber: TcxGridDBBandedColumn;
    TktSColKind: TcxGridDBBandedColumn;
    TktSColAlert: TcxGridDBBandedColumn;
    TktSColDueDate: TcxGridDBBandedColumn;
    TktSColWorkAddressNumber: TcxGridDBBandedColumn;
    TktSColWorkAddressStreet: TcxGridDBBandedColumn;
    TktSColStatus: TcxGridDBBandedColumn;
    TktSColWorkDescription: TcxGridDBBandedColumn;
    TktSColLegalGoodThru: TcxGridDBBandedColumn;
    TktSColWorkCounty: TcxGridDBBandedColumn;
    TktSColMapPage: TcxGridDBBandedColumn;
    TktSColLocates: TcxGridDBBandedColumn;
    TktSColRoute: TcxGridDBBandedColumn;
    TktSColWorkPriorityDescription: TcxGridDBBandedColumn;
    TktSColWorkPriority: TcxGridDBBandedColumn;
    TktSColQuickClose: TcxGridDBBandedColumn;
    TktSColFirstTask: TcxGridDBBandedColumn;
    TktSColExport: TcxGridDBBandedColumn;
    TktSColTicketStatus: TcxGridDBBandedColumn;
    TktDColWorkloadDate: TcxGridDBBandedColumn;
    TktSColWorkloadDate: TcxGridDBBandedColumn;
    TktDColTicketDist: TcxGridDBBandedColumn;
    TktSColTicketDist: TcxGridDBBandedColumn;
    TktDColRouteOrder: TcxGridDBBandedColumn;
    TktSColRouteOrder: TcxGridDBBandedColumn;
    TktSColTicketType: TcxGridDBBandedColumn;
    TickDColTciketRisk: TcxGridDBBandedColumn;
    TckSColRiskScore: TcxGridDBBandedColumn;
    lblFirstTask: TLabel;
    TickDColExpDate: TcxGridDBBandedColumn;
    TickDColAltDueDate: TcxGridDBBandedColumn;
    TickSColExpDate: TcxGridDBBandedColumn;
    TickSColAltDueDate: TcxGridDBBandedColumn;
    procedure ApplyButtonClick(Sender: TObject);
    procedure OpenTicketsAfterScroll(DataSet: TDataSet);
    procedure ClearButtonClick(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure OpenTicketsCalcFields(DataSet: TDataSet);
    procedure OpenTicketsBeforeOpen(DataSet: TDataSet);
  private
    ROLimited:boolean; //qm-902 sr
    ROLimitCnt:integer;//qm-902 sr
    FCurrentTicketFormat: string;
    FOnSelectTicket: TItemSelectedEvent;
    procedure ExpandAll;
    procedure UMExpandAll(var Message: TMessage); message UM_EXPANDALL;
    procedure SetupPriorityDisplay;
    procedure RouteOnSetText(Sender: TField; const Text: string);
    procedure UpdateQuickCloseStatuses;
    procedure ColFirstTaskChange(Sender: TObject);
    procedure ColWorkPriorityGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    function QuickCloseColumn: TcxGridColumn;
    function TicketStatusColumn: TcxGridColumn;
    function FirstTaskColumn: TcxGridColumn;
    procedure SetTicketEstimatedStartTime(const TicketID: Integer;
      const StartTime: TDateTime);
    function GetLimitedTicketIDsString(Limit: Integer): string;
    procedure RefreshTickets;
    function IsValidStatus(const Status: string): Boolean;
    procedure SetRecordValue(AView: TcxCustomGridTableView; ARecordIndex,
      AItemIndex: Integer; AValue: Variant);

  protected
    function ItemIDColumn: TcxGridColumn; override;
    procedure SetupColumns; override;
    procedure SetGridEvents; override;
  public
    MyFirstTaskReminder: TFirstTaskReminder; //QM-494 First Task EB
    property OnSelectTicket: TItemSelectedEvent read FOnSelectTicket write FOnSelectTicket;
    procedure TicketMap;  //qm-744 sr
    procedure SortGridByFields; override;
    function ExportMyItems: string; override;
    procedure SetAllExportBoxes(Checked: Boolean); override;
    procedure RefreshData; override;
    procedure RefreshCaptionCounts; override;
    procedure ChangeViewEmergencies(FilterOn: Boolean);
    procedure InitFT; //QM-494 First Task EB
    procedure FreeFT; //QM-494 First Task EB
    procedure CheckFirstTask; //QM-494 First Task EB
  end;

var
  MyWorkTicketsFrame: TMyWorkTicketsFrame;

implementation

{$R *.dfm}

uses OdHourglass, OdDBUtils, OdMiscUtils, OdCxUtils, OdDBISAMUtils, DateUtils,
  OdExceptions, StartTimeDialog, SharedDevExStyles, xqGeoFunctions, LocalPermissionsDMu,
   ShellAPI;

const
  NoStatus = '---';
  TicketSortColumns: array[0..4] of string = ('route_order', 'work_priority', 'first_task', 'due_date', 'ticket_number');  //QMANTWO-775 EB
  TicketSortDirections: array[0..4] of TcxGridSortOrder = (soAscending, soDescending, soDescending, soAscending, soAscending);

  SelectMyTickets = 'select ticket.ticket_id, ticket_number, kind, due_date, ticket_type, ' +
    'company, priority, work_type, work_city, work_address_street, work_county, ' +
    'work_description, legal_good_thru, map_page, ticket_format, ' +
    'TRIM(BOTH ''-'' FROM (CONCAT(work_address_number + ''-'', work_address_number_2))) as work_address_number, ' +   //QM-997 EB
   // 'work_address_number, ' +
    'do_not_mark_before, work_date, LocalStringKey, alert, ' +
    'coalesce(ref.sortby, 0) work_priority, coalesce(ref.description, ''Normal'') priority_disp, ' +
    'work_lat, work_long, work_state, '+        //QMANTWO-504 & 515
    //return first_task = true for any active task that has not been performed
    'if(task.est_start_date is null, False, True) first_task ' +
    ', False export, ''---'' quick_close, cast(workload_date as date) as workload_date ' +
    ', route_order, risk_score, est_start_date, expiration_date, alt_due_date ' +  //QMANTWO-775 EB   //QM-10000 EB Expiration Date Coloring
//     'select coalesce(risk_score, ''--'') as risk_score,ticket_version_id ' +
//        'from ticket_risk ' +
//         'where (ticket_id = 9470)  ' +
//         'order by ticket_version_id desc top 1 ' +
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'left join task_schedule task on (task.ticket_id=ticket.ticket_id and task.active=True ' +
    'and task.est_start_date >= :start_date_before and task.est_start_date < :start_date_after and task.performed_date is null) ' +

    'where locate.locator_id=:emp_id ' +
    'and locate.active ' +
    'and NOT locate.closed ' +
    'and ((do_not_mark_before is NULL) ' +
    'or (do_not_mark_before <= :last_sync)) ' +
    '%s ' + // placeholder for 'ticket.ticket_id in ()' criteria
    '%s ' + // placeholder for DueDateCriteria
    '%s ' + // placeholder for WorkloadDateCriteria
    'group by ticket_id, ticket_number, kind, due_date, ticket_type, company, ' +
    'priority, work_type, work_city, work_address_street, work_county, ' +
    'legal_good_thru, map_page, ticket_format, ' +
    'work_address_number, do_not_mark_before, work_priority, work_lat, work_long ';

    SelOrderBy = ' order by work_priority desc, first_task, due_date, ticket_number';  //QM-896 EB ROute Order Limit
    SelOrderByRO = ' order by route_order, due_date, ticket_number';


procedure TMyWorkTicketsFrame.RefreshCaptionCounts;
var
  OpenTicketCount: Integer;
begin
  inherited;
  OpenTicketCount := DM.nEmergency + DM.nNew + DM.nOngoing;
  CountLabelSummary.Caption := IntToStr(OpenTicketCount);
  CountLabelDataSummary.Caption := IntToStr(OpenTicketCount);
end;

procedure TMyWorkTicketsFrame.SetupColumns;
begin
  SetupPriorityDisplay;
  if Assigned(OpenTickets.FindField('LocalStringKey')) then
    OpenTickets.FieldByName('LocalStringKey').OnSetText := RouteOnSetText;
  UpdateQuickCloseStatuses;
  TktDColFirstTask.Properties.OnChange := ColFirstTaskChange;
  TktSColFirstTask.Properties.OnChange := ColFirstTaskChange;
  TktDColExport.Visible := PermissionsDM.CanI(RightTicketsSelectiveExport);
  TktSColExport.Visible := PermissionsDM.CanI(RightTicketsSelectiveExport);
  TktSColRouteOrder.Visible := PermissionsDM.CanI(RightTicketsUseRouteOrder); //QMANTWO-775 EB
  TktDColRouteOrder.Visible := PermissionsDM.CanI(RightTicketsUseRouteOrder); //QMANTWO-775 EB
  TktSColTicketDist.Visible := not PermissionsDM.CanI(RightTicketsUseRouteOrder); //QMANTWO-775 EB hide if Route Order visible
  TktDColTicketDist.Visible := not PermissionsDM.CanI(RightTicketsUseRouteOrder); //QMANTWO-775 EB hide if Route Order visible
  SortGridByFieldNames(BaseGrid, TicketSortColumns, TicketSortDirections);
  ExpandAll;
end;

procedure TMyWorkTicketsFrame.ExpandAll;
begin
  PostMessage(Self.Handle, UM_EXPANDALL, 0, 0);
end;

procedure TMyWorkTicketsFrame.UMExpandAll(var Message: TMessage);
begin
  FullExpandGrid(BaseGrid);
  if BaseGrid.CanFocus then
    BaseGrid.SetFocus;
//  MyWorkForm.btnTicketMapClick(nil);  //qm-744  sr
end;

procedure TMyWorkTicketsFrame.SetupPriorityDisplay;
  procedure SetupPriorityColumn(Column: TcxGridColumn);
  begin
    Column.OnGetDisplayText := ColWorkPriorityGetDisplayText;
    if (Column.PropertiesClass = TcxTextEditProperties) then
      TcxTextEditProperties(Column.Properties).Alignment.Horz := taLeftJustify;
  end;
begin
  SetupPriorityColumn(TktDColWorkPriority);
  SetupPriorityColumn(TktSColWorkPriority);
end;



procedure TMyWorkTicketsFrame.RouteOnSetText(Sender: TField; const Text: string);
begin
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeRouteTicket, Sender.DataSet.FieldByName('ticket_id').AsString);
  SetRoute.ParamByName('Route').AsString := Text;
  SetRoute.ParamByName('TicketID').AsInteger := Sender.DataSet.FieldByName('ticket_id').AsInteger;
  SetRoute.ExecSQL;
  Sender.AsString := Text;
end;

procedure TMyWorkTicketsFrame.UpdateQuickCloseStatuses;
var
  TicketFormat: string;
  QuickCloseStatusList: TStringList;
begin
  QuickCloseStatusList := TStringList.Create;
  try
    if OpenTickets.IsEmpty then begin
      CxComboBoxItems(TktDColQuickClose).Clear;
      CxComboBoxItems(TktSColQuickClose).Clear;
      FCurrentTicketFormat := '';
    end
    else begin
      TicketFormat := OpenTickets.FieldByName('ticket_format').AsString;
      if FCurrentTicketFormat <> TicketFormat then begin
        QuickCloseStatusList.Clear;
        FCurrentTicketFormat := TicketFormat;
        DM.GetQuickCloseStatuses(FCurrentTicketFormat, QuickCloseStatusList);
        DM.ApplyEmployeeStatusLimit(QuickCloseStatusList);
        QuickCloseStatusList.Insert(0, NoStatus);
        CxComboBoxItems(TktDColQuickClose).Assign(QuickCloseStatusList);
        CxComboBoxItems(TktSColQuickClose).Assign(QuickCloseStatusList);
      end;
    end;
  finally
    FreeAndNil(QuickCloseStatusList);
  end;
end;

procedure TMyWorkTicketsFrame.ColFirstTaskChange(Sender: TObject);
var
  CurrentTicketID: Integer;
  TaskedTicketID: Integer;
  EstStartDateTime: TDateTime;
  MarkedAsFirstTask: Boolean;
begin
  Assert(Assigned(Sender) and (Sender is TcxCheckBox));

  MarkedAsFirstTask := False;
  if FocusedGridRecord <> nil then begin
    if (not VarIsNull(FocusedGridRecord.Values[FirstTaskColumn.Index])) then
      MarkedAsFirstTask := (FocusedGridRecord.Values[FirstTaskColumn.Index] = True)
  end;
  if MarkedAsFirstTask then begin
    CancelDataSet(OpenTickets);
    raise EOdDataEntryError.Create('You cannot change the ticket marked as tomorrow''s first task.');
  end;

  if not MarkedAsFirstTask then begin
    if DM.FirstTicketTaskedForTomorrow(TaskedTicketID) then begin
      CancelDataSet(OpenTickets);
      raise EOdDataEntryError.Create('There is already a ticket marked as tomorrow''s first task.');
    end;
    if not GetCurrentItemID(CurrentTicketID) or (CurrentTicketID < 0) then begin
      CancelDataSet(OpenTickets);
      raise Exception.Create('Selected ticket cannot be marked as tomorrow''s first task. Sync and try again.');
    end;
    if not PromptForTaskStartTime(EstStartDateTime) then begin
      CancelDataSet(OpenTickets);
      Abort;
    end;
    SetTicketEstimatedStartTime(CurrentTicketID, EstStartDateTime);
     lblFirstTask.Visible := False;  //QM-494 First Task Reminder
  end;
end;

procedure TMyWorkTicketsFrame.ColWorkPriorityGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: String);
var
  DescCol : TcxGridColumn;
begin
  inherited;
  Assert(Assigned(Sender) and (Sender is TcxGridColumn));
  if (ARecord is TcxGridGroupRow) then
    Exit;

  if (Sender as TcxGridColumn) = TktSColWorkPriority then
    DescCol := TktSColWorkPriorityDescription
  else
    DescCol := TktDColWorkPriorityDescription;
  if (not VarIsNull(ARecord.Values[DescCol.Index])) then
    AText := ARecord.Values[DescCol.Index];
end;

function TMyWorkTicketsFrame.ItemIDColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColTicketID
  else
    Result := TktSColTicketID;
end;

function TMyWorkTicketsFrame.QuickCloseColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColQuickClose
  else
    Result := TktSColQuickClose;
end;

function TMyWorkTicketsFrame.TicketStatusColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColTicketStatus
  else
    Result := TktSColTicketStatus;
end;

function TMyWorkTicketsFrame.FirstTaskColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColFirstTask
  else
    Result := TktSColFirstTask;
end;





procedure TMyWorkTicketsFrame.SetTicketEstimatedStartTime(const TicketID: Integer; const StartTime: TDateTime);
begin
  DM.AddTicketToTaskSchedule(DM.EmpID, TicketID, StartTime);
  TicketsGridDetailsTableView.BeginUpdate;
  TicketsGridSummaryTableView.BeginUpdate;
  RefreshDataSet(OpenTickets);
  TicketsGridDetailsTableView.EndUpdate;
  TicketsGridSummaryTableView.EndUpdate;
end;

function TMyWorkTicketsFrame.ExportMyItems: string;
const
  SelectMyTicketsExport = 'select distinct ticket_number "Ticket #", ' +
    'work_lat "Lat", work_long "Long", due_date, ' +
    'do_not_mark_before, coalesce(ref.sortby, 0) "work_priority", ' +
    'TRIM(LEADING '' '' FROM (COALESCE(work_address_number, '''')+' +
    ''' ''+work_address_street)) "Name", ' +
    'CAST(due_date AS varchar(16)) "Name2", ' +
    'work_address_number "Address", work_address_street "Street", ' +
    'work_city "City", work_state "State", ' +
    'work_county "County", ' +  //QM-284 EB Added back in
    'route_order, kind, ticket_number ' +  //QM-51 EB
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'where locate.locator_id = %d ' +
    'and locate.active = True ' +
    'and locate.closed = False ' +
    'and (do_not_mark_before is NULL or do_not_mark_before <= %s) ' +
    '%s '; // Placeholder for 'ticket.ticket_id in ()' criteria

    OrderBy = 'order by work_priority desc, due_date, ticket_number';     //QM-51 Eb Updated to handle Ro and absense of RO
    OrderByRO = 'order by route_order';
var
  Wait: IInterface;
  ExportFileName: string;
  TicketViewLimit: Integer;
  TicketIDCriteria: string;
  VisibleOrSelectedTicketIDs: string;
  TicketsToExport: TDataSet;
  Select: string;
  TicketExportCount: Integer;
  CanSelectTicketsToExport: Boolean;
  UseRouteOrder: Boolean;
  GPXOptions: TGPXOptions;
begin
  Result := '';
  Wait := ShowHourGlass;
  TicketViewLimit := DM.MyTicketViewLimit;
  TicketIDCriteria := '';
  CanSelectTicketsToExport := PermissionsDM.CanI(RightTicketsSelectiveExport);
  GPXOptions.IncludeRouteOrder := PermissionsDM.CanI(RightTicketsUseRouteOrder);
  GPXOptions.IncludeEmpName := False;
  if CanSelectTicketsToExport then // export only selected tickets
    VisibleOrSelectedTicketIDs := GetItemIDsForExportString(OpenTickets, 'ticket_id', SelectedItemID)
  else if TicketViewLimit > 0 then // export only viewable tickets
    VisibleOrSelectedTicketIDs := GetLimitedTicketIDsString(TicketViewLimit)
  else                             // export all assigned tickets
    VisibleOrSelectedTicketIDs := '';

  if CanSelectTicketsToExport and IsEmpty(VisibleOrSelectedTicketIDs) then begin // nothing to export
    Exit;
  end;

  if NotEmpty(VisibleOrSelectedTicketIDs) then
    TicketIDCriteria := 'and ticket.ticket_id in (' + VisibleOrSelectedTicketIDs + ') ';

  Select := Format(SelectMyTicketsExport, [DM.EmpID,
  DateToDBISAMDate(DM.UQState.LastLocalSyncTime), TicketIDCriteria]);

  if UseRouteOrder then
    Select := Select + OrderByRO
  else
    Select := Select + OrderBy;

  TicketsToExport := DM.Engine.OpenQuery(Select, 'MyTickets');
  try
    TicketExportCount := TicketsToExport.RecordCount;
    ExportFileName := DM.CreateExportFileName('-My Tickets', '.gpx');
    DM.SaveGpxToFile(TicketsToExport, ExportFileName, GPXOptions );
    Result := ExportFileName;
  finally
    FreeAndNil(TicketsToExport);
  end;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketRouteExport, IntToStr(TicketExportCount));
end;


function TMyWorkTicketsFrame.GetLimitedTicketIDsString(Limit: Integer): string;
var
  List: TStringList;
begin
  Result := '';
  ROLimitCnt:=0; //qm-902 sr
  List := TStringList.Create;
  try
    DM.GetLimitedTicketIdList(List, Limit);
    ROLimitCnt:=Limit; //qm-902 sr
    Result := GetSQLInClauseForStrings(List, False);
  finally
    FreeAndNil(List);
  end;
end;

procedure TMyWorkTicketsFrame.RefreshTickets;
var
  lSQL: string;   //QM-896 EB Route Order Limit

  procedure SetMyTicketsSelectStatement;
  var
    TicketViewLimit: Integer;
    TicketIDCriteria: string;
    DueDateCriteria : string;
    WorkloadDateCriteria: string;

  begin
    TicketViewLimit := DM.MyTicketViewLimit;
    OpenTickets.Close;
    if PermissionsDM.CanI(RightTicketsUseRouteOrder) then  //QM-896 Route Order Limit
    begin
      lSQL := SelectMyTickets + SelOrderbyRO;
      ROLimited:=true;
    end
    else
      lSQL := SelectMyTickets + SelOrderBy;


    if PermissionsDM.CanI(RightTicketsShowFutureWorkload) then begin
      WorkloadDateCriteria := '';
      if DM.ShowFutureWorkForLocator then
        DueDateCriteria := ''
      else
        DueDateCriteria := ' and ((due_date < ' + DateToDBISAMDate(Date + 1) +
          ') or (due_date is null))'; //include today; i.e. any less than tomorrow
    end
    else begin
      WorkloadDateCriteria := ' and ((workload_date < ' +
        DateToDBISAMDate(Date + 1) + ') or (workload_date is null))';
      DueDateCriteria := '';
    end;

    if TicketViewLimit > 0 then begin
      TicketIDCriteria := GetLimitedTicketIDsString(TicketViewLimit);
      if NotEmpty(TicketIDCriteria) then
        TicketIDCriteria := 'and ticket.ticket_id in (' + TicketIDCriteria + ') ';
      OpenTickets.SQL.Text := Format(lSQL, [TicketIDCriteria,              //QM-896 Route Order Limit,  QMANTWO-775 EB
        DueDateCriteria, WorkloadDateCriteria]);
    end
    else
      OpenTickets.SQL.Text := Format(lSQL, ['', DueDateCriteria,           //QM-896 Route Order Limit,  QMANTWO-775 EB
        WorkloadDateCriteria]);

  end;

var
  Cursor: IInterface;
begin
  ROLimited:=False;
  Cursor := ShowHourGlass;
  OpenTickets.DisableControls;
  try
    SetMyTicketsSelectStatement;
    OpenTickets.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenTickets.ParamByName('last_sync').AsDateTime := DM.UQState.LastLocalSyncTime;
    OpenTickets.ParamByName('start_date_before').AsDateTime := Today;
    OpenTickets.ParamByName('start_date_after').AsDateTime := Tomorrow + 3;
    OpenTickets.Open;
    SetFieldDisplayFormat(OpenTickets, 'work_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenTickets, 'due_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenTickets, 'workload_date', 'mmm d');
    TicketLocates.Close;
    RefreshDataSet(CenterTicketSummary);
  finally
    OpenTickets.EnableControls;
  end;

  SetupColumns;
  CheckFirstTask;
  ExpandAll;
  if dm.RequireTktStrMap then TicketMap; //qm-744  777 sr
end;

procedure TMyWorkTicketsFrame.TicketMap;  //qm-744 sr
var
  EmpIDStr:string;
  Parameters : string;
begin
  inherited;
    EmpIDStr := IntToStr(DM.EmpID);
    if ROLimited then  //qm-902 sr
    Parameters := EmpIDStr +' '+'ROLimited'+' '+IntToStr(ROLimitCnt)  //qm-902 sr
    else
    Parameters := EmpIDStr;
    begin
      ShellExecute(handle, 'open',PChar('StreetMapProj.exe'),PChar(parameters), nil, SW_SHOWNORMAL);
    end;
end;

procedure TMyWorkTicketsFrame.ApplyButtonClick(Sender: TObject);
var
  Cursor: IInterface;
  TicketsView : TcxCustomGridTableView;
  I: Integer;
  QuickCloseStatus: String;
  TicketID: Integer;
begin
  Cursor := ShowHourGlass;

  TicketsView := TcxCustomGridTableView(BaseGrid.FocusedView);
  for I := 0 to TicketsView.DataController.RecordCount -1 do begin
    if VarIsNull(TicketsView.DataController.Values[I, QuickCloseColumn.Index]) then
      QuickCloseStatus := NoStatus
    else
      QuickCloseStatus := TicketsView.DataController.Values[I, QuickCloseColumn.Index];
    if VarIsNull(TicketsView.DataController.Values[I, ItemIDColumn.Index]) then
      TicketID := InvalidID
    else
      TicketID := TicketsView.DataController.Values[I, ItemIDColumn.Index];

    if IsValidStatus(QuickCloseStatus) then begin
      DM.QuickClose(TicketID, QuickCloseStatus);
      SetRecordValue(TicketsView, I, QuickCloseColumn.Index, NoStatus);
    end;
    DM.SyncNow;
  end;
  RefreshTickets;
end;


procedure TMyWorkTicketsFrame.InitFT; //QM-494 First Task Reminder EB
const
  UseINIOnly = True;
begin
  if not assigned(MyFirstTaskReminder) then
    MyFirstTaskReminder := TFirstTaskReminder.Create(DM.EmpID, UseINIOnly);
end;

procedure TMyWorkTicketsFrame.FreeFT;  //QM-494 First Task Reminder EB
begin
  FreeAndNil(MyFirstTaskReminder);
end;

function TMyWorkTicketsFrame.IsValidStatus(const Status: string): Boolean;
begin
  Result := True;
  if (Trim(Status) = '') or (Status = NoStatus) then
    Result := False;
end;

procedure TMyWorkTicketsFrame.OpenTicketsAfterScroll(DataSet: TDataSet);  //QMANTWO-646
begin   //QMANTWO-646
  inherited;
  if Assigned(FOnSelectTicket) then
    FOnSelectTicket(Self, DataSet.FieldByName('ticket_id').AsInteger);
  UpdateQuickCloseStatuses;
end;

procedure TMyWorkTicketsFrame.ChangeViewEmergencies(FilterOn: Boolean);
begin

  OpenTickets.Filtered := FilterOn;
end;

procedure TMyWorkTicketsFrame.CheckFirstTask;
var
  FT: TDateTime;
  ShowIt: Boolean;
  FT_TicketID: integer;
begin
  Showit := False;
  if not assigned(MyFirstTaskReminder) then begin
    ShowIt := False;
    Exit;
  end;
  try
    If (MyFirstTaskReminder.Required = False) then
      ShowIt := False
    else if (MyFirstTaskReminder.EstStartDate > (Date + 1)) then
      ShowIt := False
    else if DM.FirstTicketTaskedForTomorrow(FT_TicketID) then  {If set, don't show}
      ShowIt := False
    else if (Now < MyFirstTaskReminder.ReminderDate) then {Don't nag them yet}
      ShowIt := False
    else
      ShowIt := True;

    If ShowIt then begin
      ShowIt := True;
      lblFirstTask.Caption := 'Select First Task';
      If MyFirstTaskReminder.Debug then
        lblFirstTask.Caption := lblFirstTask.Caption +
                               ' ( ' + DateTimeToStr(MyFirstTaskReminder.ReminderDate) + ' )';
    end;
  finally
   lblFirstTask.Visible := ShowIt;
  end;
end;

procedure TMyWorkTicketsFrame.ClearButtonClick(Sender: TObject);
begin
  inherited;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeRouteTicket, 'Clear route');
  ClearRoutes.ExecSQL;
  RefreshTickets;
end;

procedure TMyWorkTicketsFrame.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  TicketStatus: TTicketStatus;
  TicketQuickClose: String;
  TTStr, DStr: string;
  ExpDate: TDateTime;     //QM-991 EB Expiration Date coloring
  TicketExpired: Boolean; //QM-991 EB Expiration Date coloring
begin
  inherited;
  DStr := '';
  if (AViewInfo.GridRecord is TcxGridGroupRow) then
    Exit;

  {Quick Close - bold}
  if VarIsNull(AViewInfo.GridRecord.Values[QuickCloseColumn.Index]) then
    TicketQuickClose := NoStatus
  else
    TicketQuickClose := AViewInfo.GridRecord.Values[QuickCloseColumn.Index];
  if IsValidStatus(TicketQuickClose) then
    ACanvas.Font.Style := [fsBold];

  {Row Not Selected}
  if not AViewInfo.Selected then begin
    if not VarIsNull(AViewInfo.GridRecord.Values[TicketStatusColumn.Index]) then
      TicketStatus := TTicketStatus(word(AViewInfo.GridRecord.Values[TicketStatusColumn.Index])) //QMANTWO-503
    else
      TicketStatus := [];
    ACanvas.Brush.Color := GetTicketColor(TicketStatus,
      (AViewInfo.GridRecord.Index mod 2) = 1);
  end;

  {Alerts - Detail}
  if TicketsGridDetailsTableView.Visible then begin
    if AViewInfo.Item.Index = TktDColAlert.Index then begin         //QM-62 EB Alert flag
      {Alert field - has to be after status so it is not overwritten}
      if ((AViewInfo.GridRecord.Values[TktDColAlert.Index]) = '!') then begin
        ACanvas.Brush.Color := cl9SofterYellow;
        ACanvas.Font.Color := clRed;
        ACanvas.Font.Style := [fsBold];
      end;
    end;
  end;

  {Alerts - Summary}
  if TicketsGridSummaryTableView.Visible then begin
    if (AViewInfo.Item.Index = TktSColAlert.Index) then begin         //QM-62 EB Alert flag
      {Alert field - has to be after status so it is not overwritten}
      DStr:= AViewInfo.GridRecord.Values[TktSColAlert.Index];
      if ((AViewInfo.GridRecord.Values[TktSColAlert.Index]) = '!') then begin
        ACanvas.Brush.Color := cl9SofterYellow;
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
      end;
    end;
   end;


  {Due Date - Detail  (CA)}
  if TicketsGridDetailsTableView.Visible then begin
    if (AViewInfo.Item.Index = TktDColDueDate.Index) then begin
      TTStr := AViewInfo.GridRecord.Values[TktDColTicketType.Index];
      If (AnsiPos('-RS', TTStr) > 0) then begin
        ACanvas.Brush.Color := cl9SofterYellow;
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
      end
      {OCC2}
      else if (AnsiPos('-ET', TTStr) > 0) then begin
        ACanvas.Brush.Color := clWebDarkgreen;
        ACanvas.Font.Color := cl9SofterYellow;
        ACanvas.Font.Style := [fsBold];
      end;
    end;
  end;

  {Due Date - Summary (CA)}
  if TicketsGridSummaryTableView.Visible then begin
    if (AViewInfo.Item.Index = TktSColDueDate.Index) then begin
      TTStr := AViewInfo.GridRecord.Values[TktSColTicketType.Index];
      If (AnsiPos('-RS', TTStr) > 0) then begin
        ACanvas.Brush.Color := cl9SofterYellow;
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
      end
      {OCC2}
      else if (AnsiPos('-ET', TTStr) > 0) then begin
        ACanvas.Brush.Color := clWebDarkgreen;
        ACanvas.Font.Color := cl9SofterYellow;
        ACanvas.Font.Style := [fsBold];
      end;
    end;
  end;

  {Expiration Date ROW - Summary}
  {Putting this separate from Ticket status color for now. It will override}
  if TicketsGridSummaryTableView.Visible then begin
    if not VarIsNull(AViewInfo.GridRecord.Values[TickSColExpDate.Index]) then begin
      If (AViewInfo.GridRecord.Values[TickSColExpDate.Index] < Now) then begin
        ACanvas.Brush.Color := clWebSaddleBrown;
        ACanvas.Font.Color := clCream;
      end;
    end;
  end;

   {Expiration Date ROW - Detail}
  if TicketsGridDetailsTableView.Visible then begin
    if not VarIsNull(AViewInfo.GridRecord.Values[TickDColExpDate.Index]) then begin
      If (AViewInfo.GridRecord.Values[TickDColExpDate.Index] < Now) then begin
        ACanvas.Brush.Color := clWebSaddleBrown;
        ACanvas.Font.Color := clCream;
      end;
    end;
  end;
  ADone := False;
end;

procedure TMyWorkTicketsFrame.OpenTicketsCalcFields(DataSet: TDataSet);
var
  Alert2: string;
    Tktlat,Tktlng, gpsLat, gpsLng : extended;  //QMANTWO-504
  DistanceStr: string;
begin
  inherited;

  Alert2 := '';
  if DataSet.FieldByName('alert').AsString = 'A' then
    Alert2 := '!';

  DataSet.FieldByName('alert2').AsString := Alert2;
  DataSet.FieldByName('ticket_status').AsInteger := word(GetTicketStatusFromRecord(DataSet)); //QMANTWO-503
  DataSet.FieldByName('locates').AsString := GetLocatesStringForTicket(TicketLocates,
    DataSet.FieldByName('ticket_id').AsInteger);

  gpsLat := 0.0;     //QMANTWO-504
  gpsLng := 0.0;     //QMANTWO-504
  Tktlat := 0.0;     //QMANTWO-504
  Tktlng  := 0.0;    //QMANTWO-504
  if ((DM.CurrentGPSLongitude <> 0) and (DM.CurrentGPSLatitude <> 0)) then  //QMANTWO-504
  begin  //QMANTWO-504
    gpsLat  :=  DM.CurrentGPSLatitude;    //QMANTWO-504
    gpsLng  := ((abs(DM.CurrentGPSLongitude))*-1);  //QMANTWO-504

    Tktlat  :=  DataSet.FieldByName('work_lat').AsFloat;   //QMANTWO-504
    Tktlng  :=  ((abs(DataSet.FieldByName('work_long').AsFloat))*-1);  //QMANTWO-504
    {the above correction is necessary due to the inconsistant and WRONG format that some call centers
    send in Lat/Lng.  }
    {QMANTWO-585 EB  format as string and then assign back as float.
      There was a problem with the distance column not sorting properly.  Besides
      changing properties to float type, this seemed to help}
    if ((TktLat <> 0) and (TktLng <> 0)) then begin  //QMANTWO-585 EB prevent calc for bad ticket lat/lng
      DistanceStr := formatfloat('0.00',(xqGeoFunctions.calculateDistance(Tktlat, gpsLat, Tktlng, gpsLng, tdMiles)));  //QMANTWO-504
      DataSet.FieldByName('tktDistance').asFloat := strtofloat(DistanceStr); //QMANTWO-585 EB
    end;
  end;            
end;

procedure TMyWorkTicketsFrame.OpenTicketsBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddCalculatedField('alert2', DataSet, TStringField, 10);
  AddCalculatedField('locates', DataSet, TStringField, 100);
  AddCalculatedField('ticket_status', DataSet, TIntegerField, 0);
  AddCalculatedField('tktDistance', DataSet, TFloatField, 0);   //QMANTWO-504  EB-QMANTWO-585
  end;

procedure TMyWorkTicketsFrame.SortGridByFields;
begin
  inherited;
  SortGridByFieldNames(BaseGrid, TicketSortColumns, TicketSortDirections);
end;

procedure TMyWorkTicketsFrame.SetAllExportBoxes(Checked: Boolean);
begin
  inherited;
  if FViewIndex = 0 then
    SetExportAll(OpenTickets, TicketsGridDetailsTableView, Checked)
  else
    SetExportAll(OpenTickets, TicketsGridSummaryTableView, Checked);
end;

procedure TMyWorkTicketsFrame.SetRecordValue(AView: TcxCustomGridTableView;
  ARecordIndex, AItemIndex: Integer; AValue: Variant);
begin
  AView.DataController.SetValue(ARecordIndex, AItemIndex, AValue);
end;

procedure TMyWorkTicketsFrame.RefreshData;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  RefreshTickets;
end;

procedure TMyWorkTicketsFrame.SetGridEvents;
  procedure SetViewEvents(View: TcxGridDBBandedTableView);
  begin
    View.DataController.OnCompare := GridCompare;
    View.OnCustomDrawCell := GridViewCustomDrawCell;
    View.OnDblClick := GridDblClick;
  end;
begin
  inherited;
  SetViewEvents(TicketsGridDetailsTableView);
  SetViewEvents(TicketsGridSummaryTableView);
end;

end.
