inherited ReportForm: TReportForm
  Left = 147
  Top = 126
  Caption = 'Reports'
  ClientHeight = 592
  ClientWidth = 818
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ReportStatusPanel: TPanel
    Left = 0
    Top = 381
    Width = 818
    Height = 188
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    Visible = False
    object ReportQueueGrid: TDBGrid
      Left = 5
      Top = 30
      Width = 808
      Height = 153
      Align = alClient
      DataSource = ReportQueueDS
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = ReportQueueGridDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'report_name'
          Title.Caption = 'Report Name'
          Width = 142
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'request_date'
          Title.Caption = 'Request Date'
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DisplayStatus'
          Title.Caption = 'Status'
          Width = 145
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Elapsed'
          Title.Caption = 'Elapsed Time'
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 5
      Top = 5
      Width = 808
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 370
        Height = 13
        Caption = 
          'Running and Completed Reports:     (Double-click to view a compl' +
          'eted report)'
      end
    end
  end
  object UpperPanel: TPanel
    Left = 0
    Top = 0
    Width = 818
    Height = 381
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 183
      Top = 0
      Width = 9
      Height = 381
      Beveled = True
    end
    object ReportSelectionPanel: TPanel
      Left = 0
      Top = 0
      Width = 183
      Height = 381
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        183
        381)
      object ReportListLabel: TLabel
        Left = 8
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Reports:'
      end
      object ReportList: TListBox
        Left = 8
        Top = 24
        Width = 167
        Height = 355
        Anchors = [akLeft, akTop, akRight, akBottom]
        ItemHeight = 13
        TabOrder = 0
        OnClick = ReportListClick
      end
    end
    object ReportPanel: TPanel
      Left = 192
      Top = 0
      Width = 626
      Height = 381
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 8
      TabOrder = 1
      object RepConfigPanel: TPanel
        Left = 8
        Top = 39
        Width = 610
        Height = 334
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
      end
      object ReportButtonPanel: TPanel
        Left = 8
        Top = 8
        Width = 610
        Height = 31
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object ReportNameLabel: TLabel
          Left = 0
          Top = 5
          Width = 28
          Height = 13
          Caption = '-------'
        end
        object PreviewBtn: TButton
          Left = 206
          Top = 0
          Width = 86
          Height = 25
          Caption = 'Print Preview'
          Enabled = False
          TabOrder = 0
          OnClick = PreviewBtnClick
        end
        object ExportButton: TButton
          Left = 299
          Top = 0
          Width = 86
          Height = 25
          Caption = 'Export...'
          TabOrder = 1
          OnClick = ExportButtonClick
        end
        object CopyConfigButton: TButton
          Left = 393
          Top = 0
          Width = 86
          Height = 25
          Caption = 'Config String'
          TabOrder = 2
          Visible = False
          OnClick = CopyConfigButtonClick
        end
      end
    end
  end
  object ReportFooterPanel: TPanel
    Left = 0
    Top = 569
    Width = 818
    Height = 23
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 2
    Visible = False
    object ReportTimeLabel: TLabel
      Left = 802
      Top = 4
      Width = 12
      Height = 15
      Align = alRight
      Caption = '---'
    end
  end
  object ReportQueueDataSet: TDBISAMTable
    OnCalcFields = ReportQueueDataSetCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'report_log'
    Left = 504
    Top = 456
    object ReportQueueDataSetrequest_id: TStringField
      FieldName = 'request_id'
      Size = 50
    end
    object ReportQueueDataSetreport_name: TStringField
      FieldName = 'report_name'
      Size = 30
    end
    object ReportQueueDataSetrequest_date: TDateTimeField
      FieldName = 'request_date'
      DisplayFormat = 'mmm d, h:nn:ss am/pm'
    end
    object ReportQueueDataSetstatus: TStringField
      FieldName = 'status'
      Size = 15
    end
    object ReportQueueDataSetupdate_count: TIntegerField
      FieldName = 'update_count'
    end
    object ReportQueueDataSetnext_update: TDateTimeField
      FieldName = 'next_update'
    end
    object ReportQueueDataSetElapsed: TStringField
      FieldKind = fkCalculated
      FieldName = 'Elapsed'
      Calculated = True
    end
    object ReportQueueDataSetDisplayStatus: TStringField
      FieldKind = fkCalculated
      FieldName = 'DisplayStatus'
      Size = 40
      Calculated = True
    end
  end
  object ReportQueueDS: TDataSource
    DataSet = ReportQueueDataSet
    Left = 552
    Top = 456
  end
  object StatusCheckTimer: TTimer
    Interval = 2000
    OnTimer = StatusCheckTimerTimer
    Left = 368
    Top = 456
  end
end
