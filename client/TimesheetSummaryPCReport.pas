unit TimesheetSummaryPCReport;

interface

// UQ does not (yet) want a client screen for this, so it is not used (yet)

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, OdRangeSelect, StdCtrls, CheckLst;

type
  TTimesheetSummaryPCReportForm = class(TReportBaseForm)
    OdRangeSelectFrame1: TOdRangeSelectFrame;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckListBox1: TCheckListBox;
    Label2: TLabel;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

procedure TTimesheetSummaryPCReportForm.ValidateParams;
begin
  inherited;
  SetReportID('ProductionHoursByPC');
  {
  IncludeCallouts
  StartDate
  EndDate
  ApproveOnly
  FinalOnly
  EmpTypes
  }
end;

procedure TTimesheetSummaryPCReportForm.InitReportControls;
begin
  inherited;
  //RangeSelect.SelectDateRange(OdRangeSelect.rsThisWeek);
  // etc....
end;

end.
