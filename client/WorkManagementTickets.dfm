inherited WorkMgtTicketsFrame: TWorkMgtTicketsFrame
  inherited SummaryPanel: TPanel
    Caption = 'Tickets'
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Tickets'
      DesignSize = (
        795
        31)
      object lblAllCounts: TLabel
        Left = 52
        Top = 9
        Width = 48
        Height = 13
        Caption = 'All Counts'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object btnOptimizeRO: TcxButton
        Left = 701
        Top = 0
        Width = 53
        Height = 30
        Anchors = [akTop, akRight]
        Caption = 'Optimize Route'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        WordWrap = True
        OnClick = cxButton1Click
      end
      object ExportButton: TcxButton
        Left = 753
        Top = 0
        Width = 42
        Height = 30
        Anchors = [akTop, akRight]
        Caption = 'Export'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = ExportButtonClick
      end
      object btnBulkDueDate: TcxButton
        Left = 634
        Top = 0
        Width = 68
        Height = 30
        Anchors = [akTop, akRight]
        Caption = 'Change Sel Due Dates'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        WordWrap = True
        OnClick = btnBulkDueDateClick
        Colors.Normal = clGradientActiveCaption
        Colors.Disabled = clBtnFace
        Colors.DisabledText = clBtnShadow
      end
    end
    inherited BaseGrid: TcxGrid
      Height = 139
      Enabled = False
      PopupMenu = PriorityPopup
      OnFocusedViewChanged = BaseGridFocusedViewChanged
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = TicketView
      end
    end
    inherited PanelDisplaySelectedItem: TPanel
      Top = 170
      Height = 269
      DesignSize = (
        795
        269)
      object Label2: TLabel
        Left = 3
        Top = 9
        Width = 43
        Height = 13
        Caption = 'Ticket #:'
      end
      object Label4: TLabel
        Left = 3
        Top = 24
        Width = 35
        Height = 13
        Caption = 'Status:'
      end
      object Label7: TLabel
        Left = 3
        Top = 40
        Width = 56
        Height = 13
        Caption = 'Work Type:'
      end
      object Label8: TLabel
        Left = 3
        Top = 111
        Width = 43
        Height = 13
        Caption = 'Address:'
      end
      object ticket_number: TDBText
        Left = 63
        Top = 9
        Width = 84
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'ticket_number'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object kind: TDBText
        Left = 202
        Top = 9
        Width = 24
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'kind'
        DataSource = TicketDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object status: TDBText
        Left = 63
        Top = 24
        Width = 36
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'status'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object work_address_number: TDBText
        Left = 63
        Top = 111
        Width = 49
        Height = 13
        Color = clBtnFace
        DataField = 'work_address_number'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object work_address_street: TDBText
        Left = 123
        Top = 111
        Width = 122
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_address_street'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label11: TLabel
        Left = 311
        Top = 9
        Width = 23
        Height = 13
        Caption = 'Grid:'
      end
      object map_page: TDBText
        Left = 379
        Top = 9
        Width = 60
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'map_page'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label3: TLabel
        Left = 311
        Top = 40
        Width = 48
        Height = 13
        Caption = 'Work For:'
      end
      object company: TDBText
        Left = 379
        Top = 40
        Width = 52
        Height = 13
        AutoSize = True
        DataField = 'company'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label9: TLabel
        Left = 311
        Top = 81
        Width = 42
        Height = 13
        Caption = 'Contact:'
      end
      object caller_contact: TDBText
        Left = 380
        Top = 81
        Width = 81
        Height = 13
        AutoSize = True
        DataField = 'caller_contact'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label12: TLabel
        Left = 311
        Top = 111
        Width = 62
        Height = 13
        Caption = 'Alt. Contact:'
      end
      object caller_altcontact: TDBText
        Left = 379
        Top = 111
        Width = 96
        Height = 13
        AutoSize = True
        DataField = 'caller_altcontact'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object caller_altphone: TDBText
        Left = 379
        Top = 126
        Width = 88
        Height = 13
        AutoSize = True
        DataField = 'caller_altphone'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object caller_phone: TDBText
        Left = 379
        Top = 96
        Width = 73
        Height = 13
        AutoSize = True
        DataField = 'caller_phone'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label13: TLabel
        Left = 311
        Top = 56
        Width = 63
        Height = 13
        Caption = 'Do Not Mark:'
      end
      object do_not_mark_before: TDBText
        Left = 379
        Top = 56
        Width = 121
        Height = 13
        AutoSize = True
        DataField = 'do_not_mark_before'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label1: TLabel
        Left = 311
        Top = 24
        Width = 27
        Height = 13
        Caption = 'Area:'
      end
      object route_area_name: TDBText
        Left = 380
        Top = 24
        Width = 103
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'route_area_name'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object EstStartLabel: TLabel
        Left = 535
        Top = 9
        Width = 50
        Height = 13
        Caption = 'Est. Start:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object ContactPhoneLabel: TLabel
        Left = 311
        Top = 96
        Width = 34
        Height = 13
        Caption = 'Phone:'
      end
      object AtlPhoneLabel: TLabel
        Left = 311
        Top = 126
        Width = 54
        Height = 13
        Caption = 'Alt. Phone:'
      end
      object PriorityLbl: TLabel
        Left = 12
        Top = 167
        Width = 38
        Height = 13
        Caption = 'Priority:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblROMessage: TLabel
        Left = 534
        Top = 115
        Width = 224
        Height = 13
        Caption = '--------------------------------------------------------'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Loads: TLabel
        Left = 12
        Top = 148
        Width = 28
        Height = 13
        Caption = 'Loads'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object EstStartText: TStaticText
        Left = 607
        Top = 9
        Width = 106
        Height = 13
        AutoSize = False
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnDblClick = ItemDblClick
      end
      object work_type: TDBMemo
        Left = 63
        Top = 40
        Width = 242
        Height = 63
        BorderStyle = bsNone
        Color = clBtnFace
        DataField = 'work_type'
        DataSource = TicketDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 1
        OnDblClick = ItemDblClick
      end
      object RefreshTicketButton: TButton
        Left = 12
        Top = 235
        Width = 106
        Height = 22
        Caption = 'Refresh'
        TabOrder = 2
        OnClick = RefreshTicketButtonClick
      end
      object DetailPageControl: TPageControl
        Left = 140
        Top = 145
        Width = 649
        Height = 109
        ActivePage = WorkDescTab
        Anchors = [akLeft, akTop, akRight, akBottom]
        Images = SharedDevExStyleData.cxImageList
        TabOrder = 3
        object LocateTab: TTabSheet
          Caption = 'Locates'
          ImageIndex = -1
          object Label6: TLabel
            Left = 520
            Top = 69
            Width = 3
            Height = 13
            Visible = False
          end
          object LocateGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 641
            Height = 80
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object LocateGridDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              OnCustomDrawCell = LocateGridDBTableViewCustomDrawCell
              DataController.DataSource = LocateDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'locate_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsData.Editing = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.GridLineColor = clBtnFace
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              Styles.Background = BottomGridStyle
              Styles.Content = BottomGridStyle
              Styles.ContentEven = BottomGridStyle
              Styles.ContentOdd = BottomGridStyle
              Styles.FilterBox = BottomGridStyle
              Styles.Inactive = BottomGridStyle
              Styles.IncSearch = BottomGridStyle
              Styles.Navigator = BottomGridStyle
              Styles.NavigatorInfoPanel = BottomGridStyle
              Styles.Selection = BottomGridStyle
              Styles.FilterRowInfoText = BottomGridStyle
              Styles.Footer = BottomGridStyle
              Styles.Group = BottomGridStyle
              Styles.GroupByBox = BottomGridStyle
              Styles.GroupFooterSortedSummary = BottomGridStyle
              Styles.GroupSortedSummary = BottomGridStyle
              Styles.GroupSummary = BottomGridStyle
              Styles.Header = BottomGridStyle
              Styles.Indicator = BottomGridStyle
              Styles.NewItemRowInfoText = BottomGridStyle
              Styles.Preview = BottomGridStyle
              object LocateGridclient_code: TcxGridDBColumn
                Caption = 'Client'
                DataBinding.FieldName = 'client_code'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Editing = False
                Options.Filtering = False
                Width = 60
              end
              object LocateGridAlert: TcxGridDBColumn
                Caption = 'Alr'
                DataBinding.FieldName = 'Alert2'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                MinWidth = 6
                Options.Filtering = False
                Width = 20
              end
              object LocateGridstatus: TcxGridDBColumn
                Caption = 'Status'
                DataBinding.FieldName = 'status'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Editing = False
                Options.Filtering = False
                Width = 39
              end
              object LocateGridqty_marked: TcxGridDBColumn
                Caption = 'Qty'
                DataBinding.FieldName = 'qty_marked'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taRightJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                HeaderAlignmentHorz = taRightJustify
                Options.Editing = False
                Options.Filtering = False
                Width = 36
              end
              object LocateGridshort_name: TcxGridDBColumn
                Caption = 'Locator'
                DataBinding.FieldName = 'short_name'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Editing = False
                Options.Filtering = False
                Width = 97
              end
              object LocateGridclosed_date: TcxGridDBColumn
                Caption = 'Closed Date'
                DataBinding.FieldName = 'closed_date'
                PropertiesClassName = 'TcxDateEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.DateButtons = [btnClear, btnToday]
                Properties.DateOnError = deToday
                Properties.InputKind = ikRegExpr
                Options.Editing = False
                Options.Filtering = False
                Width = 111
              end
            end
            object LocateGridLevel: TcxGridLevel
              GridView = LocateGridDBTableView
            end
          end
        end
        object WorkDescTab: TTabSheet
          Caption = 'Work Description'
          ImageIndex = -1
          object work_description: TDBMemo
            Left = 0
            Top = 0
            Width = 641
            Height = 59
            Align = alClient
            BorderStyle = bsNone
            Color = clBtnFace
            DataField = 'work_description'
            DataSource = TicketDS
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
            OnDblClick = ItemDblClick
          end
          object cxHyperLinkEdit1: TcxHyperLinkEdit
            Left = 0
            Top = 59
            Align = alBottom
            Properties.SingleClick = True
            Properties.UsePrefix = upNever
            TabOrder = 1
            Visible = False
            Width = 641
          end
        end
        object ResponseTab: TTabSheet
          Caption = 'Responses'
          ImageIndex = -1
          object ResponsesGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 641
            Height = 80
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object ResponsesGridDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ResponseDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'response_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsData.Editing = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.GridLineColor = clBtnFace
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              Styles.Background = BottomGridStyle
              Styles.Content = BottomGridStyle
              Styles.Selection = BottomGridStyle
              Styles.Header = BottomGridStyle
              object ResponsesGridclient_code: TcxGridDBColumn
                Caption = 'Client'
                DataBinding.FieldName = 'client_code'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Filtering = False
                Width = 65
              end
              object ResponsesGridstatus: TcxGridDBColumn
                Caption = 'Status'
                DataBinding.FieldName = 'status'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Filtering = False
                Width = 48
              end
              object ResponsesGridresponse_date: TcxGridDBColumn
                Caption = 'Response Date'
                DataBinding.FieldName = 'response_date'
                PropertiesClassName = 'TcxDateEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.DateButtons = [btnClear, btnToday]
                Properties.DateOnError = deToday
                Properties.InputKind = ikRegExpr
                Options.Filtering = False
                Width = 86
              end
              object ResponsesGridresponse_sent: TcxGridDBColumn
                Caption = 'Sent'
                DataBinding.FieldName = 'response_sent'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Filtering = False
                Width = 103
              end
              object ResponsesGridreply: TcxGridDBColumn
                Caption = 'Reply'
                DataBinding.FieldName = 'reply'
                PropertiesClassName = 'TcxMaskEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Filtering = False
                Width = 100
              end
            end
            object ResponsesGridLevel: TcxGridLevel
              GridView = ResponsesGridDBTableView
            end
          end
        end
        object NotesTab: TTabSheet
          Caption = 'Notes'
          ImageIndex = 1
          object NotesGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 641
            Height = 80
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object NotesGridDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = NotesDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'notes_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsCustomize.ColumnMoving = False
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.CellAutoHeight = True
              OptionsView.GridLineColor = clBtnFace
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              Styles.Background = BottomGridStyle
              Styles.Content = BottomGridStyle
              Styles.Selection = BottomGridStyle
              Styles.Header = BottomGridStyle
              object NoteDateColumn: TcxGridDBColumn
                Caption = 'Date'
                DataBinding.FieldName = 'entry_date'
                PropertiesClassName = 'TcxTextEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Editing = False
                Options.Filtering = False
                Width = 91
              end
              object NoteColumn: TcxGridDBColumn
                Caption = 'Note '
                DataBinding.FieldName = 'Note'
                PropertiesClassName = 'TcxMemoProperties'
                Properties.Alignment = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Properties.ScrollBars = ssVertical
                Options.Editing = False
                Options.Filtering = False
                Width = 221
              end
            end
            object NotesGridLevel: TcxGridLevel
              GridView = NotesGridDBTableView
            end
          end
        end
        object AttachmentsTab: TTabSheet
          Caption = 'Attachments'
          ImageIndex = -1
          object AttachmentsGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 641
            Height = 80
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object AttachmentsGridTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = AttachmentsDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'attachment_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsCustomize.ColumnMoving = False
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.GridLineColor = clBtnFace
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              Styles.Background = BottomGridStyle
              Styles.Content = BottomGridStyle
              Styles.Selection = BottomGridStyle
              Styles.Header = BottomGridStyle
              object ColAttachmentID: TcxGridDBColumn
                Caption = 'Attachment ID'
                DataBinding.FieldName = 'attachment_id'
                PropertiesClassName = 'TcxTextEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Visible = False
                Options.Editing = False
                Options.Filtering = False
                Width = 91
              end
              object ColOrigFileName: TcxGridDBColumn
                Caption = 'File Name'
                DataBinding.FieldName = 'orig_filename'
                PropertiesClassName = 'TcxMemoProperties'
                Properties.Alignment = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Properties.ScrollBars = ssVertical
                Options.Editing = False
                Options.Filtering = False
                SortIndex = 0
                SortOrder = soAscending
                Width = 95
              end
              object ColSize: TcxGridDBColumn
                DataBinding.FieldName = 'size'
                Visible = False
                Options.Editing = False
              end
              object ColAttachDate: TcxGridDBColumn
                Caption = 'Attach Date'
                DataBinding.FieldName = 'attach_date'
                Options.Editing = False
                Width = 99
              end
              object ColUploadDate: TcxGridDBColumn
                Caption = 'Upload Date'
                DataBinding.FieldName = 'upload_date'
                Options.Editing = False
                Width = 88
              end
              object ColDocumentType: TcxGridDBColumn
                Caption = 'Doc Type'
                DataBinding.FieldName = 'doc_type'
                Options.Editing = False
                Width = 50
              end
              object ColComment: TcxGridDBColumn
                Caption = 'Comment'
                DataBinding.FieldName = 'comment'
                Options.Editing = False
                Width = 61
              end
              object ColFileName: TcxGridDBColumn
                Caption = 'Generated File Name'
                DataBinding.FieldName = 'filename'
                Options.Editing = False
                Width = 137
              end
              object ColForeignType: TcxGridDBColumn
                DataBinding.FieldName = 'foreign_type'
                Visible = False
                Options.Editing = False
              end
              object ColForeignID: TcxGridDBColumn
                DataBinding.FieldName = 'foreign_id'
                Visible = False
                Options.Editing = False
              end
              object ColActive: TcxGridDBColumn
                Caption = 'Active'
                DataBinding.FieldName = 'active'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.ReadOnly = True
                Options.Editing = False
                Width = 41
              end
              object ColShortName: TcxGridDBColumn
                Caption = 'Attached By'
                DataBinding.FieldName = 'short_name'
                Options.Editing = False
                Width = 115
              end
              object ColAttachedBy: TcxGridDBColumn
                Caption = 'Attached By ID'
                DataBinding.FieldName = 'attached_by'
                Visible = False
                Options.Editing = False
              end
              object ColSource: TcxGridDBColumn
                Caption = 'Source'
                DataBinding.FieldName = 'source'
                Visible = False
                Options.Editing = False
              end
              object ColUploadMachineName: TcxGridDBColumn
                Caption = 'Upload Machine Name'
                DataBinding.FieldName = 'upload_machine_name'
                Visible = False
                Options.Editing = False
              end
            end
            object AttachmentsGridLevel: TcxGridLevel
              GridView = AttachmentsGridTableView
            end
          end
        end
        object tabRisk: TTabSheet
          Caption = 'Risk Score'
          ImageIndex = 3
          object RiskGrid: TcxGrid
            Left = 0
            Top = 0
            Width = 641
            Height = 80
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            object RiskView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = RiskScoreHistoryDS
              DataController.Filter.MaxValueListCount = 1000
              DataController.KeyFieldNames = 'ticket_risk_id'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnPopup.MaxDropDownItemCount = 12
              OptionsCustomize.ColumnMoving = False
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.InvertSelect = False
              OptionsView.CellAutoHeight = True
              OptionsView.GridLineColor = clBtnFace
              OptionsView.GroupByBox = False
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.AutoHeight = False
              Preview.MaxLineCount = 2
              Styles.Background = BottomGridStyle
              Styles.Content = BottomGridStyle
              Styles.Selection = BottomGridStyle
              Styles.Header = BottomGridStyle
              object ColRiskViewRiskID: TcxGridDBColumn
                Caption = 'Risk ID'
                DataBinding.FieldName = 'ticket_risk_id'
                Width = 49
              end
              object ColRiskViewTicketVersionID: TcxGridDBColumn
                Caption = 'Version ID'
                DataBinding.FieldName = 'ticket_version_id'
                PropertiesClassName = 'TcxTextEditProperties'
                Properties.Alignment.Horz = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Options.Editing = False
                Options.Filtering = False
                Width = 61
              end
              object ColRiskViewRevision: TcxGridDBColumn
                Caption = 'Revision'
                DataBinding.FieldName = 'ticket_revision'
                PropertiesClassName = 'TcxMemoProperties'
                Properties.Alignment = taLeftJustify
                Properties.MaxLength = 0
                Properties.ReadOnly = True
                Properties.ScrollBars = ssVertical
                Options.Editing = False
                Options.Filtering = False
                Width = 98
              end
              object ColRiskViewTicketID: TcxGridDBColumn
                Caption = 'Ticket ID'
                DataBinding.FieldName = 'ticket_id'
              end
              object ColRiskViewLocateClient: TcxGridDBColumn
                Caption = 'Client'
                DataBinding.FieldName = 'client_code'
                Width = 79
              end
              object ColRiskViewSource: TcxGridDBColumn
                Caption = 'Risk Source'
                DataBinding.FieldName = 'risk_source'
                Width = 59
              end
              object ColRiskViewkScoreType: TcxGridDBColumn
                Caption = 'Score Type'
                DataBinding.FieldName = 'risk_score_type'
                Width = 60
              end
              object ColRiskViewScore: TcxGridDBColumn
                Caption = 'Score'
                DataBinding.FieldName = 'risk_score'
                Styles.Content = SharedDevExStyleData.BoldFontStyle
                Width = 35
              end
              object ColRiskViewTransmitDate: TcxGridDBColumn
                Caption = 'Transmit Date'
                DataBinding.FieldName = 'transmit_date'
                Width = 125
              end
            end
            object RiskGridLevel: TcxGridLevel
              GridView = RiskView
            end
          end
        end
      end
      object AckButton: TButton
        Left = 12
        Top = 207
        Width = 106
        Height = 22
        Action = AcknowledgeAction
        ModalResult = 4
        TabOrder = 4
      end
      object TicketPriorityCombo: TComboBox
        Left = 12
        Top = 182
        Width = 106
        Height = 19
        AutoComplete = False
        Style = csOwnerDrawFixed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 5
        OnChange = TicketPriorityComboChange
        OnKeyPress = TicketPriorityComboKeyPress
      end
      object FocusedvsSelectedBtn: TButton
        Left = 668
        Top = 27
        Width = 121
        Height = 25
        Caption = 'Focused vs. Selected'
        TabOrder = 6
        Visible = False
        OnClick = FocusedvsSelectedBtnClick
      end
    end
  end
  inherited ActionListFrame: TActionList
    OnUpdate = ActionListUpdate
    inherited AcknowledgeAction: TAction
      OnExecute = AcknowledgeActionExecute
    end
    inherited NormalPriorityAction: TAction
      OnExecute = NormalPriorityActionExecute
    end
    inherited HighPriorityAction: TAction
      OnExecute = HighPriorityActionExecute
    end
    inherited ReleasePriorityAction: TAction
      OnExecute = ReleasePriorityActionExecute
    end
    object DispatchingPriorityAction: TAction
      Caption = 'Dispatching'
      OnExecute = DispatchingPriorityActionExecute
    end
  end
  inherited PriorityPopup: TPopupMenu
    OnPopup = PriorityPopupPopup
    object Dispatching1: TMenuItem
      Action = DispatchingPriorityAction
    end
    object N1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object OptimizeRouteFromTkt1: TMenuItem
      Caption = 'Optimize Route From Tkt'
      OnClick = OptimizeRouteFromTkt1Click
    end
  end
  inherited ListTable: TDBISAMTable
    AfterOpen = ItemListTableAfterOpen
    TableName = 'TMTicketList'
  end
  inherited DetailTable: TDBISAMTable
    OnCalcFields = ItemDetailTableCalcFields
    TableName = 'ticket'
  end
  object TicketGridViewRepository: TcxGridViewRepository
    Left = 555
    Top = 115
    object TicketView: TcxGridDBTableView
      DragMode = dmAutomatic
      OnDblClick = ViewDblClick
      OnMouseDown = TicketViewMouseDown
      OnMouseUp = TicketViewMouseUp
      OnStartDrag = TicketViewStartDrag
      Navigator.Buttons.CustomButtons = <>
      OnCellClick = TicketViewCellClick
      OnCustomDrawCell = TicketViewCustomDrawCell
      OnFocusedRecordChanged = TicketViewFocusedRecordChanged
      OnSelectionChanged = TicketViewSelectionChanged
      DataController.DataSource = TicketListSource
      DataController.KeyFieldNames = 'ticket_id;activity_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHiding = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.NoDataToDisplayInfoText = '<No tickets for selected locator>'
      OptionsView.GridLines = glHorizontal
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      Styles.Inactive = UnfocusedGridSelStyle
      Styles.Selection = GridSelectionStyle
      OnCustomDrawIndicatorCell = TicketViewCustomDrawIndicatorCell
      object ColUtilities: TcxGridDBColumn
        Caption = 'Utilities'
        DataBinding.FieldName = 'util'
        OnCustomDrawCell = ColUtilitiesCustomDrawCell
        Options.Editing = False
        Width = 20
      end
      object ColName: TcxGridDBColumn
        Caption = 'Name'
        DataBinding.FieldName = 'short_name'
        Visible = False
        Width = 168
      end
      object ColActivityID: TcxGridDBColumn
        Caption = 'Act ID'
        DataBinding.FieldName = 'activity_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Visible = False
        HeaderAlignmentHorz = taRightJustify
        VisibleForCustomization = False
        Width = 80
      end
      object ColActivity: TcxGridDBColumn
        Caption = 'Activity'
        DataBinding.FieldName = 'activity'
        Visible = False
        VisibleForCustomization = False
        Width = 80
      end
      object ColActivityDate: TcxGridDBColumn
        Caption = 'Activity Time'
        DataBinding.FieldName = 'activity_date'
        Visible = False
        VisibleForCustomization = False
        Width = 115
      end
      object ColActivityEmp: TcxGridDBColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'short_name'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object ColTicketNumber: TcxGridDBColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 126
      end
      object ColAlert: TcxGridDBColumn
        Caption = '!      Alert'
        DataBinding.FieldName = 'alert'
        BestFitMaxWidth = 30
        GroupSummaryAlignment = taCenter
        HeaderAlignmentHorz = taCenter
        MinWidth = 15
        Options.Editing = False
        Styles.Content = SharedDevExStyleData.BoldLargeRedFontStyle
        Width = 55
      end
      object ColKind: TcxGridDBColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Visible = False
        VisibleForCustomization = False
        Width = 85
      end
      object ColCompany: TcxGridDBColumn
        Caption = 'Work Done For'
        DataBinding.FieldName = 'company'
        Width = 120
      end
      object ColCon_Name: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'con_name'
        Width = 120
      end
      object ColTicketType: TcxGridDBColumn
        Caption = 'Kind/Tkt Type'
        DataBinding.FieldName = 'ticket_type'
        Width = 84
      end
      object ColDueDate: TcxGridDBColumn
        Caption = 'Due'
        DataBinding.FieldName = 'due_date'
        Width = 110
      end
      object ColWorkAddressNumber: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'work_address_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 38
      end
      object ColWorkAddressStreet: TcxGridDBColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Width = 120
      end
      object ColWorkCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Width = 70
      end
      object ColWorkType: TcxGridDBColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        Width = 150
      end
      object ColArea: TcxGridDBColumn
        Caption = 'Area'
        DataBinding.FieldName = 'area_name'
        Width = 81
      end
      object ColMapPage: TcxGridDBColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Width = 60
      end
      object ColWorkCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Width = 65
      end
      object ColWorkPriority: TcxGridDBColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        Width = 45
      end
      object ColEstStartTime: TcxGridDBColumn
        Caption = 'Est. Start'
        DataBinding.FieldName = 'est_start_time'
        Width = 110
      end
      object ColRouteOrder: TcxGridDBColumn
        Caption = 'Route Order'
        DataBinding.FieldName = 'route_order'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Styles.Content = SharedDevExStyleData.BoldFontStyle
        VisibleForCustomization = False
        Width = 75
      end
      object ColLat: TcxGridDBColumn
        Caption = 'Lat'
        DataBinding.FieldName = 'lat'
        Visible = False
        VisibleForCustomization = False
      end
      object ColLng: TcxGridDBColumn
        Caption = 'Lon'
        DataBinding.FieldName = 'lon'
        Visible = False
        VisibleForCustomization = False
      end
      object ColTicketID: TcxGridDBColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        HeaderAlignmentHorz = taRightJustify
        Styles.Header = SharedDevExStyleData.DebugHeaderCol
        VisibleForCustomization = False
      end
      object ColTicketStatus: TcxGridDBColumn
        Caption = 'Ticket Status'
        DataBinding.FieldName = 'ticket_status'
        Visible = False
        VisibleForCustomization = False
        Width = 25
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'Mod Date'
        DataBinding.FieldName = 'modified_date'
        Visible = False
        VisibleForCustomization = False
      end
      object ColTicketIsVisible: TcxGridDBColumn
        Caption = 'Visible'
        DataBinding.FieldName = 'ticket_is_visible'
        Visible = False
        VisibleForCustomization = False
        Width = 25
      end
      object ColTicketAgeHours: TcxGridDBColumn
        Caption = 'Age (hrs)'
        DataBinding.FieldName = 'age_hours'
      end
      object ColRiskScore: TcxGridDBColumn
        Caption = 'Risk Score'
        DataBinding.FieldName = 'risk'
        Options.Editing = False
      end
      object ColTktCount: TcxGridDBColumn
        Caption = 'Ticket Count'
        DataBinding.FieldName = 'tkt_count'
        HeaderAlignmentHorz = taRightJustify
        Width = 120
      end
      object ColLocCount: TcxGridDBColumn
        Caption = 'Locate Count'
        DataBinding.FieldName = 'loc_count'
        HeaderAlignmentHorz = taRightJustify
        Width = 120
      end
      object ColAltDueDate: TcxGridDBColumn
        Caption = 'Alt Due Date'
        DataBinding.FieldName = 'alt_due_date'
      end
      object ColExpirationDate: TcxGridDBColumn
        Caption = 'Expiration Date'
        DataBinding.FieldName = 'expiration_date'
      end
    end
  end
  object TicketListSource: TDataSource
    DataSet = ListTable
    OnDataChange = TicketListSourceDataChange
    Left = 60
    Top = 123
  end
  object TicketDS: TDataSource
    DataSet = DetailTable
    Left = 720
    Top = 344
  end
  object LocateData: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select locate.*, locate.locator_id, employee.short_name, '
      'client.alert, locate.alert as locatealert'
      'from locate '
      'left join employee on locate.locator_id=employee.emp_id'
      'left join client on locate.client_id=client.client_id'
      ' where locate.active'
      '  and locate.ticket_id=:ticket_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    ReadOnly = True
    Left = 744
    Top = 253
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object LocateDS: TDataSource
    DataSet = LocateData
    Left = 744
    Top = 285
  end
  object Responses: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select response_log.*, locate.client_code'
      ' from ticket '
      '  inner join locate on ticket.ticket_id = locate.ticket_id '
      
        '  inner join response_log on response_log.locate_id = locate.loc' +
        'ate_id '
      ' where ticket.ticket_id = :ticket_id'
      '  and locate.active')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
        Value = 0
      end>
    ReadOnly = True
    Left = 708
    Top = 253
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
        Value = 0
      end>
  end
  object ResponseDS: TDataSource
    DataSet = Responses
    Left = 708
    Top = 285
  end
  object Notes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select *'
      'from notes'
      'where notes.active and'
      '  ('
      
        '    (notes.foreign_type = :foreign_type_ticket and notes.foreign' +
        '_id = :ticket_id)'
      '    or'
      
        '    (notes.foreign_type = :foreign_type_locate and notes.foreign' +
        '_id in (select locate_id from locate where ticket_id = :ticket_i' +
        'd))'
      '  )')
    Params = <
      item
        DataType = ftInteger
        Name = 'foreign_type_ticket'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'foreign_type_locate'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    ReadOnly = True
    Left = 676
    Top = 254
    ParamData = <
      item
        DataType = ftInteger
        Name = 'foreign_type_ticket'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'foreign_type_locate'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object NotesDS: TDataSource
    DataSet = Notes
    Left = 676
    Top = 285
  end
  object Attachments: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select a.*, Coalesce(e.short_name, '#39'Unknown emp ID '#39' + Cast(a.at' +
        'tached_by as varchar(9))) as short_name'
      'from attachment a'
      'left join employee e on e.emp_id = a.attached_by '
      
        'where a.foreign_type = :foreign_type and a.foreign_id = :foreign' +
        '_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'foreign_type'
      end
      item
        DataType = ftUnknown
        Name = 'foreign_id'
      end>
    ReadOnly = True
    Left = 644
    Top = 253
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'foreign_type'
      end
      item
        DataType = ftUnknown
        Name = 'foreign_id'
      end>
  end
  object AttachmentsDS: TDataSource
    DataSet = Attachments
    Left = 644
    Top = 285
  end
  object cxStyleRepoTickets: TcxStyleRepository
    PixelsPerInch = 96
    object BottomGridStyle: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object GridSelectionStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clHotLight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object UnfocusedGridSelStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 11829830
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = 14745599
    end
    object cxStyle1: TcxStyle
    end
  end
  object UtilityDots: TImageList
    BkColor = clWhite
    Masked = False
    ShareImages = True
    Left = 28
    Top = 92
    Bitmap = {
      494C01010900AC00700210001000FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000004CB122004CB122004CB122004CB122004CB122004CB122000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      2200000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      2200000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      2200000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000241CED00241CED00241CED00241CED00241CED00241CED00241CED00241C
      ED00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000241CED00241CED00241CED00241CED00241CED00241CED00241CED00241C
      ED00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000241CED00241CED00241CED00241CED00241CED00241CED00241CED00241C
      ED00000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      000000000000241CED00241CED00241CED00241CED00241CED00241CED000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004691FF004691FF004691
      FF004691FF004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF004691FF004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF00000000000000000000000000000000000000000000000000000000000000
      00004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      000000000000277FFF00277FFF00277FFF00277FFF00277FFF00277FFF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      000000000000277FFF00277FFF00277FFF00277FFF00277FFF00277FFF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000004691FF004691FF004691FF004691FF004691FF004691FF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      000000000000277FFF00277FFF00277FFF00277FFF00277FFF00277FFF000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF00000000000000000000000000000000000000000000000000000000000000
      00004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF00000000000000000000000000000000000000000000000000000000000000
      00004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF000000
      000000000000EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF004691FF004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF004691FF004691FF004691
      FF004691FF004691FF004691FF004691FF004691FF004691FF004691FF004691
      FF004691FF004691FF004691FF00FFFFFF00FFFFFF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      1600327416003274160032741600327416003274160032741600327416003274
      1600327416003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000000000000000000000000000000000000000
      000000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C00FFAF
      1C00000000000000000000000000000000000000000000000000000000000000
      0000FFAF1C00FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      1600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00327416003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C000000000000000000FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF0032741600327416003274160032741600327416003274160032741600FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C000000000000000000FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      160032741600327416003274160032741600327416003274160032741600FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF00000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C000000000000000000FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      160032741600327416003274160032741600327416003274160032741600FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF00000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C000000000000000000FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C000000000000000000FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF003274160032741600327416003274160032741600327416003274
      1600327416003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF003274160032741600327416003274160032741600327416003274
      1600327416003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF003274160032741600327416003274160032741600327416003274
      1600FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C000000
      000000000000FFAF1C00FFAF1C00FFFFFF00FFFFFF003274160032741600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF00000000000000000000000000000000000000000000000000000000000000
      000000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      1600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00327416003274160032741600FFFFFF00FFFFFF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2FF0000F2
      FF0000F2FF0000F2FF0000F2FF00FFFFFF00FFFFFF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10CF001A10
      CF001A10CF001A10CF001A10CF00FFFFFF00FFFFFF00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF1C00FFAF
      1C00FFAF1C00FFAF1C00FFAF1C00FFFFFF00FFFFFF0032741600327416003274
      1600327416003274160032741600327416003274160032741600327416003274
      1600327416003274160032741600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF00FFFF0000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000FFFF000000000000FFFFFFFFFFFFFFFF8001800180018001
      8001800180018001800180018001800180018001800180018001800180018001
      8001800180018001800180018001800180018001800180018001800180018001
      8001800180018001800180018001800180018001800180018001800180018001
      8001800180018001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8001800180018001
      80019FF980018FF180019FF980019FF980019801800190198001980180018019
      800198018001801980019FC180019FF980019FC180019FF98001980180019801
      8001980180019801800198018001980980019FF980019FF980019FF980018FF1
      8001800180018001FFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object RiskScoreHistory: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select tr.*, c.client_name, l.client_code '
      'from ticket_risk tr, locate l, client c'
      'where (tr.ticket_id=:ticket_id)'
      'and (tr.locate_id = l.locate_id)'
      'and (tr.ticket_id = l.ticket_id)'
      'and (l.client_id = c.client_id)'
      'order by tr.ticket_version_id desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 613
    Top = 253
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object RiskScoreHistoryDS: TDataSource
    DataSet = RiskScoreHistory
    Left = 616
    Top = 224
  end
end
