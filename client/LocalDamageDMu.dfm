object LDamageDM: TLDamageDM
  OldCreateOrder = False
  Height = 378
  Width = 362
  object OpenDamages: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      'select distinct damage_id '
      '  from damage    '
      '  where damage.investigator_id=:emp_id'
      '  and damage.active'
      '  and damage_type = '#39'PENDING'#39
      ''
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 158
    Top = 41
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object Damage: TDBISAMTable
    BeforeOpen = DamageBeforeOpen
    AfterOpen = DamageAfterOpen
    AfterInsert = DamageAfterInsert
    BeforeEdit = DamageBeforeEdit
    BeforePost = DamageBeforePost
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    TableName = 'damage'
    Left = 46
    Top = 44
  end
  object DamageClaimant: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      
        'select 0 as "type", d.damage_id as "claimant_id", d.utility_co_d' +
        'amaged as "claimant"'
      '  from damage d where d.damage_id = :damage_id1'
      
        'union select 1 as "type", t.third_party_id as "claimant_id", t.c' +
        'laimant as "claimant"'
      '  from damage_third_party t where t.damage_id = :damage_id2'
      
        'union select 2 as "type", l.litigation_id as "claimant_id", l.pl' +
        'aintiff as "claimant"'
      '  from damage_litigation l where l.damage_id = :damage_id3'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'damage_id1'
      end
      item
        DataType = ftUnknown
        Name = 'damage_id2'
      end
      item
        DataType = ftUnknown
        Name = 'damage_id3'
      end>
    Left = 158
    Top = 114
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'damage_id1'
      end
      item
        DataType = ftUnknown
        Name = 'damage_id2'
      end
      item
        DataType = ftUnknown
        Name = 'damage_id3'
      end>
  end
  object DamageEstimate: TDBISAMTable
    BeforeOpen = DamageEstimateBeforeOpen
    AfterOpen = DamageEstimateAfterOpen
    BeforePost = DamageEstimateBeforePost
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    IndexName = 'damage_id'
    TableName = 'damage_estimate'
    Left = 46
    Top = 112
  end
  object DamageLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    TableName = 'damage'
    Left = 46
    Top = 188
  end
  object DamageProfitCenterRules: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      'select  *,'
      '  if( ((county <> '#39'*'#39') and (city <> '#39'*'#39')) then 1 else '
      '  if( ((county = '#39'*'#39') and (city <> '#39'*'#39')) then 2 else '
      '  if( ((county <> '#39'*'#39') and (city = '#39'*'#39')) then 3 else '
      '  if(((county = '#39'*'#39') and (city = '#39'*'#39')) then 4 else 5) ) ) ) '
      '  as rule_priority'
      'from damage_profit_center_rule'
      'where active'
      'order by rule_priority, state, county, city'
      '')
    Params = <>
    Left = 57
    Top = 246
  end
  object DamageChangeReasonLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      
        'select ref_id, code, description from reference where type='#39'dmgr' +
        'eason'#39' and active_ind order by description')
    Params = <>
    Left = 62
    Top = 304
  end
  object RefDescForDamageCos: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.33 Build 4'
    SQL.Strings = (
      'Select description'
      'From reference where '
      'ref_id=:refid '
      'and'
      'type='#39'dmgco'#39' '
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'refid'
      end>
    Left = 194
    Top = 205
    ParamData = <
      item
        DataType = ftInteger
        Name = 'refid'
      end>
  end
end
