inherited BillingReviewSearchCriteria: TBillingReviewSearchCriteria
  Left = 555
  Top = 289
  Caption = 'Billing Review Search'
  ClientHeight = 237
  ClientWidth = 662
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel [0]
    Left = 0
    Top = 0
    Width = 662
    Height = 203
    Align = alTop
    Shape = bsBottomLine
  end
  inherited RecordsLabel: TLabel
    Left = 429
    Top = 172
    Width = 113
  end
  object CallCentersLabel: TLabel [2]
    Left = 13
    Top = 7
    Width = 62
    Height = 13
    Caption = 'Call Centers:'
  end
  object DescriptionLabel: TLabel [3]
    Left = 320
    Top = 8
    Width = 57
    Height = 13
    Caption = 'Description:'
  end
  object CommittedLabel: TLabel [4]
    Left = 13
    Top = 146
    Width = 55
    Height = 13
    Caption = 'Committed:'
  end
  inline PeriodStartRange: TOdRangeSelectFrame [5]
    Left = 13
    Top = 24
    Width = 296
    Height = 63
    TabOrder = 2
    inherited FromLabel: TLabel
      Left = 0
      Width = 88
      Alignment = taRightJustify
      Caption = 'Period Start From:'
    end
    inherited ToLabel: TLabel
      Left = 184
    end
    inherited DatesLabel: TLabel
      Left = 0
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 93
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 93
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 205
    end
  end
  inherited SearchButton: TButton [6]
    Left = 105
    Top = 168
    TabOrder = 7
  end
  inherited CancelButton: TButton [7]
    Left = 414
    Top = 168
    TabOrder = 10
    Visible = False
  end
  inherited CopyButton: TButton [8]
    Left = 184
    Top = 168
    TabOrder = 8
  end
  inherited ExportButton: TButton [9]
    Left = 261
    Top = 168
    TabOrder = 9
  end
  inherited PrintButton: TButton [10]
    Left = 434
    TabOrder = 15
  end
  inherited ClearButton: TButton
    Left = 337
    Top = 168
    TabOrder = 16
  end
  object CallCenter: TComboBox
    Left = 106
    Top = 4
    Width = 199
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    Enabled = False
    ItemHeight = 13
    TabOrder = 0
  end
  object CommitButton: TButton
    Left = 2
    Top = 208
    Width = 159
    Height = 25
    Action = CommitAction
    TabOrder = 11
  end
  object DeleteButton: TButton
    Left = 164
    Top = 208
    Width = 159
    Height = 25
    Action = DeleteAction
    TabOrder = 12
  end
  object UncommitButton: TButton
    Left = 327
    Top = 208
    Width = 159
    Height = 25
    Action = UncommitAction
    TabOrder = 13
  end
  object ReExportButton: TButton
    Left = 490
    Top = 208
    Width = 159
    Height = 25
    Action = ReexportAction
    TabOrder = 14
  end
  inline PeriodEndRange: TOdRangeSelectFrame
    Left = 312
    Top = 24
    Width = 296
    Height = 63
    TabOrder = 3
    inherited FromLabel: TLabel
      Width = 82
      Caption = 'Period End From:'
    end
    inherited ToLabel: TLabel
      Left = 184
    end
    inherited DatesComboBox: TComboBox
      Left = 94
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 94
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 205
    end
  end
  inline BillingRunRange: TOdRangeSelectFrame
    Left = 312
    Top = 79
    Width = 296
    Height = 63
    TabOrder = 5
    inherited FromLabel: TLabel
      Width = 79
      Alignment = taRightJustify
      Caption = 'Billing Run From:'
    end
    inherited ToLabel: TLabel
      Left = 184
    end
    inherited DatesLabel: TLabel
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 94
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 94
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 205
    end
  end
  inline CommitDateRange: TOdRangeSelectFrame
    Left = 13
    Top = 79
    Width = 296
    Height = 63
    TabOrder = 4
    inherited FromLabel: TLabel
      Left = 0
      Width = 82
      Alignment = taRightJustify
      Caption = 'Committed From:'
    end
    inherited ToLabel: TLabel
      Left = 184
    end
    inherited DatesLabel: TLabel
      Left = 0
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 93
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 93
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 205
    end
  end
  object Description: TEdit
    Left = 406
    Top = 4
    Width = 200
    Height = 21
    TabOrder = 1
  end
  object BillingCommitted: TComboBox
    Left = 106
    Top = 141
    Width = 199
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 569
    Top = 168
    object CommitAction: TAction
      Caption = 'Commit && Approve for Invoicing'
      OnExecute = CommitActionExecute
    end
    object DeleteAction: TAction
      Caption = 'Delete Selected'
      OnExecute = DeleteActionExecute
    end
    object UncommitAction: TAction
      Caption = 'Un-commit Selected'
      OnExecute = UncommitActionExecute
    end
    object ReexportAction: TAction
      Caption = 'Re-export Selected'
      OnExecute = ReExportActionExecute
    end
  end
end
