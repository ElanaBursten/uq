unit UQUtils;

interface

uses DB, Graphics, DBISAMTb, QMServerLibrary_Intf;

type
  TTicketFlag = (tsPastDue, tsDueToday, tsDueHours, tsEmergency, tsUpdate,
    tsNotDue, tsDoNotMark, tsPastWorkloadDate, ts7NextBusDay);
  TTicketStatus = set of TTicketFlag;
  TDamageFlag = (dsPastDue, dsDueToday, dsDueHours, dsNotDue);
  TDamageStatus = set of TDamageFlag;
  TWorkOrderFlag = (wsPastDue, wsDueToday, wsDueHours, wsPriority, wsNotDue);
  TWorkOrderStatus = set of TWorkOrderFlag;

  {QM-444 Part 2 EB}
  TQualFlag = (oqfIsQualified, oqfIsNotQualified, oqfAboutToExpire);

function GetTicketStatusFromRecord(DataSet: TDataSet): TTicketStatus;
function GetWorkOrderStatusFromRecord(DataSet: TDataSet): TWorkOrderStatus;
function GetDamageStatusFromRecord(DataSet: TDataSet): TDamageStatus;
function GetTicketStatusFromValues(const Kind, TicketType, TicketFormat: string;
  const DueDate, LegalGoodThru, DoNotMarkBefore: TDateTime; const
  UseWorkloadDate: Boolean=False; const WorkloadDate: TDateTime=0): TTicketStatus;
function GetWorkOrderStatusFromValues(const Kind: string; const DueDate: TDateTime): TWorkOrderStatus;
function GetDamageStatusFromValues(const DueDate: TDateTime): TDamageStatus;
function GetWorkOrderColor(Status: TWorkOrderStatus; Shaded: Boolean; DefaultColor: TColor = clWindow): TColor;
function GetTicketColor(Status: TTicketStatus; Shaded: Boolean;
  DefaultColor: TColor=clWindow): TColor;
function GetValidColor(QualFlag: TQualFlag): TColor;
function GetDamageColor(Status: TDamageStatus; Shaded: Boolean;
  DefaultColor: TColor = clWindow): TColor;
function GetLocatesStringForTicket(Locates: TDBISAMTable; TicketID: Integer;
  const Delimiter: string = ' '): string;
function GetKBStringForBytes(Size: Integer): string;
function AddParam(List: NVPairList; const ParamName, ParamValue: string): NVPair; overload;
function AddParam(List: NVPairList; const ParamName: string; ParamValue: Integer): NVPair; overload;
function AddParam(List: NVPairList; const ParamName: string; ParamValue: TDateTime): NVPair; overload;

function At7NextBusDay(const DueDate: TDateTime; CallCenter: string = '*'): boolean; //QMANTWO-503
function GetNextBusDay(Const OriginDate: TDateTime; CallCenter: string = '*'): TDateTime; //QM-991 Returns the next busness date
function IsNextBusDay(const SomeDate: TDateTime; CallCenter: string = '*'; FromOriginDate: TDateTime = 0): boolean; //QM-991 Expiration Date Coloring and alerts  EB
function IsBeforeNextBusDay(const SomeDate: TDateTime; CallCenter: string = '*'; FromOriginDate: TDateTime = 0): boolean;  //QM-991 Expiration Date Coloring and alerts EB

implementation

uses SysUtils, DateUtils, Math, DMu, OdMiscUtils, QMConst, OdIsoDates,
  Types;

function GetTicketStatusFromRecord(DataSet: TDataSet): TTicketStatus;
var
  DueDate: TDateTime;
  LegalGoodThru: TDateTime;
  Kind: string;
  TicketType: string;
  TicketFormat: string;
  DoNotMarkBefore: TDateTime;
  WorkloadDateField: TField;
begin
  Assert(Assigned(DataSet));
  Kind := Dataset.FieldByName('kind').AsString;
  TicketType := Dataset.FieldByName('ticket_type').AsString;
  TicketFormat := Dataset.FieldByName('ticket_format').AsString;
  DueDate := Dataset.FieldByName('due_date').AsDateTime;
  LegalGoodThru := Dataset.FieldByName('legal_good_thru').AsDateTime;
  DoNotMarkBefore := Dataset.FieldByName('do_not_mark_before').AsDateTime;

  WorkloadDateField := DataSet.FindField('workload_date');
  if (WorkloadDateField = nil) or WorkloadDateField.IsNull then
    Result := GetTicketStatusFromValues(Kind, TicketType, TicketFormat, DueDate,
      LegalGoodThru, DoNotMarkBefore)
  else
    Result := GetTicketStatusFromValues(Kind, TicketType, TicketFormat, DueDate,
      LegalGoodThru, DoNotMarkBefore, True, WorkloadDateField.AsDateTime);
end;

function GetWorkOrderStatusFromRecord(DataSet: TDataSet): TWorkOrderStatus;
var
  DueDate: TDateTime;
  Kind: String;
begin
  Assert(Assigned(DataSet));
  DueDate := Dataset.FieldByName('due_date').AsDateTime;
  Kind := Dataset.FieldByName('kind').AsString;
  Result := GetWorkOrderStatusFromValues(Kind, DueDate);
end;

function GetDamageStatusFromRecord(DataSet: TDataSet): TDamageStatus;
var
  DueDate: TDateTime;
begin
  Assert(Assigned(DataSet));
  DueDate := Dataset.FieldByName('due_date').AsDateTime;
  Result := GetDamageStatusFromValues(DueDate);
end;

function At7NextBusDay(const DueDate: TDateTime; CallCenter:String): boolean; //QMANTWO-503 EB
var
  NextBusDate: TDateTime;
  Exactly7AM: TDateTime;
begin
  Result := False;
  Exactly7AM := EncodeTime(7,0,0,0);

  {Establish that DueDate has not passed and it is a 7AM ticket}
  if (DueDate > Today) and (SameTime(DueDate, Exactly7AM))
      and ((CallCenter = 'OCC2') or (CallCenter = 'FDE2')) then begin  //EB QM-74
      //EB: It would be better to move this to a table, but for rush-job...

    {Get the Next Business Day}
    case DayOfTheWeek(Today) of
      DayFriday :   NextBusDate := IncDay(Today, 3);
      DaySaturday : NextBusDate := IncDay(Today, 2);
      else          NextBusDate := Tomorrow;
    end;

    {Skip holidays}
    While DM.IsHoliday(NextBusDate, CallCenter) do begin
      NextBusDate := IncDay(NextBusDate, 1);
    end;

    if DueDate <= (NextBusDate + Exactly7AM) then
      Result := True;
  end;
end;

function GetNextBusDay(Const OriginDate: TDateTime; CallCenter: string = '*'): TDateTime; //QM-991 Returns the next busness date
var
  NextBusDay: TDateTime;
  OriginDayStr, NBD: string;
  OriginDay: TDateTime;  {The day without time}
begin
  Assert((OriginDate <> 0),'No Origin Date specified.');
  OriginDay := DateOf(OriginDate);  {Date without time}

  {Get the Next Business Day}
  case DayOfTheWeek(OriginDay) of
    DayFriday :   NextBusDay := IncDay(OriginDay, 3);
    DaySaturday : NextBusDay := IncDay(OriginDay, 2);
  else
    NextBusDay := Tomorrow;
  end;

  {Skip holidays}
  While DM.IsHoliday(NextBusDay, CallCenter) do begin
    NextBusDay := IncDay(NextBusDay, 1);
  end;

  NBD := DateTimeToStr(NextBusDay);
  OriginDayStr := DateTimeToStr(NextBusDay);

  Result := NextBusDay;
end;


function IsNextBusDay(const SomeDate: TDateTime; CallCenter: string = '*'; FromOriginDate: TDateTime = 0): boolean; //QM-991 Expiration Date Coloring and alerts  EB
var
  SomeDay: TDateTime;
begin
  SomeDay := DateOf(SomeDate); {we just want the date}
  if (SomeDay) = (GetNextBusDay(FromOriginDate, CallCenter)) then
    Result := True
  else
    Result := False;
end;

 function IsBeforeNextBusDay(const SomeDate: TDateTime; CallCenter: string = '*'; FromOriginDate: TDateTime = 0): boolean;
var
  SomeDay: TDateTime;
begin
  SomeDay := DateOf(SomeDate); {we just want the date}
  if (SomeDay) = (GetNextBusDay(FromOriginDate, CallCenter)) then
    Result := True
  else
    Result := False;
end;

function GetTicketStatusFromValues(const Kind, TicketType, TicketFormat: string;
  const DueDate, LegalGoodThru, DoNotMarkBefore: TDateTime;
  const UseWorkloadDate: Boolean=False; const WorkloadDate: TDateTime=0):
  TTicketStatus;
const
  HoursDue = 2;
var
  IsOngoing: Boolean;
  IsDueToday: Boolean;
  IsDueInHours: Boolean;
  IsPastDue: Boolean;
  IsDone: Boolean;
  EndOfToday: TDateTime;
  TwoHoursFromNow: TDateTime;
  NextBusDay7: Boolean;

  BeginningOfToday: TDateTime;
  EffectiveDueDate: TDateTime;
  IsEmergency: Boolean;
  IsUpdate: Boolean;
  IsNewJersey: Boolean;
  IsDoNotMark: Boolean;
begin
  IsOngoing := Kind = TicketKindOngoing;
  if IsOngoing then
    EffectiveDueDate := Max(DueDate, LegalGoodThru)
  else
    EffectiveDueDate := DueDate;
  TwoHoursFromNow := DateUtils.IncHour(Now, 2);
  EndOfToday := DateUtils.EndOfTheDay(Now);
  BeginningOfToday := DateUtils.StartOfTheDay(Now);
  NextBusDay7 := At7NextBusDay(EffectiveDueDate, TicketFormat);      //QMANTWO-503
  IsDone := Kind = TicketKindDone;
  IsPastDue := (not IsDone) and (EffectiveDueDate < Now);
  IsDueToday := (not IsDone) and (EffectiveDueDate >= BeginningOfToday)
                and (EffectiveDueDate < EndOfToday);
  IsDueInHours := (not IsDone) and (EffectiveDueDate >= Now)
                and (EffectiveDueDate <= TwoHoursFromNow);
  IsDoNotMark := (DoNotMarkBefore > 1) and (DoNotMarkBefore > Now);
  IsEmergency := Kind = TicketKindEmergency;
  IsUpdate    := TicketType = 'UPDATE';
  IsNewJersey := TicketFormat = CallCenterNewJersey;

  if IsPastDue then
    Result := [tsPastDue]
  else if IsDueInHours then
    Result := [tsDueHours]
  else if IsDueToday then
    Result := [tsDueToday]
  else
    Result := [tsNotDue];

  if IsNewJersey and IsEmergency then
    Include(Result, tsEmergency)
  else if IsNewJersey and IsUpdate then
    Include(Result, tsUpdate);

  if IsDoNotMark then
    Include(Result, tsDoNotMark);

  if UseWorkloadDate and (CompareDate(Now, WorkloadDate) = GreaterThanValue) then
    Include(Result, tsPastWorkloadDate);

  if NextBusDay7 then
    Include(Result, ts7NextBusDay);
end;

function GetWorkOrderStatusFromValues(const Kind: string; const DueDate: TDateTime): TWorkOrderStatus;
const
  HoursDue = 2;
var
  IsDueToday: Boolean;
  IsDueInHours: Boolean;
  IsPastDue: Boolean;
  EndOfToday: TDateTime;
  TwoHoursFromNow: TDateTime;
  BeginningOfToday: TDateTime;
  IsPriority: Boolean;
begin
  TwoHoursFromNow := DateUtils.IncHour(Now, 2);
  EndOfToday := DateUtils.EndOfTheDay(Now);
  BeginningOfToday := DateUtils.StartOfTheDay(Now);
  IsPastDue := (DueDate < Now);
  IsDueToday := (DueDate >= BeginningOfToday) and (DueDate < EndOfToday);
  IsDueInHours := (DueDate >= Now) and (DueDate <= TwoHoursFromNow);
  IsPriority := Kind = WorkOrderKindPriority;

  if IsPastDue then
    Result := [wsPastDue]
  else if IsDueInHours then
    Result := [wsDueHours]
  else if IsDueToday then
    Result := [wsDueToday]
  else
    Result := [wsNotDue];
  if IsPriority then
    Include(Result, wsPriority);
end;

function GetDamageStatusFromValues(const DueDate: TDateTime): TDamageStatus;
const
  HoursDue = 2;
var
  IsDueToday: Boolean;
  IsDueInHours: Boolean;
  IsPastDue: Boolean;
  EndOfToday: TDateTime;
  TwoHoursFromNow: TDateTime;
  BeginningOfToday: TDateTime;
begin
  TwoHoursFromNow := DateUtils.IncHour(Now, 2);
  EndOfToday := DateUtils.EndOfTheDay(Now);
  BeginningOfToday := DateUtils.StartOfTheDay(Now);
  IsPastDue := (DueDate < Now);
  IsDueToday := (DueDate >= BeginningOfToday) and (DueDate < EndOfToday);
  IsDueInHours := (DueDate >= Now) and (DueDate <= TwoHoursFromNow);

  if IsPastDue then
    Result := [dsPastDue]
  else if IsDueInHours then
    Result := [dsDueHours]
  else if IsDueToday then
    Result := [dsDueToday]
  else
    Result := [dsNotDue];
end;

function GetWorkOrderColor(Status: TWorkOrderStatus; Shaded: Boolean; DefaultColor: TColor): TColor;
begin
  if (wsPastDue in Status) or (wsPriority in Status) then
    Result := DM.UQState.CriticalTicketColor
  else if wsDueHours in Status then
    Result := DM.UQState.DueHoursTicketColor
  else if wsDueToday in Status then
    Result := DM.UQState.DueTicketColor
  else if Shaded then
    Result := DM.UQState.ShadeColor
  else
    Result := DefaultColor;
end;

function GetTicketColor(Status: TTicketStatus; Shaded: Boolean;
  DefaultColor: TColor=clWindow): TColor;
begin
  if (tsPastDue in Status) or (tsEmergency in Status) or (tsUpdate in Status) then
    Result := DM.UQState.CriticalTicketColor
  else if tsDueHours in Status then
    Result := DM.UQState.DueHoursTicketColor
  else if tsDueToday in Status then
    Result := DM.UQState.DueTicketColor
  else if ts7NextBusDay in Status then
    Result := DM.UQState.Due7TomorrowColor
  else if tsDoNotMark in Status then
    Result := DM.UQState.DoNotMarkTicketColor
  else if Shaded then
    Result := DM.UQState.ShadeColor
  else
    Result := DefaultColor;
end;

function GetValidColor(QualFlag: TQualFlag): TColor;  //QM-444 Part 2 EB   (Can be used for other grid color needs}
begin
  if QualFlag = oqfIsQualified then
    Result := DM.UQState.IsQualified
  else if QualFlag = oqfIsNotQualified then       
    Result := DM.UQState.IsNotQualified
  else if QualFlag = oqfAboutToExpire then
    Result := DM.UQState.IsAboutToExpire
  else
    Result := clWhite;

end;

function GetDamageColor(Status: TDamageStatus; Shaded: Boolean; DefaultColor: TColor): TColor;
begin
  if (dsPastDue in Status) then
    Result := DM.UQState.CriticalTicketColor
  else if dsDueHours in Status then
    Result := DM.UQState.DueHoursTicketColor
  else if dsDueToday in Status then
    Result := DM.UQState.DueTicketColor
  else if Shaded then
    Result := DM.UQState.ShadeColor
  else
    Result := DefaultColor;
end;

function GetLocatesStringForTicket(Locates: TDBISAMTable; TicketID: Integer;
  const Delimiter: string): string;

  procedure AddLocateString(Loc: string);
  begin
    if Trim(Loc) = '' then
      Exit;
    if Result = '' then
      Result := Loc
    else
      Result := Result + Delimiter + Loc;
  end;

begin
  Assert(Assigned(Locates));

  if not Locates.Active then
    Locates.Open;

  Locates.IndexFieldNames := 'ticket_id';
  Locates.SetRange([TicketID], [TicketID]);
  Locates.First;
  while not Locates.Eof do
  begin
    AddLocateString(Locates.FieldByName('client_code').AsString);
    Locates.Next;
  end;
end;

function GetKBStringForBytes(Size: Integer): string;
begin
  if Size = 0 then
    Result := ''
  else begin
    Size := Round(Size / 1024);
    Result := IntToStr(Size);
  end;
end;

function AddParam(List: NVPairList; const ParamName, ParamValue: string): NVPair;
begin
  Assert(Assigned(List));
  Assert(Trim(ParamName) <> '');
  Result := List.Add;
  Result.Name := ParamName;
  Result.Value := ParamValue;
end;

function AddParam(List: NVPairList; const ParamName: string; ParamValue: Integer): NVPair;
begin
  Result := AddParam(List, ParamName, IntToStr(ParamValue));
end;

function AddParam(List: NVPairList; const ParamName: string; ParamValue: TDateTime): NVPair;
begin
  Result := AddParam(List, ParamName, IsoDateTimeToStr(ParamValue));
end;

end.
