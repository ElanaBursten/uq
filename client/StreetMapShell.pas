unit StreetMapShell;   //QMANTWO-711

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ShellApi, StdCtrls, ExtCtrls, MainFU;

type
  TfrmMapShell = class(TForm)
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure EmbeddMapForm;
  protected
    procedure WMSysCommand(var Message:TWMSysCommand); message WM_SYSCOMMAND;
    { Private declarations }
  public
    { Public declarations }
    fStreetMapHandle :THandle;
  end;

var
  frmMapShell: TfrmMapShell;

implementation

{$R *.dfm}

procedure TfrmMapShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TfrmMapShell.FormCreate(Sender: TObject);
begin
  EnableMenuItem(GetSystemMenu(Self.Handle, LongBool(False)), SC_CLOSE, MF_BYCOMMAND or MF_GRAYED); //QMANTWO-711
  EmbeddMapForm;
end;

procedure TfrmMapShell.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ((ssAlt in Shift) and (Key = VK_F4)) then
     Key := 0;  //QMANTWO-711
end;

procedure TfrmMapShell.FormResize(Sender: TObject);
begin
  if IsWindow(fStreetMapHandle) then begin
    SetWindowPos(fStreetMapHandle, 0, 0, 0, ClientWidth, ClientHeight,
      SWP_ASYNCWINDOWPOS);
  end;
end;

procedure TfrmMapShell.EmbeddMapForm;
var
  Rec: TShellExecuteInfo;
const
  AVerb = 'open';
  AParams = '';
  AFileName = 'StreetMapProj.exe';
  ADir = '';
begin
  inherited;
  FillChar(Rec, SizeOf(Rec), #0);

  Rec.cbSize       := SizeOf(Rec);
  Rec.fMask        := SEE_MASK_NOCLOSEPROCESS;
  Rec.lpVerb       := PChar( AVerb );
  Rec.lpFile       := PChar( AfileName );
  Rec.lpParameters := PChar( AParams );
  Rec.lpDirectory  := PChar( Adir );
  Rec.nShow        := SW_HIDE;

  ShellExecuteEx(@Rec);
  WaitForInputIdle(Rec.hProcess, infinite);
  fStreetMapHandle := FindWindow( PChar('TfrmStreetMap'), PChar('StreetMap') );
  MainFU.shellHandle := fStreetMapHandle;
  Windows.SetParent( fStreetMapHandle, Handle );

  Resize;
  ShowWindow(fStreetMapHandle, SW_SHOW);
end;

procedure TfrmMapShell.FormShow(Sender: TObject);
begin
//  EmbeddMapForm;
  FormResize(Sender);
end;

procedure TfrmMapShell.WMSysCommand(var Message: TWMSysCommand);
begin
 if Message.CmdType and $FFF0 = SC_MINIMIZE then Hide;
 inherited;
end;

end.
