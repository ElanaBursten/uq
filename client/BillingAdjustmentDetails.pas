unit BillingAdjustmentDetails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ActnList, StdCtrls, ExtCtrls, DB, DBCtrls,
  DBISAMTb, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles,
  cxDataStorage, cxDBData, cxCurrencyEdit,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, 
  cxDBExtLookupComboBox, cxCalendar, cxDBEdit, cxNavigator, cxCustomData,
  cxFilter, cxData, cxLookupEdit, cxDBLookupEdit, Mask;

const
  UM_UPDATECOMBOS = WM_USER + 424;

type
  TBillingAdjustmentDetailsForm = class (TEmbeddableForm)
    Actions: TActionList;
    NewAdjustmentAction: TAction;
    AddedDateLabel: TLabel;
    AddedDateText: TDBText;
    AdjDateLabel: TLabel;
    AdjustmentSource: TDataSource;
    AmountLabel: TLabel;
    BillingAdjustment: TDBISAMTable;
    AddButton: TButton;
    ButtonPanel: TPanel;
    CancelAction: TAction;
    CancelButton: TButton;
    CustomerCodeLabel: TLabel;
    DescriptionLabel: TLabel;
    SaveAction: TAction;
    SaveButton: TButton;
    TypeLabel: TLabel;
    AdjustmentType: TDBComboBox;
    CustomerSource: TDataSource;
    AdjustmentAmount: TDBEdit;
    AdjustmentDescription: TDBEdit;
    DeleteButton: TButton;
    DeleteAdjustmentAction: TAction;
    WhichInvoiceLabel: TLabel;
    WhichInvoice: TDBComboBox;
    GLCodeLabel: TLabel;
    GLCodes: TDBISAMQuery;
    GLSource: TDataSource;
    GLCodeLookup: TcxDBExtLookupComboBox;
    ViewRepo: TcxGridViewRepository;
    CustomerID: TcxDBExtLookupComboBox;
    GLCodesView: TcxGridDBBandedTableView;
    ColGLLookupGLCode: TcxGridDBBandedColumn;
    ColGLLookupDescription: TcxGridDBBandedColumn;
    CustomerLookupView: TcxGridDBBandedTableView;
    ColCustLookupCustomerID: TcxGridDBBandedColumn;
    ColCustLookupCustomerName: TcxGridDBBandedColumn;
    ColCustLookupCustomerNumber: TcxGridDBBandedColumn;
    ColCustLookupCity: TcxGridDBBandedColumn;
    ColCustLookupState: TcxGridDBBandedColumn;
    ColCustLookupPCCode: TcxGridDBBandedColumn;
    AdjustmentDate: TcxDBDateEdit;
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure NewAdjustmentActionExecute(Sender: TObject);
    procedure BillingAdjustmentNewRecord(DataSet: TDataSet);
    procedure CancelActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SaveActionExecute(Sender: TObject);
    procedure BillingAdjustmentAfterOpen(DataSet: TDataSet);
    procedure BillingAdjustmentBeforeOpen(DataSet: TDataSet);
    procedure DeleteAdjustmentActionExecute(Sender: TObject);
    procedure CustomerIDPropertiesChange(Sender: TObject);
    procedure AdjustmentTypeChange(Sender: TObject);
  private
    FOnDone: TNotifyEvent;
    FLastAdjustmentDate: TDateTime;
    procedure AfterGoToBillingAdjustment;
    procedure ValidateFields;
    procedure UMUpdateCombos(var Msg: TMessage); message UM_UPDATECOMBOS;
    procedure UpdateDependentDropdowns(UserChanged: Boolean);
    function IsCommentData: Boolean;
    function IsCommentText: Boolean;
  public
    procedure AddNewAdjustment;
    procedure CloseDataSets; override;
    procedure GoToBillingAdjustmentID(const ID: Integer);
    procedure OpenDataSets; override;
    property OnDone: TNotifyEvent read FOnDone write FOnDone;
    procedure LeavingNow; override;
  end;

implementation

uses
  DMu, OdVclUtils, OdHourglass, OdExceptions, OdDbUtils, QMConst;

{$R *.dfm}

{ TBillingAdjustmentDetailsForm }

procedure TBillingAdjustmentDetailsForm.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  SaveAction.Enabled := EditingDataSet(BillingAdjustment);
  NewAdjustmentAction.Enabled := not SaveAction.Enabled;
  DeleteAdjustmentAction.Enabled := not SaveAction.Enabled;
  AdjustmentAmount.Enabled := not IsCommentText;
end;

procedure TBillingAdjustmentDetailsForm.NewAdjustmentActionExecute(Sender: TObject);
begin
  BillingAdjustment.Insert;
  BillingAdjustmentNewRecord(BillingAdjustment);
  UpdateDependentDropdowns(False);
  if CustomerID.CanFocus then
    CustomerID.SetFocus;
end;

procedure TBillingAdjustmentDetailsForm.AddNewAdjustment;
begin
  OpenDataSets;
  Show;
  NewAdjustmentAction.Execute;
end;

procedure TBillingAdjustmentDetailsForm.AfterGoToBillingAdjustment;
begin
  UpdateDependentDropdowns(False);
end;

procedure TBillingAdjustmentDetailsForm.BillingAdjustmentNewRecord(DataSet: TDataSet);
begin
  Dataset.FieldByName('added_date').AsDateTime := Now;
  Dataset.FieldByName('active').AsBoolean := True;

  if FLastAdjustmentDate > 0 then
    Dataset.FieldByName('adjustment_date').AsDateTime := FLastAdjustmentDate;
end;

procedure TBillingAdjustmentDetailsForm.CancelActionExecute(Sender: TObject);
begin
  CancelDataset(BillingAdjustment);
  FOnDone(Self);
end;

procedure TBillingAdjustmentDetailsForm.CloseDataSets;
begin
  BillingAdjustment.Close;
end;

procedure TBillingAdjustmentDetailsForm.FormCreate(Sender: TObject);
begin
  inherited;
  Self.HandleNeeded;
  DM.GetRefDisplayList('adjtype', AdjustmentType.Items);
  SizeComboDropdownToItems(AdjustmentType);
  FLastAdjustmentDate := 0;
  CustomerSource.DataSet := DM.Customers;
end;

procedure TBillingAdjustmentDetailsForm.FormDestroy(Sender: TObject);
begin
  CloseDataSets;
  inherited;
end;

procedure TBillingAdjustmentDetailsForm.GoToBillingAdjustmentID(const ID: Integer);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  OpenDataSets;

  if not BillingAdjustment.Locate('adjustment_id', ID, []) then
    raise Exception.Create('Unable to find adjustment id: ' + IntToStr(ID));

  AfterGoToBillingAdjustment;
  if BillingAdjustment.FieldByName('adjustment_id').AsInteger <> ID then
    raise Exception.CreateFmt('Expected to be showing adjustment ID %d, but %d is showing.',
      [ID, BillingAdjustment.FieldByName('adjustment_id').AsInteger]);

  if not Visible then
    Show;
end;

procedure TBillingAdjustmentDetailsForm.OpenDataSets;
begin
  DM.Customers.Open;
  BillingAdjustment.Open;
end;

procedure TBillingAdjustmentDetailsForm.SaveActionExecute(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if SaveButton.CanFocus then
    SaveButton.SetFocus;
  if EditingDataSet(BillingAdjustment) then begin
    BillingAdjustment.UpdateRecord;
    ValidateFields;
    if BillingAdjustment.State = dsInsert then
      FLastAdjustmentDate := BillingAdjustment.FieldByName('adjustment_date').AsDateTime;
    PostDataSet(BillingAdjustment);
  end;
  DM.Engine.SendBillingAdjustments;
  BillingAdjustment.Refresh;
end;

procedure TBillingAdjustmentDetailsForm.ValidateFields;
begin
  if (BillingAdjustment.FieldByName('customer_name').AsString = '') then
    raise EOdEntryRequired.Create('Customer must be specified');
  if (BillingAdjustment.FieldByName('type').AsString = '') then
    raise EOdEntryRequired.Create('Type must be specified');
  if (not IsCommentData) and (BillingAdjustment.FieldByName('amount').AsCurrency = 0) then
    raise EOdEntryRequired.Create('Amount must be specified');
  if (BillingAdjustment.FieldByName('adjustment_date').IsNull) then
    raise EOdEntryRequired.Create('Adjustment date must be specified');
  if (BillingAdjustment.FieldByName('description').AsString = '') then
    raise EOdEntryRequired.Create('Description must be specified');
end;

procedure TBillingAdjustmentDetailsForm.BillingAdjustmentAfterOpen(DataSet: TDataSet);
begin
  DM.SetDateFieldGetTextEvents(DataSet);
  DM.SetRefFieldGetSetTextEvents(Dataset.FieldByName('type'), 'adjtype');
end;

procedure TBillingAdjustmentDetailsForm.BillingAdjustmentBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('customer_name', BillingAdjustment, 'customer_id',
    DM.Customers, 'customer_id', 'customer_name', 40, 'CustomerNameField');
  DM.Engine.LinkEventsAutoInc(BillingAdjustment);
end;

procedure TBillingAdjustmentDetailsForm.LeavingNow;
begin
  inherited;
  CancelDataSet(BillingAdjustment);
end;

procedure TBillingAdjustmentDetailsForm.DeleteAdjustmentActionExecute(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to delete the selected billing adjustment?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    EditDataSet(BillingAdjustment);
    BillingAdjustment.FieldByName('active').AsBoolean := False;
    PostDataSet(BillingAdjustment);
    DM.Engine.SendBillingAdjustments;
    BillingAdjustment.Refresh;
    FOnDone(Self);
  end;
end;

procedure TBillingAdjustmentDetailsForm.CustomerIDPropertiesChange(
  Sender: TObject);
begin
  inherited;
  // The User changed it
  PostMessage(Handle, UM_UPDATECOMBOS, 0, 0);
end;

procedure TBillingAdjustmentDetailsForm.UMUpdateCombos(var Msg: TMessage);
begin
  UpdateDependentDropdowns(True);
end;

procedure TBillingAdjustmentDetailsForm.UpdateDependentDropdowns(UserChanged: Boolean);

  procedure SetupDependentCombo(const CustomerID: Integer; Combo: TDBComboBox);
  var
    ExpectedValue, NewValue: string;
    D: TDataSet;
  begin
    D := Combo.DataSource.DataSet;
    ExpectedValue := D.FieldByName(Combo.DataField).AsString;
    if CustomerID < 1 then begin
      Combo.Items.Clear;
      Combo.Enabled := False;
    end
    else begin
      DM.CustomerInvoiceList(Combo.Items, CustomerID);
      Combo.Enabled := True;
      D.DisableControls;
      D.EnableControls;
      if (ExpectedValue<>'') and (Combo.Text='') then begin
        if UserChanged then begin
          Assert(D.State in dsEditModes, 'Should be in edit mode, if changing combo');
          D.FieldByName(Combo.DataField).AsString := '';
          ExpectedValue := '';
        end
        else begin
          ShowMessage('Warning - the values for Customer and Which Invoice do not match; '
           + ExpectedValue +  ' is not in the dropdown list');
        end;
      end;
    end;

    NewValue := D.FieldByName(Combo.DataField).AsString;
    Assert(ExpectedValue = NewValue,
      'Internal error, data loss in combo box handling, ' + Combo.DataField +
      ': ' + ExpectedValue + '/' + NewValue);
  end;

var
  CustomerID: Integer;
begin
  if BillingAdjustment.State in dsEditModes then
    BillingAdjustment.UpdateRecord;
  CustomerID := BillingAdjustment.FieldByName('customer_id').AsInteger;
  SetupDependentCombo(CustomerID, WhichInvoice);
  RefreshDataSet(GLCodes);
end;

function TBillingAdjustmentDetailsForm.IsCommentData: Boolean;
begin
  Result := BillingAdjustment.Active and SameText(AdjTypeComment, BillingAdjustment.FieldByName('type').AsString);
end;

function TBillingAdjustmentDetailsForm.IsCommentText: Boolean;
begin
  Result := BillingAdjustment.Active and SameText(AdjTypeComment, AdjustmentType.Text);
end;

procedure TBillingAdjustmentDetailsForm.AdjustmentTypeChange(Sender: TObject);
begin
  inherited;
  if IsCommentText and EditingDataSet(BillingAdjustment) then
    BillingAdjustment.FieldByName('amount').AsCurrency := 0;
end;

end.

