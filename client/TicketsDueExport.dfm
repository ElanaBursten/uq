inherited TicketsDueExportForm: TTicketsDueExportForm
  Left = 393
  Top = 198
  Caption = 'TicketsDueExportForm'
  ClientHeight = 202
  ClientWidth = 426
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 1
    Top = 11
    Width = 160
    Height = 13
    Caption = 'Include Tickets Due on or Before:'
  end
  object Label2: TLabel [1]
    Left = 1
    Top = 37
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label3: TLabel [2]
    Left = 49
    Top = 87
    Width = 337
    Height = 50
    AutoSize = False
    Caption = 
      'The date above is a point in time, so to include tickets due by ' +
      'the end of today, select tomorrow above.  This Report is for Exp' +
      'ort only and will NOT create a PDF.'
    WordWrap = True
  end
  object Label6: TLabel [3]
    Left = 2
    Top = 63
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [4]
    Left = 167
    Top = 34
    Width = 188
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
  end
  object EmployeeStatusCombo: TComboBox [5]
    Left = 167
    Top = 60
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object DateTimeSelect: TcxDateEdit [6]
    Left = 167
    Top = 8
    Properties.DateButtons = [btnNow, btnToday]
    Properties.Kind = ckDateTime
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 2
    Width = 153
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
    Top = 369
  end
end
