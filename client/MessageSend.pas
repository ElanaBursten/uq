unit MessageSend;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, EmployeeSelectFrame, StdCtrls, ComCtrls,
  ActnList, UQUtils, OdIsoDates, DMu, OdExceptions, QMServerLibrary_Intf,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, ExtCtrls,
  SharedDevExStyles, dxCore, cxDateUtils, Buttons, hteditactions, htmlcomp,
  htmledit, Menus, cxButtons, ImgList, OleCtrls, SHDocVw;

type
  TMessageSendForm = class(TEmbeddableForm)
    PageControl: TPageControl;
    AddMessageTab: TTabSheet;
    ConfirmTab: TTabSheet;
    lblMessageNumber: TLabel;
    MessageNumber: TEdit;
    MessageSentButton: TButton;
    DeliveryGroup: TGroupBox;
    lblSendTo: TLabel;
    lblDestination: TLabel;
    lblMessageDesc: TLabel;
    SendTo: TEmployeeSelect;
    Destination: TEdit;
    MessageGroup: TGroupBox;
    BottomPanel: TPanel;
    SendMessageButton: TButton;
    lblShowOn: TLabel;
    ShowDate: TcxDateEdit;
    lblExpires: TLabel;
    ExpirationDate: TcxDateEdit;
    MessageStatus: TLabel;
    lblDestinationDesc: TLabel;
    ImageListActions: TImageList;
    BodyPageControl: TPageControl;
    FormattedBody: TTabSheet;
    HTML: TTabSheet;
    MemoHTML: TMemo;
    HTMLButtonPanel: TPanel;
    btnBold: TSpeedButton;
    btnItalic: TSpeedButton;
    btnUnderline: TSpeedButton;
    btnStrikethrough: TSpeedButton;
    btnLeftAlign: TSpeedButton;
    btnMiddleAlign: TSpeedButton;
    btnRightAlign: TSpeedButton;
    btnNumber: TSpeedButton;
    btnBullet: TSpeedButton;
    lblTextColor: TLabel;
    lblHighlight: TLabel;
    lblFont: TLabel;
    Label2: TLabel;
    btnFont: THtFontCombo;
    BtnFontSize: THtFontSizeCombo;
    btnFontColor: THtTextColorCombo;
    btnFontHighlight: THtBgColorCombo;
    ActionList1: TActionList;
    HtActionFontBold: THtActionFontBold;
    HtActionFontItalic: THtActionFontItalic;
    HtActionFontUnderline: THtActionFontUnderline;
    HtActionFontStrikeout: THtActionFontStrikeout;
    HtActionAlignLeft: THtActionAlignLeft;
    HtActionAlignRight: THtActionAlignRight;
    HtActionAlignCenter: THtActionAlignCenter;
    HtActionOrderedList: THtActionOrderedList;
    HtActionUnorderedList: THtActionUnorderedList;
    SendMessageAction: TAction;
    HtmlEditor1: THtmlEditor;
    HtActionCopy1: THtActionCopy;
    HtActionPaste1: THtActionPaste;
    HtActionCut1: THtActionCut;
    HtActionUndo1: THtActionUndo;
    HtActionRedo1: THtActionRedo;
    HtActionSubscript1: THtActionSubscript;
    HtActionSuperscript1: THtActionSuperscript;
    HtActionIncreaseIndent1: THtActionIncreaseIndent;
    HtActionDecreaseIndent1: THtActionDecreaseIndent;
    btnCut: TSpeedButton;
    btnIncreaseIndent: TSpeedButton;
    btnDecreaseIndent: TSpeedButton;
    btnUndo: TSpeedButton;
    btnRedo: TSpeedButton;
    btnSubscript: TSpeedButton;
    btnSuperscript: TSpeedButton;
    pnlSubjectLine: TPanel;
    SubjectLabel: TLabel;
    Subject: TEdit;
    HTMLPanel: TPanel;
    btnImportHTMLFile: TSpeedButton;
    HTMLTextActionList: TActionList;
    MakeHTMLEditableAction: TAction;
    btnhtmledit: TcxButton;
    PasteHTMLbtn: TSpeedButton;
    PasteHTMLAction: TAction;
    btnExportSaveHTMLFile: TSpeedButton;
    SaveHTMLToFileAction: TAction;
    ImportHTMLFileAction: TAction;
    FileOpenDialog1: TFileOpenDialog;
    FileSaveDialog1: TFileSaveDialog;
    btnCopyToClipboard: TSpeedButton;
    CopyHTMLToClipboardAction: TAction;
    btnCutHTML: TSpeedButton;
    CutHTMLAction: TAction;
    HtActionAddUrl1: THtActionAddUrl;
    URLBtn: TSpeedButton;
    HTActionRemoveURL: TAction;
    ViewIt: TTabSheet;
    WebBrowser1: TWebBrowser;
    SpeedButton1: TSpeedButton;
    procedure SendMessageActionExecute(Sender: TObject);
    procedure MessageSentButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RefreshView;
    procedure btnEditHTMLClick(Sender: TObject);
    procedure btnHTMLEditClick(Sender: TObject);
    procedure btnImportHTMLFileClick(Sender: TObject);
    procedure PasteHTMLActionExecute(Sender: TObject);
    procedure MakeHTMLEditableActionExecute(Sender: TObject);
    procedure ImportHTMLFileActionExecute(Sender: TObject);
    procedure SaveHTMLToFileActionExecute(Sender: TObject);
    procedure CopyHTMLToClipboardActionExecute(Sender: TObject);
    procedure MemoHTMLChange(Sender: TObject);
    procedure CutHTMLActionExecute(Sender: TObject);
    procedure BodyPageControlChange(Sender: TObject);
    procedure HtActionAddUrl1Execute(Sender: TObject);   //QM-1008 EB HTML fix
    procedure WebBrowser1BeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure SpeedButton1Click(Sender: TObject);   //QM-1008 EB HTML fix
  private
    fNeedsRefresh: Boolean;
    fMessageLoaded: Boolean;  //QM-1008 EB Tracking for the VIewer
    procedure ValidateFields;
    procedure ClearFields;
    procedure CheckForExistingMessage;
    procedure EnableHTMLEdit(Value: Boolean);
    function GetLink(Existinglink: string = ''): string;  //QM-1008 EB HTML fix
  public
    procedure ActivatingNow; override;
  end;

implementation

uses LocalEmployeeDMu, SimpleLinkDlg,
   htmlpars, OdBrowserUtils, OdMiscUtils;

{$R *.dfm}

{ TMessageSendForm }

procedure TMessageSendForm.ActivatingNow;
begin
  inherited;
  fNeedsRefresh := False;
  EmployeeDM.ManagerListMessages(SendTo.EmployeeCombo.Items);
  ClearFields;
  PageControl.ActivePage := AddMessageTab;
  BodyPageControl.ActivePage := FormattedBody;
  fMessageLoaded := False;    //QM-1008 EB HTML fix
end;

procedure TMessageSendForm.ClearFields;
begin
  SendTo.SelectFirstItem;
  Destination.Text := 'All reporting to ' + SendTo.SelectedText;
  ShowDate.Clear;
  Subject.Clear;
  HTMLEditor1.HTML.Clear;
  ExpirationDate.Clear;
end;

procedure TMessageSendForm.CopyHTMLToClipboardActionExecute(Sender: TObject);
begin
  inherited;
  MemoHTML.CopyToClipboard;
end;

procedure TMessageSendForm.CutHTMLActionExecute(Sender: TObject);
begin
  inherited;
  MemoHTML.CutToClipboard;
end;

procedure TMessageSendForm.EnableHTMLEdit(Value: Boolean);
begin
  MemoHTML.Enabled := Value;
  MemoHTML.ReadOnly := not Value;
  btnImportHTMLFile.Enabled := Value;
  PasteHTMLbtn.Enabled := Value;
  btnCopyToClipboard.Enabled := Value;
  btnCutHTML.Enabled := Value;
  btnhtmledit.Enabled := not Value;

end;

procedure TMessageSendForm.SaveHTMLToFileActionExecute(Sender: TObject);
var
  DTString: string;
begin
  inherited;
  FileSaveDialog1.DefaultFolder := ExtractFilePath(Application.ExeName);
  DateTimeToString(DTString, 'YYMMDDHHNN', Now);
  FileSaveDialog1.FileName := 'QM_Message_' + DTString;
  If FileSaveDialog1.Execute then begin
    MemoHTML.Lines.SaveToFile(FileSaveDialog1.FileName);
  end;
end;

procedure TMessageSendForm.SendMessageActionExecute(Sender: TObject);
var
  HTMLstr: string;
begin
  SendMessageAction.Enabled := False;
  try
    ValidateFields;
    CheckForExistingMessage;
    MessageNumber.Text := FormatDateTime('yyyymmddhhnnss', Now);

    HTMLstr := HTMLEditor1.Doc.OuterHTML;    //QM-454 EB Use the HTMLEditor1 text
    MessageStatus.Caption := DM.SendMessage(MessageNumber.Text, Destination.Text,
                                            Subject.Text, HTMLstr,
                                            ShowDate.Date, ExpirationDate.Date,
                                            SendTo.SelectedID);
    PageControl.ActivePage := ConfirmTab;
    ClearFields;
  finally
    SendMessageAction.Enabled := True;
  end;
end;

procedure TMessageSendForm.SpeedButton1Click(Sender: TObject);
var
  HREF1: string;
  s ,st :string;
   A: TElement;
  link: string;
  existinglink: boolean;
  startpos, endpos, tagstartpos: integer;
  OrigText, OrigHTML: string;

begin
  inherited;
  startpos := 0;
  endpos := 0;
//  HTMLEditor1.UntagSelection('a');
 // HTMLEditor1.Doc.Selection.Clear;

   A := TElement(HTMLEditor1.Current.UpTo('a'));
 // HTMLEditor1.SelectWordAtCursor;
  if not HtmlEditor1.Doc.Selection.Empty then
  begin
    HTmlEditor1.Selection.Clear;
//    OrigHTML := HTMLEditor1.Doc.GetSelectedHTML;
//    OrigText := HTMLEditor1.Doc.Selection.Text;
//      HtmlEditor1.DeleteSelection();
   // A := TElement(HTMLEditor1.Current.UpTo(OrigHTML));
   // A := HTMLEditor1.AddHTMLAtCursor(OrigText) ;
//
//      startpos := HtmlEditor1.Doc.Selection.StartPos;
//      HtmlEditor1.Doc.ReplaceWord(OrigHTML, OrigText);


   // HTMLEditor1.Doc.InsertHTML('ppppp');
  //  endpos := startpos + length(OrigText);
//    tagstartpos := startpos;
//    while tagstartpos > 0 do begin
//      if HTMLEditor1.Doc. then
//
//    end;
   // ShowMessage(OrigText + ' ' + IntToStr(StartPos));
//
//    HtmlEditor1.DocChanged(HtmlEditor1.Doc.Selection.TopElement);
//    s := 'href="' + Link + '"';
//    HtmlEditor1.UnTagSelection('a');
//        HtmlEditor1.CopySelectiontoClipboard;
  end;
end;

procedure TMessageSendForm.btnHTMLEditClick(Sender: TObject);
begin
  inherited;
  EnableHTMLEdit(True);
end;

procedure TMessageSendForm.btnImportHTMLFileClick(Sender: TObject);
begin
  inherited;
  EnableHTMLEdit(False);
end;

procedure TMessageSendForm.ValidateFields;

  function IsEmpty(const Value: string): Boolean;
  begin
    Result := Trim(Value) = '';
  end;

begin
  if IsEmpty(Destination.Text) then
  begin
    Destination.SetFocus;
    raise EOdDataEntryError.Create('Please enter a descriptive destination');
  end;
  if SendTo.SelectedID < 1 then
  begin
    SendTo.EmployeeCombo.SetFocus;
    raise EOdDataEntryError.Create('Please select a group to send the message to');
  end;
  if ShowDate.Date <= 0 then
  begin
    ShowDate.SetFocus;
    raise EOdDataEntryError.Create('Please select a date for this message to be shown');
  end;
  if IsEmpty(Subject.Text) then
  begin
    Subject.SetFocus;
    raise EOdDataEntryError.Create('Please enter a subject');
  end;
  if IsEmpty(HTMLEditor1.Doc.OuterHTML) then
  begin
    HTMLEditor1.SetFocus;
    raise EOdDataEntryError.Create('Please enter the message body');
  end;
end;

procedure TMessageSendForm.WebBrowser1BeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);   //QM-1008 & QM-1014 EB HTML fix
begin
  inherited;
  if not fMessageLoaded then exit;
    
    Cancel := True;
    if URL <> '' then begin
      BrowseURLSpecified(URL, 'chrome.exe'); //QM-1008 & QM-1014 EB HTML fix
    end;
end;

procedure TMessageSendForm.MakeHTMLEditableActionExecute(Sender: TObject);
begin
  inherited;
  EnableHTMLEdit(True);
end;

procedure TMessageSendForm.MemoHTMLChange(Sender: TObject);
begin
  inherited;
  if MemoHtml.Enabled then
    fNeedsRefresh := True;
end;



procedure TMessageSendForm.MessageSentButtonClick(Sender: TObject);
begin
  PageControl.ActivePage := AddMessageTab;
end;

procedure TMessageSendForm.PasteHTMLActionExecute(Sender: TObject);
begin
  inherited;
  if (PageControl.ActivePage = AddMessageTab) and (MemoHTML.Enabled) then
    MemoHTML.PasteFromClipboard;
end;

procedure TMessageSendForm.RefreshView;
var
  PrevHTML: string;
begin
  PrevHTML := HTMLEditor1.Doc.OuterHTML;
  HTMLEditor1.HTML.Clear;
  try
    HTMLEditor1.HTML.Text := MemoHTML.Text;
  except
    MessageDlg('Invalid HTML', mterror, [mbOK],0);
    HTMLEditor1.HTML.Text := '';
  end;
  fNeedsRefresh := False;
end;


procedure TMessageSendForm.BodyPageControlChange(Sender: TObject);
begin
  inherited;
   if (BodyPageControl.ActivePage = FormattedBody) then begin
     if fNeedsRefresh then
       RefreshView;
    EnableHTMLEdit(False);
   end
   else if (BodyPageControl.ActivePage = HTML) then begin
     MemoHTML.Lines.clear;
     MemoHTML.Lines.Add(HTMLEditor1.Doc.OuterHtml);
   end
   else if (BodyPageControl.ActivePage = Viewit) then begin     //QM-1008 EB HTML fix
     LoadHTML(WebBrowser1, HTMLEditor1.Doc.OuterHTML);
     fMessageLoaded := True;
   //  LoadHtml(WebBrowser1, HTML.text);
   end
end;

procedure TMessageSendForm.btnEditHTMLClick(Sender: TObject);
begin
  inherited;
  EnableHTMLEdit(True);
end;

procedure TMessageSendForm.CheckForExistingMessage;
var
  Params: NVPairList;
  XmlString: string;
  Response: Integer;
begin
  Params := NVPairList.Create;
  try
    AddParam(Params, 'destination', Destination.Text);
    AddParam(Params, 'show_date_start', IsoDateTimeToStr(ShowDate.Date));
    AddParam(Params, 'show_date_end', IsoDateTimeToStr(ShowDate.Date+1));
    DM.CallingServiceName('MessageSearch');
    XmlString := DM.Engine.Service.MessageSearch(DM.Engine.SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
  if DM.GetSearchResultsRecordCount(XmlString) > 0 then begin
    Response := MessageBox(Application.Handle, 'A message already exists for the ' +
      'selected day and destination.'#13'Do you want to continue sending this one?',
      'Confirm Multiple Send', MB_YESNO or MB_ICONWARNING);
    if Response = IDNO then
      Abort;
  end;
end;

procedure TMessageSendForm.FormCreate(Sender: TObject);
begin
  inherited;
  //We are assigning the glyph manually because the dfm's may produce different
  //binary image data based on the bit settings of the individual developer's monitor.
  SharedDevExStyleData.SetGlyph(ExpirationDate, 0);
  SharedDevExStyleData.SetGlyph(ShowDate, 0);
end;

function TMessageSendForm.GetLink(Existinglink: string = ''): string;    //QM-1008 EB HTML fix
  var
    LinkDlg: TDlgSimpleLink;
begin
    LinkDlg := TDlgSimpleLink.Create(Self);
    LinkDlg.EdtLink.Text := ExistingLink;
  try
    If (LinkDlg.ShowModal = mrOK) then
      Result := LinkDlg.MyLink;
  finally
    FreeAndNil(LinkDlg);
  end;
end;

procedure TMessageSendForm.HtActionAddUrl1Execute(Sender: TObject);
const
  defLink = 'https://en.wikipedia.org/wiki/J._Robert_Oppenheimer';   //for testing only
var
  HREF1: string;
  s ,Orig, OrigHTML :string;
   A: TElement;
  prevLink, link: string;
  existinglink: boolean;
  startpos: integer;
  StartElement: TElement;
begin
 // inherited;     {QM-1020 EB Fix Mesasge links - inherited was prepending some URLs with about}
   {QM-1008 and QM-1020 EB HTML Link - put in the Editor}
   startpos := 0;
   PrevLink := '';
   A := TElement(HTMLEditor1.Current.UpTo('a'));
  if not HtmlEditor1.Doc.Selection.Empty then
  begin
    Orig := HTMLEditor1.Doc.GetSelectedText;   {Just the text portion}
    OrigHTML := HTMLEditor1.Doc.GetSelectedHTML;  {full html around selection}
    startpos := ansipos(OrigHTML, HTMLEditor1.Doc.OuterHTML);

    {Clear if there is already an existing link on the text}
    if Orig <> OrigHTML then begin
       s := OrigHTML;
      //HtmlEditor1.UnTagSelection('a', s);
      HtmlEditor1.DeleteSelection();
      HtmlEditor1.Doc.Selection.Clear;
      HTMLEditor1.AddHTMLAtCursor(Orig);

      {QM-1008 EB:Leaving this commented block to see if I can eventually get
        text reselected and a new link replaced.  Right now, the selection does
        not work}
     // PrevLink := ParseLink(OrigHTML);

      {Reselect the text}
   //   HTMLEditor1.SelectAll;
  //   if Assigned(StartElement) and StartElement.IsText then
       //   StartPos := TTextElement(StartElement).FirstChar

    //    HTMLEditor1.Doc.Selection.StartPos := startpos;
    //  HTMLEditor1.Doc.Selection.EndPos := startpos + length(Orig);
    //  HTMLEditor1.Doc.Selection.SaveSelection;
    //  HTMLEditor1.Repaint;   // ResetIMEInput;
    //         A := TElement(HTMLEditor1.Current.UpTo('a'));
    end

    else begin
      link := GetLink(PrevLink);
      if link <> '' then begin

        if ansipos('http', link) = 0 then begin       {QM-1020 EB Fix Mesasge links}
          if ansipos('//', link) = 0 then
            link := 'http://' + link
          else
            link := 'http' + link;
        end;

        s := 'href="' + Link + '"';
        HtmlEditor1.TagSelection('a', s);
            HtmlEditor1.CopySelectiontoClipboard;
      end;
    end;
  end;
end;

procedure TMessageSendForm.ImportHTMLFileActionExecute(Sender: TObject);
var
  BackupStr, ImportFileName: string;
begin
  inherited;
  BackupStr := MemoHTML.Lines.Text;
  FileOpenDialog1.DefaultFolder := ExtractFilePath(Application.ExeName);
  If FileOpenDialog1.Execute then begin
    ImportFileName := FileOpenDialog1.FileName;
    MemoHTML.Clear;
    MemoHTML.Lines.LoadFromFile(ImportFileName);
  end;
  fNeedsRefresh := True;
end;


end.
