inherited DamageSearchCriteria: TDamageSearchCriteria
  Left = 223
  Top = 152
  Caption = 'Damage Search'
  ClientHeight = 328
  ClientWidth = 692
  Font.Charset = ANSI_CHARSET
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object InvestigatorLabel: TLabel [0]
    Left = 20
    Top = 108
    Width = 93
    Height = 13
    Caption = 'Investigator Name:'
  end
  object TicketNumberLabel: TLabel [1]
    Left = 20
    Top = 12
    Width = 72
    Height = 13
    Caption = 'Ticket Number:'
  end
  object ExcavatorLabel: TLabel [2]
    Left = 20
    Top = 132
    Width = 101
    Height = 13
    Caption = 'Excavator Company:'
  end
  object CityLabel: TLabel [3]
    Left = 329
    Top = 60
    Width = 23
    Height = 13
    Caption = 'City:'
  end
  object LocationLabel: TLabel [4]
    Left = 329
    Top = 36
    Width = 44
    Height = 13
    Caption = 'Location:'
  end
  object UQDamageIDLabel: TLabel [5]
    Left = 20
    Top = 36
    Width = 57
    Height = 13
    Caption = 'Damage ID:'
  end
  object TypeLabel: TLabel [6]
    Left = 20
    Top = 60
    Width = 101
    Height = 13
    Caption = 'Investigation Status:'
  end
  object StateLabel: TLabel [7]
    Left = 329
    Top = 84
    Width = 30
    Height = 13
    Caption = 'State:'
  end
  object ProfitCenterLabel: TLabel [8]
    Left = 20
    Top = 84
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object UtilityCoDamagedLabel: TLabel [9]
    Left = 20
    Top = 204
    Width = 99
    Height = 13
    Caption = 'Utility Co. Damaged:'
  end
  object ClientClaimIDLabel: TLabel [10]
    Left = 20
    Top = 180
    Width = 73
    Height = 13
    Caption = 'Client Claim ID:'
  end
  inherited RecordsLabel: TLabel
    Left = 317
    Top = 273
  end
  object WorkToBeDoneLabel: TLabel [12]
    Left = 329
    Top = 12
    Width = 87
    Height = 13
    Caption = 'Work To Be Done:'
  end
  object AttachmentsLabel: TLabel [13]
    Left = 20
    Top = 228
    Width = 65
    Height = 13
    Caption = 'Attachments:'
  end
  object ClaimStatusLabel: TLabel [14]
    Left = 20
    Top = 252
    Width = 63
    Height = 13
    Caption = 'Claim Status:'
  end
  object InvoicesLabel: TLabel [15]
    Left = 20
    Top = 276
    Width = 44
    Height = 13
    Caption = 'Invoices:'
  end
  object InvoiceCodeLabel: TLabel [16]
    Left = 20
    Top = 300
    Width = 67
    Height = 13
    Caption = 'Invoice Code:'
  end
  object Label3: TLabel [17]
    Left = 330
    Top = 222
    Width = 100
    Height = 33
    AutoSize = False
    Caption = 'Only Include Responsbilities:'
    WordWrap = True
  end
  object ExcvTypeLabel: TLabel [18]
    Left = 20
    Top = 156
    Width = 84
    Height = 13
    Caption = 'Excavation Type:'
  end
  object LocatorLabel: TLabel [19]
    Left = 329
    Top = 108
    Width = 70
    Height = 13
    Caption = 'Locator Name:'
  end
  inherited SearchButton: TButton
    Left = 303
    Top = 291
    TabOrder = 22
  end
  inherited CancelButton: TButton
    Left = 625
    Top = 291
    TabOrder = 25
  end
  inherited CopyButton: TButton
    Left = 383
    Top = 291
    TabOrder = 23
  end
  inherited ExportButton: TButton
    Left = 463
    Top = 291
    TabOrder = 24
  end
  inherited PrintButton: TButton
    TabOrder = 27
  end
  inherited ClearButton: TButton
    Left = 544
    Top = 291
    TabOrder = 28
  end
  object TicketNumber: TEdit
    Left = 124
    Top = 8
    Width = 156
    Height = 21
    TabOrder = 0
  end
  object Investigator: TEdit
    Left = 124
    Top = 104
    Width = 156
    Height = 21
    TabOrder = 4
  end
  object Excavator: TEdit
    Left = 124
    Top = 128
    Width = 156
    Height = 21
    TabOrder = 5
  end
  object City: TEdit
    Left = 423
    Top = 56
    Width = 198
    Height = 21
    TabOrder = 14
  end
  object Location: TEdit
    Left = 423
    Top = 32
    Width = 197
    Height = 21
    TabOrder = 13
  end
  object UQDamageID: TEdit
    Left = 124
    Top = 32
    Width = 156
    Height = 21
    TabOrder = 1
  end
  object DamageType: TComboBox
    Left = 124
    Top = 56
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      ''
      'INCOMING'
      'PENDING'
      'PENDING APPROVAL'
      'APPROVED')
  end
  inline DamageDate: TOdRangeSelectFrame
    Left = 311
    Top = 125
    Width = 310
    Height = 50
    TabOrder = 16
    inherited FromLabel: TLabel
      Left = 19
      Top = 0
      Width = 61
      Height = 26
      Caption = 'Damage Date Range:'
      WordWrap = True
    end
    inherited ToLabel: TLabel
      Left = 203
      Top = 6
    end
    inherited DatesLabel: TLabel
      Left = 19
      Top = 31
    end
    inherited DatesComboBox: TComboBox
      Left = 112
      Top = 27
      Width = 198
      DropDownCount = 14
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 112
      Top = 2
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 224
      Top = 2
    end
  end
  object State: TComboBox
    Left = 423
    Top = 80
    Width = 198
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 15
  end
  object ProfitCenter: TComboBox
    Left = 124
    Top = 80
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 3
  end
  inline DueDate: TOdRangeSelectFrame
    Left = 311
    Top = 173
    Width = 310
    Height = 52
    TabOrder = 17
    inherited FromLabel: TLabel
      Left = 18
      Top = 7
      Width = 83
      Caption = 'Due Date Range:'
    end
    inherited ToLabel: TLabel
      Left = 203
      Top = 6
    end
    inherited DatesLabel: TLabel
      Left = 19
      Top = 29
    end
    inherited DatesComboBox: TComboBox
      Left = 112
      Top = 27
      Width = 198
      DropDownCount = 14
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 112
      Top = 3
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 224
      Top = 3
    end
  end
  object ClientClaimID: TEdit
    Left = 124
    Top = 176
    Width = 156
    Height = 21
    TabOrder = 7
  end
  object WorkToBeDone: TEdit
    Left = 423
    Top = 8
    Width = 197
    Height = 21
    TabOrder = 12
  end
  object UQResp: TCheckBox
    Left = 423
    Top = 229
    Width = 123
    Height = 17
    Caption = 'CompanyShortName Goes Here'
    TabOrder = 18
  end
  object EXResp: TCheckBox
    Left = 542
    Top = 229
    Width = 74
    Height = 17
    Caption = 'Excavator'
    TabOrder = 19
  end
  object UtilityResp: TCheckBox
    Left = 622
    Top = 229
    Width = 50
    Height = 17
    Caption = 'Utility'
    TabOrder = 20
  end
  object Attachments: TComboBox
    Left = 124
    Top = 224
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 8
  end
  object ClaimStatus: TComboBox
    Left = 124
    Top = 248
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 9
  end
  object IncludeEstimates: TCheckBox
    Left = 330
    Top = 252
    Width = 152
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Include Current Estimate: '
    TabOrder = 21
  end
  object Invoices: TComboBox
    Left = 124
    Top = 272
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 10
  end
  object InvoiceCode: TComboBox
    Left = 124
    Top = 296
    Width = 156
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 11
  end
  object ExcavationType: TComboBox
    Left = 124
    Top = 152
    Width = 156
    Height = 21
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 6
  end
  object Locator: TEdit
    Left = 423
    Top = 104
    Width = 198
    Height = 21
    TabOrder = 26
  end
  object cbUtilityCo: TcxComboBox
    Left = 125
    Top = 201
    Style.BorderColor = clWindowFrame
    Style.Color = clWindow
    TabOrder = 29
    Width = 155
  end
  object cbQuickClose: TCheckBox
    Left = 488
    Top = 252
    Width = 113
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Quick Close Eligible'
    TabOrder = 30
    Visible = False
  end
  object btnQuickClose: TButton
    Left = 609
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Quick Close'
    TabOrder = 31
    Visible = False
    OnClick = btnQuickCloseClick
  end
end
