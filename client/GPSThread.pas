unit GPSThread;

interface

uses
  Classes, SysUtils, Windows, Messages, ComObj, BackgroundWorker, UQGPS,
  ActiveX;

const
  // Message Id used by background GPS thread communications
  UM_GPS_STATUS = WM_USER + 1011;
  DEFAULT_GPS_POLL_SECONDS = 5;  // seconds bt GPS device checks
  DEFAULT_LOG_SAVE_SECONDS = 60; // seconds bt log file saves to disk
  MAX_LOG_LINES = 10000; // max number of lines before creating a new log file

type
  EGPSError = class(Exception);

  TGPSStatus = class(TWorkerStatus)
  public
    HavePosition: Boolean;
    Latitude: Double;
    Longitude: Double;
    HDOP: Integer;
  end;

  TBackgroundGPSThread = class(TBackgroundWorker)
  private
    FGPS: TGPSInformation;
    FGPSLog: TStringList;
    FNextRunTime: TDateTime;
    FDebug: Boolean;
    FDataDir: string;
    FLastGPSConn: Integer;
    FNextLogSaveTime: TDateTime;
    FGPSLogName: string;
    FGPSPollSecs: Integer; // time in seconds to wait between asking the GPS device for info

    function TimeToRun: Boolean;
    procedure ComputeNextLogSaveTime;
    function TimeToSaveLog: Boolean;
    procedure Log(Sender: TObject; const LogMessage: string);
    procedure SaveLogToDisk(const StartNewLog: Boolean=False);
    procedure CheckConnectionStatus;
    procedure SetupGPSObjects;
    procedure UpdateStatus(const Msg: string; GPSPosition: TGPSPositionInfo);
  protected
    function AnyWorkToDo: Boolean; override;
    procedure BeforeWork; override;
    procedure AfterWork; override;
    procedure PerformWork; override;
    procedure Setup; override;
    procedure Cleanup; override;
  public
    FGPSPort: Integer;     // the port to look for the GPS Device
    constructor Create(ParentHandle: HWND; Debug: Boolean; const DataDir: string; GPSPollSecs: Integer; GPSPort: Integer);
  end;

implementation

uses
  StdConvs, DateUtils, QMConst, OdLog, OdMiscUtils;

{ TBackgroundGPSThread }

constructor TBackgroundGPSThread.Create(ParentHandle: HWND; Debug: Boolean;
  const DataDir: string; GPSPollSecs: Integer; GPSPort: Integer);
begin
  inherited Create;
  FDebug := Debug;
  FDataDir := DataDir;
  if GPSPollSecs < 1 then
    FGPSPollSecs := DEFAULT_GPS_POLL_SECONDS
  else
    FGPSPollSecs := GPSPollSecs;
  FGPSPort := GPSPort;
  FNextRunTime := Now;
  FNextLogSaveTime := Now;

  RegisterSupervisor(ParentHandle); // this Resumes the thread
end;

procedure TBackgroundGPSThread.Setup;
begin
  inherited;
  CoInitialize(nil);
  SetupGPSObjects;
end;

procedure TBackgroundGPSThread.Cleanup;
begin
  Log(Self, 'GPS worker is shutting down');
  SaveLogToDisk;
  FreeAndNil(FGPS);
  FreeAndNil(FGPSLog);
  CoUninitialize;

  inherited;
end;

procedure TBackgroundGPSThread.SetupGPSObjects;
begin
  FGPSLog := TStringList.Create;
  FLastGPSConn := -1;
  FGPSLogName := '';
  try
    FGPS := TGPSInformation.Create(duFeet, FGPSPort);
    FGPS.OnLog := Log;
  except
    on E: EOleSysError do begin
      E.Message := 'Unable to create the GPS tracking object. ' +
        'You may need to register GPSToolsXP230.dll first using regsvr32. ' +
        E.Message;
      Log(Self, E.Message);
      UpdateStatus(E.Message, nil);
      raise;
    end;
    on E: Exception do begin
      E.Message := 'Unable to create the GPS tracking object. ' + E.Message;
      Log(Self, E.Message);
      UpdateStatus(E.Message, nil);
      raise;
    end;
  end;
end;

function TBackgroundGPSThread.AnyWorkToDo: Boolean;
begin
  Result := (not Terminated) and TimeToRun;
end;

procedure TBackgroundGPSThread.BeforeWork;
begin
  inherited;

  if TimeToSaveLog then begin
    SaveLogToDisk(False);
    ComputeNextLogSaveTime;
  end;
end;

procedure TBackgroundGPSThread.PerformWork;
var
  GPSPos: TGPSPositionInfo;
begin
  Assert(Assigned(FGPS), 'No GPS assigned in TBackgroundGPSThread.Execute');
  try
    if not FGPS.Enabled then
      FGPS.Enabled := True;
    CheckConnectionStatus;
    if (not Assigned(FGPS)) then
      raise EGPSError.Create('GPS object is no longer assigned');

    GPSPos := FGPS.GetMyPosition;
    UpdateStatus('', GPSPos);
  except
    on E: Exception do begin
      if FDebug then
        GetLog.LogException(E);
      PostErrorMessage(E);
    end;
  end;
end;

procedure TBackgroundGPSThread.AfterWork;
begin
  inherited;
  FNextRunTime := IncSecond(Now, FGPSPollSecs);
end;

procedure TBackgroundGPSThread.CheckConnectionStatus;
var
  CurrConn: Integer;
begin
  if not Assigned(FGPS) then begin
    Log(Self, 'CheckConnectionStatus - GPS object is not assigned');
    Exit;
  end;

  CurrConn := FGPS.ComPort;
  if FLastGPSConn <> CurrConn then begin
    if not FGPS.Enabled then
      Log(Self, 'Disconnected from GPS')
    else
      Log(Self, 'Connected device ' + IntToStr(FGPS.ComPort));
    FLastGPSConn := FGPS.ComPort;
  end;
end;

procedure TBackgroundGPSThread.ComputeNextLogSaveTime;
begin
  FNextLogSaveTime := IncSecond(Now, DEFAULT_LOG_SAVE_SECONDS);
end;

function TBackgroundGPSThread.TimeToSaveLog: Boolean;
begin
  Result := (not Terminated) and (FNextLogSaveTime > 0) and (FNextLogSaveTime <= Now);
end;

procedure TBackgroundGPSThread.SaveLogToDisk(const StartNewLog: Boolean=False);
begin
  if not Assigned(FGPSLog) then
    Exit;

  if FGPSLog.Count > 0 then begin
    if IsEmpty(FGPSLogName) then begin
      FGPSLogName := IncludeTrailingPathDelimiter(FDataDir) + GPSLogFilePrefix +
        FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.log';
    end;
    FGPSLog.SaveToFile(FGPSLogName);

    if StartNewLog then begin
      FGPSLogName := '';
      FGPSLog.Clear;
    end;
  end;
end;

procedure TBackgroundGPSThread.Log(Sender: TObject; const LogMessage: string);
begin
  if not Assigned(FGPSLog) then
    Exit;

  FGPSLog.Add(FormatDateTime('hh:nn:ss ', Now) + LogMessage);
  if FGPSLog.Count >= MAX_LOG_LINES then
    SaveLogToDisk(True);
end;

procedure TBackgroundGPSThread.UpdateStatus(const Msg: string; GPSPosition: TGPSPositionInfo);
var
  GPSStatus: TGPSStatus;
begin
  // GPSStatus must be freed by the message consumer
  GPSStatus := TGPSStatus.Create(ThreadID);
  GPSStatus.MessageText := Msg;
  GPSStatus.HavePosition := False;
  if Assigned(GPSPosition) then begin
    GPSStatus.Latitude := GPSPosition.Latitude;
    GPSStatus.Longitude := GPSPosition.Longitude;
    GPSStatus.HDOP := Trunc(GPSPosition.HDOP);
    GPSStatus.HavePosition := GPSPosition.HaveFix and GPSPosition.HavePosition;
  end;

  NotifySupervisor(UM_GPS_STATUS, GPSStatus);
end;

function TBackgroundGPSThread.TimeToRun: Boolean;
begin
  Result := (FNextRunTime > 0) and (FNextRunTime <= Now);
end;

end.
