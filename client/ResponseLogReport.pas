unit ResponseLogReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, OdRangeSelect;

type
  TResponseLogReportForm = class(TReportBaseForm)
    Label2: TLabel;
    Label3: TLabel;
    ClientList: TCheckListBox;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    CallCenterList: TCheckListBox;
    TransmitRange: TOdRangeSelectFrame;
    Label4: TLabel;
    SuccessfulResponses: TCheckBox;
    LimitClients: TCheckBox;
    FailedResponses: TCheckBox;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure CallCenterListClick(Sender: TObject);
    procedure LimitClientsClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, OdExceptions, ODMiscUtils;

{$R *.dfm}

procedure TResponseLogReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('ResponseLog');
  SetParamDate('StartDate', TransmitRange.FromDate);
  SetParamDate('EndDate', TransmitRange.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);

  CheckedItems := GetCheckedItemCodesString(CallCenterList);
  if IsEmpty(CheckedItems) then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one call center for the report.');
  end;

  SetParam('CallCenterList', CheckedItems);

  if LimitClients.Checked then begin
    CheckedItems := GetCheckedItemString(ClientList, True);
    if IsEmpty(CheckedItems) then begin
      ClientList.SetFocus;
      raise EOdEntryRequired.Create('Please select the clients to report on.');
    end;
    SetParam('ClientList', CheckedItems);
  end;

  SetParamBoolean('IncludeSuccess', SuccessfulResponses.Checked);
  SetParamBoolean('IncludeFailure', FailedResponses.Checked);
end;

procedure TResponseLogReportForm.InitReportControls;
begin
  inherited;
  TransmitRange.SelectDateRange(rsThisWeek);
  SetupCallCenterList(CallCenterList.Items, False);
  SetUpManagerList(ManagersComboBox);
  LimitClients.Checked := False;
  ManagersComboBox.Enabled := True;
  CallCenterList.Enabled := True;
  ClientList.Enabled := False;
  SuccessfulResponses.Checked := False;
  FailedResponses.Checked := True;
end;

procedure TResponseLogReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(ClientList, True);
end;

procedure TResponseLogReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(ClientList, False);
end;

procedure TResponseLogReportForm.CallCenterListClick(Sender: TObject);
var
  CallCenters: string;
begin
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure TResponseLogReportForm.LimitClientsClick(Sender: TObject);
begin
  ClientList.Enabled := LimitClients.Checked;
end;

end.

