unit LocateStatusQuestions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxScrollBox, StdCtrls, StatusFrame, DB, DMu, ExtCtrls;

type
  TLocStatusQuestions = class(TForm)
    QuestionScrollBox: TcxScrollBox;
    DataSource1: TDataSource;
    btnPanel: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    TopPanel: TPanel;
    InstructionsLabel: TLabel;
    procedure OKButtonClick(Sender: TObject);
  private
    fOldStatusCode: string;
    fNewStatusCode: string;
    fTicketID: integer;
    fClientID: integer;
    fLocatorID: integer;
    fCallCenter: string; {ticket_format}
    fFrameList: TList;
    fReturnCode: integer; //QM-604 EB Nipsco
  public
    property NewStatusCode : string read fNewStatusCode write  fNewStatusCode;
    property TicketID   : integer read fTicketID   write fTicketID;
    property ClientID   : integer read fClientID   write fClientID;
    property LocatorID:   integer read fLocatorID  write fLocatorID;
    property ReturnCode:  integer read fReturnCode write fReturnCode;

    procedure SetValues(pTicketID: Integer; pClientID, pLocatorID: Integer;
                        pOldStatusCode, pNewStatusCode: String; pCallCenter: String = '');
    function RetrieveQuestions : Boolean;
    procedure SaveAnswers;
    function AllQuestionsAnswered: Boolean;
    procedure FreeFrameList;
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; overload;
    class function GetStatusQuestions(pTicketID, pClientID, pLocatorID: integer;
                                      pOldStatusCode, pNewStatusCode: String; pCallCenter: string = '' ): Integer;
  end;
var
  LocStatusQuestions: TLocStatusQuestions;
  DigAlertNoteTxt:string;  //qm-578

const
  {Return Values for GetStatusQuestions}
  SQFalse = 0;
  SQTrue = 1;
  SQRS = 2;


implementation
uses ODMiscUtils, QMConst, LocalEmployeeDMu, DigAlertNote;

{$R *.dfm}




constructor TLocStatusQuestions.Create(AOwner:TComponent);
begin
  inherited;
  fFrameList := TList.Create;
end;

destructor TLocStatusQuestions.Destroy;
begin
  FreeAndNil(fFrameList);
  inherited;
end;

procedure TLocStatusQuestions.FreeFrameList;
var
  i: integer;
begin
  for i := 0 to fFramelist.Count - 1 do
    TStatFrame(fFrameList.Items[i]).Free;
end;



class function TLocStatusQuestions.GetStatusQuestions(pTicketID, pClientID,
                                                      pLocatorID: Integer;
                                                      pOldStatusCode, pNewStatusCode: String;
                                                      pCallCenter: String = '' ): Integer;
var
  QuestionaireDialog: TLocStatusQuestions;
begin
  Result :=   SQFalse;

//  SQTrue = 1;
//  SQRS = 2;
  if DM.OpenLocStatusQuestions(pClientID, pNewStatusCode) then begin
    QuestionaireDialog := TLocStatusQuestions.Create(nil);
    try
      QuestionaireDialog.SetValues(pTicketID, pClientID, pLocatorID, pOldStatusCode, pNewStatusCode, pCallCenter);

      QuestionaireDialog.RetrieveQuestions;

      if (QuestionaireDialog.ShowModal = mrOK) then begin
        QuestionaireDialog.SaveAnswers;
        Result := QuestionaireDialog.ReturnCode;
      end
      else begin
        Result := SQFalse;
      end;
    finally
      FreeAndNil(QuestionaireDialog);
    end;
  end;
end;

procedure TLocStatusQuestions.OKButtonClick(Sender: TObject);
begin
  if AllQuestionsAnswered then
    ModalResult := mrOK
  else
    MessageDlg('Please complete/correct all questions before saving.', mtError, [mbOK], 0);
end;

function TLocStatusQuestions.RetrieveQuestions : Boolean;
var
  ControlType : StatusFrame.TacControl;
  FormField : string;
  Question : string;
  InfoType : string;
  aFrame :  TStatFrame;
  PresetList : TStringlist;
  LoadPreset : Boolean;

begin
  If DM.LocStatusQuestions.IsEmpty then begin
    Result := False;
    Exit;
  end;

  PresetList := TStringList.Create;
  try
//    LoadPreset := DM.GetLocStatusPresets(TicketID, PresetList);  QM-28 SR

    DM.LocStatusQuestions.First;
    while not DM.LocStatusQuestions.EOF do
    begin
       FormField:= DM.LocStatusQuestions.FieldByName('form_field').AsString;
       Question:=  DM.LocStatusQuestions.FieldByName('qi_description').AsString;
       InfoType:= DM.LocStatusQuestions.FieldByName('info_type').AsString;

       if FormField = 'FF' then
         ControlType := acFreeForm
       else if formField = 'FL' then    //QMANTWO-259
         ControlType := acFirstLast    //QMANTWO-259
       else if formField = 'DATETIME' then
         ControlType := acDateTime
       else if formField = 'DATE' then
         ControlType := acDate
       else if formField = 'PHONE' then
         ControlType := acPhone
       else if formField = 'BOOL' then
         ControlType := acCheckBox
       else if ((formField = 'DROPDOWN') OR (formField = 'DROPDN')) and (InfoType = ASSIGNED_TO) then
         ControlType := acLocDropDown
       {QMANTWO-350 Custom Dropdown list - pulled from Reference table by info_type}
       else if ((formField = 'DROPDOWN') OR (formField = 'DROPDN')) and (InfoType <> ASSIGNED_TO) then begin
         ControlType := acCustomDropDown;
       end
       else
         ControlType := acFreeForm; //If all else fails, allow text to be entered in the field

       aFrame :=  TStatFrame.Create(Question, ControlType, TicketID, InfoType, fLocatorID, fCallCenter);
       if LoadPreset then
         aFrame.LoadPresetValue(PresetList.Values[InfoType]);
       aFrame.Parent := QuestionScrollBox;
       aFrame.Align := alTop;
       fFrameList.Add(aFrame);

       DM.LocStatusQuestions.Next;
    end;
  finally
    FreeAndNil(PresetList);
  end;
end;


function TLocStatusQuestions.AllQuestionsAnswered: Boolean;
var
  i: Integer;
  CurFrame: TStatFrame;
  AStr: string;     //QMANTWO-431
begin
  Result := True;
  for i := 0 to fFrameList.Count - 1 do begin
    CurFrame := TStatFrame(fFrameList.Items[i]);
    AStr := Trim(CurFrame.Answer);
    if (AStr = '') or
       (AStr = Trim(PHONEMASKEXT)) or
       (AStr = Trim(EMPTY_ANSWER)) or
       (AStr = Trim(EMPTY_DATETIME)) or
       (AStr = Trim(EMPTY_DATE)) or
       (CurFrame.ValidRestriction = False) then begin
      Result := False;
      Exit;
    end
  end;
end;

procedure TLocStatusQuestions.SaveAnswers;
var
  i: Integer;
  CurFrame : TStatFrame;
  IDs: TStringList;
  TicketInfoAnswer : string;
  NewLocatorID: integer;
  QANote : string;
  BGEFlag:boolean; //qm-578 flag

begin   //Save the Questionaire data
  IDs := TStringList.Create;
  BGEFlag:=false;  //qm-578 for BGE questions only
  ReturnCode := SQTrue; {Since we are saving answers, this defaults to True unless we need a specific code}

  try
    for i := 0 to fFrameList.Count - 1 do begin
      CurFrame := TStatFrame(fFrameList.Items[i]);

      {Everything but Dropdowns}
      if CurFrame.ControlType <> acLocDropDown then
        TicketInfoAnswer := Trim(CurFrame.Answer)
           
      {Locator}
      else begin
        try
          //Need more info to verify that it is a LocatorID and not some other data being saved
          TicketInfoAnswer := IntToStr(CurFrame.AnswerID); //Save the location ID as string
          NewLocatorID := CurFrame.AnswerID;
          if LocatorID <> NewLocatorID then begin
            IDs.AddInteger('', TicketID);
            DM.MoveTheseTickets(IDs, LocatorID, NewLocatorID);  //QMANTWO-720 EB - Changed this while working on Q&A Notes
            if IDs.Count > 0 then
              EmployeeDM.AddEmployeeActivityEvent(ActivityTypeMoveTicket, IntToStr(IDs.Count) + ' moved');
          end;
        except
          on E: Exception do
            raise Exception.Create('Error Moving Ticket to ' + CurFrame.Answer + ' : ' + E.Message);
        end;
      end;

      DM.AddTicketInfo(TicketID, CurFrame.InfoType, TicketInfoAnswer);
      QANote := QANotePrefix + CurFrame.Question + '  ' + TicketInfoAnswer;             //QMANTWO-720 EB
      DM.SaveNote(QANote, 'QA', PrivateTicketSubType, qmftTicket, TicketID, True);    //QMANTWO-720 EB

      if TicketInfoAnswer='TRUE' then    //qm-578 for BGE case sensitive  sr
        BGEFlag:=True;

     {BGEALERT qm-578 sr}
     if (CurFrame.InfoType = BGEALERT) and BGEFlag then
     begin
        try
          DigAlertNoteTxt:=''; //qm-578
          frmDigAlertNoteDialog:= TfrmDigAlertNoteDialog.Create(self);
          frmDigAlertNoteDialog.ShowModal;
          if frmDigAlertNoteDialog.ModalResult=1 then
          begin
            DM.SaveNote(DigAlertNoteTxt, BGEALERT, PublicTicketSubType, qmftTicket, TicketID, False);
          end;
        finally
          frmDigAlertNoteDialog.free;
          DigAlertNoteTxt:=''; //qm-578
          BGEFlag:=false; //qm-578
        end;
     end

     else if (CurFrame.InfoType = RESCHED) then begin //QM-604 EB Nipsco Reschedule
      ReturnCode := SQRS;
     end;
    end;

   finally
     FreeAndNil(IDs);
  end;
end;

procedure TLocStatusQuestions.SetValues(pTicketID, pClientID, pLocatorID: integer;
  pOldStatusCode, pNewStatusCode: String; pCallCenter: String='');
begin
    fTicketID := pTicketID;
    fClientID := pClientID;
    fOldStatusCode := pOldStatusCode;
    fNewStatusCode := pNewStatusCode;
    fLocatorID := pLocatorID;
    fCallCenter := pCallCenter;
end;

end.
