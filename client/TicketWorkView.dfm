object TicketWorkFrame: TTicketWorkFrame
  Left = 0
  Top = 0
  Width = 331
  Height = 173
  TabOrder = 0
  object ButtonPanel: TPanel
    Left = 0
    Top = 132
    Width = 331
    Height = 41
    Align = alBottom
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      331
      41)
    object CloseButton: TButton
      Left = 207
      Top = 8
      Width = 113
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Close'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = CloseButtonClick
    end
  end
  object WorkToDoBrowser: TWebBrowser
    Left = 0
    Top = 0
    Width = 331
    Height = 132
    Align = alClient
    TabOrder = 1
    ControlData = {
      4C00000036220000A50D00000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object AutoCloseTimer: TTimer
    Enabled = False
    OnTimer = AutoCloseTimerTimer
    Left = 216
    Top = 136
  end
end
