inherited TimesheetAuditReportForm: TTimesheetAuditReportForm
  Left = 361
  Top = 135
  Caption = 'TimesheetAuditReportForm'
  ClientHeight = 442
  ClientWidth = 537
  PixelsPerInch = 96
  TextHeight = 13
  object WorkDateRangeLabel: TLabel [0]
    Left = 0
    Top = 8
    Width = 89
    Height = 13
    Caption = 'Work Date Range:'
  end
  object DateLabel: TLabel [1]
    Left = 258
    Top = 8
    Width = 171
    Height = 13
    Caption = 'Entry / Edit / Approval Date Range:'
  end
  object Label2: TLabel [2]
    Left = 312
    Top = 276
    Width = 185
    Height = 43
    AutoSize = False
    Caption = 
      'This report includes data from only the new "Timesheet Entry" sy' +
      'stem, not the older "Hours Entry" system.'
    Visible = False
    WordWrap = True
  end
  inline WorkDateRange: TOdRangeSelectFrame [3]
    Left = 3
    Top = 22
    Width = 248
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 7
    end
    inherited DatesLabel: TLabel
      Left = 7
    end
  end
  inline EntryDateRange: TOdRangeSelectFrame
    Left = 256
    Top = 22
    Width = 248
    Height = 63
    TabOrder = 1
    inherited FromLabel: TLabel
      Left = 7
    end
    inherited DatesLabel: TLabel
      Left = 7
    end
  end
  object ChangeTypeGroupBox: TRadioGroup
    Left = 0
    Top = 276
    Width = 137
    Height = 75
    Caption = 'Changes to Include '
    ItemIndex = 0
    Items.Strings = (
      'All'
      'Time Entry Only'
      'Approvals Only')
    TabOrder = 3
  end
  object IncludeChangesGroup: TRadioGroup
    Left = 0
    Top = 86
    Width = 502
    Height = 101
    Caption = 'Include Changes Made By '
    ItemIndex = 0
    Items.Strings = (
      'Anyone'
      'Worker'
      'Anyone but the Worker'
      'Specified Person:')
    TabOrder = 2
    OnClick = UpdateVisibility
  end
  object IncludeSheetsForGroup: TRadioGroup
    Left = 0
    Top = 187
    Width = 502
    Height = 89
    Caption = 'Include Timesheets For '
    ItemIndex = 0
    Items.Strings = (
      'Any Worker'
      'Specific Worker:'
      'Anyone Under:')
    TabOrder = 4
    OnClick = UpdateVisibility
  end
  object WorkersUnderManagerBox: TComboBox
    Left = 136
    Top = 247
    Width = 160
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
  end
  inline ChangesPersonBox: TEmployeeSelect
    Left = 135
    Top = 157
    Width = 190
    Height = 25
    TabOrder = 6
    inherited EmployeeCombo: TComboBox
      Top = 2
      Width = 160
    end
  end
  inline SpecificWorkerBox: TEmployeeSelect
    Left = 135
    Top = 218
    Width = 190
    Height = 25
    TabOrder = 7
    inherited EmployeeCombo: TComboBox
      Top = 4
      Width = 160
    end
  end
  object IncludeMgrAlterTimeResponse: TCheckBox
    Left = 8
    Top = 356
    Width = 289
    Height = 17
    Caption = 'Include Manager Altered Time Response Report'
    TabOrder = 8
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 0
    Top = 374
  end
end
