object AddProjectLocateForm: TAddProjectLocateForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Add Project Locate'
  ClientHeight = 289
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNote: TLabel
    Left = 16
    Top = 140
    Width = 56
    Height = 13
    Caption = 'Short Note:'
  end
  object NamePanel: TPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object LocateNoEdit: TEdit
      Left = 16
      Top = 4
      Width = 393
      Height = 24
      BorderStyle = bsNone
      Color = clMenu
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      Text = 'LocateNoEdit'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 251
    Width = 427
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      427
      38)
    object OKBtn: TButton
      Left = 138
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akBottom]
      Caption = 'OK'
      Default = True
      Enabled = False
      ModalResult = 1
      TabOrder = 0
    end
    object Button1: TButton
      Left = 219
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akBottom]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 31
    Width = 393
    Height = 106
    Caption = ' Locate Information '
    TabOrder = 2
    DesignSize = (
      393
      106)
    object Label2: TLabel
      Left = 23
      Top = 20
      Width = 68
      Height = 13
      Caption = 'Project Client:'
    end
    object Label3: TLabel
      Left = 23
      Top = 47
      Width = 35
      Height = 13
      Caption = 'Status:'
    end
    object Label1: TLabel
      Left = 23
      Top = 73
      Width = 40
      Height = 13
      Caption = 'Locator:'
    end
    object ClientCombo: TComboBox
      Left = 93
      Top = 16
      Width = 292
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 18
      ItemHeight = 0
      TabOrder = 0
      OnChange = ClientComboChange
    end
    object LocatorComboBox: TcxComboBox
      Left = 93
      Top = 70
      Properties.DropDownListStyle = lsFixedList
      TabOrder = 1
      OnClick = LocatorComboBoxClick
      Width = 292
    end
    object StatusCombo: TcxComboBox
      Left = 93
      Top = 43
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      TabOrder = 2
      OnClick = LocatorComboBoxClick
      Width = 60
    end
  end
  object edtShortNote: TEdit
    Left = 16
    Top = 155
    Width = 393
    Height = 21
    TabOrder = 3
  end
  object MessagePanel: TPanel
    Left = 0
    Top = 191
    Width = 427
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    object MessageMemo: TcxMemo
      Left = 16
      Top = 4
      Margins.Left = 50
      Margins.Right = 50
      ParentFont = False
      Properties.ReadOnly = True
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Edges = [bLeft, bTop, bRight, bBottom]
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.NativeStyle = True
      Style.TextColor = clMaroon
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Height = 57
      Width = 393
    end
  end
  object QryOrigLocate: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select L.*, '
      '       E.short_name,'
      '       C.client_name, C.oc_code , c.office_id,'
      '       c.call_center, c.allow_edit_locates '
      ' from locate L'
      '  left join employee E on L.assigned_to_id=E.emp_id'
      '  left join client C on L.client_id=C.client_id'
      ' where L.active'
      '  and L.locate_id=:locate_id'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end>
    Left = 52
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end>
  end
  object ClientQry: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      '')
    Params = <>
    Left = 84
    Top = 260
  end
end
