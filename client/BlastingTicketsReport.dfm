inherited BlastingTicketsReportForm: TBlastingTicketsReportForm
  Left = 501
  Top = 301
  Caption = 'BlastingTicketsReportForm'
  ClientHeight = 340
  ClientWidth = 549
  DesignSize = (
    549
    340)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 0
    Top = 173
    Width = 73
    Height = 13
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  object Label3: TLabel [1]
    Left = 280
    Top = 173
    Width = 36
    Height = 13
    Caption = 'Clients:'
  end
  object Label1: TLabel [2]
    Left = 0
    Top = 123
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label4: TLabel [3]
    Left = 0
    Top = 8
    Width = 105
    Height = 13
    Caption = 'Transmit Date Range:'
  end
  object ClientList: TCheckListBox [4]
    Left = 280
    Top = 189
    Width = 183
    Height = 148
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
  end
  object ManagersComboBox: TComboBox [5]
    Left = 63
    Top = 120
    Width = 179
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object SelectAllButton: TButton [6]
    Left = 468
    Top = 189
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 4
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [7]
    Left = 468
    Top = 221
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 5
    OnClick = ClearAllButtonClick
  end
  object CallCenterList: TCheckListBox [8]
    Left = 0
    Top = 189
    Width = 275
    Height = 148
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 2
    OnClick = CallCenterListClick
  end
  object LimitManagerBox: TCheckBox [9]
    Left = 63
    Top = 96
    Width = 121
    Height = 17
    Caption = 'Limit to one manager'
    TabOrder = 0
    OnClick = LimitManagerBoxClick
  end
  inline TransmitRange: TOdRangeSelectFrame [10]
    Left = 10
    Top = 22
    Width = 258
    Height = 63
    TabOrder = 6
    inherited FromLabel: TLabel
      Left = 11
    end
    inherited ToLabel: TLabel
      Left = 144
    end
    inherited DatesLabel: TLabel
      Left = 11
      Top = 37
    end
    inherited DatesComboBox: TComboBox
      Left = 53
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 53
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 165
      Width = 87
    end
  end
  object LimitClients: TCheckBox
    Left = 63
    Top = 150
    Width = 145
    Height = 17
    Caption = 'Limit to Specific Clients'
    TabOrder = 7
    OnClick = LimitClientsClick
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
  end
end
