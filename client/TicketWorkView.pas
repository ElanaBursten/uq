unit TicketWorkView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, OleCtrls, SHDocVw;

const
  ClosedAutomatically = 'AUTO';
  ClosedByUser = 'USER';

type
  TTicketWorkFrame = class(TFrame)
    ButtonPanel: TPanel;
    CloseButton: TButton;
    AutoCloseTimer: TTimer;
    WorkToDoBrowser: TWebBrowser;
    procedure CloseButtonClick(Sender: TObject);
    procedure AutoCloseTimerTimer(Sender: TObject);
  private
    FTicketVersionID: Variant;
    FOnCloseClick: TNotifyEvent;
    FHowClosed: string;
    FAutoCloseTime: TDateTime;
    function GetAllowClose: Boolean;
    procedure SetAllowClose(const Value: Boolean);
    procedure ShowWorkToDo(const WorkLines: string);
  public
    property HowClosed: string read FHowClosed;
    property AllowClose: Boolean read GetAllowClose write SetAllowClose;
    property OnCloseClick: TNotifyEvent read FOnCloseClick write FOnCloseClick;
    procedure Prepare(TicketID: Integer; FullScreenMode: Boolean=False; AutoCloseSeconds: Integer = 0);
  end;

implementation

uses DateUtils, QMConst, OdMiscUtils, OdVclUtils, OdBrowserUtils, DMu, LocalEmployeeDMu;

{$R *.dfm}

procedure TTicketWorkFrame.CloseButtonClick(Sender: TObject);
begin
  AutoCloseTimer.Enabled := False;
  if IsEmpty(FHowClosed) then
    FHowClosed := ClosedByUser;
  if Assigned(FOnCloseClick) then
    FOnCloseClick(Sender);

  if FTicketVersionID = Null then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketViewWorkEnd, '', Now, FHowClosed)
  else
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketViewWorkEnd, IntToStr(FTicketVersionID), Now, FHowClosed);
end;

function TTicketWorkFrame.GetAllowClose: Boolean;
begin
  Result := ButtonPanel.Visible;
end;

procedure TTicketWorkFrame.SetAllowClose(const Value: Boolean);
begin
  ButtonPanel.Visible := Value;
  SetEnabledOnControlAndChildren(ButtonPanel, Value);
  if Value then
    CloseButton.Left := ButtonPanel.Width - (CloseButton.Width + 4);
end;

procedure TTicketWorkFrame.Prepare(TicketID: Integer; FullScreenMode: Boolean; AutoCloseSeconds: Integer);
var
  WorkLines: string;
begin
  FHowClosed := '';
  Assert(DM.Ticket.Active);
  Assert(DM.Ticket.FieldByName('ticket_id').AsInteger = TicketID);

  WorkLines := DM.GetTicketWorkViewHTML(FullScreenMode);
  ShowWorkToDo(WorkLines);

  FTicketVersionID := DM.GetLatestTicketVersionForTicket(TicketID);
  if FullScreenMode then begin
    AllowClose := True;
    if FTicketVersionID = Null then
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketViewWorkStart, '', Now)
    else
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketViewWorkStart, IntToStr(FTicketVersionID), Now);
    Assert(AutoCloseSeconds > 0, 'Need AutoCloseSeconds when in Full Screen mode');
    FAutoCloseTime := IncSecond(Now, AutoCloseSeconds);
    AutoCloseTimer.Enabled := True;
  end;
end;

procedure TTicketWorkFrame.AutoCloseTimerTimer(Sender: TObject);
var
  SecondsRemaining: Integer;
begin
  if Now >= FAutoCloseTime then begin
    AutoCloseTimer.Enabled := False;
    FHowClosed := ClosedAutomatically;
    CloseButton.Click;
  end else begin
    SecondsRemaining := SecondsBetween(FAutoCloseTime, Now);
    if SecondsRemaining > 0 then
      ButtonPanel.Caption := '  Closing in ' + IntToStr(SecondsRemaining) + ' seconds...'
    else
      ButtonPanel.Caption := '  Closing now';
  end;
end;

procedure TTicketWorkFrame.ShowWorkToDo(const WorkLines: string);
begin
  LoadHTML(WorkToDoBrowser, WorkLines);
end;

end.
