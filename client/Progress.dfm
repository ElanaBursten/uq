object ProgressFrame: TProgressFrame
  Left = 0
  Top = 0
  Width = 231
  Height = 81
  TabOrder = 0
  object MajorProgressLabel: TLabel
    Left = 4
    Top = 4
    Width = 31
    Height = 13
    Caption = 'Status'
  end
  object MinorProgressLabel: TLabel
    Left = 4
    Top = 42
    Width = 31
    Height = 13
    Caption = 'Status'
  end
  object MajorProgressBar: TProgressBar
    Left = 4
    Top = 22
    Width = 223
    Height = 17
    Smooth = True
    TabOrder = 0
  end
  object MinorProgressBar: TProgressBar
    Left = 4
    Top = 59
    Width = 223
    Height = 17
    Smooth = True
    TabOrder = 1
  end
end
