unit EmployeeSelectFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, QMConst;

type
  TEmployeeSelect = class(TFrame)
    EmployeeCombo: TComboBox;
    procedure SearchButtonClick(Sender: TObject);
  private
    FMode: TEmployeeSelectMode;
    FEnabled: Boolean;
    FManagerID: Integer;
    procedure SetMode(const Value: TEmployeeSelectMode);
    procedure SetEnabled(const Value: Boolean); reintroduce;
  public
    procedure RefreshEmployees(ManagerID: Integer = -1);
    procedure Initialize(AMode: TEmployeeSelectMode; DefaultID: Integer = -1; ManagerID: Integer = -1);
    procedure SelectID(ID: Integer);
    procedure SelectFirstItem;
    function SelectedText: string;
    function SelectedID: Integer;
    function HaveSelection: Boolean;
  published
    property Mode: TEmployeeSelectMode read FMode write SetMode;
    property Enabled: Boolean read FEnabled write SetEnabled;
  end;

implementation

{$R *.dfm}

uses DMu, EmployeeSearch, OdVclUtils, LocalEmployeeDMu;

{ TEmployeeSelect }

procedure TEmployeeSelect.RefreshEmployees(ManagerID: Integer);
begin
  EmployeeDM.GetListForEmployeeDisplayMode(EmployeeCombo.Items, Mode, ManagerID);
end;

procedure TEmployeeSelect.SetMode(const Value: TEmployeeSelectMode);
begin
  FMode := Value;
end;

procedure TEmployeeSelect.SearchButtonClick(Sender: TObject);
var
  SelectedID: Variant;
begin
  if TEmployeeSearchForm.Execute(Mode, SelectedID, '', FManagerID) then
    SelectComboBoxItemFromObjects(EmployeeCombo, SelectedID);
end;

procedure TEmployeeSelect.Initialize(AMode: TEmployeeSelectMode; DefaultID: Integer; ManagerID: Integer);
begin
  Mode := AMode;
  FManagerID := ManagerID;
  RefreshEmployees(ManagerID);
  if DefaultID > -1 then
    SelectComboBoxItemFromObjects(EmployeeCombo, DefaultID);
  AddEmployeeSearchButtonForCombo(EmployeeCombo, Mode, '', FManagerID);
end;

function TEmployeeSelect.HaveSelection: Boolean;
begin
  Result := EmployeeCombo.ItemIndex > -1;
end;

function TEmployeeSelect.SelectedID: Integer;
begin
  Result := GetComboObjectInteger(EmployeeCombo);
end;

function TEmployeeSelect.SelectedText: string;
begin
  Result := EmployeeCombo.Text;
end;

procedure TEmployeeSelect.SelectID(ID: Integer);
begin
  SelectComboBoxItemFromObjects(EmployeeCombo, ID);
end;

procedure TEmployeeSelect.SelectFirstItem;
begin
  if EmployeeCombo.Items.Count > 0 then
    EmployeeCombo.ItemIndex := 0;
end;

procedure TEmployeeSelect.SetEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  // SearchButton.Enabled := FEnabled;
  EmployeeCombo.Enabled := FEnabled;
end;

end.

