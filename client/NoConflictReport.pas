unit NoConflictReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls, CheckLst, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, ComCtrls, dxCore,
  cxDateUtils;

type
  TNoConflictReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DateEdit: TcxDateEdit;
    CallCenterComboBox: TComboBox;
    StatusCheckListBox: TCheckListBox;
    State: TComboBox;
    County: TEdit;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  OdExceptions, DMu, OdVclUtils;

{ TNoConflictReportForm }

procedure TNoConflictReportForm.ValidateParams;
var
  StatusList: string;
begin
  inherited;
  StatusList := GetCheckedItemString(StatusCheckListBox);
  if StatusList = '' then
    raise EOdEntryRequired.Create('You must select at least one status code');

  SetReportID('NoConflict');
  SetParamDate('StartDate', DateEdit.Date);
  SetParamDate('EndDate', DateEdit.Date+1);
  SetParam('CallCenter', FCallCenterList.GetCode(CallCenterComboBox.Text));
  SetParam('Statuses', StatusList);
  SetParam('County', County.Text);
  SetParam('State', State.Text);
end;

procedure TNoConflictReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  DM.GetStatusStrings(StatusCheckListBox.Items, True);
  DateEdit.Date := Date;
  CallCenterComboBox.ItemIndex := 0;
  DM.GetRefDisplayList('state', State.Items);
end;

procedure TNoConflictReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(StatusCheckListBox, True);
end;

procedure TNoConflictReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(StatusCheckListBox, False);
end;

end.

