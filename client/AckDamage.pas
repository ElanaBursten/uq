unit AckDamage;

interface

uses Windows, SysUtils, Classes, StdCtrls, Controls, ExtCtrls, Forms;

type
  TAckDamageDialog = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    Bevel: TBevel;
    InvestigatorLabel: TLabel;
    CreateDamageCheckbox: TCheckBox;
    ManagerComboBox: TComboBox;
    EmployeeComboBox: TComboBox;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CreateDamageCheckboxClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure ManagerComboBoxChange(Sender: TObject);
  protected
    procedure Loaded; override;
  public
    function GetInvestigatorID: Integer;
    procedure RepopulateList;
    function ManagerID: Integer;
  end;

implementation

{$R *.dfm}

uses DMu, OdExceptions, OdVclUtils, Terminology, LocalEmployeeDMu;

procedure TAckDamageDialog.FormShow(Sender: TObject);
begin
  ManagerComboBox.Enabled := False;
  EmployeeComboBox.Enabled := False;
  EmployeeDM.ManagerList(ManagerComboBox.Items);
end;

procedure TAckDamageDialog.CreateDamageCheckboxClick(Sender: TObject);
begin
  ManagerComboBox.Enabled := CreateDamageCheckbox.Checked;
  EmployeeComboBox.Enabled := CreateDamageCheckbox.Checked;
end;

function TAckDamageDialog.GetInvestigatorID: Integer;
begin
  Result := -1;
  if CreateDamageCheckbox.Checked  and (EmployeeComboBox.ItemIndex > -1) then
    Result := GetComboObjectInteger(EmployeeComboBox);
end;

procedure TAckDamageDialog.OKButtonClick(Sender: TObject);
begin
  if CreateDamageCheckbox.Checked and (EmployeeComboBox.ItemIndex = -1) then
    raise EOdEntryRequired.Create('Please select a damage investigator')
  else
    ModalResult := mrOk;
end;

procedure TAckDamageDialog.RepopulateList;
begin
  if ManagerID > 0 then
    EmployeeDM.EmployeeList(EmployeeComboBox.Items, ManagerID)
  else
    EmployeeComboBox.Items.Clear;
end;

function TAckDamageDialog.ManagerID: Integer;
begin
  Result := GetComboObjectIntegerDef(ManagerComboBox);
end;

procedure TAckDamageDialog.ManagerComboBoxChange(Sender: TObject);
begin
  RepopulateList;
end;

procedure TAckDamageDialog.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

end.
