unit DMu;
{
  QMANTWO-681:  DMus broken out for maitenance and minimize conflicts.
  New DMus will act as an extension to the main DMu
     * LocalPermissionsDMu
     * LocalEmployeeDMu                                                                 
     * LocalDamageDMu                                                
}

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, MSXML2_TLB, DBISAMTb, DBISAMCN, SyncEngineU, StateMgr, ServerProxy, Types,
  HoursCalc, QMConst, SharedImages, ODEmbeddable, OdMSXMLUtils, OdUqInternet, 
  OdMiscUtils, TrayIcon, ExtCtrls, LocalAttachmentDMu, UploadThread, TlHelp32,
  BaseAttachmentDMu, GPSThread, DMHttpServer, OdIdleTimer, APIServerIntf,
  QMTransport, CodeLookupList, OdContainer, DBClient, RouteOrderMgt;

const
  AppName = 'Q Manager';
  HelpDeskContact = 'Help Desk Phone 800-531-0003';
  {$I ..\version.inc} // Copy QManager\SAMPLE.version.inc to version.inc to compile this
  AppVersionMarker = '';

  STATUS_LIST_FOR_CLIENTS =        //QM-265
  'select distinct sl.status, sl.meet_only, sgi.TicketTypeExclusion, '+
  'sgi.NotesRequired, sgi.PicturesRequired, c.client_id, sl.mark_type_required, ' +
  'sl.default_mark_type, sl.status_name, c.client_name ' +
  'from client c  '+
   'inner join status_group_item sgi on c.status_group_id=sgi.sg_id  '+
   'inner join status_group sg on (sgi.sg_id=sg.sg_id)  '+
   'inner join statuslist sl on sgi.status=sl.status  '+
   'where sgi.active  '+
   'and sl.status not in(%s)  '+ //qm-559
   'and sl.active '+
   'and client_id in (%s)  '+
   'order by 1  ';

   
type
  TGPXItem = record    //QM-10 EB Export
    LocatorShortName,
    TicketNum,
    WorkLat,
    WorkLong,
    Name,
    DueDate,
    Address,
    Street,
    City,
    County,
    State,
    RouteOrder,
    Kind,
    WorkType: ansistring;
    LabelText: ansistring;
    DescText: ansistring;
    RteText: ansistring;
    WptText: ansistring;
  end;



  TGPXArray = Array of TGPXItem;   //QM-10 EB

  TGPXOptions = record
   IncludeRouteOrder,
   IncludeEmpName: Boolean;
  end;

  TPlusRestricted = record   //QM-585 EB PLUS WhiteList
    TicketID: integer;
    Loaded: Boolean;
    HasRestriction:Boolean;
    CC: TStringList;
  end;

  TItemSelectedEvent = procedure(Sender: TObject; ID: Integer) of object;
  TBooleanSelectedEvent = procedure(Sender: TObject; Value: Boolean) of object;
  TDisplayLocateHoursEvent = procedure(Sender: TObject;
    TicketID, LocateID: Integer; CanAddHours, CanAddUnits: Boolean) of object;
  TEditTimesheetEvent = procedure(EmpID: Integer; EditDate: TDateTime; Mode: TTimesheetEditMode) of object;

  // Note: Do not reference the DM variable inside TDM methods.  There can be more
  // >1 TDM instance (in the tests for example) so the methods need to operate on Self.
  TDM = class(TDataModule)
    Database1: TDBISAMDatabase;
    TicketKindCount: TDBISAMQuery;
    Ticket: TDBISAMTable;
    Locate: TDBISAMTable;
    StatusList: TDBISAMTable;
    Reference: TDBISAMTable;
    CallCenters: TDBISAMQuery;
    TicketNotesData: TDBISAMQuery;
    Notes: TDBISAMTable;
    CallCenterClients: TDBISAMQuery;
    Offices: TDBISAMQuery;
    LocateHoursData: TDBISAMQuery;
    LocateLookup: TDBISAMTable;
    SaveLocateHours: TDBISAMTable;
    FollowupTicketTypes: TDBISAMQuery;
    Attachment: TDBISAMTable;
    UploadLocation: TDBISAMQuery;
    AssetCount: TDBISAMQuery;
    FindRuleCode: TDBISAMQuery;
    ValidStatusQuery: TDBISAMQuery;
    CenterGroups: TDBISAMQuery;
    Customers: TDBISAMQuery;
    SaveLocatePlat: TDBISAMTable;
    LocatePlatData: TDBISAMQuery;
    UnitTypeLookup: TDBISAMTable;
    LocateUnitType: TDBISAMQuery;
    Area: TDBISAMTable;
    BillingOutputConfig: TDBISAMQuery;
    EstimateTypeLookup: TDBISAMQuery;
    MarkMessagesAcked: TDBISAMQuery;
    StatusGroup: TDBISAMQuery;
    CenterGroup: TDBISAMQuery;
    SaveTicketInfo: TDBISAMTable;
    LocatorQuery: TDBISAMQuery;
    SaveJobsiteArrival: TDBISAMTable;
    LocateFacilityData: TDBISAMQuery;
    SaveLocateFacility: TDBISAMTable;
    ClientLookup: TDBISAMQuery;
    JobsiteArrivalData: TDBISAMQuery;
    SaveBreakRuleResponse: TDBISAMTable;
    SaveTaskSchedule: TDBISAMTable;
    HighlightRule: TDBISAMQuery;
    SaveGPSPosition: TDBISAMTable;
    WorkOrder: TDBISAMTable;
    OpenWorkOrders: TDBISAMQuery;
    WorkOrderNotesData: TDBISAMQuery;
    WorkOrderInspection: TDBISAMTable;
    WorkOrderRemedy: TDBISAMTable;
    WorkOrderCGAReasonLookup: TDBISAMQuery;
    LocStatusQuestions: TDBISAMQuery;
    LocStatusPrefill: TDBISAMQuery;
    NotesSubTypeLookup: TDBISAMQuery;
    RefDescForNotesSubType: TDBISAMQuery;
    GetWorkOrderNumQuery: TDBISAMQuery;
    LocatesForTickets: TDBISAMQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    BooleanField1: TBooleanField;
    IntegerField3: TIntegerField;
    BooleanField2: TBooleanField;
    IntegerField4: TIntegerField;
    StringField3: TStringField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    BooleanField3: TBooleanField;
    StringField5: TStringField;
    IntegerField6: TIntegerField;
    BCDField1: TBCDField;
    BooleanField4: TBooleanField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    StringField6: TStringField;
    BooleanField5: TBooleanField;
    IntegerField7: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    Holidays: TDBISAMQuery;
    qryRefIDFromClient: TDBISAMQuery;
    LocatorsMgr: TDBISAMQuery;
    IntegerField8: TIntegerField;
    StringField9: TStringField;
    IntegerField9: TIntegerField;
    StringField10: TStringField;
    BooleanField6: TBooleanField;
    IntegerField10: TIntegerField;
    BooleanField7: TBooleanField;
    IntegerField11: TIntegerField;
    StringField11: TStringField;
    IntegerField12: TIntegerField;
    StringField12: TStringField;
    DateTimeField3: TDateTimeField;
    DateTimeField4: TDateTimeField;
    BooleanField8: TBooleanField;
    StringField13: TStringField;
    IntegerField13: TIntegerField;
    BCDField4: TBCDField;
    BooleanField9: TBooleanField;
    BCDField5: TBCDField;
    BCDField6: TBCDField;
    StringField14: TStringField;
    BooleanField10: TBooleanField;
    IntegerField14: TIntegerField;
    StringField15: TStringField;
    StringField16: TStringField;
    ProjectClients: TDBISAMQuery;
    ClientHasChildClientsQry: TDBISAMQuery;
    LocateData: TDBISAMQuery;
    LocateDatalocate_id: TIntegerField;
    LocateDataclient_code: TStringField;
    LocateDataclient_id: TIntegerField;
    LocateDatastatus: TStringField;
    LocateDatahigh_profile: TBooleanField;
    LocateDataqty_marked: TIntegerField;
    LocateDataclosed: TBooleanField;
    LocateDatalocator_id: TIntegerField;
    LocateDatashort_name: TStringField;
    LocateDataclosed_by_id: TIntegerField;
    LocateDataclosed_how: TStringField;
    LocateDataclosed_date: TDateTimeField;
    LocateDatamodified_date: TDateTimeField;
    LocateDataactive: TBooleanField;
    LocateDataDeltaStatus: TStringField;
    LocateDataticket_id: TIntegerField;
    LocateDataprice: TBCDField;
    LocateDatainvoiced: TBooleanField;
    LocateDataregular_hours: TBCDField;
    LocateDataovertime_hours: TBCDField;
    LocateDataadded_by: TStringField;
    LocateDatawatch_and_protect: TBooleanField;
    LocateDatahigh_profile_reason: TIntegerField;
    LocateDatahigh_profile_multi: TStringField;
    LocateDatahigh_profile_multi_old: TStringField;
    QryProjectLocates: TDBISAMQuery;
    qryClientInfo: TDBISAMQuery;
    dsClientInfo: TDataSource;
    TicketFullAddressQry: TDBISAMQuery;  //QMANTWO-810
   	RouteOrderQuery: TDBISAMQuery;
    InsertRouteOrderInfo: TDBISAMQuery;
    QryTicketInfoRouteOrder: TDBISAMQuery;
    qryUtilityCo: TDBISAMQuery;
    qryClient: TDBISAMQuery;
    qryEmpPhoneCo: TDBISAMQuery;
    qrySearchLocator: TDBISAMQuery;
    ROForLocator: TDBISAMQuery;
    StatusListForClientQry: TDBISAMQuery;
    qryAbandonedCallOut: TDBISAMQuery;
    updNextUnusedCallOut: TDBISAMQuery;
    customer_locator: TDBISAMQuery;
    qryPLUSWhitelist: TDBISAMQuery;
    UpdateAbandonedCO: TDBISAMQuery;
    MultiDueDateQuery: TDBISAMQuery;
    ValidStatusPlusQry: TDBISAMQuery;
    TicketAlertsQuery: TDBISAMQuery;
    LocateHoursDataTotals: TDBISAMQuery;
    AddLocateQry: TDBISAMQuery;
    IntegerField15: TIntegerField;
    StringField17: TStringField;
    IntegerField16: TIntegerField;
    StringField18: TStringField;
    BooleanField11: TBooleanField;
    IntegerField17: TIntegerField;
    BooleanField12: TBooleanField;
    IntegerField18: TIntegerField;
    StringField19: TStringField;
    IntegerField19: TIntegerField;
    StringField20: TStringField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    BooleanField13: TBooleanField;
    StringField21: TStringField;
    IntegerField20: TIntegerField;
    BCDField7: TBCDField;
    BooleanField14: TBooleanField;
    BCDField8: TBCDField;
    BCDField9: TBCDField;
    StringField22: TStringField;
    BooleanField15: TBooleanField;
    IntegerField21: TIntegerField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    procedure LocateDataStatusChange(Sender: TField);
    procedure LocateDataBeforeEdit(DataSet: TDataSet);
    procedure LocateDataBeforePost(DataSet: TDataSet);
    procedure LocateDataHighProfileChange(Sender: TField);
    procedure LocateDataCalcFields(DataSet: TDataSet);
    procedure LocateHoursDataCalcFields(DataSet: TDataSet);
    procedure TicketAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure LocateFacilityDataBeforeEdit(DataSet: TDataSet);
    procedure LocateLookupBeforeOpen(DataSet: TDataSet);
    procedure TicketNotesDataCalcFields(DataSet: TDataSet);
    procedure TicketNotesDataBeforeOpen(DataSet: TDataSet);
    procedure WorkOrderBeforeOpen(DataSet: TDataSet);
    procedure WorkOrderBeforePost(DataSet: TDataSet);
    procedure WorkOrderInspectionBeforePost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure SaveGPSPositionPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
  private
    FStatusStrings: TStringList;
    fPlusRestrictedList: TPlusRestricted; //QM-585 EB PLUS WHITELIST - These are the restricted items for the
    FOnUpdateCountLabels: TNotifyEvent;
    FOnReportStatusChange: TNotifyEvent;
    FDisplayLocateHoursEvent: TDisplayLocateHoursEvent;
    FOnEditTimesheet: TEditTimesheetEvent;
    FDataDir: string;
    FClosedHow: string;
    FTicketChanged: Boolean;
    FLocFacilityDeleted: Boolean;
    FStatusAsAssigned: Boolean;
    FRetainClosedDate: Boolean;
    FOutstandingWOAttachments: Boolean; //QMANTWO-815 EB Attempt to Keep sync from running while Work Order Attachments are loading
    FTicketDueDateChanged: Boolean;  //QM-216 EB
    //    FEffectivePCCache: TStringList;   //LED
    FTrayIcon: TTrayIcon;
    FTrayIconHideTimer: TTimer;
    FLocalAttachment: TLocalAttachment;
    FMyAllowedStatusList: TStringList;
    FBackgroundGPS: TBackgroundGPSThread;
    DMHttp: TDMHttp;
    QMTransport: THttpTransport;
    fConnectedDB: string;
    ackedTimeClockIn:boolean;
    procedure CloseQueries;
    procedure OpenLocateData;
    procedure OpenTicketNotesData;
    procedure OpenWorkOrderNotesData;
    procedure StartUp;

    function MatchesPreviousNamePW: Boolean;
    function NamePwHash: string;
    procedure SaveNamePwHash;
    procedure ClearNamePwHash;
    procedure EmptyDateTimeGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure RefFieldOnGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure RefFieldOnSetText(Sender: TField; const Text: string);

    procedure SetTicketKind(AnyOngoing: Boolean; AnyOpen: Boolean);
    procedure UpdateDatabaseSchema;
    procedure GetEmbeddedSchema(var Schema: string);
    procedure SetLocateFieldsInTable;
    function LocateNeedsToPost: Boolean;
    procedure CheckForAndFixCorruptTables;
    procedure UpdateSyncStatusTable;
    function LocalTableExists(const TableName: string): Boolean;
    procedure CheckForInstalledComponents;
    procedure ZeroCounts;
    procedure ShowLocateHoursForm(TicketID, LocateID: Integer; CanAddHours: Boolean = True; CanAddUnits: Boolean = True);
    procedure SaveLocateChanges;
    procedure SaveLocateHoursChanges;
    procedure SaveLocatePlatChanges;


    procedure PrepareToSync;
    procedure RunUpdateFile;
    function HaveEditClientOverride(const ClientCode: string): Boolean;
    procedure CheckForPasswordChange;

    procedure DeleteTimesheets(EmpID: Integer; StartDate: TDateTime; EndDate: TDateTime);
    procedure RemoveOldRecords;
    procedure VerifyShouldSaveForInvoicedLocates(DataSet: TDataSet);
    procedure TicketUppercaseFieldOnSetText(Sender: TField; const Text: string);

    function GetLocatePlatStr(const LocateID: Integer): string;
    function GetDelimitedStringFromDataSet(Data: TDataSet; const DisplayField: string; Delimiter: string=','): string;
    procedure DecodeRefListCriteria(const RefListCriteria: string;
      var RefType, Modifier: string);
    procedure SetUpDatabase(DataDir: string);
    procedure CloseDataSetsForRefresh;
    function GetOngoingContactName(TicketID: Integer; var First, Last: string): Boolean;
    procedure LogLastPowerOnOffEvents;
    procedure FixTicketNotes;
    procedure ProcessConfigurationData;
    procedure SetupLocateFacilityData(TicketID: Integer);
    procedure SaveTicketNotesFromCache;
    procedure SaveArrivalChanges(const ArrivalDataset: TDataset);
    procedure CheckForUnackedMessages;
    procedure SaveLocateLength(LocateID, LengthMarked: Integer;
      LocateStatus: string; WorkDate: TDateTime);
    procedure StartTrayIconHideTimer;
    procedure HideTrayIcon(Sender: TObject);
    function GetExportFolderName: string;
    function ApplicationTrayIcon: TTrayIcon;
    function GetBackgroundUploadLocationID: Integer;
    procedure NeedAttachmentParentData(Sender: TObject; FileItem: TUploadFileItem);
    function GetAutoAttachFolderName: string;
    procedure CleanAutoAttachFailedFolder(const DaysOld: Integer);
    procedure CleanGPSLogFiles(const DaysOld: Integer);
    procedure UpgradeDatabase;

  public
    MyClients: TStrings;            //QM-265
    MyMissingClients: TStrings;     //QM-1035 Fix
    AD_Active : Boolean;
    AllowTktStrMap:boolean;
    RequireTktStrMap:boolean; //qm-777 sr
    HaveData: Boolean;
    Engine: TLocalCache;
    QMLogic2Server: TAPIServerInterface;
    UQState: TUQState;
    nEmergency, nNew, nOngoing, nOpenDamages, nOpenWorkOrders: Integer;
    IgnoreSetStatus: Boolean;
    FormHandle: THandle;
    TestMode: Boolean;
    CurrentReportID: string;
    SyncRequestSource: string;
    CurrentGPSLatitude: Double;
    CurrentGPSLongitude: Double;
    CurrentGPSHDOP: Integer;
    CurrentGPSFixTime: TDateTime;
    LastSavedExcRespCode: string;
    LastSavedUQRespCode: string;
    LastSavedSpecialRespCode: string;
    eSketchIsRunning: Boolean;
    FBackgroundUploader: TBackgroundUploadThread;
    CanAddYesdayCallout:boolean;
    property ConnectedDB:string read fConnectedDB write fConnectedDB;   //QM-44   SR
    property TicketChanged: boolean read fTicketChanged write fTicketChanged;
    property OutStandingWOAttachments: boolean read FOutstandingWOAttachments write FOutstandingWOAttachments;  //QMANTWO-815 EB
    property TicketDueDateChanged: boolean read fTicketDueDateChanged write fTicketDueDateChanged;   //QM-216 EB
    property LocFacilityDeleted: boolean read fLocFacilityDeleted write FLocFacilityDeleted;
    procedure SaveNote(const Note, Source: string; NoteSubType: Integer = 0; ForeignType: Integer = qmftTicket; ForeignId: Integer = -1; Active: Boolean = True);    //QMANTWO-681 EB Moved to public
    procedure SaveNoteToServer(TicketID: Integer; NoteText:string; SubType: integer; EnteredByID:integer;ClientCode:String ='');  //qm-747 
    procedure ChangeNote(const NoteID: integer; Note, Source: string;     //QM-188 EB Delete Bad Notes
                             NOteSubType: Integer = 0; ForeignType: Integer = qmftTicket; ForeignId: Integer = -1; Active: Boolean = True);

    procedure RunRoboCopy(LocatorEmpID: integer); //qm-436  sr
    procedure CheckForApproachingEOD;  //QM-370  sr
    function KillTask(ExeFileName: string): integer;
    procedure ThirdPartyBooleanOnGetText(Sender: TField; var Text: string; DisplayText: Boolean); //QMANTWO-681 EB Made public
    procedure StartTransaction;    //QMANTWO-681 - moved from private (DMu split-out)
    procedure Rollback;
    procedure Commit;
    procedure GetListFromDataSet(Data: TDataSet; List: TStrings; const DisplayField, IDField: string; IncludeBlank: Boolean = False);
    function GetEmpBreadCrumbFromServer(EmpID:Integer): string;
    function GetMgrBreadCrumbsFromServer(MgrID: Integer): string; //QMANTWO-302
    procedure RecreateLocalTableFiles(const TableName: string);
    procedure CopyTableData(Src, Dest: TDBISAMTable);
    procedure Connect(const UserName, Password: string);
    procedure CheckLogin(const UserName, Password: string);
    function CheckADLogin(Const UserName, Password: string): boolean;
    procedure SyncNow;  //DMS
    procedure UpdateCounts;
    function GetOpenCount(const TableName: string; const Query: TDBISAMQuery): Integer;
    procedure DumpLocalData;
    procedure ReloadTicketData(IncludeSync: Boolean = False); //QM-730 EB Reload Ticket Data (delete and sync)
    procedure ReloadReferenceData; //QM-933 Order issue with drop downs - Reference table not refreshing
    procedure ClearSyncStatusDate; //QM-979 Data folders not updating with sync
    procedure GoToTicket(ID: Integer);
    procedure GoToTicketAck(ID: Integer);
    procedure GoToWorkOrder(ID: Integer);
    function GetWorkOrderNum(ID: Integer): string;  {EB: used only when WO is KNOWN to already be in cache} //QMANTWO-296
    function TicketIsOpen: Boolean; overload;
    function TicketIsOpen(TicketData: TDataSet): Boolean; overload;
    function TicketIsOpen(TicketID: Integer): Boolean; overload;
    procedure SaveTicket(const ClosedHow: string; pStatusAsAssigned: Boolean; pRetainClosedDate: Boolean = FALSE);
    procedure QuickClose(TicketID: Integer; const Status: string);
    procedure GetStatusStrings(Strings: TStrings; ExcludeMinusR: Boolean);
    procedure GetHighProfileReasons(Reasons: TStrings);
    procedure GetInvoiceCodeList(Descriptions, Codes: TStrings);
    function EmpID: Integer;
    procedure UpdateTicketInCache(TicketID: Integer; ModifiedDate: TDateTime = 0.0);
    {Damage related calls are now in LocalDamageDMu - QMANTWO-681 EB}
    procedure UpdateWorkOrderInCache(const WorkOrderID: Integer);
    procedure GetTicketHistory(TicketID: Integer);
    procedure GetWorkOrderHistory(WorkOrderID: Integer);
    property OnUpdateCountLabels: TNotifyEvent read FOnUpdateCountLabels write FOnUpdateCountLabels;
    property OnReportStatusChange: TNotifyEvent read FOnReportStatusChange write FOnReportStatusChange;
    function GetRefList(const RefNames: string; Strings: TStrings; RaiseException: Boolean = True; const Modifier: string = ''): Integer;
    function GetRefCodeList(const RefNames: string; Strings: TStrings; const Modifier: string = ''): Integer;
    function GetRefDisplayList(const RefNames: string; Strings: TStrings; const Modifier: string = ''; IncludeBlank: Boolean = False): Integer;
    function GetRefCodeForDisplay(const RefName, Display: string; const Modifier: string = ''): string;
    function GetRefModifier(const RefName, Code: string): string;
    procedure GetRefModifierList(const RefName, Code: string; Modifiers: TStrings);
    function GetRefLookupList(RefType: string; ReferenceList: TStrings; SortOrder: Boolean): Boolean;  //QM-933 EB
    procedure GetRefPlusCodeLookupList(RefType: string; ReferenceList: TStrings);
    procedure GetInspectionRefList(RefType: string; ReferenceList: TStrings);
    procedure GetWorkPriorityRefList(ReferenceList: TStrings); // QM-376 EB
    procedure CallCenterList(List: TStrings; IncludeBlank: Boolean = False);
    procedure CallCenterDescriptionList(CodeLookupList: TCodeLookupList; IncludeBlank: Boolean);
    procedure CallCenterListForTickets(CodeLookupList: TCodeLookupList; IncludeBlank: Boolean);

    procedure OfficeList(List: TStrings);
    procedure ProfitCenterList(List: TStrings);
    procedure SolomonProfitCenterList(List: TStrings);
    procedure ClientList(const CallCenters: string; List: TStrings);

    procedure BillableCenterGroupList(List: TStrings; Rights: array of string; IncludeBlank: Boolean = False);
    procedure CustomerList(List: TStrings; IncludeBlank: Boolean = False);
    procedure ReportableTimesheetProfitCenterList(List: TStrings);
    procedure TimesheetProfitCenterList(List: TStrings);
    procedure UploadLocationList(List: TStrings);
    procedure ReportHierarchyOptionList(List: TStrings);
    function HaveSyncedData: Boolean;
    function IsLocateEditable(LocateID: Integer): Boolean;
    function IsLocateInvoiced(LocateID: Integer): Boolean;

    procedure CloseTicketDataSets;
    procedure CloseWorkOrderDataSets;
    procedure FlushDatabaseBuffers;
    destructor Destroy; override;
    procedure SetTicketAcknowledged(TicketID: Integer);
    procedure CleanUQFilesFromTempDir;
    function GetFirstLocatorOnTicket(TicketID: Integer; var LocatorID: Integer): Boolean;
    property OnDisplayLocateHours: TDisplayLocateHoursEvent read FDisplayLocateHoursEvent write FDisplayLocateHoursEvent;
    property OnEditTimesheet: TEditTimesheetEvent read FOnEditTimesheet write FOnEditTimesheet;
    procedure SetupLocateHoursData(TicketID: Integer);
    procedure SetDateFieldGetTextEvents(DataSet: TDataSet);
    procedure SetRefFieldGetSetTextEvents(Field: TField; const RefNames: string);
    procedure PopulateCarrierList(List: TStrings);
    function ChargeForCOV: Boolean;
    procedure SendChangesToServer;
    procedure SendTimeChangesToServer;
    function GetCallCenterForClientID(ClientID: Integer): string;
    function GetCallCenterAlertPhoneNoForCallCenterID(CallCenterCode: string): string;
    procedure GetFollowupTicketTypesForCallCenter(const CallCenter: string; List: TStrings);
    procedure SetTicketFollowupType(TypeID: Integer; TransmitDate, DueDate: TDateTime);
    procedure SetTicketModified;
    procedure SetWorkOrderModified;
    function GetClientUtilityType(ClientID: Integer): string;
    procedure GetClientDataForLocateID(LocateID: Integer; var ClientCode: string; var ClientID: Integer);
    function GetClientCodeForLocateID(LocateID: Integer): string;
    function IsClientLocateEditable(ClientID: Integer): Boolean;
    procedure GetAssetTypes(AssetTypes: TStrings);
    procedure UploadPendingAttachments(Location: Integer);
    procedure AddAttachmentToRecord(ForeignType, ForeignID: Integer; const FileName: string; const Source: string = '');
    function GetAttachmentDirectory: string;
    procedure GetAttachmentCriteria(Criteria: TStrings);
    procedure GetWOTicketCriteria(Criteria: TStrings);
    procedure GetInvoiceCriteria(Criteria: TStrings);
    procedure DownloadAttachmentToFile(AttachmentID: Integer; LocalFileName: string);
    function DownloadAttachment(AttachmentID: Integer;
      DownloadFileNumber: Integer=0; NumberFilesToDownload: Integer=0): string;
    function IHaveAssets: Boolean;
    function CreateHoursCalculatorForEmployee(EmpID: Integer): TBaseHoursCalculator;
    procedure DownloadAndEditTimesheetDay(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode);

    function GetProfitCenterForTicket(TicketID: Integer): string;
    function GetProfitCenterForClient(ClientID: Integer): string;
    function GetProfitCenterForOffice(OfficeID: Integer): string;
    function GetProfitCenterForEmployee(EmployeeID: Integer): string;
    function GetAttachmentDownloadFolder: string;
    function GetForeignTypeDirFromForeignType(ForeignType: Integer): string;
    procedure RequestPasswordChange(ForceChange: Boolean);
    procedure CallingServiceName(Name: string);
    procedure DumpDataIfNewUserOrServer;
    procedure StartReport(OutputType: string; Params: TStrings);
    function CreateNewTicket(Source: TDataSet): Integer;
    procedure RemoveNewTicket;
    procedure GetClientListForCallCenter(CallCenter: string; ClientList: TStrings; TicketID: Integer = -1);
    procedure GetProjectClientList(CallCenter: string; ParentClientId: integer; ClientList: TStrings; TicketID: Integer = -1);
    function LocateHasChildLocates(TicketID, LocateID: Integer): boolean; //EB QMANTWO-616
    function ClientHasSubClients(ParentClientID: integer): boolean;   //EB QMANTWO-616
    function GetClientCode(ClientID: Integer): string;
    procedure UpdateProfitCenterCache;
    function ValidStatusForClient(ClientID: Integer; Status: string): Boolean;
    function AddLocateToTicket(TicketID, ClientID, AssignedTo: Integer): Integer;

    function GetPCFromTimesheetComboText(const Text: string): string;
    procedure SetPasswordExpirationDate(Date: TDateTime);
    procedure SetupLocatePlatData(TicketID: Integer);
    procedure GetLocatePlatList(LocateID: Integer; PlatList: TStrings);
    function UnitTypeForLocate(const LocateID: Integer; var UnitType: string): Integer;
    procedure AreaList(Areas: TStrings);
    procedure SetTicketRouteArea(const NewAreaID: Integer);
    procedure CustomerInvoiceList(CustomerInvoices: TStrings; const CustomerID: Integer=0);

    class procedure RegisterForm(const FormID: string; FormClass: TEmbeddableFormClass);
    class function GetRegisteredFormClass(const FormID: string): TEmbeddableFormClass;
    procedure SetupQMService;
    procedure SetupQMLogic2Server;
    function ServerInClient: Boolean;
    procedure AckMessages(const RuleID: Integer=-1);
    function SendMessage(const MessageNumber, Destination, Subject, Body: string;
      ShowDate, ExpirationDate: TDateTime; SendToEmpID: Integer; Directed: boolean = False): string;
    procedure SendTextToLocator(const SendToEmpID: Integer; TextToSend: String);
    procedure SetMessageActive(const MessageID: Integer; const IsActive: Boolean);

    function GetSearchResultsRecordCount(XMLString: string): Integer;
    procedure GetOngoingStatuses(LocateID: Integer; Statuses: TStrings);
    procedure GetEditDueDateStatuses(LocateID: Integer; Statuses: TStrings);  //QM-216 EB
    function IsDueDateStatus(Status: string; LocateID: Integer): boolean;     //QM-216 EB
    function UpdateBulkDueDates(ListOfTickets: TStrings; NewDateTime: TDateTime): boolean; //QM-959 EB Bulk Due Date
    function GetStatusGroup(const GroupName: string; Statuses: TStrings): Boolean;
    function MatchesStatusGroup(const StatusGroupName, Status: string): Boolean;
    function GetCenterGroup(const GroupName: string; Centers: TStrings): Boolean;
    function MatchesCenterGroup(const GroupName, CallCenter: string): Boolean;

    procedure PopulateScheduleSortByList(List: TStrings);
    procedure GetQuickCloseStatuses(CallCenter: string; Statuses: TStrings);
    procedure ApplyEmployeeStatusLimit(CustomerStatuses: TStrings);
    procedure RefreshMyAllowedStatusCodes;
    function ValidStatusForEmployee(const Status: string): Boolean;
    function ValidExclusionTicketType(LocClientID:integer; Status:String; ticketType:String; exclusionType: string):Boolean;//qm-265 sr
    function GetSyncReminderInterval: Integer;
    procedure ClientListForProfitCenter(const ProfitCenter: string; List: TStrings);
    function IsManualTicket(TicketID: Integer): Boolean;
    function CanAccessProfitCenterCache: Boolean;
    function CanPlaceOnPerDiem: Boolean; // QMANTWO-789 sr
    function FindLocatorByPartialShortName(LocatorName: string; ManagerList: TStrings; EmployeeStatus: Integer; Reset: Boolean = False): Integer;
    function GetCustomerBillingPeriod(CustomerID: Integer): string;
    function UserHasLimitedHours: Boolean;
    function GetFieldLength(const TableName, FieldName: string): Integer;
    procedure VerifyNoteLength(const NoteText: string);
    function IsAttachmentDriveLetter(DriveLetter: string): Boolean;
    function IsTicketAssignedToLocator(const TicketID, LocatorID: Integer): Boolean;
    function TicketNeedsArrivalDate(const TicketID: Integer): Boolean;
    procedure CreateArrival(const TicketOrDamageID: Integer; const EntryMethod: string; const EmpID: Integer; const When: TDateTime; const ArrivalType: TArrivalType; const LocationStatus: String='');
    procedure DeleteJobsiteArrival;
    procedure OpenJobsiteArrivalData(const TicketOrDamageID: Integer; const ArrivalType: TArrivalType);

    procedure GetLocateListForTicket(TicketID: Integer; List: TStrings);
    procedure GetLocatorListForTicket(TicketID: Integer; List: TStrings);
    procedure SaveLocateFacilityChanges;
    function IsLocateFacilityEditable(LocateFacilityID: Integer): Boolean;
    function GetLocateFacilityTypeDesc(const LocateID: Integer): string;
    procedure TicketAndLocatesList(TicketNumber: string; List: TStrings);
    procedure SaveTicketNoteToCache(const Note: string; ForeignType, ForeignId: Integer);
    procedure SaveBaseNoteToCache(const Note: string; ForeignType, ForeignID: Integer;
                                  CachedNotesData: TDataSet; NoteSubType: Integer = 0; SubTypeText:string = '');
    procedure DeleteNote(CachedNotesData: TDataSet); //QM-188 EB Delete Note
    procedure ChangeNoteToPrivate(CachedNotesData: TDataSet); //QM-188 EB
    procedure ChangeNoteToPublic(CachedNotesData: TDataSet); //QM-188 EB 


    procedure GetAttachmentList(ForeignType, ForeignID: Integer; List: TStrings; ImagesOnly: Boolean);
    function GetTicketID(const TicketNumber: string; const CallCenter: string; const TransmitDateFrom, TransmitDateTo: TDateTime): Integer;

    function CanIPrintTickets: Boolean;
    function CanViewEmergencies: Boolean;  //QM-11 SR
    procedure SetTicketPriority(TicketID: Integer; TicketPriorityRefID: Integer);
    function GetTicketPrioritySort(const WorkPriorityID: Integer): string;
    function GetTicketPriority(const TicketID: Integer): string;
    function GetTicketPriorityIDByCode(Code: string): integer;
    function UnackedMessageCount: Integer;
    function UnackedBreakRuleMessageCount: Integer;
    function MessageExists(MsgSearchText: string; EmpID: Integer; InDate: TDateTime): Boolean;
    function TicketViewLimitForLocator(const EmployeeID: Integer): Integer;
    function ShowFutureWorkForLocator: Boolean;
    function MyTicketViewLimit: Integer;
    procedure SetEmployeeTicketViewLimit(EmployeeID, TicketViewLimit: Integer);
    function CopyAttachmentToDownloadFolder(AttachmentID: Integer): string;
    procedure GetPrintableAttachmentList(ForeignType, ForeignID: Integer; List: TStrings);
    function GetFileExtensionForPrintableFileType(FileType: string): string;
    function GetMarkableUnitStatusCodes: TStringArray;
    procedure UpdateLocatesTotalUnits;
    function ValidStatusForTicket(TicketID: Integer; MeetOnlyStatus: Boolean): Boolean;
    function IsValidStatusAll(pStatus: string; pTicketID: integer; pTktType: string;
                              pClientID: integer; pMeetOnly: boolean; pTTExclusion: string): Boolean;   //QM-703 Dropdown Issue EB
    function IsMeetTicket(TicketID: Integer): Boolean;
    function IsTicketDetailHidden: Boolean;
    procedure SaveArrivalsToServer(const TicketOrDamageID: Integer; const ArrivalType: TArrivalType);
    procedure ShowTransientMessage(const Msg: string);
    procedure GetLimitedTicketIDList(List: TStrings; TicketLimit: Integer);
    procedure SafeSendChangesToServer;
    procedure SetTicketActivitiesAcknowledged(ActivityAckIDArray: TIntegerArray);
    procedure GetValidStatusesForTicketAndClient(Statuses: TStrings; TicketID:Integer; ClientID: Integer);
    function GetLocatorByTicketAndClient(const TicketID, ClientID: Integer; var EmployeeID: Integer): Boolean;
    function GetConfigurationDataValue(const SettingName: string; const Default: string = ''; IgnoreCase: boolean = FALSE): string;
    function GetConfigurationDataInt(const SettingName: string; const Default: Integer = -1): Integer;
    function GetTicketImage(TicketData: TDataSet; var TicketIsXml: Boolean): string;
    procedure EmpTimeRuleList(List: TStrings; IncludeBlank: Boolean=False);
    procedure AddBreakRuleResponse(const RuleID: Integer; const Response, ContactPhone: string; const ResponseDate: TDateTime; const MessageDestID: Integer = -1);
    function CreateExportFileName(const Name, Extension: string; LocatorName: string = ''): string; //QM-10 EB By Default, if blank, the user
    procedure CleanExportFolder(const DaysOld: Integer);
    function FirstTicketTaskedForTomorrow(var TicketID: Integer): Boolean;
    procedure AddTicketToTaskSchedule(const EmpID, TicketID: Integer; const EstStartDate: TDateTime);
    procedure SetFirstTaskForTomorrowPerformed(const TicketID: Integer; const PerformedDate: TDateTime);
    procedure ExportQMDataAsXML;
    procedure TimesheetChangeReasonList(List: TStrings; IncludeBlank: Boolean=False);

    procedure GetChangeReasonList(List: TStrings; ReasonType: string; IncludeBlank: Boolean=False);
    function AttachmentManager: TLocalAttachment;
    procedure CreateBackgroundUploader;
    procedure BackgroundUploaderStart;
    procedure KillBackgroundUploader;
    procedure SetTrayIconHint(const Msg: string);
    procedure AddToQMLog(const Msg: string);
    function SafeToSyncNow(var NoSyncReason: string): Boolean;
    function AnyDatasetInEditMode(var NameOfDatasetInEditMode: string): Boolean;
    procedure GetAutoSyncIdleLimits(var DelaySeconds, DelaySecondsMinimized, SecondsBetween: Integer);
    function HavePendingTimesheetChanges: Boolean;
    procedure GetAttachmentParentData(const ForeignType, ForeignID: Integer);
    function IsESketchFilename(const Filename: string;
      out TicketNumber: string): Boolean;
    function ESketchFileMask(const TicketNumber: string): string;
    function AddAutoAttachments(const ForeignType, ForeignID: Integer;
      const TicketNumber: string='';
      const AttachmentFileMask: string='*.*';
      const AttachmentSource: string=AutoAttachmentSource;
      const WarnIfNoAttachments: Boolean=True): boolean;
    function HasPendingAutoAttachments: boolean;  //QM-966 EB Fix Required Pics and Notes
    function GetTicketWorkViewHTML(FullScreen: Boolean): string;
    function GetLatestTicketVersionForTicket(TicketID: Integer): Variant;
    procedure ShutdownWindows;
    function GetLunchStart(const WorkDate: TDateTime): TDateTime;
    function AmIClockedIn: Boolean;
    function IsLunchLockout: Boolean;
    function GetLunchUnlockTime: TDateTime;
    function ConfirmShortLunchBreak: Boolean;
    procedure AssignLocalKeys(Data: TDataSet; const Prefix: string);
    function WeekHasAdditionalWorkTimes(EmpID: Integer; WeekStartDate: TDateTime): Boolean;
    function GetMyGPSPosition(var Lat, Lon: Double; var HDOP: Integer): Boolean;
    function SaveMyGPSPosition(TicketID: integer = 0): Variant;   //QM-799 EB Input Ticket ID
    procedure BackgroundGPSStart;
    procedure BackgroundGPSStop;
    function GetUserProfitCenter: string;
    function CanBackgroundUpload: Boolean;
    procedure SetEmployeeShowFutureWork(EmpID: Integer; ShowFutureTickets: Boolean);
    function GetIDListFromObjects(List: TStrings): string;
    function GetWorkOrderImage(WorkOrderData: TDataSet; var WOIsXml: Boolean): string;
    procedure SaveBaseNotesFromCache(Source: string; CachedNotesData: TDataSet);
    procedure PrepareToImportTime;
    procedure StartTimeImportTimer;
    procedure StopTimeImportTimer;
    procedure InitializeHttpServer(IdleTimer: TOdIdleTimer);

    procedure SaveGpxToFile(D: TDataSet; const FileName: string; var GPXOptions: TGPXOptions);  //QM-10 EB
    procedure SaveGpxToStream(D: TDataSet; Stream: TStream);
    procedure SaveWMGpxToStream(D: TDataSet; Stream: TStream; var GPXOptions: TGPXOptions);
    function GetWOSource: string;
    procedure PopulateActiveStatusList(List: TStrings);
    function OpenLocStatusQuestions(ClientID: Integer; NewStatus: String): Boolean;
    procedure AddTicketInfo(TicketID: Integer; const InfoType, Data: string);
    function GetCallCenterForTicket(TicketID: Integer): string;
    function GetLocStatusPresets(TicketID: Integer; Presets: TStringList): Boolean;
    procedure LoadNoteSubTypeList(NoteType: Integer; SubTypeList: TStrings);
    procedure NotesSubTypeList(CodeLookupList: TCodeLookupList; ForeignType: Integer);
    function GetSubTypeDescription(Code:Integer): string;
    function ChangeAllLocStatus(TicketID: Integer; NewStatus: String): Boolean;
    //Ken Funk 2016-01-14
    procedure GetLocationMarkStatusList(var List: TStringList);
    function CheckNoWOFilter: boolean; //QMANTWO-446 EB - temporary check (can be removed after everyone has upgraded)
    function IsHoliday(ADate: TDateTime; CallCenter: string): boolean;  //QMANTWO-503 EB
    function MoveTheseTickets(pTickets: TStringlist; pFromLocatorID, pToLocatorID:Integer): boolean;  //QMANTWO-573  EB
    function MoveTheseLocates(pLocates, pNewLocators: TIntegerList): boolean;
    procedure AddLocatetoProject(pTicketID, pClientID, pTempLocateID, pAssignedToID: Integer);  //QMANTWO-616
    function GetFullTicketAddress(pTicketID: Integer): string;    //EB TRY THIS

    {QM-697 Resurrect Route Order from QMANTWO-775 2019}
//    function SetRouteOrder(pTicketID: Integer; pRouteOrder: Double): string; //QM-166 EB Set Route Order
//    function ScanROforLocator(LocatorID: integer; var ARouteArray: TDyRouteArray): integer;
    function GetSavedROTicketInfo(ParentTicketID: integer): boolean;
    function GetEarlierRouteOrderArray(pTicketID: Integer; CurRouteOrder: Double; var Count: integer): TRouteArray;

    procedure updatePhoneNo(const EmpID: Integer; phoneNumber: String);  //qm-388  sr

    {QM-164 EB Config changes}
    procedure ReplaceDeviceInfo(ADeviceRec: TDeviceRec);
    procedure SaveHP_TicketType(pTicketID: Integer);    //qm-373 SR
    procedure SaveTicketDueDate(pTicketID: integer; NewDueDate, OldDueDate: TDateTime); //QM-216 EB
    procedure SaveTicketReschedTicketType(pTicketID: integer);
    function BulkTicketDueDatesToServer(TicketStr: string; NewDueDate: TDateTime): string; //QM-959 EB Bulk Ticket Due Date change

    {QM-585 EB Employee PLUS Whitelist}
    function PlusWhiteListed(pClient_Code: string): boolean;
    function IsRestricted(pClient_Code: string): boolean;

    {QM-635 EB Status Plus}
    function IsStatusPlus(pStatus, pClientCode, pCallCenter: string; var pResultStr: string; var pTempStrList: TStringList): boolean;
    function UseStatusPlus(pStatus: string; pClientID: integer; pClientCode: string; pCallCenter: string; var pDataSet: TDBISAMQuery): boolean;
    function IsStatusPlusException(pStatus, pClientCode, pCallCenter: string): Boolean;

    {QM-771 EB Ticket Alerts}
    procedure GetTicketAlerts(TicketID: integer);

    {QM-966 Required Notes and Pictures}
    function IsAutomatedNoteColor(NoteText: string; var OutColorText: TColor; var OutColorHighLight: TColor): Boolean;   //QM-905 EB Note Colorizatio
    function IsAutomatedNoteCat(NoteText: string; var OutCategory: integer): boolean;

    function CheckInternet: Boolean;


    {QM-1037 Bigfoot (ESketch footage changes)}
    function GetLocateHoursLength(LocateID: integer; EsketchFlag: Boolean): integer;

    {QM-1042}
    procedure RefreshLocateData;
end;

var
  DM: TDM;

implementation

{$R *.DFM}

uses DateUtils, OdExceptions, JclStrings, OdDbUtils, Contnrs,
  OdDBISAMXml, LocalDataDef, OdGetNewPassword, UQUtils, 
  QMServerLibrary_Intf, OdSecureHash, OdHourGlass,
  Variants, JclSysInfo, JclFileUtils, OdIsoDates, IdTCPClient,
  JclShell, ServiceLink, OdDBISAMUtils, ContactName, OdWmi,
  LengthConverter, TicketImage, ExportLocalData, OdLog, OdAckMessageDlg,
  TicketHighlighter, ApplicationFiles, JclMiscel, BreakRules, HoursDataset,
  TimeClockImport, {DamageProfitCenterRules,} SharedDevExStyles, WorkOrderImage,
  TimeFileMonitor, QMTempFolder, PerlRegEx, OdNetUtils, LocateStatusQuestions, strUtils,
  LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDMu, OdCdsUtils, PsUtils;  //QM-147 EB POwershell

const
  YesValues: array [0..5] of string = ('Y', 'YE', 'YES', '1', 'T', 'True');
  NoValues:  array [0..4] of string = ('N', 'NO',        '0', 'F', 'False');
  SchemaResourceName = 'CLIENT_SCHEMA';
  SchemaResourceType = 'STRING';
  ColDelim = ':';
  ESketchFilenamePrefix = 'manifest';
  ESketchFilenameRegularExpression = '^' + ESketchFilenamePrefix +
    ' (.*)-[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{4}$';



var
  PrivateRegisteredForms: TStringList;
  TempFolder: ITemporaryFolder;

{ TDM }

procedure TDM.Connect(const UserName, Password: string);
begin
  // Don't do anything that touches the local DB here, since it may not exist yet
  Assert(Assigned(Engine), 'Engine must be assigned in Connect');  // An engine/server is required before login
  UQState.UserName := UserName;
  UQState.Password := Password;

  if UserName = '' then
    raise ELoginFailure.Create('Login failure, username empty');
  if (UQState.DeveloperMode) and (UQState.DebugADIgnore) then   //QMANTWO-607 EB - Developer Debug allows bypassing
    Ad_Active := true
  else if not CheckADLogin(UserName, Password) then     //QMANTWO-521
      AD_Active := false
  else
     AD_Active := true;  //QMANTWO-521


  // Check to see if this is a new user/password/url/role/EmpID/UserID
  if not MatchesPreviousNamePW then
    CheckLogin(UserName, Password);

  StartUp;
end;

procedure TDM.CheckLogin(const UserName, Password: string);
var
  ChangePassword, RetEmpID, RetUserID: Integer;
  Role, ShortName, APIKey: string;
  ExpirationDate: TDateTime;
begin
  // Don't do anything that touches the local DB here, since it may not exist yet
  Assert(Assigned(Engine), 'Engine must be assigned in CheckLogin');
  try
    Assert(Assigned(Engine.Server), 'Engine.Server must be assigned in CheckLogin');

    if (UQState.DeveloperMode) and (UQState.DebugADIgnore) then   //QMANTWO-607 Different call for development
      Engine.Login(UserName, Password, RetEmpID, RetUserID, ChangePassword, ExpirationDate, Role, ShortName)
    else
      Engine.Login2(UserName, Password, RetEmpID, RetUserID, ChangePassword, ExpirationDate, Role, ShortName, APIKey);

    UQState.EmpID := RetEmpID;
    UQState.DisplayName := ShortName;
    UQState.UserType := Role;
    UQState.UserID := RetUserID;
    UQState.APIKey := APIKey;
    if ChangePassword = PasswordChangeSuggest then
      RequestPasswordChange(False)
    else if ChangePassword = PasswordChangeForce then
      RequestPasswordChange(True);
    SaveNamePwHash;
  except
    ClearNamePwHash;
    UQState.EmpID := 0;
    UQState.UserID := 0;
    UQState.UserType := '';
    UQState.APIKey := '';
    raise;
  end;
end;

function TDM.CheckADLogin(const UserName, Password: string): boolean;
begin
  if (UQState.DeveloperMode) and (UQState.DebugADIgnore) then    //EB QMANTWO-607
    Result := True  //Don't check
  else
    Result := Engine.LoginAD(UserName, Password)  //QMANTWO-521
end;

function TDM.CheckNoWOFilter: boolean;
const
  SQL = 'select count(*) N from work_order_work_type where wo_source <> ''''';
var
  ValidCount: integer;
begin
  Result := True;
  ValidCount := Engine.RunSQLReturnN(SQL);
  if ValidCount > 0 then
    Result := False
end;

procedure TDM.StartUp;
begin
  GetChan.MiscAppend := AppVersion + '-' + IntToStr(UQState.EmpID);

  TempFolder := NewTemporaryFolder;
  DBISAMTb.Session.PrivateDir := TempFolder.FullPathName;
  Engine.SetSecurityInfo(UQState.UserName, UQState.Password);
  CheckForInstalledComponents;

  UpgradeDatabase;
  DumpDataIfNewUserOrServer;
  CheckForAndFixCorruptTables;
  UpdateDatabaseSchema;
  UpdateSyncStatusTable;
  RemoveOldRecords;
  // Don't do anything at the application-level that accesses the local database until after this point
  CleanUQFilesFromTempDir;

  FLocalAttachment := TLocalAttachment.Create(AppName, Database1.DatabaseName, EmpID, False);
  FLocalAttachment.OnLogMessage := Engine.AddToMyLog;
  FLocalAttachment.OnNeedDataFromServer := NeedAttachmentParentData;
  FLocalAttachment.CleanAttachmentDownloads;

  FPlusRestrictedList.CC := TStringList.Create;  //QM-585 EB PLUS WHITELIST

  CleanExportFolder(1);
  CleanAutoAttachFailedFolder(DaysToKeepFailedAttachments);
  CleanTimeImportFolders(DaysToKeepFailedImports);
  CleanGPSLogFiles(DaysToKeepGPSLogs);

  UpdateCounts;
  StatusList.Open;

  SetLocalConversionDatabase(Database1);
  LogLastPowerOnOffEvents;
  FixTicketNotes;
  RequireTktStrMap:= False;  //qm-777 sr
  AllowTktStrMap:= PermissionsDM.CanI(RightEnableTicketStreetMap); // qm-646 disabled until street map converted.
  If PermissionsDM.GetEmployeeLimitation(EmpID,RightEnableTicketStreetMap)='MyWork' then  //qm-777 sr
  RequireTktStrMap:= True;
  if not UQState.DeveloperMode then begin
    RunUpdateFile;
    UQState.SaveToIni; // This saves the last boot/shutdown date/time and the login data in case of an app crash
  end;

//  if CheckInternet and AllowTktStrMap then //QMANTWO-763     qm-646 disabled until street map converted.
//  frmMapShell:= TfrmMapShell.Create(self); //QMANTWO-711
//  if ((CheckInternet = false) and (AllowTktStrMap = true)) then //QMANTWO-763
//    MessageDlg('You do not have Internet Access. Ticket Street Maps will not be available without Internet Access.'
//    +#13+#10+'When you gain access to the Internet, re-start QManager and the Ticket Street Maps will be available.', mtWarning, [mbOK], 0);

end;

destructor TDM.Destroy;
begin
  Database1.CloseDataSets;
  FlushDatabaseBuffers;
  FreeAndNil(FStatusStrings);
  FreeAndNil(FLocalAttachment);
  FreeAndNil(FMyAllowedStatusList);
  FreeAndNil(FBackgroundUploader);
  FreeAndNil(FPlusRestrictedList.CC);  //QM-585 EB PLUS WhiteList
  BackgroundGPSStop;
  inherited;
end;


procedure TDM.PrepareToSync;
var
  i: Integer;
begin
  Assert(Assigned(Engine), 'Engine must be assigned in PrepareToSync');

  for i := 0 to Database1.DataSetCount - 1 do
    with Database1.DataSets[i] do
      if State in dsEditModes then
        Post;
  FlushDatabaseBuffers;
  CloseDataSetsForRefresh;
  UQState.PreviousServerURL := UQState.ServerURL;
end;

procedure TDM.PrepareToImportTime;
var
  i: Integer;
  WarnUser: Boolean;
begin
  WarnUser := False;
  for i := 0 to Database1.DataSetCount - 1 do begin
    if Database1.DataSets[i].State in dsEditModes then begin
      WarnUser := True;
      Database1.DataSets[i].Cancel;
    end;
  end;
  if WarnUser then
    ShowTransientMessage('Q Manager changes were cancelled so time files could be imported.');
end;

procedure TDM.SyncNow;
var
  Errors: TObjectList;
begin
  Errors := TObjectList.Create;
  try
    try
      AttachmentDataCS.Enter;
      try
        PrepareToSync;
        Engine.Sync(EmpID, Errors, SyncRequestSource);
        FlushDatabaseBuffers;
      finally
        AttachmentDataCS.Leave;
      end;
      UpdateProfitCenterCache;
      FlushDatabaseBuffers;

      ProcessConfigurationData;
      Engine.AddToLog('------------------');
      CheckForPasswordChange;
      CheckForUnackedMessages;

      UQState.LastLocalSyncTime := Now;
      UpdateCounts;
      ExportQMDataAsXML;
      Engine.HandleSyncErrors(Errors);
    except
      if IsEmpty(SyncRequestSource) then
        EmployeeDM.AddEmployeeActivityEvent(ActivityTypeSyncFailed, '')
      else
        EmployeeDM.AddEmployeeActivityEvent(ActivityTypeAutoSyncFailed, '');
      raise;
    end;
  finally
    SyncRequestSource := '';
    FreeAndNil(Errors);
  end;
end;

procedure TDM.SendChangesToServer;
var
  SyncErrors: TObjectList;
begin
  PrepareToSync;
  SyncErrors := TObjectList.Create;
  try
    Engine.SendChangesToServer(EmpID, SyncErrors);
    Engine.HandleSyncErrors(SyncErrors);
  finally
    FreeAndNil(SyncErrors);
  end;
  BackgroundUploaderStart;
end;

procedure TDM.SafeSendChangesToServer;
begin
  try
    SendChangesToServer;
  except
    on E: Exception do begin
      ShowTransientMessage('Warning: Unable to sync changes immediately.' +
        sLineBreak + 'Reason: ' + E.Message + sLineBreak +
        'The changes will be re-sent on the next ticket status change or sync.');
    end;
  end;
end;

procedure TDM.SendTimeChangesToServer;
begin
  SendChangesToServer;  // for now, send all of them, but that's not ideal
end;

function TDM.UpdateBulkDueDates(ListOfTickets: TStrings;         //QM-959 EB Bulk Due Dates
  NewDateTime: TDateTime): boolean;
var
 i: integer;
begin
  {This is the end call after tickets have been selected.  Putting it here in case
   we have any other needs for adjusting}
   if not assigned(ListOfTickets) or (ListOfTickets.Count < 1) then
     Result := False
   else begin
     ListOfTickets.Delimiter := ',';
     ListOfTickets.StrictDelimiter := True;

     {Check each ticket to make sure it contains a viable status group/status (EditTicketDueDate-)}
     for i := 0 to ListOfTickets.Count - 1 do
     begin
       
     end;
   end;
end;

procedure TDM.UpdateCounts;

  function GetQueryResult(Query: TDBISAMQuery; Kind: string): Integer;
  begin
    Query.Close;
    Query.ParamByName('emp_id').AsInteger := EmpID;
    Query.ParamByName('kind').AsString := Kind;
    Query.ParamByName('last_sync').AsDateTime := UQState.LastLocalSyncTime;
    Query.Open;
    Result := Query.RecordCount;
    Query.Close;
  end;

begin
  if not LocalTableExists('ticket') or not LocalTableExists('locate') then
    Exit;

  nEmergency := GetQueryResult(TicketKindCount, TicketKindEmergency);
  nNew := GetQueryResult(TicketKindCount, TicketKindNormal);
  nOngoing := GetQueryResult(TicketKindCount, TicketKindOngoing);
  nOpenDamages := GetOpenCount('damage', LDamageDM.OpenDamages);
  nOpenWorkOrders := GetOpenCount('work_order', OpenWorkOrders);

  if Assigned(FOnUpdateCountLabels) then
    FOnUpdateCountLabels(Self);
end;

function TDM.GetOpenCount(const TableName: string; const Query: TDBISAMQuery): Integer;
begin
  Result := 0;
  if not LocalTableExists(TableName) then
    Exit;

  Query.Close;
  Query.ParamByName('emp_id').AsInteger := EmpID;
  Query.Open;
  Result := Query.RecordCount;
  Query.Close;
end;

procedure TDM.ZeroCounts;
begin
  nEmergency := 0;
  nNew := 0;
  nOngoing := 0;
  nOpenDamages := 0;

  if Assigned(FOnUpdateCountLabels) then
    FOnUpdateCountLabels(Self);
end;

procedure TDM.CloseQueries;
begin
  Database1.CloseDataSets;
end;

procedure TDM.DumpDataIfNewUserOrServer;
var
  UserChanged: Boolean;
  ServerChanged: Boolean;
begin
  UserChanged := (UQState.UserName <> UQState.PreviousUser);
  ServerChanged := (not UQState.AppLocal) and (UQState.PreviousServerURL <> '') and (UQState.ServerURL <> UQState.PreviousServerURL);
  if UserChanged or ServerChanged then begin
    Application.NormalizeTopMosts;  //EB - Try this to see if we can prevent hidden dialogs
    if UQState.DeveloperMode and (MessageDlg('DeveloperMode: Delete local data?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
      Exit;
    Application.RestoreTopMosts;
    DumpLocalData;
  end;
end;

procedure TDM.DumpLocalData;
begin
  CloseQueries;
  Engine.EmptyAllTables;
  UQState.LastLocalSyncTime := 0;
  UQState.SaveToIni;
  ZeroCounts;
end;

procedure TDM.LocateDataStatusChange(Sender: TField);
var
  EnteredStatus: string;
begin
  EnteredStatus := LocateData.FieldByName('status').AsString;
  if EnteredStatus = EmergencyAckStatus then
    Exit; // EB - We do not need to change any other information nor validate it

  StatusList.Open;
  if StatusList.Locate('status', EnteredStatus, []) then begin
    LocateData.FieldByName('closed').AsBoolean :=
      StatusList.FieldByName('complete').AsBoolean;
  end else
    raise Exception.Create('Invalid Status: ' + EnteredStatus);
  if LocateData.FieldByName('qty_marked').AsInteger = 0 then
    LocateData.FieldByName('qty_marked').AsInteger := 1;

  if IgnoreSetStatus then   // this is being done by the software (hours entry)
    Exit;                   // so don't try to pop up a dialog

  if ((EnteredStatus = 'OH') or (EnteredStatus = 'H')) then begin
    ShowLocateHoursForm(LocateData.FieldByName('ticket_id').AsInteger,
      LocateData.FieldByName('locate_id').AsInteger, True, False);
  end;
end;

procedure TDM.GoToTicket(ID: Integer);
begin
  //TicketDueDateChanged := False;  {!}
  Ticket.DisableControls;
  try
    CloseTicketDataSets;
    Ticket.Open;
    Ticket.IndexFieldNames := 'ticket_id';
    Ticket.BeforePost := Engine.BeforePost;
    Ticket.SetRange([ID], [ID]);
    if Ticket.Eof then
      raise Exception.Create('Ticket not in cache: ' + IntToStr(ID));

    SetupLocatePlatData(ID);

    ClientLookup.Open;
    OpenLocateData;
    OpenTicketNotesData;
    SetupLocateHoursData(ID);
    SetupLocateFacilityData(ID);

    // The Delphi checkbox does not like NULLs
    if Ticket.FieldByName('watch_and_protect').IsNull then begin
      Ticket.Edit;
      Ticket.FieldByName('watch_and_protect').AsBoolean := False;
      Ticket.Post;
    end;

  finally
    Ticket.EnableControls;
  end;
end;

procedure TDM.GoToTicketAck(ID: Integer);
begin
    Ticket.Open;
    Ticket.IndexFieldNames := 'ticket_id';
    Ticket.BeforePost := Engine.BeforePost;
    Ticket.SetRange([ID], [ID]);  //make this the current ticket
    if Ticket.IsEmpty then
      raise Exception.Create('Ticket not in cache: ' + IntToStr(ID));

    //New - for ticket ack changing to "Ack" status
    SetupLocatePlatData(ID);
    OpenLocateData;
end;

procedure TDM.GoToWorkOrder(ID: Integer);
begin
  WorkOrder.DisableControls;
  try
    CloseWorkOrderDatasets;
    ClientLookup.Open;

    WorkOrder.Open;
    WorkOrder.IndexFieldNames := 'wo_id';
    WorkOrder.BeforePost := Engine.BeforePost;
    WorkOrder.SetRange([ID], [ID]);
    if WorkOrder.Eof then
      raise Exception.Create('Work Order not in cache: ' + IntToStr(ID));

    OpenWorkOrderNotesData;

    WorkOrderInspection.IndexFieldNames := 'wo_id'; //TODO review
    WorkOrderInspection.Open;
    WorkOrderInspection.SetRange([ID], [ID]);

    WorkOrder.Refresh;
  finally
    WorkOrder.EnableControls;
  end;
end;

function TDM.LocateNeedsToPost: Boolean;

  function Different(FieldName: string): Boolean;
  begin
    Result := Locate.FieldByName(FieldName).AsString <>
              LocateData.FieldByName(FieldName).AsString;
  end;

begin
  Result := Different('status') or
            Different('qty_marked') or
            Different('high_profile') or
            Different('high_profile_reason') or
            Different('mark_type') or
            Different('length_total');
end;

procedure TDM.SetLocateFieldsInTable;
var
  AllowChanges: Boolean;
  OldLocateClosedDate: TDateTime;
  GPSID: integer;

  procedure CopyField(const FieldName: string);
  const
    NoChanges = 'The locate for %s can not be modified and saved until the status has been set to something other than -R';
  var
    ClientCode: string;
  begin
    if Locate.FieldByName(FieldName).Value <> LocateData.FieldByName(FieldName).Value then begin
      if AllowChanges then
        Locate.FieldByName(FieldName).Value := LocateData.FieldByName(FieldName).Value
      else begin
        ClientCode := Locate.FieldByName('client_code').AsString;
        Locate.Cancel;  // DBISAM had strange lock errors on a second error without this
        raise EOdDataEntryError.CreateFmt(NoChanges, [ClientCode]);
      end;
    end;
  end;

begin
  // Shared field handling for locate insert and updates


  OldLocateClosedDate := Locate.FieldByName('closed_date').AsDatetime;
  if Locate.FieldByName('status').AsString <> LocateData.FieldByName('status').AsString then begin
    AllowChanges := True;
    CopyField('status');
    if (not FRetainClosedDate) then
      Locate.FieldByName('closed_date').AsDateTime := RoundSQLServerTimeMS(Now)  // Used as statused_date
    else begin
      Locate.FieldByName('closed_date').AsDateTime := OldLocateClosedDate;  //QM-463 EB
      if Locate.FieldByName('closed_date').IsNull then   //QM-869 EB Set it anyway if it is null
        Locate.FieldByName('closed_date').AsDateTime := RoundSQLServerTimeMS(Now);
    end;

    Locate.FieldByName('closed_how').AsString := FClosedHow;

    if FStatusAsAssigned then begin
      // The closed_by_id field should be called "statused_by_id"
      Locate.FieldByName('closed_by_id').AsInteger := Locate.FieldByName('locator_id').AsInteger;
      Locate.FieldByName('StatusAsAssignedUser').AsInteger := 1;
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeLocateStatusAsAssignedLocator, Locate.FieldByName('locate_id').AsString, Now, Locate.FieldByName('locator_id').AsString);
    end else begin
      Locate.FieldByName('closed_by_id').AsInteger := EmpID;
      Locate.FieldByName('StatusAsAssignedUser').AsInteger := 0;
    end;

    //Keep track of who actually statused the locate
    Locate.FieldByName('status_changed_by_id').AsInteger := EmpID;
  end
  else // Note: closed_date is set if the status has been changed from -R
    AllowChanges := (Locate.FieldByName('status').AsString <> '-R') or (not Locate.FieldByName('closed_date').IsNull);

  CopyField('qty_marked');
  CopyField('high_profile');
  CopyField('high_profile_reason');
  CopyField('added_by');
  CopyField('closed');
  CopyField('mark_type');
  CopyField('length_total');
  if Locate.FieldByName('ticket_id').AsInteger < 0 then begin
    CopyField('assigned_to');
    CopyField('LocalStringKey');
  end;
  // Skip regular_hours and overtime_hours, since they are not used
  // Make sure all future changes respect the AllowChanges flag

  VerifyShouldSaveForInvoicedLocates(Locate);
  FTicketChanged := True;
  if EditingDataSet(Locate) then begin
    Locate.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
    if PermissionsDM.CanI(RightTrackGPSPosition) then
      GPSID := SaveMyGPSPosition(locate.FieldByName('ticket_id').Value);    //QN-860 EB Fixing this for empty GPS
      if GPSID > 0 then Locate.FieldByName('gps_id').Value :=  GPSID;
  end;
  PostDataSet(Locate);
end;

procedure TDM.SaveLocateChanges;
var
  HPM, HPMO: string;
  LocateID: Integer;
  ClientCode: string;
  TicketID: Integer;
  LocateStatus: string;
  AnyOngoing, AnyOpen: Boolean;
  TicketInfoCollected: Boolean;
  OldStatus, NewStatus: string;
  OldHP, NewHP: boolean;
  FirstName, LastName: string;
  Found : Boolean;
  ClientID: Integer;
  CurLocatorID: Integer;
  QuestionsDisplayedStatuses: TStringlist;   //This just to keep track of statuses where ticket_info for ticket_info
  QANote: string;
  SQCode: integer;  //QM-604 EB Nipsco
  GPSID: integer; // QM-860 locate HP error - adding this to make sure we have a GPS before assigning it.
  
  procedure AskAdditionalQuestions;
  begin
    // If the locate changed from a non-ongoing status to an ongoing status for VA, so we collect contact info
    ClientID := Locate.FieldByName('client_id').AsInteger;
    OldStatus := Locate.FieldByName('status').AsString;
    NewStatus := LocateData.FieldByName('status').AsString;


    CurLocatorID := Locate.FieldByName('locator_id').AsInteger;
    if (not QuestionsDisplayedStatuses.Contains(NewStatus)) and (NewStatus <> OldStatus) then
      if OpenLocStatusQuestions(ClientID, NewStatus) then begin
        if not QuestionsDisplayedStatuses.Contains(NewStatus) then begin
          QuestionsDisplayedStatuses.Add(NewStatus);

          {Status Questions}
          SQCode := TLocStatusQuestions.GetStatusQuestions(TicketID, ClientID, CurLocatorID, OldStatus, NewStatus);
          if (SQCODE = SQFALSE) then begin
            raise EOdDataEntryError.CreateFmt('Q Manager can not save ticket %s because the ticket info is required for ongoing locates', [Ticket.FieldByName('ticket_number').AsString])
          end
          else if SQCODE = SQRS then begin //QM-604 EB Nipsco
             SaveTicketReschedTicketType(TicketID);

          end;

        end;
      end
      else if MatchesStatusGroup('VUPS-RequiresContact', NewStatus) then       //QMANTWO-259
        if not MatchesStatusGroup('VUPS-RequiresContact', OldStatus) then
          if MatchesCenterGroup('RequiresContactName', Ticket.FieldByName('ticket_format').AsString) then begin
            if not GetOngoingContactName(Ticket.FieldByName('ticket_id').AsInteger, FirstName, LastName) then
              raise EOdDataEntryError.CreateFmt('Q Manager can not save ticket %s because the contact name is required for ongoing locates', [Ticket.FieldByName('ticket_number').AsString])
            else begin
              AddTicketInfo(Ticket.FieldByName('ticket_id').AsInteger, ONGOING, FirstName + ' ' + LastName);
              QANote := 'Contact Name Reported: ' + FirstName + ' ' + LastName;                //QMANTWO-720 EB
              SaveNote(QANote, 'Contact', PrivateTicketSubType, qmftTicket, TicketID, True);              //QMANTWO-720 EB
              TicketInfoCollected := True;
            end;
          end;
  end;

  procedure ValidateStatus;
  var
    lStatus: string;   //QM-635 EB Status Plus
    lClientCode: string;
    lCallCenter: string;
    lTicketNum: string;
    lTicketID: integer;

  begin
    lStatus := LocateData.FieldByName('status').AsString;
    lClientCode := LocateData.FieldByName('client_code').AsString;
    lCallCenter := GetCallCenterForTicket(LocateData.FieldByName('ticket_id').AsInteger);
    lTicketNum :=  Ticket.FieldByName('ticket_number').AsString;
    lTicketID :=   Ticket.FieldByName('ticket_id').AsInteger;

    if lStatus = EmergencyAckStatus then
      exit

    else if IsStatusPlusException(lStatus, lClientCode, lCallCenter) then  //QM-635 EB Status Plus
      exit;

    if (not ValidStatusForEmployee(lStatus))  then
      raise EOdDataEntryError.CreateFmt('Q Manager can not save ticket %s because the user is not authorized to use the status %s', [lTicketNum, lStatus]);


  end;

begin
  // DBISAM does not support updatable joins, so we must manually apply
  // changes to the table.  This ends up being a benefit, of sorts,
  // since we can carefully control all changes that are moved over,
  // after several verification steps.

  Assert(Ticket.Active, 'Ticket must be Active in SaveLocateChanges');
  QuestionsDisplayedStatuses := TStringList.Create;
  AnyOngoing := False;
  AnyOpen := False;
  TicketInfoCollected := False;

  Locate.Open;
  Engine.LinkEventsAutoInc(Locate);
  LocateData.First;
  MyClients.Clear;    
  while not LocateData.EOF do begin

    LocateID := LocateData.FieldByName('locate_id').AsInteger;
    ClientCode := LocateData.FieldByName('client_code').Value;
    TicketID := LocateData.FieldByName('ticket_id').Value;
    //Set these equal so that we don't have a false change
    OldHP := False;
    NewHP := OldHP;

    Locate.CancelRange;
    //Look to see if there's an existing locate on this ticket that has this client;server also detects duplicates by ClientCode
    Found := Locate.Locate('ticket_id;client_code', VarArrayOf([TicketID, ClientCode]), []);
    Locate.SetRange([LocateID], [LocateID]);

    try
      if Locate.Eof then begin   // new locate to add
        if not Found then begin //ignore any duplicate records that were added to the temp dataset
          ValidateStatus;
          Locate.Append;
          // Set up the fields that a newly added locate will need
          Locate.FieldByName('locate_id').AsInteger := LocateID;
          Locate.FieldByName('locator_id').AsInteger := LocateData.FieldByName('locator_id').AsInteger;
          Locate.FieldByName('ticket_id').Value := LocateData.FieldByName('ticket_id').Value;
          Locate.FieldByName('client_code').Value := ClientCode;
          Locate.FieldByName('client_id').Value := LocateData.FieldByName('client_id').Value;
 //         Locate.FieldByName('bigfoot').Value := False;  {QM-1037 EB Anytime we set something from QManager client, this is false}
          Locate.FieldByName('active').AsBoolean := True;

          GPSID := SaveMyGPSPosition(locate.FieldByName('ticket_id').Value); //QM-860 EB Locate HP error
          If GPSID > 0 then
            Locate.FieldByName('gps_id').AsInteger := GPSID; //QM-799 EB the one we saved earler

          //QMANTWO-616 EB - These are the fields we need updated for new locates with different locators
          Locate.FieldByName('assigned_to').AsInteger := LocateData.FieldByName('assigned_to').AsInteger;
          Locate.FieldBYName('assigned_to_id').AsInteger := LocateData.FieldByName('assigned_to_id').AsInteger;
          Locate.FieldBYName('DeltaStatus').AsString := LocateData.FieldByName('DeltaStatus').AsString;

          AskAdditionalQuestions;
          SetLocateFieldsInTable;
        end;
      end else begin
       //Has the High Priority checkbox changed?
        OldHP := Locate.FieldByName('high_profile').AsBoolean;
        NewHP := LocateData.FieldByName('high_profile').AsBoolean;
        if LocateNeedsToPost then begin  // Edit the existing record
          if (Locate.FieldByName('status').AsString <> LocateData.FieldByName('status').AsString) then
            ValidateStatus;
          AskAdditionalQuestions;
          //call edit on the dataset AFTER any places where exceptions could occur
          //or dataset could be left in an edit state that causes DBISAM errors

          EditDataSet(Locate);
          SetLocateFieldsInTable;
        end;
      end;
    finally
      //If anything goes wrong, make sure we don't leave Locate dataset in an edited state.
      CancelDataSet(Locate);
    end;

    HPM := LocateData.FieldByName('high_profile_multi').AsString;
    HPMO := LocateData.FieldByName('high_profile_multi_old').AsString;

    if (HPM <> HPMO) or (OldHP <> NewHP) then begin {If there was a change in the High Profile Status}
      if not (NewHP) then {High Profile was cleared}
        SaveNote(XHighProfileNotePrefix + HPM + ' ' +
                LocateData.FieldByName('client_code').AsString + ' ' +
                ColDelim + HighProfileOffMsg, 'hpr', HighProfileLocateSubType, qmftLocate, LocateID)
      else if (OldHP <> NewHP) then  {High Profile was added}    //QMANTWO-720 EB This looks like old code that parts were removed, but is preventing HP note.
        SaveNote(HighProfileNotePrefix + HPM + ' ' +
                 LocateData.FieldByName('client_code').AsString + ' ' +
                 ColDelim + HighProfileOnMsg, 'hpr', HighProfileLocateSubType, qmftLocate, LocateID)
    end;

    LocateStatus := LocateData.FieldByName('status').AsString;

    if (StringInArray(LocateStatus, OngoingTicketStatus)) then //EB - lets consolidate (LocateStatus = 'O') or (LocateStatus = 'OX') then
      AnyOngoing := True;

    if not LocateData.FieldByName('closed').AsBoolean then
      AnyOpen := True;

    LocateData.Next;
  end;
  LocateData.First;
  Locate.Close;

  SetTicketKind(AnyOngoing, AnyOpen);
  FreeAndNil(QuestionsDisplayedStatuses);
end;

procedure TDM.SaveLocateHoursChanges;

  procedure AssignFieldValue(const FieldName: string);
  var
    Source: TField;
    Dest: TField;
  begin
    Source := LocateHoursData.FieldByName(FieldName);
    Dest := SaveLocateHours.FieldByName(FieldName);
    if Dest.Value <> Source.Value then begin
      EditDataSet(Dest.DataSet);
      Dest.Value := Source.Value;
    end;
  end;

  procedure PostHoursChanges;
  begin
    if EditingDataSet(SaveLocateHours) then begin
      VerifyShouldSaveForInvoicedLocates(SaveLocateHours);
      FTicketChanged := True;
      PostDataSet(SaveLocateHours);
    end;
  end;

  procedure SetLocalKeys(const DeltaStatus: string);
  begin
    if EditingDataSet(SaveLocateHours) then
      AssignLocalKeys(SaveLocateHours, DeltaStatus);
  end;

var
  i: Integer;
  DeltaStatus: string;
  CopyField: string;
  Found: Boolean;
  HoursID: Integer;
begin
  Assert(LocateHoursData.Active, 'LocateHoursData must be Active in SaveLocateChanges');
  LocateHoursData.First;
  SaveLocateHours.Open;
  Engine.LinkEventsAutoInc(SaveLocateHours);
  while not LocateHoursData.Eof do begin
    DeltaStatus := LocateHoursData.FieldByName('DeltaStatus').AsString;
    Assert((DeltaStatus = '') or (DeltaStatus = 'I') or (DeltaStatus = 'U')
       , 'DeltaStatus must be [IU ] in SaveLocateChanges');
    HoursID := LocateHoursData.FieldByName('locate_hours_id').AsInteger;
    Found := SaveLocateHours.Locate('locate_hours_id', HoursID, []);

    if DeltaStatus = 'I' then begin
      if not Found then
        SaveLocateHours.Insert;
      for i := 0 to SaveLocateHours.FieldCount - 1 do begin
        CopyField := SaveLocateHours.Fields[i].FieldName;
        if (CopyField <> 'DeltaStatus') then
          AssignFieldValue(CopyField);
      end;
      SetLocalKeys(DeltaStatus);
      PostHoursChanges;
      Assert(SaveLocateHours.FieldByName('DeltaStatus').AsString = 'I',
          'DeltaStatus must be I in SaveLocateChanges');
    end
    else if DeltaStatus = 'U' then begin
      if not Found then
        raise Exception.Create('Could not find an edited locate hours record in the local DB');
      // Only these 4 fields are supposed to be editable, so don't try to save the rest
      AssignFieldValue('regular_hours');
      AssignFieldValue('overtime_hours');
      AssignFieldValue('status');
      AssignFieldValue('units_marked');
      SetLocalKeys(DeltaStatus);
      PostHoursChanges;
    end;
    LocateHoursData.Next;
  end;
end;

procedure TDM.SaveNote(const Note, Source: string; NoteSubType: Integer; ForeignType, ForeignId: Integer; Active: Boolean);
begin
  Assert(Note <> '', 'Note must not be empty in SaveNote');
  Assert(ForeignType > -1, 'ForeignType must not be less than 0');
  Assert(ForeignID <> 0, 'ForeignID must not be 0');

  if ForeignType = qmftTicket then begin
    if not Ticket.Active then
      raise Exception.Create('No ticket in view');
    if ForeignId = -1 then
      ForeignId := Ticket.FieldByName('ticket_id').AsInteger;
  end;
 //BGEALERT
//  showmessage('ForeignId: '+IntToStr(ForeignId));  //qm-747
  Engine.LinkEventsAutoInc(Notes);
  Notes.Open;
  try
//    if NoteSubType = DeletedNoteSubType then  {QM-188 EB We are going to change the note info}
//      Notes.Edit
//    else
      Notes.Append;
    Notes.FieldByName('foreign_type').AsInteger := ForeignType;
    Notes.FieldByName('foreign_id').AsInteger := ForeignId;
    Notes.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
    Notes.FieldByName('modified_date').AsDateTime := RoundSQLServerTimeMS(Now);
    Notes.FieldByName('uid').AsInteger := UQState.UserID;
    Notes.FieldByName('active').AsBoolean := Active;
    Notes.FieldByName('note').AsString := ReplaceControlChars(Note);
    Notes.FieldByName('sub_type').AsInteger := NoteSubType;
    Notes.FieldByName('DeltaStatus').AsString := 'I';
    AssignLocalKeys(Notes, Source);

    Notes.Post;
    Sleep(10); // Prevent any two notes with the same timestamp
  finally
    Notes.Close;
  end;

  if ForeignType = qmftTicket then
    FTicketChanged := True
//  else if ForeignType = qmftWorkOrder then   //QMANTWO-815 EB  - Replaced with FOutstandingWOAttachments
//    FWorkOrderChanged := True;
end;

procedure TDM.ChangeNote(const NoteID: integer; Note, Source: string; NoteSubType,
  ForeignType, ForeignId: Integer; Active: Boolean);
const
  SQL = 'update notes set note=%s';
 // Engine.ExecQuery('update employee set under_me_reports=false, under_me_timesheets=false where under_me_reports or under_me_timesheets');
var
  NoteFound: boolean;
begin
  Assert(Note <> '', 'Note must not be empty in SaveNote');
  Assert(ForeignType > -1, 'ForeignType must not be less than 0');
  Assert(ForeignID <> 0, 'ForeignID must not be 0');

  if ForeignType = qmftTicket then begin
    if not Ticket.Active then
      raise Exception.Create('No ticket in view');
    if ForeignId = -1 then
      ForeignId := Ticket.FieldByName('ticket_id').AsInteger;
  end;

  Engine.LinkEventsAutoInc(Notes); {EB - Leave this}
  Notes.Open;
  NoteFound := Notes.FindKey([NoteID]);    //QM-188 EB Check this
  try
    if NoteFound then begin
      Notes.Edit;
      Notes.FieldByName('modified_date').AsDateTime := RoundSQLServerTimeMS(Now);
      Notes.FieldByName('note').AsString := ReplaceControlChars(Note);
      Notes.FieldByName('sub_type').AsInteger := NoteSubType;
      Notes.Post;
    end;
  finally
    Notes.Close;
  end;

  if ForeignType = qmftTicket then
    FTicketChanged := True
//  else if ForeignType = qmftWorkOrder then   //QMANTWO-815 EB  - Replaced with FOutstandingWOAttachments
//    FWorkOrderChanged := True;
end;







procedure TDM.SetTicketKind(AnyOngoing, AnyOpen: Boolean);
var
  Kind: string;
begin
  // This also sets ticket.kind *locally*; the server takes care of itself with
  // a trigger on locate.

  if AnyOngoing then
    Kind := TicketKindOngoing
  else if AnyOpen then begin
    Kind := Ticket.FieldByName('kind').AsString;
    if (Kind = TicketKindDone) or (Kind=TicketKindOngoing) then
      Kind := TicketKindNormal;

  end else
    Kind := TicketKindDone;

  if Ticket.FieldByName('kind').AsString <> Kind then begin
    // Don't set FTicketChanged because it's just a local field, not
    // something we propogate to the DB; don't mark the ticket record
    // changed in the Update either.
    Ticket.Tag := DisableDeltaStatusTag;
    try
      Ticket.Edit;
      Ticket.FieldByName('kind').AsString := Kind;
      Ticket.Post;
    finally
      Ticket.Tag := 0;
    end;
  end;
end;

procedure TDM.GetStatusStrings(Strings: TStrings; ExcludeMinusR: Boolean);
var
  i: Integer;
  Index: Integer;
begin
  Assert(Assigned(Strings));
  Strings.Clear;
  if not Assigned(FStatusStrings) then begin
    FStatusStrings := TStringList.Create;
    StatusList.Open;
    StatusList.First;
    while not StatusList.EOF do begin
      if StatusList.FieldByName('active').AsBoolean then
        FStatusStrings.Add(StatusList.FieldByName('Status').AsString);
      StatusList.Next;
    end;
  end;
  if FStatusStrings.Count < 3 then
    ShowMessage('Error - unable to load status list');
  Strings.Assign(FStatusStrings);

  for i := Low(InternalStatuses) to High(InternalStatuses) do begin
    Index := Strings.IndexOf(InternalStatuses[i]);
    if Index > -1 then
      Strings.Delete(Index);
  end;

  if ExcludeMinusR then begin
    Index := Strings.IndexOf('-R');
    if Index <> -1 then
      Strings.Delete(Index);
  end;
end;

function TDM.GetSubTypeDescription(Code: Integer): string;

begin
  if RefDescForNotesSubType.active then RefDescForNotesSubType.close;
  RefDescForNotesSubType.ParamByName('Code').AsString := IntToStr(Code);
  RefDescForNotesSubType.Open;
  if not RefDescForNotesSubType.EOF then
    Result := IntToStr(Code) + '-' + RefDescForNotesSubType.FieldByName('Description').AsString;

end;



function TDM.EmpID: Integer;
begin
  Result := UQState.EmpID;
end;

const
  LocatorFilterLineNo = 10;

procedure TDM.OpenLocateData;
begin  //qm-403  sr
  LocateData.ParamByName('ticket_id').AsInteger := Ticket.FieldByName('ticket_id').AsInteger;
  if PermissionsDM.IsTicketManager then begin
    if NotEmpty(LocateData.SQL[LocatorFilterLineNo]) then
      Assert(Trim(LocateData.SQL[LocatorFilterLineNo]) = 'and locate.locator_id=:my_id', 'Check SQL text in OpenLocateData');
//      or if permission  remove HCL unless permission allows.  reqs change to LocateData query qm-403  sr
//      Assert(Trim(LocateData.SQL[LocatorFilterLineNo]) = 'and locate.locator_id=:my_id and locate.closed_how <> ''HCL'' ', 'Check SQL text in OpenLocateData');  //qm-403  sr
//     Delete "and locate.locator_id=:my_id" from the query to see all locates
    LocateData.SQL[LocatorFilterLineNo] := '';
  end else
    LocateData.ParamByName('my_id').AsInteger := EmpID;


  AddCalculatedField('locate_plats', LocateData, TStringField, 40);
  AddCalculatedField('alert2', LocateData, TStringField, 10);
  AddLookupField('client_name', LocateData, 'client_id', ClientLookup, 'client_id', 'client_name');
  AddLookupField('require_locate_units', LocateData, 'client_id', ClientLookup, 'client_id', 'require_locate_units');

  fPlusRestrictedList.TicketID := Ticket.FieldByName('ticket_id').AsInteger;  //QM-585 EB PLUS WhiteList
  fPlusRestrictedList.HasRestriction := False;
  fPlusRestrictedList.CC.Clear;
  LocateData.Open;
  LocateData.First;  //qm-265
  while not LocateData.EOF do   //qm-265
  begin
  //  if (LocateData.FieldByName('client_id').asInteger > 0) then begin
       MyClients.Add(LocateData.FieldByName('client_id').asString);    //qm-265
     if not PlusWhiteListed(LocateData.FieldByName('client_code').asString) then begin  //QM-585 EB PLUS WhiteList
       fPlusRestrictedList.CC.Add(LocateData.FieldByName('client_code').AsString);
       fPlusRestrictedList.HasRestriction := True;
     end;
 //   end
 //   else


     LocateData.Next;
  end;
  fPlusRestrictedList.Loaded := True;
  LocateData.First;  //qm-265

  Assert(not LocateData.ResultIsLive);
end;

procedure TDM.OpenTicketNotesData;
var
  Note: string;
  ClientCode: string;
  ClientCodePos, DelimPos: Integer;
  Reasons: string;
  HPRAdded: boolean;
  HPRRemoved: boolean;
begin
  TicketNotesData.ParamByName('ticket_id').AsInteger := Ticket.FieldByName('ticket_id').AsInteger;
  TicketNotesData.ParamByName('foreign_type_ticket').AsInteger := qmftTicket;
  TicketNotesData.ParamByName('foreign_type_locate').AsInteger := qmftLocate;
  TicketNotesData.Open;

  while not TicketNotesData.EOF do begin

  {IMPORTANT: We have to parse the note to pull information about the High Profile Reason - bleh.
              Any changes to the High Priority Note must keep similar format of previous items
              otherwise, it will not be able to load the reason. Everything before the colon 
              needs to stay in that format to pull out the reason.  Anything after
              the colon can be modified without causing problems to the parsing}
    Note := TicketNotesData.FieldByName('note').AsString;
    
    HPRAdded := (Pos(HighProfileNotePrefix, Note) = 1);
    HPRRemoved := (Pos(XHighProfileNotePrefix, Note) = 1);
    If HPRAdded then begin
      {Previous format before 10/1/2015:
           "HPR-45 PC0098"   HPR-[Reason Code] [Client Code]
       New format:
           "HPR-45 PC0098: HighProfileOnMsg" }

       {We just want to extract the Reason codes, so strip the description at the end}
       DelimPos := Pos(ColDelim, Note);
       if DelimPos > 0 then
         Note := Copy(Note, 0, DelimPos-1);

       Note := trim(Note);
       ClientCodePos := StrLastPos(' ', Note);

       if ClientCodePos = -1 then    {Can't find either format}
         ShowMessage('Invalid format for high profile (HPR-) note: ' + Note)
       else begin
        ClientCode := Copy(Note, ClientCodePos + 1, Length(Note));
        if LocateData.Locate('client_code', ClientCode, []) then begin
          if HPRAdded then
            Reasons := Copy(Note, Length(HighProfileNotePrefix) + 1, ClientCodePos - 1 - Length(HighProfileNotePrefix));

          LocateData.Edit;
          LocateData.FieldByName('high_profile_multi').AsString := Reasons;
          LocateData.FieldByName('high_profile_multi_old').AsString := Reasons;
          LocateData.Post;
        end;
      end;
    end
    {clear it out because it has been removed...}
    else if HPRRemoved then begin
      LocateData.Edit;
      LocateData.FieldByName('high_profile_multi').AsString := '';
      LocateData.FieldByName('high_profile_multi_old').AsString := '';
      locateData.Post;
    end;
    TicketNotesData.Next;
  end;
end;

procedure TDM.OpenWorkOrderNotesData;
begin
  WorkOrderNotesData.ParamByName('wo_id').AsInteger := WorkOrder.FieldByName('wo_id').AsInteger;
  WorkOrderNotesData.ParamByName('foreign_type_work_order').AsInteger := qmftWorkOrder;
  WorkOrderNotesData.Open;
end;

procedure TDM.UpdateTicketInCache(TicketID: Integer; ModifiedDate: TDateTime);
begin
  if Engine.HavePendingTicketChanges then begin
    SendChangesToServer;
  end;
  Engine.RemoveTicketFromCache(TicketID);
  Engine.GetTicketDataFromServer(TicketID);    {On Demand Ticket}
  Engine.GetEPRHistory(TicketID);  //QMANTWO-338 EB
  Engine.GetRiskHistory(TicketID);  //QM-387 pt 3 EB
end;



procedure TDM.UpdateWorkOrderInCache(const WorkOrderID: Integer);
begin
  if Engine.HavePendingWorkOrderChanges then begin
    SendChangesToServer;
  end;
  Engine.RemoveWorkOrderFromCache(WorkOrderID);   //QMANTWO-391  EB (OHM added to the Remove)
  Engine.GetWorkOrderDataFromServer(WorkOrderID);
  Engine.GetWorkOrderOHMDetailsFromServer(WorkOrderID);     //QMANTWO-391  EB
  
end;


procedure TDM.SetUpDatabase(DataDir: string);
begin
  FDataDir := GetClientDataLocalDir(DataDir);
  if not DirectoryExists(FDataDir) then
    if not ForceDirectories(FDataDir) then
      raise Exception.Create('Unable to create the data directory.  Make sure you have access to create this directory: ' + FDataDir);

  Database1.Close;
  Database1.Directory := FDataDir;
  Database1.Open;

end;

function TDM.MatchesPreviousNamePW: Boolean;
begin
  Result := (NamePwHash = UQState.PreviousLoginHash) and (NotEmpty(UQState.APIKey));
end;

function TDM.NamePwHash: string;
var
  Data: string;
begin
  Data := 'junk' + UQState.UserName + '/' + UQState.Password +
    '/' + UQState.ServerURL + '/' + UQState.UserType +
    '/' + IntToStr(UQState.EmpID) + '/' + IntToStr(UQState.UserID);
  Result := HashToString16(Data);
  // ShowMessage(Data + ' ' + Result); Useful for debugging
end;

procedure TDM.SaveNamePwHash;
begin
  UQState.PreviousLoginHash := NamePwHash;
end;

procedure TDM.ClearNamePwHash;
begin
  UQState.PreviousLoginHash := '';
end;



procedure CheckApplicationVersion;
var
  VersionString: string;
  Error: Boolean;
begin
  if not SameText(ExtractFileName(Application.ExeName), 'QManager.exe') then
    Exit; // Dont bother us during testing
  Error := not GetFileVersionNumberString(Application.ExeName, VersionString);
  if not Error then
    Error := VersionString <> AppVersion;
  if Error then
    MessageDlg('The application EXE/DOF versions are mismatched: '+VersionString+'/'+AppVersion, mtError, [mbOK], 0);
end;

procedure TDM.LocateDataBeforeEdit(DataSet: TDataSet);
begin
  if not PermissionsDM.CanI(RightTicketsEditAnotherLocatorsLocates) then
    if DataSet.FieldByName('locator_id').AsInteger <> EmpID then
      raise Exception.Create('You can not modify another locator''s locate');
end;

procedure TDM.LocateDataBeforePost(DataSet: TDataSet);
begin
  if DataSet.FieldByName('Status').AsString = EmergencyAckStatus then
    Exit
  else if DataSet.FieldByName('Status').AsString='' then
    raise Exception.Create('Status cannot be blank');

  OpenDataSet(StatusList);
  if not StatusList.Locate('Status', DataSet.FieldByName('status').AsString, []) then
    raise EOdDataEntryError.Create('Invalid status code: ' + DataSet.FieldByName('status').AsString);
  if LocateData.FieldByName('require_locate_units').AsBoolean and
    StatusList.FieldByName('allow_locate_units').AsBoolean and
    (LocateData.FieldByName('length_total').AsInteger <= 0) then begin
    raise EOdDataEntryError.CreateFmt('Client %s requires a length greater than 0.',
      [LocateData.FieldByName('client_code').AsString]);
  end;
  if DataSet.FieldByName('length_marked').AsInteger > 0 then begin
    SaveLocateLength(DataSet.FieldByName('locate_id').AsInteger,
      DataSet.FieldByName('length_marked').AsInteger,
      DataSet.FieldByName('Status').AsString, Date);
    LocateData.FieldByName('length_marked').Clear;
  end;
end;




function TDM.GetRefList(const RefNames: string; Strings: TStrings; RaiseException: Boolean; const Modifier: string): Integer;
var
  Code: string;
  Refs: TStringList;
  i: Integer;
begin
  Assert(Assigned(Strings));
  Assert(RefNames <> '');
  Result := 0;
  Strings.Clear;
  Refs := TStringList.Create;
  try
    StrToStrings(RefNames, ';', Refs);
    for i := 0 to Refs.Count - 1 do begin
      Reference.Open;
      // The records are sorted by type;sortby
      if not Reference.Locate('type', Refs[i], [loCaseInsensitive]) then begin
        if RaiseException then
          raise EOdReferenceMissing.Create('Missing reference type requested: ' + Refs[i] +SLineBreak+ 'Please sync and then try again.')
        else
          Exit;
      end;
      while SameText(Refs[i], Reference.FieldByName('type').AsString) and not Reference.Eof do begin
        if Reference.FieldByName('active_ind').AsBoolean then begin
          Code := Trim(Reference.FieldByName('code').AsString);
          if Code = '' then
            Code := Reference.FieldByName('description').AsString;
          if (Trim(Modifier) = '') or SameText(Modifier, Reference.FieldByName('modifier').AsString) then
            Strings.Add(Code+'='+ Reference.FieldByName('description').AsString);
        end;
        Reference.Next;
      end;
    end;
    Result := Strings.Count;
  finally
    FreeAndNil(Refs);
  end;
end;

function TDM.GetRefCodeList(const RefNames: string; Strings: TStrings;
  const Modifier: string): Integer;
var
  i: Integer;
begin
  Result := GetRefList(RefNames, Strings, False, Modifier);
  for i := 0 to Strings.Count - 1 do
    Strings[i] := Strings.Names[i];
end;

function TDM.GetRefDisplayList(const RefNames: string; Strings: TStrings;
  const Modifier: string; IncludeBlank: Boolean): Integer;
var
  i: Integer;
begin
  Result := GetRefList(RefNames, Strings, True, Modifier);
  for i := 0 to Strings.Count - 1 do
    Strings[i] := Strings.Values[Strings.Names[i]];
  if IncludeBlank then
    Strings.Insert(0, '')
end;

function TDM.GetRefCodeForDisplay(const RefName, Display: string;
  const Modifier: string): string;
var
  List: TStringList;
  i: Integer;
begin
  Result := '';
  if (RefName = '') or (Display = '') then
    Exit;
  List := TStringList.Create;
  try
    GetRefList(RefName, List, False, Modifier);
    for i := 0 to List.Count - 1 do begin
      if List.Values[List.Names[i]] = Display then begin
        Result := List.Names[i];
        Break;
      end;
    end;
  finally
    FreeAndNil(List);
  end;
end;

function TDM.GetRefModifier(const RefName, Code: string): string;
begin
  Assert((RefName <> '') and (Code <> ''));
  if Reference.Locate('type;code', VarArrayOf([RefName, Code]), [loCaseInsensitive]) then
    Result := Reference.FieldByName('modifier').AsString
  else
    raise EOdReferenceMissing.Create('Missing reference type requested: ' + RefName +':'+ Code +SLineBreak+ 'Please sync and then try again.');
end;


procedure TDM.EmptyDateTimeGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  Assert(Sender is TDateTimeField);
  if Sender.AsDateTime = 0 then
    Text := ''
  else
    Text := Sender.AsString;
end;

procedure TDM.SetDateFieldGetTextEvents(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to DataSet.FieldCount - 1 do begin
    if DataSet.Fields[i] is TDateTimeField then
      DataSet.Fields[i].OnGetText := EmptyDateTimeGetText;
  end;
end;

procedure TDM.SetRefFieldGetSetTextEvents(Field: TField; const RefNames: string);
begin
  Assert(Assigned(Field));
  Assert(RefNames <> '');
  // A '.' is used to dynamically apply a modifier restriction to the ref list
  Assert(not StrContains('.', RefNames));
  // Note: We misuse Origin here to store the RefNames so the code is
  // more generic.  Origin should be unused otherwise, anyway.
  Field.Origin := RefNames;
  //Assert(not Assigned(Field.OnSetText));
  //Assert(not Assigned(Field.OnGetText));
  Field.OnGetText := RefFieldOnGetText;
  Field.OnSetText := RefFieldOnSetText;
end;



//function TDM.SetRouteOrder(pTicketID: Integer; pRouteOrder: Double): string;    //QM-166 EB Set Route Order
//var
//  AMsg: string;
//begin
//  CallingServiceName('UpdateRouteOrder');
//  if RouteOrderObj.SetRO(pTicketID, pRouteOrder, AMsg) then
//    Engine.Service.UpdateRouteOrder(Engine.SecurityInfo, pTicketID, pRouteOrder);
//  Result := AMsg;
//end;

procedure TDM.RefFieldOnGetText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  RefList: TStringList;
  Index: Integer;
  RefType: string;
  Modifier: string;
begin
  if Sender.AsString = '' then
    Text := ''
  else begin
    Assert(Sender.Origin <> '');
    DecodeRefListCriteria(Sender.Origin, RefType, Modifier);
    RefList := TStringList.Create;
    try
      GetRefList(RefType, RefList, False, Modifier);
      Index := RefList.IndexOfName(Sender.AsString);
      if Index = -1 then
        //raise EOdBadGetReference.CreateFmt('Invalid value "%s" for reference field "%s"', [Sender.AsString, Sender.FieldName])
        Text := Sender.AsString
      else
        Text := RefList.Values[Sender.AsString];
    finally
      FreeAndNil(RefList);
    end;
  end;
end;

procedure TDM.RefFieldOnSetText(Sender: TField; const Text: string);
var
  CodeList: TStringList;
  DispList: TStringList;
  Index: Integer;
  RefType: string;
  Modifier: string;
begin
  Assert(Sender.Origin <> '');
  DecodeRefListCriteria(Sender.Origin, RefType, Modifier);
  DispList := nil;
  CodeList := TStringList.Create;
  try
    DispList := TStringList.Create;
    GetRefCodeList(RefType, CodeList, Modifier);
    GetRefDisplayList(RefType, DispList, Modifier);
    Index := DispList.IndexOf(Text);
    if Index = -1 then begin
      Index := CodeList.IndexOf(Text);
      if Index = -1 then
        raise EOdBadSetReference.CreateFmt('"%s" is not a valid value for "%s"', [Sender.AsString, Sender.FieldName])
      else
        Sender.AsString := Text;
    end
    else
      Sender.AsString := CodeList[Index];

    //if we are still trying to save a null string after translation the desired
    //behavior is to clear the field instead
    if Sender.AsString = '' then
      Sender.Clear
  finally
    FreeAndNil(CodeList);
    FreeAndNil(DispList);
  end;

  //TODO: This is a temporary hack until I figure out a better place for this code
  if (Sender.DataSet = LDamageDM.Damage) and (Sender.FieldName = 'claim_status') then
  begin
    if (Sender.AsString <> '') then begin
      if not PermissionsDM.IsInLimitationList(RightDamagesSetClaimStatus, Sender.AsString) then
        raise EOdDataEntryError.CreateFmt('You are not allowed to use the %s claim status.  Press escape to cancel your changes.', [Text]);
    end;
    LDamageDM.Damage.FieldByName('claim_status_date').AsDateTime := RoundSQLServerTimeMS(Now);
  end;
  if (Sender.DataSet = LDamageDM.Damage) and (Sender.FieldName = 'invoice_code') then
  begin
    if not StringInArray(Sender.AsString, ['', 'EXPECT', 'NOPAY']) then begin
      if not PermissionsDM.IsInLimitationList(RightDamagesSetInvoiceCode, Sender.AsString) then
        raise EOdDataEntryError.CreateFmt('You are not allowed to use the %s invoice code.  Press escape to cancel your changes.', [Text]);
    end;
    LDamageDM.Damage.FieldByName('claim_status_date').AsDateTime := RoundSQLServerTimeMS(Now);
  end;
end;

procedure TDM.DecodeRefListCriteria(const RefListCriteria: string; var RefType, Modifier: string);
begin
  RefType := RefListCriteria;
  Modifier := '';
  if StrContains('.', RefListCriteria) then begin
    RefType := StrBefore('.', RefListCriteria);
    Modifier := StrAfter('.', RefListCriteria);
  end;
end;

procedure TDM.CallCenterDescriptionList(CodeLookupList: TCodeLookupList; IncludeBlank: Boolean);
var
  CloseData: Boolean;
begin
  Assert(Assigned(CodeLookupList));
  CodeLookupList.Clear;
  CloseData := not CallCenters.Active;
  CallCenters.Open;
  try
    if IncludeBlank then
      CodeLookupList.Add('','');
    CallCenters.First;
    while not CallCenters.Eof do begin
      CodeLookupList.Add(CallCenters.FieldByName('cc_code').AsString,
        CallCenters.FieldByName('cc_name').AsString);
      CallCenters.Next;
    end;
  finally
    if CloseData then
      CallCenters.Close;
  end;
end;


procedure TDM.NotesSubTypeList(CodeLookupList: TCodeLookupList; ForeignType: Integer);
var
  ForeignTypeStr: string;
  CodeStr, Desc: string;
  HasRecords: boolean;
begin
  HasRecords := False;
  if not Assigned(CodeLookupList) then exit; //QM-576  sr
  
  Assert(Assigned(CodeLookupList), 'Failed Assigned(CodeLookupList) '+'ForeignTypeStr: '+ForeignTypeStr);  //QM-576  sr
  CodeLookupList.Clear;
  ForeignTypeStr := IntToStr(ForeignType) + '%';

  if NotesSubTypeLookup.Active then NotesSubTypeLookup.Close;
  NotesSubTypeLookup.ParamByName('ForeignTypeStr').AsString := ForeignTypeStr;
  try
    NotesSubTypeLookup.Open;
    NotesSubTypeLookup.First;
    while not NotesSubTypeLookup.Eof do begin
      HasRecords := True;
      CodeStr := NotesSubTypeLookup.FieldByName('code').AsString;
      Desc := NotesSubTypeLookup.FieldByName('description').AsString;
      CodeLookupList.Add(CodeStr, Desc);
      NotesSubTypeLookup.Next;
    end;
    if not HasRecords then begin
      CodeStr := IntToStr((ForeignType) * 100);
      Desc := 'Default';
      CodeLookupList.Add(CodeStr, Desc);
    end;
  except on e: Exception do
    MessageDlg('Unable to load Note Subtypes from database: ' + e.Message, mtError, [mbOK], 0);
  end;

end;


procedure TDM.CallCenterListForTickets(CodeLookupList: TCodeLookupList; IncludeBlank: Boolean);
const
  Select = 'select cc_code, cc_name from call_center ' +
    'where active and allowed_work = ''ticket'' ' +
    'order by cc_code';
var
  Data: TDataSet;
begin
  Assert(Assigned(CodeLookupList));
  CodeLookupList.Clear;

  Data := Engine.OpenQuery(Select);
  try
    if IncludeBlank then
      CodeLookupList.Add('','');
    while not Data.Eof do begin
      CodeLookupList.Add(Data.FieldByName('cc_code').AsString,
        Data.FieldByName('cc_name').AsString);
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDM.CallCenterList(List: TStrings; IncludeBlank: Boolean);
begin
  GetListFromDataSet(CallCenters, List, 'cc_code', '', IncludeBlank);
end;


procedure TDM.LoadNoteSubTypeList(NoteType: Integer; SubTypeList: TStrings);
begin
  GetRefPlusCodeLookupList('notes_sub', SubTypeList);
end;



procedure TDM.OfficeList(List: TStrings);
var
  Name: string;
  i: Integer;
begin
  Assert(Assigned(List));
  List.Clear;
  try
    Offices.Open;
    while not Offices.Eof do begin
      Name := Offices.FieldByName('office_name').AsString;
      i := List.Add(Name);
      List.Objects[i] := Pointer(Offices.FieldByName('office_id').AsInteger);
      Offices.Next;
    end;
  finally
    Offices.Close;
  end;
end;


function TDM.HaveSyncedData: Boolean;
begin
  Reference.Close;
  Reference.Open;
  Reference.Refresh;
  Result := Reference.RecordCount > 0;
end;

//procedure TDM.DamageEstimateAfterOpen(DataSet: TDataSet);
//begin
//  DataSet.FieldByName('third_party').OnGetText := ThirdPartyBooleanOnGetText;
//end;

procedure TDM.ThirdPartyBooleanOnGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.IsNull then
    Text := ''
  else if Sender.AsBoolean then
    Text := '3rd Party'
  else
    Text := UQState.CompanyShortName;
end;

//EB Comment: Locate is open, OpenLocate, LocateIsOpen
function TDM.IsLocateEditable(LocateID: Integer): Boolean;
const
  SQL = 'select status, closed, DeltaStatus from locate ' +
        'where locate_id=%d and active';
var
  DataSet: TDataSet;
  Status: string;
  ClientCode: string;
  ClientID: Integer;
begin
  // A negative locate ID indicates a newly added locate record,
  // Newly added locates are always editable
  if LocateID < 0 then begin
    Result := True;
    Exit;
  end;

  GetClientDataForLocateID(LocateID, ClientCode, ClientID);

  // IsClientLocateEditable takes precedence over RightTicketsEditClosedLocates
  if not IsClientLocateEditable(ClientID) then
    Result := False
  else if PermissionsDM.CanI(RightTicketsEditClosedLocates) then
    Result := True
  else begin
    DataSet := Engine.OpenQuery(Format(SQL, [LocateID]));
    try
      Assert(not DataSet.IsEmpty, 'Locate not found in local database');
      Status := DataSet.FieldByName('status').AsString;
//      Result := (Status = '-R') or (Status = 'O') or (Status = 'OX') or       //EB Check 720
      Result := (Status = '-R') or (StringInArray(Status, OngoingTicketStatus)) or
        (not DataSet.FieldByName('closed').AsBoolean) or
        (not (DataSet.FieldByName('DeltaStatus').AsString = ''));
    finally
      FreeAndNil(DataSet);
    end;
  end;
end;

function TDM.IsLocateInvoiced(LocateID: Integer): Boolean;
const
  SQL = 'select invoiced from locate where locate_id=%d and active';
var
  DataSet: TDataSet;
begin
  Result := False;
  if LocateID < 0 then
    Exit;
  DataSet := Engine.OpenQuery(Format(SQL, [LocateID]));
  try
    if DataSet.IsEmpty then
      raise Exception.Create('Locate not found');
    Result := DataSet.FieldByName('invoiced').AsBoolean;
  finally
    FreeAndNil(DataSet);
  end;
end;


procedure TDM.UpdateDatabaseSchema;
var
  XMLDoc: IXMLDOMDocument;
  Schema: string;
begin
  // TODO: Only apply the schema if the EXE date has changed?
  XMLDoc := CoDOMDocument.Create;
  XMLDoc.async := False;
  GetEmbeddedSchema(Schema);

  if not XMLDoc.loadXML(Schema) then
    raise Exception.Create('The embedded schema definition is corrupt');

  Database1.CloseDataSets;
  ApplyXMLSchemaToDatabase(Database1, XMLDoc);
end;



procedure TDM.GetEmbeddedSchema(var Schema: string);
var
  ResourceStream: TResourceStream;
  StringStream: TStringStream;
  DebugSchema: TStringList;  //QM-1037 Bigfoot
  {This  was added to pull the EmbeddedSchema when in DeveloperMode (Debug). It will save a text
   file with the Schema being used with the executable (not what is in DBISAM).}
begin
  if FindResource(HInstance, SchemaResourceName, SchemaResourceType) = 0 then
    Exit;
  StringStream := nil;
  ResourceStream := TResourceStream.Create(HInstance, SchemaResourceName, SchemaResourceType);
  try
    ResourceStream.Position := 0;
    StringStream := TStringStream.Create('');
    StringStream.CopyFrom(ResourceStream, ResourceStream.Size);
    Schema := StringStream.DataString;
    if UQState.DeveloperMode then begin
      DebugSchema := TStringList.Create;
      DebugSchema.Add('Run Date: ' + DateTimeToStr(Now));
      DebugSchema.Add('----------------------------');
      DebugSchema.Add(Schema);
      DebugSchema.SaveToFile(ExtractFilePath(Application.ExeName) + '/DebugSchema.txt');
    end;

  finally
    FreeAndNil(ResourceStream);
    FreeAndNil(StringStream);
    if UQState.DeveloperMode then
      FreeAndNil(DebugSchema);
  end;

  if Trim(Schema) = '' then
    raise Exception.Create('Embedded schema definition is missing');
end;

procedure TDM.CloseTicketDataSets;
begin
  FTicketChanged := False;
  FLocFacilityDeleted := False;
  Ticket.Close;
  Locate.Close;
  LocateData.Close;

  TicketNotesData.Close;
  LocateHoursData.Close;
  LocatePlatData.Close;
  LocateFacilityData.Close;
  ClientLookup.Close;
  JobsiteArrivalData.Close;
  TicketAlertsQuery.Close;  //QM-771 Eb Ticket Alerts
end;

procedure TDM.CloseWorkOrderDataSets;
begin
  FOutstandingWOAttachments := False;  //QMANTWO-815 EB
  WorkOrder.Close;
  WorkOrderNotesData.Close;
  ClientLookup.Close;
  WorkOrderInspection.Close;
end;



procedure TDM.CheckForAndFixCorruptTables;
var
  i: Integer;
  TableList: TStringList;
  Table: TDBISAMTable;
begin
  Table := nil;
  TableList := TStringList.Create;
  try
    Table := TDBISAMTable.Create(nil);
    Database1.Handle.ListTableNames(TableList);
    for i := (TableList.Count - 1) downto 0 do begin
      Table.Close;
      Table.DatabaseName := Database1.DatabaseName;
      Table.TableName := TableList[i];
      try
        Table.FieldDefs.Update;
        Table.IndexDefs.Update;
        Table.Open;
      except
        try
          Table.RepairTable;
          if not Table.VerifyTable then
            raise Exception.Create('Corrupt table detected');
        except
          MessageDlg(AppName + ' detected the local database table "' +TableList[i]+
            '" is corrupt.  Please contact your system administrator', mtError, [mbOK], 0);
        end;
      end
    end;
  finally
    FreeAndNil(TableList);
    FreeAndNil(Table);
  end;
end;

procedure TDM.FlushDatabaseBuffers;
var
  i: Integer;
begin
  if not Assigned(Database1) then
    Exit;
  for i := 0 to Database1.DataSetCount - 1 do
    Database1.DataSets[i].FlushBuffers;
end;

procedure TDM.CopyTableData(Src, Dest: TDBISAMTable);
var
  i: Integer;
  SrcField: TField;
  DestField: TField;
begin
  Assert(Assigned(Src) and Assigned(Dest));

  StartTransaction;
  try
    Src.Open;
    Src.First;
    Dest.Open;
    while not Src.Eof do begin
      Dest.Insert;
      for i := 0 to Src.Fields.Count - 1 do begin
        SrcField := Src.Fields[i];
        DestField := Dest.Fields.FindField(SrcField.FieldName);
        if Assigned(DestField) then
          DestField.AsVariant := SrcField.AsVariant;
      end;
      Dest.Post;
      Src.Next;
    end;
    Assert(Src.RecordCount = Dest.RecordCount);
    Commit;
  except
    Rollback;
    raise;
  end;
end;

// TODO: This procedure is a mess of nested stuff
procedure TDM.UpdateSyncStatusTable;
var
  Status: TDataSet;

  procedure DeleteOldSyncRecords;

    function TableInArray(const TableName: string): Boolean;
    var
      i: Integer;
    begin
      Result := False;
      for i := Low(SyncStatusRows) to High(SyncStatusRows) do begin
        if SyncStatusRows[i].TableName = TableName then begin
          Result := True;
          Break;
        end;
      end;
    end;

  begin
    Status.First;
    while not Status.Eof do
      if TableInArray(Status.FieldByName('TableName').AsString) then
        Status.Next
      else
        Status.Delete;
  end;

  procedure VerifyCurrentRecords;

    procedure EditIfNecessary(Field: TField; Value: Variant);
    begin
      if Field.AsVariant <> Value then begin
        if Field.IsNull and (Field.DataType = ftString) and (Value = '') then
          Exit;
        Status.Edit;
        Field.AsVariant := Value;
      end;
    end;

  var
    i: Integer;
  begin
    for i := Low(SyncStatusRows) to High(SyncStatusRows) do begin
      if not Status.Locate('TableName', SyncStatusRows[i].TableName, [loCaseInsensitive]) then
        Status.Insert;
      EditIfNecessary(Status.FieldByName('TableName'), SyncStatusRows[i].TableName);
      EditIfNecessary(Status.FieldByName('PrimaryKey'), SyncStatusRows[i].PrimaryKey);
      if Status.State in dsEditModes then
        Status.Post;
    end;
  end;

begin
  Status := Engine.GetSyncStatusTable;
  DeleteOldSyncRecords;
  VerifyCurrentRecords;
  Assert(Status.RecordCount = Length(SyncStatusRows));
  FlushDatabaseBuffers;
end;

function TDM.LocalTableExists(const TableName: string): Boolean;
begin
  Result := False;
  if Assigned(Database1) and Assigned(Database1.Handle) then
    Result := Database1.Handle.DataTableExists(TableName, False, False);
end;

procedure TDM.SetTicketAcknowledged(TicketID: Integer);
var
  Tick: TicketKey;
begin
  Tick := TicketKey.Create;
  try
    Tick.TicketID := TicketID;

    CallingServiceName('AckTicket');
    Engine.Service.AckTicket(Engine.SecurityInfo, Tick, UQState.EmpID);
  finally
    FreeAndNil(Tick);
  end;
end;

function TDM.MoveTheseLocates(pLocates, pNewLocators: TIntegerList): boolean;
var
  i: integer;
begin
  try
  if Assigned(pLocates) and Assigned(pNewLocators) then begin
    for i := 0 to (pLocates.Count - 1) do
      Engine.Service.AssignProjLocate(Engine.SecurityInfo, pLocates.Items[i], pNewLocators.Items[i]);
  Result := True;
  end
  else
    Result := False;
  except
    Result := False;
  end;
end;

function TDM.MoveTheseTickets(pTickets: TStringList; pFromLocatorID, pToLocatorID:Integer): boolean;  //QMANTWO-573  EB
var
  MyTicketList: TicketList;
  CurTicketKey: TicketKey;
  i: integer;
begin
  Result := False;
  try
    MyTicketList := TicketList.Create;
    for i := 0 to pTickets.Count - 1 do begin
      CurTicketKey := MyTicketList.Add;
      CurTicketKey.TicketID := pTickets.GetInteger(i);
    end;
    CallingServiceName('MoveTickets');
    Engine.Service.MoveTickets(Engine.SecurityInfo, MyTicketList, pFromLocatorID, pToLocatorID);
    Result := True;
  finally
    FreeAndNil(MyTicketList);
  end;
end;

procedure TDM.Commit;
begin
  Assert(Database1.InTransaction);
  Database1.Commit(True);
end;

procedure TDM.Rollback;
begin
  Assert(Database1.InTransaction);
  Database1.Rollback;
end;

procedure TDM.StartTransaction;
begin
  Assert(not Database1.InTransaction);
  Database1.StartTransaction;
end;

procedure TDM.CleanUQFilesFromTempDir;
var
  Path: string;
  Wait: IInterface;
begin
  Wait := ShowHourglass;
  try
    Path := GetWindowsTempPath;
    OdDeleteFile(Path, 'QMR*.pdf', 1, False, False);
    Path := Path + 'QManager\';
    OdDeleteFile(Path, '*.*', 1, True, False);
  except on E: Exception do
    MessageDlg('Unable to clean out temp directory: ' + E.Message, mtError, [mbOK], 0);
  end;
end;

procedure TDM.CheckForInstalledComponents;
const
  NoXMLParser = 'This computer does not appear to have the required Microsoft ' +
    'XML library installed.  Please contact ' + AppName + ' technical support.';
begin
  try
    CoDOMDocument.Create;
  except
    MessageDlg(NoXMLParser, mtError, [mbOK], 0);
  end;
end;

procedure TDM.ProfitCenterList(List: TStrings);
const
  SQL = 'select office_id, profit_center from office '+
        'where profit_center <> '''' and profit_center is not NULL ' +
        'order by profit_center ascending';
var
  Offices: TDataSet;
  ProfitCenter: string;
  OfficeID: Integer;
begin
  Assert(Assigned(List));
  List.Clear;
  Offices := Engine.OpenQuery(SQL);
  try
    Offices.First;
    while not Offices.Eof do begin
      ProfitCenter := trim(Offices.FieldByName('profit_center').AsString);
      if Trim(ProfitCenter) <> '' then begin
        OfficeID := Offices.FieldByName('office_id').AsInteger;
        if List.IndexOf(ProfitCenter) = -1 then
          List.AddObject(ProfitCenter, TObject(OfficeID));
      end;
      Offices.Next;
    end;
  finally
    FreeAndNil(Offices);
  end;
end;

procedure TDM.SolomonProfitCenterList(List: TStrings);
const
  SQL = 'select distinct timesheet_num from profit_center where timesheet_num is not null and timesheet_num not in ('''', '' '')';
var
  Centers: TDataSet;
begin
  Assert(Assigned(List));
  List.Clear;
  Centers := Engine.OpenQuery(SQL);
  try
    while not Centers.Eof do begin
      List.Add(Centers.FieldByName('timesheet_num').AsString);
      Centers.Next;
    end;
  finally
    FreeAndNil(Centers);
  end;
end;

procedure TDM.UploadLocationList(List: TStrings);
const
  SQL = 'select * from upload_location where group_name in (''%s'', ''*'') order by description';
var
  DataSet: TDataSet;
  Description: string;
  LocationID: Integer;
  UploadGroup: string;
begin
  Assert(Assigned(List));
  List.Clear;
  UploadGroup := PermissionsDM.GetLimitation(RightUploadGroup);
  if UploadGroup = '' then
    Exit;
  DataSet := Engine.OpenQuery(Format(SQL, [UploadGroup]));
  try
    DataSet.First;
    while not DataSet.Eof do begin
      Description := DataSet.FieldByName('description').AsString;
      if Trim(Description) <> '' then begin
        LocationID := DataSet.FieldByName('location_id').AsInteger;
        List.AddObject(Description, TObject(LocationID));
      end;
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function QuoteCommaSetStr(S: string): string;
var
  SL: TStringList;
  i: Integer;
begin
  // ABC -> 'ABC"
  // ABC,DEF -> 'ABC','DEF'
  SL := TStringList.Create;
  try
    SL.CommaText := S;
    for i := 0 to SL.Count-1 do
      SL[i] := QuotedStr(SL[i]);
    Result := SL.CommaText;
  finally
    FreeAndNil(SL);
  end;
end;



function TDM.ClientHasSubClients(ParentClientID: integer): boolean;
begin
  ClientHasChildClientsQry.Close;
  ClientHasChildClientsQry.ParamByName('parent_client_id').AsInteger := ParentClientID;
  try
    ClientHasChildClientsQry.Open;
    if ClientHasChildClientsQry.FieldByName('NumOfRecords').AsInteger > 0 then
      Result := True
    else
      Result := False;
  except
    Result := False;
  end;
end;

procedure TDM.ClientList(const CallCenters: string; List: TStrings);
const
  SQL = 'select call_center, client_id, oc_code from client '+
        'where active and call_center in (%s) '+
        'order by call_center, oc_code';
var
  Clients: TDataSet;
  ClientCode: string;
  ClientID: Integer;
  MultiList: Boolean;
  CallCenter: string;
begin
  Assert(Assigned(List));
  MultiList := Pos(',', CallCenters) > 0;
  List.BeginUpdate;
  try
    List.Clear;
    Clients := Engine.OpenQuery(Format(SQL, [QuoteCommaSetStr(CallCenters)]));
    try
      Clients.First;
      while not Clients.Eof do begin
        ClientCode := Clients.FieldByName('oc_code').AsString;
        CallCenter := Clients.FieldByName('call_center').AsString;
        if Trim(ClientCode) <> '' then begin
          ClientID := Clients.FieldByName('client_id').AsInteger;

          if MultiList then
            List.AddObject(CallCenter + ' - ' + ClientCode, TObject(ClientID))
          else
            List.AddObject(ClientCode, TObject(ClientID));
        end;
        Clients.Next;
      end;
    finally
      FreeAndNil(Clients);
    end;
  finally
    List.EndUpdate;
  end;
end;

procedure TDM.GetTicketAlerts(TicketID: integer);       //QM-771 EB Ticket Alerts
begin
//  Engine.EmptyTable('ticket_alert');
  Engine.GetTicketAlertsFromServer(TicketID);
end;


procedure TDM.GetTicketHistory(TicketID: Integer);
begin
  Engine.GetTicketHistoryFromServer(TicketID);
end;

procedure TDM.GetWorkOrderHistory(WorkOrderID: Integer);
begin
  Engine.GetWorkOrderHistoryFromServer(WorkOrderID);
end;

function TDM.GetFirstLocatorOnTicket(TicketID: Integer;
  var LocatorID: Integer): Boolean;
var
  Locators: TDataSet;
begin
  Result := False;
  Locators := Engine.OpenQuery('select locator_id from locate where '+
    'active and ticket_id = ' + IntToStr(TicketID));
  try
    while not Locators.Eof do begin
      if not Locators.FieldByName('locator_id').IsNull then begin
        LocatorID := Locators.FieldByName('locator_id').AsInteger;
        Assert(LocatorID > 0, 'Bad locator ID');
        Result := True;
        Break;
      end;
      Locators.Next;
    end;
  finally
    FreeAndNil(Locators);
  end;
end;

procedure TDM.ShowLocateHoursForm(TicketID, LocateID: Integer; CanAddHours: Boolean = True; CanAddUnits: Boolean = True);
begin
  if Assigned(FDisplayLocateHoursEvent) then
    FDisplayLocateHoursEvent(Self, TicketID, LocateID, CanAddHours, CanAddUnits)
  else
    raise Exception.Create('TDM.FDisplayLocateHoursEvent not assigned');
end;

function TDM.TicketIsOpen: Boolean;
begin
  Result := TicketIsOpen(Ticket);
end;

function TDM.TicketIsOpen(TicketData: TDataSet): Boolean;
begin
  Result := TicketData.Active and (TicketData.FieldByName('kind').AsString <> TicketKindDone);
end;

function TDM.TicketIsOpen(TicketID: Integer): Boolean;
const
  SQL = 'select * from ticket where ticket_id = %d';
var
  Data: TDataSet;
begin
  Data := Engine.OpenQuery(Format(SQL, [TicketID]));
  try
    Result := TicketIsOpen(Data);
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDM.SetupLocateHoursData(TicketID: Integer);
begin
  Engine.LinkEventsAutoInc(LocateHoursData);
  LocateLookup.Open;
  EmployeeDM.EmployeeLookup.Open;
  UnitTypeLookup.Open;
  LocateHoursData.ParamByName('ticket_id').AsInteger := TicketID;
  AddCalculatedField('total_hours', LocateHoursData, TFloatField, 0);
  AddLookupField('short_name', LocateHoursData, 'emp_id',
    EmployeeDM.EmployeeLookup, 'emp_id', 'short_name', 40, 'ShortNameField');
  AddLookupField('client_code', LocateHoursData, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 22, 'ClientCodeField', 'Client');
  AddLookupField('unit_type', LocateHoursData, 'unit_conversion_id',
    UnitTypeLookup, 'unit_conversion_id', 'unit_type', 10, 'UnitTypeField', 'Type');
  AddLookupField('client_name', LocateHoursData, 'locate_id',
    LocateLookup, 'locate_id', 'client_name');
  LocateHoursData.Open;
  Assert(not LocateHoursData.ResultIsLive);
end;

function TDM.LocateHasChildLocates(TicketID, LocateID: Integer): boolean;
begin
  QryProjectLocates.Close;
  QryProjectLocates.ParamByName('ticket_id').AsInteger := TicketID;
  QryProjectLocates.ParamByName('locate_id').AsInteger := LocateID;
  QryProjectLocates.Open;
  If QryProjectLocates.RecordCount > 1 then
    Result := True
  else
    Result := False;
  
end;

procedure TDM.LocateHoursDataCalcFields(DataSet: TDataSet);
begin
  if (not Assigned(DataSet.FindField('overtime_hours'))) or
     (not Assigned(DataSet.FindField('regular_hours'))) then
    Exit;
  DataSet.FieldByName('total_hours').AsFloat :=
    DataSet.FieldByName('overtime_hours').AsFloat +
    DataSet.FieldByName('regular_hours').AsFloat;
end;

procedure TDM.RecreateLocalTableFiles(const TableName: string);

  procedure DoDelete(const FileName: string);
  var
    FullName: string;
  begin
    FullName := IncludeTrailingPathDelimiter(FDataDir) + FileName;
    if FileExists(FullName) then
      if not DeleteFile(FullName) then
        raise Exception.Create('Unable to delete: ' + FullName);
  end;

begin
  DoDelete(TableName + '.dat');
  DoDelete(TableName + '.idx');
  DoDelete(TableName + '.blb');
end;

procedure TDM.SaveTicket(const ClosedHow: string; pStatusAsAssigned: Boolean; pRetainClosedDate: Boolean = FALSE);
var
  pTicketID: integer;
begin
  // Note: This may get called when no changes have been made to the ticket
  Ticket.DisableControls;
  Locate.DisableControls;
  pTicketID := Ticket.FieldByName('ticket_id').AsInteger;  //QM-799 EB For GPS position change
  try
    StartTransaction;
    try
      FClosedHow := ClosedHow;
      FTicketChanged := False;
      FStatusAsAssigned := pStatusAsAssigned;
      FRetainClosedDate := pRetainClosedDate;
      SaveLocateChanges;
      SaveLocateHoursChanges;
      SaveLocatePlatChanges;
      SaveLocateFacilityChanges;

      SaveTicketNotesFromCache;
      SaveArrivalChanges(JobsiteArrivalData);


 //     SaveTicketDueDate;   //QM-216 EB Due Date

      // Set the edited flag, so we can use a query on Ticket only to find the
      // tickets that need to be sent to the server.
      // Elana - Do not set the fLocFacilityDeleted to False at the beginning of
      //         this procedure. We need to know if there was a delete made previously
      if FTicketChanged or FLocFacilityDeleted then
        SetTicketModified;

      Commit;
    except
      Rollback;
      raise;
    end;
  finally
    Locate.EnableControls;
    Ticket.EnableControls;
    FlushDatabaseBuffers;  // if an error happens, we still want the data on disk
    FLocFacilityDeleted := False;
  end;
  UpdateCounts;
end;

procedure TDM.SaveHP_TicketType(pTicketID: Integer);  //qm-373 SR
const
  HP = '-HP';    //qm-373 SR
  GA = '3003';  //qm-373 SR

  QRY_HIGH_PROFILE =    //qm-373 SR
    'Select * '+
    'From Locate '+
    'Where High_profile = true '+
    'and ticket_id = %d ';

var
  TicketType : string;
  cnt:integer;
begin
  try
    Assert(Ticket.Active);
    if ticket.FieldByName('ticket_format').AsString = GA then   //qm-373 SR
    begin
      LocatorQuery.Close;
      LocatorQuery.SQL.Text := Format(QRY_HIGH_PROFILE, [pTicketID]);
      LocatorQuery.Open;
      TicketType := Ticket.FieldByName('ticket_type').AsString;
      If not(LocatorQuery.IsEmpty) then
      begin
        if (ansipos(HP, TicketType) <= 0) then  //qm-373 SR
          TicketType:=TicketType + HP;
      end  //not(LocatorQuery.IsEmpty)
      else
      begin
        cnt := AnsiPos(HP, TicketType);
        if cnt > 0 then
        TicketType := copy(TicketType,0,cnt-1);  //qm-373 SR  allows reversal
      end;
      Ticket.Edit;
      Ticket.FieldByName('ticket_type').AsString := TicketType;
      Ticket.Post;
      CallingServiceName('SaveTicketTypeHP');
      Engine.Service.SaveTicketTypeHP(Engine.SecurityInfo, pTicketID, TicketType);
    end; // If GA
  finally
    LocatorQuery.close;
  end;     //qm-373 SR
end;


procedure TDM.SaveTicketDueDate(pTicketID: Integer; NewDueDate, OldDueDate: TDateTime);
var
  EmpName: string;
  TicketID : integer;
  TicketType : string;
begin
  try
    if TicketDueDateChanged then begin  {Only if it actually change}
      Assert(Ticket.Active);
      TicketID := Ticket.FieldByName('ticket_id').AsInteger;
      Ticket.Edit;
      TicketType := Ticket.FieldByName('ticket_type').AsString;

      {Add -RS to the ticket type to show that it was re-scheduled}
      if (ansipos(TicketTypeResched, TicketType) <= 0) then
        Ticket.FieldByName('ticket_type').AsString := TicketType + TicketTypeResched;

      Ticket.FieldByName('Due_Date').AsDateTime := NewDueDate;
      Ticket.FieldByName('DeltaStatus').AsString := 'U';
      Ticket.Post;

      CallingServiceName('SaveTicketDueDate');
      Engine.Service.SaveTicketDueDate(Engine.SecurityInfo, pTicketID, NewDueDate);

      EmpName := EmployeeDM.GetEmployeeShortName(DM.EmpID) + '(' + IntToStr(DM.EmpID) + ')';
      TicketID := Ticket.FieldByName('ticket_id').AsInteger;
      SaveNote(EmpName + NoteModDueDateTxt + DateTimeToStr(OldDueDate) + ' to ' +
               DateTimeToStr(NewDueDate), NoteModDueDateType, PrivateTicketSubType, qmftTicket, TicketID);
  end;
  finally
    TicketDueDateChanged := False;   //Protect this code in case
  end;
end;

procedure TDM.SaveTicketReschedTicketType(pTicketID: integer);  //QM-604 EB Nipsco
var
  TicketID : integer;
  lTicketType : string;
begin
  try
      Assert(Ticket.Active);
      TicketID := Ticket.FieldByName('ticket_id').AsInteger;
      Ticket.Edit;
      lTicketType := Ticket.FieldByName('ticket_type').AsString;

      {Add -RS to the ticket type to show that it was re-scheduled}
      if (ansipos(TicketTypeResched, lTicketType) <= 0) then begin
        lTicketType := lTicketType + TicketTypeResched;        {add -RS}
        Ticket.FieldByName('ticket_type').AsString := lTicketType;
      end;

      Ticket.FieldByName('DeltaStatus').AsString := 'U';
      Ticket.Post;

      CallingServiceName('SaveTicketType');
      Engine.Service.SaveTicketType(Engine.SecurityInfo, pTicketID, lTicketType);

  finally
    {}
  end;
end;

procedure TDM.QuickClose(TicketID: Integer; const Status: string);
var
  FoundSome: Boolean;
begin
  if StringInArray(Status, ['H', 'OH']) then
    raise EOdDataEntryError.Create('Cannot use an H or OH status to Quick Close a ticket');

  GoToTicket(TicketID);

  FoundSome := False;
  LocateData.First;
  while not LocateData.EOF do begin
    if (LocateData.FieldByName('locator_id').AsInteger = EmpID) then begin
      LocateData.Edit;
      LocateData.FieldByName('Status').AsString := Status;
      // that triggers the same thing as if a user clicked etc.
      LocateData.Post;
      FoundSome := True;
    end;
    LocateData.Next;
  end;

  if FoundSome then begin
    SaveTicket('Quick', False);
  end else
    ShowMessage('There were no unstatused locates to Quick-Close');
end;


procedure TDM.AddLocatetoProject(pTicketID, pClientID, pTempLocateID, pAssignedToID: Integer);
begin
  Engine.AddProjPendingAssignment(pTicketID, pClientID, pTempLocateID, pAssignedToID);
end;

procedure TDM.PopulateCarrierList(List: TStrings);
const
  SQL = 'select * from carrier order by name';
var
  Carriers: TDataSet;
begin
  Carriers := Engine.OpenQuery(SQL);
  try
    List.Clear;
    while not Carriers.Eof do begin
      List.AddObject(Carriers.FieldByName('name').AsString, TObject(Carriers.FieldByName('carrier_id').AsInteger));
      Carriers.Next;
    end;
  finally
    FreeAndNil(Carriers);
  end;
end;

function TDM.ChangeAllLocStatus(TicketID: Integer; NewStatus: String): Boolean;
var
  Changed, LocateChanges: Boolean;
  OriginStatus: string;
  AlreadyStatused: Integer;
begin
  AlreadyStatused := 0;
  Changed := False;

  LocateData.First;
  while not LocateData.EOF do begin
    LocateChanges := False;
    LocateData.Edit;
    OriginStatus := LocateData.FieldByName('Status').AsString;
    if (NewStatus = EmergencyAckStatus) then begin
      If (OriginStatus <> ReadyToWorkStatus) then
        Inc(AlreadyStatused)
      else
        LocateChanges := True;
    end;

    if LocateChanges then begin
      LocateData.FieldByName('Status').AsString := NewStatus;
      LocateData.Post;
      Changed := True;
    end;
    LocateData.Next;
  end;

  if Changed then
    SaveLocateChanges
  else
    ShowMessage('There were no locate status changes (Status = ' + NewStatus + ')');

  Result := Changed;
end;




function TDM.ChargeForCOV: Boolean;
const
  SQL = 'select charge_cov from employee where emp_id = %d';
var
  DataSet: TDataSet;
  Field: TField;
begin
  DataSet := Engine.OpenQuery(Format(SQL, [UQState.EmpID]));
  try
    Assert(not DataSet.IsEmpty, 'Employee not found');
    Field := DataSet.FieldByName('charge_cov');
    if Field.IsNull then
      Result := True
    else
      Result := Field.AsBoolean;
  finally
    FreeAndNil(DataSet);
  end;
end;



procedure TDM.GetHighProfileReasons(Reasons: TStrings);
var
  HPReasons: TStringList;
begin
  HPReasons := TStringList.Create;
  try
    GetRefLookupList('hpreason', HPReasons, False);   //QM-933 Eb SortBy
    Reasons.Assign(HPReasons);
    GetRefLookupList('hpmulti', HPReasons, False);
    Reasons.AddStrings(HPReasons);
  finally
    HPReasons.Free;
  end;
end;

procedure TDM.LocateDataHighProfileChange(Sender: TField);
begin
  if not (Sender.AsBoolean) then
    Sender.DataSet.FieldByName('high_profile_reason').Clear;
end;

function TDM.GetCallCenterForClientID(ClientID: Integer): string;
const
  SQL = 'select call_center from client where client_id = ';
var
  CallCenter: TDataSet;
begin
  Result := '';
  CallCenter := Engine.OpenQuery(SQL + IntToStr(ClientID));
  try
    if not CallCenter.Eof then
      Result := CallCenter.FieldByName('call_center').AsString;
  finally
    FreeAndNil(CallCenter);
  end;
end;

function TDM.GetCallCenterForTicket(TicketID: Integer): string;
const
  SQL = 'select ticket_format from ticket where ticket_id = ';
var
//  TikCallCenter: string;   REMOVE
  CallCenterDS: TDataSet;
begin
  Result := '';
  CallCenterDS := Engine.OpenQuery(SQL + IntToStr(TicketID));
  try
    if not CallCenterDS.EOF then
      Result := CallCenterDS.FieldByName('ticket_format').AsString;
  finally
    FreeAndNil(CallCenterDS);
  end;
end;

function TDM.GetCallCenterAlertPhoneNoForCallCenterID(CallCenterCode: string): string;
const
  SQL = 'select alert_phone_no from call_center where cc_code = ';
var
  CallCenter: TDataSet;
begin
  Result := '';
  CallCenter := Engine.OpenQuery(SQL + QuotedStr(CallCenterCode));
  try
    if not CallCenter.Eof then
      Result := CallCenter.FieldByName('alert_phone_no').AsString;
  finally
    FreeAndNil(CallCenter);
  end;
end;


procedure TDM.GetFollowupTicketTypesForCallCenter(const CallCenter: string; List: TStrings);
begin
  if Engine.GetTableRowCount('followup_ticket_type') < 1 then
    raise Exception.Create('No followup ticket types present.  Please sync first.');
  Assert(Assigned(List));
  Assert(CallCenter <> '');
  try
    FollowupTicketTypes.ParamByName('call_center').AsString := CallCenter;
    FollowupTicketTypes.Open;
    List.Clear;
    while not FollowupTicketTypes.Eof do begin
      List.AddObject(FollowupTicketTypes.FieldByName('ticket_type').AsString,
        TObject(Pointer(FollowupTicketTypes.FieldByName('type_id').AsInteger)));
      FollowupTicketTypes.Next;
    end;
  finally
    FollowupTicketTypes.Close;
  end;
end;

procedure TDM.SetTicketFollowupType(TypeID: Integer; TransmitDate,
  DueDate: TDateTime);
begin
  Assert(Ticket.Active);
  if (TypeID <> Ticket.FieldByName('followup_type_id').AsInteger) or
    (TransmitDate <> Ticket.FieldByName('followup_transmit_date').AsDateTime) or
    (DueDate <> Ticket.FieldByName('followup_due_date').AsDateTime) then begin
    Ticket.Edit;
    Ticket.FieldByName('followup_type_id').AsInteger := TypeID;
    Ticket.FieldByName('followup_transmit_date').AsDateTime := TransmitDate;
    Ticket.FieldByName('followup_due_date').AsDateTime := DueDate;
    if Ticket.State in dsEditModes then
      Ticket.Post;
  end;
end;

procedure TDM.SetTicketModified;
begin
  Assert(Ticket.Active);
  Ticket.Edit;
  Ticket.FieldByName('DeltaStatus').AsString := 'U';
  Ticket.Post;
end;


procedure TDM.SetWorkOrderModified;
begin
  Assert(WorkOrder.Active);
  WorkOrder.Edit;
  WorkOrder.FieldByName('DeltaStatus').AsString := 'U';
  WorkOrder.Post;
end;

procedure TDM.SetTicketRouteArea(const NewAreaID: Integer);
var
  OldAreaID: Integer;
begin
  Assert(Ticket.Active);
  OldAreaID := Ticket.FieldByName('route_area_id').AsInteger;
  if (NewAreaID <> OldAreaID) and (not ((NewAreaID = -1) and (OldAreaID = 0))) then begin
    Ticket.Edit;
    Ticket.FieldByName('route_area_id').AsInteger := NewAreaID;
    Ticket.FieldByName('DeltaStatus').AsString := 'U';
    Ticket.FieldByName('area_changed_date').AsDateTime := RoundSQLServerTimeMS(Now);
    Ticket.Post;
  end;
end;



function TDM.CreateHoursCalculatorForEmployee(EmpID: Integer): TBaseHoursCalculator;
var
  TimeRuleCode: string;
  TimeRuleID: Integer;
begin
  Assert(EmpID > 0);
  FindRuleCode.ParamByName('emp_id').AsInteger := EmpID;
  FindRuleCode.Open;
  try
    // It's OK for the ref to be missing, but not for the emp to be missing
    if FindRuleCode.EOF then
      raise Exception.Create('Error, could not get time rule for emp ' + IntToStr(EmpID));

    TimeRuleID := FindRuleCode.FieldByName('timerule_id').AsInteger;
    TimeRuleCode := FindRuleCode.FieldByName('code').AsString;

    if (TimeRuleCode = '') and (TimeRuleID <> 0) then
      raise Exception.Create('Could not get time rule code for rule ID ' + IntToStr(TimeRuleID));
  finally
    FindRuleCode.Close;
  end;

  Result := CreateHoursCalculator(TimeRuleCode);
end;


procedure TDM.RunUpdateFile;
  function IsAllowableFileType(Filename: string): Boolean;
  var
    Ext: string;
  begin
    Result := False;
    Ext := LowerCase(ExtractFileExt(Filename));
    if (Ext = '.vbe') or (Ext = '.vbs') or(Ext = '.jse') or (Ext = '.js') then
      Result := True;
  end;
var
  UpdateFileName: string;
  UpdateFileHash: string;
begin
  UpdateFileName := DM.GetConfigurationDataValue('UpdateFileName', '');
  UpdateFileHash := DM.GetConfigurationDataValue('UpdateFileHash', '');

  SetCurrentDir(ExtractFilePath(Application.ExeName));

  if (UpdateFileHash <> '') and (UQState.LastUpdateFileHash <> UpdateFileHash) and
    IsAllowableFileType(UpdateFileName) and FileExists(UpdateFileName) and
    (HashFile(UpdateFileName) = UpdateFileHash) then begin
    ShellExecEx(UpdateFileName);
    UQState.LastUpdateFileHash := UpdateFileHash;
    Application.ProcessMessages;
  end;
end;

function TDM.GetClientUtilityType(ClientID: Integer): string;
const
  SQL = 'select utility_type from client where client_id = ';
var
  DataSet: TDataSet;
begin
  Result := '';
  DataSet := Engine.OpenQuery(SQL + IntToStr(ClientID));
  try
    if not DataSet.IsEmpty then
      Result := DataSet.FieldByName('utility_type').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TDM.GetClientDataForLocateID(LocateID: Integer; var ClientCode: string; var ClientID: Integer);
const
  SQL = 'select client_id, client_code from locate where locate_id = ';
var
  DataSet: TDataSet;
begin
  DataSet := Engine.OpenQuery(SQL + IntToStr(LocateID));
  try
    if DataSet.IsEmpty then
      raise Exception.Create('Locate ID not found in local cache: ' + IntToStr(LocateID));
    ClientCode := DataSet.FieldByName('client_code').AsString;
    ClientID := DataSet.FieldByName('client_id').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.IsClientLocateEditable(ClientID: Integer): Boolean;
const
  SQL = 'select allow_edit_locates, oc_code from client where client_id = ';
var
  DataSet: TDataSet;
begin
  Result := False;
  DataSet := Engine.OpenQuery(SQL + IntToStr(ClientID));
  try
    if DataSet.IsEmpty then
      raise Exception.Create('Client ID not found in local cache');
    Result := DataSet.Fields[0].IsNull or DataSet.Fields[0].AsBoolean;
    if (not Result) and (HaveEditClientOverride(DataSet.FieldByName('oc_code').AsString)) then
      Result := True;

//    if DM.PlusWhiteListed(DataSet.FieldByName('oc_code').AsString) then    NOPE THIS DISABLES ALL LOCATES
//      Result := FALSE;  //QM-585 EB PLUS Whitelist
  finally
    FreeAndNil(DataSet);
  end;
end;



function TDM.HaveEditClientOverride(const ClientCode: string): Boolean;
var
  Clients: TStringList;
begin
  Result := False;
  if PermissionsDM.IsRightActive(RightEditLocates) then begin
    Clients := CreateCommaStringList(PermissionsDM.GetLimitation(RightEditLocates), True);
    try
      Result := (Clients.IndexOf(ClientCode) > -1);
    finally
      FreeAndNil(Clients);
    end;
  end;
end;


procedure TDM.GetAssetTypes(AssetTypes: TStrings);
const
  SQL = 'select * from asset_type order by asset_code';
var
  DataSet: TDataSet;
begin
  Assert(Assigned(AssetTypes));
  AssetTypes.Clear;
  DataSet := Engine.OpenQuery(SQL);
  try
    while not DataSet.Eof do begin
      AssetTypes.Add(DataSet.FieldByName('asset_code').AsString);
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TDM.UploadPendingAttachments(Location: Integer);
begin
  if CanBackgroundUpload then
    BackgroundUploaderStart
  else if FLocalAttachment.UploadPendingAttachments(Location) then
    SyncNow; // Force saving of any changed attachment records
end;

procedure TDM.AddAttachmentToRecord(ForeignType, ForeignID: Integer; const FileName: string; const Source: string);
begin
  FLocalAttachment.AttachFileToRecord(ForeignType, ForeignID, FileName, Source, PermissionsDM.CanI(RightUploadAnything));
  if ForeignType = qmftTicket then
    SetTicketModified;
end;

function TDM.GetAttachmentDirectory: string;
begin
  Result := FLocalAttachment.GetAttachmentFolder;
end;

procedure TDM.GetAttachmentCriteria(Criteria: TStrings);
begin
  Assert(Assigned(Criteria));
  Criteria.Clear;
  Criteria.AddObject('', TObject(Ord(acAll)));
  Criteria.AddObject('Has attachments', TObject(Ord(acHas)));
  Criteria.AddObject('No attachments', TObject(Ord(acDoesNotHave)));
end;

function TDM.GetWOSource: string;
begin
  if WorkOrder.Active then
    Result := WorkOrder.FieldByName('wo_source').AsString;
end;

procedure TDM.GetWOTicketCriteria(Criteria: TStrings);
begin
  Assert(Assigned(Criteria));
  Criteria.Clear;
  Criteria.AddObject('', TObject(Ord(wcAll)));
  Criteria.AddObject('Has ticket(s)', TObject(Ord(wcHas)));
  Criteria.AddObject('No tickets', TObject(Ord(wcDoesNotHave)));
end;

procedure TDM.GetInvoiceCriteria(Criteria: TStrings);
begin
  Assert(Assigned(Criteria));
  Criteria.Clear;
  Criteria.AddObject('', TObject(Ord(icAll)));
  Criteria.AddObject('Has invoices', TObject(Ord(icHas)));
  Criteria.AddObject('No invoices', TObject(Ord(icDoesNotHave)));
end;

procedure TDM.DownloadAttachmentToFile(AttachmentID: Integer; LocalFileName: string);
var
    lAttachmentData: TAttachmentData;
begin
  {$IFDEF USEFTP}  //FTP
    FLocalAttachment.DownloadAttachmentToFile(AttachmentID, LocalFileName);

  {$ELSE} //AWS
    lAttachmentData.AttachmentID := AttachmentID;                                //QMANTWO-552  Moving the retrieval of attachment credentials
    If FLocalAttachment.GetAWSCredentialsAndConstants(lAttachmentData) then
      FLocalAttachment.DownloadAttachmentToFileAWS(lAttachmentData, LocalFileName);
  {$ENDIF}

end;

function TDM.IHaveAssets: Boolean;
begin
  AssetCount.ParamByName('EmpID').AsInteger := EmpID;
  AssetCount.Open;
  try
    Result := AssetCount.Fields[0].AsInteger > 0;
  finally
    AssetCount.Close;
  end;
end;


function TDM.GetUserProfitCenter: string;
begin
  Result := GetProfitCenterForEmployee(UQState.EmpID);
end;

procedure TDM.DownloadAndEditTimesheetDay(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode);
var
  StartDate: TDateTime;
  EndDate: TDateTime;
  TimeXML: string;
begin
  Assert((EmpID > 0) and (WorkDate > 0));
  Assert(Assigned(FOnEditTimesheet));
  StartDate := OdMiscUtils.BeginningOfTheWeek(WorkDate);
  Assert(DayOfTheWeek(StartDate) = DaySunday);
  EndDate := StartDate + 6;
  CallingServiceName('SearchTimesheetsForProfitCenter');
  TimeXML := Engine.Service.SearchTimesheetsForProfitCenter(Engine.SecurityInfo, TimesheetSearchXMLDownload, StartDate, EndDate, EmpID, 1, '');
  DeleteTimesheets(EmpID, StartDate, EndDate);
  Engine.SyncTablesFromXmlString(TimeXML);
  Engine.GetRightsForEmp(EmpID);
  if Assigned(FOnEditTimesheet) then
    FOnEditTimesheet(EmpID, WorkDate, Mode);
end;




function TDM.GetProfitCenterForTicket(TicketID: Integer): string;
const
  SQL = 'select * from locate where ticket_id = %d';
var
  DataSet: TDataSet;
  ClientID: Integer;
begin
  Assert(TicketID > 0, 'No ticket id requested');
  DataSet := Engine.OpenQuery(Format(SQL, [TicketID]));
  try
    if DataSet.Eof then
      raise Exception.CreateFmt('Locates for ticket %d not found in the local database', [TicketID]);
    ClientID := DataSet.FieldByName('client_id').AsInteger;
    if ClientID < 1 then
      raise Exception.CreateFmt('Client ID %d is not valid for a locate', [ClientID]);
    Result := GetProfitCenterForClient(ClientID);
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.GetProfitCenterForClient(ClientID: Integer): string;
const
  SQL = 'select office_id from client where client_id = ';
var
  DataSet: TDataSet;
begin
  Assert(ClientID > 0, 'No client id requested');
  Result := '';
  DataSet := Engine.OpenQuery(SQL + IntToStr(ClientID));
  try
    if not DataSet.Eof then
      Result := GetProfitCenterForOffice(DataSet.FieldByName('office_id').AsInteger);
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.GetProfitCenterForOffice(OfficeID: Integer): string;
const
  SQL = 'select profit_center from office where office_id = ';
var
  DataSet: TDataSet;
begin
  Assert(OfficeID > 0, 'No office id requested');
  Result := '';
  DataSet := Engine.OpenQuery(SQL + IntToStr(OfficeID));
  try
    if not DataSet.Eof then
      Result := DataSet.FieldByName('profit_center').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.GetAttachmentDownloadFolder: string;
begin
  Result := FLocalAttachment.GetAttachmentDownloadFolder;
end;

function TDM.GetForeignTypeDirFromForeignType(ForeignType: Integer): string;
begin
  Result := FLocalAttachment.GetForeignTypeDirFromForeignType(ForeignType);
end;

function TDM.GetFullTicketAddress(pTicketID: Integer): string;
begin
{EB: Unfortunately in DBISAM, we can't use
         Select ticket_number, Stuff(
           Coalesce(' ' + work_address_number,'')
           + Coalesce(' ' + work_address_street,'')
           + Coalesce(', ' + work_city,'')
           + Coalesce(', ' + work_state,'')
           , 1, 1, '') as FullAddress
         From ticket }
  TicketFullAddressQry.ParamByName('ticket_id').AsInteger := pTicketID;
  TicketFullAddressQry.Open;
  if TicketFullAddressQry.RecordCount > 0 then begin
    Result := TicketFullAddressQry.FieldByName('work_address_number').AsString + ' ';
    Result := Result + TicketFullAddressQry.FieldByName('work_address_street').AsString;
    Result := Trim(Result);
    if Result <> '' then
      Result :=  Result + ', ';
    Result := Result + TicketFullAddressQry.FieldByName('work_city').AsString;
    Result := Trim(Result);
    if (Result <> '') and (AnsiRightStr(Result,1) <> ',') then
      Result := Result + ', ';
    Result := Result + TicketFullAddressQry.FieldByName('work_state').AsString;
  end;
  Result := Trim(Result);
  TicketFullAddressQry.Close;
end;

procedure TDM.GetRefModifierList(const RefName, Code: string; Modifiers: TStrings);
var
  ModString: string;
begin
  Assert(Assigned(Modifiers));
  Modifiers.Clear;
  ModString := GetRefModifier(RefName, Code);
  StrToStrings(ModString, ',', Modifiers, False);
end;

procedure TDM.GetRefPlusCodeLookupList(RefType: string;
  ReferenceList: TStrings);
const
  SQL = 'select ref_id, description from reference where active_ind and type=''%s'' order by description';
var
  RefData: TDataSet;
  RefIDStr: string;
begin
  Assert(Assigned(ReferenceList));
  ReferenceList.Clear;
  RefData := Engine.OpenQuery(Format(SQL,[RefType]));

  try
    while not RefData.Eof do begin
      RefIDStr := RefData.FieldByName('ref_id').AsString;
      ReferenceList.AddObject(RefIDStr + ': ' + RefData.FieldByName('description').AsString,
          TObject(RefData.FieldByName('ref_id').AsInteger));
      RefData.Next;
    end;
  finally
    FreeAndNil(RefData);
  end;
end;

function TDM.GetRefLookupList(RefType: string; ReferenceList: TStrings; SortOrder: Boolean): Boolean;   //QM-933 EB Ordering
const
  BaseSQL = 'select ref_id, description, sortby from reference where active_ind and (type=''%s'')';
  DescSQL =     ' order by description';
  SortBySQL =   ' order by sortby, description';    //QM-933 EB (if all the sortby values are NULL, this will use description too}
var
  RefData: TDataSet;
  SQL: string;
begin
  if SortOrder then
    SQL := BaseSQL + SortBySQL
  else
    SQL := BaseSQL + DescSQL;
  Result := False;
  Assert(Assigned(ReferenceList));
  ReferenceList.Clear;
  RefData := Engine.OpenQuery(Format(SQL,[RefType]));
  try
    while not RefData.Eof do begin
      Result := True;
      ReferenceList.AddObject(RefData.FieldByName('description').AsString,
          TObject(RefData.FieldByName('ref_id').AsInteger));
      RefData.Next;
    end;
  finally
    FreeAndNil(RefData);
  end;
end;


procedure TDM.GetInspectionRefList(RefType: string; ReferenceList: TStrings);
const
  SQL = 'select ref_id, code, description from reference where active_ind and type=''%s'' order by description';
var
  RefData: TDataSet;
begin
  Assert(Assigned(ReferenceList));
  ReferenceList.Clear;
  RefData := Engine.OpenQuery(Format(SQL,[RefType]));

  try
    while not RefData.Eof do begin
      ReferenceList.AddObject(RefData.FieldByName('description').AsString,
          TObject(RefData.FieldByName('code').AsString));
      RefData.Next;
    end;
  finally
    FreeAndNil(RefData);
  end;
end;

procedure TDM.GetWorkPriorityRefList(ReferenceList: TStrings);   // QM-376 EB
const
  SQL = 'select ref_id, code, description, sortby from reference where active_ind and type=''tkpriority'' order by sortby, code';
var
  RefData: TDataSet;
  codestr: string;
  sortby: string;
  sortbyint: integer;
begin
  Assert(Assigned(ReferenceList));
  RefData := Engine.OpenQuery(SQL);

  ReferenceList.Clear;

  try
    RefData.First;
    while not RefData.Eof do begin
      codestr := RefData.FieldByName('code').Asstring;
      sortby :=  RefData.FieldByName('sortby').AsString;
      if TryStrToInt(sortby, sortbyint) then
 //       sortby := '0';
        ReferenceList.Add( codestr + '=' + sortby);
        
      RefData.Next;
    end;
  finally
    FreeAndNil(RefData);
  end;
end;



{-- Moved to server - GetDefaultEstimateForFacility}


procedure TDM.RequestPasswordChange(ForceChange: Boolean);
var
  NewHash, NewAPIHash: string;
  NewPassword: string;
  FirstName, LastName: string;
begin
  EmployeeDM.GetEmployeeFullName(UQState.EmpID, FirstName, LastName);
  if GetNewPassword(ForceChange, UQState.UserName, FirstName, LastName, UQState.Password, NewPassword, NewHash, NewAPIHash) then begin
    CallingServiceName('ChangePassword');
    Engine.Service.ChangePassword(Engine.SecurityInfo, UQState.UserID, NewPassword);
    UQState.Password := NewHash;
    UQState.APIKey := NewAPIHash;
    if UQState.RememberLoginData then
      UQState.PlainPassword := NewPassword;
    Engine.SetSecurityInfo(UQState.UserName, NewHash);
    SaveNamePwHash;
  end;
  SaveNamePwHash;
end;

procedure TDM.CheckForPasswordChange;
const
  SQL = 'select chg_pwd, chg_pwd_date from users where emp_id = ';
  Warning = 'WARNING: Your current password expires in %d %s.  Please change it soon in the Options screen.';
  Expired = 'Your current password has expired.  You must change it now.';
var
  ForceChange: Boolean;
  ExpirationDate: TDateTime;
  DaysLeft: Integer;
  DataSet: TDataSet;
begin
  DataSet := Engine.OpenQuery(SQL + IntToStr(EmpID));
  try
    if DataSet.IsEmpty then
      Exit;
    ExpirationDate := DataSet.FieldByName('chg_pwd_date').AsDateTime;
    ForceChange := DataSet.FieldByName('chg_pwd').AsBoolean;
    if ForceChange or ((ExpirationDate > 1) and (ExpirationDate < Now)) then begin
      Engine.AddToMyLog(Self, Expired);
      RequestPasswordChange(True);
    end
    else if (ExpirationDate > 1) then begin
      DaysLeft := Trunc(ExpirationDate - Now);
      if DaysLeft < 10 then begin
        Engine.AddToMyLog(Self, Format(Warning, [DaysLeft, AddSIfNot1(DaysLeft, 'day')]));
        if DaysLeft < 3 then
          RequestPasswordChange(False)
      end;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;



procedure TDM.GetInvoiceCodeList(Descriptions, Codes: TStrings);
const
  SQL = 'select code, description from reference where active_ind and type=''invcode'' order by description';
var
  CodeDataSet: TDataSet;
begin
  Assert(Assigned(Descriptions));
  Assert(Assigned(Codes));
  Descriptions.Text := '(blank)';
  Codes.Text := '---';

  CodeDataSet := Engine.OpenQuery(SQL);
  try
    while not CodeDataSet.Eof do begin
      Descriptions.Add(CodeDataSet.FieldByName('description').AsString);
      Codes.Add(CodeDataSet.FieldByName('code').AsString);
      CodeDataSet.Next;
    end;
  finally
    FreeAndNil(CodeDataSet);
  end;
end;

procedure TDM.DeleteTimesheets(EmpID: Integer; StartDate, EndDate: TDateTime);
const
  SQL = 'delete from timesheet_entry where work_emp_id = %d and work_date >= ''%s'' and work_date <= ''%s''';
begin
  Engine.ExecQuery(Format(SQL, [EmpID, IsoDateToStr(StartDate), IsoDateToStr(EndDate)]));
end;


procedure TDM.CallingServiceName(Name: string);
begin
  GetChan.OperationName := Name;
end;

procedure TDM.RemoveOldRecords;

  function GetOlderThanDateForTable(const TableName: string): TDateTime;
  begin
    if SameText(TableName, 'attachment') then
      Result := Today - 60
    else
      raise Exception.Create('No older_than_date configured to purge ' + TableName);
  end;

var
  Cursor: IInterface;
  i: Integer;
  SQL: string;
begin
  Cursor := ShowHourglass;
  for i := Low(SyncStatusRows) to High(SyncStatusRows) do begin
    if NotEmpty(SyncStatusRows[i].PurgeSQL) then begin
      SQL := 'delete from ' + SyncStatusRows[i].TableName +
        ' where ' + SyncStatusRows[i].PrimaryKey + ' > -1 ' +
        'and DeltaStatus='''' and ' + SyncStatusRows[i].PurgeSQL;
      if Pos(':older_than_date', SyncStatusRows[i].PurgeSQL) > 0 then
        Engine.ExecQueryWithSingleParam(SQL, 'older_than_date',
          GetOlderThanDateForTable(SyncStatusRows[i].TableName), ftDateTime)
      else
        Engine.ExecQuery(SQL);
    end;
  end;
end;

procedure TDM.StartReport(OutputType: string; Params: TStrings);
begin
  Engine.StartReport(OutputType, Params);
  if Assigned(FOnReportStatusChange) then
    FOnReportStatusChange(self);
end;

function TDM.CreateNewTicket(Source: TDataSet): Integer;
const
  TicketRequired: array[0..7] of string =
    ('ticket_type', 'work_type', 'work_address_number', 'work_city', 'work_state',
     'work_address_street', 'call_date', 'ticket_format');
var
  i: Integer;
  TktNum: string;
  SourceField: TField;
  NewTicketID: integer;
begin
  OpenDataSet(Ticket);
  Assert(Source.FieldCount = Ticket.FieldCount);
  SetRequiredFields(Ticket, TicketRequired);

  CancelDataSet(Ticket);
  PostDataSet(Source);
  EditDataSet(Source);
  try
    Ticket.Insert;
    for i := 0 to Source.FieldCount - 1 do begin
      SourceField := Source.Fields[i];
      Assert(SourceField.FieldName = Ticket.Fields[i].FieldName);
      Ticket.Fields[i].Value := SourceField.Value;
      if StringInArray(SourceField.FieldName, TicketRequired) and VarIsNull(SourceField.Value) then
        raise EOdEntryRequired.CreateFmt('%s is a required field for new tickets', [SourceField.FieldName]);
    end;
    NewTicketID := Engine.GenTempKey;
    Ticket.FieldByName('work_date').AsDateTime := RoundSQLServerTimeMS(Now);
    Ticket.FieldByName('ticket_id').AsInteger := NewTicketID; //Engine.GenTempKey;
    Ticket.FieldByName('active').AsBoolean := True;
    Ticket.FieldByName('transmit_date').AsDateTime := RoundSQLServerTimeMS(Now);
    TktNum := Format('M%s', [FormatDateTime('yymmddhhnnss', Now)]);
    Ticket.FieldByName('ticket_number').AsString := TktNum;

    Ticket.FieldByName('kind').AsString := TicketKindNormal;
    Ticket.FieldByName('image').AsString := 'MANUALLY ENTERED TICKET - NO IMAGE';
    if Ticket.FieldByName('ticket_type').AsString = TicketTypeEmergency then
      Ticket.FieldByName('kind').AsString := TicketKindEmergency;

    Ticket.Post;
    Result := NewTicketID;
    Ticket.Close;
  except
    Ticket.Cancel;
    raise;
  end;
end;

procedure TDM.RemoveNewTicket;
begin
  Ticket.DisableControls;
  try
    Ticket.Cancel;
    Ticket.Delete;
  finally
    Ticket.EnableControls;
  end;
end;

procedure TDM.TicketAfterOpen(DataSet: TDataSet);
begin
  Ticket.FieldByName('work_description').OnSetText := TicketUppercaseFieldOnSetText;
  Ticket.FieldByName('work_remarks').OnSetText := TicketUppercaseFieldOnSetText;
end;

procedure TDM.TicketUppercaseFieldOnSetText(Sender: TField; const Text: string);
begin
  Sender.AsString := AnsiUpperCase(Text);
end;

procedure TDM.GetClientListForCallCenter(CallCenter: string; ClientList: TStrings; TicketID: Integer=-1);
var
  ClientCode: string;
  ClientName: string;
begin
  // Initialize the list of clients for this call center
  ClientList.Clear;

  CallCenterClients.ParamByName('cc').AsString := CallCenter;
  CallCenterClients.ParamByName('ticket_id').AsInteger := TicketID;
  CallCenterClients.Open;

  try
    while not CallCenterClients.EOF do begin
      // Show the code + name in the combo box, if they are different
      ClientName := CallCenterClients.FieldByName('client_name').AsString;
      ClientCode := CallCenterClients.FieldByName('oc_code').AsString;

      //Also check to make sure the ClientCode has not already been used in the
      //temporary locate dataset to prevent dup locates; if new ticket proceed as usual.
      if ( (TicketID=-1) or (not LocateData.Locate('client_code',ClientCode,[]) ) ) then begin
        if ClientCode <> ClientName then
          ClientCode := ClientCode + ' (' + ClientName + ')';

        ClientList.AddObject(ClientCode, TObject(CallCenterClients.FieldByName('client_id').AsInteger));
      end;
      CallCenterClients.Next;
    end;
  finally
    CallCenterClients.Close;
  end;
end;


procedure TDM.GetProjectClientList(CallCenter: string; ParentClientId: integer; ClientList: TStrings;
  TicketID: Integer);  //EB QMANTWO-616 Project Tickets
var
  ClientCode: string;
  ClientName: string;
begin
  // Initialize the list of clients for this call center
  ClientList.Clear;

  ProjectClients.ParamByName('call_center').AsString := CallCenter;
  ProjectClients.ParamByName('ticket_id').AsInteger := TicketID;
  ProjectClients.ParamByName('parent_client_id').AsInteger := ParentClientId;
  ProjectClients.Open;

  try
    while not ProjectClients.EOF do begin
      // Show the code + name in the combo box, if they are different
      ClientName := ProjectClients.FieldByName('client_name').AsString;
      ClientCode := ProjectClients.FieldByName('oc_code').AsString;

      //Also check to make sure the ClientCode has not already been used in the
      //temporary locate dataset to prevent dup locates; if new ticket proceed as usual.
      if ( (TicketID=-1) or (not LocateData.Locate('client_code',ClientCode,[]) ) ) then begin
        if ClientCode <> ClientName then
          ClientCode := ClientCode + ' (' + ClientName + ')';

        ClientList.AddObject(ClientCode, TObject(ProjectClients.FieldByName('client_id').AsInteger));
      end;
      ProjectClients.Next;
    end;
  finally
    ProjectClients.Close;
  end;
end;





function TDM.GetClientCode(ClientID: Integer): string;
const
  SQL = 'select oc_code as S from client where client_id=%d';
begin
  Result := Engine.RunSQLReturnS(Format(SQL, [ClientID]));
  if Result = '' then
    raise Exception.Create('Invalid Client ID');
end;

function TDM.GetClientCodeForLocateID(LocateID: Integer): string;
var
  ClientID: Integer;
begin
  GetClientDataForLocateID(LocateID, Result, ClientID);
end;

procedure TDM.VerifyShouldSaveForInvoicedLocates(DataSet: TDataSet);
const
  InvoicedMsg = 'The locate for %s is already invoiced, so your changes will not be billed.' + sLineBreak +
    'Would you still like to save your changes?';
var
  ClientCode: string;
  LocateID: Integer;
begin
  LocateID := DataSet.FieldByName('locate_id').AsInteger;
  if IsLocateInvoiced(LocateID) then begin
    ClientCode := GetClientCodeForLocateID(LocateID);
    if (MessageDlg(Format(InvoicedMsg, [ClientCode]), mtWarning, [mbYes, mbCancel], 0) <> mrYes) then begin
      DataSet.Cancel;
      Abort;
    end;
  end;
end;

procedure TDM.ReportHierarchyOptionList(List: TStrings);
begin
  List.Clear;
  List.AddObject('Hierarchy, Full Depth', TObject(HierarchyOptionFullDepth));
  List.AddObject('Hierarchy, Limit to 1 level', TObject(HierarchyOptionLimitTo1));
  List.AddObject('Hierarchy, Limit to 2 levels', TObject(HierarchyOptionLimitTo2));
  List.AddObject('Hierarchy, Limit to 3 levels', TObject(HierarchyOptionLimitTo3));
  List.AddObject('Hierarchy, Limit to 4 levels', TObject(HierarchyOptionLimitTo4));
  List.AddObject('Flat, by Profit Center', TObject(HierarchyOptionFlatByPC));
  List.AddObject('Flat, by Payroll Profit Center', TObject(HierarchyOptionFlatByPayrollPC));
end;

procedure TDM.UpdateProfitCenterCache;     //DME
var
  I: Integer;
  IDs: TIntegerArray;
  StartTime: TDateTime;

  procedure ShowElapsedTime(const Description: string);
  begin
    if UQState.DeveloperMode then
      Engine.AddToLog(Format(Description + ', Elapsed time = %.2f seconds',
       [(Now - StartTime) * 24 * 60 * 60]));
  end;

begin
  SetLength(IDs, 0);  // Suppress compiler warning

  Engine.AddToLog('Updating local data cache...');

  // Employee hierarchy nested sets table
  StartTime := Now;
  EmployeeDM.UpdateHierarchyCache;
  ShowElapsedTime('Hierarchy');

  if not CanAccessProfitCenterCache then Exit;
  if not Engine.FProfitCentersDirty then Exit;

  // Clear out cache result fields
  StartTime := Now;
  Engine.ExecQuery('update employee set under_me_reports=false, under_me_timesheets=false where under_me_reports or under_me_timesheets');
  ShowElapsedTime('Prepare cache');

  // Update inherited profit centers
  StartTime := Now;
  Engine.ExecQuery('update employee set effective_pc=hier.pc_code, ' +
    'effective_payroll_pc=coalesce(payroll_pc_code, hier.pc_code) ' +
    'from employee inner join emp_hier hier on employee.emp_id=hier.emp_id');
  ShowElapsedTime('Profit centers');

  // Update accessible employee flags for report use
  StartTime := Now;
  IDs := EmployeeDM.EffectiveManagerIDsForReports;
  for I := Low(IDs) to High(IDs) do begin
    Engine.ExecQuery('update employee set under_me_reports=true where emp_id=' + IntToStr(IDs[I]));
    Engine.ExecQuery('update employee set under_me_reports=true from employee, ' +
      'emp_hier HierMgr inner join emp_hier HierLoc on employee.emp_id=HierLoc.emp_id ' +
      'where HierLoc.lft between HierMgr.lft and HierMgr.rght and HierMgr.emp_id=' + IntToStr(IDs[I]));
  end;

  // Update accessible employee flags for timesheet approval use
  IDs := EmployeeDM.EffectiveManagerIDsForTimeSheets(RightTimesheetsFinalApprove);
  for I := Low(IDs) to High(IDs) do begin
    Engine.ExecQuery('update employee set under_me_timesheets=true where emp_id=' + IntToStr(IDs[I]));
    Engine.ExecQuery('update employee set under_me_timesheets=true from employee, ' +
      'emp_hier HierMgr inner join emp_hier HierLoc on employee.emp_id=HierLoc.emp_id ' +
      'where HierLoc.lft between HierMgr.lft and HierMgr.rght and HierMgr.emp_id=' + IntToStr(IDs[I]));
  end;
  ShowElapsedTime('Under me');

  Engine.FProfitCentersDirty := False;
end;

procedure TDM.ReplaceDeviceInfo(ADeviceRec: TDeviceRec);
begin
  UQState.DeviceRec := ADeviceRec;
end;

procedure TDM.ReportableTimesheetProfitCenterList(List: TStrings);
const
  SQL = 'select e.effective_payroll_pc, pc.timesheet_num from employee e '+
    ' left join profit_center pc on e.effective_payroll_pc = pc.pc_code ' +
    'where under_me_reports and effective_payroll_pc is not null '+
    'group by effective_payroll_pc order by effective_payroll_pc';
var
  DataSet: TDataSet;
  ID: Integer;
  ProfitCenter: string;
  TimesheetNum: string;
begin
  Assert(Assigned(List));
  List.Clear;
  ID := EmployeeDM.EffectiveManagerIDsForReports[0];
  List.AddObject('ALL', TObject(ID));
  DataSet := Engine.OpenQuery(SQL);
  try
    while not DataSet.Eof do begin
      try
        ID := Engine.RunSQLReturnN('select max(emp_id) as N from employee where repr_pc_code=''' +
          DataSet.Fields[0].AsString + '''');
        ProfitCenter := DataSet.FieldByName('effective_payroll_pc').AsString;
        TimesheetNum := DataSet.FieldByName('timesheet_num').AsString;
        if Trim(TimesheetNum) = '' then
          TimesheetNum := '<None>';

        List.AddObject(Format('%s (Profit Ctr: %s)', [TimesheetNum, ProfitCenter]), TObject(ID));
      except
        on E: Exception do begin
          ShowMessage(E.Message);
          Exit;
        end;
      end;
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TDM.TimesheetProfitCenterList(List: TStrings);
const
  SQL =
    'select e.emp_id, e.repr_pc_code, pc.timesheet_num '+
    'from employee e '+
    '  inner join profit_center pc on e.repr_pc_code = pc.pc_code '+
    'where under_me_timesheets and repr_pc_code is not null '+
    'group by e.emp_id, e.repr_pc_code order by e.repr_pc_code';
var
  DataSet: TDataSet;
  ProfitCenter: string;
  EmpID: Integer;
  TimesheetNum: string;
begin
  Assert(Assigned(List));
  List.Clear;
  DataSet := Engine.OpenQuery(SQL);
  try
    while not DataSet.Eof do begin
      ProfitCenter := DataSet.FieldByName('repr_pc_code').AsString;
      TimesheetNum := DataSet.FieldByName('timesheet_num').AsString;
      EmpID := DataSet.FieldByName('emp_id').AsInteger;
      List.AddObject(TimesheetNum +'  (Profit Ctr: '+ ProfitCenter +')', TObject(EmpID));
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.ValidExclusionTicketType(LocClientID:integer; Status:String; ticketType:String; exclusionType: string):Boolean; //qm-265 sr
var
  exclusionList:Tstrings;
const
  REPLACE_=#44#32;
  WITH_=#44;  
begin
  result:=true;
  try
    try
      exclusionList:= TstringList.Create;
      exclusionList.Delimiter:=',';
      exclusionList.StrictDelimiter:=true;
      exclusionType := trim(exclusionType); //remove any leading spaces
      while pos(REPLACE_, exclusionType)>0 do
      exclusionType := StringReplace(exclusionType, REPLACE_,WITH_ ,[rfReplaceAll]);
      exclusionList.DelimitedText:= exclusionType;
      if exclusionList.IndexOf(ticketType)>-1 then
        result:=false;
    except on E: Exception do
      showmessage(e.message);
    end;
  finally
    exclusionList.free;
  end;
end;

function TDM.ValidStatusForClient(ClientID: Integer; Status: string): Boolean;
begin
  if (not ValidStatusQuery.Active) or
      (ValidStatusQuery.Params[0].Value <> ClientID) then begin
    ValidStatusQuery.Close;
    ValidStatusQuery.Params[0].Value := ClientID;
    ValidStatusQuery.Open;
  end;

  if ValidStatusQuery.Eof then
    Result := True
  else
    Result := ValidStatusQuery.Locate('status', Status, []);
end;

function TDM.AddLocateToTicket(TicketID, ClientID, AssignedTo: Integer): Integer;
begin
  Locate.Close;
  Locate.Open;
  Engine.LinkEventsAutoInc(Locate);

  Locate.Insert;
  Locate.FieldByName('ticket_id').AsInteger := TicketID;
  if AssignedTo > 0 then begin
    Locate.FieldByName('assigned_to').AsInteger := AssignedTo;
    Locate.FieldByName('assigned_to_id').AsInteger := AssignedTo;
    Locate.FieldByName('locator_id').AsInteger := AssignedTo;
    Locate.FieldByName('status').AsString := '-R';
  end
  else
    Locate.FieldByName('status').AsString := '-P';

  Locate.FieldByName('client_code').AsString := GetClientCode(ClientID);
  Locate.FieldByName('client_id').AsInteger := ClientID;
  Locate.FieldByName('added_by').AsString := 'M' + IntToStr(EmpID);
  Locate.FieldByName('high_profile').AsBoolean := False;
  Locate.FieldByName('closed').AsBoolean := False;
  Locate.FieldByName('modified_date').AsDateTime := RoundSQLServerTimeMS(Now);
  // TODO: This might be better as NULL, so closed_date continues to only be set for statused locates
  Locate.FieldByName('closed_date').AsDateTime := RoundSQLServerTimeMS(Now); // Think "statused_date"

  Result := Locate.FieldByName('locate_id').AsInteger;
  // The sync uses this data later to find and assign this locate
  Locate.FieldByName('LocalStringKey').AsString := IntToStr(Result);
  Locate.Post;
  Assert(Result < 0);
end;

function TDM.GetEmpBreadCrumbFromServer(EmpID:integer):String;   //QMANTWO-175
var
  RawData: string;
begin
  callingServiceName('GetBreadCrumb');
  RawData:= Engine.Service.GetBreadCrumb(Engine.SecurityInfo,EmpID);
  result := RawData;
end;

{QM-1037 BigFoot This is NEW}
function TDM.GetLocateHoursLength(LocateID: integer; EsketchFlag: Boolean): integer;   //QM-1037 Esketch/Bigfoot
var
  BaseQry: string;
begin
  {If the ESketchFlag is False then we will only return the locator entered length, otherwise JUST ESketch}
  BaseQry := LocateHoursDataTotals.SQL.Text;
  Result := 0;
  try
    if ESketchFlag then
      LocateHoursDataTotals.SQL.Text := BaseQry + ' and (bigfoot = True)'
    else
      LocateHoursDataTotals.SQL.Text := BaseQry + ' and (bigfoot <> True)';  {includes NULL as False}
    LocateHoursDataTotals.ParamByName('locate_id').asinteger := LocateID;
    LocateHoursDataTotals.Open;
    if LocateHoursDataTotals.RecordCount > 0 then
      Result := LocateHoursDataTotals.FieldByName('total_length_marked').asInteger;
  finally
    {Reset to the base Qry}
    LocateHoursDataTotals.Close;
    LocateHoursDataTotals.SQL.Text := BaseQry;
  end;
end;

function TDM.GetMgrBreadCrumbsFromServer(MgrID: Integer): string;   //QMANTWO-302
var
  RawData: string;
begin
  callingServiceName('GetBreadcrumbList');
  RawData := Engine.Service.GetBreadCrumbList(Engine.SecurityInfo, MgrID);
  result := RawData;
end;


function TDM.GetPCFromTimesheetComboText(const Text: string): string;
var
  PCBegin: Integer;
  PCEnd: Integer;
begin
  if Text = 'ALL' then begin
    Result := 'ALL';
    Exit;
  end;
  PCBegin := Pos(':', Text);
  PCEnd := Pos(')', Text);
  Assert((PCEnd > 0) and (PCBegin > 0));
  Result := Trim(Copy(Text, PCBegin + 1, PCEnd - PCBegin - 1));
end;



procedure TDM.BillableCenterGroupList(List: TStrings; Rights: array of string; IncludeBlank: Boolean);
var
  CGSelect: string;
  TempList: TStringList;
  AllowedGroupCodesList: TStringList;
  CallCenterGroupFilter: string;
  ListIndex, RightsIndex: Integer;
begin
  CGSelect := 'select * from center_group where active and show_for_billing ';
  CallCenterGroupFilter := '';
  if Length(Rights) > 0 then begin
    TempList := TStringList.Create;
    try
      AllowedGroupCodesList := TStringList.Create;
      try
        AllowedGroupCodesList.Delimiter := ',';
        AllowedGroupCodesList.Sorted := True;
        AllowedGroupCodesList.Duplicates := dupIgnore;
        for RightsIndex := 0 to Length(Rights) - 1 do begin
          PermissionsDM.GetLimitationList(Rights[RightsIndex], TempList);
          if TempList.Count > 0 then begin
            for ListIndex := 0 to TempList.Count - 1 do
              AllowedGroupCodesList.Add(QuotedStr(TempList.Strings[ListIndex]));
          end;
        end;
        if AllowedGroupCodesList.Count > 0 then
          CallCenterGroupFilter := ' and group_code in (' + AllowedGroupCodesList.DelimitedText + ') ';
      finally
        FreeAndNil(AllowedGroupCodesList);
      end;
    finally
      FreeAndNil(TempList);
    end;
  end;

  CGSelect := CGSelect + CallCenterGroupFilter + ' order by group_code';
  CenterGroups.SQL.Text := CGSelect;
  GetListFromDataSet(CenterGroups, List, 'group_code', 'center_group_id', IncludeBlank);
end;

function TDM.BulkTicketDueDatesToServer(TicketStr: string;    // QM-959 EB
  NewDueDate: TDateTime): string;
var
  ResultMsg: string;
begin
  {An incoming ticket list here is still checked in the status groups for the
  EditTicketDueDate-  <client> and whether the status allows the change.
  Because it is a bulk action, information about what has been processed will be sent
  down.  Those with distinction down to the call center are not processed through the
  bulk functionality}
  callingServiceName('BulkChangeTicketDueDates');
  Result := Engine.Service.BulkChangeTicketDueDates(Engine.SecurityInfo, TicketStr, NewDueDate);

end;

procedure TDM.CustomerList(List: TStrings; IncludeBlank: Boolean);
const
  SQL = 'select * from customer where active order by customer_name, city, state';
var
  Display: string;
  ID: Integer;
  DataSet: TDataSet;
begin
  Assert(Assigned(List));

  DataSet := Engine.OpenQuery(SQL);
  List.Clear;
  try
    Dataset.First;
    while not Dataset.Eof do begin
      Display := Format('%s - %s (%s) Profit Ctr: %s', [Dataset.FieldByName('customer_name').AsString, Dataset.FieldByName('customer_number').AsString,
        Trim(Dataset.FieldByName('city').AsString + ' ' + Dataset.FieldByName('state').AsString), Dataset.FieldByName('pc_code').AsString]);
      ID := Dataset.FieldByName('customer_id').AsInteger;
      List.AddObject(Display, TObject(ID));
      Dataset.Next;
    end;
  finally
    Dataset.Close;
  end;

  if IncludeBlank then
    List.InsertObject(0, '', TObject(0));
end;

procedure TDM.GetListFromDataSet(Data: TDataSet; List: TStrings;
  const DisplayField, IDField: string; IncludeBlank: Boolean);
var
  Display: string;
  ID: Integer;
  CloseData: Boolean;
begin
  Assert(Assigned(List));
  List.Clear;
  CloseData := not Data.Active;
  Data.Open;
  try
    Data.First;
    while not Data.Eof do begin
      Display := Data.FieldByName(DisplayField).AsString;
      if IDField <> '' then
        ID := Data.FieldByName(IDField).AsInteger
      else
        ID := 0;
      List.AddObject(Display, TObject(ID));
      Data.Next;
    end;
    if IncludeBlank then
      List.InsertObject(0, '', TObject(0));
  finally
    if CloseData then
      Data.Close;
  end;
end;

procedure TDM.SetPasswordExpirationDate(Date: TDateTime);
const
  SQL = 'update users set chg_pwd_date = %s where emp_id = %d';
begin
  Engine.ExecQuery(Format(SQL, [DateToDBISAMDate(Date), EmpID]));
end;

procedure TDM.SetupLocatePlatData(TicketID: Integer);
begin
  Engine.LinkEventsAutoInc(LocatePlatData);
  LocatePlatData.ParamByName('ticket_id').AsInteger := TicketID;
  LocatePlatData.Open;
  Assert(not LocatePlatData.ResultIsLive);
end;

procedure TDM.SaveLocatePlatChanges;

  procedure AssignFieldValue(const FieldName: string);
  var
    Source: TField;
    Dest: TField;
  begin
    Source := LocatePlatData.FieldByName(FieldName);
    Dest := SaveLocatePlat.FieldByName(FieldName);
    if Dest.Value <> Source.Value then begin
      EditDataSet(Dest.DataSet);
      Dest.Value := Source.Value;
    end;
  end;

  procedure PostPlatChanges;
  begin
    if EditingDataSet(SaveLocatePlat) then begin
      FTicketChanged := True;
      PostDataSet(SaveLocatePlat);
    end;
  end;

var
  i: Integer;
  DeltaStatus: string;
  CopyField: string;
  Found: Boolean;
  PlatID: Integer;
  Active: Boolean;
begin
  Assert(LocatePlatData.Active);
  LocatePlatData.First;
  SaveLocatePlat.Open;
  Engine.LinkEventsAutoInc(SaveLocatePlat);
  while not LocatePlatData.Eof do begin
    DeltaStatus := LocatePlatData.FieldByName('DeltaStatus').AsString;
    Active := LocatePlatData.FieldByName('Active').AsBoolean;
    Assert((DeltaStatus = '') or (DeltaStatus = 'I') or (DeltaStatus = 'U'));
    PlatID := LocatePlatData.FieldByName('locate_plat_id').AsInteger;
    Found := SaveLocatePlat.Locate('locate_plat_id', PlatID, []);

    if DeltaStatus = 'I' then begin
      if (not Found) and Active then begin//make sure active, otherwise don't insert duplicate
        SaveLocatePlat.Insert;
        for i := 0 to SaveLocatePlat.FieldCount - 1 do begin
          CopyField := SaveLocatePlat.Fields[i].FieldName;
          if (CopyField <> 'DeltaStatus') then
            AssignFieldValue(CopyField);
        end;
        PostPlatChanges;
        Assert(SaveLocatePlat.FieldByName('DeltaStatus').AsString = 'I');
      end;
    end
    else if DeltaStatus = 'U' then begin
      if not Found then
        raise Exception.Create('Could not find an edited locate plat record in the local DB');
      AssignFieldValue('plat');
      AssignFieldValue('modified_by');
      AssignFieldValue('modified_date');
      AssignFieldValue('active');
      PostPlatChanges;
    end;
    LocatePlatData.Next;
  end;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  DeleteFile('_DebuggerFile.txt');  //qm-265 test
  KillTask('StreetMapProj.exe');  //defensive code in case it still exists
  KillTask('EMDispatchProj.exe');  //QM-11  SR
  CurrentGPSLongitude := 0;
  CurrentGPSLatitude := 0;
  CurrentGPSHDOP := 0;
  CurrentGPSFixTime := 0;
  // VCL magic manages linking to this and destroying it
  TSharedImagesDM.Create(Self);
  SharedDevExStyleData := TSharedDevExStyleData.Create(Self);

  SetupTimeImportFolders;
  TimeFileMonitorDM := TTimeFileMonitorDM.Create(self);
  MyClients:=TStringList.Create;
  MyClients.Delimiter:=',';
  ackedTimeClockIn := false;  //QM-370  sr
end;

procedure TDM.RunRoboCopy(LocatorEmpID:integer);
const
  APP_ROBOCOPY = 'robocopy.exe';
  SEP = ' ';
  DBL_QUOTES='"'; 
var  //qm-436  sr calls Robocopy
  RoBoSource,RoBoDest, RoBoOptions, RoBoFiles, RoBoLogs:string;
  FileName, Params, Folder: string;  //QM-11
  WaitUntilTerminated, WaitUntilIdle, RunMinimized: Boolean;
  ErrorCode: integer;
  appPath:string;
begin
 customer_locator.params.ParamByName('EmpID').Value :=  LocatorEmpID;
 customer_locator.Open;
 if not customer_locator.IsEmpty then
 begin
   RoBoSource:=customer_locator.FieldByName('RoBoSource').AsString;
   RoBoDest:=customer_locator.FieldByName('RoBoDest').AsString;
   RoBoOptions:=customer_locator.FieldByName('RoBoOptions').AsString;
   RoBoFiles:=customer_locator.FieldByName('RoBoFiles').AsString;
   RoBoLogs:=customer_locator.FieldByName('RoBoLogs').AsString;
 end;
 

  ErrorCode:=0;
  WaitUntilTerminated:= false;
  WaitUntilIdle := false;
  RunMinimized:= true;

  appPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)));
    FileName :=APP_ROBOCOPY;
    Folder := appPath;
    Params := DBL_QUOTES+RoBoSource+DBL_QUOTES+Sep+DBL_QUOTES+RoBoDest+DBL_QUOTES+Sep+RoBoOptions;   //qm-436 requires double quotes SR
    if RoBoFiles<>'' then Params:= Params+Sep+DBL_QUOTES+RoBoFiles+DBL_QUOTES;
    if RoBoLogs<>'' then Params:= Params+Sep+DBL_QUOTES+RoBoLogs+DBL_QUOTES;
    
    if (Params<>'') then   //Maintenance protective code
      odMIscUtils.ExecuteProcess(FileName, Params, Folder, WaitUntilTerminated, WaitUntilIdle, RunMinimized, ErrorCode);

  if ErrorCode > 0 then  ShowMessage(SysErrorMessage(GetLastError));
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  MyClients.free; //qm-265
  DM.KillTask('StreetMapProj.exe');
end;

procedure TDM.LocateDataCalcFields(DataSet: TDataSet);
var
  Alert2: string;
begin
  DataSet.FieldByName('locate_plats').AsString := GetLocatePlatStr(DataSet.FieldByName('locate_id').asInteger);

  Alert2 := '';
  if DataSet.FieldByName('alert').AsBoolean or SameText(DataSet.FieldByName('locatealert').AsString, 'A') then
    Alert2 := '!';
  DataSet.FieldByName('alert2').AsString := Alert2;
end;

function TDM.GetLocatePlatStr(const LocateID: Integer): string;
begin
  Assert(LocatePlatData.Active);
  with LocatePlatData do begin
    Filter := 'active and locate_id=' + IntToStr(LocateID);
    Filtered := True;
    Result := GetDelimitedStringFromDataset(LocatePlatData, 'plat');
    Filtered := False;
  end;
end;

procedure TDM.GetLocationMarkStatusList(var List: TStringList);
var
  Data: TDataSet;
begin
  //Ken Funk 2016-01-14
  List.Clear;
  Data := Engine.OpenQuery(
    'select distinct sgi.status from status_group_item sgi ' +
    'inner join status_group sg on sg.sg_id = sgi.sg_id ' +
    'where sg.sg_name = ''Location-MarkStatusList'' and sg.active and sgi.active');
  try
    while not(Data.Eof) do begin
      List.Add(Data.FieldByName('status').AsString);
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

function TDM.GetDelimitedStringFromDataSet(Data: TDataSet; const DisplayField: string; Delimiter: string=','): string;
var
  LocalOpen: Boolean;
begin
  Assert(Assigned(Data));
  LocalOpen := not Data.Active;
  try
    if LocalOpen then
     Data.Open;
    if not Data.IsEmpty then begin
      Data.First;
      Result := Data.FieldByName(DisplayField).AsString;
      Data.Next;
      while not Data.Eof do begin
        Result := Result + Delimiter + Data.FieldByName(DisplayField).AsString;
        Data.Next;
      end;
    end
    else
      Result := '';
  finally
    if LocalOpen then
      Data.Close;
  end;
end;

procedure TDM.GetLocatePlatList(LocateID: Integer; PlatList: TStrings);
begin
  Assert(Assigned(PlatList));
  LocatePlatData.Filter := 'active and locate_id=' + IntToStr(LocateID);
  LocatePlatData.Filtered := True;
  GetListFromDataSet(LocatePlatData, PlatList, 'plat', 'locate_plat_id');
  LocatePlatData.Open;
end;

function TDM.UnitTypeForLocate(const LocateID: Integer; var UnitType: string): Integer;
begin
  UnitType := 'Feet';
  if (not LocateUnitType.Active) or (LocateUnitType.Params[0].Value <> LocateID) then begin
    LocateUnitType.Close;
    LocateUnitType.Params[0].Value := LocateID;
    LocateUnitType.Open;
  end;
  if LocateUnitType.IsEmpty then
    Result := NoConversionID
  else if LocateUnitType.FieldByName('unit_conversion_id').IsNull then
    Result := NoConversionID
  else begin
    Result := LocateUnitType.FieldByName('unit_conversion_id').AsInteger;
    UnitType := LocateUnitType.FieldByName('unit_type').asString;
  end;
end;

procedure TDM.AreaList(Areas: TStrings);
begin
  GetListFromDataSet(Area, Areas, 'area_name', 'area_id', True);
end;

procedure TDM.CustomerInvoiceList(CustomerInvoices: TStrings; const CustomerID: Integer);
const
  Select = 'select distinct which_invoice from billing_output_config where which_invoice is not null';
begin
  if CustomerID > 0 then
    BillingOutputConfig.SQL.Text := Select + ' and customer_id=' + IntToStr(CustomerID)
  else
    BillingOutputConfig.SQL.Text := Select;
  GetListFromDataSet(BillingOutputConfig, CustomerInvoices, 'which_invoice', '', True);
end;


class function TDM.GetRegisteredFormClass(const FormID: string): TEmbeddableFormClass;
var
  Index: Integer;
begin
  Assert(Assigned(PrivateRegisteredForms));
  Result := nil;
  Index := PrivateRegisteredForms.IndexOf(FormID);
  if Index > -1 then
    Result := TEmbeddableFormClass(PrivateRegisteredForms.Objects[Index]);
end;

class procedure TDM.RegisterForm(const FormID: string; FormClass: TEmbeddableFormClass);
begin
  if not Assigned(PrivateRegisteredForms) then
    PrivateRegisteredForms := TStringList.Create;
  PrivateRegisteredForms.AddObject(FormID, TObject(FormClass));
end;

procedure TDM.ReloadReferenceData;  //QM-933 EB SortBy - Adding a way to refresh Reference data
begin
  CloseQueries;
  Engine.EmptyAndLoadRefTables;
  UQState.LastLocalSyncTime := 0;
  UQState.SaveToIni;
  ZeroCounts;
end;

procedure TDM.ClearSyncStatusDate;
begin
  CloseQueries;
  Engine.ResetSyncStatusDate;
  UQState.LastLocalSyncTime := 0;
  UQState.SaveToIni;
  ZeroCounts;
end;

procedure TDM.ReloadTicketData(IncludeSync: Boolean = False);  //QM-780 EB Reload Ticket Data
begin
  CloseQueries;
  Engine.EmptyAndLoadTicketTables(IncludeSync);     //QM-986 EB Reload Ticket Data
  UQState.LastTicketDataClean := Now;
  UQState.SaveToIni;
  ZeroCounts;
end;

procedure TDM.SetupQMService;
begin
  SetUpDatabase('data');
  Engine := TLocalCache.CreateWithDatabase(Database1);
  Engine.DeveloperMode := UQState.DeveloperMode;
  Engine.Server := CreateRemoteServer(Engine.AddToMyLog, UQState.ServerURL);
  Engine.Service := CreateQMService(UQState.ServerURL);
  Engine.SaveSyncXML := UQState.SaveSyncXML;
end;

procedure TDM.SetupQMLogic2Server;

  function BuildQMLogic2URL(const QMLogicURL: string): string;
  var
    Prot, User, Pass, Host, Path: string;
    Port, Scheme: Integer;
  begin
    ParseURL(QMLogicURL, Prot, User, Pass, Host, Path, Port, Scheme);
    Port := 5041; // TODO: Get this from config data

    // TODO: Need a better Base URL?
    Result := Prot + '://' + Host + ':' + IntToStr(Port);
  end;

var
  BaseURL: string;
begin
  BaseURL := UQState.QMLogic2URL;
  if IsEmpty(BaseURL) then
    BaseURL := BuildQMLogic2URL(UQState.ServerURL);

  FreeAndNil(QMLogic2Server);
  FreeAndNil(QMTransport);
  QMTransport := THttpTransport.Create(BaseURL, UQState.UserName, UQState.APIKey);
  QMLogic2Server := TAPIServerInterface.Create(QMTransport);
  QMLogic2Server.AppVersion := AppVersion;
end;

procedure TDM.AckMessages(const RuleID: Integer=-1);
begin
  if RuleID > 0 then
    MarkMessagesAcked.Params.ParamByName('rule_id').Value := RuleID
  else
    MarkMessagesAcked.Params.ParamByName('rule_id').Clear;
  MarkMessagesAcked.ExecSQL;
end;

procedure TDM.SendTextToLocator(const SendToEmpID: Integer;TextToSend:String);
begin //QM-9 sr  use this pattern for Sending texts
  Engine.Service.SendTextViaEmail(Engine.SecurityInfo, SendToEmpID, TextToSend);
end;

procedure TDM.updatePhoneNo(const EmpID: Integer;phoneNumber:String);
begin //qm-388  sr
  Engine.Service.updatePhoneNo(Engine.SecurityInfo, EmpID, phoneNumber);
end;

function TDM.SendMessage(const MessageNumber, Destination, Subject, Body: string;
  ShowDate, ExpirationDate: TDateTime; SendToEmpID: Integer; Directed: boolean = False): string;
var
  MessageData: MessageChange;
  ChangeList: NVPairList;
  SendToEmpList: TStringList;
  I: Integer;
  Cursor: IInterface;
begin
  Result := '';
  Cursor := ShowHourglass;
  MessageData := MessageChange.Create;
  try
    CallingServiceName('SaveMessage');
    ChangeList := MessageData.MessageParams.Create;
    AddParam(ChangeList, 'message_number', MessageNumber);
    AddParam(ChangeList, 'destination', Destination);
    AddParam(ChangeList, 'show_date', ShowDate);
    AddParam(ChangeList, 'subject', Subject);
    AddParam(ChangeList, 'body', Body);
    if ExpirationDate > 0 then
      AddParam(ChangeList, 'expiration_date', ExpirationDate)
    else
      AddParam(ChangeList, 'expiration_date', '2500-12-31T00:00:00.000');
    SendToEmpList := TStringList.Create;
    try
      if not Directed then begin   //Send to Employee List
        EmployeeDM.EmployeeList(SendToEmpList, SendToEmpId);
        for I := 0 to SendToEmpList.Count - 1 do
          MessageData.MessageDestEmpIds.Add(Integer(SendToEmpList.Objects[I]));
      end
      else //Send this to a single Employee
        SendToEmpList.Add(EmployeeDM.GetEmployeeShortName(SendToEmpId));
        MessageData.MessageDestEmpIds.Add(SendToEmpId);
    finally
      FreeAndNil(SendToEmpList);
    end;
    Assert(MessageData.MessageDestEmpIds.Count > 0, 'No employees in the message recipient list.');
    Result := Engine.Service.SaveMessage(Engine.SecurityInfo, MessageData);
  finally
    FreeAndNil(MessageData);
  end;
end;

procedure TDM.SetMessageActive(const MessageID: Integer; const IsActive: Boolean);
begin
  Engine.Service.SetMessageActive(Engine.SecurityInfo, MessageID, IsActive);
end;




function TDM.GetSavedROTicketInfo(ParentTicketID: integer): boolean;    //QM-697 EB
begin
  QryTicketInfoRouteOrder.Close;
  QryTicketInfoRouteOrder.ParamByName('ticket_id').AsInteger := ParentTicketID;
  QryTicketInfoRouteOrder.Open;
  Result := (QryTicketInfoRouteOrder.RecordCount > 0)
end;

function TDM.GetSearchResultsRecordCount(XMLString: string): Integer;

  function GetIntAttr(Node: IXMLDOMNode; Attr: string): Integer;
  var
    Attribute: IXMLDOMNode;
  begin
    Result := -1;
    Attribute := Node.Attributes.GetNamedItem(Attr);
    if Assigned(Attribute) then
      Result := StrToIntDef(Attribute.Text, -1);
  end;

var
  Node: IXmlDomNode;
  Dom: IXmlDomDocument;
begin
  Dom := ParseXML(XmlString);
  Node := Dom.SelectSingleNode('//rows');
  if Assigned(Node) then
    Result := GetIntAttr(Node, 'ActualRows')
  else
    Result := 0;
end;

procedure TDM.GetOngoingStatuses(LocateID: Integer; Statuses: TStrings);
var
  CallCenter: string;
  ClientCode: string;
  ClientID: Integer;
  StatusGroupName: string;
  MarkableStatusCodes: TStringArray;
begin
  Assert(LocateID <> 0);
  GetClientDataForLocateID(LocateID, ClientCode, ClientID);
  CallCenter := GetCallCenterForClientID(ClientID);
  StatusGroupName := 'Ongoing-'+ CallCenter +'-'+ ClientCode;
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  StatusGroupName := 'Ongoing-'+ CallCenter;
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  StatusGroupName := 'OngoingStatusList';
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  SetLength(MarkableStatusCodes, 1);
  MarkableStatusCodes := GetMarkableUnitStatusCodes;
  if MarkableStatusCodes[0] <> '' then begin
    Statuses.CommaText := StringArrayToDelimitedString(MarkableStatusCodes, '', ',');
    Exit;
  end;
  Statuses.CommaText := 'C,M,N,XA';
end;

function TDM.GetEarlierRouteOrderArray(pTicketID: Integer;    //QM-697 EB Resurrect RO
  CurRouteOrder: Double; var Count: integer): TRouteArray;
var
 i: integer;
 SkipPriority: integer;
begin
  {QMANTWO-775 EB We will only record the first 5 records' data that has been skipped}
  {init}
  for i := 0 to maxRORec do begin
    Result[i].Found := False;
    Result[i].OrderNum  := 0;
    Result[i].TicketID := 0;
    Result[i].TicketNum := '';
    Result[i].DueDate:= Now;
  end;
  SkipPriority := DM.GetTicketPriorityIDByCode(TicketPrioritySkipped);  //QM-720 Route Order Skip Fix EB
  RouteOrderQuery.Close;
  RouteOrderQuery.ParamByName('emp_id').AsInteger := EmpID;
  RouteOrderQuery.ParamByName('last_sync').AsDateTime := UQState.LastLocalSyncTime;
  RouteOrderQuery.ParamByName('route_order').AsFloat := CurRouteOrder;
  RouteOrderQuery.ParamByName('workpriority').AsInteger := SkipPriority;
  RouteOrderQuery.Open;
  Count := RouteOrderQuery.RecordCount;

  RouteOrderQuery.First; i:= 0;

  for i := 0 to maxRORec do begin
    if (not RouteOrderQuery.Eof) and ((RouteOrderQuery.FieldByName('ticket_id').AsInteger) <> pTicketID) then begin
      Result[i].Found := True;
      Result[i].OrderNum := RouteOrderQuery.FieldByName('route_order').AsFloat;
      Result[i].TicketID := RouteOrderQuery.FieldByName('ticket_id').AsInteger;
      Result[i].TicketNum := RouteOrderQuery.FieldByName('ticket_number').AsString;
      Result[i].DueDate := RouteOrderQuery.FieldByName('due_date').AsDateTime;
      RouteOrderQuery.Next;
    end;
  end;
end;



procedure TDM.GetEditDueDateStatuses(LocateID: Integer; Statuses: TStrings);     //QM-216 EB
var
  CallCenter: string;
  ClientCode: string;
  ClientID: Integer;
  StatusGroupName: string;
const
  DD = 'EditTicketDueDate-';
begin
  Assert(LocateID <> 0);
  GetClientDataForLocateID(LocateID, ClientCode, ClientID);
  CallCenter := GetCallCenterForClientID(ClientID);
  {Look for EditDueDate-CallCenter-Client}
  StatusGroupName := DD + CallCenter + '-' + ClientCode;
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  {Look for EditDueDate-CallCenter}
  StatusGroupName := DD + CallCenter;
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  //StatusGroupName := DD + Do we want to add the state?

end;

function TDM.IsDueDateStatus(Status: string; LocateID: Integer): boolean;  //QM-216 EB
var
  DDList: TStringList;
  i : integer;
begin
  Result := False;
  DDList := TStringList.Create;
  try
    GetEditDueDateStatuses(LocateID, DDList);
    for i := 0 to DDList.Count - 1 do
      if Status = DDList[i] then begin
        Result := True;
        Break;
      end;
       
  finally
    FreeAndNil(DDLIst);
  end;
end;

procedure TDM.GetQuickCloseStatuses(CallCenter: string; Statuses: TStrings);
var
  StatusGroupName: string;
begin
  Assert(Trim(CallCenter) <> '');
  StatusGroupName := 'QuickClose-' + CallCenter;
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  StatusGroupName := 'QuickCloseStatusList';
  if GetStatusGroup(StatusGroupName, Statuses) then
    Exit;
  Statuses.Text := '';
end;

function TDM.GetStatusGroup(const GroupName: string; Statuses: TStrings): Boolean;
begin
  Assert(Trim(GroupName) <> '');
  Assert(Assigned(Statuses));
  StatusGroup.ParamByName('sg_name').AsString := GroupName;
  StatusGroup.Open;
  try
    Statuses.Clear;
    Result := not StatusGroup.IsEmpty;
    while not StatusGroup.Eof do begin
      Statuses.Add(StatusGroup.FieldByName('Status').AsString);
      StatusGroup.Next;
    end;
  finally
    StatusGroup.Close;
  end;
end;



procedure TDM.ApplyEmployeeStatusLimit(CustomerStatuses: TStrings);
var
  MergedStatusList: TOdStringList;
begin
  if not Assigned(FMyAllowedStatusList) then
    RefreshMyAllowedStatusCodes;

  if FMyAllowedStatusList.Count = 0 then begin
    CustomerStatuses.Clear;
    Exit;
  end;

  MergedStatusList := TOdStringList.Create;
  try
    MergedStatusList.Assign(CustomerStatuses);
    MergedStatusList.Intersect(FMyAllowedStatusList);
    CustomerStatuses.Assign(MergedStatusList);
  finally
    FreeAndNil(MergedStatusList);
  end;
end;

//QM-1042 Add Locate Issue and QM-1037 Bigfoot   NEW
procedure TDM.RefreshLocateData;
begin
  DM.LocateData.Close;
  DM.OpenLocateData;
end;

procedure TDM.RefreshMyAllowedStatusCodes;
const
  MyAllowedStatuses = 'select distinct sgi.status from status_group_item sgi ' +
    'inner join status_group sg on sg.sg_id = sgi.sg_id ' +
    'where sg.sg_name in (%s) and sg.active and sgi.active';
var
  AllowedGroupNames: string;
  StatusData: TDataSet;
begin
  if not Assigned(FMyAllowedStatusList) then
    FMyAllowedStatusList := TOdStringList.Create;

  Assert(Assigned(FMyAllowedStatusList), 'MyAllowedStatusList is not assigned');
  AllowedGroupNames := '';
  FMyAllowedStatusList.Clear;

  
  if PermissionsDM.CanI(RightTicketsAllowStatusCodes) then
    AllowedGroupNames := QuotedStr('UnrestrictedStatusList');
  if PermissionsDM.CanI(RightTicketsAllowRestrictedStatusCodes) then begin
    if NotEmpty(AllowedGroupNames) then
      AllowedGroupNames := AllowedGroupNames + ',' + QuotedStr('RestrictedStatusList')
    else
      AllowedGroupNames := QuotedStr('RestrictedStatusList');
  end;
  if IsEmpty(AllowedGroupNames) then
    Exit;

  StatusData := Engine.OpenQuery(Format(MyAllowedStatuses, [AllowedGroupNames]));
  try
    GetListFromDataSet(StatusData, FMyAllowedStatusList, 'status', '');
  finally
    FreeAndNil(StatusData);
  end;
end;

function TDM.ValidStatusForEmployee(const Status: string): Boolean;
begin
  Assert(Assigned(FMyAllowedStatusList), 'MyAllowedStatusList is unassigned');

  Result := FMyAllowedStatusList.IndexOf(Status) > -1;
end;

function TDM.MatchesStatusGroup(const StatusGroupName, Status: string): Boolean;
var
  Statuses: TStringList;
begin
  Result := False;
  Statuses := TStringList.Create;
  try
    if GetStatusGroup(StatusGroupName, Statuses) then begin
      if Statuses.IndexOf(Status) > -1 then
        Result := True;
    end;
  finally
    FreeAndNil(Statuses);
  end;
end;



function TDM.GetCenterGroup(const GroupName: string; Centers: TStrings): Boolean;
begin
  Assert(Trim(GroupName) <> '');
  Assert(Assigned(Centers));
  CenterGroup.ParamByName('group_code').AsString := GroupName;
  CenterGroup.Open;
  try
    Centers.Clear;
    Result := not CenterGroup.IsEmpty;
    while not CenterGroup.Eof do begin
      Centers.Add(CenterGroup.FieldByName('call_center').AsString);
      CenterGroup.Next;
    end;
  finally
    CenterGroup.Close;
  end;
end;

function TDM.MatchesCenterGroup(const GroupName, CallCenter: string): Boolean;
var
  Centers: TStringList;
begin
  Assert(NotEmpty(GroupName));
  Assert(NotEmpty(CallCenter));
  Result := False;
  Centers := TStringList.Create;
  try
    if GetCenterGroup(GroupName, Centers) then begin
      if Centers.IndexOf(CallCenter) > -1 then
        Result := True;
    end;
  finally
    FreeAndNil(Centers)
  end;
end;



procedure TDM.CloseDataSetsForRefresh;
{ Close any datasets here that are safe to close during a sync, so they
  automatically get any updated data next time they are opened }
begin
  ValidStatusQuery.Close;
end;

procedure TDM.PopulateScheduleSortByList(List: TStrings);
begin
  List.Clear;
  List.Add('Schedule date');
  List.Add('Locator');
end;

function TDM.ServerInClient: Boolean;
begin
  Result := ServiceLink.ServerInClient;
end;


function TDM.PlusWhiteListed(pClient_Code: string): boolean;   //QM-585 EB PLUS Whitelist
begin
  Result := False;

  qryPlusWhiteList.ParamByName('client_code').AsString := pClient_Code;
  qryPlusWhiteList.Open;
  if (qryPlusWhiteList.RecordCount > 0) then
    Result := qryPlusWhiteList.FieldByName('can_status').AsBoolean
  else
    Result := TRUE;
  qryPlusWhiteList.Close;
end;



procedure TDM.PopulateActiveStatusList(List: TStrings);
var
  i: Integer;
begin
  Assert(List <> nil);
  List.Clear;
  i := List.Add('Active Only');
  List.Objects[i] := Pointer(StatusActive);
  i := List.Add('Inactive Only');
  List.Objects[i] := Pointer(StatusInactive);
  i := List.Add('All');
  List.Objects[i] := Pointer(StatusAll);
end;



function TDM.GetOngoingContactName(TicketID: Integer; var First, Last: string): Boolean;
begin
  Result := TContactNameDialog.GetOngoingContactName(TicketID, First, Last);
end;

//Don't need this do I?
//function TDM.GetStatusQuestions(TicketID: Integer): Boolean;
//begin
//  Result := TLocStatusQuestions.GetStatusQuestions(TicketID, ClientID, OldStatus, NewStatus);
//end;

procedure TDM.AddTicketInfo(TicketID: Integer; const InfoType, Data: string);
begin
  Engine.LinkEventsAutoInc(SaveTicketInfo);
  SaveTicketInfo.Open;
  SaveTicketInfo.Insert;
  SaveTicketInfo.FieldByName('ticket_id').AsInteger := TicketID; // This may be a negative number
  SaveTicketInfo.FieldByName('info_type').AsString := InfoType;
  SaveTicketInfo.FieldByName('info').AsString := Data;
  SaveTicketInfo.FieldByName('added_by').AsInteger := EmpID;
  SaveTicketInfo.FieldByName('modified_date').AsDateTime := RoundSQLServerTimeMS(Now);
  SaveTicketInfo.Post;
end;


// The result is in Seconds
function TDM.GetSyncReminderInterval: Integer;
begin
  Result := 0;
  if PermissionsDM.CanI(RightSyncReminderInterval) then
    Result := StrToIntDef(PermissionsDM.GetLimitation(RightSyncReminderInterval), 0) * 60;
end;

procedure TDM.ClientListForProfitCenter(const ProfitCenter: string; List: TStrings);
const
  SQL = 'select call_center, client_id, oc_code from client ' +
    'inner join office on office.office_id = client.office_id ' +
    'where office.profit_center = %s';
var
  Clients: TDataSet;
  ClientCode: string;
  ClientID: Integer;
  CallCenter: string;
begin
  Assert(Assigned(List));
  List.BeginUpdate;
  try
    List.Clear;
    Clients := Engine.OpenQuery(Format(SQL, [QuotedStr(ProfitCenter)]));
    try
      Clients.First;
      while not Clients.Eof do begin
        ClientCode := Clients.FieldByName('oc_code').AsString;
        CallCenter := Clients.FieldByName('call_center').AsString;
        if Trim(ClientCode) <> '' then begin
          ClientID := Clients.FieldByName('client_id').AsInteger;
          List.AddObject(CallCenter + ' - ' + ClientCode, TObject(ClientID))
        end;
        Clients.Next;
      end;
    finally
      FreeAndNil(Clients);
    end;
  finally
    List.EndUpdate;
  end;
end;

function TDM.IsManualTicket(TicketID: Integer): Boolean;
const
  SQL = 'select status from ticket where ticket_id = %d';
var
  Dataset: TDataset;
begin
  DataSet := Engine.OpenQuery(Format(SQL, [TicketID]));
  try
    Assert(not DataSet.Eof);
    Result := StrContains(TicketStatusManual, DataSet.FieldByName('status').AsString);
    DataSet.Close;
  finally
    FreeAndNil(DataSet);
  end;
end;


function TDM.GetProfitCenterForEmployee(EmployeeID: Integer): string;
const
  SQL = 'select effective_pc from employee where emp_id=';
var
  DataSet: TDataSet;
begin
  Assert(EmployeeID > 0, 'No employee id requested');
  Result := '';
  DataSet := Engine.OpenQuery(SQL + IntToStr(EmployeeID));
  try
    if not DataSet.Eof then
      Result := DataSet.FieldByName('effective_pc').AsString;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.CanAccessProfitCenterCache: Boolean;
begin
  Result := (PermissionsDM.CanI(RightReportScreen) or PermissionsDM.CanI(RightTimesheetsApprove)
    or PermissionsDM.CanI(RightTimesheetsFinalApprove) or PermissionsDM.CanI(RightTimesheetsExport));
end;

function TDM.CanPlaceOnPerDiem: Boolean;
begin
  Result := PermissionsDM.CanI(RightTimesheetsPerDiemEntry);  // QMANTWO-789 sr
end;

function TDM.CanBackgroundUpload: Boolean;
begin
  Result := PermissionsDM.CanI(RightAttachmentsBackgroundUpload);
end;

function TDM.CanViewEmergencies: Boolean; //QM-11 SR
begin
  Result := PermissionsDM.CanI(RightTicketViewEmergencies);
end;

function TDM.FindLocatorByPartialShortName(LocatorName: string; ManagerList: TStrings; EmployeeStatus: Integer; Reset: Boolean = False): Integer;
const
  SQL = 'select short_name, emp_id from employee '+
        'where upper(short_name) LIKE ''%s'' %s %s '+
        'order by short_name';

var
  EmployeeStatusFilter: string;
begin
  Result := 0;
  Reset := Reset or (not LocatorQuery.Active);
  if Reset then begin
    case EmployeeStatus of
      0: EmployeeStatusFilter := ' and active ';
      1: EmployeeStatusFilter := ' and not active ';
    else
      EmployeeStatusFilter := '';
    end;
    LocatorQuery.Close;
    LocatorQuery.SQL.Text := Format(SQL, ['%' + Uppercase(LocatorName) + '%', EmployeeStatusFilter,
      ' and report_to in (' + EmployeeDM.GetManagerIDListUnderManagerID(ManagerList) + ')']);
    LocatorQuery.Open;
  end
  else begin
    LocatorQuery.Next;
    if LocatorQuery.EOF then
      LocatorQuery.First;
  end;
  if LocatorQuery.RecordCount > 0 then
    Result := LocatorQuery.FieldByName('emp_id').AsInteger;
end;

procedure TDM.LogLastPowerOnOffEvents;
var
  PowerOnDT: TDateTime;
  PowerOffDT: TDateTime;
  LastPowerOffDT: TDateTime;
  PowerOnDTForToday: TDateTime;
  PowershellUsed: Boolean;
begin
  PowershellUsed := False;
  if (UQState.UsePowershellTime) or (PermissionsDM.CanI(RightPowershellBootup)) then begin//Can use a permission or INI setting
    PowerOnDT := GetPSBootupTime;
    PowershellUsed := True;
  end  {QM-147 EB Powershell}
  else PowerOnDT := GetLastBootTime; {QM-147 EB Powershell - trace}
  if (PowerOnDT > 0) then begin
    if PowerOnDT > IsoStrToDateTimeDef(UQState.PowerOnLastDateTime) then
    begin
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCBoot, '', PowerOnDT);
      UQState.PowerOnLastDateTime := IsoDateTimeToStr(PowerOnDT);
    end;
    //Retain first time PC was booted today. Ignore subsequent reboots in same day.
      PowerOnDTForToday := IsoStrToDateTimeDef(UQState.PowerOnFirstDateTimeToday, 0);
      if not IsSameDay(PowerOnDTForToday, PowerOnDT) then
        UQState.PowerOnFirstDateTimeToday := IsoDateTimeToStr(PowerOnDT);
  end;

  LastPowerOffDT := IsoStrToDateTimeDef(UQState.PowerOffLastDateTime);
  PowerOffDT := GetLastShutdownTime(LastPowerOffDT);
  if (PowerOffDT > 0) then
  begin
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCShutdown, '', PowerOffDT);
    UQState.PowerOffLastDateTime := IsoDateTimeToStr(PowerOffDT);
  end;
end;

procedure TDM.FixTicketNotes;
const
  UpdateTicketForTicketNotes = 'update ticket set deltastatus = ''U'' where ' +
    'deltastatus is null and ticket_id in (select foreign_id from notes ' +
    'where foreign_type = %d and notes_id < 0 and deltastatus is null)';
  UpdateTicketForLocateNotes = 'update ticket set deltastatus = ''U'' where ' +
    'deltastatus is null and ticket_id in (select ticket_id from locate ' +
    'where locate_id in (select foreign_id from notes where foreign_type = ' +
    '%d and notes_id < 0 and deltastatus is null))';
  UpdateNotes = 'update notes set deltastatus = ''I'' where foreign_type in ' +
    '(%d, %d) and notes_id < 0 and deltastatus is null';
begin
  if not IsEmpty(UQState.NotesFixDateTime) then
    Exit;

  StartTransaction;
  try
    Engine.ExecQuery(Format(UpdateTicketForTicketNotes, [qmftTicket]));
    Engine.ExecQuery(Format(UpdateTicketForLocateNotes, [qmftLocate]));
    Engine.ExecQuery(Format(UpdateNotes, [qmftTicket, qmftLocate]));
    Commit;
  except
    Rollback;
    raise;
  end;
  UQState.NotesFixDateTime := IsoDateTimeToStr(Now);
end;

procedure TDM.ProcessConfigurationData;
const
  SQL = 'select * from configuration_data where name = ''ClientURL''';
var
  DataSet: TDataSet;
  NewURL: string;
begin
  DataSet := Engine.OpenQuery(SQL);
  try
    if (not DataSet.Eof) and (not UQState.DeveloperMode) then begin
      NewURL := DataSet.FieldByName('value').AsString;
      if NotEmpty(NewURL) and (UQState.ServerURL <> NewURL) then
        UQState.NewServerURL := NewUrl; // Used on the next restart
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;


// Unused for now...
function TDM.GetCustomerBillingPeriod(CustomerID: Integer): string;
const
  SQL = 'select period_type from customer where customer_id = %d';

var
  DataSet: TDataSet;
begin
  Assert(CustomerID > 0);
  DataSet := Engine.OpenQuery(Format(SQL, [CustomerID]));
  try
    if DataSet.Eof then
      raise Exception.CreateFmt('Customer ID %d not found', [CustomerID]);
    Result := DataSet.FieldByName('period_type').AsString;
    if IsEmpty(Result) then
      raise Exception.Create('Blank billing period type not allowed');
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.UserHasLimitedHours: Boolean;
begin
  Result := PermissionsDM.CanI(RightRestrictUsageLimitHours);
end;



function TDM.IsStatusPlus(pStatus, pClientCode, pCallCenter: string; var pResultStr: string; var pTempStrList: TStringList): boolean;
var
  i: integer;
begin
  Result := False;  {default}
  pResultStr := '';

  try
    ValidStatusPlusQry.Close;
    ValidStatusPlusQry.ParamByName('status').AsString := pStatus;
    ValidStatusPlusQry.ParamByName('client_code').AsString := pClientCode;
    ValidStatusPlusQry.ParamByName('call_center').AsString := pCallCenter;
    ValidStatusPlusQry.Open;

    If ValidStatusPlusQry.RecordCount > 0 then begin

      pResultStr := ValidStatusPlusQry.FieldByName('statlist').AsString;

      ptempStrList.Clear;
      ptempStrList.Delimiter := ',';
      ptempStrList.StrictDelimiter := True;
      ptempStrList.DelimitedText := ValidStatusPlusQry.FieldByName('statlist').AsString;
      ptempStrList.Add(ValidStatusPlusQry.FieldByName('status').asString);

      pResultStr := '';
      for i := 0 to ptempStrList.Count - 1 do begin
        pResultStr := pResultStr + '''' + trim(ptempStrList[i]) + '''';  //QM-635 Status Plus EB - it would really help if the trim was in the right place!
        if (i < ptempStrList.Count-1) then
          pResultStr := pResultStr + ',';
      end;
      Result := True;
    end;
  Except
    ShowMessage('An Exception occurred retrieving status list.  [IsStatusPlus]')
  end;
end;


function TDM.IsStatusPlusException(pStatus, pClientCode, pCallCenter: string): Boolean;
begin
  Result := False;  {default}

  try
    ValidStatusPlusQry.Close;
    ValidStatusPlusQry.ParamByName('status').AsString := pStatus;
    ValidStatusPlusQry.ParamByName('client_code').AsString := pClientCode;
    ValidStatusPlusQry.ParamByName('call_center').AsString := pCallCenter;
    ValidStatusPlusQry.Open;

    If ValidStatusPlusQry.RecordCount > 0 then
      Result := True
    else
      Result := False;
  finally

  end;
end;

function TDM.UseStatusPlus(pStatus: string; pClientID: integer; pClientCode: string; pCallCenter: string; var pDataSet: TDBISAMQuery): boolean; //QM-635 Status Plus
const
  {This query grabs all the values from statuslist that are pulled via Status_plus}
  cStatusPlusSQL = 'Select s.status, ' +
                   '''False'' as meet_only, ''False'' as TicketTypeExclusion, ' +
                   '''False'' as NotesRequired, ''False'' as PicturesRequired, ' +
                   '%S as client_id, s.mark_type_required, ' +
                   's.default_mark_type, s.status_name ' +
                   'from statuslist s ' +
                   'where (s.status in (%S)) ';

var
  tempstr:string;
  tempStrList: TStringList;
  BackupSQL: string;
  BackupParams: TDBISAMParams;
begin
  Result := False;  {default}
  BackupSQL := '';
  tempStrList := TStringList.Create;
//  ShowMessage('This is starting Status: ' + pStatus);
  try
   try
      If IsStatusPlus(pStatus, pClientCode, pCallCenter, tempStr, tempStrList) then begin
        BackupSQL := pDataSet.SQL.Text;
        BackupParams := pDataSet.Params;

        pDataSet.DisableControls;
        pDataSet.Close;
        pDataSet.SQL.Clear;
        pDataSet.SQL.Text := '';
        pDataSet.SQL.Text := Format(cStatusPlusSQL,[IntToStr(pClientID),tempstr]);

        pDataSet.Open;
 //       ShowMessage('* Record Count: ' + inttoStr(pDataSet.RecordCount) + #13#10 + 'QRY: ' + pDataSet.SQL.Text);
        Result := True;
      end
      else
        Result := False;

    except
      ShowMessage('An Exception occured retrieving statuslist. [UseStatusPlus]');
      Result := False;
    end;
  finally
    FreeAndNil(tempStrList);
    pDataSet.EnableControls;
  end;
end;


function TDM.GetFieldLength(const TableName, FieldName: string): Integer;
var
  Data: TDataSet;
begin
  Data := Engine.CreateTable(TableName);
  try
    Result := Data.FieldByName(FieldName).Size;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDM.VerifyNoteLength(const NoteText: string);
begin
  if Length(NoteText) > MaxNoteLength then
    raise EOdDataEntryError.CreateFmt('Notes can have at most %d characters in them.', [MaxNoteLength]);
end;

function TDM.IsAttachmentDriveLetter(DriveLetter: string): Boolean;
begin
  DriveLetter := GetStringBeforeCharacter(DriveLetter, ':');
  Result := StrContains(DriveLetter, UQState.DriveLettersForAttachment);
end;


function TDM.IsAutomatedNoteCat(NoteText: string;
  var OutCategory: integer): boolean;
    {Note types}  //QM-966 Fix for Picture and Note Required
//  notecatUserEnterd  = 1;
//  notecatHP          = 2;
//  notecatQA          = 3;
//  notecatAccent      = 5;
//  notecatAutoGen     = 6;
//  notecatROSkipped   = 7;
//  notecatAppExternal = 20;
begin
  Result := False;
   {Wayne's External notes (UID < 200}
  if NoteText = NoteExternal then begin
    OutCategory := notecatAppExternal;
    Result := True;
  end
  {User Entered Note - Accented with ! at the beginning of note}
  else if (AnsiPos('!', NoteText) = 1) then begin
    OutCategory := notecatAccent;
    Result := False;
  end
  {High Profile, High Work Priority}
  else if (AnsiPos(NoteWorkPriority + 'High', NoteText) = 1) or
          (AnsiPos(HighProfileNotePrefix, NoteText) = 1)or
          (AnsiPos(XHighProfileNotePrefix, NoteText) = 1) then begin
    OutCategory := notecatHP;
    Result := True;
  end
  {Auto - Work Priority, Due Date, Area Change, Status As Assigned}
  else if (AnsiPos(NoteWorkPriority, NoteText) = 1) or
          (AnsiPos(NoteModDueDateTxt, NoteText) = 1) or
          (AnsiPos(NoteAreaChanged, NoteText) = 1) or
          (AnsiPos(ROPrefix, NoteText) = 1) or
          (AnsiPos(NoteStatusedasAssigned, NoteText) > 0) or
          (AnsiPos(NoteChangeCodes[1], NoteText) = 1) or
          (AnsiPos(NoteCHangeCodes[2], NoteText) = 1) or
          (AnsiPos(NoteChangeCodes[3], NoteText) = 1) or
          (AnsiPos(NoteROSkipped, NoteText) = 1)or
          (AnsiPos(NoteROSkippedOld, NoteText) = 1) or
          (AnsiPos(NoteModDueDateTxt, NoteText) > 0) or
          (AnsiPos(BulkModDueDateTxt2, NoteText) > 0) then begin   //QM-959 EB Bulk Due Date
    OutCategory := notecatAutoGen;
    Result := True;
  end
  {Route Order Skipped}
  else if (AnsiPos(NoteROSkipped, NoteText) > 0) then  begin
    OutCategory := notecatROSkipped;
    Result := True;
  end
  {Question and Answer Notes}
  else if (AnsiPos(QANotePrefix, NoteText) = 1) then begin
    OutCategory := notecatQA;
    Result := True;
  end
  {User entered text}
  else begin
    OutCategory := notecatUserEnterd;
    Result := False;
  end;

end;

function TDM.IsAutomatedNoteColor(NoteText: string; var OutColorText,
  OutColorHighLight: TColor): Boolean;
var
  lCat: integer;
begin
  {Used for color coding the notes andfiltering out automated notes when evaluating required notes.}
  Result := False;
  OutColorHighLight := clWhite;

  Result := IsAutomatedNoteCat(NoteText, lCat);

  //QM-933 EB All colorcoding added below
  {Wayne's External notes (UID < 200}
  case lCat of
    notecatUserEnterd: begin
          OutColorText := clNavy;
          OutColorHighLight := clWhite;
    end;
    notecatHP: begin
          OutColorText := clMaroon;
          OutColorHighLight := clWebMistyRose;
    end;
    notecatQA: begin
          OutColorText := clWebDarkgreen;
          OutColorHighLight := clWhite;
    end;
    notecatAccent: begin
          OutColorText := clMaroon;
          OutColorHighlight := clWebMistyRose;
    end;
    notecatAutoGen: begin
          OutColorText := clWebSaddleBrown;
          OutColorHighLight := clWhite;
    end;
    notecatROSkipped: begin
          OutColorText := clWebSlateGray;
          OutColorHighlight := clWhite;
    end;
    notecatAppExternal: begin
          OutColorText := clWebSaddleBrown;
    end;
  end;

end;

function TDM.IsTicketAssignedToLocator(const TicketID, LocatorID: Integer): Boolean;
const
  Select = 'select count(*) N from locate where ticket_id = %d and locator_id = %d and active';
begin
  Result := Engine.RunSQLReturnN(Format(Select, [TicketID, LocatorID])) > 0;
end;



function TDM.TicketNeedsArrivalDate(const TicketID: Integer): Boolean;
const
  RequiredByCallCenter = 'select count(*) N from ticket inner join call_center ' +
    'on ticket_format=cc_code where coalesce(track_arrivals,False)=True and ticket_id=';
  CountTicketArrivals = 'select count(*) N from jobsite_arrival where active = true and ticket_id=';
begin
  Result := TicketIsOpen and (Engine.RunSQLReturnN(RequiredByCallCenter + IntToStr(TicketID)) = 1);
  if Result then begin
    if JobsiteArrivalData.Active then // If the user has viewed the history tab for the current ticket
      Result := not HasActiveRecords(JobsiteArrivalData)
    else
      Result := Engine.RunSQLReturnN(CountTicketArrivals + IntToStr(TicketID)) = 0;
  end;
end;

// The arrival can only be created when viewing the relevant ticket.  This adds to the
// JobsiteArrivalData table that holds changes until they are finalized (Click "Done", etc)
procedure TDM.CreateArrival(const TicketOrDamageID: Integer; const EntryMethod: string; const EmpID: Integer; const When: TDateTime; const ArrivalType: TArrivalType; const LocationStatus: String);
var
  ArrivalDateTime: TDateTime;
begin
  if not JobsiteArrivalData.Active then
    OpenJobsiteArrivalData(TicketOrDamageID, ArrivalType);

  Engine.LinkEventsAutoInc(JobsiteArrivalData);
  JobsiteArrivalData.Append;
  if ArrivalType = atDamage then begin
    JobsiteArrivalData.FieldByName('damage_id').AsInteger := TicketOrDamageID;
    JobsiteArrivalData.FieldByName('location_status').AsString := LocationStatus;
  end
  else
    JobsiteArrivalData.FieldByName('ticket_id').AsInteger := TicketOrDamageID;
  JobsiteArrivalData.FieldByName('emp_id').AsInteger := EmpID;
  ArrivalDateTime := RoundSQLServerTimeMS(When);
  JobsiteArrivalData.FieldByName('arrival_date').AsDateTime := ArrivalDateTime;
  JobsiteArrivalData.FieldByName('entry_method').AsString := EntryMethod;
  JobsiteArrivalData.FieldByName('active').AsBoolean := True;
  JobsiteArrivalData.FieldByName('added_by').AsInteger := Self.EmpID;
  JobsiteArrivalData.Post;
  if ArrivalType = atDamage then
    LDamageDM.SetDamageModified
  else begin
    SetTicketModified;
    SetFirstTaskForTomorrowPerformed(TicketOrDamageID, ArrivalDateTime);
  end;
end;

procedure TDM.DeleteJobsiteArrival;
begin
  Engine.LinkEventsAutoInc(JobsiteArrivalData);

  if not JobsiteArrivalData.FieldByName('active').AsBoolean then
    raise EOdDataEntryError.Create('This arrival is already inactive.');
  JobsiteArrivalData.Edit;
  JobsiteArrivalData.FieldByName('active').AsBoolean := False;
  JobsiteArrivalData.FieldByName('deleted_date').AsDateTime := RoundSQLServerTimeMS(Now);
  JobsiteArrivalData.FieldByName('deleted_by').AsInteger := EmpID;
  JobsiteArrivalData.Post;
  SetTicketModified;
end;

procedure TDM.DeleteNote(CachedNotesData: TDataSet);     //QM-188 EB
var
  ReplacementTxt: string;
  EmpNum: string;
  CurNoteType: integer;
  PrivateSubType: integer;
  NoteToDelete: Integer;
begin

  EmpNum := EmployeeDM.GetEmployeeNumber(DM.UQState.EmpID);
  ReplacementTxt:= NoteChangeCodes[1] + ' ' + //'NOTE DELETED by ' +
                    DM.UQState.DisplayName +
                    '(' + EmpNum + ') ' +
                    'on ' + DateToStr(Now);

  try
   EditDataSet(CachedNOtesData);
   NoteToDelete := CachedNotesData.FieldByName('notes_id').AsInteger; //Deactivates it
   if NoteToDelete < 1 then begin
     CachedNotesData.Delete;
     Exit;
   end;

    {What kind of Note is it}
    CurNoteType := CachedNotesData.FieldByName('foreign_type').AsInteger;

      CachedNotesData.FieldByName('note').AsString := ReplacementTxt;
      CachedNotesData.FieldByName('on_cache').asBoolean := True;
//      CachedNotesData.FieldByName('active').AsBoolean := False;
      CachedNOtesData.FieldByName('DeltaStatus').AsString := 'D';
      case CurNoteType of
        qmftTicket: PrivateSubType :=PrivateTicketSubType;
        qmftLocate: PrivateSubType := PrivateLocateSubType;
        qmftDamage: PrivateSubType := PrivateDamageSubType;
        qmftWorkOrder: PrivateSubType := PrivateWorkOrderSubType;
      end;
      CachedNotesData.FieldByName('sub_type').AsInteger := PrivateSubType;
      PostDataSet(CachedNotesData);
//    end;

  except
    CachedNotesData.Cancel;
    raise;
  end;
end;

procedure TDM.ChangeNoteToPrivate(CachedNotesData: TDataSet);//QM-188 EB
var
  ReplacementTxt: string;
  EmpNum: string;
  CurNoteType: integer;
  PrivateSubType: integer;
  NoteToChange: Integer;
  NewNote: Boolean;
  subtype: integer;
begin
  subtype := CachedNotesData.FieldByName('sub_type').AsInteger;
  if (AnsiPos(NoteChangeCodes[2], CachedNotesData.FieldByName('note').AsString) > 0)
      or ( subtype = PrivateTicketSubType)
      or (subtype = PrivateLocateSubType)
      or (subtype = PrivateDamageSubType)
      or (subtype = PrivateWorkOrderSubType)  then begin
    MessageDlg('Note has already been restricted to Private.', mtError, [mbOK], 0);
    Exit;
  end;

  EmpNum := EmployeeDM.GetEmployeeNumber(DM.UQState.EmpID);
  ReplacementTxt:= NoteChangeCodes[2] + ' ' +  {Private}
                    DM.UQState.DisplayName +
                    '(' + EmpNum + ') ' +
                    'on ' + DateToStr(Now);

  try
   EditDataSet(CachedNOtesData);
   NoteToChange := CachedNotesData.FieldByName('notes_id').AsInteger; //Deactivates it



   CurNoteType := CachedNotesData.FieldByName('foreign_type').AsInteger;
   NewNote := CachedNotesData.FieldByName('on_cache').AsBoolean;
   case CurNoteType of
     qmftTicket: PrivateSubType :=PrivateTicketSubType;
     qmftLocate: PrivateSubType := PrivateLocateSubType;
     qmftDamage: PrivateSubType := PrivateDamageSubType;
     qmftWorkOrder: PrivateSubType := PrivateWorkOrderSubType;
   end;

   if NoteToChange < 1 then begin
     CachedNotesData.FieldByName('sub_type').AsInteger := PrivateSubType;
     PostDataSet(CachedNotesData);
     Exit;
   end;

   CachedNotesData.FieldByName('note').AsString := ReplacementTxt + #13#10 + CachedNotesData.FieldByName('note').AsString;;
   CachedNotesData.FieldByName('on_cache').asBoolean := True;
   if not NewNote then CachedNotesData.FieldByName('DeltaStatus').AsString := 'U';
   CachedNotesData.FieldByName('sub_type').AsInteger := PrivateSubType;
   PostDataSet(CachedNotesData);

  except
    CachedNotesData.Cancel;
    raise;
  end;
end;

procedure TDM.ChangeNoteToPublic(CachedNotesData: TDataSet);  //QM-188  EB
var
  ReplacementTxt: string;
  EmpNum: string;
  CurNoteType: integer;
  PublicSubType: integer;
  NoteToChange: Integer;
  NewNote: Boolean;
  subtype: integer;
begin
  subtype := CachedNotesData.FieldByName('sub_type').AsInteger;
  if (AnsiPos(NoteChangeCodes[3], CachedNotesData.FieldByName('note').AsString) > 0)
      or ( subtype = PublicTicketSubType)
      or (subtype = PublicLocateSubType)
      or (subtype = PublicDamageSubType)
      or (subtype = PublicWorkOrderSubType)  then begin
    MessageDlg('Note restriction has already been set to Public.', mtError, [mbOK], 0);
    Exit;
  end;

    if (AnsiPos(NoteChangeCodes[1], CachedNotesData.FieldByName('note').AsString) > 0) then begin
    MessageDlg('Deleted Notes must be private.', mtError, [mbOK], 0);
    Exit;
  end;

  EmpNum := EmployeeDM.GetEmployeeNumber(DM.UQState.EmpID);
  ReplacementTxt:= NoteChangeCodes[3] + ' ' +  {Private}
                    DM.UQState.DisplayName +
                    '(' + EmpNum + ') ' +
                    'on ' + DateToStr(Now);

  try
   EditDataSet(CachedNOtesData);
   NoteToChange := CachedNotesData.FieldByName('notes_id').AsInteger; //Deactivates it

   CurNoteType := CachedNotesData.FieldByName('foreign_type').AsInteger;
   NewNote := CachedNotesData.FieldByName('on_cache').AsBoolean;
   case CurNoteType of
     qmftTicket: PublicSubType :=PublicTicketSubType;
     qmftLocate: PublicSubType := PublicLocateSubType;
     qmftDamage: PublicSubType := PublicDamageSubType;
     qmftWorkOrder: PublicSubType := PublicWorkOrderSubType;
   end;

   if NoteToChange < 1 then begin
     CachedNotesData.FieldByName('sub_type').AsInteger := PublicSubType;
     PostDataSet(CachedNotesData);
     Exit;
   end;

   CachedNotesData.FieldByName('note').AsString := ReplacementTxt + #13#10 + CachedNotesData.FieldByName('note').AsString;;
   CachedNotesData.FieldByName('on_cache').asBoolean := True;
   if not NewNote then CachedNotesData.FieldByName('DeltaStatus').AsString := 'U';
   CachedNotesData.FieldByName('sub_type').AsInteger := PublicSubType;
   PostDataSet(CachedNotesData);

  except
    CachedNotesData.Cancel;
    raise;
  end;
end;

procedure TDM.OpenJobsiteArrivalData(const TicketOrDamageID: Integer; const ArrivalType: TArrivalType);
begin
  if JobsiteArrivalData.Active then
    JobsiteArrivalData.Close;
  if ArrivalType = atDamage then begin
    JobsiteArrivalData.ParamByName('damage_id').AsInteger := TicketOrDamageID;
    JobsiteArrivalData.ParamByName('ticket_id').Clear;
  end
  else begin
    JobsiteArrivalData.ParamByName('ticket_id').AsInteger := TicketOrDamageID;
    JobsiteArrivalData.ParamByName('damage_id').Clear;
  end;
  JobsiteArrivalData.Open;
end;

procedure TDM.SaveArrivalChanges(const ArrivalDataset: TDataset);

  procedure AssignFieldValue(const FieldName: string);
  var
    Source: TField;
    Dest: TField;
  begin
    Source := ArrivalDataset.FieldByName(FieldName);
    Assert(Assigned(Source), 'Field ' + FieldName + ' not found in the JobsiteArrivalData dataset.');
    Dest := SaveJobsiteArrival.FieldByName(FieldName);
    Assert(Assigned(Dest), 'Field ' + FieldName + ' not found in the SaveJobsiteArrival dataset.');
    if Dest.Value <> Source.Value then begin
      EditDataSet(Dest.DataSet);
      Dest.Value := Source.Value;
    end;
  end;

  procedure PostArrivalChanges;
  begin
    if SaveJobsiteArrival.State in dsEditModes then begin
      if (not ArrivalDataset.FieldByName('ticket_id').IsNull) then
        FTicketChanged := True;
      PostDataSet(SaveJobsiteArrival);
      // Mark the temporary change table row as saved, so it doesn't get re-saved in the
      // case of a manual arrival and then clicking Done from ticket management, etc.
      Engine.ClearDeltaStatusInCurrentRow(ArrivalDataset);
      Assert(ArrivalDataset.FieldByName('DeltaStatus').IsNull);
    end;
  end;

var
  i: Integer;
  DeltaStatus: string;
  CopyField: string;
  Found: Boolean;
  ID: Integer;
begin
  if not ArrivalDataset.Active then
    Exit;

  ArrivalDataset.First;
  SaveJobsiteArrival.Open;
  Engine.LinkEventsAutoInc(SaveJobsiteArrival);
  while not ArrivalDataset.Eof do begin
    DeltaStatus := ArrivalDataset.FieldByName('DeltaStatus').AsString;
    Assert((DeltaStatus = '') or (DeltaStatus = 'I') or (DeltaStatus = 'U'));
    ID := ArrivalDataset.FieldByName('arrival_id').AsInteger;
    Assert(ID <> 0);
    Found := SaveJobsiteArrival.Locate('arrival_id', ID, []);
    if DeltaStatus = 'I' then begin
      Assert(ID < 0);
      if not Found then begin
        SaveJobsiteArrival.Insert;
        for i := 0 to SaveJobsiteArrival.FieldCount - 1 do begin
          CopyField := SaveJobsiteArrival.Fields[i].FieldName;
          if (CopyField <> 'DeltaStatus') then
            AssignFieldValue(CopyField);
        end;
        PostArrivalChanges;
        Assert(SaveJobsiteArrival.FieldByName('DeltaStatus').AsString = 'I');
      end;
    end
    else if DeltaStatus = 'U' then begin
      if not Found then
        raise Exception.Create('Could not find an edited arrival record in the local DB');
      // We only allow "deletes" (no other edits)
      AssignFieldValue('deleted_by');
      AssignFieldValue('deleted_date');
      AssignFieldValue('active');
      PostArrivalChanges;
    end;
    ArrivalDataset.Next;
  end;
end;


procedure TDM.GetLocateListForTicket(TicketID: Integer; List: TStrings);
  //Ken Funk 2016-01-14
  //Prevent adding Clients where the locate status is 'DUP'
  //Original SelectClients value
  //SelectClients = 'select l.locate_id, c.client_id, c.oc_code, c.client_name ' +
  //'from client c inner join locate l on c.client_id=l.client_id where ' +
  //'l.ticket_id = %d and l.status <> %s %s %s';
const
  SelectClients = 'select l.locate_id, c.client_id, c.oc_code, c.client_name ' +
    'from client c inner join locate l on c.client_id=l.client_id where ' +
    'l.ticket_id = %d and l.status not in (%s,''DUP'') %s %s';
  LocatorRestriction = 'and l.assigned_to = ';
  ClientListOrder = 'order by c.oc_code';
var
  SQL: string;
  ClientCode: string;
  ClientName: string;
  Data: TDataSet;
begin
  List.Clear;
  if PermissionsDM.IsTicketManager then
    SQL := Format(SelectClients, [TicketID, QuotedStr(NotAClientStatus),
      ClientListOrder, ''])
  else
    SQL := Format(SelectClients, [TicketID, QuotedStr(NotAClientStatus),
      LocatorRestriction + IntToStr(EmpID), ClientListOrder]);
  Data := Engine.OpenQuery(SQL);
  try
    while not Data.EOF do begin
      ClientName := Data.FieldByName('client_name').AsString;
      ClientCode := Data.FieldByName('oc_code').AsString;
      if not StrContains(ClientCode, ClientName) then
        ClientName := ClientName + ' (' + ClientCode + ')';

      List.AddObject(ClientName, TObject(Data.FieldByName('locate_id').AsInteger));
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDM.SetupLocateFacilityData(TicketID: Integer);
begin
  Engine.LinkEventsAutoInc(LocateFacilityData);
  LocateFacilityData.ParamByName('ticket_id').AsInteger := TicketID;
  AddLookupField('client_code', LocateFacilityData, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 22, 'FacilityClientCodeField', 'Client');
  AddLookupField('client_id', LocateFacilityData, 'locate_id',
    LocateLookup, 'locate_id', 'client_id');
  AddLookupField('client_name', LocateFacilityData, 'client_id',
    ClientLookup, 'client_id', 'client_name');
  LocateFacilityData.Open;
  Assert(not LocateFacilityData.ResultIsLive);
end;

procedure TDM.SaveLocateFacilityChanges;

  procedure AssignFieldValue(const FieldName: string);
  var
    Source: TField;
    Dest: TField;
  begin
    Source := LocateFacilityData.FieldByName(FieldName);
    Dest := SaveLocateFacility.FieldByName(FieldName);
    if Dest.Value <> Source.Value then begin
      EditDataSet(Dest.DataSet);
      Dest.Value := Source.Value;
    end;
  end;

  procedure PostFacilityChanges;
  begin
    if EditingDataSet(SaveLocateFacility) then begin
      FTicketChanged := True;
      PostDataSet(SaveLocateFacility);
    end;
  end;

var
  i: Integer;
  DeltaStatus: string;
  CopyField: string;
  Found: Boolean;
  ID: Integer;
begin
  Assert(LocateFacilityData.Active);
  LocateFacilityData.First;
  SaveLocateFacility.Open;
  Engine.LinkEventsAutoInc(SaveLocateFacility);
  while not LocateFacilityData.Eof do begin
    DeltaStatus := LocateFacilityData.FieldByName('DeltaStatus').AsString;
    Assert((DeltaStatus = '') or (DeltaStatus = 'I') or (DeltaStatus = 'U'));
    ID := LocateFacilityData.FieldByName('locate_facility_id').AsInteger;
    Found := SaveLocateFacility.Locate('locate_facility_id', ID, []);
    if DeltaStatus = 'I' then begin
      if not Found then
        SaveLocateFacility.Insert;
      for i := 0 to SaveLocateFacility.FieldCount - 1 do begin
        CopyField := SaveLocateFacility.Fields[i].FieldName;
        if (CopyField <> 'DeltaStatus') then
          AssignFieldValue(CopyField);
      end;
      PostFacilityChanges;
      Assert(SaveLocateFacility.FieldByName('DeltaStatus').AsString = 'I');
    end
    else if DeltaStatus = 'U' then begin
      if not Found then
        raise Exception.Create('Could not find an edited locate facility record in the local DB');
      AssignFieldValue('offset');
      AssignFieldValue('deleted_by');
      AssignFieldValue('deleted_date');
      AssignFieldValue('active');
      AssignFieldValue('DeltaStatus');
      FLocFacilityDeleted := True;
      if EditingDataSet(SaveLocateFacility) then
        SaveLocateFacility.FieldByName('modified_by').Value := EmpID;
      PostFacilityChanges;
    end;
    LocateFacilityData.Next;
  end;
end;

procedure TDM.LocateFacilityDataBeforeEdit(DataSet: TDataSet);
begin
  if not IsLocateFacilityEditable(DataSet.FieldByName('locate_facility_id').Value) then
    raise EOdDataEntryError.Create('You can only change facilities you add.');
end;

function TDM.IsLocateFacilityEditable(LocateFacilityID: Integer): Boolean;
begin
  Assert(LocateFacilityID = LocateFacilityData.FieldByName('locate_facility_id').Value,
    Format('expecting locate_facility_id=%d, but it is %d', [LocateFacilityID,
    LocateFacilityData.FieldByName('locate_facility_id').Value]));

  Result := PermissionsDM.CanI(RightTicketsEditFacilities)
    or (EmpID = LocateFacilityData.FieldByName('added_by').Value);
end;

procedure TDM.LocateLookupBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('client_name', DataSet, 'client_id', ClientLookup, 'client_id', 'client_name');
end;

function TDM.GetLocateFacilityTypeDesc(const LocateID: Integer): string;
const
  Select = 'select coalesce(r2.description, ''Other'') as S from locate l ' +
    'inner join client c on c.client_id=l.client_id ' +
    'left join reference r1 on (r1.type=''utility'' and c.utility_type=r1.code) ' +
    'left join reference r2 on (r2.type=''factype'' and r1.modifier=r2.code) ' +
    'where locate_id=';
begin
  Result := Engine.RunSQLReturnS(Select + IntToStr(LocateID));
end;

procedure TDM.TicketAndLocatesList(TicketNumber: string; List: TStrings);
begin
  //Ken Funk 2016-01-14
  //Prevent adding Clients where the locate status is 'DUP'
  // Formerly used GetListFromDataSet(LocateData, List, 'client_name', 'locate_id');

  Assert(Assigned(List));
  List.Clear;
  LocateData.First;
  while not LocateData.Eof do begin
    if LocateData.FieldByName('status').asstring <> 'DUP' then begin
      //Ken Funk 2016-04-29 JIRA Issue QMANTWO-195 - Should store as Integer value.
      List.AddObject(LocateData.FieldByName('client_name').AsString,
        TObject(LocateData.FieldByName('locate_id').AsInteger));
    end;
    LocateData.Next;
  end;

  // ticket number always go on top of the list
  List.Insert(0, 'Ticket # ' + TicketNumber);
end;



procedure TDM.TicketNotesDataBeforeOpen(DataSet: TDataSet);
begin
  AddCalculatedField('notes_source', Dataset, TStringField, 40);
  AddCalculatedField('Restriction', Dataset, TStringField, 40);
end;

procedure TDM.TicketNotesDataCalcFields(DataSet: TDataSet);
var
  Text: string;
begin
  if Dataset.FieldByName('foreign_type').AsInteger = qmftTicket then
    Text := 'Ticket # ' + Ticket.FieldByName('ticket_number').AsString
  else if Dataset.FieldByName('foreign_type').AsInteger = qmftLocate then begin
    if LocateData.Locate('locate_id', Dataset.FieldByName('foreign_id').AsInteger, []) then
      Text := 'Loc. ' + LocateData.FieldByName('client_name').AsString
  end;

  Dataset.FieldByName('notes_source').AsString := Text;
  if Dataset.FieldByName('sub_type').AsInteger >= 0 then
    Dataset.FieldByName('Restriction').AsString :=
      GetSubTypeDescription(Dataset.FieldByName('sub_type').AsInteger);
end;

{Not sure if this one is still used....didn't see it}
procedure TDM.SaveTicketNoteToCache(const Note: string; ForeignType, ForeignId: Integer);
begin
  Assert(TicketNotesData.Active, 'TicketNotesData must be active/open');
  SaveBaseNoteToCache(Note, ForeignType, ForeignID, TicketNotesData);
end;





procedure TDM.SaveBaseNoteToCache(const Note: string; ForeignType, ForeignID: Integer;
  CachedNotesData: TDataSet; NoteSubType:Integer; SubTypeText: String);
begin
  Assert(CachedNotesData.Active, CachedNotesData.Name + ' must be active/open.');
  CachedNotesData.Insert;
  try
    if NoteSubType = NULL then begin
      NoteSubType := 0;
      SubTypeText := '0';
    end;
    CachedNotesData.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
    CachedNotesData.FieldByName('note').AsString := ReplaceControlChars(Note);
    CachedNotesData.FieldByName('foreign_type').AsInteger := ForeignType;
    CachedNotesData.FieldByName('foreign_id').AsInteger := ForeignID;
    CachedNotesData.FieldByName('on_cache').asBoolean := True;
    CachedNotesData.FieldByName('uid').AsInteger := DM.UQState.UserID;
    CachedNotesData.FieldByName('active').AsBoolean := True;
    CachedNotesData.FieldByName('short_name').AsString := UQState.DisplayName;
    CachedNotesData.FieldByName('sub_type').AsInteger :=  NoteSubType;
    CachedNotesData.FieldByName('Restriction').AsString := SubTypeText;
    CachedNotesData.Post;
  except
    CachedNotesData.Cancel;
    raise;
  end;
end;

procedure TDM.SaveTicketNotesFromCache;
begin
  SaveBaseNotesFromCache('tkd', TicketNotesData);
end;

procedure TDM.SaveBaseNotesFromCache(Source: string; CachedNotesData: TDataSet);
var
  DeltaStatus: string;
  NoteID: integer;
begin
  CachedNotesData.First;
  while not CachedNotesData.Eof do begin
    DeltaStatus := CachedNotesData.FieldByName('DeltaStatus').AsString;
    NoteID := CachedNotesData.FieldByName('notes_id').AsInteger;
    if CachedNotesData.FieldByName('on_cache').AsBoolean then begin
      if (DeltaStatus = 'D') or (DeltaStatus = 'U') then
        ChangeNote(NoteID,
        CachedNotesData.FieldByName('note').AsString, Source,
        CachedNotesData.FieldByName('sub_type').AsInteger,
        CachedNotesData.FieldByName('foreign_type').AsInteger,
        CachedNotesData.FieldByName('foreign_id').AsInteger)

      else SaveNote(CachedNotesData.FieldByName('note').AsString, Source,
        CachedNotesData.FieldByName('sub_type').AsInteger,
        CachedNotesData.FieldByName('foreign_type').AsInteger,
        CachedNotesData.FieldByName('foreign_id').AsInteger);
      case CachedNotesData.FieldByName('foreign_type').AsInteger of
        qmftWorkOrder : SetWorkOrderModified;
        qmftTicket, qmftLocate: SetTicketModified;
      end;
    end;
    CachedNotesData.Next;
  end;
end;

procedure TDM.GetAttachmentList(ForeignType, ForeignID: Integer; List: TStrings; ImagesOnly: Boolean);
var
  Table: TDBISAMTable;
begin
  Table := TDBISAMTable.Create(nil);
  Table.DatabaseName := 'Memory';
  try
    Engine.GetAttachmentList(ForeignType, ForeignID, Table, ImagesOnly);
    Table.First;
    List.Clear;
    while not Table.Eof do begin
      List.AddObject(Table.FieldByName('orig_filename').AsString, TObject(Table.FieldByName('attachment_id').AsInteger));
      Table.Next;
    end;
  finally
    FreeAndNil(Table);
  end;
end;

function TDM.GetTicketID(const TicketNumber: string; const CallCenter: string; const TransmitDateFrom, TransmitDateTo: TDateTime): Integer;
begin
  Result := Engine.GetTicketID(TicketNumber, CallCenter, TransmitDateFrom, TransmitDateTo, 0); //
end;


function TDM.CanIPrintTickets: Boolean;
begin
  Result := PermissionsDM.CanI(RightReportScreen) or PermissionsDM.CanI('ClosedTicketDetail') or PermissionsDM.CanI('TicketDetail');
end;

procedure TDM.SetTicketPriority(TicketID: Integer; TicketPriorityRefID: Integer);
begin
  CallingServiceName('SaveTicketPriority');
  if CheckInternet then
    Engine.Service.SaveTicketPriority(Engine.SecurityInfo, TicketID, RoundSQLServerTimeMS(Now), TicketPriorityRefID);
end;

procedure TDM.SaveNoteToServer(TicketID: Integer; NoteText:string; SubType: integer; EnteredByID:integer;ClientCode:String =''); //qm-747
begin
  CallingServiceName('AddNote');
  Engine.Service.InsertTicketNoteDirect(Engine.SecurityInfo, TicketID, RoundSQLServerTimeMS(Now), NoteText,SubType, ClientCode, EnteredByID );
//      DM.SaveNoteToServer(fEarlierRouteArray[i].TicketID, RouteText, PrivateTicketSubType, dm.EmpID); //qm-747 sr
end;


function TDM.GetTicketPrioritySort(const WorkPriorityID: Integer): string;
const
  Select = 'select coalesce(sortby, 0) S from reference where ref_id = ';
begin
  if WorkPriorityID < 1 then
    Result := '0'
  else
    Result := Engine.RunSQLReturnS(Select + IntToStr(WorkPriorityID));
end;

function TDM.GetTicketPriority(const TicketID: Integer): string;
const
  Select = 'select coalesce(description, ''Normal'') S from ticket ' +
    'left join reference on work_priority_id = ref_id ' +
    'where ticket_id = ';
begin
  Result := Engine.RunSQLReturnS(Select + IntToStr(TicketID));
end;

function TDM.GetTicketPriorityIDByCode(Code: string): integer;  //QM-720 RO Skipped EB
const
  Select = 'select ref_id N from reference where ' +
           '[type] = ''tkpriority'' and ' +
           'code = ';
var
  SQL: string;
begin
  SQL := Select + '''' + Code + '''';
  Result := Engine.RunSQLReturnN(SQL);
end;

function TDM.DownloadAttachment(AttachmentID: Integer;
  DownloadFileNumber: Integer=0; NumberFilesToDownload: Integer=0): string;
begin
  FLocalAttachment.DownloadFileNumber := DownloadFileNumber;
  FLocalAttachment.NumberFilesToDownload := NumberFilesToDownload;
  try
    Result := FLocalAttachment.DownloadAttachment(AttachmentID);
  finally
    FLocalAttachment.DownloadFileNumber := 0;
    FLocalAttachment.NumberFilesToDownload := 0;
  end;
end;

function TDM.CopyAttachmentToDownloadFolder(AttachmentID: Integer): string;
begin
  Result := FLocalAttachment.CopyAttachmentToDownloadFolder(AttachmentID);
end;

function TDM.UnackedMessageCount: Integer;
const
  Select = 'select count(*) N from message_dest ' +
    'where ack_date is null and show_date <= current_timestamp ' +
    'and expiration_date > current_timestamp ' +
    'and active = True and rule_id is null';
begin
  Result := Engine.RunSQLReturnN(Select);
end;

function TDM.UnackedBreakRuleMessageCount: Integer;
const
  Select = 'select count(*) N from message_dest ' +
    'where ack_date is null and show_date <= current_timestamp ' +
    'and expiration_date > current_timestamp ' +
    'and active = True and rule_id is not null';
begin
  Result := Engine.RunSQLReturnN(Select);
end;

procedure TDM.CheckForUnackedMessages;
var
  MessageCount: Integer;
begin
  MessageCount := UnackedMessageCount;
  if MessageCount > 0 then
    Engine.AddToMyLog(Self, 'NOTICE: You must acknowledge ' + IntToStr(MessageCount) +
      ' unread ' + AddSIfNot1(MessageCount, 'message') + ' before continuing.');
end;

function TDM.CheckInternet: Boolean;
var TCPClient : TIdTCPClient;
begin
  TCPClient := TIdTCPClient.Create (NIL);
  try
    try
      TCPClient.ReadTimeout := 2000;
      TCPClient.ConnectTimeout := 2000;
      TCPClient.Port := 80;
      TCPClient.Host := 'google.com';
      TCPClient.Connect;
      TCPClient.Disconnect;
      Result := true;

    except
      Result := false;
    end; { try / except }

    finally
      TCPClient.Free;
    end; { try / finally }
  end;


function TDM.MessageExists(MsgSearchText: string; EmpID: Integer; InDate: TDateTime): Boolean;
const
  Select = 'select count(*) N from message_dest ' +
    'where ack_date is null and show_date <= current_timestamp ' +
    'and expiration_date > current_timestamp ' +
    'and active = True and rule_id is not null ' +
    'and message_number like ';
var
  count : integer;
  Sel2: string;
begin
  Result := False;
  Sel2 := Select + ''' + MsgSearchText + ''';
  count := Engine.RunSQLReturnN(Sel2);
  if  count > 0 then
    Result := True;
end;



function TDM.TicketViewLimitForLocator(const EmployeeID: Integer): Integer;
const
  Select = 'select coalesce(ticket_view_limit, 0) N from employee where emp_id = ';
begin
  Result := Engine.RunSQLReturnN(Select + IntToStr(EmployeeID));
end;

function TDM.MyTicketViewLimit: Integer;
begin
  Result := TicketViewLimitForLocator(EmpID);
end;

function TDM.ShowFutureWorkForLocator: Boolean;
{ Return True if the employee has the show_future_tickets flag = true -or- if the flag
  is false then only show future work if all the past/current due tickets are closed.}
const
  Select = 'select e.show_future_tickets, count(*) as OpenPastDueTktCnt from ticket t join locate l on l.ticket_id = t.ticket_id ' +
    ' join employee e on l.assigned_to = e.emp_id where (e.emp_id = %d and e.show_future_tickets) ' +
    ' or ((not e.show_future_tickets) and t.due_date < %s ' +
    ' and l.assigned_to = %d and l.active and t.kind <> ''DONE'') group by e.show_future_tickets';
var
  DataSet : TDataSet;
begin
  DataSet := Engine.OpenQuery(Format(Select, [EmpID, DateToDBISAMDate(Date+1), EmpID]));
  try
    Result := DataSet.FieldByName('show_future_tickets').AsBoolean;
    if not Result then
      Result := (DataSet.FieldByName('OpenPastDueTktCnt').AsInteger = 0);//no open, past due tkts- so ok to show future tkts
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TDM.SetEmployeeTicketViewLimit(EmployeeID: Integer; TicketViewLimit: Integer);
begin
  CallingServiceName('SaveEmployeeTicketViewLimit');
  Engine.Service.SaveEmployeeTicketViewLimit(Engine.SecurityInfo, EmployeeID, TicketViewLimit);
end;


procedure TDM.GetPrintableAttachmentList(ForeignType, ForeignID: Integer; List: TStrings);
var
  Table: TDBISAMTable;
begin
  Table := TDBISAMTable.Create(nil);
  try
    Table.DatabaseName := 'Memory';
    Engine.GetPrintableAttachmentList(ForeignType, ForeignID, Table);
    Table.First;
    List.Clear;
    while not Table.Eof do begin
      List.AddObject(Table.FieldByName('orig_filename').AsString, TObject(Table.FieldByName('attachment_id').AsInteger));
      Table.Next;
    end;
  finally
    FreeAndNil(Table);
  end;
end;

function TDM.GetFileExtensionForPrintableFileType(FileType: string): string;
const
  SQL = 'select code from reference where type = ''%s'' and modifier = %s and active_ind = True';
var
  DataSet: TDBISAMQuery;
begin
  DataSet := TDBISAMQuery.Create(nil);
  try
    DataSet.DatabaseName := Database1.DatabaseName;
    DataSet.SQL.Text := Format(SQL, [ReferenceTypePrintableFile, QuotedStr(FileType)]);
    DataSet.Open;
    while not DataSet.EOF do begin
      Result := Result + DataSet.FieldByName('code').AsString + ' ';
      DataSet.Next;
    end;
  finally
    FreeAndNil(DataSet);
  end;
end;

function TDM.GetMarkableUnitStatusCodes: TStringArray;
const
  Select = 'select status from statuslist where allow_locate_units and active';
var
  Data: TDataSet;
  i: Integer;
begin
  SetLength(Result, 1);
  Result[0] := '';
  Data := Engine.OpenQuery(Select);
  try
    SetLength(Result, Data.RecordCount);
    i := 0;
    while not Data.EOF do begin
      Result[i] := Data.FieldByName('status').AsString;
      Inc(i);
      Data.Next;
    end;
  finally
    FreeAndNil(Data);
  end;
end;



procedure TDM.UpdateLocatesTotalUnits;
var
  Cursor: IInterface;
  LengthTotal: Integer;

  function LocateLengthTotal: Integer;
  begin
    LocateHoursData.Filter := 'locate_id=' + LocateData.FieldByName('locate_id').AsString;
    LocateHoursData.Filtered := True;
    try
      //AddToQMLog('LocateLengthTotal Filter: ' + LocateHoursData.Filter + '  Count: ' + IntToStr(LocateHoursData.RecordCount));
      LocateHoursData.First;
      Result := 0;
      while not LocateHoursData.Eof do begin
        Result := Result + LocateHoursData.FieldByName('units_marked').AsInteger;
        LocateHoursData.Next;
      end;
    finally
      LocateHoursData.Filtered := False;
    end;
    //AddToQMLog('LocateLengthTotal Result: ' + IntToStr(Result));
  end;

  procedure SaveChangedLengthTotals;
  var
    Calc: TLengthConversionCalc;
    QuantityTotal: Integer;
  begin
    //AddToQMLog('SaveChangedLengthTotals LengthTotal: ' + IntToStr(LengthTotal));
    if (LocateData.FieldByName('length_total').AsInteger <> LengthTotal) then begin
      EditDataSet(LocateData);
      if LengthTotal = 0 then
        LocateData.FieldByName('length_total').Clear
      else
        LocateData.FieldByName('length_total').AsInteger := LengthTotal;

      if not LocateData.FieldByName('invoiced').AsBoolean then begin
        //AddToQMLog('not LocateData.FieldByName(''invoiced'').AsBoolean');
        Calc := TLengthConversionCalc.Create(LocateData.FieldByName('locate_id').AsInteger);
        try
          QuantityTotal := Calc.LengthAsBillingUnits(LengthTotal);
        finally
          FreeAndNil(Calc);
        end;
        if QuantityTotal < 1 then
          QuantityTotal := 1;
        LocateData.FieldByName('qty_marked').AsInteger := QuantityTotal;
      end;

      //AddToQMLog('LocateData.FieldByName(''length_marked'').Clear');
      LocateData.FieldByName('length_marked').Clear;
      try
        PostDataSet(LocateData);
      except
        on E: EOdDataEntryError do begin
          //AddToQMLog('PostDataSet(LocateData) EOdDataEntryError: ' + E.Message);
          CancelDataSet(LocateData);
          raise;
        end;
      end;
    end;
  end;

begin
  //AddToQMLog('TDM.UpdateLocatesTotalUnits');
  Assert(LocateData.Active, 'LocateData is not active in UpdateLocatesTotalUnits');
  Assert(LocateHoursData.Active, 'LocateHoursData is not active in UpdateLocatesTotalUnits');

  Cursor := ShowHourglass;
  //AddToQMLog('LocateData.DisableControls, Count: ' + IntToStr(LocateData.RecordCount));
  LocateData.DisableControls;
  try
    //AddToQMLog('LocateData.First');
    LocateData.First;
    //AddToQMLog('while not LocateData.Eof do begin');
    while not LocateData.Eof do begin
      LengthTotal := LocateLengthTotal;
      SaveChangedLengthTotals;
      LocateData.Next;
    end;
  finally
    //AddToQMLog('LocateData.EnableControls');
    LocateData.EnableControls;
  end;
end;

procedure TDM.SaveLocateLength(LocateID, LengthMarked: Integer; LocateStatus: string; WorkDate: TDateTime);
var
  UnitConversionID: Integer;
  UnitType: string;
begin
  if EditingDataSet(LocateHoursData) then begin
    Assert(not LocateHoursData.FieldByName('emp_id').IsNull);
    Exit;
  end;

  LocateHoursData.Append;
  LocateHoursData.FieldByName('emp_id').AsInteger := EmpID;
  LocateHoursData.FieldByName('work_date').AsDateTime := RoundSQLServerTimeMS(WorkDate);
  LocateHoursData.FieldByName('locate_id').AsInteger := LocateID;
  LocateHoursData.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
  LocateHoursData.FieldByName('status').AsString := LocateStatus;
  LocateHoursData.FieldByName('units_marked').AsInteger := LengthMarked;
//  LocateHoursData.FieldByName('bigfoot').AsBoolean := False;  //QM-1037 EB Automatically set length as locator
  UnitConversionId := UnitTypeForLocate(LocateID, UnitType);
  if UnitConversionID = NoConversionID then
    LocateHoursData.FieldByName('unit_conversion_id').Clear
  else
    LocateHoursData.FieldByName('unit_conversion_id').AsInteger := UnitConversionID;
  PostDataSet(LocateHoursData);
end;

function TDM.IsMeetTicket(TicketID: Integer): Boolean;
const
  SQL = 'select ticket_type as S from ticket where ticket_id = %d';
var
  TicketType: string;
begin
  if Ticket.Active and (Ticket.FieldByName('ticket_id').AsInteger = TicketID) then
    TicketType := Ticket.FieldByName('ticket_type').AsString
  else
    TicketType := Engine.RunSQLReturnS(Format(SQL, [TicketID]));
  Result := StrContains(TicketTypeMeet, TicketType);
end;

function TDM.IsRestricted(pClient_Code: string): boolean;   //QM-585 EB PLUS WHITELIST
var
  i: integer;
begin
  Result := False;

  for i := 0 to fPlusRestrictedList.CC.Count -1 do begin
    if (UPPERCASE(pClient_Code)) = (UPPERCASE(fPlusRestrictedList.CC[i])) then begin
      Result := TRUE;
      Break;
    end;
  end;
end;

function TDM.ValidStatusForTicket(TicketID: Integer; MeetOnlyStatus: Boolean): Boolean;
begin
  Result := True;
  if MeetOnlyStatus then
    Result := IsMeetTicket(TicketID);
end;

function TDM.IsTicketDetailHidden: Boolean;
begin
  Result := (not PermissionsDM.CanI(RightTicketsShowWithoutArriving));
end;

function TDM.IsValidStatusAll(pStatus: string; pTicketID: integer;
  pTktType: string; pClientID: integer; pMeetOnly: boolean;
  pTTExclusion: string): Boolean;
begin
  {consolidating for use with possible exit check}  //
  Result :=  ValidStatusForTicket(pTicketID, pMeetOnly)
              and ValidStatusForEmployee(pStatus)
              and ValidExclusionTicketType(pClientID, pStatus, pTktType, pTTExclusion);
end;

procedure TDM.SaveArrivalsToServer(const TicketOrDamageID: Integer; const ArrivalType: TArrivalType);
begin
  SaveArrivalChanges(JobsiteArrivalData); // Saves from the temp dataset to the local-disk DB
  try
    Engine.SendArrivalChanges(ArrivalType);
    OpenJobsiteArrivalData(TicketOrDamageID, ArrivalType);
  except
    on E: Exception do
      ShowTransientMessage('Warning: Unable to sync arrival status immediately.'  + sLineBreak + 'Reason: ' + E.Message + sLineBreak + 'The arrival status will be re-sent on the next arrival or sync.');
  end;
  // A threaded version should handle DM.Free, multiple arrivals before the first one succeeds,
  // application exit/terminate, use Synchronize() for DB local DB write access, thread exceptions,
  // access only thread safe portions of the service interface (or use a new instance), a full sync
  // before the thread stops, handle aplication of returned server IDs where the old ID no longer
  // exists because of a previous successful background sync, etc.
end;

function TDM.ApplicationTrayIcon: TTrayIcon;
begin
  if not Assigned(FTrayIcon) then begin
    FTrayIcon := TTrayIcon.Create(Self);
    FTrayIcon.BalloonTimeout := 10000; // Show balloon hint for 10 seconds
    FTrayIcon.BalloonTitle := Application.Title;
  end;
  Result := FTrayIcon;
end;

procedure TDM.ShowTransientMessage(const Msg: string);
var
  TrayIcon: TTrayIcon;
begin
  TrayIcon := ApplicationTrayIcon;
  TrayIcon.Visible := True;
  TrayIcon.Hint := Msg;
  TrayIcon.BalloonHint := Msg;
  TrayIcon.ShowBalloonHint;
  StartTrayIconHideTimer;
end;

procedure TDM.SetTrayIconHint(const Msg: string);
var
  TrayIcon: TTrayIcon;
begin
  TrayIcon := ApplicationTrayIcon;
  TrayIcon.Visible := True;
  TrayIcon.Hint := Msg;
  StartTrayIconHideTimer;
end;

procedure TDM.StartTrayIconHideTimer;
begin
  if not Assigned(FTrayIconHideTimer) then begin
    FTrayIconHideTimer := TTimer.Create(Self);
    FTrayIconHideTimer.Interval := 60 * 1000; // Completely go away in 1 minute
    FTrayIconHideTimer.OnTimer := HideTrayIcon;
    FTrayIconHideTimer.Enabled := True;
  end;
end;

procedure TDM.HideTrayIcon(Sender: TObject);
begin
  if Assigned(FTrayIconHideTimer) then
    FTrayIconHideTimer.Enabled := False;
  FreeAndNil(FTrayIconHideTimer);
  FTrayIcon.Visible := False;
end;

procedure TDM.GetLimitedTicketIDList(List: TStrings; TicketLimit: Integer);
const
  SelectLimitedTickets = 'select distinct ticket.ticket_id, ' +
    'coalesce(ref.sortby, 0) work_priority, due_date, ticket_number, kind, route_order ' +   //QM-896 EB Route Order Limit (adding field)
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'where locate.locator_id=%d ' +
    'and locate.active ' +
    'and not locate.closed ' +
    'and ((do_not_mark_before is null) ' +
    'or (do_not_mark_before <= ''%s'')) ' +
    'and ticket.kind not in %s ' +
    'and not ref.code = ''Release'' ';


    LimOrderBy =  ' order by work_priority desc, due_date, ticket_number ' +
                  'TOP %d';
    LimOrderByRO = ' and (route_order >= 0)' +
                   ' order by route_order, due_date, ticket_number ' +
                   'TOP %d';        //QM-896 EB Route Order Limitation

  SelectAlwaysVisibleTickets = 'select distinct ticket_id ' +
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'where locate.locator_id=%d ' +
    'and locate.active ' +
    'and not locate.closed ' +
    'and ((do_not_mark_before is null) ' +
    'or (do_not_mark_before <= ''%s'')) ' +
    'and ticket.kind in %s ' +
    'or ref.code = ''Release'' ';
var
  SyncDate: string;
  Data: TDataSet;
  AlwaysVisibleTicketKinds: string;
  LimitedTicketsList: TStringList;
  FirstTaskTicket: Integer;
  lSQL : string;  //QM-896 Route Order Limitation EB
begin
  if TicketLimit < 1 then
    Exit;

  AlwaysVisibleTicketKinds := Format('(''%s'', ''%s'')', [TicketKindEmergency, TicketKindOngoing]);
  SyncDate := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', UQState.LastLocalSyncTime);
  Data := Engine.OpenQuery(Format(SelectAlwaysVisibleTickets, [EmpID, SyncDate, AlwaysVisibleTicketKinds]));
  try
    GetListFromDataSet(Data, List, 'ticket_id', 'ticket_id');
  finally
    FreeAndNil(Data);
  end;

  if FirstTicketTaskedForTomorrow(FirstTaskTicket) then
    List.Add(IntToStr(FirstTaskTicket));

  LimitedTicketsList := TStringList.Create;
  try
    if PermissionsDM.CanI(RightTicketsUseRouteOrder) then      //QM-896 EB Route Order Limit
      lSQL := SelectLimitedTickets +  LimOrderByRO
    else
      lSQL := SelectLimitedTickets + LimOrderBy;
    
    Data := Engine.OpenQuery(Format(lSQL,                      //QM-896 EB Route Order Limit
      [DM.EmpID, SyncDate, AlwaysVisibleTicketKinds, TicketLimit]));
    GetListFromDataSet(Data, LimitedTicketsList, 'ticket_id', 'ticket_id');
    List.AddStrings(LimitedTicketsList);
  finally
    FreeAndNil(LimitedTicketsList);
    FreeAndNil(Data);
  end;
end;

procedure TDM.SetTicketActivitiesAcknowledged(ActivityAckIDArray: TIntegerArray);
var
  i: Integer;
  AckIDList: IntegerList;
begin
  CallingServiceName('AckTicketActivities');
  AckIDList := IntegerList.Create;
  try
    for i := Low(ActivityAckIDArray) to High(ActivityAckIDArray) do
      AckIDList.Add(ActivityAckIDArray[i]);
    Engine.Service.AckTicketActivities(Engine.SecurityInfo, AckIDList);
  finally
    FreeAndNil(AckIDList);
  end;
end;


procedure TDM.GetValidStatusesForTicketAndClient(Statuses: TStrings; TicketID:Integer; ClientID: Integer);   //QM-282 EB Updated
CONST
  STATUS_RESTRICTION='TicketDetailStatus';
var
  Status, Desc : string;
  ValidForTicket: Boolean;
  ValidForClient: Boolean;
  ValidForLocate: Boolean;
  TempSL: TStringList;

  StatusLimit:string;  //qm-559 SR
  StatusListLimits:TStrings;      //qm-559 SR
begin
  Assert(Assigned(Statuses));
  Assert(ClientID > 0);
  Statuses.Clear;

  try
  TempSL := TStringList.Create;
  StatusListLimits:= TStringList.Create;   //qm-559 SR
  StatusListLimits.Delimiter:=',';   //qm-559 SR
  StatusListLimits.Add('''-N''');    //qm-559 SR
  StatusListLimits.Add('''-P''');   //qm-559 SR


  TempSL.Delimiter := ',';
  TempSL.Add(IntToStr(ClientID));

  if PermissionsDM.CanI(STATUS_RESTRICTION, False) then  //qm-559 SR
  begin //returned restrictions must look like '
    StatusLimit:= PermissionsDM.GetLimitation(STATUS_RESTRICTION);
    StatusListLimits.DelimitedText:=StatusLimit;
  end;

  StatuslistForClientQry.SQL.Text := Format(STATUS_LIST_FOR_CLIENTS,[StatusListLimits.DelimitedText, TempSL.DelimitedText]);   //qm-559 SR
  RefreshDataSet(StatuslistForClientQry);
  StatusList.First;
  while not StatusList.EOF do begin
    Status := StatusList.FieldByName('status').AsString;
    Desc := StatusList.FieldByName('status_name').AsString;
    ValidForTicket := ValidStatusForTicket(TicketID, StatusList.FieldByName('meet_only').AsBoolean);
    ValidForClient := ValidStatusForClient(ClientID, Status);
    ValidForLocate := not StringInArray(Status, InternalStatuses);
    if ValidForTicket and ValidForClient and ValidForLocate then
      Statuses.Add(Status);  //QM-282 EB updated

    StatusList.Next;
  end;
  finally
    FreeAndNil(TempSL);
  end;
end;

function TDM.GetLocatorByTicketAndClient(const TicketID, ClientID: Integer; var EmployeeID: Integer): Boolean;
const
  Select = 'select assigned_to_id as N from locate ' +
    'where ticket_id = %d and client_id = %d ' +
    'and status <> %s';
begin
  EmployeeID := Engine.RunSQLReturnN(Format(Select, [TicketID, ClientID, QuotedStr(NotAClientStatus)]));
  Result := EmployeeID > 0;
  if not Result then
    Result := GetFirstLocatorOnTicket(TicketID, EmployeeID);
end;


procedure TDM.GetLocatorListForTicket(TicketID: Integer; List: TStrings);
begin

end;

function TDM.GetLocStatusPresets(TicketID: Integer; Presets: TStringList): Boolean;
const
  SQL1 = 'select * from ticket_info where ticket_id = %d order by modified_date desc';
  SQL2 = 'select ticket_number, ticket_id, caller, ' +
         'caller_phone, caller_cellular, ' +
         'caller_altcontact, caller_altphone ' +
         'from Ticket ' +
         'where ticket_id = %d';
var
  DataSet1, DataSet2 : TDataSet;
  InfoType, InfoValue : string;
  caller, callerPhone, callerCell, callerAlt, callerAltPhone: string;
begin
  Result := False;
  if not Assigned(Presets) then
    exit;

  DataSet1 := Engine.OpenQuery(Format(SQL1, [TicketID]));
  DataSet2 := Engine.OpenQuery(Format(SQL2, [TicketID]));
  try
    while not DataSet1.EOF do begin
      InfoType := DataSet1.FieldByName('Info_type').AsString;
      InfoValue := DataSet1.FieldByName('Info').AsString;
      Presets.Add(InfoType + '=' + InfoValue);
      DataSet1.Next;
    end;

    if not DataSet2.Eof then begin
      caller :=         DataSet2.FieldByName('caller').AsString;
      callerPhone :=    DataSet2.FieldByName('caller_phone').AsString;
      callerCell :=     DataSet2.FieldByName('caller_cellular').AsString;
      callerAlt :=      DataSet2.FieldByName('caller_altcontact').AsString;
      callerAltPhone := DataSet2.FieldByName('caller_altphone').AsString;

      //If caller/phone is blank, we will prepopulate with next available field
      if (caller <> '') then begin
        Presets.Add(ONGOING + '=' + caller);
        if (callerPhone <> '') then
          Presets.Add(CONPHONE + '=' + callerPhone)
        else if (callerCell <> '') then
          Presets.Add(CONPHONE + '=' + callerPhone)
      end
      else if (callerAlt <> '') then begin
        Presets.Add(ONGOING + '=' + callerAlt);
        if (callerAltPhone <> '') then
        Presets.Add(CONPHONE + '=' + callerAltPhone);
      end;

     //At later date, we may add the Alternate information, cellular
    end;
    if Presets.Count > 0 then
      Result := True
    else
      Result := False;
  finally
    FreeAndNil(DataSet1);
    FreeAndNil(DataSet2);
  end;
end;

function TDM.GetConfigurationDataValue(const SettingName, Default: string; IgnoreCase: boolean): string;
const
  SQL = 'select * from configuration_data where name = %s';
  SQL_ig = 'select * from configuration_data where upper(name) = %s';
var
  Data: TDataSet;
begin
  Result := Default;
  if IgnoreCase then
    Data := Engine.OpenQuery(Format(SQL_ig, [UpperCase(QuotedStr(SettingName))]))
  else
    Data := Engine.OpenQuery(Format(SQL, [QuotedStr(SettingName)]));
  try
    if Data.RecordCount = 1 then
      Result := Data.FieldByName('value').AsString;
  finally
    FreeAndNil(Data);
  end;
end;

function TDM.GetConfigurationDataInt(const SettingName: string; const Default: Integer): Integer;
var
  Value: string;
begin
  Value := GetConfigurationDataValue(SettingName, IntToStr(Default));
  if not TryStrToInt(Value, Result) then
    Result := Default;
end;

function TDM.GetTicketImage(TicketData: TDataSet; var TicketIsXml: Boolean): string;
const
  SQL = 'select xml_ticket_format from call_center where cc_code = ''%s''';
var
  Data: TDataSet;
const
  GA811_TICKET='[TICKET]';     //qm-253  sr
  GA_3003='3003';             //qm-253  sr
begin
  if Copy(TicketData.FieldByName('ticket_number').AsString, 0,3) <> 'MAN' then //QMANTWO-307
  Data := Engine.OpenQuery(Format(SQL, [TicketData.FieldByName('ticket_format').AsString]))
  else
   Data := Engine.OpenQuery(Format(SQL, ['MANUAL']));   //QMANTWO-307
  try
    if ((TicketData.FieldByName('ticket_format').AsString = GA_3003) and   //qm-253  sr
          (LeftStr(TicketData.FieldByName('image').AsString, 8) = GA811_TICKET)) then    //qm-253  sr
       result := GetGa811Image(TicketData.FieldByName('image').AsString)  //qm-253  sr
     else
     begin
       Result := GetDisplayableTicketImage(TicketData.FieldByName('image').AsString,
                 Data.FieldByName('xml_ticket_format').AsString, TicketIsXml);
       Result := StringReplace(Result, LF, CRLF, [rfReplaceAll]);
     end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TDM.EmpTimeRuleList(List: TStrings; IncludeBlank: Boolean);
const
  Select = 'select ref_id, coalesce(code, ''NO RULE'') code, description, sortby ' +
    'from reference where type = ''timerule'' and active_ind order by sortby';
var
  Timerules: TDataSet;
begin
  Assert(Assigned(List));
  List.Clear;
  if IncludeBlank then
    List.Add('');

  Timerules := Engine.OpenQuery(Select);
  try
    while not Timerules.Eof do begin
      List.AddObject(Timerules.FieldByName('code').AsString,
        TObject(Timerules.FieldByName('ref_id').AsInteger));
      Timerules.Next;
    end;
  finally
    FreeAndNil(Timerules);
  end;
end;

procedure TDM.AddBreakRuleResponse(const RuleID: Integer; const Response, ContactPhone: string; const ResponseDate: TDateTime; const MessageDestID: Integer);
begin
  OpenDataSet(SaveBreakRuleResponse);
  Engine.LinkEventsAutoInc(SaveBreakRuleResponse);
  SaveBreakRuleResponse.Insert;
  SaveBreakRuleResponse.FieldByName('emp_id').AsInteger := EmpID;
  SaveBreakRuleResponse.FieldByName('response_date').AsDateTime := RoundSQLServerTimeMS(ResponseDate);
  SaveBreakRuleResponse.FieldByName('rule_id').AsInteger := RuleID;
  SaveBreakRuleResponse.FieldByName('response').AsString := Response;
  SaveBreakRuleResponse.FieldByName('contact_phone').AsString := ContactPhone;
  if MessageDestID > 0 then
    SaveBreakRuleResponse.FieldByName('message_dest_id').AsInteger := MessageDestID;
  SaveBreakRuleResponse.Post;
end;

function TDM.GetExportFolderName: string;
begin
  Result := GetClientExportsLocalDir;
end;

function TDM.CreateExportFileName(const Name, Extension: string; LocatorName: string): string;   //QM-11 EB Work with WorkMgmt Lists
var
  ExportPath: string;
begin
  if LocatorName = '' then      //QM-11 EB
    LocatorName := UQState.DisplayName;

  ExportPath := GetExportFolderName;
  if not ForceDirectories(ExportPath) then
    raise Exception.CreateFmt('Unable to create export folder %s.', [ExportPath]);
  Result := IncludeTrailingBackslash(ExportPath) + StrReplaceChar(LocatorName, '.', ' ') +   //QM-11 EB
    Name + Extension;
end;

procedure TDM.CleanExportFolder(const DaysOld: Integer);
var
  Path: string;
begin
  try
    Path := IncludeTrailingBackslash(GetExportFolderName);
    OdDeleteFile(Path, UQState.DisplayName + '*.*', DaysOld, False, False);
  except on E: Exception do
    MessageDlg('Unable to clean out export directory: ' + E.Message, mtError, [mbOK], 0);
  end;
end;

function TDM.FirstTicketTaskedForTomorrow(var TicketID: Integer): Boolean;
const
  Select = 'select task.ticket_id, task.est_start_date from ticket ' +
    'inner join task_schedule task on ticket.ticket_id = task.ticket_id ' +
    'where task.est_start_date >= %s and task.est_start_date < %s and task.active ' +
    'and task.performed_date is NULL ' +
    'and ticket.kind <> ''DONE'' order by task.est_start_date';
var
  Tasks: TDataSet;
begin
  TicketID := 0;

  Tasks := Engine.OpenQuery(Format(Select, [DateToDBISAMDate(Tomorrow),
    DateToDBISAMDate(Tomorrow+1)]));
  try
    if HasRecords(Tasks) then
      TicketID := Tasks.FieldByName('ticket_id').Value;
  finally
    FreeAndNil(Tasks);
  end;
  Result := TicketID > 0;
end;

procedure TDM.AddTicketToTaskSchedule(const EmpID, TicketID: Integer; const EstStartDate: TDateTime);
begin
  SaveTaskSchedule.Open;
  Engine.LinkEventsAutoInc(SaveTaskSchedule);
  SaveTaskSchedule.Insert;
  SaveTaskSchedule.FieldByName('emp_id').Value := EmpID;
  SaveTaskSchedule.FieldByName('ticket_id').Value := TicketID;
  SaveTaskSchedule.FieldByName('est_start_date').Value := EstStartDate;
  SaveTaskSchedule.FieldByName('added_date').Value := RoundSQLServerTimeMS(Now);
  SaveTaskSchedule.FieldByName('added_by').Value := Self.EmpID;
  SaveTaskSchedule.FieldByName('active').Value := True;
  PostDataSet(SaveTaskSchedule);
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketScheduled, IntToStr(TicketID), Now, '');
  ExportQMDataAsXML;
end;

procedure TDM.SetFirstTaskForTomorrowPerformed(const TicketID: Integer; const PerformedDate: TDateTime);
begin
  Assert(PerformedDate>0,'First Task cannot be set with an empty performed_date.');
  SaveTaskSchedule.Open;
  Engine.LinkEventsAutoInc(SaveTaskSchedule);
  if SaveTaskSchedule.Locate('ticket_id;active;performed_date', VarArrayOf([TicketID,True,Null]), [loPartialKey]) then begin
    SaveTaskSchedule.Edit;
    SaveTaskSchedule.FieldByName('performed_date').AsDateTime := PerformedDate;
    PostDataSet(SaveTaskSchedule);
  end;
end;


procedure TDM.ExportQMDataAsXML;
const
  QMDataXMLFile = 'QMData.xml';
var
  FileName: string;
begin
  try
    FileName := IncludeTrailingBackslash(FDataDir) + QMDataXMLFile;
    CreateQMDataXMLFile(FileName);
  except
    on E: Exception do begin
      ShowTransientMessage('Warning: Unable to create ' + FileName +
        sLineBreak + 'Reason: ' + E.Message);
    end;
  end;
end;

procedure TDM.TimesheetChangeReasonList(List: TStrings; IncludeBlank: Boolean);
const
  SQL = 'select ref_id, code, description, sortby ' +
    'from reference where active_ind and type=''tsereason'' order by sortby';
var
  ReasonData: TDataSet;
begin
  Assert(Assigned(List));
  List.Clear;
  if IncludeBlank then
    List.Add('');

  ReasonData := Engine.OpenQuery(SQL);
  try
    while not ReasonData.Eof do begin
      List.AddObject(ReasonData.FieldByName('description').AsString,
        TObject(ReasonData.FieldByName('ref_id').AsInteger));
      ReasonData.Next;
    end;
  finally
    FreeAndNil(ReasonData);
  end;
end;


procedure TDM.GetChangeReasonList(List: TStrings; ReasonType: string; IncludeBlank: Boolean);
const
  SQL = 'select ref_id, code, description, sortby ' +
    'from reference where active_ind and type=''%s'' order by sortby';
var
  ReasonData: TDataSet;
begin
  Assert(Assigned(List));
  List.Clear;
  if IncludeBlank then
    List.Add('');

  ReasonData := Engine.OpenQuery(Format(SQL,[ReasonType]));
  try
    while not ReasonData.Eof do begin
      List.AddObject(ReasonData.FieldByName('description').AsString,
        TObject(ReasonData.FieldByName('ref_id').AsInteger));
      ReasonData.Next;
    end;
  finally
    FreeAndNil(ReasonData);
  end;
end;

function TDM.AttachmentManager: TLocalAttachment;
begin
  Result := FLocalAttachment;
end;

procedure TDM.CreateBackgroundUploader;
begin
  Assert(Assigned(Application.MainForm), 'CreateBackgroundUploader needs a handle to the main form');
  Assert(not Assigned(FBackgroundUploader), 'Must only CreateBackgroundUploader once');

  FBackgroundUploader := TBackgroundUploadThread.Create(Application.MainForm.Handle, AppName, EmpID, UQState.DebugUploadThread);
end;

procedure TDM.BackgroundUploaderStart;
begin
  Assert(Assigned(FBackgroundUploader), 'Background Upload should be created at startup');

  FBackgroundUploader.RightEnabled := CanBackgroundUpload;
  if CanBackgroundUpload then begin
    FBackgroundUploader.UploadLocationID := GetBackgroundUploadLocationID;
    if not FBackgroundUploader.UploadLocationID < 0 then
      FBackgroundUploader.NextRunTime := Now
    else
      FBackgroundUploader.PostNextRunStatus; //update status bar about upload location
  end;
end;

procedure TDM.KillBackgroundUploader;
begin
  Assert(Assigned(FBackgroundUploader), 'Background uploader must be initialized');
  FBackgroundUploader.StopNow;
  FBackgroundUploader.Terminate;
  FBackgroundUploader.WaitFor;
end;

function TDM.GetBackgroundUploadLocationID: Integer;
var
  LocationID: Integer;
begin
  LocationID := StrToIntDef(PermissionsDM.GetLimitation(RightAttachmentsBackgroundUpload), -1);
  if LocationID < 0 then
    ShowTransientMessage('Background Upload right needs a upload location id for a modifier');
  Result := LocationID;
end;

procedure TDM.AddToQMLog(const Msg: string);
var
  Log: TOdLog;
begin
  Log := OdLog.GetLog;
  if not Assigned(Log) then
    Exit;

  Log.OpenLog;
  Log.WriteStamp;
  Log.Write(Msg);
  Log.CloseLog;
end;

function TDM.SafeToSyncNow(var NoSyncReason: string): Boolean;
var
  ForegroundWinHandle: HWND;
  BlockingWindow: PChar;
  EditedDatasetName: String;
  recHandle : THandle;    //QM-88  SR
begin
  Result := True;
  NoSyncReason := '';
  ForegroundWinHandle := GetLastActivePopup(Application.Handle) ;
  BlockingWindow := nil;
  recHandle := FindWindow('TfrmMain', nil);    //QM-88  SR
  // Can't sync if any cached pending ticket changes
  if FTicketChanged or FLocFacilityDeleted then begin
    NoSyncReason := 'unsaved ticket changes';
    Result := False;

  // Can't sync if a modal dialog is showing
  end
  else if OutstandingWOAttachments then begin   //QMANTWO-815 EB
    NoSyncReason := 'unsaved work order attachments';
    Result := False;
  end

  else if fLocalAttachment.UploadInProgress then begin
    NoSyncReason := 'Attachment Upload in progress';
    Result := False;
  end

  else if recHandle <> 0 then begin  //QM-88  SR
    NoSyncReason := 'Quarter Min Editor Open';
    Result := False;
  end

  else if (ForegroundWinHandle <> Application.MainForm.Handle)
    and (ForegroundWinHandle <> Application.Handle)
    and (Windows.IsWindowVisible(ForegroundWinHandle)) then begin
    Windows.GetWindowText(ForegroundWinHandle, BlockingWindow, 255);
    if IsEmpty(BlockingWindow) then
      Windows.GetClassName(ForegroundWinHandle, BlockingWindow, 255);
    if not IsEmpty(BlockingWindow) then begin
      NoSyncReason := BlockingWindow + ' modal dialog box is visible';
      Result := False;
    end;
  end else begin
  // Can't sync if any datasets are in edit or insert mode
    if AnyDatasetInEditMode(EditedDatasetName) then begin
      Result := False;
      NoSyncReason := 'unsaved changes in ' + EditedDatasetName;
    end;
  end;
end;

function TDM.AnyDatasetInEditMode(var NameOfDatasetInEditMode: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Database1.DataSetCount - 1 do begin
    if Database1.DataSets[i].State in dsEditModes then begin
      Result := True;
      NameOfDatasetInEditMode := Database1.DataSets[i].Name;
      Exit;
    end;
  end;
end;

procedure TDM.GetAutoSyncIdleLimits(var DelaySeconds, DelaySecondsMinimized, SecondsBetween: Integer);
const
  DefaultIdleSyncLimits = '10,5,30';  // Defaults in minutes: delay, delay minimized, wait between syncs
var
  IdleSyncLimits: TStringList;
begin
  IdleSyncLimits := TStringList.Create;
  try
    PermissionsDM.GetLimitationList(RightSyncAfterIdle, IdleSyncLimits, False);
    if not ValidateIntegerCommaSeparatedList(IdleSyncLimits.Text) or (IdleSyncLimits.Count <> 3) then begin
      AddToQMLog(Format('Right "%s" has an invalid limitation %s for idle time settings. ' +
        'Using default values: %s.', [RightSyncAfterIdle, IdleSyncLimits.Text, DefaultIdleSyncLimits]));
      IdleSyncLimits.DelimitedText := DefaultIdleSyncLimits;
    end;

    DelaySeconds := StrToInt(IdleSyncLimits[0]) * 60;
    DelaySecondsMinimized := StrToInt(IdleSyncLimits[1]) * 60;
    SecondsBetween := StrToInt(IdleSyncLimits[2]) * 60;
  finally
    FreeAndNil(IdleSyncLimits);
  end;
end;

function TDM.HavePendingTimesheetChanges: Boolean;
begin
  Result := Engine.TableHasPendingUpdates('timesheet_entry');
end;

procedure TDM.NeedAttachmentParentData(Sender: TObject; FileItem: TUploadFileItem);
begin
  Assert(Assigned(FileItem));
  GetAttachmentParentData(FileItem.ForeignType, FileItem.ForeignID);
end;



function TDM.OpenLocStatusQuestions(ClientID: Integer; NewStatus: String): Boolean;
begin
  LocStatusQuestions.Close;
  LocStatusQuestions.ParamByName('ClientId').AsInteger := ClientID;
  LocStatusQuestions.ParamByName('Status').AsString := NewStatus;
  LocStatusQuestions.Open;
  Result := not LocStatusQuestions.IsEmpty;
end;

procedure TDM.GetAttachmentParentData(const ForeignType, ForeignID: Integer);
begin
  try
    case ForeignType of
      qmftTicket: UpdateTicketInCache(ForeignID);
      qmftDamage: LDamageDM.UpdateDamageInCache(ForeignID);
      qmft3rdParty: LDamageDM.UpdateDamageThirdPartyInCache(ForeignID);
      qmftLitigation: LDamageDM.UpdateDamageLitigationInCache(ForeignID);
      {qmftLocate: N/A - with Ticket}
      qmftWorkOrder: UpdateWorkOrderInCache(ForeignID);  //QMANTWO-681 EB, noticed this was missing
      {qmftDamageNarrative: N/A - with Damage}
    end;
  except
    on E: Exception do
      raise Exception.CreateFmt('cannot get missing attachment parent row (Type=%d,ID=%d). %s',
        [ForeignType, ForeignID, E.Message]);
  end;
end;

function TDM.IsESketchFilename(const Filename: string; out TicketNumber: string):
  Boolean;
var
  PRE: TPerlRegEx;
begin
  PRE := TPerlRegEx.Create(nil);
  try
    PRE.Options := [preCaseLess];
    PRE.RegEx := ESketchFilenameRegularExpression;
    PRE.Subject := Filename;
    if PRE.Match then begin
      TicketNumber := PRE.SubExpressions[1];
      Result := True;
    end else begin
      TicketNumber := '';
      Result := False;
    end;
  finally
    PRE.Free;
  end;
end;

function TDM.IsHoliday(ADate: TDateTime; CallCenter: string): boolean;  //QMANTWO-503
var
  ShortDate: TDatetime;
  ccField: string;
begin
  Result := FALSE;
  if Holidays.Active then
    Holidays.Close;

  ShortDate := Trunc(ADate);
  Holidays.ParamByName('inputdate').AsDate := ShortDate;

  Holidays.Open;
  Holidays.First;
  while not Holidays.EOF do begin
    ccField := Trim(Holidays.FieldByName('cc_code').AsString);
    if (CallCenter = ccField) OR (ccField = '*') then
      Result := True;
    Holidays.Next;
  end;

  //Leave Active so we don't have to keep hitting server
end;

function TDM.ESketchFileMask(const TicketNumber: string): string;
begin
  Result := ESketchFilenamePrefix + '?' + TicketNumber + '-????-??-??-??-????';
end;

function TDM.GetAutoAttachFolderName: string;
begin
  Result := IncludeTrailingPathDelimiter(
    IncludeTrailingPathDelimiter(GetClientLocalDir) + 'Auto Attach');
end;

function TDM.HasPendingAutoAttachments: boolean;
//QM-966 EB Fix Required Pics and Notes
var
  AutoAttachFolder: string;
  lFileList: TStringLIst;
  i: integer;
  lCurName: string;
begin
   Result := False;
   AutoAttachFolder := GetAutoAttachFolderName;
   lFileList := TStringList.Create;
   try
     if not DirectoryExists(AutoAttachFolder) then
       Result := False
     else begin
        AdvBuildFileList(AutoAttachFolder + '*.*', faArchive, lFileList,amSubSetOf);
        for i := 0 to lFileList.Count - 1 do begin
          lCurName := lowercase(lFileList[i]);
          if (ansipos(picPrefix, lCurName) > 0 ) or
             (ansipos(movPrefix, lCurName) > 0 )  then begin
            Result := True;
            Break;
          end;
        end;
     end;
   finally
     FreeAndNil(lFileList);
   end;
end;


function TDM.AddAutoAttachments(const ForeignType, ForeignID: Integer;
  const TicketNumber: string='';
  const AttachmentFileMask: string='*.*';
  const AttachmentSource: string=AutoAttachmentSource;
  const WarnIfNoAttachments: Boolean=True): boolean;
var
  AutoAttachFolder: string;
  FailedAttachFolder: string;
  FileList: TStringList;
  ErrorList: TStringList;
  i: Integer;
  AttachCount: Integer;
  FileName: string;
  MovedOk: Boolean;
  AttachmentTicketNumber: string;
begin
  Result := False;
  AutoAttachFolder := GetAutoAttachFolderName;
  if not ForceDirectories(AutoAttachFolder) then
    raise Exception.CreateFmt('Unable to create auto attach folder %s.', [AutoAttachFolder]);

  FailedAttachFolder := IncludeTrailingPathDelimiter(AutoAttachFolder + 'Failed');
  if not ForceDirectories(FailedAttachFolder) then
    raise Exception.CreateFmt('Unable to create auto attach failed folder %s.',
      [FailedAttachFolder]);

  AttachCount := 0;
  ErrorList := nil;
  FileList := TStringList.Create;
  try
    ErrorList := TStringList.Create;
    AdvBuildFileList(AutoAttachFolder + AttachmentFileMask, faArchive, FileList,
      amSubSetOf);
    for i := 0 to FileList.Count-1 do begin
      try
        FileName := FileList[i];
        if (TicketNumber <> '') and IsESketchFilename(FileName,
          AttachmentTicketNumber) and not SameText(AttachmentTicketNumber,
          TicketNumber) then begin
          { TODO -odan -c3398 : Refactor? }
          raise Exception.Create('Error: eSketch attachment ticket number ' +
            'does not match the current ticket.');
        end;

        AddAttachmentToRecord(ForeignType, ForeignID, AutoAttachFolder +
          FileName, AttachmentSource);
        OdDeleteFile(AutoAttachFolder, FileName);
        Inc(AttachCount);
      except
        on E: Exception do begin
          MovedOk := FileMove(AutoAttachFolder + FileName, FailedAttachFolder + FileName, True);
          if MovedOk and (not FileExists(AutoAttachFolder + FileName)) then
            ErrorList.Add(FileName + ' - ' + E.Message)
          else
            ErrorList.Add(FileName + ' - [not moved] ' + E.Message);
        end;
      end;
    end;

    if ErrorList.Count > 0 then begin
      ShowMessageAckDialog('The files listed below could not be attached and were ' +
        'moved to the Auto Attach Failed folder:' + sLineBreak + sLineBreak +
        ErrorList.Text, 'Ok', 'WARNING: Files Not Attached');
    end;
    if WarnIfNoAttachments and (AttachCount < 1) and (not SameText(
      PermissionsDM.GetLimitation(RightAutoAttach), 'NoWarn')) then begin
      if not YesNoDialog('No images or files were found to attach.' + sLineBreak + sLineBreak +
        'Are you sure you want to save without attaching photos or screenshots?') then
        Abort;
    end;
    if AttachCount > 0 then       //QM-966 EB Fix Required Pics and Notes
      Result := True
    else
      Result := False;
      
  finally
    FreeAndNil(FileList);
    FreeAndNil(ErrorList);
  end;
end;

procedure TDM.CleanAutoAttachFailedFolder(const DaysOld: Integer);
var
  Path: string;
begin
  try
    Path := IncludeTrailingBackslash(GetAutoAttachFolderName) + 'Failed\';
    OdDeleteFile(Path, '*.*', DaysOld, False, False);
  except on E: Exception do
    MessageDlg('Unable to clean out failed auto attach folder: ' + E.Message, mtError, [mbOK], 0);
  end;
end;

function TDM.GetTicketWorkViewHTML(FullScreen: Boolean): string;
begin
  HighlightRule.ParamByName('call_center').Value := Ticket.FieldByName('ticket_format').AsString;
  Result := GetTicketWorkAsHTML(Ticket, HighlightRule, FullScreen);
end;

function TDM.GetLatestTicketVersionForTicket(TicketID: Integer): Variant;
const
  VersionSelect = 'select ticket_version_id, arrival_date ' +
    'from ticket_version where ticket_id=%d ' +
    'order by arrival_date desc top 1';
var
  VersionData: TDataSet;
begin
  VersionData := Engine.OpenQuery(Format(VersionSelect, [TicketID]));
  try
    if VersionData.IsEmpty then
      // TODO: Manual tickets don't have a version; what should we do about them?
      Result := Null
    else
      Result := VersionData.FieldByName('ticket_version_id').AsInteger;
  finally
    FreeAndNil(VersionData);
  end;
end;

procedure TDM.ShutdownWindows;
begin
  ShowTransientMessage('Q Manager is shutting down the PC');
  if UQState.DeveloperMode and (MessageDlg('DeveloperMode: Are you sure you want to close Q Manager ' + CRLF +
    'and shutdown your PC? Use care from Remote Desktop sessions!', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then begin
    ShowTransientMessage('Canceled Q Manager PC shutdown');
    Exit;
  end;

  if not ShutDownOS(klTimeOut) then
    raise Exception.Create('Unable to complete Windows shutdown request.');
end;

function TDM.AmIClockedIn: Boolean;
//Returns True if Work or callout period was started but not stopped
const
  SelectTimesheetDay = 'select * from timesheet_entry where work_emp_id = %d and work_date = %s and status = ''ACTIVE'' ';
var
  Data: TDataSet;
begin
  {$IF (CompilerVersion >= 16) and (CompilerVersion < 18)}
  Result := 0; // fixes BDS2005-RS2007 warning only
  {$IFEND}
  Data := Engine.OpenQuery(Format(SelectTimesheetDay, [EmpID, DateToDBISAMDate(Now)]));
  try
    Result := IsClockedIn(Data);
  finally
    FreeAndNil(Data);
  end;
end;

function TDM.GetLunchStart(const WorkDate: TDateTime): TDateTime;
const
  SelectLunchStart = 'select lunch_start from timesheet_entry '+
    'where work_emp_id = %d and work_date = %s and status = ''ACTIVE'' ';
var
  Data: TDataSet;
begin
  {$IF (CompilerVersion >= 16) and (CompilerVersion < 18)}
  Result := 0; // fixes BDS2005-RS2007 warning only
  {$IFEND}
  Data := Engine.OpenQuery(Format(SelectLunchStart, [EmpID, DateToDBISAMDate(WorkDate)]));
  try
    Result := Data.FieldByName('lunch_start').AsDateTime;
  finally
    FreeAndNil(Data);
  end;
end;

function TDM.IsLunchLockout: Boolean;
var
  UnlockTime: TDateTime;
begin
  UnlockTime := GetLunchUnlockTime;
  Result := (not FloatEquals(UnlockTime, 0)) and (UnlockTime > Time);
end;

function TDM.GetLunchUnlockTime: TDateTime;
//returns 0 if no lunch lock out
//returns time when QM's lunch lockout ends
const
  Select = 'select * from break_rules where active and pc_code = %s and rule_type in (%s)';
var
  LunchStartTime: TDateTime;
  RuleConfig: TDataset;
  BreakChecker: TBreakRulesChecker;
begin
  Result := 0;
  if AmIClockedIn then
    Exit;

  LunchStartTime := GetLunchStart(Today);
  if FloatEquals(LunchStartTime, 0) then //emps not using time clock screen won't have a lunch start
    Exit;

  Assert(LunchStartTime <= Time, 'Meal break start time cannot occur after the current system time.');
  BreakChecker := nil;
  RuleConfig := Engine.OpenQuery(Format(Select, [
    QuotedStr(DM.GetProfitCenterForEmployee(EmpID)),
    QuotedStr(BreakRuleTypeLunch)]));
  try
    if RuleConfig.IsEmpty then
      Exit;

    BreakChecker := CreateBreakRuleChecker(BreakRuleTypeLunch,
      RuleConfig.FieldByName('break_length').AsInteger,
      RuleConfig.FieldByName('break_needed_after').AsInteger);

    BreakChecker.Add(LunchStartTime, Time);
    if not BreakChecker.Check then // current time is within the lockout period
      Result := IncMinute(LunchStartTime, RuleConfig.FieldByName('break_length').AsInteger);
  finally
    FreeAndNil(BreakChecker);
    FreeAndNil(RuleConfig);
  end;
end;

function TDM.ConfirmShortLunchBreak: Boolean;
const
  Select = 'select * from break_rules where active and pc_code = %s and rule_type in (%s)';
var
  RuleConfig: TDataset;
  Msg: string;
begin
  Result := True;
  RuleConfig := Engine.OpenQuery(Format(Select, [
    QuotedStr(DM.GetProfitCenterForEmployee(EmpID)),
    QuotedStr(BreakRuleTypeLunch)]));
  try
    if RuleConfig.IsEmpty then
      Exit;

    Msg := RuleConfig.FieldByName('rule_message').AsString;
    if ShowMessageAckDialog(Msg, 'Yes, No') = 'Yes' then begin   // QMANTWO-237
      Result := True; //user is overriding lunch break
      DM.AddBreakRuleResponse(RuleConfig.FieldByName('rule_id').Value,
        RuleConfig.FieldByName('rule_message').AsString, '', Now);
    end else
      Result := False; //user decides not to override lunch break
  finally
    FreeAndNil(RuleConfig);
  end;
end;

procedure TDM.AssignLocalKeys(Data: TDataSet; const Prefix: string);
begin
  Assert(Assigned(Data));
  if not EditingDataSet(Data) then
    Data.Edit;
  Data.FieldByName('LocalKey').AsDateTime := Now;
  Data.FieldByName('LocalStringKey').AsString := Format('%s %s %s', [Prefix, UQState.UserName, GetLocalUserName]);
end;

function TDM.WeekHasAdditionalWorkTimes(EmpID: Integer; WeekStartDate: TDateTime): Boolean;
const
  Select = 'select count(*) as N from timesheet_entry ' +
    'where work_emp_id = %d and work_date between %s and %s ' +
    'and status <> ''OLD'' and ((work_start5 is not null) or (callout_start7 is not null))';
var
  TimeData: TDataset;
begin
  TimeData := Engine.OpenQuery(Format(Select, [EmpID,
    DateToDBISAMDate(WeekStartDate), DateToDBISAMDate(WeekStartDate+6)]));
  try
    Result := TimeData.Fields[0].AsInteger > 0;
  finally
    FreeAndNil(TimeData);
  end;
end;

{ RightDef: RightTrackGPSPosition
  - Used to determine if the user has the right to use gps tracking.
  - The modifiers are F,S,P. F = Threshold in feet to compute on site vs off site; S = GPS polling delay seconds; P = Port procedure/number. Default is 2000,5,-1.}
procedure TDM.BackgroundGPSStart;
var
  GPSPollSeconds: Integer;
  OldGPSPort: Integer;
  GPSPort: Integer;
  Limitations: TStringList;
begin
  //This will ensure that the old gps port will always be different at startup
  OldGPSPort := -9999;
  if Assigned(FBackgroundGPS) then
    OldGPSPort := FBackgroundGPS.FGPSPort;
    Assert(Assigned(Application.MainForm), 'CreateBackgroudGPS needs a handle to the main form');
    Limitations := TStringList.Create;
    try
      PermissionsDM.GetLimitationList(RightTrackGPSPosition, Limitations);
      GPSPort := -1;
      GPSPollSeconds := 0;
      if Limitations.Count > 1 then
        GPSPollSeconds := StrToIntDef(Limitations[1], 0);
      if Limitations.Count > 2 then
        GPSPort := StrToIntDef(Limitations[2], -1);
    finally
      FreeAndNil(Limitations);
    end;
//    if CanI(RightTrackGPSPosition) then begin
      if OldGPSPort <> GPSPort then begin
        BackgroundGPSStop;
        FBackgroundGPS := TBackgroundGPSThread.Create(Application.MainForm.Handle,
          UQState.DeveloperMode, FDataDir, GPSPollSeconds, GPSPort);
      end;
 //   end else begin
 //     BackgroundGPSStop;
//  end;
end;

procedure TDM.BackgroundGPSStop;
begin
  if Assigned(FBackgroundGPS) then begin
     FBackgroundGPS.Terminate;
     FBackgroundGPS.WaitFor;
     FreeAndNil(FBackgroundGPS);
    end;
end;

function TDM.GetMyGPSPosition(var Lat, Lon: Double; var HDOP: Integer): Boolean;
const
  MaxGPSAgeInSeconds = 60;
begin
  Result := WithinPastSeconds(Now, CurrentGPSFixTime, MaxGPSAgeInSeconds) and
    (CurrentGPSLatitude <> 0) and (CurrentGPSLongitude <> 0);
  if Result then begin
    Lat := CurrentGPSLatitude;
    Lon := CurrentGPSLongitude;
    HDOP := CurrentGPSHDOP;
  end;
end;

function TDM.SaveMyGPSPosition(TicketID: integer): Variant;  //QM-799 EB Input Ticket ID
var
  Lat, Lon: Double;
  HDOP: Integer;
  AddedDateTime: TDateTime;
begin
  Result := Null;
  if not GetMyGPSPosition(Lat, Lon, HDOP) then begin
    Result := 0;      //QM-860 Make sure that something comes back if we can't get the GPS
    Exit;
  end;

  AddedDateTime := Now;

      SaveGPSPosition.Open;
      Engine.LinkEventsAutoInc(SaveGPSPosition);
      SaveGPSPosition.Insert;
      SaveGPSPosition.FieldByName('added_by_id').Value := EmpID;
      SaveGPSPosition.FieldByName('added_date').Value := RoundSQLServerTimeMS(AddedDateTime);
      SaveGPSPosition.FieldByName('latitude').Value := Lat;
      SaveGPSPosition.FieldByName('longitude').Value := Lon;
      SaveGPSPosition.FieldByName('hdop_feet').Value := HDOP;
      if TicketID > 0 then
        SaveGPSPosition.FieldByName('LocalStringKey').Value := IntToStr(TicketID);
      PostDataSet(SaveGPSPosition);
      Result := SaveGPSPosition.FieldByName('gps_id').AsInteger;

end;

procedure TDM.SaveGPSPositionPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);   //qm-799  sr
begin
   Action:=daAbort;

   if (E is EDBISAMEngineError) then
      begin
      if (EDBISAMEngineError(E).ErrorCode=DBISAM_KEYVIOL) then
      begin
        ShowTransientMessage('A record with the same key value(s) '+
                             'already exists, please change the '+
                             'record to make the value(s) unique '+
                             'and re-post the record');
      end
//         ShowMessage('A record with the same key value(s) '+
//                     'already exists, please change the '+
//                     'record to make the value(s) unique '+
//                     'and re-post the record')
      else
         ShowMessage(E.Message);
      end
   else
      ShowMessage(E.Message);
end;

procedure TDM.CleanGPSLogFiles(const DaysOld: Integer);
begin
  try
    OdDeleteFile(FDataDir, GPSLogFilePrefix + '*.*', DaysOld, False, False);
  except on E: Exception do
    MessageDlg('Unable to clean out old GPS logs: ' + E.Message, mtError, [mbOK], 0);
  end;
end;


procedure TDM.UpgradeDatabase;
var
  TableNames: TStringList;
  Table: TDBISAMTable;
  i: Integer;
begin
  Table := nil;
  TableNames := TStringList.Create;
  try
    Table := TDBISAMTable.Create(nil);
    Table.DatabaseName := GetClientDataLocalDir;
    if not BuildFileList(AddSlash(GetClientDataLocalDir) + '*.dat', faHidden or faSysFile or faArchive, TableNames) then
      Exit;
    for i := 0 to TableNames.Count - 1 do begin
      Table.TableName := ChangeFileExt(ExtractFileName(TableNames[i]), '');
      if Table.Exists then begin
        // This does nothing if the version on the table and library match
        // A caught "DBISAM Engine Error #8965 Index Page buffers corrupt" here is normal.  Indexes are rebuilt in the new format anyway.
        Table.UpgradeTable;
      end;
    end;
  finally
    FreeAndNil(TableNames);
    FreeAndNil(Table);
  end;
end;

procedure TDM.SetEmployeeShowFutureWork(EmpID: Integer; ShowFutureTickets: Boolean);
begin
  CallingServiceName('SaveEmployeeShowFutureWork');
  Engine.Service.SaveEmployeeShowFutureWork(Engine.SecurityInfo, EmpID, ShowFutureTickets);
end;


function TDM.GetIDListFromObjects(List: TStrings): string;
var
  i: Integer;
begin
  Result := '';
  Assert(Assigned(List));
  for i := 0 to List.Count - 1 do
    if i > 0 then
      Result := Result + ',' + IntToStr(Integer(List.Objects[i]))
    else
      Result := IntToStr(Integer(List.Objects[i]));

end;

procedure TDM.WorkOrderBeforeOpen(DataSet: TDataSet);
begin
  Engine.LinkEvents(DataSet);

  AddLookupField('employee_name', DataSet, 'assigned_to_id', EmployeeDM.EmployeeLookup,
    'emp_id', 'short_name', ShortNameLenth, 'EmployeeNameLookup', 'Employee');
  AddLookupField('oc_code', DataSet, 'client_id', ClientLookup, 'client_id', 'oc_code');
  AddLookupField('client_name', DataSet, 'client_id', ClientLookup, 'client_id', 'client_name');
end;

procedure TDM.WorkOrderBeforePost(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to WorkOrder.Fields.Count - 1 do
    if WorkOrder.Fields[i] is TDateTimeField then
      if WorkOrder.Fields[i].AsDateTime = 0 then
        WorkOrder.Fields[i].Clear;
end;

procedure TDM.WorkOrderInspectionBeforePost(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to WorkOrderInspection.Fields.Count - 1 do //todo review; needed?
    if WorkOrderInspection.Fields[i] is TDateTimeField then
      if WorkOrderInspection.Fields[i].AsDateTime = 0 then
        WorkOrderInspection.Fields[i].Clear;

  if SameText(Trim(WorkOrderInspection.FieldByName('actual_meter_number').AsString),
    Trim(WorkOrderInspection.FieldByName('meter_number').AsString)) then
    WorkOrderInspection.FieldByName('actual_meter_number').AsString := '';
end;

function TDM.GetWorkOrderImage(WorkOrderData: TDataSet; var WOIsXml: Boolean): string;
const
  SQL = 'select xml_ticket_format from call_center where cc_code = ''%s''';
var
  Data: TDataSet;
begin
  Data := Engine.OpenQuery(Format(SQL, [WorkOrderData.FieldByName('wo_source').AsString]));
  try
    Result := GetDisplayableWorkOrderImage(WorkOrderData.FieldByName('image').AsString,
      Data.FieldByName('xml_ticket_format').AsString, WOIsXml);
    Result := StringReplace(Result, LF, CRLF, [rfReplaceAll]);
  finally
    FreeAndNil(Data);
  end;
end;

function TDM.GetWorkOrderNum(ID: Integer): string;    //QMANTWO-296
begin
  Result := '';
  try
    If GetWorkOrderNumQuery.Active then GetWorkOrderNumQuery.Close;
    GetWorkOrderNumQuery.ParamByName('wo_id').AsInteger := ID;
    GetWorkOrderNumQuery.Open;
    if GetWorkOrderNumQuery.HasRecords then begin
      GetWorkOrderNumQuery.First;
      Result := GetWorkOrderNumQuery.FieldByName('wo_number').AsString;
    end;
  finally
    GetWorkOrderNumQuery.Close;
  end;

end;

procedure TDM.StartTimeImportTimer;
begin
  TimeFileMonitorDM.TimeImportTimer.Enabled := PermissionsDM.CanI(RightTimesheetsClockImport);
end;

procedure TDM.CheckForApproachingEOD;  //QM-370  sr
begin
  if (SysUtils.Time > SysUtils.StrToTime('23:30')) and (ackedTimeClockIn<> true) then
  MessageDlg('You are nearing the end of the work day.  To continue past midnight, you must clock in for the new day.', mtConfirmation, [mbOK], 0);
  ackedTimeClockIn := true;
end;

procedure TDM.StopTimeImportTimer;
begin
  TimeFileMonitorDM.TimeImportTimer.Enabled := False;
end;

procedure TDM.InitializeHttpServer(IdleTimer: TOdIdleTimer);
begin
  Assert(Assigned(IdleTimer), 'The IdleTimer must exist.');
  if PermissionsDM.CanI(RightTimesheetsClockImport) then begin
    if not Assigned(DMHttp) then
      DMHttp := TDMHttp.Create(IdleTimer);
    DMHttp.ActivateHttpServer;
  end
  else begin
    if Assigned(DMHttp) then begin
      DMHttp.StopHttpServer;
      FreeAndNil(DMHttp);
    end;
  end;
end;

procedure TDM.SaveGpxToFile(D: TDataSet; const FileName: string; var GPXOptions: TGPXOptions);
var
  SaveFile: TFileStream;
begin
  SaveFile := MakeSaveFile(FileName);
  try
    if (D.Name = 'ListTable') then
      SaveWMGpxToStream(D, SaveFile, GPXOptions)  //QM-10 EB
    else
      SaveGpxToStream(D, SaveFile);
  finally
    FreeAndNil(SaveFile);
  end;
end;

procedure TDM.SaveWMGpxToStream(D: TDataSet; Stream: TStream; var GPXOptions: TGPXOptions);    //QM-10 EB
const
  XML_VERSION = '<?xml version="1.0" encoding="UTF-8"?>';
  GPX_START = '<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="QManager" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">';
  XMLRTE_START = '<rte>'; XMLRTE_END = '</rte>';

var
  GPX: TGPXArray;
  GPXSortList: TStringList;
  i: integer;
  XmlWpt, XmlRtePoint,
  XmlName, XmlDesc: string;
  CloneD: TClientDataSet;

  STREET: string;
  tempROStr: string;
  CityOrCounty: string;
begin
  GPXSortList := TSTringList.Create;
  D.DisableControls;
  try
    CloneD := TClientDataSet.Create(Self);
    CloneDataSet(D, CloneD, True);

  finally
    D.EnableControls;
  end;
  if GPXOptions.IncludeRouteOrder then  {If the employee is set to use Route Order, then order the list}
    CloneD.IndexFieldNames := 'route_order';
  try
    WriteStringToStream(Stream, XML_VERSION);
    WriteStringToStream(Stream, GPX_START);
    SetLength(GPX, CloneD.RecordCount);

 {1 - Save values to the GPX array}
 i := 0;
 CloneD.First;
 while not CloneD.EOF do begin
   STREET := CloneD.FieldByName('work_address_street').AsString;
      GPX[i].TicketNum := XmlEncodeString(CloneD.FieldByName('ticket_number').DisplayText);
      GPX[i].WorkLat := XmlEncodeString(CloneD.FieldByName('Lat').DisplayText);
      GPX[i].WorkLong := XmlEncodeString(CloneD.FieldByName('Lon').DisplayText);
      GPX[i].Name := XmlEncodeString(CloneD.FieldByName('work_address_number').DisplayText);
      GPX[i].Name := GPX[i].Name + ' ' + XmlEncodeString(CloneD.FieldByName('work_address_street').DisplayText);
      GPX[i].DueDate := FormatDateTime('m/d/yyyy hh:nn',CloneD.FieldByName('due_date').AsDateTime);
      GPX[i].Address := XmlEncodeString(CloneD.FieldByName('work_address_number').DisplayText);
      GPX[i].Street := XmlEncodeString(CloneD.FieldByName('work_address_street').DisplayText);
      GPX[i].City := XmlEncodeString(CloneD.FieldByName('work_city').DisplayText);
      GPX[i].County := XmlEncodeString(CloneD.FieldByName('work_county').DisplayText);
      GPX[i].State := XmlEncodeString(CloneD.FieldByName('work_state').DisplayText);
      GPX[i].LocatorShortName := XmlEncodeString(CloneD.FieldByName('short_name').DisplayText);
      tempROStr := xmlEncodeString(CloneD.FieldByName('route_order').DisplayText);
      if tempROStr = '0' then
        GPX[i].RouteOrder := 'N/A' //xmlEncodeString(CloneD.FieldByName('route_order').DisplayText);
      else
        GPX[i].RouteOrder := tempROStr;
      GPX[i].Kind := xmlEncodeString(CloneD.FieldByName('kind').DisplayText);
      GPX[i].WorkType := xmlEncodeString(CloneD.FieldByName('work_type').DisplayText);
      CloneD.Next;
      inc(i);
 end;
  {Remove Control Characters}  //QM-1029 EB Removing NBSP and Control Chars
  i:= 0;
  CloneD.First;
  while not CloneD.EOF do begin
   STREET := RemoveControlChars(STREET);
      GPX[i].TicketNum := RemoveControlChars(GPX[i].TicketNum);
      GPX[i].WorkLat := RemoveControlChars(GPX[i].WorkLat);
      GPX[i].WorkLong := RemoveControlChars(GPX[i].WorkLong);
      GPX[i].Name := RemoveControlChars(GPX[i].Name);

      GPX[i].DueDate := RemoveControlChars(GPX[i].DueDate);
      GPX[i].Address := RemoveControlChars(GPX[i].Address);
      GPX[i].Street := RemoveControlChars(GPX[i].Street);
      GPX[i].City := RemoveControlChars(GPX[i].City);
      GPX[i].County := RemoveControlChars(GPX[i].County);
      GPX[i].State := RemoveControlChars(GPX[i].State);
      GPX[i].LocatorShortName := RemoveControlChars(GPX[i].LocatorShortName);
      tempROStr := RemoveControlChars(tempROStr);
      if tempROStr = '0' then
        GPX[i].RouteOrder := 'N/A' //xmlEncodeString(CloneD.FieldByName('route_order').DisplayText);
      else
        GPX[i].RouteOrder := tempROStr;
      GPX[i].Kind := RemoveControlChars(GPX[i].Kind);
      GPX[i].WorkType := RemoveControlChars(GPX[i].WorkType);
      CloneD.Next;
      inc(i);
 end;


    {Create WayPoints with labels first}
    for i := 0 to CloneD.RecordCount - 1 do begin
      XmlWpt := '<wpt lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';

      if GPXOptions.IncludeRouteOrder then begin
        XmlName := '<name>' + 'RO# ' + GPX[i].RouteOrder + ' ' + GPX[i].Name + ' ' + GPX[i].DueDate + ' (Tkt# ' + GPX[i].TicketNum + ')' + '</name>' ;
        XmlDesc := '<desc>' + GPX[i].WorkLat + ',' + GPX[i].WorkLong + #13#10 + #13#10 +
                   'Ticket #: ' + GPX[i].TicketNum + #13#10 +
                   'Route Order #: ' + GPX[i].RouteOrder;

      end
      else begin
        XmlName := '<name>' + GPX[i].Name + ' ' + GPX[i].DueDate + ' (Tkt# ' + GPX[i].TicketNum + ')' + '</name>';
        XmlDesc := '<desc>' + GPX[i].WorkLat + ',' + GPX[i].WorkLong + #13#10 + #13#10 +
                   'Ticket #: ' + GPX[i].TicketNum;
      end;
      if (GPXOptions.IncludeEmpName) and (GPX[i].LocatorShortName <> '') then
        XMLDesc := XMLDesc + #13#10 +
                   'Locator: ' + GPX[i].LocatorShortName;

      if GPX[i].City <> '' then
        CityOrCounty := 'City: ' + GPX[i].City +  #13#10
      else
        CityOrCounty := 'County: ' + GPX[i].County + #13#10;

      XmlDesc := XmlDesc + #13#10 +
                  'Kind: ' + GPX[i].Kind + #13#10 + //' / ' + GPX[i].WorkType + #13#10 +
                  'Address: ' + GPX[i].Address + #13#10 +
                  'Street: ' + GPX[i].Street + #13#10 +
                  CityOrCounty + //'City: ' + GPX[i].City + #13#10 +
                  'State: ' + GPX[i].State + #13#10 + '</desc>';


      WriteStringToStream(Stream, XmlWpt);
      WriteStringToStream(Stream, XmlName);

      WriteStringToStream(Stream, XmlDesc);
      WriteStringToStream(Stream, '</wpt>');
    end;

    {Create Routes}
    if GPXOptions.IncludeRouteOrder then begin
      WriteStringToStream(Stream, XMLRTE_START);
      for i := 0 to CloneD.RecordCount - 1 do begin
        if GPX[i].RouteOrder <> 'N/A' then begin
          XmlRtePoint := '<rtept lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';
          XmlName := '<name>' + 'RO# ' + GPX[i].RouteOrder + ' ' + GPX[i].Name + ' ' + GPX[i].DueDate + ' (Tkt# ' + GPX[i].TicketNum + ')' + '</name>';
          XmlDesc := '<desc>' + GPX[i].WorkLat + ',' + GPX[i].WorkLong + #13#10 + #13#10 +
                       'Route Order #: ' + GPX[i].RouteOrder;


          if GPXOptions.IncludeEmpName then
             XMLDesc := XMLDesc + #13#10 +
                   'Locator: ' + GPX[i].LocatorShortName;

          XmlDesc := XmlDesc + #13#10 +
                  'Kind: ' + GPX[i].Kind + #13#10 + //' / ' + GPX[i].WorkType + #13#10 +
                  'Address: ' + GPX[i].Address + #13#10 +
                  'Street: ' + GPX[i].Street + #13#10 +
                  CityOrCounty + //'City: ' + GPX[i].City + #13#10 +
                  'State: ' + GPX[i].State + #13#10 + '</desc>';



          WriteStringToStream(Stream, XmlRtePoint);
          WriteStringToStream(Stream, XmlName);
          WriteStringToStream(Stream, '</rtept>');
        end;
      end;

      {To Catch non-RO Items mixed in with RO items}
      for i := 0 to CloneD.RecordCount - 1 do begin
        If (GPX[i].RouteOrder = 'N/A') then begin
          XmlRtePoint := '<rtept lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';
          XmlName := '<name>' + 'RO# ' + GPX[i].RouteOrder + ' ' + GPX[i].Name + ' ' + GPX[i].DueDate + ' (Tkt# ' + GPX[i].TicketNum + ')' + '</name>';
          XmlDesc := '<desc>' + GPX[i].WorkLat + ',' + GPX[i].WorkLong + #13#10 + #13#10;

          if (GPXOptions.IncludeEmpName) and (GPX[i].LocatorShortName <> '') then
             XMLDesc := XMLDesc + #13#10 +
                   'Locator: ' + GPX[i].LocatorShortName;

          XmlDesc := XmlDesc + #13#10 +
                  'Kind: ' + GPX[i].Kind + #13#10 + //' / ' + GPX[i].WorkType + #13#10 +
                  'Address: ' + GPX[i].Address + #13#10 +
                  'Street: ' + GPX[i].Street + #13#10 +
                  CityOrCounty + //'City: ' + GPX[i].City + #13#10 +
                  'State: ' + GPX[i].State + #13#10 + '</desc>';

          WriteStringToStream(Stream, XmlRtePoint);
          WriteStringToStream(Stream, XmlName);
          WriteStringToStream(Stream, '</rtept>');
        end
        else  {skip it ahead}
          Break;
      end;

    WriteStringToStream(Stream, XMLRTE_END);
    end; //end Routes

    WriteStringToStream(Stream, '</gpx>');

  finally
    FreeAndNil(GPXSortList);
    FreeAndNil(CloneD);
  end;

  Stream.Size := Stream.Position;
end;

//
//function TDM.ScanROforLocator(LocatorID: integer; var ARouteArray: TDyRouteArray): integer;   //QM-166 EB Route Order
//{Returns the number of records in fRouteArray}
//var
//  i, NewRec : integer;
//begin
//  NewRec := 0;
//  ROforLocator.Close;
//  ROforLocator.ParamByName('Locator_id').AsInteger := LocatorID;
////  ROforLocator.ParamByName('last_sync').AsDateTime := UQState.LastLocalSyncTime;
//  ROForLocator.Open;
//  ROForLocator.First;
//  While not ROforLocator.EOF do begin
//    SetLength(ARouteArray, NewRec + 1);
//    ARouteArray[NewRec].IsSet := True;
//    ARouteArray[NewRec].OrderNum := ROforLocator.FieldByName('route_order').AsFloat;
//    ARouteArray[NewRec].TicketID := ROForLocator.FieldByName('ticket_id').AsInteger;
//    inc(NewRec);
//    ROForLocator.Next;
//  end;
//
//  Result := NewRec;
//end;

procedure TDM.SaveGpxToStream(D: TDataSet; Stream: TStream);
const
  XML_VERSION = '<?xml version="1.0" encoding="UTF-8"?>';
  GPX_START = '<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="QManager" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">';
  XMLRTE_START = '<rte>'; XMLRTE_END = '</rte>';

var
  GPX: TGPXArray;
//  XmlRtePoint: string;
  IncludeRO: Boolean;
  i : integer;
  TheseAreTickets: Boolean;

  TicketLabel, TicketDesc, RoLabel, RODesc, KindDesc: string;
begin
  D.DisableControls;
  try
    TheseAreTickets := (D.Name = 'MyTickets');
    IncludeRO := PermissionsDM.CanI(RightTicketsUseRouteOrder, False);
    IncludeRO := IncludeRO and TheseAreTickets;  //No routing for WO/Damages for QM-51
    WriteStringToStream(Stream, XML_VERSION);
    WriteStringToStream(Stream, GPX_START);
    SetLength(GPX, D.RecordCount);
    i:=0;

    {Gather all the data}
    while not D.EOF do begin
      if TheseAreTickets then begin      //QM-51 EB Updated to match Work Management
        GPX[i].TicketNum := XmlEncodeString(D.FieldByName('ticket_number').DisplayText);
        GPX[i].Kind := xmlEncodeString(D.FieldByName('kind').DisplayText);
      end;
      GPX[i].WorkLat := XmlEncodeString(D.FieldByName('Lat').DisplayText);
      GPX[i].WorkLong := XmlEncodeString(D.FieldByName('Long').DisplayText);
      GPX[i].Name := XmlEncodeString(D.FieldByName('Name').DisplayText);
      GPX[i].DueDate := FormatDateTime('m/d/yyyy hh:nn',D.FieldByName('due_date').AsDateTime);
      GPX[i].Address := XmlEncodeString(D.FieldByName('Address').DisplayText);
      GPX[i].Street := XmlEncodeString(D.FieldByName('Street').DisplayText);
      GPX[i].City := XmlEncodeString(D.FieldByName('City').DisplayText);
      GPX[i].County := XmlEncodeString(D.FieldByName('County').DisplayText);  //QM-284 EB Fixed
      GPX[i].State := XmlEncodeString(D.FieldByName('State').DisplayText);
      if IncludeRO then
        GPX[i].RouteOrder := xmlEncodeString(D.FieldByName('route_order').DisplayText);
      if (GPX[i].RouteOrder = '0') or (not IncludeRO) then
        GPX[i].RouteOrder := 'N/A';
      D.Next;
      inc(i);
    end;

     {Remove Control Characters}  //QM-1029 EB Removing NBSP and Control Chars
     D.First;
     i := 0;
    while not D.EOF do begin
    if TheseAreTickets then begin      //QM-51 EB Updated to match Work Management
        GPX[i].TicketNum := RemoveControlChars(GPX[i].TicketNum);
        GPX[i].Kind := RemoveControlChars(GPX[i].Kind);
      end;
      GPX[i].WorkLat := RemoveControlChars(GPX[i].WorkLat);
      GPX[i].WorkLong := RemoveControlChars(GPX[i].WorkLong);
      GPX[i].Name := RemoveControlChars(GPX[i].Name);
      GPX[i].DueDate := RemoveControlChars(GPX[i].DueDate);
      GPX[i].Address := RemoveControlChars(GPX[i].Address);
      GPX[i].Street := RemoveControlChars(GPX[i].Street);
      GPX[i].City := RemoveControlChars(GPX[i].City);
      GPX[i].County := RemoveControlChars(GPX[i].County);
      GPX[i].State := RemoveControlChars(GPX[i].State);
      if IncludeRO then
        GPX[i].RouteOrder := RemoveControlChars(GPX[i].RouteOrder);
      if (GPX[i].RouteOrder = '0') or (not IncludeRO) then
        GPX[i].RouteOrder := 'N/A';
      D.Next;
      inc(i);
    end;

    {Create WayPoints with labels first}    //QM-51 EB Updated to match Work Management
    for i := 0 to D.RecordCount - 1 do begin
      GPX[i].WptText := '<wpt lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';


      if IncludeRO then begin
        ROLabel := 'RO# ' + GPX[i].RouteOrder + ' - ';
        RODesc :=  'Route Order #: ' + GPX[i].RouteOrder + #13#10;
      end
      else begin
        ROLabel := '';
        RODesc := '';
      end;

      if TheseAreTickets then begin
        TicketLabel := ' (Tkt# ' + GPX[i].TicketNum + ')';
        TicketDesc :=  'Ticket #: ' + GPX[i].TicketNum + #13#10;
      end
      else begin
        TicketLabel := '';
        TicketDesc := '';
      end;

      if GPX[i].Kind <> '' then
        KindDesc := 'Kind: ' + GPX[i].Kind + #13#10
      else
        KindDesc := '';

      GPX[i].LabelText := '<name>' + ROLabel + GPX[i].Name + ' ' + GPX[i].DueDate + TicketLabel + '</name>';
      GPX[i].DescText := '<desc>' + GPX[i].WorkLat + ',' + GPX[i].WorkLong + #13#10 + #13#10 +
                   TicketDesc +
                   RODesc +
                   KindDesc +
                   'Address: ' + GPX[i].Address + #13#10 +
                   'Street: ' + GPX[i].Street + #13#10 +
                   'City: ' + GPX[i].City + #13#10 +
                   'State: ' + GPX[i].State + #13#10 + '</desc>' + #13#10;

      WriteStringToStream(Stream, GPX[i].WptText);
      WriteStringToStream(Stream, GPX[i].LabelText);
      WriteStringToStream(Stream, GPX[i].DescText);
      WriteStringToStream(Stream, '</wpt>');
    end;

    {Create Routes}
    if IncludeRO then begin
      WriteStringToStream(Stream, XMLRTE_START);
      for i := 0 to D.RecordCount - 1 do begin
        if GPX[i].RouteOrder <> 'N/A' then begin
          GPX[i].RteText := '<rtept lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';
          WriteStringToStream(Stream, GPX[i].RteText);
          WriteStringToStream(Stream, GPX[i].LabelText);
          WriteStringToStream(Stream, '</rtept>');
        end;
      end;

      {To Catch non-RO Items mixed in with RO items}
      for i := 0 to D.RecordCount - 1 do begin
        If (GPX[i].RouteOrder = 'N/A') then begin
          GPX[i].RteText := '<rtept lat="' + GPX[i].WorkLat + '" lon="' + GPX[i].WorkLong + '">';
          WriteStringToStream(Stream, GPX[i].RteText);
          WriteStringToStream(Stream, GPX[i].LabelText);
          WriteStringToStream(Stream, '</rtept>');
        end
        else  {skip it ahead}
          Break;
      end;

      WriteStringToStream(Stream, XMLRTE_END);
    end; //end Routes

    WriteStringToStream(Stream, '</gpx>');

    D.EnableControls;
  finally
    /////
  end;
  Stream.Size := Stream.Position;
end;


function TDM.KillTask(ExeFileName: string): integer;
const
  PROCESS_TERMINATE=$0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle,ProcessHandl: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  while integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExeFileName)) or
       (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then
    begin
      ProcessHandl:=OpenProcess(PROCESS_TERMINATE,BOOL(0),FProcessEntry32.th32ProcessID) ;
      Result := Integer(TerminateProcess(ProcessHandl, 0));
      CloseHandle(ProcessHandl);
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

initialization
  Randomize;
  CheckApplicationVersion;

finalization
  FreeAndNil(PrivateRegisteredForms);

end.
