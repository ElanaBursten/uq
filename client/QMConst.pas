unit QMConst;

interface                                                     

type
  // Never change the order of these, or add to the middle of the list (only the end)
  TDamageLiability = (dlUQHas, dlUQNotHave, dlOnlyUQ, dlUQAndExcavator, dlNeither, dlAny);
  TOpenClosedCriteria = (ocAll, ocOpen, ocClosed);
  TAtachmentsCriteria = (acAll, acHas, acDoesNotHave);
  TWOTicketsCriteria = (wcAll, wcHas, wcDoesNotHave);
  TInvoicesCriteria = (icAll, icHas, icDoesNotHave);
  TManualTicketCriteria = (mtNone, mtManual, mtNeedsApproval, mtApproved);
  TTimesheetSearchType = (tsAll, tsApproval, tsFinalApproval);
  TTimesheetEditMode = (teNormal, teManager, teITOverride, teReadOnly);
  TEmployeeSelectMode = (esAll, esReportManangers, esTimesheetEmployees, esAllEmployeesUnderManager, esReportEmployees, esAllActiveEmployeesUnderManager);
  TAttachmentMode = (amStillImage, amDeviceNotification);
  TArrivalType = (atTicket, atDamage);
  TmaMapActivity=(maMapPlot, maWazeLookup,maDriveTimeDist,maViewTicketDetails,maMapOpen); //QMANTWO-711
  TmatMapActType=(matUser,matSystem);  //QMANTWO-711
const
  ApplicationFamilyName = 'Q Manager';
  QMApplicationMutex = 'UtiliQuestQManager';
  ShortNameLenth = 30;
  InvalidID = Low(Integer);
  HttpOK = 200;

  BlankTime = '00:00';  //QM-494 First Task EB
  QTimeFormat = 'hh:mm';  //QM-597 & QM-606 EB
  QMPTimeFormat = 'hh:mm:ss';  //QM-597 & QM-606 EB

 //reference table look-ups for StreetMaps -- QMANTWO-711
  PLOT =      'MAP_PLOT';
  WAZE =      'WAZE_LU';
  TIME_DIST = 'MAP_TIME_DIS';
  VIEW_DET =  'MAP_VIEW_DET';
  MAP_SHOW =  'MAP_OPEN';

  BillingEventLocate = 'LOCATE';
  BillingEventTicket = 'TICKET';
  BillingEventLabor = 'LABOR';

  TicketKindEmergency = 'EMERGENCY';
  TicketKindNormal = 'NORMAL';
  TicketKindOngoing = 'ONGOING';
  TicketKindDone = 'DONE';

  WorkOrderKindPriority = 'PRIORITY';
  WorkOrderKindCancel = 'CANCEL';

  WOSourceOHM = 'OHM';   //QMANTWO-387
  WOSourceWGL = 'WGL';
  WOSourceLAM01 = 'LAM01';
  WOSourceINR = 'INR'; //QMANTWO-434

  TicketTypeEmergency = 'EMERGENCY';
  TicketTypeMeet      = 'MEET';
  TicketTypeResched   ='-RS'; //QM-604 EB
  TicketTypeHP        = '-HP';

  CallCenterNewJersey = 'NewJersey';
  CallCenterAtlanta = 'Atlanta';

  DamageInvoiceCodeExpect = 'EXPECT';
  DamageInvoiceCodeNopay = 'NOPAY';

  OngoingTicketStatus: array [1..3] of string = (
    'O',
    'OX',
    'XOG');

  {Rescheduling Statuses}
  ReschedStatus = 'RS';
  OReschedStatus = 'ORS';

  // Right's entity_data defined in the right_definition table:
  // insert into right_definition (right_description, entity_data, right_type, modifier_desc)
  //   values ('Tickets - Edit Arrivals', 'TicketsEditArrivals', 'General', 'Limitation does not apply to this right.')
  RightReportScreen = 'ReportScreen';
  RightDamages = 'Damages';
  RightDamageLitigation = 'DamageLitigation';
  RightSearchTickets = 'SearchTickets';
  RightSearchWorkOrders = 'SearchWorkOrders';
  RightRunReports = 'RunReports'; // Unused
  RightEditLocates = 'EditLocates';
  RightRunReportForManager = 'RunReportForManager';
  RightStatusLocatesAsAssigned = 'StatusLocatesAsAssigned';
  RightDamageAttachments = 'DamageAttachments'; // Deprecated, do not use
  RightUploadGroup = 'UploadGroup';
  {Timesheet Permissions}
  RightTimesheetsSeeAll = 'TimesheetsSeeAll';
  RightTimesheetsApprove = 'TimesheetsApprove';
  RightTimesheetsFinalApprove = 'TimesheetsFinalApprove';
  RightTimesheetsApproveSelectAll = 'TimesheetsApproveSelectAll'; //QM-118 EB Timesheet Select All Permissions
  RightTimesheetsExport = 'TimesheetsExport';
  RightTimesheetsUnApprove = 'TimesheetsUnApprove';
  RightPOVCOVEntry = 'POVCOVEntry';
  RightTimesheetsEntry = 'TimesheetsEntry';
  RightTimesheetsClockEntry = 'TimesheetsClockEntry';
  RightTimesheetsClockImport = 'TimesheetsClockImport';
  RightTimesheetsPriorDayEntry = 'TimesheetsPriorDayEntry';
  RightTimesheetsPerDiemEntry = 'TimesheetsPerDiemEntry';  //QMANTWO-789  SR
  RightTimesheetsAddYesterdayCallout='TimesheetsAddYesterdayCallout';//QM-442 SR Allows adding Callouts for day before
  RightTimesheetsUtiliSafe = 'TimesheetsUtiliSafe'; //QM-100 EB Timesheet UtiliSafe Link
  RightTimesheetsEmpTypeEntry = 'TimesheetsEmpTypeEntry';
  RightTimesheetsViewSickLeave = 'TimeSheetsViewSickLeave'; //QM-517 Show Sick Leave again on timesheet
  RightTimesheetsUTA = 'TimesheetsUTA';  //QM-613 EB Timesheet UTA
  {Damage Permissions}
  RightDamagesSetClaimStatus = 'DamagesSetClaimStatus';
  RightDamagesApprove = 'DamagesApprove';
  RightDamagesAssign = 'DamagesAssign';
  RightDamagesViewAll = 'DamagesViewAll';
  RightDamagesSetInvoiceCode = 'DamagesSetInvoiceCode';

  RightTicketManagement = 'TicketManagement';
//Currently not used:  RightTicketManagementV2 = 'TicketManagementV2'; // temporary access to new TM screen
  RightCreateNewTicket = 'CreateNewTicket';

  RightViewTicketHistory = 'ViewTicketHistory';
  RightViewWorkOrderHistory = 'ViewWorkOrderHistory';
  RightViewTicketBilling = 'ViewTicketBilling';
  RightManageAssets = 'ManageAssets'; // Edit comments/condition only, assign, view
  RightTicketsApproveManual = 'TicketsApproveManual';
  RightBillingAdjustmentsView = 'BillingAdjustmentsView';
  RightBillingAdjustmentsAdd = 'BillingAdjustmentsAdd';
  RightDamagesV2 = 'DamagesV2'; // This is treated exactly the same as RightDamages
  RightLitigationView = 'LitigationView';
  RightLitigationEdit = 'LitigationEdit';
  RightBillingRun = 'BillingRun';
  RightBillingDelete = 'BillingDelete';
  RightBillingCommit = 'BillingCommit';
  RightBillingUncommit = 'BillingUncommit';
  RightBillingReexport = 'BillingReexport';
  RightTicketsAssignArea = 'TicketsAssignArea';
  RightDamagesApproveInvoice = 'DamagesApproveInvoice';
  RightDamagesBillDamage = 'DamagesBillDamage';
  RightDamagesAddEstimate = 'DamagesAddEstimate';
  RightMessagesDeleteAny = 'MessagesDeleteAny';
  RightMessagesSend = 'MessagesSend';
  RightMessagesReview = 'MessagesReview';
  RightEditAssets = 'EditAssets'; // Edit anything on an asset, delete, add
  RightSyncReminderInterval = 'SyncReminderInterval';
  RightUploadAnything = 'UploadAnything';
  RightAutoSyncAndUpload = 'AutoSyncAndUpload';
  RightRestrictUsageLimitHours = 'RestrictUsageLimitHours';
  RightRestrictUsageGrantExemption = 'RestrictUsageGrantExemption';

  RightTicketsEditFacilities = 'TicketsEditFacilities';
  RightTicketsAddLocates = 'TicketsAddLocates';
  RightTicketsAddLocatesWithLocator = 'TicketsAddLocatesWLocator';  //EB QM-142  Different Locator on Ticket
  RightTicketsEditArrivals = 'TicketsEditArrivals';
  RightTicketsChangeViewLimits = 'TicketsChangeViewLimits';
  RightRoutingEditAreas = 'RoutingEditAreas';
  RightTicketsShowWithoutArriving = 'TicketsShowWithoutArriving';
  RightTicketsImmediateSync = 'TicketsImmediateSync';
  RightWorkOrdersImmediateSync = 'WorkOrdersImmediateSync';
  RightTicketsAcknowledgeActivity = 'TicketsAcknowledgeActivity';
  RightTicketsAcknowledgeAllActivities = 'TicketsAcknowledgeAllActivities';
  RightUpdatePhoneNumber='ManageAssets';  //qm-388 sr
  RightTicketsAutoApproveNewTicket = 'TicketsAutoApproveNewTickets';
  RightTicketsAssignNewTicket = 'TicketsAssignNewTickets';
  RightWorkOrdersAssignNewWorkOrders = 'WorkOrdersAssignNewWorkOrders';
  RightTicketsAddReadyToMarkLocate = 'TicketsAddReadyToMarkLocates';
  RightTicketsAssignNewLocatesToTicketLocator = 'TicketsAssignNewLocatesToTicketLocator';
  RightTicketsCreateFollowUp = 'TicketsCreateFollowUp';
  RightTicketsEditClosedLocates = 'TicketsEditClosedLocates';
  RightTicketsViewAnotherLocatorsLocates = 'TicketsViewAnotherLocatorsLocates';
  RightTicketsEditAnotherLocatorsLocates = 'TicketsEditAnotherLocatorsLocates';
  RightTicketsEditAnotherLocatorsUnits = 'TicketsEditAnotherLocatorsUnits';
  RightTicketsShowFullWorkScreen = 'TicketsShowWorkFullScreen';
  RightTicketsAllowStatusCodes = 'TicketsAllowStatusCodes';
  RightTicketsAllowRestrictedStatusCodes = 'TicketsAllowRestrictedStatusCodes';
  RightTicketsReleaseForWork = 'TicketsReleaseForWork';
  RightTicketsSelectiveExport ='TicketsSelectiveExport';
  RightTicketsManageFutureTickets = 'TicketsManageFutureTickets';
  RightTicketsShowFutureWorkload = 'TicketsShowFutureWorkload';
  RightTicketsUseRouteOrder = 'TicketsUseRouteOrder';  //QMANTWO-775 Orig Route Order EB
  RightTicketsUseRouteOptimize = 'TicketsUseRouteOptimize'; //QM-702 Route Order Optimize EB
  RightTicketsPastDueTree = 'TicketsPastDueTree';  //QM-248 & QM-133 Past Due Permission EB
  RightTicketsOpenTree = 'TicketsOpenTree'; //QM-428 Open Items EB
  RightTicketsOpenExpTree = 'TicketsOpenExpTree'; //QM-695 Open Tickets List Expanded EB
  RightTicketsTodayOpenPastDueTree = 'TicketsTodayOpenPastDueTree'; //QM-318 Past Due & Today Permission  EB
  RightTicketsVirtualBucket = 'TicketsVirtualBucket'; //QM-486 Virtual Bucket EB
  RightTicketsHideAttachmentsForClosed = 'TicketsHideAttachmentsForClosed'; //QM-255 Hide Attachments for Closed Tickets
  RightTicketsViewUtilIcons = 'TicketsViewUtilIcons';  //QM-306 Add Icons to Work Management Grid EB
  RightTicketsPreserveClosedDate = 'TicketsPreserveClosedDate'; //QM-463 Closed Date EB
  RightTicketsSetFirstTaskReminder = 'TicketsSetFirstTaskReminder'; //QM-494 First Task EB
  RightTicketsViewFirstTaskReminder = 'TicketsViewFirstTaskReminder'; //QM-494 First Task EB
  RightTicketsCacheAutoClear = 'TicketsCacheAutoClear'; //QM-986 Tickets Clear Cache on First Sync  EB

  RightShowOldTickets = 'ShowOldTickets'; //QM-754 OldTicketsButton SR
  RightDamagesAssignLocator = 'DamagesAssignLocator';
  RightRemoveAttachments = 'AttachmentsRemove';
  RightDamagesViewAccrualHistory = 'DamagesViewAccrualHistory';

  RightAttachmentsBackgroundUpload = 'AttachmentsBackgroundUpload';
  RightSyncAfterIdle = 'SyncAfterIdle';
  RightSyncAfterTicketSave = 'SyncAfterTicketSave';
  RightSyncAfterTimesheetSave = 'SyncAfterTimesheetSave';
  RightAutoAttach = 'AttachmentsAutoAttach';
  RightWorkAfterWorkClockOut = 'WorkAfterWorkClockOut';
  RightWorkBeforeClockIn = 'WorkBeforeClockIn';
  RightWorkDuringLunch = 'WorkDuringLunchBreak';
  RightTrackGPSPosition = 'TrackGPSPosition';
  RightDamagesEditWhenApproved = 'DamagesEditWhenApproved';
  RightDamagesEditWithoutArriving = 'DamagesEditWithoutArriving';
  RightDamagesEditArrivals = 'DamagesEditArrivals';
  RightDamagesAssignProfitCenter = 'DamagesAssignProfitCenter';
  RightDamagesManagement = 'DamagesManagement';
  RightDamagesSelectiveExport = 'DamagesSelectiveExport';
  RightWorkOrdersManagement = 'WorkOrdersManagement';
  RightWorkOrdersSelectiveExport = 'WorkOrdersSelectiveExport';
  RightAttachmentsAddToClosedWorkOrder = 'AttachmentsAddToClosedWorkOrder';
  RightSyncMyComputerSerialNum = 'SyncMyComputerSerialNum';
  RightMapAllLocators = 'MapAllLocators'; //QMANTWO-302
  RightMultiSelPriorityChange = 'MultiSelectPriorityChange'; //QMANTWO-564 EB New Permission
  RightWhichWazeToUse = 'WhichWaze';//QMANTWO-646 New Permission SR
  RightMultiLocatorsForProjTicket = 'MultiLocatorsForProjTicket'; //QMANTWO-616 EB
  RightEnableTicketStreetMap = 'TicketStreetMap';  //QMANTWO-763  SR

  RightTicketViewEmergencies = 'EmerViewer';  //QM-11  SR
  RightPowershellBootup = 'UsePowershellBootup';  //QM-147 EB Use Powershell for bootup
  RightEditTicketDueDate = 'TicketDetailDueDate'; //QM-216 for Ca. tickets statused -RS     SR
  RightWorkManagementBulkDueDate = 'BulkTicketDueDate'; //QM-959 EB
  RightNotesDeleteNote = 'NotesDeleteNote';  //QM-188 EB Delete Note
  RightNotesChangeRestriction = 'NotesChangeRestriction'; //QM-188 EB Change Note To Private
  RightNotesChangeNoteText = 'NotesChangeNoteText'; //QM-188 EB Change Note Text


  RightLimitationOptional = 'OPTIONAL';
  RightLimitationUndefined = '(Limitation/modifier usage is not yet defined)';

  ReportRightType = 'Report';

  RightWorkManagementExport = 'ManageWorkExport';   //QM-10 EB Moving button visibility to QManagerDebugConfig.ini
  RightPlatsManagerUpdate = 'PlatsManagerUpdateAll';

  RightOQList = 'MainShowOQList';  //QM-444 Part 2 EB
  RightEmpPlus = 'EmployeePlusWhitelist';  //QM-585 EB

  RightTicketsMARSBulkStatusMgmt = 'TicketsMARSBulkStatusMgmt';  //QM-671 EB MARS

  RightTimesheetsEntryTSE = 'TimesheetsTSE'; //QM-833 TSE This is at the Employee level

  // This length is lost in the DBISAM conversion due to it becoming a memo field
  MaxNoteLength = 8000;

  // The notes table has a foreign_type field defining the
  // table the note references.  These are the valid values:
  qmftTicket = 1;
  qmftDamage = 2;
  qmft3rdParty = 3;
  qmftLitigation = 4;
  qmftLocate = 5;
  qmftDamageNarrative = 6;
  qmftWorkOrder = 7;

  bEmerg = 'EMERGENCY';
  bAct = 'ACTIVITY';
  bPD = 'PAST DUE';
  bOpen = 'OPEN';
  bTODAY = 'TODAY';
  bVirtual ='VIRTUAL';
  bLoc = 'LOCATOR';

  ForeignTypeDescription: array [1..7] of string = (
    'Ticket',
    'Damage',
    '3rd Party',
    'Litigation',
    'Locate',
    'Damage Narrative',
    'Work Order');

  PublicTicketSubType = 100;
  PrivateTicketSubType = 101;
  HighProfileTicketSubType = 102;
  PublicLocateSubType = 500;
  PrivateLocateSubType = 501;
  HighProfileLocateSubType = 502;
  PublicDamageSubType = 200;
  PrivateDamageSubType = 201;
  PublicWorkOrderSubType = 700;
  PrivateWorkOrderSubType = 701;

  {AUTO NOTES  QM-933 EB - Notes automatically entered when an event occures.
                           These are used to set note and to also set the coloring in the
                           notes grid. Some phrases may be incomplete here.
						   Moved from other areas of application to consolidate}
    NoteChangeCodes: array [1..4] of string = (
     'NOTE DELETED [code 01] by',
     'Restriction changed to PRIVATE [code 02] by',
     'Restriction changed to PUBLIC [code 03] by',
     'NOTE TEXT CHANGED [code 04] by');

    NoteStatusedasAssigned = ' statused as assigned locator by ';
    NoteWorkPriority = 'Work priority set to ';

    HighProfileNotePrefix = 'HPR-';
      HighProfileOnMsg = 'This locate has been marked High Profile';
      HighProfileOffMsg = 'Removed High Profile Designation from this locate';
      XHighProfileNotePrefix = 'X' + HighProfileNotePrefix;
    QANotePrefix = 'Q/A - ';
    ROPrefix = 'Route Order: [code R1] ';  {Note used for auto note detection currently EB}
    NoteROSkipped = 'Route Order: This ticket was skipped for the following reason:';   //qm-747 sr
    NoteROSkippedOld = 'Route Order: The following ticket was skipped for the following reason:';  //QM-
    NoteModDueDateTxt = ' modifed the due date from ';  //QM-216 EB
    NoteModDueDateType = 'TDD';
    NoteAreaChanged = 'Area changed to';
    NoteExternal = '[external]'; //QM-905 EB Notes Coloring - this is a temp
    BulkModDueDateTxt1 = ' modifed the due date to %s ';        //QM-959 EB Bulk Due Date
    BulkModDueDateTxt2 = 'as part of a bulk due date change';  //QM-959 EB Bulk Due Date
    HPEMAILNOSEND = '<NOSEND HP EMAIL>';  //QM-1076 EB High Profile Restriction

  {Note types}  //QM-966 Fix for Picture and Note Required
  notecatUserEnterd  = 1;
  notecatHP          = 2;
  notecatQA          = 3;
  notecatAccent      = 5;
  notecatAutoGen     = 6;
  notecatROSkipped   = 7;
  notecatAppExternal = 20;

  qmestAll = -1;
  qmestDamage = 0;
  qmest3rdParty = 1;
  qmestLitigation = 2;

  qminvAll = -1;
  qminvDamage = 0;
  qminv3rdParty = 1;
  qminvLitigation = 2;

  PasswordChangeNone = 0;
  PasswordChangeSuggest = 1;
  PasswordChangeForce = 2;

  {TimeSheet}
  TimesheetNumWorkPeriods = 16;
  TimesheetNumCalloutPeriods = 16;
  TimesheetSearchXMLDownload = -1;
  TimesheetUTAEnteredBy = 20;  //QM-613 Timesheet UTA: reserved ID for UTA Entry
  TimesheetRtasqSource = 'RTASQ';  //QM-760 Timesheet RTASQ (incoming will be "Rtasq" but we will set case to keep from errors later.
  TimesheetTSESource = 'TSE';  //QM-833 Timesheet TSE EB
  TimesheetClockSource = 'clock'; //QM-833 Timesheet TSE EB
  TimesheetSearchDescription: array[TTimesheetSearchType] of string = (
    'All timesheets',
    'Need approval',
    'Need final approval'
  );

  TimesheetStatusSubmit = 'SUBMIT';
  TimesheetStatusActive = 'ACTIVE';
  TimesheetStatusOld = 'OLD';
  TimesheetInvalid = 'INVALID';

  HighTolerance = 0.000001;  //QM-606 EB Precision

  OpenClosedDescription: array[TOpenClosedCriteria] of string = (
    'All',
    'Open',
    'Closed'
    );



  TicketStatusManual = 'MANUAL';
  TicketStatusManualApproved = 'MANUAL APPROVED';
  TicketStatusManualDenied = 'MANUAL DENIED';
  ManualTicketNumberPrefix = 'MAN-';

  CompanyCarNone = 'NONE';
  CompanyCarCOV = 'COV';

  DamageTypeIncoming = 'INCOMING'; // Automatically created damage from a ticket
  DamageTypePending = 'PENDING'; // Open and assigned to an investigator
  DamageTypeCompleted = 'COMPLETED'; // Investigation done..old
  DamageTypePendingApproval = 'PENDING APPROVAL'; // replacing 'COMPLETED'
  DamageTypeApproved = 'APPROVED'; // Investigation approved by management

  TicketTypeWatchAndProtect = 'WATCH AND PROTECT';

  SyncResponseDataRow = 'sync_response_data';

  MatchTypeStarts = 'STARTS';
  MatchTypeEnds = 'ENDS';
  MatchTypeIs = 'IS';
  MatchTypeContains = 'CONTAINS';

  ONSITE = 'ONSITE';
  OFFSITE = 'OFFSITE';

  PLAT_FILE_PATH = 'C:\Tools\_Base\PlatHistory.html'; //QM-23 sr
type
  TTransferSpeed = record
    Description: string;
    KBPerSec: Integer;
  end;

const
  TransferSpeeds: array[0..3] of TTransferSpeed = (
    (
      Description: 'Mobile Phone';
      KBPerSec: 3;
    ),
    (
      Description: 'Analog Modem';
      KBPerSec: 5;
    ),
    (
      Description: 'Cable Modem';
      KBPerSec: 200;
    ),
    (
      Description: 'T1/LAN';
      KBPerSec: 800;
    )
  );

  END_OF_QM_REPORT_PARAMS = 'END_OF_QM_REPORT_PARAMS';

  AdjTypeAmount  = 'AMOUNT';
  AdjTypePercent = 'PERCENT';
  AdjTypeComment = 'COMMENT';

  // Responder Status Translations active status for reports
  StatusInactive = 0;
  StatusActive   = 1;
  StatusAll      = 2;

  // Employee rights status for reports
  EmpRightDenied  = 0;
  EmpRightGranted = 1;
  EmpRightAll     = 2;


  // Damage Detail Report Modes
  DamageReportOnDamage = 'ShowDamage';
  DamageReportOnThirdParty = 'ShowThirdParty';
  DamageReportOnLitigation = 'ShowLitigation';

  // Attachments Directory Formatting
  DirFormatForAttachments = 'yyyy-mm-dd';

  // Employee Activity Logging event types
  ActivityTypePCBoot = 'PWRON';
  ActivityTypePCSleep = 'SLEEP';
  ActivityTypePCWake = 'WAKE';
  ActivityTypePCShutdown = 'PWROFF';
  ActivityTypeWinLogOn = 'WINON';
  ActivityTypeWinLogOff = 'WINOFF';
  ActivityTypeWinLock = 'WINLCK';
  ActivityTypeWinUnLock = 'WINULK';
  ActivityTypeWinConnect = 'WINCON';
  ActivityTypeWinDisconnect = 'WINDIS';
  ActivityTypeWinUserChange = 'WINUCH';
  ActivityTypeQuit = 'QUIT';
  ActivityTypeSyncFailed = 'SYNCF';
  ActivityTypeAutoSyncFailed = 'SYNCAF';
  ActivityTypeACKEmergTicket = 'ACKEMERG';  //QM-838 EB Emergency Ticket Ack
  ActivityTypeViewTicket = 'TKVUE';
  ActivityTypeRouteTicket = 'TKRTE';
  ActivityTypeFindTicket = 'TKFND';
  ActivityTypeFindWorkOrder = 'WOFND';
  ActivityTypeStart = 'START';
  ActivityTypeViewMessage = 'MSGV';
  ActivityTypeNewTicket = 'TKNEW';
  ActivityTypeMoveTicket = 'TKMOV';
  ActivityTypeMoveWorkOrder = 'WOMOV';
  ActivityTypeWorkStart = 'WSTAR';
  ActivityTypeWorkStop = 'WSTOP';
  ActivityTypeCalloutStart = 'CSTAR';
  ActivityTypeCalloutStop = 'CSTOP';
  ActivityTypeSync = 'SYNC';
  ActivityTypeAddNote = 'ADDNT';
  ActivityTypeLocateStatus = 'LOCST';
  ActivityTypeLocateStatusAsAssignedLocator = 'LOCSTA';
  ActivityTypeAttachmentUpload = 'ATUPL';
  ActivityTypeLocateHours = 'LOCHR';
  ActivityTypeRunReport = 'RUNRE';
  ActivityTypeTimeSheetEntry = 'TSENT';
  ActivityTypeTimeEntryImport = 'TEIMP';
  ActivityTypeApproveTime = 'APRTS';
  ActivityTypeWebUpdate = 'WEBUPD';
  ActivityTypeAttachmentAdd = 'ATADD';
  ActivityTypeTimeSubmitMsgAck = 'TESUBM';
  ActivityTypeTicketAttachAddinStart = 'TIADST';
  ActivityTypeTicketAttachAddinEnd   = 'TIADEN';
  ActivityTypeAddinActivity = 'ADDACT';
  ActivityTypeDamageAttachAddinStart = 'DMADST';
  ActivityTypeDamageAttachAddinEnd   = 'DMADEN';
  ActivityTypeTicketScheduled = 'TKSCHED';
  ActivityTypeTicketPlatAddinStart = 'TIPAST';
  ActivityTypeTicketPlatAddinEnd = 'TIPAEN';
  ActivityTypeTicketViewWorkStart = 'TIVWKST';
  ActivityTypeTicketViewWorkEnd = 'TIVWKEN';
  ActivityTypeTicketRouteExport = 'TKRTEXP';
  ActivityTypeLunchBreakException = 'TELUNCHEX';
  ActivityTypeStartTimeOverride = 'TESTRTOVRD';  // QMANTWO-235
  ActivityTypeDamageNote = 'DMGNOTE';
  ActivityTypeDamageArrival = 'DMGARRV';
  ActivityTypeDamageAddAttachment = 'DMGATTCH';
  ActivityTypeDamageUploadAttachment = 'DMGATTUP';
  ActivityTypeDamageSaveChanges = 'DMGSAVE';
  ActivityTypeDamageComplete = 'DMGCOMP';
  ActivityTypeDamageApproved = 'DMGAPPR';
  ActivityTypeDamageAdd = 'DMGADD';
  ActivityTypeFindDamage = 'DMGFND';
  ActivityTypeViewDamage = 'DMGVUE';
  ActivityTypeDamageAddEstimate = 'DMGEST';
  ActivityTypeDamageAddInvoice = 'DMGINV';
  ActivityTypeDamageThirdPartyAdd = 'DMGTHDPTYADD';
  ActivityTypeDamageThirdPartyView = 'DMGTHDPTYVUE';
  ActivityTypeDamageAddThirdPartySaveChanges = 'DMGTHDPTYSAV';
  ActivityTypeDamageAddLitigationAdd = 'DMGLITGADD';
  ActivityTypeDamageAddLitigationView = 'DMGLITGVUE';
  ActivityTypeDamageAddLitigationSaveChanges = 'DMGLITGSAVE';
  ActivityTypeDamageRouteExport = 'DMGRTEXP';
  ActivityTypeViewWorkOrder = 'WOVUE';
  ActivityTypeWorkOrderClosed = 'WOCLOSE';
  ActivityTypeWorkOrderSaved = 'WOSAVE';
  ActivityTypeWOAttachAddinStart = 'WOADST';
  ActivityTypeWOAttachAddinEnd = 'WOADEN';
  ActivityTypeWorkOrderRouteExport = 'WORTEXP';
  ActivityTypeWorkOrderPlatAddinStart = 'WOPAST';
  ActivityTypeWorkOrderPlatAddinEnd = 'WOPAEN';

  ActivityTypeUtiliSafe = 'UTILISAFE';   //QMANTWO-100 EB

  ActivityTypeSerialMismatch = 'SERIALMISMATCH';
  // New activities should also be added to the reference table: insert into reference(type, code, description) values ('empact', 'ACTCODE', 'Activity Description')

  // Ticket Activity Types
  TicketActivityTypeArrive = 'Arrived';
  TicketActivityTypeComplete = 'Completed';
  TicketActivityTypeOngoing = 'Ongoing';
  TicketActivityTypePartial = 'Partial';

  SecondsToAutoSyncDialog = 5;

  MyComputerCLSID = '::{20D04FE0-3AEA-1069-A2D8-08002B30309D}';

  // Usage Restriction Types
  UsageRestrictionTypeWeekday = 'WEEKDAY';
  UsageRestrictionTypeWeekend = 'WEEKEND';

  NeverSendFieldNames: array[0..3] of string = (
    'DeltaStatus',
    'modified_date',
    'LocalKey',
    'LocalStringKey');

  NeverSendFieldNamesNoModifiedDate: array[0..2] of string = (
    'DeltaStatus',
    'LocalKey',
    'LocalStringKey');

  AddinsIni = 'Addins.ini';
  AddinAttachmentSource = 'ADDIN';
  WhiteLineAttachmentSource = 'WLADDIN';
  ManualAttachmentSource = 'MANAWS';   //QMANTWO-552 EB Previously 'MANUAL'  no conditionals
  AutoAttachmentSource = 'AUTOAWS';    //QMANTWO-552 EB Previously "AUTO'
  PhotoAttachmentSource = 'PHOTO';
  CopyFromWorkOrderSource = 'WORKORDR';
  CustomerAttachmentSource = 'CUSTOMER';
  DocumentTypeManifest = 'MANIFEST';

  PersequorProgramFileName = 'Persequor\Persequor.exe';

  MemeProgramFileName = 'UtiliQuest\Meme\Meme.exe';      //qm-972 sr 
  //We need to determine where to look for versioning of these services or executable
  TicketPriorityAny = 'Any';
  TicketPrioritySkipped = 'SKIPPED';

  TableEditableAreas = 'editable_areas';

  ImageFilesModifier = 'Image';
  GPSLogFilePrefix = 'QM-GPS-';

  ReferenceTypePrintableFile = 'printable';
  ReferenceTypeUnknown = 'Unknown';

  {Colors}
  WarningColor = $00BBBBFF;
  clLightRed = 10987499;
  cl9SofterYellow = $6FFFFF;  //QM-991
  cl9LightMaroon = $0000C9;  //QM-991  {Between Red and Maroon}

  NotAClientStatus = '-N';
  ParsingStatus = '-P';
  ReadyToWorkStatus = '-R';
  //ReschedStatus ='-RS';
  CGAStatus = 'CGA';//used by WGL for gas inspection work order
  EmergencyAckStatus = 'ACK';

  InternalStatuses: array [0..1] of string = (NotAClientStatus, ParsingStatus);

  {reference codes for employee types - for special query handling}
  TOMCODE = 'UQTOM';  //QM-938 EB Emp Search Changes

  //WGL Gas Inspection work order work types (these correspond to work_order_work_type table; not all are listed here)
  //TODO: These may be removed if the screen is redesigned to no longer need them.
  WorkTypeVC = 'VC'; //Inadequate Vent Clearance Obstructions
  WorkTypeVI = 'VI'; //Inadequate Vent Clearance Ignition Sources
  WorkTypeNOAOC = 'No AOC Observed'; //Description vs code is used

type
  TPayrollHourFieldNames = record
    FieldName: string;
    DisplayName: string;
  end;

const
  PayrollHourFieldNameArray: array[0..14] of TPayrollHourFieldNames = (
    (
      FieldName: 'total_hours';
      DisplayName: 'Total (All Types)';
    ),
    (
      FieldName: 'working_hours';
      DisplayName: 'Working (RT+OT+DT+CO)';
    ),
    (
      FieldName: 'reg_hours';
      DisplayName: 'Regular Time';
    ),
    (
      FieldName: 'ot_hours';
      DisplayName: 'Overtime';
    ),
    (
      FieldName: 'dt_hours';
      DisplayName: 'Doubletime';
    ),
    (
      FieldName: 'callout_hours';
      DisplayName: 'Call Out';
    ),
    (
      FieldName: 'vac_hours';
      DisplayName: 'Vacation';
    ),
    (
      FieldName: 'leave_hours';
      DisplayName: 'Leave';
    ),
    (
      FieldName: 'pto_hours';
      DisplayName: 'Personal Time Off';
    ),
    (
      FieldName: 'br_hours';
      DisplayName: 'Bereavement';
    ),
    (
      FieldName: 'hol_hours';
      DisplayName: 'Holiday';
    ),
    (
      FieldName: 'floating_holidays';
      DisplayName: 'Floating Holiday';
    ),
    (
      FieldName: 'jury_hours';
      DisplayName: 'Jury';
    ),
    (
      FieldName: 'pov_hours';
      DisplayName: 'POV';
    ),
    (
      FieldName: 'cov_hours';
      DisplayName: 'COV';
    )
  );

  HierarchyOptionFullDepth = 99;
  HierarchyOptionLimitTo1 = 1;
  HierarchyOptionLimitTo2 = 2;
  HierarchyOptionLimitTo3 = 3;
  HierarchyOptionLimitTo4 = 4;
  HierarchyOptionFlatByPC = 1000;
  HierarchyOptionFlatByPayrollPC = 1001;

  // Break Rule types
  BreakRuleTypeBreakAfter = 'AFTER';
  BreakRuleTypeBreakEvery = 'EVERY';
  BreakRuleTypeSubmit = 'SUBMIT';
  BreakRuleTypeSubmitDisagree = 'SUBMITDIS';
  BreakRuleTypeManagerAlteredTime = 'MGRALTER';
  BreakRuleTypeLunch = 'LUNCH';      //qmantwo-236 used internally

  DaysToKeepFailedAttachments = 14;
  DaysToKeepFailedImports = 30;
  DaysToKeepGPSLogs = 30;

  MinFTPPort = 1;
  MaxFTPPort = 65535;

  FlagColorOrange = 'ORANGE';
  FlagColorRed = 'RED';
  FlagColorNone = 'NONE';

  // JSON object names
  // TODO: These constants need to go away and be replaced with the Thrift structures
  JsonTicketId = 'ticket_id';
  JsonFromLocator = 'from_locator';
  JsonToLocator = 'to_locator';
  JsonWorkDate = 'workload_date';
  JsonChangedBy = 'changed_by';
  JsonAssignedTo = 'assigned_to';
  JsonTicketKind = 'kind';

  // QMLogic2 URLs
  QMLogic2BaseURL = '/api';
  ReassignTicketURL = '/v1/ticket/reassign';

  HighestGeocodePrecision = 50; // Google geocoder's ROOFTOP location_type

  // ReportEngineLog
  EmailResponses = 'EmailResponses-';
  FaxResponses = 'FaxResponses-';

  //Override Status code (for CO) to trigger the Questionnaire
  CO_Acknowledge_Status = '666';

  //Questionnaire Info Types In Code - These are defined in question_info.
  //If they are mismatched with these constants, it will cause errors, of course
  ONGOING =         'ONGOCONT';
  CONPHONE =        'CONPHNE';
  ASSIGNED_TO =     'TK_ASSGN';
  BGEALERT=         'BGEALERT';   //qm-578 flag to save Dig Alert note for BGE
  RESCHED =         'RSREASON';   //QM-604 EB Nipsco RS reschedule



  //Questionnaire field types
  QuestionFieldTypes: array [1..7] of string = (
    'FL',
    'FF',
    'DATETIME',
    'DATE',
    'PHONE',
    'BOOL',
    'DROPDN');

  REVOKED = 'REVOKED';  //QM-10

  RouteOrderNull = 9999; //QM-916 Route Order Limitation Refactoring.

  DashLineSep = '----------------------';  //QM-959

  {Attachment Prefix for attachment names}
  picPrefix = 'camera';   //QM-966 EB Fix Required pictures
  movPrefix = 'movie';    //QM-966 EB Fix Required pictures



implementation

end.
