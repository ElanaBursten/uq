unit BaseNotes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DB, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxTextEdit, cxCalendar,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxNavigator, Menus,
  cxCustomData, cxFilter, cxData, ActnList;

type
  TFunctionalityPermissions = record //QM-188 EB
  public
    CanDeleteNote: boolean;
    CanChangeNoteToPrivate: boolean;
    CanChangeNoteToPublic: boolean;
    CanEditNote: boolean;
  end;

  TBaseNotesFrame = class(TFrame)
    HeaderPanel: TPanel;
    NoteLabel: TLabel;
    NoteText: TMemo;
    AddNoteButton: TButton;
    FooterPanel: TPanel;
    NotesSource: TDataSource;
    NotesGrid: TcxGrid;
    NotesGridLevel: TcxGridLevel;
    NotesGridView: TcxGridDBTableView;
    ColNotesID: TcxGridDBColumn;
    ColNotesForeignID: TcxGridDBColumn;
    ColNotesEntryDate: TcxGridDBColumn;
    ColNotesAddedByName: TcxGridDBColumn;
    ColNotesUID: TcxGridDBColumn;
    ColNotesActive: TcxGridDBColumn;
    ColNotesNote: TcxGridDBColumn;
    ColNotesForeignType: TcxGridDBColumn;
    ColNotesSource: TcxGridDBColumn;
    ColRestriction: TcxGridDBColumn;
    NotesGridPopupMenu: TPopupMenu;
    DeleteNote1: TMenuItem;
    NotesGridActionList: TActionList;
    DeleteNoteAction: TAction;
    CopyNoteAction: TAction;
    CopyNote1: TMenuItem;
    N1: TMenuItem;
    ChangeToPrivateAction: TAction;
    MakeNotePrivate1: TMenuItem;
    ChangeToPublicAction: TAction;
    MakeNotePublic1: TMenuItem;
    UndoAction: TAction;
    N2: TMenuItem;
    procedure AddNoteButtonClick(Sender: TObject);
    procedure DeleteNoteActionExecute(Sender: TObject);
    procedure CopyNoteActionExecute(Sender: TObject);
    procedure NotesGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ChangeToPrivateActionExecute(Sender: TObject); //QM-188 EB
    procedure ChangeToPublicActionExecute(Sender: TObject);  //QM-188 EB
    procedure NotesSourceDataChange(Sender: TObject; Field: TField);
    procedure ColNotesEntryDateCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean); //QM-188 EB
  private

    FNoteType: Integer;
    FNoteSubType: String; //Set from the controlling form. Code saved in Reference table
    FSubTypeText: String; //Text from the reference table
    FForeignID: Integer;
    FExternalCheck: Boolean; //Determines the enabled behavior of the Add Note button
    NotesData: TDataSet;
    function IsPublicNote: Boolean; //QM-188 EB
    function IsPrivateNote: Boolean; //QM-188 EB
  public
    NotePermissions: TFunctionalityPermissions;
    procedure InitNotePermissions(Value: Boolean); //QM-188 EB Init to single value

    property NoteType: Integer read FNoteType write FNoteType;
    property ForeignID: Integer read FForeignID write FForeignID;
    property NoteSubType: string read fNoteSubType write fNoteSubType;
    property SubTypeText: string read fSubTypeText write fSubTypeText;
    property ExternalCheck: Boolean read fExternalCheck write fExternalCheck;
    procedure SetParent(aParent: TWinControl); override;
    procedure RefreshNow;
    procedure Initialize(const NoteType, ForeignID: Integer; CachedNotesDataSet: TDataSet);
    procedure SaveCachedNotes;
    function NotesInCache: Boolean;
    procedure EnableNote(Active: Boolean);
    procedure MakeVisible(Value: Boolean);
    procedure AddNote;
  end;

  implementation

uses OdVclUtils, OdMiscUtils, OdExceptions, QMConst,
  DMu, StrUtils, Clipbrd, SharedDevExStyles, Variants;

{$R *.dfm}

{ TBaseNotesFrame }

procedure TBaseNotesFrame.SetParent(aParent: TWinControl);
begin
  inherited;
end;


procedure TBaseNotesFrame.AddNoteButtonClick(Sender: TObject);
begin
  if Trim(NoteText.Text) = '' then
    raise EOdEntryRequired.Create('Please enter some note text');

  AddNote;
end;

procedure TBaseNotesFrame.ChangeToPrivateActionExecute(Sender: TObject);
begin
  if (NotePermissions.CanChangeNoteToPrivate) then
    DM.ChangeNoteToPrivate(NotesData);
end;

procedure TBaseNotesFrame.ChangeToPublicActionExecute(Sender: TObject);
begin
   if NotePermissions.CanChangeNoteToPublic then
    DM.ChangeNoteToPublic(NotesData);
end;

procedure TBaseNotesFrame.ColNotesEntryDateCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  //ACanvas.Brush
end;

procedure TBaseNotesFrame.CopyNoteActionExecute(Sender: TObject);
var
  tempStrList: TStringList;
begin
  tempStrList := TStringList.Create;
  try
  tempStrList.Clear;
  tempStrList.Add('Restriction: ' + NotesData.FieldByName('restriction').AsString);
  tempStrList.Add('Added By: ' + NotesData.FieldByName('short_name').AsString);
  tempStrList.Add('Entry Date: ' + DateTimeToStr(NotesData.FieldByName('Entry_Date').AsDateTime));
  tempStrList.Add('Note: ' + NotesData.FieldByName('note').AsString);
  Clipboard.AsText :=  tempStrList.Text;
  finally
    FreeAndNil(tempStrList);
  end;
end;

procedure TBaseNotesFrame.DeleteNoteActionExecute(Sender: TObject);
begin
  If (NotePermissions.CanDeleteNote) then
    DM.DeleteNote(NotesData);
end;

procedure TBaseNotesFrame.EnableNote(Active: Boolean);
begin
   NoteText.Enabled := Active;
   AddNoteButton.Enabled := Active;
end;

procedure TBaseNotesFrame.AddNote;
var
  NoteSubTypeInt : Integer;
begin

  DM.VerifyNoteLength(NoteText.Text);
  try
    NoteSubTypeInt := StrToInt(NoteSubType);
  except
    MessageDlg('Invalid Restriction code value. Please select a code from the dropdown.',mtError,[mbOK],0);
    exit;
  end;

  {EB - NOTES UPDATE DEBUG: Leave this here in case we want to watch what is going across}
  //ShowMessage('Text Version: ' + SubTypeText + ' | Integer Version: ' + IntToStr(NoteSubTypeInt));
  DM.SaveBaseNoteToCache(OdMiscUtils.ReplaceControlChars(NoteText.Text), NoteType, FForeignID, NotesData, NoteSubTypeInt, SubTypeText);
  NoteText.Clear;

  RefreshNow;
end;

procedure TBaseNotesFrame.InitNotePermissions(Value: Boolean);
begin
  With NotePermissions do begin
    CanDeleteNote := Value;
    CanChangeNoteToPrivate := Value;
    CanChangeNoteToPublic :=Value;
    CanEditNote := Value;
  end;
end;



function TBaseNotesFrame.IsPrivateNote: Boolean;   //QM-188 EB
var
  CurNoteSubType: Integer;
begin
  CurNoteSubType := NotesData.FieldByName('sub_type').AsInteger;
  case CurNoteSubType of
     PrivateTicketSubType,PrivateLocateSubType, PrivateDamageSubType: Result := True
     else Result := False;
  end;
end;

function TBaseNotesFrame.IsPublicNote: Boolean;   //QM-188 EB
var
  CurNoteSubType: Integer;
begin
  CurNoteSubType := NotesData.FieldByName('sub_type').AsInteger;
  case CurNoteSubType of
     PublicTicketSubType,PublicLocateSubType, PublicDamageSubType: Result := True
     else Result := False;
  end;
end;

procedure TBaseNotesFrame.Initialize(const NoteType, ForeignID: Integer; CachedNotesDataSet: TDataSet);
begin
  InitNotePermissions(False);
  ExternalCheck := True; //Unless it is set externally, it should be True
  NoteText.MaxLength := MaxNoteLength;
  FNoteType := NoteType;
  FForeignID := ForeignID;
  NotesData := CachedNotesDataSet;
  NotesSource.DataSet := NotesData;
  RefreshNow;
end;

procedure TBaseNotesFrame.MakeVisible(Value: Boolean);
begin
  AddNoteButton.Enabled := Value;
  fExternalCheck := Value;
end;

procedure TBaseNotesFrame.RefreshNow;
var
  EnableAdd: Boolean;
begin
  EnableAdd := (FForeignID > 0);
  SetEnabledOnControlAndChildren(HeaderPanel, EnableAdd);
  AddNoteButton.Enabled := fExternalCheck;
end;

procedure TBaseNotesFrame.SaveCachedNotes;
begin
  if not IsEmpty(NoteText.Text) then  {Adds notes that are sitting in text box}
    AddNote;

  DM.SaveBaseNotesFromCache(LeftStr(Self.Name, 3), NotesData);

  NotesData.Close;
  NotesData.Open;
end;


procedure TBaseNotesFrame.NotesGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  lnote: string;
  lColorText, lColorHighlight : TColor;

begin
  lColorText := clNone;
  lColorHighlight := clNone;
  {Wayne's Addd Notes (under 200 for user id)}
  if (AViewInfo.GridRecord.Values[ColNotesUID.Index] < 200) or
     (VarIsNull(AViewInfo.GridRecord.Values[ColNotesUID.Index])) then
       lNote := NoteExternal  //QM-905 EB (assigning a value to get colors back}

    {Only do this for ticket notes/locate notes }
  else if (AViewInfo.GridRecord.Values[ColNotesForeignType.Index] = 1) or
       (AViewInfo.GridRecord.Values[ColNotesForeignType.Index] = 5) then
      lnote := AViewInfo.GridRecord.Values[ColNotesNote.Index];

    DM.IsAutomatedNoteColor(lnote, lColorText, lColorHighlight);  //QM-966 EB Required Pic Fix
    ACanvas.Font.Color := lColorText;
    ACanvas.Brush.Color := lColorHighlight;
end;


function TBaseNotesFrame.NotesInCache: Boolean;
begin
  Result := NotesData.Locate('on_cache', True, []);
end;

procedure TBaseNotesFrame.NotesSourceDataChange(Sender: TObject; Field: TField);
begin
  if (NotePermissions.CanChangeNoteToPrivate) and (not IsPrivateNote) then
    ChangeToPrivateAction.Visible := True
  else
    ChangeToPrivateAction.Visible := False;

  if (NotePermissions.CanChangeNoteToPublic) and (not IsPublicNote) then
    ChangeToPublicAction.Visible := True
  else
    ChangeToPublicAction.Visible := False;

end;

end.
