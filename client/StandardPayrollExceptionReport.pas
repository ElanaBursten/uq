unit StandardPayrollExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, ComCtrls, dxCore, cxDateUtils;

type
  TStandardPayrollExceptionReportForm = class(TReportBaseForm)
    PeriodLabel: TLabel;
    PCLabel: TLabel;
    PCComboBox: TComboBox;
    ChooseDate: TcxDateEdit;
    procedure ChooseDateChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdMiscUtils;

{ TReportBaseForm1 }

procedure TStandardPayrollExceptionReportForm.InitReportControls;
begin
  inherited;
  DM.ReportableTimesheetProfitCenterList(PCComboBox.Items);
  ChooseDate.Date := ThisSaturday;
end;

procedure TStandardPayrollExceptionReportForm.ValidateParams;
begin
  inherited;
  RequireComboBox(PCComboBox, 'Please select a payroll timesheet number.');
  SetReportID('PayrollExceptionStandard');
  SetParamIntCombo('manager_id', PCComboBox, True);
  SetParamDate('end_date', ChooseDate.Date);
end;

procedure TStandardPayrollExceptionReportForm.ChooseDateChange(Sender: TObject);
begin
  if ChooseDate.Date <> SaturdayIze(ChooseDate.Date) then
    ChooseDate.Date := SaturdayIze(ChooseDate.Date);
end;

end.
