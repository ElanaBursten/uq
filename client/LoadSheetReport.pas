unit LoadSheetReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls;

type
  TLoadSheetReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label3: TLabel;
    ManagersComboBox: TComboBox;
    LocatorListBox: TListBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure PopulateLocators;
    procedure EmployeeStatusComboChange(Sender: TObject);
  protected
    FCurrentManager, FCurrentStatus: Integer;
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    function GetLocatorList: string;
  end;

implementation

uses
  DMu, OdExceptions, OdVclUtils, LocalEmployeeDmu;

{$R *.dfm}

{ TLoadSheetForm }

procedure TLoadSheetReportForm.ValidateParams;
begin
  inherited;
  if LocatorListBox.SelCount = 0 then begin
    LocatorListBox.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one locator.');
  end;

  SetReportID('LoadSheet');
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParam('Locators', GetLocatorList);
end;

procedure TLoadSheetReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  ManagersComboBox.ItemIndex := -1;
  FCurrentManager := -1;
  FCurrentStatus  := -1;

  LocatorListBox.Items.Clear;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

function TLoadSheetReportForm.GetLocatorList: string;
var
  i: Integer;

  procedure Add(Id: Integer);
  begin
    if Result = '' then
      Result := IntToStr(Id)
    else
      Result := Result + ',' + IntToStr(Id);
  end;

begin
  Result := '';
  with LocatorListBox do
    for i := 0 to Count - 1 do
      if Selected[i] then
        Add(Integer(Items.Objects[i]));
end;

//Populate Locators
procedure TLoadSheetReportForm.PopulateLocators;
var
  ManagerID: Integer;
begin
  if (ManagersComboBox.ItemIndex <> FCurrentManager) or
     (GetComboObjectInteger(EmployeeStatusCombo)<> FCurrentStatus) then begin
    ManagerID := GetComboObjectInteger(ManagersComboBox);
    EmployeeDM.EmployeeList(LocatorListBox.Items, ManagerID, GetComboObjectInteger(EmployeeStatusCombo));
    LocatorListBox.SelectAll;
    LocatorListBox.TopIndex := 0;

    FCurrentManager := ManagersComboBox.ItemIndex;
    FCurrentStatus  := GetComboObjectInteger(EmployeeStatusCombo);
  end;
end;

procedure TLoadSheetReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  PopulateLocators;
end;

procedure TLoadSheetReportForm.EmployeeStatusComboChange(Sender: TObject);
begin
  inherited;
  PopulateLocators;
end;

end.

