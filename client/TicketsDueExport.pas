unit TicketsDueExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, dxCore, cxDateUtils;

type
  TTicketsDueExportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ManagersComboBox: TComboBox;
    DateTimeSelect: TcxDateEdit;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TTicketsDueExportForm }

procedure TTicketsDueExportForm.ValidateParams;
begin
  inherited;
  SetReportID('TicketsDueExport');
  SetParamDate('DueDate', DateTimeSelect.Date);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TTicketsDueExportForm.InitReportControls;
begin
  inherited;
  DateTimeSelect.Date := Date + 1;
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.
