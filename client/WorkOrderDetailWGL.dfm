inherited WOWGLFrame: TWOWGLFrame
  Width = 894
  Height = 561
  inherited DetailsPanel: TPanel
    Width = 894
    Height = 148
    inherited DueDateLabel: TLabel
      Top = 47
      Width = 49
      Caption = 'Due Date:'
    end
    inherited TransmitDateLabel: TLabel
      Top = 63
      Width = 86
      Caption = 'Transaction Date:'
    end
    object AccountNumber: TDBText [2]
      Left = 517
      Top = 63
      Width = 93
      Height = 17
      DataField = 'account_number'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object ComplianceDueDate: TDBText [3]
      Left = 112
      Top = 31
      Width = 114
      Height = 13
      AutoSize = True
      DataField = 'compliance_due_date'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    inherited TransmitDate: TDBText
      Left = 112
      Top = 63
    end
    object AssignedToLabel: TLabel [5]
      Left = 3
      Top = 0
      Width = 62
      Height = 13
      Caption = 'Assigned To:'
      Transparent = True
    end
    object AssignedTo: TDBText [6]
      Left = 112
      Top = 0
      Width = 65
      Height = 13
      AutoSize = True
      DataField = 'employee_name'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object Label4: TLabel [7]
      Left = 411
      Top = 63
      Width = 83
      Height = 13
      Caption = 'Account Number:'
      Transparent = True
    end
    object PremiseIDLabel: TLabel [8]
      Left = 411
      Top = 79
      Width = 55
      Height = 13
      Caption = 'Premise ID:'
      Transparent = True
    end
    object PremiseID: TDBText [9]
      Left = 517
      Top = 79
      Width = 93
      Height = 17
      DataField = 'premise_id'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object ComplianceDueDateLabel: TLabel [10]
      Left = 3
      Top = 31
      Width = 106
      Height = 13
      Caption = 'Compliance Due Date:'
      Transparent = True
    end
    inherited DueDate: TDBText
      Left = 112
      Top = 47
    end
    inherited ClientCode: TDBText
      Left = 517
      Top = 15
    end
    inherited ClientCodeLabel: TLabel
      Left = 411
      Top = 15
    end
    inherited ClientNameLabel: TLabel
      Left = 411
      Top = 31
    end
    inherited ClientName: TDBText
      Left = 517
      Top = 31
    end
    inherited ClientWONumberLabel: TLabel
      Left = 411
      Top = 47
      Visible = False
    end
    inherited ClientWONumber: TDBText
      Left = 517
      Top = 47
      Visible = False
    end
    inherited ClientOrderNumLabel: TLabel
      Left = 411
    end
    inherited ClientOrderNum: TDBText
      Left = 517
    end
    inherited WorkLong: TDBText
      Left = 192
      Top = 15
    end
    inherited WorkLat: TDBText
      Left = 112
      Top = 15
    end
    inherited TerminalGPSLabel: TLabel
      Top = 15
      Width = 52
      Caption = 'Lat - Long:'
    end
    inherited CustomerPanel: TPanel
      Top = 90
      Width = 894
      Height = 58
      object CallerPhoneLabel: TLabel [0]
        Left = 443
        Top = 20
        Width = 34
        Height = 13
        Caption = 'Phone:'
        Transparent = True
      end
      object AtlPhoneLabel: TLabel [1]
        Left = 443
        Top = 36
        Width = 50
        Height = 13
        Caption = 'Alt Phone:'
        Transparent = True
      end
      object Phone: TDBText [2]
        Left = 517
        Top = 20
        Width = 93
        Height = 17
        DataField = 'caller_phone'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object AltPhone: TDBText [3]
        Left = 517
        Top = 36
        Width = 93
        Height = 17
        DataField = 'caller_altphone'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      inherited WorkAddressLabel: TLabel
        Top = 19
      end
      inherited WorkAddressNumber: TDBText
        Left = 112
        Top = 20
      end
      inherited City: TDBText
        Left = 112
        Top = 39
      end
      inherited WorkAddressStreet: TDBText
        Left = 162
        Top = 20
      end
      inherited State: TDBText
        Left = 285
        Top = 36
        Width = 40
      end
      inherited Zip: TDBText
        Left = 346
        Top = 36
      end
      inherited CallerNameLabel: TLabel
        Top = 2
      end
      inherited CallerName: TDBText
        Left = 112
        Top = 2
      end
    end
    object btnRemediation: TButton
      Left = 808
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Remediation'
      TabOrder = 1
      OnClick = btnRemediationClick
    end
  end
  inherited FeedDetailsPanel: TPanel
    Top = 148
    Width = 894
    Height = 15
    inherited MapPageLabel: TLabel
      Left = 376
      Top = -2
    end
    inherited MapPage: TDBText
      Left = 411
      Top = -2
    end
    inherited MapRef: TDBText
      Left = 587
      Top = -2
    end
    inherited MapPageGrid: TLabel
      Left = 528
      Top = -2
      Width = 53
      Caption = 'Quad Map:'
    end
  end
  inherited EditingPanel: TPanel
    Top = 163
    Width = 894
    Height = 366
    object AOCInspectionLabel: TLabel
      Left = 4
      Top = 135
      Width = 79
      Height = 13
      Caption = 'AOC Inspection:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 334
      Top = 237
      Width = 86
      Height = 13
      Caption = 'Potential Hazard: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 423
      Top = 237
      Width = 180
      Height = 13
      Caption = ' Immediate Communication Required  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
      Transparent = True
    end
    object CustInspectionLabel: TLabel
      Left = 334
      Top = 135
      Width = 171
      Height = 13
      Caption = 'Customer Inspection: (Orange Tag)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object InitialInspectionPanel: TPanel
      Left = 0
      Top = 0
      Width = 894
      Height = 130
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object InitialInspectionLabel: TLabel
        Left = 3
        Top = 6
        Width = 83
        Height = 13
        Caption = 'Initial Inspection:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object DateOfVisitLabel: TLabel
        Left = 3
        Top = 25
        Width = 62
        Height = 13
        Caption = 'Date of Visit:'
      end
      object CGACountLabel: TLabel
        Left = 3
        Top = 45
        Width = 57
        Height = 13
        Caption = 'CGA Count:'
      end
      object CompletionCodeLabel: TLabel
        Left = 3
        Top = 66
        Width = 85
        Height = 13
        Caption = 'Completion Code:'
      end
      object BuildingClassificationLabel: TLabel
        Left = 314
        Top = 111
        Width = 105
        Height = 13
        Caption = 'Building Classification:'
      end
      object MapStatusLabel: TLabel
        Left = 2
        Top = 112
        Width = 58
        Height = 13
        Caption = 'Map Status:'
      end
      object MeterNumberProvidedLabel: TLabel
        Left = 248
        Top = 45
        Width = 72
        Height = 13
        Caption = 'Meter Number:'
      end
      object MeterNumberActual: TLabel
        Left = 248
        Top = 66
        Width = 76
        Height = 13
        Caption = 'Actual Meter #:'
      end
      object DateOfVisit: TDBText
        Left = 112
        Top = 25
        Width = 63
        Height = 13
        AutoSize = True
        DataField = 'status_date'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CGACount: TDBText
        Left = 112
        Top = 45
        Width = 65
        Height = 19
        DataField = 'cga_visits'
        DataSource = WorkOrderInspectionDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object MeterNumber: TDBText
        Left = 326
        Top = 45
        Width = 86
        Height = 17
        DataField = 'meter_number'
        DataSource = WorkOrderInspectionDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object MeterLocationLabel: TLabel
        Left = 248
        Top = 25
        Width = 75
        Height = 13
        Caption = 'Meter Location:'
      end
      object MercuryRegulatorLabel: TLabel
        Left = 459
        Top = 25
        Width = 93
        Height = 13
        Caption = 'Mercury Regulator:'
      end
      object CGAReasonLabel: TLabel
        Left = 2
        Top = 87
        Width = 67
        Height = 13
        Caption = 'CGA Reason: '
      end
      object GasLightLabel: TLabel
        Left = 459
        Top = 45
        Width = 51
        Height = 13
        Caption = 'Gas Light: '
      end
      object ActualMeterNumber: TDBEdit
        Left = 326
        Top = 64
        Width = 90
        Height = 21
        CharCase = ecUpperCase
        DataField = 'actual_meter_number'
        DataSource = WorkOrderInspectionDS
        TabOrder = 4
      end
      object BuildingClassificationComboBox: TcxDBExtLookupComboBox
        Left = 420
        Top = 109
        DataBinding.DataField = 'building_type'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.DropDownAutoSize = True
        Properties.View = BuildingView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = BuildingDescription
        TabOrder = 8
        OnEditing = ComboBoxEditing
        Width = 186
      end
      object MapStatusComboBox: TcxDBExtLookupComboBox
        Left = 112
        Top = 109
        DataBinding.DataField = 'map_status'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.DropDownAutoSize = True
        Properties.View = MapStatusView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = MapStatusDescription
        TabOrder = 7
        OnEditing = ComboBoxEditing
        Width = 186
      end
      object MeterLocationComboBox: TcxDBExtLookupComboBox
        Left = 326
        Top = 22
        DataBinding.DataField = 'actual_meter_location'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.View = MeterLocationView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = MeterLocationDescription
        TabOrder = 0
        OnEditing = ComboBoxEditing
        Width = 116
      end
      object MercuryRegulatorComboBox: TcxDBExtLookupComboBox
        Left = 565
        Top = 22
        DataBinding.DataField = 'mercury_regulator'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.View = YnubView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = YnubDescription
        TabOrder = 1
        OnEditing = ComboBoxEditing
        Width = 135
      end
      object CompletionCodeCombobox: TDBLookupComboBox
        Left = 112
        Top = 63
        Width = 118
        Height = 21
        DataField = 'status'
        DataSource = WorkOrderDS
        DropDownWidth = 200
        KeyField = 'status'
        ListField = 'status;status_name'
        ListSource = StatusSource
        TabOrder = 3
      end
      object IllegibleButton: TButton
        Left = 416
        Top = 63
        Width = 44
        Height = 21
        Caption = 'Illegible'
        TabOrder = 5
        OnClick = IllegibleButtonClick
      end
      object CGAReasonCombo: TcxDBExtLookupComboBox
        Left = 112
        Top = 85
        DataBinding.DataField = 'cga_reason'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.DropDownAutoSize = True
        Properties.View = CGAReasonView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = CGAReasonDescription
        TabOrder = 6
        OnEditing = ComboBoxEditing
        Width = 118
      end
      object GasLightComboBox: TcxDBExtLookupComboBox
        Left = 565
        Top = 44
        DataBinding.DataField = 'gas_light'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.View = GasLightView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = GasLightDescription
        TabOrder = 2
        OnEditing = ComboBoxEditing
        Width = 135
      end
    end
    object PotentialHazardCheckList: TcxCheckListBox
      Left = 334
      Top = 256
      Width = 272
      Height = 120
      Items = <>
      TabOrder = 7
      OnClickCheck = RemedyCheckListClickCheck
    end
    object AOCInspectionCheckList: TcxCheckListBox
      Left = 4
      Top = 185
      Width = 294
      Height = 191
      Items = <
        item
          Text = 'Item 1'
        end
        item
          Text = 'Item 2'
        end
        item
          Text = 'Item 3'
        end>
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      OnClickCheck = RemedyCheckListClickCheck
    end
    object CustomerInspectionCheckList: TcxCheckListBox
      Left = 334
      Top = 150
      Width = 272
      Height = 84
      Items = <>
      TabOrder = 6
      OnClickCheck = RemedyCheckListClickCheck
    end
    object VentClearanceDistCombobox: TcxDBExtLookupComboBox
      Left = 234
      Top = 141
      DataBinding.DataField = 'vent_clearance_dist'
      DataBinding.DataSource = WorkOrderInspectionDS
      Enabled = False
      Properties.DropDownAutoSize = True
      Properties.View = VentClearanceView
      Properties.KeyFieldNames = 'code'
      Properties.ListFieldItem = VentClearanceDescription
      TabOrder = 2
      OnEditing = VentClearanceDistComboboxEditing
      Width = 64
    end
    object VentIgnitionDistComboBox: TcxDBExtLookupComboBox
      Left = 234
      Top = 162
      DataBinding.DataField = 'vent_ignition_dist'
      DataBinding.DataSource = WorkOrderInspectionDS
      Enabled = False
      Properties.DropDownAutoSize = True
      Properties.View = VentIgnitionView
      Properties.KeyFieldNames = 'code'
      Properties.ListFieldItem = VentIgnitionDescription
      TabOrder = 4
      OnEditing = VentClearanceDistComboboxEditing
      Width = 64
    end
    object InadequateVentClearance: TcxCheckBox
      Left = 2
      Top = 146
      Caption = 'Inadequate Vent Clearance'
      ParentColor = False
      TabOrder = 1
      OnClick = InadequateVentClearanceClick
      Width = 228
    end
    object InadequateVentClearanceIg: TcxCheckBox
      Left = 2
      Top = 161
      Caption = 'Inadequate Vent Clearance Ignition'
      ParentColor = False
      TabOrder = 3
      OnClick = InadequateVentClearanceIgClick
      Width = 228
    end
  end
  inherited ButtonPanel: TPanel
    Top = 529
    Width = 894
    Height = 32
    inherited PlatAddinButton: TButton
      Top = 2
    end
  end
  inherited StatusList: TDBISAMQuery
    OnFilterRecord = nil
    Left = 606
    Top = 520
  end
  inherited StatusSource: TDataSource
    DataSet = StatusList
    Left = 573
    Top = 520
  end
  inherited WorkOrderDS: TDataSource
    Left = 638
    Top = 520
  end
  object WorkOrderInspectionDS: TDataSource
    DataSet = DM.WorkOrderInspection
    Left = 638
    Top = 8
  end
  object Remedies: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'select wt.work_type, wt.work_description, wt.flag_color, wt.aler' +
        't, wor.*'
      'from'
      'work_order_work_type wt'
      'JOIN work_order_remedy wor'
      'on wor.work_type_id= wt.work_type_id'
      'where (wor.wo_id=:WOID)'
      '  and wor.active')
    Params = <
      item
        DataType = ftUnknown
        Name = 'WOID'
      end>
    ReadOnly = True
    Left = 254
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'WOID'
      end>
  end
  object WorkOrderRemedyDS: TDataSource
    AutoEdit = False
    DataSet = DM.WorkOrderRemedy
    Left = 125
    Top = 536
  end
  object WorkTypes: TDBISAMQuery
    Tag = 999
    Filtered = True
    OnFilterRecord = WorkTypesFilterRecord
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Select * '
      'from work_order_work_type'
      'where active = True'
      'order by work_type_id'
      '')
    Params = <>
    ReadOnly = True
    Left = 614
    Top = 424
  end
  object ViewRepo: TcxGridViewRepository
    Left = 352
    Top = 80
    object BuildingView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = BuildingRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object BuildingDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object MapStatusView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = MapStatusRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object MapStatusDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object MeterLocationView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = MeterLocationRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object MeterLocationDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object YnubView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = YnubRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object YnubDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object VentClearanceView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = VentClearanceRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object VentClearanceDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object VentIgnitionView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = VentIgnitionRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object VentIgnitionDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object CGAReasonView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = CGAReasonRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object CGAReasonDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object GasLightView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = GasLightRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object GasLightDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
  end
  object BuildingRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'bldtype'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 560
  end
  object BuildingRefSource: TDataSource
    AutoEdit = False
    DataSet = BuildingRef
    Left = 205
    Top = 560
  end
  object MapStatusRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'mapstat'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 528
  end
  object MapStatusRefSource: TDataSource
    AutoEdit = False
    DataSet = MapStatusRef
    Left = 205
    Top = 528
  end
  object MeterLocationRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'metrloc'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 496
  end
  object MeterLocationRefSource: TDataSource
    AutoEdit = False
    DataSet = MeterLocationRef
    Left = 205
    Top = 496
  end
  object YnubRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ynub'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 464
  end
  object YnubRefSource: TDataSource
    AutoEdit = False
    DataSet = YnubRef
    Left = 205
    Top = 464
  end
  object VentClearanceRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ventclr'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 432
  end
  object VentClearanceRefSource: TDataSource
    AutoEdit = False
    DataSet = VentClearanceRef
    Left = 205
    Top = 432
  end
  object VentIgnitionRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ventign'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 400
  end
  object VentIgnitionRefSource: TDataSource
    AutoEdit = False
    DataSet = VentIgnitionRef
    Left = 205
    Top = 400
  end
  object UpdateWORemedy: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'update work_order_remedy'
      'set active=:active, DeltaStatus='#39'U'#39
      'where wo_id=:wo_id and work_type_id=:work_type_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'active'
      end
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end>
    ReadOnly = True
    Left = 358
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'active'
      end
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end>
  end
  object InsertWORemedy: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'insert into work_order_remedy(wo_id, work_type_id, active, modif' +
        'ied_date)'
      'values(:wo_id, :work_type_id, 1, :current)')
    Params = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end
      item
        DataType = ftDateTime
        Name = 'current'
      end>
    ReadOnly = True
    Left = 406
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end
      item
        DataType = ftDateTime
        Name = 'current'
      end>
  end
  object CGAReasonRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'cgareason'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 246
    Top = 520
  end
  object CGAReasonRefSource: TDataSource
    AutoEdit = False
    DataSet = CGAReasonRef
    Left = 277
    Top = 520
  end
  object GasLightRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'gaslight'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 246
    Top = 488
  end
  object GasLightRefSource: TDataSource
    AutoEdit = False
    DataSet = GasLightRef
    Left = 277
    Top = 488
  end
end
