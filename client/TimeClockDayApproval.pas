unit TimeClockDayApproval;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TimeClockDay, DBISAMTb, ActnList, DB, StdCtrls,
  DBCtrls, ExtCtrls, TimeClockDB, TimeClockEntry,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxDropDownEdit, cxMaskEdit, cxDBEdit, cxTextEdit,
  cxCurrencyEdit, cxStyles, cxDataStorage,
  cxDBData, cxTimeEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxNavigator, cxCustomData, cxFilter, cxData;

type
  TTimeClockDayApprovalFrame = class(TTimeClockDayFrame)
    LegendGroup: TGroupBox;
    NotEnteredShape: TShape;
    NotSubmittedShape: TShape;
    NotEnteredLabel: TLabel;
    NotSubmittedLabel: TLabel;
    ApprovedLabel: TLabel;
    ApprovedShape: TShape;
    FinalApprovedLabel: TLabel;
    FinalApprovedShape: TShape;
    SubmittedShape: TShape;
    SubmittedLabel: TLabel;
    WorkHoursLabel: TLabel;
    CalloutHoursLabel: TLabel;
    RegularHoursLabel: TLabel;
    OvertimeHoursLabel: TLabel;
    DoubletimeHoursLabel: TLabel;
    LeaveHoursLabel: TLabel;
    VacationHoursLabel: TLabel;
    BereavementHoursLabel: TLabel;
    HolidayLabel: TLabel;
    JuryDutyHoursLabel: TLabel;
    DayHoursLabel: TLabel;
    EmployeeTypeLabel: TLabel;
    VehicleUseLabel: TLabel;
    MilesStart1Label: TLabel;
    FloatingHolidayLabel: TLabel;
    ApprovalMemoLabel: TLabel;
    PTOHoursLabel: TLabel;
    UTAEnteredLabel: TLabel;
    UTAEnteredShape: TShape;
    RTASQEnteredLabel: TLabel;
    RTASQEnteredShape: TShape;
    TSEEnteredShape: TShape;
    TSEEnteredLabel: TLabel;
  private
    function GetFloatingHolidayVisible: Boolean;
    procedure SetFloatingHolidayVisible(const Value: Boolean);
  protected
    function TimeClockTableName: string; override;
  public
    procedure DisplayDataSet(DataSet: TDataSet); override;
    property FloatingHolidayVisible: Boolean read GetFloatingHolidayVisible write SetFloatingHolidayVisible;
  end;

var
  TimeClockDayApprovalFrame: TTimeClockDayApprovalFrame;

implementation

uses OdDbUtils;

{$R *.dfm}

{ TTimeClockDayApprovalFrame }

procedure TTimeClockDayApprovalFrame.DisplayDataSet(DataSet: TDataSet);
begin
  inherited;
  if not HasRecords(DataSet) then begin
    TimeClockSource.DataSet := nil;
    Exit;
  end;

  if Assigned(TimeClockTranslator) then
    FreeAndNil(TimeClockTranslator);

  //Initialize data used by the grid for the approval timesheet
  DataSaver := TDbDataSaver.Create(DataSet, ClockDetailData);
  RowWriter := TGridRowWriter.Create(ClockDetailData);
  TimeClockTranslator := TTimeClockTranslator.Create(DataSaver, RowWriter);
  FloatingHoliday.Checked := False; //DataSet.FieldByName('floating_holiday').AsBoolean;
  PerDiem.Checked := DataSet.FieldByName('per_diem').AsBoolean;
  DisplayFloatingHolidayCaption;
  DisplayPerDiemCaption;

  PrepareTimeClockDetailTable;
  RefreshTimeClockGrid;
end;

function TTimeClockDayApprovalFrame.GetFloatingHolidayVisible: Boolean;
begin
  Result := FloatingHoliday.Visible;
end;

procedure TTimeClockDayApprovalFrame.SetFloatingHolidayVisible(const Value: Boolean);
begin
  FloatingHolidayLabel.Visible := Value;
  FloatingHoliday.Visible := False; //Value;
end;

function TTimeClockDayApprovalFrame.TimeClockTableName: string;
begin
  Result := 'time_clock_approval';
end;

end.
