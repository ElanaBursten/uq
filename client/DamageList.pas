unit DamageList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, ComCtrls, DB, DBISAMTb, DMu,
  ActnList;

type
  TDamageListForm = class(TEmbeddableForm)
    SearchDamageButton: TButton;
    NewButton: TButton;
    SearchInvoiceButton: TButton;
    SearchLitigationButton: TButton;
    SearchThirdPartyButton: TButton;
    ActionList: TActionList;
    NewDamageAction: TAction;
    SearchDamageAction: TAction;
    SearchLitigationAction: TAction;
    SearchThirdPartyAction: TAction;
    SearchInvoiceAction: TAction;
    procedure FormShow(Sender: TObject);
    procedure NewDamageActionExecute(Sender: TObject);
    procedure SearchDamageActionExecute(Sender: TObject);
    procedure SearchLitigationActionExecute(Sender: TObject);
    procedure SearchThirdPartyActionExecute(Sender: TObject);
    procedure SearchInvoiceActionExecute(Sender: TObject);
  private
    FOnDamagePrint: TItemSelectedEvent;
    FOnNewDamage: TNotifyEvent;
    FOnDamageSearch: TNotifyEvent;
    FOnNewInvoice: TNotifyEvent;
    FOnInvoiceSearch: TNotifyEvent;
    FOnNewLitigation: TNotifyEvent;
    FOnLitigationSearch: TNotifyEvent;
    FOnNewThirdParty: TNotifyEvent;
    FOnThirdPartySearch: TNotifyEvent;
    procedure SetControlsBasedOnRights;
    //procedure PrintDamage(Sender: TObject; ID: Integer);
  public
    property OnDamagePrint: TItemSelectedEvent read FOnDamagePrint write FOnDamagePrint;
    property OnNewDamage: TNotifyEvent read FOnNewDamage write FOnNewDamage;
    property OnDamageSearch: TNotifyEvent read FOnDamageSearch write FOnDamageSearch;
    property OnNewInvoice: TNotifyEvent read FOnNewInvoice write FOnNewInvoice;
    property OnInvoiceSearch: TNotifyEvent read FOnInvoiceSearch write FOnInvoiceSearch;
    property OnLitigationSearch: TNotifyEvent read FOnLitigationSearch write FOnLitigationSearch;
    property OnThirdPartySearch: TNotifyEvent read FOnThirdPartySearch write FOnThirdPartySearch;
    property OnNewLitigation: TNotifyEvent read FOnNewLitigation write FOnNewLitigation;
    property OnNewThirdParty: TNotifyEvent read FOnNewThirdParty write FOnNewThirdParty;
  end;

implementation

uses OdVclUtils, OdExceptions, QMConst, BaseSearchForm, DamageDetails,
  DamageSearchHeader, DamageLitigationSearchHeader, DamageThirdPartySearchHeader,
  DamageInvoiceSearchHeader, MainFU, LocalPermissionsDMu;

{$R *.dfm}

procedure TDamageListForm.FormShow(Sender: TObject);
begin
  SetControlsBasedOnRights;
end;

procedure TDamageListForm.SetControlsBasedOnRights;
begin
  NewDamageAction.Enabled := PermissionsDM.CanI(RightDamagesAssign);
  SearchLitigationAction.Enabled := PermissionsDM.CanI(RightLitigationView) or PermissionsDM.CanI(RightLitigationEdit);
  SearchThirdPartyAction.Enabled := SearchLitigationAction.Enabled;
end;

procedure TDamageListForm.NewDamageActionExecute(Sender: TObject);
begin
  if Assigned(FOnNewDamage) then
    FOnNewDamage(Self);
end;

procedure TDamageListForm.SearchDamageActionExecute(Sender: TObject);
begin
  if Assigned(FOnDamageSearch) then
    FOnDamageSearch(Self);
end;

procedure TDamageListForm.SearchLitigationActionExecute(Sender: TObject);
begin
  if Assigned(FOnLitigationSearch) then
    FOnLitigationSearch(Self);
end;

procedure TDamageListForm.SearchThirdPartyActionExecute(Sender: TObject);
begin
  if Assigned(FOnThirdPartySearch) then
    FOnThirdPartySearch(Self);
end;

procedure TDamageListForm.SearchInvoiceActionExecute(Sender: TObject);
begin
  if Assigned(FOnInvoiceSearch) then
    FOnInvoiceSearch(Self);
end;

(*
procedure TDamageListForm.PrintDamage(Sender: TObject; ID: Integer);
begin
  if Assigned(FOnDamagePrint) then
    FOnDamagePrint(Sender, ID);
end;
*)

end.

