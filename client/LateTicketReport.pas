unit LateTicketReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls, OdRangeSelect, CheckLst;

type
  TLateTicketReportForm = class(TReportBaseForm)
    RangeSelect: TOdRangeSelectFrame;
    Label1: TLabel;
    Label2: TLabel;
    ManagersComboBox: TComboBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    CallCenterList: TCheckListBox;
    Label4: TLabel;
    ArrivalModeCheckBox: TCheckBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdVclUtils, OdExceptions, OdMiscUtils, LocalEmployeeDMu;

{ TLateTicketReportForm }

procedure TLateTicketReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('LateTicket');
  SetParamDate('StartDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);
  SetParamIntCombo('ManagerId', ManagersComboBox);

  CheckedItems := GetCheckedItemCodesString(CallCenterList);
  if IsEmpty(CheckedItems) then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one call center for the report.');
  end;

  SetParam('CallCenter', CheckedItems);
  SetParam('SortBy', 'C');
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
  SetParamBoolean('ArrivalMode', ArrivalModeCheckBox.Checked);
end;

procedure TLateTicketReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.

