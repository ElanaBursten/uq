inherited FaxStatusReportForm: TFaxStatusReportForm
  Left = 453
  Top = 508
  Caption = 'FaxStatusReportForm'
  ClientHeight = 255
  ClientWidth = 536
  PixelsPerInch = 96
  TextHeight = 13
  object CallCenterLabel: TLabel [0]
    Left = 0
    Top = 98
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 8
    Width = 118
    Height = 13
    Caption = 'FAX Queue Date Range:'
  end
  object CallCenterComboBox: TComboBox [2]
    Left = 69
    Top = 95
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
  end
  inline QueueDate: TOdRangeSelectFrame [3]
    Left = 26
    Top = 24
    Width = 287
    Height = 53
    TabOrder = 1
    inherited FromLabel: TLabel
      Left = 7
      Top = 10
    end
    inherited ToLabel: TLabel
      Left = 137
      Top = 10
    end
    inherited DatesLabel: TLabel
      Left = 7
      Top = 35
    end
    inherited DatesComboBox: TComboBox
      Left = 43
      Top = 32
      Width = 206
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 43
      Top = 7
      Width = 88
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 159
      Top = 7
      Width = 90
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
  end
end
