unit ContactName;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Mask;

type
  TContactNameDialog = class(TForm)
    FirstNameLabel: TLabel;
    FirstNameEdit: TEdit;
    LastNameLabel: TLabel;
    LastNameEdit: TEdit;
    Label3: TLabel;
    OKButton: TButton;
    CancelButton: TButton;
    dpSchedMarkDSate: TDateTimePicker;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    edtPhone: TMaskEdit;
    Label4: TLabel;
    ComboBox1: TComboBox;
    procedure OKButtonClick(Sender: TObject);
    procedure NameEditChange(Sender: TObject);
    procedure dpSchedMarkDSateChange(Sender: TObject);
  public
    class function GetOngoingContactName(TicketID: Integer; var First, Last: string): Boolean;
  end;

implementation

uses OdMiscUtils, OdExceptions;

{$R *.dfm}

procedure TContactNameDialog.OKButtonClick(Sender: TObject);
begin
  if IsEmpty(FirstNameEdit.Text) or IsEmpty(LastNameEdit.Text) then
    raise EOdDataEntryError.Create('Both a first and last name are required');
end;

procedure TContactNameDialog.NameEditChange(Sender: TObject);
begin
  OKButton.Enabled := NotEmpty(FirstNameEdit.Text) and NotEmpty(LastNameEdit.Text);
end;

procedure TContactNameDialog.dpSchedMarkDSateChange(Sender: TObject);
begin
  // max date 10 business past due date
  { TODO -cFCO : max date 10 business past due date }
end;

class function TContactNameDialog.GetOngoingContactName(TicketID: Integer;
  var First, Last: string): Boolean;
var
  Dialog: TContactNameDialog;
begin
  Dialog := TContactNameDialog.Create(nil);
  try
    Result := Dialog.ShowModal = mrOk;
    if Result then begin
      First := Dialog.FirstNameEdit.Text;
      Last := Dialog.LastNameEdit.Text;
      if IsEmpty(First) or IsEmpty(Last) then
        Result := False;
    end
  finally
    FreeAndNil(Dialog);
  end;
end;

end.
