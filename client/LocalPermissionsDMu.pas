unit LocalPermissionsDMu;
{QMANTWO-681 EB - Splitting off permissions calls from the DMu}

interface

uses
  SysUtils, Classes, DB, dbisamtb,
  DMu, SyncEngineU, QMConst;

type
  TPermissionsDM = class(TDataModule)
    Rights: TDBISAMTable;
    GroupRights: TDBISAMTable;
    RightDefinition: TDBISAMTable;
  private

  public
    PermissionsEngine: TLocalCache;
    function CanI(const RightName: string; DisplayPermissionFailure: Boolean = False): Boolean;
    function CanEmployee(EmpID: Integer; const RightName: string; DisplayPermissionFailure: Boolean = False): Boolean;
    function CanIUseComputer(EmpID: Integer) : Boolean;
    function CanIRunReports: Boolean;
    function IsRightActive(const RightName: string): Boolean;
    function GetRightID(const RightName: string): Integer;
    function GetEmployeeLimitation(EmpID: Integer; const RightName: string): string;
    function GetLimitation(const RightName: string): string;
    function GetLimitationDef(const RightName, Default: string): string;
    procedure GetLimitationList(const RightName: string; Limitations: TStrings; IncludeBlank: Boolean = False);
    function GetLimitationListCount(const RightName: string): Integer;
    function IsInEmployeeLimitationList(EmpID: Integer; const RightName: string; const Limitation: string): Boolean;
    function IsInLimitationList(const RightName: string; const Limitation: string): Boolean;
    function IsTicketManager: Boolean;
    function IsWorkOrderManager: Boolean;

    function EmployeeHasLitigationRights: Boolean;
    function EmployeeHasBillingRights: Boolean;
    procedure EmployeeRightsList(List: TStrings; const IncludeBlank: Boolean=False);

  end;

var
  PermissionsDM: TPermissionsDM;

implementation

{$R *.dfm}
uses
  OdDBISAMUtils, Variants, OdWmi, JclStrings, OdMiscUtils;

{ TPermissionsDM }




function TPermissionsDM.CanEmployee(EmpID: Integer; const RightName: string;
  DisplayPermissionFailure: Boolean): Boolean;
var
  RightID: Integer;
begin
  RightID := GetRightID(RightName);
  Result := False;
  Rights.Close;
  Rights.Open;
  if Rights.Locate('emp_id;right_id', VarArrayOf([EmpID, RightID]), []) then
    if Rights.FieldByName('Allowed').AsString = 'Y' then
      Result := True;
  if not Result then begin
    GroupRights.Open;
    if GroupRights.Locate('emp_id;right_id', VarArrayOf([EmpID, RightID]), []) then begin
      if GroupRights.FieldByName('Allowed').AsString = 'Y' then
        Result := True;
    end;
  end;
  if DisplayPermissionFailure and (not Result) then
    raise Exception.CreateFmt('You do not have the necessary permissions to perform this action (%d)', [RightID]);
end;


function TPermissionsDM.CanI(const RightName: string;
  DisplayPermissionFailure: Boolean): Boolean;
begin
  Result := CanEmployee(DM.UQState.EmpID, RightName, DisplayPermissionFailure);
end;


function TPermissionsDM.CanIRunReports: Boolean;
const
  EmpRightSQL = 'select count(*) from employee_right '+
        'where  '+
        ' right_id in (select right_id from right_definition where right_type = ''%s'')'+
        ' and allowed= ''Y'' ';
  GrpRightSQL = 'select count(*) from emp_group_right '+
        'where  '+
        ' right_id in (select right_id from right_definition where right_type = ''%s'')'+
        ' and allowed= ''Y'' ';
var
  EmpRights: TDataSet;
  GrpRights: TDataSet;
begin
  GrpRights := nil;
  EmpRights := DM.Engine.OpenQuery(Format(EmpRightSQL, [ReportRightType]));
  try
    Result := CanI(RightReportScreen) or (EmpRights.Fields[0].AsInteger > 0);
    if not Result then begin
      GrpRights := DM.Engine.OpenQuery(Format(GrpRightSQL, [ReportRightType]));
      Result := (GrpRights.Fields[0].AsInteger > 0);
    end;
  finally
    FreeAndNil(EmpRights);
    FreeAndNil(GrpRights);
  end;
end;

function TPermissionsDM.CanIUseComputer(EmpID: Integer): Boolean;
var
  AssignedSerial, CurrentSerial: string;

begin
  CurrentSerial := GetLocalComputerSerial;
  AssignedSerial := GetEmployeeLimitation(EmpID, RightSyncMyComputerSerialNum);
  CurrentSerial := UpperCase(CurrentSerial);
  AssignedSerial := UpperCase(AssignedSerial);
  if (CurrentSerial = AssignedSerial) or (AssignedSerial = '') then begin
    Result := True;
  end
  else
   {MisMatch}
    Result := False;
end;

function TPermissionsDM.EmployeeHasBillingRights: Boolean;
begin
  Result := CanI(RightBillingAdjustmentsView) or CanI(RightBillingAdjustmentsAdd)
    or CanI(RightBillingRun) or CanI(RightBillingDelete)
    or CanI(RightBillingCommit) or CanI(RightBillingUncommit)
    or CanI(RightBillingReexport);
end;

function TPermissionsDM.EmployeeHasLitigationRights: Boolean;
begin
  Result := PermissionsDM.CanI(RightLitigationView) or PermissionsDM.CanI(RightLitigationEdit);
end;

procedure TPermissionsDM.EmployeeRightsList(List: TStrings;
  const IncludeBlank: Boolean);
begin
    DM.GetListFromDataSet(RightDefinition, List, 'entity_data', 'right_id', IncludeBlank);
end;

function TPermissionsDM.GetEmployeeLimitation(EmpID: Integer;
  const RightName: string): string;
var
  RightID: Integer;
begin
  Assert(EmpID > 0);
  Assert(RightName <> '');
  Result := '';
  RightID := GetRightID(RightName);
  Rights.Open;
  GroupRights.Open;
  if Rights.Locate('emp_id;right_id', VarArrayOf([EmpID, RightID]), []) and (Rights.FieldByName('allowed').AsString = 'Y') then
    Result := Rights.FieldByName('limitation').AsString
  else if GroupRights.Locate('emp_id;right_id', VarArrayOf([EmpID, RightID]), []) and (GroupRights.FieldByName('allowed').AsString = 'Y') then
    Result := GroupRights.FieldByName('limitation').AsString;

  if Result = '-' then
    Result := '';
end;

function TPermissionsDM.GetLimitation(const RightName: string): string;
begin
  Result := GetEmployeeLimitation(DM.UQState.EmpID, RightName);
end;

function TPermissionsDM.GetLimitationDef(const RightName,
  Default: string): string;
begin
  Result := GetLimitation(RightName);
  if IsEmpty(Result) or (Result = '-') then
    Result := Default;
end;

procedure TPermissionsDM.GetLimitationList(const RightName: string;
  Limitations: TStrings; IncludeBlank: Boolean);
var
  LimitationStr: string;
begin
  Assert(Assigned(Limitations));
  Limitations.Clear;
  LimitationStr := GetLimitation(RightName);
  StrToStrings(LimitationStr, ',', Limitations, False);
  if IncludeBlank then
    Limitations.Insert(0, '');
end;

function TPermissionsDM.GetLimitationListCount(
  const RightName: string): Integer;
var
  Limitations: TStringList;
begin
  Limitations := TStringList.Create;
  try
    GetLimitationList(RightName, Limitations);
    Result := Limitations.Count;
  finally
    FreeAndNil(Limitations);
  end;
end;


function TPermissionsDM.GetRightID(const RightName: string): Integer;
begin
  Result := 0;
  RightDefinition.Open;
  if RightDefinition.Locate('entity_data', VarArrayOf([RightName]), []) then
    Result := RightDefinition.FieldByName('right_id').AsInteger;
end;

function TPermissionsDM.IsInEmployeeLimitationList(EmpID: Integer;
  const RightName, Limitation: string): Boolean;
var
  Limitations: TStringList;
begin
  Limitations := TStringList.Create;
  try
    StrToStrings(GetEmployeeLimitation(EmpID, RightName), ',', Limitations, False);
    Result := (Limitations.IndexOf(Limitation) > -1);
  finally
    FreeAndNil(Limitations);
  end;
end;

function TPermissionsDM.IsInLimitationList(const RightName,
  Limitation: string): Boolean;
begin
  Result := IsInEmployeeLimitationList(DM.UQState.EmpID, RightName, Limitation);
end;

function TPermissionsDM.IsRightActive(const RightName: string): Boolean;
begin
  Result := CanI(RightName, False);
end;

function TPermissionsDM.IsTicketManager: Boolean;
begin
  Result := CanI(RightTicketManagement);
end;

function TPermissionsDM.IsWorkOrderManager: Boolean;
begin
  Result := CanI(RightWorkOrdersManagement);
end;

end.
