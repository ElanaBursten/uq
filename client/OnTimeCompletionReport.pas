unit OnTimeCompletionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, OdRangeSelect;

type
  TOnTimeCompletionReportForm = class(TReportBaseForm)
    Label2: TLabel;
    Label3: TLabel;
    ClientList: TCheckListBox;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    CallCenterList: TCheckListBox;
    LimitManagerBox: TCheckBox;
    DueDateRange: TOdRangeSelectFrame;
    DueDateLabel: TLabel;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure CallCenterListClick(Sender: TObject);
    procedure LimitManagerBoxClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, OdExceptions;

{$R *.dfm}

procedure TOnTimeCompletionReportForm.ValidateParams;
var
  CallCenters: string;
  CheckedItems: string;
begin
  inherited;
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters = '' then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select a call center.');
  end;

  CheckedItems := GetCheckedItemString(ClientList, True);
  if CheckedItems = '' then begin
    ClientList.SetFocus;
    raise EOdEntryRequired.Create('Please select the clients to report on.');
  end;

  if LimitManagerBox.Checked then
    RequireComboBox(ManagersComboBox, 'Please select a manager.');

  SetReportID('OnTimeCompletion');
  SetParamDate('DueDate', DueDateRange.FromDate);
  SetParamDate('EndDueDate', DueDateRange.ToDate);
  SetParam('CallCenters', CallCenters);
  SetParam('Clients', CheckedItems);

  if LimitManagerBox.Checked then
    SetParamIntCombo('ManagerID', ManagersComboBox, True)
  else
    SetParamInt('ManagerID', -1);

end;

procedure TOnTimeCompletionReportForm.InitReportControls;
begin
  inherited;
  DueDateRange.SelectDateRange(rsYesterday);
  SetupCallCenterList(CallCenterList.Items, False);
  SetUpManagerList(ManagersComboBox);
  LimitManagerBox.Checked := False;
  ManagersComboBox.Enabled := False;
end;

procedure TOnTimeCompletionReportForm.SelectAllButtonClick(
  Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, True);
end;

procedure TOnTimeCompletionReportForm.ClearAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, False);
end;

procedure TOnTimeCompletionReportForm.CallCenterListClick(Sender: TObject);
var
  CallCenters: string;
begin
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure TOnTimeCompletionReportForm.LimitManagerBoxClick(
  Sender: TObject);
begin
  inherited;
  ManagersComboBox.Enabled := LimitManagerBox.Checked;
end;

end.

