inherited DamageDetailsReportForm: TDamageDetailsReportForm
  Left = 339
  Top = 168
  Caption = 'DamageDetailsReportForm'
  ClientHeight = 520
  ClientWidth = 774
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 57
    Height = 13
    Caption = 'Damage ID:'
  end
  object LabelSelectDamageImages: TLabel [1]
    Left = 0
    Top = 228
    Width = 171
    Height = 13
    Caption = 'Select damage attachments to print'
  end
  object LabelSelectTicketImages: TLabel [2]
    Left = 379
    Top = 228
    Width = 159
    Height = 13
    Caption = 'Select ticket attachments to print'
  end
  object EditUQDamageID: TEdit [3]
    Left = 71
    Top = 8
    Width = 104
    Height = 21
    TabOrder = 0
    OnChange = EditUQDamageIDChange
    OnKeyPress = EditUQDamageIDKeyPress
  end
  object StyleRadioGroup: TRadioGroup [4]
    Left = 194
    Top = 3
    Width = 129
    Height = 57
    Caption = 'Report Style'
    ItemIndex = 0
    Items.Strings = (
      'Detailed'
      'Short')
    TabOrder = 1
    OnClick = StyleRadioGroupClick
  end
  object SectionsGroup: TGroupBox [5]
    Left = 0
    Top = 68
    Width = 662
    Height = 109
    Caption = 'Sections to Include'
    TabOrder = 2
    object IncludeNarrative: TCheckBox
      Left = 14
      Top = 39
      Width = 150
      Height = 17
      Caption = 'Include Narrative'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object IncludeDiscussion: TCheckBox
      Left = 14
      Top = 61
      Width = 150
      Height = 17
      Caption = 'Include Discussion'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object IncludeConclusion: TCheckBox
      Left = 14
      Top = 83
      Width = 150
      Height = 17
      Caption = 'Include Conclusion'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object IncludeDamageAttachments: TCheckBox
      Left = 190
      Top = 17
      Width = 162
      Height = 17
      Caption = 'Include Damage Attachments'
      Checked = True
      State = cbChecked
      TabOrder = 4
      OnClick = IncludeDamageAttachmentsClick
    end
    object IncludeNotes: TCheckBox
      Left = 190
      Top = 83
      Width = 150
      Height = 17
      Caption = 'Include Notes'
      TabOrder = 7
    end
    object IncludeThirdParty: TCheckBox
      Left = 190
      Top = 39
      Width = 150
      Height = 17
      Caption = 'Include Third Party'
      TabOrder = 5
    end
    object IncludeLitigation: TCheckBox
      Left = 190
      Top = 61
      Width = 150
      Height = 17
      Caption = 'Include Litigation'
      TabOrder = 6
    end
    object IncludeTicket: TCheckBox
      Left = 369
      Top = 39
      Width = 150
      Height = 17
      Caption = 'Include Ticket Detail '
      Checked = True
      State = cbChecked
      TabOrder = 9
      OnClick = IncludeTicketClick
    end
    object IncludeDamage: TCheckBox
      Left = 14
      Top = 17
      Width = 150
      Height = 17
      Caption = 'Include Damage Claim'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = IncludeDamageClick
    end
    object IncludeHistory: TCheckBox
      Left = 369
      Top = 17
      Width = 150
      Height = 17
      Caption = 'Include History'
      TabOrder = 8
    end
    object IncludeTicketAttachments: TCheckBox
      Left = 369
      Top = 61
      Width = 150
      Height = 17
      Caption = 'Include Ticket Attachments'
      Checked = True
      State = cbChecked
      TabOrder = 10
      OnClick = IncludeTicketAttachmentsClick
    end
  end
  object DamageAttachmentList: TCheckListBox [6]
    Left = 0
    Top = 248
    Width = 285
    Height = 249
    ItemHeight = 13
    TabOrder = 4
  end
  object SelectAllButton: TButton [7]
    Left = 294
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 5
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [8]
    Left = 294
    Top = 281
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 6
    OnClick = ClearAllButtonClick
  end
  object TicketAttachmentList: TCheckListBox [9]
    Left = 379
    Top = 248
    Width = 285
    Height = 249
    ItemHeight = 13
    TabOrder = 7
  end
  object SelectAllButton2: TButton [10]
    Left = 675
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 8
    OnClick = SelectAllButton2Click
  end
  object ClearAllButton2: TButton [11]
    Left = 675
    Top = 281
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 9
    OnClick = ClearAllButton2Click
  end
  object UpdateListButton: TButton [12]
    Left = 248
    Top = 198
    Width = 161
    Height = 25
    Caption = '&Refresh Attachment Lists'
    ParentShowHint = False
    ShowHint = False
    TabOrder = 3
    OnClick = UpdateListButtonClick
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 65534
  end
end
