inherited LoadReportForm: TLoadReportForm
  Left = 347
  Top = 258
  Caption = 'Locator Load Report'
  ClientHeight = 118
  ClientWidth = 328
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 0
    Top = 11
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label6: TLabel [1]
    Left = 0
    Top = 40
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [2]
    Left = 92
    Top = 8
    Width = 205
    Height = 21
    Style = csDropDownList
    DropDownCount = 25
    ItemHeight = 13
    TabOrder = 0
  end
  object EmployeeStatusCombo: TComboBox [3]
    Left = 92
    Top = 37
    Width = 181
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
end
