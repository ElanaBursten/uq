inherited AssetManagementForm: TAssetManagementForm
  Left = 271
  Top = 193
  Caption = 'Asset Management'
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 205
    Top = 0
    Width = 8
    Height = 400
    Beveled = True
  end
  object AssetPanel: TPanel
    Left = 213
    Top = 0
    Width = 335
    Height = 400
    Align = alClient
    TabOrder = 1
    object ActionPanel: TPanel
      Left = 1
      Top = 1
      Width = 333
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object AddAssetButton: TButton
        Left = 8
        Top = 10
        Width = 89
        Height = 25
        Action = AddAssetAction
        TabOrder = 0
      end
      object RemoveAssetButton: TButton
        Left = 112
        Top = 10
        Width = 89
        Height = 25
        Action = RemoveAssetAction
        TabOrder = 1
      end
      object SaveChangesButton: TButton
        Left = 216
        Top = 10
        Width = 89
        Height = 25
        Action = SaveChangesAction
        TabOrder = 2
      end
    end
    object AssetGrid: TcxGrid
      Left = 1
      Top = 49
      Width = 333
      Height = 350
      Align = alClient
      DragMode = dmAutomatic
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object AssetGridView: TcxGridDBTableView
        DragMode = dmAutomatic
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = AssetSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'asset_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.ImmediateEditor = False
        OptionsData.Appending = True
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsView.ShowEditButtons = gsebForFocusedRecord
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object ColAssetCode: TcxGridDBColumn
          Caption = 'Asset Code'
          DataBinding.FieldName = 'asset_code'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownListStyle = lsEditFixedList
          Properties.DropDownRows = 15
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 67
        end
        object ColAssetNumber: TcxGridDBColumn
          Caption = 'Asset Number'
          DataBinding.FieldName = 'asset_number'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 114
        end
        object ColAssetID: TcxGridDBColumn
          DataBinding.FieldName = 'asset_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Visible = False
          Options.Filtering = False
          Width = 109
        end
        object ColModifiedDate: TcxGridDBColumn
          DataBinding.FieldName = 'modified_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Properties.ReadOnly = False
          Visible = False
          Options.Filtering = False
          Width = 191
        end
        object ColSerialNum: TcxGridDBColumn
          Caption = 'Serial #'
          DataBinding.FieldName = 'serial_num'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 82
        end
        object ColCondition: TcxGridDBColumn
          Caption = 'Condition'
          DataBinding.FieldName = 'condition'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownListStyle = lsEditFixedList
          Properties.DropDownRows = 13
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 55
        end
        object ColDescription: TcxGridDBColumn
          Caption = 'Description'
          DataBinding.FieldName = 'description'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 159
        end
        object ColSolomonProfitCenter: TcxGridDBColumn
          Caption = 'Profit Ctr'
          DataBinding.FieldName = 'solomon_profit_center'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DropDownListStyle = lsEditFixedList
          Properties.DropDownRows = 15
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 72
        end
        object ColComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 137
        end
        object ColCost: TcxGridDBColumn
          Caption = 'Cost'
          DataBinding.FieldName = 'cost'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taRightJustify
          Properties.AssignedValues.MaxValue = True
          Properties.AssignedValues.MinValue = True
          Properties.DecimalPlaces = 2
          Properties.DisplayFormat = '$,0.00;-$,0.00'
          Properties.Nullable = False
          Properties.ReadOnly = False
          HeaderAlignmentHorz = taRightJustify
          Options.Filtering = False
          Width = 60
        end
      end
      object AssetGridLevel: TcxGridLevel
        GridView = AssetGridView
      end
    end
  end
  object EmpList: TListBox
    Left = 0
    Top = 0
    Width = 205
    Height = 400
    Align = alLeft
    ItemHeight = 13
    TabOrder = 0
    OnClick = EmpListClick
    OnDragDrop = EmpListDragDrop
    OnDragOver = EmpListDragOver
  end
  object AssetSource: TDataSource
    DataSet = AssetQuery
    Left = 244
    Top = 104
  end
  object AssetQuery: TDBISAMQuery
    AfterEdit = AssetQueryAfterEdit
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select a.*, aa.emp_id, aa.modified_date'
      'from asset_assignment aa'
      '  inner join asset a on aa.asset_id = a.asset_id'
      '  inner join asset_type at on a.asset_code = at.asset_code'
      'where aa.emp_id =  :EmpID'
      '  and aa.active'
      'order by a.asset_code')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 200
      end>
    Left = 244
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 200
      end>
  end
  object AssetActions: TActionList
    OnUpdate = AssetActionsUpdate
    Left = 244
    Top = 200
    object AddAssetAction: TAction
      Caption = 'Add Asset'
      OnExecute = AddAssetActionExecute
    end
    object RemoveAssetAction: TAction
      Caption = 'Remove Asset'
      OnExecute = RemoveAssetActionExecute
    end
    object SaveChangesAction: TAction
      Caption = 'Save Changes'
      OnExecute = SaveChangesActionExecute
    end
  end
end
