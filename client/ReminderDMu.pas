unit ReminderDMu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DMu;   // ,kbmMemTable

type
  TReminderDM = class(TDataModule)
    // Reminder: TkbmMemTable;
  private
  public
    { Public declarations }
    procedure AddReminder(TaskCode, Description, RepeatInterval: string;
            TriggerDay: Integer; NextDueDate: TDateTime);
    function WhatsDueDataSet(AsOf: TDateTime): TDataSet;
    procedure CompleteAndReschedule(TaskCode: string; DoneOn: TDateTime);
  end;

var
  ReminderDM: TReminderDM;

implementation

{$R *.DFM}

{ TReminderDM }

procedure TReminderDM.AddReminder(TaskCode, Description,
  RepeatInterval: string; TriggerDay: Integer; NextDueDate: TDateTime);
begin
{
  DoSQL('INSERT INTO Reminder VALUES (:TaskCode, :Description, :RepeatInterval, :TriggerDay, :NextDueDateDT, NULL)',
    [TaskCode, Description, RepeatInterval, TriggerDay, NextDueDate]);
  }
end;

function Matches(D: TDateTime; Interval: string; Match: Integer): Boolean;
var
  Year, Month, Day: Word;
begin
  if Interval='month' then begin
    DecodeDate(D, Year, Month, Day);
    Result := Day = Match;
  end else if Interval='week' then begin
    Result := DayOfWeek(D) = Match;
  end else
    raise Exception.Create('Interval not recognized: ' + Interval);
end;

function NextDate(LastDate: TDateTime; Interval: string; Match: Integer): TDateTime;
var
  Limit: TDateTime;
begin
  Limit := LastDate + 1000;  // so we catch it eventually
  Result := LastDate;
  repeat
    Result := Result + 1;
    if Result>Limit then
      raise Exception.Create('Could not find next date');
  until Matches(Result, Interval, Match);
end;

procedure TReminderDM.CompleteAndReschedule(TaskCode: string;
  DoneOn: TDateTime);
begin
{
  with Reminder do begin
    Params[0].AsString := TaskCode;
    Open;
    try
      if EOF then
        raise Exception.Create('Task not found: ' + TaskCode);
      Edit;
      FieldByName('LastDoneDate').AsDateTime := DoneOn;
      FieldByName('NextDueDate').AsDateTime := NextDate(DoneOn,
          FieldByName('RepeatInterval').AsString,
          FieldByName('TriggerDay').AsInteger);
      //DM.CommitDatasets([Reminder]);
    finally
      Close;
    end;
  end;
  }
end;

function TReminderDM.WhatsDueDataSet(AsOf: TDateTime): TDataSet;
begin
  Result := nil;
  Assert(False);
{
  with Due do begin
    Close;
    Params[0].AsDateTime := AsOf;
    Open;
  end;
  Result := Due;
 }
end;

end.

