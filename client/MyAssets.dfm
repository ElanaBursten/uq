inherited MyAssetsForm: TMyAssetsForm
  Left = 266
  Top = 198
  Caption = 'My Assets'
  ClientHeight = 438
  ClientWidth = 574
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object NoAssetsPanel: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 438
    Align = alClient
    BevelOuter = bvNone
    Caption = 'There are no assets currently assigned to you.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object AssetGrid: TDBGrid
    Left = 0
    Top = 0
    Width = 574
    Height = 438
    Align = alClient
    DataSource = AssetSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
    Columns = <
      item
        Expanded = False
        FieldName = 'description'
        Title.Caption = 'Asset'
        Width = 175
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'asset_number'
        Title.Caption = 'Asset Number'
        Width = 175
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'condition_description'
        Title.Caption = 'Condition'
        Width = 175
        Visible = True
      end>
  end
  object AssetSource: TDataSource
    DataSet = Assets
    Left = 44
    Top = 84
  end
  object Assets: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select asset.asset_id, asset.asset_code, asset.asset_number,'
      
        '  reference.description as condition_description, asset_type.des' +
        'cription'
      'from asset'
      
        '  inner join asset_assignment on asset.asset_id = asset_assignme' +
        'nt.asset_id'
      
        '  left join asset_type on asset.asset_code = asset_type.asset_co' +
        'de'
      
        '  left join reference on (asset.condition = reference.code and r' +
        'eference.type = '#39'acond'#39')'
      'where asset_assignment.emp_id = :EmpID'
      '  and asset_assignment.active'
      'order by asset_type.description')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 0
      end>
    Left = 44
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 0
      end>
  end
end
