inherited PlusMgrListForm: TPlusMgrListForm
  Caption = 'Employee Plus List Report'
  ClientHeight = 701
  ClientWidth = 1015
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Headerpnl: TPanel
    Left = 0
    Top = 0
    Width = 1015
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object ReportingToLbl: TLabel
      Left = 5
      Top = 10
      Width = 76
      Height = 14
      Caption = 'Reporting To:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object MgrLbl: TLabel
      Left = 87
      Top = 10
      Width = 20
      Height = 14
      Caption = '-----'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object PLUSAllowedOnlyCBx: TCheckBox
      Left = 304
      Top = 8
      Width = 257
      Height = 17
      Caption = 'Whitelisted employees only'
      TabOrder = 0
      OnClick = PLUSAllowedOnlyCBxClick
    end
  end
  object PlusGrid: TcxGrid
    Left = 0
    Top = 32
    Width = 1015
    Height = 669
    Align = alClient
    DragMode = dmAutomatic
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object PlusView: TcxGridDBTableView
      DragMode = dmAutomatic
      PopupMenu = PlusPopup
      Navigator.Buttons.CustomButtons = <>
      Navigator.InfoPanel.Visible = True
      OnCustomDrawCell = PlusViewCustomDrawCell
      DataController.DataSource = EmpPlusSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Selection = SharedDevExStyleData.GridSelectionStyle
      object ColEmpID: TcxGridDBColumn
        Caption = 'Emp ID'
        DataBinding.FieldName = 'emp_id'
        Visible = False
        Options.Editing = False
        Width = 55
      end
      object ColEmpNumber: TcxGridDBColumn
        Caption = 'Emp #'
        DataBinding.FieldName = 'emp_number'
        Options.Editing = False
        Width = 63
      end
      object ColLastName: TcxGridDBColumn
        Caption = 'Last Name'
        DataBinding.FieldName = 'last_name'
        BestFitMaxWidth = 1
        Options.Editing = False
        Width = 99
      end
      object ColFirstName: TcxGridDBColumn
        Caption = 'First Name'
        DataBinding.FieldName = 'first_name'
        BestFitMaxWidth = 1
        Options.Editing = False
        Width = 96
      end
      object ColEmpShortName: TcxGridDBColumn
        Caption = 'Short Name'
        DataBinding.FieldName = 'short_name'
        BestFitMaxWidth = 1
        Options.Editing = False
        Width = 151
      end
      object ColAllowed: TcxGridDBColumn
        Caption = 'Whitelisted'
        DataBinding.FieldName = 'tg_active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DisplayGrayed = 'False'
        Options.Editing = False
        Styles.Content = SharedDevExStyleData.BoldLargeRedFontStyle
        Width = 66
      end
      object ColPlusCode: TcxGridDBColumn
        Caption = 'Plus Code'
        DataBinding.FieldName = 'PLUS_code'
        Options.Editing = False
        Width = 143
      end
      object ColPlusDesc: TcxGridDBColumn
        Caption = 'Plus Desc'
        DataBinding.FieldName = 'PLUS_desc'
        Options.Editing = False
        Width = 216
      end
      object ColModDate: TcxGridDBColumn
        Caption = 'Mod Date'
        DataBinding.FieldName = 'modified_date'
        PropertiesClassName = 'TcxTextEditProperties'
        Options.Editing = False
        Width = 121
      end
      object ColModifiedBy: TcxGridDBColumn
        Caption = 'Modified By'
        DataBinding.FieldName = 'modified_by'
        Options.Editing = False
      end
    end
    object PlusGridLevel: TcxGridLevel
      GridView = PlusView
    end
  end
  object EmpPlusSource: TDataSource
    Left = 244
    Top = 192
  end
  object PlusPopup: TPopupMenu
    Left = 216
    Top = 216
  end
end
