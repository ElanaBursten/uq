unit DamageSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, ExtCtrls, StdCtrls, OdRangeSelect, OdXMLToDataSet,
  Buttons, DB, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit;

type
  TDamageSearchCriteria = class(TSearchCriteria)
    InvestigatorLabel: TLabel;
    TicketNumberLabel: TLabel;
    ExcavatorLabel: TLabel;
    CityLabel: TLabel;
    LocationLabel: TLabel;
    UQDamageIDLabel: TLabel;
    TicketNumber: TEdit;
    Investigator: TEdit;
    Excavator: TEdit;
    City: TEdit;
    Location: TEdit;
    UQDamageID: TEdit;
    TypeLabel: TLabel;
    DamageType: TComboBox;
    DamageDate: TOdRangeSelectFrame;
    State: TComboBox;
    StateLabel: TLabel;
    ProfitCenter: TComboBox;
    ProfitCenterLabel: TLabel;
    DueDate: TOdRangeSelectFrame;
    UtilityCoDamagedLabel: TLabel;
    ClientClaimID: TEdit;
    ClientClaimIDLabel: TLabel;
    WorkToBeDoneLabel: TLabel;
    WorkToBeDone: TEdit;
    UQResp: TCheckBox;
    EXResp: TCheckBox;
    UtilityResp: TCheckBox;
    Attachments: TComboBox;
    AttachmentsLabel: TLabel;
    ClaimStatus: TComboBox;
    ClaimStatusLabel: TLabel;
    IncludeEstimates: TCheckBox;
    Invoices: TComboBox;
    InvoicesLabel: TLabel;
    InvoiceCodeLabel: TLabel;
    InvoiceCode: TComboBox;
    Label3: TLabel;
    ExcavationType: TComboBox;
    ExcvTypeLabel: TLabel;
    LocatorLabel: TLabel;
    Locator: TEdit;
    cbUtilityCo: TcxComboBox;
    cbQuickClose: TCheckBox;    //qm-676 sr
    btnQuickClose: TButton;     //qm-676 sr
    function GetClaimStatusCode: string;
    function GetInvoiceCode: string;
    procedure FormCreate(Sender: TObject);
    procedure btnQuickCloseClick(Sender: TObject);  //qm-676 sr
  private
    function GetExcavationType: string;
    procedure SetUtilCoLookup;
    procedure PerformQuickClose(uqDamageID: integer);   //qm-676 sr
  public
    procedure BeforeItemSelected(ID: Variant); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure InitializeSearchCriteria; override;
    procedure Showing; override;
  end;

  TUtilityCoObj = class(TObject)    //qm-396  sr
    private
    Active_Ind:Boolean;
    Ref_ID:integer;
  end;

implementation

{$R *.dfm}

uses DMu, OdVclUtils, QMConst, OdHourGlass, DateUtils,
LocalPermissionsDmu, LocalEmployeeDMu, LocalDamageDmu;

{ TDamageSearchCriteria }

procedure TDamageSearchCriteria.BeforeItemSelected(ID: Variant);
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  LDamageDM.UpdateDamageInCache(ID);
end;

procedure TDamageSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
  DM.GetRefDisplayList('state', State.Items, '', True);
  UQResp.Caption := DM.UQState.CompanyShortName;

  DM.GetAttachmentCriteria(Attachments.Items);

  DM.GetInvoiceCriteria(Invoices.Items);

  DM.GetRefDisplayList('claimstat', ClaimStatus.Items);
  ClaimStatus.Items.Insert(0, '');

  DM.GetRefDisplayList('invcode', InvoiceCode.Items);
  InvoiceCode.Items.Insert(0, '');

  DM.GetRefDisplayList('excntype', ExcavationType.Items, '', True);

  if PermissionsDM.CanI(RightDamagesViewAll) then begin
    if PermissionsDM.GetLimitationListCount(RightDamagesViewAll) > 0 then begin
      PermissionsDM.GetLimitationList(RightDamagesViewAll, ProfitCenter.Items);
    end;
  end;
  RestoreCriteria;
end;

function TDamageSearchCriteria.GetXmlString: string;
var
  LimitInvestigatorId: Integer;
begin
  if PermissionsDM.CanI(RightDamagesViewAll) then
    LimitInvestigatorId := 0
  else
    LimitInvestigatorId := DM.EmpID;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeFindDamage, '');

  DM.CallingServiceName('DamageSearch9');
  Result := DM.Engine.Service.DamageSearch9(
    DM.Engine.SecurityInfo,
    TicketNumber.Text,
    StrToIntDef(UQDamageID.Text, 0),
    DamageDate.FromDate, DamageDate.ToDate,
    DueDate.FromDate, DueDate.ToDate,
    Investigator.Text,
    Excavator.Text,
    Location.Text,
    City.Text,
    State.Text,
    ProfitCenter.Text,
    DamageType.Text,
    cbUtilityCo.Text,  //396  sr
    ClientClaimID.Text,
    WorkToBeDone.Text,
    UQResp.Checked,
    EXResp.Checked,
    UtilityResp.Checked,
    LimitInvestigatorId,
    GetComboObjectInteger(Attachments),
    GetComboObjectInteger(Invoices),
    GetClaimStatusCode,
    IncludeEstimates.Checked,
    GetInvoiceCode,
    GetExcavationType,
    Locator.Text);
end;

procedure TDamageSearchCriteria.btnQuickCloseClick(Sender: TObject);   //qm-676 sr
begin
  inherited;
//  PerformQuickClose(12345); //qm-676 sr
end;
procedure TDamageSearchCriteria.PerformQuickClose(uqDamageID:integer);   //qm-676 sr
begin
  //qm-676 sr
end;


procedure TDamageSearchCriteria.DefineColumns;
begin
  inherited;
  with Defs do begin
    SetDefaultTable('damage');
//    AddColumn('Pick', 10, '', ftUnknown, '', 0, True, False);  //qm-676 sr
    AddColumn('DamageID', 55, 'damage_id', ftUnknown, '', 0, True, False);
    AddColumn('ID', 55, 'uq_damage_id', ftUnknown, '', 0);
    AddColumn('Type', 115, 'damage_type');
    AddColumn('Ticket Number', 85, 'ticket_number');
    AddColumn('Notified Date', 126, 'uq_notified_date', ftDateTime);
    AddColumn('Damage Date', 80, 'damage_date', ftDateTime);
    AddColumn('Due Date', 126, 'due_date', ftDateTime);
    AddColumn('Our Resp', 60, 'uq_resp_code');
    AddColumn('Exc Resp', 60, 'exc_resp_code');
    AddColumn('Utl Resp', 60, 'spc_resp_code');
    AddColumn('Claim Status', 68, 'claim_status');
    AddColumn('City', 130, 'city');
    AddColumn('State', 32, 'state');
    AddColumn('Location', 200, 'location');
    AddColumn('Investigator', 120, 'short_name');
    AddColumn('Excavator Co', 120, 'excavator_company');
    AddColumn('Work To Be Done', 130, 'work_description');
    AddColumn('Work Type', 90, 'work_type'); //QM-117 SR
    AddColumn('Utility Co', 120, 'utility_co_damaged');
    AddColumn('Profit Ctr', 55, 'profit_center');
    AddColumn('County', 75, 'county');
    AddColumn('Estimate', 65, 'amount', ftUnknown, 'damage_estimate');
    AddColumn('Client Claim ID', 100, 'client_claim_id');
    AddColumn('Size / Type', 125, 'size_type');
    AddColumn('Inv Code', 65, 'invoice_code');
    AddColumn('Invoices', 55, 'invoices', ftInteger);
    AddColumn('Attachments', 65, 'attachments', ftInteger);
    AddColumn('Added By', 120, 'added_by_name');
    AddColumn('Locator', 120, 'locator_name');
  end;
end;

procedure TDamageSearchCriteria.FormCreate(Sender: TObject);
begin
  inherited;
  SetUtilCoLookup;  //QMANTWO-810   QM-32
end;

function TDamageSearchCriteria.GetClaimStatusCode: string;
begin
  Result := '';
  if Trim(ClaimStatus.Text) <> '' then
    Result := DM.GetRefCodeForDisplay('claimstat', ClaimStatus.Text);
end;

function TDamageSearchCriteria.GetInvoiceCode: string;
begin
  Result := '';
  if Trim(InvoiceCode.Text) <> '' then
    Result := DM.GetRefCodeForDisplay('invcode', InvoiceCode.Text);
end;

function TDamageSearchCriteria.GetExcavationType: string;
begin
  Result := '';
  if Trim(ExcavationType.Text) <> '' then
    Result := DM.GetRefCodeForDisplay('excntype', ExcavationType.Text);
end;

procedure TDamageSearchCriteria.InitializeSearchCriteria;
begin
  inherited;
  Attachments.ItemIndex := 0;
  Invoices.ItemIndex := 0;
  ClaimStatus.ItemIndex := 0;
  InvoiceCode.ItemIndex := 0;
  ExcavationType.ItemIndex := 0;
  if ProfitCenter.Items.Count > 0 then
    ProfitCenter.ItemIndex := 0;
  DamageDate.SelectDateRange(rsCustom, Today - 135, Today);
end;


procedure TDamageSearchCriteria.SetUtilCoLookup;  //qm-396  sr
var
  MyObject:TUtilityCoObj;  //add a copy of this to Damage Search SR
begin
  cbUtilityCo.Properties.Items.clear;
  cbUtilityCo.Properties.Items.AddObject('', TObject(Ord(mtNone))); 
  with dm.qryUtilityCo do
  begin
    try
      Open;
      while not(Eof) do
      begin
        MyObject:=TUtilityCoObj.Create;
        MyObject.ref_id:= FieldByName('ref_id').AsInteger;
        MyObject.Active_Ind:= FieldByName('Active_ind').AsBoolean;
        cbUtilityCo.Properties.Items.AddObject(FieldByName('description').asString,  MyObject);
        next;
      end;
    finally
      close;
    end;
  end;
end;


end.
