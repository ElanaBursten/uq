unit OdDxXmlUtils;

interface

uses dxDBGrid, DBISAMTb, OdDBISAMUtils;

function CreateGridColumnsFromDef(Grid: TdxDBGrid; Table: TDBISAMDataSet; const Def: TColumnList): Integer;

implementation

uses dxDBCtrl, dxTL, SysUtils;

function CreateGridColumnsFromDef(Grid: TdxDBGrid; Table: TDBISAMDataSet; const Def: TColumnList): Integer;
var
  i: Integer;
  ColInfo: TColumnInfo;
  Column: TdxDBTreeListColumn;
begin
  Grid.ClearGroupColumns;
  Grid.DestroyColumns;
  Result := 0;
  Grid.OptionsView := Grid.OptionsView - [edgoAutoWidth];

  if not Table.Active then
    raise Exception.Create('The dataset must be active');
  Grid.CreateDefaultColumns(Table, Grid);

  for i := 0 to Def.ColumnCount - 1 do
  begin
    Def.GetColumnInfo(ColInfo, i);
    Column := Grid.ColumnByFieldName(ColInfo.FieldName);
    Assert(Assigned(Column));
    Column.Width := ColInfo.Width;
    if ColInfo.Visible then
      Inc(Result, ColInfo.Width);
    Column.Caption := ColInfo.Caption;
    Column.Visible := ColInfo.Visible;
  end;
  Grid.KeyField := Def.GetPrimaryKeyFieldName;
end;

end.
