unit DamageLitigationSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls, OdRangeSelect, OdXMLToDataSet, Mask,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit;

type
  TDamageLitigationSearchCriteria = class(TSearchCriteria)
    ClosedDate: TOdRangeSelectFrame;
    FileNumber: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    PlaintiffSearch: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    PartySearch: TComboBox;
    AttorneySearch: TComboBox;
    Label1: TLabel;
    UQDamageID: TEdit;
    CarrierSearch: TComboBox;
    Label3: TLabel;
    LitigationID: TEdit;
    Accrual: TcxCurrencyEdit;
    Demand: TcxCurrencyEdit;
    Settlement: TcxCurrencyEdit;
  public
    procedure BeforeItemSelected(ID: Variant); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure Showing; override;
  end;

implementation

{$R *.dfm}

uses DMu, OdVCLUtils, SyncEngineU, DB, LocalDamageDmu;

procedure TDamageLitigationSearchCriteria.BeforeItemSelected(ID: Variant);
begin
  LDamageDM.UpdateDamageLitigationInCache(ID);
end;

procedure TDamageLitigationSearchCriteria.DefineColumns;
begin
  inherited;
  with Defs do begin
    SetDefaultTable('damage_litigation');
    AddColumn('Litigation ID', 70, 'litigation_id', ftUnknown, '', 0, True);
    AddColumn('File Number', 90, 'file_number');
    AddColumn('Damage ID', 70, 'uq_damage_id');
    AddColumn('Closed Date', 95, 'closed_date');
    AddColumn('Plaintiff', 100, 'plaintiff', ftString, '', 40);
    AddColumn('Attorney', 125, 'attorney', ftString, '', 40);
    AddColumn('Party', 80, 'party', ftString, '', 15);
    AddColumn('Carrier', 110, 'carrier', ftString, '', 40);
    AddColumn('Demand', 65, 'demand');
    AddColumn('Accrual', 65, 'accrual');
    AddColumn('Settlement', 65, 'settlement');
  end;
end;

procedure TDamageLitigationSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.GetRefDisplayList('litpar', PartySearch.Items);
  PartySearch.Items.Insert(0, '');
  DM.GetRefDisplayList('atty', AttorneySearch.Items);
  AttorneySearch.Items.Insert(0, '');
  SizeComboDropdownToItems(AttorneySearch);
  DM.PopulateCarrierList(CarrierSearch.Items);
  CarrierSearch.Items.Insert(0, '');
  SizeComboDropdownToItems(CarrierSearch);
  RestoreCriteria;
end;

function TDamageLitigationSearchCriteria.GetXmlString: string;

  function GetCarrierID: Integer;
  begin
    Result := 0;
    if CarrierSearch.ItemIndex > 0 then // 0 is blank
      Result := GetComboObjectInteger(CarrierSearch);
  end;

begin
  DM.CallingServiceName('DamageLitigationSearch3');
  Result := DM.Engine.Service.DamageLitigationSearch3(
    DM.Engine.SecurityInfo,
    StrToIntDef(UQDamageID.Text, 0),
    StrToIntDef(LitigationID.Text, 0),
    FileNumber.Text,
    PlaintiffSearch.Text,
    GetCarrierID,
    DM.GetRefCodeForDisplay('atty', AttorneySearch.Text),
    DM.GetRefCodeForDisplay('litpar', PartySearch.Text),
    Demand.Text,
    Accrual.Text,
    Settlement.Text,
    ClosedDate.FromDate,
    ClosedDate.ToDate);
end;

end.
