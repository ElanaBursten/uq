inherited LocateStatusActivityReportForm: TLocateStatusActivityReportForm
  Left = 256
  Top = 179
  Caption = 'LocateStatusActivityReportForm'
  ClientHeight = 403
  ClientWidth = 371
  PixelsPerInch = 96
  TextHeight = 13
  object DataGroup: TGroupBox [0]
    Left = 0
    Top = 129
    Width = 360
    Height = 124
    Caption = 'Data Filter'
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 101
      Width = 80
      Height = 13
      Caption = 'Employee Status'
    end
    object ManagerRadio: TRadioButton
      Left = 8
      Top = 19
      Width = 81
      Height = 17
      Caption = 'Manager'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = UpdateVisibility
    end
    object OfficeRadio: TRadioButton
      Left = 8
      Top = 73
      Width = 81
      Height = 17
      Caption = 'Office'
      TabOrder = 4
      OnClick = UpdateVisibility
    end
    object ManagersComboBox: TComboBox
      Left = 101
      Top = 17
      Width = 160
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 1
      OnEnter = ManagersComboBoxEnter
    end
    object OfficesComboBox: TComboBox
      Left = 101
      Top = 71
      Width = 160
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 5
      OnEnter = OfficesComboBoxEnter
    end
    object EmployeeStatusCombo: TComboBox
      Left = 101
      Top = 98
      Width = 134
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 6
    end
    inline SpecificWorkerBox: TEmployeeSelect
      Left = 91
      Top = 43
      Width = 190
      Height = 25
      TabOrder = 3
      inherited EmployeeCombo: TComboBox
        Left = 10
        Width = 160
      end
    end
    object LocatorRadio: TRadioButton
      Left = 8
      Top = 46
      Width = 81
      Height = 17
      Caption = 'Locator'
      TabOrder = 2
      OnClick = UpdateVisibility
    end
  end
  object DateGroup: TGroupBox [1]
    Left = 0
    Top = 4
    Width = 360
    Height = 124
    Caption = 'Date Filter'
    TabOrder = 0
    object LastReportLabel: TLabel
      Left = 150
      Top = 102
      Width = 106
      Height = 13
      Caption = '1/1/2002, 4:34:34 PM'
    end
    object SinceTimeRadio: TRadioButton
      Left = 8
      Top = 72
      Width = 100
      Height = 17
      Caption = 'Since Time'
      TabOrder = 2
    end
    object DateRangeRadio: TRadioButton
      Left = 8
      Top = 19
      Width = 97
      Height = 17
      Caption = 'Date Range'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object LastReportTimeRadio: TRadioButton
      Left = 8
      Top = 101
      Width = 137
      Height = 17
      Caption = 'Since Last Report Time'
      TabOrder = 4
    end
    inline RangeSelect: TOdRangeSelectFrame
      Left = 105
      Top = 9
      Width = 248
      Height = 58
      TabOrder = 1
      OnEnter = RangeSelectEnter
      inherited FromLabel: TLabel
        Top = 11
        Visible = False
      end
      inherited ToLabel: TLabel
        Top = 11
      end
      inherited DatesLabel: TLabel
        Top = 37
        Visible = False
      end
      inherited DatesComboBox: TComboBox
        Left = 45
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 45
      end
      inherited ToDateEdit: TcxDateEdit
        Width = 88
      end
    end
    object DateTimeSelect: TcxDateEdit
      Left = 150
      Top = 70
      Properties.DateButtons = [btnNow]
      Properties.Kind = ckDateTime
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 3
      Width = 199
    end
  end
  object SortingGroup: TRadioGroup [2]
    Left = 0
    Top = 261
    Width = 362
    Height = 114
    Caption = 'Sorting'
    Items.Strings = (
      'Sync Time, then Locate Time'
      'Locator Name, then Locate Time'
      'Locate Time'
      'Sync Time, Ticket, Client'
      'Locator Name, then Ticket Number')
    TabOrder = 2
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 4
    Top = 395
  end
end
