unit AddLocate;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Db, DBISAMTb, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxImageComboBox, Dialogs;

type
  TAddLocateForm = class(TForm)
    c: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ClientCombo: TComboBox;
    HighProfileCheckBox: TCheckBox;
    CancelBtn: TButton;
    OKBtn: TButton;
    LocatorComboBox: TcxComboBox; //EB QM-142
    lblLocator: TLabel;
    StatusCombo: TcxComboBox; //EB QM-142
    procedure StatusComboChange(Sender: TObject);
    procedure ClientComboChange(Sender: TObject);
    procedure LocatorComboBoxClick(Sender: TObject);
    procedure FormShow(Sender: TObject);  //EB QM-142
  private
    LocatorMgr: integer;
    CurLocatorID: integer; //EB QM-142 Pt. 2
    ShowLocatorList: Boolean;   //EB QM-142
    procedure ClearLocatorList; //EB QM-142
  protected
    FTicketID: Integer;
    procedure Loaded; override;
    procedure SetCallCenter(CallCenter: string; TicketID: Integer);
    function GetSelectedClient: Integer;
    procedure EnableDisableControls;
    function GetNewLocateEmpID(pTicketID: integer): Integer;
    procedure PopulateStatusList;
    procedure LoadLocatorList(ManagerID: integer); //EB QM-142
  public
    NewClientID: integer;
    function SaveRecord(TicketID: Integer): Boolean;
  end;

// Note: The locate is added to the memory dataset (not yet committed to disk)
function ExecuteAddLocateDialog(CallCenter: string; TicketID: Integer; var RetClientID: integer): Boolean;
function ExecuteAddLocateWLocatorDialog(CallCenter: string; TicketID: Integer;  Mgr: Integer; var RetClientID: integer): Boolean;  //EB QM-142

implementation

uses
  DMu, OdVclUtils, Terminology, QMConst, LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.DFM}

{ TAddLocateForm }

procedure TAddLocateForm.SetCallCenter(CallCenter: string; TicketID: Integer);
begin
  FTicketID := TicketID;
  DM.GetClientListForCallCenter(CallCenter, ClientCombo.Items, TicketID);
  SizeComboDropdownToItems(ClientCombo);
  // Exclude the -R status from the list if the user is not a manager
  DM.GetStatusStrings(StatusCombo.Properties.Items, not PermissionsDM.CanI(RightTicketsAddReadyToMarkLocate));
  EnableDisableControls;
end;

function TAddLocateForm.GetSelectedClient: Integer;
begin
  Result := ClientCombo.ItemIndex;
  if Result >= 0 then
    Result := GetComboObjectInteger(ClientCombo);
end;

procedure TAddLocateForm.EnableDisableControls;
begin
 if ShowLocatorList then begin
  OKBtn.Enabled := (ClientCombo.ItemIndex <> -1) and
    (StatusCombo.ItemIndex <> -1) and (LocatorComboBox.ItemIndex <> -1);
  StatusCombo.Enabled := (ClientCombo.ItemIndex <> -1);
 end
 else begin
  OKBtn.Enabled := (ClientCombo.ItemIndex <> -1) and
    (StatusCombo.ItemIndex <> -1);
  StatusCombo.Enabled := (ClientCombo.ItemIndex <> -1);
 end;
end;

procedure TAddLocateForm.FormShow(Sender: TObject);
var
  LocName: string;
 // tempIndex: integer;  //QM-1042 Add Locate Issue
begin
   locName := EmployeeDM.GetEmployeeShortName(CurLocatorID);

//   tempIndex := LocatorComboBox.Properties.Items.IndexOfObject(TObject(CurLocatorID));  //QM-1042 Add Locate Issue
//   if tempIndex > 0 then  //QM-1042 Add Locate Issue
     LocatorComboBox.SelectedItem := LocatorComboBox.Properties.Items.IndexOfObject(TObject(CurLocatorID))
//   else if LocatorComboBox.Properties.Items.Count > 0 then  //QM-1042 Add Locate Issue
 //    LocatorComboBox.SelectedItem := 0;  //QM-1042 Add Locate Issue


end;

procedure TAddLocateForm.StatusComboChange(Sender: TObject);
begin
  EnableDisableControls;
end;

procedure TAddLocateForm.ClearLocatorList;
begin
  LocatorComboBox.Clear;
end;

procedure TAddLocateForm.ClientComboChange(Sender: TObject);
begin
  PopulateStatusList;
  EnableDisableControls;
end;

function TAddLocateForm.SaveRecord(TicketID: Integer): Boolean;
var
  {CurLocatorID,} NewLocateID: Integer;
begin
  Result := False;
  //Don't try to popup the hours dialog on status change during the locate add
  DM.IgnoreSetStatus := True;

  try
    NewClientID := GetSelectedClient;
    if ShowLocatorList then
      CurLocatorID := Integer(LocatorComboBox.Properties.Items.Objects[LocatorComboBox.ItemIndex])
    else
      CurLocatorID := GetNewLocateEmpID(TicketID);
    if NewClientID >= 0 then
      with DM.LocateData do begin
        Append;

        FieldByName('locator_id').AsInteger := CurLocatorID;
        FieldByName('short_name').AsString :=  EmployeeDM.GetEmployeeShortName(FieldByName('locator_id').AsInteger);
        // Some code in TDM expects locate_id to be negative for new locates
        NewLocateID := DM.Engine.GenTempKey;
        FieldByName('locate_id').AsInteger := NewLocateID;
        FieldByName('ticket_id').AsInteger := TicketID;
        FieldByName('client_code').AsString := DM.GetClientCode(NewClientID);
        FieldByName('client_id').AsInteger := NewClientID;
        FieldByName('status').AsString := StatusCombo.Text;
        FieldByName('high_profile').AsBoolean := HighProfileCheckBox.Checked;
        FieldByName('qty_marked').AsInteger := 1;
        FieldByName('active').AsBoolean := True;
        FieldByName('added_by').AsString := IntToStr(DM.UQState.EmpID);
        if TicketID < 0 then begin // a newly added unsynced ticket...
          // The sync uses this data later to find and assign this locate
          FieldByName('LocalStringKey').AsString := FieldByName('locate_id').AsString;
          if ShowLocatorList then
            FieldByName('assigned_to').AsInteger := CurLocatorID
          else
            FieldByName('assigned_to').AsInteger := FieldByName('locator_id').AsInteger;
        end;
        // Do not set the fields that are set by other code, include the highly
        // important Closed field.
        Post;

        Result := True;
      end;

      {If we are adding a different locator, then we need to make sure it is assigned}
      if ShowLocatorList then
        DM.AddLocatetoProject(TicketID, NewClientID, NewLocateID, CurLocatorID);
  finally
    DM.IgnoreSetStatus := False;
  end;

end;

{QM-1042 Version that we will eventually put back in}

//function TAddLocateForm.SaveRecord(TicketID: Integer): Boolean;
//var
//  NewLocateID: Integer;
//  SetDate: TDatetime;
//begin
//  Result := False;
//  //Don't try to popup the hours dialog on status change during the locate add
//  DM.IgnoreSetStatus := True;
//  SetDate := NOW;
//  try
//    NewClientID := GetSelectedClient;
//    if ShowLocatorList then
//      CurLocatorID := Integer(LocatorComboBox.Properties.Items.Objects[LocatorComboBox.ItemIndex])
//    else
//      CurLocatorID := GetNewLocateEmpID(TicketID);
//
//    if NewClientID >= 0 then
//      //QM-1042 Add Locate Error - no longer trying to edit an empty query (when there are no locates)
//      With DM.AddLocateQry do begin
//        NewLocateID := DM.Engine.GenTempKey;
//        ParamByName('locate_id').AsInteger := NewLocateID;
//        ParamByName('ticket_id').AsInteger := TicketID;
//        ParamByName('locator_id').asInteger := CurLocatorID;
//        ParamByName('client_code').asString := DM.GetClientCode(NewClientID);
//        ParamByName('client_id').AsInteger := NewClientID;
//        ParamByName('status').AsString := StatusCombo.Text;
//        ParamByName('high_profile').asBoolean := HighProfileCheckBox.Checked;
//        ParamByName('qty_marked').AsInteger := 1;
//        ParamByName('added_by').AsString := IntToStr(DM.UQState.EmpID);
//        ParamByName('LocalStringKey').AsString := IntToStr(NewLocateID);
//        ParamByName('assigned_to').AsInteger := CurLocatorID;
//        ParamByName('assigned_to_id').AsInteger := CurLocatorID;
//        ParamByName('entry_date').AsDateTime := SetDate;
//        ParamByName('modified_date').AsDateTime := SetDate;
//
//        DM.AddLocateQry.ExecSQL;
//        Result := True;
//
//        DM.RefreshLocateData;
//      end;
//
//      {If we are adding a different locator, then we need to make sure it is assigned}
//      if ShowLocatorList then
//        DM.AddLocatetoProject(TicketID, NewClientID, NewLocateID, CurLocatorID);
//  finally
//    DM.IgnoreSetStatus := False;
//  end;
//
//end;

function TAddLocateForm.GetNewLocateEmpID(pTicketID: integer): Integer;
var
  LocatorID: Integer;
begin
  Result := DM.UQState.EmpID;
  FTicketID := pTicketID;
  if PermissionsDM.CanI(RightTicketsAssignNewLocatesToTicketLocator) then begin
    if DM.GetFirstLocatorOnTicket(pTicketID, LocatorID) then
      Result := LocatorID;
  end;
end;

function ExecuteAddLocateDialog(CallCenter: string; TicketID: Integer; var RetClientID: Integer): Boolean;
begin
  Result := False;
  with TAddLocateForm.Create(nil) do
    try
      ShowLocatorList := False;
      SetCallCenter(CallCenter, TicketID);

      {Hide Locator dropdown}
      LocatorComboBox.Visible := False;    //EB QM-142
      lblLocator.Visible := False;
      Height := 175;


      if ShowModal = mrOK then begin
        Result := SaveRecord(TicketID);
        RetClientID := NewClientID;   //QM-282 EB
      end;
    finally
      Free;
    end;
end;

function ExecuteAddLocateWLocatorDialog(CallCenter: string; TicketID: Integer; Mgr: Integer; var RetClientID: Integer ): Boolean; //EB QM-142 Add Different Locator

begin
  Result := False;

  with TAddLocateForm.Create(nil) do
    try
      LocatorMgr := Mgr;
      ShowLocatorList := True;
      CurLocatorID := GetNewLocateEmpID(TicketID);
      LoadLocatorList(Mgr);
      SetCallCenter(CallCenter, TicketID);


      {Show Locator dropdown}
      LocatorComboBox.Visible := True;
      lblLocator.Visible := True;
      Height := 200;

      if ShowModal = mrOK then begin
        Result := SaveRecord(TicketID);
        RetClientID := NewClientID;  //QM-282 EB
      end;
    finally
      Free;
    end;
end;

procedure TAddLocateForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

procedure TAddLocateForm.LoadLocatorList(ManagerID: integer); //QM-142 Add Locator
var
  IndexOfDefaultLoc: integer;
  {Debug}
 // DCount: integer;  //QM-1042 Add Locate Issue
begin
  ClearLocatorList;
  EmployeeDM.EmployeeList(LocatorComboBox.Properties.Items, ManagerID, 1);
 // DCount := LocatorComboBox.Properties.Items.Count;  //QM-1042 Add Locate Issue
  IndexOfDefaultLoc := LocatorComboBox.Properties.Items.IndexOfObject(TObject(CurLocatorID));
  if IndexOfDefaultLoc >= 0 then
    LocatorComboBox.ItemIndex := IndexOfDefaultLoc;
end;

procedure TAddLocateForm.LocatorComboBoxClick(Sender: TObject);
begin
  EnableDisableControls;
end;

procedure TAddLocateForm.PopulateStatusList;
begin
  NewClientID := GetSelectedClient;
  StatusCombo.Clear;
  DM.GetValidStatusesForTicketAndClient(StatusCombo.Properties.Items, FTicketID, NewClientID);
  DM.ApplyEmployeeStatusLimit(StatusCombo.Properties.Items);
  StatusCombo.ItemIndex := StatusCombo.Properties.Items.IndexOf(ReadyToWorkStatus);
end;

end.

