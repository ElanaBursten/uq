object TicketAlertsFrame: TTicketAlertsFrame
  Left = 0
  Top = 0
  Width = 672
  Height = 341
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 17
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    Visible = False
  end
  object AlertGrid: TcxGrid
    Left = 0
    Top = 17
    Width = 672
    Height = 324
    Align = alClient
    TabOrder = 1
    object AlertGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = AlertGridViewCustomDrawCell
      DataController.DataSource = AlertDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsView.DataRowHeight = 18
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.RowSeparatorWidth = 2
      Preview.Column = ColAlertDesc
      Preview.MaxLineCount = 1000
      Preview.Visible = True
      Styles.Content = SharedDevExStyleData.SmallGridHighlight
      Styles.ContentEven = SharedDevExStyleData.OldTicketsEven
      Styles.ContentOdd = SharedDevExStyleData.OldTicketsOdd
      Styles.Selection = SharedDevExStyleData.SkyBlueHighlightSelection
      object ColShowColor: TcxGridDBColumn
        Caption = '*'
        Width = 20
      end
      object ColColor: TcxGridDBColumn
        Caption = 'Color'
        DataBinding.FieldName = 'modifier'
        Visible = False
        Width = 20
      end
      object ColAlertID: TcxGridDBColumn
        Caption = 'Alert ID'
        DataBinding.FieldName = 'alert_id'
        Options.Editing = False
        Width = 42
      end
      object ColAlertSource: TcxGridDBColumn
        Caption = 'Source'
        DataBinding.FieldName = 'alert_source'
        Width = 89
      end
      object ColAlertTypeCode: TcxGridDBColumn
        Caption = 'Type Code'
        DataBinding.FieldName = 'alert_type_code'
        Width = 98
      end
      object ColAlertTypeCodeDesc: TcxGridDBColumn
        Caption = 'Code Desc'
        DataBinding.FieldName = 'alert_code_desc'
        Width = 158
      end
      object ColAlertDesc: TcxGridDBColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert_desc'
        PropertiesClassName = 'TcxRichEditProperties'
        Properties.PlainText = True
        Properties.ReadOnly = True
        HeaderGlyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00020000000A0000000F00000011000000110000001200000012000000130000
          00130000001300000014000000130000000D0000000300000000000000008059
          4ABAB37C68FFB37B67FFB27B66FFB27A66FFB27864FFB17965FFB17965FFB177
          65FFB17763FFB07664FFB07664FF7E5548C50000000C0000000000000000B67F
          6BFFFDFBFAFFFBF7F4FFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2
          EEFFFAF2ECFFF8F0EBFFF9F0EAFFB17866FF000000120000000000000000B681
          6FFFFDFBFBFFC4907BFFC38E7AFFC28E79FFC28E78FFC18C78FFF6ECE5FF137C
          D4FF116BCBFF0E5FC4FFF9F1ECFFB37A68FF000000130000000000000000B984
          72FFFEFCFBFFF1E6DEFFF1E5DDFFF0E4DDFFF0E4DDFFF1E4DCFFF7EEE7FF168E
          E0FF137FD8FF1171D0FFFAF2EEFFB57D6BFF000000120000000000000000BC87
          75FFFEFDFCFFC69580FFC5947FFFC5937EFFC4907DFFC5907CFFF8EEE9FF19A1
          EAFF1794E1FF1485DAFFFAF4EFFFB7816EFF000000110000000000000000BE8B
          7AFFFEFDFCFFF3E8E2FFF2E7E1FFF2E7E1FFF2E6DFFFF2E6DEFFF8F0EAFFF8F0
          E9FFF8EFE9FFF8EFE8FFFAF5F1FFBA8571FF000000100000000000000000C190
          7FFFFEFEFDFFC99A85FFC89984FFC89883FFC79782FFC79682FFC69381FFC694
          80FFC5937EFFC5917EFFFBF6F3FFBB8975FF0000000F0000000000000000C494
          82FFFEFEFDFFF4EBE5FFF4EAE5FFF4EAE4FFF4E9E4FFF3E8E3FFF3E8E3FFF2E8
          E1FFF2E7E1FFF2E6DFFFFCF7F4FFBE8B79FF0000000F0000000000000000C697
          87FFFEFEFEFFCD9F8BFFCC9F8AFFCB9C8AFFCA9D89FFCA9B86FFC99B87FFC899
          84FFC89884FFC89783FFFDF9F7FFC18E7DFF0000000E0000000000000000C99B
          8AFFFFFEFEFFF4ECE7FFF4EBE7FFF4EBE7FFF5EBE7FFF5EBE7FFF5E9E6FFF4EA
          E5FFF4EAE4FFF3EAE4FFFDFAF8FFC39382FF0000000D0000000000000000CBA0
          8DFFFFFEFEFFCFA48FFFCEA18FFFCEA18EFFCDA08DFFCDA18CFFCDA08BFFCC9E
          8AFFCB9E8AFFCB9D89FFFDFBF9FFC69786FF0000000C0000000000000000CDA1
          91FFFFFFFEFFF5EFEAFFF6EEEAFFF6EEEAFFF5EDEAFFF5EDE9FFF5ECE8FFF4EC
          E8FFF4ECE7FFF5ECE7FFFDFCFAFFC89B8AFF0000000B0000000000000000CFA4
          93FFFFFFFFFFD1A693FFD0A792FFD0A692FFD0A592FFCFA491FFCFA490FFCFA4
          8FFFCFA28FFFCEA28DFFFEFCFCFFCB9D8DFF0000000B0000000000000000D1A7
          96FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFE
          FEFFFEFEFEFFFEFEFEFFFEFEFDFFCDA191FF0000000900000000000000009C7D
          71BED2A898FFD2A898FFD1A897FFD1A897FFD1A797FFD0A696FFD0A696FFD0A6
          95FFD0A595FFCFA595FFCFA494FF98796EC20000000600000000}
        Options.Editing = False
        Width = 185
      end
      object ColInsertDate: TcxGridDBColumn
        Caption = 'Date Added'
        DataBinding.FieldName = 'insert_date'
        Width = 121
      end
      object ColModifiedDate: TcxGridDBColumn
        Caption = 'Date Modified'
        DataBinding.FieldName = 'modified_date'
        Width = 119
      end
      object ColSortBy: TcxGridDBColumn
        Caption = 'Sort By'
        DataBinding.FieldName = 'sort_by'
        Visible = False
        Width = 20
      end
    end
    object ALertGridView_B: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = AlertDS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.RowSeparatorWidth = 3
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 0
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'ALERTS'
        end>
      object ColBAlertID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'alert_id'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColBAlertSource: TcxGridDBBandedColumn
        Caption = 'Source'
        DataBinding.FieldName = 'alert_source'
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ColBAlertTypeCode: TcxGridDBBandedColumn
        Caption = 'Type Code'
        DataBinding.FieldName = 'alert_type_code'
        Width = 75
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ColBAlertTypeCodeDesc: TcxGridDBBandedColumn
        Caption = 'Code Desc'
        DataBinding.FieldName = 'alert_code_desc'
        Width = 135
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ColBAlertDesc: TcxGridDBBandedColumn
        Caption = 'Desc'
        DataBinding.FieldName = 'alert_desc'
        Options.Editing = False
        Width = 512
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ColBInsertDate: TcxGridDBBandedColumn
        Caption = 'Added On'
        DataBinding.FieldName = 'insert_date'
        Width = 125
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ColBModifiedDate: TcxGridDBBandedColumn
        Caption = 'Modified on'
        DataBinding.FieldName = 'modified_date'
        Width = 125
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object ColBSortBy: TcxGridDBBandedColumn
        Caption = 'Sort By'
        DataBinding.FieldName = 'sort_by'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object ColBColor: TcxGridDBBandedColumn
        Caption = 'Color'
        DataBinding.FieldName = 'modifier'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
    end
    object AlertGridLevel1: TcxGridLevel
      GridView = AlertGridView
    end
  end
  object AlertDS: TDataSource
    DataSet = DM.TicketAlertsQuery
    Left = 552
    Top = 8
  end
end
