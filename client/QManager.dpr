
{$R '..\QMIcon.res'}
{$R QManager.TLB}
program QManager;

{$R '..\QMVersion.res' '..\QMVersion.rc'}
{$R 'QMResources.res' 'QMResources.rc'}

// Compile with the following packages for end-user distribution
// rtl;vcl;vclx;dbrtl;vcldb;vclie;dbisamr;DevExD2007RT1224;
// Development search path:
// ..\thirdparty\zlib;..\common;..\thirdparty;..\thirdparty\ExtListView;..\server;..\reportengine;..\thirdparty\BetterADO;..\thirdparty\FastMM;..\thirdparty\PerlRegEx;..\thirdparty\GPSTools

uses
  Windows,
  Forms,
  Dialogs,
  SysUtils,
  MainFU in 'MainFU.pas' {MainForm},
  DMu in 'DMu.pas' {DM: TDataModule},
  LoginFU in 'LoginFU.pas' {LoginForm},
  OdSecureHash in '..\common\OdSecureHash.pas',
  OdBrowserUtils in '..\common\OdBrowserUtils.pas',
  ReminderDMu in 'ReminderDMu.pas' {ReminderDM: TDataModule},
  OdMiscUtils in '..\common\OdMiscUtils.pas',
  OdEmbeddable in '..\common\OdEmbeddable.pas' {EmbeddableForm},
  TicketDetail in 'TicketDetail.pas' {TicketDetailForm},
  OldTickets in 'OldTickets.pas' {OldTicketsForm},
  Report in 'Report.pas' {ReportForm},
  Config in 'Config.pas' {ConfigForm},
  OdIsoDates in '..\common\OdIsoDates.pas',
  OdWinInet in '..\common\OdWinInet.pas',
  SyncEngineU in '..\common\SyncEngineU.pas',
  AddLocate in 'AddLocate.pas' {AddLocateForm},
  WorkManagement in 'WorkManagement.pas' {WorkManagementForm},
  OdVclUtils in '..\common\OdVclUtils.pas',
  OdHourglass in '..\common\OdHourglass.pas',
  StateMgr in 'StateMgr.pas',
  Sync in 'Sync.pas' {SyncForm},
  About in 'About.pas' {AboutForm},
  UQUtils in 'UQUtils.pas',
  WorkManageUtils in 'WorkManageUtils.pas',
  OdExceptions in '..\common\OdExceptions.pas',
  OdNetUtils in '..\common\OdNetUtils.pas',
  DamageList in 'DamageList.pas' {DamageListForm},
  OdWebUpdate in '..\common\OdWebUpdate.pas',
  OdWebUpdateShared in '..\common\OdWebUpdateShared.pas',
  QMConst in 'QMConst.pas',
  LocateStatusActivityReport in 'LocateStatusActivityReport.pas' {LocateStatusActivityReportForm},
  OdReportBase in '..\common\OdReportBase.pas' {ReportBaseForm},
  SyncReport in 'SyncReport.pas' {SyncReportForm},
  OdLog in '..\common\OdLog.pas' {ExceptionDialog},
  LoadReport in 'LoadReport.pas' {LoadReportForm},
  OdHttpDialog in '..\common\OdHttpDialog.pas' {OdNetProgressDialog},
  AuditReport in 'AuditReport.pas' {AuditReportForm},
  BaseSearchForm in 'BaseSearchForm.pas' {SearchForm},
  SearchHeader in 'SearchHeader.pas' {SearchCriteria},
  TicketSearchHeader in 'TicketSearchHeader.pas' {TicketSearchCriteria},
  EmployeeSearchHeader in 'EmployeeSearchHeader.pas' {EmployeeSearchCriteria},
  DamageInvoiceForm in 'DamageInvoiceForm.pas' {DamageInvoiceForm},
  DailyClientClosingReport in 'DailyClientClosingReport.pas' {DailyClientLocatesReportForm},
  LateTicketReport in 'LateTicketReport.pas' {LateTicketReportForm},
  TicketsDueReport in 'TicketsDueReport.pas' {TicketsDueReportForm},
  DamageSearchHeader in 'DamageSearchHeader.pas' {DamageSearchCriteria},
  OdDbUtils in '..\common\OdDbUtils.pas',
  NoConflictReport in 'NoConflictReport.pas' {NoConflictReportForm},
  OdDBISAMXml in '..\common\OdDBISAMXml.pas',
  OdMSXMLUtils in '..\common\OdMSXMLUtils.pas',
  HighProfileReport in 'HighProfileReport.pas' {HighProfileReportForm},
  OdRangeSelect in '..\common\OdRangeSelect.pas' {OdRangeSelectFrame: TFrame},
  ProductivityReport in 'ProductivityReport.pas' {ProductivityReportForm},
  OdTimeRangeSelect in '..\common\OdTimeRangeSelect.pas' {OdTimeRangeSelectFrame: TFrame},
  ClosedTicketDetailReport in 'ClosedTicketDetailReport.pas' {ClosedTicketDetailReportForm},
  AfterHoursReport in 'AfterHoursReport.pas' {AfterHoursReportForm},
  InitialStatusReport in 'InitialStatusReport.pas' {InitialStatusReportForm},
  OdGetNewPassword in '..\common\OdGetNewPassword.pas' {GetNewPasswordForm},
  DamageInvoiceSearchHeader in 'DamageInvoiceSearchHeader.pas' {DamageInvoiceSearchCriteria},
  NewTicketDetailReport in 'NewTicketDetailReport.pas' {NewTicketReportForm},
  LoadSheetReport in 'LoadSheetReport.pas' {LoadSheetReportForm},
  TicketsReceivedReport in 'TicketsReceivedReport.pas' {TicketsReceivedReportForm},
  LocateHours in 'LocateHours.pas' {LocateWorkForm},
  TimesheetReport in 'TimesheetReport.pas' {TimesheetReportForm},
  LocalDataDef in 'LocalDataDef.pas',
  OdXmlToDataSet in '..\common\OdXmlToDataSet.pas',
  DamagesByLocatorReport in 'DamagesByLocatorReport.pas' {DamagesByLocatorReportForm},
  ServerProxy in 'ServerProxy.pas',
  QMServerLibrary_Intf in '..\server\QMServerLibrary_Intf.pas',
  OdHttpROChannel in '..\common\OdHttpROChannel.pas',
  OdDataSetToXml in '..\common\OdDataSetToXml.pas',
  DamageDetailsReport in 'DamageDetailsReport.pas' {DamageDetailsReportForm},
  ServiceLink in 'ServiceLink.pas',
  DamageThirdPartySearchHeader in 'DamageThirdPartySearchHeader.pas' {DamageThirdPartySearchCriteria},
  AckDamage in 'AckDamage.pas' {AckDamageDialog},
  TimesheetExceptionReport in 'TimesheetExceptionReport.pas' {TimesheetExceptionReportForm},
  HoursCalc in '..\common\HoursCalc.pas',
  DamageLiabilityChangesReport in 'DamageLiabilityChangesReport.pas' {DamageLiabilityChangesReportForm},
  OnTimeCompletionReport in 'OnTimeCompletionReport.pas' {OnTimeCompletionReportForm},
  OldOpenTicketsReport in 'OldOpenTicketsReport.pas' {OldOpenTicketsReportForm},
  NoResponseTicketsReport in 'NoResponseTicketsReport.pas' {NoResponseTicketsReportForm},
  FaxStatusReport in 'FaxStatusReport.pas' {FaxStatusReportForm},
  EmailStatusReport in 'EmailStatusReport.pas' {EmailStatusReportForm},
  EmailedHPTicketsReport in 'EmailedHPTicketsReport.pas' {EmailedHPTicketsReportForm},
  MyAssets in 'MyAssets.pas' {MyAssetsForm},
  AssetManagement in 'AssetManagement.pas' {AssetManagementForm},
  AssetReport in 'AssetReport.pas' {AssetReportForm},
  Attachments in 'Attachments.pas' {AttachmentsFrame: TFrame},
  OdWmi in '..\common\OdWmi.pas',
  Timesheet in 'Timesheet.pas' {TimesheetForm},
  TimesheetDay in 'TimesheetDay.pas' {TimesheetDayFrame: TFrame},
  TimesheetApproval in 'TimesheetApproval.pas' {TimesheetApprovalForm},
  TimesheetAuditReport in 'TimesheetAuditReport.pas' {TimesheetAuditReportForm},
  PersonSelController in 'PersonSelController.pas',
  DamageListReport in 'DamageListReport.pas' {DamageListReportForm},
  Invoice in 'Invoice.pas' {InvoiceFrame: TFrame},
  HoursDataset in '..\common\HoursDataset.pas',
  TimesheetRuleCheckReport in 'TimesheetRuleCheckReport.pas' {TimesheetRuleCheckReportForm},
  TimesheetSummaryReport in 'TimesheetSummaryReport.pas' {TimesheetSummaryReportForm},
  ProductivityClientReport in 'ProductivityClientReport.pas' {ProductivityClientReportForm},
  OdFtp in '..\common\OdFtp.pas',
  NewTicket in 'NewTicket.pas' {NewTicketForm},
  OdIntPacking in '..\common\OdIntPacking.pas',
  BlastingTicketsReport in 'BlastingTicketsReport.pas' {BlastingTicketsReportForm},
  PayrollSummaryReport in 'PayrollSummaryReport.pas' {PayrollSummaryReportForm},
  TimesheetExportsReport in 'TimesheetExportsReport.pas' {TimesheetExportsReportForm},
  LocalSearch in 'LocalSearch.pas' {LocalSearchForm},
  EmployeeSelectFrame in 'EmployeeSelectFrame.pas' {EmployeeSelect: TFrame},
  EmployeeSearch in 'EmployeeSearch.pas' {EmployeeSearchForm},
  StandardPayrollExceptionReport in 'StandardPayrollExceptionReport.pas' {StandardPayrollExceptionReportForm},
  AdHocPayrollExceptionReport in 'AdHocPayrollExceptionReport.pas' {AdHocPayrollExceptionReportForm},
  StatusGroupReport in 'StatusGroupReport.pas' {StatusGroupReportForm},
  PayrollExport in 'PayrollExport.pas' {PayrollExportForm},
  WbemScripting_TLB in '..\common\WbemScripting_TLB.pas',
  OdDBISAMUtils in '..\common\OdDBISAMUtils.pas',
  OdSQLBuilder in '..\common\OdSQLBuilder.pas',
  OdComboBoxController in '..\common\OdComboBoxController.pas',
  BillingAdjustmentSearchHeader in 'BillingAdjustmentSearchHeader.pas' {BillingAdjustmentCriteria},
  BillingMain in 'BillingMain.pas' {BillingMainForm},
  DamageEstimate in 'DamageEstimate.pas' {EstimateFrame: TFrame},
  DamageLitigationSearchHeader in 'DamageLitigationSearchHeader.pas' {DamageLitigationSearchCriteria},
  InvoiceIDExceptionReport in 'InvoiceIDExceptionReport.pas' {InvoiceIDExceptionReportForm},
  EmployeeReport in 'EmployeeReport.pas' {EmployeeReportForm},
  DamageInvoiceFrame in 'DamageInvoiceFrame.pas' {DamageInvoiceFrame: TFrame},
  DamageDetails in 'DamageDetails.pas' {DamageDetailsForm},
  SharedImages in 'SharedImages.pas' {SharedImagesDM: TDataModule},
  BillingAdjustmentDetails in 'BillingAdjustmentDetails.pas' {BillingAdjustmentDetailsForm},
  BillingReviewSearchHeader in 'BillingReviewSearchHeader.pas' {BillingReviewSearchCriteria},
  LocatePlats in 'LocatePlats.pas' {LocatePlatForm},
  DailySalesReport in 'DailySalesReport.pas' {DailySalesReportForm},
  BillingExceptionReport in 'BillingExceptionReport.pas' {BillingExceptionReportForm},
  MSXML2_TLB in '..\common\MSXML2_TLB.pas',
  DamageIDExceptionReport in 'DamageIDExceptionReport.pas' {DamageIDExceptionReportForm},
  PasswordRules in '..\common\PasswordRules.pas',
  DamageInvoiceReport in 'DamageInvoiceReport.pas' {DamageInvoiceReportForm},
  MessageAck in 'MessageAck.pas' {MessageAckForm},
  MessageMain in 'MessageMain.pas' {MessageMainForm},
  MessageSend in 'MessageSend.pas' {MessageSendForm},
  MessageSearchHeader in 'MessageSearchHeader.pas' {MessageSearchCriteria},
  MessageDetail in 'MessageDetail.pas' {MessageDetailForm},
  Terminology in '..\common\Terminology.pas',
  PhotoAttachment in 'PhotoAttachment.pas' {PhotoAttachmentFrame: TFrame},
  OdSelectImage in '..\common\OdSelectImage.pas',
  ContactName in 'ContactName.pas' {ContactNameDialog},
  DamageNotes in 'DamageNotes.pas' {NotesFrame: TFrame},
  BillableDamagesReport in 'BillableDamagesReport.pas' {BillableDamagesReportForm},
  DamageDefaultEstimateReport in 'DamageDefaultEstimateReport.pas' {DamageDefaultEstimateReportForm},
  DamageProductivityReport in 'DamageProductivityReport.pas' {DamageProductivityReportForm},
  DamagesPerLocatesReport in 'DamagesPerLocatesReport.pas' {DamagesPerLocatesReportForm},
  WinSessionNotify in '..\common\WinSessionNotify.pas',
  OdUqInternet in '..\common\OdUqInternet.pas',
  BreakRules in '..\common\BreakRules.pas',
  EmployeeActivityReport in 'EmployeeActivityReport.pas' {EmployeeActivityReportForm},
  OdAckMessageDlg in '..\common\OdAckMessageDlg.pas' {AckMessageDialog},
  OdUtcDates in '..\common\OdUtcDates.pas',
  OdStillImage in '..\common\OdStillImage.pas',
  OdTimerDialog in '..\common\OdTimerDialog.pas' {TimerDialogForm},
  OdAutoSyncTimerDialog in '..\common\OdAutoSyncTimerDialog.pas' {AutoSyncTimerDialogForm},
  OdWiaManager in '..\common\OdWiaManager.pas',
  WIALib_TLB in '..\common\WIALib_TLB.pas',
  OdTimeKeeper in '..\common\OdTimeKeeper.pas',
  RestrictedUseNotice in 'RestrictedUseNotice.pas' {RestrictedUseForm},
  RestrictedUseExemptionReport in 'RestrictedUseExemptionReport.pas' {RestrictedUseExemptionReportForm},
  TimesheetEmpSummaryReport in 'TimesheetEmpSummaryReport.pas' {TimesheetEmpSummaryReportForm},
  STI in '..\common\STI.pas',
  ResponseLogReport in 'ResponseLogReport.pas' {ResponseLogReportForm},
  TicketsReceivedSummaryReport in 'TicketsReceivedSummaryReport.pas' {TicketsReceivedSummaryReportForm},
  TimesheetSummaryPCReport in 'TimesheetSummaryPCReport.pas' {TimesheetSummaryPCReportForm},
  OdDeviceNotification in '..\common\OdDeviceNotification.pas',
  HighLevelProductivityReport in 'HighLevelProductivityReport.pas' {HighLevelProductivityReportForm},
  AreaEditor in 'AreaEditor.pas' {AreaEditorForm},
  AttachmentsAddin in 'AttachmentsAddin.pas',
  LocateLengthDetail in 'LocateLengthDetail.pas' {LocateLengthEditor},
  LengthConverter in '..\common\LengthConverter.pas',
  EmployeeCovStatusReport in 'EmployeeCovStatusReport.pas' {EmployeeCovStatusReportForm},
  LocatesCompletedSummaryReport in 'LocatesCompletedSummaryReport.pas' {LocatesCompletedSummaryReportForm},
  EmployeesInTrainingReport in 'EmployeesInTrainingReport.pas' {EmployeesInTrainingReportForm},
  AckAllTicketActivity in 'AckAllTicketActivity.pas' {AckAllTicketActivityForm},
  BaseAttachmentDMu in '..\common\BaseAttachmentDMu.pas' {BaseAttachment: TDataModule},
  LocalAttachmentDMu in 'LocalAttachmentDMu.pas' {LocalAttachment: TBaseAttachment},
  TicketImage in '..\common\TicketImage.pas',
  BreakExceptionReport in 'BreakExceptionReport.pas' {BreakExceptionReportForm},
  TicketActivityReport in 'TicketActivityReport.pas' {TicketActivityReportForm},
  OdAckMessagePromptDlg in '..\common\OdAckMessagePromptDlg.pas' {AckMessagePromptDialog},
  StartTimeDialog in 'StartTimeDialog.pas' {TaskTimeDialog},
  EmployeeRightsReport in 'EmployeeRightsReport.pas' {EmployeeRightsReportForm},
  ExportLocalData in 'ExportLocalData.pas',
  Progress in 'Progress.pas' {ProgressFrame: TFrame},
  UploadThread in 'UploadThread.pas',
  AttachmentUploadStatistics in 'AttachmentUploadStatistics.pas',
  OdIdleTimer in '..\common\OdIdleTimer.pas',
  BaseAddinManager in 'BaseAddinManager.pas',
  PlatAddin in 'PlatAddin.pas',
  ApplicationFiles in 'ApplicationFiles.pas',
  TicketWorkView in 'TicketWorkView.pas' {TicketWorkFrame: TFrame},
  FormatCriticalDisplayText in '..\common\FormatCriticalDisplayText.pas',
  TicketHighlighter in '..\common\TicketHighlighter.pas',
  BaseTimesheetDay in 'BaseTimesheetDay.pas' {BaseTimesheetDayFrame: TFrame},
  TimeClockDB in 'TimeClockDB.pas',
  TimeClockDay in 'TimeClockDay.pas' {TimeClockDayFrame: TFrame},
  TimeClockEntry in 'TimeClockEntry.pas',
  TimeClockImport in 'TimeClockImport.pas',
  TimeClockDayApproval in 'TimeClockDayApproval.pas' {TimeClockDayApprovalFrame: TFrame},
  UQGPS in '..\common\UQGPS.pas',
  GPSThread in 'GPSThread.pas',
  GpsToolsXP_TLB in '..\thirdparty\GPSTools\GpsToolsXP_TLB.pas',
  OdCxXmlUtils in 'OdCxXmlUtils.pas',
  DamageProfitCenterRules in '..\common\DamageProfitCenterRules.pas',
  PriorityRulesBase in '..\common\PriorityRulesBase.pas',
  BackgroundWorker in 'BackgroundWorker.pas',
  SharedDevExStyles in 'SharedDevExStyles.pas' {SharedDevExStyleData: TDataModule},
  WorkOrderDetail in 'WorkOrderDetail.pas' {WorkOrderDetailForm},
  WorkOrderImage in '..\common\WorkOrderImage.pas',
  BaseNotes in 'BaseNotes.pas' {BaseNotesFrame: TFrame},
  WorkOrderSearchHeader in 'WorkOrderSearchHeader.pas',
  WorkOrderDetailReport in 'WorkOrderDetailReport.pas' {WorkOrderDetailReportForm},
  WorkOrderStatusActivityReport in 'WorkOrderStatusActivityReport.pas' {WorkOrderStatusActivityReportForm},
  MyWork in 'MyWork.pas' {MyWorkForm},
  BaseExpandableDataFrame in 'BaseExpandableDataFrame.pas' {ExpandableDataFrameBase: TFrame},
  MyWorkWOs in 'MyWorkWOs.pas' {MyWorkWOsFrame: TFrame},
  MyWorkTickets in 'MyWorkTickets.pas' {MyWorkTicketsFrame: TFrame},
  MyWorkDamages in 'MyWorkDamages.pas' {MyWorkDamagesFrame: TFrame},
  BaseExpandableGridFrame in 'BaseExpandableGridFrame.pas' {ExpandableGridFrameBase: TFrame},
  DMHttpServer in 'DMHttpServer.pas' {DMHttp: TDataModule},
  TimeFileMonitor in 'TimeFileMonitor.pas' {TimeFileMonitorDM: TDataModule},
  QMTempFolder in '..\common\QMTempFolder.pas',
  OdCxUtils in '..\common\OdCxUtils.pas',
  OdDBISAMSQLBuilder in '..\common\OdDBISAMSQLBuilder.pas',
  BaseExpandableWorkMgtGridFrame in 'BaseExpandableWorkMgtGridFrame.pas' {ExpandableWorkMgtGridFrameBase: TFrame},
  WorkManagementDamages in 'WorkManagementDamages.pas' {WorkMgtDamagesFrame: TFrame},
  WorkManagementTickets in 'WorkManagementTickets.pas' {WorkMgtTicketsFrame: TFrame},
  WorkManagementWOs in 'WorkManagementWOs.pas' {WorkMgtWOsFrame: TFrame},
  WorkOrderDetailBase in 'WorkOrderDetailBase.pas' {WODetailBaseFrame: TFrame},
  WorkOrderDetailLAM01 in 'WorkOrderDetailLAM01.pas' {WOLAM01Frame: TFrame},
  WorkOrderDetailWGL in 'WorkOrderDetailWGL.pas' {WOWGLFrame: TFrame},
  PotentialHazard in 'PotentialHazard.pas' {PotentialHazardDialog},
  SuperObjectUtils in '..\common\SuperObjectUtils.pas',
  APIServerIntf in 'APIServerIntf.pas',
  QMTransport in 'QMTransport.pas',
  ResponderStatusTranslationsReport in 'ResponderStatusTranslationsReport.pas' {ResponderStatusTranslationsReportForm},
  LocateStatusQuestions in 'LocateStatusQuestions.pas' {LocStatusQuestions},
  StatusFrame in 'StatusFrame.pas' {StatFrame: TFrame},
  TimesclockOverride in 'TimesclockOverride.pas' {frmTimeSheetOverride},
  streetMap in 'streetMap.pas' {frmStreetMap},
  WorkOrderDetailOHM in 'WorkOrderDetailOHM.pas' {WOOHMFrame: TFrame},
  WorkOrderDetailINR in 'WorkOrderDetailINR.pas' {WOINRFrame},
  QManager_TLB in 'QManager_TLB.pas',
  xqGeoFunctions in '..\common\xqGeoFunctions.pas',
  uDLLIntf in '..\SharedSecureAttachments\uDLLIntf.pas',
  WazeQRCode in '..\common\WazeQRCode.pas',
  MyWorkWaze in 'MyWorkWaze.pas' {frmWaze},
  uSecure in '..\common\uSecure.pas',
  BillPeriod in '..\XE10Billing\BillPeriod.pas',
  AddProjectLocate in 'AddProjectLocate.pas' {AddProjectLocateForm},
  ClientInfo in 'ClientInfo.pas' {frmClientInfo},
  LocalPermissionsDMu in 'LocalPermissionsDMu.pas' {PermissionsDM: TDataModule},
  LocalEmployeeDMu in 'LocalEmployeeDMu.pas' {EmployeeDM: TDataModule},
  LocalDamageDMu in 'LocalDamageDMu.pas' {LDamageDM: TDataModule},
  TicketsDueExport in 'TicketsDueExport.pas' {TicketsDueExportForm},
  TextMessageBox in 'TextMessageBox.pas' {frmTextMessageBox},
  xqFadeBox in '..\common\xqFadeBox.pas',
  eMailBodyViewer in 'eMailBodyViewer.pas' {frmEmailBody},
  PSUtils in '..\common\PSUtils.pas',
  PJConsoleApp in '..\thirdparty\DabblerConsoleApp\PJConsoleApp.pas',
  PJFileHandle in '..\thirdparty\DabblerConsoleApp\PJFileHandle.pas',
  PJPipe in '..\thirdparty\DabblerConsoleApp\PJPipe.pas',
  PJPipeFilters in '..\thirdparty\DabblerConsoleApp\PJPipeFilters.pas',
  PlatMgmtUtils in '..\common\PlatMgmtUtils.pas',
  PhoneVerifyDialog in 'PhoneVerifyDialog.pas' {OKRightDlg},
  FirstTaskSetDialog in 'FirstTaskSetDialog.pas' {FirstTaskReminderForm},
  DigAlertNote in 'DigAlertNote.pas' {frmDigAlertNoteDialog},
  PlusMgrList in 'PlusMgrList.pas' {PlusMgrListForm},
  TimeSheetCalloutCompletion in 'TimeSheetCalloutCompletion.pas' {CalloutCompletionDlg},
  Acustomform in '..\common\Acustomform.pas' {ACustForm},
  ACustomFormConst in '..\common\ACustomFormConst.pas',
  AcustomFormDMu in '..\common\AcustomFormDMu.pas' {AcustomFormDM: TDataModule},
  Acustomformframe in '..\common\Acustomformframe.pas' {CustomFormFrame: TFrame},
  MARSBulkStatus in 'MARSBulkStatus.pas' {frmMARSBulkStatus},
  RouteOrderOverride in 'RouteOrderOverride.pas' {RouteOrderOVerrideFrm},
  TicketAlertsFrameU in 'TicketAlertsFrameU.pas' {TicketAlertsFrame: TFrame},
  BulkDueDateDlg in 'BulkDueDateDlg.pas' {frmBulkDueDateEdit},
  OKCANCL1 in 'c:\program files (x86)\codegear\rad studio\5.0\ObjRepos\DelphiWin32\OKCANCL1.PAS' {OKBottomDlg},
  SimpleLinkDlg in 'SimpleLinkDlg.pas' {DlgSimpleLink};

//QM-147

var
  State: TUQState;
  MutexError: Integer;
  IniFile, AppDirLogFile, ProperLogFile: string;
begin
  {$IF CompilerVersion < 22} // prior to Delphi XE
  { The new dialogs may cause QManager to crash, when running on Windows 7. This
    may have been fixed in Delphi, after version 2007. See JIRA #3311. }
  Dialogs.UseLatestCommonDialogs := False;
  {$IFEND}

  IniFile := ChangeFileExt(ParamStr(0), '.ini');
  InitializeHandler(DefaultFileName, IniFile, DMu.AppName);
  AppDirLogFile := ChangeFileExt(ParamStr(0), '.log');
  ProperLogFile := OdLog.GetLog.FileName;
  if FileExists(AppDirLogFile) and (AppDirLogFile <> ProperLogFile) then
    if not MoveFile(PChar(AppDirLogFile), PChar(ProperLogFile)) then
      ShowMessageFmt('The log file location has changed. The new log file location is %s. The old log ' +
                     'file (%s) can be safely deleted.', [ProperLogFile, AppDirLogFile]);
  State := TUQState.CreateFromIniFile(IniFile);
  ConfigureDataAndExecuteApp(State); // This can terminate this process

  if CheckCmdLineParameter('/camera') then
    if RegisterLaunchApplication(AppName, Application.ExeName) = 0 then begin
      State.RegisteredForCamera := True;
      ShowMessage('Q Manager camera interface is now enabled.');
      State.SaveToIni;
      Application.Terminate;
      Exit;
    end;

  if CheckCmdLineParameter('/nocamera') then
    if UnregisterLaunchApplication(AppName) = 0 then begin
      State.RegisteredForCamera := False;
      ShowMessage('Q Manager camera interface is now disabled.');
      State.SaveToIni;
      Application.Terminate;
      Exit;
    end;

  // The installer checks for this mutex to see if the app is running (no need to delete it)
  CreateMutex(nil, False, QMApplicationMutex);
  MutexError := GetLastError;

  if (MutexError = ERROR_ALREADY_EXISTS) and LaunchedBySTI then begin
    if State.RegisteredForCamera then
      SendMessageToApplication('TMainForm', AppName, WM_STI_LAUNCH);
    Application.Terminate;
    Exit;
  end;

  if ((MutexError = ERROR_ALREADY_EXISTS) and (not State.AllowMultipleInstances)) then
  begin
    MessageDlg('You may only start one instance of ' +AppName+ ' at once.' +sLineBreak+
               'Please activate the existing program instead of starting a second copy.', mtError, [mbOK], 0);
    Application.Terminate;
    Exit;
  end;

  if LaunchedBySTI then begin
    if State.RegisteredForCamera then
      MessageDlg(AppName + ' can transfer photos from your camera and attach them to a ticket. Please start '+ AppName +
        ' and select a ticket before connecting your camera.', mtError, [mbOK], 0);
    Application.Terminate;
    Exit;
  end;

  Application.Initialize;
  if False then // Stop Delphi from adding forms here automatically when created
    Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TPermissionsDM, PermissionsDM);
  Application.CreateForm(TEmployeeDM, EmployeeDM);
  Application.CreateForm(TLDamageDM, LDamageDM);
  Application.CreateForm(TAcustomFormDM, AcustomFormDM);
  DM.UQState := State;
  DM.SetupQMService;
  DM.Engine.AppVersion := AppVersion;
  GetChan.MiscAppend := AppVersion;
  DM.Engine.Server.DeveloperMode := DM.UQState.DeveloperMode;
  AppTerminology.Initialize(Terminology.TermSetFromState);

  with Application do
    Title := AppName;
  if LoginFU.LogIn then begin
    InitializeWIA;
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
    State.SaveToIni;
    FinalizeWIA;
    UnInitializeHandler;
  end;
  FreeAndNil(State);
end.


