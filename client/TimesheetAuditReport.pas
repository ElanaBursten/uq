unit TimesheetAuditReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect, EmployeeSelectFrame,
  ActnList;

type
  TTimesheetAuditReportForm = class(TReportBaseForm)
    WorkDateRange: TOdRangeSelectFrame;
    EntryDateRange: TOdRangeSelectFrame;
    ChangeTypeGroupBox: TRadioGroup;
    IncludeChangesGroup: TRadioGroup;
    IncludeSheetsForGroup: TRadioGroup;
    WorkDateRangeLabel: TLabel;
    DateLabel: TLabel;
    WorkersUnderManagerBox: TComboBox;
    Label2: TLabel;
    ChangesPersonBox: TEmployeeSelect;
    SpecificWorkerBox: TEmployeeSelect;
    IncludeMgrAlterTimeResponse: TCheckBox;
    procedure UpdateVisibility(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, EmployeeSearch, QMConst;

{$R *.dfm}

{ TTimesheetAuditReportForm }

procedure TTimesheetAuditReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(WorkersUnderManagerBox);
  WorkDateRange.SelectDateRange(rsThisWeek);
  EntryDateRange.SelectDateRange(rsThisWeek);

  ChangesPersonBox.Initialize(esAll);
  SpecificWorkerBox.Initialize(esAll);
  AddEmployeeSearchButtonForCombo(WorkersUnderManagerBox, esReportManangers);

  UpdateVisibility(Self);
end;

procedure TTimesheetAuditReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimesheetAudit');
  SetParamDate('WorkStart', WorkDateRange.FromDate, True);
  SetParamDate('WorkEnd', WorkDateRange.ToDate, True);
  SetParamDate('EntryStart', EntryDateRange.FromDate, True);
  SetParamDate('EntryEnd', EntryDateRange.ToDate, True);

  SetParamInt('ChangedBy', IncludeChangesGroup.ItemIndex);
  if IncludeChangesGroup.ItemIndex = 3 then begin
    SetParamIntCombo('ChangedByID', ChangesPersonBox.EmployeeCombo);
  end;

  SetParamInt('SheetsFor', IncludeSheetsForGroup.ItemIndex);
  if IncludeSheetsForGroup.ItemIndex = 1 then begin
    SetParamIntCombo('SheetsForID', SpecificWorkerBox.EmployeeCombo);
  end;
  if IncludeSheetsForGroup.ItemIndex = 2 then begin
    RequireComboBox(WorkersUnderManagerBox, 'Please select a manager to include sheets for');
    SetParamIntCombo('SheetsForID', WorkersUnderManagerBox);
  end;

  SetParamInt('ChangeType', ChangeTypeGroupBox.ItemIndex);
  SetParamBoolean('IncludeMgrAlterTimeResponse', IncludeMgrAlterTimeResponse.Checked);
end;

procedure TTimesheetAuditReportForm.UpdateVisibility(Sender: TObject);
begin
  ChangesPersonBox.Enabled := (IncludeChangesGroup.ItemIndex = 3);
  SpecificWorkerBox.Enabled := (IncludeSheetsForGroup.ItemIndex = 1);
  WorkersUnderManagerBox.Enabled := (IncludeSheetsForGroup.ItemIndex = 2);
  IncludeMgrAlterTimeResponse.Enabled := IncludeChangesGroup.ItemIndex <> 1;
  if not IncludeMgrAlterTimeResponse.Enabled then
    IncludeMgrAlterTimeResponse.Checked := False;
end;

end.


