inherited SyncForm: TSyncForm
  Left = 270
  Top = 190
  Caption = 'Synchronization'
  ClientHeight = 441
  ClientWidth = 737
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 0
    Top = 251
    Width = 737
    Height = 8
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    Beveled = True
  end
  object AttachmentsPanel: TPanel
    Left = 0
    Top = 259
    Width = 737
    Height = 182
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object AttachmentsActionPanel: TPanel
      Left = 0
      Top = 0
      Width = 737
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LocationLabel: TLabel
        Left = 137
        Top = 14
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Location:'
      end
      object TimeLabel: TLabel
        Left = 360
        Top = 7
        Width = 229
        Height = 29
        AutoSize = False
        Caption = 
          'Estimated upload time is 5 minutes 8 seconds when using this typ' +
          'e of network:'
        WordWrap = True
      end
      object UploadButton: TButton
        Left = 8
        Top = 8
        Width = 121
        Height = 25
        Caption = '&Upload Attachments'
        TabOrder = 0
        OnClick = UploadButtonClick
      end
      object LocationCombo: TComboBox
        Left = 188
        Top = 10
        Width = 153
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
      end
      object NetworkCombo: TComboBox
        Left = 592
        Top = 10
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = NetworkComboChange
      end
    end
    inline UploadProgressFrame: TProgressFrame
      Left = 211
      Top = 63
      Width = 231
      Height = 81
      TabOrder = 1
      Visible = False
    end
    object AttachmentGrid: TcxGrid
      Left = 0
      Top = 42
      Width = 737
      Height = 140
      Align = alClient
      TabOrder = 2
      OnExit = AttachmentGridExit
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object AttachmentView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = AttachmentsSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'attachment_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object ColAttachmentID: TcxGridDBColumn
          DataBinding.FieldName = 'attachment_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 24
        end
        object ColFileName: TcxGridDBColumn
          Caption = 'File Name'
          DataBinding.FieldName = 'filename'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 130
        end
        object ColOrigFileName: TcxGridDBColumn
          Caption = 'File Name'
          DataBinding.FieldName = 'orig_filename'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soDescending
          Width = 143
        end
        object ColSize: TcxGridDBColumn
          Caption = 'Size (KB)'
          DataBinding.FieldName = 'size'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taRightJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          OnGetDisplayText = ColSizeGetDisplayText
          HeaderAlignmentHorz = taRightJustify
          Options.Editing = False
          Options.Filtering = False
          Width = 70
        end
        object ColAttachDate: TcxGridDBColumn
          Caption = 'Attach Date'
          DataBinding.FieldName = 'attach_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Options.Editing = False
          Options.Filtering = False
          Width = 120
        end
        object ColUploadDate: TcxGridDBColumn
          Caption = 'Upload Date'
          DataBinding.FieldName = 'upload_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Options.Editing = False
          Options.Filtering = False
          Width = 120
        end
        object ColComment: TcxGridDBColumn
          Caption = 'Comment'
          DataBinding.FieldName = 'comment'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 252
        end
      end
      object AttachmentLevel: TcxGridLevel
        GridView = AttachmentView
      end
    end
  end
  object SyncPanel: TPanel
    Left = 0
    Top = 0
    Width = 737
    Height = 251
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object HeaderPanel: TPanel
      Left = 0
      Top = 0
      Width = 737
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Synchronization Log:'
      TabOrder = 0
      object LunchLockoutTimeLabel: TLabel
        Left = 555
        Top = 5
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '00:00:00'
        Visible = False
      end
      object OverrideLunchLockButton: TButton
        Left = 607
        Top = 0
        Width = 137
        Height = 25
        Caption = '&Emergency Ticket'
        TabOrder = 0
        Visible = False
        OnClick = OverrideLunchLockButtonClick
      end
    end
    object SyncLog: TMemo
      Left = 0
      Top = 25
      Width = 737
      Height = 226
      Align = alClient
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
  object AttachmentsSource: TDataSource
    DataSet = Attachments
    Left = 80
    Top = 340
  end
  object Attachments: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * from attachment'
      'where active'
      '  and upload_date is null'
      '  and attached_by = :EmpID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
    Left = 136
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
  end
  object UploadSizeTotal: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select sum(size) from attachment'
      'where active'
      '  and upload_date is null'
      '  and attached_by = :EmpID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
    Left = 188
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
  end
  object LunchLockoutTimer: TTimer
    Enabled = False
    OnTimer = LunchLockoutTimerTimer
    Left = 344
  end
end
