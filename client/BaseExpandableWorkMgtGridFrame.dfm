inherited ExpandableWorkMgtGridFrameBase: TExpandableWorkMgtGridFrameBase
  Width = 795
  Height = 470
  inherited SummaryPanel: TPanel
    Width = 795
    Height = 31
    Align = alTop
    Caption = 'SumPanel'
    Font.Height = -13
    inherited ExpandLabel: TLabel
      Left = 686
    end
  end
  inherited DataPanel: TPanel
    Top = 31
    Width = 795
    Height = 439
    Align = alClient
    inherited DataSummaryPanel: TPanel
      Width = 795
      Height = 31
      Caption = 'DataPanel'
      Font.Height = -13
    end
    object BaseGrid: TcxGrid
      Left = 0
      Top = 31
      Width = 795
      Height = 133
      Align = alClient
      TabOrder = 1
      OnEnter = BaseGridEnter
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object BaseGridLevelDetail: TcxGridLevel
      end
    end
    object PanelDisplaySelectedItem: TPanel
      Left = 0
      Top = 164
      Width = 795
      Height = 275
      Align = alBottom
      TabOrder = 2
    end
  end
  object ActionListFrame: TActionList
    Left = 476
    Top = 88
    object AcknowledgeAction: TAction
      Caption = '&Acknowledge'
    end
    object NormalPriorityAction: TAction
      Caption = 'Normal Priority'
    end
    object HighPriorityAction: TAction
      Caption = 'High Priority'
    end
    object ReleasePriorityAction: TAction
      Caption = 'Release For Work'
    end
  end
  object PriorityPopup: TPopupMenu
    Left = 440
    Top = 88
    object HighPriorityTicketMenuItem: TMenuItem
      Action = HighPriorityAction
    end
    object NormalPriorityTicketMenuItem: TMenuItem
      Action = NormalPriorityAction
    end
    object ReleaseforWorkMenuItem: TMenuItem
      Action = ReleasePriorityAction
    end
  end
  object ListTable: TDBISAMTable
    DatabaseName = 'Memory'
    EngineVersion = '4.44 Build 3'
    Exclusive = True
    Left = 76
    Top = 91
  end
  object DetailTable: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    Left = 752
    Top = 344
  end
end
