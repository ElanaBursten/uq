unit TimesheetReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, Spin,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TTimesheetReportForm = class(TReportBaseForm)
    PeriodLabel: TLabel;
    ChooseDate: TcxDateEdit;
    ManagerLabel: TLabel;
    EmployeeLabel: TLabel;
    ManagersComboBox: TComboBox;
    LocatorComboBox: TComboBox;
    SortLabel: TLabel;
    SortCombo: TComboBox;
    Depth: TSpinEdit;
    DepthLabel: TLabel;
    AllCheckBox: TCheckBox;
    lbIncludeEmployee: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure ChooseDateChange(Sender: TObject);
    procedure AllCheckBoxClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    function GetBaseFileName: string; override;
  private
    procedure SetVisibility;
    function LocatorMode: Boolean;
    procedure RepopulateLocatorList;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdMiscUtils, OdVclUtils, LocalEmployeeDMu;

{ TTimesheetReportForm }

procedure TTimesheetReportForm.ValidateParams;
begin
  inherited;
  RequireComboBox(ManagersComboBox, 'Please select a manager.');
  if LocatorMode then
    RequireComboBox(LocatorComboBox, 'Please select an employee.');

  SetReportID('TimesheetDetailNew');
  SetParamDate('end_date', ChooseDate.Date);

  if LocatorMode then begin
    SetParamInt('one_emp', 1);
    SetParamInt('emp_id', GetComboObjectInteger(LocatorComboBox));
  end else begin
    SetParamInt('one_emp', 0);
    SetParamIntCombo('emp_id', ManagersComboBox, True);
    SetParamInt('HierarchyDepth', Depth.Value);
    SetParam('sort', SortCombo.Text);
    SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
  end;
end;

procedure TTimesheetReportForm.InitReportControls;
begin
  inherited;
  AllCheckBox.Checked := True;
  SetUpManagerList(ManagersComboBox);
  ChooseDate.Date := ThisSaturday;
  SortCombo.ItemIndex := 0;
  SetVisibility;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TTimesheetReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  RepopulateLocatorList;
end;

procedure TTimesheetReportForm.SetVisibility;
begin
  LocatorComboBox.Enabled := LocatorMode;
  Depth.Enabled := not LocatorMode;
  SortCombo.Enabled := not LocatorMode;
  EmployeeStatusCombo.Enabled := not LocatorMode;
end;

procedure TTimesheetReportForm.ChooseDateChange(Sender: TObject);
begin
  if ChooseDate.Date <> SaturdayIze(ChooseDate.Date) then
    ChooseDate.Date := SaturdayIze(ChooseDate.Date);
end;

function TTimesheetReportForm.LocatorMode: Boolean;
begin
  Result := not AllCheckBox.Checked;
end;

function TTimesheetReportForm.GetBaseFileName: string;
begin
  Result := 'TimesheetReport';
end;

procedure TTimesheetReportForm.RepopulateLocatorList;
begin
  if LocatorMode then begin
    if ManagersComboBox.ItemIndex > -1 then begin
      EmployeeDM.EmployeeList(LocatorComboBox.Items, GetComboObjectInteger(ManagersComboBox))
    end;
  end else
    LocatorComboBox.Items.Clear;
end;

procedure TTimesheetReportForm.AllCheckBoxClick(Sender: TObject);
begin
  inherited;
  SetVisibility;
  RepopulateLocatorList;
end;

end.

