unit AreaEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, DMu, DB, DBISAMTb, StdCtrls, ExtCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxButtonEdit, cxDBLookupComboBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxNavigator, cxCustomData, cxFilter, cxData;

type
  TmapRecord = packed record
    mapID: string[8];
    d: TDateTime;
  end;

  TAreaEditorForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    DataPanel: TPanel;
    AreaGrid: TcxGrid;
    AreaGridLevel: TcxGridLevel;
    AreaGridView: TcxGridDBTableView;
    SaveChangesButton: TButton;
    EditableAreasSource: TDataSource;
    EditableAreas: TDBISAMTable;
    ColMapName: TcxGridDBColumn;
    ColAreaName: TcxGridDBColumn;
    ColShortName: TcxGridDBColumn;
    ColLocatorID: TcxGridDBColumn;
    ColState: TcxGridDBColumn;
    ColCounty: TcxGridDBColumn;
    ColTicketCode: TcxGridDBColumn;
    ColMapID: TcxGridDBColumn;
    ColAreaID: TcxGridDBColumn;
    QtrMinBtn: TButton;
    QtrMinMap: TcxGridDBColumn;
    procedure SaveChangesButtonClick(Sender: TObject);
    procedure ColLocatorButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure EditableAreasBeforeOpen(DataSet: TDataSet);
    procedure QtrMinBtnClick(Sender: TObject);
    procedure AreaEditorBtnPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
    FLocatorRoot: Integer;
    ReceiverHandle: THandle;

  public
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure GoToMap;
  end;

const
  QM_ED = 'QtrMinuteGrid.exe';

implementation

uses
  OdHourglass, EmployeeSearch, QMConst, OdDbUtils, OdCxUtils, ShellAPI, OdMiscUtils,
  LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

{ TAreaEditorForm }

procedure TAreaEditorForm.ActivatingNow;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  inherited;
  EditableAreas.Close;
  DM.Engine.UpdateMyMapAreas;
  EditableAreas.Open;
  DM.Engine.LinkEvents(EditableAreas);
  AreaGridView.ViewData.Collapse(True);
  FocusFirstGridRow(AreaGrid, True);
  FLocatorRoot := StrToIntDef(PermissionsDM.GetLimitationDef(RightRoutingEditAreas, IntToStr(DM.EmpID)), DM.EmpID);
  ColLocatorID.Visible := DM.Engine.DeveloperMode;
  ColAreaID.Visible := DM.Engine.DeveloperMode;
  ColMapID.Visible := DM.Engine.DeveloperMode;
  QtrMinBtn.Enabled := DM.UQState.QtrMinEnabled;
end;

procedure TAreaEditorForm.LeavingNow;
  var
  receiverHandle: HWND;

begin
  inherited;
  receiverHandle := FindWindow('TfrmMain', nil);
  if receiverHandle > 0 then
    PostMessage(receiverHandle, WM_QUIT, 0, 0);
end;


procedure TAreaEditorForm.SaveChangesButtonClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  PostDataSet(EditableAreas);
  DM.Engine.SendAreaChanges;
end;



procedure TAreaEditorForm.QtrMinBtnClick(Sender: TObject);
begin
  inherited;
  GoToMap;
end;

procedure TAreaEditorForm.AreaEditorBtnPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  GoToMap;
end;

procedure TAreaEditorForm.ColLocatorButtonClick(Sender: TObject; AbsoluteIndex: Integer);   //QM-938 EB Emp Search Changes
var
  SelectedID: integer;
  SearchForm: TEmployeeSearchForm;
  lShortName, LEmpNum, lManager: string;
  lID, lMgrID: integer;
  lFN, lLN: string;
begin
  SearchForm := TEmployeeSearchForm.Create(nil);
  try
    lID := EditableAreas.FieldByName('locator_id').AsInteger;   //QM-938 EB Emp Search Changes
   // lShortName := EmployeeDM.GetEmployeeShortName(lID);   {Short name messes up Tomorrow Bucket searches}
   EmployeeDM.GetEmployeeFullName(lID, lFN, lLN);
    lShortName := lLN;
    LEmpNum := EmployeeDM.GetEmployeeNumber(lID);
    lMGrID := EmployeeDM.GetManagerIDForEmp(lID);
    lManager := EmployeeDM.GetEmployeeShortName(lMgrID);

    // if SearchForm.Execute(esAllEmployeesUnderManager, Selected, '', FLocatorRoot) then begin
    If SearchForm.PreloadExecute(lID, lShortName, lEmpNum, lManager, '') then  begin     //QM-938 EB Emp Search Changes
      EditDataSet(EditableAreas);
      EditableAreas.FieldByName('locator_id').AsInteger := Lid;
    end;
  finally
    FreeAndNil(SearchForm);
  end;
end;

procedure TAreaEditorForm.EditableAreasBeforeOpen(DataSet: TDataSet);
var
  lResult: TStringField;
begin
  inherited;

  lResult := AddLookupField('short_name', EditableAreas, 'locator_id', EmployeeDM.EmployeeLookup, 'emp_id', 'short_name');
end;

procedure TAreaEditorForm.GoToMap;
var
  mapRecord: TmapRecord;
  copyDataStruct: TCopyDataStruct;
  MapIDStr, EmpIDStr, PassHash, Parameters : string;

  procedure SetMapStruct(curMapID: string);
  begin
    mapRecord.mapID := EditableAreas.FieldByName('Map_id').AsString;
    mapRecord.d := Now;

    copyDataStruct.dwData := integer(1000);
    copyDataStruct.cbData := SizeOf(mapRecord);
    copyDataStruct.lpData := @mapRecord;
  end;


begin
    MapIDStr := EditableAreas.FieldByName('Map_id').AsString;
    EmpIDStr := IntToStr(DM.EmpID);
    PassHash := EmployeeDM.GetPasswordHash;
    ReceiverHandle :=  FindWindow('TfrmMain', nil);
    Parameters := MapIDStr + ' ' + EmpIDStr + ' ' + PassHash;
    SetMapStruct(MapIDStr);

    if ReceiverHandle = 0 then
    begin
      ShellExecute(handle, 'open',PChar('QtrMinuteGrid.exe'),PChar(parameters), nil, SW_SHOWNORMAL);
    end
    else
      SendMessage(receiverHandle, WM_COPYDATA, integer(handle), integer(@copyDataStruct));
end;



initialization
  TDM.RegisterForm('AreaEditor', TAreaEditorForm);

end.
