object EmployeeDM: TEmployeeDM
  OldCreateOrder = False
  Height = 520
  Width = 427
  object EmpHier: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    IndexFieldNames = 'emp_id'
    TableName = 'emp_hier'
    Left = 36
    Top = 22
  end
  object ManagersUnderEmp: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    Left = 200
    Top = 88
  end
  object GetEmployeeQuery: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select employee.*'
      ' from employee'
      ' where employee.emp_id=:emp_id'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 45
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object GetManagerID: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select report_to'
      'from employee'
      'where emp_id = :EmpID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
    Left = 204
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
  end
  object EmployeeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'employee'
    Left = 36
    Top = 136
  end
  object Employees: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select HierLoc.depth - HierMgr.depth as level, HierLoc.emp_id,'
      
        '  Locator.short_name, Locator.active, HierLoc.lft, Locator.type_' +
        'id,'
      
        '  Substring('#39'                              '#39' from 1 for ((HierLo' +
        'c.depth - HierMgr.depth)*2)) + Locator.short_name as indented'
      'from emp_hier HierLoc, emp_hier HierMgr'
      '  inner join employee Locator on (HierLoc.emp_id=Locator.emp_id)'
      'where HierLoc.lft between HierMgr.lft and HierMgr.rght'
      '  and HierMgr.emp_id = :report_to'
      'order by HierLoc.lft')
    Params = <
      item
        DataType = ftUnknown
        Name = 'report_to'
      end>
    Left = 40
    Top = 82
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'report_to'
      end>
  end
  object ManagersWithInactiveEmps: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select manager.emp_id,  count(e.emp_id) '
      'from employee manager '
      '    inner join employee e on manager.emp_id = e.report_to'
      'where'
      '  e.active = false'
      'group by manager.emp_id'
      'having count(e.emp_id) > 0')
    Params = <>
    Left = 208
    Top = 144
  end
  object UserLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select u.uid, u.emp_id, e.short_name, e.first_name, e.last_name'
      'from users u'
      '  inner join employee e on e.emp_id = u.emp_id')
    Params = <>
    Left = 172
    Top = 209
  end
  object EmpTypes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select ref_id, code, description, sortby from reference where ty' +
        'pe = '#39'emptype'#39
      'order by sortby')
    Params = <>
    Left = 124
    Top = 20
  end
  object ActivityLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select * from employee_activity where (activity_type=:activity_t' +
        'ype) and activity_date >=:activity_date')
    Params = <
      item
        DataType = ftUnknown
        Name = 'activity_type'
      end
      item
        DataType = ftUnknown
        Name = 'activity_date'
      end>
    Left = 180
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'activity_type'
      end
      item
        DataType = ftUnknown
        Name = 'activity_date'
      end>
  end
  object EmpIsManagerQuery: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select count(emp_id)'
      'from employee'
      ' where (report_to=:emp_id) and active = 1'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end>
    Left = 65469
    Top = 544
    ParamData = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end>
  end
  object FirstTaskLookup: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select ticket_id, est_start_date'
      'from task_schedule'
      'where emp_id=: EMPID')
    Params = <>
    Left = 53
    Top = 288
  end
  object qryEmployeeBalance: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select *'
      'from Employee_Balance'
      'where emp_id = :EmpID'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
    Left = 117
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
  end
  object qryAbandonedCallOut: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'SELECT entry_id'
      '      ,work_emp_id'
      '      ,work_date'
      '      ,status'
      '      ,callout_start1'
      '      ,callout_stop1'
      '      ,callout_start2'
      '      ,callout_stop2'
      '      ,callout_start3'
      '      ,callout_stop3'
      '      ,callout_start4'
      '      ,callout_stop4'
      '      ,callout_start5'
      '      ,callout_stop5'
      '      ,callout_start6'
      '      ,callout_stop6'
      '      ,callout_start7'
      '      ,callout_stop7'
      '      ,callout_start8'
      '      ,callout_stop8'
      '      ,callout_start9'
      '      ,callout_stop9'
      '      ,callout_start10'
      '      ,callout_stop10'
      '      ,callout_start11'
      '      ,callout_stop11'
      '      ,callout_start12'
      '      ,callout_stop12'
      '      ,callout_start13'
      '      ,callout_stop13'
      '      ,callout_start14'
      '      ,callout_stop14'
      '      ,callout_start15'
      '      ,callout_stop15'
      '      ,callout_start16'
      '      ,callout_stop16'
      '  FROM timesheet_entry'
      '  where work_emp_id= :empID'
      '  and  work_date = :lookupdate'
      'and status = '#39'ACTIVE'#39
      'order by entry_id desc top 1')
    Params = <
      item
        DataType = ftInteger
        Name = 'empID'
      end
      item
        DataType = ftDateTime
        Name = 'lookupdate'
      end>
    Left = 56
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'empID'
      end
      item
        DataType = ftDateTime
        Name = 'lookupdate'
      end>
  end
end
