inherited TimesheetDayFrame: TTimesheetDayFrame
  object DayName: TLabel [1]
    Left = 0
    Top = 1
    Width = 80
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Mon Jan 19'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited BelowCalloutPanel: TPanel
    TabOrder = 13
    inherited BelowCalloutBottomPanel: TPanel
      inherited ApprovalMemo: TMemo
        TabOrder = 6
      end
      inherited SubmitButton: TButton
        TabOrder = 5
      end
      inherited BereavementHours: TcxDBCurrencyEdit
        TabOrder = 7
      end
      inherited HolidayHours: TcxDBCurrencyEdit
        TabOrder = 8
      end
      inherited JuryDutyHours: TcxDBCurrencyEdit
        TabOrder = 9
      end
      inherited MilesStart1: TcxDBCurrencyEdit
        TabOrder = 3
      end
      inherited MilesStop1: TcxDBCurrencyEdit
        TabOrder = 4
      end
      inherited FloatingHoliday: TDBCheckBox
        TabOrder = 10
        Visible = True
      end
      inherited VehicleUse: TcxDBComboBox
        TabOrder = 2
      end
      inherited EmployeeType: TComboBox
        TabOrder = 0
      end
      inherited ReasonChanged: TcxComboBox
        TabOrder = 1
      end
    end
  end
  inherited SubmitHint: TStaticText
    TabOrder = 14
  end
  object WorkStart1: TcxDBTimeEdit [4]
    Left = 0
    Top = 15
    AutoSize = False
    DataBinding.DataField = 'work_start1'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 0
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Height = 21
    Width = 36
  end
  object WorkStop1: TcxDBTimeEdit [5]
    Left = 35
    Top = 15
    DataBinding.DataField = 'work_stop1'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 1
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStart2: TcxDBTimeEdit [6]
    Left = 0
    Top = 34
    DataBinding.DataField = 'work_start2'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 2
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStop2: TcxDBTimeEdit [7]
    Left = 35
    Top = 34
    DataBinding.DataField = 'work_stop2'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 3
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStart3: TcxDBTimeEdit [8]
    Left = 0
    Top = 53
    DataBinding.DataField = 'work_start3'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 4
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStop3: TcxDBTimeEdit [9]
    Left = 35
    Top = 53
    DataBinding.DataField = 'work_stop3'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 5
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStart4: TcxDBTimeEdit [10]
    Left = 0
    Top = 72
    DataBinding.DataField = 'work_start4'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 6
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object WorkStop4: TcxDBTimeEdit [11]
    Left = 35
    Top = 72
    DataBinding.DataField = 'work_stop4'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 7
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object CalloutStart1: TcxDBTimeEdit [12]
    Left = 0
    Top = 97
    DataBinding.DataField = 'callout_start1'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 8
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object CalloutStop1: TcxDBTimeEdit [13]
    Left = 35
    Top = 97
    DataBinding.DataField = 'callout_stop1'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 9
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object CalloutStart2: TcxDBTimeEdit [14]
    Left = 0
    Top = 116
    DataBinding.DataField = 'callout_start2'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 10
    OnExit = TimeStartExit
    OnKeyDown = TimeKeyDown
    Width = 37
  end
  object CalloutStop2: TcxDBTimeEdit [15]
    Left = 35
    Top = 116
    DataBinding.DataField = 'callout_stop2'
    DataBinding.DataSource = TimesheetSource
    Enabled = False
    Properties.Alignment.Horz = taRightJustify
    Properties.AutoSelect = False
    Properties.ReadOnly = False
    Properties.SpinButtons.Visible = False
    Properties.TimeFormat = tfHourMin
    TabOrder = 11
    OnKeyDown = TimeKeyDown
    Width = 36
  end
  object MoreCalloutsPanel: TPanel [16]
    Left = 0
    Top = 138
    Width = 79
    Height = 79
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 12
    Visible = False
    object CalloutStart3: TcxDBTimeEdit
      Left = 0
      Top = -2
      DataBinding.DataField = 'callout_start3'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 0
      OnExit = TimeStartExit
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStart4: TcxDBTimeEdit
      Left = 0
      Top = 17
      DataBinding.DataField = 'callout_start4'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 2
      OnExit = TimeStartExit
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStart5: TcxDBTimeEdit
      Left = 0
      Top = 37
      DataBinding.DataField = 'callout_start5'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 4
      OnExit = TimeStartExit
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStop3: TcxDBTimeEdit
      Left = 35
      Top = -2
      DataBinding.DataField = 'callout_stop3'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 1
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStop4: TcxDBTimeEdit
      Left = 35
      Top = 17
      DataBinding.DataField = 'callout_stop4'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 3
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStop5: TcxDBTimeEdit
      Left = 35
      Top = 37
      DataBinding.DataField = 'callout_stop5'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 5
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStart6: TcxDBTimeEdit
      Left = 0
      Top = 56
      DataBinding.DataField = 'callout_start6'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 6
      OnExit = TimeStartExit
      OnKeyDown = TimeKeyDown
      Width = 36
    end
    object CalloutStop6: TcxDBTimeEdit
      Left = 35
      Top = 56
      DataBinding.DataField = 'callout_stop6'
      DataBinding.DataSource = TimesheetSource
      Enabled = False
      Properties.Alignment.Horz = taRightJustify
      Properties.AutoSelect = False
      Properties.ReadOnly = False
      Properties.SpinButtons.Visible = False
      Properties.TimeFormat = tfHourMin
      TabOrder = 7
      OnKeyDown = TimeKeyDown
      Width = 36
    end
  end
  inherited NothingButton: TButton
    TabOrder = 15
  end
  inherited TimesheetSource: TDataSource
    Left = 88
    Top = 158
  end
  inherited TimesheetData: TDBISAMTable
    BeforePost = nil
    Left = 72
    Top = 113
  end
  inherited DayActions: TActionList
    Top = 258
  end
  inherited DefaultEditStyleController: TcxDefaultEditStyleController
    Left = 96
    Top = 207
    PixelsPerInch = 96
  end
end
