inherited NewTicketReportForm: TNewTicketReportForm
  Left = 410
  Top = 186
  Caption = 'New Ticket Detail'
  ClientHeight = 511
  ClientWidth = 396
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 61
    Width = 37
    Height = 13
    Caption = 'Tickets:'
  end
  object Label3: TLabel [2]
    Left = 0
    Top = 103
    Width = 45
    Height = 13
    Caption = 'Locators:'
  end
  object Label6: TLabel [3]
    Left = 0
    Top = 37
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [4]
    Left = 91
    Top = 8
    Width = 246
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 0
    TabOrder = 0
    OnChange = ManagersComboBoxChange
  end
  object PrintNewRadio: TRadioButton [5]
    Left = 91
    Top = 60
    Width = 174
    Height = 17
    Caption = 'Print only new or changed '
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object PrintAllRadio: TRadioButton [6]
    Left = 91
    Top = 75
    Width = 174
    Height = 17
    Caption = 'Print all open for each locator'
    TabOrder = 3
  end
  object LocatorListBox: TListBox [7]
    Left = 91
    Top = 99
    Width = 263
    Height = 399
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 4
  end
  object EmployeeStatusCombo: TComboBox [8]
    Left = 91
    Top = 34
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 1
    OnChange = EmployeeStatusComboChange
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 3
    Top = 370
  end
end
