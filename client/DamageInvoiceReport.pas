unit DamageInvoiceReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TDamageInvoiceReportForm = class(TReportBaseForm)
    Label1: TLabel;
    ReceivedDateRange: TOdRangeSelectFrame;
    Label2: TLabel;
    ProfitCenterLabel: TLabel;
    ProfitCenter: TComboBox;
    InvoiceCompany: TEdit;
    UtilityCoDamagedLabel: TLabel;
    PaidDateRangeLabel: TLabel;
    PaidDateRange: TOdRangeSelectFrame;
    ShowEstimates: TCheckBox;
    ShowPaidAmount: TCheckBox;
    InvoiceStatus: TComboBox;
    Label3: TLabel;
    procedure InvoiceStatusChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  OdExceptions, QMConst, DMu;

{ TDamageInvoiceReportForm }

procedure TDamageInvoiceReportForm.InitReportControls;
begin
  inherited;
  ReceivedDateRange.SelectDateRange(rsCustom, Date-1, 0);
  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
  PaidDateRange.SelectDateRange(rsAllDates);

  InvoiceStatus.Clear;
  InvoiceStatus.AddItem('All Invoices', TObject(0));
  InvoiceStatus.AddItem('Payable Invoices', TObject(1));
  InvoiceStatus.AddItem('Denied Invoices', TObject(2));
  InvoiceStatus.ItemIndex := 0;
end;

procedure TDamageInvoiceReportForm.ValidateParams;
begin
  inherited;
  SetReportID('DamageInvoice');
  if ReceivedDateRange.FromDate = 0 then
    raise EOdEntryRequired.Create('A starting received date is required.');

  SetParamDate('ReceivedFromDate', ReceivedDateRange.FromDate);
  if ReceivedDateRange.ToDate > 0 then
    SetParamDate('ReceivedToDate', ReceivedDateRange.ToDate)
  else
    SetParamDate('ReceivedToDate', EncodeDate(9999,12,31));
  SetParamDate('PaidFromDate', PaidDateRange.FromDate);
  SetParamDate('PaidToDate', PaidDateRange.ToDate);
  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParam('Company', InvoiceCompany.Text);
  SetParamBoolean('ShowPaidAmount', ShowPaidAmount.Checked);
  SetParamBoolean('ShowEstimates', ShowEstimates.Checked);
  SetParamIntCombo('InvoiceStatus', InvoiceStatus, True);
end;

procedure TDamageInvoiceReportForm.InvoiceStatusChange(Sender: TObject);
begin
  ShowPaidAmount.Enabled := InvoiceStatus.ItemIndex < 2;
  PaidDateRange.Enabled := InvoiceStatus.ItemIndex < 2;
end;

end.
