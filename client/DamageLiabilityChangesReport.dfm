inherited DamageLiabilityChangesReportForm: TDamageLiabilityChangesReportForm
  Left = 631
  Top = 133
  Caption = 'Damage Liability Changes Report'
  ClientHeight = 333
  ClientWidth = 565
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 0
    Top = 8
    Width = 120
    Height = 13
    Caption = 'Activity Date (Required):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 0
    Top = 96
    Width = 96
    Height = 13
    Caption = 'Damage Date Filter:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [2]
    Left = 0
    Top = 176
    Width = 94
    Height = 13
    Caption = 'Notified Date Filter:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [3]
    Left = 270
    Top = 35
    Width = 30
    Height = 13
    Caption = 'State:'
  end
  object Label2: TLabel [4]
    Left = 270
    Top = 59
    Width = 66
    Height = 13
    Caption = 'Profit Center:'
  end
  object LiabilityLabel: TLabel [5]
    Left = 270
    Top = 119
    Width = 80
    Height = 13
    Caption = 'Starting Liability:'
  end
  object Label6: TLabel [6]
    Left = 270
    Top = 145
    Width = 74
    Height = 13
    Caption = 'Ending Liability:'
  end
  object Label7: TLabel [7]
    Left = 270
    Top = 84
    Width = 66
    Height = 13
    Caption = 'Report Mode:'
  end
  object IncludeNoUQCheckBoxLabel: TLabel [8]
    Left = 378
    Top = 203
    Width = 173
    Height = 64
    AutoSize = False
    Caption = 
      'Include damages with no UQ liability at start or end of range.  ' +
      'These can be safely omitted for UQ liability reporting.'
    WordWrap = True
  end
  object Label8: TLabel [9]
    Left = 270
    Top = 176
    Width = 84
    Height = 13
    Caption = 'Minimum Change:'
  end
  object Label9: TLabel [10]
    Left = 442
    Top = 176
    Width = 94
    Height = 13
    Caption = '(for example, 500.)'
  end
  inline ActivityDate: TOdRangeSelectFrame [11]
    Left = 6
    Top = 24
    Width = 248
    Height = 59
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
      Alignment = taRightJustify
    end
  end
  inline DamageDate: TOdRangeSelectFrame
    Left = 6
    Top = 112
    Width = 248
    Height = 55
    TabOrder = 1
    inherited FromLabel: TLabel
      Top = 7
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Top = 7
    end
    inherited DatesLabel: TLabel
      Top = 33
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Top = 30
    end
    inherited FromDateEdit: TcxDateEdit
      Top = 4
    end
    inherited ToDateEdit: TcxDateEdit
      Top = 4
    end
  end
  inline NotifiedDate: TOdRangeSelectFrame
    Left = 6
    Top = 192
    Width = 248
    Height = 50
    TabOrder = 2
    inherited FromLabel: TLabel
      Top = 4
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Top = 4
    end
    inherited DatesLabel: TLabel
      Top = 30
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Top = 27
    end
    inherited FromDateEdit: TcxDateEdit
      Top = 1
    end
    inherited ToDateEdit: TcxDateEdit
      Top = 1
    end
  end
  object State: TComboBox
    Left = 358
    Top = 32
    Width = 98
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 18
    ItemHeight = 13
    MaxLength = 2
    TabOrder = 3
  end
  object ProfitCenter: TComboBox
    Left = 358
    Top = 56
    Width = 98
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 4
  end
  object LiabilityStart: TComboBox
    Left = 358
    Top = 116
    Width = 161
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 5
  end
  object LiabilityEnd: TComboBox
    Left = 358
    Top = 142
    Width = 161
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 6
  end
  object Mode: TComboBox
    Left = 358
    Top = 81
    Width = 185
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = ModeChange
    Items.Strings = (
      'UQ Liability Change Reporting'
      'Misc. Change Reporting')
  end
  object IncludeNoUQCheckBox: TCheckBox
    Left = 358
    Top = 202
    Width = 17
    Height = 17
    TabOrder = 8
  end
  object EditMinChange: TEdit
    Left = 358
    Top = 173
    Width = 73
    Height = 21
    TabOrder = 9
    OnKeyPress = EditMinChangeKeyPress
  end
  object ExcludeInitialEstimateCheckBox: TCheckBox
    Left = 358
    Top = 271
    Width = 193
    Height = 17
    Caption = 'Exclude initial estimate change'
    TabOrder = 10
  end
  object IncludeThirdParty: TCheckBox
    Left = 358
    Top = 291
    Width = 193
    Height = 17
    Caption = 'Include third party claims'
    TabOrder = 11
  end
  object IncludeLitigation: TCheckBox
    Left = 358
    Top = 311
    Width = 193
    Height = 17
    Caption = 'Include litigation claims'
    TabOrder = 12
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 14
    Top = 248
  end
end
