inherited LocalSearchForm: TLocalSearchForm
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Search'
  ClientHeight = 485
  ClientWidth = 586
  Constraints.MinHeight = 300
  Constraints.MinWidth = 430
  KeyPreview = True
  OldCreateOrder = True
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel: TPanel
    Left = 0
    Top = 107
    Width = 586
    Height = 378
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    Caption = 'GridPanel'
    TabOrder = 1
    object SearchGrid: TcxGrid
      Left = 8
      Top = 8
      Width = 570
      Height = 362
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object SearchGridView: TcxGridDBTableView
        OnDblClick = SearchGridDblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = SearchDS
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'emp_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.IncSearch = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.HideSelection = True
        OptionsSelection.InvertSelect = False
        OptionsView.GridLineColor = clBtnFace
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
      end
      object SearchGridLevel: TcxGridLevel
        GridView = SearchGridView
      end
    end
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 586
    Height = 107
    Align = alTop
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 0
    OnEnter = HeaderPanelEnter
    OnExit = HeaderPanelExit
    object RecordsLabel: TLabel
      Left = 260
      Top = 60
      Width = 55
      Height = 13
      Caption = '1000 found'
      Visible = False
    end
    object SearchButton: TButton
      Left = 259
      Top = 80
      Width = 75
      Height = 25
      Caption = '&Search'
      TabOrder = 0
      OnClick = SearchButtonClick
    end
    object CancelButton: TButton
      Left = 343
      Top = 80
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 3
    end
    object CopyButton: TButton
      Left = 427
      Top = 80
      Width = 75
      Height = 25
      Caption = 'C&opy'
      Enabled = False
      TabOrder = 1
      Visible = False
    end
    object ExportButton: TButton
      Left = 511
      Top = 80
      Width = 75
      Height = 25
      Caption = '&Export'
      Enabled = False
      ModalResult = 4
      TabOrder = 2
      Visible = False
    end
  end
  object SearchDS: TDataSource
    DataSet = SearchQuery
    Left = 16
    Top = 136
  end
  object SearchQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from employee where 0=1')
    Params = <>
    ReadOnly = True
    Left = 52
    Top = 136
  end
end
