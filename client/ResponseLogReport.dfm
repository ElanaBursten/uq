inherited ResponseLogReportForm: TResponseLogReportForm
  Left = 501
  Top = 301
  Caption = 'ResponseLogReportForm'
  ClientHeight = 307
  ClientWidth = 543
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 0
    Top = 131
    Width = 75
    Height = 12
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  object Label3: TLabel [1]
    Left = 278
    Top = 133
    Width = 36
    Height = 13
    Caption = 'Clients:'
  end
  object Label1: TLabel [2]
    Left = 0
    Top = 96
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label4: TLabel [3]
    Left = 0
    Top = 8
    Width = 111
    Height = 13
    Caption = 'Response Date Range:'
  end
  object ClientList: TCheckListBox [4]
    Left = 278
    Top = 149
    Width = 183
    Height = 156
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 2
  end
  object ManagersComboBox: TComboBox [5]
    Left = 66
    Top = 93
    Width = 176
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
  end
  object SelectAllButton: TButton [6]
    Left = 465
    Top = 149
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 3
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [7]
    Left = 465
    Top = 181
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 4
    OnClick = ClearAllButtonClick
  end
  object CallCenterList: TCheckListBox [8]
    Left = 0
    Top = 149
    Width = 275
    Height = 156
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 1
    OnClick = CallCenterListClick
  end
  inline TransmitRange: TOdRangeSelectFrame [9]
    Left = 15
    Top = 22
    Width = 271
    Height = 63
    TabOrder = 5
    inherited FromLabel: TLabel
      Left = 12
      Top = 11
    end
    inherited ToLabel: TLabel
      Left = 141
      Top = 11
    end
    inherited DatesLabel: TLabel
      Left = 12
      Top = 37
    end
    inherited DatesComboBox: TComboBox
      Left = 50
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 50
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 162
    end
  end
  object SuccessfulResponses: TCheckBox
    Left = 286
    Top = 32
    Width = 187
    Height = 17
    BiDiMode = bdLeftToRight
    Caption = 'Include successful response detail'
    ParentBiDiMode = False
    TabOrder = 6
  end
  object LimitClients: TCheckBox
    Left = 331
    Top = 131
    Width = 136
    Height = 17
    Caption = 'Limit to Specific Clients'
    TabOrder = 7
    OnClick = LimitClientsClick
  end
  object FailedResponses: TCheckBox
    Left = 286
    Top = 58
    Width = 163
    Height = 17
    Caption = 'Include failed response detail'
    TabOrder = 8
  end
end
