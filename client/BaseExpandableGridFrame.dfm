inherited ExpandableGridFrameBase: TExpandableGridFrameBase
  inherited SummaryPanel: TPanel
    Caption = 'SumPanel'
    object CountLabelSummary: TLabel
      Left = 111
      Top = 10
      Width = 31
      Height = 20
      Alignment = taRightJustify
      AutoSize = False
      Caption = '000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SummaryPanelClick
    end
    object OpenLabelSummary: TLabel
      Left = 150
      Top = 10
      Width = 38
      Height = 19
      Caption = 'Open'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SummaryPanelClick
    end
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'DataPanel'
      object CountLabelDataSummary: TLabel
        Left = 111
        Top = 10
        Width = 31
        Height = 20
        Alignment = taRightJustify
        AutoSize = False
        Caption = '000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object OpenLabelDataSummary: TLabel
        Left = 150
        Top = 10
        Width = 38
        Height = 19
        Caption = 'Open'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object BaseGrid: TcxGrid
      Left = 0
      Top = 41
      Width = 916
      Height = 255
      Align = alClient
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object BaseGridLevelDetail: TcxGridLevel
      end
      object BaseGridLevelSummary: TcxGridLevel
      end
    end
  end
end
