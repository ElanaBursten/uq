unit TimesheetDay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, DB, ComCtrls, QMConst, ActnList, ExtCtrls,
  BaseTimesheetDay, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit,
  cxDBEdit, cxDropDownEdit, cxMaskEdit, cxSpinEdit, cxTimeEdit, cxClasses,
  dbisamtb;

type
  TTimesheetDayFrame = class(TBaseTimesheetDayFrame)
    DayName: TLabel;
    WorkStart1: TcxDBTimeEdit;
    WorkStop1: TcxDBTimeEdit;
    WorkStart2: TcxDBTimeEdit;
    WorkStop2: TcxDBTimeEdit;
    WorkStart3: TcxDBTimeEdit;
    WorkStop3: TcxDBTimeEdit;
    WorkStart4: TcxDBTimeEdit;
    WorkStop4: TcxDBTimeEdit;
    CalloutStart1: TcxDBTimeEdit;
    CalloutStop1: TcxDBTimeEdit;
    CalloutStart2: TcxDBTimeEdit;
    CalloutStop2: TcxDBTimeEdit;
    CalloutStart3: TcxDBTimeEdit;
    CalloutStop3: TcxDBTimeEdit;
    CalloutStart4: TcxDBTimeEdit;
    CalloutStop4: TcxDBTimeEdit;
    CalloutStart5: TcxDBTimeEdit;
    CalloutStop5: TcxDBTimeEdit;
    CalloutStart6: TcxDBTimeEdit;
    CalloutStop6: TcxDBTimeEdit;
    MoreCalloutsPanel: TPanel;
    procedure TimeStartExit(Sender: TObject);
    procedure DayActionsUpdate(Action: TBasicAction; var Handled: Boolean);
  protected
    procedure SetDayLabel; override;
    procedure SetExpanded(const Value: Boolean); override;
    procedure RefreshScreenControls; override;
  public
    procedure Display(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode); override;
    procedure SaveChanges; override;
    constructor Create(Owner: TComponent); override;
    procedure FocusIfPossible; override;
  end;

implementation

uses DMu, OdDbUtils, OdMiscUtils, OdDBISAMUtils,
  Variants;

{$R *.dfm}

{ TTimesheetDayFrame }

constructor TTimesheetDayFrame.Create(Owner: TComponent);
begin
  inherited;
  SubmitHint.Left := WorkStart1.Left;
  SubmitHint.Top := WorkStart1.Top;
  SubmitHint.Height := (WorkStart1.Height-1) * 4;
  SubmitHint.Width := WorkStart1.Width + WorkStart2.Width;
end;

procedure TTimesheetDayFrame.Display(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode);
begin
  inherited;
end;

procedure TTimesheetDayFrame.SaveChanges;
begin
  inherited;
end;

procedure TTimesheetDayFrame.TimeStartExit(Sender: TObject);
begin
  // Fill in a default stop time?
end;

procedure TTimesheetDayFrame.FocusIfPossible;
const
  WorkTimeEdits: array[0..7] of string = ('WorkStart1','WorkStop1','WorkStart2',
    'WorkStop2','WorkStart3','WorkStop3','WorkStart4','WorkStop4');
var
  i: Integer;
  ControlToFocus: TComponent;
  FieldName: string;
begin
  if TimeSheet.Active and (not ReadOnly) then begin
    try
      for i := Low(WorkTimeEdits) to High(WorkTimeEdits) do begin
        ControlToFocus := FindComponent(WorkTimeEdits[i]);
        Assert(Assigned(ControlToFocus) and (ControlToFocus is TcxDBTimeEdit),
          'Could not find a time edit component named ' + WorkTimeEdits[i]);
        with ControlToFocus as TcxDBTimeEdit do begin
          FieldName := DataBinding.DataField;
          if DataBinding.DataSource.DataSet.FieldByName(FieldName).IsNull then begin
            Enabled := True;
            if CanFocus then begin
              EditDataSet(DataBinding.DataSource.DataSet);
              SetFocus;
              DataBinding.DataSource.DataSet.FieldByName(FieldName).Value := SysUtils.Time;
              Break;
            end;
          end;
        end;
      end;
    except
    end;
  end;
end;

procedure TTimesheetDayFrame.SetDayLabel;
begin
  inherited;
  DayName.Caption := FormatDateTime('ddd mmm d', FWorkDate);
  if DisplayingToday then
    DayName.Font.Style := DayName.Font.Style + [fsBold]
  else
    DayName.Font.Style := DayName.Font.Style - [fsBold];
end;

procedure TTimesheetDayFrame.DayActionsUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  RefreshScreenControls;
end;

procedure TTimesheetDayFrame.RefreshScreenControls;
begin
  inherited;

  WorkStart1.Enabled := not ReadOnly;
  WorkStart2.Enabled := not ReadOnly;
  WorkStart3.Enabled := not ReadOnly;
  WorkStart4.Enabled := not ReadOnly;
  WorkStop1.Enabled := not ReadOnly;
  WorkStop2.Enabled := not ReadOnly;
  WorkStop3.Enabled := not ReadOnly;
  WorkStop4.Enabled := not ReadOnly;
  CalloutStart1.Enabled := not ReadOnly;
  CalloutStart2.Enabled := not ReadOnly;
  CalloutStart3.Enabled := not ReadOnly;
  CalloutStart4.Enabled := not ReadOnly;
  CalloutStart5.Enabled := not ReadOnly;
  CalloutStart6.Enabled := not ReadOnly;
  CalloutStop1.Enabled := not ReadOnly;
  CalloutStop2.Enabled := not ReadOnly;
  CalloutStop3.Enabled := not ReadOnly;
  CalloutStop4.Enabled := not ReadOnly;
  CalloutStop5.Enabled := not ReadOnly;
  CalloutStop6.Enabled := not ReadOnly;
end;

procedure TTimesheetDayFrame.SetExpanded(const Value: Boolean);
var
  BreakComponent: TWinControl;
  DayHeight: Integer;
begin
  inherited;
  if FExpanded then begin
    BreakComponent := MoreCalloutsPanel;
    DayHeight := DefaultDayHeight + ((CalloutStart6.Top + CalloutStart6.Height) - CalloutStart3.Top);
  end
  else begin
    BreakComponent := CalloutStart2;
    DayHeight := DefaultDayHeight;
  end;
  MoreCalloutsPanel.Visible := FExpanded;
  BelowCalloutPanel.Top := BreakComponent.Top + BreakComponent.Height;
  Height := DayHeight + 8;
end;

end.


