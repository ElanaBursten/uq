// Add this unit to access commonly used Dev Ex styles.
unit SharedDevExStyles;

interface

uses
  SysUtils, Classes, cxStyles, cxLookAndFeels, cxEdit, ImgList, Controls,
  cxGraphics, cxDropDownEdit, cxClasses, Graphics, Forms, DBCtrls, ClipBrd;

type
  TSharedDevExStyleData = class(TDataModule)
    GridStyleRepo: TcxStyleRepository;
    GrayBackgroundStyle: TcxStyle;
    BoldFontStyle: TcxStyle;
    BoldRedFontStyle: TcxStyle;
    LookAndFeelController: TcxLookAndFeelController;
    DefaultEditStyleController: TcxDefaultEditStyleController;
    NavyGridGroupSummary: TcxStyle;
    BoldLargeFontStyle: TcxStyle;
    cxImageList: TcxImageList;
    GridSelStyleRepo: TcxStyleRepository;
    BottomGridStyle: TcxStyle;
    GridSelectionStyle: TcxStyle;
    UnfocusedGridSelStyle: TcxStyle;
    SkyBlueHighlightSelection: TcxStyle;
    BoldLargeRedFontStyle: TcxStyle;
    DebugHeaderCol: TcxStyle;
    AltColorEven: TcxStyle;
	BoldMaroonFont: TcxStyle;
    SmallGridHighlight: TcxStyle;
    OldTicketsOdd: TcxStyle;
    IncSearchHighlight: TcxStyle;
    OldTicketsGroupTitle: TcxStyle;
    OldTicketsEven: TcxStyle;
    GrayFontInactiveNode: TcxStyle;
    BoldBlueFont: TcxStyle;
    BoldBrownStyle: TcxStyle;
    HeaderPlain: TcxStyle;
    NavyHeader: TcxStyle;      //QM-671 EB MARS Bulk Status
  public
    procedure SetGlyph(Control: TcxCustomDropDownEdit; const ImageIdx: Integer);
  end;

var
  SharedDevExStyleData : TSharedDevExStyleData;

implementation

{$R *.dfm}

{ TSharedDevExStyleData }


procedure TSharedDevExStyleData.SetGlyph(Control: TcxCustomDropDownEdit;
  const ImageIdx: Integer);
begin
  cxImageList.GetImage(ImageIdx, Control.Properties.ButtonGlyph);
end;

end.
