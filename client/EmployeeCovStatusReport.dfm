inherited EmployeeCovStatusReportForm: TEmployeeCovStatusReportForm
  Left = 460
  Top = 204
  Caption = 'Employee COV Status'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 11
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object ManagerCombo: TComboBox [1]
    Left = 59
    Top = 8
    Width = 229
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object ShowDetailsCheckBox: TCheckBox [2]
    Left = 59
    Top = 42
    Width = 209
    Height = 17
    Caption = 'Show Employee Details'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
end
