unit TimeClockDB;

interface

uses TimeClockEntry, DB;

type
  TGridRowWriter = class(TInterfacedObject, IRowWriter)
  private
    FDataSet: TDataSet;
  public
    procedure AddRow(EmpID: Integer; WorkDate, ActivityTime: TDateTime; const ActivityType: string);
    constructor Create(DataSet: TDataSet);
  end;

  TDbDataSaver = class(TInterfacedObject, IDataSaver)
  private
    FTimesheetData: TDataSet;
    FTimeclockData: TDataSet;
  public
    function GetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; var WorkTimes, CalloutTimes: array of TTimeSpan; var LunchStartTime: TDateTime): Boolean;
    procedure SetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime; WorkTimes, CalloutTimes: array of TTimeSpan; LunchStartTime: TDateTime);
    procedure ClearTimesheetTimes(EmpID: Integer; WorkDate: TDateTime);
    procedure ClearTimeclockTimes(EmpID: Integer; WorkDate: TDateTime);
    constructor Create(TimeSheetData, TimeClockData: TDataSet);
  end;

implementation

uses
  SysUtils, OdDbUtils, OdDBISAMUtils, OdMiscUtils, QMConst;

{ TGridRowWriter }

procedure TGridRowWriter.AddRow(EmpID: Integer; WorkDate, ActivityTime: TDateTime; const ActivityType: string);
begin
  if not  Assigned(FDataSet) then
    raise Exception.Create('DataSet not assigned in TGridRowWriter.AddRow');
  FDataSet.Append;
  FDataSet.FieldByName('emp_id').AsInteger := EmpID;
  FDataSet.FieldByName('work_date').AsDateTime := WorkDate;
  FDataSet.FieldByName('activity_time').AsDateTime := ActivityTime;
  FDataSet.FieldByName('activity_type').AsString := ActivityType;
  FDataSet.Post;
end;

constructor TGridRowWriter.Create(DataSet: TDataSet);
begin
  inherited Create;
  FDataSet := DataSet;
end;

{ TDbDataSaver }

constructor TDbDataSaver.Create(TimeSheetData, TimeClockData: TDataSet);
begin
  inherited Create;
  FTimesheetData := TimeSheetData;
  FTimeclockData := TimeClockData;
  Assert(Assigned(FTimesheetData));
  Assert(Assigned(FTimeclockData));
end;

procedure TDbDataSaver.ClearTimeclockTimes(EmpID: Integer; WorkDate: TDateTime);
begin
  FTimeclockData.Filter := 'emp_id=' + IntToStr(EmpID) + ' and work_date=' + DateToDBISAMDate(WorkDate);
  FTimeclockData.Filtered := True;
  try
    FTimeclockData.First;
    while not FTimeclockData.Eof do
      FTimeclockData.Delete;
  finally
    FTimeclockData.Filtered := False;
  end;
end;

procedure TDbDataSaver.ClearTimesheetTimes(EmpID: Integer; WorkDate: TDateTime);
var
  I: Integer;
begin
  Assert((EmpID = FTimesheetData.FieldByName('work_emp_id').AsInteger) and
    FloatEquals(WorkDate, FTimesheetData.FieldByName('work_date').AsDateTime),
    'Timesheet_entry is not on the right row to clear times.');
  Assert(EditingDataSet(FTimesheetData), 'Timesheet_entry needs to be in edit mode before clearing times.');

  for I := 1 to TimesheetNumWorkPeriods do begin
    FTimesheetData.FieldByName('work_start'+IntToStr(I)).Clear;
    FTimesheetData.FieldByName('work_stop'+IntToStr(I)).Clear;
  end;
  for I := 1 to TimesheetNumCalloutPeriods do begin
    FTimesheetData.FieldByName('callout_start'+IntToStr(I)).Clear;
    FTimesheetData.FieldByName('callout_stop'+IntToStr(I)).Clear;
  end;
  FTimesheetData.FieldByName('lunch_start').Clear;
end;

procedure TDbDataSaver.SetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime;
  WorkTimes, CalloutTimes: array of TTimeSpan; LunchStartTime: TDateTime);

  procedure SetTimeValue(const TimeField: string; TimeValue: TDateTime);
  begin
    if FloatEquals(TimeValue, 0) then
      FTimesheetData.FieldByName(TimeField).Clear
    else
      FTimesheetData.FieldByName(TimeField).AsDateTime := TimeValue;
  end;

var
  I: Integer;
begin
  Assert((EmpID = FTimesheetData.FieldByName('work_emp_id').AsInteger) and
    FloatEquals(WorkDate, FTimesheetData.FieldByName('work_date').AsDateTime),
    'Timesheet_entry is not on the right row to set times.');
  EditDataSet(FTimesheetData);
  Assert(EditingDataSet(FTimesheetData), 'Timesheet_entry needs to be in edit mode before setting times.');
  for I := Low(WorkTimes) to High(WorkTimes) do begin
    SetTimeValue('work_start' + IntToStr(I+1), WorkTimes[I].StartTime);
    SetTimeValue('work_stop' + IntToStr(I+1), WorkTimes[I].StopTime);
  end;
  for I := Low(CalloutTimes) to High(CalloutTimes) do begin
    SetTimeValue('callout_start' + IntToStr(I+1), CalloutTimes[I].StartTime);
    SetTimeValue('callout_stop' + IntToStr(I+1), CalloutTimes[I].StopTime);
  end;
  SetTimeValue('lunch_start', LunchStartTime);
  FTimesheetData.FieldByName('source').AsString := 'clock';
end;

function TDbDataSaver.GetTimesheetTimes(EmpID: Integer; WorkDate: TDateTime;
  var WorkTimes, CalloutTimes: array of TTimeSpan; var LunchStartTime: TDateTime): Boolean;

  function TimeValue(const TimeField: string): TDateTime;
  begin
    if FTimesheetData.FieldByName(TimeField).IsNull then
      Result := 0
    else
      Result := FTimesheetData.FieldByName(TimeField).AsDateTime;
  end;

var
  I: Integer;
begin
  Assert((EmpID = FTimesheetData.FieldByName('work_emp_id').AsInteger) and
    FloatEquals(WorkDate, FTimesheetData.FieldByName('work_date').AsDateTime),
    'Timesheet_entry is not on the right row to get times.');
  Result := True;
  for I := Low(WorkTimes) to High(WorkTimes) do begin
    WorkTimes[I].StartTime := TimeValue('work_start' + IntToStr(I+1));
    WorkTimes[I].StopTime  := TimeValue('work_stop' + IntToStr(I+1));
  end;
  for I := Low(CalloutTimes) to High(CalloutTimes) do begin
    CalloutTimes[I].StartTime := TimeValue('callout_start' + IntToStr(I+1));
    CalloutTimes[I].StopTime  := TimeValue('callout_stop' + IntToStr(I+1));
  end;
  LunchStartTime := TimeValue('lunch_start');
end;

end.
