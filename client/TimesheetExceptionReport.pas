unit TimesheetExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, ComCtrls, dxCore, cxDateUtils;

type
  TTimesheetExceptionReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    ManagersComboBox: TComboBox;
    DateEdit: TcxDateEdit;
    lbIncludeEmployee: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

procedure TTimesheetExceptionReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimesheetExceptionNew');
  SetParamDate('StartDate', DateEdit.Date);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TTimesheetExceptionReportForm.InitReportControls;
begin
  inherited;
  DateEdit.Date := Date - 1;
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.

