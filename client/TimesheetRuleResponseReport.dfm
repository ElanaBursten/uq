inherited TimesheetRuleResponseReportForm: TTimesheetRuleResponseReportForm
  Left = 576
  Top = 201
  Caption = 'TimesheetRuleResponseReportForm'
  ClientHeight = 251
  ClientWidth = 509
  PixelsPerInch = 96
  TextHeight = 13
  object ResponseDateRangeLabel: TLabel [0]
    Left = 0
    Top = 2
    Width = 111
    Height = 13
    Caption = 'Response Date Range:'
  end
  inline ResponseDateRange: TOdRangeSelectFrame [1]
    Left = 23
    Top = 16
    Width = 248
    Height = 63
    TabOrder = 0
  end
  object IncludeNoticeGroup: TRadioGroup
    Left = 0
    Top = 80
    Width = 237
    Height = 73
    Caption = ' Notices to Include '
    ItemIndex = 0
    Items.Strings = (
      'Submit Time Message'
      'Manager Changed Time Message'
      'Both Messages')
    TabOrder = 1
  end
  object IncludeResponseGroup: TRadioGroup
    Left = 250
    Top = 80
    Width = 181
    Height = 73
    Caption = ' Responses to Include '
    ItemIndex = 0
    Items.Strings = (
      'Accept, Agree'
      'Reject, Disagree'
      'All')
    TabOrder = 2
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 16
    Top = 168
  end
end
