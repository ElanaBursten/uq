unit MyWorkWOs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableDataFrame, BaseExpandableGridFrame, StdCtrls, ExtCtrls,
  cxStyles, cxGraphics, cxDataStorage, cxEdit, DB,
  cxDBData, cxTextEdit, cxCheckBox, cxDropDownEdit, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, DBISAMTb, dxCore,
  cxLookAndFeels, cxLookAndFeelPainters, cxGridLevel, DMu, UQUtils, QMConst,
  cxNavigator, cxCustomData, cxFilter, cxData;

const
  UM_WORK_ORDERS_EXPANDALL = WM_USER + $103;
type
  TMyWorkWOsFrame = class(TExpandableGridFrameBase)
    OpenWorkOrders: TDBISAMQuery;
    WorkOrdersSource: TDataSource;
    MyWorkViewRepository: TcxGridViewRepository;
    WorkOrdersGridDetailsTableView: TcxGridDBBandedTableView;
    WoDColWorkOrderID: TcxGridDBBandedColumn;
    WoDColWorkOrderNumber: TcxGridDBBandedColumn;
    WoDColClientID: TcxGridDBBandedColumn;
    WoDColWOSource: TcxGridDBBandedColumn;
    WoDColKind: TcxGridDBBandedColumn;
    WoDColStatus: TcxGridDBBandedColumn;
    WoDColMapPage: TcxGridDBBandedColumn;
    WoDColMapRef: TcxGridDBBandedColumn;
    WoDColDueDate: TcxGridDBBandedColumn;
    WoDColWorkType: TcxGridDBBandedColumn;
    WoDColWorkDescription: TcxGridDBBandedColumn;
    WoDColWorkAddressNumber: TcxGridDBBandedColumn;
    WoDColWorkAddressStreet: TcxGridDBBandedColumn;
    WoDColWorkCounty: TcxGridDBBandedColumn;
    WoDColWorkCity: TcxGridDBBandedColumn;
    WoDColWorkState: TcxGridDBBandedColumn;
    WoDColWorkZip: TcxGridDBBandedColumn;
    WoDColWorkLat: TcxGridDBBandedColumn;
    WoDColWorkLong: TcxGridDBBandedColumn;
    WoDColCallerName: TcxGridDBBandedColumn;
    WoDColCallerPhone: TcxGridDBBandedColumn;
    WoDColCallerCellular: TcxGridDBBandedColumn;
    WoDColClientTransmitDate: TcxGridDBBandedColumn;
    WoDColJobNumber: TcxGridDBBandedColumn;
    WoDColWireCenter: TcxGridDBBandedColumn;
    WoDColWorkCenter: TcxGridDBBandedColumn;
    WoDColCentralOffice: TcxGridDBBandedColumn;
    WoDColServingTerminal: TcxGridDBBandedColumn;
    WoDColCircuitNumber: TcxGridDBBandedColumn;
    WoDColF2Cable: TcxGridDBBandedColumn;
    WoDColTerminalPort: TcxGridDBBandedColumn;
    WoDColF2Pair: TcxGridDBBandedColumn;
    WoDColStateHwyROW: TcxGridDBBandedColumn;
    WoDColRoadBoreCount: TcxGridDBBandedColumn;
    WoDColDrivewayBoreCount: TcxGridDBBandedColumn;
    WoDColExport: TcxGridDBBandedColumn;
    WorkOrdersGridSummaryTableView: TcxGridDBBandedTableView;
    WoSColWorkOrderID: TcxGridDBBandedColumn;
    WoSColWorkOrderNumber: TcxGridDBBandedColumn;
    WoSColDueDate: TcxGridDBBandedColumn;
    WoSColKind: TcxGridDBBandedColumn;
    WoSColStatus: TcxGridDBBandedColumn;
    WoSColWorkAddressNumber: TcxGridDBBandedColumn;
    WoSColWorkAddressStreet: TcxGridDBBandedColumn;
    WoSColWorkCounty: TcxGridDBBandedColumn;
    WoSColMapPage: TcxGridDBBandedColumn;
    WoSColMapRef: TcxGridDBBandedColumn;
    WoSColExport: TcxGridDBBandedColumn;
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure OpenWorkOrdersCalcFields(DataSet: TDataSet);
    procedure OpenWorkOrdersBeforeOpen(DataSet: TDataSet);
    procedure OpenWorkOrdersAfterScroll(DataSet: TDataSet);
  private
      FOnSelectWorkOrder: TItemSelectedEvent;
    procedure WorkOrdersExpandAll;
    procedure UMWorkOrdersExpandAll(var Message: TMessage); message UM_WORK_ORDERS_EXPANDALL;
    procedure RefreshWorkOrders;
    function WorkOrderStatusColumn: TcxGridColumn;
  protected
    function ItemIDColumn: TcxGridColumn; override;
    procedure SetupColumns; override;
    procedure SetGridEvents; override;
  public
      property OnSelectWorkOrder: TItemSelectedEvent read FOnSelectWorkOrder write FOnSelectWorkOrder;
    procedure SortGridByFields; override;
    function ExportMyItems: string; override;
    procedure SetAllExportBoxes(Checked: Boolean); override;
    procedure RefreshData; override;
    procedure RefreshCaptionCounts; override;
  end;

var
  MyWorkWOsFrame: TMyWorkWOsFrame;

implementation

{$R *.dfm}

uses OdHourglass, OdDBUtils, OdMiscUtils, OdCxUtils, OdDBISAMUtils, 
  SharedDevExStyles, 
  LocalPermissionsDMu, LocalEmployeeDMu;

const
  WorkOrderSortColumns: array[0..0] of string = ('due_date');
  WorkOrderSortDirections: array[0..0] of TcxGridSortOrder = (soAscending);

  SelectMyWorkOrders = 'select wo_id, wo_number, client.client_name, wo_source, Kind, status, ' +
    'map_page, map_ref, due_date, work_type, work_description, work_address_number, ' +
    'work_address_street, work_county, work_city, work_state, work_zip, work_lat, work_long, ' +
    'caller_name, caller_phone, caller_cellular, transmit_date, job_number, wire_center, work_center, ' +
    'central_office, serving_terminal, circuit_number, f2_cable, terminal_port, f2_pair, state_hwy_row, ' +
    ' road_bore_count, driveway_bore_count, False export  ' +
    'from work_order ' +
    '  left join client on client.client_id = work_order.client_id ' +
    'where assigned_to_id = :emp_id ' +
    'and active ' +
    'and not closed ' +
    'and kind <> ''' + WorkOrderKindCancel + ''' ' +
    'order by due_date, wo_number';

{ TWorkOrderDataFrame }

procedure TMyWorkWOsFrame.RefreshCaptionCounts;
begin
  inherited;
  CountLabelSummary.Caption := IntToStr(DM.nOpenWorkOrders);
  CountLabelDataSummary.Caption := IntToStr(DM.nOpenWorkOrders);
end;

procedure TMyWorkWOsFrame.SetupColumns;
begin
  WoDColExport.Visible := PermissionsDM.CanI(RightWorkOrdersSelectiveExport);
  WoSColExport.Visible := PermissionsDM.CanI(RightWorkOrdersSelectiveExport);
  SortGridByFieldNames(BaseGrid, WorkOrderSortColumns, WorkOrderSortDirections);
  WorkOrdersExpandAll;
end;

procedure TMyWorkWOsFrame.WorkOrdersExpandAll;
begin
  PostMessage(Self.Handle, UM_WORK_ORDERS_EXPANDALL, 0, 0);
  If BaseGrid.CanFocus then
    BaseGrid.SetFocus;
end;

procedure TMyWorkWOsFrame.UMWorkOrdersExpandAll(var Message: TMessage);
begin
  FullExpandGrid(BaseGrid);
  if BaseGrid.CanFocus then
    BaseGrid.SetFocus
end;

procedure TMyWorkWOsFrame.RefreshWorkOrders;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  OpenWorkOrders.DisableControls;
  try
    OpenWorkOrders.Close;
    OpenWorkOrders.SQL.Text := SelectMyWorkOrders;
    OpenWorkOrders.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenWorkOrders.Open;
    SetFieldDisplayFormat(OpenWorkOrders, 'transmit_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenWorkOrders, 'due_date', 'mmm d, t');
  finally
    OpenWorkOrders.EnableControls;
  end;

  Self.Visible := (OpenWorkOrders.RecordCount >= 1);
  if BaseGrid.Visible then
    SetupColumns;
end;

procedure TMyWorkWOsFrame.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  WorkOrderStatus: TWorkOrderStatus;
begin
  inherited;
  if (AViewInfo.GridRecord is TcxGridGroupRow) then
    Exit;

  if not AViewInfo.Selected then begin
    if not VarIsNull(AViewInfo.GridRecord.Values[WorkOrderStatusColumn.Index]) then
      WorkOrderStatus := TWorkOrderStatus(Byte(AViewInfo.GridRecord.Values[WorkOrderStatusColumn.Index]))
    else
      WorkOrderStatus := [];
    ACanvas.Brush.Color := GetWorkOrderColor(WorkOrderStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
  end;
  ADone := False;
end;

procedure TMyWorkWOsFrame.OpenWorkOrdersCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('work_order_status').AsInteger := Byte(GetWorkOrderStatusFromRecord(DataSet));
end;

procedure TMyWorkWOsFrame.OpenWorkOrdersAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if Assigned(FOnSelectWorkOrder) then
    FOnSelectWorkOrder(Self, DataSet.FieldByName('wo_id').AsInteger);
end;

procedure TMyWorkWOsFrame.OpenWorkOrdersBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddCalculatedField('work_order_status', DataSet, TIntegerField, 0);
end;

procedure TMyWorkWOsFrame.SortGridByFields;
begin
  inherited;
  SortGridByFieldNames(BaseGrid, WorkOrderSortColumns, WorkOrderSortDirections);
end;

function TMyWorkWOsFrame.ExportMyItems: string;
const
  SelectMyWorkOrdersExport = 'select distinct wo_number "Ticket #", ' +
    'work_lat "Lat", work_long "Long", due_date, kind, ' +
    'TRIM(LEADING '' '' FROM (COALESCE(work_address_number, '''')+' +
    ''' ''+work_address_street)) "Name", ' +
    'CAST(due_date AS varchar(16)) "Name2", ' +
    'work_address_number "Address", work_address_street "Street", ' +
    'work_city "City", work_state "State", ' +
    'work_county "County" '+         //QM-364  sr/eb
    'from work_order wo ' +
    'where wo.assigned_to_id = %d ' +
    'and wo.active = True ' +
    'and wo.closed = False ' +
    '%s ' + //Placeholder for 'and wo_id in ()' criteria
    'order by wo.kind desc, wo.due_date, wo.wo_number ';
var
  Wait: IInterface;
  ExportFileName: string;
  WorkOrderIDCriteria: string;
  SelectedWorkOrderIDs : string;
  WorkOrdersToExport: TDataSet;
  Select: string;
  SelectedWorkOrderExportCount: Integer;
  CanSelectWorkOrdersToExport: Boolean;
  GPXOptions : TGPXOptions;
begin
  Result := '';
  Wait := ShowHourGlass;
  WorkOrderIDCriteria := '';
  CanSelectWorkOrdersToExport := PermissionsDM.CanI(RightWorkOrdersSelectiveExport);
  GPXOptions.IncludeRouteOrder := False;  //QM-133 Past Due
  GPXOptions.IncludeEmpName := False;

  if CanSelectWorkOrdersToExport then
    SelectedWorkOrderIDs := GetItemIDsForExportString(OpenWorkOrders, 'wo_id', SelectedItemID)
  else
    SelectedWorkOrderIDs := '';

  if CanSelectWorkOrdersToExport and IsEmpty(SelectedWorkOrderIDs) then
    Exit;

  if NotEmpty(SelectedWorkOrderIDs) then
    WorkOrderIDCriteria := 'and wo_id in (' + SelectedWorkOrderIDs + ') ';

  Select := Format(SelectMyWorkOrdersExport, [DM.EmpID, WorkOrderIDCriteria]);
  WorkOrdersToExport := DM.Engine.OpenQuery(Select, 'MyWO');  //QM-51 EB
  try
    SelectedWorkOrderExportCount := WorkOrdersToExport.RecordCount;
    ExportFileName := DM.CreateExportFileName('-My Work Orders', '.gpx');
    DM.SaveGpxToFile(WorkOrdersToExport, ExportFileName, GPXOptions); //QM-133 Past Due (Refactoring of SaveGPXToFile)
    Result := ExportFileName;
  finally
    FreeAndNil(WorkOrdersToExport);
  end;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWorkOrderRouteExport, IntToStr(SelectedWorkOrderExportCount));
end;

procedure TMyWorkWOsFrame.SetAllExportBoxes(Checked: Boolean);
begin
  inherited;
  if FViewIndex = 0 then
    SetExportAll(OpenWorkOrders,WorkOrdersGridDetailsTableView, Checked)
  else
    SetExportAll(OpenWorkOrders, WorkOrdersGridSummaryTableView, Checked);
end;

function TMyWorkWOsFrame.ItemIDColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = WorkOrdersGridDetailsTableView then
    Result := WoDColWorkOrderID
  else
    Result := WoSColWorkOrderID;
end;

function TMyWorkWOsFrame.WorkOrderStatusColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = WorkOrdersGridDetailsTableView then
    Result := WoDColStatus
  else
    Result := WoSColStatus;
end;

procedure TMyWorkWOsFrame.RefreshData;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  RefreshWorkOrders;
end;

procedure TMyWorkWOsFrame.SetGridEvents;
  procedure SetViewEvents(View: TcxGridDBBandedTableView);
  begin
    View.DataController.OnCompare := GridCompare;
    View.OnCustomDrawCell := GridViewCustomDrawCell;
    View.OnDblClick := GridDblClick;
  end;
begin
  inherited;
  SetViewEvents(WorkOrdersGridDetailsTableView);
  SetViewEvents(WorkOrdersGridSummaryTableView);
end;

end.

