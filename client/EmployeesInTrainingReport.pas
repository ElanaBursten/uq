unit EmployeesInTrainingReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst, OdRangeSelect;

type
  TEmployeesInTrainingReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    ShowDetailsCheckBox: TCheckBox;
    Label1: TLabel;
    TrainingDateRange: TOdRangeSelectFrame;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu;

procedure TEmployeesInTrainingReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
  TrainingDateRange.SelectDateRange(rsMonthToDate);
end;

procedure TEmployeesInTrainingReportForm.ValidateParams;
begin
  inherited;
  SetReportID('EmployeesInTraining');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamDate('StartDate', TrainingDateRange.FromDate, True);
  SetParamDate('EndDate', TrainingDateRange.ToDate, True);
  SetParamBoolean('ShowDetails', ShowDetailsCheckBox.Checked);
end;

end.
