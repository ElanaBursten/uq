unit WorkOrderDetailOHM;

interface

uses                                                     
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDBEdit, StdCtrls, DBCtrls, ExtCtrls,
  DB, cxCheckListBox, dbisamtb, WorkOrderDetailBase, 
  cxDropDownEdit, cxStyles, cxDataStorage,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxGridCustomView,
  cxGrid, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxCheckBox,
  ComCtrls, cxCustomData, cxFilter, cxData;

type
  TWOOHMFrame = class(TWODetailBaseFrame)
    AccountNumber: TDBText;
    WorkOrderInspectionDS: TDataSource;
    ComplianceDueDate: TDBText;
    AssignedToLabel: TLabel;
    AssignedTo: TDBText;
    Label4: TLabel;
    PremiseIDLabel: TLabel;
    PremiseID: TDBText;
    CallerPhoneLabel: TLabel;
    AtlPhoneLabel: TLabel;
    Phone: TDBText;
    AltPhone: TDBText;
    ComplianceDueDateLabel: TLabel;
    AOCInspectionLabel: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    CustInspectionLabel: TLabel;
    InitialInspectionPanel: TPanel;
    InitialInspectionLabel: TLabel;
    DateOfVisitLabel: TLabel;
    CGACountLabel: TLabel;
    CompletionCodeLabel: TLabel;
    DateOfVisit: TDBText;
    CGACount: TDBText;
    CompletionCodeCombobox: TDBLookupComboBox;
    Remedies: TDBISAMQuery;
    PotentialHazardCheckList: TcxCheckListBox;
    AOCInspectionCheckList: TcxCheckListBox;
    CustomerInspectionCheckList: TcxCheckListBox;
    WorkTypes: TDBISAMQuery;
    ViewRepo: TcxGridViewRepository;
    BuildingView: TcxGridDBBandedTableView;
    BuildingRef: TDBISAMQuery;
    BuildingRefSource: TDataSource;
    BuildingDescription: TcxGridDBBandedColumn;
    MapStatusRef: TDBISAMQuery;
    MapStatusRefSource: TDataSource;
    MeterLocationRef: TDBISAMQuery;
    MeterLocationRefSource: TDataSource;
    YnubRef: TDBISAMQuery;
    YnubRefSource: TDataSource;
    VentClearanceRef: TDBISAMQuery;
    VentClearanceRefSource: TDataSource;
    VentIgnitionRef: TDBISAMQuery;
    VentIgnitionRefSource: TDataSource;
    VentClearanceDistCombobox: TcxDBExtLookupComboBox;
    VentIgnitionDistComboBox: TcxDBExtLookupComboBox;
    InadequateVentClearance: TcxCheckBox;
    InadequateVentClearanceIg: TcxCheckBox;
    UpdateWORemedy: TDBISAMQuery;
    InsertWORemedy: TDBISAMQuery;
    CGAReasonLabel: TLabel;
    CGAReasonCombo: TcxDBExtLookupComboBox;
    CGAReasonRef: TDBISAMQuery;
    CGAReasonRefSource: TDataSource;
    CGAReasonView: TcxGridDBBandedTableView;
    CGAReasonDescription: TcxGridDBBandedColumn;
    GasLightView: TcxGridDBBandedTableView;
    GasLightDescription: TcxGridDBBandedColumn;
    GasLightRef: TDBISAMQuery;
    GasLightRefSource: TDataSource;
    WorkOrderOHMdetails: TDBISAMTable;
    dsWorkOrderOHMdetails: TDataSource;
    pcOHM: TPageControl;
    tsGasMain: TTabSheet;
    pnlGasMain: TPanel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    COATINGTYP: TcxDBTextEdit;
    INSTALLEDD: TcxDBTextEdit;
    NOMINALDIA: TcxDBTextEdit;
    MATERIAL: TcxDBTextEdit;
    MEASUREDLE: TcxDBTextEdit;
    tsPremise: TTabSheet;
    pnlPremise: TPanel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    mai_1: TcxDBTextEdit;
    sl_pi: TcxDBTextEdit;
    slin_s: TcxDBTextEdit;
    slp_2: TcxDBTextEdit;
    slp_1: TcxDBTextEdit;
    slpi_p: TcxDBTextEdit;
    meter_1: TcxDBTextEdit;
    curb_b: TcxDBTextEdit;
    meter_2: TcxDBTextEdit;
    main_r: TcxDBTextEdit;
    main_p: TcxDBTextEdit;
    slpre: TcxDBTextEdit;
    slp_3: TcxDBTextEdit;
    dbCkBox_has_c: TcxDBCheckBox;
    cmbxCurbValueFound: TcxComboBox;
    Label23: TLabel;
    procedure InadequateVentClearanceClick(Sender: TObject);
    procedure VItemDblClick(Sender: TObject);
    procedure RemedyCheckListClickCheck(Sender: TObject;
      AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure ComboBoxEditing(Sender: TObject; var CanEdit: Boolean);
    procedure VentClearanceDistComboboxEditing(Sender: TObject; var CanEdit: Boolean);
    procedure WorkTypesFilterRecord(DataSet: TDataSet; var Accept: Boolean);     //QMANTWO-446 EB
  private
    CompletionCode: string;
    FLoading: Boolean;
    fOHMtype: string;  //QMANTWO-393
    
    procedure SaveVentClearance(WorkType: string; Checkbox: TcxCheckbox);
    procedure SetRemedyEditing;
    procedure CheckVentDistanceCheckboxChanged(WorkType: string; Checkbox: TcxCheckbox);
    procedure SaveInspectionList(var CheckList: TcxCheckListBox);
    procedure SaveAllInspectionLists;
    function GetCGAReason(Description: String): String;
    procedure setOHMtype(const Value: string);
  protected
    function HasPotentialHazard: Boolean;
    procedure SetInheritedFrameEvents; override;
    procedure LoadInspectionLists;
    procedure LoadWorkOrderOHMdetails;
    procedure AfterGotoWorkOrder; override;
    function CurbValueFoundAnswered: boolean; //QMANTWO-391
    procedure SaveOHMDetails;
  public
    CurbValueChanged: Boolean; //QMANTWO-391 EB
    property OHMtype : string read fOHMtype  write setOHMtype; //QMANTWO-393
    procedure CheckCGAReason;
    procedure Load(ID: Integer); override;
    procedure EnableEditing(Enabled: Boolean); override;
    function ValidateWorkOrderFields: Boolean; override;
    function NeedToSave: Boolean; override;
    procedure SaveWorkOrderDetails; override;
    procedure CancelWorkOrder; override;
  end;

const
  NO_VALUE = '--';
implementation

uses DMu, OdMiscUtils, OdDbUtils, OdCxUtils, QMConst;

{$R *.dfm}

{ TWOOHMFrame }

procedure TWOOHMFrame.EnableEditing(Enabled: Boolean);
begin
  inherited;
  VentClearanceDistCombobox.Enabled := InadequateVentClearance.Checked;
  VentIgnitionDistComboBox.Enabled := InadequateVentClearanceIg.Checked;
  CGAReasonCombo.Enabled := Enabled and (CompletionCodeCombobox.Text = CGAStatus);
end;

function TWOOHMFrame.HasPotentialHazard: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to PotentialHazardCheckList.Items.Count - 1 do begin
    Result := PotentialHazardCheckList.Items[i].Checked;
    if Result then
      Break;
  end;
end;

procedure TWOOHMFrame.InadequateVentClearanceClick(Sender: TObject);
begin
  inherited;
  VentClearanceDistCombobox.Enabled := InadequateVentClearance.Checked;
  CheckVentDistanceCheckboxChanged(WorkTypeVC,InadequateVentClearance);
end;

procedure TWOOHMFrame.VItemDblClick(Sender: TObject);
begin
  inherited;
  VentIgnitionDistComboBox.Enabled := InadequateVentClearanceIg.Checked;
  CheckVentDistanceCheckboxChanged(WorkTypeVI,InadequateVentClearanceIg);
end;

procedure TWOOHMFrame.AfterGotoWorkOrder;
begin
  inherited;
  FLoading := False;
end;

procedure TWOOHMFrame.CheckVentDistanceCheckboxChanged(WorkType: string; Checkbox: TcxCheckbox);
begin
  if not FLoading then
    SetRemedyEditing;
end;

procedure TWOOHMFrame.Load(ID: Integer);
begin
  FLoading := True;
  inherited;
  try
    CompletionCode := WorkOrderDS.DataSet.FieldByName('status').AsString;
    LoadWorkOrderOHMdetails;
    LoadInspectionLists;
    RefreshDataSet(BuildingRef);
    RefreshDataSet(MapStatusRef);
    RefreshDataSet(MeterLocationRef);
    RefreshDataSet(YnubRef);
    RefreshDataSet(VentClearanceRef);
    RefreshDataSet(VentIgnitionRef);
    RefreshDataSet(CGAReasonRef);
    RefreshDataSet(GasLightRef);
  finally
    FLoading := False;
  end;
end;

procedure TWOOHMFrame.LoadWorkOrderOHMdetails;
begin
   CurbValueChanged := False;

   if WorkOrderOHMDetails.Active then
     WorkOrderOHMdetails.Close;
   WorkOrderOHMdetails.Filter := 'wo_id='+WorkOrderDS.DataSet.FieldByName('wo_id').asString;
   WorkOrderOHMdetails.Active := True;

   if fLoading then begin  //QMANTWO-391 QMANTWO-393 EB
     {Values for WorkOrderOHMdetails that can change when saved}
     if dsWorkOrderOHMDetails.DataSet.FieldByName('curbvaluefound').IsNull then
       cmbxCurbValueFound.Text := NO_VALUE
     else begin
       cmbxCurbValueFound.Text := UPPERCASE(BooleanToStringYesNo(dsWorkOrderOHMDetails.DataSet.FieldByName('curbvaluefound').AsBoolean));
     end;
   CurbValueChanged := False;
   end;
end;

procedure TWOOHMFrame.LoadInspectionLists;
var
  TargetCheckList: TcxCheckListBox;
  ListItem: TcxCheckListBoxItem;
  WorkTypeCode: string;
  IsPopulated: Boolean;
  FlagColor: string;
begin
  RefreshDataSet(WorkTypes);

  Remedies.Close;
  Remedies.ParamByName('WOID').AsInteger := WorkOrderDS.DataSet.FieldByName('wo_id').AsInteger;
  Remedies.Open;

  AOCInspectionCheckList.Clear;
  PotentialHazardCheckList.Clear;
  CustomerInspectionCheckList.Clear;

  //First use WorkTypes to populate the checkbox lists.
  WorkTypes.First;
  while not WorkTypes.EOF do begin
    FlagColor := WorkTypes.FieldByName('flag_color').AsString;
    WorkTypeCode := WorkTypes.FieldByName('work_type').AsString;
    //Use Remedies to set any checked values
    IsPopulated := Remedies.Locate('work_type',WorkTypeCode,[]);
    //Find out whick checklist to put it in
    if FlagColor = 'RED' then
      TargetChecklist := PotentialHazardChecklist
    else if FlagColor = 'ORANGE' then
      TargetChecklist := CustomerInspectionChecklist
    else
      TargetChecklist := AOCInspectionChecklist;

    if WorkTypeCode = WorkTypeVC then
      InadequateVentClearance.Checked := IsPopulated;
    if WorkTypeCode = WorkTypeVI then
      InadequateVentClearanceIg.Checked := IsPopulated;

    if (WorkTypeCode <> WorkTypeVC) and (WorkTypeCode <> WorkTypeVI) then begin
      ListItem := TargetChecklist.Items.Add;
      ListItem.Text := WorkTypes.FieldByName('work_description').AsString;
      ListItem.Tag := WorkTypes.FieldByName('work_type_id').AsInteger;
      ListItem.DisplayName := WorkTypeCode;
      ListItem.Checked := IsPopulated;
    end;
    WorkTypes.Next;
  end;
end;



function TWOOHMFrame.NeedToSave: Boolean;
begin
  Result := EditingDataSet(WorkOrderDS.DataSet)
    or EditingDataSet(WorkOrderInspectionDS.DataSet)
    or CurbValueChanged;
end;

procedure TWOOHMFrame.RemedyCheckListClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
  inherited;
  SetRemedyEditing;
end;

procedure TWOOHMFrame.ComboBoxEditing(Sender: TObject; var CanEdit: Boolean);
begin
  inherited;
  EditDataSet(WorkOrderDS.DataSet);
end;


function TWOOHMFrame.CurbValueFoundAnswered: boolean;   //QMANTWO-391
begin
  if cmbxCurbValueFound.Text = NO_VALUE then
    Result := False
  else
    Result := True;
end;

function TWOOHMFrame.GetCGAReason(Description: String): String; //todo this should be a common method for handling our blank descriptions?
begin
  if CGAReasonRef.Locate('description',Description,[]) then
    Result := CGAReasonRef.FieldByName('code').AsString
  else
    Result := 'B'; //Return default blank code
end;

procedure TWOOHMFrame.CheckCGAReason;
begin
  CGAReasonCombo.Enabled := (CompletionCodeCombobox.Text = CGAStatus);
  if not CGAReasonCombo.Enabled then
    CGAReasonCombo.Clear;
end;

procedure TWOOHMFrame.SaveAllInspectionLists;
begin
  SaveVentClearance(WorkTypeVC, InadequateVentClearance);
  SaveVentClearance(WorkTypeVI, InadequateVentClearanceIg);
  SaveInspectionList(AOCInspectionCheckList);
  SaveInspectionList(CustomerInspectionCheckList);
  SaveInspectionList(PotentialHazardCheckList);
end;

procedure TWOOHMFrame.SaveVentClearance(WorkType: string; Checkbox: TcxCheckbox);
const
  GetWorkTypeIDSQL = 'select work_type_id as N from work_order_work_type where work_type=''%s''';
  ExistsSQL = 'select count(*) as N from work_order_remedy where wo_id=%d and work_type_id=%d';
var
  WOID, WorkTypeID: Integer;
begin
  WOID := DM.WorkOrder.FieldByName('wo_id').AsInteger;
  WorkTypeID := DM.Engine.RunSQLReturnN(Format(GetWorkTypeIDSQL,[WorkType]));

  if DM.Engine.RunSQLReturnN(Format(ExistsSQL, [WOID, WorkTypeID])) = 1 then begin//work_order_remedy record already exists
    UpdateWORemedy.Params.ParamByName('active').Value := Checkbox.Checked;
    UpdateWORemedy.Params.ParamByName('wo_id').Value := WOID;
    UpdateWORemedy.Params.ParamByName('work_type_id').Value := WorkTypeID;
    UpdateWORemedy.ExecSQL;
  end
  else if Checkbox.Checked then begin //It's checked but not in the table, so we need to insert a new record
    OpenDataSet(DM.WorkOrderRemedy);
    DM.WorkOrderRemedy.Insert;
    DM.WorkOrderRemedy.FieldByName('wo_id').AsInteger := WOID;
    DM.WorkOrderRemedy.FieldByName('work_type_id').AsInteger := WorkTypeID;
    DM.WorkOrderRemedy.FieldByName('active').AsBoolean := True;
    DM.WorkOrderRemedy.Post;
  end;
end;

procedure TWOOHMFrame.SaveInspectionList(var CheckList: TcxCheckListBox);
const
  ExistsSQL = 'select count(*) as N from work_order_remedy where wo_id=%d and work_type_id=%d';
var
  i, WOID, WorkTypeID: Integer;
  Active: Boolean;
begin
  WOID := DM.WorkOrder.FieldByName('wo_id').AsInteger;
  for i := 0 to Pred(CheckList.Count) do begin
    WorkTypeID := CheckList.Items[i].Tag;
    Active := CheckList.Items[i].Checked;

    if DM.Engine.RunSQLReturnN(Format(ExistsSQL, [WOID, WorkTypeID])) = 1 then begin
      UpdateWORemedy.Params.ParamByName('active').Value := Ord(Active);
      UpdateWORemedy.Params.ParamByName('wo_id').Value := WOID;
      UpdateWORemedy.Params.ParamByName('work_type_id').Value := WorkTypeID;
      UpdateWORemedy.ExecSQL;
    end
    else if Active then begin
      OpenDataSet(DM.WorkOrderRemedy);
      DM.WorkOrderRemedy.Insert;
      DM.WorkOrderRemedy.FieldByName('wo_id').AsInteger := WOID;
      DM.WorkOrderRemedy.FieldByName('work_type_id').AsInteger := WorkTypeID;
      DM.WorkOrderRemedy.FieldByName('active').AsBoolean := True;
      DM.WorkOrderRemedy.Post;
    end;
  end;
end;

procedure TWOOHMFrame.SaveOHMDetails; //QMANTWO-391 & QMANTWO-393
var
  NewCurbValFound: string;
begin
  try
    NewCurbValFound :=  UpperCase(cmbxCurbValueFound.Text);
    WorkOrderOHMdetails.Close;
    WorkOrderOHMdetails.ReadOnly := False;
    EditDataSet(WorkOrderOHMdetails);
    WorkOrderOHMdetails.FieldByName('curbvaluefound').AsBoolean := (NewCurbValFound = 'YES');
    WorkOrderOHMdetails.FieldByName('modified_date').asDateTime := Now;
    WorkOrderOHMdetails.FieldByName('DeltaStatus').AsString := 'U';
    PostDataSet(WorkOrderOHMdetails);
  finally
    WorkOrderOHMdetails.Close;
    WorkOrderOHMdetails.ReadOnly := True;
    LoadWorkOrderOHMdetails;
  end;

end;

procedure TWOOHMFrame.SaveWorkOrderDetails;
var
  CGAVisitCount: Integer;
  AlertFirst, AlertLast, AlertConfirmation, AlertPhoneNo: string;
begin
  inherited;

  if EditingDataSet(WorkOrderInspectionDS.DataSet) then begin
    WorkOrderInspectionDS.DataSet.UpdateRecord;  // Force an update in case the user did not leave the field
    EditDataSet(WorkOrderInspectionDS.DataSet);
  end;

  EditDataSet(WorkOrderDS.DataSet);
  ValidateWorkOrderFields;

  StatusList.Locate('status', CompletionCodeCombobox.Text, []);
  if StatusList.FieldByName('complete').AsBoolean then begin
    WorkOrderDS.DataSet.FieldByName('closed').AsBoolean := True;
    WorkOrderDS.DataSet.FieldByName('closed_date').AsDateTime := Now;
  end;

  //OHM Details (those that may change)    //QMANTWO-391 EB
  if (fOHMtype = 'Premise') and CurbValueChanged and CurbValueFoundAnswered then begin
    SaveOHMDetails;
  end;


  //Associated Inspection record
  if not EditingDataSet(DM.WorkOrderInspection) then
    EditDataSet(DM.WorkOrderInspection);

  if CompletionCodeCombobox.Text = CGAStatus then begin
    CGAVisitCount := DM.WorkOrderInspection.FieldByName('cga_visits').AsInteger;
    Inc(CGAVisitCount);
    DM.WorkOrderInspection.FieldByName('cga_visits').AsInteger := CGAVisitCount;
  end
  else//make sure CGA reason is cleared for non-cga status
    DM.WorkOrderInspection.FieldByName('cga_reason').AsString := GetCGAReason('');

  //Grab Alert Data
  if HasPotentialHazard then begin
    AlertPhoneNo := DM.GetCallCenterAlertPhoneNoForCallCenterID(WorkOrderDS.DataSet.FieldByName('wo_source').AsString);
    AlertFirst := DM.WorkOrderInspection.FieldByName('alert_first_name').AsString;
    AlertLast := DM.WorkOrderInspection.FieldByName('alert_last_name').AsString;
    AlertConfirmation := DM.WorkOrderInspection.FieldByName('alert_order_number').AsString;
//    TPotentialHazardDialog.GetPotentialHazardData(               //QMANTWO-423
//      PotentialHazardChecklist.CheckedItemsString(False, sLineBreak),  //QMANTWO-423
//      AlertFirst, AlertLast, AlertConfirmation, AlertPhoneNo);          //QMANTWO-423
    DM.WorkOrderInspection.FieldByName('alert_first_name').AsString := AlertFirst;
    DM.WorkOrderInspection.FieldByName('alert_last_name').AsString := AlertLast;
    DM.WorkOrderInspection.FieldByName('alert_order_number').AsString := AlertConfirmation;
  end;

  PostDataSet(DM.WorkOrderInspection);

  //Work Order Remedies
  SaveAllInspectionLists;  //This will call save each of the checkbox lists

  if StatusList.FieldByName('complete').AsBoolean then begin
    WorkOrderDS.DataSet.FieldByName('closed').AsBoolean := True;
    WorkOrderDS.DataSet.FieldByName('closed_date').AsDateTime := Now;
  end;

  WorkOrderDS.DataSet.FieldByName('status_date').AsDateTime := Now;
  WorkOrderDS.DataSet.FieldByName('statused_how').AsString := FStatusedHow;
  WorkOrderDS.DataSet.FieldByName('statused_by_id').AsInteger := DM.EmpID;
  PostDataSet(WorkOrderDS.DataSet);
end;

procedure TWOOHMFrame.CancelWorkOrder;
begin
  inherited;
  DM.WorkOrderInspection.Cancel;
end;

function TWOOHMFrame.ValidateWorkOrderFields: Boolean;
const
  MsgReq = ' is required to save work order.';
  MsgGasLampReq = ' is required to close work order.';
  MsgMustBeForCGA = ' must be set to %s when Completion Code is %s';
  MsgNoAOCObReq = ' No Inspection items are checked, please check "%s" to continue.'; //WorkTypeNOAOC
  MSGNoAOCObClear = ' Inspection items are checked, please clear the "%s" checkbox to continue.';  //WorkTypeNOAOC
  MSGCGAClearAll = ' %s Completion Code is selected so all Inspection items must be unchecked to continue. These items must be unchecked: %s';//CGAStatus
var
  AOCMessage: string;
  IsCGA: Boolean;
  IsReadyToWork: Boolean;

  function ValidateInspectionCheckboxes: Boolean;
  var
    NOAOCisChecked: Boolean;
    CheckedItemList: TOdStringList;
    ItemsAreChecked: Boolean;
    NoAOCIdx: Integer;
  begin
    //Started to add a class helper here (i.e. IndexOfDisplayName), but the underlying classes
    //do not support it as DisplayName is not used as expected (i.e. always returns Text)
    NoAOCIdx := AOCInspectionCheckList.Items.IndexOf(WorkTypeNOAOC);
    if NoAOCIdx = -1 then begin
      Result := True;
      Exit; //NOAOC work type is not present, so validation rules cannot be applied.
    end;
    NoAOCisChecked := AOCInspectionCheckList.items[NoAOCIdx].Checked;

    CheckedItemList := TOdStringList.Create;
    try
      if InadequateVentClearance.Checked then
        CheckedItemList.Add(InadequateVentClearance.Caption);
      if InadequateVentClearanceIg.Checked then
        CheckedItemList.Add(InadequateVentClearanceIg.Caption);
      AOCInspectionChecklist.AddCheckedItemsToList(CheckedItemList);
      CustomerInspectionChecklist.AddCheckedItemsToList(CheckedItemList);
      PotentialHazardChecklist.AddCheckedItemsToList(CheckedItemList);
      if NOAOCisChecked and (not IsCGA)then
        RemoveStringFromList(WorkTypeNOAOC,CheckedItemList);
      ItemsAreChecked := (CheckedItemList.Count > 0);

      if IsCGA then begin
       //if CGA completion code is selected - then NO checkboxes at all can be checked.
       Result := (not ItemsAreChecked) and (not NOAOCisChecked);
       AOCMessage := Format(MSGCGAClearAll,[CGAStatus, CheckedItemList.CommaText]);
      end
      else begin
       if not ItemsAreChecked then begin
         //if no checkbox is checked- then "No AOC Observed" must be checked.
         Result := NOAOCisChecked;
         AOCMessage := Format(MsgNoAOCObReq,[WorkTypeNOAOC]);
       end
       else begin
         //if any checkbox is checked- then "No AOC Observed" must be UNchecked/cleared.
         Result := not NOAOCisChecked;
         AOCMessage := Format(MSGNoAOCObClear,[WorkTypeNOAOC]);
       end;
      end;
    finally
      FreeAndNil(CheckedItemList);
    end;
  end;

begin
  IsReadyToWork := (CompletionCodeCombobox.Text = ReadyToWorkStatus);
  Result := IsReadyToWork;
  IsCGA := (CompletionCodeCombobox.Text = CGAStatus);

  if (not ISReadyToWork) then begin //No need to validate -R status because responses are not generated for this status
    if (not ValidateInspectionCheckboxes) then
      RaiseErrorOnFrame(AOCInspectionLabel.Caption + AOCMessage, AOCInspectionCheckList)
    else if IsEmpty(CompletionCodeCombobox.Text) then
      RaiseErrorOnFrame(CompletionCodeLabel.Caption + MsgReq, CompletionCodeCombobox)
    else if IsCGA and IsEmpty(CGAReasonCombo.Text) then
      RaiseErrorOnFrame('When Completion Code is CGA, ' + CGAReasonLabel.Caption + MsgReq, CompletionCodeCombobox)
    else if (InadequateVentClearance.Checked and IsEmpty(VentClearanceDistCombobox.Text)) then
      RaiseErrorOnFrame(InadequateVentClearance.Caption + MsgReq, VentClearanceDistCombobox)
    else if (InadequateVentClearanceIg.Checked and IsEmpty(VentIgnitionDistComboBox.Text)) then
      RaiseErrorOnFrame(InadequateVentClearanceIg.Caption + MsgReq, VentIgnitionDistComboBox)
    else if not CurbValueFoundAnswered and (fOHMtype = 'Premise') then
      RaiseErrorOnFrame('Curb Value Found must be answered.', cmbxCurbValueFound)
    else
      Result := True;
  end;
end;

procedure TWOOHMFrame.VentClearanceDistComboboxEditing(Sender: TObject; var CanEdit: Boolean);
begin
  inherited;
  SetRemedyEditing;
end;

procedure TWOOHMFrame.WorkTypesFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);  //QMANTWO-446 EB
var
  RowSource: string;
begin
  inherited;
  Assert(Assigned(DataSet));
  RowSource := WorkTypes.FieldByName('wo_source').AsString;
  Accept := WorkTypeisValidForWOSource(RowSource);
end;

procedure TWOOHMFrame.SetRemedyEditing;
begin
  EditDataSet(WorkOrderDS.DataSet);
end;

procedure TWOOHMFrame.SetInheritedFrameEvents;
begin
  inherited;
  //Hook any dataset, etc events; inherited frames will not fire them.
  StatusList.OnFilterRecord := StatusListFilterRecord; //todo refactor to base
  DM.Engine.LinkEventsAutoInc(DM.WorkOrderRemedy);
end;

procedure TWOOHMFrame.setOHMtype(const Value: string);   //QMANTWO-393
begin
  tsPremise.TabVisible := False;
  tsPremise.Visible := False;
  tsGasMain.TabVisible := False;
  tsGasMain.Visible := False;


  fOHMtype := Value;
  if fOHMtype = 'Premise' then begin
    pcOHM.ActivePage:=tsPremise;
    tsPremise.Visible := True;
    PremiseIDLabel.caption := 'PSID: '
  end
  else
  if fOHMtype = 'GasMain' then  begin
    pcOHM.ActivePage:=tsGasMain;
    tsGasMain.Visible := True;
    PremiseIDLabel.caption := 'GSID: '
  end;
  PremiseIDLabel.Visible := true;
end;

end.
