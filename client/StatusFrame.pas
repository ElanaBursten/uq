unit StatusFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, dateUtils,
  Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls,
  ComCtrls, StdCtrls, Mask, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxCore, cxDateUtils,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBLookupComboBox, EmployeeSelectFrame,
  QMConst, CodeLookupList;

type
  TacControl = (acFreeForm, acPhone, acDate, acDateTime, acCheckBox, acLocDropDown, acFirstLast, acCustomDropDown); //QMANTWO-259

  TStatFrame = class(TFrame)
    pnlQuestion: TPanel;
    pnlAnswer: TPanel;
    Splitter1: TSplitter;
    btnClearIt: TButton;
    CountLbl: TLabel;
    edtQuestion: TMemo;
    procedure btnClearItClick(Sender: TObject);
    procedure EdtAnswerChange(Sender: TObject);
    procedure EdtAnswerExit(Sender: TObject);
    procedure DTPickerRestrictionExit(Sender: TObject);
    procedure DateTPickerRestrictionExit(Sender: TObject);
  private
    edtAnswer: TEdit;
    edtFirst: TEdit;
    edtLast: TEdit;
    cbYesNo: TCheckBox;
    edtPhone: TMaskEdit;
    DTPicker: TcxDateEdit;
    DatePicker: TcxDateEdit;
    TechList : TEmployeeSelect;
    cmboCustom: TComboBox; {QMANTWO-350}
    QuestionLookupList: TCodeLookupList;  {QMANTWO-350}
    fControl: TacControl;
    fTicketID : integer;
    fInfoType: string;
    fCallCenter: string;
    fPrevLocatorID: integer;
    fPresetValue: string;
    RestrictionInt: Integer;   {QM-389}
    procedure deductCountKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    function GetAnswer: string;
    function GetNewLocID: Integer;
    function GetAnswerID: integer;
    function GetQuestion: string;
  public
    Complete: Boolean;
    property Answer: String read GetAnswer;
    property AnswerID: integer read GetAnswerID;
    property NewLocID: Integer read GetNewLocID;
    property ControlType: TacControl read fControl;
    property InfoType: String read fInfoType;
    property TicketID: integer read fTicketID;
    property PresetValue: string read fPresetValue write fPresetValue;
    property Question: string read GetQuestion;
//    constructor Create(Question: String; acControl: TacControl; aTicketID: integer; aInfoType: String; aCallCenter: String = '');
//      reintroduce; overload;
    constructor Create(Question: String; acControl: TacControl; aTicketID: integer; aInfoType: String; PreLocID: integer; aCallCenter: String = ''); reintroduce; overload;
    destructor Destroy;
    function GetCCManager: integer;
    procedure LoadPresetValue(Value: string);
    procedure Clear;
    function HasRestriction: boolean; {QM-389}
    function ValidRestrictionInput(Sender: TObject): boolean;  {QM-389}
    function ValidRestriction: boolean; {QM-389}

  end;

const
  DEFWIDTH = 172;
  FNAMEWIDTH = 152;
  LNAMEWIDTH = 152;
  DEFHEIGHT = 15;
  MAXSIZE = 40;
  PHONEMASKEXT = '!\(000\)000-0000 \ext. 999999'; //QM-28 EB Mask changed for simplification
  EXT = ' ext.';  //QM-28  SR
  DATEFORMAT = 'mm/dd/yyy tt';
  DATEONLYFORMAT =  'mm/dd/yyy'; //QMANTWO-491
  EMPTY_ANSWER = '--';
  EMPTY_DATETIME = ' /  /       :  :';
  EMPTY_DATE = ' /  /    ';


implementation
uses DMu, OdMiscUtils, StrUtils, LocalEmployeeDMu;
{$R *.dfm}
{ TquestionFrame }

procedure TStatFrame.btnClearItClick(Sender: TObject);
begin
  Clear;
end;

function TStatFrame.HasRestriction: boolean;  {QM-389}
var
  lowercasequestion: string;
  NumStr: string;
  startpos, endpos, TempNum: integer;

begin
  Result := False;
  TempNum := 0;
  NumStr := '';
  RestrictionInt := 0;
  lowercasequestion := lowercase(edtQuestion.Text);

   {Day Limit}
   if (ContainsText(lowercasequestion, '[5 day limit]')) then
     RestrictionInt := 5
   else if (ContainsText(lowercasequestion, '[10 day limit]')) then
     RestrictionInt := 10
   else if (ContainsText(lowercasequestion, '[15 day limit]')) then
     RestrictionInt := 15
   else if (ContainsText(lowercasequestion, '[20 day limit]')) then
     RestrictionInt := 20

   {Other day limit amounts}  //QM-604 EB Nipsco
   else if (ContainsText(lowercasequestion,'[')) and (ContainsText(lowercasequestion,'day limit]')) then begin
     endpos := pos(']', lowercasequestion);
     startpos := pos('[', lowercasequestion);

     NumStr := Copy(lowercasequestion, startpos, endpos);
     NumStr := ReplaceStr(NumStr,'[', '');
     NumStr := ReplaceStr(NumStr, 'day limit]', '');
     NumStr := Trim(NumStr);
     if TryStrToInt(NumStr, TempNum) then
       RestrictionInt := TempNum;
   end
   else
     RestrictionInt := 0;

   if RestrictionInt <> 0 then
     Result := True;
end;

procedure TStatFrame.Clear;
begin
  case fControl of
    acFreeForm: begin
      edtAnswer.Text := '';
      Countlbl.Caption := '0';
    end;
    acPhone: edtPhone.Text := '';
    acDate: DatePicker.Text := '';
    acDateTime:  DtPicker.Text := '';
    acFirstLast :
    begin
      edtFirst.Text := '';
      edtLast.Text  := '';
      Countlbl.Caption := '';
    end;
  end;
end;

constructor TStatFrame.Create(Question: String; acControl: TacControl; aTicketID: integer;
                              aInfoType: String; PreLocID: Integer; aCallCenter: String = '');
var
  ccManagerID: Integer;
  MaxDate: TDateTime;
begin
  inherited Create(Owner);
  fTicketID := aTicketid;
  fControl := acControl;
  fInfoType := aInfoType;
  fCallCenter := aCallCenter;
  fPrevLocatorID := PreLocID;
  edtQuestion.WordWrap := True;  {QM-389}

  if length(edtQuestion.Text) > 130 then  {QM-389}
    edtQuestion.ScrollBars := ssVertical;

  edtQuestion.Text := Question;

  Complete := False;

  case acControl of
    acFreeForm:
      begin
        edtAnswer := TEdit.Create(Self);
        btnClearIt.Visible := True;
        Countlbl.Parent := edtAnswer;
        Countlbl.Visible := True;
        with edtAnswer do
        begin
          Name := 'edtAnswer';
          Parent := pnlAnswer;
          Left := 1;
          Top := 1;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          MaxLength := MAXSIZE;
          Align := alClient;
          TabOrder := 0;
          edtAnswer.OnChange := EdtAnswerChange;
          edtAnswer.OnExit := EdtAnswerExit;
          Text := '';
        end;
      end;

    acPhone:
      begin
        edtPhone := TMaskEdit.Create(Self);
        btnClearIt.Visible := True;
        with edtPhone do
        begin
          Name := 'edtPhone';
          Parent := pnlAnswer;
          Left := 1;
          Top := 1;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          Align := alClient;
          EditMask := PHONEMASKEXT;// QM-28
          MaxLength := 31;
          TabOrder := 0;
          Text := '';  //QM-28 EB Do not set to mask
        end;
      end;

    acDate:
      begin
        DatePicker := TcxDateEdit.Create(Self);
        with DatePicker do
        begin
          Name := 'DatePicker';
          Parent := pnlAnswer;
          properties.Kind := ckDate;
          properties.DateButtons:= [btnClear,btnToday];
          Properties.DisplayFormat :=  DATEONLYFORMAT; //QMANTWO-491
          Date := Tomorrow;  //QMANTWO-491
          if HasRestriction then begin            //QM-389
            MaxDate := Now + RestrictionInt;
            Date := MaxDate;
            DatePicker.Style.Font.Color := clMaroon;
            DatePicker.Properties.OnCloseUp := DateTPickerRestrictionExit;
          end;
          properties.SaveTime := False;
          Left := 1;
          Top := 1;
          TabOrder := 0;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          Align := alClient;
        end;
      end;

    acDateTime:
      begin
        DTPicker := TcxDateEdit.Create(Self);
        with DTPicker do
        begin
          Name := 'DTPicker';
          Parent := pnlAnswer;
          properties.Kind := ckDateTime;
          properties.ShowTime := True;
          properties.DateButtons:= [btnClear,btnNow,btnToday];
          Properties.DisplayFormat :=  DATEFORMAT;
          if HasRestriction then begin  //QM-389
            MaxDate := Now + RestrictionInt;
            Date := MaxDate;
            DtPicker.Style.Font.Color := clMaroon;
            DTPicker.Properties.OnCloseUp := DTPickerRestrictionExit;
          end;

          properties.SaveTime := true;
    
          Left := 1;
          Top := 1;
          TabOrder := 0;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          Align := alClient;

        end;
      end;

    acCheckBox:
      begin
        cbYesNo := TCheckBox.Create(Self);
        with cbYesNo do
        begin
          Name := 'cbYesNo';
          Parent := pnlAnswer;
          Left := 1;
          Top := 1;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          Align := alClient;
          Caption := 'Check if True';
          TabOrder := 0;

        end;
      end;

    //Currently, only applies to CO:FCO1.  acLocDropDown will only be used for location lookup
    acLocDropDown:
      begin
        try
         Techlist := TEmployeeSelect.Create(Self);
         ccManagerID := GetCCManager;
         Techlist.Initialize(esAllActiveEmployeesUnderManager{esAll}, DM.EmpID, ccManagerID);
         Techlist.SelectID(fPrevLocatorID);
        with techList do
          begin
            Name := 'techList';
            Parent := pnlAnswer;
              Left := 1;
              Top := 1;
              Width := DEFWIDTH;
              Height := DEFHEIGHT;
              Align := alClient;
              TabOrder := 0;
         end;
        except on E:Exception do
          MessageDlg('Unable to load Employee List -' + E.Message, mtError, [mbOK], 0);
        end;
      end;

    acFirstLast:       //QMANTWO-259
      begin
        edtFirst := TEdit.Create(Self);
        edtLast := TEdit.Create(Self);
        Countlbl.Parent := pnlQuestion;
        Countlbl.Width := DEFWIDTH;
        Countlbl.Align := alright;
        Countlbl.Visible := True;
        Countlbl.Transparent := true;
        Countlbl.caption := '';
        btnClearIt.Visible := True;
        with edtFirst do
        begin
          Name := 'edtFirst';
          Parent := pnlAnswer;
          Top := 1;
          Width := FNAMEWIDTH;
          Height := DEFHEIGHT;
          MaxLength := MAXSIZE;
          Align := alLeft;
          TabOrder := 1;
          Hint := 'First Name';
          ShowHint := True;
          Text := '';
          OnKeyUp :=  DeductCountKeyUp;
        end;
        with edtLast do
        begin
          Name := 'edtLast';
          Parent := pnlAnswer;
          Top := 1;
          Width := LNAMEWIDTH;
          Height := DEFHEIGHT;
          MaxLength := MAXSIZE;
          Align := alLeft;
          TabOrder := 2;
          Hint := 'Last Name';
          ShowHint := True;
          Text := '';
          OnKeyUp :=  DeductCountKeyUp;
        end;
        btnClearIt.Align := alLeft;
      end;

    acCustomDropDown:   //QMANTWO-350 Custom DropDown
      begin
        cmboCustom := TComboBox.Create(Self);
        with cmboCustom do
        begin
          Name := 'cmboCustom';
          cmboCustom.text := '--';
          cmboCustom.Style := csDropDownList;
          Parent := pnlAnswer;
          Left := 1;
          Top := 1;
          Width := DEFWIDTH;
          Height := DEFHEIGHT;
          Align := alClient;
          TabOrder := 0;
          if not DM.GetRefLookupList(aInfoType, cmboCustom.Items, True) then begin
            DM.GetRefLookupList(LowerCase(aInfoType), cmboCustom.Items, True);     //QM-933 EB Sort Order
          end;
        end;
      end;
    end;
end;



procedure TStatFrame.deductCountKeyUp(Sender: TObject; var Key: Word;   //QMANTWO-259
  Shift: TShiftState);
const
  REMAIN = '  %d characters remaining';
var
  cnt : integer;
  len : integer;
  actEdit : Tedit;
begin
  cnt := MAXSIZE- (length(edtLast.text)+ length(edtFirst.text));
  Countlbl.Caption := format(REMAIN,[cnt-1]);   // +1 for the space
  if cnt < 1 then
  begin
    Countlbl.Caption := 'All characters used!';
    actEdit := (Sender as Tedit);
    len := length(actEdit.text);
    actEdit.Text := RightStr(actEdit.Text, len-1);
  end;
end;

destructor TStatFrame.Destroy;
begin
  if Assigned(techList) then
    FreeAndNil(techList);
  if Assigned(QuestionLookupList) then   {QMANTWO-350}
    FreeAndNil(QuestionLookupList);
end;


procedure TStatFrame.DateTPickerRestrictionExit(Sender: TObject); //QM-389
var
  IsValid: boolean;
begin
  IsValid := ValidRestrictionInput(DatePicker);
  if not IsValid then
    DatePicker.Style.Font.Color := clMaroon;
end;


procedure TStatFrame.DTPickerRestrictionExit(Sender: TObject);   //QM-389
var
  IsValid: boolean;
begin
  IsValid := ValidRestrictionInput(DtPicker);
  if not IsValid then
    DtPicker.Style.Font.Color := clMaroon;
end;

procedure TStatFrame.EdtAnswerChange(Sender: TObject);
var
  Count: integer;
begin
  if (Sender is TEdit) then begin
    if TEdit(Sender).Focused then begin
      Count := MAXSIZE - Length(TEdit(Sender).Text);
      case Count of
        40: CountLbl.Caption := '';
        0: CountLbl.Caption := EMPTY_ANSWER;
        else
          CountLbl.Caption := IntToStr(Count);
      end;
      CountLbl.Visible := True;
    end
    else
      CountLbl.Visible := False;
  end;

end;

procedure TStatFrame.EdtAnswerExit(Sender: TObject);
begin
  CountLbl.Visible := False;
end;

function TStatFrame.GetAnswer: string;
var
  ExtPos: integer;
  ExtStr: string;
  PhoneStr: string;
begin
  try
    case fControl of
    acCheckBox:
      begin
        If cbYesNo.Checked then
          Result := 'TRUE'
        else
          Result := 'FALSE'
      end;
    acDate: begin
      Result := DatePicker.Text;
    end;

    acDateTime: begin
        Result := DTPicker.Text;
    end;

    acPhone:
      begin
        Result := Trim(edtPhone.Text);

        // Elana - Change to add extension (or trim off if it is not used)
        ExtPos := Pos(EXT, edtPhone.Text);
        PhoneStr := Trim(copy(edtPhone.Text, 0, ExtPos-1));
        if ExtPos > 0 then begin
          ExtStr := Trim(Copy(edtPhone.Text, ExtPos + length(EXT), Length(edtPhone.Text)));
          if ExtStr = '' then
            Result := PhoneStr;
        end;
        {Check the main part of the string to see if it is viable}
        PhoneStr := StringReplace(PhoneStr, ' ', '', [rfReplaceAll]);
		if length(PhoneStr) < 13  then
          Result := EMPTY_ANSWER;
      end;

    acFreeForm:
        Result := edtAnswer.Text;

    acLocDropDown:
        Result := trim(techList.SelectedText);

    acFirstLast:
        Result := edtFirst.Text + ' ' + edtLast.Text;   //QMANTWO-259

    acCustomDropDown: //QMANTWO-350
       Result := cmboCustom.Text;
    end;

    if (Result = '') then Result := EMPTY_ANSWER;

  except
    Result := EMPTY_ANSWER;
    raise Exception.Create('Unable to retrieve answer for ' + '''' + edtQuestion.Text + '''');
  end;
end;

function TStatFrame.GetAnswerID: integer;
begin
  case fControl of
    acLocDropDown:
      Result := techList.SelectedID;

    acFreeForm,acPhone,acDate,acDateTime,acCheckBox:
      Result := -1;  //means nothing
  end;
end;

function TStatFrame.GetNewLocID: Integer;
begin
   if fControl = acLocDropDown then
 //    Result := integer(Techlist.Properties.Items.Objects[techlist.ItemIndex]);
     Result := Techlist.SelectedID
   else
     Result := -1;  // Doesn't apply to this question
end;

function TStatFrame.GetQuestion: string;
begin
  Result := edtQuestion.Text;
end;

procedure TStatFrame.LoadPresetValue(Value: string);
var
  valDate: TDate;
begin
  fPresetValue := Value;
  if (Value = '') and (fControl <> acDate) then
    exit;
  try
    case fControl of
      acCheckBox:
        cbYesNo.Checked := StrToBool(Value);

      acDate:
       begin
         valDate := TODAY;
         case DayOfWeek(valDate) of
          1..5: DatePicker.Date :=  valDate + 1;  // Sun thru Thur
             6: DatePicker.Date :=  valDate + 3;  // Fri
             7: DatePicker.Date :=  valDate + 2;  // Sat
         end;
       end;

      acDateTime:
        DTPicker.Text := Value;

      acPhone:
        edtPhone.Text := Value;

      acFreeForm:
        edtAnswer.Text := Value;

    end;
  except
    //swallow - blank value is fine if the preset value is not valid
  end;
end;



function TStatFrame.ValidRestriction: boolean;  {QM-389}
begin
  If not HasRestriction then begin
    Result := True;
    Exit;
  end;

  Result := False;
  if fControl = acDate then
    Result := ValidRestrictionInput(DatePicker)
  else if fControl = acDateTime then
    Result := ValidRestrictionInput(DTPicker)
end;

function TStatFrame.ValidRestrictionInput(Sender: TObject): boolean;  {QM-389}
var
  SelectedDate: TDateTime;
  RestrictDate: TDateTime;
begin
  Result := False;
  if HasRestriction then begin
    if Sender is TcxDateEdit then begin
      RestrictDate := Now + RestrictionInt;
      SelectedDate := TcxDateEdit(Sender).Date;
      if (SelectedDate > (RestrictDate)) or (SelectedDate < Now) then
        ShowMessage('The selected date must be between now and ' + DateTimeToSTr(RestrictDate) + '.')
      else
        Result := True;
    end;
  end
end;

// Added only for CO. Need to find a better way to associate Profit Center and Call Center
function TStatFrame.GetCCManager: integer;
begin
  if (fCallCenter = 'FCO1') or (fCallCenter = 'FCO2') then
 //   Result := 4903;  {Mark Hinshaw}
    Result := EmployeeDM.GetManagerIDForEmp(18522) //This will pull up whoever is assigned as the 150 Manager
  else
    Result := DM.EmpID;
end;

end.

