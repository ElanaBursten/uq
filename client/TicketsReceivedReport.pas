unit TicketsReceivedReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls, OdRangeSelect, CheckLst;

type
  TTicketsReceivedReportForm = class(TReportBaseForm)
    RangeSelect: TOdRangeSelectFrame;
    Label1: TLabel;
    Label3: TLabel;
    ClientList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    ManagersComboBox: TComboBox;
    Label4: TLabel;
    IncludeAll: TCheckBox;
    TicketTypeBox: TCheckBox;
    CallCenterList: TCheckListBox;
    Label5: TLabel;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure IncludeAllClick(Sender: TObject);
    procedure CallCenterListClickCheck(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdVclUtils, OdExceptions, ODMiscUtils;

{ TTicketsReceivedReportForm }

procedure TTicketsReceivedReportForm.ValidateParams;
var
  CheckedItems: string;
  CallCenterList: string;
begin
  inherited;
  //Note: Both the stored procedure used by this report and the report itself
  //can handle multiple call centers, although the label at the top of the
  //report form needs to be changed or removed. Other than that, the only change
  //needed to allow the report to work for multiple call centers is to change
  //the combo box on this form to a multiple-choice list box.

  CallCenterList := GetCheckedItemCodesString(Self.CallCenterList);
  if IsEmpty(CallCenterList) then
    raise EOdEntryRequired.Create('Please select a call center.');

  if IncludeAll.Checked then CheckedItems := '*'
  else CheckedItems := GetCheckedItemString(ClientList, False);

  SetReportID('TicketsReceived');
  SetParamDate('StartDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);
  SetParam('CallCenterList', CallCenterList);
  SetParam('ClientList', CheckedItems);
  SetParamIntCombo('ManagerID', ManagersComboBox);

  if TicketTypeBox.Checked then
    SetParam('GroupByTicketType', '1')
  else
    SetParam('GroupByTicketType', '0')
end;

procedure TTicketsReceivedReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  SetUpManagerList(ManagersComboBox);
  IncludeAll.Checked := True;
  IncludeAllClick(Self);
  TicketTypeBox.Checked := False;
  //CallCenterCombo.ItemIndex := 0;
end;

procedure TTicketsReceivedReportForm.SelectAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, True);
end;

procedure TTicketsReceivedReportForm.ClearAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, False);
end;

procedure TTicketsReceivedReportForm.IncludeAllClick(Sender: TObject);
var
  ClientSelectionEnabled: Boolean;
begin
  ClientSelectionEnabled := not IncludeAll.Checked;
  ClientList.Enabled := ClientSelectionEnabled;
  SelectAllButton.Enabled := ClientSelectionEnabled;
  ClearAllButton.Enabled := ClientSelectionEnabled;
end;

procedure TTicketsReceivedReportForm.CallCenterListClickCheck(
  Sender: TObject);
var
  i: Integer;
  CallCenterClientList: TStringList;
  CC: string;
begin
  CallCenterClientList := TStringList.Create;
  ClientList.Items.BeginUpdate;
  ClientList.Items.Clear;
  try
    for i := 0 to CallCenterList.Count - 1 do
      if CallCenterList.Checked[i] then begin
        CC := FCallCenterList.GetCode(CallCenterList.Items[i]);
        DM.ClientList(CC, CallCenterClientList);
        ClientList.Items.Text := ClientList.Items.Text + CallCenterClientList.Text;
      end;
   finally
     ClientList.Items.EndUpdate;
     CallCenterClientList.Free;
   end;
end;

end.
