unit Progress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls;

type
  TProgressFrame = class(TFrame)
    MajorProgressLabel: TLabel;
    MajorProgressBar: TProgressBar;
    MinorProgressLabel: TLabel;
    MinorProgressBar: TProgressBar;
  private
    procedure Refresh;
  public
    procedure Initialize;
    procedure ShowMinorProgress(const Text: string; PctComplete: Integer = -1);
    procedure ShowMajorProgress(const Text: string; PctComplete: Integer = -1);
  end;

implementation

{$R *.dfm}

procedure TProgressFrame.Initialize;
begin
  MinorProgressBar.Max := 100;
  MinorProgressBar.Position := 0;
  MinorProgressBar.Step := 1;
  MajorProgressBar.Max := 100;
  MajorProgressBar.Position := 0;
  MajorProgressBar.Step := 1;
end;

procedure TProgressFrame.ShowMajorProgress(const Text: string; PctComplete: Integer);
begin
  MajorProgressLabel.Caption := Text;
  if PctComplete > -1 then
    MajorProgressBar.Position := PctComplete;
  Refresh;
end;

procedure TProgressFrame.ShowMinorProgress(const Text: string; PctComplete: Integer);
begin
  MinorProgressLabel.Caption := Text;
  if PctComplete > -1 then
    MinorProgressBar.Position := PctComplete;
  Refresh;
end;

procedure TProgressFrame.Refresh;
begin
  Self.Repaint;
  if Self.HasParent and (Parent is TForm) then
    TForm(Parent).Repaint;
end;

end.
