unit DamageIDExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls;

type
  TDamageIDExceptionReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    StartDamageIDEdit: TEdit;
    EndDamageIDEdit: TEdit;
    Label3: TLabel;
    Label4: TLabel;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  OdExceptions;

{ TDamageIDExceptionReportForm }

procedure TDamageIDExceptionReportForm.InitReportControls;
begin
  inherited;

end;

procedure TDamageIDExceptionReportForm.ValidateParams;
begin
  inherited;
  SetReportID('DamageIDException');

  if (StartDamageIDEdit.Text='') or (EndDamageIDEdit.Text='') then
    raise EOdEntryRequired.Create('Start and end of Damage Number Range are required.');

  SetParamInt('StartDamageID', StrToInt(StartDamageIDEdit.Text));
  SetParamInt('EndDamageID', StrToInt(EndDamageIDEdit.Text));
end;

end.
