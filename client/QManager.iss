; Use Inno Setup 5.1.6+
#define Version GetEnv("VERSION")
#define GitCommitID GetEnv("GIT_COMMIT")
#define GitCommitIDShort copy(GitCommitID,1,7) 
#if GitCommitIDShort == ""
 #define GitCommitIDWithDash= "-CO"
#else
 #define GitCommitIDWithDash= "-" + GitCommitIDShort
#endif

[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager
AppVerName=UtiliQuest Q Manager {#Version} 
AppID=UtiliQuestQManager
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\Q Manager
DefaultGroupName=UtiliQuest
AppPublisher=UtiliQuest LLC
AppPublisherURL=http://www.utiliquest.com/
AppMutex=UtiliQuestQManager
AppVersion={#Version}{#GitCommitIDWithDash}
OutputDir=..\build
OutputBaseFilename=QManagerSetup-{#Version}{#GitCommitIDWithDash}
DisableProgramGroupPage=yes
PrivilegesRequired=none
OutputManifestFile=QManagerSetup-Manifest.txt
SetupLogging=yes

[Files]
Source: "QManager.exe";                           DestDir: "{app}"; Flags: ignoreversion
Source: "WebView2Loader.dll";                     DestDir: "{app}"; Flags: ignoreversion
Source: "libeay32.dll";                           DestDir: "{app}"; Flags: ignoreversion
Source: "ssleay32.dll";                           DestDir: "{app}"; Flags: ignoreversion
Source: "..\GeoCodeLib\GeoCode.dll";              DestDir: "{app}"; Flags: ignoreversion
Source: "..\common\DoUpdate.exe";                 DestDir: "{app}"; Flags: ignoreversion
Source: "Default.ini";                            DestDir: "{app}"; DestName: "QManager.ini"; Flags: onlyifdoesntexist uninsneveruninstall
Source: "..\StreetMapTickets\StreetMapProj.exe";  DestDir: "{app}"; Flags: ignoreversion
//Source: "..\StreetMapTickets\StreetMapTickets.html";  DestDir: "{app}"; Flags: ignoreversion
Source: "RoboCopyRunner.exe";                     DestDir: "{app}"; Flags: ignoreversion

;Quarter Minute Editor and support files    QMANTWO-272
Source: "..\QtrMinEditor\QtrMinuteGridProject\QtrMinuteGrid.exe"; DestDir: "{app}";  Flags: ignoreversion
Source: "..\QtrMinEditor\QtrMinuteGridProject\clientSettings.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\QtrMinEditor\QtrMinuteGridProject\colorFile.txt"; DestDir: "{app}"; Flags: onlyifdoesntexist uninsneveruninstall
Source: "..\QtrMinEditor\QtrMinuteGridProject\HTML\QtrMinGrid.htm"; DestDir: "{app}\HTML"; Flags: ignoreversion
Source: "..\QtrMinEditor\QtrMinuteGridProject\HTML\savedLocations.xml"; DestDir: "{app}\HTML"; Flags: onlyifdoesntexist 
Source: "..\QtrMinEditor\QtrMinuteGridProject\WebView2Loader.dll"; DestDir: "{app}"; Flags: ignoreversion


Source: "Print2PDF.exe"; DestDir: "{app}"; Flags: ignoreversion

Source: "..\PlatsParser\VerizonTiffsParser.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\PlatsParser\VerizonTiffsParser.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist

;Middleware to authenticate and manage AWS image migration
Source: "..\SharedSecureAttachments\DSSS.dll";  DestDir: "{app}";  Flags: ignoreversion


Source: "..\lib\rtl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclx100.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcliex100.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\dbrtl100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcldb100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclsmp100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclie100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcljpg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclimg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\adortl100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\midas.dll";     DestDir: "{sys}"; Flags: sharedfile
; DBISAM
;4.34 Build 7 is the current version to support XE2/3
Source: "..\lib\dbisamr434rsdelphi2007.bpl"; DestDir: "{sys}"; Flags: sharedfile
; TClientDataSet
Source: "..\lib\midas.dll";    DestDir: "{app}"
; New Developer Express
Source: "..\lib\DevExD2007RT1224.bpl"; DestDir: "{sys}"; Flags: sharedfile
; GPSTools
Source: "..\lib\GPSToolsXP230.dll"; DestDir: "{sys}"; Flags: sharedfile regserver

[Tasks]
Name: uq; Description: "UtiliQuest"; GroupDescription: "Company"; Flags: exclusive

[INI]
Filename: "{app}\QManager.ini"; Section: "Server"; Key: "URL"; String: "https://qmcluster.utiliquest.com/QMServer"
Filename: "{app}\QManager.ini"; Section: "Server"; Key: "PreviousURL"; String: "https://qmcluster.utiliquest.com/QMServer"
Filename: "{app}\QManager.ini"; Section: "Server"; Key: "BackupURL"; String: "https://qmcluster2.utiliquest.com/QMServer" 
Filename: "{app}\QManager.ini"; Section: "Server"; Key: "WebUpdateURL"; String: "https://qmcluster.utiliquest.com/QMUpdate/qm.ini"
Filename: "{app}\QManager.ini"; Section: "Server2"; Key: "URL"; String: "https://qmcluster.utiliquest.com:7000"; 
Filename: "{app}\clientSettings.ini"; Section: "ServerSettings"; Key: "port"; string: "443"; 


;These entries are neccessary to prevent GoogleMapsAPI JavaScript errors.   ;QMANTWO-272 QMANTWO-175
;[Registry] 
;Root: HKCU; Subkey: "Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION"; ValueType: dword; ValueName: "QtrMinuteGrid.exe"; ValueData: "11000" 
;Root: HKCU; Subkey: "Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION"; ValueType: dword; ValueName: "QManager.exe"; ValueData: "11000" 
;Root: HKCU; Subkey: "Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION"; ValueType: dword; ValueName: "StreetMapProj.exe"; ValueData: "11000" 

[Icons]
Name: "{group}\Q Manager"; Filename: "{app}\QManager.exe"

[Run]
Filename: "{app}\QManager.exe"; Description: "Start Q Manager now"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent unchecked

[Dirs]
Name: "{app}\Data"

[UninstallDelete]
Type: files; Name: "{app}\Data\*.*"
Type: files; Name: "{app}\*.bpl"
Type: files; Name: "{app}\QManager.log"

[Code]

const
  IEMinVersion = '6';
  MSXMLDOMDocument3GUID = '{f5078f32-c551-11d3-89b9-0000f81fe221}';

function GetEnvDef(Param: string): string;
begin
  Result := GetEnv('GIT_COMMIT');
end;

procedure ShowError(Msg: string);
begin
  MsgBox(Msg, mbError, MB_OK);
end;

function VerifyMSXML3Installed: Boolean;
begin
  Result := RegValueExists(HKCR, 'CLSID\' + MSXMLDOMDocument3GUID, '');
  if not Result then
    ShowError('This program requires the Microsoft XML Parser (MSXML) version 3.' +#13+#10+
              'Please install MSXML 3 and then re-run this setup.  Note that IE 6 installs this component.');
end;

function InitializeSetup: Boolean;
begin
  Result := VerifyMSXML3Installed;
end;

