inherited DamageLitigationSearchCriteria: TDamageLitigationSearchCriteria
  Left = 288
  Top = 207
  Caption = 'Damage Litigation Search'
  ClientHeight = 210
  ClientWidth = 590
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 23
    Top = 16
    Width = 60
    Height = 13
    Caption = 'File Number:'
  end
  object Label4: TLabel [1]
    Left = 23
    Top = 101
    Width = 40
    Height = 13
    Caption = 'Plaintiff:'
  end
  object Label5: TLabel [2]
    Left = 23
    Top = 129
    Width = 37
    Height = 13
    Caption = 'Carrier:'
  end
  object Label6: TLabel [3]
    Left = 23
    Top = 157
    Width = 47
    Height = 13
    Caption = 'Attorney:'
  end
  object Label7: TLabel [4]
    Left = 23
    Top = 185
    Width = 30
    Height = 13
    Caption = 'Party:'
  end
  object Label8: TLabel [5]
    Left = 284
    Top = 16
    Width = 43
    Height = 13
    Caption = 'Demand:'
  end
  object Label9: TLabel [6]
    Left = 284
    Top = 44
    Width = 39
    Height = 13
    Caption = 'Accrual:'
  end
  object Label10: TLabel [7]
    Left = 284
    Top = 72
    Width = 56
    Height = 13
    Caption = 'Settlement:'
  end
  object Label1: TLabel [8]
    Left = 23
    Top = 44
    Width = 57
    Height = 13
    Caption = 'Damage ID:'
  end
  inherited RecordsLabel: TLabel
    Left = 264
    Top = 158
    Width = 113
  end
  object Label3: TLabel [10]
    Left = 23
    Top = 73
    Width = 61
    Height = 13
    Caption = 'Litigation ID:'
  end
  inherited SearchButton: TButton
    Left = 236
    Top = 178
    TabOrder = 11
  end
  inherited CancelButton: TButton
    Left = 557
    Top = 177
    TabOrder = 14
  end
  inherited CopyButton: TButton
    Left = 316
    Top = 178
    TabOrder = 12
  end
  inherited ExportButton: TButton
    Left = 397
    Top = 178
    TabOrder = 13
  end
  inherited PrintButton: TButton
    TabOrder = 15
  end
  inherited ClearButton: TButton
    Left = 477
    Top = 177
    TabOrder = 16
  end
  inline ClosedDate: TOdRangeSelectFrame
    Left = 255
    Top = 95
    Width = 302
    Height = 54
    TabOrder = 10
    inherited FromLabel: TLabel
      Left = 29
      Top = -2
      Width = 65
      Height = 26
      Caption = 'Closed Date From:'
      WordWrap = True
    end
    inherited ToLabel: TLabel
      Left = 189
      Top = 5
    end
    inherited DatesLabel: TLabel
      Left = 29
      Top = 32
    end
    inherited DatesComboBox: TComboBox
      Left = 98
      Top = 28
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 98
      Top = 1
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 210
      Top = 1
    end
  end
  object FileNumber: TEdit
    Left = 88
    Top = 12
    Width = 143
    Height = 21
    TabOrder = 0
  end
  object PlaintiffSearch: TEdit
    Left = 88
    Top = 97
    Width = 143
    Height = 21
    TabOrder = 3
  end
  object Demand: TcxCurrencyEdit
    Left = 353
    Top = 12
    Properties.DisplayFormat = '0.00;(0.00)'
    Properties.MaxLength = 15
    Properties.UseDisplayFormatWhenEditing = True
    TabOrder = 7
    Width = 85
  end
  object Accrual: TcxCurrencyEdit
    Left = 353
    Top = 40
    Properties.DisplayFormat = '0.00;(0.00)'
    Properties.MaxLength = 15
    Properties.UseDisplayFormatWhenEditing = True
    TabOrder = 8
    Width = 85
  end
  object Settlement: TcxCurrencyEdit
    Left = 353
    Top = 68
    Properties.DisplayFormat = '0.00;(0.00)'
    Properties.MaxLength = 15
    Properties.UseDisplayFormatWhenEditing = True
    TabOrder = 9
    Width = 85
  end
  object PartySearch: TComboBox
    Left = 88
    Top = 181
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
  end
  object AttorneySearch: TComboBox
    Left = 88
    Top = 153
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
  end
  object UQDamageID: TEdit
    Left = 88
    Top = 40
    Width = 143
    Height = 21
    TabOrder = 1
  end
  object CarrierSearch: TComboBox
    Left = 88
    Top = 125
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
  end
  object LitigationID: TEdit
    Left = 88
    Top = 69
    Width = 143
    Height = 21
    TabOrder = 2
  end
end
