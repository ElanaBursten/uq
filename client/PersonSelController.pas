unit PersonSelController;

interface

uses
  Classes, SysUtils, StdCtrls;

type
  TPersonSelController = class
  private
    FManagerBox: TComboBox;
    FPersonBox: TComboBox;
    FEnabled: Boolean;
    FManagerID: Integer;
    FIncludeInactiveEmps: Boolean;
    FForceUpdate: Boolean;
    procedure ManagerBoxChange(Sender: TObject);
    procedure Initialize;
    procedure SetEnabled(Enabled: Boolean);
    procedure EnableDisablePersonBox;
    procedure SetIncludeInactiveEmps(const Value: Boolean);
  public
    constructor Create(AManagerBox, APersonBox: TComboBox);
    procedure FindManagerID;
    function WorkerID: Integer;
    procedure Validate;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property ManagerID: Integer read FManagerID;
    property IncludeInactiveEmps: Boolean read FIncludeInactiveEmps write SetIncludeInactiveEmps;
  end;

implementation

uses
  DMu, OdVclUtils, OdReportBase, QMConst, LocalEmployeeDMu;

const
  ChooseManagerText = '<Choose Manager>';
  ChoosePersonText = '<Choose Employee>';

{ TManagerPersonSelectionController }

constructor TPersonSelController.Create(AManagerBox,
  APersonBox: TComboBox);
begin
  FManagerBox := AManagerBox;
  FPersonBox := APersonBox;
  FManagerBox.OnChange := ManagerBoxChange;
  FForceUpdate := False;
  Initialize;
end;

procedure TPersonSelController.Initialize;
begin
  EmployeeDM.ManagerList(FManagerBox.Items);
  FManagerBox.Text := ChooseManagerText;
  FPersonBox.Text := ChoosePersonText;
  SetEnabled(False);
end;

procedure TPersonSelController.ManagerBoxChange(Sender: TObject);
var
  OldManagerID: Integer;
begin
  OldManagerID := FManagerID;
  FindManagerID;

  if FManagerID > 0 then begin
    if (OldManagerID <> FManagerID) or FForceUpdate then begin
      if FIncludeInactiveEmps then
        EmployeeDM.EmployeeList(FPersonBox.Items, FManagerID, StatusAll)
      else
        EmployeeDM.EmployeeList(FPersonBox.Items, FManagerID);
      FPersonBox.Text := ChoosePersonText;
      FForceUpdate := False;
    end;
  end else begin
    FPersonBox.Items.Clear;
  end;
  EnableDisablePersonBox;
end;

procedure TPersonSelController.FindManagerID;
begin
  FManagerID := -1;
  if FManagerBox.ItemIndex > -1 then
    FManagerID := GetComboObjectInteger(FManagerBox);
end;

procedure TPersonSelController.SetEnabled(Enabled: Boolean);
begin
  FEnabled := Enabled;
  FManagerBox.Enabled := FEnabled;
  EnableDisablePersonBox;
end;

procedure TPersonSelController.Validate;
begin
  RequireComboBox(FManagerBox, 'Please choose a manager');
  RequireComboBox(FPersonBox, 'Please choose an employee');
end;

function TPersonSelController.WorkerID: Integer;
begin
  Result := -1;
  if ManagerID > 0 then begin
    Result := GetComboObjectInteger(FPersonBox);
  end;
end;

procedure TPersonSelController.EnableDisablePersonBox;
begin
  FPersonBox.Enabled := FEnabled and (FManagerID > 0);
end;

procedure TPersonSelController.SetIncludeInactiveEmps(const Value: Boolean);
begin
  FIncludeInactiveEmps := Value;
  FForceUpdate := True;
  ManagerBoxChange(FManagerBox);
end;

end.
