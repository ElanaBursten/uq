unit NewTicketDetailReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls;

type
  TNewTicketReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ManagersComboBox: TComboBox;
    PrintNewRadio: TRadioButton;
    PrintAllRadio: TRadioButton;
    LocatorListBox: TListBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure PopulateLocators;
    procedure EmployeeStatusComboChange(Sender: TObject);
  protected
    FCurrentManager, FCurrentStatus: Integer;
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    function GetLocatorList: string;
  end;

implementation

uses
  DMu, OdExceptions, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TNewTicketDetailForm }

procedure TNewTicketReportForm.ValidateParams;
begin
  inherited;
  if LocatorListBox.SelCount = 0 then begin
    LocatorListBox.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one locator.');
  end;
  SetReportID('NewTicketDetail');
  SetParamInt('All', Ord(PrintAllRadio.Checked));
  SetParam('Locators', GetLocatorList);
end;

procedure TNewTicketReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  ManagersComboBox.ItemIndex := -1;
  FCurrentManager := -1;
  FCurrentStatus  := -1;
  LocatorListBox.Items.Clear;
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

function TNewTicketReportForm.GetLocatorList: string;
var
  i: Integer;

  procedure Add(ID: Integer);
  begin
    if Result = '' then
      Result := IntToStr(ID)
    else
      Result := Result + ',' + IntToStr(ID);
  end;

begin
  Result := '';
  for i := 0 to LocatorListBox.Count-1 do
    if LocatorListBox.Selected[i] then
      Add(Integer(LocatorListBox.Items.Objects[i]));
end;

//Populate Locators
procedure TNewTicketReportForm.PopulateLocators;
var
  ManagerID: Integer;
begin
  if (ManagersComboBox.ItemIndex <> FCurrentManager) or
   (GetComboObjectInteger(EmployeeStatusCombo) <> FCurrentStatus) then begin
    ManagerID := GetComboObjectInteger(ManagersComboBox);
    EmployeeDM.EmployeeList(LocatorListBox.Items, ManagerID, GetComboObjectInteger(EmployeeStatusCombo));
    LocatorListBox.SelectAll;
    LocatorListBox.TopIndex := 0;
    FCurrentManager := ManagersComboBox.ItemIndex;
    FCurrentStatus  := GetComboObjectInteger(EmployeeStatusCombo);
  end;
end;

procedure TNewTicketReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  PopulateLocators;
end;

procedure TNewTicketReportForm.EmployeeStatusComboChange(Sender: TObject);
begin
  inherited;
  PopulateLocators;
end;

end.
