unit LoginFU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ClipBrd,
  StdCtrls, Buttons, ExtCtrls;

type
  TLoginForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    EditUserName: TEdit;
    EditPassword: TEdit;
    LoginBtn: TButton;
    CancelBtn: TButton;
    AppLabel: TLabel;
    Label5: TLabel;
    ServerLabel: TLabel;
    Bevel2: TBevel;
    RememberBox: TCheckBox;
    VersionLabel: TLabel;
    LogoImage: TImage;
    ServerListCombo: TComboBox;
    HelpDeskContact: TLabel;
    btnPWReset: TBitBtn;    //qm-837 sr
    procedure LoginBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ServerListComboChange(Sender: TObject);
    procedure ServerListComboDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormActivate(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure btnPWResetClick(Sender: TObject);
  private
    cnt:integer; //qm-837 sr
    procedure FailIfUrlMissing;
    procedure LoadRememberedValues;
    procedure AutoLoginClipboard;
  protected
    procedure Loaded; override;
  end;

function LogIn: Boolean;

implementation

uses
  DMu, Registry, OdVclUtils, StateMgr, OdExceptions, OdHourglass,
  OdMiscUtils, Terminology, PasswordRules, ShellAPI;

{$R *.DFM}

function LogIn: Boolean;
begin
  Result := False;
  with TLoginForm.Create(nil) do
  try
    if ShowModal = mrOK then
      Result := True;
  finally
    Release;
  end;
end;

procedure TLoginForm.LoginBtnClick(Sender: TObject);
var
  Cursor: IInterface;
  PlainPassword: string;
  aValue:string;
 const
   ZPASTATE  = 'TUNNEL_FORWARDING';

function GetRegistryVars:string;
const
  MyKey = '\Software\Zscaler\App';
begin
  with TRegistry.Create do
    try
      RootKey := HKEY_CURRENT_USER;
      if OpenKey(MyKey, False) then
      begin
        Result := ReadString('ZPA_State');
        CloseKey;
      end;
    finally
      Free;
    end;
end;

begin
  IF not DM.UQState.DebugZScalerIgnore then begin   //QM-876 EB Cleanup
    aValue:=  GetRegistryVars;
    if not(ZPASTATE=aValue) then  //qm-872
//        raise ExqScalerOffline.Create    1045  no longer kick them out
        showmessage('ZScaler is not running. Please Contact the Help Desk to restore');
  end;

  if EditUserName.Text = '' then
    raise EOdEntryRequired.Create('Please specify a User ID to log in.');

  Cursor := ShowHourGlass;
  try
    LoginBtn.Enabled := False;
    try
      PlainPassword := EditPassword.Text;
      if (DM.UQState.DeveloperMode) and (PasswordIsHashed(EditPassword.Text)) then
        DM.Connect(EditUserName.Text, EditPassword.Text)  // Accept direct hash entry as password only in developer mode
      else
        DM.Connect(EditUserName.Text, GeneratePasswordHash(EditUserName.Text, EditPassword.Text));
    finally
      LoginBtn.Enabled := True;
    end;

    DM.UQState.RememberLoginData := RememberBox.Checked;
    if RememberBox.Checked then
      DM.UQState.PlainPassword := PlainPassword
    else
      DM.UQState.PlainPassword := '';

    ModalResult := mrOk;
  except
    on E: Exception do begin
    inc(cnt);      //qm-837 sr
    if cnt>2 then
    begin
        btnPWReset.Visible:=true;
        exit;
    end;

      if EditUserName.CanFocus then
        EditUserName.SetFocus;
      E.Message := 'Unable to log in: ' + E.Message;
      raise;
    end;
  end;
end;

procedure TLoginForm.FormShow(Sender: TObject);
var
  i: Integer;
begin
  if DM.UQState.BrandLocatorsInc then
    LoadBitmapResource(LogoImage, 'LOC_LOGO')
  else
    LoadBitmapResource(LogoImage, 'LOGO');

  FailIfUrlMissing;

  AppLabel.Caption := AppName;
  VersionLabel.Caption := 'Version ' + AppVersion + ' ' + DMu.AppVersionMarker;
  Caption := AppName + ' - Log In';
  HelpDeskContact.Caption := DMu.HelpDeskContact;
  if DM.ServerInClient then
    ServerLabel.Caption := 'SERVER_IN_CLIENT mode, see client\QMServer.ini'
  else
  begin     //QMANTWO-320
//     DM.UQState.ServerURL
  if StrContainsText('QM-Dev', DM.UQState.ServerURL) then  //QM-155
  begin
    DM.ConnectedDB:='Test';     //QM-44   SR
    ServerLabel.Caption := 'Connected to Test Database';
  end
  else
  if StrContainsText('QMCluster', DM.UQState.ServerURL) then
  begin
    DM.ConnectedDB:='Prod';    //QM-44   SR
    ServerLabel.Caption := 'Connected to Production Database';
  end
  else
   ServerLabel.Caption :=  DM.UQState.ServerURL;
  end;

  if (not DM.ServerInClient) and DM.UQState.DeveloperMode and (DM.UQState.ServerUrls.Count > 0) then begin
    ServerListCombo.Visible := True;
    ServerListCombo.Items.Assign(DM.UQState.ServerUrls);
    for i := 0 to DM.UQState.ServerUrls.Count - 1 do begin
      if Pos(UpperCase(DM.UQState.ServerURL), UpperCase(DM.UQState.ServerUrls[i])) > 0 then
        ServerListCombo.ItemIndex := i;
    end;
  end;

  RememberBox.Visible := DM.UQState.AllowRememberedLogin;
  LoadRememberedValues;
end;

procedure TLoginForm.btnPWResetClick(Sender: TObject);
const   //qm-837 sr
  URL_PROD = 'https://uqview.utiliquest.com/QMPR/ForgotPassword.aspx'; //qm-1030  sr
  URL_DEV  = 'https://uqview-dev.utiliquest.com/QMPR/ForgotPassword.aspx'; //qm-1030  sr
begin
  if DM.ConnectedDB='Test' then
  ShellExecute(0,'open', PChar(URL_DEV), nil, nil, SW_SHOW)//qm-1030  sr
  else
  ShellExecute(0,'open', PChar(URL_PROD), nil, nil, SW_SHOW);   //qm-837 //qm-1030  sr
end;

procedure TLoginForm.CancelBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TLoginForm.FailIfUrlMissing;
begin
  if Trim(DM.UQState.ServerURL) = '' then
    raise Exception.Create('No server URL configured in ' + ExtractFileName(DM.UQState.IniFileName));
end;

procedure TLoginForm.LoadRememberedValues;
begin
  if DM.UQState.RememberLoginData then
  begin
    EditUserName.Text := DM.UQState.UserName;
    EditPassword.Text := DM.UQState.Password;
  end;
  RememberBox.Checked := DM.UQState.RememberLoginData or (EditUserName.Text <> '');
end;

procedure TLoginForm.ServerListComboChange(Sender: TObject);
begin
  DM.UQState.ServerURL := ServerListCombo.Items.Values[Copy(ServerListCombo.Text, 1, Pos('=', ServerListCombo.Text) - 1)];
  DM.DumpLocalData;
end;

procedure TLoginForm.ServerListComboDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  ServerName: string;
  ServerUrl: string;
begin
  with ServerListCombo do begin
    ServerName := Items.Names[Index];
    ServerUrl := Items.Values[ServerName];
    Canvas.TextRect(Rect, Rect.Left, Rect.Top, ServerUrl);
    Rect.Left := Rect.Left + Canvas.TextWidth(ServerUrl);
    Canvas.TextRect(Rect, Rect.Right - Canvas.TextWidth(ServerName) - 1, Rect.Top, ServerName);
  end;
end;

procedure TLoginForm.FormActivate(Sender: TObject);
begin
  AutoLoginClipboard;
  cnt:=0;   //qm-837 sr
end;

procedure TLoginForm.AutoLoginClipboard;
// This fills out the login information
// from the clipboard in a format like this
// QML-user-pass: QML-talvarez-zzz
var
  Clip: string;
begin
  if DM.UQState.DeveloperMode then begin
    Clip := Clipboard.AsText;
    if StrBeginsWith('QML-', Clip) then begin
      Delete(Clip, 1, 4);
      EditUserName.Text := Copy(Clip, 1, Pos('-', Clip) - 1);
      Delete(Clip, 1, Length(EditUserName.Text) + 1);
      EditPassword.Text := Clip;
    end;
  end;
end;

procedure TLoginForm.FormClick(Sender: TObject);
begin
  AutoLoginClipboard;
end;

procedure TLoginForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

end.
