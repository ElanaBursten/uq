unit LocalDamageDMu;

interface

uses
  SysUtils, Classes, dbisamtb, DB, Dmu, LocalEmployeeDMu, LocalPermissionsDMu, Dialogs,
  StdCtrls;

type
  TLDamageDM = class(TDataModule)
    OpenDamages: TDBISAMQuery;
    Damage: TDBISAMTable;
    DamageClaimant: TDBISAMQuery;
    DamageEstimate: TDBISAMTable;
    DamageLookup: TDBISAMTable;
    DamageProfitCenterRules: TDBISAMQuery;
    DamageChangeReasonLookup: TDBISAMQuery;
    RefDescForDamageCos: TDBISAMQuery;
    procedure DamageAfterInsert(DataSet: TDataSet);
    procedure DamageAfterOpen(DataSet: TDataSet);
    procedure DamageBeforeEdit(DataSet: TDataSet);
    procedure DamageBeforeOpen(DataSet: TDataSet);
    procedure DamageBeforePost(DataSet: TDataSet);
    procedure DamageEstimateAfterOpen(DataSet: TDataSet);
    procedure DamageEstimateBeforeOpen(DataSet: TDataSet);
    procedure DamageEstimateBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    function HaveDamagesRights: Boolean;

    {Lists}
    procedure GetDamageClaimantList(Claimants: TStrings; const DamageID: Integer; ClaimantType: Integer);
    procedure GetRequiredDamageDocumentList(DocumentTypes: TStrings; const LocateRequested: Boolean; const EstimateAmt: Currency);
    procedure PopulateDamageTypesList(List: TStrings);
    procedure DamageChangeReasonList(List: TStrings; IncludeBlank: Boolean=False);
    procedure InitDamageTypesListBox(DamageTypesListBox: TCustomListBox);

    {ID retrieval}
    function GetDamageIDForUQDamageID(const UQDamageID: Integer): Integer;
    function GetTicketIDForDamageID(const DamageID: Integer): Integer;
    function GetDamageIDForInvoiceID(const InvoiceID: Integer): Integer;

    {Update Cache}
    procedure UpdateDamageInCache(const DamageID: Integer);
    procedure UpdateDamageThirdPartyInCache(const ThirdPartyID: Integer);
    procedure UpdateDamageLitigationInCache(const LitigationID: Integer);

    {Priority}
    function GetDamagePriority(const DamageID: Integer): string;
    procedure SetDamagePriority(DamageID: Integer; PriorityRefID: Integer);

    {Modify/Approve}
    procedure SetDamageModified;
    procedure SetDamagePropertiesFromTicketIDField;
    procedure AddNewEstimateForDamage(DamageID: Integer; PaidAmount: Double; const Company, Comment: string);
    procedure ApproveDamage(DamageID: Integer; out ReturnedMsg: string);
    procedure SetDamageTicketAcknowledged(TicketID, InvestigatorID: Integer);

    procedure ArchiveDamageInvestigatorNarrative(const DamageID: Integer; const Narrative: string);

    {Get...}
    function GetCurrentDamageEstimateAmount(DamageID: Integer): Double;
    function GetDamageAccrualAmount(DamageID: Integer; InvoiceCode: string): Double;
    function GetDamagePaidAmount(const DamageID: Integer): Double;
    function GetCurrentDamageResponsibilityCount(DamageID: Integer): Integer;
    function GetTotalDamageEstimateAmount(DamageID: Integer; EstimateType: Integer): Double;

    {Locator}
    function IsDamageAssignedToLocator(const DamageID, LocatorID: Integer): Boolean;


  end;

var
  LDamageDM: TLDamageDM;

implementation
{$R *.dfm}
uses
  OdDBUtils, DateUtils, QMConst, DamageProfitCenterRules, SyncEngineU, OdMiscUtils,
  QMServerLibrary_Intf;



procedure TLDamageDM.AddNewEstimateForDamage(DamageID: Integer;
  PaidAmount: Double; const Company, Comment: string);
begin
  Assert(DamageID > 0);
  DamageEstimate.Open;
  DamageEstimate.Insert;
  try
    DamageEstimate.FieldByName('damage_id').AsInteger := DamageID;
    DamageEstimate.FieldByName('emp_id').AsInteger := DM.UQState.EmpID;
    DamageEstimate.FieldByName('Company').AsString := Company;
    DamageEstimate.FieldByName('comment').AsString := Comment;
    DamageEstimate.FieldByName('amount').AsFloat := PaidAmount;
    DamageEstimate.Post;
  except
    DamageEstimate.Cancel;
  end;
end;

procedure TLDamageDM.ApproveDamage(DamageID: Integer; out ReturnedMsg: string);
begin
  DM.CallingServiceName('ApproveDamage');
  ReturnedMsg := DM.Engine.Service.ApproveDamage(DM.Engine.SecurityInfo, DamageID, Now);
end;

procedure TLDamageDM.ArchiveDamageInvestigatorNarrative(const DamageID: Integer;
  const Narrative: string);
begin
  Assert(DamageID <> 0);
  Assert(NotEmpty(Narrative));
  DM.SaveNote(Narrative, '', 600, qmftDamageNarrative, DamageID, False);  //600 sub_type is damage narrative
end;

procedure TLDamageDM.DamageAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('active').AsBoolean := True;
  DataSet.FieldByName('uq_notified_date').AsDateTime := RoundSQLServerTimeMS(Now);
  DataSet.FieldByName('damage_date').AsDateTime := StartOfTheDay(Now);
  DataSet.FieldByName('damage_type').AsString := DamageTypePending;
  DataSet.FieldByName('claim_status').AsString := 'OPEN';
  DataSet.FieldByName('added_by').AsInteger := DM.EmpID;
end;


procedure TLDamageDM.DamageAfterOpen(DataSet: TDataSet);
const
  DamageRequired: array[0..16] of string =                            //QMANTWO-615    QMANTWO-703
    ('damage_date', 'uq_notified_date', 'notified_by_person', 'notified_by_company',
     'notified_by_phone', 'utility_co_damaged', 'state', 'county', 'investigator_id',
     'city', 'location', 'excavator_company', 'locate_requested',
     'locate_marks_present', 'facility_type', 'facility_size', 'facility_material');
//     'facility_type_2', 'facility_size_2', 'facility_material_2',   //QMANTWO-615 QMANTWO-703
//     'facility_type_3', 'facility_size_3', 'facility_material_3');  //QMANTWO-615  QMANTWO-703

begin
  SetRequiredFields(DataSet, DamageRequired);
  DM.SetDateFieldGetTextEvents(DataSet);
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('locate_marks_present'), 'ynub');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('locate_requested'), 'ynub');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('site_pot_holed'), 'ynub');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('site_tracer_wire_intact'), 'yesnoblank');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('site_marked_in_white'), 'yesnoblank');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('marks_within_tolerance'), 'yesnoblank');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('offset_visible'), 'yesnoblank');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('state'), 'state');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('site_buried_under'), 'buried');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('site_clarity_of_marks'), 'visibility');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('excavator_type'), 'excrtype');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('excavation_type'), 'excntype');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('exc_resp_code'), 'exres');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('uq_resp_code'), 'uqres');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('spc_resp_code'), 'utres');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('uq_resp_ess_step'), 'locess');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_type'), 'factype');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_size'), 'facsize');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_material'), 'facmtrl');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_type_2'), 'factype');  //QMANTWO-615 QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_size_2'), 'facsize');  //QMANTWO-615  QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_material_2'), 'facmtrl');  //QMANTWO-615 QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_type_3'), 'factype');   //QMANTWO-615  QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_size_3'), 'facsize');    //QMANTWO-615 QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('facility_material_3'), 'facmtrl'); //QMANTWO-615 QMANTWO-703
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('locator_experience'), 'locexp');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('locate_equipment'), 'locequip');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('claim_status'), 'claimstat');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('invoice_code'), 'invcode');
end;

procedure TLDamageDM.DamageBeforeEdit(DataSet: TDataSet);
begin
  DM.LastSavedExcRespCode := Damage.FieldByName('exc_resp_code').AsString;
  DM.LastSavedUQRespCode := Damage.FieldByName('uq_resp_code').AsString;
  DM.LastSavedSpecialRespCode := Damage.FieldByName('spc_resp_code').AsString;
end;

procedure TLDamageDM.DamageBeforeOpen(DataSet: TDataSet);
begin
  DM.Engine.LinkEventsAutoInc(DataSet);
end;

procedure TLDamageDM.DamageBeforePost(DataSet: TDataSet);
var
  i: Integer;
begin
  if Damage.FieldByName('utility_co_damaged').AsString = '' then
  begin
    MessageDlg('A Damage Company is required.  '+#13+#10+'It is selected based on the client that is selected.  '+#13+#10+'If the client you select does not have a cooresponding Damage Company, the Help'+#13+#10+'Desk should be called so that a  Damage Company can be assigned to the '+#13+#10+'Client you have selected'+#13+#10+''+#13+#10+'This Damage cannot be saved until corrected.', mtError, [mbOK], 0);
    exit;
  end;
  //QMANTWO-615
  if Damage.FieldByName('Facility_Type_2').IsNull then Damage.FieldByName('Facility_Type_2').asstring := '--';
  if Damage.FieldByName('Facility_Size_2').IsNull then Damage.FieldByName('Facility_Size_2').asstring := '--';
  if Damage.FieldByName('Facility_Material_2').IsNull then Damage.FieldByName('Facility_Material_2').asstring := '--';
  if Damage.FieldByName('Facility_Type_3').IsNull then Damage.FieldByName('Facility_Type_3').asstring := '--';
  if Damage.FieldByName('Facility_Size_3').IsNull then Damage.FieldByName('Facility_Size_3').asstring := '--';
  if Damage.FieldByName('Facility_Material_3').IsNull then Damage.FieldByName('Facility_Material_3').asstring := '--';

  for i := 0 to Damage.Fields.Count - 1 do
    if Damage.Fields[i] is TDateTimeField then
      if Damage.Fields[i].AsDateTime = 0 then
        Damage.Fields[i].Clear;
  if Damage.FieldByName('uq_resp_code').IsNull then
    Damage.FieldByName('uq_resp_type').Clear;
  if Damage.FieldByName('exc_resp_code').IsNull then
    Damage.FieldByName('exc_resp_type').Clear;
  if Damage.FieldByName('spc_resp_code').IsNull then
    Damage.FieldByName('spc_resp_type').Clear;
  Damage.FieldByName('modified_date').AsDateTime := RoundSQLServerTimeMS(Now);
end;

procedure TLDamageDM.DamageChangeReasonList(List: TStrings;
  IncludeBlank: Boolean);
begin
  DM.GetChangeReasonList(List, 'dmgreason', IncludeBlank);
end;

procedure TLDamageDM.DamageEstimateAfterOpen(DataSet: TDataSet);
begin
  DataSet.FieldByName('third_party').OnGetText := DM.ThirdPartyBooleanOnGetText;
end;

procedure TLDamageDM.DamageEstimateBeforeOpen(DataSet: TDataSet);
begin
  DM.Engine.LinkEventsAutoInc(DataSet);
  AddLookupField('employee_name', DataSet, 'emp_id', EmployeeDM.EmployeeLookup,
    'emp_id', 'short_name', ShortNameLenth, 'EmployeeNameLookup', 'Employee');
  AddLookupField('estimate_type_desc', DataSet, 'estimate_type', DM.EstimateTypeLookup,
    'code', 'description', 25, 'EstimateTypeLookup', 'EstimateType');
  EmployeeDM.EmployeeLookup.Open;
  DM.EstimateTypeLookup.Open;
end;

procedure TLDamageDM.DamageEstimateBeforePost(DataSet: TDataSet);
var
  IsThirdParty: Boolean;
begin
  Assert(not DataSet.FieldByName('third_party').IsNull);
  DataSet.FieldByName('amount').Required := True;
  IsThirdParty := DataSet.FieldByName('third_party').AsBoolean;
  DataSet.FieldByName('comment').Required := not IsThirdParty;
  DataSet.FieldByName('company').Required := IsThirdParty;
end;

function TLDamageDM.GetCurrentDamageEstimateAmount(DamageID: Integer): Double;
const
  Query = 'select modified_date, amount from damage_estimate where damage_id = %d order by modified_date desc';
var
  Estimates: TDataSet;
begin
  Result := 0;
  Estimates := DM.Engine.OpenQuery(Format(Query, [DamageID]));
  try
    if not Estimates.Eof then
      Result := Estimates.FieldByName('amount').AsFloat;
  finally
    FreeAndNil(Estimates);
  end;
end;

function TLDamageDM.GetCurrentDamageResponsibilityCount(
  DamageID: Integer): Integer;
const
  Query = 'SELECT COUNT(*) AS N FROM damage_history '+
    'WHERE damage_id = %d AND (exc_resp_code IS NOT NULL ' +
    '  OR uq_resp_code IS NOT NULL OR spc_resp_code IS NOT NULL)';
begin
  Result := DM.Engine.RunSQLReturnN(Format(Query, [DamageID]));
end;

function TLDamageDM.GetDamageAccrualAmount(DamageID: Integer; InvoiceCode: string): Double;
var
  Ratio: Double;
  EstimateAmount: Double;
  PaidAmount: Double;
begin
  if InvoiceCode = '' then
    Result := 0
  else begin
    Ratio := StrToFloatDef(DM.GetRefModifier('invcode', InvoiceCode), 0.00);
    EstimateAmount := GetCurrentDamageEstimateAmount(DamageID);
    PaidAmount := GetDamagePaidAmount(DamageID);
    Result := (EstimateAmount - PaidAmount) * Ratio;
  end;
end;


procedure TLDamageDM.GetDamageClaimantList(Claimants: TStrings;
  const DamageID: Integer; ClaimantType: Integer);
begin
  Assert(DamageID > 0);
  DamageClaimant.Params.ParamByName('damage_id1').Value := DamageID;
  DamageClaimant.Params.ParamByName('damage_id2').Value := DamageID;
  DamageClaimant.Params.ParamByName('damage_id3').Value := DamageID;
  DamageClaimant.Filter := 'type = ' + IntToStr(ClaimantType);
  DamageClaimant.Filtered := True;
  DM.GetListFromDataSet(DamageClaimant, Claimants, 'claimant', 'claimant_id', False);
end;

function TLDamageDM.GetDamageIDForInvoiceID(const InvoiceID: Integer): Integer;
const
  SQL = 'select damage_id as N from damage_invoice where invoice_id = %d';
begin
  Result := DM.Engine.RunSQLReturnN(Format(SQL, [InvoiceID]));
end;

function TLDamageDM.GetDamageIDForUQDamageID(
  const UQDamageID: Integer): Integer;
begin
  Result := DM.Engine.GetDamageIDForUQDamageID(UQDamageID);
end;

function TLDamageDM.GetDamagePaidAmount(const DamageID: Integer): Double;
const
  Query = 'select sum(paid_amount) as total_paid from damage_invoice where damage_id=%d';
var
  InvoiceTotal: TDataSet;
begin
  Result := 0;
  InvoiceTotal := DM.Engine.OpenQuery(Format(Query, [DamageID]));
  try
    if not InvoiceTotal.Eof then
      Result := InvoiceTotal.FieldByName('total_paid').AsFloat;
  finally
    FreeAndNil(InvoiceTotal);
  end;
end;

function TLDamageDM.GetDamagePriority(const DamageID: Integer): string;
const
  Select = 'select coalesce(description, ''Normal'') S from damage ' +
    'left join reference on work_priority_id = ref_id ' +
    'where damage_id = ';
begin
  Result := DM.Engine.RunSQLReturnS(Select + IntToStr(DamageID));
end;

procedure TLDamageDM.GetRequiredDamageDocumentList(DocumentTypes: TStrings;
  const LocateRequested: Boolean; const EstimateAmt: Currency);
const
  SqlRequiredDocuments ='select * from required_document where foreign_type = %d ' +
    'and (Coalesce(require_for_min_estimate, 0) = 0 or (require_for_min_estimate <= %f)) ' +
    'and active ';
  SqlLocateReqCriteria = 'and require_for_locate';
  SqlNoLocateReqCriteria = 'and require_for_no_locate';
var
  Data: TDataSet;
  Select: string;
begin
  Select := Format(SqlRequiredDocuments, [qmftDamage, EstimateAmt]);
  if LocateRequested then
    Select := Select + SqlLocateReqCriteria
  else
    Select := Select + SqlNoLocateReqCriteria;
  Data := DM.Engine.OpenQuery(Select);
  try
    DM.GetListFromDataSet(Data, DocumentTypes, 'name', 'required_doc_id', False);
  finally
    FreeAndNil(Data);
  end;
end;

function TLDamageDM.GetTicketIDForDamageID(const DamageID: Integer): Integer;
begin
  Result := DM.Engine.GetTicketID('', '',  0, 0, DamageID);
end;

function TLDamageDM.GetTotalDamageEstimateAmount(DamageID,
  EstimateType: Integer): Double;
const
  Query = 'select sum(amount) from damage_estimate where damage_id = %d and estimate_type = %d';
var
  Estimates: TDataSet;
begin
  Result := 0;
  Estimates := DM.Engine.OpenQuery(Format(Query, [DamageID, EstimateType]));
  try
    if not Estimates.IsEmpty then
      Result := Estimates.Fields[0].AsFloat;
  finally
    FreeAndNil(Estimates);
  end;
end;

function TLDamageDM.HaveDamagesRights: Boolean;
begin
  Result := PermissionsDM.CanI(RightDamages) or PermissionsDM.CanI(RightDamagesV2);
end;

procedure TLDamageDM.InitDamageTypesListBox(DamageTypesListBox: TCustomListBox);
begin
  Assert(DamageTypesListBox <> nil);
  PopulateDamageTypesList(DamageTypesListBox.Items);
  if DamageTypesListBox.Items.Count > 0 then
    DamageTypesListBox.ItemIndex := 0;
end;

function TLDamageDM.IsDamageAssignedToLocator(const DamageID,
  LocatorID: Integer): Boolean;
const
  Select = 'select count(*) N from damage where damage_id = %d and investigator_id = %d and active';
begin
  Result := DM.Engine.RunSQLReturnN(Format(Select, [DamageID, LocatorID])) > 0;
end;

procedure TLDamageDM.PopulateDamageTypesList(List: TStrings);
begin
  Assert(List <> nil);
  List.Clear;
  List.Add(DamageTypeApproved);
  List.Add(DamageTypePendingApproval);
  List.Add(DamageTypePending);
  List.Add(DamageTypeIncoming);
end;

procedure TLDamageDM.SetDamageModified;
begin
  Assert(LDamageDM.Damage.Active);
  Damage.Edit;
  Damage.FieldByName('DeltaStatus').AsString := 'U';
  Damage.Post;
end;

procedure TLDamageDM.SetDamagePriority(DamageID, PriorityRefID: Integer);
begin
  DM.CallingServiceName('SaveDamagePriority');
  DM.Engine.Service.SaveDamagePriority(DM.Engine.SecurityInfo, DamageID, RoundSQLServerTimeMS(Now), PriorityRefID);
end;

procedure TLDamageDM.SetDamagePropertiesFromTicketIDField;
const
  SQL = 'select * from ticket where ticket_id = %d';
var
  DataSet: TDataSet;
  TicketID: Integer;
  WorkLocation: string;
  LocatorID: Integer;

  function TicketValue(const FieldName: string): string;
  begin
    Result := DataSet.FieldByName(FieldName).AsString;
  end;

  procedure IfBlankAssignTicketValue(const DamageField, TicketField: string);
  begin
    if Trim(Damage.FieldByName(DamageField).AsString) = '' then
      Damage.FieldByName(DamageField).Value := DataSet.FieldByName(TicketField).Value;
  end;

  procedure IfBlankAssignValue(const DamageField, Value: string);
  begin
    if Trim(Damage.FieldByName(DamageField).AsString) = '' then
      Damage.FieldByName(DamageField).AsString := Value;
  end;

begin
  TicketID := Damage.FieldByName('ticket_id').AsInteger;
  Assert(TicketID > 0, 'No ticket id present on damage');
  DataSet := DM.Engine.OpenQuery(Format(SQL, [TicketID]));
  try
    if DataSet.Eof then
      raise Exception.CreateFmt('Ticket %d not found in the local database to set damage properties', [TicketID]);
    EditDataSet(Damage);
    IfBlankAssignTicketValue('page', 'map_page');
    IfBlankAssignTicketValue('city', 'work_city');
    IfBlankAssignTicketValue('state', 'work_state');
    IfBlankAssignTicketValue('county', 'work_county');
    WorkLocation := Trim(TicketValue('work_address_number') +' '+ TicketValue('work_address_street'));
    IfBlankAssignValue('location', WorkLocation);
    try
      IfBlankAssignValue('profit_center', AutoDetermineDamageProfitCenter(
        Damage.FieldByName('state').AsString, Damage.FieldByName('county').AsString,
        Damage.FieldByName('city').AsString, DamageProfitCenterRules));
    except
      // Ignore errors
    end;
    if Trim(Damage.FieldByName('remarks').AsString) = '' then
      Damage.FieldByName('remarks').AsString := WorkLocation
    else if not SameText(Damage.FieldByName('remarks').AsString, WorkLocation) then
      Damage.FieldByName('remarks').AsString := Damage.FieldByName('remarks').AsString + ' ' + WorkLocation;
    if Damage.FieldByName('locator_id').IsNull and
      DM.GetLocatorByTicketAndClient(TicketID, Damage.FieldByName('client_id').AsInteger, LocatorID) then
      Damage.FieldByName('locator_id').Value := LocatorID;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TLDamageDM.SetDamageTicketAcknowledged(TicketID, InvestigatorID: Integer);
var
  Tick: TicketKey;
begin
  Tick := TicketKey.Create;
  try
    Tick.TicketID := TicketID;
    DM.CallingServiceName('AckDamageTicket');
    DM.Engine.Service.AckDamageTicket(DM.Engine.SecurityInfo, Tick, DM.UQState.EmpID, InvestigatorID);
  finally
    FreeAndNil(Tick);
  end;
end;

procedure TLDamageDM.UpdateDamageInCache(const DamageID: Integer);
begin
  DM.Engine.GetDamageDataFromServer(DamageID);
end;

procedure TLDamageDM.UpdateDamageLitigationInCache(const LitigationID: Integer);
begin
  DM.Engine.GetDamageLitigationDataFromServer(LitigationID);
end;

procedure TLDamageDM.UpdateDamageThirdPartyInCache(const ThirdPartyID: Integer);
begin
  DM.Engine.GetDamageThirdPartyDataFromServer(ThirdPartyID);
end;

end.
