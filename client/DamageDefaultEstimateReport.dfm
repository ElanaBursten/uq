inherited DamageDefaultEstimateReportForm: TDamageDefaultEstimateReportForm
  Left = 651
  Top = 279
  Caption = 'DamageDefaultEstimateReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object UtilityCoLabel: TLabel [0]
    Left = 0
    Top = 43
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Utility Company:'
  end
  object ProfitCenterLabel: TLabel [1]
    Left = 0
    Top = 14
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Profit Center:'
  end
  object FacilityTypeLabel: TLabel [2]
    Left = 0
    Top = 73
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Facility Type:'
  end
  object UtilityCoDamaged: TComboBox [3]
    Left = 86
    Top = 40
    Width = 181
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 1
  end
  object ProfitCenter: TComboBox [4]
    Left = 86
    Top = 11
    Width = 181
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 0
  end
  object FacilityType: TComboBox [5]
    Left = 86
    Top = 70
    Width = 181
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 2
  end
end
