unit EmployeeCovStatusReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst;

type
  TEmployeeCovStatusReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    ShowDetailsCheckBox: TCheckBox;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu;

procedure TEmployeeCovStatusReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
end;

procedure TEmployeeCovStatusReportForm.ValidateParams;
begin
  inherited;
  SetReportID('EmployeeCovStatus');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamBoolean('ShowDetails', ShowDetailsCheckBox.Checked);
end;

end.
