unit QMTransport;

interface

uses SysUtils, Classes, IdHttp, OdExceptions;

type
  EHttpTransportError = class(EOdException);

  THttpTransport = class
  private
    FUsername: string;
    FAPIKey: string;
    FBaseURL: string;
  public
    constructor Create(const BaseURL, Username, APIKey: string);
    destructor Destroy; override;
    procedure PostStream(const OpURL: string; Stream: TStringStream);
  end;

implementation

{ THttpTransport }

constructor THttpTransport.Create(const BaseURL, Username, APIKey: string);
begin
  inherited Create;

  FBaseURL := BaseURL;        {http://localhost:5041}
  FUsername := UserName;
  FAPIKey := APIKey;
end;

destructor THttpTransport.Destroy;
begin
  inherited;
end;

procedure THttpTransport.PostStream(const OpURL: string; Stream: TStringStream);
var
  Http: TIdHttp;
begin
  Assert(Assigned(Stream));
  Http := TIdHTTP.Create(nil);
  try
    Http.Request.Clear;
    Http.Request.BasicAuthentication := True;
    Http.Request.Username := FUserName;
    Http.Request.Password := FAPIKey;
    Http.Request.ContentType := 'application/json';
    try
      Http.Post(FBaseURL + OpURL, Stream);
    except
      on E: EIdHTTPProtocolException do begin
        raise EHttpTransportError.CreateFmt('The server returned an error: %d - %s',
          [E.ErrorCode, E.ErrorMessage]);
      end;
    end;
    if Http.ResponseCode >= 400 then
      raise EHttpTransportError.CreateFmt('The server returned an error: %d - %s',
        [Http.ResponseCode, Http.ResponseText]);
    if not Http.ResponseCode in [200, 201, 202, 204] then
      raise EHttpTransportError.CreateFmt('The server returned an unexpected result: %d - %s',
        [Http.ResponseCode, Http.ResponseText]);
  finally
    FreeAndNil(Http);
  end;
end;

end.
