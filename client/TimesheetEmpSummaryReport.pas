unit TimesheetEmpSummaryReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, OdRangeSelect, StdCtrls, EmployeeSelectFrame;

type
  TTimesheetEmpSummaryReportForm = class(TReportBaseForm)
    ReportDateLabel: TLabel;
    ReportDate: TOdRangeSelectFrame;
    EmployeeLabel: TLabel;
    EmployeeBox: TEmployeeSelect;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses QMConst, DMu;

{$R *.dfm}

procedure TTimesheetEmpSummaryReportForm.InitReportControls;
begin
  inherited;
  EmployeeBox.Initialize(esReportEmployees);
  ReportDate.SelectDateRange(OdRangeSelect.rsLastWeek);
end;

procedure TTimesheetEmpSummaryReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimesheetEmpSummary');
  SetParamIntCombo('emp_id', EmployeeBox.EmployeeCombo, True);
  SetParamDate('start_date', ReportDate.FromDate);
  SetParamDate('end_date', ReportDate.ToDate);
end;

end.
