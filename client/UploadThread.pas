unit UploadThread;

interface

uses
  Classes, SysUtils, Windows, Messages, OdMiscUtils, BaseAttachmentDMu,
  LocalAttachmentDMu, DateUtils, JclDebug, SyncObjs;

const
  // Message Ids used by background upload thread communications
  UM_UPLOAD_STATUS = WM_USER + 1009;
  UM_UPLOAD_NEED_DATA = WM_USER + 1010;

  NO_UPLOAD_LOCATION_ID = -1;

type
  TUploadMessageType = (umtError, umtInfo, umtMajor, umtMinor);

  TUploadStatus = class
  public
    MessageType: TUploadMessageType;
    MessageText: string;
    PercentComplete: Integer;
    PendingUploadCount: Integer;
    PendingUploadSize: LongInt;
    LongestFileWaitSeconds: LongInt;
  end;

  TParentDataRequest = class
  public
    ForeignType: Integer;
    ForeignID: Integer;
  end;

// TODO: Update this to descend from TBackgroundWorker

  TBackgroundUploadThread = class(TJclDebugThread)
  private
    FAttach: TLocalAttachment;
    FParentHandle: HWND;
    FNextRunTime: TDateTime;
    FNextRunTimeCS: TCriticalSection;
    FDebug: Boolean;
    procedure PostStatusMessage(const MsgType: TUploadMessageType; const Msg: string; PercentComplete: Integer=-1; PendingUploadCount: Integer=-1; PendingUploadSize: Integer=-1; LongestFileWait: Integer=-1);
    procedure PostErrorMessage(const E: Exception);
    procedure ComputeNextRunTime;
    function TimeToRun: Boolean;
    procedure Log(Sender: TObject; const LogMessage: string);
    procedure DisplayTransientError(Sender:TObject; const ErrorMsg: String);
    procedure MajorProgressUpdate(Sender: TObject; const ProgressMessage: string; const PctComplete: Integer);
    procedure MinorProgressUpdate(Sender: TObject; const ProgressMessage: string; const PctComplete: Integer);
    procedure PendingUploadFilesChanged(Sender: TObject);
    procedure SetNextRunTime(const Value: TDateTime);
    function GetNextRunTime: TDateTime;
    procedure NeedParentDataFromServer(Sender: TObject; FileItem: TUploadFileItem);
  protected
    procedure Execute; override;
  public
    InProgress: Boolean;   // Means an upload is running
    UploadLocationID: Integer;
    RightEnabled: Boolean;
    constructor Create(ParentHandle: HWND; const ApplicationName: string; const EmpID: Integer; Debug: Boolean);
    destructor Destroy; override;
    procedure StopNow;
    procedure PostNextRunStatus;
    property NextRunTime: TDateTime read GetNextRunTime write SetNextRunTime;
  end;

implementation

uses
  OdLog;

{ TBackgroundUploadThread }

constructor TBackgroundUploadThread.Create(ParentHandle: HWND; const ApplicationName: string;
  const EmpID: Integer; Debug: Boolean);
begin
  inherited Create(True, 'BackgroundUploadThread');
  FreeOnTerminate := False;
  FDebug := Debug;
  FParentHandle := ParentHandle;
  FAttach := TLocalAttachment.Create(ApplicationName, '', EmpID, True);
  FAttach.OnLogMessage := Log;
  FAttach.OnMajorProgress := MajorProgressUpdate;
  FAttach.OnMinorProgress := MinorProgressUpdate;
  FAttach.OnPendingFilesChanged := PendingUploadFilesChanged;
  FAttach.OnNeedDataFromServer := NeedParentDataFromServer;
  FAttach.OnDisplayTransientError := DisplayTransientError;
  UploadLocationID := NO_UPLOAD_LOCATION_ID;
  FNextRunTimeCS := TCriticalSection.Create;
  FNextRunTime := 0;
  Resume;
end;

procedure TBackgroundUploadThread.PendingUploadFilesChanged(Sender: TObject);
begin
  PostStatusMessage(umtInfo, Format('Pending Files: %d', [FAttach.PendingUploadCount]),
    -1, FAttach.PendingUploadCount, FAttach.PendingUploadSize,
    FAttach.LongestFileWaitSeconds);
end;

procedure TBackgroundUploadThread.NeedParentDataFromServer(Sender: TObject; FileItem: TUploadFileItem);
var
  DataRequest: TParentDataRequest;
begin
  Assert(Assigned(FileItem), 'FileItem is not assigned');
  // DataRequest must be freed by the message consumer
  DataRequest := TParentDataRequest.Create;
  DataRequest.ForeignType := FileItem.ForeignType;
  DataRequest.ForeignID := FileItem.ForeignID;
  PostMessage(FParentHandle, UM_UPLOAD_NEED_DATA, 0, UINT(DataRequest));
end;

destructor TBackgroundUploadThread.Destroy;
begin
  inherited;
  FreeAndNil(FAttach);
  FreeAndNil(FNextRunTimeCS);
end;

function TBackgroundUploadThread.TimeToRun: Boolean;
begin
  Result := (NextRunTime > 0) and (NextRunTime <= Now);
end;

procedure TBackgroundUploadThread.ComputeNextRunTime;
var
  MinutesToSleep: Integer;
begin
  MinutesToSleep := 3 + Random(7);
  NextRunTime := IncMinute(Now, MinutesToSleep);
end;

procedure TBackgroundUploadThread.PostNextRunStatus;
var
  Msg: string;
begin
  if UploadLocationID < 0 then
    Msg := 'Cannot upload; background upload right needs upload location id modifier'
  else if NextRunTime > 0 then begin
    if Assigned(FAttach) and (FAttach.LongestFileWaitSeconds > 0) then
      Msg := 'Trying again at ' + FormatDateTime('t', NextRunTime) +
        '; longest file wait is ' + SecondsToTime(FAttach.LongestFileWaitSeconds, False)
    else
      Msg := 'Checking again at ' + FormatDateTime('t', NextRunTime);
  end else
    Msg := 'Not scheduled to upload';
  PostStatusMessage(umtInfo, Msg);
end;

procedure TBackgroundUploadThread.Execute;
begin
  Assert(Assigned(FAttach),
    'No LocalAttachment assigned in TBackgroundUploadThread.Execute');

  while not Terminated do begin
    if TimeToRun and RightEnabled then begin
      try
        if FAttach.HaveFilesToUpload then begin
          InProgress := True;
          try
            FAttach.UploadPendingAttachments(UploadLocationID);
          finally
            InProgress := False;
          end;
        end;
      except
        on E: Exception do begin
          if FDebug then
            GetLog.LogException(E);
          PostErrorMessage(E);
          HandleException;
        end;
      end;
      ComputeNextRunTime;
    end;
    Sleep(1000);
  end;
end;

procedure TBackgroundUploadThread.Log(Sender: TObject; const LogMessage: string);
begin
  PostStatusMessage(umtInfo, LogMessage);
end;

procedure TBackgroundUploadThread.DisplayTransientError(Sender: TObject;
  const ErrorMsg: String);
begin
  PostStatusMessage(umtError, ErrorMsg);
end;

procedure TBackgroundUploadThread.PostErrorMessage(const E: Exception);
begin
  PostStatusMessage(umtError, E.Message);
end;

procedure TBackgroundUploadThread.MajorProgressUpdate(Sender: TObject;
  const ProgressMessage: string; const PctComplete: Integer);
begin
  PostStatusMessage(umtMajor, ProgressMessage, PctComplete,
    FAttach.PendingUploadCount, FAttach.PendingUploadSize,
    FAttach.LongestFileWaitSeconds);
end;

procedure TBackgroundUploadThread.MinorProgressUpdate(Sender: TObject;
  const ProgressMessage: string; const PctComplete: Integer);
begin
  PostStatusMessage(umtMinor, ProgressMessage, PctComplete);
end;

procedure TBackgroundUploadThread.PostStatusMessage(const MsgType: TUploadMessageType;
  const Msg: string; PercentComplete: Integer;
  PendingUploadCount: Integer; PendingUploadSize: Integer; LongestFileWait: Integer);
var
  UploadStatus: TUploadStatus;
begin
  // UploadStatus must be freed by the message consumer
  UploadStatus := TUploadStatus.Create;
  UploadStatus.MessageType := MsgType;
  UploadStatus.MessageText := Msg;
  UploadStatus.PercentComplete := PercentComplete;
  UploadStatus.PendingUploadCount := PendingUploadCount;
  UploadStatus.PendingUploadSize := PendingUploadSize;
  UploadStatus.LongestFileWaitSeconds := LongestFileWait;
  PostMessage(FParentHandle, UM_UPLOAD_STATUS, 0, UINT(UploadStatus));
end;

procedure TBackgroundUploadThread.StopNow;
begin
  NextRunTime := 0;
  FAttach.Stop;
end;

procedure TBackgroundUploadThread.SetNextRunTime(const Value: TDateTime);
begin
  Assert(UploadLocationID <> NO_UPLOAD_LOCATION_ID, 'No UploadLocationID set in TBackgroundUploadThread.SetNextRunTime');

  FNextRunTimeCS.Enter;
  try
    FNextRunTime := Value;
  finally
    FNextRunTimeCS.Leave;
  end;
  PostNextRunStatus;
end;

function TBackgroundUploadThread.GetNextRunTime: TDateTime;
begin
  FNextRunTimeCS.Enter;
  try
    Result := FNextRunTime;
  finally
    FNextRunTimeCS.Leave;
  end;
end;

end.

