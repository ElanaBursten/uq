unit Config;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OdEmbeddable, Db, StdCtrls, Buttons, ExtCtrls, 
  ComCtrls, DBISAMTb, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxDBData, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridLevel, cxGrid,
  cxNavigator, cxPC, cxListBox, cxContainer, Menus, PlatMgmtUtils,
  cxCheckBox, cxTextEdit, cxMemo, cxRichEdit, dxBarBuiltInMenu,
  cxCustomData, cxFilter, cxData, dxSkinsCore;

const
  UM_PLATSSTART = WM_USER + 1200;  //QM-164 Plats Start
  UM_PLATSDONE = WM_USER + 1250;
type
  TConfigCompInfo = Record
    CompUser: string;
    Phone: string;
    SerialNum: string;
    ComputerName: string;
    SimCardICC: string;
    CellDeviceID: string;
    OSVersion: string;
    OsServicePackVersion: string;
    PlatsUpdated: string;
  End;


  TConfigForm = class(TEmbeddableForm)
    ColorDialog: TColorDialog;
    ViewTableSrc: TDataSource;
    TableData: TDBISAMQuery;
    ChildTable: TDBISAMQuery;
    ConfigPageCtrl: TPageControl;
    tabGeneral: TTabSheet;
    tabTicketColors: TTabSheet;
    Colors: TPageControl;
    TicketColorsTab: TTabSheet;
    AlternateRowColorButton: TButton;
    DueTicketColorButton: TButton;
    CriticalTicketColorButton: TButton;
    DoNotMarkTicketColorButton: TButton;
    GeocodeColorsTab: TTabSheet;
    HighestGeocodePrecisionColorButton: TButton;
    AcceptableGeocodePrecisionColorButton: TButton;
    ZeroGeocodeMatchesColorButton: TButton;
    TabCamera: TTabSheet;
    DriveLettersGroup: TGroupBox;
    DriveLetter: TEdit;
    AddDriveLetterButton: TButton;
    RemoveDriveLetterButton: TButton;
    DriveLettersListBox: TListBox;
    PnlGeneralTop: TPanel;
    TabPlats: TTabSheet;
    pnlClient: TPanel;
    pnlBottom: TPanel;
    btnPlatAge: TButton;
    btnWhatToUpdate: TButton;
    btnUpdateThisMachine: TButton;
    pgCtrlPlatsMgr: TcxPageControl;
    tabPlatAge: TcxTabSheet;
    tabUpdateList: TcxTabSheet;
    tabUpdateLog: TcxTabSheet;
    MainMenu1: TMainMenu;
    dsPlats: TDataSource;
    MyPlatGridView: TcxGridDBTableView;
    MyPlatGridLevel1: TcxGridLevel;
    MyPlatGrid: TcxGrid;
    ColTopName: TcxGridDBColumn;
    ColLocalPath: TcxGridDBColumn;
    ColLastUpdated: TcxGridDBColumn;
    ColCompany: TcxGridDBColumn;
    UpdateGrid: TcxGrid;
    UpdateGridView: TcxGridDBTableView;
    UpdateGridLevel1: TcxGridLevel;
    dsUpdates: TDataSource;
    ColUpdatesMethod: TcxGridDBColumn;
    ColUpdatesCompany: TcxGridDBColumn;
    ColUpdatesCenter: TcxGridDBColumn;
    ColUpdatesPM7Age: TcxGridDBColumn;
    ColUPdatesStatus: TcxGridDBColumn;
    ColUpdatesPM7File: TcxGridDBColumn;
    ColUpdatesLocalFileAge: TcxGridDBColumn;
    ColUpdatesChecked: TcxGridDBColumn;
    ColCenter: TcxGridDBColumn;
    ColLocInfo: TcxGridDBColumn;
    edUpdateLog: TRichEdit;
    ColAge: TcxGridDBColumn;
    ColRecIndex: TcxGridDBColumn;
    Panel1: TPanel;
    lblPlatsUpdated2: TLabel;
    edPlatsUpdated2: TLabel;
    lblLocalNum: TLabel;
    edtLocalNum: TLabel;
    lblWarnings: TLabel;
    edtWarnings: TLabel;
    lblNewMissing: TLabel;
    edtNewMissing: TLabel;
    lblUpdates: TLabel;
    edtUpdates: TLabel;
    lblDuplicates: TLabel;
    edtDuplicates: TLabel;
    lblErrors: TLabel;
    EdtErrors: TLabel;
    btnClearChecks: TButton;
    Label2: TLabel;
    Label3: TLabel;
    lblUpdated: TLabel;
    edtUpdated: TLabel;
    edtUpdateErrors: TLabel;
    lblUpdateErrors: TLabel;
    ColExtractTo: TcxGridDBColumn;
    ColCompanyName: TcxGridDBColumn;
    btnCancel: TButton;
    GeneralPages: TPageControl;
    tsDatabaseMaint: TTabSheet;
    tsApplication: TTabSheet;
    tsComputerInfo: TTabSheet;
    pnlDataMaint: TPanel;
    MiscGroupBx: TGroupBox;
    DiscardButton: TButton;
    GetCRCButton: TButton;
    ReloadTicketButton: TButton;
    pnlComputerInfo: TPanel;
    GrpBxComputerInfo: TGroupBox;
    lblSerialNumber: TLabel;
    lblComputer: TLabel;
    lblSimCardNum: TLabel;
    lblUser: TLabel;
    lblPhone: TLabel;
    lblOSVersion: TLabel;
    Label1: TLabel;
    edtUser: TEdit;
    edtPhone: TEdit;
    edtSerialNumber: TEdit;
    edtComputer: TEdit;
    edtSimCard: TEdit;
    edtOSVersion: TEdit;
    EdtCellDeviceID: TEdit;
    pnlApplication: TPanel;
    Database: TGroupBox;
    UpdateButton: TButton;
    ChangePassswordButton: TButton;
    TableName: TComboBox;
    GetSQLButton: TButton;
    TableGrid: TcxGrid;
    TableView: TcxGridDBTableView;
    TableLevel: TcxGridLevel;
    lblPlatsUpdated: TLabel;
    lblConfigStatus: TLabel;
    edtPlatsUpdated: TEdit;
    btnReloadRefernce: TButton;
    grpUserInfo: TGroupBox;
    ShowRightsButton: TButton;
    gpbxSync: TGroupBox;
    QrySync: TDBISAMQuery;
    lblLastSync: TLabel;
    lblLastSyncDate: TLabel;
    btnResetSyncStatusDates: TBitBtn;
    procedure AlternateRowColorButtonClick(Sender: TObject);
    procedure DiscardButtonClick(Sender: TObject);
    procedure DueTicketColorButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateButtonClick(Sender: TObject);
    procedure CriticalTicketColorButtonClick(Sender: TObject);
    procedure DoNotMarkTicketColorButtonClick(Sender: TObject);
    procedure HighestGeocodePrecisionColorButtonClick(Sender: TObject);
    procedure AcceptableGeocodePrecisionColorButtonClick(Sender: TObject);
    procedure ZeroGeocodeMatchesColorButtonClick(Sender: TObject);
    procedure ChangePassswordButtonClick(Sender: TObject);
    procedure GetCRCButtonClick(Sender: TObject);
    procedure TableNameChange(Sender: TObject);
    procedure GetSQLButtonClick(Sender: TObject);
    procedure AddDriveLetterButtonClick(Sender: TObject);
    procedure RemoveDriveLetterButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ShowRightsButtonClick(Sender: TObject);
    function RequireSync: Boolean; override;
    procedure ActivatingNow; override;
    procedure LeavingNow; override;
    procedure RefreshNow; override;
    procedure edDblClick(Sender: TObject);
    procedure btnTryClick(Sender: TObject);
    procedure btnPlatAgeClick(Sender: TObject);
    procedure btnWhatToUpdateClick(Sender: TObject);
    procedure UpdateGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure btnUpdateThisMachineClick(Sender: TObject);
    procedure btnClearChecksClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure ReloadTicketButtonClick(Sender: TObject);
    procedure btnReloadRefernceClick(Sender: TObject);
    procedure btnResetSyncStatusDatesClick(Sender: TObject);

  private
    fPlatsUpdatingInProcess: Boolean;
    fPlatMgr: TPlatMgr;
    ConfigCompInfo: TConfigCompInfo;
    procedure ShowUpdateLabels(ShowIt: Boolean);
    procedure UpdateRights;
    procedure SetOldTabsInvisible;
    procedure DisplayComputerInfo;
    procedure ListPlatAge;   //Currently not used QM-800 Cleanup 1/2024
    procedure GetUpdateList;
    procedure UpdatePlats;
    procedure DisplaySyncStatus; //QM-979 EB Debug Sync
  end;

implementation

uses DMu, OdWebUpdate, OdIsoDates, QMConst, OdHourglass, OdMiscUtils,
  Clipbrd, OdExceptions, OdDbUtils, OdAckMessageDlg,
  LocalPermissionsDmu, LocalEmployeeDmu, JCLSysInfo, OdWMi, psutils;

{$R *.DFM}

procedure TConfigForm.AlternateRowColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.ShadeColor;
  if ColorDialog.Execute then
    DM.UQState.ShadeColor := ColorDialog.Color;
end;

procedure TConfigForm.btnResetSyncStatusDatesClick(Sender: TObject);   //QM-986 EB Reload Ticket Data
begin
  inherited;
  DM.ClearSyncStatusDate;
  DisplaySyncStatus;
end;

procedure TConfigForm.btnCancelClick(Sender: TObject);
begin
  inherited;
  fPlatMgr.CancelUpdate;
  fPlatsUpdatingInProcess := False;
  btnCancel.Enabled := False;
end;

procedure TConfigForm.btnClearChecksClick(Sender: TObject);
var
  ds: TDataSet;
begin
  inherited;
  try
    ds := dsUpdates.DataSet;
    ds.First;
    while not ds.EOF do begin
      ds.Edit;
      ds.FieldByName('CheckedOff').AsBoolean := False;
      ds.Post;
      ds.Next;
    end;
    ds.First;
      
  except
    {Swallow}
  end;
end;

procedure TConfigForm.btnPlatAgeClick(Sender: TObject);
var
 NumOfLocalPlats: integer;
begin
  inherited;
  ShowUpdateLabels(False);
  MyPlatGridView.OptionsView.NoDataToDisplayInfoText := '...';
  NumOfLocalPlats := fPlatMgr.GetPlatList(dsPlats);
  pgCtrlPlatsMgr.Properties.ActivePage := tabPlatAge;
  edtLocalNum.Caption := IntToStr(fPlatMgr.LocalFileCount);
  if NumOfLocalPlats = 0 then
    MyPlatGridView.OptionsView.NoDataToDisplayInfoText := 'No plats found on machine';
 // if NumOfLocalPlats > 0 then
    btnWhatToUpdate.Enabled := True;

  btnUpdateThisMachine.Enabled := False;
  MyPlatGridView.ApplyBestFit;
end;

procedure TConfigForm.btnReloadRefernceClick(Sender: TObject);
begin
  inherited;
  DM.ReloadReferenceData;     //QM-933 Reload Reference Table
end;

procedure TConfigForm.btnWhatToUpdateClick(Sender: TObject);
begin
  inherited;
  {This is only set if we are using a debug location to test (instead of a Plats7 drive}
  if (DM.UQState.DeveloperMode) and (DM.UQState.DebugUseAltPlatsLoc)
  and (DM.UQState.DebugPlatsAltLocation <> '') then
    fPlatMgr.SetDebugPlatsLocation(DM.UQState.DebugPlatsAltLocation);
  
  ShowUpdateLabels(False);
  GetUpdateList;
  pgCtrlPlatsMgr.Properties.ActivePage := tabUpdateList;
  if fPlatMgr.ProcessStage.UpdateListLoaded then
    btnUpdateThisMachine.Enabled := True;
  edtWarnings.Caption := IntToStr(fPlatMgr.UpdateCounts.Warnings);
  edtNewMissing.Caption := IntToStr(fPlatMgr.UpdateCounts.New);
  edtUpdates.Caption := IntToStr(fPlatMgr.UpdateCounts.Updates);
  edtDuplicates.Caption := IntToStr(fPlatMgr.UpdateCounts.Duplicates);
  edtErrors.Caption := IntToStr(fPlatMgr.UpdateCounts.Errors);
  UpdateGridView.ApplyBestFit;
end;


procedure TConfigForm.btnUpdateThisMachineClick(Sender: TObject);
begin
  inherited;
  btnCancel.Enabled := True;
  fPlatsUpdatingInProcess := True;
  UpdatePlats;
  edtUpdated.Caption := intToStr(fPlatMgr.UpdateCounts.TotalUpdated);
  edtUpdateErrors.Caption := IntToStr(fPlatMgr.UpdateCounts.ErrorsDuringUpdate);
  GetUpdateList;
  ShowUpdateLabels(True);
  pgCtrlPlatsMgr.Properties.ActivePage := tabUpdateLog;
  fPlatsUpdatingInProcess := False;
end;

procedure TConfigForm.btnTryClick(Sender: TObject);
var
  VolName : string;
begin
  inherited;
  VolName := fPlatMgr.FindDrive(PLATS7_VOL);

end;

procedure TConfigForm.DiscardButtonClick(Sender: TObject);
begin
  if DM.Engine.HavePendingTicketChanges then
  begin
    if MessageDlg('Your data contains changes that have not been synced to the'
        +sLinebreak+ ' server.  Are you sure you want to delete your local data?',
        mtWarning, [mbYes, mbNo], 0) <> mrYes then
      Exit;
  end;
  DM.DumpLocalData;
end;

procedure TConfigForm.DisplayComputerInfo;
var
  dtPlatsUpdated: TDatetime;
begin
  try
    //QM-805 Eb Marking for testing
    ConfigCompInfo.SimCardICC := DM.UQState.DeviceRec.SimCardDeviceID;
    ConfigCompInfo.CellDeviceID := DM.UQState.DeviceRec.AirCardDeviceID;    //Not registering
    ConfigCompInfo.CompUser := GetLocalUserName;
    ConfigCompInfo.OsVersion := GetOSVersion;
  //  ConfigCompInfo.OsServicePackVersion := GetWindowsServicePackVersion;
    ConfigCompInfo.ComputerName := GetLocalComputerName;
    ConfigCompInfo.SerialNum := GetLocalComputerSerial;
  except
   {swallow}
  end;

  try
    DM.qryEmpPhoneCo.Params.ParamByName('EmpID').Value:= DM.UQState.EmpID;
    DM.qryEmpPhoneCo.open;
    DM.qryEmpPhoneCo.First;
    if (not DM.qryEmpPhoneCo.Eof) and (DM.qryEmpPhoneCo.FieldByName('contact_phone').AsString <> '') then
      ConfigCompInfo.Phone := DM.qryEmpPhoneCo.FieldByName('contact_phone').AsString
    else
      ConfigCompInfo.Phone := '--';
  except
    {swallow}
  end;
  //QM-805 EB Marking for Testing
  edtUser.Text := ConfigCompInfo.CompUser;
  edtSimCard.Text := ConfigCompInfo.SimCardICC;  //QM-164 EB
  EdtCellDeviceID.Text := ConfigCompInfo.CellDeviceID;
  edtPhone.Text := ConfigCompInfo.Phone;
  edtComputer.Text := ConfigCompInfo.ComputerName;
  edtSerialNumber.Text := ConfigCompInfo.SerialNum;
  edtOSVersion.Text := ConfigCompInfo.OSVersion;
  edtPhone.Text := EmployeeDM.GetEmployeePhone(DM.EmpID);
  dtPlatsUpdated := EmployeeDM.GetLastPlatUpdate(DM.EmpID);
  if dtPlatsUpdated > 0 then
    edtPlatsUpdated.Text := DateTimeToStr(EmployeeDM.GetLastPlatUpdate(DM.EmpID))
  else
    edtPlatsUpdated.Text := '--';

  edPlatsUpdated2.Caption := edtPlatsUpdated.Text;
end;

procedure TConfigForm.DisplaySyncStatus;   //QM-986 EB Reload Ticket Data
begin
  if QrySync.Active then
    QrySync.Close;
  QrySync.Open;

  if QrySync.RecordCount > 0 then begin
    QrySync.First;
    lblLastSyncDate.Caption := QrySync.FieldByName('LastSyncDown').asstring;
  end
  else
    lblLastSyncDate.Caption := 'Date not available';
end;

procedure TConfigForm.DueTicketColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.DueTicketColor;
  if ColorDialog.Execute then
    DM.UQState.DueTicketColor := ColorDialog.Color;
end;

procedure TConfigForm.edDblClick(Sender: TObject);
begin
  inherited;
  ClearSelectionsDBText(PnlGeneralTop);  //QM-164
  if Sender is Tlabel then
    CopyLabelText(Sender as Tlabel);
end;

procedure TConfigForm.FormShow(Sender: TObject);
begin
  Colors.Visible := DM.UQState.ShowColorPrefs;
  pgCtrlPlatsMgr.Properties.ActivePage := tabPlatAge;
  ConfigPageCtrl.ActivePage := tabGeneral;

end;

procedure TConfigForm.UpdateButtonClick(Sender: TObject);
var
  Cursor: IInterface;
  WebUpdate: TOdWebUpdate;
begin
  if DM.AttachmentManager.HaveFilesToUpload then
    raise EOdNonLoggedException.Create('Cannot update while upload is in progress');

  Cursor := ShowHourGlass;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWebUpdate, '');
  WebUpdate := TOdWebUpdate.Create;
  try
    WebUpdate.URL := DM.UQState.WebUpdateURL;
    WebUpdate.ApplicationMutex := QMApplicationMutex;
    WebUpdate.MapSysToApp := DM.UQState.AppLocal;
    if IsoIsValidDateTime(DM.UQState.WebUpdateLastDateTime) then
      WebUpdate.LastUpdateDateTime := DM.UQState.WebUpdateLastDateTime;
    // TODO: Create and show a progress dialog here
    try
      WebUpdate.Update; // This never returns if updates were downloaded
    except
      on E: EOdCorruptFile do begin
        DM.Engine.AddErrorReport(DM.UQState.EmpID, Now, 'Corrupt Download', E.Message);
        raise;
      end;
    end;
    if not WebUpdate.DownloadedUpdate then
      ShowMessage('No updates are available');
  finally
    FreeAndNil(WebUpdate);
  end;
end;

procedure TConfigForm.UpdateGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  If AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_WARNING then begin
    ACanvas.Brush.Color := clWebLightCoral;
    ACanvas.Font.Color := clBlack;
    ACanvas.Font.Style := [fsBold];
  end
  else if AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_UPDATE then begin
    ACanvas.Brush.Color := clMoneyGreen;
    ACanvas.Font.Color := clBlack;
    ACanvas.Font.Style := [fsBold];
  end
  else if AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_DWARNINGDUP then begin
    ACanvas.Brush.Color := clWebLemonChiffon;
    ACanvas.Font.Color := clMaroon;
    ACanvas.Font.Style := [fsBold];
  end
  else if AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_LWARNINGDUP then begin
    ACanvas.Brush.Color := clWebLemonChiffon;
    ACanvas.Font.Color := clBlack;
    ACanvas.Font.Style := [fsBold];
  end
  else if AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_ERR then begin
    ACanvas.Font.Color := clWebFirebrick;
    ACanvas.Font.Style := [fsBold];
  end
  else if AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_UPV then begin
    ACanvas.Brush.Color := clWebAliceBlue;
    ACanvas.Font.Color := clBlack;
  end
  else If (AViewInfo.GridRecord.Values[ColUpdatesStatus.Index] = PLATSTAT_NEW) then begin
      ACanvas.Brush.Color := clSkyBlue;
      ACanvas.Font.Color := clBlack;
      ACanvas.Font.Style := [fsBold];
    end;
end;


procedure TConfigForm.UpdatePlats;   {QM-164 EB Plats Update}
var
  EnabledState: Boolean;
begin
  fPlatMgr.ResetAfterCancel;
  if (Owner is TForm) then  {Send Message to Main form that work has started}
      PostMessage(TForm(Owner).Handle, UM_PLATSSTART, 0, 0);  {prevent sync and Main menu clicks}
  if (DSUpdates.DataSet.State = dsEdit) then
    DSUpdates.DataSet.Post;
 try
   Screen.Cursor := crHourGlass;
   fPlatMgr.KillOtherApps;
   EnabledState := pgCtrlPlatsMgr.Enabled;
   pgCtrlPlatsMgr.Enabled := False;
   pnlBottom.Enabled := False;
   fPlatMgr.UpdatePlats(edUpdateLog);
   EmployeeDM.UpdateEmployee_PlatUpdate(Now);
   edtPlatsUpdated.Text := DateTimeToStr(EmployeeDM.GetLastPlatUpdate(DM.EmpID));
   edPlatsUpdated2.Caption := edtPlatsUpdated.Text;
 finally
   Screen.Cursor := crDefault;
   pgCtrlPlatsMgr.Enabled := EnabledState;
   if (Owner is TForm) then  {Send Message to Main form that work has finished}
      PostMessage(TForm(Owner).Handle, UM_PLATSDONE, 0, 0);  {unfreeze}
   pnlBottom.Enabled := True;
 end;
end;

procedure TConfigForm.CriticalTicketColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.CriticalTicketColor;
  if ColorDialog.Execute then
    DM.UQState.CriticalTicketColor := ColorDialog.Color;
end;

procedure TConfigForm.DoNotMarkTicketColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.DoNotMarkTicketColor;
  if ColorDialog.Execute then
    DM.UQState.DoNotMarkTicketColor := ColorDialog.Color;
end;

procedure TConfigForm.HighestGeocodePrecisionColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.HighestGeocodePrecisionColor;
  if ColorDialog.Execute then
    DM.UQState.HighestGeocodePrecisionColor := ColorDialog.Color;
end;



procedure TConfigForm.AcceptableGeocodePrecisionColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.AcceptableGeocodePrecisionColor;
  if ColorDialog.Execute then
    DM.UQState.AcceptableGeocodePrecisionColor := ColorDialog.Color;
end;

procedure TConfigForm.ZeroGeocodeMatchesColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := DM.UQState.ZeroGeocodeMatchesColor;
  if ColorDialog.Execute then
    DM.UQState.ZeroGeocodeMatchesColor := ColorDialog.Color;
end;

function TConfigForm.RequireSync: Boolean;
begin
  Result := False;
end;

procedure TConfigForm.ActivatingNow;

begin
  inherited;
  UpdateRights;
  SetOldTabsInvisible;
  DisplayComputerInfo;
  if not assigned(fPlatMgr) then begin
    fPlatMgr := TPlatMgr.Create(ConfigCompInfo.CompUser, ConfigCompInfo.ComputerName);
    fPlatMgr.AssignRichEdit(EdUpdateLog);
  end;
  if PermissionsDM.CanI(RightPlatsManagerUpdate) then begin
    tabPlats.TabVisible := True;
    ConfigPageCtrl.ActivePage := tabPlats;  {LEAVE IN: to fix DevEx issue, we have to toggle the activepage}
  end
  else begin
    tabPlats.TabVisible := False;
    ConfigPageCtrl.ActivePage := tabGeneral;
  end;

  {EB - Reset to General Tab}
  tabGeneral.Visible := True;
  ConfigPageCtrl.ActivePage := tabGeneral;
  DisplaySyncStatus;  //QM
end;

procedure TConfigForm.RefreshNow;
begin
  inherited;
  UpdateRights;
end;

procedure TConfigForm.ReloadTicketButtonClick(Sender: TObject);
const
  IncSync = True;
begin
  inherited;
  if DM.Engine.HavePendingTicketChanges then
  begin
    if MessageDlg('Your data contains changes that have not been synced to the'
        +sLinebreak+ ' server.  Are you sure you want to delete your local data?',
        mtWarning, [mbYes, mbNo], 0) <> mrYes then
      Exit;
  end;
  DM.ReloadTicketData(IncSync);     //QM-780 Delete Ticket Data     //QM-986 EB Reload Ticket Data
end;

procedure TConfigForm.UpdateRights;
begin
  lblConfigStatus.Visible := False;  //QM-805 EB Debug Refactor
  DiscardButton.Visible := DM.UQState.DeveloperMode;
  ReloadTicketButton.Visible := DM.UQState.DeveloperMode;
  TableData.Close;

  DM.Database1.Session.GetTableNames('DB1', TableName.Items);
  TableName.Items.Insert(0, '');
  TableName.ItemIndex := 0;

  TableName.Visible := DM.UQState.DeveloperMode;
  TableGrid.Visible := DM.UQState.DeveloperMode;
  GetCRCButton.Visible := DM.UQState.DeveloperMode;
  GetSQLButton.Visible := DM.UQState.DeveloperMode;
  ShowRightsButton.Visible := DM.UQState.DeveloperMode;

  {//QM-805 EB Refactor Debug}
  lblConfigStatus.Caption := '--';
  If (DM.UQState.DeveloperMode) then
    lblConfigStatus.Caption := 'Debug On';
  If (DM.UQState.SecondaryDebugOn) then
    lblConfigStatus.Caption := 'Debug 2 On';
  if lblConfigStatus.Caption <> '--' then
    lblConfigStatus.Visible := True;

end;

procedure TConfigForm.LeavingNow;
begin
  inherited;
  TableData.Close;
  FreeAndNil(fPlatMgr);
  tabPlats.Visible := False;
  tabPlats.TabVisible := False;
end;

procedure TConfigForm.ListPlatAge;
begin

end;

procedure TConfigForm.ChangePassswordButtonClick(Sender: TObject);
begin
  DM.RequestPasswordChange(False);
end;

procedure TConfigForm.GetCRCButtonClick(Sender: TObject);
var
  FileCRC: Cardinal;
  FileCRC64: Int64;
  CRCMsg: string;
begin
  if GetFileCRC32(Application.ExeName, FileCRC) then begin
    FileCRC64 := FileCRC;
    CRCMsg := Format('CRC32=%d', [FileCRC64]);
    Clipboard.AsText := CRCMsg;
    ShowMessage(CRCMsg);
  end else
    raise Exception.Create('Unable to get CRC32 value for: ' + Application.ExeName);
end;

procedure TConfigForm.TableNameChange(Sender: TObject);
var
  i: Integer;
  Column: TcxGridDBColumn;
begin
  if Trim(TableName.Text) = '' then
    TableData.Close
  else begin
    TableGrid.BeginUpdate;
    try
      TableData.SQL.Text := 'select * from ' + TableName.Text;
      TableData.Open;
      for i := 0 to TableData.FieldCount - 1 do
        if TableData.Fields[i].DisplayWidth > 35 then
          TableData.Fields[i].DisplayWidth := 35;

      TableView.DataController.KeyFieldNames := TableData.Fields[0].FieldName;
      TableView.ClearItems;
      TableView.DataController.CreateAllItems;
      for i := 0 to TableView.ColumnCount - 1 do begin
        Column := TableView.Columns[i];
        if Column.DataBinding.FieldName = 'active' then
          Column.Width := 38
        else if Column.DataBinding.FieldName = 'LocalStringKey' then
          Column.Width := 80
        else if Column.DataBinding.FieldName = 'LocalKey' then
          Column.Width := 55
        else if StrContainsText('_date', Column.DataBinding.FieldName) then
          Column.Width := 128;
      end;
    finally
      TableGrid.EndUpdate;
    end;
  end;
end;

procedure TConfigForm.GetSQLButtonClick(Sender: TObject);
var
  i: Integer;
  InsertSQL: string;
  SQL: string;
  KeyValue: Variant;

  function GetInsertSQL(Data: TDataSet; const TableName: string): string;
  const
    InsertStmt = 'insert into %s (%s) values (%s)' + sLineBreak;
    IgnoreFields = ';ticket.route_area_name;ticket.area_changed;ticket.area_changed_date;locate.locator_id;locate.StatusAsAssignedUser;';
  var
    j: Integer;
    Fields: string;
    Values: string;
    FieldName: string;
    FieldValue: string;
  begin
    for j := 0 to Data.FieldCount - 1 do begin
      FieldName := Data.Fields[j].FieldName;
      if (not StringInArray(FieldName, NeverSendFieldNames)) and (not StrContainsText(';'+TableName+'.'+FieldName+';', IgnoreFields)) then begin
        if IsEmpty(Fields) then
          Fields := FieldName
        else
          Fields := Fields + ', ' + FieldName;
        FieldValue := FieldToSQLValue(Data.Fields[j]);
        if SameText(FieldValue, 'null') and SameText(TableName, 'ticket') and SameText(FieldName, 'revision') then
          FieldValue := '1';
        if IsEmpty(Values) then
          Values := FieldValue
        else
          Values := Values + ', ' + FieldValue;
      end;
    end;
    Result := Format(InsertStmt, [TableName, Fields, Values])
  end;

begin
  TableData.DisableControls;
  try
    SQL := 'SET IDENTITY_INSERT ' + TableName.Text + ' ON' + sLineBreak;
    for i := 0 to TableView.Controller.SelectedRecordCount - 1 do begin
      KeyValue := TableView.DataController.GetRecordId(TableView.Controller.SelectedRecords[i].RecordIndex);
      if not TableData.Locate(TableData.Fields[0].FieldName, KeyValue, []) then
        raise Exception.Create('Record Not Found: ' + VarToString(KeyValue));
      InsertSQL := GetInsertSQL(TableData, TableName.Text);
      SQL := SQL + InsertSQL;
      if SameText(TableName.Text, 'ticket') then begin
        SQL := SQL + 'SET IDENTITY_INSERT ticket OFF' + sLineBreak;
        SQL := SQL + 'SET IDENTITY_INSERT locate ON'  + sLineBreak;
        ChildTable.SQL.Text := 'select * from locate where ticket_id = ' + TableData.FieldByName('ticket_id').AsString;
        RefreshDataSet(ChildTable);
        while not ChildTable.Eof do begin
          SQL := SQL + GetInsertSQL(ChildTable, 'locate');
          ChildTable.Next;
        end;
        SQL := SQL + 'SET IDENTITY_INSERT locate OFF' + sLineBreak;
        SQL := SQL + 'SET IDENTITY_INSERT ticket ON'  + sLineBreak;
      end;
    end;
    SQL := SQL + 'SET IDENTITY_INSERT ' + TableName.Text + ' OFF';
    Clipboard.AsText := SQL;
  finally
    TableData.EnableControls
  end;
end;

procedure TConfigForm.GetUpdateList;
begin
  fPlatMgr.GetPlatList(dsPlats);
  fPlatMgr.GetUpdateList(dsUpdates);
end;

procedure TConfigForm.AddDriveLetterButtonClick(Sender: TObject);

  procedure ValidateDriveLetter(DriveLetter: string);
  begin
    if (Length(DriveLetter) <> 1) or (not (DriveLetter[1] in ['A'..'Z'])) then
      raise EOdDataEntryError.Create(DriveLetter + ' is not a valid drive letter (Valid Examples: F, H, etc.)');
  end;

  procedure AddDriveLetterToIni(const DriveLetter: string);
  begin
    if DriveLettersListBox.Items.IndexOf(DriveLetter) = -1 then begin
      DriveLettersListBox.Items.Add(DriveLetter);
      DM.UQState.DriveLettersForAttachment := DriveLettersListBox.Items.CommaText;
    end;
  end;

begin
  ValidateDriveLetter(UpperCase(DriveLetter.Text));
  AddDriveLetterToIni(UpperCase(DriveLetter.Text));
  DriveLetter.Text := '';
end;

procedure TConfigForm.RemoveDriveLetterButtonClick(Sender: TObject);
begin
  if DriveLettersListBox.ItemIndex = -1 then
    raise EOdDataEntryError.Create('Please select a drive letter from the list.');

  DriveLettersListBox.Items.Delete(DriveLettersListBox.ItemIndex);
  DM.UQState.DriveLettersForAttachment := DriveLettersListBox.Items.CommaText;
end;

procedure TConfigForm.FormCreate(Sender: TObject);
begin
  DriveLettersListBox.Items.CommaText := DM.UQState.DriveLettersForAttachment;
end;

procedure TConfigForm.SetOldTabsInvisible;
begin
    tabTicketColors.TabVisible := False;
end;

procedure TConfigForm.ShowRightsButtonClick(Sender: TObject);
var
  Data: TDataSet;
  Rights: TStringList;
  RightID: Integer;
  RightName: string;
  RightDesc: string;
  Limitation: string;
begin
  Rights := nil;
  Data := DM.Engine.OpenQuery('select * from right_definition order by right_description');
  try
    Rights := TStringList.Create;
    while not Data.Eof do begin
      RightID := Data.FieldByName('right_id').AsInteger;
      RightDesc := Data.FieldByName('right_description').AsString;
      RightName := Data.FieldByName('entity_data').AsString;
      if PermissionsDM.CanI(RightName) then begin
        Limitation := PermissionsDM.GetLimitation(RightName);
        Rights.Add(Format('%s: [%s, %d] (%s)', [RightDesc, RightName, RightID, Limitation]));
      end;
      Data.Next;
    end;
    ShowMessageAckDialog(Rights.Text, 'OK', 'Granted Rights List');
  finally
    FreeAndNil(Data);
    FreeAndNil(Rights);
  end;
end;

procedure TConfigForm.ShowUpdateLabels(ShowIt: Boolean);
begin
  edtUpdateErrors.Visible := ShowIt;
  lblUpdateErrors.Visible := ShowIt;
  lblUpdated.Visible := ShowIt;
  edtUpdated.Visible := ShowIt;
end;

end.

