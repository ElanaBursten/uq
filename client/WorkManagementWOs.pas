unit WorkManagementWOs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableWorkMgtGridFrame, cxGraphics, cxControls, cxLookAndFeels,
  cxGridTableView, cxLookAndFeelPainters, cxStyles, cxClasses, cxGridLevel, cxGrid,
  StdCtrls, ExtCtrls, cxCustomData, OdDbUtils,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView, WorkManageUtils,
  cxGridDBTableView, cxGridCustomView, cxMaskEdit, dbisamtb, OdDBISAMUtils,
  cxNavigator, DBCtrls, ActnList, Menus, ComCtrls, OdContainer,
  cxFilter, cxData;

type
  TWorkMgtWOsFrame = class(TExpandableWorkMgtGridFrameBase)
    WOGridViewRepository: TcxGridViewRepository;
    WorkOrderListSource: TDataSource;
    ColWOWorkOrderNumber: TcxGridDBColumn;
    ColWOKind: TcxGridDBColumn;
    ColWODueDate: TcxGridDBColumn;
    ColWOWorkOrderDate: TcxGridDBColumn;
    ColWOWorkAddressNumber: TcxGridDBColumn;
    ColWOStreet: TcxGridDBColumn;
    ColWOCity: TcxGridDBColumn;
    ColWOCounty: TcxGridDBColumn;
    ColWOState: TcxGridDBColumn;
    ColWOMapPage: TcxGridDBColumn;
    ColWOMapRef: TcxGridDBColumn;
    ColWOClient: TcxGridDBColumn;
    ColWOStatusCode: TcxGridDBColumn;
    ColWOWorkOrderStatus: TcxGridDBColumn;
    ColWOID: TcxGridDBColumn;
    WorkOrderView: TcxGridDBTableView;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    WOSumWONumber: TDBText;
    WOSumStatus: TDBText;
    WOSumAddress: TDBText;
    WOSumClientWONumber: TDBText;
    Label35: TLabel;
    WOSumTransmitDate: TDBText;
    Label36: TLabel;
    WOSumDueDate: TDBText;
    WoSumWorkType: TDBText;
    WOSumClientMON: TDBText;
    WOSumWireCenter: TDBText;
    Label37: TLabel;
    WOSumCity: TDBText;
    Label38: TLabel;
    WOSumContactPhone: TDBText;
    Label39: TLabel;
    WOSumJobNum: TDBText;
    Label40: TLabel;
    Label41: TLabel;
    WOSumClosedDate: TDBText;
    Label42: TLabel;
    WOSumWorkCenter: TDBText;
    Label43: TLabel;
    WOSumCallerName: TDBText;
    Label44: TLabel;
    WOSumCentralOffice: TDBText;
    Label45: TLabel;
    WOSumServingTerm: TDBText;
    Label46: TLabel;
    WOSumCircuitNum: TDBText;
    Label47: TLabel;
    WOSumF2Cable: TDBText;
    WOSumTermPort: TDBText;
    Label48: TLabel;
    Label49: TLabel;
    WOSumF2Pair: TDBText;
    Label50: TLabel;
    Label51: TLabel;
    WOSumRoadBores: TDBText;
    Label52: TLabel;
    WOSumDrivewayBores: TDBText;
    WOSumStateHwyROW: TDBText;
    WOSumWorkState: TDBText;
    WOSumWorkStreet: TDBText;
    WOSumWorkDesc: TDBMemo;
    RefreshWorkOrderButton: TButton;
    WorkOrder: TDBISAMTable;
    WorkOrderSource: TDataSource;
    ColWOWorkOrderStatusDescription: TcxGridDBColumn;
    SendMessageGroupBox: TGroupBox;
    WOAssignedToID: TDBText;
    SendMessageButton: TButton;
    MessageLabel: TLabel;
    MessageNumberEdit: TEdit;
    MessageStatus: TLabel;
    CGAMessage: TMemo;
    SendMessageAction: TAction;
    cxStyleRepoTickets: TcxStyleRepository;
    BottomGridStyle: TcxStyle;
    GridSelectionStyle: TcxStyle;
    UnfocusedGridSelStyle: TcxStyle;
    IndicatorStyle: TcxStyle;
    procedure WorkOrderViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure WorkOrderViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure RefreshWorkOrderButtonClick(Sender: TObject);
    procedure BaseGridEnter(Sender: TObject);
    procedure ItemListTableAfterOpen(DataSet: TDataSet);
    procedure SendMessageActionExecute(Sender: TObject);
    procedure ActionListFrameUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure WorkOrderViewCustomDrawIndicatorCell(Sender: TcxGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxCustomGridIndicatorItemViewInfo;
      var ADone: Boolean);
    procedure WorkOrderViewSelectionChanged(Sender: TcxCustomGridTableView);
    procedure WorkOrderViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FWorkOrderColumns: TColumnList;
    procedure CustomizeWorkOrderList(DataSet: TDBISAMTable; Event: TCustomizeEvent);

  protected
    function ItemIDColumn: TcxGridColumn; override;
    procedure SetGridAndViewEvents; override;
    procedure WMDestroy(var Msg: TWMDestroy); message WM_DESTROY;
  public
    PrevSelList: TIntegerList;  //QMANTWO-690 EB
    procedure RefreshCaptionCounts; override;
    procedure ReassignSelectedWorkOrders(SelectedLocatorID, LocatorID: Integer);
    procedure CreateWorkOrderListTable;
    function LoadWorkOrderList(SelectedLocatorID, GetNumTicketDays: Integer;
      NodeIsUnderMe,ShowOpenWorkOnly: Boolean): Boolean;
    procedure UpdateFrameData(const SourceDataSet: TDataSet); override;
    procedure ShowSelectedItem; override;
    procedure ShowWorkOrder;
    procedure UpdateItemInCache; override;
    procedure SendCGAMessage;
    procedure ShowMessageStatus(ShowMessage: Boolean);
    procedure CreateStructures;    //QMANTWO-690 EB
    procedure DestroyStructures;   //QMANTWO-690 EB
    procedure LoadSelList;         //QMANTWO-690 EB
    procedure MoveToWOID(GoToWOID: Integer); //QMANTWO-690 EB
//    procedure ClearSelectionsDBText;
  end;

var
  WorkMgtWOsFrame: TWorkMgtWOsFrame;

implementation

uses QMServerLibrary_Intf, DMu, QMConst, MSXML2_TLB, OdXmlToDataSet, UQUtils,
     ServiceLink, OdHourglass, DateUtils, OdMiscUtils, LocalEmployeeDMu, ClipBrd;

{$R *.dfm}

{ TWorkMgtWOsFrame }

procedure TWorkMgtWOsFrame.RefreshCaptionCounts;
begin
  inherited;
{ If/when the Ticket Goals feature is implemented for Work Orders, make visible and set these labels:
  TodayCountLabelSummary.Caption :=
  TodayCountLabelDataSummary.Caption :=
  TomorrowCountLabelSummary.Caption :=
  TomorrowCountLabelDataSummary.Caption :=
  UnscheduledCountLabelSummary.Caption :=
  UnscheduledCountLabelDataSummary.Caption :=}
end;

procedure TWorkMgtWOsFrame.UpdateItemInCache;
begin
  DM.UpdateWorkOrderInCache(SelectedItemID);
end;

procedure TWorkMgtWOsFrame.RefreshWorkOrderButtonClick(Sender: TObject);
begin
  inherited;
  RefreshItem;
end;

procedure TWorkMgtWOsFrame.ActionListFrameUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  SendMessageButton.Enabled := NotEmpty(CGAMessage.Text);
end;

procedure TWorkMgtWOsFrame.BaseGridEnter(Sender: TObject);
begin
  inherited;
  GridState.InitialDisplay := False;  //QMANTWO-690 EB
end;



//procedure TWorkMgtWOsFrame.ClearSelectionsDBText;
//var
//  i : integer;
//begin
//  {Clear any other "selection"}
//  for i := 0 to PanelDisplaySelectedItem.ControlCount - 1 do begin
//    if PanelDisplaySelectedItem.Controls[i] is TDBText then begin
//      (PanelDisplaySelectedItem.Controls[i] as TDBText).Transparent := True;
//      (PanelDisplaySelectedItem.Controls[i] as TDBText).Color := PanelDisplaySelectedItem.Color;
//    end;
//  end;
//  GridState.BottomItemHighlighted := False;
//end;


procedure TWorkMgtWOsFrame.CreateStructures;
begin
  PrevSelList:= TIntegerList.Create; //QMANTWO-690 EB
  GridState.CheckSelection:= False;
  GridState.LastOpenedID:= 0;
end;

procedure TWorkMgtWOsFrame.CreateWorkOrderListTable;
begin
  FWorkOrderColumns := TColumnList.Create;
  with FWorkOrderColumns do begin
    SetDefaultTable('TMWorkOrderList');
    AddColumn('WorkOrderID', 55, 'wo_id', ftInteger, '', 0, True, False);
    AddColumn('Work Order Number', 20, 'wo_number', ftString);
    AddColumn('Kind', 40, 'kind', ftString);
    AddColumn('Client Code', 10, 'oc_code', ftString);
    AddColumn('Client Name', 60, 'client_name', ftString);
    AddColumn('Due Date', 126, 'due_date', ftDateTime);
    AddColumn('Work Order Date', 126, 'transmit_date', ftDateTime);
    AddColumn('Closed Date', 126, 'closed_date', ftDateTime);
    AddColumn('Work Address Number', 10, 'work_address_number', ftString);
    AddColumn('Work Address Street', 60, 'work_address_street', ftString);
    AddColumn('County', 40, 'work_county', ftString);
    AddColumn('City', 40, 'work_city', ftString);
    AddColumn('State', 2, 'work_state', ftString);
    AddColumn('Zip', 10, 'work_zip', ftString);
    AddColumn('Work Description', 3500, 'work_description', ftString);
    AddColumn('Work Type', 90, 'work_type', ftString);
    AddColumn('Map Page', 30, 'map_page', ftString);
    AddColumn('Map Grid', 30, 'map_ref', ftString);
    AddColumn('Status Code', 5, 'status', ftString);
    AddColumn('Tickets', 55, 'ticket_count', ftInteger, '', 0);
    AddColumn('Visible', 20, 'work_order_is_visible', ftBoolean);
    AddColumn('Modified Date', 126, 'modified_date', ftDateTime);
    AddColumn('Status', 10, 'status_description', ftString);
  end;
  if (DM.UQState.SecondaryDebugOn) and (DM.UQState.DebugWorkMgmtDebug) then  //QM-805 EB             //QM-338 EB - Fields Available to view in Debug Mode
    ColWOID.VisibleForCustomization := True;
end;

function TWorkMgtWOsFrame.ItemIDColumn: TcxGridColumn;
begin
  Result := ColWOID;
end;

procedure TWorkMgtWOsFrame.LoadSelList;
var
  SelRecord: TcxCustomGridRecord;
  i : integer;
begin
  {EB QMANTWO-690 Multi-Select Tweak}
    PrevSelList.Clear;
    for i := 0 to WorkOrderView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := WorkOrderView.Controller.SelectedRecords[i];
      PrevSelList.Add(SelRecord.Index);
    end;
end;

function TWorkMgtWOsFrame.LoadWorkOrderList(SelectedLocatorID,
  GetNumTicketDays: Integer; NodeIsUnderMe, ShowOpenWorkOnly: Boolean): Boolean;
var
  Doc: IXMLDomDocument;
begin
  FPopulatingWorkSummary := True;
  try
    if (SelectedLocatorID<=0) or not NodeIsUnderMe then begin
      ClearListTable;
    end else begin
      Doc := GetWorkOrderListFromServer(SelectedLocatorID, GetNumTicketDays, ShowOpenWorkOnly);

      BaseGrid.BeginUpdate;
      try
        ConvertXMLToDataSet(Doc, FWorkOrderColumns, ListTable, CustomizeWorkOrderList);
        if SelectedItemID = 0 then
          WorkOrderView.DataController.GotoFirst
        else
          WorkOrderView.DataController.LocateByKey(SelectedItemID);
      finally
        FBlockItemUpdates := True;
        if WorkOrderView.DataController.RecordCount > 0 then begin   //QM-338 EB
          WorkOrderView.OptionsCustomize.ColumnsQuickCustomization := True;
          BaseGrid.Enabled := True;
        end
        else begin
          BaseGrid.Enabled := False;
          WorkOrderView.OptionsCustomize.ColumnsQuickCustomization := False;

        end;
        try
          BaseGrid.EndUpdate;
        finally
          FBlockItemUpdates := False;
        end;
      end;
    end;
  finally
    FPopulatingWorkSummary := False;
  end;

  //Close the detail dataset in preparation for selecting first row
  DetailTable.Close;
  Result := HasRecords(ListTable);
  if Result then begin
    SelectFirstGridRow;
    GridState.InitialDisplay := True;
  end;
end;

procedure TWorkMgtWOsFrame.MoveToWOID(GoToWOID: Integer);
var
  i: integer;
begin
  {QMANTWO-690 EB Allows us to move back to the last opened work order}
  for i := 0 to WorkOrderView.ViewData.RecordCount - 1 do
  If (GoToWOID = WorkOrderView.ViewData.Records[i].Values[ItemIDColumn.ID]) then begin
    WorkOrderView.ViewData.Records[i].Focused := True;
    WorkOrderView.ViewData.Records[i].Selected := True;
    Exit;
  end;
end;

procedure TWorkMgtWOsFrame.CustomizeWorkOrderList(DataSet: TDBISAMTable; Event: TCustomizeEvent);
begin
  if Event = ceFieldDefs then begin
    DataSet.FieldDefs.Add('work_order_status', ftInteger, 0);
  end else if Event = ceDataRow then begin
    DataSet.FieldByName('work_order_status').Value := Byte(GetWorkOrderStatusFromRecord(DataSet));
  end;
end;

procedure TWorkMgtWOsFrame.DestroyStructures;
begin
  FreeAndNil(PrevSelList);
end;

procedure TWorkMgtWOsFrame.ReassignSelectedWorkOrders(SelectedLocatorID, LocatorID: Integer);
var
  SelRecord: TcxCustomGridRecord;
  i: Integer;
  WorkOrders: QMServerLibrary_Intf.IntegerList;
  WorkOrderID: Integer;
begin
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeMoveWorkOrder, IntToStr(WorkOrderView.Controller.SelectedRecordCount) + ' moved');
  WorkOrders := QMServerLibrary_Intf.IntegerList.Create;
  for i := 0 to WorkOrderView.Controller.SelectedRecordCount - 1 do begin
    SelRecord := WorkOrderView.Controller.SelectedRecords[i];
    if SelRecord.IsData then begin
      WorkOrderID := SelRecord.Values[ColWOID.Index];
      if WorkOrderID > 0 then
        WorkOrders.Add(WorkOrderID);
    end;
  end;

  if WorkOrders.Count > 0 then begin
    DM.CallingServiceName('MoveWorkOrders');
    DM.Engine.Service.MoveWorkOrders(DM.Engine.SecurityInfo, WorkOrders, SelectedLocatorID, LocatorID);
  end;
end;

procedure TWorkMgtWOsFrame.SetGridAndViewEvents;
begin
  inherited;
  WorkOrderView.OnDblClick := ViewDblClick;
  WorkOrderView.OnFocusedRecordChanged := ViewFocusedRecordChanged;
  BaseGrid.OnEnter := BaseGridEnter;
  RefreshWorkOrderButton.OnClick := RefreshWorkOrderButtonClick;
end;

procedure TWorkMgtWOsFrame.ShowMessageStatus(ShowMessage: Boolean);
begin
  CGAMessage.Visible := Not ShowMessage;
  MessageLabel.Visible := ShowMessage;
  MessageStatus.Visible := ShowMessage;
  MessageNumberEdit.Visible := ShowMessage;
  SendMessageButton.Visible := Not ShowMessage;

  if not ShowMessage then
    CGAMessage.Clear;
end;

procedure TWorkMgtWOsFrame.ShowSelectedItem;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  ShowWorkOrder;
  SendMessageGroupBox.Visible := WOSumStatus.Caption = 'CGA';
  ShowMessageStatus(not SendMessageGroupBox.Visible);
  GridState.LastDisplayedID := GetSelectedItemID;   //QMANTWO-690
  ClearSelectionsDBText; //QMANTWO-577 EB
end;

procedure TWorkMgtWOsFrame.ShowWorkOrder;
begin
  if (BaseGrid.Dragging) then
    Exit;
  DetailTable.DisableControls;
  try
    if SelectedItemID <> 0 then begin
      if HaveFocusedItem then begin
        // Hack: Use a RO call without a progress dialog to allow drag and drop without focus loss
        GetChan.ShowProgress := False;
        try
          DM.UpdateWorkOrderInCache(SelectedItemID);
        finally
          GetChan.ShowProgress := True;
        end;
      end;

      DetailTable.Open;
      DetailTable.Refresh;
      DetailTable.SetRange([SelectedItemID], [SelectedItemID]);

      if HaveFocusedItem then begin
        // Update the GUI/grid in case any values just changed
        UpdateFrameData(DetailTable);
      end;
    end;
  finally
    DetailTable.EnableControls;
  end;
end;

procedure TWorkMgtWOsFrame.UpdateFrameData(const SourceDataSet: TDataSet);
var
  i: Integer;
  FieldName: string;
begin
  Assert(ListTable.FieldByName('wo_id').AsInteger = SourceDataSet.FieldByName('wo_id').AsInteger,
    Format('Expected work_order_list.wo_id=%d but it is %d',
    [SourceDataSet.FieldByName('wo_id').AsInteger,
    ListTable.FieldByName('wo_id').AsInteger]));

  FBlockItemUpdates := True;
  EditDataSet(ListTable);
  for i := 0 to ListTable.Fields.Count - 1 do begin
    FieldName := ListTable.Fields[i].FieldName;
    if Assigned(SourceDataSet.FindField(FieldName)) then
      ListTable.FieldByName(FieldName).Value := SourceDataSet.FieldByName(FieldName).Value;
  end;
  ListTable.Post; // Saves to memory only
  FBlockItemUpdates := False;
end;

procedure TWorkMgtWOsFrame.WMDestroy(var Msg: TWMDestroy);
begin
  if (csDestroying in ComponentState) then
    FreeAndNil(FWorkOrderColumns);
  inherited;
end;

procedure TWorkMgtWOsFrame.ItemListTableAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SetFieldDisplayFormat(DataSet, 'due_date', 'mmm d, yyyy t');
  SetFieldDisplayFormat(DataSet, 'transmit_date', 'mmm d, yyyy t');
end;

procedure TWorkMgtWOsFrame.WorkOrderViewCellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  //The reason Showing[Ticket|WO|Damage]Summary checks are there is to prevent
  //excessive fetching of the ticket|damage|wo details from the server. If the
  //summary is already showing the selected row, then there is no need to sync
  //those details down again.
  if (not ShowingSummary) and (Assigned(Sender.Controller)) then
    ViewFocusedRecordChanged(Sender, Sender.Controller.FocusedRecord, Sender.Controller.FocusedRecord, False);

  AHandled := False;
end;

procedure TWorkMgtWOsFrame.WorkOrderViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  Status: TWorkOrderStatus;
begin
  if not AViewInfo.Selected then begin
    if not VarIsNull(AViewInfo.GridRecord.Values[ColWOWorkOrderStatus.Index]) then
      Status := TWorkOrderStatus(Byte(AViewInfo.GridRecord.Values[ColWOWorkOrderStatus.Index]))
    else
      Status := [];
    ACanvas.Brush.Color := GetWorkOrderColor(Status, (AViewInfo.GridRecord.Index mod 2) = 1);
  end;
  ADone := False;
end;

procedure TWorkMgtWOsFrame.WorkOrderViewCustomDrawIndicatorCell(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
begin
  inherited;
  {Grid Indicator highlighting}
  if (AViewInfo is TcxGridIndicatorRowItemViewInfo) then begin
     If TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Selected then begin
        ACanvas.Brush.Color := clYellow;
     end
     else if TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.focused then
       ACanvas.Brush.Color := clSkyBlue;
  end;
end;

procedure TWorkMgtWOsFrame.WorkOrderViewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  FocusedRec: TcxCustomGridRecord;
  ShouldBeSelected: boolean;

begin
////Elana - Do no call this explicitly (it should only be called when the mouse is clicked on Grid)
 // inherited;
 if HaveFocusedItem then begin
      FocusedRec := ListView.Controller.FocusedRecord;
      If not PrevSelList.Has(FocusedRec.Index) then
        ShouldBeSelected := True;  //QMANTWO-690 EB
 end
 else  ShouldBeSelected := False;  //QM-876 EB Code Cleanup

  if ((ssCtrl in Shift) or (ssShift in Shift)) then begin   //QMANTWO-690

     If (not WorkOrderView.Controller.FocusedRecord.Selected) and ShouldBeSelected then      //QMANTWO-690 EB
        WorkOrderView.Controller.FocusedRecord.Selected := True;
  end
  else begin
    WorkOrderView.Controller.ClearSelection;
    WorkOrderView.Controller.FocusedRecord.Selected := True;
  end;
  GridState.CheckSelection := True;
end;

procedure TWorkMgtWOsFrame.WorkOrderViewSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  inherited;
  if (not GridState.InitialDisplay) and (GridState.LastDisplayedID <> GetSelectedItemID) then begin   //QMANTWO-690
    ShowSelectedItem;
  end;

end;

//procedure TWorkMgtWOsFrame.ItemDblClick(Sender: TObject);
//begin
//  inherited;
//  if Sender is TDBText then
//    CopyDBText(Sender as TDBText);
//  GridState.BottomItemHighlighted := True;
//end;

procedure TWorkMgtWOsFrame.SendCGAMessage;
var
  Destination,
  MessageNumber,
  MsgSubject: string;
  EmpID: integer;
  ExpDate: TDateTime;
begin
   if NotEmpty(CGAMessage.Text) then begin
     MessageNumber := 'WO' + FormatDateTime('yyyymmddhhnnss', Now);
     EmpId := StrToInt(WOAssignedToID.Caption);
     Destination := EmployeeDM.GetEmployeeShortName(EmpID);
     MsgSubject := 'Inspection Followup for WO # ' + WOSumWONumber.Caption;
     ExpDate := IncDay(Now, 10);
     MessageNumberEdit.Text := MessageNumber;
     MessageStatus.Caption := DM.SendMessage(MessageNumber, Destination, MsgSubject, CGAMessage.Text, Now, ExpDate, EmpID, True);
   end;
end;

procedure TWorkMgtWOsFrame.SendMessageActionExecute(Sender: TObject);
begin
  inherited;
  SendCGAMessage;
  ShowMessageStatus(True);
end;

end.
