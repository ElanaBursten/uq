inherited LocatesCompletedSummaryReportForm: TLocatesCompletedSummaryReportForm
  Left = 460
  Top = 204
  Caption = 'Locates Completed Summary'
  ClientHeight = 440
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 1
    Top = 87
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object WorkDatesLabel: TLabel [1]
    Left = 0
    Top = 8
    Width = 60
    Height = 13
    Caption = 'Work Dates:'
  end
  object EmpTypesLabel: TLabel [2]
    Left = 1
    Top = 118
    Width = 82
    Height = 13
    Caption = 'Employee Types:'
  end
  object ManagerCombo: TComboBox [3]
    Left = 93
    Top = 85
    Width = 204
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  inline WorkDateRange: TOdRangeSelectFrame [4]
    Left = 47
    Top = 21
    Width = 273
    Height = 63
    TabOrder = 1
    inherited ToLabel: TLabel
      Left = 142
    end
    inherited DatesComboBox: TComboBox
      Left = 46
      Width = 204
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 46
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 164
    end
  end
  object EmployeeTypeList: TCheckListBox
    Left = 93
    Top = 118
    Width = 227
    Height = 315
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 2
  end
  object SelectAllButton: TButton
    Left = 326
    Top = 118
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 3
    OnClick = SelectAllButtonClick
  end
  object SelectNoneButton: TButton
    Left = 326
    Top = 148
    Width = 75
    Height = 25
    Caption = 'Select None'
    TabOrder = 4
    OnClick = SelectNoneButtonClick
  end
end
