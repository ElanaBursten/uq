object ContactNameDialog: TContactNameDialog
  Left = 347
  Top = 358
  BorderStyle = bsDialog
  Caption = 'Ongoing Ticket Contact Name'
  ClientHeight = 249
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object FirstNameLabel: TLabel
    Left = 18
    Top = 66
    Width = 51
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'First Name'
  end
  object LastNameLabel: TLabel
    Left = 216
    Top = 67
    Width = 50
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Last Name'
  end
  object Label3: TLabel
    Left = 18
    Top = 10
    Width = 383
    Height = 41
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    AutoSize = False
    Caption = 
      'This call center requires that you collect a contact name for al' +
      'l tickets that go into the ongoing state.  Please enter the firs' +
      't and last name of the contact person for this ongoing ticket.  ' +
      'The contact data is required to save the ticket.'
    WordWrap = True
  end
  object Label1: TLabel
    Left = 18
    Top = 115
    Width = 105
    Height = 13
    Caption = 'Scheduled Mark Delay'
  end
  object Label2: TLabel
    Left = 33
    Top = 139
    Width = 90
    Height = 13
    Caption = 'Contact DateTime:'
  end
  object Label4: TLabel
    Left = 46
    Top = 92
    Width = 50
    Height = 13
    Caption = 'Phone No:'
  end
  object FirstNameEdit: TEdit
    Left = 74
    Top = 64
    Width = 125
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 0
    OnChange = NameEditChange
  end
  object LastNameEdit: TEdit
    Left = 274
    Top = 64
    Width = 125
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 1
    OnChange = NameEditChange
  end
  object OKButton: TButton
    Left = 124
    Top = 164
    Width = 75
    Height = 25
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 223
    Top = 164
    Width = 74
    Height = 25
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object dpSchedMarkDSate: TDateTimePicker
    Left = 150
    Top = 115
    Width = 147
    Height = 21
    Date = 42047.638694699080000000
    Time = 42047.638694699080000000
    TabOrder = 4
    OnChange = dpSchedMarkDSateChange
  end
  object DateTimePicker1: TDateTimePicker
    Left = 150
    Top = 139
    Width = 147
    Height = 21
    Date = 42047.638694699080000000
    Time = 42047.638694699080000000
    TabOrder = 5
    OnChange = dpSchedMarkDSateChange
  end
  object edtPhone: TMaskEdit
    Left = 102
    Top = 89
    Width = 97
    Height = 21
    EditMask = '!\(999\)000-0000;1;_'
    MaxLength = 13
    TabOrder = 6
    Text = '(   )   -    '
  end
  object ComboBox1: TComboBox
    Left = 130
    Top = 208
    Width = 118
    Height = 21
    ItemHeight = 13
    TabOrder = 7
    Text = 'ComboBox1'
  end
end
