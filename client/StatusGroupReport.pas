unit StatusGroupReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase;

type
  TStatusGroupReportForm = class(TReportBaseForm)
  protected
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

{ TStatusGroupReportForm }

procedure TStatusGroupReportForm.ValidateParams;
begin
  inherited;
  SetReportID('StatusGroups');
end;

end.
