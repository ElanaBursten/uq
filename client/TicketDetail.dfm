inherited TicketDetailForm: TTicketDetailForm
  Left = 440
  Top = 195
  Caption = 'Ticket Detail'
  ClientHeight = 508
  ClientWidth = 1032
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TicketPages: TPageControl
    Left = 0
    Top = 34
    Width = 1032
    Height = 474
    ActivePage = ImageTab
    Align = alClient
    Images = SharedImagesDM.Images
    TabOrder = 0
    OnChange = TicketPagesChange
    OnChanging = TicketPagesChanging
    object DetailsTab: TTabSheet
      Caption = 'Details'
      ImageIndex = -1
      object DetailsPanel: TPanel
        Left = 0
        Top = 0
        Width = 1024
        Height = 216
        Margins.Left = 0
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object CrossStreetLabel: TLabel
          Left = 322
          Top = 74
          Width = 64
          Height = 13
          Caption = 'Cross Street:'
          Transparent = True
        end
        object ContractorLabel: TLabel
          Left = 3
          Top = 124
          Width = 56
          Height = 13
          Caption = 'Contractor:'
          Transparent = True
        end
        object WorkDoneForLabel: TLabel
          Left = 3
          Top = 109
          Width = 76
          Height = 13
          Caption = 'Work Done For:'
          Transparent = True
        end
        object ContactLabel: TLabel
          Left = 3
          Top = 141
          Width = 42
          Height = 13
          Caption = 'Contact:'
          Transparent = True
        end
        object AltContactLabel: TLabel
          Left = 3
          Top = 157
          Width = 62
          Height = 13
          Caption = 'Alt. Contact:'
          Transparent = True
        end
        object WorkAddressLabel: TLabel
          Left = 3
          Top = 74
          Width = 71
          Height = 13
          Caption = 'Work Address:'
          Transparent = True
        end
        object WorkToBeDone: TLabel
          Left = 3
          Top = 19
          Width = 85
          Height = 13
          Caption = 'Work to be Done:'
          Transparent = True
        end
        object MapPageLabel: TLabel
          Left = 322
          Top = 157
          Width = 51
          Height = 13
          Caption = 'Map Page:'
          Transparent = True
        end
        object WorkDateLabel: TLabel
          Left = 322
          Top = 109
          Width = 81
          Height = 13
          Caption = 'Work Date/Time:'
          Transparent = True
        end
        object TicketReceivedLabel: TLabel
          Left = 322
          Top = 124
          Width = 79
          Height = 13
          Caption = 'Ticket Received:'
          Transparent = True
        end
        object Company: TDBText
          Left = 90
          Top = 109
          Width = 53
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'company'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object CallerAltContact: TDBText
          Left = 90
          Top = 157
          Width = 112
          Height = 13
          Color = clBtnFace
          DataField = 'caller_altcontact'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object CallDate: TDBText
          Left = 409
          Top = 124
          Width = 47
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'call_date'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object Address: TDBText
          Left = 157
          Top = 59
          Width = 159
          Height = 13
          Color = clBtnFace
          DataField = 'work_address_street'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Visible = False
          OnDblClick = ItemDblClick
        end
        object ConName: TDBText
          Left = 90
          Top = 124
          Width = 53
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'con_name'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object CallerContact: TDBText
          Left = 90
          Top = 141
          Width = 112
          Height = 13
          Color = clBtnFace
          DataField = 'caller_contact'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object CallerPhone: TDBText
          Left = 213
          Top = 141
          Width = 90
          Height = 13
          Color = clBtnFace
          DataField = 'caller_phone'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object CallerAltPhone: TDBText
          Left = 213
          Top = 157
          Width = 90
          Height = 13
          Color = clBtnFace
          DataField = 'caller_altphone'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object MapPage: TDBText
          Left = 409
          Top = 157
          Width = 130
          Height = 13
          Color = clBtnFace
          DataField = 'map_page'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object WorkCross: TDBText
          Left = 409
          Top = 74
          Width = 217
          Height = 13
          Color = clBtnFace
          DataField = 'work_cross'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object City: TDBText
          Left = 90
          Top = 90
          Width = 124
          Height = 13
          Color = clBtnFace
          DataField = 'work_city'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object DueDate: TDBText
          Left = 94
          Top = -1
          Width = 49
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'due_date'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object State: TDBText
          Left = 220
          Top = 90
          Width = 37
          Height = 13
          Color = clBtnFace
          DataField = 'work_state'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object County: TDBText
          Left = 256
          Top = 90
          Width = 250
          Height = 13
          Color = clBtnFace
          DataField = 'work_county'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object WorkAddressNumber: TDBText
          Left = 86
          Top = 59
          Width = 66
          Height = 17
          Color = clBtnFace
          DataField = 'work_address_number'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Visible = False
          OnDblClick = ItemDblClick
        end
        object WorkType: TDBText
          Left = 90
          Top = 18
          Width = 695
          Height = 17
          Color = clBtnFace
          DataField = 'work_type'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          WordWrap = True
          OnDblClick = ItemDblClick
        end
        object WorkDate: TDBText
          Left = 409
          Top = 109
          Width = 57
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'work_date'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object Due: TLabel
          Left = 65
          Top = -1
          Width = 23
          Height = 13
          Caption = 'Due:'
          Transparent = True
        end
        object LatitudeLabel: TLabel
          Left = 554
          Top = 141
          Width = 43
          Height = 13
          Caption = 'Latitude:'
          Transparent = True
        end
        object LongitudeLabel: TLabel
          Left = 554
          Top = 157
          Width = 51
          Height = 13
          Caption = 'Longitude:'
          Transparent = True
        end
        object Latitude: TDBText
          Left = 609
          Top = 141
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Color = clBtnFace
          DataField = 'work_lat'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object Longitude: TDBText
          Left = 609
          Top = 157
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Color = clBtnFace
          DataField = 'work_long'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object IDLabel: TLabel
          Left = 791
          Top = 157
          Width = 15
          Height = 13
          Caption = 'ID:'
        end
        object AreaNameLabel: TLabel
          Left = 322
          Top = 141
          Width = 57
          Height = 13
          Caption = 'Area Name:'
          Transparent = True
        end
        object CenterLabel: TLabel
          Left = 791
          Top = 173
          Width = 37
          Height = 13
          Caption = 'Center:'
        end
        object FTTPLabel: TLabel
          Left = 621
          Top = 173
          Width = 27
          Height = 13
          Caption = 'FTTP'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          OnDblClick = ItemDblClick
        end
        object AreaName: TDBText
          Left = 409
          Top = 141
          Width = 123
          Height = 13
          DataField = 'route_area_name'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object DetailFollowupTicketTypeLabel: TLabel
          Left = 299
          Top = -1
          Width = 104
          Height = 13
          Caption = 'Followup Ticket Type:'
          Transparent = True
        end
        object DetailFollowupTicketType: TLabel
          Left = 413
          Top = -1
          Width = 112
          Height = 13
          Caption = 'FollowupTicketType'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object FTTPStaticLabel: TLabel
          Left = 554
          Top = 173
          Width = 28
          Height = 13
          Caption = 'FTTP:'
        end
        object lblLastScheduledDate: TLabel
          Left = 3
          Top = 173
          Width = 102
          Height = 13
          Caption = 'Last Scheduled Date:'
        end
        object dbLastSMD: TDBText
          Left = 111
          Top = 173
          Width = 63
          Height = 13
          AutoSize = True
          Color = clBtnFace
          DataField = 'LastSMD'
          DataSource = LastSMDSource
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          OnDblClick = ItemDblClick
        end
        object lblProjectLocatesText: TLabel
          Left = 3
          Top = 188
          Width = 157
          Height = 13
          Caption = '** Contains Project Locates'
          Color = 13499135
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object CombinedAddressLbl: TLabel
          Left = 791
          Top = 188
          Width = 118
          Height = 13
          Caption = 'CombinedAddressLbl'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowFrame
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Visible = False
          OnDblClick = ItemDblClick
        end
        object lblRS: TLabel
          Left = 251
          Top = -1
          Width = 22
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = '*RS'
          Color = 13499135
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          Visible = False
        end
        object lblLocateMessage: TLabel
          Left = 3
          Top = 201
          Width = 10
          Height = 13
          Caption = '--'
          Color = 13499135
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Label13: TLabel
          Left = 596
          Top = -1
          Width = 76
          Height = 13
          Caption = 'Last Risk Score:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object RiskScore: TDBText
          Left = 683
          Top = -1
          Width = 56
          Height = 13
          AutoSize = True
          DataField = 'risk_score'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object UpdatedTicketsLabel: TLabel
          Left = 596
          Top = 23
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Updated Tickets:'
          Transparent = True
          WordWrap = True
        end
        object UpdateRelatedTickets: TDBText
          Left = 692
          Top = 22
          Width = 106
          Height = 16
          Color = clBtnFace
          DataField = 'update_of'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold, fsUnderline]
          ParentColor = False
          ParentFont = False
          PopupMenu = updTicketPopupMenu
          OnDblClick = UpdateRelatedTicketsDblClick
        end
        object StreetLabel: TLabel
          Left = 88
          Top = 74
          Width = 66
          Height = 13
          Caption = 'StreetLabel'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnDblClick = ItemDblClick
        end
        object btnReturn: TBitBtn
          Left = 683
          Top = 16
          Width = 94
          Height = 25
          Caption = 'Return to Ticket'
          TabOrder = 7
          OnClick = btnReturnClick
        end
        object StatusAsAssignedLocatorBox: TCheckBox
          Left = 322
          Top = 186
          Width = 153
          Height = 17
          Caption = 'Status As Assigned Locator'
          TabOrder = 0
          OnClick = StatusAsAssignedLocatorBoxClick
        end
        object TicketID: TDBEdit
          Left = 843
          Top = 158
          Width = 97
          Height = 12
          BevelInner = bvNone
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'ticket_id'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          OnDblClick = ItemDblClick
        end
        object CallCenter: TDBEdit
          Left = 843
          Top = 173
          Width = 97
          Height = 17
          BevelInner = bvNone
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'ticket_format'
          DataSource = TicketDS
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          OnDblClick = ItemDblClick
        end
        object RouteAreaID: TComboBox
          Left = 409
          Top = 137
          Width = 139
          Height = 21
          DropDownCount = 14
          ItemHeight = 13
          TabOrder = 3
        end
        object btnMap: TBitBtn
          Left = 704
          Top = 147
          Width = 41
          Height = 19
          Caption = 'Coord'
          TabOrder = 4
          OnClick = btnMapClick
        end
        object btnAddr: TBitBtn
          Left = 704
          Top = 88
          Width = 41
          Height = 19
          Caption = 'Addr'
          TabOrder = 5
          OnClick = btnMapClick
        end
        object EdtDueDate: TcxDBDateEdit
          Left = 90
          Top = -4
          DataBinding.DataField = 'due_date'
          DataBinding.DataSource = TicketDS
          Enabled = False
          ParentFont = False
          Properties.DateButtons = [btnToday]
          Properties.Kind = ckDateTime
          Properties.ReadOnly = True
          Properties.OnCloseUp = EdtDueDatePropertiesCloseUp
          Style.Color = 13499135
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          TabOrder = 6
          Visible = False
          Width = 183
        end
        object PreserveClosedDateCBox: TCheckBox
          Left = 322
          Top = 200
          Width = 210
          Height = 17
          Caption = 'Preserve Closed Date on Status Change'
          TabOrder = 9
        end
        object FlowPanel1: TPanel
          Left = 704
          Top = 41
          Width = 94
          Height = 48
          TabOrder = 8
          VerticalAlignment = taAlignBottom
          Visible = False
          object btnWaldoAddr: TSpeedButton
            Left = 0
            Top = 0
            Width = 51
            Height = 25
            Margins.Right = 0
            AllowAllUp = True
            Caption = 'Waldo'
            Layout = blGlyphRight
            OnClick = btnWaldoAddrClick
          end
          object btnWaldoLatLng: TSpeedButton
            Left = 0
            Top = 24
            Width = 51
            Height = 25
            Caption = 'Waldo  '
            Spacing = 0
            OnClick = btnWaldoAddrClick
          end
          object Label18: TLabel
            Left = 54
            Top = 6
            Width = 39
            Height = 13
            Caption = 'Address'
          end
          object Label19: TLabel
            Left = 54
            Top = 30
            Width = 34
            Height = 13
            Caption = 'Coords'
          end
        end
      end
      object FooterPanel: TPanel
        Left = 0
        Top = 413
        Width = 1024
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          1024
          32)
        object AddLocateButton: TButton
          Left = 1
          Top = 4
          Width = 90
          Height = 25
          Caption = 'Add a Locate...'
          Enabled = False
          TabOrder = 0
          OnClick = AddLocateButtonClick
        end
        object HoursUnitsButton: TButton
          Left = 97
          Top = 4
          Width = 90
          Height = 25
          Caption = 'Hours/Units...'
          Enabled = False
          TabOrder = 1
          OnClick = HoursUnitsButtonClick
        end
        object PlatButton: TButton
          Left = 193
          Top = 4
          Width = 90
          Height = 25
          Caption = 'Plat...'
          Enabled = False
          TabOrder = 2
          OnClick = LocatePlatButtonClick
        end
        object PlatAddinButton: TButton
          Left = 486
          Top = 4
          Width = 120
          Height = 25
          Caption = '(Addin Button2)'
          TabOrder = 3
          Visible = False
        end
        object CENButtonPanel: TPanel
          Left = 289
          Top = 2
          Width = 93
          Height = 29
          Color = clRed
          ParentBackground = False
          TabOrder = 4
          Visible = False
          object CENButton: TButton
            Left = 2
            Top = 2
            Width = 90
            Height = 25
            Caption = 'TEMA'
            TabOrder = 0
            OnClick = CENButtonClick
          end
        end
        object btnReturnToWO: TButton
          Left = 829
          Top = 4
          Width = 191
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Return to WO#: '
          TabOrder = 5
          Visible = False
          WordWrap = True
          OnClick = btnReturnToWOClick
        end
        object EFDButtonPanel: TPanel
          Left = 388
          Top = 2
          Width = 93
          Height = 29
          Color = clRed
          ParentBackground = False
          TabOrder = 6
          Visible = False
          object btnEfdColl: TButton
            Left = 2
            Top = 2
            Width = 90
            Height = 25
            Caption = 'EFD Coll'
            TabOrder = 0
            OnClick = btnEfdCollClick
          end
        end
        object btnProjectLocate: TButton
          Left = 612
          Top = 4
          Width = 120
          Height = 25
          Caption = 'Add a Project Locate'
          TabOrder = 7
          Visible = False
          OnClick = btnProjectLocateClick
        end
        object btnPelican: TButton
          Left = 390
          Top = 4
          Width = 90
          Height = 25
          Caption = 'One Call Map'
          TabOrder = 8
          OnClick = btnPelicanClick
        end
      end
      object FunctionKeyPanel: TPanel
        Left = 0
        Top = 216
        Width = 0
        Height = 197
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object F5Label: TLabel
          Left = 8
          Top = 21
          Width = 12
          Height = 13
          Caption = 'F5'
          Transparent = True
        end
        object F6Label: TLabel
          Left = 8
          Top = 38
          Width = 12
          Height = 13
          Caption = 'F6'
          Transparent = True
        end
        object F7Label: TLabel
          Left = 8
          Top = 55
          Width = 12
          Height = 13
          Caption = 'F7'
          Transparent = True
        end
        object F8Label: TLabel
          Left = 8
          Top = 72
          Width = 12
          Height = 13
          Caption = 'F8'
          Transparent = True
        end
        object F9Label: TLabel
          Left = 8
          Top = 89
          Width = 12
          Height = 13
          Caption = 'F9'
          Transparent = True
        end
      end
      object LocateGrid: TcxGrid
        Left = 0
        Top = 216
        Width = 1024
        Height = 197
        Align = alClient
        TabOrder = 3
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        object LocateGridView: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          OnCellDblClick = LocateGridViewCellDblClick
          OnCustomDrawCell = LocateGridViewCustomDrawCell
          OnEditing = LocateGridViewEditing
          OnEditValueChanged = LocateGridViewEditValueChanged
          OnFocusedItemChanged = LocateGridViewFocusedItemChanged
          OnFocusedRecordChanged = LocateGridViewFocusedRecordChanged
          DataController.DataSource = LocateDS
          DataController.KeyFieldNames = 'locate_id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnVertSizing = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.InvertSelect = False
          OptionsView.ShowEditButtons = gsebForFocusedRecord
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Styles.Selection = SharedDevExStyleData.SkyBlueHighlightSelection
          Bands = <
            item
            end>
          object ColLocAlert2: TcxGridDBBandedColumn
            Caption = 'Alert'
            DataBinding.FieldName = 'alert2'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            OnCustomDrawCell = ColLocAlert2CustomDrawCell
            MinWidth = 60
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 60
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object ColLocClientCode: TcxGridDBBandedColumn
            Caption = 'Client Code'
            DataBinding.FieldName = 'client_code'
            Visible = False
            MinWidth = 64
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object ColLocClientName: TcxGridDBBandedColumn
            Caption = 'Client'
            DataBinding.FieldName = 'client_name'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            MinWidth = 333
            Options.Editing = False
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 333
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object ColParentClientCode: TcxGridDBBandedColumn
            Caption = 'Proj Parent'
            DataBinding.FieldName = 'parent_code'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Visible = False
            Options.Editing = False
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 87
            Position.BandIndex = 0
            Position.ColIndex = 8
            Position.RowIndex = 0
          end
          object ColLocClosed: TcxGridDBBandedColumn
            Caption = 'Closed'
            DataBinding.FieldName = 'closed'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ReadOnly = True
            MinWidth = 34
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 34
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object ColLocStatus: TcxGridDBBandedColumn
            Caption = 'Status'
            DataBinding.FieldName = 'status'
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.AutoSearchOnPopup = False
            Properties.AutoSelect = False
            Properties.DropDownAutoSize = True
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownSizeable = True
            Properties.DropDownWidth = 40
            Properties.View = StatusListView
            Properties.KeyFieldNames = 'status'
            Properties.ListFieldItem = ColStatusListStatus
            Properties.OnCloseUp = ColLocStatusPropertiesCloseUp
            Properties.OnInitPopup = ColLocStatusPropertiesInitPopup
            MinWidth = 60
            Width = 60
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 1
          end
          object ColLocMarkType: TcxGridDBBandedColumn
            Caption = 'Mark Type'
            DataBinding.FieldName = 'mark_type'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownWidth = 60
            Properties.Items.Strings = (
              'NEW MARK'
              'NO MARK'
              'RE-MARK')
            Properties.ReadOnly = False
            MinWidth = 90
            Width = 90
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 1
          end
          object ColLocLengthMarked: TcxGridDBBandedColumn
            Caption = 'Len Marked'
            DataBinding.FieldName = 'length_marked'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.PopupAutoSize = False
            Properties.PopupControl = LocateLengthEditor.LocateLengthDetailPanel
            Properties.PopupHeight = 165
            Properties.PopupMinHeight = 165
            Properties.PopupMinWidth = 200
            Properties.PopupSizeable = False
            Properties.PopupSysPanelStyle = True
            Properties.PopupWidth = 200
            Properties.OnChange = ColLocLengthMarkedPropertiesChange
            Properties.OnCloseUp = ColLocLengthMarkedPropertiesCloseUp
            Properties.OnInitPopup = ColLocLengthMarkedPropertiesInitPopup
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 89
            Width = 89
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 1
          end
          object ColLocQtyMarked: TcxGridDBBandedColumn
            Caption = 'Qty Marked'
            DataBinding.FieldName = 'qty_marked'
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 73
            Width = 73
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 1
          end
          object ColLocLengthTotal: TcxGridDBBandedColumn
            Caption = 'Len Total'
            DataBinding.FieldName = 'length_total'
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = True
            HeaderAlignmentHorz = taRightJustify
            MinWidth = 82
            Options.Editing = False
            Width = 82
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 1
          end
          object ColLocClosedDate: TcxGridDBBandedColumn
            Caption = 'When Closed'
            DataBinding.FieldName = 'closed_date'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            MinWidth = 122
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 122
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object ColLocShortName: TcxGridDBBandedColumn
            Caption = 'Assigned To'
            DataBinding.FieldName = 'short_name'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            MinWidth = 217
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 217
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object ColLocHighProfile: TcxGridDBBandedColumn
            Caption = 'High Profile'
            DataBinding.FieldName = 'high_profile'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Properties.OnValidate = ColLocHighProfilePropertiesValidate
            MinWidth = 34
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 1
          end
          object ColLocHighProfileReason: TcxGridDBBandedColumn
            Caption = 'High Prof Reason'
            DataBinding.FieldName = 'high_profile_reason'
            PropertiesClassName = 'TcxExtLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownRows = 10
            Properties.DropDownSizeable = True
            Properties.View = HPReasonView
            Properties.KeyFieldNames = 'ref_id'
            Properties.ListFieldItem = ColHPReasCode
            Visible = False
            MinWidth = 122
            Options.ShowEditButtons = isebNever
            Width = 122
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 1
          end
          object ColLocHighProfilePopup: TcxGridDBBandedColumn
            Caption = 'High Profile Reasons'
            DataBinding.FieldName = 'high_profile_multi'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = ReasonCheckListPanel
            Properties.OnCloseUp = ColLocHighProfilePopupPropertiesCloseUp
            Properties.OnInitPopup = ColLocHighProfilePopupPropertiesInitPopup
            MinWidth = 122
            Options.ShowEditButtons = isebNever
            Width = 122
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 1
          end
          object ColLocLocatePlats: TcxGridDBBandedColumn
            Caption = 'Plats'
            DataBinding.FieldName = 'locate_plats'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = ColLocLocatePlatsPropertiesButtonClick
            MinWidth = 217
            Options.ShowEditButtons = isebNever
            Width = 217
            Position.BandIndex = 0
            Position.ColIndex = 8
            Position.RowIndex = 1
          end
          object ColLocLocateID: TcxGridDBBandedColumn
            Caption = 'Locate ID'
            DataBinding.FieldName = 'locate_id'
            Visible = False
            MinWidth = 64
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object ColProject: TcxGridDBBandedColumn
            Caption = 'Parent'
            DataBinding.FieldName = 'parent_code'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Visible = False
            Options.Editing = False
            Styles.Content = SharedDevExStyleData.GrayBackgroundStyle
            Width = 52
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 0
          end
        end
        object LocateGridLevel: TcxGridLevel
          GridView = LocateGridView
        end
      end
    end
    object ImageTab: TTabSheet
      Caption = 'Image'
      ImageIndex = -1
      DesignSize = (
        1024
        445)
      object TicketImageMemo: TMemo
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
      object cxHyperLinkEdit1: TcxHyperLinkEdit
        Left = 3
        Top = 359
        Anchors = [akLeft, akBottom]
        Properties.SingleClick = True
        Properties.UsePrefix = upNever
        TabOrder = 1
        Visible = False
        Width = 513
      end
    end
    object WorkDescriptionTab: TTabSheet
      Caption = 'Work Description'
      ImageIndex = -1
      inline TicketWorkTabFrame: TTicketWorkFrame
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        TabOrder = 0
        inherited ButtonPanel: TPanel
          Top = 404
          Width = 1024
          Visible = False
          DesignSize = (
            1024
            41)
          inherited CloseButton: TButton
            Left = 941
            Visible = False
          end
        end
        inherited WorkToDoBrowser: TWebBrowser
          Width = 1024
          Height = 404
          ControlData = {
            4C000000D5690000C12900000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object FacilityTab: TTabSheet
      Caption = 'Facilities'
      ImageIndex = -1
      OnShow = FacilityTabShow
      object TopFacilityPanel: TPanel
        Left = 0
        Top = 0
        Width = 1024
        Height = 172
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object NoFacilityEditLabel: TLabel
          Left = 336
          Top = 6
          Width = 183
          Height = 39
          Caption = 
            'You need to save and sync the newly added locate before editing ' +
            'facility data.'
          Visible = False
          WordWrap = True
        end
        object AddFacilityPanel: TPanel
          Left = 0
          Top = 0
          Width = 325
          Height = 172
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 8
            Top = 8
            Width = 31
            Height = 13
            Caption = 'Client:'
          end
          object Label2: TLabel
            Left = 8
            Top = 35
            Width = 58
            Height = 13
            Caption = 'Utility Type:'
          end
          object Label3: TLabel
            Left = 8
            Top = 62
            Width = 42
            Height = 13
            Caption = 'Material:'
          end
          object SizeLabel: TLabel
            Left = 8
            Top = 89
            Width = 23
            Height = 13
            Caption = 'Size:'
          end
          object Label6: TLabel
            Left = 8
            Top = 116
            Width = 46
            Height = 13
            Caption = 'Pressure:'
          end
          object Label4: TLabel
            Left = 8
            Top = 144
            Width = 54
            Height = 13
            Caption = 'How Many:'
          end
          object FacilityClient: TComboBox
            Left = 71
            Top = 5
            Width = 226
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnSelect = FacilityClientSelect
          end
          object FacilityType: TComboBox
            Left = 71
            Top = 32
            Width = 142
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = FacilityTypeChange
          end
          object FacilityMaterial: TComboBox
            Left = 71
            Top = 59
            Width = 142
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            TabOrder = 2
          end
          object FacilitySize: TComboBox
            Left = 71
            Top = 86
            Width = 142
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            TabOrder = 3
          end
          object FacilityPressure: TComboBox
            Left = 71
            Top = 113
            Width = 142
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            TabOrder = 4
          end
          object AddHowMany: TSpinEdit
            Left = 71
            Top = 140
            Width = 56
            Height = 22
            MaxValue = 10
            MinValue = 1
            TabOrder = 7
            Value = 1
          end
          object AddFacilityButton: TButton
            Left = 138
            Top = 138
            Width = 80
            Height = 25
            Caption = 'Add Facility'
            TabOrder = 6
            OnClick = AddFacilityButtonClick
          end
          object DeleteFacilityButton: TButton
            Left = 227
            Top = 138
            Width = 80
            Height = 25
            Caption = 'Delete Facility'
            TabOrder = 5
            OnClick = DeleteFacilityButtonClick
          end
        end
      end
      object TicketFacilitiesPanel: TPanel
        Left = 0
        Top = 172
        Width = 1024
        Height = 273
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 4
        TabOrder = 1
        object FacilityGrid: TcxGrid
          Left = 4
          Top = 4
          Width = 1016
          Height = 265
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          object FacilityGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = True
            Navigator.Buttons.PriorPage.Visible = True
            Navigator.Buttons.Prior.Visible = True
            Navigator.Buttons.Next.Visible = True
            Navigator.Buttons.NextPage.Visible = True
            Navigator.Buttons.Last.Visible = True
            Navigator.Buttons.Insert.Visible = True
            Navigator.Buttons.Append.Visible = False
            Navigator.Buttons.Delete.Visible = True
            Navigator.Buttons.Edit.Visible = True
            Navigator.Buttons.Post.Visible = True
            Navigator.Buttons.Cancel.Visible = True
            Navigator.Buttons.Refresh.Visible = True
            Navigator.Buttons.SaveBookmark.Visible = True
            Navigator.Buttons.GotoBookmark.Visible = True
            Navigator.Buttons.Filter.Visible = True
            DataController.DataSource = LocateFacilityDS
            DataController.KeyFieldNames = 'locate_facility_id'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.HeaderAutoHeight = True
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            object ColFacClientName: TcxGridDBColumn
              Caption = 'Client'
              DataBinding.FieldName = 'client_name'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 155
            end
            object ColFacFacilityType: TcxGridDBColumn
              Caption = 'Type'
              DataBinding.FieldName = 'facility_type'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 104
            end
            object ColFacMaterial: TcxGridDBColumn
              Caption = 'Material'
              DataBinding.FieldName = 'facility_material'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 103
            end
            object ColFacFacilitySize: TcxGridDBColumn
              Caption = 'Size'
              DataBinding.FieldName = 'facility_size'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 106
            end
            object ColFacFacilityPressure: TcxGridDBColumn
              Caption = 'Pressure'
              DataBinding.FieldName = 'facility_pressure'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Width = 116
            end
            object ColFacOffset: TcxGridDBColumn
              Caption = 'Number of Offsets'
              DataBinding.FieldName = 'offset'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.AssignedValues.MinValue = True
              Properties.MaxValue = 99.000000000000000000
              HeaderAlignmentHorz = taRightJustify
              Options.ShowEditButtons = isebAlways
              Width = 116
            end
            object ColFacAddedDate: TcxGridDBColumn
              Caption = 'Date Added'
              DataBinding.FieldName = 'added_date'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              OnGetDataText = ColFacAddedDateGetDataText
              Width = 128
            end
            object ColFacLocateFacilityID: TcxGridDBColumn
              Caption = 'Locate Facility ID'
              DataBinding.FieldName = 'locate_facility_id'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.ReadOnly = True
              Visible = False
              HeaderAlignmentHorz = taRightJustify
              Width = 33
            end
            object ColFacLocateID: TcxGridDBColumn
              Caption = 'Locate ID'
              DataBinding.FieldName = 'locate_id'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.ReadOnly = True
              Visible = False
              HeaderAlignmentHorz = taRightJustify
              Options.Editing = False
              Width = 62
            end
            object ColFacAddedBy: TcxGridDBColumn
              Caption = 'Added By'
              DataBinding.FieldName = 'added_by'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Visible = False
              OnGetDisplayText = AssignColAddedByGetDisplayText
              Options.Editing = False
              Width = 65
            end
            object ColFacDeletedByName: TcxGridDBColumn
              Caption = 'Deleted By'
              DataBinding.FieldName = 'deleted_by_name'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Visible = False
              Options.Editing = False
              Width = 61
            end
            object ColFacDeletedDate: TcxGridDBColumn
              Caption = 'Deleted Date'
              DataBinding.FieldName = 'deleted_date'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Visible = False
              Width = 51
            end
            object ColFacActive: TcxGridDBColumn
              Caption = 'Active'
              DataBinding.FieldName = 'active'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Visible = False
              Width = 85
            end
          end
          object FacilityGridLevel: TcxGridLevel
            GridView = FacilityGridView
          end
        end
      end
    end
    object NotesTab: TTabSheet
      Caption = 'Notes'
      ImageIndex = -1
      object NotesPanel: TPanel
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        TabOrder = 0
        inline TicketNotesFrame: TBaseNotesFrame
          Left = 1
          Top = 43
          Width = 1022
          Height = 401
          Align = alClient
          TabOrder = 1
          OnClick = TicketNotesFrameClick
          inherited NotesGrid: TcxGrid
            Width = 1022
            Height = 254
            inherited NotesGridView: TcxGridDBTableView
              inherited ColNotesEntryDate: TcxGridDBColumn [2]
                Width = 123
              end
              inherited ColNotesAddedByName: TcxGridDBColumn [3]
                Width = 85
              end
              inherited ColNotesUID: TcxGridDBColumn [4]
              end
              inherited ColNotesActive: TcxGridDBColumn [5]
              end
              inherited ColNotesNote: TcxGridDBColumn [6]
              end
              inherited ColNotesForeignID: TcxGridDBColumn [7]
                VisibleForCustomization = False
                Width = 163
              end
              inherited ColRestriction: TcxGridDBColumn [8]
              end
              inherited ColNotesSource: TcxGridDBColumn
                Visible = True
                Width = 163
              end
            end
          end
          inherited HeaderPanel: TPanel
            Width = 1022
            inherited NoteLabel: TLabel
              Top = -2
            end
            inherited NoteText: TMemo
              Left = 36
              Top = 0
              Height = 134
            end
            inherited AddNoteButton: TButton
              Top = 0
              OnClick = TicketNotesFrameAddNoteButtonClick
            end
          end
          inherited FooterPanel: TPanel
            Top = 391
            Width = 1022
          end
        end
        object NoteHeaderPanel: TPanel
          Left = 1
          Top = 1
          Width = 1022
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EnterNoteLabel: TLabel
            Left = 44
            Top = 22
            Width = 125
            Height = 13
            AutoSize = False
            Caption = 'Enter Note for:'
            Transparent = True
            WordWrap = True
          end
          object Label17: TLabel
            Left = 278
            Top = 4
            Width = 111
            Height = 13
            Caption = 'Restriction (Sub Type):'
            WordWrap = True
          end
          object lblEnterNoteFor: TLabel
            Left = 36
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Enter Note for:'
            WordWrap = True
          end
          object NoteSource: TComboBox
            Left = 37
            Top = 17
            Width = 236
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnChange = NoteSourceChange
            Items.Strings = (
              'Ticket'
              'Locate 1'
              'Locate 2'
              'Locate 3')
          end
          object NoteSubTypeCombo: TComboBox
            Left = 277
            Top = 17
            Width = 243
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = NoteSubTypeComboChange
            Items.Strings = (
              'Ticket'
              'Locate 1'
              'Locate 2'
              'Locate 3')
          end
        end
      end
    end
    object HistoryTab: TTabSheet
      Caption = 'History'
      ImageIndex = -1
      object HistPanel: TPanel
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 5
        TabOrder = 0
        object HistoryPages: TPageControl
          Left = 5
          Top = 5
          Width = 1014
          Height = 445
          ActivePage = TicketVersionsTab
          Align = alClient
          TabOrder = 0
          OnResize = HistoryPagesResize
          object LocateStatusTab: TTabSheet
            Caption = 'Locate Status'
            object LocateStatusPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object LocateStatusGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 399
                Align = alClient
                TabOrder = 0
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object LocateStatusGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = LocateStatusSource
                  DataController.KeyFieldNames = 'locate_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object colLocStLocateID: TcxGridDBColumn
                    Caption = 'Locate ID'
                    DataBinding.FieldName = 'locate_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 85
                  end
                  object colLocStClientCode: TcxGridDBColumn
                    Caption = 'Client Code'
                    DataBinding.FieldName = 'client_code'
                    Visible = False
                    Options.Editing = False
                    Width = 86
                  end
                  object colLocStClientName: TcxGridDBColumn
                    Caption = 'Client'
                    DataBinding.FieldName = 'client_name'
                    Options.Editing = False
                    Width = 157
                  end
                  object colLocStStatusDate: TcxGridDBColumn
                    Caption = 'Statused'
                    DataBinding.FieldName = 'status_date'
                    Options.Editing = False
                    Width = 118
                  end
                  object colLocStStatus: TcxGridDBColumn
                    Caption = 'Status'
                    DataBinding.FieldName = 'status'
                    Width = 37
                  end
                  object colLocStMarkType: TcxGridDBColumn
                    Caption = 'Mark Type'
                    DataBinding.FieldName = 'mark_type'
                    Width = 77
                  end
                  object colLocStHighProfile: TcxGridDBColumn
                    Caption = 'High Profile'
                    DataBinding.FieldName = 'high_profile'
                    Width = 60
                  end
                  object colLocStQtyMarked: TcxGridDBColumn
                    Caption = 'Qty'
                    DataBinding.FieldName = 'qty_marked'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Width = 24
                  end
                  object colLocStStatusedBy: TcxGridDBColumn
                    Caption = 'Statused ID'
                    DataBinding.FieldName = 'statused_by'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 64
                  end
                  object colLocStShortName: TcxGridDBColumn
                    Caption = 'Statused By'
                    DataBinding.FieldName = 'short_name'
                    OnGetDisplayText = AssignColAddedByGetDisplayText
                    Options.Editing = False
                    Width = 113
                  end
                  object colLocStStatusedHow: TcxGridDBColumn
                    Caption = 'Statused'
                    DataBinding.FieldName = 'statused_how'
                    Options.Editing = False
                    Width = 65
                  end
                  object colLocStEntryDate: TcxGridDBColumn
                    Caption = 'Entered'
                    DataBinding.FieldName = 'entry_date'
                    Width = 118
                  end
                  object colLocStInsertDate: TcxGridDBColumn
                    Caption = 'Modified'
                    DataBinding.FieldName = 'insert_date'
                    Width = 118
                  end
                  object colLocStRegularHours: TcxGridDBColumn
                    DataBinding.FieldName = 'regular_hours'
                    Visible = False
                  end
                  object colLocStOvertimeHours: TcxGridDBColumn
                    DataBinding.FieldName = 'overtime_hours'
                    Visible = False
                  end
                  object colLocStStatusChangedBy: TcxGridDBColumn
                    Caption = 'Changed By ID'
                    DataBinding.FieldName = 'status_changed_by_id'
                  end
                  object colLocStChangedByNAME: TcxGridDBColumn
                    Caption = 'Changed By'
                    DataBinding.FieldName = 'changedby_name'
                  end
                end
                object LocateStatusGridLevel: TcxGridLevel
                  GridView = LocateStatusGridView
                end
              end
            end
          end
          object AssignmentsTab: TTabSheet
            Caption = 'Assignments'
            ImageIndex = 1
            object AssignmentsPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object AssignmentsGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 399
                Align = alClient
                TabOrder = 0
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object AssignmentsGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = AssignmentSource
                  DataController.KeyFieldNames = 'assignment_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object AssignColAssignmentID: TcxGridDBColumn
                    Caption = 'Assignment ID'
                    DataBinding.FieldName = 'assignment_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 105
                  end
                  object AssignColLocateID: TcxGridDBColumn
                    Caption = 'Locate ID'
                    DataBinding.FieldName = 'locate_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 86
                  end
                  object AssignColClientName: TcxGridDBColumn
                    Caption = 'Client'
                    DataBinding.FieldName = 'client_name'
                    Options.Editing = False
                    Width = 119
                  end
                  object AssignColClientCode: TcxGridDBColumn
                    Caption = 'Client Code'
                    DataBinding.FieldName = 'client_code'
                    Visible = False
                    Options.Editing = False
                  end
                  object AssignColLocatorID: TcxGridDBColumn
                    Caption = 'Locator ID'
                    DataBinding.FieldName = 'locator_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Width = 70
                  end
                  object AssignColstatus: TcxGridDBColumn
                    DataBinding.FieldName = 'status'
                    Visible = False
                  end
                  object AssignColDownloadDate: TcxGridDBColumn
                    DataBinding.FieldName = 'download_date'
                    Visible = False
                  end
                  object AssignColShortName: TcxGridDBColumn
                    Caption = 'Locator'
                    DataBinding.FieldName = 'short_name'
                    Width = 102
                  end
                  object AssignColInsertDate: TcxGridDBColumn
                    Caption = 'Assigned'
                    DataBinding.FieldName = 'insert_date'
                    Options.Editing = False
                    Width = 172
                  end
                  object AssignColAddedBy: TcxGridDBColumn
                    Caption = 'Added By'
                    DataBinding.FieldName = 'added_by'
                    OnGetDisplayText = AssignColAddedByGetDisplayText
                    Options.Editing = False
                    Width = 75
                  end
                  object AssignColActive: TcxGridDBColumn
                    Caption = 'Active'
                    DataBinding.FieldName = 'active'
                    Options.Editing = False
                    Width = 67
                  end
                  object AssignColWorkLoadDate: TcxGridDBColumn
                    Caption = 'Workload Date'
                    DataBinding.FieldName = 'workload_date'
                    Options.Editing = False
                    Width = 172
                  end
                end
                object AssignmentsGridLevel: TcxGridLevel
                  GridView = AssignmentsGridView
                end
              end
            end
          end
          object ResponsesTab: TTabSheet
            Caption = 'Responses'
            ImageIndex = 3
            OnResize = ResponsesTabResize
            object ResponsesSplitter: TSplitter
              Left = 0
              Top = 194
              Width = 1006
              Height = 8
              Cursor = crVSplit
              Align = alBottom
              Beveled = True
            end
            object ResponsesPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 194
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object ResponsesHeader: TPanel
                Left = 4
                Top = 4
                Width = 998
                Height = 22
                Align = alTop
                Alignment = taLeftJustify
                BevelOuter = bvNone
                Caption = 'Fax / Modem Responses'
                TabOrder = 0
              end
              object ResponsesGrid: TcxGrid
                Left = 4
                Top = 26
                Width = 998
                Height = 164
                Align = alClient
                TabOrder = 1
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object ResponsesGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = ResponseSource
                  DataController.KeyFieldNames = 'response_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object RespColResponseID: TcxGridDBColumn
                    Caption = 'Response ID'
                    DataBinding.FieldName = 'response_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 85
                  end
                  object RespColLocateID: TcxGridDBColumn
                    Caption = 'Locate ID'
                    DataBinding.FieldName = 'locate_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 83
                  end
                  object RespColClientCode: TcxGridDBColumn
                    Caption = 'Client Code'
                    DataBinding.FieldName = 'client_code'
                    Visible = False
                    Options.Editing = False
                  end
                  object RespColClientName: TcxGridDBColumn
                    Caption = 'Client'
                    DataBinding.FieldName = 'client_name'
                    Options.Editing = False
                    Width = 136
                  end
                  object RespColResponseDate: TcxGridDBColumn
                    Caption = 'Response Date'
                    DataBinding.FieldName = 'response_date'
                    Width = 118
                  end
                  object RespColCallCenter: TcxGridDBColumn
                    Caption = 'Call Center'
                    DataBinding.FieldName = 'call_center'
                    Width = 93
                  end
                  object RespColStatus: TcxGridDBColumn
                    Caption = 'Status'
                    DataBinding.FieldName = 'status'
                    Options.Editing = False
                    Width = 51
                  end
                  object RespColResponseSent: TcxGridDBColumn
                    Caption = 'Response Sent'
                    DataBinding.FieldName = 'response_sent'
                    Options.Editing = False
                    Width = 118
                  end
                  object RespColSuccess: TcxGridDBColumn
                    Caption = 'Success'
                    DataBinding.FieldName = 'success'
                    Options.Editing = False
                    Width = 54
                  end
                  object RespColReply: TcxGridDBColumn
                    Caption = 'Reply'
                    DataBinding.FieldName = 'reply'
                    Options.Editing = False
                    Width = 125
                  end
                end
                object ResponsesGridLevel: TcxGridLevel
                  GridView = ResponsesGridView
                end
              end
            end
            object EmailPanel: TPanel
              Left = 0
              Top = 202
              Width = 1006
              Height = 205
              Align = alBottom
              BorderWidth = 4
              TabOrder = 1
              object EmailHeader: TPanel
                Left = 5
                Top = 5
                Width = 996
                Height = 22
                Align = alTop
                Alignment = taLeftJustify
                BevelOuter = bvNone
                Caption = 'Email Notifications'
                TabOrder = 0
              end
              object EmailsGrid: TcxGrid
                Left = 5
                Top = 27
                Width = 996
                Height = 70
                Align = alTop
                TabOrder = 1
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                RootLevelOptions.DetailTabsPosition = dtpTop
                object EmailsGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = EmailsGridViewCellClick
                  DataController.DataSource = EmailResultsSource
                  DataController.KeyFieldNames = 'eqr_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColEmailEqrID: TcxGridDBColumn
                    Caption = 'Email ID'
                    DataBinding.FieldName = 'eqr_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 62
                  end
                  object ColEmailTicketID: TcxGridDBColumn
                    Caption = 'Ticket ID'
                    DataBinding.FieldName = 'ticket_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 62
                  end
                  object ColEmailSentDate: TcxGridDBColumn
                    Caption = 'Sent At'
                    DataBinding.FieldName = 'sent_date'
                    Options.Editing = False
                    Width = 116
                  end
                  object ColEmailAddress: TcxGridDBColumn
                    Caption = 'Email To'
                    DataBinding.FieldName = 'email_address'
                    Options.Editing = False
                    Width = 155
                  end
                  object ColEmailStatus: TcxGridDBColumn
                    Caption = 'Email Status'
                    DataBinding.FieldName = 'status'
                    Width = 108
                  end
                  object ColEmailBody: TcxGridDBColumn
                    Caption = 'Email Body'
                    DataBinding.FieldName = 'email_body'
                    PropertiesClassName = 'TcxBlobEditProperties'
                    Properties.BlobEditKind = bekMemo
                    Properties.BlobPaintStyle = bpsText
                    Properties.MemoScrollBars = ssBoth
                  end
                  object ColEmailErrorDetails: TcxGridDBColumn
                    Caption = 'Error Details'
                    DataBinding.FieldName = 'error_details'
                    Width = 292
                  end
                end
                object EmailsGridLevel: TcxGridLevel
                  Caption = 'EMail Notifications'
                  GridView = EmailsGridView
                end
              end
              object eprGrid: TcxGrid
                Left = 5
                Top = 105
                Width = 996
                Height = 95
                Align = alClient
                TabOrder = 2
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                RootLevelOptions.DetailTabsPosition = dtpTop
                object eprRespView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellDblClick = eprRespViewCellDblClick
                  DataController.DataSource = dsEprHistory
                  DataController.KeyFieldNames = 'history_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object eprRespViewHistID: TcxGridDBColumn
                    DataBinding.FieldName = 'history_id'
                    Visible = False
                  end
                  object eprRespViewSentDate: TcxGridDBColumn
                    DataBinding.FieldName = 'insert_date'
                    Width = 239
                  end
                  object eprRespViewEmail: TcxGridDBColumn
                    DataBinding.FieldName = 'contact_to'
                    Width = 436
                  end
                  object eprRespViewStatus: TcxGridDBColumn
                    DataBinding.FieldName = 'status'
                    Width = 121
                  end
                  object eprRespViewLink: TcxGridDBColumn
                    DataBinding.FieldName = 'link'
                    PropertiesClassName = 'TcxRichEditProperties'
                    Properties.AutoURLDetect = True
                    Properties.WantReturns = False
                    Properties.WordWrap = False
                    Width = 178
                  end
                end
                object eprGridLevel: TcxGridLevel
                  Caption = 'EPR History'
                  GridView = eprRespView
                  Options.DetailTabsPosition = dtpTop
                end
              end
              object cxSplitter1: TcxSplitter
                Left = 5
                Top = 97
                Width = 996
                Height = 8
                AlignSplitter = salTop
                Control = EmailsGrid
              end
            end
          end
          object BillingTab: TTabSheet
            Caption = 'Billing'
            ImageIndex = 2
            object BillingPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object BillingGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 399
                Align = alClient
                TabOrder = 0
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object BillingGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = BillingSource
                  DataController.KeyFieldNames = 'billing_detail_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColBillBill: TcxGridDBColumn
                    Caption = 'Bill #, Date'
                    DataBinding.FieldName = 'bill'
                    Visible = False
                    GroupIndex = 0
                    Options.Editing = False
                    SortIndex = 0
                    SortOrder = soAscending
                    Width = 85
                  end
                  object ColBillLocateID: TcxGridDBColumn
                    Caption = 'Locate ID'
                    DataBinding.FieldName = 'locate_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 83
                  end
                  object ColBillClientCode: TcxGridDBColumn
                    Caption = 'Client Code'
                    DataBinding.FieldName = 'client_code'
                    Visible = False
                    Options.Editing = False
                  end
                  object ColBillClientName: TcxGridDBColumn
                    Caption = 'Client'
                    DataBinding.FieldName = 'cl_name'
                    Options.Editing = False
                    Width = 112
                  end
                  object ColBillBillingCC: TcxGridDBColumn
                    Caption = 'Billed To'
                    DataBinding.FieldName = 'billing_cc'
                    Width = 57
                  end
                  object ColBillStatus: TcxGridDBColumn
                    Caption = 'Status'
                    DataBinding.FieldName = 'status'
                    Width = 43
                  end
                  object ColBillQtyMarked: TcxGridDBColumn
                    Caption = 'Qty Marked'
                    DataBinding.FieldName = 'qty_marked'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 68
                  end
                  object ColBillClosedDate: TcxGridDBColumn
                    Caption = 'Closed'
                    DataBinding.FieldName = 'closed_date'
                    Options.Editing = False
                    Width = 78
                  end
                  object ColBillInitialStatusDate: TcxGridDBColumn
                    Caption = 'Initial Status'
                    DataBinding.FieldName = 'initial_status_date'
                    Options.Editing = False
                    Width = 88
                  end
                  object ColBillBillCode: TcxGridDBColumn
                    Caption = 'Bill Code'
                    DataBinding.FieldName = 'bill_code'
                    Visible = False
                    Options.Editing = False
                    Width = 125
                  end
                  object ColBillBucket: TcxGridDBColumn
                    Caption = 'Bucket'
                    DataBinding.FieldName = 'bucket'
                    Width = 123
                  end
                  object ColBillPrice: TcxGridDBColumn
                    Caption = 'Price'
                    DataBinding.FieldName = 'price'
                    PropertiesClassName = 'TcxCurrencyEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    Properties.ReadOnly = True
                    HeaderAlignmentHorz = taRightJustify
                    Width = 64
                  end
                  object ColBillRate: TcxGridDBColumn
                    Caption = 'Rate'
                    DataBinding.FieldName = 'rate'
                    PropertiesClassName = 'TcxCurrencyEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    Properties.ReadOnly = True
                    HeaderAlignmentHorz = taRightJustify
                    Width = 43
                  end
                  object ColBillBillID: TcxGridDBColumn
                    Caption = 'Bill ID'
                    DataBinding.FieldName = 'bill'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    Visible = False
                    HeaderAlignmentHorz = taRightJustify
                  end
                  object ColBillInvoiceID: TcxGridDBColumn
                    Caption = 'Invoice #'
                    DataBinding.FieldName = 'invoice_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Width = 73
                  end
                end
                object BillingGridLevel: TcxGridLevel
                  GridView = BillingGridView
                end
              end
            end
          end
          object TicketVersionsTab: TTabSheet
            Caption = 'Ticket Versions'
            ImageIndex = 4
            object VersionsPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object VersionSplitter: TSplitter
                Left = 4
                Top = 127
                Width = 998
                Height = 7
                Cursor = crDefault
                Align = alTop
                Color = clBtnFace
                ParentColor = False
              end
              object TicketVersionImageMemo: TDBMemo
                Left = 4
                Top = 134
                Width = 998
                Height = 269
                Align = alClient
                DataField = 'image_display'
                DataSource = TicketVersionSource
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 0
              end
              object TicketVersionsGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 123
                Align = alTop
                TabOrder = 1
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object TicketVersionsGridView: TcxGridDBTableView
                  PopupMenu = mnuPrintTktVer
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = TicketVersionSource
                  DataController.KeyFieldNames = 'ticket_version_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColTktVerCallCenter: TcxGridDBColumn
                    Caption = 'Call Center'
                    DataBinding.FieldName = 'call_center'
                    Options.Editing = False
                    Width = 156
                  end
                  object ColTktVerActive: TcxGridDBColumn
                    Caption = 'Active'
                    DataBinding.FieldName = 'active'
                    Options.Editing = False
                    Width = 37
                  end
                  object ColTktVerVersionID: TcxGridDBColumn
                    Caption = 'Version ID'
                    DataBinding.FieldName = 'ticket_version_id'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taRightJustify
                    HeaderAlignmentHorz = taRightJustify
                    Options.Editing = False
                    Width = 64
                  end
                  object ColTktVerTicketRevision: TcxGridDBColumn
                    Caption = 'Revision'
                    DataBinding.FieldName = 'ticket_revision'
                    Options.Editing = False
                    Width = 65
                  end
                  object ColTktVerTicketType: TcxGridDBColumn
                    Caption = 'Type'
                    DataBinding.FieldName = 'ticket_type'
                    Width = 78
                  end
                  object ColTktVerTransmitDate: TcxGridDBColumn
                    Caption = 'Transmit Date'
                    DataBinding.FieldName = 'transmit_date'
                    Width = 124
                  end
                  object ColTktVerArrivalDate: TcxGridDBColumn
                    Caption = 'Arrival Date'
                    DataBinding.FieldName = 'arrival_date'
                    Options.Editing = False
                    Width = 143
                  end
                  object ColTktVerProcessedDate: TcxGridDBColumn
                    Caption = 'Processed Date'
                    DataBinding.FieldName = 'processed_date'
                    Options.Editing = False
                    Width = 156
                  end
                end
                object TicketVersionsGridLevel: TcxGridLevel
                  GridView = TicketVersionsGridView
                end
              end
            end
          end
          object AcknowledgementTab: TTabSheet
            Caption = 'Acknowledgements'
            ImageIndex = 5
            object AcknowledgementPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              Caption = 'AcknowledgementPanel'
              TabOrder = 0
              object cxGrid1: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 399
                Align = alClient
                TabOrder = 0
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object AcknowledgmentGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = AcknowledgeSource
                  DataController.KeyFieldNames = 'ticket_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColAckHasAck: TcxGridDBColumn
                    Caption = 'Acknowledged'
                    DataBinding.FieldName = 'has_ack'
                    Options.Editing = False
                    Width = 112
                  end
                  object ColAckShortName: TcxGridDBColumn
                    Caption = 'Employee'
                    DataBinding.FieldName = 'short_name'
                    Options.Editing = False
                    Width = 210
                  end
                  object ColAckTicketID: TcxGridDBColumn
                    Caption = 'Ticket ID'
                    DataBinding.FieldName = 'ticket_id'
                    Visible = False
                    Options.Editing = False
                    Width = 119
                  end
                  object ColAckInsertDate: TcxGridDBColumn
                    Caption = 'Requested Date'
                    DataBinding.FieldName = 'insert_date'
                    Options.Editing = False
                    Width = 239
                  end
                  object ColAckAckDate: TcxGridDBColumn
                    Caption = 'Acknowledged Date'
                    DataBinding.FieldName = 'ack_date'
                    Width = 242
                  end
                  object ColAckAckEmpID: TcxGridDBColumn
                    DataBinding.FieldName = 'ack_emp_id'
                    Visible = False
                  end
                  object ColAckDeltaStatus: TcxGridDBColumn
                    DataBinding.FieldName = 'delta_status'
                    Visible = False
                  end
                  object ColAckLocakKey: TcxGridDBColumn
                    Caption = 'Local Key'
                    DataBinding.FieldName = 'LocalKey'
                    Visible = False
                    Width = 102
                  end
                  object ColAckLocalStringKey: TcxGridDBColumn
                    Caption = 'Local String Key'
                    DataBinding.FieldName = 'LocalStringKey'
                    Visible = False
                    Options.Editing = False
                    Width = 172
                  end
                end
                object AcknowledgmentGridLevel: TcxGridLevel
                  GridView = AcknowledgmentGridView
                end
              end
            end
          end
          object TicketInfoTab: TTabSheet
            Caption = 'Ticket Info'
            ImageIndex = 6
            object TicketInfoPanel: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 4
              TabOrder = 0
              object TicketInfoGrid: TcxGrid
                Left = 4
                Top = 4
                Width = 998
                Height = 399
                Align = alClient
                TabOrder = 0
                LookAndFeel.Kind = lfStandard
                LookAndFeel.NativeStyle = True
                object TicketInfoGridView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  Navigator.Buttons.First.Visible = True
                  Navigator.Buttons.PriorPage.Visible = True
                  Navigator.Buttons.Prior.Visible = True
                  Navigator.Buttons.Next.Visible = True
                  Navigator.Buttons.NextPage.Visible = True
                  Navigator.Buttons.Last.Visible = True
                  Navigator.Buttons.Insert.Visible = True
                  Navigator.Buttons.Append.Visible = False
                  Navigator.Buttons.Delete.Visible = True
                  Navigator.Buttons.Edit.Visible = True
                  Navigator.Buttons.Post.Visible = True
                  Navigator.Buttons.Cancel.Visible = True
                  Navigator.Buttons.Refresh.Visible = True
                  Navigator.Buttons.SaveBookmark.Visible = True
                  Navigator.Buttons.GotoBookmark.Visible = True
                  Navigator.Buttons.Filter.Visible = True
                  OnCellClick = IDCopyGridViewCellClick
                  DataController.DataSource = TicketInfoSource
                  DataController.KeyFieldNames = 'ticket_info_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.InvertSelect = False
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  OptionsView.HeaderAutoHeight = True
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object DateColumn: TcxGridDBColumn
                    Caption = 'Modified Date'
                    DataBinding.FieldName = 'Modified_Date'
                    Width = 223
                  end
                  object DescriptionColumn: TcxGridDBColumn
                    Caption = 'Question Code'
                    DataBinding.FieldName = 'info_type'
                    Width = 239
                  end
                  object AnswerColumn: TcxGridDBColumn
                    Caption = 'Answer'
                    DataBinding.FieldName = 'info'
                    Width = 289
                  end
                end
                object TicketInfoGridLevel: TcxGridLevel
                  GridView = TicketInfoGridView
                end
              end
            end
          end
          object JobsiteArrivalTab: TTabSheet
            Caption = 'Arrivals'
            ImageIndex = 7
            object PanelArrivalButtons: TPanel
              Left = 0
              Top = 0
              Width = 1006
              Height = 41
              Align = alTop
              TabOrder = 0
              object ButtonDeleteArrival: TButton
                Left = 8
                Top = 9
                Width = 95
                Height = 25
                Action = DeleteArrivalAction
                TabOrder = 0
              end
              object ButtonAddNewArrival: TButton
                Left = 110
                Top = 9
                Width = 95
                Height = 25
                Caption = 'Add Arrival'
                TabOrder = 1
                OnClick = ButtonAddNewArrivalClick
              end
            end
            object PanelArrivalModify: TPanel
              Left = 0
              Top = 41
              Width = 1006
              Height = 49
              Align = alTop
              TabOrder = 1
              Visible = False
              object Label1: TLabel
                Left = 8
                Top = 6
                Width = 87
                Height = 13
                Caption = 'Arrival Date/Time:'
              end
              object LabelLocInv: TLabel
                Left = 177
                Top = 6
                Width = 64
                Height = 13
                Caption = 'Who Arrived:'
              end
              object ButtonNewArrivalOK: TButton
                Left = 368
                Top = 20
                Width = 75
                Height = 25
                Caption = 'OK'
                TabOrder = 1
                OnClick = ButtonNewArrivalOKClick
              end
              object ButtonNewArrivalCancel: TButton
                Left = 450
                Top = 20
                Width = 75
                Height = 25
                Caption = 'Cancel'
                TabOrder = 2
                OnClick = ButtonNewArrivalCancelClick
              end
              inline LocatorBox: TEmployeeSelect
                Left = 175
                Top = 21
                Width = 190
                Height = 25
                TabOrder = 0
                inherited EmployeeCombo: TComboBox
                  Width = 160
                end
              end
              object ArrivalDate: TcxDateEdit
                Left = 7
                Top = 23
                Properties.ButtonGlyph.Data = {
                  C2030000424DC203000000000000B6000000280000000F0000000D0000000100
                  2000000000000C03000000000000000000001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
                  8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
                  00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
                  000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00808080008080000080800000808080008080800080808000808080008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080800000}
                Style.LookAndFeel.NativeStyle = True
                StyleDisabled.LookAndFeel.NativeStyle = True
                StyleFocused.LookAndFeel.NativeStyle = True
                StyleHot.LookAndFeel.NativeStyle = True
                TabOrder = 3
                Width = 89
              end
              object ArrivalTime: TcxTimeEdit
                Left = 98
                Top = 23
                EditValue = 0d
                TabOrder = 4
                Width = 68
              end
            end
            object ArrivalGrid: TcxGrid
              Left = 0
              Top = 90
              Width = 1006
              Height = 317
              Align = alClient
              TabOrder = 2
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object ArrivalGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                OnCellClick = IDCopyGridViewCellClick
                DataController.DataSource = JobsiteArrivalSource
                DataController.KeyFieldNames = 'arrival_id'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object ColArrArrivalID: TcxGridDBColumn
                  Caption = 'Arrival ID'
                  DataBinding.FieldName = 'arrival_id'
                  Visible = False
                  Options.Editing = False
                  Width = 112
                end
                object ColArrArrivalDate: TcxGridDBColumn
                  Caption = 'Arrival Date'
                  DataBinding.FieldName = 'arrival_date'
                  Options.Editing = False
                  Width = 139
                end
                object ColArrEmpID: TcxGridDBColumn
                  Caption = 'Emp ID'
                  DataBinding.FieldName = 'emp_id'
                  Visible = False
                  Options.Editing = False
                  Width = 142
                end
                object ColArrShortName: TcxGridDBColumn
                  Caption = 'Employee'
                  DataBinding.FieldName = 'short_name'
                  Options.Editing = False
                  Width = 150
                end
                object ColArrEntryMethod: TcxGridDBColumn
                  Caption = 'Entered How'
                  DataBinding.FieldName = 'entry_method'
                  Width = 84
                end
                object ColArrAddedByName: TcxGridDBColumn
                  Caption = 'Entered By'
                  DataBinding.FieldName = 'added_by_name'
                  Width = 133
                end
                object ColArrActive: TcxGridDBColumn
                  Caption = 'Active'
                  DataBinding.FieldName = 'active'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  OnGetDataText = ColArrActiveGetDataText
                  Width = 62
                end
                object ColArrDeletedBy: TcxGridDBColumn
                  Caption = 'Deleted By'
                  DataBinding.FieldName = 'deleted_by_name'
                  Width = 139
                end
                object ColArrDeletedDate: TcxGridDBColumn
                  Caption = 'Deleted Date'
                  DataBinding.FieldName = 'deleted_date'
                  Width = 104
                end
              end
              object ArrivalGridLevel: TcxGridLevel
                GridView = ArrivalGridView
              end
            end
          end
          object LocateFacilityTab: TTabSheet
            Caption = 'Facilities'
            ImageIndex = 8
            object FacilityHistoryGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object FacilityHistoryGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                OnCellClick = IDCopyGridViewCellClick
                DataController.DataSource = LocateFacilitySource
                DataController.KeyFieldNames = 'locate_facility_id'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object ColFacHistLocateFacilityID: TcxGridDBColumn
                  Caption = 'Locate Facility ID'
                  DataBinding.FieldName = 'locate_facility_id'
                  Visible = False
                  Options.Editing = False
                  Width = 112
                end
                object ColFacHistLocateID: TcxGridDBColumn
                  Caption = 'Locate ID'
                  DataBinding.FieldName = 'locate_id'
                  Visible = False
                  Options.Editing = False
                  Width = 139
                end
                object ColFacHistClientName: TcxGridDBColumn
                  Caption = 'Client'
                  DataBinding.FieldName = 'client_name'
                  Options.Editing = False
                  Width = 153
                end
                object ColFacHistTypeDesc: TcxGridDBColumn
                  Caption = 'Type'
                  DataBinding.FieldName = 'type_desc'
                  Options.Editing = False
                  Width = 72
                end
                object ColFacHistMaterialDesc: TcxGridDBColumn
                  Caption = 'Material'
                  DataBinding.FieldName = 'material_desc'
                  Width = 87
                end
                object ColFacHistSizeDesc: TcxGridDBColumn
                  Caption = 'Size'
                  DataBinding.FieldName = 'size_desc'
                  Width = 67
                end
                object ColFacHistPressureDesc: TcxGridDBColumn
                  Caption = 'Pressure'
                  DataBinding.FieldName = 'pressure_desc'
                  Width = 65
                end
                object ColFacHistOffset: TcxGridDBColumn
                  Caption = 'No. of Offsets'
                  DataBinding.FieldName = 'offset'
                  FooterAlignmentHorz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Width = 91
                end
                object ColFacHistAddedByName: TcxGridDBColumn
                  Caption = 'Added By'
                  DataBinding.FieldName = 'added_by_name'
                  Width = 112
                end
                object ColFacHistAddedDate: TcxGridDBColumn
                  Caption = 'Date Added'
                  DataBinding.FieldName = 'added_date'
                  Width = 71
                end
                object ColFacHistDeletedByName: TcxGridDBColumn
                  Caption = 'Deleted By'
                  DataBinding.FieldName = 'deleted_by_name'
                  Width = 64
                end
                object ColFacHistDeletedDate: TcxGridDBColumn
                  Caption = 'Date Deleted'
                  DataBinding.FieldName = 'deleted_date'
                  Width = 75
                end
                object ColFacHistActive: TcxGridDBColumn
                  Caption = 'Active'
                  DataBinding.FieldName = 'active'
                  Width = 53
                end
              end
              object FacilityHistoryGridLevel: TcxGridLevel
                GridView = FacilityHistoryGridView
              end
            end
          end
          object TicketRiskScoreTab: TTabSheet
            Caption = 'Risk Scores'
            ImageIndex = 9
            object RiskScoreGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 1006
              Height = 407
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object RiskScoreView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.First.Visible = True
                Navigator.Buttons.PriorPage.Visible = True
                Navigator.Buttons.Prior.Visible = True
                Navigator.Buttons.Next.Visible = True
                Navigator.Buttons.NextPage.Visible = True
                Navigator.Buttons.Last.Visible = True
                Navigator.Buttons.Insert.Visible = True
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.Delete.Visible = True
                Navigator.Buttons.Edit.Visible = True
                Navigator.Buttons.Post.Visible = True
                Navigator.Buttons.Cancel.Visible = True
                Navigator.Buttons.Refresh.Visible = True
                Navigator.Buttons.SaveBookmark.Visible = True
                Navigator.Buttons.GotoBookmark.Visible = True
                Navigator.Buttons.Filter.Visible = True
                OnCellClick = IDCopyGridViewCellClick
                DataController.DataSource = TicketRiskDS
                DataController.KeyFieldNames = 'ticket_risk_id'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsView.GroupByBox = False
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                OptionsView.HeaderAutoHeight = True
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
                object ColTicketRiskID: TcxGridDBColumn
                  Caption = 'Risk ID'
                  DataBinding.FieldName = 'ticket_risk_id'
                  Options.Editing = False
                  Width = 50
                end
                object ColTicketVersionID: TcxGridDBColumn
                  Caption = 'Version ID'
                  DataBinding.FieldName = 'ticket_version_id'
                  Options.Editing = False
                  Width = 55
                end
                object ColLocateClient: TcxGridDBColumn
                  Caption = 'Client'
                  DataBinding.FieldName = 'client_code'
                  PropertiesClassName = 'TcxMaskEditProperties'
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Width = 112
                end
                object ColTicketRevision: TcxGridDBColumn
                  Caption = 'Revision'
                  DataBinding.FieldName = 'ticket_revision'
                  Options.Editing = False
                  Width = 118
                end
                object ColRiskSource: TcxGridDBColumn
                  Caption = 'Risk Source'
                  DataBinding.FieldName = 'risk_source'
                  Options.Editing = False
                  Width = 78
                end
                object ColRiskScoreType: TcxGridDBColumn
                  Caption = 'Risk Score Type'
                  DataBinding.FieldName = 'risk_score_type'
                  Options.Editing = False
                  Width = 124
                end
                object ColRiskScore: TcxGridDBColumn
                  Caption = 'Risk Score'
                  DataBinding.FieldName = 'risk_score'
                  Options.Editing = False
                  Styles.Content = SharedDevExStyleData.BoldFontStyle
                  Width = 143
                end
                object ColTransmitDate: TcxGridDBColumn
                  Caption = 'Transmit Date'
                  DataBinding.FieldName = 'transmit_date'
                  Options.Editing = False
                  Width = 156
                end
              end
              object RiskScoreLevel: TcxGridLevel
                GridView = RiskScoreView
              end
            end
          end
        end
      end
    end
    object AttachmentsTab: TTabSheet
      Caption = 'Attachments'
      ImageIndex = -1
      inline AttachmentsFrame: TAttachmentsFrame
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        TabOrder = 0
        inherited AttachmentsPanel: TPanel
          Width = 1024
          Height = 414
          inherited AttachmentList: TcxGrid
            Width = 1014
            Height = 404
            inherited AttachmentListDBTableView: TcxGridDBTableView
              inherited ColAttachmentID: TcxGridDBColumn
                Properties.Alignment.Horz = taRightJustify
                HeaderAlignmentHorz = taRightJustify
              end
              inherited ColForeignID: TcxGridDBColumn
                Properties.Alignment.Horz = taRightJustify
                HeaderAlignmentHorz = taRightJustify
              end
            end
          end
        end
        inherited FooterPanel: TPanel
          Top = 414
          Width = 1024
        end
      end
    end
    object PhotoAttachmentsTab: TTabSheet
      Caption = 'Photo Attachments'
      ImageIndex = -1
      inline PhotoAttachmentFrame: TPhotoAttachmentFrame
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        AutoSize = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        inherited PanelPhotos: TPanel
          Width = 1024
          Height = 387
          inherited ScrollBox: TScrollBox
            Width = 1014
            Height = 377
          end
        end
        inherited PanelFooter: TPanel
          Top = 387
          Width = 1024
          Height = 58
          Anchors = [akLeft, akTop, akRight, akBottom]
        end
      end
    end
    object TicketSummaryTab: TTabSheet
      Caption = 'Summary'
      ImageIndex = -1
      object Label7: TLabel
        Left = 13
        Top = 29
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Work Address:'
        Transparent = True
      end
      object SummaryWorkAddressNumber: TDBText
        Left = 89
        Top = 29
        Width = 35
        Height = 17
        DataField = 'work_address_number'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryWorkAddressStreet: TDBText
        Left = 130
        Top = 29
        Width = 167
        Height = 13
        DataField = 'work_address_street'
        DataSource = TicketDS
        Transparent = True
      end
      object Label8: TLabel
        Left = 300
        Top = 29
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cross Street:'
        Transparent = True
      end
      object SummaryWorkCross: TDBText
        Left = 371
        Top = 29
        Width = 217
        Height = 13
        DataField = 'work_cross'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryMapPage: TDBText
        Left = 371
        Top = 45
        Width = 130
        Height = 13
        DataField = 'map_page'
        DataSource = TicketDS
        Transparent = True
      end
      object Label12: TLabel
        Left = 300
        Top = 45
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Map Page:'
        Transparent = True
      end
      object SummaryWorkCity: TDBText
        Left = 89
        Top = 45
        Width = 115
        Height = 13
        DataField = 'work_city'
        DataSource = TicketDS
        Transparent = True
      end
      object Label9: TLabel
        Left = 13
        Top = 45
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Work City:'
        Transparent = True
      end
      object Label10: TLabel
        Left = 13
        Top = 62
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Work State:'
        Transparent = True
      end
      object SummaryWorkState: TDBText
        Left = 89
        Top = 62
        Width = 87
        Height = 13
        DataField = 'work_state'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryWorkCounty: TDBText
        Left = 89
        Top = 79
        Width = 250
        Height = 13
        DataField = 'work_county'
        DataSource = TicketDS
        Transparent = True
      end
      object Label11: TLabel
        Left = 13
        Top = 79
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Work County:'
        Transparent = True
      end
      object ArriveForMoreDetailsLabel: TLabel
        Left = 13
        Top = 154
        Width = 348
        Height = 13
        Caption = 
          'More ticket information can be viewed after clicking the I Arriv' +
          'ed button.'
      end
      object Label14: TLabel
        Left = 13
        Top = 108
        Width = 42
        Height = 13
        Caption = 'Contact:'
        Transparent = True
      end
      object SummaryCallerContact: TDBText
        Left = 89
        Top = 108
        Width = 112
        Height = 13
        DataField = 'caller_contact'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryCallerPhone: TDBText
        Left = 208
        Top = 108
        Width = 90
        Height = 13
        DataField = 'caller_phone'
        DataSource = TicketDS
        Transparent = True
      end
      object Label15: TLabel
        Left = 13
        Top = 124
        Width = 62
        Height = 13
        Caption = 'Alt. Contact:'
        Transparent = True
      end
      object SummaryCallerAltContact: TDBText
        Left = 89
        Top = 124
        Width = 112
        Height = 13
        DataField = 'caller_altcontact'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryCallerAltPhone: TDBText
        Left = 208
        Top = 124
        Width = 90
        Height = 13
        DataField = 'caller_altphone'
        DataSource = TicketDS
        Transparent = True
      end
      object Label16: TLabel
        Left = 13
        Top = 2
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'Due:'
        Transparent = True
      end
      object SummaryDueDate: TDBText
        Left = 89
        Top = 2
        Width = 86
        Height = 13
        AutoSize = True
        DataField = 'due_date'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryWorkLatLabel: TLabel
        Left = 300
        Top = 62
        Width = 43
        Height = 13
        Caption = 'Latitude:'
        Transparent = True
      end
      object SummaryWorkLat: TDBText
        Left = 371
        Top = 62
        Width = 134
        Height = 17
        DataField = 'work_lat'
        DataSource = TicketDS
        Transparent = True
      end
      object SummaryWorkLongLabel: TLabel
        Left = 300
        Top = 79
        Width = 51
        Height = 13
        Caption = 'Longitude:'
        Transparent = True
      end
      object SummaryWorkLong: TDBText
        Left = 371
        Top = 79
        Width = 134
        Height = 17
        DataField = 'work_long'
        DataSource = TicketDS
        Transparent = True
      end
    end
    object TicketAlertsTab: TTabSheet
      Caption = 'Alerts'
      ImageIndex = 51
      TabVisible = False
      inline TicketAlertsFrame: TTicketAlertsFrame
        Left = 0
        Top = 0
        Width = 1024
        Height = 445
        Align = alClient
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 1024
        end
        inherited AlertGrid: TcxGrid
          Width = 1024
          Height = 428
          inherited AlertGridView: TcxGridDBTableView
            inherited ColAlertDesc: TcxGridDBColumn
              Properties.PlainText = False
            end
          end
          inherited ALertGridView_B: TcxGridDBBandedTableView
            inherited ColBAlertDesc: TcxGridDBBandedColumn
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            inherited ColBInsertDate: TcxGridDBBandedColumn
              Position.ColIndex = 5
            end
            inherited ColBModifiedDate: TcxGridDBBandedColumn
              Position.ColIndex = 6
            end
            inherited ColBSortBy: TcxGridDBBandedColumn
              Position.ColIndex = 7
              Position.RowIndex = 0
            end
            inherited ColBColor: TcxGridDBBandedColumn
              Position.ColIndex = 8
            end
          end
        end
      end
    end
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 1032
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object TicketNum: TLabel
      Left = 15
      Top = 1
      Width = 43
      Height = 13
      Caption = 'Ticket #:'
      Transparent = True
    end
    object Kind: TDBText
      Left = 14
      Top = 17
      Width = 81
      Height = 17
      DataField = 'kind'
      DataSource = TicketDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object TicketType: TDBText
      Left = 99
      Top = 17
      Width = 178
      Height = 17
      DataField = 'ticket_type'
      DataSource = TicketDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object TicketAlertLabel: TLabel
      Left = 4
      Top = -1
      Width = 13
      Height = 16
      Caption = ' ! '
      Color = clHighlightText
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object TicketNumber: TDBText
      Left = 64
      Top = 0
      Width = 213
      Height = 17
      DataField = 'ticket_number'
      DataSource = TicketDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object FollowupPopupEdit: TcxPopupEdit
      Left = 392
      Top = 5
      Properties.Alignment.Horz = taLeftJustify
      Properties.PopupAlignment = taRightJustify
      Properties.PopupControl = FollowupPanel
      Properties.OnCloseQuery = FollowupPopupEditPropertiesCloseQuery
      Properties.OnCloseUp = FollowupPopupEditPropertiesCloseUp
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = btsFlat
      Style.ButtonTransparency = ebtNone
      StyleFocused.Color = clBtnFace
      TabOrder = 5
      Text = 'FollowupPopupEdit'
      Width = 41
    end
    object DoneButton: TButton
      Left = 718
      Top = 2
      Width = 85
      Height = 25
      Caption = 'Done (F12)'
      TabOrder = 0
      OnClick = DoneButtonClick
    end
    object PrintButton: TButton
      Left = 625
      Top = 2
      Width = 85
      Height = 25
      Caption = 'Print Preview'
      TabOrder = 1
      OnClick = PrintButtonClick
    end
    object ArriveButtonPanel: TPanel
      Left = 529
      Top = 0
      Width = 91
      Height = 31
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 2
      object ArriveButton: TButton
        Left = 3
        Top = 2
        Width = 85
        Height = 25
        Caption = 'I Arrived'
        TabOrder = 0
        OnClick = ArriveButtonClick
      end
    end
    object FollowupButton: TBitBtn
      Left = 348
      Top = 2
      Width = 85
      Height = 25
      Caption = '  Followup '
      TabOrder = 3
      Visible = False
      OnClick = FollowupButtonClick
      Glyph.Data = {
        7E000000424D7E000000000000003E0000002800000020000000100000000100
        010000000000400000000000000000000000020000000000000000000000FFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
        FF7FFE3FFE3FFC1FFC1FF80FF80FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF}
      Layout = blGlyphRight
      NumGlyphs = 2
    end
    object FollowupPanel: TPanel
      Left = 212
      Top = 30
      Width = 332
      Height = 118
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = clWindow
      TabOrder = 4
      Visible = False
      object FollowupPanelButtonPanel: TPanel
        Left = 2
        Top = 80
        Width = 328
        Height = 36
        Align = alBottom
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 1
        object FollowupPanelButtonPanelBevel: TBevel
          Left = 0
          Top = 0
          Width = 328
          Height = 36
          Align = alClient
          Shape = bsTopLine
          Style = bsRaised
        end
        object CreateFollowupButton: TButton
          Left = 142
          Top = 6
          Width = 88
          Height = 25
          Caption = 'Create Followup'
          Default = True
          ModalResult = 1
          TabOrder = 0
          OnClick = CreateFollowupButtonClick
        end
        object CancelFollowupButton: TButton
          Left = 235
          Top = 6
          Width = 88
          Height = 25
          Cancel = True
          Caption = 'Cancel Followup'
          ModalResult = 2
          TabOrder = 1
          OnClick = CancelFollowupButtonClick
        end
      end
      object FollowupPanelTopPanel: TPanel
        Left = 2
        Top = 2
        Width = 328
        Height = 78
        Align = alClient
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        object FollowupDueDateLabel: TLabel
          Left = 25
          Top = 54
          Width = 75
          Height = 13
          Caption = 'Due Date/Time:'
        end
        object FollowupTicketTypeLabel: TLabel
          Left = 42
          Top = 10
          Width = 59
          Height = 13
          Caption = 'Ticket Type:'
        end
        object FollowupTransmitDate: TLabel
          Left = 108
          Top = 32
          Width = 25
          Height = 13
          Caption = 'None'
        end
        object FollowupTransmitDateLabel: TLabel
          Left = 5
          Top = 32
          Width = 97
          Height = 13
          Caption = 'Transmit Date/Time:'
        end
        object FollowupTicketType: TComboBox
          Left = 108
          Top = 6
          Width = 217
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
        end
        object FollowupDueDate: TcxDateEdit
          Left = 108
          Top = 50
          Properties.ButtonGlyph.Data = {
            C2030000424DC203000000000000B6000000280000000F0000000D0000000100
            2000000000000C03000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
            8000808080008080800080808000808080008080800080808000808080008080
            800080808000808080008080800080808000808000008080000080808000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
            0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
            0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
            0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
            8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
            00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
            0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
            000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
            8000808080008080800080808000808080008080800080808000808080008080
            800080808000808080008080800080808000808000008080000080808000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
            0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
            0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00808080008080000080800000808080008080800080808000808080008080
            8000808080008080800080808000808080008080800080808000808080008080
            800080800000}
          Style.LookAndFeel.NativeStyle = True
          StyleDisabled.LookAndFeel.NativeStyle = True
          StyleFocused.LookAndFeel.NativeStyle = True
          StyleHot.LookAndFeel.NativeStyle = True
          TabOrder = 1
          Width = 89
        end
        object FollowupDueTime: TcxTimeEdit
          Left = 199
          Top = 50
          EditValue = 0d
          TabOrder = 2
          Width = 68
        end
      end
    end
    object btnTktSafety: TBitBtn
      Left = 809
      Top = 2
      Width = 85
      Height = 25
      Caption = 'JSA'
      TabOrder = 6
      OnClick = btnTktSafetyClick
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3
        CAC9C9BFBDBDBAB9B9B5B3B3D7D6D6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFDCDBDBC5C3C3FFFFFFFFFFFFFFFFFFFFFFFFBFBDBDAEACACF0EFEFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFE3E2E2D5D2D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFAFADADF7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFAFAFAD7D6D6FFFFFFFFFFFFFFFFFF0000A60000
        A7FFFFFFFFFFFFFFFFFFD7D5D5C3C1C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECEBEBFFFFFFFFFEF3FFFFF9
        FFFFFF0510BB0A13BCFFFFFFFFFFF9FEFDF1E9E8E7C3C1C1F8F8F8FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E6E6FB
        FAEF6E74D9757ADD6167DB151ECB161FCB5259D7787DDE6268D6F3F2EDD5D3D2
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFE6E6E5FFFFF50D16D0111BD41721D61F29D91F28D71922D6131DD300
        04CDFAF9F1DFDDDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE9E9E8FFFFF5030FDE0612E10F1AE4212BE5212B
        E5121DE20814E10000DCFAFAF2E0DEDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFEFF4F3F1FFFFFFFFFFFF
        CFD2FB111CED1520EDAAAEF7FFFFFEFFFFFFF2F2F0E0DFDEFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCEC
        EBEBFAFAF7FFFFFCCFD1FB0914EC0D18ECA8ACF6FFFFFDFFFFFFE9E8E7F4F3F3
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFF5F4F4F1F1F0FFFFF9D5D7FA3B43EF3D46F0B7BBF7FFFFFAF0
        EFEFEDECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F0F0F0FFFFFFFFFFFBFFFF
        FBFFFFF7FFFFFFF2F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
        F7F7F7F5F5F5F4F4F3F6F5F5FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnServiceCard: TBitBtn
      Left = 439
      Top = 2
      Width = 85
      Height = 25
      Caption = 'QCard'
      TabOrder = 7
      OnClick = btnServiceCardClick
    end
  end
  object ReasonCheckListPanel: TPanel
    Left = 864
    Top = 53
    Width = 160
    Height = 116
    BevelOuter = bvNone
    BorderWidth = 3
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object HpReasonCheckList: TCheckListBox
      Left = 3
      Top = 3
      Width = 154
      Height = 110
      Align = alClient
      ItemHeight = 13
      Items.Strings = (
        'This'
        'That'
        'The Other')
      TabOrder = 0
    end
  end
  object TicketDS: TDataSource
    Left = 684
    Top = 30
  end
  object LocateDS: TDataSource
    OnDataChange = LocateDSDataChange
    Left = 719
    Top = 30
  end
  object TicketVersions: TDBISAMQuery
    OnCalcFields = TicketVersionsCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select tr.*, c.client_name, l.client_code '
      'from ticket_risk tr, locate l, client c'
      'where (tr.ticket_id=:ticket_id)'
      'and (tr.locate_id = l.locate_id)'
      'and (tr.ticket_id = l.ticket_id)'
      'and (l.client_id = c.client_id)'
      'order by tr.ticket_version_id desc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 564
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object Assignments: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'assignment'
    Left = 692
    Top = 328
  end
  object Responses: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'response_log'
    Left = 692
    Top = 368
  end
  object LocateStatus: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'locate_status'
    Left = 780
    Top = 316
  end
  object LocateStatusSource: TDataSource
    AutoEdit = False
    DataSet = LocateStatus
    Left = 748
    Top = 316
  end
  object TicketVersionSource: TDataSource
    AutoEdit = False
    DataSet = TicketVersions
    Left = 540
    Top = 248
  end
  object BillingSource: TDataSource
    AutoEdit = False
    DataSet = Billing
    Left = 660
    Top = 408
  end
  object ResponseSource: TDataSource
    AutoEdit = False
    DataSet = Responses
    Left = 660
    Top = 368
  end
  object AssignmentSource: TDataSource
    AutoEdit = False
    DataSet = Assignments
    Left = 660
    Top = 328
  end
  object LocateLookup: TDBISAMTable
    BeforeOpen = LocateLookupBeforeOpen
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'locate'
    Left = 612
    Top = 336
  end
  object ClientLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'client'
    Left = 340
    Top = 312
  end
  object EmployeeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'employee'
    Left = 612
    Top = 376
  end
  object HPReason: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from reference'
      'where type = '#39'hpreason'#39
      '  and active_ind'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 869
    Top = 108
  end
  object ReferenceLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'reference'
    Left = 612
    Top = 304
  end
  object StatusList: TDBISAMQuery
    Filtered = True
    OnFilterRecord = StatusListFilterRecord
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select distinct sl.status, sl.meet_only, sgi.TicketTypeExclusion' +
        ', '
      'sgi.NotesRequired, sgi.PicturesRequired, client_id, '
      'sl.mark_type_required, default_mark_type, sl.status_name'
      ''
      'from client c'
      ' inner join status_group_item sgi on c.status_group_id=sgi.sg_id'
      ' inner join status_group sg on (sgi.sg_id=sg.sg_id)'
      ' inner join statuslist sl on sgi.status=sl.status'
      ' where sgi.active'
      ' and sl.status not in('#39'-N'#39', '#39'-P'#39')'
      ' and sl.active'
      ' and client_id in ( 3436,4490 )'
      ' order by 1')
    Params = <>
    ReadOnly = True
    Left = 451
    Top = 376
  end
  object StatusSource: TDataSource
    AutoEdit = False
    DataSet = StatusList
    Left = 423
    Top = 376
  end
  object Acknowledged: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'ticket_ack'
    Left = 140
    Top = 176
  end
  object AcknowledgeSource: TDataSource
    AutoEdit = False
    DataSet = Acknowledged
    Left = 164
    Top = 200
  end
  object Billing: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select bd.*, bh.bill_end_date, cast(bd.bill_id as varchar(15)) +'
      '  '#39', ending '#39' + cast(bh.bill_end_date as varchar(10)) as bill,'
      
        '  inv.invoice_id, c.customer_id as client_customer_id, inv.custo' +
        'mer_id invoice_customer_id,'
      ' c.client_name as cl_name '
      ' from billing_detail bd'
      '  left join billing_header bh on bd.bill_id = bh.bill_id'
      
        '  left join billing_invoice inv on inv.billing_header_id = bd.bi' +
        'll_id'
      '  left join client c on c.client_id = bd.client_id '
      
        'where ((inv.which_invoice = bd.which_invoice) or ((inv.which_inv' +
        'oice is null) and (bd.which_invoice is null)))'
      'and bd.area_name <> '#39'DEL_GAS'#39)
    Params = <>
    ReadOnly = True
    Left = 692
    Top = 408
  end
  object HpMultiReasonQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select r."sortby", r.code, r.code + '#39' - '#39' + r.description as rea' +
        'son,'
      '   r.ref_id as hp_id, chp.active'
      '  from reference as r'
      '    inner join call_center_hp as chp'
      '     on r.ref_id = chp.hp_id and chp.call_center=:cc'
      '     and chp.active'
      '  where r.type='#39'hpmulti'#39
      '  and active_ind = 1'
      '  order by  1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cc'
      end>
    Left = 869
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cc'
      end>
  end
  object TicketInfo: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'ticket_info'
    Left = 908
    Top = 384
  end
  object TicketInfoSource: TDataSource
    AutoEdit = False
    DataSet = TicketInfo
    Left = 882
    Top = 384
  end
  object JobsiteArrivalSource: TDataSource
    DataSet = DM.JobsiteArrivalData
    Left = 747
    Top = 397
  end
  object LocateFacilitySource: TDataSource
    DataSet = LocateFacility
    Left = 747
    Top = 360
  end
  object LocateFacility: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from locate_facility where 0=1')
    Params = <>
    ReadOnly = True
    Left = 780
    Top = 360
  end
  object LocateFacilityDS: TDataSource
    Left = 784
    Top = 30
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 144
    object DeleteArrivalAction: TAction
      Caption = 'Delete Arrival'
      OnExecute = DeleteArrivalActionExecute
    end
  end
  object EmailResults: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'email_queue_result'
    Left = 436
    Top = 318
  end
  object EmailResultsSource: TDataSource
    AutoEdit = False
    DataSet = EmailResults
    Left = 404
    Top = 318
  end
  object cxGridViewRepository: TcxGridViewRepository
    Left = 288
    Top = 424
    object HPReasonView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = True
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = True
      Navigator.Buttons.GotoBookmark.Visible = True
      Navigator.Buttons.Filter.Visible = True
      DataController.DataSource = HPReasonSource
      DataController.KeyFieldNames = 'ref_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Header = False
      OptionsView.HeaderAutoHeight = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ColHPReasRefID: TcxGridDBColumn
        DataBinding.FieldName = 'ref_id'
        Visible = False
      end
      object ColHPReasCode: TcxGridDBColumn
        Caption = 'Code'
        DataBinding.FieldName = 'code'
      end
      object ColHPReasDescription: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'description'
        Visible = False
      end
      object ColHPReasSortBy: TcxGridDBColumn
        DataBinding.FieldName = 'sortby'
        Visible = False
      end
    end
    object StatusListView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = True
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = True
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = True
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = True
      Navigator.Buttons.SaveBookmark.Visible = True
      Navigator.Buttons.GotoBookmark.Visible = True
      Navigator.Buttons.Filter.Visible = True
      DataController.DataSource = StatusSource
      DataController.KeyFieldNames = 'status'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ScrollBars = ssVertical
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.Header = False
      OptionsView.HeaderAutoHeight = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object ColStatusListStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
      end
      object ColStatusListStatusName: TcxGridDBColumn
        Caption = 'Description'
        DataBinding.FieldName = 'status_name'
      end
    end
  end
  object HPReasonSource: TDataSource
    DataSet = HPReason
    Left = 912
    Top = 112
  end
  object LastSMDSource: TDataSource
    AutoEdit = False
    DataSet = LastSMD
    Left = 810
    Top = 352
  end
  object LastSMD: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select Info as LastSMD, ticket_info_id'
      'from ticket_info'
      'where ticket_id = :ticketID'
      'and info_type = '#39'SMDTIME'#39
      'and info <>'#39#39
      'order by ticket_info_id desc top 1')
    Params = <
      item
        DataType = ftInteger
        Name = 'TicketID'
      end>
    Left = 872
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'TicketID'
      end>
  end
  object qryEprHistory: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select *'
      'from epr_response_history'
      'where ticket_id = :ticketID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticketID'
      end>
    ReadOnly = True
    Left = 616
    Top = 432
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticketID'
      end>
  end
  object dsEprHistory: TDataSource
    DataSet = qryEprHistory
    Left = 544
    Top = 432
  end
  object TicketRiskQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select tr.*, c.client_name, l.client_code '
      'from ticket_risk tr, locate l, client c'
      'where (tr.ticket_id=:ticket_id)'
      'and (tr.locate_id = l.locate_id)'
      'and (tr.ticket_id = l.ticket_id)'
      'and (l.client_id = c.client_id)'
      'order by tr.ticket_version_id desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 624
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object TicketRiskDS: TDataSource
    DataSet = TicketRiskQry
    Left = 648
    Top = 160
  end
  object updTicketPopupMenu: TPopupMenu
    Left = 552
    Top = 80
    object CopyTicket1: TMenuItem
      Caption = 'Copy Ticket #'
      OnClick = CopyTicket1Click
    end
  end
  object mnuPrintTktVer: TPopupMenu
    Left = 280
    Top = 200
    object PrintTicketVersionImage1: TMenuItem
      Caption = 'Print Ticket Version Image'
      OnClick = PrintTicketVersionImage1Click
    end
  end
  object statuslist_sp: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      '//Replaced by code in UseStatusPlus')
    Params = <>
    ReadOnly = True
    Left = 451
    Top = 406
  end
  object StatusSource_sp: TDataSource
    AutoEdit = False
    DataSet = statuslist_sp
    Left = 423
    Top = 406
  end
end
