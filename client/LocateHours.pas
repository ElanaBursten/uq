unit LocateHours;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridLevel, cxGrid, cxTextEdit, cxMaskEdit,
  cxCalendar, cxSpinEdit, cxDBLookupComboBox, cxContainer, cxDropDownEdit,
  ComCtrls, dxCore, cxDateUtils, cxNavigator, Buttons, cxFilter, cxData;

type
  TLocateWorkForm = class(TForm)
    WorkGroup: TGroupBox;
    HoursGrid: TcxGrid;
    Label1: TLabel;
    LocateHoursDS: TDataSource;
    ColEmployee: TcxGridDBColumn;
    ColWorkDate: TcxGridDBColumn;
    ColOvertimeHours: TcxGridDBColumn;
    WorkDate: TcxDateEdit;
    Label2: TLabel;
    CloseButton: TButton;
    ColClient: TcxGridDBColumn;
    ColTotalHours: TcxGridDBColumn;
    ColEntryDate: TcxGridDBColumn;
    ColRegularHours: TcxGridDBColumn;
    ColLocateId: TcxGridDBColumn;
    AddHoursButton: TButton;
    ColStatus: TcxGridDBColumn;
    ColUnitsMarked: TcxGridDBColumn;
    HoursGroup: TGroupBox;
    CloseLocates: TCheckBox;
    ApplyToAll: TCheckBox;
    OvertimeHoursLabel: TLabel;
    RegularHoursLabel: TLabel;
    Label3: TLabel;
    TotalHoursLabel: TLabel;
    UnitsGroup: TGroupBox;
    UnitsLabel: TLabel;
    UnitsMarked: TcxSpinEdit;
    Label4: TLabel;
    StatusCombo: TComboBox;
    ColUnitType: TcxGridDBColumn;
    ColClientName: TcxGridDBColumn;
    AllLocatesMemo: TMemo;
    WarningLabel: TLabel;
    HoursGridTableView: TcxGridDBTableView;
    HoursGridLevel: TcxGridLevel;
    edtRegHrs: TEdit;
    Label5: TLabel;
    edtRegMins: TEdit;
    Label6: TLabel;
    edtOTHrs: TEdit;
    Label7: TLabel;
    edtOTMins: TEdit;
    Label8: TLabel;
    btnTotalHrs: TBitBtn;
    procedure AddHoursButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CloseButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure HoursGridTableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure HoursGridTableViewFocusedItemChanged(
      Sender: TcxCustomGridTableView; APrevFocusedItem,
      AFocusedItem: TcxCustomGridTableItem);
    procedure HoursGridTableViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems4GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure btnTotalHrsClick(Sender: TObject);
    procedure edtRegHrsKeyPress(Sender: TObject; var Key: Char);
  private
    decimalHoursReg:double;  //QM-299  sr
    decimalHoursOT:double;   //QM-299  sr
    FLocateID: Integer;
    FTicketID: Integer;
    FLocateInvoiced: Boolean;
    FBookmark: string;
    FUnitConversionID: Integer;
    procedure AddHoursToLocates;
    procedure SetAllowAdd(const Hours, Units: Boolean);

    function Locates: TDataSet;
    function LocateHoursData: TDataSet;
    procedure SetBookmark;
    procedure SetLocatesAffectedLabel;
    procedure DebugLog(const Msg: string);
  protected
    procedure Loaded; override;
  public
    procedure ShowDialog;
    procedure SetupDialog(TicketID, LocateID: Integer; CanAddHours, CanAddUnits: Boolean);
  end;

implementation

{$R *.dfm}

uses DMu, OdDBUtils, OdVclUtils, OdExceptions,
  OdMiscUtils, Terminology, LengthConverter, QMConst, OdCxUtils,
  LocalPermissionsDMu;

{ TLocateHoursForm }

procedure TLocateWorkForm.SetupDialog(TicketID, LocateID: Integer; CanAddHours, CanAddUnits: Boolean);
var
  UnitTypeLabel: string;
begin
  FLocateID := LocateID;
  FTicketID := TicketID;
  FLocateInvoiced := DM.IsLocateInvoiced(FLocateID);
  SetAllowAdd(CanAddHours, CanAddUnits);
  SetBookmark;

  DM.GetOngoingStatuses(LocateID, StatusCombo.Items);
  DM.ApplyEmployeeStatusLimit(StatusCombo.Items);
  SetGridComboItems(ColStatus, StatusCombo.Items);
  StatusCombo.Items.Insert(0, ''); // Allow none for this combo

  StatusCombo.ItemIndex := -1;
  WorkDate.Date := Trunc(Now);
  edtRegHrs.text := '';
  edtRegMins.text := '';
  UnitsMarked.Value := 0;
  edtOTHrs.text := '';
  edtOTMins.text := '';
  FUnitConversionID := DM.UnitTypeForLocate(FLocateID, UnitTypeLabel);
  UnitsLabel.Caption := UnitTypeLabel + ' Marked';
  SetLocatesAffectedLabel;
end;

procedure TLocateWorkForm.ShowDialog;
begin
  Assert(DM.LocateData.FieldByName('ticket_id').AsInteger = FTicketID);
  LocateHoursDS.DataSet := LocateHoursData;
  if PermissionsDM.CanI(RightTicketsEditClosedLocates) then
    HoursGridTableView.OptionsSelection.HideSelection := True
  else
    HoursGridTableView.OptionsSelection.HideSelection := False;
  FocusFirstGridRow(HoursGrid, False);


  ShowModal;
  DebugLog('TLocateWorkForm.ShowDialog done');
end;

procedure TLocateWorkForm.AddHoursToLocates;

  function ConsiderLocateForHours(LocateID: Integer; const Status: string): Boolean;
  begin
    Result := False;
    if LocateID = FLocateID then
      Result := (Status = 'OH') or (Status = 'H')
    else if ApplyToAll.Checked then
      Result := (Status = 'OH') or (Status = '-R');
  end;

  function ConsiderLocateForUnits(LocateID: Integer; const Status: string): Boolean;
  begin
    Result := (LocateID = FLocateID) and (StringInArray(Status, DM.GetMarkableUnitStatusCodes));
  end;

  procedure AddLocateWork(LocateID: Integer);
  begin
    if EditingDataSet(LocateHoursData) then begin
      Assert(not LocateHoursData.FieldByName('emp_id').IsNull);
      Exit;
    end;

    LocateHoursData.Append;
    LocateHoursData.FieldByName('emp_id').AsInteger := DM.EmpID;
    LocateHoursData.FieldByName('work_date').AsDateTime := WorkDate.Date;
    LocateHoursData.FieldByName('locate_id').AsInteger := LocateID;
    LocateHoursData.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
  end;

  procedure AddLocateHours(LocateID: Integer);
  begin
    AddLocateWork(LocateID);

    Assert(EditingDataSet(LocateHoursData));

    LocateHoursData.FieldByName('regular_hours').AsFloat := decimalHoursReg; //QM-299  sr


    LocateHoursData.FieldByName('overtime_hours').AsFloat := decimalHoursOT;  //QM-299  sr

  end;

  procedure AddLocateUnits(LocateID: Integer);
  begin
    AddLocateWork(LocateID);

    Assert(EditingDataSet(LocateHoursData));

    if Length(StatusCombo.Text) > 0 then
      LocateHoursData.FieldByName('status').AsString := StatusCombo.Text;

    LocateHoursData.FieldByName('units_marked').AsInteger := Trunc(UnitsMarked.Value);
    if FUnitConversionID = NoConversionID then
      LocateHoursData.FieldByName('unit_conversion_id').Clear
    else
      LocateHoursData.FieldByName('unit_conversion_id').AsInteger := FUnitConversionID;
  end;

  function HasEnteredHours: Boolean;
  begin
    Result := (decimalHoursReg <> 0.0) or (decimalHoursOT <> 0.0) or CloseLocates.Checked;
  end;

  function HasEnteredUnits: Boolean;
  begin
    Result := (UnitsMarked.Value <> 0) or (Length(StatusCombo.Text) > 0);
  end;

var
  CurLocateID: Integer;
  CurLocateStatus: string;
begin
  DebugLog('In AddHoursToLocates');
  Assert(decimalHoursOT >= 0);
  Assert(decimalHoursReg >= 0);
  DebugLog('Validity Check 1');
  if (UnitsMarked.Value = 0) and (Length(StatusCombo.Text) > 0) then
    raise EOdDataEntryError.Create('Status is set without any Units');

  DebugLog('Validity Check 2');
  if (Length(StatusCombo.Text) = 0) and (UnitsMarked.Value > 0) then
    raise EOdDataEntryError.Create('Units marked without any Status');

  DebugLog('Validity Check 3');
  if (WorkDate.Date < (BeginningOfTheWeek(Now) - 7)) or (WorkDate.Date > EndingOfTheWeek(Now)) then
    raise EOdDataEntryError.Create('The work date must be during this week or last week.');

  DebugLog('DM.IgnoreSetStatus := True');
  DM.IgnoreSetStatus := True;
  try
    DebugLog('Locates.First;  Count: : ' + IntToStr(Locates.RecordCount));
    Locates.First;
    while not Locates.Eof do begin
      CurLocateID := Locates.FieldByName('locate_id').AsInteger;
      CurLocateStatus := Locates.FieldByName('status').AsString;
      DebugLog(Format('Locate: %d, %s', [CurLocateID, CurLocateStatus]));
      if ConsiderLocateForHours(CurLocateID, CurLocateStatus) and HasEnteredHours then begin
        DebugLog('AddLocateHours(CurLocateID)');
        AddLocateHours(CurLocateID);
        DebugLog('Locates.Edit');
        Locates.Edit;
        DebugLog('CloseLocates.Checked');
        if CloseLocates.Checked then
          Locates.FieldByName('status').AsString := 'H'
        else
          Locates.FieldByName('status').AsString := 'OH';
        DebugLog('Locates.Post');
        Locates.Post;
      end;

      DebugLog('ConsiderLocateForUnits');
      if ConsiderLocateForUnits(CurLocateID, CurLocateStatus) and HasEnteredUnits then begin
        DebugLog('AddLocateUnits');
        AddLocateUnits(CurLocateID);
      end;

      DebugLog('PostDataSet(LocateHoursData)');
      PostDataSet(LocateHoursData);
      DebugLog('Locates.Next');
      Locates.Next;
    end;
  finally
    DM.IgnoreSetStatus := False;
  end;
  btnTotalHrs.Enabled:=false;
end;

procedure TLocateWorkForm.btnTotalHrsClick(Sender: TObject);
var
  HrsReg,MinsReg:integer;   //QM-299  sr
  HrsOT,MinsOT:integer;    //QM-299  sr
begin
  AddHoursButton.Enabled:=false;
  decimalHoursReg:= 0.0; HrsReg := 0; MinsReg:=0;  //QM-299  sr

  if edtRegHrs.text<>'' then
  HrsReg := StrToInt(edtRegHrs.text)
  else HrsReg:=0;   //QM-299  sr
  if edtRegMins.text<>'' then
  MinsReg :=  StrToInt(edtRegMins.text)
  else MinsReg:=0;    //QM-299  sr


  decimalHoursOT := 0.0; HrsOT := 0; MinsOT:=0;  //QM-299  sr
  if edtOTHrs.text<>'' then
  HrsOT := StrToInt(edtOTHrs.text)
  else HrsOT:=0;   //QM-299  sr
  if edtOTMins.text<>'' then
  MinsOT :=  StrToInt(edtOTMins.text)
  else MinsOT:=0;    //QM-299  sr

  try
      decimalHoursReg := ((HrsReg*60)+MinsReg)/60;  //QM-299  sr
      decimalHoursOT := ((HrsOT*60)+MinsOT)/60;  //QM-299  sr

      TotalHoursLabel.Caption :=FloatToStrF((decimalHoursOT + decimalHoursReg),ffGeneral,5,2); //qm-299 sr
  except on E: Exception do
    showmessage(e.message);
  end;

  if (decimalHoursOT + decimalHoursReg)>0 then AddHoursButton.Enabled:=true;

end;

procedure TLocateWorkForm.SetBookmark;
begin
  FBookmark := Locates.Bookmark;
end;

procedure TLocateWorkForm.AddHoursButtonClick(Sender: TObject);
begin
  DebugLog('TLocateWorkForm.AddHoursButtonClick');
  DebugLog('PostDataSet(LocateHoursData)');
  PostDataSet(LocateHoursData);
  DebugLog('AddHoursToLocates');
  AddHoursToLocates;
  DebugLog('ModalResult := mrOk');
  ModalResult := mrOk;
end;

procedure TLocateWorkForm.SetLocatesAffectedLabel;
var
  LocateList: string;
  Status: string;
  Client: string;
begin
  Locates.DisableControls;//Prevent controls (i.e. TicketDetail.LocateGrid) from updating while looping through the Locates dataset
  try
    Locates.First;
    while not Locates.Eof do begin
      Status := Locates.FieldByName('status').AsString;
      if (Status = '-R') or (Status = 'OH') then begin
        Client := Locates.FieldByName('client_name').AsString;
        if LocateList = '' then
          LocateList := Client
        else
          LocateList := LocateList + ', ' + Client;
      end;
      Locates.Next;
    end;
  finally
    Locates.EnableControls;
  end;
  if LocateList = '' then
    LocateList := 'none';
  AllLocatesMemo.Text := '(' + LocateList + ')';
  AddMemoScrollbarsIfNecessary(AllLocatesMemo);
end;

procedure TLocateWorkForm.SetAllowAdd(const Hours, Units: Boolean);

  procedure AddWarning(Msg: string);
  begin
    if IsEmpty(WarningLabel.Caption) then
      WarningLabel.Caption := Msg
    else
      WarningLabel.Caption := WarningLabel.Caption + sLineBreak + Msg;
  end;

begin
  WarningLabel.Caption := '';
  SetFontColor(WarningLabel, clMaroon);
  // Let managers edit, but still enforce the status restrictions for new units/hours
  AddHoursButton.Enabled := ((Hours or Units) or PermissionsDM.CanI(RightTicketsEditAnotherLocatorsUnits)) and (not FLocateInvoiced);
  SetEnabledOnControlAndChildren(WorkGroup, (Hours or Units) and (not FLocateInvoiced));
  SetEnabledOnControlAndChildren(HoursGroup, Hours and (not FLocateInvoiced));
  SetEnabledOnControlAndChildren(UnitsGroup, Units and (not FLocateInvoiced));
  WarningLabel.Enabled := True;
  if FLocateInvoiced then
    AddWarning('Hours/units can not be changed because this locate is already invoiced.');
  if not Hours then
    AddWarning('Hours can only be added to OH status locates.');
  if not Units then
    AddWarning('This locate''s status does not allow adding units.');
end;

procedure TLocateWorkForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DebugLog('TLocateWorkForm.FormClose DM.UpdateLocatesTotalUnits');
  DM.UpdateLocatesTotalUnits;
  DebugLog('TLocateWorkForm.FormClose Bookmark: ' + FBookmark);
  if (FBookmark <> '') then
    Locates.Bookmark := FBookmark;
  DebugLog('TLocateWorkForm.FormClose Done');
end;

function TLocateWorkForm.Locates: TDataSet;
begin
  Result := DM.LocateData;
end;

function TLocateWorkForm.LocateHoursData: TDataSet;
begin
  Result := DM.LocateHoursData;
end;

procedure TLocateWorkForm.HoursGridTableViewEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  RegHoursCol: Integer;
  OverHoursCol: Integer;
  StatusCol: Integer;
  UnitsCol: Integer;
  EditingCol: Integer;
  LocateInvoiced: Boolean;
begin
  RegHoursCol := HoursGridTableView.GetColumnByFieldName('regular_hours').Index;
  OverHoursCol := HoursGridTableView.GetColumnByFieldName('overtime_hours').Index;
  StatusCol := HoursGridTableView.GetColumnByFieldName('status').Index;
  UnitsCol := HoursGridTableView.GetColumnByFieldName('units_marked').Index;

  EditingCol := AItem.Index;
  LocateInvoiced := DM.IsLocateInvoiced(HoursGridTableView.Controller.FocusedRow.Values[ColLocateId.Index]);
  AAllow :=  PermissionsDM.CanI(RightTicketsEditAnotherLocatorsUnits) and (not LocateInvoiced) and
            ((EditingCol = RegHoursCol) or (EditingCol = OverHoursCol) or
             (EditingCol = StatusCol) or (EditingCol = UnitsCol));
end;

procedure TLocateWorkForm.CloseButtonClick(Sender: TObject);
begin
  Assert( LocateHoursData <> nil , 'LocateHoursData is not assigned. - CloseButtonClick');
  PostDataSet(LocateHoursData);
end;

procedure TLocateWorkForm.HoursGridTableViewFocusedItemChanged(
  Sender: TcxCustomGridTableView; APrevFocusedItem,
  AFocusedItem: TcxCustomGridTableItem);
begin
  Assert(LocateHoursData <> nil, 'LocateHoursData is not assigned. - HoursGridTableFocusedItemChanged');
  PostDataSet(LocateHoursData);
end;

procedure TLocateWorkForm.HoursGridTableViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems4GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  AText:='Feet';
end;

procedure TLocateWorkForm.FormShow(Sender: TObject);
begin
  decimalHoursReg := 0.0; //qm-299 sr
  decimalHoursOT  := 0.0; //qm-299 sr
  ApplyToAll.Checked := False;
  AddHoursButton.Enabled:=false;
  try
    if edtRegHrs.CanFocus then  //qm-299 sr
      ActiveControl := edtRegHrs  //qm-299 sr
    else if UnitsMarked.CanFocus then
      ActiveControl := UnitsMarked
    else if HoursGrid.CanFocus then
      ActiveControl := HoursGrid;
  except
    // Ignore any focus errors
  end;
end;

procedure TLocateWorkForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
const
  WarningMsg = 'WARNING: You have not recorded any hours/units for this visit.  Add hours/units now?';
begin
  DebugLog('TLocateWorkForm.FormCloseQuery');
  if Assigned(LocateHoursDS.DataSet) then
    if LocateHoursDS.DataSet.Active then
      if LocateHoursDS.DataSet.RecordCount < 1 then
        if not FLocateInvoiced then
          if (MessageDlg(WarningMsg, mtWarning, [mbYes, mbNo], 0) = mrYes) then
            CanClose := False;
  DebugLog('TLocateWorkForm.FormCloseQuery Done');
end;

procedure TLocateWorkForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

procedure TLocateWorkForm.DebugLog(const Msg: string);
begin
  //DM.AddToQMLog(Msg);
end;

procedure TLocateWorkForm.edtRegHrsKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9']) then   //qm-299 sr
  begin
    key := #0;
    btnTotalHrs.Enabled:=false;
  end
    else
    btnTotalHrs.Enabled:=true;
end;

end.

