inherited TimesheetApprovalForm: TTimesheetApprovalForm
  Left = 332
  Top = 0
  Caption = 'Timesheet Approval'
  ClientHeight = 682
  ClientWidth = 915
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 915
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object ShowLabel: TLabel
      Left = 32
      Top = 9
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Show:'
    end
    object EmployeesLabel: TLabel
      Left = 7
      Top = 37
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Employees:'
    end
    object Label1: TLabel
      Left = 700
      Top = 42
      Width = 158
      Height = 13
      Caption = 'DO NOT RESIZE EMPLOYEES...'
      Color = clRed
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    inline EmployeeSelect: TEmployeeSelect
      Left = 64
      Top = 33
      Width = 209
      Height = 22
      TabOrder = 1
      inherited EmployeeCombo: TComboBox
        Width = 176
      end
    end
    object ApproveButton: TButton
      Left = 602
      Top = 4
      Width = 80
      Height = 25
      Action = ApproveAction
      TabOrder = 5
    end
    object FinalApproveButton: TButton
      Left = 602
      Top = 32
      Width = 80
      Height = 25
      Action = FinalApproveAction
      TabOrder = 6
    end
    object SelectAllButton: TButton
      Left = 514
      Top = 32
      Width = 80
      Height = 25
      Action = SelectAllAction
      TabOrder = 4
    end
    inline DateRange: TOdRangeSelectFrame
      Left = 261
      Top = 0
      Width = 248
      Height = 63
      TabOrder = 2
      inherited FromLabel: TLabel
        Left = 16
        Top = 9
      end
      inherited ToLabel: TLabel
        Left = 139
        Top = 9
      end
      inherited DatesLabel: TLabel
        Left = 12
        Top = 37
      end
      inherited DatesComboBox: TComboBox
        Left = 48
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 48
        Top = 6
        Properties.ButtonGlyph.Data = {
          C2030000424DC203000000000000B6000000280000000F0000000D0000000100
          2000000000000C03000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080808000808080008080800080808000808000008080000080808000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
          8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
          00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
          000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080808000808080008080800080808000808000008080000080808000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
          0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
          0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00808080008080000080800000808080008080800080808000808080008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080800000}
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 160
        Top = 6
        Properties.ButtonGlyph.Data = {
          C2030000424DC203000000000000B6000000280000000F0000000D0000000100
          2000000000000C03000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080808000808080008080800080808000808000008080000080808000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
          8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
          00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
          0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
          000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080808000808080008080800080808000808000008080000080808000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
          0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
          0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00808080008080000080800000808080008080800080808000808080008080
          8000808080008080800080808000808080008080800080808000808080008080
          800080800000}
      end
    end
    object SearchButton: TButton
      Left = 514
      Top = 4
      Width = 80
      Height = 25
      Action = SearchAction
      TabOrder = 3
    end
    object SearchTypeCombo: TComboBox
      Left = 65
      Top = 6
      Width = 195
      Height = 21
      Style = csDropDownList
      DropDownCount = 16
      ItemHeight = 13
      TabOrder = 0
    end
    object ExportButton: TButton
      Left = 691
      Top = 4
      Width = 80
      Height = 25
      Action = ExportAction
      TabOrder = 7
    end
  end
  object RightPanel: TPanel
    Left = 783
    Top = 61
    Width = 132
    Height = 621
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    inline TimesheetDetails: TTimeClockDayApprovalFrame
      Left = 1
      Top = 0
      Width = 131
      Height = 621
      Align = alRight
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      inherited BelowCalloutPanel: TPanel
        inherited LeaveVacPanel: TPanel
          inherited VacationHours: TcxDBCurrencyEdit
            ParentColor = True
          end
        end
        inherited BelowCalloutBottomPanel: TPanel
          inherited DayHours: TLabel
            Top = 101
          end
          inherited DayHoursLabel: TLabel
            Top = 101
          end
          inherited ReasonChanged: TcxComboBox [10]
          end
          inherited ApprovalMemo: TMemo [11]
            Top = 334
          end
          inherited BereavementHours: TcxDBCurrencyEdit [12]
            ParentColor = True
          end
          inherited HolidayHours: TcxDBCurrencyEdit [13]
            ParentColor = True
          end
          inherited JuryDutyHours: TcxDBCurrencyEdit [14]
            ParentColor = True
          end
          inherited MilesStart1: TcxDBCurrencyEdit [15]
            ParentColor = True
          end
          inherited MilesStop1: TcxDBCurrencyEdit [16]
            ParentColor = True
          end
          inherited FloatingHoliday: TDBCheckBox [17]
            Left = 85
            Top = 61
            Width = 76
          end
          inherited VehicleUse: TcxDBComboBox [18]
          end
          inherited EmployeeType: TComboBox [19]
            ParentColor = True
          end
        end
      end
      inherited LegendGroup: TGroupBox
        Left = 4
        Top = 481
        Width = 122
      end
      inherited TimeClockGrid: TcxGrid
        Width = 131
      end
      inherited TimesheetSource: TDataSource
        Left = 51
        Top = 79
      end
      inherited TimesheetData: TDBISAMTable
        Left = 21
        Top = 78
      end
      inherited DefaultEditStyleController: TcxDefaultEditStyleController
        PixelsPerInch = 96
      end
      inherited TimeClockSource: TDataSource
        Left = 95
        Top = 69
      end
      inherited ClockDetailData: TDBISAMQuery
        Left = 90
      end
      inherited TimeClockStyleRepo: TcxStyleRepository
        PixelsPerInch = 96
      end
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 61
    Width = 783
    Height = 621
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object GridDBTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridDBTableViewCustomDrawCell
      OnEditing = GridDBTableViewEditing
      DataController.DataSource = TimesheetSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'unique_pk'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.BandHeaderLineCount = 2
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Bands = <
        item
        end>
      object ColUniquePK: TcxGridDBBandedColumn
        DataBinding.FieldName = 'unique_pk'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Filtering = False
        Width = 56
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ColEntryID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'entry_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 7329654
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ColWorkDate: TcxGridDBBandedColumn
        Caption = 'Date'
        DataBinding.FieldName = 'work_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        Width = 38608
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ColApprove: TcxGridDBBandedColumn
        Caption = 'Approve'
        DataBinding.FieldName = 'approve'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        OnCustomDrawCell = ColApproveCustomDrawCell
        MinWidth = 33
        Options.Filtering = False
        Width = 33
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColSelectionFlag: TcxGridDBBandedColumn
        Caption = 'Flag'
        DataBinding.FieldName = 'flag'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        MinWidth = 33
        Options.Editing = False
        Options.Filtering = False
        Width = 33
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ColShortName: TcxGridDBBandedColumn
        Caption = 'Employee'
        DataBinding.FieldName = 'short_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        MinWidth = 130
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 130
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ColMilesDriven: TcxGridDBBandedColumn
        Caption = 'Miles'
        DataBinding.FieldName = 'miles_driven'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 45
        Options.Editing = False
        Options.Filtering = False
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object ColVehicleUse: TcxGridDBBandedColumn
        Caption = 'Vehicle Use'
        DataBinding.FieldName = 'vehicle_use'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        MinWidth = 85
        Options.Editing = False
        Options.Filtering = False
        Width = 85
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object ColWorkStart1: TcxGridDBBandedColumn
        Caption = 'Wk Start 1'
        DataBinding.FieldName = 'work_start1'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object ColWorkStop1: TcxGridDBBandedColumn
        Caption = 'Wk Stop 1'
        DataBinding.FieldName = 'work_stop1'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object ColWorkStart2: TcxGridDBBandedColumn
        Caption = 'Wk Start 2'
        DataBinding.FieldName = 'work_start2'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object ColWorkStop2: TcxGridDBBandedColumn
        Caption = 'Wk Stop 2'
        DataBinding.FieldName = 'work_stop2'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object ColWorkStart3: TcxGridDBBandedColumn
        Caption = 'Wk Start 3'
        DataBinding.FieldName = 'work_start3'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object ColWorkStop3: TcxGridDBBandedColumn
        Caption = 'Wk Stop 3'
        DataBinding.FieldName = 'work_stop3'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
      object ColWorkStart4: TcxGridDBBandedColumn
        Caption = 'Wk Start 4'
        DataBinding.FieldName = 'work_start4'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object ColWorkStop4: TcxGridDBBandedColumn
        Caption = 'Wk Stop 4'
        DataBinding.FieldName = 'work_stop4'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 1
      end
      object ColCalloutStart1: TcxGridDBBandedColumn
        Caption = 'Co Start 1'
        DataBinding.FieldName = 'callout_start1'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object ColCalloutStop1: TcxGridDBBandedColumn
        Caption = 'Co Stop 1'
        DataBinding.FieldName = 'callout_stop1'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Properties.TimeFormat = tfHourMin
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 1
      end
      object ColCalloutStart2: TcxGridDBBandedColumn
        Caption = 'Co Start 2'
        DataBinding.FieldName = 'callout_start2'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object ColCalloutStop2: TcxGridDBBandedColumn
        Caption = 'Co Stop 2'
        DataBinding.FieldName = 'callout_stop2'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 57
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 1
      end
      object ColRegHrs: TcxGridDBBandedColumn
        Caption = 'Reg'
        DataBinding.FieldName = 'reg_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object ColOTHrs: TcxGridDBBandedColumn
        Caption = 'OT'
        DataBinding.FieldName = 'ot_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object ColCalloutHrs: TcxGridDBBandedColumn
        Caption = 'Callout'
        DataBinding.FieldName = 'callout_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 1
      end
      object ColLeaveHrs: TcxGridDBBandedColumn
        Caption = 'Sick'
        DataBinding.FieldName = 'leave_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 1
      end
      object ColHolHrs: TcxGridDBBandedColumn
        Caption = 'Holiday'
        DataBinding.FieldName = 'hol_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object ColDTHrs: TcxGridDBBandedColumn
        Caption = 'DT'
        DataBinding.FieldName = 'dt_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object ColVacHrs: TcxGridDBBandedColumn
        DataBinding.FieldName = 'vac_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 1
        IsCaptionAssigned = True
      end
      object ColPTO: TcxGridDBBandedColumn
        Caption = 'PTO'
        DataBinding.FieldName = 'pto_hours'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 42
        Options.Editing = False
        Options.Filtering = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 1
      end
      object ColHidden1: TcxGridDBBandedColumn
        Caption = 'Total'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 41
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object ColFloatingHoliday: TcxGridDBBandedColumn
        Caption = 'Float-H'
        DataBinding.FieldName = 'floating_holiday'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Options.Editing = False
        Options.Filtering = False
        Width = 41
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object ColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Editing = False
        Options.Filtering = False
        Width = 74
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 1
      end
      object ColApproveBy: TcxGridDBBandedColumn
        Caption = 'Approve By'
        DataBinding.FieldName = 'approve_by'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Filtering = False
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object ColFinalApproveBy: TcxGridDBBandedColumn
        Caption = 'Final Approve By'
        DataBinding.FieldName = 'final_approve_by'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Visible = False
        Options.Filtering = False
        Width = 86
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object ColPerDiem: TcxGridDBBandedColumn
        Caption = 'P-Diem'
        DataBinding.FieldName = 'per_diem'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DisplayGrayed = 'False'
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object ColEntryBy: TcxGridDBBandedColumn
        Caption = 'Entry By'
        DataBinding.FieldName = 'entry_by'
        Visible = False
        VisibleForCustomization = False
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object ColSource: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Source'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
    end
    object GridDBBandedTableView1: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Bands = <
        item
        end>
      object GridDBBandedTableView1Column1: TcxGridDBBandedColumn
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  object TimesheetSource: TDataSource
    DataSet = Timesheets
    Left = 128
    Top = 188
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 28
    Top = 188
    object SearchAction: TAction
      Caption = '&Search'
      OnExecute = SearchActionExecute
    end
    object ApproveAction: TAction
      Caption = '&Approve'
      OnExecute = ApproveActionExecute
    end
    object FinalApproveAction: TAction
      Caption = '&Final Approve'
      OnExecute = FinalApproveActionExecute
    end
    object SelectAllAction: TAction
      Caption = 'Select &All'
      OnExecute = SelectAllActionExecute
    end
    object ShowCalloutsAction: TAction
      Caption = 'All'
    end
    object ExportAction: TAction
      Caption = 'Export...'
      OnExecute = ExportActionExecute
    end
  end
  object Timesheets: TDBISAMTable
    AfterScroll = TimesheetsAfterScroll
    DatabaseName = 'Memory'
    EngineVersion = '4.34 Build 7'
    Exclusive = True
    IndexDefs = <
      item
        Name = 'TimesheetsWorkDateShortName'
        Fields = 'work_date;short_name'
        Options = [ixCaseInsensitive]
      end>
    IndexName = 'TimesheetsWorkDateShortName'
    StoreDefs = True
    Left = 80
    Top = 188
  end
end
