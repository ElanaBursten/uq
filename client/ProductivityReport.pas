unit ProductivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdTimeRangeSelect, CheckLst;

type
  TProductivityReportForm = class(TReportBaseForm)
    DateRangeGroup: TGroupBox;
    RangeSelect: TOdTimeRangeSelectFrame;
    OfficeGroup: TGroupBox;
    ManagerLabel: TLabel;
    ManagersComboBox: TComboBox;
    OptionsGroup: TGroupBox;
    StatusCheckListBox: TCheckListBox;
    CallCenterClientsBox: TGroupBox;
    Label2: TLabel;
    CallCenterList: TCheckListBox;
    Label3: TLabel;
    ClientList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure CallCenterListClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdExceptions, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TProductivityReportForm }

procedure TProductivityReportForm.ValidateParams;
var
  CallCenters: string;
  Clients: string;
begin
  inherited;
  SetReportID('Productivity');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParam('ExcludeStatuses', GetCheckedItemString(StatusCheckListBox));

  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters<>'' then begin
    Clients := GetCheckedItemString(ClientList, True);
    if Clients = '' then begin
      ClientList.SetFocus;
      raise EOdEntryRequired.Create('Please select the clients to report on.');
    end;
  end;
  SetParam('LimitCallCenters', CallCenters);
  SetParam('LimitClients', Clients);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TProductivityReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  DM.GetStatusStrings(StatusCheckListBox.Items, True);
  SetupCallCenterList(CallCenterList.Items, False);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TProductivityReportForm.CallCenterListClick(Sender: TObject);
var
  CallCenters: string;
begin
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure TProductivityReportForm.SelectAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, True);
end;

procedure TProductivityReportForm.ClearAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, False);
end;

end.
