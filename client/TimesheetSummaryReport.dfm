inherited TimesheetSummaryReportForm: TTimesheetSummaryReportForm
  Left = 310
  Top = 319
  Caption = 'TimesheetSummaryReportForm'
  ClientHeight = 351
  ClientWidth = 363
  PixelsPerInch = 96
  TextHeight = 13
  object PeriodLabel: TLabel [0]
    Left = 0
    Top = 12
    Width = 80
    Height = 18
    AutoSize = False
    Caption = 'Week Ending:'
    WordWrap = True
  end
  object ManagerLabel: TLabel [1]
    Left = 0
    Top = 44
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label2: TLabel [2]
    Left = 0
    Top = 100
    Width = 47
    Height = 13
    Caption = 'Approval:'
  end
  object Label3: TLabel [3]
    Left = 0
    Top = 132
    Width = 73
    Height = 13
    Caption = 'Report Layout:'
  end
  object Label4: TLabel [4]
    Left = 0
    Top = 159
    Width = 55
    Height = 13
    Caption = 'Sort Order:'
  end
  object Label6: TLabel [5]
    Left = 0
    Top = 187
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [6]
    Left = 96
    Top = 41
    Width = 204
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object IncludeDailyDetailBox: TCheckBox [7]
    Left = 96
    Top = 73
    Width = 121
    Height = 17
    Caption = 'Include Daily Detail'
    TabOrder = 2
    OnClick = IncludeDailyDetailBoxClick
  end
  object ApprovalCombo: TComboBox [8]
    Left = 96
    Top = 97
    Width = 185
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 3
    Text = 'Entered Time Data'
    Items.Strings = (
      'Entered Time Data'
      'Final-Approved Data Only'
      'Non Final-Approved Data Only')
  end
  object ReportLayoutCombo: TComboBox [9]
    Left = 96
    Top = 129
    Width = 185
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
  end
  object SortCombo: TComboBox [10]
    Left = 96
    Top = 156
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    Items.Strings = (
      'Short Name'
      'Last Name'
      'Emp Number')
  end
  object EmployeeStatusCombo: TComboBox [11]
    Left = 96
    Top = 184
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
  end
  object ChooseDate: TcxDateEdit [12]
    Left = 96
    Top = 9
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = ChooseDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 100
  end
end
