unit FirstTaskSetDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxTimeEdit;

type
  TFirstTaskReminderForm = class(TForm)    //QM-494 First Task Reminder EB
    lblEmployee: TLabel;
    ReminderTimeEdit: TcxTimeEdit;
    btnSet: TcxButton;
    btnCancel: TcxButton;
    procedure btnSetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fEmpID: integer;
    fResetIt: boolean;
  public
    property EmpID: integer read fEmpID write fEmpID;
  end;
  function OpenFirstTaskReminderFor(EmployeeID: integer): boolean;

var
  FirstTaskReminderForm: TFirstTaskReminderForm;

implementation
uses LocalEmployeeDMu;

{$R *.dfm}
function OpenFirstTaskReminderFor(EmployeeID: integer): boolean;   //QM-494 First Task Reminder EB
var
  TimeStr: string;
begin
  FirstTaskReminderForm := TFirstTaskReminderForm.Create(nil);
  try
    With FirstTaskReminderForm do begin
      EmpID := EmployeeID;
      lblEmployee.Caption := EmployeeDM.GetEmployeeShortName(EmpID);
      TimeStr := EmployeeDM.GetCurrentFirstTaskTime(EmpID);
//      Showmessage(TimeStr);
      try
       ReminderTimeEdit.Time := StrToTime(TimeStr);
      except
        ReminderTimeEdit.Time := 0.0;
      end;

      If ShowModal = mrOK then begin
        if fResetIt then
          EmployeeDM.UpdateFirstTaskReminder(EmpID, ReminderTimeEdit.EditText);
      end;
      fResetIt := False;
    end;
  finally
    FreeAndNil(FirstTaskReminderForm);
  end;
end;

procedure TFirstTaskReminderForm.btnSetClick(Sender: TObject);
begin
  fResetIt := True;
end;

procedure TFirstTaskReminderForm.FormCreate(Sender: TObject);
begin
  fResetIt := False;
end;

end.
