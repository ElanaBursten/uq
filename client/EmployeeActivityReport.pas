unit EmployeeActivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ComCtrls, OdRangeSelect;

type
  TEmployeeActivityReportForm = class(TReportBaseForm)
    lblManager: TLabel;
    ManagersComboBox: TComboBox;
    lbIncludeEmployee: TLabel;
    EmployeeStatusCombo: TComboBox;
    ActivitiesGroupBox: TGroupBox;
    rbOutOfHorkingHours: TRadioButton;
    rbAllActivities: TRadioButton;
    rbOutOfSpanHours: TRadioButton;
    lblFrom: TLabel;
    dtpHourSpanStart: TDateTimePicker;
    lblTo: TLabel;
    dtpHourSpanEnd: TDateTimePicker;
    RangeSelect: TOdRangeSelectFrame;
    AllCheckBox: TCheckBox;
    EmployeeLabel: TLabel;
    LocatorComboBox: TComboBox;
    ReportTypeGroupBox: TGroupBox;
    rbActivityDetail: TRadioButton;
    rbActivitySummary: TRadioButton;
    AttachLSACheckBox: TCheckBox;
    Label1: TLabel;
    IncludeChartCombo: TComboBox;
    ContentsLabel: TLabel;
    procedure rbOutOfHorkingHoursClick(Sender: TObject);
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure AllCheckBoxClick(Sender: TObject);
    procedure rbActivityDetailClick(Sender: TObject);
    procedure rbActivitySummaryClick(Sender: TObject);
  private
    procedure RepopulateLocatorList;
    function LocatorMode: Boolean;
    procedure SetReportLimits;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure SetVisibility;
  end;

implementation

uses DMu, OdVclUtils, OdIsoDates, OdMiscUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TEmployeeActivityReportForm }

procedure TEmployeeActivityReportForm.InitReportControls;
begin
  inherited;
  AllCheckBox.Checked := True;
  RangeSelect.SelectDateRange(rsLastWeek);
  SetUpManagerList(ManagersComboBox);
  if ManagersComboBox.Items.Count > 1 then
    ManagersComboBox.ItemIndex := 0;
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  SetVisibility;
  rbOutOfHorkingHours.Checked := True;
end;

procedure TEmployeeActivityReportForm.SetVisibility;
begin
  dtpHourSpanStart.Enabled := rbOutOfSpanHours.Checked;
  dtpHourSpanEnd.Enabled := rbOutOfSpanHours.Checked;
  EmployeeStatusCombo.Enabled := not LocatorMode;
  LocatorComboBox.Enabled := LocatorMode;
end;


procedure TEmployeeActivityReportForm.SetReportLimits;
begin
  rbOutOfSpanHours.Enabled := not rbActivitySummary.Checked;
  // set activties to acceptable value
  if rbActivitySummary.Checked and rbOutOfSpanHours.Checked then
    rbOutOfHorkingHours.Checked := True;
end;

procedure TEmployeeActivityReportForm.ValidateParams;
begin
  inherited;
  if rbActivitySummary.Checked then
    SetReportID('EmployeeActivitySummary')
  else
    SetReportID('EmployeeActivity');

  SetParamDate('StartDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));

  if rbOutOfHorkingHours.Checked then
    SetParamInt('OutOfRangeActivities',1)
  else if rbOutOfSpanHours.Checked then begin
    dtpHourSpanStart.Date := StrToDate('1/1/1900');
    dtpHourSpanEnd.Date := StrToDate('1/1/1900');
    if dtpHourSpanStart.Time>dtpHourSpanEnd.Time then
      raise Exception.Create('End time is earlier than start time');
    SetParam('HourSpanStart',IsoDateTimeToStr(dtpHourSpanStart.Time));
    SetParam('HourSpanEnd',IsoDateTimeToStr(dtpHourSpanEnd.Time));
  end;

  if LocatorMode then
    SetParamInt('EmployeeId', GetComboObjectInteger(LocatorComboBox))
  else
    SetParamInt('EmployeeId', -1);
  SetParam('IncludeLSA', BooleanToString01(AttachLSACheckBox.Checked));
  SetParam('ReportContents', IncludeChartCombo.Text);
end;

procedure TEmployeeActivityReportForm.rbOutOfHorkingHoursClick(Sender: TObject);
begin
  inherited;
  SetVisibility;
end;

function TEmployeeActivityReportForm.LocatorMode: Boolean;
begin
  Result := not AllCheckBox.Checked;
end;

procedure TEmployeeActivityReportForm.RepopulateLocatorList;
begin
  if LocatorMode then begin
    if ManagersComboBox.ItemIndex > -1 then begin
      EmployeeDM.EmployeeList(LocatorComboBox.Items, GetComboObjectInteger(ManagersComboBox))
    end;
  end else
    LocatorComboBox.Items.Clear;
end;

procedure TEmployeeActivityReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  RepopulateLocatorList;
end;

procedure TEmployeeActivityReportForm.AllCheckBoxClick(Sender: TObject);
begin
  SetVisibility;
  RepopulateLocatorList;
end;

procedure TEmployeeActivityReportForm.rbActivityDetailClick(Sender: TObject);
begin
  inherited;
  SetReportLimits;
end;

procedure TEmployeeActivityReportForm.rbActivitySummaryClick(Sender: TObject);
begin
  inherited;
  SetReportLimits;
end;

end.
