object TaskTimeDialog: TTaskTimeDialog
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Select Task Time'
  ClientHeight = 112
  ClientWidth = 245
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object InstructionsLabel: TLabel
    Left = 10
    Top = 7
    Width = 202
    Height = 26
    Caption = 'Enter the time you expect to start on this ticket.'
    Transparent = True
    WordWrap = True
  end
  object ErrorLabel: TLabel
    Left = 10
    Top = 52
    Width = 142
    Height = 13
    Caption = 'A date and time are requried.'
    Transparent = True
  end
  object OKBtn: TButton
    Left = 45
    Top = 76
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelBtn: TButton
    Left = 125
    Top = 76
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object DateEdit: TcxDateEdit
    Left = 50
    Top = 24
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 89
  end
  object TimeEdit: TcxTimeEdit
    Left = 147
    Top = 24
    EditValue = 0d
    Properties.TimeFormat = tfHourMin
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 1
    Width = 59
  end
end
