unit MARSBulkStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ExtCtrls, Dmu, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, LocalPermissionsDMu,LocalEmployeeDmu, cxCheckBox, StdCtrls, DBClient,
  MSXML2_TLB, OdMSXMLUtils, OdMiscUtils, OdIsoDates, 
  cxDBLookupComboBox, cxButtonEdit, cxEditRepositoryItems, 
  ActnList, cxDBExtLookupComboBox, cxCustomData, cxFilter, cxData;

type

  TfrmMARSBulkStatus = class(TEmbeddableForm)
    pnlTop: TPanel;
    lblManager: TLabel;
    btnRefresh: TButton;
    StatusGrid: TcxGrid;
    StatusGridView: TcxGridDBTableView;
    CheckBoxCol: TcxGridDBColumn;
    EmpIDCol: TcxGridDBColumn;
    ShortNameCol: TcxGridDBColumn;
    EmpNumCol: TcxGridDBColumn;
    NewStatusCol: TcxGridDBColumn;
    InsertDateCol: TcxGridDBColumn;
    InsertedByIDCol: TcxGridDBColumn;
    InsertedByNameCol: TcxGridDBColumn;
    MarsActiveCol: TcxGridDBColumn;
    StatusGridLevel: TcxGridLevel;
    MarsSource: TDataSource;
    lblCount: TLabel;
    DSNewStatus: TDataSource;
    ButtonCol: TcxGridDBColumn;
    RowMessageCol: TcxGridDBColumn;
    ProcessStatusCol: TcxGridDBColumn;
    cxEditRepository1: TcxEditRepository;
    RepoButtonSubmit: TcxEditRepositoryButtonItem;
    RepoButtonSTOP: TcxEditRepositoryButtonItem;
    ActionList1: TActionList;
    actionStop: TAction;
    actionSubmit: TAction;
    RepoButtonSubmitFreeze: TcxEditRepositoryButtonItem;
    RepoButtonSTOPFreeze: TcxEditRepositoryButtonItem;
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxColPropertiesChange(Sender: TObject);
    procedure StatusGridViewFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure StatusGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ButtonColGetProperties(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure actionStopExecute(Sender: TObject);
    procedure actionSubmitExecute(Sender: TObject);
    procedure ButtonColPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btnRefreshClick(Sender: TObject);
  private
    fListLoaded: Boolean;
    fOneTimeListRefresh: Boolean;
    fMgrID: integer;  {Manager level set in permission}

    procedure Init;
    procedure ClearList;
    procedure CreateAvailStatusCDS;
    procedure PostActionRefresh;
  public
    AvailStatusCDS: TClientDataSet;
    MarsListDS: TClientDataSet;
//    function GetMyMarsList: integer; //return number of employees
    function GetMgrLevelPermissions: integer;
    function GetEmpStatusList(pEmpID: integer; var pStatList: TStringList): integer;
    function LoadMars(MgrID: integer): boolean;
    function RefreshAvailStatus: boolean;
    function PopulateMarsListfromDOM(Doc: IXMLDomDocument; TargetNode: Integer): integer;
    function CreateMarsTable: boolean;

    function SendMarsRequest: boolean;
    function STOPMarsRequest: boolean;
  end;

var
  frmMARSBulkStatus: TfrmMARSBulkStatus;
  
const
  MARSMgmtpermission = 'TicketsMARSBulkStatusMgmt';
  MARSpermission =     'TicketsMARSBulkStatus';


  USFAIL1 = -102;     {New status is blank}
  USFAIL2 = -102;     {Server Error}
  USFAIL3 = -103;     {Request No longer Pending Error}

  USSTOPPENDING = -10;
  USSTOPPED = -11;
  USREADY = 1;
  USPENDING = 2;
  USPROCESSING = 3;

  MSGPROCESSING = 'Processing';
  MSGPENDING = 'Pending: Request Sent';
  MSGSTOPPED = 'Request Stopped Successfully';

  MSGSTOPPED2 = 'Cancelled';

  MSGFAIL1 = 'Submit Failed: New Status is blank';
  MSGFAIL2 = 'Submit Failed: An Error occurred sending to the server';
  MSGFAIL3 = 'Stop Req Failed: Request was no longer pending';

implementation


{$R *.dfm}

{ TfrmMARSBulkStatus }

procedure TfrmMARSBulkStatus.actionStopExecute(Sender: TObject);
begin
  inherited;
  STOPMarsRequest;

end;

procedure TfrmMARSBulkStatus.actionSubmitExecute(Sender: TObject);
begin
  inherited;
  SendMarsRequest;
end;

procedure TfrmMARSBulkStatus.btnRefreshClick(Sender: TObject);
begin
  inherited;
  LoadMars(fMgrID);
end;

procedure TfrmMARSBulkStatus.ButtonColGetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  lProgressStatus: integer;
begin
  inherited;
  lProgressStatus := ARecord.Values[ProcessStatusCol.Index];

  if (lProgressStatus = USSTOPPENDING) then
    AProperties := RepoButtonSubmitFreeze.Properties
  else if (lProgressStatus = USPENDING) then
    AProperties := RepoButtonSTOPFreeze.Properties
  else if (lProgressStatus = USPROCESSING) then
    AProperties := RepoButtonSTOP.Properties
  else
    AProperties := RepoButtonSubmit.Properties;
end;


procedure TfrmMARSBulkStatus.ButtonColPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  PostActionRefresh;
end;

procedure TfrmMARSBulkStatus.CheckBoxColPropertiesChange(Sender: TObject);
begin
  inherited;
  //Leaving in for now
end;

procedure TfrmMARSBulkStatus.ClearList;
begin
  MarsListDS.EmptyDataSet;
  fListLoaded := False;
end;

procedure TfrmMARSBulkStatus.CreateAvailStatusCDS;
begin
  AvailStatusCDS:= TClientDataSet.Create(Application);
  AvailStatusCDS.FieldDefs.Add('new_status',ftstring, 5);
  AvailStatusCDS.CreateDataSet;
end;

function TfrmMARSBulkStatus.CreateMarsTable: boolean;
begin
  MarsListDS := TClientDataSet.Create(Application);
  MarsListDS.FieldDefs.Add('mCheckbox', ftBoolean);
  MarsListDS.FieldDefs.Add('mars_id', ftinteger);
  MarsListDS.FieldDefs.Add('emp_id', ftInteger);
  MarsListDS.FieldDefs.Add('short_name', ftString, 30);
  MarsListDS.FieldDefs.Add('emp_number', ftString, 20);
  MarsListDS.FieldDefs.Add('new_status', ftString, 5);
  MarsListDS.FieldDefs.Add('insert_date', ftDateTime);
  MarsListDS.FieldDefs.Add('inserted_by', ftInteger);
  MarsListDS.FieldDefs.Add('inserted_by_name', ftString, 30);
  MarsListDS.FieldDefs.Add('mars_active', ftBoolean);
  MarsListDS.FieldDefs.Add('limitation', ftString, 200);
  MarsListDS.FieldDefs.Add('inProgress', ftinteger);
  MarsListDS.FieldDefs.Add('RowMessage',ftString, 100);
  MarsListDS.CreateDataSet;
end;

procedure TfrmMARSBulkStatus.FormCreate(Sender: TObject);
begin
  inherited;
  fListLoaded := False;
  fOneTimeListRefresh := False;
  Init;
end;

function TfrmMARSBulkStatus.GetEmpStatusList(pEmpID: integer;
  var pStatList: TStringList): integer;
var
  lStatStr: string;
begin
  if PermissionsDM.CanEmployee(pEmpID, MARSPermission) then begin
    lStatStr := PermissionsDM.GetEmployeeLimitation(pEmpID, MARSpermission);
    if not Assigned(pStatList) then
      pStatList := TStringList.Create;
    pstatList.StrictDelimiter := True;
    pStatList.Delimiter := ',';
    pStatList.DelimitedText := lStatStr;
    Result := pStatList.Count; {Return the number of Statuses}
  end
  else
    Result := 0;
end;

function TfrmMARSBulkStatus.GetMgrLevelPermissions: integer;
var
  my_emp_id: integer;
  lMgrStr: string;
begin
  my_emp_id := DM.UQState.EmpID;
  try
      lMgrStr := PermissionsDM.GetLimitation(MARSMgmtpermission);
      fMgrID := StrToInt(lMgrSTr);
      Result := fMgrID;
  except
   ShowMessage('Manager is invalid in permission. Cannot load list.');
   Result := 0;
   fMgrID := 0;
  end;
end;



procedure TfrmMARSBulkStatus.Init;
begin
  fMgrID := GetMgrLevelPermissions;
  if DM.UQState.DeveloperMode then begin
    EmpIDCol.VisibleForCustomization := True;
    InsertedByIDCol.VisibleForCustomization := True;
    ProcessStatusCol.VisibleForCustomization := True;
  end;

  CreateAvailStatusCDS;
  CreateMarsTable;
  LoadMars(fMgrID);
end;

function TfrmMARSBulkStatus.LoadMars(MgrID: integer): boolean;
var
  Doc: IXMLDomDocument;
  RecCount: integer;
begin
  fListLoaded := False; {needed during refresh}
  Doc := ParseXML(EmployeeDM.GetMARSEmpList(MgrID));
  RecCount := PopulateMarsListFromDOM(Doc, MgrID);
  lblManager.Caption := 'Manager:  '  + EmployeeDM.GetEmployeeShortName(MgrID);
  RefreshAvailStatus;
  fListLoaded :=True;
end;




function TfrmMARSBulkStatus.PopulateMarsListfromDOM(Doc: IXMLDomDocument;
  TargetNode: Integer): integer;
var
  i: integer;
  Nodes: IXMLDOMNodeList;
  Elem: IXMLDOMElement;
  Count: integer;
  RecActive: boolean;
  RecActiveTxt: string;

  {Copying from WorkManageUtils - could refactor later}
  function AttrText(const AttrName: string): string;
  var
    AttrNode: IXMLDomNode;
  begin
    AttrNode := Elem.attributes.getNamedItem(AttrName);
    if AttrNode <> nil then
      Result := AttrNode.text
    else
      Result := '-';
  end;

  function AttrInt(const AttrName: string): Integer;
  var
    AttrStr: string;
  begin
    AttrStr := AttrText(AttrName);
    try
      Result := StrToInt(AttrStr);
    except
      on E: Exception do begin
        E.Message := Format('Invalid Integer text "%s" for attribute "%s"', [AttrStr, AttrName]);
        raise;
      end;
    end;
  end;

  function AttrDateTime(const AttrName: string): Variant;
  var
    AttrStr: string;
  begin
    Result := Null;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and IsoIsValidDateTime(AttrStr) then
      Result := IsoStrToDateTime(AttrStr);
  end;

  function AttrFloat(const AttrName: string): Double;
  var
    AttrStr: string;
  begin
    Result := 0.0;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and (AttrStr <> NULL) then
      Result := StrToFloat(AttrStr);
  end;

{BEGIN}
begin
  Count := 0;
  MarsListDS.DisableControls;
  while not MarsListDS.IsEmpty do
    MarsListDS.Delete;

  try
    Nodes := Doc.SelectNodes('//MARS_EmpList');
    {Process the records}
    for i := 0 to Nodes.Length-1 do begin
      RecActive := True;
      RecActiveTxt := '';
      Elem := Nodes[i] as IXMLDOMElement;

      MarsListDS.Open;
      MarsListDS.Append;
      inc(Count);
      {----}

      RecActiveTxt := AttrText('mars_active');
      {If something in the field, that means something was loaded in MARS table, now find out if it is still active}
      if (AttrText('mars_active') <> '-') and (AttrText('mars_active') <> '') then begin
        if AttrInt('mars_active') = 1 then begin
          RecActive := True;
          MarsListDS.FieldByName('mars_active').AsBoolean := RecActive;
        end
        else begin
          RecActive := False;
          MarsListDS.FieldByName('mars_active').AsBoolean := RecActive;
        end;
      end
      {Nothing there....}
      else begin
        RecActive := False;
        MarsListDS.FieldByName('mars_active').AsBoolean := RecActive;

      end;


      If (AttrText('mars_id') <> '-') and (AttrText('mars_id') <> '') then
        MARSListDS.FieldByName('mars_id').AsInteger := AttrInt('mars_id')
      else
        MARSListDS.FieldByName('mars_id').AsInteger := 0;  {There is no entry in MARS table}

      MarsListDS.FieldByName('emp_id').asInteger := AttrInt('emp_id');
      MarsListDS.FieldByName('short_name').AsString := AttrText('short_name');
      MarsListDS.FieldByName('emp_number').AsString := AttrText('emp_number');
      MarsListDS.FieldByName('new_status').AsString := AttrText('new_status');

      if (AttrText('insert_date') <> '-') and (AttrText('insert_date') <> '') then begin
        MarsListDS.FieldByName('insert_date').AsDateTime := AttrDateTime('insert_date');
         If RecActive then begin
          MarsListDS.FieldByName('inProgress').AsInteger := USPROCESSING;
          MarsListDS.FieldByName('RowMessage').AsString := MSGPROCESSING;
         end
         else begin
           MarsListDS.FieldByName('inProgress').AsInteger := USSTOPPED;
           MarsListDS.FieldByName('RowMessage').AsString := MSGSTOPPED2;
         end;
      end
      else begin
        MarsListDS.FieldByName('inProgress').AsInteger := 0;
        MarsListDS.FieldByName('RowMessage').AsString := '-';
      end;

      if (AttrText('inserted_by') <> '-') and (AttrText('inserted_by') <> '') then
        MarsListDS.FieldByName('inserted_by').asInteger := AttrInt('inserted_by');
      MarsListDS.FieldByName('inserted_by_name').AsString := AttrText('inserted_by_name');
      MarsListDS.FieldByName('limitation').AsString := AttrText('limitation');

      if (MarsListDS.FieldByName('inserted_by').AsInteger > 0) then
        MarsListDS.FieldByName('mCheckbox').AsBoolean := True
      else
        MarsListDS.FieldByName('mCheckBox').AsBoolean := False;


      {----}
      MarsListDS.Post;
    end;
    Result := Count;
  finally
    MarsListDS.Open;
    MarsSource.DataSet := MarsListDS;
    MarsListDS.First;
    MarsListDS.EnableControls;
    lblCount.Caption := 'Count: ' + intToStr(Count);
  end;
end;

procedure TfrmMARSBulkStatus.PostActionRefresh;
begin
 ///////
end;

function TfrmMARSBulkStatus.RefreshAvailStatus: boolean;
var
  lLimitation: string;
  lStatList: TStringList;
  i: integer;
  lInProgress: boolean;
  lCurStatus, lAddStatus: string;
begin
  lStatList := TStringList.Create;
  Result := False;
  lInProgress := False;
  lCurStatus := '';
  lAddStatus := '';

  while not AvailStatusCDS.IsEmpty do
    AvailStatusCDS.Delete;
  AvailStatusCDS.DisableControls;
  AvailStatusCDS.EmptyDataSet;

  try
     {Is there a change in progress}
     lInProgress := MarsListDS.FieldByName('inProgress').AsInteger > 0;
     if lInProgress then begin
       AvailStatusCDS.Insert;
       lCurStatus :=  MarsListDS.FieldByName('new_status').AsString;
       AvailStatusCDS.FieldByName('new_status').AsString := lCurStatus;
       AvailStatusCDS.Post;
     end;

     {Get Permission}
     lLimitation := trim(MarsListDS.FieldByName('limitation').AsString);
 //    ShowMessage(lLimitation);
     if (lLimitation <> '') and (lLimitation <> '-') then begin
       {Parse}
        LStatList.StrictDelimiter:= True;
        lStatList.Delimiter := ',';
        LStatList.DelimitedText := lLimitation;

        try
          for i := 0 to lStatList.Count - 1 do begin
            lAddStatus := trim(lStatList[i]);
            if (lAddStatus <> 'new_status') and not lInProgress then begin
              AvailStatusCDS.Append;
              AvailStatusCDS.FieldByName('new_status').AsString := lAddStatus;
              AvailStatusCDS.Post;
            end;
            lAddStatus := '';
          end;
          Result := True;
        except
          ShowMessage('Unable to load status for this employee.');
        end;
     end;

   If lInProgress then begin
     NewStatusCol.Properties.ReadOnly := True;
     NewStatusCol.Options.Editing := False;
     RowMessageCol.Options.Editing := False;
   end
   else begin
     NewStatusCol.Properties.ReadOnly := False;
     NewStatusCol.Options.Editing := True;
     RowMessageCol.Options.Editing := True;
   end;

  {set to the combobox}
  AvailStatusCDS.Open;
  DSNewStatus.DataSet := AvailStatusCDS;
  AvailStatusCDS.First;


  finally
    AvailStatusCDS.EnableControls;
    FreeAndNil(lStatList);
  end;
end;

function TfrmMARSBulkStatus.SendMarsRequest: boolean;
var
  lEmpID: integer;
  lNewStatus: string;
  lInsertedBy: integer;
  lTimeStr: string;
begin
  if not fListLoaded then exit;

  {Get values of current record}
  lEmpID := MarsListDS.FieldByName('emp_id').asInteger;
  lNewStatus :=  MarsListDS.FieldByName('new_status').AsString;
  lInsertedBy := DM.EmpID;

  {Fail: No Status}
  if (lNewStatus = '-') or (lNewStatus = '') then begin
    Result := False;
    ShowMessage(MSGFAIL1);
    MarsListDS.Edit;
    MarsListDS.FieldByName('InProgress').AsInteger := USFAIL1;
    MarsListDS.FieldByName('RowMessage').AsString := MSGFAIL1;
    MarsListDS.Post;
  end

  {Sent - but failed}
  else begin
    Result := EmployeeDM.AddMARSRequest(lEmpID, lNewStatus, lInsertedBy);
    if not Result then begin
      MarsListDS.Edit;
      MarsListDS.FieldByName('InProgress').AsInteger := USFAIL2;
      MarsListDS.FieldByName('RowMessage').AsString := MSGFAIL2;
      MarsListDS.Post;
    end
    {Sent}
    else begin
      MarsListDS.Edit;
      MarsListDS.FieldByName('InProgress').AsInteger := USPENDING;
      MarsListDS.FieldByName('RowMessage').AsString := MSGPENDING;
      MarsListDS.FieldByName('Inserted_by').AsInteger := lInsertedBy;
      MarsListDS.FieldByName('inserted_by_name').AsString := EmployeeDM.GetEmployeeShortName(lInsertedBy);
      MarsListDS.FieldByName('insert_date').AsDateTime := NOW;
      MarsListDS.Post;
    end;
  end;
  lTimeStr := TimeToStr(Now);
end;



function TfrmMARSBulkStatus.STOPMarsRequest: boolean;
var
  lMARSID: integer;
begin
  if StatusGridView.Focused then
    lMARSID := MarsListDS.FieldByName('mars_id').asInteger
  else
    lMARSID := 0;
  try

    if lMARSID > 0 then
      Result := EmployeeDM.RemoveMARSRequest(lMARSID);
    if Result then begin
      MarsListDS.Edit;
      MarsListDS.FieldByName('InProgress').AsInteger := USSTOPPENDING;
      MarsListDS.FieldByName('RowMessage').AsString := MSGSTOPPED;
      MarsListDS.Post;
    end
    else begin
      MarsListDS.Edit;
      MarsListDS.FieldByName('InProgress').AsInteger := USFAIL3;
      MarsListDS.FieldByName('RowMessage').AsString := MSGFAIL3;
      MarsListDS.Post;

    end;
  except
    MessageDlg('A Grid Error occurred during STOP command. Refresh and try again.', mtError, [mbOK],  0);
  end;
end;



procedure TfrmMARSBulkStatus.StatusGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  lNameStr: string;
  lProcessStatus: integer;

begin
  inherited;
  if not fListLoaded then exit;

  lProcessStatus := AViewInfo.GridRecord.Values[ProcessStatusCol.Index];
  if (Not VarIsNull(AViewInfo.GridRecord.Values[InsertedByNameCol.Index])) then begin
    lNameStr := AViewInfo.GridRecord.Values[InsertedByNameCol.Index];

    if (lNameStr <> '') and (lNameStr <> '-') then begin
        ACanvas.Font.Color := clMaroon;
        ACanvas.Font.Style := [fsBold];
        ACanvas.Brush.Color := clCream;
    end
  end;

  case lProcessStatus of
    USPENDING:
    begin

      if (AViewInfo.Item.Index = ButtonCol.Index) then
        ACanvas.Brush.Color := clGreen;
    end;

    USPROCESSING:
    begin
      if (AViewInfo.Item.Index = ButtonCol.Index) then
        ACanvas.Brush.Color := clMaroon;
    end;

    USSTOPPED:
    begin
      ACanvas.Font.Color := clMaroon;
      ACanvas.Brush.Color := clCream;
    end;
  end;


  if (lProcessStatus > 0) then begin
    if (AViewInfo.Selected) then begin
      ACanvas.Font.Color:= clYellow;
      ACanvas.Brush.Color := clNavy;
    end
    else begin
      ACanvas.Font.Color := clGreen;
      ACanvas.Brush.Color := clCream;
    end;
  end;
end;

procedure TfrmMARSBulkStatus.StatusGridViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  if not fListLoaded then exit;
  RefreshAvailStatus;
end;

end.
