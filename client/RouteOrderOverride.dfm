object RouteOrderOVerrideFrm: TRouteOrderOVerrideFrm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Route Order Override Reason'
  ClientHeight = 292
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblReason: TLabel
    Left = 8
    Top = 210
    Width = 99
    Height = 13
    Caption = 'Reason for Skipping:'
  end
  object lblNumberTicketsSkipped: TLabel
    Left = 8
    Top = 52
    Width = 80
    Height = 13
    Caption = 'Tickets Skipped: '
  end
  object lblInstructions: TLabel
    Left = 8
    Top = 16
    Width = 359
    Height = 26
    Caption = 
      'Ticket # %S was done out of order. Please provide reason for ski' +
      'pping the following ticket.'
    WordWrap = True
  end
  object TicketsSkipped: TLabel
    Left = 91
    Top = 52
    Width = 86
    Height = 13
    Caption = 'TicketsSkipped'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblProvideReasonCount: TLabel
    Left = 246
    Top = 52
    Width = 132
    Height = 13
    Caption = '* Provide Reason for first 5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cmboReason: TcxComboBox
    Left = 8
    Top = 225
    Properties.DropDownListStyle = lsFixedList
    Properties.OnChange = cmboReasonPropertiesChange
    TabOrder = 0
    Width = 314
  end
  object btnApply: TButton
    Left = 328
    Top = 225
    Width = 50
    Height = 21
    Caption = 'Apply'
    Default = True
    Enabled = False
    TabOrder = 1
    OnClick = btnApplyClick
  end
  object RouteCheckList: TcxCheckListBox
    Left = 8
    Top = 70
    Width = 370
    Height = 137
    Items = <>
    TabOrder = 2
    OnClickCheck = RouteCheckListClickCheck
  end
  object btnClear: TButton
    Left = 8
    Top = 259
    Width = 82
    Height = 25
    Caption = 'Clear Reasons'
    Enabled = False
    TabOrder = 3
    OnClick = btnClearClick
  end
  object btnDone: TButton
    Left = 296
    Top = 259
    Width = 82
    Height = 25
    Caption = 'Done'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 4
    OnClick = btnDoneClick
  end
end
