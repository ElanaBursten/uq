inherited InvoiceIDExceptionReportForm: TInvoiceIDExceptionReportForm
  Left = 501
  Top = 301
  Caption = 'InvoiceIDExceptionReportForm'
  ClientHeight = 196
  ClientWidth = 411
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Invoice Dates:'
  end
  inline InvoiceDate: TOdRangeSelectFrame [1]
    Left = 28
    Top = 27
    Width = 248
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
end
