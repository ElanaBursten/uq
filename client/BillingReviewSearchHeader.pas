unit BillingReviewSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, ExtCtrls, StdCtrls, OdRangeSelect, 
  Buttons, DB, QMConst, QMServerLibrary_Intf, OdHourGlass,
  OdVclUtils, OdIsoDates, DMu, ActnList, OdDBISAMUtils, cxGridTableView,
  cxGridDBTableView, DBISAMtb, cxMaskEdit;

type
  TBillingReviewSearchCriteria = class(TSearchCriteria)
    CallCenter: TComboBox;
    CallCentersLabel: TLabel;
    PeriodStartRange: TOdRangeSelectFrame;
    CommitButton: TButton;
    DeleteButton: TButton;
    UncommitButton: TButton;
    ReExportButton: TButton;
    Bevel1: TBevel;
    PeriodEndRange: TOdRangeSelectFrame;
    BillingRunRange: TOdRangeSelectFrame;
    CommitDateRange: TOdRangeSelectFrame;
    DescriptionLabel: TLabel;
    Description: TEdit;
    BillingCommitted: TComboBox;
    CommittedLabel: TLabel;
    ActionList: TActionList;
    CommitAction: TAction;
    DeleteAction: TAction;
    UncommitAction: TAction;
    ReexportAction: TAction;
    procedure CommitActionExecute(Sender: TObject);
    procedure DeleteActionExecute(Sender: TObject);
    procedure UncommitActionExecute(Sender: TObject);
    procedure ReExportActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
  private
    FBillID: Integer;
    FCallCenterGroupID: Integer;
    FIsCommitted: Boolean;
    procedure BillingAction(ActionToTake: string);
    function CreateSearchParams: NVPairList;
  public
    procedure GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure Showing; override;
    procedure CustomizeDataSet(DataSet: TDBISAMTable; Event: TCustomizeEvent); override;
    procedure InitializeSearchCriteria; override;
    procedure CustomizeGrid(GridView: TcxGridDBTableView); override;
  end;

implementation

uses
  UQUtils, LocalPermissionsDMu;

{$R *.dfm}

{ TBillingReviewSearchCriteria }

function TBillingReviewSearchCriteria.GetXmlString: string;
var
  Cursor: IInterface;
  Params: NVPairList;
begin
  Cursor := ShowHourGlass;
  Params := CreateSearchParams;
  try
    DM.CallingServiceName('BillingHeaderSearch');
    Result := DM.Engine.Service.BillingHeaderSearch(DM.Engine.SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
end;

function TBillingReviewSearchCriteria.CreateSearchParams: NVPairList;

  procedure AddPair(const aName, aValue: string);
  begin
    AddParam(Result, aName, aValue);
  end;

begin
  Result := NVPairList.Create;
  AddPair('center_group_id', IntToStr(GetComboObjectInteger(CallCenter, False)));
  AddPair('period_start_from', IsoDateTimeToStr(PeriodStartRange.FromDate));
  AddPair('period_start_to', IsoDateTimeToStr(PeriodStartRange.ToDate));
  AddPair('period_end_from', IsoDateTimeToStr(PeriodEndRange.FromDate));
  AddPair('period_end_to', IsoDateTimeToStr(PeriodEndRange.ToDate));
  AddPair('commit_date_from', IsoDateTimeToStr(CommitDateRange.FromDate));
  AddPair('commit_date_to', IsoDateTimeToStr(CommitDateRange.ToDate));
  AddPair('run_date_from', IsoDateTimeToStr(BillingRunRange.FromDate));
  AddPair('run_date_to', IsoDateTimeToStr(BillingRunRange.ToDate));
  AddPair('description', Description.Text);
  AddPair('committed', DM.GetRefCodeForDisplay('yesnoblank', BillingCommitted.Text));
end;

procedure TBillingReviewSearchCriteria.DefineColumns;
begin
  inherited;
  Assert(Assigned(Defs));
  with Defs do begin
    SetDefaultTable('billing_header');
    AddColumn('Bill ID', 60, 'bill_id', ftInteger, '', 0, True, True);
    AddColumn('Call Center ID', 40, 'center_group_id', ftInteger, '', 0, False, False);
    AddColumn('Committed', 70, 'committed', ftBoolean, '', 0, False, False);
    AddColumn('Description', 150, 'description');
    AddColumn('Period Start', 70, 'bill_start_date', ftDateTime, '', 0, False, True, ShortDateFormat);
    AddColumn('Period End', 70, 'bill_end_date', ftDateTime, '', 0, False, True, ShortDateFormat);
    AddColumn('Run Date', 90, 'bill_run_date');
    AddColumn('Committed Date', 90, 'committed_date');
    AddColumn('Committed By', 90, 'committed_by_name');
    AddColumn('# Tickets', 60, 'n_tickets', ftInteger, '', 0, False, True, ',0;-,0; ');
    AddColumn('# Locates', 60, 'n_locates', ftInteger, '', 0, False, True, ',0;-,0; ');
    AddColumn('Total Amount', 90, 'totalamount', ftFloat, '', 0, False, True, ',0.00;-,0.00; ');
  end;
end;

procedure TBillingReviewSearchCriteria.CustomizeGrid(
  GridView: TcxGridDBTableView);
const
  ColNames: array[0..3] of string = ('totalamount','n_tickets','n_locates','bill_id');
var
  i: Integer;
  Col: TcxGridDBColumn;
begin
  for i := Low(ColNames) to High(ColNames) do begin
    Col := GridView.GetColumnByFieldName(ColNames[i]);
    if Assigned(Col) then begin
      Col.HeaderAlignmentHorz := taRightJustify;
      Col.PropertiesClass := TcxMaskEditProperties;
      Col.Properties.Alignment.Horz := taRightJustify;
    end;
  end;
end;

procedure TBillingReviewSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.GetRefDisplayList('yesnoblank', BillingCommitted.Items);
  DM.BillableCenterGroupList(CallCenter.Items,
    [RightBillingDelete, RightBillingCommit, RightBillingUnCommit, RightBillingReexport], True);

  CallCenter.Enabled := True;
  CommitAction.Visible := PermissionsDM.CanI(RightBillingCommit);
  DeleteAction.Visible := PermissionsDM.CanI(RightBillingDelete);
  UnCommitAction.Visible := PermissionsDM.CanI(RightBillingUncommit);
  ReExportAction.Visible := PermissionsDM.CanI(RightBillingReexport);
  Self.Align := alTop;
  RestoreCriteria;
end;

procedure TBillingReviewSearchCriteria.GridNodeChanged(GridView: TcxGridDBTableView; Row: TcxCustomGridRow);
begin
  if Row = nil then begin
    FBillID := 0;
    FCallCenterGroupID := -1;
    FIsCommitted := False;
  end
  else begin
    FBillID := GridView.DataController.DataSet.FieldByName('bill_id').AsInteger;
    FCallCenterGroupID := GridView.DataController.Dataset.FieldByName('center_group_id').AsInteger;
    FIsCommitted := GridView.DataController.DataSet.FieldByName('committed').AsBoolean;
  end;
end;

procedure TBillingReviewSearchCriteria.BillingAction(ActionToTake: string);
var
  Params: NVPairList;
begin
  Assert(FBillID > 0);
  Params := NVPairList.Create;
  with Params.Add do begin
    Name := 'bill_id';
    Value := IntToStr(FBillID);
  end;
  with Params.Add do begin
    Name := 'bill_action';
    Value := ActionToTake;
  end;
  with Params.Add do begin
    Name := 'call_center_group_id';
    Value := IntToStr(FCallCenterGroupID);
  end;

  DM.CallingServiceName('BillingAction');
  DM.Engine.Service.BillingAction(DM.Engine.SecurityInfo, Params);

  if ActionToTake = 'REEXPORT' then
    MessageDlg('The selected billing run has been queued for re-export.', mtInformation, [mbOk], 0)
  else
    SearchButton.Click;
end;

procedure TBillingReviewSearchCriteria.CommitActionExecute(Sender: TObject);
begin
  BillingAction('COMMIT');
end;

procedure TBillingReviewSearchCriteria.DeleteActionExecute(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to delete the selected billing?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    BillingAction('DELETE');
end;

procedure TBillingReviewSearchCriteria.UncommitActionExecute(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to uncommit the selected billing?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    BillingAction('UNCOMMIT');
end;

procedure TBillingReviewSearchCriteria.ReExportActionExecute(Sender: TObject);
begin
  BillingAction('REEXPORT');
end;

procedure TBillingReviewSearchCriteria.ActionListUpdate(
  Action: TBasicAction; var Handled: Boolean);
begin
  CommitAction.Enabled := (not FIsCommitted) and (FBillID > 0);
  DeleteAction.Enabled := (not FIsCommitted) and (FBillID > 0);
  ReexportAction.Enabled := (FBillID > 0);
  UncommitAction.Enabled := FIsCommitted and (FBillID > 0);
end;

procedure TBillingReviewSearchCriteria.CustomizeDataSet(DataSet: TDBISAMTable;
  Event: TCustomizeEvent);
begin
  inherited;

  if Event = ceFieldDefs then
    // Add text version of committed bit
    Dataset.FieldDefs.Add('committed_text', ftString, 3, False)
  else if Event = ceAfterFieldDefs then begin
    // Customize format of added column
    with Dataset.FieldByName('committed_text') do begin
      Index := 0;
      DisplayLabel := 'Committed';
      DisplayWidth := 11;
    end;
  end
  else if Event = ceDataRow then begin
    // Populate committed_text
    if Dataset.FieldByName('committed').Value then
      Dataset.FieldByName('committed_text').Value := 'Yes'
    else
      Dataset.FieldByName('committed_text').Value := 'No';
  end;
end;

procedure TBillingReviewSearchCriteria.InitializeSearchCriteria;
begin
  inherited;
  BillingRunRange.SelectDateRange(rsAllDates);
  CommitDateRange.SelectDateRange(rsAllDates);
  PeriodStartRange.SelectDateRange(rsAllDates);
  PeriodEndRange.SelectDateRange(rsLastMonth);
end;

end.

