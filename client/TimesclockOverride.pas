unit TimesclockOverride;   // QMANTWO-235

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxTimeEdit,DBISAMTb, DMu, DB;

type
  TfrmTimeSheetOverride = class(TForm)
    Label1: TLabel;
    edtOther: TEdit;
    Label2: TLabel;
    btnClose: TBitBtn;
    btnCancel: TBitBtn;
    cbReason: TComboBox;
    cxTimeEdit1: TcxTimeEdit;
    orReasons: TDBISAMQuery;
    Label3: TLabel;
    lblLength: TLabel;
    procedure cbReasonCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtOtherKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    procedure TestForOther(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  end;



implementation
uses TimeClockDay;
const
  OTHER = '<<other>>';

{$R *.dfm}

procedure TfrmTimeSheetOverride.btnCancelClick(Sender: TObject);
begin
  TimeClockDay.overRideReason := '';
end;

procedure TfrmTimeSheetOverride.cbReasonCloseUp(Sender: TObject);
begin
  TestForOther(Sender);
  btnClose.Enabled :=  (cbReason.ItemIndex > -1);

end;

procedure TfrmTimeSheetOverride.TestForOther(Sender: TObject);
begin
  if (cbReason.text = OTHER) then
  begin
    lblLength.Caption := '';
    edtOther.Enabled := true;
    edtOther.SetFocus;
  end
  else
  begin
  lblLength.Caption := '';
  edtOther.text := '';
  edtOther.Enabled := false;
  end;
end;


procedure TfrmTimeSheetOverride.edtOtherKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  REMAIN = '%d characters remaining';
var
  cnt : integer;
begin
  cnt := edtOther.MaxLength - length(edtOther.text);
  lblLength.Caption := format(REMAIN,[cnt]);
end;

procedure TfrmTimeSheetOverride.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  TimeClockDay.newTime := cxTimeEdit1.Time;
  if not(edtOther.Enabled) then
  TimeClockDay.overRideReason := cbReason.Text
  else
  TimeClockDay.overRideReason := edtOther.text;
end;

procedure TfrmTimeSheetOverride.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if  (self.ModalResult = mrOk) then
  begin
    if ((cbReason.text = OTHER) and (length(edtOther.text)<4)) then
    begin
      showmessage('A reason must be entered when <<other>> is selected');
      CanClose := false;
    end
    else
    CanClose := true;
  end
  else
  CanClose := true;
end;

procedure TfrmTimeSheetOverride.FormCreate(Sender: TObject);
begin
  cbReason.Items.Clear;
  edtOther.Enabled := false;
  orReasons.Open;
  orReasons.First;
  while not orReasons.Eof do begin
    cbReason.Items.Add(orReasons.FieldByName('description').AsString);
    orReasons.Next;
  end;

  orReasons.Close;
  cbReason.Items.Add(OTHER);
end;

procedure TfrmTimeSheetOverride.FormShow(Sender: TObject);
begin
  cxTimeEdit1.Time := Now;
  btnClose.Enabled := false;
end;

end.
