unit TextMessageBox;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TfrmTextMessageBox = class(TForm)
    textMemo: TMemo;
    btnSendMsg: TBitBtn;
    btnCancelMsg: TBitBtn;
    charCount: TLabel;
    Label1: TLabel;
    procedure textMemoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTextMessageBox: TfrmTextMessageBox;

implementation

{$R *.dfm}

procedure TfrmTextMessageBox.textMemoChange(Sender: TObject);
var
  cnt:integer;
const
  total=130;
  Statement = '%d of %d characters remaining';
begin
  cnt := Length(textMemo.text);
  
  charCount.Caption := format(Statement,[(total-cnt),total]);
end;

end.
