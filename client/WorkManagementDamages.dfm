inherited WorkMgtDamagesFrame: TWorkMgtDamagesFrame
  inherited SummaryPanel: TPanel
    Caption = 'Damages'
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Damages'
    end
    inherited BaseGrid: TcxGrid
      Enabled = False
      PopupMenu = PriorityPopup
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = DamageView
      end
    end
    inherited PanelDisplaySelectedItem: TPanel
      object Label25: TLabel
        Left = 8
        Top = 119
        Width = 23
        Height = 13
        Caption = 'City:'
      end
      object Label28: TLabel
        Left = 8
        Top = 155
        Width = 30
        Height = 13
        Caption = 'State:'
      end
      object Label14: TLabel
        Left = 8
        Top = 19
        Width = 54
        Height = 13
        Caption = 'Damage #:'
      end
      object Label15: TLabel
        Left = 8
        Top = 38
        Width = 101
        Height = 13
        Caption = 'Investigation Status:'
      end
      object Label16: TLabel
        Left = 304
        Top = 100
        Width = 85
        Height = 13
        Caption = 'Facility Damaged:'
      end
      object Label17: TLabel
        Left = 8
        Top = 57
        Width = 79
        Height = 13
        Caption = 'Utility Company:'
      end
      object Label18: TLabel
        Left = 8
        Top = 100
        Width = 43
        Height = 13
        Caption = 'Address:'
      end
      object Label19: TLabel
        Left = 8
        Top = 174
        Width = 38
        Height = 13
        Caption = 'Priority:'
      end
      object DSumUQDamageID: TDBText
        Left = 116
        Top = 19
        Width = 109
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'uq_damage_id'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumDamageType: TDBText
        Left = 116
        Top = 38
        Width = 108
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'damage_type'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumAddress: TDBText
        Left = 116
        Top = 100
        Width = 79
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'location'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumWorkPriority: TDBText
        Left = 116
        Top = 174
        Width = 105
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'work_priority'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label20: TLabel
        Left = 304
        Top = 19
        Width = 69
        Height = 13
        Caption = 'Damage Date:'
      end
      object DSumDamageDate: TDBText
        Left = 402
        Top = 19
        Width = 107
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'damage_date'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label23: TLabel
        Left = 304
        Top = 38
        Width = 49
        Height = 13
        Caption = 'Due Date:'
      end
      object DSumDueDate: TDBText
        Left = 402
        Top = 38
        Width = 82
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'due_date'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumUtilityCo: TDBText
        Left = 116
        Top = 57
        Width = 81
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'utility_co_damaged'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumFacilityType: TDBText
        Left = 402
        Top = 100
        Width = 101
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'facility_type'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumFacilitySize: TDBText
        Left = 402
        Top = 119
        Width = 96
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'facility_size'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label24: TLabel
        Left = 304
        Top = 119
        Width = 59
        Height = 13
        Caption = 'Facility Size:'
      end
      object DSumCity: TDBText
        Left = 116
        Top = 119
        Width = 55
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'city'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label26: TLabel
        Left = 8
        Top = 137
        Width = 39
        Height = 13
        Caption = 'County:'
      end
      object DSumCounty: TDBText
        Left = 116
        Top = 137
        Width = 73
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'county'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label27: TLabel
        Left = 8
        Top = 76
        Width = 53
        Height = 13
        Caption = 'Excavator:'
      end
      object DSumExcavator: TDBText
        Left = 116
        Top = 76
        Width = 90
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'excavator_company'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object DSumState: TDBText
        Left = 116
        Top = 155
        Width = 64
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'state'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label21: TLabel
        Left = 304
        Top = 57
        Width = 62
        Height = 13
        Caption = 'Closed Date:'
      end
      object DSumClosedDate: TDBText
        Left = 402
        Top = 57
        Width = 97
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'closed_date'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object Label22: TLabel
        Left = 304
        Top = 137
        Width = 78
        Height = 13
        Caption = 'Facility Material:'
      end
      object DSumFacilityMaterial: TDBText
        Left = 402
        Top = 137
        Width = 120
        Height = 13
        AutoSize = True
        Color = clBtnFace
        DataField = 'facility_material'
        DataSource = DamageSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object RefreshDamageButton: TButton
        Left = 3
        Top = 201
        Width = 85
        Height = 27
        Caption = 'Refresh'
        TabOrder = 0
        OnClick = RefreshDamageButtonClick
      end
      object DamagePriorityCombo: TComboBox
        Left = 240
        Top = 171
        Width = 106
        Height = 21
        ItemHeight = 13
        TabOrder = 1
        OnChange = DamagePriorityComboChange
      end
    end
  end
  inherited ActionListFrame: TActionList
    OnUpdate = ActionListUpdate
    inherited NormalPriorityAction: TAction
      OnExecute = NormalPriorityActionExecute
    end
    inherited HighPriorityAction: TAction
      OnExecute = HighPriorityActionExecute
    end
  end
  inherited PriorityPopup: TPopupMenu
    OnPopup = PriorityPopupPopup
  end
  inherited ListTable: TDBISAMTable
    TableName = 'TMDamageList'
  end
  inherited DetailTable: TDBISAMTable
    AfterOpen = ItemDetailTableAfterOpen
    OnCalcFields = ItemDetailTableCalcFields
    TableName = 'damage'
  end
  object DamageGridViewRepository: TcxGridViewRepository
    Left = 536
    Top = 112
    object DamageView: TcxGridDBTableView
      DragMode = dmAutomatic
      OnDblClick = ViewDblClick
      OnMouseUp = DamageViewMouseUp
      Navigator.Buttons.CustomButtons = <>
      OnCellClick = DamageViewCellClick
      OnCustomDrawCell = DamageViewCustomDrawCell
      OnFocusedRecordChanged = ViewFocusedRecordChanged
      OnSelectionChanged = DamageViewSelectionChanged
      DataController.DataSource = DamageListSource
      DataController.KeyFieldNames = 'damage_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.NoDataToDisplayInfoText = '<No damage investigations for selected locator>'
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      Styles.Inactive = UnfocusedGridSelStyle
      Styles.Selection = GridSelectionStyle
      OnCustomDrawIndicatorCell = DamageViewCustomDrawIndicatorCell
      object ColDamageDamageID: TcxGridDBColumn
        Caption = 'Damage ID'
        DataBinding.FieldName = 'damage_id'
        Visible = False
        VisibleForCustomization = False
      end
      object ColDamageUQDamageID: TcxGridDBColumn
        Caption = 'Damage ID'
        DataBinding.FieldName = 'uq_damage_id'
      end
      object ColDamageDamageDate: TcxGridDBColumn
        Caption = 'Damage Date'
        DataBinding.FieldName = 'damage_date'
        Width = 87
      end
      object ColDamageDueDate: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        MinWidth = 32
        Width = 124
      end
      object ColDamageDamageType: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'damage_type'
        Width = 77
      end
      object ColDamageUtilityCoDamaged: TcxGridDBColumn
        Caption = 'Utility Company'
        DataBinding.FieldName = 'utility_co_damaged'
        Width = 111
      end
      object ColDamageLocation: TcxGridDBColumn
        Caption = 'Location'
        DataBinding.FieldName = 'location'
        Width = 141
      end
      object ColDamageCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Width = 81
      end
      object ColDamageState: TcxGridDBColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Width = 37
      end
      object ColDamageCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Width = 82
      end
      object ColDamageFacilityType: TcxGridDBColumn
        Caption = 'Facility Type'
        DataBinding.FieldName = 'facility_type'
        Width = 92
      end
      object ColDamageFacilitySize: TcxGridDBColumn
        Caption = 'Facility Size'
        DataBinding.FieldName = 'facility_size'
        Width = 74
      end
      object ColDamageFacilityMaterial: TcxGridDBColumn
        Caption = 'Facility Material'
        DataBinding.FieldName = 'facility_material'
        Width = 98
      end
      object ColDamageExcavator: TcxGridDBColumn
        Caption = 'Excavator Company'
        DataBinding.FieldName = 'excavator_company'
        Width = 144
      end
      object ColDamageClosedDate: TcxGridDBColumn
        Caption = 'Closed Date'
        DataBinding.FieldName = 'closed_date'
        Width = 90
      end
      object ColDamageProfitCenter: TcxGridDBColumn
        Caption = 'Center'
        DataBinding.FieldName = 'profit_center'
        Visible = False
        Width = 71
      end
      object ColDamageModifiedDate: TcxGridDBColumn
        Caption = 'Modified Date'
        DataBinding.FieldName = 'modified_date'
        Visible = False
        VisibleForCustomization = False
      end
      object ColDamagePriority: TcxGridDBColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        Width = 59
      end
    end
  end
  object DamageListSource: TDataSource
    DataSet = ListTable
    Left = 46
    Top = 92
  end
  object DamageSource: TDataSource
    DataSet = DetailTable
    Left = 720
    Top = 344
  end
  object cxStyleRepoTickets: TcxStyleRepository
    PixelsPerInch = 96
    object BottomGridStyle: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object GridSelectionStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clHotLight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object UnfocusedGridSelStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 11829830
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = 14745599
    end
  end
end
