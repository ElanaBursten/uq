object LocStatusQuestions: TLocStatusQuestions
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Status Change Questions'
  ClientHeight = 242
  ClientWidth = 696
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object QuestionScrollBox: TcxScrollBox
    Left = 0
    Top = 25
    Width = 696
    Height = 168
    Align = alTop
    TabOrder = 0
  end
  object btnPanel: TPanel
    Left = 0
    Top = 194
    Width = 696
    Height = 48
    Align = alBottom
    TabOrder = 1
    object OKButton: TButton
      Left = 286
      Top = 13
      Width = 75
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = OKButtonClick
    end
    object CancelButton: TButton
      Left = 374
      Top = 13
      Width = 74
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 696
    Height = 25
    Align = alTop
    BevelEdges = []
    TabOrder = 2
    object InstructionsLabel: TLabel
      Left = 0
      Top = 8
      Width = 322
      Height = 13
      Caption = 
        'The following questions are required based on the Status changed' +
        '.'
    end
  end
  object DataSource1: TDataSource
    Left = 544
    Top = 16
  end
end
