object ContactNameDialog: TContactNameDialog
  Left = 347
  Top = 358
  BorderStyle = bsDialog
  Caption = 'Ongoing Ticket Contact Name'
  ClientHeight = 137
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object FirstNameLabel: TLabel
    Left = 18
    Top = 67
    Width = 55
    Height = 13
    Caption = 'First Name:'
  end
  object LastNameLabel: TLabel
    Left = 219
    Top = 67
    Width = 54
    Height = 13
    Caption = 'Last Name:'
  end
  object Label3: TLabel
    Left = 18
    Top = 10
    Width = 383
    Height = 41
    AutoSize = False
    Caption = 
      'This call center requires that you collect a contact name for al' +
      'l tickets that go into the ongoing state.  Please enter the firs' +
      't and last name of the contact person for this ongoing ticket.  ' +
      'The contact data is required to save the ticket.'
    WordWrap = True
  end
  object FirstNameEdit: TEdit
    Left = 74
    Top = 64
    Width = 125
    Height = 21
    TabOrder = 0
    OnChange = NameEditChange
  end
  object LastNameEdit: TEdit
    Left = 274
    Top = 64
    Width = 125
    Height = 21
    TabOrder = 1
    OnChange = NameEditChange
  end
  object OKButton: TButton
    Left = 123
    Top = 99
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 219
    Top = 99
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
