object frmDigAlertNoteDialog: TfrmDigAlertNoteDialog
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = '    BG&E Alert    '
  ClientHeight = 218
  ClientWidth = 393
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 104
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Save'
    ModalResult = 1
    TabOrder = 1
    OnClick = Button1Click
  end
  object BGE_NOTE: TRichEdit
    Left = 0
    Top = 0
    Width = 393
    Height = 161
    Align = alTop
    Lines.Strings = (
      '**DigAlert**')
    PlainText = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 200
    Top = 185
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    OnClick = Button1Click
  end
end
