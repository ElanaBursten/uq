unit DamageProductivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdTimeRangeSelect, CheckLst;

type
  TDamageProductivityReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    InvestigatorLabel: TLabel;
    CheckBoxAll: TCheckBox;
    InvestigatorComboBox: TComboBox;
    ManagersComboBox: TComboBox;
    EmployeeStatusLabel: TLabel;
    EmployeeStatusCombo: TComboBox;
    RangeSelect: TOdTimeRangeSelectFrame;
    DamageTypesLabel: TLabel;
    DamageTypesListBox: TCheckListBox;
    DateRangeLabel: TLabel;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure CheckBoxAllClick(Sender: TObject);
  private
    procedure SetVisibility;
    function ManagerSelected: Boolean;
    function SelectInvMode: Boolean;
    procedure PopulateInvestigatorList;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, OdExceptions, LocalEmployeeDmu, LocalDamageDMu;

{$R *.dfm}

{ TDamageProductivityReportForm }

procedure TDamageProductivityReportForm.ValidateParams;
var
  DamageTypes: string;
begin
  inherited;
  SetReportID('DamageProductivity');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParamIntCombo('Manager', ManagersComboBox, True);
  if not CheckBoxAll.Checked then
    SetParamIntCombo('Investigator',InvestigatorComboBox, SelectInvMode);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));

  DamageTypes := GetCheckedItemString(DamageTypesListBox);
  if DamageTypes = '' then
    raise EOdEntryRequired.Create('Please select at least one Damage Type.');
  SetParam('DamageTypes', DamageTypes);
end;

procedure TDamageProductivityReportForm.InitReportControls;
begin
  inherited;
  RangeSelect.SelectDateRange(rsLastWeek);
  SetUpManagerList(ManagersComboBox);
  if ManagersComboBox.Items.Count > 1 then
    ManagersComboBox.ItemIndex := 0;
  LDamageDM.InitDamageTypesListBox(DamageTypesListBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  SetCheckListBox(DamageTypesListBox, True);
  CheckBoxAll.Checked := True;
  SetVisibility;
end;

procedure TDamageProductivityReportForm.SetVisibility;
begin
  InvestigatorComboBox.Enabled := SelectInvMode;
end;

function TDamageProductivityReportForm.ManagerSelected: Boolean;
begin
  Result := ManagersComboBox.ItemIndex <> -1;
end;

procedure TDamageProductivityReportForm.PopulateInvestigatorList;
begin
  if SelectInvMode then begin
    if ManagerSelected then
      EmployeeDM.EmployeeList(InvestigatorComboBox.Items, GetComboObjectInteger(ManagersComboBox));
  end else
    InvestigatorComboBox.Items.Clear;
end;

function TDamageProductivityReportForm.SelectInvMode: Boolean;
begin
  Result := (not CheckBoxAll.Checked);
end;

procedure TDamageProductivityReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  inherited;
  SetVisibility;
  PopulateInvestigatorList;
end;

procedure TDamageProductivityReportForm.CheckBoxAllClick(Sender: TObject);
begin
  inherited;
  SetVisibility;
  PopulateInvestigatorList;
end;

end.
