inherited TicketsReceivedReportForm: TTicketsReceivedReportForm
  Left = 397
  Top = 59
  Caption = 'TicketsReceivedReportForm'
  ClientHeight = 320
  ClientWidth = 553
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 129
    Height = 15
    AutoSize = False
    Caption = 'Transmit Date Range:'
    WordWrap = True
  end
  object Label3: TLabel [1]
    Left = 279
    Top = 37
    Width = 90
    Height = 13
    Caption = 'Clients (Term IDs):'
  end
  object Label4: TLabel [2]
    Left = 0
    Top = 113
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label5: TLabel [3]
    Left = 0
    Top = 145
    Width = 65
    Height = 16
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  inline RangeSelect: TOdRangeSelectFrame [4]
    Left = 26
    Top = 22
    Width = 248
    Height = 63
    TabOrder = 0
    inherited DatesComboBox: TComboBox
      Width = 202
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 161
      Width = 85
    end
  end
  object ClientList: TCheckListBox
    Left = 279
    Top = 75
    Width = 183
    Height = 240
    Anchors = [akLeft, akTop, akBottom]
    Columns = 2
    ItemHeight = 13
    Sorted = True
    TabOrder = 4
  end
  object SelectAllButton: TButton
    Left = 468
    Top = 75
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 5
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton
    Left = 468
    Top = 105
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 6
    OnClick = ClearAllButtonClick
  end
  object ManagersComboBox: TComboBox
    Left = 70
    Top = 110
    Width = 183
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 2
  end
  object IncludeAll: TCheckBox
    Left = 279
    Top = 56
    Width = 153
    Height = 17
    Caption = 'Include All Clients / Terms'
    TabOrder = 3
    OnClick = IncludeAllClick
  end
  object TicketTypeBox: TCheckBox
    Left = 71
    Top = 87
    Width = 193
    Height = 17
    Caption = 'Show Detail By Ticket Type'
    TabOrder = 1
  end
  object CallCenterList: TCheckListBox
    Left = 0
    Top = 160
    Width = 275
    Height = 155
    OnClickCheck = CallCenterListClickCheck
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 7
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 16
    Top = 225
  end
end
