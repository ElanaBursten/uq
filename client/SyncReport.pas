unit SyncReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, OdReportBase, OdRangeSelect;

type
  TSyncReportForm = class(TReportBaseForm)
    GroupBox1: TGroupBox;
    RangeSelect: TOdRangeSelectFrame;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

procedure TSyncReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TSyncReportForm.ValidateParams;
begin
  inherited;
  SetReportID('SyncSummary');
  SetParamDate('DateStart', RangeSelect.FromDate);
  SetParamDate('DateEnd', RangeSelect.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

end.

