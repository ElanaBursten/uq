inherited ProductivityReportForm: TProductivityReportForm
  Left = 297
  Top = 185
  Caption = 'Productivity Report'
  ClientHeight = 362
  ClientWidth = 556
  PixelsPerInch = 96
  TextHeight = 13
  object DateRangeGroup: TGroupBox [0]
    Left = 1
    Top = 5
    Width = 270
    Height = 105
    Caption = ' Date Range '
    TabOrder = 0
    inline RangeSelect: TOdTimeRangeSelectFrame
      Left = 2
      Top = 13
      Width = 266
      Height = 87
      TabOrder = 0
      inherited FromLabel: TLabel
        Top = 11
        Width = 28
        Caption = 'From:'
      end
      inherited ToLabel: TLabel
        Top = 37
        Width = 16
        Caption = 'To:'
      end
      inherited DatesLabel: TLabel
        Width = 32
        Caption = 'Dates:'
      end
      inherited DatesComboBox: TComboBox
        Left = 61
        Width = 172
      end
      inherited FromTimeEdit: TDateTimePicker
        Left = 155
        Width = 78
      end
      inherited ToTimeEdit: TDateTimePicker
        Left = 155
        Width = 78
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 61
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 61
      end
    end
  end
  object OfficeGroup: TGroupBox [1]
    Left = 1
    Top = 118
    Width = 270
    Height = 77
    Caption = ' Employees '
    TabOrder = 1
    object ManagerLabel: TLabel
      Left = 11
      Top = 20
      Width = 46
      Height = 13
      Caption = 'Manager:'
    end
    object Label6: TLabel
      Left = 11
      Top = 41
      Width = 46
      Height = 26
      Caption = 'Employee Status:'
      WordWrap = True
    end
    object ManagersComboBox: TComboBox
      Left = 63
      Top = 17
      Width = 172
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 0
    end
    object EmployeeStatusCombo: TComboBox
      Left = 63
      Top = 45
      Width = 172
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object OptionsGroup: TGroupBox [2]
    Left = 280
    Top = 5
    Width = 265
    Height = 196
    Caption = ' Exclude Statuses '
    TabOrder = 2
    object StatusCheckListBox: TCheckListBox
      Left = 8
      Top = 22
      Width = 250
      Height = 167
      Columns = 5
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object CallCenterClientsBox: TGroupBox [3]
    Left = 1
    Top = 201
    Width = 544
    Height = 159
    Anchors = [akLeft, akTop, akBottom]
    Caption = ' Limit To Specific Call Centers / Clients '
    ParentShowHint = False
    ShowHint = False
    TabOrder = 3
    DesignSize = (
      544
      159)
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 105
      Height = 13
      AutoSize = False
      Caption = 'Call Centers:'
      WordWrap = True
    end
    object Label3: TLabel
      Left = 286
      Top = 20
      Width = 36
      Height = 13
      Caption = 'Clients:'
    end
    object CallCenterList: TCheckListBox
      Left = 8
      Top = 35
      Width = 275
      Height = 118
      Anchors = [akLeft, akTop, akBottom]
      ItemHeight = 13
      TabOrder = 0
      OnClick = CallCenterListClick
    end
    object ClientList: TCheckListBox
      Left = 286
      Top = 35
      Width = 183
      Height = 118
      Anchors = [akLeft, akTop, akBottom]
      Columns = 2
      ItemHeight = 13
      TabOrder = 1
    end
    object SelectAllButton: TButton
      Left = 472
      Top = 35
      Width = 65
      Height = 25
      Caption = 'Select All'
      TabOrder = 2
      OnClick = SelectAllButtonClick
    end
    object ClearAllButton: TButton
      Left = 472
      Top = 67
      Width = 65
      Height = 25
      Caption = 'Clear All'
      TabOrder = 3
      OnClick = ClearAllButtonClick
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 480
    Top = 200
  end
end
