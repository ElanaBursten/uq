unit BackgroundWorker;

interface

uses
  Classes, Windows, Messages, SysUtils;

const
  // Message Ids used by background thread communications
  UM_WORKER_STATUS = WM_USER + 1009;

  // possible worker thread states
  WORKER_IDLE = 'IDLE';
  WORKER_BUSY = 'BUSY';
  WORKER_TERM = 'TERM';

type
  TWorkMessageType = (wmtError, wmtInfo, wmtMajor, wmtMinor);

  TWorkerStatus = class
  public
    WorkerID: Integer;
    State: string;
    MessageType: TWorkMessageType;
    MessageText: string;
    PercentComplete: Integer;

    constructor Create(ThreadID: Integer);
  end;

  TBackgroundWorker = class(TThread)
  private
    FSupervisorHandle: HWND; // A Windows handle of an object to receive worker status messages
  protected
    procedure PostStatusMessage(const MsgType: TWorkMessageType; const Msg, State: string; PercentComplete: Integer=-1);
    procedure PostErrorMessage(const E: Exception);
    procedure NotifySupervisor(const MsgNumber: Integer; Status: TWorkerStatus);
    procedure Execute; override;            // Do not override in descendants.

    // Use these methods in descendents to direct a worker thread do some task
    procedure PerformWork; virtual;         // override this to do work on a task
    function AnyWorkToDo: Boolean; virtual; // override & return true to do some work
    procedure BeforeWork; virtual;          // override to do something just before starting a task
    procedure AfterWork; virtual;           // override to do something after finishing a task
    procedure Setup; virtual;               // override to do any setup chores when the worker first starts
    procedure Cleanup; virtual;             // override to do any cleanup when terminating the worker
  public
    constructor Create;
    procedure RegisterSupervisor(Handle: HWND); // Hook up an object to get worker status messages
  end;

implementation

{ TWorkerStatus }

constructor TWorkerStatus.Create(ThreadID: Integer);
begin
  WorkerID := ThreadID;
end;


{ TBackgroundWorker }

procedure TBackgroundWorker.Setup;
begin
  // Do any setup before entering the worker thread's Execute loop.
  Assert(FSupervisorHandle > 0, 'RegisterSupervisor() should be used to resume a worker thread.');
end;

procedure TBackgroundWorker.Cleanup;
begin
  // Do any cleanup when leaving the work thread's Execute loop.
end;

procedure TBackgroundWorker.Execute;
begin
  // Main worker processing ... Don't override in descendents.
  Setup;
  try
    while not Terminated do begin
      try
        if AnyWorkToDo then begin
          BeforeWork;
          PerformWork;
          AfterWork;
        end;
      except
        on E: Exception do PostErrorMessage(E);
      end;
      Sleep(250);
    end;
  finally
    Cleanup;
    Assert(not FreeOnTerminate, 'FreeOnTerminate erroneously enabled');
  end;
end;

function TBackgroundWorker.AnyWorkToDo: Boolean;
begin
  Result := not Terminated;
  // Detect when there is work to do
end;

procedure TBackgroundWorker.PerformWork;
begin
  // Work on a task
end;

procedure TBackgroundWorker.BeforeWork;
begin
  // Do something before starting a task
end;

procedure TBackgroundWorker.AfterWork;
begin
  // Do something after finishing a task
end;

constructor TBackgroundWorker.Create;
begin
  inherited Create(True); // create suspended
  FSupervisorHandle := 0;
  Assert(not FreeOnTerminate, 'FreeOnTerminate defaults to False');
end;

procedure TBackgroundWorker.PostErrorMessage(const E: Exception);
begin
  Assert(Assigned(E), 'No Exception defined');
  PostStatusMessage(wmtError, E.Message, WORKER_BUSY);
end;

procedure TBackgroundWorker.PostStatusMessage(const MsgType: TWorkMessageType;
  const Msg, State: string; PercentComplete: Integer);
var
  WorkStatus: TWorkerStatus;
begin
  // WorkStatus must be freed by the message consumer
  WorkStatus := TWorkerStatus.Create(ThreadID);
  WorkStatus.MessageType := MsgType;
  WorkStatus.MessageText := Msg;
  WorkStatus.State := State;
  WorkStatus.PercentComplete := PercentComplete;

  NotifySupervisor(UM_WORKER_STATUS, WorkStatus);
end;

procedure TBackgroundWorker.NotifySupervisor(const MsgNumber: Integer; Status: TWorkerStatus);
begin
  Assert(Assigned(Status), 'WorkerStatus is undefined');
  PostMessage(FSupervisorHandle, MsgNumber, 0, UINT(Status));
end;

procedure TBackgroundWorker.RegisterSupervisor(Handle: HWND);
begin
  Assert(Handle > 0, 'RegisterSupervisor() needs a valid Handle parameter.');
  FSupervisorHandle := Handle;
  Resume;
end;

end.
