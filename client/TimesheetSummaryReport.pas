unit TimesheetSummaryReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TTimesheetSummaryReportForm = class(TReportBaseForm)
    ManagersComboBox: TComboBox;
    PeriodLabel: TLabel;
    ChooseDate: TcxDateEdit;
    ManagerLabel: TLabel;
    IncludeDailyDetailBox: TCheckBox;
    Label2: TLabel;
    ApprovalCombo: TComboBox;
    ReportLayoutCombo: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    SortCombo: TComboBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    procedure ChooseDateChange(Sender: TObject);
    procedure OldNewComboChange(Sender: TObject);
    procedure IncludeDailyDetailBoxClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    function GetBaseFileName: string; override;
  private
    procedure SetOptionsEnabled;
  end;

implementation

{$R *.dfm}

uses
  DMu, OdMiscUtils, OdVclUtils, LocalEmployeeDMu;

{ TTimesheetSummaryReportForm }

function TTimesheetSummaryReportForm.GetBaseFileName: string;
begin
  Result := 'TimesheetSummary';
end;

procedure TTimesheetSummaryReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  ChooseDate.Date := ThisSaturday;
  IncludeDailyDetailBox.Checked := True;
  ApprovalCombo.ItemIndex := 0;
  DM.ReportHierarchyOptionList(ReportLayoutCombo.Items);
  ReportLayoutCombo.ItemIndex := 0;
  SortCombo.ItemIndex := 0;
  SetOptionsEnabled;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TTimesheetSummaryReportForm.ValidateParams;
begin
  inherited;
  RequireComboBox(ManagersComboBox, 'Please select a manager.');

  if IncludeDailyDetailBox.Checked then begin
    SetReportID('TimesheetSummaryNew');
  end else begin
    SetReportID('TimesheetTotals');
    SetParamInt('FinalOnly', ApprovalCombo.ItemIndex);
  end;

  SetParamIntCombo('manager_id', ManagersComboBox, True);
  SetParamDate('end_date', ChooseDate.Date);
  SetParamIntCombo('HierarchyDepth', ReportLayoutCombo, True);
  SetParam('Sort', SortCombo.Text);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TTimesheetSummaryReportForm.ChooseDateChange(Sender: TObject);
begin
  inherited;
  if ChooseDate.Date <> SaturdayIze(ChooseDate.Date) then
    ChooseDate.Date := SaturdayIze(ChooseDate.Date);
end;

procedure TTimesheetSummaryReportForm.OldNewComboChange(Sender: TObject);
begin
  inherited;
  SetOptionsEnabled;
end;

procedure TTimesheetSummaryReportForm.SetOptionsEnabled;
begin
  IncludeDailyDetailBox.Enabled := True;
  ApprovalCombo.Enabled := (not IncludeDailyDetailBox.Checked);
end;

procedure TTimesheetSummaryReportForm.IncludeDailyDetailBoxClick(
  Sender: TObject);
begin
  inherited;
  SetOptionsEnabled;
end;

end.

