inherited EmployeeRightsReportForm: TEmployeeRightsReportForm
  Left = 200
  Top = 318
  Caption = 'EmployeeRightsReportForm'
  ClientHeight = 368
  ClientWidth = 680
  PixelsPerInch = 96
  TextHeight = 13
  object DatesGroupBox: TGroupBox [0]
    Left = 352
    Top = 8
    Width = 321
    Height = 81
    Caption = ' Modified Dates '
    TabOrder = 2
    Visible = False
    inline ModifiedDateRange: TOdRangeSelectFrame
      Left = 8
      Top = 16
      Width = 297
      Height = 52
      TabOrder = 0
      inherited FromLabel: TLabel
        Top = 5
      end
      inherited ToLabel: TLabel
        Left = 182
        Top = 5
      end
      inherited DatesLabel: TLabel
        Top = 31
      end
      inherited DatesComboBox: TComboBox
        Left = 91
        Top = 27
        Width = 200
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 91
        Top = 1
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 203
        Top = 1
      end
    end
  end
  object EmployeesGroupBox: TGroupBox [1]
    Left = 0
    Top = 8
    Width = 348
    Height = 136
    Caption = ' Employees '
    TabOrder = 0
    object lblManager: TLabel
      Left = 12
      Top = 29
      Width = 46
      Height = 13
      Caption = 'Manager:'
    end
    object EmployeeLabel: TLabel
      Left = 12
      Top = 73
      Width = 50
      Height = 13
      Caption = 'Employee:'
    end
    object EmployeeStatusLabel: TLabel
      Left = 12
      Top = 98
      Width = 84
      Height = 13
      Caption = 'Employee Status:'
    end
    object ManagersComboBox: TComboBox
      Left = 101
      Top = 25
      Width = 212
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 0
      OnChange = ManagersComboBoxChange
    end
    object AllCheckBox: TCheckBox
      Left = 101
      Top = 49
      Width = 244
      Height = 17
      Caption = 'Include all this manager'#39's employees'
      TabOrder = 1
      OnClick = AllCheckBoxClick
    end
    object EmployeeComboBox: TComboBox
      Left = 101
      Top = 69
      Width = 234
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 2
    end
    object EmployeeStatusCombo: TComboBox
      Left = 101
      Top = 94
      Width = 143
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
    end
  end
  object RightsGroupBox: TGroupBox [2]
    Left = -2
    Top = 143
    Width = 348
    Height = 224
    Anchors = [akLeft, akTop, akBottom]
    Caption = ' Rights '
    TabOrder = 1
    DesignSize = (
      348
      224)
    object RightStatusLabel: TLabel
      Left = 12
      Top = 25
      Width = 63
      Height = 13
      Caption = 'Right Status:'
    end
    object Label3: TLabel
      Left = 12
      Top = 52
      Width = 85
      Height = 13
      Caption = 'Rights to Include:'
    end
    object RightStatusCombo: TComboBox
      Left = 86
      Top = 21
      Width = 143
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object RightsList: TCheckListBox
      Left = 12
      Top = 67
      Width = 258
      Height = 152
      Anchors = [akLeft, akTop, akBottom]
      Columns = 2
      ItemHeight = 13
      TabOrder = 1
    end
    object SelectAllButton: TButton
      Left = 276
      Top = 66
      Width = 65
      Height = 25
      Caption = 'Select All'
      TabOrder = 2
      OnClick = SelectAllButtonClick
    end
    object ClearAllButton: TButton
      Left = 276
      Top = 98
      Width = 65
      Height = 25
      Caption = 'Clear All'
      TabOrder = 3
      OnClick = ClearAllButtonClick
    end
  end
  object OrigGrantDatesGroupBox: TGroupBox [3]
    Left = 352
    Top = 92
    Width = 321
    Height = 81
    Caption = ' Original Grant Dates '
    TabOrder = 3
    Visible = False
    inline OrigGrantDateRange: TOdRangeSelectFrame
      Left = 8
      Top = 16
      Width = 297
      Height = 52
      TabOrder = 0
      inherited FromLabel: TLabel
        Top = 5
      end
      inherited ToLabel: TLabel
        Left = 182
        Top = 5
      end
      inherited DatesLabel: TLabel
        Top = 31
      end
      inherited DatesComboBox: TComboBox
        Left = 91
        Top = 27
        Width = 200
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 91
        Top = 1
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 203
        Top = 1
      end
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 214
  end
end
