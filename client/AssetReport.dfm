inherited AssetReportForm: TAssetReportForm
  Caption = 'Assets'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 12
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object ManagerCombo: TComboBox [1]
    Left = 72
    Top = 8
    Width = 229
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
end
