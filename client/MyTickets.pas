unit MyTickets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, ExtCtrls, DB, DBISAMTb, StdCtrls, DMu, UQUtils, QMConst,
  Mask, DBCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxTextEdit, cxMaskEdit, cxCalendar, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel,
  cxGrid, cxGridBandedTableView, cxGridDBBandedTableView, cxMemo, cxDropDownEdit,
  cxVariants, cxLabel, cxGridDBDataDefinitions, cxNavigator;

const
  UM_EXPANDALL = WM_USER + $101;
  UM_DAMAGES_EXPANDALL = WM_USER + $102;
  UM_WORK_ORDERS_EXPANDALL = WM_USER + $103;

type
  {An interposer TScrollbox class to fix scrolling problem with embedded grids.
  Declared here to avoid the need to install a custom TScrollbox descendant into the IDE.}
  TScrollBox = class(Forms.TScrollBox)
    protected
      procedure AutoScrollInView(AControl: TControl); Override;
  end;

  TMyTicketsForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    TicketLocates: TDBISAMTable;
    OpenTickets: TDBISAMQuery;
    TicketSource: TDataSource;
    ViewStyleCombo: TComboBox;
    ViewLabel: TLabel;
    ApplyButton: TButton;
    ClearButton: TButton;
    ClearRoutes: TDBISAMQuery;
    SetRoute: TDBISAMQuery;
    OpenTicketCount: TDBEdit;
    DueTodayCount: TDBEdit;
    DueTomorrowCount: TDBEdit;
    CenterTicketSummary: TDBISAMTable;
    OpenTicketsLabel: TLabel;
    DueTodayLabel: TLabel;
    DueTomorrowLabel: TLabel;
    CenterTicketSummarySource: TDataSource;
    ExportButton: TButton;
    DamageSource: TDataSource;
    OpenDamages: TDBISAMQuery;
    MyWorkViewRepository: TcxGridViewRepository;
    DamagesGridDetailsTableView: TcxGridDBBandedTableView;
    DmgDColDamagePriority: TcxGridDBBandedColumn;
    DmgDColWorkPriorityDesc: TcxGridDBBandedColumn;
    DmgDColDamageType: TcxGridDBBandedColumn;
    DmgDColUQDamageID: TcxGridDBBandedColumn;
    DmgDColDamageID: TcxGridDBBandedColumn;
    DmgDColDueDate: TcxGridDBBandedColumn;
    DmgDColDamageDate: TcxGridDBBandedColumn;
    DmgDColTicketNumber: TcxGridDBBandedColumn;
    DmgDColProfitCenter: TcxGridDBBandedColumn;
    DmgDColCompanyDamaged: TcxGridDBBandedColumn;
    DmgDColFacilityType: TcxGridDBBandedColumn;
    DmgDColFacilitySize: TcxGridDBBandedColumn;
    DmgDColExcavatorCompany: TcxGridDBBandedColumn;
    DmgDColLocation: TcxGridDBBandedColumn;
    DmgDColDetailsCity: TcxGridDBBandedColumn;
    DmgDColDetailsCounty: TcxGridDBBandedColumn;
    DmgDColDetailsState: TcxGridDBBandedColumn;
    DmgDColExport: TcxGridDBBandedColumn;
    TicketsGridDetailsTableView: TcxGridDBBandedTableView;
    TktDColTicketNumber: TcxGridDBBandedColumn;
    TktDColKind: TcxGridDBBandedColumn;
    TktDColDueDate: TcxGridDBBandedColumn;
    TktDColWorkDate: TcxGridDBBandedColumn;
    TktDColTicketType: TcxGridDBBandedColumn;
    TktDColCompany: TcxGridDBBandedColumn;
    TktDColPriority: TcxGridDBBandedColumn;
    TktDColWorkType: TcxGridDBBandedColumn;
    TktDColWorkCity: TcxGridDBBandedColumn;
    TktDColStreetNum: TcxGridDBBandedColumn;
    TktDColStreet: TcxGridDBBandedColumn;
    TktDColWorkAddressStreet: TcxGridDBBandedColumn;
    TktDColWorkCounty: TcxGridDBBandedColumn;
    TktDColAlert: TcxGridDBBandedColumn;
    TktDColWorkDescription: TcxGridDBBandedColumn;
    TktDColLegalGoodThru: TcxGridDBBandedColumn;
    TktDColMapPage: TcxGridDBBandedColumn;
    TktDColTicketID: TcxGridDBBandedColumn;
    TktDColLocates: TcxGridDBBandedColumn;
    TktDColQuickClose: TcxGridDBBandedColumn;
    TktDColRoute: TcxGridDBBandedColumn;
    TktDColWorkPriority: TcxGridDBBandedColumn;
    TktDColFirstTask: TcxGridDBBandedColumn;
    TktDColExport: TcxGridDBBandedColumn;
    TktDColTicketStatus: TcxGridDBBandedColumn;
    TktDColWorkPriorityDescription: TcxGridDBBandedColumn;
    TktSColTicketID: TcxGridDBBandedColumn;
    TktSColTicketNumber: TcxGridDBBandedColumn;
    TktSColKind: TcxGridDBBandedColumn;
    TktSColAlert: TcxGridDBBandedColumn;
    TktSColDueDate: TcxGridDBBandedColumn;
    TktSColWorkAddressNumber: TcxGridDBBandedColumn;
    TktSColWorkAddressStreet: TcxGridDBBandedColumn;
    TktSColStatus: TcxGridDBBandedColumn;
    TktSColWorkDescription: TcxGridDBBandedColumn;
    TktSColLegalGoodThru: TcxGridDBBandedColumn;
    TktSColWorkCounty: TcxGridDBBandedColumn;
    TktSColMapPage: TcxGridDBBandedColumn;
    TktSColLocates: TcxGridDBBandedColumn;
    TktSColRoute: TcxGridDBBandedColumn;
    TktSColWorkPriority: TcxGridDBBandedColumn;
    TktSColQuickClose: TcxGridDBBandedColumn;
    TktSColFirstTask: TcxGridDBBandedColumn;
    TktSColExport: TcxGridDBBandedColumn;
    TktSColTicketStatus: TcxGridDBBandedColumn;
    TktSColWorkPriorityDescription: TcxGridDBBandedColumn;
    OpenWorkOrders: TDBISAMQuery;
    WorkOrdersSource: TDataSource;
    WorkOrdersGridDetailsTableView: TcxGridDBBandedTableView;
    WoDColWorkOrderID: TcxGridDBBandedColumn;
    WoDColWorkOrderNumber: TcxGridDBBandedColumn;
    WoDColClientID: TcxGridDBBandedColumn;
    WoDColWOSource: TcxGridDBBandedColumn;
    WoDColKind: TcxGridDBBandedColumn;
    WoDColStatus: TcxGridDBBandedColumn;
    WoDColMapPage: TcxGridDBBandedColumn;
    WoDColMapRef: TcxGridDBBandedColumn;
    WoDColDueDate: TcxGridDBBandedColumn;
    WoDColWorkType: TcxGridDBBandedColumn;
    WoDColWorkDescription: TcxGridDBBandedColumn;
    WoDColWorkAddressNumber: TcxGridDBBandedColumn;
    WoDColWorkAddressStreet: TcxGridDBBandedColumn;
    WoDColWorkCounty: TcxGridDBBandedColumn;
    WoDColWorkCity: TcxGridDBBandedColumn;
    WoDColWorkState: TcxGridDBBandedColumn;
    WoDColWorkZip: TcxGridDBBandedColumn;
    WoDColWorkLat: TcxGridDBBandedColumn;
    WoDColWorkLong: TcxGridDBBandedColumn;
    WoDColCallerName: TcxGridDBBandedColumn;
    WoDColCallerPhone: TcxGridDBBandedColumn;
    WoDColCallerCellular: TcxGridDBBandedColumn;
    WoDColClientTransmitDate: TcxGridDBBandedColumn;
    WoDColJobNumber: TcxGridDBBandedColumn;
    WoDColWireCenter: TcxGridDBBandedColumn;
    WoDColCentralOffice: TcxGridDBBandedColumn;
    WoDColServingTerminal: TcxGridDBBandedColumn;
    WoDColCircuitNumber: TcxGridDBBandedColumn;
    WoDColF2Cable: TcxGridDBBandedColumn;
    WoDColTerminalPort: TcxGridDBBandedColumn;
    WoDColF2Pair: TcxGridDBBandedColumn;
    WoDColStateHwyROW: TcxGridDBBandedColumn;
    WoDColRoadBoreCount: TcxGridDBBandedColumn;
    WoDColDrivewayBoreCount: TcxGridDBBandedColumn;
    WoDColWorkCenter: TcxGridDBBandedColumn;
    MyWorkScrollbox: TScrollBox;
    WorkOrdersGrid: TcxGrid;
    WorkOrdersGridLevelDetail: TcxGridLevel;
    WorkOrdersGridLevelSummary: TcxGridLevel;
    TicketsGrid: TcxGrid;
    TicketsGridLevelDetail: TcxGridLevel;
    TicketsGridLevelSummary: TcxGridLevel;
    DamagesGrid: TcxGrid;
    DamagesGridLevelDetail: TcxGridLevel;
    DamagesGridLevelSummary: TcxGridLevel;
    WorkOrdersGridSummaryTableView: TcxGridDBBandedTableView;
    WoSColWorkOrderID: TcxGridDBBandedColumn;
    WoSColWorkOrderNumber: TcxGridDBBandedColumn;
    WoSColDueDate: TcxGridDBBandedColumn;
    WoSColKind: TcxGridDBBandedColumn;
    WoSColStatus: TcxGridDBBandedColumn;
    WoSColWorkAddressNumber: TcxGridDBBandedColumn;
    WoSColWorkAddressStreet: TcxGridDBBandedColumn;
    WoSColWorkCounty: TcxGridDBBandedColumn;
    WoSColMapPage: TcxGridDBBandedColumn;
    WoSColMapRef: TcxGridDBBandedColumn;
    TicketsGridSummaryTableView: TcxGridDBBandedTableView;
    DamagesGridSummaryTableView: TcxGridDBBandedTableView;
    DmgSColDamageType: TcxGridDBBandedColumn;
    DmgSColUQDamageID: TcxGridDBBandedColumn;
    DmgSColDamageID: TcxGridDBBandedColumn;
    DmgSColDueDate: TcxGridDBBandedColumn;
    DmgSColDamageDate: TcxGridDBBandedColumn;
    DmgSColTicketNumber: TcxGridDBBandedColumn;
    DmgSColExcavatorCompany: TcxGridDBBandedColumn;
    DmgSColLocation: TcxGridDBBandedColumn;
    DmgSColCity: TcxGridDBBandedColumn;
    DmgSColCounty: TcxGridDBBandedColumn;
    DmgSColState: TcxGridDBBandedColumn;
    DmgSColPriorityDescription: TcxGridDBBandedColumn;
    DmgSColWorkPriority: TcxGridDBBandedColumn;
    DmgSColExport: TcxGridDBBandedColumn;
    WoDColExport: TcxGridDBBandedColumn;
    WoSColExport: TcxGridDBBandedColumn;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ColWorkPriorityGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure ColFirstTaskChange(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure ViewStyleComboChange(Sender: TObject);
    procedure OpenTicketsAfterScroll(DataSet: TDataSet);
    procedure ClearButtonClick(Sender: TObject);
    procedure OpenTicketsCalcFields(DataSet: TDataSet);
    procedure OpenTicketsBeforeOpen(DataSet: TDataSet);
    procedure ExportButtonClick(Sender: TObject);
    procedure DamagesGridDblClick(Sender: TObject);
    procedure GridCompare(ADataController: TcxCustomDataController; ARecordIndex1,
      ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);
    procedure OpenWorkOrdersCalcFields(DataSet: TDataSet);
    procedure WorkOrdersGridDblClick(Sender: TObject);
    procedure OpenWorkOrdersBeforeOpen(DataSet: TDataSet);
    procedure MyWorkScrollboxMouseWheel(Sender: TObject;
      Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
      var Handled: Boolean);
    procedure ViewMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ViewMouseWheel(Sender: TObject;
      Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
      var Handled: Boolean);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
  private
    FLastRefresh: TDateTime;
    FOnItemSelected: TItemSelectedEvent;
    FOnDamageSelected: TItemSelectedEvent;
    FOnWorkOrderSelected: TItemSelectedEvent;
    FOnSelectTicket: TItemSelectedEvent;
    FCurrentTicketFormat: string;
    procedure RefreshTickets;
    procedure RefreshDamages;
    procedure RefreshWorkOrders;
    function SelectedTicketID: Integer;
    function SelectedDamageID: Integer;
    function SelectedWorkOrderID: Integer;
    function DamageDueDateColumn: TcxGridColumn;
    function QuickCloseColumn: TcxGridColumn;
    function TicketStatusColumn: TcxGridColumn;
    function WorkOrderStatusColumn: TcxGridColumn;
    function IsValidStatus(const Status: string): Boolean;
    procedure RouteOnSetText(Sender: TField; const Text: string);
    procedure SetupColumns;
    procedure SetupDamageColumns;
    procedure SetupWorkOrderColumns;
    procedure ExpandAll;
    procedure DamagesExpandAll;
    procedure WorkOrdersExpandAll;
    procedure UMExpandAll(var Message: TMessage); message UM_EXPANDALL;
    procedure UMDamagesExpandAll(var Message: TMessage); message UM_DAMAGES_EXPANDALL;
    procedure UMWorkOrdersExpandAll(var Message: TMessage); message UM_WORK_ORDERS_EXPANDALL;
    procedure UpdateQuickCloseStatuses;
    procedure SyncFirstAndSecondRowColumns;
    procedure SetupPriorityDisplay;
    function ExportMyTickets: string;
    function ExportMyDamages: string;
    function ExportMyWorkOrders: string;
    function GetLimitedTicketIDsString(Limit: Integer): string;
    function FirstTaskColumn: TcxGridColumn;
    procedure SetTicketEstimatedStartTime(const TicketID: Integer; const StartTime: TDateTime);
    function GetItemIDsForExportString(Dataset: TDataset; ItemFieldName: string): string; override;
    function FocusedDamageRecord: TcxCustomGridRecord;
    function FocusedTicketRecord: TcxCustomGridRecord;
    function FocusedWorkOrderRecord: TcxCustomGridRecord;
    function IsDamageView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
    function IsTicketView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
    function IsWorkOrderView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
    procedure SetRecordValue(AView: TcxCustomGridTableView; ARecordIndex,
      AItemIndex: Integer; AValue: Variant);
    function TicketIDColumn: TcxGridColumn;
    function DamageIDColumn: TcxGridColumn;
    function WorkOrderIDColumn: TcxGridColumn;
    procedure ResetGridHeights(ExpandView: Boolean);
    procedure SetExportAll(DataSet: TDataSet; AView: TcxCustomGridTableView; Checked: Boolean);
    procedure CheckAllExportCheckboxes(Checked: Boolean);
  public
    property OnItemSelected: TItemSelectedEvent read FOnItemSelected write FOnItemSelected;
    property OnDamageSelected: TItemSelectedEvent read FOnDamageSelected write FOnDamageSelected;
    property OnWorkOrderSelected: TItemSelectedEvent read FOnWorkOrderSelected write FOnWorkOrderSelected;
    property OnSelectTicket: TItemSelectedEvent read FOnSelectTicket write FOnSelectTicket;
    function GetCurrentTicketID(var TicketID: Integer): Boolean;
    function GetCurrentDamageID(var DamageID: Integer): Boolean;
    function GetCurrentWorkOrderID(var WorkOrderID: Integer): Boolean;
    procedure ActivatingNow; override;
  end;

implementation

{$R *.dfm}

uses StateMgr, OdHourglass, OdDBUtils, OdMiscUtils, OdCxUtils, OdDBISAMUtils, DateUtils,
  OdExceptions, StartTimeDialog, SharedDevExStyles;

const
  NoStatus = '---';
  InvalidID = Low(Integer);
  TicketSortColumns: array[0..3] of string = ('work_priority', 'first_task', 'due_date', 'ticket_number');
  TicketSortDirections: array[0..3] of TcxGridSortOrder = (soDescending, soDescending, soAscending, soAscending);
  DamageSortColumns: array[0..1] of string = ('work_priority', 'due_date');
  DamageSortDirections: array[0..1] of TcxGridSortOrder = (soDescending, soAscending);
  WorkOrderSortColumns: array[0..0] of string = ('due_date');
  WorkOrderSortDirections: array[0..0] of TcxGridSortOrder = (soAscending);

  SelectMyTickets = 'select ticket.ticket_id, ticket_number, kind, due_date, ticket_type, ' +
    'company, priority, work_type, work_city, work_address_street, work_county, ' +
    'work_description, legal_good_thru, map_page, ticket_format, ' +
    'work_address_number, do_not_mark_before, work_date, LocalStringKey, alert, ' +
    'coalesce(ref.sortby, 0) work_priority, coalesce(ref.description, ''Normal'') priority_disp, ' +
    'if(task.est_start_date is null, False, True) first_task ' +
    ', False export, ''---'' quick_close ' +
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'left join task_schedule task on (task.ticket_id=ticket.ticket_id and task.active=True ' +
    'and task.est_start_date >= :start_date_before and task.est_start_date < :start_date_after) ' +
    'where locate.locator_id=:emp_id ' +
    'and locate.active ' +
    'and NOT locate.closed ' +
    'and ((do_not_mark_before is NULL) ' +
    'or (do_not_mark_before <= :last_sync)) ' +
    '%s ' + // placeholder for 'ticket.ticket_id in ()' criteria
    '%s ' + // placeholder for DueDateCriteria
    'group by ticket_id, ticket_number, kind, due_date, ticket_type, company, ' +
    'priority, work_type, work_city, work_address_street, work_county, ' +
    'legal_good_thru, map_page, ticket_format, ' +
    'work_address_number, do_not_mark_before, work_priority ' +
    'order by work_priority desc, first_task, due_date, ticket_number';

  SelectMyDamages = 'select damage.damage_id, uq_damage_id, damage_date, due_date, damage_type,  ' +
    'ticket_id, t.ticket_number, profit_center, utility_co_damaged, location, city, county, state,  ' +
    'facility_type, facility_size, excavator_company, ' +
    'coalesce(ref.sortby, 0) work_priority, coalesce(ref.description, ''Normal'') priority_description ' +
    ', False export ' +
    'from damage ' +
    'left join reference ref on damage.work_priority_id= ref.ref_id ' +
    'left join ticket t on damage.ticket_id = t.ticket_id ' +
    'where investigator_id = :emp_id ' +
    'and damage_type in (''' + DamageTypePending + ''') ' +
    'and damage.active ' +
    'order by work_priority desc, due_date';

  SelectMyWorkOrders = 'select wo_id, wo_number, client.client_name, wo_source, Kind, status, ' +
    'map_page, map_ref, due_date, work_type, work_description, work_address_number, ' +
    'work_address_street, work_county, work_city, work_state, work_zip, work_lat, work_long, ' +
    'caller_name, caller_phone, caller_cellular, transmit_date, job_number, wire_center, work_center, ' +
    'central_office, serving_terminal, circuit_number, f2_cable, terminal_port, f2_pair, state_hwy_row, ' +
    ' road_bore_count, driveway_bore_count, False export  ' +
    'from work_order ' +
    '  left join client on client.client_id = work_order.client_id ' +
    'where assigned_to_id = :emp_id ' +
    'and active ' +
    'and not closed ' +
    'and kind <> ''' + WorkOrderKindCancel + ''' ' +
    'order by due_date, wo_number';

{ TScrollBox }

procedure TScrollBox.AutoScrollInView(AControl: TControl);
begin
  //Do not call inherited;
end;

{ TMyTicketsForm }

procedure TMyTicketsForm.FormShow(Sender: TObject);
var
  LastSync: TDateTime;
  ExportSelectionButtonsVisible: Boolean;
begin
  LastSync := DM.UQState.LastLocalSyncTime;
  if FLastRefresh <> LastSync then begin
    RefreshTickets;
    RefreshDamages;
    RefreshWorkOrders;
    FLastRefresh := LastSync;
  end;
  ExportSelectionButtonsVisible := DM.CanI(RightWorkOrdersSelectiveExport) or
    DM.CanI(RightTicketsSelectiveExport) or DM.CanI(RightDamagesSelectiveExport);
  SelectAllButton.Visible := ExportSelectionButtonsVisible;
  SelectNoneButton.Visible := ExportSelectionButtonsVisible;
end;

function TMyTicketsForm.GetLimitedTicketIDsString(Limit: Integer): string;
var
  List: TStringList;
begin
  Result := '';
  List := TStringList.Create;
  try
    DM.GetLimitedTicketIdList(List, Limit);
    Result := GetSQLInClauseForStrings(List, False);
  finally
    FreeAndNil(List);
  end;
end;

procedure TMyTicketsForm.RefreshTickets;

  procedure SetMyTicketsSelectStatement;
  var
    TicketViewLimit: Integer;
    TicketIDCriteria: string;
    DueDateCriteria : string;
  begin
    TicketViewLimit := DM.MyTicketViewLimit;
    OpenTickets.Close;
    if DM.ShowFutureWorkForLocator then
      DueDateCriteria := ''
    else
      DueDateCriteria := ' and ((due_date < ' + DateToDBISAMDate(Date + 1) + ') or (due_date is null))'; //include today; i.e. any less than tomorrow
    if TicketViewLimit > 0 then begin
      TicketIDCriteria := GetLimitedTicketIDsString(TicketViewLimit);
      if NotEmpty(TicketIDCriteria) then
        TicketIDCriteria := 'and ticket.ticket_id in (' + TicketIDCriteria + ') ';
      OpenTickets.SQL.Text := Format(SelectMyTickets, [TicketIDCriteria, DueDateCriteria]);
    end
    else
      OpenTickets.SQL.Text := Format(SelectMyTickets, ['', DueDateCriteria]);
  end;

var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  OpenTickets.DisableControls;
  try
    SetMyTicketsSelectStatement;
    OpenTickets.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenTickets.ParamByName('last_sync').AsDateTime := DM.UQState.LastLocalSyncTime;
    OpenTickets.ParamByName('start_date_before').AsDateTime := Today;
    OpenTickets.ParamByName('start_date_after').AsDateTime := Tomorrow + 3;
    OpenTickets.Open;
    SetFieldDisplayFormat(OpenTickets, 'work_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenTickets, 'due_date', 'mmm d, t');
    TicketLocates.Close;
    RefreshDataSet(CenterTicketSummary);
  finally
    OpenTickets.EnableControls;
  end;
  SetupColumns;
  ExpandAll;

  ResetGridHeights(True);
end;

procedure TMyTicketsForm.RefreshDamages;
var
  Cursor: IInterface;
begin
  if not DM.HaveDamagesRights then begin
    DamagesGrid.Visible := False;
    Exit;
  end;

  Cursor := ShowHourGlass;
  OpenDamages.DisableControls;
  try
    OpenDamages.Close;
    OpenDamages.SQL.Text := SelectMyDamages;
    OpenDamages.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenDamages.Open;
    SetFieldDisplayFormat(OpenDamages, 'damage_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenDamages, 'due_date', 'mmm d, t');
  finally
    OpenDamages.EnableControls;
  end;

  DamagesGrid.Visible := (OpenDamages.RecordCount >= 1);
  if DamagesGrid.Visible then
    SetupDamageColumns;

  ResetGridHeights(True);
end;

procedure TMyTicketsForm.RefreshWorkOrders;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  OpenWorkOrders.DisableControls;
  try
    OpenWorkOrders.Close;
    OpenWorkOrders.SQL.Text := SelectMyWorkOrders;
    OpenWorkOrders.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenWorkOrders.Open;
    SetFieldDisplayFormat(OpenWorkOrders, 'transmit_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenWorkOrders, 'due_date', 'mmm d, t');
  finally
    OpenWorkOrders.EnableControls;
  end;

  WorkOrdersGrid.Visible := (OpenWorkOrders.RecordCount >= 1);
  if WorkOrdersGrid.Visible then
    SetupWorkOrderColumns;

  ResetGridHeights(True);
end;

procedure TMyTicketsForm.GridDblClick(Sender: TObject);
var
  TicketID: Integer;
begin
  if Assigned(FOnItemSelected) then
    if GetCurrentTicketID(TicketID) then
      FOnItemSelected(Self, TicketID);
end;

procedure TMyTicketsForm.DamagesGridDblClick(Sender: TObject);
var
  DamageID: Integer;
begin
  if Assigned(FOnDamageSelected) then
    if GetCurrentDamageID(DamageID) then
      FOnDamageSelected(Self, DamageID);
end;

procedure TMyTicketsForm.WorkOrdersGridDblClick(Sender: TObject);
var
  WorkOrderID: Integer;
begin
  inherited;
  if Assigned(FOnWorkOrderSelected) then
    if GetCurrentWorkOrderID(WorkOrderID) then
      FOnWorkOrderSelected(Self, WorkOrderID);
end;

procedure TMyTicketsForm.FormCreate(Sender: TObject);
begin
  ViewStyleCombo.Items.Add('Details');
  ViewStyleCombo.Items.Add('Summary');
  ViewStyleCombo.ItemIndex := 0;
  ViewStyleComboChange(ViewStyleCombo);
end;

procedure TMyTicketsForm.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  DamageStatus: TDamageStatus;
  TicketStatus: TTicketStatus;
  WorkOrderStatus: TWorkOrderStatus;
  DueDate: TDateTime;
  TicketQuickClose: String;
begin
  inherited;
  if (AViewInfo.GridRecord is TcxGridGroupRow) then
    Exit;

  if IsTicketView(AViewInfo) then begin
    if VarIsNull(AViewInfo.GridRecord.Values[QuickCloseColumn.Index]) then
      TicketQuickClose := NoStatus
    else
      TicketQuickClose := AViewInfo.GridRecord.Values[QuickCloseColumn.Index];
    if IsValidStatus(TicketQuickClose) then
      ACanvas.Font.Style := [fsBold];
    if not AViewInfo.Selected then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[TicketStatusColumn.Index]) then
        TicketStatus := TTicketStatus(Byte(AViewInfo.GridRecord.Values[TicketStatusColumn.Index]))
      else
        TicketStatus := [];
      ACanvas.Brush.Color := GetTicketColor(TicketStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
    end;
  end
  else if IsDamageView(AViewInfo) then begin
    if not AViewInfo.Selected then begin
      if VarIsNull(AViewInfo.GridRecord.Values[DamageDueDateColumn.Index]) then
        DueDate := 0
      else
        DueDate := (AViewInfo.GridRecord.Values[DamageDueDateColumn.Index]);
      DamageStatus := GetDamageStatusFromValues(DueDate);
      ACanvas.Brush.Color := GetDamageColor(DamageStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
    end;
  end
  else if IsWorkOrderView(AViewInfo) then begin
    if not AViewInfo.Selected then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[WorkOrderStatusColumn.Index]) then
        WorkOrderStatus := TWorkOrderStatus(Byte(AViewInfo.GridRecord.Values[WorkOrderStatusColumn.Index]))
      else
        WorkOrderStatus := [];
      ACanvas.Brush.Color := GetWorkOrderColor(WorkOrderStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
    end;
  end;
  ADone := False;
end;

function TMyTicketsForm.IsDamageView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
begin
  Result := (AViewInfo.GridView = DamagesGridDetailsTableView) or
    (AViewInfo.GridView = DamagesGridSummaryTableView);
end;

function TMyTicketsForm.IsTicketView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
begin
  Result := (AViewInfo.GridView = TicketsGridDetailsTableView) or
    (AViewInfo.GridView = TicketsGridSummaryTableView);
end;

function TMyTicketsForm.IsWorkOrderView(AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
begin
  Result := (AViewInfo.GridView = WorkOrdersGridDetailsTableView) or
    (AViewInfo.GridView = WorkOrdersGridSummaryTableView);
end;

function TMyTicketsForm.SelectedTicketID: Integer;
begin
  Result := InvalidID;
  if (FocusedTicketRecord is TcxGridGroupRow) then
    Exit;

  if FocusedTicketRecord <> nil then begin
    if (not VarIsNull(FocusedTicketRecord.Values[TicketIDColumn.Index])) then
      Result := FocusedTicketRecord.Values[TicketIDColumn.Index];
  end;
end;

function TMyTicketsForm.SelectedDamageID: Integer;
begin
  Result := InvalidID;
  if (FocusedDamageRecord is TcxGridGroupRow) then
    Exit;

  if FocusedDamageRecord <> nil then begin
    if not VarIsNull(FocusedDamageRecord.Values[DamageIDColumn.Index]) then
      Result := FocusedDamageRecord.Values[DamageIDColumn.Index];
  end;
end;

function TMyTicketsForm.SelectedWorkOrderID: Integer;
begin
  Result := InvalidID;
  if (FocusedWorkOrderRecord is TcxGridGroupRow) then
    Exit;

  if FocusedWorkOrderRecord <> nil then begin
    if not VarIsNull(FocusedWorkOrderRecord.Values[WorkOrderIDColumn.Index]) then
      Result := FocusedWorkOrderRecord.Values[WorkOrderIDColumn.Index];
  end;
end;

function TMyTicketsForm.FocusedDamageRecord: TcxCustomGridRecord;
var
  View : TcxGridTableView;
begin
  Result := nil;
  View := TcxGridTableView(DamagesGrid.FocusedView);
  if Assigned(View) then
    Result := View.Controller.FocusedRecord;
end;

function TMyTicketsForm.FocusedTicketRecord: TcxCustomGridRecord;
var
  View : TcxGridTableView;
begin
  Result := nil;
  View := TcxGridTableView(TicketsGrid.FocusedView);
  if Assigned(View) then
    Result := View.Controller.FocusedRecord;
end;

function TMyTicketsForm.FocusedWorkOrderRecord: TcxCustomGridRecord;
var
  View : TcxGridTableView;
begin
  Result := nil;
  View := TcxGridTableView(WorkOrdersGrid.FocusedView);
  if Assigned(View) then
    Result := View.Controller.FocusedRecord;
end;

function TMyTicketsForm.DamageIDColumn: TcxGridColumn;
begin
  if DamagesGrid.FocusedView = DamagesGridDetailsTableView then
    Result := DmgDColDamageID
  else
    Result := DmgSColDamageID;
end;

function TMyTicketsForm.WorkOrderIDColumn: TcxGridColumn;
begin
  if WorkOrdersGrid.FocusedView = WorkOrdersGridDetailsTableView then
    Result := WoDColWorkOrderID
  else
    Result := WoSColWorkOrderID;
end;

function TMyTicketsForm.DamageDueDateColumn: TcxGridColumn;
begin
  if DamagesGrid.FocusedView = DamagesGridSummaryTableView then
    Result := DmgSColDueDate
  else
    Result := DmgDColDueDate;
end;

function TMyTicketsForm.TicketIDColumn: TcxGridColumn;
begin
  if TicketsGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColTicketID
  else
    Result := TktSColTicketID;
end;

function TMyTicketsForm.QuickCloseColumn: TcxGridColumn;
begin
  if TicketsGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColQuickClose
  else
    Result := TktSColQuickClose;
end;

function TMyTicketsForm.TicketStatusColumn: TcxGridColumn;
begin
  if TicketsGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColTicketStatus
  else
    Result := TktSColTicketStatus;
end;

function TMyTicketsForm.WorkOrderStatusColumn: TcxGridColumn;
begin
  if WorkOrdersGrid.FocusedView = WorkOrdersGridDetailsTableView then
    Result := WoDColStatus
  else
    Result := WoSColStatus;
end;

function TMyTicketsForm.FirstTaskColumn: TcxGridColumn;
begin
  if TicketsGrid.FocusedView = TicketsGridDetailsTableView then
    Result := TktDColFirstTask
  else
    Result := TktSColFirstTask;
end;

procedure TMyTicketsForm.SetRecordValue(AView: TcxCustomGridTableView;
  ARecordIndex, AItemIndex: Integer; AValue: Variant);
begin
  AView.DataController.SetValue(ARecordIndex, AItemIndex, AValue);
end;

procedure TMyTicketsForm.ApplyButtonClick(Sender: TObject);
var
  Cursor: IInterface;
  TicketsView : TcxCustomGridTableView;
  I: Integer;
  QuickCloseStatus: String;
  TicketID: Integer;
begin
  Cursor := ShowHourGlass;

  TicketsView := TcxCustomGridTableView(TicketsGrid.FocusedView);
  for I := 0 to TicketsView.DataController.RecordCount -1 do begin
    if VarIsNull(TicketsView.DataController.Values[I, QuickCloseColumn.Index]) then
      QuickCloseStatus := NoStatus
    else
      QuickCloseStatus := TicketsView.DataController.Values[I, QuickCloseColumn.Index];
    if VarIsNull(TicketsView.DataController.Values[I, TicketIDColumn.Index]) then
      TicketID := InvalidID
    else
      TicketID := TicketsView.DataController.Values[I, TicketIDColumn.Index];

    if IsValidStatus(QuickCloseStatus) then begin
      DM.QuickClose(TicketID, QuickCloseStatus);
      SetRecordValue(TicketsView, I, QuickCloseColumn.Index, NoStatus);
    end;
  end;

  RefreshTickets;
  RefreshDamages;
  RefreshWorkOrders;
end;

function TMyTicketsForm.IsValidStatus(const Status: string): Boolean;
begin
  Result := True;
  if (Trim(Status) = '') or (Status = NoStatus) then
    Result := False;
end;

procedure TMyTicketsForm.ViewStyleComboChange(Sender: TObject);
begin
  FCurrentTicketFormat := ''; // force a refresh of the QuickClose combo
  TicketsGrid.Levels[ViewStyleCombo.ItemIndex].MakeVisible;
  DamagesGrid.Levels[ViewStyleCombo.ItemIndex].MakeVisible;
  WorkOrdersGrid.Levels[ViewStyleCombo.ItemIndex].MakeVisible;
  SetupColumns;
  SetupDamageColumns;
  SetupWorkOrderColumns;
  ResetGridHeights(True);
end;

procedure TMyTicketsForm.SetupColumns;
begin
  SetupPriorityDisplay;
  if Assigned(OpenTickets.FindField('LocalStringKey')) then
    OpenTickets.FieldByName('LocalStringKey').OnSetText := RouteOnSetText;
  UpdateQuickCloseStatuses;
  TktDColFirstTask.Properties.OnChange := ColFirstTaskChange;
  TktSColFirstTask.Properties.OnChange := ColFirstTaskChange;
  TktDColExport.Visible := DM.CanI(RightTicketsSelectiveExport);
  TktSColExport.Visible := DM.CanI(RightTicketsSelectiveExport);
  DmgDColExport.Visible := DM.CanI(RightDamagesSelectiveExport);
  DmgSColExport.Visible := DM.CanI(RightDamagesSelectiveExport);
  WoDColExport.Visible := DM.CanI(RightWorkOrdersSelectiveExport);
  WoSColExport.Visible := DM.CanI(RightWorkOrdersSelectiveExport);
  SortGridByFieldNames(TicketsGrid, TicketSortColumns, TicketSortDirections);
  SyncFirstAndSecondRowColumns;
  ExpandAll;
end;

procedure TMyTicketsForm.SetupDamageColumns;
begin
  SortGridByFieldNames(DamagesGrid, DamageSortColumns, DamageSortDirections);
  DamagesExpandAll;
end;

procedure TMyTicketsForm.SetupWorkOrderColumns;
begin
  SortGridByFieldNames(WorkOrdersGrid, WorkOrderSortColumns, WorkOrderSortDirections);
  WorkOrdersExpandAll;
end;

procedure TMyTicketsForm.ExpandAll;
begin
  PostMessage(Self.Handle, UM_EXPANDALL, 0, 0);
end;

procedure TMyTicketsForm.DamagesExpandAll;
begin
  PostMessage(Self.Handle, UM_DAMAGES_EXPANDALL, 0, 0);
end;

procedure TMyTicketsForm.WorkOrdersExpandAll;
begin
  PostMessage(Self.Handle, UM_WORK_ORDERS_EXPANDALL, 0, 0);
  If WorkOrdersGrid.CanFocus then
    WorkOrdersGrid.SetFocus;
end;

procedure TMyTicketsForm.UMExpandAll(var Message: TMessage);
begin
  FullExpandGrid(TicketsGrid);
  if TicketsGrid.CanFocus then
    TicketsGrid.SetFocus
end;

procedure TMyTicketsForm.UMDamagesExpandAll(var Message: TMessage);
begin
  FullExpandGrid(DamagesGrid);
  if DamagesGrid.CanFocus then
    DamagesGrid.SetFocus
end;

procedure TMyTicketsForm.UMWorkOrdersExpandAll(var Message: TMessage);
begin
  FullExpandGrid(WorkOrdersGrid);
  if WorkOrdersGrid.CanFocus then
    WorkOrdersGrid.SetFocus
end;

function TMyTicketsForm.GetCurrentTicketID(var TicketID: Integer): Boolean;
begin
  Result := True;
  TicketID := SelectedTicketID;
  if TicketID = InvalidID then
    Result := False;
end;

function TMyTicketsForm.GetCurrentDamageID(var DamageID: Integer): Boolean;
begin
  Result := True;
  DamageID := SelectedDamageID;
  if DamageID = InvalidID then
    Result := False;
end;

function TMyTicketsForm.GetCurrentWorkOrderID(var WorkOrderID: Integer): Boolean;
begin
  Result := True;
  WorkOrderID := SelectedWorkOrderID;
  if WorkOrderID = InvalidID then
    Result := False;
end;

procedure TMyTicketsForm.OpenTicketsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if Assigned(FOnSelectTicket) then
    FOnSelectTicket(Self, DataSet.FieldByName('ticket_id').AsInteger);
  UpdateQuickCloseStatuses;
end;

procedure TMyTicketsForm.ClearButtonClick(Sender: TObject);
begin
  DM.AddEmployeeActivityEvent(ActivityTypeRouteTicket, 'Clear route');
  ClearRoutes.ExecSQL;
  RefreshTickets;
end;

procedure TMyTicketsForm.RouteOnSetText(Sender: TField; const Text: string);
begin
  DM.AddEmployeeActivityEvent(ActivityTypeRouteTicket, Sender.DataSet.FieldByName('ticket_id').AsString);
  SetRoute.ParamByName('Route').AsString := Text;
  SetRoute.ParamByName('TicketID').AsInteger := Sender.DataSet.FieldByName('ticket_id').AsInteger;
  SetRoute.ExecSQL;
  Sender.AsString := Text;
end;

procedure TMyTicketsForm.GridCompare(ADataController: TcxCustomDataController;
  ARecordIndex1, ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant;
  var Compare: Integer);
const
  SortValueA: array [Boolean] of Integer = (-1, 1);
begin
  if VarType(V1) = varNull then begin
    if VarType(V2) = varNull then
      Compare := 0
    else
      Compare := SortValueA[ADataController.GetItemSortOrder(AItemIndex) = soAscending];
  end else begin
    if VarType(V2) = varNull then
      Compare := SortValueA[ADataController.GetItemSortOrder(AItemIndex) = soDescending]
    else
      Compare := VarCompare(V1, V2);
  end;
end;

procedure TMyTicketsForm.OpenTicketsCalcFields(DataSet: TDataSet);
var
  Alert2: string;
begin
  inherited;
  Alert2 := '';
  if DataSet.FieldByName('alert').AsString = 'A' then
    Alert2 := '!';

  DataSet.FieldByName('alert2').AsString := Alert2;
  DataSet.FieldByName('ticket_status').AsInteger := Byte(GetTicketStatusFromRecord(DataSet));
  DataSet.FieldByName('locates').AsString := GetLocatesStringForTicket(TicketLocates,
    DataSet.FieldByName('ticket_id').AsInteger);
end;

procedure TMyTicketsForm.OpenTicketsBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddCalculatedField('alert2', DataSet, TStringField, 10);
  AddCalculatedField('locates', DataSet, TStringField, 100);
  AddCalculatedField('ticket_status', DataSet, TIntegerField, 0);
end;

procedure TMyTicketsForm.OpenWorkOrdersCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('work_order_status').AsInteger := Byte(GetWorkOrderStatusFromRecord(DataSet));
end;

procedure TMyTicketsForm.OpenWorkOrdersBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  AddCalculatedField('work_order_status', DataSet, TIntegerField, 0);
end;

procedure TMyTicketsForm.UpdateQuickCloseStatuses;
var
  TicketFormat: string;
begin
  if OpenTickets.IsEmpty then begin
    CxComboBoxItems(QuickCloseColumn).Clear;
    FCurrentTicketFormat := '';
  end
  else begin
    TicketFormat := OpenTickets.FieldByName('ticket_format').AsString;
    if FCurrentTicketFormat <> TicketFormat then begin
      CxComboBoxItems(QuickCloseColumn).Clear;
      FCurrentTicketFormat := TicketFormat;
      DM.GetQuickCloseStatuses(FCurrentTicketFormat, CxComboBoxItems(QuickCloseColumn));
      DM.ApplyEmployeeStatusLimit(CxComboBoxItems(QuickCloseColumn));
      CxComboBoxItems(QuickCloseColumn).Insert(0, NoStatus);
    end;
  end;
end;

procedure TMyTicketsForm.SyncFirstAndSecondRowColumns;
{var
  Row1Column: TdxDBTreeListColumn;
  Row2Column: TdxDBTreeListColumn;
  i: Integer;}
begin
{  // TODO: This does not work in our old DX grid version.  We should try again once we upgrade.
  Exit;
  if ViewStyleCombo.Text = 'Details' then begin
    Grid.BeginUpdate;
    Grid.OptionsView := Grid.OptionsView - [edgoAutoWidth];
    for i := 10 downto 0 do begin
      Row1Column := FindGridColumn(Grid, 0, i);
      Row2Column := FindGridColumn(Grid, 1, i);
      if Assigned(Row1Column) and Assigned(Row2Column) then
        Row2Column.Width := Row1Column.Width;
    end;
    Grid.OptionsView := Grid.OptionsView + [edgoAutoWidth];
    Grid.EndUpdate;
  end;}
end;

procedure TMyTicketsForm.ActivatingNow;
begin
  inherited;
  SortGridByFieldNames(TicketsGrid, TicketSortColumns, TicketSortDirections);
  SortGridByFieldNames(DamagesGrid, DamageSortColumns, DamageSortDirections);
  SortGridByFieldNames(WorkOrdersGrid, WorkOrderSortColumns, WorkOrderSortDirections);
end;

procedure TMyTicketsForm.ColWorkPriorityGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: String);
var
  DescCol : TcxGridColumn;
begin
  inherited;
  Assert(Assigned(Sender) and (Sender is TcxGridColumn));
  if (ARecord is TcxGridGroupRow) then
    Exit;

  if (Sender as TcxGridColumn) = TktSColWorkPriority then
    DescCol := TktSColWorkPriorityDescription
  else
    DescCol := TktDColWorkPriorityDescription;
  if (not VarIsNull(ARecord.Values[DescCol.Index])) then
    AText := ARecord.Values[DescCol.Index];
end;

procedure TMyTicketsForm.ColFirstTaskChange(Sender: TObject);
var
  CurrentTicketID: Integer;
  TaskedTicketID: Integer;
  EstStartDateTime: TDateTime;
  MarkedAsFirstTask: Boolean;
begin
  Assert(Assigned(Sender) and (Sender is TcxCheckBox));

  MarkedAsFirstTask := False;
  if FocusedTicketRecord <> nil then begin
    if (not VarIsNull(FocusedTicketRecord.Values[FirstTaskColumn.Index])) then
      MarkedAsFirstTask := (FocusedTicketRecord.Values[FirstTaskColumn.Index] = True)
  end;
  if MarkedAsFirstTask then begin
    CancelDataSet(OpenTickets);
    raise EOdDataEntryError.Create('You cannot change the ticket marked as tomorrow''s first task.');
  end;

  if not MarkedAsFirstTask then begin
    if DM.FirstTicketTaskedForTomorrow(TaskedTicketID) then begin
      CancelDataSet(OpenTickets);
      raise EOdDataEntryError.Create('There is already a ticket marked as tomorrow''s first task.');
    end;
    if not GetCurrentTicketID(CurrentTicketID) or (CurrentTicketID < 0) then begin
      CancelDataSet(OpenTickets);
      raise Exception.Create('Selected ticket cannot be marked as tomorrow''s first task. Sync and try again.');
    end;
    if not PromptForTaskStartTime(EstStartDateTime) then begin
      CancelDataSet(OpenTickets);
      Abort;
    end;
    SetTicketEstimatedStartTime(CurrentTicketID, EstStartDateTime);
  end;
end;

procedure TMyTicketsForm.SetTicketEstimatedStartTime(const TicketID: Integer; const StartTime: TDateTime);
begin
  DM.AddTicketToTaskSchedule(DM.EmpID, TicketID, StartTime);
  RefreshDataSet(OpenTickets);
end;

procedure TMyTicketsForm.SetupPriorityDisplay;
  procedure SetupPriorityColumn(Column: TcxGridColumn);
  begin
    Column.OnGetDisplayText := ColWorkPriorityGetDisplayText;
    if (Column.PropertiesClass = TcxTextEditProperties) then
      TcxTextEditProperties(Column.Properties).Alignment.Horz := taLeftJustify;
  end;
begin
  SetupPriorityColumn(TktDColWorkPriority);
  SetupPriorityColumn(TktSColWorkPriority);
end;

function TMyTicketsForm.ExportMyTickets: string;
const
  SelectMyTicketsExport = 'select distinct ticket_number "Ticket #", ' +
    'work_lat "Lat", work_long "Long", due_date, ' +
    'do_not_mark_before, coalesce(ref.sortby, 0) "work_priority", ' +
    'TRIM(LEADING '' '' FROM (COALESCE(work_address_number, '''')+' +
    ''' ''+work_address_street)) "Name", ' +
    'CAST(due_date AS varchar(16)) "Name2", ' +
    'work_address_number "Address", work_address_street "Street", ' +
    'work_city "City", work_state "State" ' +
    'from ticket inner join locate on ticket.ticket_id=locate.ticket_id ' +
    'left join reference ref on ticket.work_priority_id=ref.ref_id ' +
    'where locate.locator_id = %d ' +
    'and locate.active = True ' +
    'and locate.closed = False ' +
    'and (do_not_mark_before is NULL or do_not_mark_before <= %s) ' +
    '%s ' + // Placeholder for 'ticket.ticket_id in ()' criteria
    'order by work_priority desc, due_date, ticket_number';
var
  Wait: IInterface;
  ExportFileName: string;
  TicketViewLimit: Integer;
  TicketIDCriteria: string;
  VisibleOrSelectedTicketIDs: string;
  TicketsToExport: TDataSet;
  Select: string;
  SkipFields: TStringList;
  TicketExportCount: Integer;
  CanSelectTicketsToExport: Boolean;
begin
  Result := '';
  Wait := ShowHourGlass;
  TicketViewLimit := DM.MyTicketViewLimit;
  TicketIDCriteria := '';
  CanSelectTicketsToExport := DM.CanI(RightTicketsSelectiveExport);

  if CanSelectTicketsToExport then // export only selected tickets
    VisibleOrSelectedTicketIDs := GetItemIDsForExportString(OpenTickets, 'ticket_id')
  else if TicketViewLimit > 0 then // export only viewable tickets
    VisibleOrSelectedTicketIDs := GetLimitedTicketIDsString(TicketViewLimit)
  else                             // export all assigned tickets
    VisibleOrSelectedTicketIDs := '';

  if CanSelectTicketsToExport and IsEmpty(VisibleOrSelectedTicketIDs) then // nothing to export
    Exit;

  if NotEmpty(VisibleOrSelectedTicketIDs) then
    TicketIDCriteria := 'and ticket.ticket_id in (' + VisibleOrSelectedTicketIDs + ') ';

  SkipFields := TStringList.Create;
  try
    SkipFields.Add('due_date');
    SkipFields.Add('do_not_mark_before');
    SkipFields.Add('work_priority');

    Select := Format(SelectMyTicketsExport, [DM.EmpID,
      DateToDBISAMDate(DM.UQState.LastLocalSyncTime), TicketIDCriteria]);

    TicketsToExport := DM.Engine.OpenQuery(Select);
    try
      TicketExportCount := TicketsToExport.RecordCount;
      ExportFileName := DM.CreateExportFileName('-My Tickets', '.csv');
      SaveDelimToFile(TicketsToExport, ExportFileName, ',', True, nil, SkipFields);
      Result := ExportFileName;
    finally
      FreeAndNil(TicketsToExport);
    end;
  finally
    FreeAndNil(SkipFields);
  end;

  DM.AddEmployeeActivityEvent(ActivityTypeTicketRouteExport, IntToStr(TicketExportCount));
end;

function TMyTicketsForm.ExportMyDamages: string; // returns exported filenames
const
  SelectMyDamagesExport = 'select distinct uq_damage_id "Ticket #", ' +
    't.work_lat "Lat", t.work_long "Long", d.due_date, ' +
    'coalesce(ref.sortby, 0) "work_priority", ' +
    'd.location "Name", ' +
    'CAST(d.due_date as varchar(16)) "Name2", ' +
    'd.location "Address", d.location "Street", ' +
    'd.city "City", d.state "State" ' +
    'from damage d ' +
    'left join ticket t on t.ticket_id = d.ticket_id ' +
    'left join reference ref on d.work_priority_id=ref.ref_id ' +
    'where d.investigator_id = %d ' +
    'and d.active = True ' +
    'and d.damage_type in (''%s'')' +
    '%s ' + // Placeholder for 'd.damage_id in ()' criteria
    'order by work_priority desc, d.due_date, d.uq_damage_id ';
var
  Wait: IInterface;
  ExportFileName: string;
  DamageIDCriteria: string;
  SelectedDamageIDs : string;
  DamagesToExport: TDataSet;
  Select: string;
  SkipFields: TStringList;
  SelectedDamageExportCount: Integer;
  CanSelectDamagesToExport: Boolean;
begin
  Result := '';
  Wait := ShowHourGlass;
  DamageIDCriteria := '';
  CanSelectDamagesToExport := DM.CanI(RightDamagesSelectiveExport);

  if CanSelectDamagesToExport then // limit export to selected damages
    SelectedDamageIDs := GetItemIDsForExportString(OpenDamages, 'damage_id')
  else                             // export all assigned damages
    SelectedDamageIDs := '';

  if CanSelectDamagesToExport and IsEmpty(SelectedDamageIDs) then // nothing selected to export
    Exit;

  if NotEmpty(SelectedDamageIDs) then
    DamageIDCriteria := 'and d.damage_id in (' + SelectedDamageIDs + ') ';

  SkipFields := TStringList.Create;
  try
    SkipFields.Add('due_date');
    SkipFields.Add('work_priority');

    Select := Format(SelectMyDamagesExport, [DM.EmpID, DamageTypePending, DamageIDCriteria]);
    DamagesToExport := DM.Engine.OpenQuery(Select);
    try
      SelectedDamageExportCount := DamagesToExport.RecordCount;
      ExportFileName := DM.CreateExportFileName('-My Damages', '.csv');
      SaveDelimToFile(DamagesToExport, ExportFileName, ',', True, nil, SkipFields);
      Result := ExportFileName;
    finally
      FreeAndNil(DamagesToExport);
    end;
  finally
    FreeAndNil(SkipFields);
  end;

  DM.AddEmployeeActivityEvent(ActivityTypeDamageRouteExport, IntToStr(SelectedDamageExportCount));
end;

procedure TMyTicketsForm.ExportButtonClick(Sender: TObject);
var
  ExportedFileNames: string;
begin
  DM.CleanExportFolder(-1);
  ExportedFileNames := ExportMyTickets + #13#10 + ExportMyDamages + #13#10 + ExportMyWorkOrders;

  if NotEmpty(ExportedFileNames) then
    MessageDlg('Export created these files: ' + #13#10 + ExportedFileNames, mtInformation, [mbOk], 0)
  else
    MessageDlg('Export did not create any files.', mtInformation, [mbOK], 0);
end;

function TMyTicketsForm.GetItemIDsForExportString(Dataset: TDataset; ItemFieldName: string): string;
var
  CurrentSelectedItem: Integer;
  Cursor: IInterface;
begin
  Result := '';
  Cursor := ShowHourGlass;
 // if ItemFieldName = 'damage_id' then     //QM-10 EB - Why does this say damages?
  if ItemFieldName = 'ticket_id' then
    CurrentSelectedItem := SelectedTicketID
  else if ItemFieldName = 'wo_id' then
    CurrentSelectedItem := SelectedWorkOrderID
  else
    CurrentSelectedItem := SelectedDamageID;
  Dataset.DisableControls;
  try
    Dataset.Filtered := False;
    Dataset.Filter := 'export = True';
    Dataset.Filtered := True;

    Result := GetDataSetFieldPlainString(Dataset, ItemFieldName, -1, ',');

    Dataset.Filtered := False;
  finally
    Dataset.Locate(ItemFieldName,CurrentSelectedItem,[]);
    Dataset.EnableControls;
  end;
end;

procedure TMyTicketsForm.MyWorkScrollboxMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
{This code allows the mousewheel to scroll the scrollbox. Adapted from online
 solutions, particularly Peter Below's.
 https://forums.embarcadero.com/message.jspa?messageID=133880}
var
  Msg: Cardinal;
  Code: Cardinal;
  i: Integer;
begin
    Handled := True;
    if (ssShift in Shift) then
      Msg := WM_HSCROLL
    else
      Msg := WM_VSCROLL;

    if WheelDelta > 0 then
      code := SB_LINEUP
    else
      code := SB_LINEDOWN;

    for i := 1 to Mouse.WheelScrollLines do
      MyWorkScrollbox.Perform(Msg, Code, 0);
    MyWorkScrollbox.Perform(Msg, SB_ENDSCROLL, 0);
end;

procedure TMyTicketsForm.ViewMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  MyWorkScrollboxMouseWheel(Sender, Shift, WheelDelta, MousePos, Handled);
end;

procedure TMyTicketsForm.ViewMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
{When DevEx is upgraded again, this logic should be placed in a new event
 handler triggered when group is expanded/collapsed if available vs
 checking that group row was clicked.}
begin
  inherited;
  Assert(Sender is TcxGridSite);
  ResetGridHeights(False);
end;

procedure TMyTicketsForm.ResetGridHeights(ExpandView: Boolean);
begin
  if OpenWorkOrders.Active then
    SetGridHeight(WorkOrdersGrid, OpenWorkOrders.RecordCount, ExpandView);
  if OpenTickets.Active then
    SetGridHeight(TicketsGrid, OpenTickets.RecordCount, ExpandView);
  if OpenDamages.Active then
    SetGridHeight(DamagesGrid, OpenDamages.RecordCount, ExpandView);
end;

function TMyTicketsForm.ExportMyWorkOrders: string;
const
  SelectMyWorkOrdersExport = 'select distinct wo_number "Ticket #", ' +
    'work_lat "Lat", work_long "Long", due_date, kind, ' +
    'TRIM(LEADING '' '' FROM (COALESCE(work_address_number, '''')+' +
    ''' ''+work_address_street)) "Name", ' +
    'CAST(due_date AS varchar(16)) "Name2", ' +
    'work_address_number "Address", work_address_street "Street", ' +
    'work_city "City", work_state "State" ' +
    'from work_order wo ' +
    'where wo.assigned_to_id = %d ' +
    'and wo.active = True ' +
    'and wo.closed = False ' +
    '%s ' + //Placeholder for 'and wo_id in ()' criteria
    'order by wo.kind desc, wo.due_date, wo.wo_number ';
var
  Wait: IInterface;
  ExportFileName: string;
  WorkOrderIDCriteria: string;
  SelectedWorkOrderIDs : string;
  WorkOrdersToExport: TDataSet;
  Select: string;
  SkipFields: TStringList;
  SelectedWorkOrderExportCount: Integer;
  CanSelectWorkOrdersToExport: Boolean;
begin
  Result := '';
  Wait := ShowHourGlass;
  WorkOrderIDCriteria := '';
  CanSelectWorkOrdersToExport := DM.CanI(RightWorkOrdersSelectiveExport);

  if CanSelectWorkOrdersToExport then
    SelectedWorkOrderIDs := GetItemIDsForExportString(OpenWorkOrders, 'wo_id')
  else
    SelectedWorkOrderIDs := '';

  if CanSelectWorkOrdersToExport and IsEmpty(SelectedWorkOrderIDs) then
    Exit;

  if NotEmpty(SelectedWorkOrderIDs) then
    WorkOrderIDCriteria := 'and wo_id in (' + SelectedWorkOrderIDs + ') ';

  SkipFields := TStringList.Create;
  try
    SkipFields.Add('due_date');
    SkipFields.Add('kind');

    Select := Format(SelectMyWorkOrdersExport, [DM.EmpID, WorkOrderIDCriteria]);
    WorkOrdersToExport := DM.Engine.OpenQuery(Select);
    try
      SelectedWorkOrderExportCount := WorkOrdersToExport.RecordCount;
      ExportFileName := DM.CreateExportFileName('-My Work Orders', '.csv');
      SaveDelimToFile(WorkOrdersToExport, ExportFileName, ',', True, nil, SkipFields);
      Result := ExportFileName;
    finally
      FreeAndNil(WorkOrdersToExport);
    end;
  finally
    FreeAndNil(SkipFields);
  end;

  DM.AddEmployeeActivityEvent(ActivityTypeWorkOrderRouteExport, IntToStr(SelectedWorkOrderExportCount));
end;

procedure TMyTicketsForm.SelectAllButtonClick(Sender: TObject);
begin
  inherited;
  CheckAllExportCheckboxes(True);
end;

procedure TMyTicketsForm.SelectNoneButtonClick(Sender: TObject);
begin
  inherited;
  CheckAllExportCheckboxes(False);
end;

procedure TMyTicketsForm.CheckAllExportCheckboxes(Checked: Boolean);
begin
  if ViewStyleCombo.ItemIndex = 0 then begin
    SetExportAll(OpenWorkOrders,WorkOrdersGridDetailsTableView, Checked);
    SetExportAll(OpenTickets, TicketsGridDetailsTableView, Checked);
    SetExportAll(OpenDamages, DamagesGridDetailsTableView, Checked);
  end
  else begin
    SetExportAll(OpenWorkOrders, WorkOrdersGridSummaryTableView, Checked);
    SetExportAll(OpenTickets, TicketsGridSummaryTableView, Checked);
    SetExportAll(OpenDamages, DamagesGridSummaryTableView, Checked);
  end;
end;

procedure TMyTicketsForm.SetExportAll(DataSet: TDataSet; AView: TcxCustomGridTableView; Checked: Boolean);
var
  Cursor: IInterface;
  CurrentFocusedRecord: Integer;
begin
  Cursor := ShowHourGlass;

  CurrentFocusedRecord := AView.DataController.FocusedRecordIndex;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do begin
      DataSet.Edit;
      DataSet.FieldByName('Export').AsBoolean := Checked;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.EnableControls;
    AView.DataController.FocusedRecordIndex := CurrentFocusedRecord;
  end;
end;

end.

