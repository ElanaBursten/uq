unit WorkOrderStatusActivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect, CheckLst, ExtCtrls, 
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit;

type
  TWorkOrderStatusActivityReportForm = class(TReportBaseForm)
    GroupBox1: TGroupBox;
    StatusDateGroup: TGroupBox;
    StatusDate: TOdRangeSelectFrame;
    TransmitDateGroup: TGroupBox;
    TransmitDate: TOdRangeSelectFrame;
    GroupBox2: TGroupBox;
    WorkOrdersClosedRadioGroup: TRadioGroup;
    CallCenterClientsBox: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    CallCenterList: TCheckListBox;
    ClientList: TCheckListBox;
    SelectAllClientsButton: TButton;
    ClearAllClientsButton: TButton;
    LimitManagerBox: TCheckBox;
    Label2: TLabel;
    ManagersComboBox: TComboBox;
    procedure CallCenterListClick(Sender: TObject);
    procedure SelectAllClientsButtonClick(Sender: TObject);
    procedure ClearAllClientsButtonClick(Sender: TObject);
    procedure LimitManagerBoxClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdExceptions, OdVclUtils, OdMiscUtils;

{$R *.dfm}

{ TWorkOrderStatusActivityReportForm }

procedure TWorkOrderStatusActivityReportForm.ValidateParams;
var
  CallCenters: string;
  CheckedItems: string;
begin
  inherited;
  if (StatusDate.FromDate = 0) and (TransmitDate.FromDate = 0) then
    raise EOdEntryRequired.Create('Please select at least one date range');

  SetReportID('WorkOrderStatusActivity');

  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters = '' then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select a call center.');
  end;
  SetParam('CallCenterCodes', CallCenters);

  CheckedItems := GetCheckedItemString(ClientList, True);
  if CheckedItems = '' then begin
    ClientList.SetFocus;
    raise EOdEntryRequired.Create('Please select the clients to report on.');
  end;

  if LimitManagerBox.Checked then
    RequireComboBox(ManagersComboBox, 'Please select a manager.');

  SetParamDate('StatusDateFrom', StatusDate.FromDate);
  SetParamDate('StatusDateTo', StatusDate.ToDate);
  SetParamInt('WorkOrderClosed', WorkOrdersClosedRadioGroup.ItemIndex);
  SetParamDate('TransmitDateFrom', TransmitDate.FromDate);
  SetParamDate('TransmitDateTo', TransmitDate.ToDate);
  SetParam('ClientIDList', CheckedItems);

  if LimitManagerBox.Checked then
    SetParamIntCombo('ManagerID', ManagersComboBox, True)
  else
    SetParamInt('ManagerID', -1);
end;

procedure TWorkOrderStatusActivityReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  StatusDate.NoDateRange;
  TransmitDate.NoDateRange;
  SetUpManagerList(ManagersComboBox);
  LimitManagerBox.Checked := False;
  ManagersComboBox.Enabled := False;
  WorkOrdersClosedRadioGroup.ItemIndex := 2;
end;

procedure TWorkOrderStatusActivityReportForm.CallCenterListClick(
  Sender: TObject);
var
  CallCenters: string;
begin
  inherited;
  CallCenters := GetCheckedItemCodesString(CallCenterList);;
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure TWorkOrderStatusActivityReportForm.SelectAllClientsButtonClick(
  Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, True);
end;

procedure TWorkOrderStatusActivityReportForm.ClearAllClientsButtonClick(
  Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, False);
end;

procedure TWorkOrderStatusActivityReportForm.LimitManagerBoxClick(
  Sender: TObject);
begin
  inherited;
  ManagersComboBox.Enabled := LimitManagerBox.Checked;
end;

end.
