inherited MyWorkDamagesFrame: TMyWorkDamagesFrame
  inherited SummaryPanel: TPanel
    Caption = 'Damages'
    inherited ExpandLabel: TLabel
      Height = 29
    end
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Damages'
    end
    inherited BaseGrid: TcxGrid
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = DamagesGridDetailsTableView
      end
      inherited BaseGridLevelSummary: TcxGridLevel
        GridView = DamagesGridSummaryTableView
      end
    end
  end
  object DamageSource: TDataSource
    DataSet = OpenDamages
    Left = 741
    Top = 8
  end
  object OpenDamages: TDBISAMQuery
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select damage_id, uq_damage_id, damage_date, damage.due_date, da' +
        'mage_type, t.ticket_number, profit_center, '
      
        '  utility_co_damaged, location, city, county, state, facility_ty' +
        'pe, facility_size, excavator_company,'
      
        '  coalesce(ref.sortby, 0) work_priority_id, coalesce(ref.descrip' +
        'tion, '#39'Normal'#39') work_priority'
      'from damage'
      '  left join reference ref on damage.work_priority_id= ref.ref_id'
      '  left join ticket t on damage.ticket_id = t.ticket_id'
      '  where investigator_id = :emp_id'
      '  and damage_type NOT in ('#39'APPROVED'#39')'
      '  and damage.active = 1'
      'order by work_priority, due_date')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 770
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object MyWorkViewRepository: TcxGridViewRepository
    Left = 680
    Top = 8
    object DamagesGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = DamageSource
      DataController.KeyFieldNames = 'damage_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      OptionsView.BandHeaders = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'DAMAGES'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object DmgDColDamagePriority: TcxGridDBBandedColumn
        Caption = 'Priority Sortby'
        DataBinding.FieldName = 'work_priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object DmgDColWorkPriorityDesc: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_description'
        PropertiesClassName = 'TcxTextEditProperties'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object DmgDColDamageType: TcxGridDBBandedColumn
        Caption = 'Damage Type'
        DataBinding.FieldName = 'damage_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object DmgDColDamageID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'damage_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object DmgDColUQDamageID: TcxGridDBBandedColumn
        Caption = 'UQ Damage #'
        DataBinding.FieldName = 'uq_damage_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 87
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object DmgDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Width = 93
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object DmgDColDamageDate: TcxGridDBBandedColumn
        Caption = 'Damage Date'
        DataBinding.FieldName = 'damage_date'
        Options.Editing = False
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object DmgDColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object DmgDColProfitCenter: TcxGridDBBandedColumn
        Caption = 'Profit Center'
        DataBinding.FieldName = 'profit_center'
        Options.Editing = False
        Options.Filtering = False
        Width = 95
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object DmgDColCompanyDamaged: TcxGridDBBandedColumn
        Caption = 'Company Damaged'
        DataBinding.FieldName = 'utility_co_damaged'
        Options.Editing = False
        Options.Filtering = False
        Width = 117
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object DmgDColFacilityType: TcxGridDBBandedColumn
        Caption = 'Facility Type'
        DataBinding.FieldName = 'facility_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 76
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object DmgDColFacilitySize: TcxGridDBBandedColumn
        Caption = 'Facility Size'
        DataBinding.FieldName = 'facility_size'
        Options.Editing = False
        Options.Filtering = False
        Width = 163
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object DmgDColExcavatorCompany: TcxGridDBBandedColumn
        Caption = 'Excavator Company'
        DataBinding.FieldName = 'excavator_company'
        Options.Editing = False
        Options.Filtering = False
        Width = 188
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object DmgDColLocation: TcxGridDBBandedColumn
        Caption = 'Location'
        DataBinding.FieldName = 'location'
        Options.Editing = False
        Options.Filtering = False
        Width = 181
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object DmgDColDetailsCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Options.Editing = False
        Options.Filtering = False
        Width = 195
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object DmgDColDetailsCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Options.Editing = False
        Options.Filtering = False
        Width = 193
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object DmgDColDetailsState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Options.Editing = False
        Options.Filtering = False
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object DmgDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = 0
        Properties.ValueUnchecked = 'False'
        Options.Filtering = False
        Options.Sorting = False
        Width = 86
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
    end
    object DamagesGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = DamageSource
      DataController.KeyFieldNames = 'damage_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'DAMAGES'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object DmgSColDamageType: TcxGridDBBandedColumn
        Caption = 'Damage Type'
        DataBinding.FieldName = 'damage_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object DmgSColUQDamageID: TcxGridDBBandedColumn
        Caption = 'UQ Damage #'
        DataBinding.FieldName = 'uq_damage_id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object DmgSColDamageID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'damage_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object DmgSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object DmgSColDamageDate: TcxGridDBBandedColumn
        Caption = 'Damage Date'
        DataBinding.FieldName = 'damage_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 76
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object DmgSColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 46
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object DmgSColExcavatorCompany: TcxGridDBBandedColumn
        Caption = 'Excavator Company'
        DataBinding.FieldName = 'excavator_company'
        Options.Editing = False
        Options.Filtering = False
        Width = 107
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object DmgSColLocation: TcxGridDBBandedColumn
        Caption = 'Location'
        DataBinding.FieldName = 'location'
        Options.Editing = False
        Options.Filtering = False
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object DmgSColCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'city'
        Options.Editing = False
        Options.Filtering = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object DmgSColCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'county'
        Options.Editing = False
        Options.Filtering = False
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object DmgSColState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'state'
        Options.Editing = False
        Options.Filtering = False
        Width = 30
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object DmgSColPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_description'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object DmgSColWorkPriority: TcxGridDBBandedColumn
        DataBinding.FieldName = 'work_priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object DmgSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = 0
        Properties.ValueUnchecked = 'False'
        Options.Filtering = False
        Options.Sorting = False
        Width = 38
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
    end
  end
end
