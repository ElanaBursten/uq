unit LocateStatusActivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls,
  OdRangeSelect, EmployeeSelectFrame, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, ComCtrls, dxCore, cxDateUtils;

type
  TLocateStatusActivityReportForm = class(TReportBaseForm)
    DateGroup: TGroupBox;
    LastReportLabel: TLabel;
    SinceTimeRadio: TRadioButton;
    DateRangeRadio: TRadioButton;
    LastReportTimeRadio: TRadioButton;
    RangeSelect: TOdRangeSelectFrame;
    DateTimeSelect: TcxDateEdit;
    DataGroup: TGroupBox;
    ManagerRadio: TRadioButton;
    OfficeRadio: TRadioButton;
    ManagersComboBox: TComboBox;
    OfficesComboBox: TComboBox;
    SortingGroup: TRadioGroup;
    Label2: TLabel;
    EmployeeStatusCombo: TComboBox;
    SpecificWorkerBox: TEmployeeSelect;
    LocatorRadio: TRadioButton;
    procedure SinceTimeEnter(Sender: TObject);
    procedure ManagersComboBoxEnter(Sender: TObject);
    procedure OfficesComboBoxEnter(Sender: TObject);
    procedure UpdateLastReportTime(IsoDateTime: string);
    procedure UpdateUQState;
    procedure SetLastModifiedDate;
    procedure RangeSelectEnter(Sender: TObject);
    procedure UpdateVisibility(Sender: TObject);
  private
    FFarFutureDate: TDateTime;
  public
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure Preview; override;
  end;

implementation

uses DateUtils, DMu, OdIsoDates, OdVclUtils, OdExceptions, QMConst, LocalEmployeeDMu;

{$R *.dfm}

{ TLocateStatusActivityReportForm }

procedure TLocateStatusActivityReportForm.InitReportControls;
begin
  inherited;
  FFarFutureDate := EncodeDate(2500, 1, 1);
  DateTimeSelect.Date := Now - 1;

  if Trim(DM.UQState.ActivityReportLastDateTime) <> '' then
    UpdateLastReportTime(DM.UQState.ActivityReportLastDateTime)
  else
    UpdateLastReportTime(IsoDateTimeToStr(Now - 1));

  SetUpManagerList(ManagersComboBox);
  SelectComboBoxItemFromObjects(ManagersComboBox, DM.UQState.ActivityReportLastManager);

  SpecificWorkerBox.Initialize(esAll);

  DM.OfficeList(OfficesComboBox.Items);
  SelectComboBoxItemFromObjects(OfficesComboBox, DM.UQState.ActivityReportLastOffice);

  SortingGroup.ItemIndex := 0;
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);

  ManagerRadio.Checked := True;

  UpdateVisibility(Self);
end;

procedure TLocateStatusActivityReportForm.ValidateParams;
var
  DateFrom, DateTo: TDateTime;
begin
  inherited;

  if SinceTimeRadio.Checked then begin
    if DateTimeSelect.Date = 0 then begin
      DateTimeSelect.SetFocus;
      raise EOdEntryRequired.Create('Please select a valid starting date');
    end;
    DateFrom := DateTimeSelect.Date;
    DateTo := FFarFutureDate;
  end else if DateRangeRadio.Checked then begin
    if (RangeSelect.FromDate = 0) or (RangeSelect.ToDate = 0) then begin
      RangeSelect.FromDateEdit.SetFocus;
      raise EOdEntryRequired.Create('Please select a valid date range');
    end;
    DateFrom := RangeSelect.FromDate;
    DateTo := RangeSelect.ToDate;
  end else if LastReportTimeRadio.Checked then begin
    try
      IsoStrToDateTime(DM.UQState.ActivityReportLastDateTime);
      DateFrom := IsoStrToDateTime(DM.UQState.ActivityReportLastDateTime);
      { TODO: Incrementing by 1 still causes some rounding issues to repeat
        items on subsequent reports.  To fix this, add a parameter to the stored
        procedure to do a non-inclusive comparison }
      DateFrom := IncMilliSecond(DateFrom, 1);
      DateTo := FFarFutureDate;
    except
      raise EOdEntryRequired.Create(DM.UQState.ActivityReportLastDateTime + ' is not a valid date/time');
    end
  end
  else
    raise EOdEntryRequired.Create('No report date range type selected');

  SetReportID('LocateStatusActivity');
  SetParamDate('DateFrom', DateFrom);
  SetParamDate('DateTo', DateTo);
  SetParamInt('SortBy', SortingGroup.ItemIndex);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));

  if OfficeRadio.Checked then begin
    RequireComboBox(OfficesComboBox, 'Please select an office.');
    SetParamIntCombo('Office', OfficesComboBox);
  end else begin
    SetParamInt('Office', -1);
  end;

  if ManagerRadio.Checked then begin
    RequireComboBox(ManagersComboBox, 'Please select a manager.');
    SetParamIntCombo('Manager', ManagersComboBox);
  end else if LocatorRadio.Checked then begin
    RequireComboBox(SpecificWorkerBox.EmployeeCombo, 'Please select a locator.');
    SetParamIntCombo('Manager', SpecificWorkerBox.EmployeeCombo);
    SetParamBoolean('SingleEmployee', True);
  end else
    SetParamInt('Manager', -1);
end;

procedure TLocateStatusActivityReportForm.RangeSelectEnter(Sender: TObject);
begin
  DateRangeRadio.Checked := True;
end;

procedure TLocateStatusActivityReportForm.SinceTimeEnter(Sender: TObject);
begin
  SinceTimeRadio.Checked := True;
end;

procedure TLocateStatusActivityReportForm.ManagersComboBoxEnter(Sender: TObject);
begin
  ManagerRadio.Checked := True;
end;

procedure TLocateStatusActivityReportForm.OfficesComboBoxEnter(Sender: TObject);
begin
  OfficeRadio.Checked := True;
end;

procedure TLocateStatusActivityReportForm.UpdateLastReportTime(IsoDateTime: string);
begin
  Assert(Trim(IsoDateTime) <> '');
  DM.UQState.ActivityReportLastDateTime := IsoDateTime;
  LastReportLabel.Caption := IsoStrToSystemStr(IsoDateTime);
end;

procedure TLocateStatusActivityReportForm.UpdateUQState;
begin
  if LastReportTimeRadio.Checked then
    SetLastModifiedDate
  else if OfficeRadio.Checked then begin
    if OfficesComboBox.ItemIndex = -1 then
      DM.UQState.ActivityReportLastOffice := 0
    else
      DM.UQState.ActivityReportLastOffice := GetComboObjectInteger(OfficesComboBox);
  end
  else if ManagerRadio.Checked then begin
    if ManagersComboBox.ItemIndex = -1 then
      DM.UQState.ActivityReportLastManager := 0
    else
      DM.UQState.ActivityReportLastManager := GetComboObjectInteger(ManagersComboBox);
  end;
end;

procedure TLocateStatusActivityReportForm.SetLastModifiedDate;
begin
  if FOutputParams.Values['MaxModifiedDate'] > DM.UQState.ActivityReportLastDateTime then
    DM.UQState.ActivityReportLastDateTime := FOutputParams.Values['MaxModifiedDate']; // parameter already in Iso Date/Time format
end;

procedure TLocateStatusActivityReportForm.Preview;
begin
  inherited;
  UpdateUQState;
end;

procedure TLocateStatusActivityReportForm.UpdateVisibility(
  Sender: TObject);
begin
  ManagersComboBox.Enabled := ManagerRadio.Checked;
  SpecificWorkerBox.Enabled := LocatorRadio.Checked;
  OfficesComboBox.Enabled := OfficeRadio.Checked;
end;

end.

