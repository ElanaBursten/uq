unit AdHocPayrollExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TAdHocPayrollExceptionReportForm = class(TReportBaseForm)
    Label4: TLabel;
    WorkDateRange: TOdRangeSelectFrame;
    ManagersComboBox: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
    FieldCombo: TComboBox;
    FilterTypeCombo: TComboBox;
    FilterValueEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FFieldNames, FDisplayNames: TStringList;
    procedure AddHourType(FieldName, DisplayName: string);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdExceptions, QMConst, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TAdHocPayrollExceptionReportForm }

procedure TAdHocPayrollExceptionReportForm.InitReportControls;
begin
  inherited;
  WorkDateRange.WeekBeginsSunday := True;
  WorkDateRange.SelectDateRange(rsLastWeek);
  SetUpManagerList(ManagersComboBox);

  FieldCombo.Items.Assign(FDisplayNames);
  FieldCombo.ItemIndex := 0;
  FilterTypeCombo.ItemIndex := 0;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

procedure TAdHocPayrollExceptionReportForm.ValidateParams;
var
  C: string;
begin
  inherited;

  SetReportID('PayrollExceptionAdHoc');
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamDate('StartDate', WorkDateRange.FromDate);
  SetParamDate('EndDate', WorkDateRange.ToDate-1);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));

  if StrToFloatDef(FilterValueEdit.Text, -1) = -1 then
    raise EOdEntryRequired.Create('You must enter a number for the filter value');

  C := FFieldNames[FieldCombo.ItemIndex] + FilterTypeCombo.Text + FilterValueEdit.Text;
  SetParam('Criteria', C);
end;

procedure TAdHocPayrollExceptionReportForm.AddHourType(FieldName,
  DisplayName: string);
begin
  FFieldNames.Add(FieldName);
  FDisplayNames.Add(DisplayName);
end;

procedure TAdHocPayrollExceptionReportForm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FFieldNames := TStringList.Create;
  FDisplayNames := TStringList.Create;
  for i := Low(PayrollHourFieldNameArray) to High(PayrollHourFieldNameArray) do
    AddHourType(PayrollHourFieldNameArray[i].FieldName, PayrollHourFieldNameArray[i].DisplayName);
  inherited;
end;

procedure TAdHocPayrollExceptionReportForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(FFieldNames);
  FreeAndNil(FDisplayNames);
end;

end.
