; Use Inno Setup 5.2.0+ and the /verysilent parameter to execute this without any prompts
[Setup]
AppCopyright=Copyright 2002-{#GetDateTimeString('yyyy', '#0', '#0')} by UtiliQuest, LLC
AppName=UtiliQuest Q Manager Packages
AppVerName=UtiliQuest Q Manager Packages
AppID=UtiliQuestQManagerPackagesD2007
Compression=lzma/max
SolidCompression=yes
DefaultDirName={pf}\UtiliQuest\Q Manager Packages
DisableDirPage=yes
AppPublisher=UtiliQuest, LLC
AppPublisherURL=http://www.utiliquest.com/
AppVersion={%VERSION|2.0.0.47}
AppMutex=UtiliQuestQManager
DisableProgramGroupPage=yes
OutputDir=..\build
OutputBaseFilename=QManagerPackages
PrivilegesRequired=none
CreateUninstallRegKey=no
Uninstallable=no

[Files]
; Borland Standard
Source: "..\lib\rtl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcl100.bpl";    DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclx100.bpl";   DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\dbrtl100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcldb100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclsmp100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclie100.bpl";  DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcliex100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vcljpg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
Source: "..\lib\vclimg100.bpl"; DestDir: "{sys}"; Flags: sharedfile
;DBISAM 4.34 Build 7 is the current version to support XE2/3
Source: "..\lib\dbisamr434rsdelphi2007.bpl"; DestDir: "{sys}"; Flags: sharedfile
; Developer Express (non-official, consolidated package)
Source: "..\lib\DevExD2007RT1224.bpl"; DestDir: "{sys}"; Flags: sharedfile
