unit LocalSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, DB, DBISAMTb, OdDBISAMUtils, 
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridLevel, cxGrid, cxNavigator, cxCustomData, cxFilter, cxData;

type
  TLocalSearchForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    GridPanel: TPanel;
    RecordsLabel: TLabel;
    SearchButton: TButton;
    CancelButton: TButton;
    CopyButton: TButton;
    ExportButton: TButton;
    SearchDS: TDataSource;
    SearchQuery: TDBISAMQuery;
    SearchGrid: TcxGrid;
    SearchGridView: TcxGridDBTableView;
    SearchGridLevel: TcxGridLevel;
    procedure SearchButtonClick(Sender: TObject);
    procedure SearchGridDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HeaderPanelEnter(Sender: TObject);
    procedure HeaderPanelExit(Sender: TObject);
  protected
    procedure SelectCurrentItem;
    function SelectedRowID: Variant;
    function GetIDFieldName: string; virtual;
    function GetWhereClause: string; virtual; abstract;
    function GetSelectSQL: string; virtual; abstract;
    procedure DefineColumns(Cols: TColumnList); virtual; abstract;
  public
    constructor Create(AOwner: TComponent); override;
    class function Execute(var SelectedID: Variant): Boolean; virtual;
  end;

implementation

uses
  OdCxXmlUtils, DMu, OdMiscUtils, OdVclUtils, OdCxUtils;

{$R *.dfm}

{ TLocalSearchForm }

function TLocalSearchForm.SelectedRowID: Variant;
begin
  Result := -1;
  if not SearchQuery.IsEmpty then
    Result := SearchQuery.FieldByName('emp_id').Value;
end;

procedure TLocalSearchForm.SearchButtonClick(Sender: TObject);
begin
  inherited;
  SearchGrid.BeginUpdate;
  try
    SearchQuery.Close;
    SearchQuery.SQL.Text := GetSelectSQL + sLineBreak + GetWhereClause;
    SearchQuery.Open;
    RecordsLabel.Caption := IntToStr(SearchQuery.RecordCount) + ' found';
    RecordsLabel.Visible := True;
    SortGridByFirstVisibleColumn(SearchGrid);
    FocusFirstGridRow(SearchGrid, True);
  finally
    SearchGrid.EndUpdate;
  end;
end;

constructor TLocalSearchForm.Create(AOwner: TComponent);
var
  Cols: TColumnList;
begin
  inherited;
  Cols := TColumnList.Create;
  try
    SearchQuery.SQL.Text := GetSelectSQL + ' where 0=1';
    SearchQuery.Open; // To get default columns
    DefineColumns(Cols);
    CreateGridColumnsFromDef(SearchGridView, SearchQuery, Cols);
    SetGridViewAutoWidth(SearchGridView, False);
  finally
    FreeAndNil(Cols);
  end;
end;

procedure TLocalSearchForm.SearchGridDblClick(Sender: TObject);
begin
  SelectCurrentItem;
end;

function TLocalSearchForm.GetIDFieldName: string;
var
  i: Integer;
begin
  Result := '<Unknown>';
  SearchQuery.FieldDefs.Update;
  for i := 0 to SearchQuery.FieldDefs.Count - 1 do begin
    if StrEndsWith('_id', SearchQuery.FieldDefs[i].Name) then begin
      Result := SearchQuery.FieldDefs[i].Name;
      Break;
    end;
  end;
end;

procedure TLocalSearchForm.SelectCurrentItem;
begin
  ModalResult := mrOk; // For now, since this is always shown modal...
end;

class function TLocalSearchForm.Execute(var SelectedID: Variant): Boolean;
var
  Form: TLocalSearchForm;
begin
  Result := False;
  SelectedID := -1;
  Form := Self.Create(nil);
  try
    if Form.ShowModal = mrOK then begin
      SelectedID := Form.SelectedRowID;
      Result := SelectedID <> -1;
    end;
  finally
    FreeAndNil(Form);
  end;
end;

procedure TLocalSearchForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) and IsChildOfParent(GridPanel, ActiveControl) then begin
    SelectCurrentItem;
    Key := 0;
  end;
end;

procedure TLocalSearchForm.HeaderPanelEnter(Sender: TObject);
begin
  SearchButton.Default := True;
end;

procedure TLocalSearchForm.HeaderPanelExit(Sender: TObject);
begin
  SearchButton.Default := False;
end;

end.
