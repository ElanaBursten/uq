object AboutForm: TAboutForm
  Left = 350
  Top = 210
  AlphaBlend = True
  AlphaBlendValue = 210
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 212
  ClientWidth = 403
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LogoImage: TImage
    Left = 1
    Top = 1
    Width = 400
    Height = 83
  end
  object AppLabel: TLabel
    Left = 45
    Top = 88
    Width = 313
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = '[Application Label]'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object VersionLabel: TLabel
    Left = 37
    Top = 120
    Width = 329
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'Version ___'
  end
  object HelpDeskContact: TLabel
    Left = 37
    Top = 141
    Width = 329
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'Help Desk Contact: _____'
  end
  object lblServer: TLabel
    Left = 71
    Top = 164
    Width = 262
    Height = 13
    AutoSize = False
    Caption = 'Server: '
  end
  object OkButton: TButton
    Left = 169
    Top = 183
    Width = 65
    Height = 25
    Cancel = True
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OkButtonClick
  end
end
