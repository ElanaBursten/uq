inherited DamageIDExceptionReportForm: TDamageIDExceptionReportForm
  Left = 651
  Top = 279
  Caption = 'DamageIDExceptionReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 91
    Height = 13
    Caption = 'Damage ID Range:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 21
    Top = 35
    Width = 28
    Height = 13
    Caption = 'Start:'
  end
  object Label3: TLabel [2]
    Left = 21
    Top = 75
    Width = 22
    Height = 13
    Caption = 'End:'
  end
  object Label4: TLabel [3]
    Left = 61
    Top = 96
    Width = 121
    Height = 49
    AutoSize = False
    Caption = 
      '(enter 0 as the ending value, to mean "through the latest damage' +
      '")'
    WordWrap = True
  end
  object StartDamageIDEdit: TEdit [4]
    Left = 61
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object EndDamageIDEdit: TEdit [5]
    Left = 61
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 1
  end
end
