inherited OnTimeCompletionReportForm: TOnTimeCompletionReportForm
  Left = 501
  Top = 301
  Caption = 'OnTimeCompletionReportForm'
  ClientHeight = 310
  ClientWidth = 535
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 0
    Top = 135
    Width = 92
    Height = 12
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  object Label3: TLabel [1]
    Left = 280
    Top = 32
    Width = 35
    Height = 13
    Caption = 'Clients:'
  end
  object Label1: TLabel [2]
    Left = 0
    Top = 110
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object DueDateLabel: TLabel [3]
    Left = 0
    Top = 8
    Width = 83
    Height = 13
    Caption = 'Due Date Range:'
    WordWrap = True
  end
  object ClientList: TCheckListBox [4]
    Left = 280
    Top = 48
    Width = 164
    Height = 257
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 4
  end
  object ManagersComboBox: TComboBox [5]
    Left = 57
    Top = 107
    Width = 198
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 2
  end
  object SelectAllButton: TButton [6]
    Left = 450
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 5
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [7]
    Left = 450
    Top = 79
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 6
    OnClick = ClearAllButtonClick
  end
  object CallCenterList: TCheckListBox [8]
    Left = 0
    Top = 151
    Width = 275
    Height = 154
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
    OnClick = CallCenterListClick
  end
  object LimitManagerBox: TCheckBox [9]
    Left = 75
    Top = 88
    Width = 121
    Height = 17
    Caption = 'Limit to one manager'
    TabOrder = 1
    OnClick = LimitManagerBoxClick
  end
  inline DueDateRange: TOdRangeSelectFrame [10]
    Left = 13
    Top = 21
    Width = 249
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesComboBox: TComboBox
      Width = 198
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 240
  end
end
