inherited MyWorkTicketsFrame: TMyWorkTicketsFrame
  Width = 966
  Height = 339
  inherited SummaryPanel: TPanel
    Caption = 'Tickets:'
    inherited ExpandLabel: TLabel
      Font.Charset = ANSI_CHARSET
    end
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Tickets:'
      DesignSize = (
        916
        41)
      inherited CountLabelDataSummary: TLabel
        Top = 11
      end
      inherited OpenLabelDataSummary: TLabel
        Top = 11
      end
      object lblFirstTask: TLabel
        Left = 296
        Top = 11
        Width = 135
        Height = 19
        Caption = 'Select First Task'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = 201
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object ClearButton: TButton
        Left = 817
        Top = 8
        Width = 97
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Clear Route Notes'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = ClearButtonClick
      end
      object ApplyButton: TButton
        Left = 712
        Top = 8
        Width = 102
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Apply Quick Close'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = ApplyButtonClick
      end
    end
    inherited BaseGrid: TcxGrid
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = TicketsGridDetailsTableView
      end
      inherited BaseGridLevelSummary: TcxGridLevel
        GridView = TicketsGridSummaryTableView
      end
    end
  end
  object TicketSource: TDataSource
    DataSet = OpenTickets
    Left = 551
    Top = 7
  end
  object OpenTickets: TDBISAMQuery
    AutoCalcFields = False
    Filter = 'kind like '#39'%EMERG%'#39
    FilterOptions = [foCaseInsensitive]
    BeforeOpen = OpenTicketsBeforeOpen
    AfterScroll = OpenTicketsAfterScroll
    OnCalcFields = OpenTicketsCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      '/* REPLACED AT RUNTIME */'
      
        'select ticket_id, ticket_number, kind, due_date, ticket_type, co' +
        'mpany,'
      
        '  priority, work_type, work_city, work_address_street, work_coun' +
        'ty,'
      '  work_description, legal_good_thru, map_page, ticket_format, '
      
        '  work_address_number, do_not_mark_before, work_date, LocalStrin' +
        'gKey, alert,'
      
        '  coalesce(ref.sortby, 0) work_priority, coalesce(ref.descriptio' +
        'n, '#39'Normal'#39') priority_disp,'
      '  if(task.est_start_date is null, False, True) first_task,'
      '  0 export'
      
        'from ticket inner join locate on ticket.ticket_id=locate.ticket_' +
        'id'
      'left join reference ref on ticket.work_priority_id=ref.ref_id'
      
        'left join task_schedule task on ticket.ticket_id = task.ticket_i' +
        'd'
      'where locate.locator_id=:emp_id'
      '  and locate.active'
      '  and NOT locate.closed'
      ' and ((do_not_mark_before is NULL)'
      '   or (do_not_mark_before <= :last_sync))'
      
        'group by ticket_id, ticket_number, kind, due_date, ticket_type, ' +
        'company,'
      
        '  priority, work_type, work_city, work_address_street, work_coun' +
        'ty,'
      '  legal_good_thru, map_page, ticket_format, '
      '  work_address_number, do_not_mark_before, work_priority'
      'order by work_priority, kind, due_date, ticket_number')
    Params = <
      item
        DataType = ftInteger
        Name = 'emp_id'
        Value = 206
      end
      item
        DataType = ftDateTime
        Name = 'last_sync'
        Value = 0d
      end>
    Left = 583
    Top = 7
    ParamData = <
      item
        DataType = ftInteger
        Name = 'emp_id'
        Value = 206
      end
      item
        DataType = ftDateTime
        Name = 'last_sync'
        Value = 0d
      end>
  end
  object TicketLocates: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    IndexName = 'ticket_id'
    MasterFields = 'ticket_id'
    TableName = 'locate'
    Left = 615
    Top = 7
  end
  object ClearRoutes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'update ticket'
      'set LocalStringKey = null')
    Params = <>
    Left = 656
    Top = 7
  end
  object SetRoute: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'update ticket'
      'set LocalStringKey = :Route'
      'where ticket_id = :TicketID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'Route'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
    Left = 688
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Route'
      end
      item
        DataType = ftUnknown
        Name = 'TicketID'
      end>
  end
  object CenterTicketSummary: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'center_ticket_summary'
    Left = 720
    Top = 7
  end
  object CenterTicketSummarySource: TDataSource
    DataSet = CenterTicketSummary
    Left = 752
    Top = 7
  end
  object MyWorkViewRepository: TcxGridViewRepository
    Left = 511
    Top = 8
    object TicketsGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = TicketSource
      DataController.KeyFieldNames = 'ticket_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      OptionsView.BandHeaders = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'TICKETS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object TktDColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 224
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object TktDColKind: TcxGridDBBandedColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object TktDColWorkloadDate: TcxGridDBBandedColumn
        Caption = 'Workload Date'
        DataBinding.FieldName = 'workload_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object TktDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 94
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object TktDColWorkDate: TcxGridDBBandedColumn
        Caption = 'Work Date'
        DataBinding.FieldName = 'work_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 122
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object TktDColTicketType: TcxGridDBBandedColumn
        Caption = 'Type'
        DataBinding.FieldName = 'ticket_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object TktDColCompany: TcxGridDBBandedColumn
        Caption = 'Company'
        DataBinding.FieldName = 'company'
        Options.Editing = False
        Options.Filtering = False
        Width = 124
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object TktDColPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Width = 79
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object TktDColWorkType: TcxGridDBBandedColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        Options.Editing = False
        Options.Filtering = False
        Width = 224
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object TktDColWorkCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Options.Editing = False
        Options.Filtering = False
        Width = 174
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object TktDColStreetNum: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 122
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 1
      end
      object TktDColStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 134
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 1
      end
      object TktDColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object TktDColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 124
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 1
      end
      object TktDColAlert: TcxGridDBBandedColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert2'
        Options.Editing = False
        Options.Filtering = False
        Styles.Content = SharedDevExStyleData.BoldLargeRedFontStyle
        Width = 20
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object TktDColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        Options.Editing = False
        Options.Filtering = False
        Width = 588
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 2
      end
      object TktDColLegalGoodThru: TcxGridDBBandedColumn
        Caption = 'Legal Thru'
        DataBinding.FieldName = 'legal_good_thru'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object TktDColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 1
      end
      object TktDColTicketID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object TktDColLocates: TcxGridDBBandedColumn
        Caption = 'Locates'
        DataBinding.FieldName = 'locates'
        Options.Editing = False
        Options.Filtering = False
        Options.Sorting = False
        Width = 160
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 2
      end
      object TktDColQuickClose: TcxGridDBBandedColumn
        Caption = 'Quick Close'
        DataBinding.FieldName = 'quick_close'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediatePost = True
        Options.Filtering = False
        Options.Sorting = False
        Width = 148
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 2
      end
      object TktDColRoute: TcxGridDBBandedColumn
        Caption = 'Route Note'
        DataBinding.FieldName = 'LocalStringKey'
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object TktDColWorkPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object TktDColWorkPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_disp'
        Visible = False
        Options.Filtering = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object TktDColFirstTask: TcxGridDBBandedColumn
        Caption = 'First Task'
        DataBinding.FieldName = 'first_task'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 82
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 2
      end
      object TktDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Options.Sorting = False
        Width = 68
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 2
      end
      object TktDColTicketStatus: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_status'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object TktDColTicketDist: TcxGridDBBandedColumn
        Caption = 'Dist (mi)'
        DataBinding.FieldName = 'tktDistance'
        Options.SortByDisplayText = isbtOff
        SortIndex = 1
        SortOrder = soAscending
        Width = 56
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object TktDColRouteOrder: TcxGridDBBandedColumn
        Caption = 'Route Order'
        DataBinding.FieldName = 'route_order'
        MinWidth = 24
        Options.Editing = False
        Options.Moving = False
        Styles.Content = SharedDevExStyleData.BoldLargeFontStyle
        Styles.Header = SharedDevExStyleData.BoldFontStyle
        Width = 103
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object TickDColTciketRisk: TcxGridDBBandedColumn
        Caption = 'Risk Score'
        DataBinding.FieldName = 'risk_score'
        Options.Editing = False
        Styles.Content = SharedDevExStyleData.BoldMaroonFont
        Styles.Header = SharedDevExStyleData.BoldFontStyle
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object TickDColExpDate: TcxGridDBBandedColumn
        Caption = 'Exp Date'
        DataBinding.FieldName = 'expiration_date'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object TickDColAltDueDate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'alt_due_date'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
    end
    object TicketsGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = TicketSource
      DataController.KeyFieldNames = 'ticket_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'TICKETS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object TktSColTicketID: TcxGridDBBandedColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object TktSColTicketNumber: TcxGridDBBandedColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object TktSColKind: TcxGridDBBandedColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object TktSColAlert: TcxGridDBBandedColumn
        Caption = 'Alert'
        DataBinding.FieldName = 'alert2'
        Options.Editing = False
        Options.Filtering = False
        Styles.Content = SharedDevExStyleData.BoldRedFontStyle
        Width = 45
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object TktSColWorkloadDate: TcxGridDBBandedColumn
        Caption = 'Workload Date'
        DataBinding.FieldName = 'workload_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 54
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object TktSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 54
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object TktSColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object TktSColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 106
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object TktSColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object TktSColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Description'
        DataBinding.FieldName = 'work_description'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object TktSColLegalGoodThru: TcxGridDBBandedColumn
        Caption = 'Legal Thru'
        DataBinding.FieldName = 'legal_good_thru'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object TktSColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object TktSColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object TktSColLocates: TcxGridDBBandedColumn
        Caption = 'Locates'
        DataBinding.FieldName = 'locates'
        Options.Editing = False
        Options.Filtering = False
        Options.Sorting = False
        Width = 89
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object TktSColRoute: TcxGridDBBandedColumn
        Caption = 'Route Note'
        DataBinding.FieldName = 'LocalStringKey'
        Options.Filtering = False
        Width = 74
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object TktSColWorkPriorityDescription: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority_disp'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object TktSColWorkPriority: TcxGridDBBandedColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'work_priority'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Options.Editing = False
        Options.Filtering = False
        Options.SortByDisplayText = isbtOff
        Width = 49
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object TktSColQuickClose: TcxGridDBBandedColumn
        Caption = 'Quick Close'
        DataBinding.FieldName = 'quick_close'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediatePost = True
        Options.Filtering = False
        Options.Sorting = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object TktSColFirstTask: TcxGridDBBandedColumn
        Caption = 'First Task'
        DataBinding.FieldName = 'first_task'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object TktSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Options.Filtering = False
        Options.Sorting = False
        Width = 42
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object TktSColTicketStatus: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_status'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object TktSColTicketDist: TcxGridDBBandedColumn
        Caption = 'Dist (mi)'
        DataBinding.FieldName = 'tktDistance'
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
      object TktSColRouteOrder: TcxGridDBBandedColumn
        Caption = 'Route Order'
        DataBinding.FieldName = 'route_order'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        MinWidth = 25
        Styles.Content = SharedDevExStyleData.BoldLargeFontStyle
        Styles.Header = SharedDevExStyleData.BoldFontStyle
        Position.BandIndex = 0
        Position.ColIndex = 22
        Position.RowIndex = 0
      end
      object TktSColTicketType: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ticket_type'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 23
        Position.RowIndex = 0
      end
      object TckSColRiskScore: TcxGridDBBandedColumn
        Caption = 'Risk Score'
        DataBinding.FieldName = 'risk_score'
        Options.Editing = False
        Styles.Content = SharedDevExStyleData.BoldMaroonFont
        Styles.Header = SharedDevExStyleData.BoldFontStyle
        Width = 106
        Position.BandIndex = 0
        Position.ColIndex = 24
        Position.RowIndex = 0
      end
      object TickSColExpDate: TcxGridDBBandedColumn
        Caption = 'Exp Date'
        DataBinding.FieldName = 'expiration_date'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 25
        Position.RowIndex = 0
      end
      object TickSColAltDueDate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'alt_due_date'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 26
        Position.RowIndex = 0
      end
    end
  end
end
