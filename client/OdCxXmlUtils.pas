unit OdCxXmlUtils;

interface

uses cxGridDBTableView, DBISAMTb, OdDBISAMUtils;

function CreateGridColumnsFromDef(View: TcxGridDBTableView; Table: TDBISAMDataSet; const Def: TColumnList): Integer;

implementation

uses SysUtils;

function CreateGridColumnsFromDef(View: TcxGridDBTableView; Table: TDBISAMDataSet; const Def: TColumnList): Integer;
var
  i: Integer;
  ColInfo: TColumnInfo;
  Column: TcxGridDBColumn;
begin
  View.DataController.Groups.ClearGrouping;
  View.ClearItems;
  Result := 0;
  View.OptionsView.ColumnAutoWidth := False;

  if not Table.Active then
    raise Exception.Create('The dataset must be active');
  View.DataController.CreateAllItems;

  for i := 0 to Def.ColumnCount - 1 do
  begin
    Def.GetColumnInfo(ColInfo, i);
    Column := View.GetColumnByFieldName(ColInfo.FieldName);
    Assert(Assigned(Column));
    Column.Width := ColInfo.Width;
    if ColInfo.Visible then
      Inc(Result, ColInfo.Width);
    Column.Caption := ColInfo.Caption;
    Column.Visible := ColInfo.Visible;
  end;
  View.DataController.KeyFieldNames := Def.GetPrimaryKeyFieldName;
end;

end.
