unit ClientInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, dbisamtb, ExtCtrls, DBCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxHyperLinkEdit, cxDBEdit;

type
  TfrmClientInfo = class(TForm)
    pnlTop: TPanel;
    dbClientRules: TDBMemo;
    dbMarkingConcerns: TDBMemo;
    dbTrainer: TDBText;
    dbFieldEngineer: TDBText;
    TraingLink: TcxDBHyperLinkEdit;
    PlatsLink: TcxDBHyperLinkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    dbPlatInfo: TDBMemo;
    Label7: TLabel;
    btnClose: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCloseClick(Sender: TObject);
  private
    { Private declarations }
      fClientID:integer;
    procedure GetClientData;
  public
    { Public declarations }
    constructor Create(AOwner:TComponent; ClientID:integer);reintroduce;
  end;

var
  frmClientInfo: TfrmClientInfo;

implementation

uses DMu;

{$R *.dfm}

{ TfrmClientInfo }

procedure TfrmClientInfo.btnCloseClick(Sender: TObject);
begin
  Close;
end;

constructor TfrmClientInfo.Create(AOwner: TComponent; ClientID: integer);
begin
  inherited Create(AOwner);
  fClientID := ClientID;
  GetClientData;
end;

procedure TfrmClientInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.qryClientInfo.close;
  Action := caFree;
end;

procedure TfrmClientInfo.GetClientData;
begin
  dm.qryClientInfo.ParamByName('ClientID').Value:=fClientID;
  dm.qryClientInfo.Open;
  self.Caption := dm.qryClientInfo.FieldByName('client_name').asString + ' Client Information';

end;

end.
