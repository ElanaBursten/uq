unit WorkOrderDetailLAM01;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Grids, DBGrids, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBEdit, StdCtrls, DBCtrls, Mask, ExtCtrls,
  DB, dbisamtb, WorkOrderDetailBase, Menus;

type
  TWOLAM01Frame = class(TWODetailBaseFrame)
    ClientMasterOrderNumLabel: TLabel;
    JobNumberLabel: TLabel;
    ServingTerminalLabel: TLabel;
    CircuitNumberLabel: TLabel;
    WorkTypeLabel: TLabel;
    F2CableLabel: TLabel;
    TerminalPortLabel: TLabel;
    F2PairLabel: TLabel;
    WorkDescriptionLabel: TLabel;
    DrivewayBoresLabel: TLabel;
    RoadwayBoresLabel: TLabel;
    WorkCrossLabel: TLabel;
    StatusLabel: TLabel;
    DiggingWithinStateROWLabel: TLabel;
    RelatedTicketsLabel: TLabel;
    SourceSentAttachment: TLabel;
    AssignedToLabel: TLabel;
    WorkDescription: TDBMemo;
    WorkCross: TDBEdit;
    DiggingWithinStateROW: TDBComboBox;
    DrivewayBores: TcxDBSpinEdit;
    RoadwayBores: TcxDBSpinEdit;
    RelatedTickets: TDBGrid;
    StatusCombo: TDBLookupComboBox;
    Tickets: TDBISAMQuery;
    TicketsDS: TDataSource;
    TicketPopupMenu: TPopupMenu;
    CopyTicket1: TMenuItem;
    LocatorWO_Ticket_DS: TDataSource;
    LocatorWO_Ticket: TDBISAMQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    BooleanField2: TBooleanField;
    StringField3: TStringField;
    StringField4: TStringField;
    DateTimeField1: TDateTimeField;
    BooleanField3: TBooleanField;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    JobNumber: TDBText;
    ClientMasterOrderNum: TDBText;
    ServingTerminal: TDBText;
    CircuitNumber: TDBText;
    WorkType: TDBText;
    F2Cable: TDBText;
    TerminalPort: TDBText;
    F2Pair: TDBText;
    AssignedTo: TDBText;
    dbTikNoLabel: TDBText;
    procedure FrameEnter(Sender: TObject);
    procedure StatusComboCloseUp(Sender: TObject);
    procedure RoadwayBoresPropertiesChange(Sender: TObject);
    procedure RoadwayBoresPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure CopyTicket1Click(Sender: TObject);
    procedure TicketPopupMenuPopup(Sender: TObject);
    procedure RelatedTicketsDblClick(Sender: TObject);
    procedure TicketsDSDataChange(Sender: TObject; Field: TField);
  private
    function GetTicketNum: string;
  protected
    procedure SetInheritedFrameEvents; override;
    procedure CheckForAndLoadReferenceCodes; override;
  public
    procedure EnableEditing(Enabled: Boolean); override;
    procedure Load(ID: Integer); override;
    function ValidateWorkOrderFields: Boolean; override;
    procedure SaveWorkOrderDetails; override;
    property SelectedTicketNum: string read GetTicketNum;
  end;


implementation

uses DMu, OdMiscUtils, OdDbUtils, Clipbrd, MainFU;

{$R *.dfm}

const
  NoTicketStr = 'No Ticket # to copy';

procedure TWOLAM01Frame.CopyTicket1Click(Sender: TObject);
begin
  inherited;
  if CopyTicket1.Caption <> NoTicketStr then
    Clipboard.AsText := SelectedTicketNum;
end;

procedure TWOLAM01Frame.EnableEditing(Enabled: Boolean);
begin
  inherited;
  RelatedTickets.Enabled := True;
  WorkDescription.Enabled := True;
  WorkDescription.ReadOnly := not Enabled;
end;

procedure TWOLAM01Frame.FrameEnter(Sender: TObject);
begin
  inherited;
  TEdit(WorkDescription).CharCase := ecUpperCase;
end;

function TWOLAM01Frame.GetTicketNum: string;
begin
  try
    Result := Tickets.FieldByName('ticket_number').asString;
  except
    Result := '';
  end;
end;

procedure TWOLAM01Frame.Load(ID: Integer);
begin
  inherited;
  CheckForAndLoadReferenceCodes;

  Tickets.Close;
  Tickets.ParamByName('wo_id').AsInteger := ID;
  Tickets.Open;

  if WorkOrderDS.DataSet.FieldByName('source_sent_attachment').AsString = 'Y' then
    SourceSentAttachment.Caption := '** Attachment Provided for Review **'
  else
    SourceSentAttachment.Caption := '';
end;


procedure TWOLAM01Frame.RelatedTicketsDblClick(Sender: TObject);
var
  WOID: integer;
begin
  inherited;
  //Elana - Need to fill out query and link CC...but should be able to open ticket from here
  //DM.GetTicketID(SelectedTicketNum, CallCenter, TransmitDateFrom, TransmitDateTo);
  //with MainForm.ActivateForm('Ticket Detail') as TTicketDetailForm do

  WOID := WorkOrderDS.DataSet.FieldByName('wo_id').AsInteger;
  if not tickets.fieldbyName('ticket_id').IsNull then  //QMANTWO-247 & QMANTWO-296
    MainForm.ShowTicketFromWO(Sender,tickets.fieldbyName('ticket_id').AsInteger, WOID); //QMANTWO-296
end;

procedure TWOLAM01Frame.RoadwayBoresPropertiesChange(Sender: TObject);
const
  BORETEXT = ' INCLUDE ROADWAY FOR BORES.';
begin
  inherited;
  if (RoadwayBores.Value > 0) and (Pos(BORETEXT, WorkDescription.Text) = 0) then begin
    //Add the text hint to WorkDescription
    WorkDescription.Text := WorkDescription.Text + BORETEXT
  end else if (RoadwayBores.Value = 0) and (Pos(BORETEXT, WorkDescription.Text) > 0) then begin
    //Attept to remove the text hint
    WorkDescription.Text := StringReplace(WorkDescription.Text, BORETEXT, '', [rfReplaceAll]);
  end;
end;

procedure TWOLAM01Frame.RoadwayBoresPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  if Error then
    ErrorText := 'Please enter a valid number of bores.';
end;

procedure TWOLAM01Frame.SaveWorkOrderDetails;
begin
  inherited;
  if StatusList.FieldByName('status').AsString <> StatusCombo.Text then
      StatusList.Locate('status', StatusCombo.Text, []);

  if StatusList.FieldByName('complete').AsBoolean then begin
    ValidateWorkOrderFields;
    WorkOrderDS.DataSet.FieldByName('closed').AsBoolean := True;
    WorkOrderDS.DataSet.FieldByName('closed_date').AsDateTime := Now;
  end;

  WorkOrderDS.DataSet.FieldByName('status_date').AsDateTime := Now;
  WorkOrderDS.DataSet.FieldByName('statused_how').AsString := FStatusedHow;
  WorkOrderDS.DataSet.FieldByName('statused_by_id').AsInteger := DM.EmpID;
  if WorkOrderDS.DataSet.FieldByName('driveway_bore_count').IsNull then
    WorkOrderDS.DataSet.FieldByName('driveway_bore_count').AsInteger := 0;
  if WorkOrderDS.DataSet.FieldByName('road_bore_count').IsNull then
    WorkOrderDS.DataSet.FieldByName('road_bore_count').AsInteger := 0;
  WorkOrderDS.DataSet.Post;
end;

procedure TWOLAM01Frame.StatusComboCloseUp(Sender: TObject);
begin
  inherited;
  if StatusCombo.Text <> '' then begin
    EditDataSet(WorkOrderDS.DataSet);
    WorkOrderDS.DataSet.FieldByName('status_date').AsDateTime := Now;
    WorkOrderDS.DataSet.FieldByName('statused_how').AsString := StatusedHow;
  end;
end;

procedure TWOLAM01Frame.TicketPopupMenuPopup(Sender: TObject);
begin
  inherited;
  if SelectedTicketNum <> '' then
    CopyTicket1.Caption := 'Copy Ticket #' + SelectedTicketNum
  else
    CopyTicket1.Caption := 'No Ticket available';
end;

procedure TWOLAM01Frame.TicketsDSDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if tickets.fieldbyName('ticket_id').IsNull then
  begin
    LocatorWO_Ticket.Close;
    exit;
  end;

  LocatorWO_Ticket.Close;
  LocatorWO_Ticket.ParamByName('ticket_id').AsInteger := tickets.fieldbyName('ticket_id').AsInteger;
  LocatorWO_Ticket.Open;
  if LocatorWO_Ticket.RecordCount = 0 then
  begin
    DM.UpdateTicketInCache(Tickets.FieldByName('ticket_id').AsInteger, 0);
    LocatorWO_Ticket.Close;
    LocatorWO_Ticket.ParamByName('ticket_id').AsInteger := tickets.fieldbyName('ticket_id').AsInteger;
    LocatorWO_Ticket.Open;
//    DM.GoToTicket(tickets.fieldbyName('ticket_id').AsInteger);
// pulls in ticket detail body only
  end;
end;

procedure TWOLAM01Frame.CheckForAndLoadReferenceCodes;
begin
  DM.GetRefCodeList('strow', DiggingWithinStateROW.Items);
end;

function TWOLAM01Frame.ValidateWorkOrderFields: Boolean;
const
  MsgReq = ' is required to save work order.';
begin
  Result := False;
  if IsEmpty(WorkOrderDS.DataSet.FieldByName('work_description').AsString) then
    RaiseErrorOnFrame(WorkDescriptionLabel.Caption + MsgReq, WorkDescription)
  else if IsEmpty(WorkOrderDS.DataSet.FieldByName('work_cross').AsString) then
    RaiseErrorOnFrame(WorkCrossLabel.Caption + MsgReq, WorkCross)
  else if IsEmpty(StatusCombo.Text) then
    RaiseErrorOnFrame(StatusLabel.Caption + MsgReq, StatusCombo)
  else if IsEmpty(DiggingWithinStateROW.Text) then
    RaiseErrorOnFrame(DiggingWithinStateROWLabel.Caption + MsgReq, DiggingWithinStateROW)
  else if WorkOrderDS.DataSet.FieldByName('road_bore_count').AsInteger < 0 then
    RaiseErrorOnFrame(RoadwayBoresLabel.Caption + MsgReq, RoadwayBores)
  else if WorkOrderDS.DataSet.FieldByName('driveway_bore_count').AsInteger < 0 then
    RaiseErrorOnFrame(DrivewayBoresLabel.Caption + MsgReq, DrivewayBores)
  else
    Result := True;
end;

procedure TWOLAM01Frame.SetInheritedFrameEvents;
begin
  inherited;
  StatusList.OnFilterRecord := StatusListFilterRecord;
end;

end.
