object WODetailBaseFrame: TWODetailBaseFrame
  Left = 0
  Top = 0
  Width = 721
  Height = 615
  HorzScrollBar.Range = 700
  HorzScrollBar.Smooth = True
  HorzScrollBar.Tracking = True
  VertScrollBar.Range = 550
  VertScrollBar.Smooth = True
  VertScrollBar.Tracking = True
  TabOrder = 0
  object DetailsPanel: TPanel
    Left = 0
    Top = 0
    Width = 721
    Height = 169
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object DueDateLabel: TLabel
      Left = 3
      Top = 17
      Width = 23
      Height = 13
      Caption = 'Due:'
      Transparent = True
    end
    object TransmitDateLabel: TLabel
      Left = 3
      Top = 32
      Width = 71
      Height = 13
      Caption = 'Transmit Date:'
      Transparent = True
    end
    object TransmitDate: TDBText
      Left = 94
      Top = 32
      Width = 78
      Height = 13
      AutoSize = True
      DataField = 'transmit_date'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object DueDate: TDBText
      Left = 94
      Top = 17
      Width = 49
      Height = 13
      AutoSize = True
      DataField = 'due_date'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object ClientCode: TDBText
      Left = 504
      Top = 34
      Width = 60
      Height = 13
      AutoSize = True
      DataField = 'oc_code'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object ClientCodeLabel: TLabel
      Left = 399
      Top = 34
      Width = 59
      Height = 13
      Caption = 'Client Code:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object ClientNameLabel: TLabel
      Left = 399
      Top = 51
      Width = 61
      Height = 13
      Caption = 'Client Name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object ClientName: TDBText
      Left = 504
      Top = 51
      Width = 64
      Height = 13
      AutoSize = True
      DataField = 'client_name'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object ClientWONumberLabel: TLabel
      Left = 399
      Top = 68
      Width = 101
      Height = 13
      Caption = 'Client Work Order #:'
      Transparent = True
    end
    object ClientWONumber: TDBText
      Left = 504
      Top = 68
      Width = 95
      Height = 13
      AutoSize = True
      DataField = 'client_wo_number'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object ClientOrderNumLabel: TLabel
      Left = 399
      Top = 0
      Width = 73
      Height = 13
      Caption = 'Client Order #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object ClientOrderNum: TDBText
      Left = 504
      Top = 0
      Width = 89
      Height = 13
      AutoSize = True
      DataField = 'client_order_num'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object WorkLong: TDBText
      Left = 172
      Top = 48
      Width = 57
      Height = 13
      AutoSize = True
      DataField = 'work_long'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object WorkLat: TDBText
      Left = 94
      Top = 48
      Width = 48
      Height = 13
      AutoSize = True
      DataField = 'work_lat'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object TerminalGPSLabel: TLabel
      Left = 3
      Top = 48
      Width = 66
      Height = 13
      Caption = 'Terminal GPS:'
      Transparent = True
    end
    object lblWO_ID: TLabel
      Left = 588
      Top = 17
      Width = 36
      Height = 13
      Caption = 'WO ID:'
    end
    object WorkOrderID: TDBText
      Left = 631
      Top = 17
      Width = 75
      Height = 13
      AutoSize = True
      DataField = 'wo_id'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object CustomerPanel: TPanel
      Left = 0
      Top = 96
      Width = 721
      Height = 73
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object WorkAddressLabel: TLabel
        Left = 3
        Top = 13
        Width = 71
        Height = 13
        Caption = 'Work Address:'
        Transparent = True
      end
      object WorkAddressNumber: TDBText
        Left = 91
        Top = 13
        Width = 45
        Height = 13
        DataField = 'work_address_number'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object City: TDBText
        Left = 91
        Top = 29
        Width = 115
        Height = 13
        DataField = 'work_city'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object WorkAddressStreet: TDBText
        Left = 161
        Top = 13
        Width = 218
        Height = 13
        DataField = 'work_address_street'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object State: TDBText
        Left = 214
        Top = 29
        Width = 87
        Height = 13
        DataField = 'work_state'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object Zip: TDBText
        Left = 305
        Top = 29
        Width = 17
        Height = 13
        AutoSize = True
        DataField = 'work_zip'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
      object CallerNameLabel: TLabel
        Left = 3
        Top = -3
        Width = 80
        Height = 13
        Caption = 'Customer Name:'
        Transparent = True
      end
      object CallerName: TDBText
        Left = 91
        Top = -3
        Width = 64
        Height = 13
        AutoSize = True
        DataField = 'caller_name'
        DataSource = WorkOrderDS
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        OnDblClick = ItemDblClick
      end
    end
  end
  object FeedDetailsPanel: TPanel
    Left = 0
    Top = 169
    Width = 721
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object MapPageLabel: TLabel
      Left = 482
      Top = 10
      Width = 24
      Height = 13
      Caption = 'Map:'
      Transparent = True
    end
    object MapPage: TDBText
      Left = 512
      Top = 10
      Width = 52
      Height = 13
      AutoSize = True
      DataField = 'map_page'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object MapRef: TDBText
      Left = 512
      Top = 27
      Width = 43
      Height = 13
      AutoSize = True
      DataField = 'map_ref'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object MapPageGrid: TLabel
      Left = 484
      Top = 27
      Width = 23
      Height = 13
      Caption = 'Grid:'
      Transparent = True
    end
  end
  object EditingPanel: TPanel
    Left = 0
    Top = 249
    Width = 721
    Height = 326
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 575
    Width = 721
    Height = 40
    Align = alBottom
    BevelEdges = []
    BevelOuter = bvNone
    TabOrder = 3
    object ClosedDateLabel: TLabel
      Left = 329
      Top = 13
      Width = 65
      Height = 13
      Caption = 'Closed Date: '
      Transparent = True
    end
    object ClosedDate: TDBText
      Left = 400
      Top = 13
      Width = 143
      Height = 13
      DataField = 'closed_date'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      OnDblClick = ItemDblClick
    end
    object PlatAddinButton: TButton
      Left = 512
      Top = 7
      Width = 130
      Height = 25
      Caption = '(Addin Button)'
      TabOrder = 0
      Visible = False
    end
  end
  object StatusList: TDBISAMQuery
    Filtered = True
    OnFilterRecord = StatusListFilterRecord
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from statuslist'
      'where status not in('#39'-N'#39', '#39'-P'#39')'
      'and active'
      'order by status')
    Params = <>
    ReadOnly = True
    Left = 46
    Top = 376
  end
  object StatusSource: TDataSource
    AutoEdit = False
    Left = 13
    Top = 376
  end
  object WorkOrderDS: TDataSource
    DataSet = DM.WorkOrder
    Left = 86
    Top = 376
  end
end
