unit EmployeeSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls;

type
  TEmployeeSearchCriteria = class(TSearchCriteria)
    FirstName: TEdit;
    LastName: TEdit;
    ShortName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EmployeeNumber: TEdit;
    CheckBox1: TCheckBox;
  public
    function GetXmlString: string; override;
    procedure DefineColumns; override;
//    function SelectManager();
  end;

implementation

{$R *.dfm}

{ TEmployeeSearchCriteria }

procedure TEmployeeSearchCriteria.DefineColumns;
begin
  inherited;
  // 'employee'
  // Nothing yet
end;

function TEmployeeSearchCriteria.GetXmlString: string;
begin
  Result := '';
  // NOT IN USE YET, it will be something like:
  // DM.CallingServiceName('EmployeeSearch');
  // DM.Engine.Service.EmployeeSearch(DM.SecurityInfo, ... )
end;

end.
