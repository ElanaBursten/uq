inherited NewTicketForm: TNewTicketForm
  Left = 280
  Top = 198
  Caption = 'New Ticket'
  ClientHeight = 490
  ClientWidth = 656
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DetailsPanel: TPanel
    Left = 0
    Top = 33
    Width = 656
    Height = 457
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label2: TLabel
      Left = 398
      Top = 150
      Width = 64
      Height = 13
      Caption = 'Cross Street:'
      Transparent = True
    end
    object Label3: TLabel
      Left = 7
      Top = 256
      Width = 56
      Height = 13
      Caption = 'Contractor:'
      Transparent = True
    end
    object Label4: TLabel
      Left = 7
      Top = 229
      Width = 76
      Height = 13
      Caption = 'Work Done For:'
      Transparent = True
    end
    object Label7: TLabel
      Left = 420
      Top = 256
      Width = 42
      Height = 13
      Caption = 'Contact:'
      Transparent = True
    end
    object Address1Label: TLabel
      Left = 7
      Top = 150
      Width = 71
      Height = 13
      Caption = 'Work Address:'
      Transparent = True
    end
    object Label10: TLabel
      Left = 7
      Top = 35
      Width = 85
      Height = 13
      Caption = 'Work to be Done:'
      Transparent = True
    end
    object Label26: TLabel
      Left = 391
      Top = 229
      Width = 68
      Height = 13
      Caption = 'Call Received:'
      Transparent = True
    end
    object Label34: TLabel
      Left = 7
      Top = 336
      Width = 45
      Height = 13
      Caption = 'Remarks:'
      Transparent = True
    end
    object Label123: TLabel
      Left = 402
      Top = 6
      Width = 57
      Height = 13
      Caption = 'Area Name:'
      Transparent = True
      Visible = False
    end
    object Label14: TLabel
      Left = 411
      Top = 177
      Width = 51
      Height = 13
      Caption = 'Map Page:'
      Transparent = True
    end
    object Label1: TLabel
      Left = 7
      Top = 9
      Width = 59
      Height = 13
      Caption = 'Ticket Type:'
    end
    object Label5: TLabel
      Left = 7
      Top = 59
      Width = 85
      Height = 13
      Caption = 'Work Description:'
      Transparent = True
    end
    object CallCenterLabel: TLabel
      Left = 7
      Top = 283
      Width = 57
      Height = 13
      Caption = 'Call Center:'
      Transparent = True
    end
    object Label6: TLabel
      Left = 88
      Top = 169
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label9: TLabel
      Left = 88
      Top = 146
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label11: TLabel
      Left = 88
      Top = 4
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label12: TLabel
      Left = 88
      Top = 31
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label13: TLabel
      Left = 461
      Top = 224
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label15: TLabel
      Left = 89
      Top = 281
      Width = 11
      Height = 12
      AutoSize = False
      Caption = '*'
      Transparent = True
    end
    object Label16: TLabel
      Left = 7
      Top = 312
      Width = 31
      Height = 13
      Caption = 'Client:'
    end
    object Label17: TLabel
      Left = 409
      Top = 312
      Width = 50
      Height = 13
      Caption = 'Assign To:'
    end
    object Address2Label: TLabel
      Left = 7
      Top = 174
      Width = 84
      Height = 13
      Caption = 'City, ST, County:'
      Transparent = True
    end
    object lblLatitude: TLabel
      Left = 46
      Top = 202
      Width = 43
      Height = 13
      Caption = 'Latitude:'
      Transparent = True
    end
    object lblLongitude: TLabel
      Left = 187
      Top = 202
      Width = 51
      Height = 13
      Caption = 'Longitude:'
      Transparent = True
    end
    object Label8: TLabel
      Left = 416
      Top = 205
      Width = 45
      Height = 13
      Caption = 'Job Num:'
    end
    object Label18: TLabel
      Left = 395
      Top = 282
      Width = 64
      Height = 13
      Caption = 'Caller phone:'
      Transparent = True
    end
    object WorkDescription: TDBMemo
      Left = 96
      Top = 56
      Width = 550
      Height = 84
      DataField = 'work_description'
      DataSource = TicketDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
    end
    object WorkRemarks: TDBMemo
      Left = 96
      Top = 336
      Width = 553
      Height = 59
      DataField = 'work_remarks'
      DataSource = TicketDS
      ScrollBars = ssVertical
      TabOrder = 21
    end
    object Company: TDBEdit
      Left = 96
      Top = 226
      Width = 273
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'company'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 13
    end
    object Address: TDBEdit
      Left = 160
      Top = 147
      Width = 209
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_address_street'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 4
    end
    object ConName: TDBEdit
      Left = 96
      Top = 254
      Width = 273
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'con_name'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 15
    end
    object CallerContact: TDBEdit
      Left = 468
      Top = 254
      Width = 177
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'caller_contact'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 16
    end
    object MapPage: TDBEdit
      Left = 468
      Top = 172
      Width = 178
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'map_page'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 9
    end
    object WorkCross: TDBEdit
      Left = 468
      Top = 147
      Width = 178
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_cross'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 8
    end
    object City: TDBEdit
      Left = 96
      Top = 171
      Width = 121
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_city'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 5
    end
    object County: TDBEdit
      Left = 280
      Top = 171
      Width = 89
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_county'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 7
    end
    object WorkAddressNumber: TDBEdit
      Left = 96
      Top = 147
      Width = 61
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_address_number'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 3
    end
    object WorkType: TDBEdit
      Left = 96
      Top = 31
      Width = 550
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_type'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 1
    end
    object AreaName: TDBEdit
      Left = 468
      Top = 5
      Width = 178
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'route_area_name'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 18
      Visible = False
    end
    object State: TDBComboBox
      Left = 220
      Top = 171
      Width = 57
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      DataField = 'work_state'
      DataSource = TicketDS
      DropDownCount = 15
      ItemHeight = 13
      TabOrder = 6
    end
    object TicketType: TDBComboBox
      Left = 96
      Top = 5
      Width = 241
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      DataField = 'ticket_type'
      DataSource = TicketDS
      DropDownCount = 15
      ItemHeight = 13
      TabOrder = 0
    end
    object ClientCombo: TComboBox
      Left = 96
      Top = 309
      Width = 274
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      Enabled = False
      ItemHeight = 13
      TabOrder = 19
    end
    object AssignedToCombo: TComboBox
      Left = 468
      Top = 309
      Width = 179
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      ItemHeight = 13
      TabOrder = 20
    end
    object CallDate: TcxDBDateEdit
      Left = 468
      Top = 226
      DataBinding.DataField = 'call_date'
      DataBinding.DataSource = TicketDS
      Properties.ButtonGlyph.Data = {
        82030000424D820300000000000076000000280000000F0000000D0000000100
        2000000000000C03000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
        8000808080008080800080808000808080008080800080808000808080008080
        800080808000808080008080800080808000808000008080000080808000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
        0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
        0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
        0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
        8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
        00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
        0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
        000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
        8000808080008080800080808000808080008080800080808000808080008080
        800080808000808080008080800080808000808000008080000080808000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
        0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
        0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00808080008080000080800000808080008080800080808000808080008080
        8000808080008080800080808000808080008080800080808000808080008080
        800080800000}
      Properties.DateButtons = [btnNow]
      Properties.Kind = ckDateTime
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 14
      Width = 177
    end
    object CallCenter: TComboBox
      Left = 96
      Top = 282
      Width = 273
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 17
      OnChange = CallCenterChange
    end
    object Latitude: TDBEdit
      Left = 96
      Top = 199
      Width = 85
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_lat'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 11
      OnExit = LatitudeExit
    end
    object Longitude: TDBEdit
      Left = 240
      Top = 199
      Width = 85
      Height = 21
      AutoSize = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_long'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 12
      OnExit = LongitudeExit
    end
    object btnGpsValues: TBitBtn
      Left = 328
      Top = 200
      Width = 50
      Height = 20
      Hint = 'Adds GPS currect value to fields'
      Caption = 'Use GPS'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 22
      OnClick = btnGpsValuesClick
    end
    object edEdtWoNum: TDBEdit
      Left = 467
      Top = 199
      Width = 178
      Height = 21
      TabStop = False
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'wo_number'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 10
    end
    object CALLERPHONE: TDBEdit
      Left = 468
      Top = 282
      Width = 178
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'caller_phone'
      DataSource = TicketDS
      ParentCtl3D = False
      TabOrder = 23
    end
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 0
    Width = 656
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object NewTicket: TButton
      Left = 4
      Top = 4
      Width = 80
      Height = 25
      Action = NewAction
      TabOrder = 0
    end
    object SaveButton: TButton
      Left = 89
      Top = 4
      Width = 80
      Height = 25
      Action = SaveAction
      TabOrder = 1
    end
    object Button1: TButton
      Left = 174
      Top = 4
      Width = 80
      Height = 25
      Action = CancelAction
      TabOrder = 2
      Visible = False
    end
  end
  object LocateDS: TDataSource
    Left = 445
    Top = 120
  end
  object TicketDS: TDataSource
    DataSet = Ticket
    Left = 380
    Top = 96
  end
  object Ticket: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from ticket where 1=0')
    Params = <>
    Left = 404
    Top = 120
  end
  object TicketActions: TActionList
    OnUpdate = TicketActionsUpdate
    Left = 264
    Top = 2
    object SaveAction: TAction
      Caption = 'Save'
      OnExecute = SaveTicket
    end
    object NewAction: TAction
      Caption = 'New'
      OnExecute = NewActionExecute
    end
    object CancelAction: TAction
      Caption = 'Cancel'
    end
  end
end
