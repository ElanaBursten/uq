inherited WorkOrderStatusActivityReportForm: TWorkOrderStatusActivityReportForm
  Left = 361
  Top = 368
  Caption = 'WorkOrderStatusActivityReportForm'
  ClientHeight = 449
  ClientWidth = 603
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox [0]
    Left = 0
    Top = 8
    Width = 585
    Height = 105
    Caption = 'Date Filters'
    TabOrder = 0
    object StatusDateGroup: TGroupBox
      Left = 8
      Top = 17
      Width = 279
      Height = 78
      Caption = 'Status Date Range '
      TabOrder = 0
      inline StatusDate: TOdRangeSelectFrame
        Left = 2
        Top = 13
        Width = 249
        Height = 57
        TabOrder = 0
      end
    end
    object TransmitDateGroup: TGroupBox
      Left = 298
      Top = 17
      Width = 279
      Height = 78
      Caption = 'Transmit Date Range '
      TabOrder = 1
      inline TransmitDate: TOdRangeSelectFrame
        Left = 3
        Top = 14
        Width = 248
        Height = 56
        TabOrder = 0
      end
    end
  end
  object GroupBox2: TGroupBox [1]
    Left = 0
    Top = 116
    Width = 585
    Height = 333
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'Data Filters'
    TabOrder = 1
    DesignSize = (
      585
      333)
    object Label2: TLabel
      Left = 19
      Top = 44
      Width = 46
      Height = 13
      Caption = 'Manager:'
    end
    object WorkOrdersClosedRadioGroup: TRadioGroup
      Left = 320
      Top = 22
      Width = 257
      Height = 43
      Caption = 'Work Orders'
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'Open'
        'Closed'
        'All')
      TabOrder = 0
    end
    object CallCenterClientsBox: TGroupBox
      Left = 8
      Top = 71
      Width = 569
      Height = 255
      Anchors = [akLeft, akTop, akBottom]
      Caption = ' Limit To Specific Call Centers / Clients '
      ParentShowHint = False
      ShowHint = False
      TabOrder = 1
      DesignSize = (
        569
        255)
      object Label1: TLabel
        Left = 10
        Top = 17
        Width = 104
        Height = 13
        AutoSize = False
        Caption = 'Call Centers:'
        WordWrap = True
      end
      object Label4: TLabel
        Left = 290
        Top = 17
        Width = 50
        Height = 13
        Caption = 'Clients:'
      end
      object CallCenterList: TCheckListBox
        Left = 10
        Top = 32
        Width = 274
        Height = 220
        Anchors = [akLeft, akTop, akBottom]
        ItemHeight = 13
        TabOrder = 0
        OnClick = CallCenterListClick
      end
      object ClientList: TCheckListBox
        Left = 290
        Top = 32
        Width = 200
        Height = 220
        Anchors = [akLeft, akTop, akBottom]
        ItemHeight = 13
        TabOrder = 1
      end
      object SelectAllClientsButton: TButton
        Left = 495
        Top = 32
        Width = 64
        Height = 25
        Caption = 'Select All'
        TabOrder = 2
        OnClick = SelectAllClientsButtonClick
      end
      object ClearAllClientsButton: TButton
        Left = 495
        Top = 64
        Width = 64
        Height = 25
        Caption = 'Clear All'
        TabOrder = 3
        OnClick = ClearAllClientsButtonClick
      end
    end
    object LimitManagerBox: TCheckBox
      Left = 75
      Top = 16
      Width = 121
      Height = 17
      Caption = 'Limit to one manager'
      TabOrder = 2
      OnClick = LimitManagerBoxClick
    end
    object ManagersComboBox: TComboBox
      Left = 75
      Top = 40
      Width = 206
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 3
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 0
    Top = 318
  end
end
