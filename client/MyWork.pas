unit MyWork;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, DBCtrls, ExtCtrls,
  MyWorkDamages, BaseExpandableDataFrame, QMConst, BaseExpandableGridFrame,
  MyWorkTickets, MyWorkWOs, DB, cxGraphics, cxLookAndFeels, MyWorkWaze,
  cxLookAndFeelPainters, Menus, cxButtons, cxControls, cxContainer,
  cxEdit, cxCheckBox, ImgList, Mask;

type
  TMyWorkForm = class(TEmbeddableForm)
    HeaderPanel: TPanel;
    ViewLabel: TLabel;
    ViewStyleCombo: TComboBox;
    RefreshButton: TButton;
    DueTomorrowCount: TDBEdit;
    ExportButton: TButton;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    MyWorkWOsFrame: TMyWorkWOsFrame;
    MyWorkTicketsFrame: TMyWorkTicketsFrame;
    MyWorkDamagesFrame: TMyWorkDamagesFrame;
    btnWaze: TcxButton;   //QMANTWO-646
    cxImageList1: TcxImageList;
    btnTicketMap: TButton;
    EmergenciesCheckBox: TcxCheckBox;  //QMANTWO-646
    procedure FormShow(Sender: TObject);
    procedure WorkOrderFrameClick(Sender: TObject);
    procedure TicketFrameClick(Sender: TObject);
    procedure DamageFrameClick(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
    procedure ViewStyleComboChange(Sender: TObject);
    procedure ExportButtonClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MyWorkWOsFrameOpenWorkOrdersCalcFields(DataSet: TDataSet);
    procedure MyWorkWOsFrameOpenWorkOrdersBeforeOpen(DataSet: TDataSet);
    procedure MyWorkTicketsFrameOpenTicketsCalcFields(DataSet: TDataSet);
    procedure MyWorkTicketsFrameOpenTicketsAfterScroll(DataSet: TDataSet);
    procedure MyWorkTicketsFrameOpenTicketsBeforeOpen(DataSet: TDataSet);
    procedure btnWazeClick(Sender: TObject);  //QMANTWO-646
    procedure MyWorkTicketsFrameEnter(Sender: TObject);
    procedure MyWorkWOsFrameEnter(Sender: TObject);
    procedure MyWorkTicketsFrameTicketSourceDataChange(Sender: TObject;
      Field: TField);
    procedure MyWorkWOsFrameWorkOrdersSourceDataChange(Sender: TObject;
      Field: TField);
    procedure btnTicketMapClick(Sender: TObject);
    procedure EmergenciesCheckBoxClick(Sender: TObject);

  private
    frmWaze: TfrmWaze;
    FLastRefresh: TDateTime;
    FCurrentExpandedFrame: TExpandableGridFrameBase;
    activeEntity :TentityType;
    ActiveDB : string;
    IsUserNav : Boolean;
    LocationToUse : string;
    procedure SetAllCheckboxesForExport(Checked: Boolean);
    procedure ProcessLocationForWazeQTip;//QMANTWO-515
    procedure ProcessLocationForWazeQR;
    procedure WhichWaze;

  public
    CanUseQR : boolean; //QMANTWO-646
    QRtoUse  : string;  //QMANTWO-646
//    procedure TicketMap;  //qm-744    sr
    function IsWazable: boolean; //QMANTWO-646
    procedure ActivatingNow; override;
    procedure LeavingNow; override;  //QM-494 First Task Reminder
  end;

const
  LATLNG = 'LL';
  STREET = 'STR';

var
  MyWorkForm: TMyWorkForm;

implementation

uses DMu, OdMiscUtils, //QMANTWO-515
     LocalPermissionsDMu;//, StreetMapShell;

{$R *.dfm}

procedure TMyWorkForm.FormCreate(Sender: TObject);
begin
  inherited;
  btnTicketMap.Visible:=  dm.AllowTktStrMap; //QMANTWO-763  qm-646
  ViewStyleCombo.Items.Add('Details');
  ViewStyleCombo.Items.Add('Summary');
  ViewStyleCombo.ItemIndex := 0;
  ViewStyleComboChange(ViewStyleCombo);
  FCurrentExpandedFrame := nil;

  if StrContainsText('QACluster', DM.UQState.ServerURL) then
     ActiveDB:='qa'
  else
  if StrContainsText('QMCluster', DM.UQState.ServerURL) then
     ActiveDB:='qm'
  else
  if StrContainsText('QDCluster', DM.UQState.ServerURL) then
     ActiveDB:='qd'
  else
  if StrContainsText('QCCluster', DM.UQState.ServerURL) then
     ActiveDB:='qc';
end;

procedure TMyWorkForm.FormShow(Sender: TObject);
var
  LastSync: TDateTime;
  ExportSelectionButtonsVisible: Boolean;
begin
  inherited;

  LastSync := DM.UQState.LastLocalSyncTime;
  if FLastRefresh <> LastSync then begin
    MyWorkWOsFrame.RefreshData;
    MyWorkTicketsFrame.RefreshData;
    MyWorkDamagesFrame.RefreshData;
    FLastRefresh := LastSync;
  end;
  
  ExportSelectionButtonsVisible := PermissionsDM.CanI(RightWorkOrdersSelectiveExport) or
    PermissionsDM.CanI(RightTicketsSelectiveExport) or PermissionsDM.CanI(RightDamagesSelectiveExport);
  QRtoUse := '';
  CanUseQR := PermissionsDM.CanI(RightWhichWazeToUse);  //QMANTWO-646
  QRtoUse  := PermissionsDM.GetLimitation(RightWhichWazeToUse);   //QMANTWO-646
 // if QRtoUse = 'W' then
 // btnWaze.OptionsImage.ImageIndex :=4;  //QMANTWO-646
 // else if QRtoUse = 'Q' then btnWaze.OptionsImage.ImageIndex :=0;  //QMANTWO-646
       
  btnWaze.visible  := false;//QMANTWO-646

  SelectAllButton.Visible := ExportSelectionButtonsVisible;
  SelectNoneButton.Visible := ExportSelectionButtonsVisible;

  MyWorkWOsFrame.Setup('Work Orders: ', WorkOrderFrameClick);
  MyWorkTicketsFrame.Setup('Tickets: ', TicketFrameClick);
  MyWorkDamagesFrame.Setup('Damages: ', DamageFrameClick);

  //Returning to screen from work item:
  if Assigned(FCurrentExpandedFrame) then begin
    if FCurrentExpandedFrame is TMyWorkWOsFrame then
      WorkOrderFrameClick(Self)
    else if FCurrentExpandedFrame is TMyWorkTicketsFrame then
      TicketFrameClick(Self)
    else if FCurrentExpandedFrame is TMyWorkDamagesFrame then
      DamageFrameClick(Self);
  end
  //Initially viewing screen:
  else if MyWorkWOsFrame.Visible then
    WorkOrderFrameClick(Self)
  else
    TicketFrameClick(Self);
  IsUserNav := True;
end;

procedure TMyWorkForm.WorkOrderFrameClick(Sender: TObject);
begin
  btnWaze.Visible := false;   //QMANTWO-646
  activeEntity := etWorkOrder; //QMANTWO-646
  MyWorkDamagesFrame.Contract(alBottom);
  MyWorkTicketsFrame.Contract(alBottom);
  EmergenciesCheckBox.Visible := False;  //QM-425 EB
  MyWorkWOsFrame.Expand;
  FCurrentExpandedFrame := MyWorkWOsFrame;
end;

procedure TMyWorkForm.TicketFrameClick(Sender: TObject);
begin
  btnWaze.Visible := false;   //QMANTWO-646
  activeEntity := etTicket;    //QMANTWO-646
  MyWorkWOsFrame.Contract(alTop);
  MyWorkDamagesFrame.Contract(alBottom);
  EmergenciesCheckBox.Visible := True;  //QM-425 EB
  MyWorkTicketsFrame.Expand;
  FCurrentExpandedFrame := MyWorkTicketsFrame;
end;

procedure TMyWorkForm.DamageFrameClick(Sender: TObject);
begin
  btnWaze.Visible := false;      //QMANTWO-646
  MyWorkWOsFrame.Contract(alTop);
  MyWorkTicketsFrame.Contract(alTop);
  EmergenciesCheckBox.Visible := False;  //QM-425 EB
  MyWorkDamagesFrame.Expand;
  FCurrentExpandedFrame := MyWorkDamagesFrame;
end;

procedure TMyWorkForm.ActivatingNow;
begin
  inherited;
  MyWorkWOsFrame.SortGridByFields;
  MyWorkTicketsFrame.SortGridByFields;
  MyWorkTicketsFrame.InitFT;
  MyWorkDamagesFrame.SortGridByFields;
  RefreshButtonClick(Self);
end;

procedure TMyWorkForm.LeavingNow;
begin
  MyWorkTicketsFrame.FreeFT;
  inherited;
end;

procedure TMyWorkForm.SetAllCheckboxesForExport(Checked: Boolean);
begin
  if MyWorkWOsFrame.Visible then MyWorkWOsFrame.SetAllExportBoxes(Checked);
  MyWorkTicketsFrame.SetAllExportBoxes(Checked);
  if MyWorkDamagesFrame.Visible then MyWorkDamagesFrame.SetAllExportBoxes(Checked);
end;

procedure TMyWorkForm.RefreshButtonClick(Sender: TObject);
begin
  inherited;
  MyWorkWOsFrame.RefreshData;
  MyWorkTicketsFrame.RefreshData;
  MyWorkDamagesFrame.RefreshData;
end;

procedure TMyWorkForm.ViewStyleComboChange(Sender: TObject);
begin
  inherited;
  MyWorkWOsFrame.ChangeView(ViewStyleCombo.ItemIndex);
  MyWorkTicketsFrame.ChangeView(ViewStyleCombo.ItemIndex);
  MyWorkDamagesFrame.ChangeView(ViewStyleCombo.ItemIndex);
end;

procedure TMyWorkForm.ExportButtonClick(Sender: TObject);
var
  ExportedFileNames: string;
begin
  inherited;
  DM.CleanExportFolder(-1);
  ExportedFileNames := MyWorkTicketsFrame.ExportMyItems;
  if MyWorkDamagesFrame.Visible then
    ExportedFileNames := ExportedFileNames + #13#10 + MyWorkDamagesFrame.ExportMyItems;
  if MyWorkWOsFrame.Visible then
    ExportedFileNames := ExportedFileNames + #13#10 + MyWorkWOsFrame.ExportMyItems;

  if NotEmpty(ExportedFileNames) then
    MessageDlg('Export created these files: ' + #13#10 + ExportedFileNames, mtInformation, [mbOk], 0)
//  else if (PermissionsDM.CanI(RightTicketsSelectiveExport)) then
//    MessageDlg('Export did not create any files. Make sure to select records to export (export checkbox).', mtInformation, [mbOK], 0)  //QM-51 EB Updated to show better message
  else
    MessageDlg('Export did not create any files.', mtInformation, [mbOK], 0);
end;

procedure TMyWorkForm.SelectAllButtonClick(Sender: TObject);
begin
  inherited;
  SetAllCheckboxesForExport(True);
end;

procedure TMyWorkForm.SelectNoneButtonClick(Sender: TObject);
begin
  inherited;
  SetAllCheckboxesForExport(False);
end;

procedure TMyWorkForm.MyWorkWOsFrameOpenWorkOrdersCalcFields(DataSet: TDataSet);
begin
  inherited;
  MyWorkWOsFrame.OpenWorkOrdersCalcFields(DataSet);
end;

procedure TMyWorkForm.MyWorkWOsFrameWorkOrdersSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if IsUserNav then
  begin
    btnWaze.Visible := (IsWazable and CanUseQR); //QMANTWO-646
  End;
end;

procedure TMyWorkForm.MyWorkTicketsFrameEnter(Sender: TObject);
begin
  inherited;
  MyWorkTicketsFrame.RefreshData;
end;

procedure TMyWorkForm.MyWorkTicketsFrameOpenTicketsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  MyWorkTicketsFrame.OpenTicketsAfterScroll(DataSet);
end;

procedure TMyWorkForm.MyWorkWOsFrameEnter(Sender: TObject);
begin
  inherited;
  MyWorkWOsFrame.RefreshData;
end;

procedure TMyWorkForm.MyWorkWOsFrameOpenWorkOrdersBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  MyWorkWOsFrame.OpenWorkOrdersBeforeOpen(DataSet);
end;

procedure TMyWorkForm.MyWorkTicketsFrameOpenTicketsBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  MyWorkTicketsFrame.OpenTicketsBeforeOpen(DataSet);
end;

procedure TMyWorkForm.MyWorkTicketsFrameOpenTicketsCalcFields(DataSet: TDataSet);
begin
  inherited;
  MyWorkTicketsFrame.OpenTicketsCalcFields(DataSet);
end;

procedure TMyWorkForm.MyWorkTicketsFrameTicketSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if IsUserNav then
  begin
    btnWaze.Visible := (IsWazable and CanUseQR); //QMANTWO-646
  End;
end;

function TMyWorkForm.IsWazable:boolean;   //QMANTWO-646
var
  bResult : boolean;
  function IsNumericString(const iStr:string):Boolean;
  var
    i: extended;
  begin
    Result := TryStrToFloat(iStr,i)
  end;
begin
  LocationToUse := '';
    Case activeEntity of
    etTicket:begin
                with MyWorkTicketsFrame.OpenTickets do
                begin
                  bResult := false;
                if ((QRtoUse = 'Q')  or (QRtoUse = 'W')) then
                begin
                  if ((IsNumericString(FieldByName('work_lat').asString))
                  and (IsNumericString(FieldByName('work_long').asString))
                  and
                  (FieldByName('work_lat').asfloat<>0)
                  and (FieldByName('work_long').asfloat<>0))
                  then bResult := true;
                  result := CanUseQR and bResult;
                  if result = true then
                  begin
                    LocationToUse := LATLNG;
                    exit;  //we have a good LatLng
                  end;
                end;
                if QRtoUse = 'W' then
                begin
                  if FieldByName('work_address_number').Isnull or FieldByName('work_address_street').Isnull or
                     FieldByName('work_city').Isnull or FieldByName('work_state').Isnull then
                     result := false
                     else
                     begin
                       LocationToUse := STREET;
                       result := true;  //We have a good address
                       exit;
                     end;
                end
                else
                result := false;
                end; //with MyWorkTicketsFrame.OpenTickets do
        end;  //etTicket:begin
   etWorkOrder:begin
                with MyWorkWOsFrame.OpenWorkOrders do
                begin
                bResult := false;
                if ((QRtoUse = 'Q')  or (QRtoUse = 'W')) then
                begin
                  if ((IsNumericString(FieldByName('work_lat').asString))
                  and (IsNumericString(FieldByName('work_long').asString))
                  and
                  (FieldByName('work_lat').asfloat<>0)
                  and (FieldByName('work_long').asfloat<>0))
                  then bResult := true;
                  result := CanUseQR and bResult;
                  if result = true then
                  begin
                    LocationToUse := LATLNG;
                    exit;  //we have a good LatLng
                  end;
                end;
                if QRtoUse = 'W' then
                begin
                  if FieldByName('work_address_number').Isnull or FieldByName('work_address_street').Isnull or
                     FieldByName('work_city').Isnull or FieldByName('work_state').Isnull then
                     result := false
                     else
                     begin
                       LocationToUse := STREET;
                       result := true;  //We have a good address
                       exit;
                     end;
                end
                else
                result := false;
                end; //with MyWorkTicketsFrame.OpenTickets do
              end;  //etWorkOrder:begin
  end;  //case
end;



procedure TMyWorkForm.WhichWaze;    //QMANTWO-646
begin
  if QRtoUse = 'Q' then
  ProcessLocationForWazeQTip
  else
  if QRtoUse = 'W' then
  ProcessLocationForWazeQR
end;

procedure TMyWorkForm.ProcessLocationForWazeQR;  //QMANTWO-646
const
  SEP =', ';
var //work_city, work_address_street, work_county,work_state,work_address_number
  myWorkLat, myWorkLng:string;  //work_lat, work_long
  myNumber, myStreet, myCity, myState:string;
  sLatLng, streetAddress:string;

  function IsNumericString(const iStr:string):Boolean;
  var
    i: extended;
  begin
    Result := TryStrToFloat(iStr,i)
  end;

begin
//  btnWaze.OptionsImage.ImageIndex :=-1;
  try
    Case activeEntity of
    etTicket:begin
      with MyWorkTicketsFrame.OpenTickets do
      begin
        if LocationToUse = LATLNG
        then
        begin
           myWorkLat := trim(FieldByName('work_lat').asString);
           myWorkLng := trim(FieldByName('work_long').asString);
           sLatLng :=  myWorkLat+','+myWorkLng;
           btnWaze.Enabled := true;
           frmWaze := TfrmWaze.CreateQR(self, stLatLng, sLatLng);
        end
        else if LocationToUse = STREET then
        begin
           myNumber := FieldByName('work_address_number').asString;
           myStreet := FieldByName('work_address_street').asString;
           myCity := FieldByName('work_city').asString;
           myState := FieldByName('work_state').asString;

           streetAddress := myNumber+' '+myStreet+SEP+myCity+SEP+myState;
           btnWaze.Enabled := true;
           frmWaze := TfrmWaze.CreateQR(self, stStreet, streetAddress);
         //QrCodeUpdate(stStreet, LatLng);
         end;
         btnWaze.OptionsImage.ImageIndex :=2;
         btnWaze.Refresh;
      end; //with MyWorkTicketsFrame.OpenTickets do
      end;//etTicket:begin
    etWorkOrder:begin
            with MyWorkWOsFrame.OpenWorkOrders do
            begin
              if LocationToUse = LATLNG
              then
              begin
                 myWorkLat := trim(FieldByName('work_lat').asString);
                 myWorkLng := trim(FieldByName('work_long').asString);
                 sLatLng :=  myWorkLat+','+myWorkLng;
                 btnWaze.Enabled := true;
                 frmWaze := TfrmWaze.CreateQR(self, stLatLng, sLatLng);
              end
              else if LocationToUse = STREET then
              begin
                 myNumber := FieldByName('work_address_number').asString;
                 myStreet := FieldByName('work_address_street').asString;
                 myCity := FieldByName('work_city').asString;
                 myState := FieldByName('work_state').asString;

                 streetAddress := myNumber+' '+myStreet+SEP+myCity+SEP+myState;
                 btnWaze.Enabled := true;
                 frmWaze := TfrmWaze.CreateQR(self, stStreet, streetAddress);
               //QrCodeUpdate(stStreet, LatLng);
               end;
               btnWaze.OptionsImage.ImageIndex :=2;
               btnWaze.Refresh;
            end; //with MyWorkTicketsFrame.OpenTickets do
     end; //etWorkOrder:begin
    End;//Case activeEntity of
  except on E: Exception do
  begin
     btnWaze.OptionsImage.ImageIndex :=1;
  end;
  end;
end;

procedure TMyWorkForm.ProcessLocationForWazeQTip;  //QMANTWO-515
const                                              //QMANTWO-646
  SEP=',';
var
  myWorkLat, myWorkLng:real; 
  TicketID, EmpID, WoID : integer;
  firstTask:boolean;
begin
  myWorkLat := 0;
  myWorkLng := 0;
  TicketID  := 0;
  WoID      := 0;
  firstTask := false;
  EmpID     := 0;

  try
    Case activeEntity of
    etTicket:begin
                with MyWorkTicketsFrame.OpenTickets do
                begin
                  myWorkLat := FieldByName('work_lat').asfloat;
                  myWorkLng := FieldByName('work_long').asfloat;
                  TicketID  := FieldByName('Ticket_ID').asinteger;
                  firstTask := FieldByName('first_task').asBoolean;
                  EmpID     := DM.EmpID;
                  frmWaze := TfrmWaze.CreateQTIP(self, stQTip, myWorkLat, myWorkLng, TicketID, EmpID, ActiveDB, firstTask, activeEntity);
                end; //with MyWorkTicketsFrame.OpenTickets do
            end; //etTicket:begin
 etWorkOrder:begin
                with MyWorkWOsFrame.OpenWorkOrders do
                begin
                  myWorkLat := FieldByName('work_lat').asfloat;
                  myWorkLng := FieldByName('work_long').asfloat;
                  WoID  := FieldByName('Wo_ID').asinteger;
                  firstTask := false;
                  EmpID     := DM.EmpID;
                  frmWaze := TfrmWaze.CreateQTIP(self, stQTip, myWorkLat, myWorkLng, WoID, EmpID, ActiveDB, firstTask, activeEntity);
                end; //with MyWorkTicketsFrame.OpenTickets do
             end; // etWorkOrder:begin
    end; //Case activeEntity of

  except on E: Exception do
  begin
    btnWaze.OptionsImage.ImageIndex :=1;
    btnWaze.Enabled := false;
  end;
  end;
end;

procedure TMyWorkForm.btnTicketMapClick(Sender: TObject);  //QMANTWO-711
begin
  MyWorkTicketsFrame.TicketMap;
end;

procedure TMyWorkForm.btnWazeClick(Sender: TObject);  //QMANTWO-646
begin
  inherited;
    WhichWaze;
    frmWaze.ShowModal;
    Case activeEntity of
    etTicket: if not(MyWorkTicketsFrame.OpenTickets.RowsAffected >1) then
              MyWorkTicketsFrame.RefreshData;
    etWorkOrder : if not(MyWorkWOsFrame.OpenWorkOrders.RowsAffected >1) then
              MyWorkWOsFrame.RefreshData;
      
    end;

end;


procedure TMyWorkForm.EmergenciesCheckBoxClick(Sender: TObject);
begin
  inherited;
  if activeEntity = etTicket then
    MyWorkTicketsFrame.ChangeViewEmergencies(EmergenciesCheckBox.Checked);   //QM-425 EB
end;

end.
