inherited DamageProductivityReportForm: TDamageProductivityReportForm
  Left = 365
  Top = 220
  Caption = 'Investigation Productivity Report'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 116
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object InvestigatorLabel: TLabel [1]
    Left = 0
    Top = 142
    Width = 63
    Height = 13
    Caption = 'Investigator:'
  end
  object EmployeeStatusLabel: TLabel [2]
    Left = 0
    Top = 190
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object DamageTypesLabel: TLabel [3]
    Left = 0
    Top = 224
    Width = 75
    Height = 13
    Caption = 'Damage Types:'
  end
  object DateRangeLabel: TLabel [4]
    Left = 0
    Top = 8
    Width = 61
    Height = 13
    Caption = 'Date Range:'
  end
  object CheckBoxAll: TCheckBox [5]
    Left = 93
    Top = 162
    Width = 97
    Height = 17
    BiDiMode = bdLeftToRight
    Caption = 'All Investigators'
    ParentBiDiMode = False
    TabOrder = 0
    OnClick = CheckBoxAllClick
  end
  object InvestigatorComboBox: TComboBox [6]
    Left = 93
    Top = 135
    Width = 210
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object ManagersComboBox: TComboBox [7]
    Left = 93
    Top = 109
    Width = 210
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 2
    OnChange = ManagersComboBoxChange
  end
  object EmployeeStatusCombo: TComboBox [8]
    Left = 93
    Top = 184
    Width = 210
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  inline RangeSelect: TOdTimeRangeSelectFrame [9]
    Left = 14
    Top = 22
    Width = 299
    Height = 87
    TabOrder = 4
    inherited FromLabel: TLabel
      Left = 12
      Width = 28
      Caption = 'From:'
    end
    inherited ToLabel: TLabel
      Left = 12
      Top = 38
      Width = 16
      Caption = 'To:'
    end
    inherited DatesLabel: TLabel
      Left = 12
      Width = 32
      Caption = 'Dates:'
    end
    inherited DatesComboBox: TComboBox
      Left = 79
      Width = 170
    end
    inherited FromTimeEdit: TDateTimePicker
      Left = 176
    end
    inherited ToTimeEdit: TDateTimePicker
      Left = 176
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 78
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 78
    end
  end
  object DamageTypesListBox: TCheckListBox
    Left = 93
    Top = 216
    Width = 210
    Height = 97
    ItemHeight = 13
    TabOrder = 5
  end
end
