unit BillingExceptionReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls,
  CheckLst, OdRangeSelect;

type
  TBillingExceptionReportForm = class(TReportBaseForm)
    Label3: TLabel;
    CallCenterList: TCheckListBox;
    Label4: TLabel;
    Units: TEdit;
    RangeSelect: TOdRangeSelectFrame;
    Label1: TLabel;
    DateRangeLabel: TLabel;
    procedure UnitsKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

uses DMu, OdVclUtils, OdExceptions, OdMiscUtils;

{$R *.dfm}

procedure TBillingExceptionReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  RangeSelect.SelectDateRange(rsLastWeek);
  Units.Text := '0';
end;

procedure TBillingExceptionReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('BillingException');
  SetParamDate('BeginDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);
  SetParamInt('Units', StrToInt(Units.Text));

  CheckedItems := GetCheckedItemCodesString(CallCenterList);
  if IsEmpty(CheckedItems) then begin
    CallCenterList.SetFocus;
    raise EOdEntryRequired.Create('Please select at least one call center for the report.');
  end;

  SetParam('CallCenters', CheckedItems);
end;

procedure TBillingExceptionReportForm.UnitsKeyPress(Sender: TObject;
  var Key: Char);
begin
  // only allow digits
  if not (Key in ['0'..'9', #13, #8, #9]) then Key := #0;
end;

end.

