unit BillingMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, BaseSearchForm, StdCtrls, ComCtrls, BillingAdjustmentDetails,
  ExtCtrls, DB, DBISAMTb, OdDBISAMUtils,
  ActnList, QMConst, CheckLst, BillPeriod, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxStyles,
  cxDataStorage, cxDBData,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridLevel, cxGrid, dxCore, cxDateUtils,
  cxNavigator, cxCustomData, cxFilter, cxData;

type
  TBillingMainForm = class(TEmbeddableForm)
    BillingPages: TPageControl;
    AdjustmentsTab: TTabSheet;
    RunTab: TTabSheet;
    ReviewTab: TTabSheet;
    QueueDS: TDataSource;
    RunBillingHeaderPanel: TPanel;
    RefreshButton: TButton;
    QueueTable: TDBISAMTable;
    QueueGrid: TcxGrid;
    QueueLevel: TcxGridLevel;
    QueueView: TcxGridDBTableView;
    BillingAdjustmentTabs: TTabControl;
    BillingAdjustmentSearchTab: TTabSheet;
    BillingAdjustmentDetailTab: TTabSheet;
    BillingLogTab: TTabSheet;
    BillingProcessLog: TMemo;
    ViewLogButton: TButton;
    RunBox: TGroupBox;
    CenterGroupLabel: TLabel;
    RunBillingButton: TButton;
    Actions: TActionList;
    ViewBillingRunLog: TAction;
    CancelBillingRun: TAction;
    CancelRunButton: TButton;
    CenterGroupListBox: TCheckListBox;
    Label6: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    PeriodStarting: TLabel;
    PeriodLookBackDate: TLabel;
    PeriodEndingDate: TcxDateEdit;
    PeriodTypeCombo: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RunBillingButtonClick(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
    procedure BillingAdjustmentTabsChange(Sender: TObject);
    procedure ViewBillingRunLogExecute(Sender: TObject);
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CancelBillingRunExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PeriodTypeComboChange(Sender: TObject);
    procedure PeriodEndingDateChange(Sender: TObject);
    procedure AdjustmentsTabShow(Sender: TObject);
    procedure ReviewTabShow(Sender: TObject);
    procedure RunTabShow(Sender: TObject);
  private
    AdjSearchForm: TSearchForm;
    AdjDetailForm: TBillingAdjustmentDetailsForm;
    BillReviewForm: TSearchForm;
    Defs: TColumnList;
    FPeriod: TBillPeriod;
    procedure AdjustmentSelected(Sender: TObject; ID: Integer);
    procedure AdjustmentChangeDone(Sender: TObject);
    procedure AdjustmentAddDone(Sender: TObject);
    procedure SearchForAdjustment;
    procedure NewAdjustment;
    procedure AdjustmentHome;
    procedure PrepareAdjustmentDetailForm;
    procedure RefreshQueue;
    procedure BillingReviewSearch;
    procedure ShowPeriodDates;
    procedure SetupFormControls;
    procedure CheckQueueForPendingEntries(const CenterGroups: string);
  public
    procedure Initialize;
    procedure LeavingNow; override;
  end;

implementation

uses
  BillingAdjustmentSearchHeader, DMu, OdMiscUtils, OdVclUtils,
  OdHourglass, MSXML2_TLB, OdMSXMLUtils, OdXmlToDataSet,
  QMServerLibrary_Intf, OdExceptions, BillingReviewSearchHeader, UQUtils,
  OdCxUtils, OdCxXmlUtils, LocalPermissionsDMu;

{$R *.dfm}

procedure TBillingMainForm.FormCreate(Sender: TObject);
begin
  inherited;
  Self.HandleNeeded;
  FPeriod := TBillPeriod.Create(Now);
  Defs := TColumnList.Create;
  Defs.AddColumn('Run ID', 40, 'run_id', ftInteger, '', 0, True, False);
  Defs.AddColumn('Call Centers', 85, 'group_code', ftString);
  Defs.AddColumn('Span', 85, 'span', ftString);
  Defs.AddColumn('Period End', 80, 'period_end_date', ftDateTime);
  Defs.AddColumn('Queued By', 100, 'short_name', ftString);
  Defs.AddColumn('Status', 60, 'status');
  Defs.AddColumn('Queued', 130, 'request_date', ftDateTime);
  Defs.AddColumn('Started', 130, 'run_date', ftDateTime);
  Defs.AddColumn('Completed', 130, 'completed_date', ftDateTime);
  Defs.AddColumn('Call Center ID', 40, 'center_group_id', ftInteger, '', 0, False, False);

  PeriodEndingDate.Date := FPeriod.DisplayEndDate;
  PeriodTypeCombo.ItemIndex := 0;

  BillingPages.ActivePageIndex := 0;
end;

procedure TBillingMainForm.AdjustmentSelected(Sender: TObject; ID: Integer);
begin
  if not PermissionsDM.CanI(RightBillingAdjustmentsAdd) then
    Exit;

  PrepareAdjustmentDetailForm;
  AdjSearchForm.LeavingNow;
  AdjSearchForm.Hide;
  BillingAdjustmentTabs.TabIndex := 1;
  AdjDetailForm.OnDone := AdjustmentChangeDone;
  AdjDetailForm.GoToBillingAdjustmentID(ID);
end;

procedure TBillingMainForm.PrepareAdjustmentDetailForm;
begin
  if Assigned(AdjDetailForm) = False then
    AdjDetailForm := CreateEmbeddedForm(TBillingAdjustmentDetailsForm, Self,
      BillingAdjustmentTabs) as TBillingAdjustmentDetailsForm;

  Assert(Assigned(AdjDetailForm));
end;

procedure TBillingMainForm.SearchForAdjustment;
begin
  if Assigned(AdjDetailForm) then
  begin
    AdjDetailForm.LeavingNow;
    AdjDetailForm.Hide;
  end;

  if not Assigned(AdjSearchForm) then begin
    AdjSearchForm := (CreateEmbeddedForm(TSearchForm, Self, BillingAdjustmentTabs) as TSearchForm);
    AdjSearchForm.CriteriaClass := TBillingAdjustmentCriteria;
    AdjSearchForm.OnItemSelected := AdjustmentSelected;
  end;

  Assert(Assigned(AdjSearchForm));
  BillingAdjustmentTabs.TabIndex := 0;
  AdjSearchForm.Show;
end;

procedure TBillingMainForm.FormDestroy(Sender: TObject);
begin
  // No need to free the subforms since they are owned by this form.
  FreeAndNil(FPeriod);
  inherited;
end;

procedure TBillingMainForm.AdjustmentHome;
begin
  SearchForAdjustment;
end;

procedure TBillingMainForm.NewAdjustment;
begin
  PrepareAdjustmentDetailForm;
  AdjDetailForm.OnDone := AdjustmentAddDone;
  AdjDetailForm.AddNewAdjustment;
end;

procedure TBillingMainForm.AdjustmentAddDone(Sender: TObject);
begin
  AdjustmentHome;
end;

procedure TBillingMainForm.AdjustmentChangeDone(Sender: TObject);
begin
  AdjustmentHome;
end;

procedure TBillingMainForm.Initialize;
begin
  AdjustmentHome;
end;

procedure TBillingMainForm.LeavingNow;
begin
  inherited;
  if Assigned(AdjDetailForm) then
    AdjDetailForm.LeavingNow;
end;

procedure TBillingMainForm.RunBillingButtonClick(Sender: TObject);
var
  CenterGroups: string;
begin
  CenterGroups := GetCheckedItemString(CenterGroupListBox, True);
  if Trim(CenterGroups) = '' then
    raise EOdEntryRequired.Create('Please select a call center group');

  CheckQueueForPendingEntries(CenterGroups);
  DM.Engine.StartBillingOnServer(FPeriod, CenterGroups);
  ShowMessage('The billing run has been queued.  You can refresh this screen occasionally to monitor its progress.');
  RefreshQueue;
end;

procedure TBillingMainForm.RefreshButtonClick(Sender: TObject);
begin
  RefreshQueue;
end;

procedure TBillingMainForm.ViewBillingRunLogExecute(Sender: TObject);
var
  Cursor: IInterface;
  Params: NVPairList;
begin
  if QueueTable.IsEmpty then
    raise EOdDataEntryError.Create('There are no billing run logs to view');

  Cursor := ShowHourGlass;
  Params := NVPairList.Create;
  try
    AddParam(Params, 'run_id', QueueTable.FieldByName('run_id').AsString);

    DM.CallingServiceName('GetBillingRunLog');
    BillingProcessLog.Lines.Text := DM.Engine.Service.GetBillingRunLog(DM.Engine.SecurityInfo, Params);
    if not BillingLogTab.Visible then begin
      BillingLogTab.TabVisible := True;
      BillingPages.ActivePage := BillingLogTab;
    end;
  finally
    FreeAndNil(Params);
  end;
end;

procedure TBillingMainForm.RefreshQueue;
var
  Cursor: IInterface;
  RawData: string;
  DOM: IXMLDomDocument;
  Params: NVPairList;
begin
  Cursor := ShowHourGlass;
  DM.CallingServiceName('BillingRunSearch');
  Params := NVPairList.Create;
  try
    RawData := DM.Engine.Service.BillingRunSearch(DM.Engine.SecurityInfo, Params);
  finally
    FreeAndNil(Params);
  end;
  DOM := ParseXml(RawData);
  QueueTable.DisableControls;
  try
    ConvertXMLToDataSet(DOM, Defs, QueueTable);
    CreateGridColumnsFromDef(QueueView, QueueTable, Defs);
  finally
    QueueTable.EnableControls;
  end;
  FocusFirstGridRow(QueueGrid, False);
end;

procedure TBillingMainForm.BillingReviewSearch;
begin
  if not Assigned(BillReviewForm) then begin
    BillReviewForm := (CreateEmbeddedForm(TSearchForm, Self, ReviewTab) as TSearchForm);
    BillReviewForm.CriteriaClass := TBillingReviewSearchCriteria;
    BillReviewForm.Show;
  end else
    BillReviewForm.Criteria.Showing;
end;

procedure TBillingMainForm.AdjustmentsTabShow(Sender: TObject);
begin
  if (BillingAdjustmentTabs.TabIndex = 0) and Assigned(AdjSearchForm) then // only if we are in the adjust tab
    AdjSearchForm.Criteria.Showing;
end;

procedure TBillingMainForm.ReviewTabShow(Sender: TObject);
begin
  BillingReviewSearch;
end;

procedure TBillingMainForm.RunTabShow(Sender: TObject);
begin
  RefreshQueue;
end;

procedure TBillingMainForm.BillingAdjustmentTabsChange(Sender: TObject);
begin
  if BillingAdjustmentTabs.TabIndex = 0  then
    SearchForAdjustment;
  if BillingAdjustmentTabs.TabIndex = 1 then
    NewAdjustment;
end;

procedure TBillingMainForm.FormShow(Sender: TObject);
begin
  inherited;
  SetupFormControls;
end;

procedure TBillingMainForm.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  ViewBillingRunLog.Enabled := (not QueueTable.IsEmpty) and (not QueueTable.FieldByName('completed_date').IsNull);
  CancelBillingRun.Enabled  := (not QueueTable.IsEmpty) and (QueueTable.FieldByName('completed_date').IsNull);
end;

procedure TBillingMainForm.CancelBillingRunExecute(Sender: TObject);
var
  Cursor: IInterface;
  RunID, CallCenterGroupID: Integer;
begin
  Cursor := ShowHourGlass;
  if QueueTable.IsEmpty then
    raise EOdDataEntryError.Create('There are no billing runs to cancel');

  RunID := QueueTable.FieldByName('run_id').asInteger;
  CallCenterGroupID := QueueTable.FieldByName('center_group_id').asInteger;
  DM.CallingServiceName('CancelBillingRun');
  DM.Engine.CancelBillingOnServer(RunID, CallCenterGroupID);
  RefreshQueue;
end;

procedure TBillingMainForm.PeriodTypeComboChange(Sender: TObject);
begin  //qm-978 added month_c and month_d to combo box.  sr 8/20
  inherited;//qm-990 added WEEK_C and WEEK_D to combo box. sr 8/20
  FPeriod.Span := PeriodTypeCombo.Text;
  ShowPeriodDates;
  PeriodEndingDate.Date := FPeriod.DisplayEndDate;
end;

procedure TBillingMainForm.ShowPeriodDates;
begin
  PeriodStarting.Caption := DateToStr(FPeriod.StartDate);
  PeriodLookBackDate.Caption := DateToStr(FPeriod.LookBackDate);
end;

procedure TBillingMainForm.PeriodEndingDateChange(Sender: TObject);
begin
  inherited;
  FPeriod.DisplayEndDate := PeriodEndingDate.Date;
  ShowPeriodDates;
end;

procedure TBillingMainForm.SetupFormControls;
begin
  DM.BillableCenterGroupList(CenterGroupListBox.Items, [RightBillingRun], False);
  RunTab.TabVisible := PermissionsDM.CanI(RightBillingRun);
  RunTab.Visible := RunTab.TabVisible;
  ReviewTab.TabVisible := PermissionsDM.CanI(RightBillingDelete)
    or PermissionsDM.CanI(RightBillingCommit) or PermissionsDM.CanI(RightBillingUncommit)
    or PermissionsDM.CanI(RightBillingReexport);
  ReviewTab.Visible := ReviewTab.TabVisible;

  BillingAdjustmentTabs.Tabs.Clear;
  if PermissionsDM.CanI(RightBillingAdjustmentsAdd) then begin
    BillingAdjustmentTabs.Tabs.Add('Adjustment Search');
    BillingAdjustmentTabs.Tabs.Add('New Adjustment');
  end
  else if PermissionsDM.CanI(RightBillingAdjustmentsView) then
    BillingAdjustmentTabs.Tabs.Add('Adjustment Search');
  AdjustmentsTab.TabVisible := BillingAdjustmentTabs.Tabs.Count > 0;
  AdjustmentsTab.Visible := AdjustmentsTab.TabVisible;
end;

procedure TBillingMainForm.CheckQueueForPendingEntries(const CenterGroups: string);
begin
  QueueTable.Filter := Format('period_end_date=%s and span=%s ' +
    'and center_group_id in (%s) and completed_date is null',
    [QuotedStr(FormatDateTime('yyyy-mm-dd', FPeriod.EndDate)),
    QuotedStr(FPeriod.Span), CenterGroups]);
  QueueTable.Filtered := True;
  try
    if QueueTable.RecordCount > 0 then
      raise EOdDataEntryError.Create('Cannot add centers to the queue. ' +
        'There are pending requests for the selected centers and period already queued.');
  finally
    QueueTable.Filtered := False;
    QueueTable.Filter := '';
  end;
end;

end.

