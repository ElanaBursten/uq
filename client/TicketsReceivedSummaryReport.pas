unit TicketsReceivedSummaryReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, OdRangeSelect, StdCtrls;

type
  TTicketsReceivedSummaryReportForm = class(TReportBaseForm)
    Label1: TLabel;
    RangeSelect: TOdRangeSelectFrame;
    IncludeBreakdown: TCheckBox;
    IncludeNoWorkTickets: TCheckBox;
    procedure IncludeBreakdownClick(Sender: TObject);
  private
  public
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

procedure TTicketsReceivedSummaryReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TicketsReceivedSummary');
  SetParamDate('StartDate', RangeSelect.FromDate);
  SetParamDate('EndDate', RangeSelect.ToDate);
  SetParamBoolean('IncludeBreakdownByCC', IncludeBreakdown.Checked);
  SetParamBoolean('IncludeNoWorkTickets', IncludeNoWorkTickets.Checked);
end;

procedure TTicketsReceivedSummaryReportForm.InitReportControls;
begin
  inherited;
  RangeSelect.SelectDateRange(OdRangeSelect.rsThisWeek);
end;

procedure TTicketsReceivedSummaryReportForm.IncludeBreakdownClick(
  Sender: TObject);
begin
  IncludeNoWorkTickets.Enabled := IncludeBreakdown.Checked;

  if not IncludeBreakdown.Checked then
    IncludeNoWorkTickets.Checked := False;
end;

end.

