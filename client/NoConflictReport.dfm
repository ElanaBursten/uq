inherited NoConflictReportForm: TNoConflictReportForm
  Left = 437
  Top = 235
  Caption = 'NoConflictReportForm'
  ClientHeight = 284
  ClientWidth = 345
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 27
    Height = 13
    Caption = 'Date:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [1]
    Left = 0
    Top = 39
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object Label2: TLabel [2]
    Left = 0
    Top = 117
    Width = 46
    Height = 13
    Caption = 'Statuses:'
  end
  object Label4: TLabel [3]
    Left = 0
    Top = 66
    Width = 30
    Height = 13
    Caption = 'State:'
  end
  object Label5: TLabel [4]
    Left = 0
    Top = 92
    Width = 39
    Height = 13
    Caption = 'County:'
  end
  object CallCenterComboBox: TComboBox [5]
    Left = 64
    Top = 36
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object StatusCheckListBox: TCheckListBox [6]
    Left = 64
    Top = 116
    Width = 153
    Height = 165
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 2
  end
  object State: TComboBox [7]
    Left = 64
    Top = 63
    Width = 73
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 18
    ItemHeight = 13
    MaxLength = 2
    TabOrder = 3
  end
  object County: TEdit [8]
    Left = 64
    Top = 89
    Width = 174
    Height = 21
    TabOrder = 4
  end
  object SelectAllButton: TButton [9]
    Left = 223
    Top = 116
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 5
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [10]
    Left = 223
    Top = 148
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 6
    OnClick = ClearAllButtonClick
  end
  object DateEdit: TcxDateEdit [11]
    Left = 64
    Top = 8
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 85
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 370
  end
end
