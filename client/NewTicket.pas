unit NewTicket;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OdEmbeddable, DBCtrls, StdCtrls, Db, ExtCtrls, ComCtrls, Buttons, 
  DBISAMTb, Mask, DMu, ActnList,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxDBEdit, CodeLookupList;

type
  TNewTicketForm = class(TEmbeddableForm)
    LocateDS: TDataSource;
    TicketDS: TDataSource;
    DetailsPanel: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Address1Label: TLabel;
    Label10: TLabel;
    Label26: TLabel;
    Label34: TLabel;
    Label123: TLabel;
    Label14: TLabel;
    WorkDescription: TDBMemo;
    WorkRemarks: TDBMemo;
    Company: TDBEdit;
    Address: TDBEdit;
    ConName: TDBEdit;
    CallerContact: TDBEdit;
    MapPage: TDBEdit;
    WorkCross: TDBEdit;
    City: TDBEdit;
    County: TDBEdit;
    WorkAddressNumber: TDBEdit;
    WorkType: TDBEdit;
    AreaName: TDBEdit;
    State: TDBComboBox;
    TicketType: TDBComboBox;
    Label1: TLabel;
    Ticket: TDBISAMQuery;
    TicketActions: TActionList;
    SaveAction: TAction;
    CallDate: TcxDBDateEdit;
    Label5: TLabel;
    CallCenterLabel: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    ClientCombo: TComboBox;
    Label16: TLabel;
    AssignedToCombo: TComboBox;
    Label17: TLabel;
    ButtonPanel: TPanel;
    NewTicket: TButton;
    SaveButton: TButton;
    NewAction: TAction;
    CancelAction: TAction;
    Button1: TButton;
    CallCenter: TComboBox;
    Address2Label: TLabel;
    Latitude: TDBEdit;
    lblLatitude: TLabel;
    Longitude: TDBEdit;
    lblLongitude: TLabel;
    btnGpsValues: TBitBtn;
    edEdtWoNum: TDBEdit;
    Label8: TLabel;
    Label18: TLabel;
    CALLERPHONE: TDBEdit;  //QM-454 SR
    procedure SaveTicket(Sender: TObject);
    procedure TicketActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CallRecButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CallCenterChange(Sender: TObject);
    procedure NewActionExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnGpsValuesClick(Sender: TObject);
    procedure LatitudeExit(Sender: TObject);
    procedure LongitudeExit(Sender: TObject);  //QMANTWO-240
  private
    FCallCenterList: TCodeLookupList;
    FOnGoToTicket: TItemSelectedEvent;
    function ChangedData: Boolean;
    procedure CheckRequiredFields;
  public
    procedure RefreshNow; override;
    procedure OpenDataSets; override;
    property OnGoToTicket: TItemSelectedEvent read FOnGoToTicket write FOnGoToTicket;
  end;

implementation

uses
  OdDbUtils, QMConst, OdVclUtils, OdExceptions, OdMiscUtils,
  LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.DFM}

{ TNewTicketForm }

type
  THackMemo = class(TMemo)
  end;

function TNewTicketForm.ChangedData: Boolean;
begin
  Result := EditingDataSet(Ticket);
end;

procedure TNewTicketForm.OpenDataSets;
begin
  inherited;
  OpenDataSet(Ticket);
  WorkDescription.MaxLength := 1500;
  WorkRemarks.MaxLength := 1200;
end;

procedure TNewTicketForm.RefreshNow;
begin
  DM.CallCenterListForTickets(FCallCenterList, False);
  DM.GetRefDisplayList('state', State.Items);
  DM.GetRefDisplayList('manticktyp', TicketType.Items);
end;

procedure TNewTicketForm.SaveTicket(Sender: TObject);
var
  NewID: Integer;
  Remarks: string;
begin
  CheckRequiredFields;
  if Ticket.FieldByName('work_remarks').AsString <> '' then begin
    Remarks := Ticket.FieldByName('work_remarks').AsString;
    Remarks := CleanRemarks(Remarks);
    Ticket.FieldByName('work_remarks').AsString := Remarks;
  end;
  DM.Database1.StartTransaction;
  try
    NewID := DM.CreateNewTicket(Ticket);
    if ClientCombo.ItemIndex > -1 then
      DM.AddLocateToTicket(NewID, GetComboObjectInteger(ClientCombo), GetComboObjectInteger(AssignedToCombo, False));
    DM.Database1.Commit;
  except
    DM.Database1.Rollback;
    raise;
  end;

  Ticket.Post;
  if Assigned(OnGoToTicket) then
    OnGoToTicket(Self, NewID);

  SetEnabledOnControlAndChildren(DetailsPanel, False);
end;

procedure TNewTicketForm.TicketActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  SaveAction.Enabled := ChangedData;
end;

procedure TNewTicketForm.CallRecButtonClick(Sender: TObject);
begin
  Ticket.Edit;
  Ticket.FieldByName('call_date').AsDateTime := RoundSQLServerTimeMS(Now);
end;

procedure TNewTicketForm.FormCreate(Sender: TObject);
begin
  inherited;
  THackMemo(WorkDescription).CharCase := ecUpperCase;
  THackMemo(WorkRemarks).CharCase := ecUpperCase;
  OpenDataSets;
  //Associate object with the combobox
  FCallCenterList := TCodeLookupList.Create(CallCenter.Items);
  RefreshNow;
  NewAction.Execute;
end;

procedure TNewTicketForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FCallCenterList);
  inherited;
end;

procedure TNewTicketForm.LatitudeExit(Sender: TObject);  //QMANTWO-666
var
  Nbr: Double;
begin
  inherited;
   If not(TryStrtoFloat(Latitude.Text, Nbr)) then
   begin
     ShowMessage('Must enter a valid decimal Latitude value');
     Latitude.Text := '';
     exit;
   end;
   if (Nbr < 23.0) or (Nbr > 52.0) then
   begin
     ShowMessage('Latitude in Continental US must be between 23 and 52');
     Latitude.SetFocus;
     Latitude.SelLength := 0;
     Latitude.Text := '';
   end;
end;

procedure TNewTicketForm.LongitudeExit(Sender: TObject);    //QMANTWO-666
var
  Nbr: Double;
begin
  inherited;
   If not(TryStrtoFloat(Longitude.Text, Nbr)) then
   begin
     ShowMessage('Must enter a valid Longitude decimal value');
     Longitude.Text := '';
     exit;
   end;
   if ((abs(Nbr) > 127.0) or (abs(Nbr) < 65.0) or (Nbr > 0))  then
   begin
     ShowMessage('Longitude in Continental US must be between -127 and -65');
     Longitude.SetFocus;
     Longitude.SelLength := 0;
     Longitude.Text := '';
   end;
end;

procedure TNewTicketForm.btnGpsValuesClick(Sender: TObject);  //QMANTWO-240
begin                                                         //QMANTWO-240
  inherited;                                                 //QMANTWO-240
  Latitude.Text  :=  floatToStrF(DM.CurrentGPSLatitude,ffGeneral, 9,6);    //QMANTWO-240
  Longitude.text :=  floatToStrF(DM.CurrentGPSLongitude,ffGeneral, 9,6 );   //QMANTWO-240
end;                                                       //QMANTWO-240

procedure TNewTicketForm.CallCenterChange(Sender: TObject);
var
  CallCenterCode: string;
begin
  inherited;
  CallCenterCode := FCallCenterList.GetCode(CallCenter.Text);
  Ticket.Edit;
  Ticket.FieldByName('ticket_format').AsString := CallCenterCode;
  DM.GetClientListForCallCenter(CallCenterCode, ClientCombo.Items);
  ClientCombo.Enabled := True;
end;

procedure TNewTicketForm.NewActionExecute(Sender: TObject);
begin
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeNewTicket, '');
  CancelDataSet(Ticket);
  OpenDataSet(Ticket);
  Ticket.Insert;
  Ticket.FieldByName('ticket_type').AsString := TicketTypeEmergency;
  Ticket.FieldByName('call_date').AsDateTime := RoundSQLServerTimeMS(Now);
  CallCenter.ClearSelection;
  ClientCombo.Clear;

  EmployeeDM.EmployeeList(AssignedToCombo.Items, DM.UQState.EmpID);
  SelectComboBoxItemFromObjects(AssignedToCombo, DM.EmpID);
  AssignedToCombo.Enabled := PermissionsDM.IsTicketManager;

  PostDataSet(Ticket);
  ClientCombo.Enabled := False;
  SetEnabledOnControlAndChildren(DetailsPanel, True);
end;       

procedure TNewTicketForm.CheckRequiredFields;
const
  Required = ' requires an entered value';
begin
  if Ticket.FieldByName('work_type').IsNull then
    raise EOdEntryRequired.Create('Work to be Done' + Required);
  if Ticket.FieldByName('ticket_format').IsNull then
    raise EOdEntryRequired.Create('Call Center' + Required);
  if Ticket.FieldByName('work_address_number').IsNull then
    raise EOdEntryRequired.Create('Work Address Number' + Required);
  if Ticket.FieldByName('work_address_street').IsNull then
    raise EOdEntryRequired.Create('Work Address Street' + Required);
  if Ticket.FieldByName('work_state').IsNull then
    raise EOdEntryRequired.Create('Work State' + Required);
  if Ticket.FieldByName('work_city').IsNull then
    raise EOdEntryRequired.Create('Work City' + Required);
end;

end.

