unit LocalDataDef;

// This is used to generate ClientSchema.xml using the TableHelper application

interface

uses DB, QMConst;

type
  TSyncStatusRow = record
    TableName: string;
    PrimaryKey: string;
    CustomSQL: string;
    PurgeSQL: string;
  end;

  TLocalIndexDef = record
    TableName: string;
    IndexName: string;
    Fields: string;
    Options: TIndexOptions;
  end;

const
  {
    CustomSQL: This local table does not match up very closely to a server table
      and the schema is created using custom SQL in TableHelper
    PurgeSQL: Some of the local already synced data should be deleted when QM
      starts; a blank entry does not delete anything; non-blanks are treated as
      conditions in the where clause.
  }
  SyncStatusRows: array[0..111] of TSyncStatusRow = (
    (
      TableName: 'client';
      PrimaryKey: 'client_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'office';
      PrimaryKey: 'office_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'statuslist';
      PrimaryKey: 'status';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket';
      PrimaryKey: 'ticket_id';
      CustomSQL: 'select *, ''12345678901234567890123456789012345678901234567890'' as route_area_name, '+
        'convert(bit, 0) as area_changed, convert(datetime, 0) as area_changed_date, ' +
        'convert(datetime, 0) as followup_transmit_date, convert(datetime, 0) as followup_due_date, ' +
        '''123456789012345678901234567890123456789012345678901234567890123456789'' as risk_score, ' +
        'convert(datetime, 0) as alt_due_date, convert(datetime, 0) as expiration_date ' +  //QM-Exp
        'from ticket where 1=0';
      PurgeSQL: '';
    ),
    (
      TableName: 'employee';
      PrimaryKey: 'emp_id';
      CustomSQL: 'select e.*, convert(bit, 0) as under_me_timesheets, convert(bit, 0) as under_me_reports, convert(bit, 0) as under_me, '+
                 '''123456789012345'' as effective_pc, ''123456789012345'' as effective_payroll_pc from employee e';
      PurgeSQL: '';
    ),
    (
      TableName: 'reference';
      PrimaryKey: 'ref_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'widget';
      PrimaryKey: 'widget_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage';
      PrimaryKey: 'damage_id';
      CustomSQL: 'Select d.* from damage d'; //'Select d.*, d.intelex_num as utilistar_id from damage d';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_estimate';
      PrimaryKey: 'estimate_id';
      CustomSQL: 'Select de.* from damage_estimate de';
      PurgeSQL: '';
      //'Select de.*, de.intelex_num as utilistar_id from damage_estimate de';
    ),
    (
      TableName: 'response_log';
      PrimaryKey: 'response_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'notes';
      PrimaryKey: 'notes_id';
      CustomSQL: 'select notes.*, 0 as damage_notes_type, 0 as damage_id from notes where notes_id = -1';
      PurgeSQL: '';
    ),
    (
      TableName: 'locate';
      PrimaryKey: 'locate_id';
      CustomSQL: 'select locate.*, assignment.locator_id, 0 as StatusAsAssignedUser, ' +
                 '0 as length_marked, 0 as length_total, assignment.workload_date ' +
//                 '0 as qty_marked_total, 0 as esketch_length_total, 0 as esketch_qty_marked ' +    //QM-1037 BigFoot
                 'from locate left join assignment on locate.locate_id=assignment.locate_id ' +
                 'where locate.ticket_id = -1';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_invoice';
      PrimaryKey: 'invoice_id';
      CustomSQL: 'select *, 0 as invoice_type ' +
        'from damage_invoice ' +
        'where invoice_id = -1';
      PurgeSQL: '';
    ),
    (
      TableName: 'locate_status';
      PrimaryKey: 'ls_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'assignment';
      PrimaryKey: 'assignment_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_detail';
      PrimaryKey: 'billing_detail_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_header';
      PrimaryKey: 'bill_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket_version';
      PrimaryKey: 'ticket_version_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'locate_hours';
      PrimaryKey: 'locate_hours_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_litigation';
      PrimaryKey: 'litigation_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'carrier';
      PrimaryKey: 'carrier_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'call_center';
      PrimaryKey: 'cc_code';
      CustomSQL: 'select *, ''12345678901234567890'' as alert_phone_no from call_center where 0=1';
      PurgeSQL: '';
    ),
    (
      TableName: 'followup_ticket_type';
      PrimaryKey: 'type_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'employee_right';
      PrimaryKey: 'emp_right_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_history';
      PrimaryKey: 'damage_id;modified_date';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'asset_type';
      PrimaryKey: 'asset_code';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'asset';
      PrimaryKey: 'asset_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'asset_assignment';
      PrimaryKey: 'assignment_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'attachment';
      PrimaryKey: 'attachment_id';
      CustomSQL: 'select *, GetDate() as upload_try_date, convert(bit, 0) as need_to_upload ' +
        'from attachment where 0=1';
      PurgeSQL: '(attach_date < :older_than_date)';
    ),
    (
      TableName: 'upload_location';
      PrimaryKey: 'location_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'open_locate';
      PrimaryKey: 'locate_id';
      CustomSQL: 'select locate_id, status from locate open_locate where locate_id = -1';
      PurgeSQL: '';
    ),
    (
      TableName: 'error_report';
      PrimaryKey: 'error_report_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'timesheet_entry';
      PrimaryKey: 'entry_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_default_est';
      PrimaryKey: 'ddest_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket_ack';
      PrimaryKey: 'ticket_ack_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'report_log';
      PrimaryKey: 'request_id';
      CustomSQL: 'select request_id, report_name, request_date, status, 100 as update_count, request_date as next_update from report_log where 1=0';
      PurgeSQL: '';
    ),
    (
      TableName: 'status_group';
      PrimaryKey: 'sg_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'status_group_item';
      PrimaryKey: 'sg_item_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'call_center_hp';
      PrimaryKey: 'call_center_hp_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'profit_center';
      PrimaryKey: 'pc_code';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'customer';
      PrimaryKey: 'customer_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'customer_locator';      //qm-416 holds roboCopy arguments sr
      PrimaryKey: 'customer_locator_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_adjustment';
      PrimaryKey: 'adjustment_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'center_group';
      PrimaryKey: 'center_group_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'center_group_detail';
      PrimaryKey: 'center_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'users';
      PrimaryKey: 'uid';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_third_party';
      PrimaryKey: 'third_party_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'locate_plat';
      PrimaryKey: 'locate_plat_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_invoice';
      PrimaryKey: 'invoice_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_unit_conversion';
      PrimaryKey: 'unit_conversion_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'area';
      PrimaryKey: 'area_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'damage_bill';
      PrimaryKey: 'damage_bill_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_output_config';
      PrimaryKey: 'output_config_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'message_dest';
      PrimaryKey: 'message_dest_id';
      CustomSQL: 'select top 1 message.message_id, message.message_number, ' +
      'message.from_emp_id, message.destination, message.subject, message.body, ' +
      'message.sent_date, message.show_date, message.expiration_date, message.active, ' +
      'md.message_dest_id, md.emp_id, md.ack_date, md.read_date, md.rule_id ' +
      'from message inner join message_dest md on message.message_id=md.message_id';
      PurgeSQL: '';
    ),
    (
      TableName: 'message';
      PrimaryKey: 'message_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'message_recip';
      PrimaryKey: 'message_dest_id';
      CustomSQL: 'select top 1 * from message_dest';
      PurgeSQL: '';
    ),
    (
      TableName: 'locating_company';
      PrimaryKey: 'company_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket_info';
      PrimaryKey: 'ticket_info_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'employee_activity';
      PrimaryKey: 'emp_activity_id';
      CustomSQL: '';
      PurgeSQL: '1=1';
    ),
    (
      TableName: 'break_rules';
      PrimaryKey: 'rule_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'configuration_data';
      PrimaryKey: 'name';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'upload_file_type';
      PrimaryKey: 'upload_file_type_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'right_definition';
      PrimaryKey: 'right_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'billing_gl';
      PrimaryKey: 'billing_gl_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'right_restriction';
      PrimaryKey: 'right_restriction_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'restricted_use_message';
      PrimaryKey: 'rum_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'restricted_use_exemption';
      PrimaryKey: 'rue_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'jobsite_arrival';
      PrimaryKey: 'arrival_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'locate_facility';
      PrimaryKey: 'locate_facility_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'emp_group_right';
      PrimaryKey: 'emp_id;right_id';
      CustomSQL: 'select 99 as emp_id, 99 as right_id, 99 as group_id, ' +
        'convert(varchar(1), ''Y'') as allowed, convert(varchar(200), ''-'') as limitation';
      PurgeSQL: '';
    ),
    (
      TableName: TableEditableAreas;
      PrimaryKey: 'area_id';
      CustomSQL: 'select 99 as map_id, convert(varchar(30), ''name'') as map_name, convert(varchar(5), ''WY'') as state, ' +
        'convert(varchar(50), ''County Name'') as county, convert(varchar(15), ''123456ABC'') as ticket_code, 99 as area_id, ' +
        'convert(varchar(40), ''Area Name'') as area_name, 99 as locator_id';
      PurgeSQL: '';
    ),
    (
      TableName: 'addin_info';
      PrimaryKey: 'addin_info_id';
      CustomSQL: 'SELECT 1234567 as addin_info_id, convert(tinyint, 1) as foreign_type, ' +
        '1234567 as foreign_id, convert(float, 12.3456789) as latitude, ' +
        'convert(float, -34.56789012) as longitude, getdate() as added_date, ' +
        '99 as added_by_id, convert(bit, 1) as active, getdate() as modified_date';
      PurgeSQL: '';
    ),
    (
      TableName: 'break_rules_response';
      PrimaryKey: 'response_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'email_queue_result';
      PrimaryKey: 'eqr_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'center_ticket_summary';
      PrimaryKey: 'pc_code';
      CustomSQL: 'select ''123456789012345'' as pc_code, 999 as tickets_open, ' +
        '999 as tickets_due_today, 999 as tickets_due_tomorrow';
      PurgeSQL: '';
    ),
    (
      TableName: 'exported_damages_history';
      PrimaryKey: 'damage_id;short_period';
      CustomSQL: 'select top 1 d.damage_id, hist.* ' +
        'from exported_damages_history hist ' +
        'inner join damage d on hist.claim_number = d.uq_damage_id';
      PurgeSQL: '';
    ),
    (
      TableName: 'task_schedule';
      PrimaryKey: 'task_schedule_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'highlight_rule';
      PrimaryKey: 'highlight_rule_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'gps_position';
      PrimaryKey: 'gps_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'required_document';
      PrimaryKey: 'required_doc_id';
      CustomSQL: '';
      PurgeSQL:  '';
    ),
    (
      TableName: 'required_document_type';
      PrimaryKey: 'rdt_id';
      CustomSQL: '';
      PurgeSQL:  '';
    ),
    (
      TableName: 'document';
      PrimaryKey: 'doc_id';
      CustomSQL: '';
      PurgeSQL:  '';
    ),
    (
      TableName: 'damage_profit_center_rule';
      PrimaryKey: 'rule_id';
      CustomSQL: '';
      PurgeSQL:  '';
    ),
    (
      TableName: 'work_order';
      PrimaryKey: 'wo_id';
      CustomSQL: 'select *, ''                    '' as statused_how, 99 as statused_by_id ' +
        'from work_order ' +
        'where wo_id = -1';
      PurgeSQL: '';
    ),
    //QMANTWO-391
    (
      TableName: 'work_order_OHM_details';
      PrimaryKey: 'wo_dtl_id';
      CustomSQL: 'select *, ''                    '' as statused_how, 99 as statused_by_id ' +
        'from work_order_OHM_details ' +
        'where wo_id = -1';
      PurgeSQL: '';
    ),
    (
      TableName: 'work_order_ticket';
      PrimaryKey: 'wo_id;ticket_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'wo_assignment';
      PrimaryKey: 'assignment_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'wo_status_history';
      PrimaryKey: 'wo_status_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'wo_response_log';
      PrimaryKey: 'wo_response_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'work_order_version';
      PrimaryKey: 'wo_version_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    ( // This is a local data only table used by work management.
      TableName: 'emp_workload';
      PrimaryKey: 'emp_tree_id';
      CustomSQL: 'select ''123456789012345'' as emp_tree_id, emp_id, ' +
        '''1234567890'' as report_to, emp_number, short_name, type_id, active, ' +
        '0 as sort, ticket_view_limit, show_future_tickets, ' +
        'convert(bit, 0) as under_me, convert(bit, 0) as is_manager, ' +
        '0 as tickets_tot, 0 as locates_tot, 0 as damages_tot, 0 as wo_tot, ' +
        '''12345678901234567890123456789012345678901234567890'' as node_name ' +
        ', ''1234567890'' as work_status_today, ''12345678901234567890'' as contact_phone '  +
        'from employee as emp_workload where 0=1';
      PurgeSQL: '';
    ),
    ( // This is a local data only table for temporarily storing current locations of set of employees.  //QMANTWO-302
      TableName: 'emp_breadcrumbs';
      PrimaryKey: 'emp_id';
      CustomSQL: 'select emp_id, report_to as mgr_id, ' +
                 ' ''ABC12345678901234567890123456789Z'' as short_name, ' +        //1234567 as breadcrumb_id,
                 '0.000000 as lat, 0.000000 as lng, ' +
                 'getdate() as last_update, getUTCdate() as UTC, ' +
                 ' ''ABC12345678901234567890123456789Z'' as label, ''ACTIVITY123'' as last_activity ' +
                 'from employee as emp_breadcrumbs where 0=1';
      PurgeSQL: '';
    ),
    (
      TableName: 'holiday';
      PrimaryKey: 'cc_code;holiday_date';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'work_order_inspection';
      PrimaryKey: 'wo_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'work_order_remedy';
      PrimaryKey: 'wo_remedy_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'work_order_work_type';
      PrimaryKey: 'work_type_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'question_info';
      PrimaryKey: 'qi_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'question_header';
      PrimaryKey: 'qh_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'question_group_detail';
      PrimaryKey: 'qgd_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'epr_response_history';
      PrimaryKey: 'history_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'api_storage_constants';        //AWS QMANTWO-432 QMANTWO-552 EB
      PrimaryKey: 'id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
     (
      TableName: 'api_storage_credentials';     //AWS QMANTWO-432 QMANTWO-552 EB
      PrimaryKey: 'id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'client_info';     //Client Info QMANTWO-676 sr
      PrimaryKey: 'client_info_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket_risk';     //QM-386 PAR Scores / Risk Scores
      PrimaryKey: 'ticket_risk_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'employee_balance';     //QM-579 for tracking PTO/Sick time  SR
      PrimaryKey: 'emp_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'empPlus';  //QM-585
      PrimaryKey: 'group_id;client_code';
      CustomSQL: 'select ''123456789012345'' as group_id, ' +
                 '''1234567890123456789012345678901234567890'' as client_code, ' +
                 'convert(bit, 0) as can_status, ' +
                 '''123456789012345'' as emp_id '+
                 'from employee as empPlus where 0=1';
      PurgeSQL: '';
    ),
    (
      TableName: 'custom_form';  //QM-593
      PrimaryKey: 'form_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'custom_form_field';  //QM-593
      PrimaryKey: 'form_field_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'custom_form_answer';  //QM-593
      PrimaryKey: 'answer_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'custom_form_reference';  //QM-593
      PrimaryKey: 'ref_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'status_plus'; //QM-635
      PrimaryKey: 'status_plus_id';
      CustomSQL: '';
      PurgeSQL: '';
    ),
    (
      TableName: 'ticket_alert';     //QM-771
      PrimaryKey: 'alert_id';
      CustomSQL: '';
      PurgeSQL: '';
    )
  ); // When adding new tables, also add them to TLocalCache.PopulateMissingTableData when appropriate

  //============================================================================

  LocalIndexes: array[0..62] of TLocalIndexDef = (
    (
      TableName: 'office';
      IndexName: 'office_name';
      Fields: 'office_name';
      Options: [];
    ),
    (
      TableName: 'client';
      IndexName: 'client_name';
      Fields: 'client_name';
      Options: [];
    ),
    (
      TableName: 'reference';
      IndexName: 'default';
      Fields: 'type;sortby';
      Options: [ixCaseInsensitive];
    ),
    (
      TableName: 'reference';
      IndexName: 'code';
      Fields: 'code';
      Options: [];
    ),
    (
      TableName: 'damage_estimate';
      IndexName: 'damage_id';
      Fields: 'damage_id';
      Options: [];
    ),
    (
      TableName: 'ticket';
      IndexName: 'kind';
      Fields: 'kind';
      Options: [];
    ),
    (
      TableName: 'ticket';
      IndexName: 'due_date';
      Fields: 'due_date';
      Options: [];
    ),
    (
      TableName: 'notes';
      IndexName: 'foreign_type_id';
      Fields: 'foreign_type;foreign_id';
      Options: [];
    ),
    (
      TableName: 'locate';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'locate';
      IndexName: 'closed';
      Fields: 'closed';
      Options: [];
    ),
    (
      TableName: 'locate';
      IndexName: 'locator_id';
      Fields: 'locator_id';
      Options: [];
    ),
    (
      TableName: 'response_log';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'locate_status';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'assignment';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'billing_detail';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'ticket_version';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'locate_hours';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'employee';
      IndexName: 'type_id';
      Fields: 'type_id';
      Options: [];
    ),
    (
      TableName: 'employee';
      IndexName: 'report_to';
      Fields: 'report_to';
      Options: [];
    ),
    (
      TableName: 'employee';
      IndexName: 'repr_pc_code';
      Fields: 'repr_pc_code';
      Options: [];
    ),
    (
      TableName: 'damage';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'damage_estimate';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'damage_invoice';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'locate';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'notes';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'locate_hours';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'damage_litigation';
      IndexName: 'damage_id';
      Fields: 'damage_id';
      Options: [];
    ),
    (
      TableName: 'damage_litigation';
      IndexName: 'carrier_id';
      Fields: 'carrier_id';
      Options: [];
    ),
    (
      TableName: 'carrier';
      IndexName: 'carrier_id';
      Fields: 'carrier_id';
      Options: [];
    ),
    (
      TableName: 'followup_ticket_type';
      IndexName: 'type_id';
      Fields: 'type_id';
      Options: [];
    ),
    (
      TableName: 'followup_ticket_type';
      IndexName: 'call_center';
      Fields: 'call_center';
      Options: [];
    ),
    (
      TableName: 'damage_history';
      IndexName: 'damage_id';
      Fields: 'damage_id';
      Options: [];
    ),
    (
      TableName: 'asset_assignment';
      IndexName: 'asset_id';
      Fields: 'asset_id';
      Options: [];
    ),
    (
      TableName: 'asset_assignment';
      IndexName: 'emp_id';
      Fields: 'emp_id';
      Options: [];
    ),
    (
      TableName: 'asset_type';
      IndexName: 'asset_code';
      Fields: 'asset_code';
      Options: [];
    ),
    (
      TableName: 'attachment';
      IndexName: 'foreign_id';
      Fields: 'foreign_id';
      Options: [];
    ),
    (
      TableName: 'attachment';
      IndexName: 'foreign_type';
      Fields: 'foreign_type';
      Options: [];
    ),
    (
      TableName: 'timesheet_entry';
      IndexName: 'work_emp_id_work_date_status';
      Fields: 'work_emp_id;work_date;status';
      Options: [];
    ),
    (
      TableName: 'locate_plat';
      IndexName: 'locate_id';
      Fields: 'locate_id';
      Options: [];
    ),
    (
      TableName: 'locate_plat';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'billing_invoice';
      IndexName: 'billing_header_id';
      Fields: 'billing_header_id';
      Options: [];
    ),
    (
      TableName: 'damage_bill';
      IndexName: 'damage_id';
      Fields: 'damage_id';
      Options: [];
    ),
    (
      TableName: 'damage_bill';
      IndexName: 'DeltaStatus';
      Fields: 'DeltaStatus';
      Options: [];
    ),
    (
      TableName: 'billing_output_config';
      IndexName: 'customer_id';
      Fields: 'customer_id';
      Options: [];
    ),
    (
      TableName: 'message_dest';
      IndexName: 'ack_date';
      Fields: 'ack_date';
      Options: [];
    ),
    (
      TableName: 'message_recip';
      IndexName: 'message_id';
      Fields: 'message_id';
      Options: [];
    ),
    (
      TableName: 'ticket_info';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'notes';
      IndexName: 'damage_id';
      Fields: 'damage_id';
      Options: [];
    ),
    (
      TableName: 'addin_info';
      IndexName: 'foreign_type_id';
      Fields: 'foreign_type;foreign_id';
      Options: [];
    ),
    (
      TableName: 'jobsite_arrival';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'email_queue_result';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'task_schedule';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
    (
      TableName: 'task_schedule';
      IndexName: 'est_start_date';
      Fields: 'est_start_date';
      Options: [];
    ),
    (
      TableName: 'work_order';
      IndexName: 'kind';
      Fields: 'kind';
      Options: [];
    ),
    (
      TableName: 'work_order';
      IndexName: 'due_date';
      Fields: 'due_date';
      Options: [];
    ),
    (
      TableName: 'wo_assignment';
      IndexName: 'assigned_to_id';
      Fields: 'assigned_to_id';
      Options: [];
    ),
    (
      TableName: 'wo_status_history';
      IndexName: 'wo_status_id';
      Fields: 'wo_status_id';
      Options: [];
    ),
    (
      TableName: 'wo_response_log';
      IndexName: 'wo_id';
      Fields: 'wo_id';
      Options: [];
    ),
    (
      TableName: 'work_order_version';
      IndexName: 'wo_id';
      Fields: 'wo_id';
      Options: [];
    ),
    (
      TableName: 'work_order_version';
      IndexName: 'transmit_date';
      Fields: 'transmit_date';
      Options: [];
    ),
    (
      TableName: 'ticket_risk';
      IndexName: 'ticket_id';
      Fields: 'ticket_id';
      Options: [];
    ),
//    (
//      TableName: 'custom_form_field';
//      IndexName: 'ff';
//      Fields: 'form_id;form_field_id;field_group;field_order';
//      Options: [];
//    ),
//    (
//     TableName: 'custom_form_answer';
//      IndexName: 'form_field_id';
//      Fields: 'form_field_id';
//      Options: [];
//    ),
//     (
//     TableName: 'custom_form_reference';
//      IndexName: 'form_id';
//      Fields: 'form_id;form_field_id';
//      Options: [];
//    )
//    ,
//    (
//      TableName: 'customer_locator';
//      IndexName: 'customer_locator_id';
//      Fields: 'customer_locator_id';
//      Options: [];
//    )
      (
        TableName: 'ticket_alert';
        IndexName: 'ticket_alert_ticket_id';
        Fields: 'ticket_id';
        Options: [];
      ),
      (
        TableName: 'ticket_alert';
        IndexName: 'ticket_alert_type';
        Fields: 'alert_type_code';
        Options: [];
      )
  ); // Primary keys do not need explicit indexes added here, they are implied/automatic

implementation

end.
