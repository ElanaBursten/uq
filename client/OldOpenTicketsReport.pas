unit OldOpenTicketsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, ComCtrls, 
  OdRangeSelect;

type
  TOldOpenTicketsReportForm = class(TReportBaseForm)
    TransmitDateRangeLabel: TLabel;
    TransmitDateRange: TOdRangeSelectFrame;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdVclUtils;

{$R *.dfm}

{ TOldOpenTicketsReportForm }

procedure TOldOpenTicketsReportForm.ValidateParams;
begin
  inherited;
  SetReportID('OldOpenTickets');
  SetParamDate('TransmitDateStart', TransmitDateRange.FromDate);
  SetParamDate('TransmitDateEnd', TransmitDateRange.ToDate);
end;

procedure TOldOpenTicketsReportForm.InitReportControls;
var
  StartDate: TDateTime;
  EndDate: TDateTime;
begin
  inherited;
  EndDate := IncMonth(Date, -2);
  StartDate := IncMonth(EndDate, -6);
  TransmitDateRange.SelectDateRange(rsCustom, StartDate, EndDate);
  SetEnabledOnControlAndChildren(TransmitDateRange, DM.UQState.DeveloperMode);
end;

end.
