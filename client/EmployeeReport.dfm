inherited EmployeeReportForm: TEmployeeReportForm
  Left = 460
  Top = 204
  Caption = 'Active / Inactive Employees'
  ClientHeight = 325
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 8
    Top = 13
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object EmployeeStatusLabel: TLabel [1]
    Left = 8
    Top = 43
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object LabelSortBy: TLabel [2]
    Left = 8
    Top = 73
    Width = 39
    Height = 13
    Caption = 'Sort by:'
  end
  object LabelEmployeeType: TLabel [3]
    Left = 8
    Top = 103
    Width = 75
    Height = 13
    Caption = 'Employee type:'
  end
  object ManagerCombo: TComboBox [4]
    Left = 94
    Top = 10
    Width = 231
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object EmployeeStatusCombo: TComboBox [5]
    Left = 94
    Top = 40
    Width = 129
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object SortByCombo: TComboBox [6]
    Left = 94
    Top = 70
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object EmployeeTypeList: TCheckListBox [7]
    Left = 94
    Top = 104
    Width = 251
    Height = 213
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 16
    Top = 288
  end
end
