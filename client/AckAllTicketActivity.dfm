object AckAllTicketActivityForm: TAckAllTicketActivityForm
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Ack All Ticket Activity'
  ClientHeight = 181
  ClientWidth = 313
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 297
    Height = 129
    Shape = bsFrame
  end
  object Label2: TLabel
    Left = 19
    Top = 68
    Width = 57
    Height = 13
    Caption = 'Cutoff Date'
  end
  object InstructionsLabel: TLabel
    Left = 19
    Top = 16
    Width = 276
    Height = 45
    AutoSize = False
    Caption = 
      'This will acknowledge all ticket activities for Jonathan Martin ' +
      'Smith-Campbell occurring before the selected Cutoff Date.'
    WordWrap = True
  end
  object AffectedRecsLabel: TLabel
    Left = 19
    Top = 100
    Width = 276
    Height = 33
    AutoSize = False
    Caption = 'Click OK to acknowledge 9999 unacknowledged activities.'
    WordWrap = True
  end
  object OKButton: TButton
    Left = 79
    Top = 148
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 159
    Top = 148
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object CutoffDate: TcxDateEdit
    Left = 114
    Top = 64
    Properties.DateButtons = [btnToday]
    Properties.DateOnError = deToday
    Properties.SaveTime = False
    Properties.OnChange = CutoffDateChange
    Properties.OnEditValueChanged = CutoffDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 2
    OnExit = CutoffDateExit
    Width = 122
  end
end
