unit ServerProxy;

// This unit uses the PIMPL idiom, used much more often with C++ than Delphi.
// In addition to keeping private things private, it helps compile time,
// which matters a lot for C++, and hardly at all for Delphi.

interface

uses
  Classes, SysUtils, OdWinInet, OdExceptions, OdMiscUtils;

type
  ELoginFailure = class(EOdNonLoggedException);

  TServer = class(TObject)
  private
    FDeveloperMode: Boolean;
  public
    // "Raw" methods
    function RoundTripCount: Integer; virtual; abstract;
    function GetReport(OutputType: string; Params: TStrings; RequestID: string): string; virtual; abstract;
    function GetBaseURL: string; virtual; abstract;
    property DeveloperMode: Boolean read FDeveloperMode write FDeveloperMode;
  end;

function CreateRemoteServer(LogEvent: TLogEvent; const BaseUrl: string): TServer;

implementation

uses
  Dialogs, Controls, OdHttpDialog;

// The rest of the app only knows about the interface above.  The
// implementation below is private
type
  TRemoteServer = class(TServer)
  private
    FLogEvent: TLogEvent;
    FBaseUrl: string;
    FRoundTripCount: Integer;
    constructor CreateWithLogger(LogEvent: TLogEvent; const BaseUrl: string);
    function HttpGet(const URL: string; Options: TOdHttpDetails = nil): string;
    // procedure AddToLog(const Msg: string);
    // function HttpPost(const URL, PostData: string): string;
  public
    function RoundTripCount: Integer; override;
    function GetReport(OutputType: string; Params: TStrings; RequestID: string): string; override;
    function GetBaseURL: string; override;
  end;

{ TRemoteServer }
constructor TRemoteServer.CreateWithLogger(LogEvent: TLogEvent; const BaseUrl: string);
begin
  inherited Create;
  FLogEvent := LogEvent;
  FBaseUrl := BaseUrl;
end;

function TRemoteServer.HttpGet(const URL: string; Options: TOdHttpDetails): string;
begin
  Inc(FRoundTripCount);
  Result := OdHttpGetString(FBaseUrl + URL, Options)
end;

{

** Not currently used, but may be helpful if ever switching from Get to Post

procedure TRemoteServer.AddToLog(const Msg: string);
begin
  if Assigned(FLogEvent) then
    FLogEvent(Self, Msg);
end;

function TRemoteServer.HttpPost(const URL, PostData: string): string;
var
  OdHttp: TOdHttp;
begin
  AddToLog(Format('Posting Packet Size: %d (before compression)', [Length(PostData)]));
  OdHttp := TOdHttp.Create;
  try
    OdHttp.ContentType := 'text/xml';
    OdHttp.URL := FBaseUrl + URL;
    OdHttp.Threaded := False;
    OdHttp.IdleMainThread := False;
    Inc(FRoundTripCount);
    OdHttp.SendString(PostData, Result);
    AddToLog(Format('Response Packet Size: %d (before compression)', [Length(Result)]));
  finally
    FreeAndNil(OdHttp);
  end;
end;

}

function CreateRemoteServer(LogEvent: TLogEvent; const BaseUrl: string): TServer;
begin
  Result := TRemoteServer.CreateWithLogger(LogEvent, BaseURL);
end;

function TRemoteServer.RoundTripCount: Integer;
begin
  Result := FRoundTripCount;
end;

function ParamsToUrl(Params: TStrings): string;
var
  i: Integer;
begin
  if Params.Count < 1 then
    raise Exception.Create('Error, No parameters were set');

  Result := '?' + Params[0];
  for i := 1 to Params.Count - 1 do
    Result := Result + '&' + Params[i];
end;

function TRemoteServer.GetReport(OutputType: string; Params: TStrings; RequestID: string): string;
var
  Url: string;
  //PostData: string;
begin
  Assert(Assigned(Params));
  Params.Add('RequestID=' + RequestID);
  Url := 'app/ReportEngineCGI.exe/' + OutputType;

  if True then begin        // Use legacy GET-based report invocation
    Result := HttpGet(Url + ParamsToUrl(Params));
  end else begin            // Use new POST-based report invocation
    //PostData := Params.Text;
    //Result := HttpPost(Url, PostData);
  end;
end;

function TRemoteServer.GetBaseURL: string;
begin
  Result := FBaseUrl;
end;

initialization
  SetDefaultHttpDetails(DefaultDialogHttp);

end.

