object BaseTimesheetDayFrame: TBaseTimesheetDayFrame
  Left = 0
  Top = 0
  Width = 124
  Height = 706
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object EditedFlagLabel: TLabel
    Left = 1
    Top = 1
    Width = 6
    Height = 13
    Caption = 'e'
    Visible = False
  end
  object BelowCalloutPanel: TPanel
    Left = 0
    Top = 220
    Width = 77
    Height = 463
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 0
    object BelowCalloutTopPanel: TPanel
      Left = 0
      Top = 0
      Width = 77
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CalloutHours: TLabel
        Left = 37
        Top = 13
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Callout'
        Color = clBtnFace
        ParentColor = False
        Transparent = True
      end
      object RegularHours: TLabel
        Left = 33
        Top = 26
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = 'Regular'
        Color = clBtnFace
        ParentColor = False
        Transparent = True
      end
      object OvertimeHours: TLabel
        Left = 26
        Top = 39
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Overtime'
        Color = clBtnFace
        ParentColor = False
        Transparent = True
      end
      object DoubleTimeHours: TLabel
        Left = 17
        Top = 52
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Doubletime'
        Color = clBtnFace
        ParentColor = False
        Transparent = True
      end
      object WorkHours: TLabel
        Left = 45
        Top = 1
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Work'
        Color = clBtnFace
        ParentColor = False
        Transparent = True
      end
    end
    object LeaveVacPanel: TPanel
      Left = 0
      Top = 65
      Width = 77
      Height = 43
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object lblDevOverride: TLabel
        Left = 3
        Top = 31
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object VacationHours: TcxDBCurrencyEdit
        Left = 17
        Top = 3
        DataBinding.DataField = 'vac_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00;-,0.00; '
        Properties.MaxLength = 5
        Properties.MaxValue = 24.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 0
        Width = 53
      end
      object SickLeaveHours: TcxDBComboBox
        Left = 16
        Top = 23
        DataBinding.DataField = 'leave_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.DropDownListStyle = lsFixedList
        TabOrder = 1
        Width = 54
      end
    end
    object PTOPanel: TPanel
      Left = 0
      Top = 108
      Width = 77
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object PTOHours: TcxDBComboBox
        Left = 16
        Top = 0
        DataBinding.DataField = 'pto_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.DropDownListStyle = lsFixedList
        TabOrder = 0
        Width = 54
      end
    end
    object BelowCalloutBottomPanel: TPanel
      Left = 0
      Top = 128
      Width = 77
      Height = 335
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
      object DayHours: TLabel
        Left = 41
        Top = 97
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object ApprovalMemo: TMemo
        Left = 0
        Top = 249
        Width = 81
        Height = 105
        TabStop = False
        BorderStyle = bsNone
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Approved Jan 31 '
          'by Michael Jones.  '
          'Final approval '
          'Feb '
          '31 by FCV '
          'Manager Test.')
        ParentFont = False
        ReadOnly = True
        TabOrder = 10
        Visible = False
      end
      object SubmitButton: TButton
        Left = 0
        Top = 225
        Width = 70
        Height = 24
        Action = SubmitAction
        TabOrder = 9
      end
      object BereavementHours: TcxDBCurrencyEdit
        Left = 17
        Top = 0
        DataBinding.DataField = 'br_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00;-,0.00; '
        Properties.MaxLength = 5
        Properties.MaxValue = 24.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 0
        Width = 53
      end
      object HolidayHours: TcxDBCurrencyEdit
        Left = 17
        Top = 19
        DataBinding.DataField = 'hol_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00;-,0.00; '
        Properties.MaxLength = 5
        Properties.MaxValue = 24.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 1
        Width = 53
      end
      object JuryDutyHours: TcxDBCurrencyEdit
        Left = 17
        Top = 38
        DataBinding.DataField = 'jury_hours'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0.00;-,0.00; '
        Properties.MaxLength = 5
        Properties.MaxValue = 24.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 2
        Width = 53
      end
      object MilesStart1: TcxDBCurrencyEdit
        Left = 0
        Top = 179
        DataBinding.DataField = 'miles_start1'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0;-,0; '
        Properties.MaxLength = 6
        Properties.MaxValue = 999999.000000000000000000
        Properties.MinValue = 1.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 7
        Visible = False
        OnExit = MilesStart1Exit
        Width = 70
      end
      object MilesStop1: TcxDBCurrencyEdit
        Left = 1
        Top = 202
        DataBinding.DataField = 'miles_stop1'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.Alignment.Horz = taRightJustify
        Properties.DisplayFormat = ',0;-,0; '
        Properties.MaxLength = 6
        Properties.MaxValue = 999999.000000000000000000
        Properties.MinValue = 1.000000000000000000
        Properties.ReadOnly = False
        TabOrder = 8
        Visible = False
        Width = 70
      end
      object FloatingHoliday: TDBCheckBox
        Left = 17
        Top = 59
        Width = 50
        Height = 19
        Color = clBtnFace
        DataField = 'floating_holiday'
        DataSource = TimesheetSource
        Enabled = False
        ParentColor = False
        TabOrder = 3
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        Visible = False
        OnClick = FloatingHolidayClick
      end
      object VehicleUse: TcxDBComboBox
        Left = 0
        Top = 156
        DataBinding.DataField = 'vehicle_use'
        DataBinding.DataSource = TimesheetSource
        Enabled = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ReadOnly = False
        TabOrder = 6
        Width = 70
      end
      object EmployeeType: TComboBox
        Left = 0
        Top = 111
        Width = 70
        Height = 21
        Style = csDropDownList
        Enabled = False
        ItemHeight = 13
        TabOrder = 4
        OnSelect = EmployeeTypeSelect
      end
      object ReasonChanged: TcxComboBox
        Left = 0
        Top = 133
        Enabled = False
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 9
        Properties.ReadOnly = False
        Properties.OnChange = ReasonChangedPropertiesChange
        TabOrder = 5
        Width = 70
      end
      object PerDiem: TDBCheckBox
        Left = 17
        Top = 77
        Width = 59
        Height = 19
        Color = clBtnFace
        DataField = 'per_diem'
        DataSource = TimesheetSource
        ParentColor = False
        TabOrder = 11
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        OnClick = PerDiemClick
      end
    end
  end
  object SubmitHint: TStaticText
    Left = 46
    Top = 18
    Width = 69
    Height = 74
    Alignment = taCenter
    AutoSize = False
    BevelOuter = bvNone
    Caption = 'Submit yesterday'#39's time before entering today'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ShowAccelChar = False
    TabOrder = 1
    Visible = False
  end
  object NothingButton: TButton
    Left = 119
    Top = 1
    Width = 2
    Height = 2
    Action = NothingAction
    TabOrder = 2
    TabStop = False
  end
  object TimesheetSource: TDataSource
    DataSet = TimesheetData
    OnDataChange = TimesheetSourceDataChange
    Top = 128
  end
  object TimesheetData: TDBISAMTable
    AfterInsert = TimesheetDataAfterInsert
    BeforePost = TimesheetDataBeforePost
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'timesheet_entry'
    Top = 99
  end
  object DayActions: TActionList
    OnUpdate = DayActionsUpdate
    Left = 4
    Top = 268
    object SubmitAction: TAction
      Caption = 'Submit'
      OnExecute = SubmitActionExecute
    end
    object NothingAction: TAction
      Caption = 'Nothing'
    end
  end
  object DefaultEditStyleController: TcxDefaultEditStyleController
    Style.BorderStyle = ebsUltraFlat
    Style.Color = clWindow
    Style.LookAndFeel.Kind = lfFlat
    Style.LookAndFeel.NativeStyle = True
    Style.TransparentBorder = True
    StyleDisabled.Color = clBtnFace
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.NativeStyle = True
    Top = 159
    PixelsPerInch = 96
  end
end
