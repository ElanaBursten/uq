inherited EmployeeActivityReportForm: TEmployeeActivityReportForm
  Left = 448
  Top = 392
  Caption = 'EmployeeActivityReportForm'
  ClientHeight = 440
  ClientWidth = 361
  PixelsPerInch = 96
  TextHeight = 13
  object lblManager: TLabel [0]
    Left = 0
    Top = 139
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object lbIncludeEmployee: TLabel [1]
    Left = 0
    Top = 166
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object EmployeeLabel: TLabel [2]
    Left = 0
    Top = 356
    Width = 50
    Height = 13
    Caption = 'Employee:'
  end
  object Label1: TLabel [3]
    Left = 0
    Top = 65
    Width = 100
    Height = 13
    Caption = 'Activity Date Range:'
  end
  object ContentsLabel: TLabel [4]
    Left = 0
    Top = 405
    Width = 48
    Height = 13
    Caption = 'Contents:'
  end
  object ManagersComboBox: TComboBox [5]
    Left = 91
    Top = 136
    Width = 214
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 0
    OnChange = ManagersComboBoxChange
  end
  object EmployeeStatusCombo: TComboBox [6]
    Left = 91
    Top = 163
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object ActivitiesGroupBox: TGroupBox [7]
    Left = 0
    Top = 188
    Width = 323
    Height = 126
    Caption = 'Activities'
    TabOrder = 2
    object lblFrom: TLabel
      Left = 28
      Top = 94
      Width = 28
      Height = 13
      Caption = 'From:'
    end
    object lblTo: TLabel
      Left = 141
      Top = 94
      Width = 16
      Height = 13
      Caption = 'To:'
    end
    object rbOutOfHorkingHours: TRadioButton
      Left = 8
      Top = 20
      Width = 161
      Height = 17
      Caption = 'Activities out of working hours'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbOutOfHorkingHoursClick
    end
    object rbAllActivities: TRadioButton
      Left = 8
      Top = 44
      Width = 169
      Height = 17
      Caption = 'All activities'
      TabOrder = 1
      OnClick = rbOutOfHorkingHoursClick
    end
    object rbOutOfSpanHours: TRadioButton
      Left = 8
      Top = 68
      Width = 169
      Height = 17
      Caption = 'Activities out of specific hours'
      TabOrder = 2
      OnClick = rbOutOfHorkingHoursClick
    end
    object dtpHourSpanStart: TDateTimePicker
      Left = 61
      Top = 91
      Width = 70
      Height = 21
      Date = 2.250000000000000000
      Format = 'hh:mm tt'
      Time = 2.250000000000000000
      Kind = dtkTime
      TabOrder = 3
    end
    object dtpHourSpanEnd: TDateTimePicker
      Left = 160
      Top = 91
      Width = 70
      Height = 21
      Date = 2.750000000000000000
      Format = 'hh:mm tt'
      Time = 2.750000000000000000
      Kind = dtkTime
      TabOrder = 4
    end
  end
  inline RangeSelect: TOdRangeSelectFrame [8]
    Left = 2
    Top = 81
    Width = 303
    Height = 52
    TabOrder = 3
    inherited FromLabel: TLabel
      Left = 40
      Top = 4
    end
    inherited ToLabel: TLabel
      Left = 180
      Top = 4
    end
    inherited DatesLabel: TLabel
      Left = 40
      Top = 30
    end
    inherited DatesComboBox: TComboBox
      Left = 89
      Top = 27
      Width = 200
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 89
      Top = 1
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 201
      Top = 1
    end
  end
  object AllCheckBox: TCheckBox
    Left = 8
    Top = 320
    Width = 282
    Height = 17
    Caption = 'Activity for all of this manager'#39's employees'
    TabOrder = 4
    OnClick = AllCheckBoxClick
  end
  object LocatorComboBox: TComboBox
    Left = 91
    Top = 353
    Width = 232
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 5
  end
  object ReportTypeGroupBox: TGroupBox
    Left = 0
    Top = 3
    Width = 323
    Height = 56
    Caption = 'Report Type'
    TabOrder = 6
    object rbActivityDetail: TRadioButton
      Left = 8
      Top = 15
      Width = 165
      Height = 17
      Caption = 'Activity Detail'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbActivityDetailClick
    end
    object rbActivitySummary: TRadioButton
      Left = 8
      Top = 34
      Width = 169
      Height = 17
      Caption = 'Activity Summary'
      TabOrder = 1
      OnClick = rbActivitySummaryClick
    end
  end
  object AttachLSACheckBox: TCheckBox
    Left = 8
    Top = 379
    Width = 282
    Height = 17
    Caption = 'Attach Locate Status Activity Report'
    TabOrder = 7
  end
  object IncludeChartCombo: TComboBox
    Left = 91
    Top = 402
    Width = 145
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 8
    Text = 'List'
    Items.Strings = (
      'List'
      'Graph'
      'Both')
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 312
    Top = 12
  end
end
