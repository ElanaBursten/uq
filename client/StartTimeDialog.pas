unit StartTimeDialog;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, DateUtils, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxSpinEdit, cxTimeEdit, ComCtrls,
  dxCore, cxDateUtils;

type
  TTaskTimeDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    InstructionsLabel: TLabel;
    ErrorLabel: TLabel;
    DateEdit: TcxDateEdit;
    TimeEdit: TcxTimeEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function GetStartDateTime: TDateTime;
    procedure SetStartDateTime(const Value: TDateTime);
    function ValidStartDateTime: Boolean;
    procedure ClearDateTime;
    function GetCanEditDate: Boolean;
    procedure SetCanEditDate(const Value: Boolean);
  public
    property CanEditDate: Boolean read GetCanEditDate write SetCanEditDate;
    property StartDateTime: TDateTime read GetStartDateTime write SetStartDateTime;
  end;

function PromptForTaskStartTime(var StartTime: TDateTime): Boolean;

implementation

uses
  OdMiscUtils, OdVclUtils;

// TODO: Very rudimentary determination of next working day.
// Some type of working day calendar is needed for a more robust function.
function GetNextWorkingDay: TDateTime;
begin
  case DayOfTheWeek(Today) of
    DayFriday :   Result := IncDay(Today, 3);
    DaySaturday : Result := IncDay(Today, 2);
    else          Result := Tomorrow;
  end;
end;

function PromptForTaskStartTime(var StartTime: TDateTime): Boolean;
var
  Dialog: TTaskTimeDialog;
begin
  Dialog := TTaskTimeDialog.Create(nil);
  try
    Dialog.ClearDateTime;
    Dialog.DateEdit.Date := GetNextWorkingDay;
    Result := Dialog.ShowModal = mrOk;
    if Result then
      StartTime := Dialog.StartDateTime;
  finally
    FreeAndNil(Dialog);
  end;
end;

{$R *.dfm}

function TTaskTimeDialog.GetStartDateTime: TDateTime;
begin
  Result := Trunc(DateEdit.Date) + Frac(TimeEdit.Time);
end;

procedure TTaskTimeDialog.SetStartDateTime(const Value: TDateTime);
begin
  DateEdit.Date := Trunc(Value);
  TimeEdit.Time := Frac(Value);
end;

function TTaskTimeDialog.ValidStartDateTime: Boolean;
begin
  Result := (StartDateTime > 0);
  if not Result then begin
    FocusControl(TimeEdit);
    ErrorLabel.Caption := 'A date and time are required.';
  end else begin
    Result := (DaysBetween(StartDateTime, Date) in [1..5]);   //QMANTWO-232
    if not Result then begin
      FocusControl(DateEdit);
      ErrorLabel.Caption := 'Date should be 1 to 5 days in the future.';  //QMANTWO-232
    end;
  end;
end;

procedure TTaskTimeDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := ((ModalResult = mrOk) and ValidStartDateTime) or (ModalResult = mrCancel);
end;

procedure TTaskTimeDialog.ClearDateTime;
begin
  DateEdit.Date := NullDate;
  TimeEdit.Time := NullDate;
end;

function TTaskTimeDialog.GetCanEditDate: Boolean;
begin
  Result := not DateEdit.Properties.ReadOnly;
end;

procedure TTaskTimeDialog.SetCanEditDate(const Value: Boolean);
begin
  if Value then begin
    DateEdit.Properties.ReadOnly := False;
    DateEdit.TabStop := True;
    DateEdit.Enabled := True;
  end else begin
    DateEdit.Properties.ReadOnly := True;
    DateEdit.TabStop := False;
    DateEdit.Enabled := False;
  end;
end;

procedure TTaskTimeDialog.FormShow(Sender: TObject);
begin
  ErrorLabel.Caption := '';
  CanEditDate := (DayOfTheWeek(Date) in [DayFriday, DaySaturday]);
end;

procedure TTaskTimeDialog.FormCreate(Sender: TObject);
begin
  SetFontBold(ErrorLabel);
end;

end.
