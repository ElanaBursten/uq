inherited ResponderStatusTranslationsReportForm: TResponderStatusTranslationsReportForm
  Caption = 'ResponderStatusTranslationsReportForm'
  ClientHeight = 613
  ClientWidth = 523
  PixelsPerInch = 96
  TextHeight = 13
  object lblResponderActive: TLabel [0]
    Left = 1
    Top = 30
    Width = 68
    Height = 13
    Caption = 'Active Status:'
  end
  object Label3: TLabel [1]
    Left = 4
    Top = 71
    Width = 65
    Height = 20
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  object ActiveStatusCombo: TComboBox [2]
    Left = 80
    Top = 27
    Width = 125
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object CallCenterList: TCheckListBox [3]
    Left = 80
    Top = 68
    Width = 275
    Height = 525
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 1
  end
  object SelectAllButton: TButton [4]
    Left = 372
    Top = 66
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 2
    OnClick = SelectAllButtonClick
  end
  object SelectNoneButton: TButton [5]
    Left = 372
    Top = 97
    Width = 75
    Height = 25
    Caption = 'Select None'
    TabOrder = 3
    OnClick = SelectNoneButtonClick
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 16
    Top = 560
  end
end
