inherited DamageDetailsForm: TDamageDetailsForm
  Left = 140
  Top = 64
  Caption = 'Damage Detail'
  ClientHeight = 577
  ClientWidth = 1128
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 33
    Width = 1128
    Height = 544
    ActivePage = DamageTab
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    TabWidth = 150
    OnChange = PageControlChange
    OnChanging = PageControlChanging
    object DamageTab: TTabSheet
      Caption = 'Damage'
      OnShow = DamageTabShow
      object ConfirmationPanel: TPanel
        Left = 116
        Top = 96
        Width = 381
        Height = 101
        TabOrder = 0
        object CreatedDamageNumLabel: TLabel
          Left = 17
          Top = 28
          Width = 227
          Height = 13
          Caption = 'Created damage investigation request number:'
        end
        object ConfirmButton: TButton
          Left = 153
          Top = 64
          Width = 75
          Height = 25
          Caption = 'OK'
          TabOrder = 0
          OnClick = ConfirmButtonClick
        end
        object InvestigationReqEdit: TEdit
          Left = 244
          Top = 24
          Width = 121
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
          Text = '<unknown>'
        end
      end
      object DamageDetailsPanel: TPanel
        Left = 0
        Top = 0
        Width = 1120
        Height = 516
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object DamagePageControl: TPageControl
          Left = 0
          Top = 0
          Width = 1120
          Height = 516
          ActivePage = NewDamageTab
          Align = alClient
          TabOrder = 0
          OnChange = DamagePageControlChange
          object NewDamageTab: TTabSheet
            Caption = 'Damage'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ImageIndex = 5
            ParentFont = False
            object MainPanel: TPanel
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              FullRepaint = False
              TabOrder = 0
              DesignSize = (
                1112
                488)
              object DamageDateLabel: TLabel
                Left = 6
                Top = 6
                Width = 111
                Height = 13
                Caption = 'Date Facility Damaged:'
              end
              object LocateMarksPresentLabel: TLabel
                Left = 328
                Top = 270
                Width = 107
                Height = 13
                Caption = 'Locate Marks Present:'
              end
              object LocateRequestedLabel: TLabel
                Left = 328
                Top = 222
                Width = 91
                Height = 13
                Caption = 'Locate Requested:'
              end
              object NotifierContactNumberLabel: TLabel
                Left = 6
                Top = 54
                Width = 120
                Height = 13
                Caption = 'Notifier Contact Number:'
              end
              object LocationDetailsLabel: TLabel
                Left = 6
                Top = 198
                Width = 79
                Height = 13
                Caption = 'Location Details:'
              end
              object OtherRemarksLabel: TLabel
                Left = 6
                Top = 317
                Width = 125
                Height = 13
                Caption = 'Other Remarks (optional):'
              end
              object UtilityCoClaimNumLabel: TLabel
                Left = 6
                Top = 291
                Width = 139
                Height = 13
                Caption = 'Utility Co. Claim # (optional):'
              end
              object InvestigatorLabel: TLabel
                Left = 6
                Top = 244
                Width = 63
                Height = 13
                Caption = 'Investigator:'
              end
              object TicketNumLabel: TLabel
                Left = 328
                Top = 246
                Width = 92
                Height = 13
                Caption = 'Ticket # (optional):'
              end
              object CityLabel: TLabel
                Left = 390
                Top = 150
                Width = 23
                Height = 13
                Alignment = taRightJustify
                Caption = 'City:'
              end
              object CountyLabel: TLabel
                Left = 238
                Top = 150
                Width = 39
                Height = 13
                Alignment = taRightJustify
                Caption = 'County:'
              end
              object StateLabel: TLabel
                Left = 106
                Top = 150
                Width = 30
                Height = 13
                Alignment = taRightJustify
                Caption = 'State:'
              end
              object DamageLocationLabel: TLabel
                Left = 6
                Top = 150
                Width = 82
                Height = 13
                Caption = 'Damage Location'
              end
              object ExcavatorLabel: TLabel
                Left = 6
                Top = 222
                Width = 136
                Height = 13
                Caption = 'Excavator Causing Damage:'
              end
              object GridLabel: TLabel
                Left = 227
                Top = 174
                Width = 50
                Height = 13
                Alignment = taRightJustify
                Caption = 'Grid (opt):'
              end
              object MapPageLabel: TLabel
                Left = 6
                Top = 174
                Width = 100
                Height = 13
                Caption = 'Map Page (optional):'
              end
              object OtherFacTypeSize: TLabel
                Left = 6
                Top = 126
                Width = 118
                Height = 13
                Caption = 'Other Facility Type/Size:'
              end
              object UtilityCoDamagedLabel: TLabel
                Left = 309
                Top = 54
                Width = 99
                Height = 13
                Caption = 'Utility Co. Damaged:'
              end
              object NotifiedByCompanyLabel: TLabel
                Left = 309
                Top = 30
                Width = 104
                Height = 13
                Caption = 'Notified By Company:'
              end
              object NotifiedByPersonLabel: TLabel
                Left = 6
                Top = 30
                Width = 92
                Height = 13
                Caption = 'Notified By Person:'
              end
              object NotifiedDateLabel: TLabel
                Left = 309
                Top = 5
                Width = 67
                Height = 13
                Caption = 'Notified Date:'
              end
              object NotifiedDate: TDBText
                Left = 423
                Top = 5
                Width = 70
                Height = 13
                AutoSize = True
                DataField = 'uq_notified_date'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ProfitCenterLabel: TLabel
                Left = 6
                Top = 79
                Width = 66
                Height = 13
                Caption = 'Profit Center:'
              end
              object InvestigatorNameLabel: TLabel
                Left = 150
                Top = 244
                Width = 135
                Height = 17
                AutoSize = False
                Caption = 'Select Investigator... -->'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LocatorLabel: TLabel
                Left = 6
                Top = 268
                Width = 40
                Height = 13
                Caption = 'Locator:'
              end
              object LocatorNameLabel: TLabel
                Left = 150
                Top = 268
                Width = 135
                Height = 17
                AutoSize = False
                Caption = 'Select Locator... -->'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object CallCenterLabel: TLabel
                Left = 618
                Top = 6
                Width = 57
                Height = 13
                Caption = 'Call Center:'
              end
              object ClientLabel: TLabel
                Left = 618
                Top = 30
                Width = 31
                Height = 13
                Caption = 'Client:'
              end
              object ClientCodeLabel: TLabel
                Left = 618
                Top = 54
                Width = 59
                Height = 13
                Caption = 'Client Code:'
              end
              object FacilityTypeLabel: TLabel
                Left = 676
                Top = 104
                Width = 64
                Height = 13
                Caption = 'Facility Type:'
              end
              object FacilitySizeLabel: TLabel
                Left = 831
                Top = 104
                Width = 23
                Height = 13
                Alignment = taRightJustify
                Caption = 'Size:'
              end
              object FacilityMaterialLabel: TLabel
                Left = 944
                Top = 104
                Width = 42
                Height = 13
                Alignment = taRightJustify
                Caption = 'Material:'
              end
              object Label25: TLabel
                Left = 626
                Top = 126
                Width = 7
                Height = 13
                Caption = '1'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label26: TLabel
                Left = 626
                Top = 153
                Width = 7
                Height = 13
                Caption = '2'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label27: TLabel
                Left = 626
                Top = 180
                Width = 7
                Height = 13
                Caption = '3'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label28: TLabel
                Left = 1031
                Top = 127
                Width = 49
                Height = 13
                Caption = '*Required'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object State: TDBComboBox
                Left = 148
                Top = 146
                Width = 78
                Height = 21
                CharCase = ecUpperCase
                DataField = 'state'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 7
                OnExit = LocationExit
              end
              object LocateMarksPresent: TDBComboBox
                Left = 440
                Top = 266
                Width = 166
                Height = 21
                Style = csDropDownList
                DataField = 'locate_marks_present'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 16
                OnKeyUp = YNUBControlKeyUp
              end
              object LocateRequested: TDBComboBox
                Left = 440
                Top = 218
                Width = 166
                Height = 21
                Style = csDropDownList
                DataField = 'locate_requested'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 14
                OnKeyUp = YNUBControlKeyUp
              end
              object NotifiedByPhone: TDBEdit
                Left = 148
                Top = 52
                Width = 139
                Height = 21
                DataField = 'notified_by_phone'
                DataSource = DamageSource
                TabOrder = 2
              end
              object LocationDetails: TDBEdit
                Left = 148
                Top = 194
                Width = 457
                Height = 21
                DataField = 'location'
                DataSource = DamageSource
                TabOrder = 12
              end
              object UtilityCoClaimNum: TDBEdit
                Left = 148
                Top = 287
                Width = 158
                Height = 21
                DataField = 'client_claim_id'
                DataSource = DamageSource
                MaxLength = 30
                TabOrder = 17
              end
              object ExcavatorCompany: TDBEdit
                Left = 148
                Top = 218
                Width = 158
                Height = 21
                DataField = 'excavator_company'
                DataSource = DamageSource
                TabOrder = 13
              end
              object Grid: TDBEdit
                Left = 286
                Top = 170
                Width = 98
                Height = 21
                DataField = 'grid'
                DataSource = DamageSource
                TabOrder = 11
              end
              object Page: TDBEdit
                Left = 148
                Top = 170
                Width = 78
                Height = 21
                DataField = 'page'
                DataSource = DamageSource
                TabOrder = 10
              end
              object City: TDBEdit
                Left = 424
                Top = 146
                Width = 181
                Height = 21
                DataField = 'city'
                DataSource = DamageSource
                TabOrder = 9
                OnExit = LocationExit
              end
              object County: TDBEdit
                Left = 286
                Top = 146
                Width = 98
                Height = 21
                DataField = 'county'
                DataSource = DamageSource
                TabOrder = 8
                OnExit = LocationExit
              end
              object FacilityTypeSize: TDBEdit
                Left = 148
                Top = 123
                Width = 457
                Height = 21
                DataField = 'size_type'
                DataSource = DamageSource
                TabOrder = 6
              end
              object NotifiedByCompany: TDBEdit
                Left = 424
                Top = 26
                Width = 181
                Height = 21
                DataField = 'notified_by_company'
                DataSource = DamageSource
                TabOrder = 3
              end
              object NotifiedByPerson: TDBEdit
                Left = 148
                Top = 26
                Width = 139
                Height = 21
                DataField = 'notified_by_person'
                DataSource = DamageSource
                TabOrder = 1
              end
              object Remarks: TDBMemo
                Left = 148
                Top = 312
                Width = 980
                Height = 182
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataField = 'remarks'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 18
              end
              object ProfitCenter: TDBComboBox
                Left = 148
                Top = 74
                Width = 139
                Height = 21
                Style = csDropDownList
                DataField = 'profit_center'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 4
                OnChange = ProfitCenterChange
              end
              object ChooseInvButton: TButton
                Left = 289
                Top = 242
                Width = 17
                Height = 19
                Caption = '...'
                TabOrder = 19
                OnClick = ChooseInvButtonClick
              end
              object InvestigatorComboBox: TComboBox
                Left = 264
                Top = 242
                Width = 25
                Height = 21
                ItemHeight = 0
                TabOrder = 20
                Text = 'InvestigatorComboBox'
                Visible = False
              end
              object LocatorCombo: TComboBox
                Left = 264
                Top = 266
                Width = 25
                Height = 21
                ItemHeight = 0
                TabOrder = 21
                Text = 'InvestigatorComboBox'
                Visible = False
              end
              object LookupLocatorButton: TButton
                Left = 289
                Top = 266
                Width = 17
                Height = 19
                Caption = '...'
                TabOrder = 22
                OnClick = LookupLocatorButtonClick
              end
              object ClearLocatorButton: TButton
                Left = 306
                Top = 266
                Width = 17
                Height = 19
                Hint = 'Clear Selected Locator'
                Caption = 'X'
                TabOrder = 23
                TabStop = False
                OnClick = ClearLocatorButtonClick
              end
              object DamageDate: TcxDBDateEdit
                Left = 148
                Top = 3
                DataBinding.DataField = 'damage_date'
                DataBinding.DataSource = DamageSource
                Properties.ButtonGlyph.Data = {
                  C2030000424DC203000000000000B6000000280000000F0000000D0000000100
                  2000000000000C03000000000000000000001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
                  8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
                  00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
                  000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00808080008080000080800000808080008080800080808000808080008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080800000}
                Properties.InputKind = ikMask
                TabOrder = 0
                Width = 140
              end
              object TicketNumber: TcxDBButtonEdit
                Left = 440
                Top = 242
                DataBinding.DataField = 'ticket_number'
                DataBinding.DataSource = DamageSource
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ReadOnly = True
                Properties.OnButtonClick = TicketNumberButtonClick
                Properties.OnChange = TicketNumberChange
                TabOrder = 15
                OnKeyUp = TicketNumberKeyUp
                Width = 166
              end
              object CallCenters: TComboBox
                Left = 700
                Top = 5
                Width = 291
                Height = 21
                Style = csDropDownList
                DropDownCount = 14
                ItemHeight = 0
                TabOrder = 24
                OnChange = CallCentersChange
              end
              object ClientID: TcxDBExtLookupComboBox
                Left = 700
                Top = 27
                DataBinding.DataField = 'client_name'
                DataBinding.DataSource = DamageSource
                Properties.DropDownRows = 15
                Properties.DropDownSizeable = True
                Properties.View = ClientFieldsView
                Properties.KeyFieldNames = 'Client_ID'
                Properties.ListFieldItem = ClientFieldsViewClient_Name
                Properties.OnCloseUp = ClientIDPropertiesCloseUp
                TabOrder = 25
                Width = 291
              end
              object UtilityCoDamaged: TEdit
                Left = 423
                Top = 52
                Width = 181
                Height = 21
                Color = 14540253
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 5
              end
              object ClientCode: TEdit
                Left = 700
                Top = 52
                Width = 162
                Height = 21
                Color = 14540253
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 26
              end
              object FacilityMaterial: TDBComboBox
                Left = 907
                Top = 123
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_material'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 29
              end
              object FacilityMaterial2: TDBComboBox
                Left = 907
                Top = 150
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_material_2'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 32
              end
              object FacilityMaterial3: TDBComboBox
                Left = 907
                Top = 177
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_material_3'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 35
              end
              object FacilitySize3: TDBComboBox
                Left = 784
                Top = 177
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_size_3'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 34
              end
              object FacilitySize2: TDBComboBox
                Left = 784
                Top = 150
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_size_2'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 31
              end
              object FacilitySize: TDBComboBox
                Left = 784
                Top = 123
                Width = 117
                Height = 21
                Style = csDropDownList
                DataField = 'facility_size'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 28
              end
              object FacilityType: TDBComboBox
                Left = 639
                Top = 123
                Width = 139
                Height = 21
                Style = csDropDownList
                DataField = 'facility_type'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 27
                OnChange = FacilityTypeChange
              end
              object FacilityType2: TDBComboBox
                Left = 639
                Top = 150
                Width = 139
                Height = 21
                Style = csDropDownList
                DataField = 'facility_type_2'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 30
                OnChange = FacilityTypeChange
              end
              object FacilityType3: TDBComboBox
                Left = 639
                Top = 177
                Width = 139
                Height = 21
                Style = csDropDownList
                DataField = 'facility_type_3'
                DataSource = DamageSource
                DropDownCount = 14
                ItemHeight = 13
                TabOrder = 33
                OnChange = FacilityTypeChange
              end
            end
          end
          object DetailsTab: TTabSheet
            Caption = 'Damage Details'
            object DateMailedLabel: TLabel
              Left = 9
              Top = 187
              Width = 60
              Height = 13
              Caption = 'Date Mailed:'
            end
            object DateFaxedLabel: TLabel
              Left = 9
              Top = 213
              Width = 60
              Height = 13
              Caption = 'Date Faxed:'
            end
            object SentToLabel: TLabel
              Left = 9
              Top = 237
              Width = 41
              Height = 13
              Caption = 'Sent To:'
              FocusControl = SentTo
            end
            object ExcavatorTypeLabel: TLabel
              Left = 326
              Top = 48
              Width = 80
              Height = 13
              Caption = 'Excavator Type:'
            end
            object ExcavationTypeLabel: TLabel
              Left = 326
              Top = 72
              Width = 84
              Height = 13
              Caption = 'Excavation Type:'
            end
            object DiagramNumberLabel: TLabel
              Left = 326
              Top = 24
              Width = 83
              Height = 13
              Caption = 'Diagram Number:'
            end
            object DamageID: TDBText
              Left = 130
              Top = 20
              Width = 60
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'uq_damage_id'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object DamageIDLabel: TLabel
              Left = 9
              Top = 20
              Width = 54
              Height = 13
              Caption = 'Damage #:'
            end
            object DueDate: TDBText
              Left = 130
              Top = 68
              Width = 49
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'due_date'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object DueDateLabel: TLabel
              Left = 9
              Top = 68
              Width = 115
              Height = 13
              Caption = 'Investigation Due Date:'
            end
            object IntelexNum: TLabel
              Left = 9
              Top = 44
              Width = 49
              Height = 13
              Caption = 'Intelex #:'
            end
            object ClosedDateLabel: TLabel
              Left = 9
              Top = 92
              Width = 115
              Height = 13
              Caption = 'Investigation Complete:'
            end
            object ClosedDate: TDBText
              Left = 130
              Top = 92
              Width = 64
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'closed_date'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object LocatorExperienceLabel: TLabel
              Left = 326
              Top = 95
              Width = 96
              Height = 13
              Caption = 'Locator Experience:'
            end
            object LocateEquipmentLabel: TLabel
              Left = 326
              Top = 118
              Width = 89
              Height = 13
              Caption = 'Locate Equipment:'
            end
            object TicketDueDate: TLabel
              Left = 130
              Top = 140
              Width = 84
              Height = 13
              Caption = 'TicketDueDate'
              Color = clBtnFace
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object TicketDueDateLabel: TLabel
              Left = 9
              Top = 140
              Width = 80
              Height = 13
              Caption = 'Ticket Due Date:'
            end
            object ClaimStatusLabel: TLabel
              Left = 326
              Top = 193
              Width = 105
              Height = 13
              Caption = 'Damage Claim Status:'
            end
            object ClaimStatusDateLabel: TLabel
              Left = 326
              Top = 213
              Width = 89
              Height = 13
              Caption = 'Claim Status Date:'
            end
            object ClaimStatusDate: TDBText
              Left = 438
              Top = 213
              Width = 95
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'claim_status_date'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object InvoiceCodeLabel: TLabel
              Left = 326
              Top = 233
              Width = 67
              Height = 13
              Caption = 'Accrual Code:'
            end
            object Label1: TLabel
              Left = 9
              Top = 163
              Width = 50
              Height = 13
              Caption = 'Added By:'
            end
            object AddedByName: TDBText
              Left = 130
              Top = 163
              Width = 82
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'added_by_name'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object ExcavatorDoingRepairsLabel: TLabel
              Left = 326
              Top = 169
              Width = 109
              Height = 13
              Caption = 'Excavator for Repairs:'
              FocusControl = ExcavatorDoingRepairs
            end
            object ClaimCoordinatorLabel: TLabel
              Left = 326
              Top = 258
              Width = 89
              Height = 13
              Caption = 'Claim Coordinator:'
            end
            object ApprovedDateLabel: TLabel
              Left = 9
              Top = 116
              Width = 77
              Height = 13
              Caption = 'Approved Date:'
            end
            object ApprovedDate: TDBText
              Left = 130
              Top = 116
              Width = 82
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'approved_datetime'
              DataSource = DamageSource
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object SentTo: TDBEdit
              Left = 130
              Top = 233
              Width = 147
              Height = 21
              DataField = 'sent_to'
              DataSource = DamageSource
              TabOrder = 2
            end
            object ExcavatorType: TDBComboBox
              Left = 438
              Top = 43
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'excavator_type'
              DataSource = DamageSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 4
            end
            object ExcavationType: TDBComboBox
              Left = 438
              Top = 67
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'excavation_type'
              DataSource = DamageSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 5
            end
            object DateMailed: TDBEdit
              Left = 130
              Top = 182
              Width = 147
              Height = 21
              DataField = 'date_mailed'
              DataSource = DamageSource
              TabOrder = 0
            end
            object DateFaxed: TDBEdit
              Left = 130
              Top = 209
              Width = 147
              Height = 21
              DataField = 'date_faxed'
              DataSource = DamageSource
              TabOrder = 1
            end
            object DiagramNumber: TDBEdit
              Left = 438
              Top = 17
              Width = 162
              Height = 21
              DataField = 'diagram_number'
              DataSource = DamageSource
              TabOrder = 3
            end
            object LocatorExperience: TDBComboBox
              Left = 438
              Top = 91
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'locator_experience'
              DataSource = DamageSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 6
            end
            object LocateEquipment: TDBComboBox
              Left = 438
              Top = 115
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'locate_equipment'
              DataSource = DamageSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 7
            end
            object LocateWasAProject: TDBCheckBox
              Left = 326
              Top = 143
              Width = 125
              Height = 17
              Alignment = taLeftJustify
              Caption = 'Locate was a Project:'
              DataField = 'was_project'
              DataSource = DamageSource
              TabOrder = 8
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
            object ClaimStatus: TDBComboBox
              Left = 438
              Top = 189
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'claim_status'
              DataSource = DamageSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 10
            end
            object InvoiceCode: TDBComboBox
              Left = 438
              Top = 229
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'invoice_code'
              DataSource = DamageSource
              ItemHeight = 13
              TabOrder = 11
            end
            object BillingGroup: TGroupBox
              Left = 23
              Top = 255
              Width = 254
              Height = 112
              Caption = ' Billing '
              TabOrder = 12
              TabStop = True
              Visible = False
              OnEnter = BillingGroupEnter
              object Label2: TLabel
                Left = 14
                Top = 36
                Width = 59
                Height = 26
                Caption = 'Billing Period Date:'
                WordWrap = True
              end
              object Label3: TLabel
                Left = 14
                Top = 69
                Width = 43
                Height = 13
                Caption = 'Billed By:'
              end
              object BilledBy: TDBText
                Left = 108
                Top = 69
                Width = 44
                Height = 13
                AutoSize = True
                Color = clBtnFace
                DataField = 'billed_by_name'
                DataSource = DamageBillSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = False
                ParentFont = False
              end
              object Label4: TLabel
                Left = 14
                Top = 90
                Width = 71
                Height = 13
                Caption = 'Billed On Date:'
              end
              object BilledOnDate: TDBText
                Left = 108
                Top = 90
                Width = 72
                Height = 13
                AutoSize = True
                Color = clBtnFace
                DataField = 'modified_date'
                DataSource = DamageBillSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = False
                ParentFont = False
              end
              object BillingPeriodDate: TcxDBDateEdit
                Left = 108
                Top = 38
                DataBinding.DataField = 'billing_period_date'
                DataBinding.DataSource = DamageBillSource
                Properties.ButtonGlyph.Data = {
                  C2030000424DC203000000000000B6000000280000000F0000000D0000000100
                  2000000000000C03000000000000000000001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
                  8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
                  00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                  0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
                  000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080808000808080008080800080808000808000008080000080808000FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                  0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
                  0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                  FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                  FF00808080008080000080800000808080008080800080808000808080008080
                  8000808080008080800080808000808080008080800080808000808080008080
                  800080800000}
                Properties.InputKind = ikMask
                TabOrder = 1
                Width = 108
              end
              object DamageBillable: TcxDBCheckBox
                Left = 11
                Top = 12
                Caption = 'Damage is Billable:'
                DataBinding.DataField = 'billable'
                DataBinding.DataSource = DamageBillSource
                ParentBackground = False
                ParentColor = False
                Properties.Alignment = taRightJustify
                Properties.NullStyle = nssUnchecked
                TabOrder = 0
                Width = 115
              end
            end
            object ExcavatorDoingRepairs: TDBEdit
              Left = 438
              Top = 165
              Width = 142
              Height = 21
              DataField = 'excavator_doing_repairs'
              DataSource = DamageSource
              TabOrder = 9
            end
            object ClaimCoordinator: TcxDBExtLookupComboBox
              Left = 438
              Top = 254
              DataBinding.DataField = 'claim_coordinator_name'
              DataBinding.DataSource = DamageSource
              Properties.DropDownRows = 16
              Properties.DropDownSizeable = True
              Properties.DropDownWidth = 190
              Properties.View = CoordinatorLookupView
              Properties.KeyFieldNames = 'emp_id'
              Properties.ListFieldItem = ColCoordinatorLookupShortName
              TabOrder = 13
              Width = 162
            end
            object IntelexID: TDBEdit
              Left = 130
              Top = 39
              Width = 147
              Height = 21
              DataField = 'intelex_num'
              DataSource = DamageSource
              TabOrder = 14
            end
          end
          object SiteTab: TTabSheet
            Caption = 'Site Details'
            ImageIndex = 7
            object UtilitiesMarkedGroup: TGroupBox
              Left = 8
              Top = 8
              Width = 169
              Height = 161
              Caption = 'Utilities Marked'
              TabOrder = 0
              object OtherMarkedLabel: TLabel
                Left = 16
                Top = 134
                Width = 28
                Height = 13
                Caption = 'Other'
                FocusControl = Other
              end
              object Sewer: TDBCheckBox
                Left = 20
                Top = 16
                Width = 130
                Height = 17
                Caption = 'Sewer'
                DataField = 'site_sewer_marked'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Water: TDBCheckBox
                Left = 20
                Top = 34
                Width = 130
                Height = 17
                Caption = 'Water'
                DataField = 'site_water_marked'
                DataSource = DamageSource
                TabOrder = 1
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object CableTV: TDBCheckBox
                Left = 20
                Top = 53
                Width = 130
                Height = 17
                Caption = 'Cable TV'
                DataField = 'site_catv_marked'
                DataSource = DamageSource
                TabOrder = 2
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Gas: TDBCheckBox
                Left = 20
                Top = 72
                Width = 130
                Height = 17
                Caption = 'Gas'
                DataField = 'site_gas_marked'
                DataSource = DamageSource
                TabOrder = 3
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Power: TDBCheckBox
                Left = 20
                Top = 91
                Width = 130
                Height = 17
                Caption = 'Power'
                DataField = 'site_power_marked'
                DataSource = DamageSource
                TabOrder = 4
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Telephone: TDBCheckBox
                Left = 20
                Top = 110
                Width = 130
                Height = 17
                Caption = 'Telephone'
                DataField = 'site_tel_marked'
                DataSource = DamageSource
                TabOrder = 5
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Other: TDBEdit
                Left = 49
                Top = 130
                Width = 104
                Height = 21
                DataField = 'site_other_marked'
                DataSource = DamageSource
                TabOrder = 6
              end
            end
            object ExcavationGroup: TGroupBox
              Left = 8
              Top = 235
              Width = 169
              Height = 64
              Caption = 'Excavation Activity'
              TabOrder = 2
              object HandDigging: TDBCheckBox
                Left = 20
                Top = 18
                Width = 130
                Height = 17
                Caption = 'Hand Digging'
                DataField = 'site_hand_dig'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object MechanizedEquipment: TDBCheckBox
                Left = 20
                Top = 42
                Width = 142
                Height = 17
                Caption = 'Mechanized Equipment'
                DataField = 'site_mechanized_equip'
                DataSource = DamageSource
                TabOrder = 1
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
            end
            object EquipmentGroup: TGroupBox
              Left = 8
              Top = 301
              Width = 169
              Height = 87
              Caption = 'Excavation Equipment'
              TabOrder = 3
              object Boring: TDBCheckBox
                Left = 20
                Top = 20
                Width = 130
                Height = 17
                Caption = 'Boring'
                DataField = 'site_exc_boring'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Grading: TDBCheckBox
                Left = 20
                Top = 40
                Width = 130
                Height = 17
                Caption = 'Grading'
                DataField = 'site_exc_grading'
                DataSource = DamageSource
                TabOrder = 1
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object OpenTrench: TDBCheckBox
                Left = 20
                Top = 60
                Width = 130
                Height = 17
                Caption = 'Open Trench'
                DataField = 'site_exc_open_trench'
                DataSource = DamageSource
                TabOrder = 2
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
            end
            object MarkPresenceGroup: TGroupBox
              Left = 8
              Top = 170
              Width = 169
              Height = 63
              Caption = 'Marks Present'
              TabOrder = 1
              object Paint: TDBCheckBox
                Left = 20
                Top = 18
                Width = 108
                Height = 17
                Caption = 'Paint'
                DataField = 'site_paint_present'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Flags: TDBCheckBox
                Left = 20
                Top = 40
                Width = 103
                Height = 17
                Caption = 'Flags'
                DataField = 'site_flags_present'
                DataSource = DamageSource
                TabOrder = 1
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
            end
            object ImagesGroup: TGroupBox
              Left = 188
              Top = 301
              Width = 184
              Height = 87
              Caption = 'Images Taken'
              TabOrder = 7
              object P35mm: TDBCheckBox
                Left = 16
                Top = 20
                Width = 108
                Height = 17
                Caption = '35mm'
                DataField = 'site_img_35mm'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Digital: TDBCheckBox
                Left = 16
                Top = 40
                Width = 108
                Height = 17
                Caption = 'Digital'
                DataField = 'site_img_digital'
                DataSource = DamageSource
                TabOrder = 1
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object Video: TDBCheckBox
                Left = 16
                Top = 60
                Width = 108
                Height = 17
                Caption = 'Video'
                DataField = 'site_img_video'
                DataSource = DamageSource
                TabOrder = 2
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
            end
            object MarksGroup: TGroupBox
              Left = 188
              Top = 126
              Width = 285
              Height = 107
              Caption = 'Marks'
              TabOrder = 5
              object ToleranceLabel: TLabel
                Left = 16
                Top = 39
                Width = 92
                Height = 13
                Caption = 'Tolerance (inches):'
                FocusControl = ToleranceMeasurement
              end
              object ClarityOfMarksLabel: TLabel
                Left = 16
                Top = 62
                Width = 79
                Height = 13
                Caption = 'Clarity of Marks:'
              end
              object ToleranceFeetInches: TLabel
                Left = 201
                Top = 36
                Width = 3
                Height = 13
              end
              object AreaMarkedInWhiteLabel: TLabel
                Left = 16
                Top = 85
                Width = 107
                Height = 13
                Caption = 'Area Marked in White:'
              end
              object MarksWithinToleranceLabel: TLabel
                Left = 16
                Top = 16
                Width = 115
                Height = 13
                Caption = 'Marks Within Tolerance:'
              end
              object ToleranceMeasurement: TDBEdit
                Left = 136
                Top = 36
                Width = 61
                Height = 21
                DataField = 'site_marks_measurement'
                DataSource = DamageSource
                TabOrder = 1
                OnChange = ToleranceMeasurementChange
              end
              object ClarityOfMarks: TDBComboBox
                Left = 136
                Top = 59
                Width = 132
                Height = 21
                Style = csDropDownList
                BevelWidth = 0
                DataField = 'site_clarity_of_marks'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 2
              end
              object AreaMarkedInWhite: TDBComboBox
                Left = 136
                Top = 82
                Width = 132
                Height = 21
                Style = csDropDownList
                DataField = 'site_marked_in_white'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 3
                OnKeyUp = YNUBControlKeyUp
              end
              object MarksWithinTolerance: TDBComboBox
                Left = 136
                Top = 13
                Width = 132
                Height = 21
                Style = csDropDownList
                DataField = 'marks_within_tolerance'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 0
                OnKeyUp = YNUBControlKeyUp
              end
            end
            object UtilityGroup: TGroupBox
              Left = 188
              Top = 8
              Width = 285
              Height = 117
              Caption = 'Utility'
              TabOrder = 4
              object SireBurriedUnderLabel: TLabel
                Left = 16
                Top = 16
                Width = 87
                Height = 13
                Caption = 'Site Buried Under:'
              end
              object FacilityWasPotholedLabel: TLabel
                Left = 16
                Top = 41
                Width = 106
                Height = 13
                Caption = 'Facility Was Potholed:'
              end
              object NearestFacilityMeasureLabel: TLabel
                Left = 16
                Top = 61
                Width = 111
                Height = 26
                Caption = 'Distance from Damage to Nearest Facility:'
                FocusControl = NearestFacilityMeasure
                WordWrap = True
              end
              object TracerWireIntactLabel: TLabel
                Left = 16
                Top = 93
                Width = 92
                Height = 13
                Caption = 'Tracer Wire Intact:'
              end
              object BuriedUnder: TDBComboBox
                Left = 136
                Top = 11
                Width = 132
                Height = 21
                Style = csDropDownList
                BevelWidth = 0
                DataField = 'site_buried_under'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 0
              end
              object FacilityWasPotholed: TDBComboBox
                Left = 136
                Top = 38
                Width = 132
                Height = 21
                Style = csDropDownList
                DataField = 'site_pot_holed'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 1
                OnKeyUp = YNUBControlKeyUp
              end
              object NearestFacilityMeasure: TDBEdit
                Left = 136
                Top = 64
                Width = 61
                Height = 21
                DataField = 'site_nearest_fac_measure'
                DataSource = DamageSource
                TabOrder = 2
              end
              object TracerWireIntact: TDBComboBox
                Left = 136
                Top = 90
                Width = 132
                Height = 21
                Style = csDropDownList
                DataField = 'site_tracer_wire_intact'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 3
                OnKeyUp = YNUBControlKeyUp
              end
            end
            object OffsetsGroup: TGroupBox
              Left = 188
              Top = 235
              Width = 184
              Height = 64
              Caption = 'Offsets'
              TabOrder = 6
              object OffsetQtyLabel: TLabel
                Left = 16
                Top = 43
                Width = 80
                Height = 13
                Caption = 'Offset Quantity:'
                FocusControl = OffsetQuantity
              end
              object OffsetVisibleLabel: TLabel
                Left = 16
                Top = 19
                Width = 67
                Height = 13
                Caption = 'Offset Visible:'
              end
              object OffsetQuantity: TDBEdit
                Left = 101
                Top = 40
                Width = 64
                Height = 21
                DataField = 'offset_qty'
                DataSource = DamageSource
                TabOrder = 1
              end
              object OffsetVisible: TDBComboBox
                Left = 101
                Top = 16
                Width = 64
                Height = 21
                Style = csDropDownList
                DataField = 'offset_visible'
                DataSource = DamageSource
                ItemHeight = 13
                TabOrder = 0
                OnKeyUp = YNUBControlKeyUp
              end
            end
          end
          object DiscussionsTab: TTabSheet
            Caption = 'Discussions'
            ImageIndex = 1
            object RepairTechGroup: TGroupBox
              Left = 0
              Top = 0
              Width = 290
              Height = 213
              Caption = 'Repair Tech'
              TabOrder = 0
              DesignSize = (
                290
                213)
              object TechniciansLabel: TLabel
                Left = 22
                Top = 16
                Width = 59
                Height = 13
                Caption = 'Technicians:'
                FocusControl = DiscRepairTechsWere
              end
              object RepairsWereLabel: TLabel
                Left = 22
                Top = 40
                Width = 69
                Height = 13
                Caption = 'Repairs Were:'
                FocusControl = DiscRepairsWere
              end
              object RepairTechNameLabel: TLabel
                Left = 22
                Top = 64
                Width = 31
                Height = 13
                Caption = 'Name:'
                FocusControl = DiscRepairPerson
              end
              object RepairTechContactLabel: TLabel
                Left = 22
                Top = 88
                Width = 42
                Height = 13
                Caption = 'Contact:'
                FocusControl = DiscRepairContact
              end
              object DiscRepairTechsWere: TDBEdit
                Left = 96
                Top = 13
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_repair_techs_were'
                DataSource = DamageSource
                TabOrder = 0
              end
              object DiscRepairsWere: TDBEdit
                Left = 96
                Top = 37
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_repairs_were'
                DataSource = DamageSource
                TabOrder = 1
              end
              object DiscRepairPerson: TDBEdit
                Left = 96
                Top = 61
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_repair_person'
                DataSource = DamageSource
                TabOrder = 2
              end
              object DiscRepairContact: TDBEdit
                Left = 96
                Top = 85
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_repair_contact'
                DataSource = DamageSource
                TabOrder = 3
              end
              object DiscRepairComment: TDBMemo
                Left = 8
                Top = 110
                Width = 274
                Height = 95
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataField = 'disc_repair_comment'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 4
              end
            end
            object ExcavatorsGroup: TGroupBox
              Left = 296
              Top = 0
              Width = 290
              Height = 213
              Caption = 'Excavators'
              TabOrder = 1
              DesignSize = (
                290
                213)
              object ExcavatorsLabel: TLabel
                Left = 30
                Top = 16
                Width = 58
                Height = 13
                Caption = 'Excavators:'
                FocusControl = DiscExcWere
              end
              object ExcavatorNameLabel: TLabel
                Left = 30
                Top = 40
                Width = 31
                Height = 13
                Caption = 'Name:'
                FocusControl = DiscExcPerson
              end
              object ExcavatorContactLabel: TLabel
                Left = 30
                Top = 64
                Width = 42
                Height = 13
                Caption = 'Contact:'
                FocusControl = DiscExcContact
              end
              object DiscExcWere: TDBEdit
                Left = 96
                Top = 13
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_exc_were'
                DataSource = DamageSource
                TabOrder = 0
              end
              object DiscExcPerson: TDBEdit
                Left = 96
                Top = 37
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_exc_person'
                DataSource = DamageSource
                TabOrder = 1
              end
              object DiscExcContact: TDBEdit
                Left = 96
                Top = 61
                Width = 186
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_exc_contact'
                DataSource = DamageSource
                TabOrder = 2
              end
              object DiscExcComment: TDBMemo
                Left = 8
                Top = 85
                Width = 274
                Height = 120
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataField = 'disc_exc_comment'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 3
              end
            end
            object OtherPerson1Group: TGroupBox
              Left = 0
              Top = 214
              Width = 290
              Height = 163
              Caption = 'Other Person 1'
              TabOrder = 2
              DesignSize = (
                290
                163)
              object Other1NameLabel: TLabel
                Left = 22
                Top = 20
                Width = 31
                Height = 13
                Alignment = taRightJustify
                Caption = 'Name:'
                FocusControl = DiscOther1Person
              end
              object Other1ContactLabel: TLabel
                Left = 22
                Top = 43
                Width = 42
                Height = 13
                Alignment = taRightJustify
                Caption = 'Contact:'
                FocusControl = DiscOther1Contact
              end
              object DiscOther1Person: TDBEdit
                Left = 80
                Top = 16
                Width = 202
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_other1_person'
                DataSource = DamageSource
                TabOrder = 0
              end
              object DiscOther1Contact: TDBEdit
                Left = 80
                Top = 40
                Width = 202
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_other1_contact'
                DataSource = DamageSource
                TabOrder = 1
              end
              object DiscOther1Comment: TDBMemo
                Left = 8
                Top = 65
                Width = 272
                Height = 90
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataField = 'disc_other1_comment'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 2
              end
            end
            object OtherPerson2Group: TGroupBox
              Left = 296
              Top = 214
              Width = 290
              Height = 163
              Caption = 'Other Person 2'
              TabOrder = 3
              DesignSize = (
                290
                163)
              object Other2NameLabel: TLabel
                Left = 30
                Top = 19
                Width = 31
                Height = 13
                Caption = 'Name:'
                FocusControl = DiscOther2Person
              end
              object Other2ContactLabel: TLabel
                Left = 30
                Top = 43
                Width = 42
                Height = 13
                Caption = 'Contact:'
                FocusControl = DiscOther2Contact
              end
              object DiscOther2Person: TDBEdit
                Left = 80
                Top = 16
                Width = 202
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_other2_person'
                DataSource = DamageSource
                TabOrder = 0
              end
              object DiscOther2Contact: TDBEdit
                Left = 80
                Top = 40
                Width = 202
                Height = 21
                Anchors = [akLeft, akTop, akRight]
                DataField = 'disc_other2_contact'
                DataSource = DamageSource
                TabOrder = 1
              end
              object DiscOther2Comment: TDBMemo
                Left = 8
                Top = 65
                Width = 275
                Height = 90
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataField = 'disc_other2_comment'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 2
              end
            end
          end
          object ResponsibilityTab: TTabSheet
            Caption = 'Responsibility'
            ImageIndex = 2
            object RespCodeExcLabel: TLabel
              Left = 9
              Top = 20
              Width = 93
              Height = 13
              Alignment = taRightJustify
              Caption = 'Responsibility Code'
            end
            object ReasonForChangeLabel: TLabel
              Left = 14
              Top = 400
              Width = 97
              Height = 13
              Caption = 'Reason for Change:'
            end
            object ExcavatorGroup: TGroupBox
              Left = 4
              Top = 1
              Width = 575
              Height = 164
              Caption = 'Excavator Responsibility'
              TabOrder = 0
              object ExcRespCodeLabel: TLabel
                Left = 12
                Top = 19
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object ExcOtherRespDescLabel: TLabel
                Left = 12
                Top = 41
                Width = 88
                Height = 13
                Caption = 'Other Description:'
                FocusControl = ExcOtherDesc
              end
              object ExcavatorSourceCommentsLabel: TLabel
                Left = 12
                Top = 64
                Width = 37
                Height = 13
                Caption = 'Source:'
              end
              object ExcResponseLabel: TLabel
                Left = 12
                Top = 113
                Width = 51
                Height = 13
                Caption = 'Response:'
              end
              object ExcOtherDesc: TDBEdit
                Left = 112
                Top = 39
                Width = 453
                Height = 21
                DataField = 'exc_resp_other_desc'
                DataSource = DamageSource
                TabOrder = 1
              end
              object ExcDetails: TDBMemo
                Left = 112
                Top = 62
                Width = 453
                Height = 46
                DataField = 'exc_resp_details'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 2
              end
              object ExcResponse: TDBMemo
                Left = 112
                Top = 110
                Width = 453
                Height = 46
                DataField = 'exc_resp_response'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 3
              end
              object ExcRespCode: TDBComboBox
                Left = 112
                Top = 16
                Width = 453
                Height = 21
                Style = csDropDownList
                DataField = 'exc_resp_code'
                DataSource = DamageSource
                DropDownCount = 12
                ItemHeight = 13
                TabOrder = 0
                OnChange = ResponsibilityCodeChange
              end
            end
            object UtiliQuestGroup: TGroupBox
              Left = 4
              Top = 166
              Width = 575
              Height = 137
              Caption = 'CompanyShortName Responsibility'
              TabOrder = 1
              object UQRespCodeLabel: TLabel
                Left = 12
                Top = 19
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object UQDetailsLabel: TLabel
                Left = 12
                Top = 89
                Width = 36
                Height = 13
                Caption = 'Details:'
                FocusControl = UQRespDetails
              end
              object UQOtherDescLabel: TLabel
                Left = 12
                Top = 42
                Width = 88
                Height = 13
                Caption = 'Other Description:'
                FocusControl = UQRespOtherDesc
              end
              object UQEssentialStepLabel: TLabel
                Left = 12
                Top = 65
                Width = 71
                Height = 13
                Caption = 'Essential Step:'
              end
              object UQRespDetails: TDBMemo
                Left = 112
                Top = 86
                Width = 453
                Height = 46
                DataField = 'uq_resp_details'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 3
              end
              object UQRespOtherDesc: TDBEdit
                Left = 112
                Top = 39
                Width = 453
                Height = 21
                DataField = 'uq_resp_other_desc'
                DataSource = DamageSource
                TabOrder = 1
              end
              object UQRespCode: TDBComboBox
                Left = 112
                Top = 16
                Width = 453
                Height = 21
                Style = csDropDownList
                DataField = 'uq_resp_code'
                DataSource = DamageSource
                DropDownCount = 12
                ItemHeight = 13
                TabOrder = 0
                OnChange = ResponsibilityCodeChange
              end
              object UQRespEssential: TDBComboBox
                Left = 112
                Top = 62
                Width = 453
                Height = 21
                Style = csDropDownList
                DataField = 'uq_resp_ess_step'
                DataSource = DamageSource
                DropDownCount = 15
                ItemHeight = 13
                TabOrder = 2
              end
            end
            object Special: TGroupBox
              Left = 4
              Top = 304
              Width = 575
              Height = 90
              Caption = 'Utility Responsibility'
              TabOrder = 2
              object UtilityRespCodeLabel: TLabel
                Left = 12
                Top = 19
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object UtilityRespDetailsLabel: TLabel
                Left = 12
                Top = 42
                Width = 36
                Height = 13
                Caption = 'Details:'
                FocusControl = SpecialDetails
              end
              object SpecialDetails: TDBMemo
                Left = 112
                Top = 39
                Width = 453
                Height = 46
                DataField = 'spc_resp_details'
                DataSource = DamageSource
                ScrollBars = ssVertical
                TabOrder = 1
              end
              object SpecialRespCode: TDBComboBox
                Left = 112
                Top = 16
                Width = 453
                Height = 21
                Style = csDropDownList
                DataField = 'spc_resp_code'
                DataSource = DamageSource
                DropDownCount = 12
                ItemHeight = 13
                TabOrder = 0
                OnChange = ResponsibilityCodeChange
              end
            end
            object ReasonChanged: TComboBox
              Left = 116
              Top = 397
              Width = 453
              Height = 21
              Style = csDropDownList
              ItemHeight = 0
              TabOrder = 3
              OnChange = ReasonChangedSelectionChange
            end
          end
          object CustomQuestionsTab: TTabSheet
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ImageIndex = 14
            ParentFont = False
            TabVisible = False
          end
          object InvestigatorTab: TTabSheet
            Caption = 'Investigator'
            ImageIndex = 6
            DesignSize = (
              1112
              488)
            object InvestigatorNarrativeLabel: TLabel
              Left = 4
              Top = 4
              Width = 111
              Height = 13
              Caption = 'Investigator Narrative:'
              FocusControl = InvestigatorNarrative
            end
            object InvestigatorNarrative: TDBMemo
              Left = 0
              Top = 21
              Width = 1108
              Height = 468
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataField = 'investigator_narrative'
              DataSource = DamageSource
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
          object EstimatesTab: TTabSheet
            Caption = 'Estimates'
            ImageIndex = 6
            inline DamageEstimateFrame: TEstimateFrame
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              TabOrder = 0
              inherited EstimatesGrid: TDBGrid
                Top = 80
                Width = 1112
                Height = 382
                TitleFont.Charset = ANSI_CHARSET
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'estimate_type_desc'
                    Title.Caption = 'Type'
                    Width = 91
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'company'
                    Title.Caption = 'Claimant'
                    Visible = False
                  end
                  item
                    Expanded = False
                    FieldName = 'modified_date'
                    Title.Caption = 'Date'
                    Width = 147
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'employee_name'
                    Title.Caption = 'Employee'
                    Width = 114
                    Visible = True
                  end
                  item
                    Alignment = taRightJustify
                    Expanded = False
                    FieldName = 'amount'
                    Title.Alignment = taRightJustify
                    Title.Caption = 'Amount'
                    Width = 67
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'contract'
                    Title.Caption = 'Contract'
                    Width = 83
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'comment'
                    Title.Caption = 'Comment'
                    Width = 219
                    Visible = True
                  end>
              end
              inherited NewEstimatePanel: TPanel
                Width = 1112
                Height = 80
                inherited EstimateAmountLabel: TLabel
                  Left = 14
                  Top = 10
                  Width = 85
                  Caption = 'Estimate Amount:'
                end
                inherited EstimateCommentLabel: TLabel
                  Left = 275
                  Top = 10
                  Width = 49
                  Caption = 'Comment:'
                end
                inherited AccrualAmountLabel: TLabel
                  Left = 14
                  Width = 79
                  Caption = 'Accrual Amount:'
                end
                inherited AccrualAmount: TLabel
                  Left = 104
                  Width = 24
                  Font.Charset = ANSI_CHARSET
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                inherited EstimateAmount: TEdit
                  Left = 104
                end
                inherited EstimateComment: TEdit
                  Left = 332
                end
              end
              inherited Panel1: TPanel
                Top = 462
                Width = 1112
                DesignSize = (
                  1112
                  26)
                inherited ViewAllck: TCheckBox
                  Left = 572
                  Width = 480
                  Anchors = [akLeft, akTop, akRight]
                end
              end
              inherited EstimateSource: TDataSource
                Left = 524
                Top = 320
              end
            end
            object EstimateLocked: TDBCheckBox
              Left = 14
              Top = 61
              Width = 275
              Height = 17
              Caption = 'Disable automatic estimates for this damage'
              DataField = 'estimate_locked'
              DataSource = DamageSource
              TabOrder = 1
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
          end
          object InvoicesTab: TTabSheet
            Caption = 'Invoices'
            ImageIndex = 9
            inline DamageInvoice: TDamageInvoiceFrame
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              TabOrder = 0
              inherited FooterPanel: TPanel
                Top = 460
                Width = 1112
                DesignSize = (
                  1112
                  28)
                inherited ViewAllCk: TCheckBox
                  Left = 963
                end
              end
              inherited EditInvoicePanel: TPanel
                Width = 1112
                inherited AccrualCodeLabel: TLabel
                  Left = 394
                end
                inherited InvoiceGroup: TGroupBox
                  inherited AmountLabel: TLabel
                    Left = 15
                  end
                  inherited InvoiceCompanyLabel: TLabel
                    Left = 7
                  end
                  inherited InvoiceNumberLabel: TLabel
                    Left = 15
                  end
                  inherited ReceivedDateLabel: TLabel
                    Left = 20
                  end
                  inherited CommentsLabel: TLabel
                    Top = 165
                  end
                  inherited InvoiceDateLabel: TLabel
                    Left = 29
                  end
                  inherited ClaimantLabel: TLabel
                    Left = 49
                  end
                end
                inherited PaymentGroup: TGroupBox
                  Width = 293
                  inherited ApprovedDateLabel: TLabel
                    Left = 17
                  end
                  inherited AmountApprovedLabel: TLabel
                    Left = 17
                  end
                  inherited PaidDateLabel: TLabel
                    Left = 17
                  end
                  inherited AmountPaidLabel: TLabel
                    Left = 17
                  end
                  inherited SatisfiedLabel: TLabel
                    Left = 17
                    Top = 127
                  end
                  inherited InvoiceApprovedLabel: TLabel
                    Left = 17
                    Top = 155
                  end
                  inherited PaymentCodeLabel: TLabel
                    Left = 17
                  end
                  inherited AmountApproved: TDBEdit
                    Left = 129
                  end
                  inherited ApprovedDate: TcxDBDateEdit
                    Left = 129
                  end
                  inherited AmountPaid: TDBEdit
                    Left = 129
                  end
                  inherited PaidDate: TcxDBDateEdit
                    Left = 129
                  end
                  inherited Satisfied: TDBCheckBox
                    Left = 129
                  end
                  inherited InvoiceApproved: TDBCheckBox
                    Left = 129
                    Top = 160
                  end
                  inherited PaymentCode: TDBComboBox
                    Left = 129
                  end
                end
                inherited DamageGroup: TGroupBox
                  inherited DamageIDLabel: TLabel
                    Left = 37
                  end
                  inherited DamageStateLabel: TLabel
                    Left = 22
                  end
                  inherited DamageCityLabel: TLabel
                    Left = 29
                  end
                  inherited DamageDateLabel: TLabel
                    Left = 25
                  end
                end
                inherited PacketGroup: TGroupBox
                  Width = 293
                  inherited Label1: TLabel
                    Left = 16
                  end
                  inherited Label3: TLabel
                    Left = 17
                  end
                end
              end
              inherited InvoiceGrid: TcxGrid
                Width = 1112
                Height = 143
              end
            end
          end
          object HistoryTab: TTabSheet
            Caption = 'History'
            ImageIndex = 7
            object HistoryPanel: TPanel
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object HistoryPageControl: TPageControl
                Left = 0
                Top = 0
                Width = 1112
                Height = 488
                ActivePage = HistoryResponsibilityTab
                Align = alClient
                TabOrder = 0
                object HistoryResponsibilityTab: TTabSheet
                  Caption = 'Responsibility'
                  object ResponsibilityHistoryGrid: TcxGrid
                    Left = 0
                    Top = 0
                    Width = 1104
                    Height = 460
                    Align = alClient
                    TabOrder = 0
                    object ResponsibilityHistoryGridView: TcxGridDBBandedTableView
                      Navigator.Buttons.CustomButtons = <>
                      DataController.DataSource = HistorySource
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      OptionsCustomize.ColumnVertSizing = False
                      OptionsData.Deleting = False
                      OptionsData.Editing = False
                      OptionsData.Inserting = False
                      OptionsView.GroupByBox = False
                      OptionsView.GroupFooters = gfVisibleWhenExpanded
                      OptionsView.BandHeaders = False
                      Bands = <
                        item
                          Caption = 'History Modified Date'
                          FixedKind = fkLeft
                          Options.Moving = False
                          Width = 250
                        end
                        item
                          Caption = 'History Details'
                        end>
                      object ColModifiedDate: TcxGridDBBandedColumn
                        Caption = 'Change Date'
                        DataBinding.FieldName = 'modified_date'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 140
                        Position.BandIndex = 0
                        Position.ColIndex = 0
                        Position.RowIndex = 0
                      end
                      object ColModifiedByShortName: TcxGridDBBandedColumn
                        Caption = 'Changed By'
                        DataBinding.FieldName = 'ColModifiedByShortName'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 110
                        Position.BandIndex = 0
                        Position.ColIndex = 1
                        Position.RowIndex = 0
                      end
                      object ColModifiedBy: TcxGridDBBandedColumn
                        Caption = 'Modified By'
                        DataBinding.FieldName = 'modified_by'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Visible = False
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Position.BandIndex = 1
                        Position.ColIndex = 0
                        Position.RowIndex = 0
                      end
                      object ColUQRespCode: TcxGridDBBandedColumn
                        Caption = 'UQ/STS Code'
                        DataBinding.FieldName = 'uq_resp_code'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 80
                        Position.BandIndex = 1
                        Position.ColIndex = 1
                        Position.RowIndex = 0
                      end
                      object ColUqRespType: TcxGridDBBandedColumn
                        Caption = 'UQ/STS Type'
                        DataBinding.FieldName = 'uq_resp_type'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 76
                        Position.BandIndex = 1
                        Position.ColIndex = 2
                        Position.RowIndex = 0
                      end
                      object ColUQRespOtherDesc: TcxGridDBBandedColumn
                        Caption = 'UQ Other Desc'
                        DataBinding.FieldName = 'uq_resp_other_desc'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 142
                        Position.BandIndex = 1
                        Position.ColIndex = 3
                        Position.RowIndex = 0
                      end
                      object ColExcRespDetails: TcxGridDBBandedColumn
                        Caption = 'Excavator Responsibility Details'
                        DataBinding.FieldName = 'exc_resp_details'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 198
                        Position.BandIndex = 1
                        Position.ColIndex = 4
                        Position.RowIndex = 0
                      end
                      object ColExcRespResponse: TcxGridDBBandedColumn
                        Caption = 'Excavator Responsibility Response'
                        DataBinding.FieldName = 'exc_resp_response'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 206
                        Position.BandIndex = 1
                        Position.ColIndex = 5
                        Position.RowIndex = 0
                      end
                      object ColUQRespDetails: TcxGridDBBandedColumn
                        Caption = 'UQ/STS Responsibility Details'
                        DataBinding.FieldName = 'uq_resp_details'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 197
                        Position.BandIndex = 1
                        Position.ColIndex = 6
                        Position.RowIndex = 0
                      end
                      object ColUqRespEssStep: TcxGridDBBandedColumn
                        Caption = 'UQ Ess Step'
                        DataBinding.FieldName = 'uq_resp_ess_step'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 68
                        Position.BandIndex = 1
                        Position.ColIndex = 7
                        Position.RowIndex = 0
                      end
                      object ColExcRespCode: TcxGridDBBandedColumn
                        Caption = 'Exc Code'
                        DataBinding.FieldName = 'exc_resp_code'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 54
                        Position.BandIndex = 1
                        Position.ColIndex = 8
                        Position.RowIndex = 0
                      end
                      object ColExcRespType: TcxGridDBBandedColumn
                        Caption = 'Exc Type'
                        DataBinding.FieldName = 'exc_resp_type'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 52
                        Position.BandIndex = 1
                        Position.ColIndex = 9
                        Position.RowIndex = 0
                      end
                      object ColExcRespOtherDesc: TcxGridDBBandedColumn
                        Caption = 'Exc Other Desc'
                        DataBinding.FieldName = 'exc_resp_other_desc'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 179
                        Position.BandIndex = 1
                        Position.ColIndex = 10
                        Position.RowIndex = 0
                      end
                      object ColSpcRespCode: TcxGridDBBandedColumn
                        Caption = 'Spc Code'
                        DataBinding.FieldName = 'spc_resp_code'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 82
                        Position.BandIndex = 1
                        Position.ColIndex = 11
                        Position.RowIndex = 0
                      end
                      object ColSpcRespType: TcxGridDBBandedColumn
                        Caption = 'Spc Type'
                        DataBinding.FieldName = 'spc_resp_type'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 86
                        Position.BandIndex = 1
                        Position.ColIndex = 12
                        Position.RowIndex = 0
                      end
                      object ColSPCRespOtherDesc: TcxGridDBBandedColumn
                        Caption = 'SPC Other Desc'
                        DataBinding.FieldName = 'spc_resp_other_desc'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 155
                        Position.BandIndex = 1
                        Position.ColIndex = 13
                        Position.RowIndex = 0
                      end
                      object ColSPCRespDetails: TcxGridDBBandedColumn
                        Caption = 'Utility Responsibility Details'
                        DataBinding.FieldName = 'spc_resp_details'
                        PropertiesClassName = 'TcxBlobEditProperties'
                        Properties.BlobEditKind = bekMemo
                        Properties.BlobPaintStyle = bpsText
                        Properties.MemoScrollBars = ssVertical
                        Properties.ReadOnly = True
                        Properties.ShowExPopupItems = False
                        Width = 220
                        Position.BandIndex = 1
                        Position.ColIndex = 14
                        Position.RowIndex = 0
                      end
                      object ColProfitCenter: TcxGridDBBandedColumn
                        Caption = 'Profit Center'
                        DataBinding.FieldName = 'profit_center'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 66
                        Position.BandIndex = 1
                        Position.ColIndex = 15
                        Position.RowIndex = 0
                      end
                      object ColReasonChangedDesc: TcxGridDBBandedColumn
                        Caption = 'Reason Changed'
                        DataBinding.FieldName = 'ColReasonChangedDesc'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.ReadOnly = True
                        Options.FilteringPopup = False
                        Options.FilteringPopupMultiSelect = False
                        Width = 350
                        Position.BandIndex = 1
                        Position.ColIndex = 16
                        Position.RowIndex = 0
                      end
                    end
                    object ResponsibilityHistoryGridLevel: TcxGridLevel
                      GridView = ResponsibilityHistoryGridView
                    end
                  end
                end
                object HistoryArrivalsTab: TTabSheet
                  Caption = 'Arrivals'
                  ImageIndex = 1
                  object ArrivalGrid: TcxGrid
                    Left = 0
                    Top = 0
                    Width = 1104
                    Height = 460
                    Align = alClient
                    TabOrder = 0
                    object ArrivalGridView: TcxGridDBTableView
                      Navigator.Buttons.CustomButtons = <>
                      FilterBox.Visible = fvNever
                      DataController.DataSource = JobsiteArrivalSource
                      DataController.Filter.MaxValueListCount = 1000
                      DataController.KeyFieldNames = 'arrival_id'
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      Filtering.ColumnPopup.MaxDropDownItemCount = 12
                      OptionsData.Deleting = False
                      OptionsData.Editing = False
                      OptionsData.Inserting = False
                      OptionsSelection.CellSelect = False
                      OptionsSelection.HideFocusRectOnExit = False
                      OptionsSelection.InvertSelect = False
                      OptionsView.GridLineColor = clBtnShadow
                      OptionsView.GroupByBox = False
                      Preview.AutoHeight = False
                      Preview.MaxLineCount = 2
                      object ColArrivalsArrivalID: TcxGridDBColumn
                        DataBinding.FieldName = 'arrival_id'
                        PropertiesClassName = 'TcxMaskEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Visible = False
                        Options.Filtering = False
                        Width = 71
                      end
                      object ColArrivalsArrivalDate: TcxGridDBColumn
                        Caption = 'Arrival Date'
                        DataBinding.FieldName = 'arrival_date'
                        PropertiesClassName = 'TcxDateEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.DateButtons = [btnClear, btnToday]
                        Properties.DateOnError = deToday
                        Properties.InputKind = ikRegExpr
                        Options.Filtering = False
                        SortIndex = 0
                        SortOrder = soAscending
                        Width = 139
                      end
                      object ColArrivalsEmpID: TcxGridDBColumn
                        Caption = 'Emp ID'
                        DataBinding.FieldName = 'emp_id'
                        PropertiesClassName = 'TcxMaskEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Visible = False
                        Options.Filtering = False
                        Width = 52
                      end
                      object ColArrivalsShortName: TcxGridDBColumn
                        Caption = 'Employee'
                        DataBinding.FieldName = 'short_name'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        MinWidth = 100
                        Options.Filtering = False
                        Width = 193
                      end
                      object ColArrivalsEntryMethod: TcxGridDBColumn
                        Caption = 'Entered How'
                        DataBinding.FieldName = 'entry_method'
                        PropertiesClassName = 'TcxMaskEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Options.Filtering = False
                        Width = 84
                      end
                      object ColArrivalsAddedByName: TcxGridDBColumn
                        Caption = 'Entered by'
                        DataBinding.FieldName = 'added_by_name'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Options.Filtering = False
                        Width = 167
                      end
                      object ColArrivalsActive: TcxGridDBColumn
                        Caption = 'Active'
                        DataBinding.FieldName = 'active'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        MinWidth = 40
                        Options.Filtering = False
                        Width = 65
                      end
                      object ColArrivalsDeletedByName: TcxGridDBColumn
                        Caption = 'Deleted by'
                        DataBinding.FieldName = 'deleted_by_name'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Visible = False
                        MinWidth = 100
                        Options.Filtering = False
                        Width = 138
                      end
                      object ColArrivalsDeletedDate: TcxGridDBColumn
                        Caption = 'Deleted Date'
                        DataBinding.FieldName = 'deleted_date'
                        PropertiesClassName = 'TcxDateEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.DateButtons = [btnClear, btnToday]
                        Properties.DateOnError = deToday
                        Properties.InputKind = ikRegExpr
                        Visible = False
                        Options.Filtering = False
                        Width = 103
                      end
                      object ColArrivalsLocationStatus: TcxGridDBColumn
                        Caption = 'Location Status'
                        DataBinding.FieldName = 'location_status'
                        PropertiesClassName = 'TcxTextEditProperties'
                        Properties.Alignment.Horz = taLeftJustify
                        Properties.MaxLength = 0
                        Properties.ReadOnly = True
                        Options.Filtering = False
                        Width = 80
                      end
                    end
                    object ArrivalGridLevel: TcxGridLevel
                      GridView = ArrivalGridView
                    end
                  end
                end
                object HistoryAccrualTab: TTabSheet
                  Caption = 'Accrual History'
                  ImageIndex = 2
                  object AccrualHistoryGrid: TDBGrid
                    Left = 0
                    Top = 0
                    Width = 1104
                    Height = 460
                    Align = alClient
                    DataSource = AccrualHistorySource
                    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                    ReadOnly = True
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'Tahoma'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'short_period'
                        Title.Alignment = taCenter
                        Title.Caption = 'Period'
                        Width = 63
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'project'
                        Title.Alignment = taCenter
                        Title.Caption = 'Project'
                        Width = 83
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'task'
                        Title.Alignment = taCenter
                        Title.Caption = 'Task'
                        Width = 169
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'invoice_code'
                        Title.Alignment = taCenter
                        Title.Caption = 'Invoice Code'
                        Width = 89
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'liability_amount'
                        Title.Alignment = taCenter
                        Title.Caption = 'Liability Amount'
                        Width = 123
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'change_in_liability_amount'
                        Title.Alignment = taCenter
                        Title.Caption = 'Change in Liability'
                        Width = 116
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
          object AttachmentsTab: TTabSheet
            Caption = 'Attachments'
            ImageIndex = 8
            inline DamageAttachmentsFrame: TAttachmentsFrame
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              TabOrder = 0
              inherited AttachmentsPanel: TPanel
                Width = 1112
                Height = 457
                inherited AttachmentList: TcxGrid
                  Width = 1102
                  Height = 447
                  inherited AttachmentListDBTableView: TcxGridDBTableView
                    inherited ColDocumentType: TcxGridDBColumn
                      Properties.ReadOnly = False
                    end
                    inherited ColComment: TcxGridDBColumn
                      SortIndex = 1
                      SortOrder = soAscending
                    end
                  end
                end
              end
              inherited FooterPanel: TPanel
                Top = 457
                Width = 1112
              end
            end
          end
          object PhotoAttachmentsTab: TTabSheet
            Caption = 'Photo Attachments'
            ImageIndex = 10
            inline PhotoAttachmentFrame: TPhotoAttachmentFrame
              Left = 0
              Top = 0
              Width = 1112
              Height = 488
              Align = alClient
              AutoSize = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              inherited PanelPhotos: TPanel
                Width = 1112
                Height = 443
                inherited ScrollBox: TScrollBox
                  Width = 1102
                  Height = 433
                end
              end
              inherited PanelFooter: TPanel
                Top = 443
                Width = 1112
                inherited LabelCurrentDirectory: TLabel
                  Font.Charset = ANSI_CHARSET
                end
                inherited LabelMessage: TLabel
                  Left = 627
                end
              end
            end
          end
          object RequiredDocumentsTab: TTabSheet
            Caption = 'Required Documents'
            ImageIndex = 13
            object DocumentGrid: TcxGrid
              Left = 0
              Top = 32
              Width = 1112
              Height = 456
              Align = alClient
              TabOrder = 0
              object DocumentDBTableView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                FilterBox.Visible = fvNever
                OnCustomDrawCell = DocumentDBTableViewCustomDrawCell
                DataController.DataSource = DocumentSource
                DataController.KeyFieldNames = 'required_doc_id'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.IncSearch = True
                OptionsBehavior.IncSearchItem = ColDocRequiredDocName
                OptionsBehavior.FocusCellOnCycle = True
                OptionsCustomize.ColumnFiltering = False
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.UnselectFocusedRecordOnExit = False
                OptionsView.CellEndEllipsis = True
                OptionsView.GridLineColor = clBtnFace
                OptionsView.GroupByBox = False
                object ColDocRequired: TcxGridDBColumn
                  Caption = 'Required'
                  DataBinding.FieldName = 'required'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Properties.ReadOnly = True
                  Visible = False
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocRequiredDocName: TcxGridDBColumn
                  Caption = 'Document Type'
                  DataBinding.FieldName = 'name'
                  PropertiesClassName = 'TcxLabelProperties'
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                  Width = 210
                end
                object ColDocAttachedCnt: TcxGridDBColumn
                  Caption = 'Attach Count'
                  DataBinding.FieldName = 'attached_cnt'
                  PropertiesClassName = 'TcxLabelProperties'
                  Properties.Alignment.Horz = taCenter
                  HeaderAlignmentHorz = taCenter
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                  Width = 90
                end
                object ColDocComment: TcxGridDBColumn
                  Caption = 'Reason Missing'
                  DataBinding.FieldName = 'comment'
                  PropertiesClassName = 'TcxTextEditProperties'
                  OnGetDisplayText = ColDocCommentGetDisplayText
                  OnGetPropertiesForEdit = ColDocCommentGetPropertiesForEdit
                  Options.IncSearch = False
                  Width = 412
                end
                object ColDocForeignID: TcxGridDBColumn
                  Caption = 'Foreign ID'
                  DataBinding.FieldName = 'foreign_id'
                  Visible = False
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocRequiredDocID: TcxGridDBColumn
                  Caption = 'Req Doc ID'
                  DataBinding.FieldName = 'required_doc_id'
                  Visible = False
                  HeaderAlignmentHorz = taRightJustify
                  MinWidth = 30
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocForeignType: TcxGridDBColumn
                  Caption = 'Foreign Type'
                  DataBinding.FieldName = 'foreign_type'
                  Visible = False
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocDocID: TcxGridDBColumn
                  Caption = 'Doc ID'
                  DataBinding.FieldName = 'doc_id'
                  Visible = False
                  HeaderAlignmentHorz = taRightJustify
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocAddedByID: TcxGridDBColumn
                  Caption = 'Added By ID'
                  DataBinding.FieldName = 'added_by_id'
                  Visible = False
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
                object ColDocAddedDate: TcxGridDBColumn
                  Caption = 'Added Date'
                  DataBinding.FieldName = 'added_date'
                  PropertiesClassName = 'TcxDateEditProperties'
                  Properties.ReadOnly = True
                  Visible = False
                  Options.Editing = False
                  Options.ShowEditButtons = isebNever
                end
              end
              object DocumentLevel: TcxGridLevel
                GridView = DocumentDBTableView
              end
            end
            object RequiredDocsTopPanel: TPanel
              Left = 0
              Top = 0
              Width = 1112
              Height = 32
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object ShowOnlyMissingDocsCheckBox: TCheckBox
                Left = 2
                Top = 6
                Width = 271
                Height = 17
                Caption = 'Check to show only missing documents'
                TabOrder = 0
                OnClick = ShowOnlyMissingDocsCheckBoxClick
              end
            end
          end
          object DamageNotesTab: TTabSheet
            Caption = 'Notes'
            ImageIndex = 11
            inline DamageNotesFrame: TNotesFrame
              Left = 0
              Top = 39
              Width = 1112
              Height = 449
              Align = alClient
              TabOrder = 0
              inherited NotesGrid: TcxGrid
                Width = 1112
                Height = 318
                inherited NotesGridView: TcxGridDBTableView
                  inherited NotesGridViewnotes_type_desc1: TcxGridDBColumn
                    Width = 100
                  end
                  inherited NotesGridViewClaimant: TcxGridDBColumn
                    SortIndex = 0
                    SortOrder = soAscending
                    Width = 64
                  end
                  inherited NotesGridViewentry_date1: TcxGridDBColumn
                    Width = 177
                  end
                  inherited NotesGridViewemployee_name1: TcxGridDBColumn
                    Width = 205
                  end
                  inherited NotesGridRestrictionCol: TcxGridDBColumn
                    Width = 241
                  end
                end
              end
              inherited HeaderPanel: TPanel
                Width = 1112
                inherited NoteLabel: TLabel
                  Caption = 'Note Text:'
                end
              end
              inherited FooterPanel: TPanel
                Top = 423
                Width = 1112
              end
              inherited NotesSource: TDataSource
                Left = 128
                Top = 160
              end
              inherited Notes: TDBISAMQuery
                Left = 80
                Top = 160
              end
            end
            object Panel1: TPanel
              Left = 0
              Top = 0
              Width = 1112
              Height = 39
              Align = alTop
              BevelEdges = []
              BevelOuter = bvNone
              TabOrder = 1
              object DamageRestrictionLabel: TLabel
                Left = 38
                Top = 5
                Width = 111
                Height = 13
                Caption = 'Restriction (Sub Type):'
                WordWrap = True
              end
              object DamageSubTypeCombo: TComboBox
                Left = 37
                Top = 18
                Width = 243
                Height = 21
                ItemHeight = 13
                TabOrder = 0
                OnChange = DamageSubTypeComboChange
                Items.Strings = (
                  'Ticket'
                  'Locate 1'
                  'Locate 2'
                  'Locate 3')
              end
            end
          end
          object ApprovalDetailsTab: TTabSheet
            Caption = 'Approval Details'
            ImageIndex = 14
            object adExcavatorRespGroup: TGroupBox
              Left = 0
              Top = 0
              Width = 403
              Height = 167
              Caption = 'Excavator Responsibility'
              TabOrder = 0
              object ExcavatorRespCodeLabel: TLabel
                Left = 8
                Top = 19
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object adExcRespCodeLabel: TDBText
                Left = 116
                Top = 19
                Width = 119
                Height = 13
                AutoSize = True
                DataField = 'exc_resp_code'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ExcavatorOtherDescLabel: TLabel
                Left = 8
                Top = 36
                Width = 88
                Height = 13
                Caption = 'Other Description:'
              end
              object adExcRespDescLabel: TDBText
                Left = 116
                Top = 36
                Width = 118
                Height = 13
                AutoSize = True
                DataField = 'exc_resp_other_desc'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ExcavatorSourceLabel: TLabel
                Left = 8
                Top = 55
                Width = 37
                Height = 13
                Caption = 'Source:'
              end
              object ExcavatorResponseLabel: TLabel
                Left = 8
                Top = 111
                Width = 51
                Height = 13
                Caption = 'Response:'
              end
              object adExcRespSourceMemo: TDBMemo
                Left = 112
                Top = 55
                Width = 288
                Height = 49
                TabStop = False
                BevelInner = bvNone
                BevelOuter = bvNone
                BorderStyle = bsNone
                DataField = 'exc_resp_details'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = True
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 0
              end
              object adExcRespResponseMemo: TDBMemo
                Left = 112
                Top = 112
                Width = 288
                Height = 49
                TabStop = False
                BevelInner = bvNone
                BevelOuter = bvNone
                BorderStyle = bsNone
                DataField = 'exc_resp_response'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = True
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 1
              end
            end
            object adUQRespGroup: TGroupBox
              Left = 0
              Top = 167
              Width = 403
              Height = 163
              Caption = 'UQ/STS Responsibility'
              TabOrder = 1
              object UQSTSRespCodeLabel: TLabel
                Left = 8
                Top = 23
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object adUQRespCodeLabel: TDBText
                Left = 116
                Top = 22
                Width = 116
                Height = 13
                AutoSize = True
                DataField = 'uq_resp_code'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object UQSTSOtherDescLabel: TLabel
                Left = 8
                Top = 39
                Width = 88
                Height = 13
                Caption = 'Other Description:'
              end
              object adUQRespDescLabel: TDBText
                Left = 116
                Top = 39
                Width = 115
                Height = 13
                AutoSize = True
                DataField = 'uq_resp_other_desc'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object UQSTSEssStepLabel: TLabel
                Left = 8
                Top = 58
                Width = 71
                Height = 13
                Caption = 'Essential Step:'
              end
              object adUQEssentialStepMemo: TDBText
                Left = 116
                Top = 58
                Width = 141
                Height = 13
                AutoSize = True
                DataField = 'uq_resp_ess_step'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object UQSTSDetailsLabel: TLabel
                Left = 8
                Top = 93
                Width = 36
                Height = 13
                Caption = 'Details:'
              end
              object adUQRespDetailsMemo: TDBMemo
                Left = 112
                Top = 93
                Width = 288
                Height = 65
                TabStop = False
                BevelInner = bvNone
                BevelOuter = bvNone
                BorderStyle = bsNone
                DataField = 'uq_resp_details'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = True
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 0
              end
            end
            object adUtilityRespGroup: TGroupBox
              Left = 0
              Top = 331
              Width = 403
              Height = 102
              Caption = 'Utility Responsibility'
              TabOrder = 2
              object UtilRespCodeLabel: TLabel
                Left = 7
                Top = 22
                Width = 97
                Height = 13
                Caption = 'Responsibility Code:'
              end
              object adSpecialRespCodeLabel: TDBText
                Left = 116
                Top = 22
                Width = 140
                Height = 13
                AutoSize = True
                DataField = 'spc_resp_code'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object UtilDetailsLabel: TLabel
                Left = 7
                Top = 44
                Width = 39
                Height = 13
                Caption = 'Details :'
              end
              object adSpecialRespDetailsMemo: TDBMemo
                Left = 112
                Top = 44
                Width = 289
                Height = 55
                TabStop = False
                BevelInner = bvNone
                BevelOuter = bvNone
                BorderStyle = bsNone
                DataField = 'spc_resp_details'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentColor = True
                ParentFont = False
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 0
              end
            end
            object ApprovalGroup: TGroupBox
              Left = 406
              Top = 184
              Width = 375
              Height = 249
              Caption = 'Approval'
              TabOrder = 3
              object DefaultEstLabel: TLabel
                Left = 16
                Top = 29
                Width = 130
                Height = 13
                Caption = 'Default / Current Estimate:'
              end
              object adEstimateLabel: TLabel
                Left = 149
                Top = 29
                Width = 31
                Height = 13
                Caption = '$0.00'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ApprovalDateTimeLabel: TLabel
                Left = 18
                Top = 178
                Width = 59
                Height = 13
                Caption = 'Date / Time:'
              end
              object adApproveDateTimeDBLabel: TDBText
                Left = 92
                Top = 178
                Width = 162
                Height = 13
                AutoSize = True
                DataField = 'approved_datetime'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ApprovedByLabel: TLabel
                Left = 18
                Top = 205
                Width = 66
                Height = 13
                Caption = 'Approved By:'
              end
              object ApprovedByDBText: TDBText
                Left = 92
                Top = 205
                Width = 110
                Height = 13
                AutoSize = True
                DataField = 'approved_by_name'
                DataSource = DamageSource
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object EstimateAgreeCheckBox: TDBCheckBox
                Left = 257
                Top = 27
                Width = 80
                Height = 17
                Alignment = taLeftJustify
                Caption = 'I Agree'
                DataField = 'estimate_agreed'
                DataSource = DamageSource
                TabOrder = 0
                ValueChecked = 'True'
                ValueUnchecked = 'False'
              end
              object adCertifyStatementMemo: TMemo
                Left = 16
                Top = 51
                Width = 333
                Height = 102
                BevelInner = bvNone
                BevelOuter = bvNone
                BorderStyle = bsNone
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Lines.Strings = (
                  'I certify that the damage package contains all of the '
                  'required attachments and documents (as available / '
                  'applicable) necessary to support the responsibility '
                  'code applied by the Investigator.  Also, I agree that '
                  'the current damage cost estimate is adequate based '
                  'on my review.')
                ParentColor = True
                ParentFont = False
                ReadOnly = True
                TabOrder = 1
              end
              object ApproveCheckbox: TCheckBox
                Left = 16
                Top = 155
                Width = 89
                Height = 17
                Alignment = taLeftJustify
                BiDiMode = bdLeftToRight
                Caption = 'Approve'
                ParentBiDiMode = False
                TabOrder = 2
              end
              object PrintApprovalDetailsButton: TButton
                Left = 252
                Top = 212
                Width = 115
                Height = 25
                Action = PrintApprovalDetailsAction
                TabOrder = 3
              end
            end
            object GroupBox1: TGroupBox
              Left = 406
              Top = 0
              Width = 375
              Height = 186
              Caption = 'Missing Required Documents'
              TabOrder = 4
              object ApprovalMissingDocGrid: TcxGrid
                Left = 2
                Top = 15
                Width = 371
                Height = 169
                Align = alClient
                TabOrder = 0
                object ApprovalMissingDocDBTableView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  FilterBox.Visible = fvNever
                  DataController.DataSource = DocumentSource
                  DataController.Filter.MaxValueListCount = 1000
                  DataController.KeyFieldNames = 'required_doc_id'
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  Filtering.ColumnPopup.MaxDropDownItemCount = 12
                  OptionsData.Deleting = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideSelection = True
                  OptionsSelection.InvertSelect = False
                  OptionsView.GridLineColor = clBtnFace
                  OptionsView.GroupByBox = False
                  OptionsView.GroupFooters = gfVisibleWhenExpanded
                  Preview.AutoHeight = False
                  Preview.MaxLineCount = 2
                  object ColApprovalMissingDocName: TcxGridDBColumn
                    Caption = 'Document Type'
                    DataBinding.FieldName = 'name'
                    PropertiesClassName = 'TcxTextEditProperties'
                    Properties.Alignment.Horz = taLeftJustify
                    Properties.MaxLength = 100
                    Properties.ReadOnly = False
                    Options.Editing = False
                    Options.Filtering = False
                    Width = 186
                  end
                  object ColApprovalMissingDocComment: TcxGridDBColumn
                    Caption = 'Reason for Missing Document'
                    DataBinding.FieldName = 'comment'
                    PropertiesClassName = 'TcxMaskEditProperties'
                    Properties.Alignment.Horz = taLeftJustify
                    Properties.MaxLength = 250
                    Properties.ReadOnly = False
                    Options.Editing = False
                    Options.Filtering = False
                    Width = 164
                  end
                  object ColApprovalMissingDocRequired: TcxGridDBColumn
                    Caption = 'Required'
                    DataBinding.FieldName = 'required'
                    PropertiesClassName = 'TcxTextEditProperties'
                    Properties.Alignment.Horz = taLeftJustify
                    Properties.MaxLength = 0
                    Properties.ReadOnly = False
                    Visible = False
                    Options.Filtering = False
                    Width = 51
                  end
                  object ColApprovalMissingDocAttachedCnt: TcxGridDBColumn
                    Caption = 'Attached Cnt'
                    DataBinding.FieldName = 'attached_cnt'
                    PropertiesClassName = 'TcxTextEditProperties'
                    Properties.Alignment.Horz = taLeftJustify
                    Properties.MaxLength = 0
                    Properties.ReadOnly = False
                    Visible = False
                    Options.Filtering = False
                    Width = 80
                  end
                end
                object ApprovalMissingDocLevel: TcxGridLevel
                  GridView = ApprovalMissingDocDBTableView
                end
              end
            end
          end
        end
      end
    end
    object ThirdPartyTab: TTabSheet
      Caption = 'Third Party'
      ImageIndex = 1
      OnShow = ThirdPartyTabShow
      object ThirdPartyPageControl: TPageControl
        Left = 0
        Top = 0
        Width = 1120
        Height = 516
        ActivePage = ThirdPartyNotifyTab
        Align = alClient
        TabOrder = 0
        OnChange = ThirdPartyPageControlChange
        object ThirdPartyNotifyTab: TTabSheet
          Caption = 'Notification'
          object EditThirdPartyPanel: TPanel
            Left = 0
            Top = 0
            Width = 1112
            Height = 294
            Align = alTop
            BevelOuter = bvNone
            Caption = ' '
            TabOrder = 0
            object Label10: TLabel
              Left = 7
              Top = 4
              Width = 45
              Height = 13
              Alignment = taRightJustify
              Caption = 'Claimant:'
            end
            object Label11: TLabel
              Left = 7
              Top = 31
              Width = 52
              Height = 13
              Alignment = taRightJustify
              Caption = 'Address 1:'
            end
            object Label12: TLabel
              Left = 7
              Top = 56
              Width = 52
              Height = 13
              Alignment = taRightJustify
              Caption = 'Address 2:'
            end
            object Label13: TLabel
              Left = 7
              Top = 82
              Width = 23
              Height = 13
              Alignment = taRightJustify
              Caption = 'City:'
            end
            object Label14: TLabel
              Left = 268
              Top = 82
              Width = 30
              Height = 13
              Alignment = taRightJustify
              Caption = 'State:'
            end
            object Label15: TLabel
              Left = 373
              Top = 82
              Width = 46
              Height = 13
              Alignment = taRightJustify
              Caption = 'Zip Code:'
            end
            object Label16: TLabel
              Left = 7
              Top = 108
              Width = 34
              Height = 13
              Alignment = taRightJustify
              Caption = 'Phone:'
            end
            object Label17: TLabel
              Left = 7
              Top = 134
              Width = 85
              Height = 13
              Alignment = taRightJustify
              Caption = 'Claim Description:'
            end
            object Label18: TLabel
              Left = 352
              Top = 6
              Width = 54
              Height = 13
              Alignment = taRightJustify
              Caption = 'Damage #:'
            end
            object ThirdPartyUQDamageID: TDBText
              Left = 442
              Top = 6
              Width = 115
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'uq_damage_id'
              DataSource = ThirdPartySource
              ParentColor = False
            end
            object Label19: TLabel
              Left = 352
              Top = 29
              Width = 84
              Height = 26
              Caption = '3rd Party Notification Date:'
              WordWrap = True
            end
            object ThirdPartyNotificationDate: TDBText
              Left = 442
              Top = 42
              Width = 127
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'uq_notified_date'
              DataSource = ThirdPartySource
              ParentColor = False
            end
            object Label20: TLabel
              Left = 7
              Top = 205
              Width = 88
              Height = 13
              Alignment = taRightJustify
              Caption = 'Insurance Carrier:'
            end
            object Label21: TLabel
              Left = 253
              Top = 203
              Width = 50
              Height = 13
              Alignment = taRightJustify
              Caption = 'Dedictible:'
            end
            object ThirdPartyDeductible: TDBText
              Left = 310
              Top = 203
              Width = 148
              Height = 17
              DataField = 'deductible'
              DataSource = ThirdPartySource
            end
            object Label22: TLabel
              Left = 7
              Top = 245
              Width = 71
              Height = 13
              Alignment = taRightJustify
              Caption = 'Claim Demand:'
            end
            object Label23: TLabel
              Left = 217
              Top = 244
              Width = 83
              Height = 13
              Alignment = taRightJustify
              Caption = '3rd Party Status:'
            end
            object ThirdPartyClaimant: TDBEdit
              Left = 99
              Top = 0
              Width = 191
              Height = 21
              DataField = 'claimant'
              DataSource = ThirdPartySource
              TabOrder = 0
            end
            object ThirdPartyAddress1: TDBEdit
              Left = 99
              Top = 26
              Width = 191
              Height = 21
              DataField = 'address1'
              DataSource = ThirdPartySource
              TabOrder = 1
            end
            object ThirdPartyAddress2: TDBEdit
              Left = 99
              Top = 52
              Width = 191
              Height = 21
              DataField = 'address2'
              DataSource = ThirdPartySource
              TabOrder = 2
            end
            object ThirdPartyCity: TDBEdit
              Left = 99
              Top = 79
              Width = 155
              Height = 21
              DataField = 'city'
              DataSource = ThirdPartySource
              TabOrder = 3
            end
            object ThirdPartyState: TDBComboBox
              Left = 305
              Top = 79
              Width = 50
              Height = 21
              CharCase = ecUpperCase
              DataField = 'state'
              DataSource = ThirdPartySource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 4
            end
            object ThirdPartyZip: TDBEdit
              Left = 424
              Top = 78
              Width = 58
              Height = 21
              DataField = 'zipcode'
              DataSource = ThirdPartySource
              TabOrder = 5
            end
            object ThirdPartyPhone: TDBEdit
              Left = 99
              Top = 105
              Width = 155
              Height = 21
              DataField = 'phone'
              DataSource = ThirdPartySource
              TabOrder = 6
            end
            object ThirdPartyClaimDesc: TDBMemo
              Left = 99
              Top = 132
              Width = 390
              Height = 66
              DataField = 'claim_desc'
              DataSource = ThirdPartySource
              ScrollBars = ssVertical
              TabOrder = 7
            end
            object ThirdPartyCarrierOnNotice: TDBCheckBox
              Left = 7
              Top = 223
              Width = 105
              Height = 17
              Alignment = taLeftJustify
              Caption = 'Carrier On Notice:'
              DataField = 'carrier_notified'
              DataSource = ThirdPartySource
              TabOrder = 9
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
            object ThirdPartyDemand: TDBEdit
              Left = 99
              Top = 241
              Width = 99
              Height = 21
              DataField = 'demand'
              DataSource = ThirdPartySource
              TabOrder = 10
            end
            object ThirdPartyStatus: TDBComboBox
              Left = 307
              Top = 240
              Width = 145
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'claim_status'
              DataSource = ThirdPartySource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 11
            end
            object ThirdPartyInsurance: TcxDBExtLookupComboBox
              Left = 99
              Top = 201
              DataBinding.DataField = 'insur_name'
              DataBinding.DataSource = ThirdPartySource
              Properties.DropDownSizeable = True
              Properties.DropDownWidth = 260
              Properties.View = CarrierLookupView
              Properties.KeyFieldNames = 'carrier_id'
              Properties.ListFieldItem = ColCarrierLookupName
              TabOrder = 8
              Width = 148
            end
          end
          object SaveThirdPartyButton: TButton
            Left = 378
            Top = 265
            Width = 75
            Height = 25
            Action = ThirdPartySaveAction
            TabOrder = 1
          end
          object CancelThirdPartyButton: TButton
            Left = 456
            Top = 265
            Width = 75
            Height = 25
            Action = ThirdPartyCancelAction
            TabOrder = 2
          end
          object ThirdPartyGrid: TDBGrid
            Left = 0
            Top = 294
            Width = 1112
            Height = 194
            Align = alClient
            DataSource = ThirdPartySource
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 3
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'claimant'
                Title.Caption = 'Claimant'
                Width = 156
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'uq_notified_date'
                Title.Caption = 'Notified Date'
                Width = 116
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'address1'
                Title.Caption = 'Address'
                Width = 91
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'city'
                Title.Caption = 'City'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'state'
                Title.Caption = 'State'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'phone'
                Title.Caption = 'Phone'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'claim_status'
                Title.Caption = 'Status'
                Visible = True
              end>
          end
          object ThirdPartyAddButton: TButton
            Left = 535
            Top = 265
            Width = 108
            Height = 25
            Action = ThirdPartyAddAction
            TabOrder = 4
          end
        end
        object ThirdPartyNotesTab: TTabSheet
          Caption = 'Notes'
          ImageIndex = 4
          inline ThirdPartyNotesFrame: TNotesFrame
            Left = 0
            Top = 39
            Width = 1112
            Height = 449
            Align = alClient
            TabOrder = 0
            inherited NotesGrid: TcxGrid
              Width = 1112
              Height = 318
              inherited NotesGridView: TcxGridDBTableView
                inherited NotesGridViewnotes_type_desc1: TcxGridDBColumn
                  Width = 151
                end
                inherited NotesGridViewClaimant: TcxGridDBColumn
                  Width = 61
                end
                inherited NotesGridViewentry_date1: TcxGridDBColumn
                  Width = 115
                end
                inherited NotesGridViewemployee_name1: TcxGridDBColumn
                  Width = 150
                end
                inherited NotesGridRestrictionCol: TcxGridDBColumn
                  Width = 195
                end
              end
            end
            inherited HeaderPanel: TPanel
              Width = 1112
              inherited NoteLabel: TLabel
                Caption = 'Note Text:'
              end
            end
            inherited FooterPanel: TPanel
              Top = 423
              Width = 1112
            end
          end
          object TPRestrictionPanel: TPanel
            Left = 0
            Top = 0
            Width = 1112
            Height = 39
            Align = alTop
            BevelEdges = []
            BevelOuter = bvNone
            TabOrder = 1
            object TPRestrictionLabel: TLabel
              Left = 38
              Top = 5
              Width = 111
              Height = 13
              Caption = 'Restriction (Sub Type):'
              WordWrap = True
            end
            object TPSubTypeCombo: TComboBox
              Left = 37
              Top = 18
              Width = 243
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = TPSubTypeComboChange
              Items.Strings = (
                'Ticket'
                'Locate 1'
                'Locate 2'
                'Locate 3')
            end
          end
        end
        object ThirdPartyEstimateTab: TTabSheet
          Caption = 'Estimates'
          ImageIndex = 2
          inline ThirdPartyEstimateFrame: TEstimateFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited EstimatesGrid: TDBGrid
              Width = 1112
              Height = 381
              TitleFont.Charset = ANSI_CHARSET
              Columns = <
                item
                  Expanded = False
                  FieldName = 'estimate_type_desc'
                  Title.Caption = 'Type'
                  Width = 91
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'company'
                  Title.Caption = 'Claimant'
                  Visible = False
                end
                item
                  Expanded = False
                  FieldName = 'modified_date'
                  Title.Caption = 'Date'
                  Width = 147
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'employee_name'
                  Title.Caption = 'Employee'
                  Width = 114
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'amount'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Amount'
                  Width = 67
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'contract'
                  Title.Caption = 'Contract'
                  Width = 83
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'comment'
                  Title.Caption = 'Comment'
                  Width = 219
                  Visible = True
                end>
            end
            inherited NewEstimatePanel: TPanel
              Width = 1112
              inherited EstimateAmountLabel: TLabel
                Left = 5
                Width = 85
                Alignment = taLeftJustify
                Caption = 'Estimate Amount:'
              end
              inherited EstimateCommentLabel: TLabel
                Left = 268
                Width = 49
                Caption = 'Comment:'
              end
              inherited Label2: TLabel
                Left = 5
                Width = 45
                Alignment = taLeftJustify
                Caption = 'Claimant:'
                Visible = True
              end
              inherited EstimateCompanyLabel: TLabel
                Left = 97
                Top = 60
                Visible = True
              end
              inherited AccrualAmountLabel: TLabel
                Left = 5
                Width = 79
                Alignment = taLeftJustify
                Caption = 'Accrual Amount:'
              end
              inherited NewEstimateButton: TButton
                Width = 149
                Caption = 'Add New 3rd Party Estimate'
              end
              inherited EstimateComment: TEdit
                Width = 269
              end
            end
            inherited Panel1: TPanel
              Top = 462
              Width = 1112
            end
          end
        end
        object ThirdPartyInvoiceTab: TTabSheet
          Caption = 'Invoices'
          ImageIndex = 3
          inline ThirdPartyInvoiceFrame: TDamageInvoiceFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited FooterPanel: TPanel
              Top = 460
              Width = 1112
              DesignSize = (
                1112
                28)
              inherited ViewAllCk: TCheckBox
                Left = 963
              end
            end
            inherited EditInvoicePanel: TPanel
              Width = 1112
              Height = 323
              inherited AccrualCodeLabel: TLabel
                Top = 271
                Visible = False
              end
              inherited InvoiceGroup: TGroupBox
                Height = 187
                inherited AmountLabel: TLabel
                  Left = 15
                end
                inherited InvoiceCompanyLabel: TLabel
                  Left = 7
                end
                inherited InvoiceNumberLabel: TLabel
                  Left = 15
                end
                inherited ReceivedDateLabel: TLabel
                  Left = 20
                end
                inherited CommentsLabel: TLabel
                  Left = 39
                end
                inherited InvoiceDateLabel: TLabel
                  Left = 29
                end
                inherited ClaimantLabel: TLabel
                  Left = 49
                end
                inherited Comments: TDBEdit
                  Top = 158
                end
              end
              inherited DamageGroup: TGroupBox
                Top = 191
                inherited DamageStateLabel: TLabel
                  Left = 22
                end
                inherited DamageCityLabel: TLabel
                  Left = 29
                end
                inherited DamageDateLabel: TLabel
                  Left = 25
                end
              end
              inherited AccrualCode: TComboBox
                Visible = False
              end
            end
            inherited InvoiceGrid: TcxGrid
              Top = 323
              Width = 1112
              Height = 137
              inherited InvoiceView: TcxGridDBTableView
                inherited ColInvoiceID: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
                inherited ColAmount: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
                inherited ColPaidAmount: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Attachments'
          ImageIndex = 4
          inline ThirdPartyAttachmentsFrame: TAttachmentsFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited AttachmentsPanel: TPanel
              Width = 1112
              Height = 457
              inherited AttachmentList: TcxGrid
                Width = 1102
                Height = 447
              end
            end
            inherited FooterPanel: TPanel
              Top = 457
              Width = 1112
            end
          end
        end
        object ThirdPartyHistory: TTabSheet
          Caption = 'History'
          ImageIndex = 5
          object ThirdPartyHistoryGrid: TDBGrid
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            DataSource = HistorySource
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'modified_date'
                Title.Caption = 'Change Date'
                Width = 135
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'modified_by'
                Title.Caption = 'Modified By'
                Visible = False
              end
              item
                Expanded = False
                FieldName = 'uq_resp_code'
                Title.Caption = 'UQ/STS Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'uq_resp_type'
                Title.Caption = 'UQ/STS Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'exc_resp_code'
                Title.Caption = 'Exc Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'exc_resp_type'
                Title.Caption = 'Exc Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'spc_resp_code'
                Title.Caption = 'Spc Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'spc_resp_type'
                Title.Caption = 'Spc Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'profit_center'
                Title.Caption = 'Profit Center'
                Width = 70
                Visible = True
              end>
          end
        end
      end
    end
    object LitigationTab: TTabSheet
      Caption = 'Litigation'
      ImageIndex = 2
      OnShow = LitigationTabShow
      object LitigationPageControl: TPageControl
        Left = 0
        Top = 0
        Width = 1120
        Height = 516
        ActivePage = LitigationNotesTab
        Align = alClient
        TabOrder = 0
        OnChange = LitigationPageControlChange
        object LitigationNotifyTab: TTabSheet
          Caption = 'Notification'
          object DamageLitigationGrid: TDBGrid
            Left = 0
            Top = 258
            Width = 1112
            Height = 230
            Align = alClient
            DataSource = LitigationSource
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'file_number'
                Title.Caption = 'Case #'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'plaintiff'
                Title.Caption = 'Plaintiff'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'defendant'
                Title.Caption = 'Defendant'
                Width = 98
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'venue'
                Title.Caption = 'Venue'
                Width = 92
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'venue_state'
                Title.Caption = 'Venue St'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'service_date'
                Title.Caption = 'Service Date'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'attorney'
                Title.Caption = 'Attorney'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'insur_name'
                Title.Caption = 'Insurance'
                Width = 74
                Visible = True
              end
              item
                Alignment = taRightJustify
                Expanded = False
                FieldName = 'deductible'
                Title.Alignment = taRightJustify
                Title.Caption = 'Deductible'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'demand'
                Title.Caption = 'Demand'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'summons_date'
                Title.Caption = 'Summons Date'
                Width = 83
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'claim_status'
                Title.Caption = 'Status'
                Width = 74
                Visible = True
              end>
          end
          object EditLitigationPanel: TPanel
            Left = 0
            Top = 0
            Width = 1112
            Height = 258
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object CaseCaptionLabel: TLabel
              Left = 6
              Top = 9
              Width = 68
              Height = 13
              Alignment = taRightJustify
              Caption = 'Case Caption:'
            end
            object vsLabel: TLabel
              Left = 261
              Top = 9
              Width = 15
              Height = 13
              Caption = 'vs.'
            end
            object CaseNumLabel: TLabel
              Left = 6
              Top = 32
              Width = 69
              Height = 13
              Alignment = taRightJustify
              Caption = 'Court Case #:'
            end
            object VenueLabel: TLabel
              Left = 6
              Top = 56
              Width = 34
              Height = 13
              Alignment = taRightJustify
              Caption = 'Venue:'
            end
            object Label5: TLabel
              Left = 494
              Top = 9
              Width = 54
              Height = 13
              Alignment = taRightJustify
              Caption = 'Damage #:'
            end
            object UQDamageID: TDBText
              Left = 561
              Top = 9
              Width = 65
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'uq_damage_id'
              DataSource = LitigationSource
              ParentColor = False
            end
            object SummonsDateLabel: TLabel
              Left = 426
              Top = 31
              Width = 122
              Height = 13
              Alignment = taRightJustify
              Caption = 'Summons Received Date:'
            end
            object SummonsReceivedDate: TDBText
              Left = 561
              Top = 31
              Width = 112
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'summons_date'
              DataSource = LitigationSource
              ParentColor = False
            end
            object ResponseDateLabel: TLabel
              Left = 471
              Top = 54
              Width = 77
              Height = 13
              Alignment = taRightJustify
              Caption = 'Response Date:'
            end
            object ResponseDate: TDBText
              Left = 561
              Top = 54
              Width = 70
              Height = 13
              AutoSize = True
              Color = clBtnFace
              DataField = 'respond_by_date'
              DataSource = LitigationSource
              ParentColor = False
            end
            object VenueStateLabel: TLabel
              Left = 6
              Top = 80
              Width = 63
              Height = 13
              Alignment = taRightJustify
              Caption = 'Venue State:'
            end
            object Label6: TLabel
              Left = 6
              Top = 104
              Width = 78
              Height = 13
              Alignment = taRightJustify
              Caption = 'Date of Service:'
            end
            object Label7: TLabel
              Left = 6
              Top = 127
              Width = 90
              Height = 13
              Alignment = taRightJustify
              Caption = 'Defense Attorney:'
            end
            object Label8: TLabel
              Left = 6
              Top = 149
              Width = 88
              Height = 13
              Alignment = taRightJustify
              Caption = 'Insurance Carrier:'
            end
            object DedictibleLabel: TLabel
              Left = 6
              Top = 170
              Width = 54
              Height = 13
              Alignment = taRightJustify
              Caption = 'Deductible:'
            end
            object Deductible: TDBText
              Left = 99
              Top = 170
              Width = 148
              Height = 17
              DataField = 'deductible'
              DataSource = LitigationSource
            end
            object DemandLabel: TLabel
              Left = 6
              Top = 212
              Width = 71
              Height = 13
              Alignment = taRightJustify
              Caption = 'Claim Demand:'
            end
            object Label9: TLabel
              Left = 6
              Top = 235
              Width = 81
              Height = 13
              Alignment = taRightJustify
              Caption = 'Litigation Status:'
            end
            object SaveLitigationButton: TButton
              Left = 393
              Top = 230
              Width = 75
              Height = 25
              Action = LitigationSaveAction
              TabOrder = 0
            end
            object CancelLitigationButton: TButton
              Left = 471
              Top = 230
              Width = 75
              Height = 25
              Action = LitigationCancelAction
              TabOrder = 1
            end
            object Plaintiff: TDBEdit
              Left = 99
              Top = 5
              Width = 155
              Height = 21
              DataField = 'plaintiff'
              DataSource = LitigationSource
              TabOrder = 2
            end
            object Defendant: TDBEdit
              Left = 283
              Top = 5
              Width = 155
              Height = 21
              DataField = 'defendant'
              DataSource = LitigationSource
              TabOrder = 3
            end
            object FileNumber: TDBEdit
              Left = 99
              Top = 29
              Width = 155
              Height = 21
              DataField = 'file_number'
              DataSource = LitigationSource
              TabOrder = 4
            end
            object Venue: TDBEdit
              Left = 99
              Top = 52
              Width = 339
              Height = 21
              DataField = 'venue'
              DataSource = LitigationSource
              TabOrder = 5
            end
            object LitigationVenueState: TDBComboBox
              Left = 99
              Top = 76
              Width = 78
              Height = 21
              CharCase = ecUpperCase
              DataField = 'venue_state'
              DataSource = LitigationSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 6
            end
            object LitigationAttorney: TDBComboBox
              Left = 99
              Top = 122
              Width = 148
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'attorney'
              DataSource = LitigationSource
              ItemHeight = 13
              TabOrder = 8
            end
            object Demand: TDBEdit
              Left = 99
              Top = 208
              Width = 99
              Height = 21
              DataField = 'demand'
              DataSource = LitigationSource
              TabOrder = 11
            end
            object CarrierOnNotice: TDBCheckBox
              Left = 6
              Top = 186
              Width = 107
              Height = 17
              Alignment = taLeftJustify
              Caption = 'Carrier On Notice:   '
              DataField = 'carrier_notified'
              DataSource = LitigationSource
              TabOrder = 10
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
            object LitigationStatus: TDBComboBox
              Left = 99
              Top = 231
              Width = 162
              Height = 21
              Style = csDropDownList
              BevelWidth = 0
              DataField = 'claim_status'
              DataSource = LitigationSource
              DropDownCount = 14
              ItemHeight = 13
              TabOrder = 12
            end
            object DateOfService: TcxDBDateEdit
              Left = 99
              Top = 99
              DataBinding.DataField = 'service_date'
              DataBinding.DataSource = LitigationSource
              Properties.ButtonGlyph.Data = {
                C2030000424DC203000000000000B6000000280000000F0000000D0000000100
                2000000000000C03000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808000008080
                8000808080008080800080808000808080008080800080808000808080008080
                800080808000808080008080800080808000808000008080000080808000FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF0080808000808000008080000080808000FFFFFF0080000000FFFFFF008000
                0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080000000FFFFFF008080
                8000808000008080000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
                00008080000080808000FFFFFF0080000000FFFFFF0080000000FFFFFF008000
                0000FFFFFF0080000000FFFFFF0080000000FFFFFF0080808000808000008080
                000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080800000808000008080
                8000808080008080800080808000808080008080800080808000808080008080
                800080808000808080008080800080808000808000008080000080808000FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF0080808000808000008080000080808000FFFFFF008000
                0000FFFFFF008000000080000000800000008000000080000000FFFFFF008000
                0000FFFFFF0080808000808000008080000080808000FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00808080008080000080800000808080008080800080808000808080008080
                8000808080008080800080808000808080008080800080808000808080008080
                800080800000}
              TabOrder = 7
              Width = 148
            end
            object LitigationCarrier: TcxDBExtLookupComboBox
              Left = 99
              Top = 145
              DataBinding.DataField = 'insur_name'
              DataBinding.DataSource = LitigationSource
              Properties.DropDownSizeable = True
              Properties.DropDownWidth = 260
              Properties.View = CarrierLookupView
              Properties.KeyFieldNames = 'carrier_id'
              Properties.ListFieldItem = ColCarrierLookupName
              TabOrder = 9
              Width = 148
            end
          end
          object AddLitigationButton: TButton
            Left = 551
            Top = 230
            Width = 105
            Height = 25
            Action = LitigationAddAction
            TabOrder = 2
          end
        end
        object LitigationNotesTab: TTabSheet
          Caption = 'Notes'
          ImageIndex = 5
          inline LitigationNotesFrame: TNotesFrame
            Left = 0
            Top = 39
            Width = 1112
            Height = 449
            Align = alClient
            TabOrder = 0
            inherited NotesGrid: TcxGrid
              Top = 104
              Width = 1112
              Height = 319
              inherited NotesGridView: TcxGridDBTableView
                inherited NotesGridViewnotes_type_desc1: TcxGridDBColumn
                  Width = 151
                end
                inherited NotesGridViewClaimant: TcxGridDBColumn
                  Width = 75
                end
                inherited NotesGridViewentry_date1: TcxGridDBColumn
                  Width = 115
                end
                inherited NotesGridViewemployee_name1: TcxGridDBColumn
                  Width = 150
                end
                inherited NotesGridRestrictionCol: TcxGridDBColumn
                  Width = 199
                end
              end
            end
            inherited HeaderPanel: TPanel
              Width = 1112
              Height = 104
              inherited NoteLabel: TLabel
                Caption = 'Note Text:'
              end
            end
            inherited FooterPanel: TPanel
              Top = 423
              Width = 1112
            end
          end
          object RestrictionPanel: TPanel
            Left = 0
            Top = 0
            Width = 1112
            Height = 39
            Align = alTop
            BevelEdges = []
            BevelOuter = bvNone
            TabOrder = 1
            object Label24: TLabel
              Left = 38
              Top = 5
              Width = 111
              Height = 13
              Caption = 'Restriction (Sub Type):'
              WordWrap = True
            end
            object LitSubTypeCombo: TComboBox
              Left = 37
              Top = 18
              Width = 243
              Height = 21
              ItemHeight = 13
              TabOrder = 0
              OnChange = LitSubTypeComboChange
              Items.Strings = (
                'Ticket'
                'Locate 1'
                'Locate 2'
                'Locate 3')
            end
          end
        end
        object LitigationEstimateTab: TTabSheet
          Caption = 'Estimates'
          ImageIndex = 2
          object EstimateAmountLabel: TLabel
            Left = 1
            Top = 11
            Width = 81
            Height = 13
            Alignment = taRightJustify
            Caption = 'Estimate Amount'
          end
          inline LitigationEstimateFrame: TEstimateFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited EstimatesGrid: TDBGrid
              Width = 1112
              Height = 381
              TitleFont.Charset = ANSI_CHARSET
              Columns = <
                item
                  Expanded = False
                  FieldName = 'estimate_type_desc'
                  Title.Caption = 'Type'
                  Width = 91
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'company'
                  Title.Caption = 'Claimant'
                  Visible = False
                end
                item
                  Expanded = False
                  FieldName = 'modified_date'
                  Title.Caption = 'Date'
                  Width = 147
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'employee_name'
                  Title.Caption = 'Employee'
                  Width = 114
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'amount'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Amount'
                  Width = 67
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'contract'
                  Title.Caption = 'Contract'
                  Width = 83
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'comment'
                  Title.Caption = 'Comment'
                  Width = 219
                  Visible = True
                end>
            end
            inherited NewEstimatePanel: TPanel
              Width = 1112
              inherited EstimateAmountLabel: TLabel
                Left = 4
                Width = 85
                Caption = 'Estimate Amount:'
              end
              inherited EstimateCommentLabel: TLabel
                Left = 269
                Width = 49
                Caption = 'Comment:'
              end
              inherited Label2: TLabel
                Left = 4
                Width = 45
                Caption = 'Claimant:'
              end
              inherited AccrualAmountLabel: TLabel
                Left = 4
                Width = 79
                Caption = 'Accrual Amount:'
              end
              inherited NewEstimateButton: TButton
                Left = 323
                Width = 145
                Caption = 'Add New Litigation Estimate'
              end
              inherited EstimateComment: TEdit
                Width = 353
              end
            end
            inherited Panel1: TPanel
              Top = 462
              Width = 1112
            end
            inherited EstimateSource: TDataSource
              Left = 548
              Top = 316
            end
          end
        end
        object LitigationInvoiceTab: TTabSheet
          Caption = 'Invoices'
          ImageIndex = 3
          inline LitigationInvoiceFrame: TDamageInvoiceFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited FooterPanel: TPanel
              Top = 460
              Width = 1112
              DesignSize = (
                1112
                28)
              inherited ViewAllCk: TCheckBox
                Left = 935
              end
            end
            inherited EditInvoicePanel: TPanel
              Width = 1112
              inherited AccrualCodeLabel: TLabel
                Visible = False
              end
              inherited InvoiceGroup: TGroupBox
                inherited AmountLabel: TLabel
                  Left = 14
                end
                inherited InvoiceNumberLabel: TLabel
                  Top = 20
                end
              end
              inherited PaymentGroup: TGroupBox
                Width = 296
                inherited InvoiceApprovedLabel: TLabel
                  Top = 163
                  Width = 115
                  Height = 13
                end
                inherited PaymentCodeLabel: TLabel
                  Visible = True
                end
                inherited AmountApproved: TDBEdit
                  Left = 132
                end
                inherited ApprovedDate: TcxDBDateEdit
                  Left = 132
                end
                inherited AmountPaid: TDBEdit
                  Left = 132
                end
                inherited PaidDate: TcxDBDateEdit
                  Left = 132
                end
                inherited Satisfied: TDBCheckBox
                  Left = 132
                  Top = 130
                end
                inherited InvoiceApproved: TDBCheckBox
                  Left = 132
                  Top = 157
                end
                inherited PaymentCode: TDBComboBox
                  Left = 132
                  Visible = True
                end
              end
              inherited PacketGroup: TGroupBox
                Width = 296
                inherited PacketRequested: TcxDBDateEdit
                  Left = 132
                end
                inherited PacketReceived: TcxDBDateEdit
                  Left = 132
                end
              end
              inherited AccrualCode: TComboBox
                Left = 509
                Visible = False
              end
            end
            inherited InvoiceGrid: TcxGrid
              Width = 1112
              Height = 143
              inherited InvoiceView: TcxGridDBTableView
                inherited ColInvoiceID: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
                inherited ColAmount: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
                inherited ColPaidAmount: TcxGridDBColumn
                  Properties.Alignment.Horz = taRightJustify
                  HeaderAlignmentHorz = taRightJustify
                end
              end
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Attachments'
          ImageIndex = 4
          inline LitigationAttachmentsFrame: TAttachmentsFrame
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            TabOrder = 0
            inherited AttachmentsPanel: TPanel
              Width = 1112
              Height = 457
              inherited AttachmentList: TcxGrid
                Width = 1102
                Height = 447
                LookAndFeel.Kind = lfFlat
              end
            end
            inherited FooterPanel: TPanel
              Top = 457
              Width = 1112
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'History'
          ImageIndex = 5
          object LitigationHistoryGrid: TDBGrid
            Left = 0
            Top = 0
            Width = 1112
            Height = 488
            Align = alClient
            DataSource = HistorySource
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'modified_date'
                Title.Caption = 'Change Date'
                Width = 135
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'modified_by'
                Title.Caption = 'Modified By'
                Visible = False
              end
              item
                Expanded = False
                FieldName = 'uq_resp_code'
                Title.Caption = 'UQ/STS Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'uq_resp_type'
                Title.Caption = 'UQ/STS Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'exc_resp_code'
                Title.Caption = 'Exc Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'exc_resp_type'
                Title.Caption = 'Exc Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'spc_resp_code'
                Title.Caption = 'Spc Code'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'spc_resp_type'
                Title.Caption = 'Spc Type'
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'profit_center'
                Title.Caption = 'Profit Center'
                Width = 70
                Visible = True
              end>
          end
        end
      end
    end
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 0
    Width = 1128
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object InvestigationIsLabel: TLabel
      Left = 3
      Top = 10
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Investigation is '
    end
    object DamageTypeLabel: TLabel
      Left = 81
      Top = 10
      Width = 29
      Height = 13
      Caption = 'Open'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object SaveButton: TButton
      Left = 301
      Top = 4
      Width = 80
      Height = 25
      Action = DamageSaveAction
      TabOrder = 0
    end
    object CancelButton: TButton
      Left = 387
      Top = 4
      Width = 80
      Height = 25
      Action = DamageCancelAction
      TabOrder = 1
    end
    object PrintButton: TButton
      Left = 471
      Top = 4
      Width = 80
      Height = 25
      Action = PrintAction
      TabOrder = 2
    end
    object CloseButton: TButton
      Left = 555
      Top = 4
      Width = 123
      Height = 25
      Action = CloseAction
      TabOrder = 3
    end
    object ArriveButtonPanel: TPanel
      Left = 209
      Top = 1
      Width = 91
      Height = 31
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 4
      object ArriveButton: TButton
        Left = 3
        Top = 3
        Width = 85
        Height = 25
        Caption = 'I Arrived'
        TabOrder = 0
        OnMouseUp = ArriveButtonMouseUp
      end
    end
  end
  object Actions: TActionList
    OnUpdate = ActionsUpdate
    Left = 749
    object DamageCancelAction: TAction
      Caption = 'Cancel'
      OnExecute = DamageCancelActionExecute
    end
    object DamageSaveAction: TAction
      Caption = 'Save'
      OnExecute = DamageSaveActionExecute
    end
    object PrintAction: TAction
      Caption = 'Print'
      OnExecute = PrintActionExecute
    end
    object CloseAction: TAction
      Caption = 'Close'
      OnExecute = CloseActionExecute
    end
    object LitigationSaveAction: TAction
      Caption = 'Save'
      OnExecute = LitigationSaveActionExecute
    end
    object LitigationCancelAction: TAction
      Caption = 'Cancel'
      OnExecute = LitigationCancelActionExecute
    end
    object ThirdPartyAddAction: TAction
      Caption = 'Add New Third Party'
      OnExecute = ThirdPartyAddActionExecute
    end
    object LitigationAddAction: TAction
      Caption = 'Add New Litigation'
      OnExecute = LitigationAddActionExecute
    end
    object ThirdPartySaveAction: TAction
      Caption = 'Save'
      OnExecute = ThirdPartySaveActionExecute
    end
    object ThirdPartyCancelAction: TAction
      Caption = 'Cancel'
      OnExecute = ThirdPartyCancelActionExecute
    end
    object PrintApprovalDetailsAction: TAction
      Caption = 'Print Approval Details'
      OnExecute = PrintApprovalDetailsActionExecute
    end
    object ArriveOnSiteAction: TAction
      Caption = 'I Arrived'
      OnExecute = ArriveOnSiteActionExecute
    end
    object ArriveOffSiteAction: TAction
      Caption = 'ArriveOffSiteAction'
      OnExecute = ArriveOffSiteActionExecute
    end
  end
  object DamageSource: TDataSource
    DataSet = LDamageDM.Damage
    OnStateChange = DamageSourceStateChange
    OnUpdateData = DamageSourceUpdateData
    Left = 180
    Top = 325
  end
  object Investigator: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'employee'
    Left = 748
    Top = 421
  end
  object InvestigatorSource: TDataSource
    AutoEdit = False
    DataSet = Investigator
    Left = 724
    Top = 413
  end
  object Client: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'client'
    Left = 976
    Top = 121
  end
  object ClientSource: TDataSource
    AutoEdit = False
    DataSet = Client
    Left = 1004
    Top = 121
  end
  object OfficeSource: TDataSource
    AutoEdit = False
    DataSet = Office
    Left = 692
    Top = 441
  end
  object Office: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'office'
    Left = 664
    Top = 441
  end
  object Ticket: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'ticket'
    Left = 664
    Top = 413
  end
  object TicketSource: TDataSource
    AutoEdit = False
    DataSet = Ticket
    Left = 692
    Top = 413
  end
  object HistorySource: TDataSource
    DataSet = History
    Left = 748
    Top = 469
  end
  object History: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from damage_history'
      'where damage_id = :DamageID'
      'order by modified_date desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'DamageID'
        Value = 0
      end>
    Left = 720
    Top = 469
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DamageID'
        Value = 0
      end>
  end
  object InvoiceSource: TDataSource
    AutoEdit = False
    DataSet = Invoices
    Left = 692
    Top = 469
  end
  object Invoices: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from damage_invoice'
      'where damage_id = :damage_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'damage_id'
        Value = 0
      end>
    Left = 664
    Top = 468
    ParamData = <
      item
        DataType = ftInteger
        Name = 'damage_id'
        Value = 0
      end>
  end
  object DamageBillSource: TDataSource
    DataSet = DamageBill
    Left = 692
    Top = 501
  end
  object DamageBill: TDBISAMQuery
    Tag = 999
    BeforeEdit = DamageBillBeforeEdit
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from damage_bill'
      'where damage_id = :damage_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'damage_id'
        Value = 0
      end>
    Left = 664
    Top = 501
    ParamData = <
      item
        DataType = ftInteger
        Name = 'damage_id'
        Value = 0
      end>
  end
  object ThirdParty: TDBISAMTable
    AfterOpen = ThirdPartyAfterOpen
    AfterScroll = ThirdPartyAfterScroll
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'damage_third_party'
    Left = 352
    Top = 240
  end
  object ThirdPartySource: TDataSource
    DataSet = ThirdParty
    Left = 380
    Top = 244
  end
  object Litigation: TDBISAMTable
    AfterOpen = LitigationAfterOpen
    AfterScroll = LitigationAfterScroll
    OnCalcFields = LitigationCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'damage_litigation'
    Left = 676
    Top = 212
  end
  object LitigationSource: TDataSource
    DataSet = Litigation
    Left = 700
    Top = 216
  end
  object Carrier: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    IndexFieldNames = 'carrier_id'
    MasterFields = 'carrier_id'
    MasterSource = LitigationSource
    TableName = 'carrier'
    Left = 679
    Top = 243
  end
  object CarrierSource: TDataSource
    DataSet = Carrier
    Left = 707
    Top = 243
  end
  object CarrierLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from carrier'
      'order by name')
    Params = <>
    Left = 673
    Top = 277
  end
  object CarrierLookupSource: TDataSource
    DataSet = CarrierLookup
    Left = 696
    Top = 272
  end
  object ClaimCoordinatorSource: TDataSource
    DataSet = ClaimCoordinatorQuery
    Left = 748
    Top = 501
  end
  object ClaimCoordinatorQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select emp_id, short_name from employee'
      ' inner join reference on (ref_id = type_id)'
      'where type = '#39'emptype'#39' '
      'and modifier like '#39'%CC%'#39' '
      'and employee.active'
      'order by short_name')
    Params = <>
    Left = 720
    Top = 501
  end
  object AccrualHistory: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from exported_damages_history'
      'where damage_id = :DamageID'
      'order by insert_date desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'DamageID'
        Value = 0
      end>
    Left = 720
    Top = 533
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DamageID'
        Value = 0
      end>
  end
  object AccrualHistorySource: TDataSource
    DataSet = AccrualHistory
    Left = 748
    Top = 533
  end
  object AttachedDocsQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    SessionName = 'Default'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      '/* Overwritten in code */'
      'select * from attachment'
      'where foreign_type = :ForeignType and foreign_id = :ForeignID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ForeignType'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ForeignID'
        Value = 0
      end>
    ReadOnly = True
    Left = 356
    Top = 428
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ForeignType'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'ForeignID'
        Value = 0
      end>
  end
  object InvestigatorNarrativeDataSource: TDataSource
    Left = 588
    Top = 244
  end
  object InvestigatorNarrativeTable: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'notes'
    Left = 559
    Top = 239
  end
  object RequiredDocsTable: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    IndexFieldNames = 'required_doc_id'
    TableName = 'required_document'
    Left = 503
    Top = 216
  end
  object ArrivePopupMenu: TPopupMenu
    AutoPopup = False
    Left = 267
    Top = 7
    object OnsiteMenuItem: TMenuItem
      Action = ArriveOnSiteAction
      Caption = 'ONSITE'
      RadioItem = True
    end
    object OffsiteMenuItem: TMenuItem
      Action = ArriveOffSiteAction
      Caption = 'OFFSITE'
      RadioItem = True
    end
  end
  object JobsiteArrivalSource: TDataSource
    Left = 725
    Top = 391
  end
  object EmployeeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    TableName = 'employee'
    Left = 180
    Top = 352
  end
  object Document: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    IndexFieldNames = 'doc_id'
    TableName = 'document'
    Left = 468
    Top = 216
  end
  object DocumentQuery: TDBISAMQuery
    AfterEdit = DocumentQueryAfterEdit
    OnCalcFields = DocumentQueryCalcFields
    OnFilterRecord = DocumentQueryFilterRecord
    DatabaseName = 'DB1'
    SessionName = 'Default'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      '/* replaced at runtime */'
      'select rd.required_doc_id, rd.name, '
      '421819 as foreign_id, 2 as foreign_type, '
      'd.doc_id, d.comment, d.added_by_id, d.added_date '
      'from required_document rd'
      'left join document d on (rd.required_doc_id = d.required_doc_id '
      '  and foreign_type=2 '
      '  and foreign_id=421819 '
      '  and d.active)'
      'where rd.active')
    Params = <>
    Left = 469
    Top = 186
  end
  object DocumentSource: TDataSource
    DataSet = DocumentQuery
    Left = 501
    Top = 185
  end
  object ViewRepo: TcxGridViewRepository
    Left = 240
    Top = 336
    object ClientLookupView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ClientSource
      DataController.KeyFieldNames = 'client_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColClientLookupClientName: TcxGridDBBandedColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_name'
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object CarrierLookupView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = CarrierLookupSource
      DataController.KeyFieldNames = 'carrier_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColCarrierLookupCarrierID: TcxGridDBBandedColumn
        Caption = 'ID'
        DataBinding.FieldName = 'carrier_id'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ColCarrierLookupName: TcxGridDBBandedColumn
        Caption = 'Name'
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 250
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ColCarrierLookupDeductible: TcxGridDBBandedColumn
        Caption = 'Deductible'
        DataBinding.FieldName = 'deductible'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = '$,0;($,0)'
        Width = 100
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
    end
    object CoordinatorLookupView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ClaimCoordinatorSource
      DataController.KeyFieldNames = 'emp_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object ColCoordinatorLookupEmpID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'emp_id'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ColCoordinatorLookupShortName: TcxGridDBBandedColumn
        Caption = 'Name'
        DataBinding.FieldName = 'short_name'
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 904
    Top = 128
    PixelsPerInch = 96
    object cxInactive: TcxStyle
      AssignedValues = [svColor]
      Color = 14540253
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 944
    Top = 120
    object ClientFieldsView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ClientSource
      DataController.KeyFieldNames = 'Client_ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.OnGetContentStyle = ClientFieldsViewStylesGetContentStyle
      object ClientFieldsViewClient_id: TcxGridDBColumn
        Caption = 'Client ID'
        DataBinding.FieldName = 'client_id'
        Visible = False
        Options.GroupFooters = False
        Options.Grouping = False
      end
      object ClientFieldsViewClient_Name: TcxGridDBColumn
        Caption = 'Client Name'
        DataBinding.FieldName = 'client_name'
        Options.GroupFooters = False
        Options.Grouping = False
      end
      object ClientFieldsViewActive: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        Options.GroupFooters = False
      end
    end
  end
end
