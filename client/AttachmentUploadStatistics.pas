unit AttachmentUploadStatistics;

interface

uses DBISAMTb, DB;

type
  TAttachmentUploadSummary = class
  private
    FEmpID: Integer;
    AttachSummary: TDBISAMQuery;
    FLongestFileWaitSeconds: Integer;
    FPendingUploadSize: Integer;
    FPendingUploadCount: Integer;
  public
    constructor Create(const DatabaseName, SessionName: string; const EmpID: Integer);
    destructor Destroy; override;
    property PendingUploadCount: Integer read FPendingUploadCount;
    property PendingUploadSize: Integer read FPendingUploadSize;
    property LongestFileWaitSeconds: Integer read FLongestFileWaitSeconds;
    procedure GetAttachmentUploadSummary;
  end;

implementation

uses SysUtils, DateUtils, OdMiscUtils;

{ TAttachmentUploadSummary }

constructor TAttachmentUploadSummary.Create(const DatabaseName, SessionName: string; const EmpID: Integer);
begin
  AttachSummary := TDBISAMQuery.Create(nil);
  AttachSummary.DatabaseName := DatabaseName;
  AttachSummary.SessionName := SessionName;
  FEmpID := EmpID;
end;

destructor TAttachmentUploadSummary.Destroy;
begin
  FreeAndNil(AttachSummary);
  inherited;
end;

procedure TAttachmentUploadSummary.GetAttachmentUploadSummary;
const
  SelectAttachSummary = 'select count(*) pending_file_count, sum(size) pending_file_size, ' +
    'min(attach_date) oldest_attach_date from attachment where upload_date is null ' +
    'and attach_date > :ignore_before_date ' +
    'and attachment_id > 0 and active and need_to_upload and attached_by = :emp_id';
var
  OldestAttach: TDateTime;

  procedure SetupParams;
  begin
    AttachSummary.SQL.Text := SelectAttachSummary;
    AttachSummary.ParamByName('emp_id').AsInteger := FEmpID;
    AttachSummary.ParamByName('ignore_before_date').AsDateTime := Today - 60;
  end;

  procedure GetValues;
  begin
    FPendingUploadCount := AttachSummary.FieldByName('pending_file_count').AsInteger;
    FPendingUploadSize := RoundUp(AttachSummary.FieldByName('pending_file_size').AsInteger / 1000);
    OldestAttach := AttachSummary.FieldByName('oldest_attach_date').AsDateTime;
  end;

begin
  FLongestFileWaitSeconds := 0;
  FPendingUploadSize := 0;
  FPendingUploadCount := 0;

  SetupParams;
  AttachSummary.Open;
  try
    Assert(not AttachSummary.EOF, 'AttachSummary should always return one row');
    GetValues;
  finally
    AttachSummary.Close;
  end;

  if (OldestAttach = 0) or (OldestAttach > Now) then
    OldestAttach := Now;
  FLongestFileWaitSeconds := SecondsBetween(Now, OldestAttach);
end;

end.
