inherited TimesheetExportsReportForm: TTimesheetExportsReportForm
  Left = 361
  Top = 218
  Caption = 'TimesheetExportsReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 66
    Height = 13
    Caption = 'Week Ending:'
  end
  object Label2: TLabel [1]
    Left = 80
    Top = 72
    Width = 273
    Height = 57
    AutoSize = False
    Caption = 
      'This report shows which timesheet profit center exports have bee' +
      'n performed.   To perform an export, select the "Timesheet Expor' +
      't" report.'
    WordWrap = True
  end
  object FinalOnly: TCheckBox [2]
    Left = 80
    Top = 40
    Width = 257
    Height = 17
    Caption = 'Only show final export per profit center'
    TabOrder = 1
  end
  object WeekEnding: TcxDateEdit [3]
    Left = 80
    Top = 8
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = WeekEndingChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 121
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 367
  end
end
