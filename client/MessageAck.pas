unit MessageAck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, OleCtrls, SHDocVw, StdCtrls, ExtCtrls, DB,
  DBISAMTb, QMServerLibrary_Intf, QMConst, Contnrs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxHyperLinkEdit, OdMiscUtils;


const
   WM_ACK_MSG = WM_APP + 99;

type
  TCheckResponseEvent = procedure (Sender: TObject; const MessageDestID, RuleID: Integer; const Response: string; var OkToClose: Boolean) of object;

type
  TMessageAckForm = class(TEmbeddableForm)
    ButtonPanel: TPanel;
    AckButton: TButton;
    Panel2: TPanel;
    WebBrowser1: TWebBrowser;
    Messages: TDBISAMQuery;
    Memo1: TMemo;
    Timer1: TTimer;
    procedure AckButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);   //QM-1008 EB HTML fix
  private
    FOnAcked: TNotifyEvent;
    FOkToClose: Boolean;
    FClickedButtonCaption: string;
    FButtons: TObjectList;
    FOnCheckResponse: TCheckResponseEvent;
    FBreakRuleID: Integer;
    FBreakRuleType: string;
    FButtonCaptions: string;
    FMessageDestID: Integer;
    FMessagesLoaded: Boolean;  //KEC

    procedure AddFormButtons(const Captions: string);
    procedure Populate;
    procedure FilterMessages;
  public
    procedure ON_WmAckMsg(var Msg: TMessage); message WM_ACK_MSG;
    procedure ActivatingNow; override;
    procedure RefreshNow; override;
    procedure ShowBreakRuleMessages(const BreakRuleType: string);
    property OnAcked: TNotifyEvent read FOnAcked write FOnAcked;
    property OnCheckResponse: TCheckResponseEvent read FOnCheckResponse write FOnCheckResponse;
  end;
var
    msgAckHandle : THandle;
implementation

uses DMu, OdDbUtils, OdBrowserUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TMessageAckForm }

procedure TMessageAckForm.Populate;
var
  HTML: TStringList;
  Subject, Body, MessageNumber: string;
  SentDate: TDateTime;
  Destination: string;
  NoMsg : boolean;
begin
  NoMsg := false;
  FClickedButtonCaption := '';
  AckButton.Enabled := False;

  HTML := TStringList.Create;
  try
    if Messages.RecordCount = 0 then begin
      HTML.Clear;
      HTML.Add('<h2>No messages</h2>');
      HTML.Add('<hr />');
      NoMsg := true;
    end else begin
        HTML.Add('<style> ');
        HTML.Add('    #floating-panel { ');
        HTML.Add('        bottom: 10px; ');
        HTML.Add('        left: 25%;  ');
        HTML.Add('        background-color: #fff; ');
        HTML.Add('        padding: 5px; ');
        HTML.Add('        border: 1px solid #999; ');
        HTML.Add('        text-align: center; ');
        HTML.Add('        font-family: "Roboto", "sans-serif"; ');
        HTML.Add('        line-height: 30px; ');
        HTML.Add('        padding-left: 10px;  ');
        HTML.Add('    } ');
        HTML.Add('</style> ');
        HTML.Add('<!DOCTYPE html> ');
        HTML.Add('<html lang="en"> ');
        HTML.Add('<head> ');
        HTML.Add('    <meta charset="UTF-8"> ');
        HTML.Add('    <hr /> ');
        HTML.Add('    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> ');
        HTML.Add('    <title>Title</title>  ');
        HTML.Add('    <STYLE TYPE="text/css">  ');
        HTML.Add('    H1 { font-size: large; color: blue } ');
        HTML.Add('    body { ');
        HTML.Add('    font-family: Tahoma, sans-serif;  ');
        HTML.Add('    font-size: x-small ; ');
        HTML.Add('    } ');
        HTML.Add('    #headingtable th { ');
        HTML.Add('    font-style: normal; ');
        HTML.Add('    font-size: smaller; ');
        HTML.Add('    text-align: left; ');
        HTML.Add('    } ');
        HTML.Add('    #headingtable td {  ');
        HTML.Add('    border-bottom: 1px solid blue;  ');
        HTML.Add('    }  ');
        HTML.Add('    </STYLE>  ');
        HTML.Add('</head> ');
        HTML.Add('<body> ');

      while not Messages.EOF do begin
        MessageNumber := Messages.FieldByName('message_number').AsString;
        Subject := Messages.FieldByName('subject').AsString;
        Body := Messages.FieldByName('body').AsString;
        SentDate :=  Messages.FieldByName('show_date').AsDateTime;
        Destination := Messages.FieldByName('destination').AsString;
        HTML.Add('<h1>' + Subject + '</h1>');
        HTML.Add('<table id="headingtable'+MessageNumber+'"> ');
        HTML.Add('<tr><th>To: </th><td>' + Destination + '</td></tr>');
        HTML.Add('<tr><th>Date:</th><td>' + DateToStr(SentDate) + '</td></tr>');
        HTML.Add('<tr><th>Message Number:</th><td>' + MessageNumber + '</td></tr>');
        HTML.Add('</table>');
        HTML.Add(Body);
        HTML.Add('<hr />');
        Messages.Next;
      end;
    end;
    Messages.Close;
    Messages.Filter := '';
    Messages.Filtered := False;
    if not NoMsg then begin
      HTML.Add('<div id="floating-panel"> ');
      HTML.Add('<input onclick="AckMessages();" id="divAck" type=button value="I Have Read Them All"> ');
      HTML.Add('</div> ');
      HTML.Add('<script type="text/javascript"> ');
      HTML.Add('    function AckMessages(){ ');
      HTML.Add('       external.AckMsg("Ack"); ');
      HTML.Add('                          } ');
    end;
    HTML.Add('</script>');
    HTML.Add('</body>');
    HTML.Add('</html>');
    LoadHtml(WebBrowser1, HTML.text);
    FMessagesLoaded := True;  //QM-1008 EB HTML fix
    AckButton.Enabled := true;
//    memo1.Lines := HTML;
  finally
    FreeAndNil(HTML);
  end;
end;

procedure TMessageAckForm.AckButtonClick(Sender: TObject);
begin
  inherited;
  timer1.Enabled := false;   //QMANTWO-492
  FOkToClose := True;
  DM.AckMessages;
  if Assigned(FOnAcked) then
    FOnAcked(Self);
  RefreshNow;
end;

procedure TMessageAckForm.ButtonClick(Sender: TObject);
begin
  inherited;
  FClickedButtonCaption := (Sender as TButton).Caption;
  FOkToClose := True;
  if Assigned(FOnCheckResponse) then
    FOnCheckResponse(Self, FMessageDestID, FBreakRuleID, FClickedButtonCaption, FOkToClose);

  if FOkToClose then begin
    DM.AckMessages(FBreakRuleID);
    if Assigned(FOnAcked) then
      FOnAcked(Self);

    FBreakRuleType := '';
    FBreakRuleID := -1;
    FButtonCaptions := '';
    FMessageDestID := -1;
    Close;
  end;
end;

procedure TMessageAckForm.ActivatingNow;
begin
  inherited;
  FMessagesLoaded := False;  //QM-1008 EB HTML fix
  Timer1.Enabled := true;   //QMANTWO-492
  FOkToClose := False;
  RefreshNow;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeViewMessage, '');
end;

procedure TMessageAckForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  fContainer.Free;
  CanClose := FOkToClose;
end;

procedure TMessageAckForm.FormCreate(Sender: TObject);
begin
  inherited;
  msgAckHandle := self.Handle;
end;

procedure TMessageAckForm.AddFormButtons(const Captions: string);
const
  ButtonWidth = 90;
  ButtonTop = 8;
  ButtonPad = 4;
var
  CaptionList: TStringList;
  I, ButtonNum: Integer;
  Button: TButton;
begin
  // always recreate the FButtons container
  FreeAndNil(FButtons);
  FButtons := TObjectList.Create(True);

//  AckButton.Visible := IsEmpty(Captions);
  FButtons.Clear;
  CaptionList := TStringList.Create;
  try
    CaptionList.CommaText := Captions;
    ButtonNum := 1;
    // adds buttons from right to left
    for I := CaptionList.Count-1 downto 0 do begin
      Button := TButton.Create(nil);  // freed when FButtons is destroyed
      Button.Parent := ButtonPanel;
      Button.Caption := CaptionList.Strings[I];
      Button.Width := ButtonWidth;
      Button.Top := ButtonTop;
      Button.Left := ButtonPanel.ClientWidth - ((Button.Width + ButtonPad) * ButtonNum);
      Button.OnClick := ButtonClick;
      FButtons.Add(Button);
      Inc(ButtonNum);
    end;
  finally
    FreeAndNil(CaptionList);
  end;
end;

procedure TMessageAckForm.ShowBreakRuleMessages(const BreakRuleType: string);
begin
  FBreakRuleType := BreakRuleType;
  FilterMessages;
  Messages.Open;
  if HasRecords(Messages) then begin
    FButtonCaptions := Messages.FieldByName('button_text').AsString;
    FBreakRuleID := Messages.FieldByName('rule_id').AsInteger;
    FMessageDestID := Messages.FieldByName('message_dest_id').AsInteger;
  end;
  RefreshNow;
end;

procedure TMessageAckForm.Timer1Timer(Sender: TObject);
begin
  inherited;
  AckButton.Visible := True;     //QMANTWO-492
  AckButton.Refresh;             //QMANTWO-492
  Timer1.Enabled := False;       //QMANTWO-492
end;

procedure TMessageAckForm.WebBrowser1BeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);   //QM-1008 & QM-1014 EB HTML fix
begin
  inherited;
    if not FMessagesLoaded then exit;
    
    Cancel := True;
    if URL <> '' then begin
      BrowseURLSpecified(URL, 'chrome.exe');  //QM-1008 & QM-1014 EB HTML fix
    end;
end;

procedure TMessageAckForm.ON_WmAckMsg(var Msg: TMessage);
begin
  AckButtonClick(nil);
end;

procedure TMessageAckForm.FilterMessages;
begin
  if NotEmpty(FBreakRuleType) then
    Messages.Filter := 'rule_type=' + QuotedStr(FBreakRuleType)
  else
    Messages.Filter := 'rule_type is null';
  Messages.Filtered := True;
end;

procedure TMessageAckForm.RefreshNow;
begin
  inherited;
  AddFormButtons(FButtonCaptions);
  Messages.Close;
  FilterMessages;
  Messages.Open;
  Populate;
end;

end.
