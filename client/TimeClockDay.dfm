inherited TimeClockDayFrame: TTimeClockDayFrame
  Width = 488
  inherited NothingButton: TButton [1]
  end
  inherited SubmitHint: TStaticText
    Left = 85
    Top = 206
  end
  inherited BelowCalloutPanel: TPanel [3]
    Top = 199
    Width = 131
    object Bevel: TBevel [0]
      Left = 0
      Top = 0
      Width = 22
      Height = 367
      Shape = bsLeftLine
    end
    inherited BelowCalloutTopPanel: TPanel
      Width = 131
      Height = 89
      inherited CalloutHours: TLabel
        Left = 89
        Top = 19
      end
      inherited RegularHours: TLabel
        Left = 85
        Top = 38
      end
      inherited OvertimeHours: TLabel
        Left = 78
        Top = 57
      end
      inherited DoubleTimeHours: TLabel
        Left = 69
        Top = 76
      end
      inherited WorkHours: TLabel
        Left = 97
      end
    end
    inherited LeaveVacPanel: TPanel
      Top = 89
      Width = 131
      inherited VacationHours: TcxDBCurrencyEdit
        Left = 76
        Width = 48
      end
      inherited SickLeaveHours: TcxDBComboBox
        Left = 75
        Width = 49
      end
    end
    inherited PTOPanel: TPanel
      Top = 132
      Width = 131
      inherited PTOHours: TcxDBComboBox
        Left = 75
        Width = 49
      end
    end
    inherited BelowCalloutBottomPanel: TPanel
      Top = 152
      Width = 131
      Height = 311
      inherited DayHours: TLabel
        Left = 95
        Top = 191
      end
      inherited ApprovalMemo: TMemo
        Left = -3
      end
      inherited SubmitButton: TButton
        Left = 54
        Top = 210
      end
      inherited BereavementHours: TcxDBCurrencyEdit
        Left = 76
        Width = 48
      end
      inherited HolidayHours: TcxDBCurrencyEdit
        Left = 76
        Width = 48
      end
      inherited JuryDutyHours: TcxDBCurrencyEdit
        Left = 76
        Width = 48
      end
      inherited MilesStart1: TcxDBCurrencyEdit
        Left = 54
      end
      inherited MilesStop1: TcxDBCurrencyEdit
        Left = 54
      end
      inherited FloatingHoliday: TDBCheckBox
        Left = 76
        Top = 60
        Width = 48
      end
      inherited VehicleUse: TcxDBComboBox
        Left = 54
      end
      inherited EmployeeType: TComboBox
        Left = 54
      end
      inherited ReasonChanged: TcxComboBox
        Left = 54
      end
      inherited PerDiem: TDBCheckBox
        Left = 76
        Top = 78
        Width = 48
      end
    end
  end
  object TimeClockGrid: TcxGrid [4]
    Left = 0
    Top = 0
    Width = 488
    Height = 200
    Align = alTop
    TabOrder = 3
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object TimeClockGridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = TimeClockGridDBTableViewCustomDrawCell
      OnEditing = TimeClockGridDBTableViewEditing
      DataController.DataSource = TimeClockSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'activity_time'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      NewItemRow.InfoText = 'Click here to add activity'
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.NoDataToDisplayInfoText = '<no activity>'
      OptionsView.ScrollBars = ssVertical
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnFace
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderHeight = 28
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Header = TimeClockGridHeaderStyle
      Styles.Indicator = TimeClockGridHeaderStyle
      object ColActivity: TcxGridDBColumn
        Caption = 'Activity'
        DataBinding.FieldName = 'activity_type'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DropDownListStyle = lsEditFixedList
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Width = 79
      end
      object ColActivityTime: TcxGridDBColumn
        Caption = 'Wed 02/28'
        DataBinding.FieldName = 'activity_time'
        PropertiesClassName = 'TcxTimeEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Properties.SpinButtons.Visible = False
        Properties.TimeFormat = tfHourMin
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Width = 48
      end
    end
    object TimeClockGridLevel: TcxGridLevel
      GridView = TimeClockGridDBTableView
    end
  end
  inherited TimesheetSource: TDataSource
    Left = 96
    Top = 64
  end
  inherited TimesheetData: TDBISAMTable
    Left = 96
    Top = 43
  end
  inherited DefaultEditStyleController: TcxDefaultEditStyleController
    Left = 24
    Top = 47
    PixelsPerInch = 96
  end
  object TimeClockSource: TDataSource
    DataSet = ClockDetailData
    OnDataChange = TimesheetSourceDataChange
    Left = 120
    Top = 64
  end
  object ClockDetailData: TDBISAMQuery
    BeforeInsert = ClockDetailDataBeforeInsert
    BeforePost = ClockDetailDataBeforePost
    BeforeDelete = ClockDetailDataBeforeDelete
    OnNewRecord = ClockDetailDataNewRecord
    DatabaseName = 'memory'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select * from timeclock_detail')
    Params = <>
    Left = 120
    Top = 40
  end
  object TimeClockStyleRepo: TcxStyleRepository
    Left = 56
    Top = 48
    PixelsPerInch = 96
    object TimeClockGridHeaderStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindowText
    end
    object TimeClockGridHeaderBoldStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clWindowText
    end
    object TimeClockGridContentStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clWindow
      TextColor = clWindowText
    end
    object TimeClockGridContentROStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clBtnText
    end
  end
end
