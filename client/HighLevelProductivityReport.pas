unit HighLevelProductivityReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, OdRangeSelect;

type
  THighLevelProductivityReportForm = class(TReportBaseForm)
    ActivityDateRange: TOdRangeSelectFrame;
    ActivityDatesLabel: TLabel;
    ProfitCentersLabel: TLabel;
    ProfitCenterList: TCheckListBox;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  OdVclUtils, DMu, OdExceptions, OdMiscUtils;

{$R *.dfm}

{ THighLevelProductivityReportForm }

procedure THighLevelProductivityReportForm.InitReportControls;
begin
  inherited;
  DM.ProfitCenterList(ProfitCenterList.Items);
  SetCheckListBox(ProfitCenterList, True);
  ActivityDateRange.SelectDateRange(rsLastWeek);
end;

procedure THighLevelProductivityReportForm.ValidateParams;
var
  CheckedPCs: string;
begin
  inherited;
  SetReportID('HighLevelProductivity');
  SetParamDate('DateFrom', ActivityDateRange.FromDate, True);
  SetParamDate('DateTo', ActivityDateRange.ToDate, True);
  CheckedPCs := GetCheckedItemString(ProfitCenterList);
  if IsEmpty(CheckedPCs) then
    raise EOdEntryRequired.Create('You must select at least one profit center.');
  if AllItemsAreChecked(ProfitCenterList) then
    CheckedPCs := '';
  SetParam('ProfitCenters', CheckedPCs);
end;

procedure THighLevelProductivityReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(ProfitCenterList, True);
end;

procedure THighLevelProductivityReportForm.SelectNoneButtonClick(Sender: TObject);
begin
  SetCheckListBox(ProfitCenterList, False);
end;

end.
