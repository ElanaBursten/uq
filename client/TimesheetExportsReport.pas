unit TimesheetExportsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TTimesheetExportsReportForm = class(TReportBaseForm)
    WeekEnding: TcxDateEdit;
    FinalOnly: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure WeekEndingChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  OdMiscUtils;

{$R *.dfm}

procedure TTimesheetExportsReportForm.InitReportControls;
begin
  inherited;
  WeekEnding.Date := Trunc(SaturdayIze(Now - 6));
end;

procedure TTimesheetExportsReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimesheetExport');
  SetParamBoolean('FinalOnly', FinalOnly.Checked);
  SetParamDate('WeekEnding', WeekEnding.Date);
end;

procedure TTimesheetExportsReportForm.WeekEndingChange(Sender: TObject);
begin
  inherited;
  WeekEnding.Date := Trunc(SaturdayIze(WeekEnding.Date));
end;

end.
