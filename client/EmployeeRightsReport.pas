unit EmployeeRightsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ComCtrls, OdRangeSelect, CheckLst;

type
  TEmployeeRightsReportForm = class(TReportBaseForm)
    DatesGroupBox: TGroupBox;
    EmployeesGroupBox: TGroupBox;
    RightsGroupBox: TGroupBox;
    ModifiedDateRange: TOdRangeSelectFrame;
    lblManager: TLabel;
    ManagersComboBox: TComboBox;
    AllCheckBox: TCheckBox;
    EmployeeComboBox: TComboBox;
    EmployeeLabel: TLabel;
    EmployeeStatusCombo: TComboBox;
    EmployeeStatusLabel: TLabel;
    RightStatusCombo: TComboBox;
    RightStatusLabel: TLabel;
    Label3: TLabel;
    RightsList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    OrigGrantDatesGroupBox: TGroupBox;
    OrigGrantDateRange: TOdRangeSelectFrame;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure AllCheckBoxClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
  private
    procedure RepopulateEmployeeList;
    function RunForOneEmployee: Boolean;
    procedure InitRightStatusCombo;
    procedure SetupRightsList;
    function SelectedSubsetOfRights: Boolean;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure SetVisibility;
  end;

implementation

uses DMu, OdVclUtils, OdMiscUtils, QMConst, OdExceptions,
LocalEmployeeDmu, LocalPermissionsDmu;

{$R *.dfm}

{ TEmployeeRightsReportForm }

procedure TEmployeeRightsReportForm.InitReportControls;
begin
  inherited;
  AllCheckBox.Checked := True;
  ModifiedDateRange.SelectDateRange(rsAllDates);
  OrigGrantDateRange.SelectDateRange(rsAllDates);
  SetUpManagerList(ManagersComboBox);
  if ManagersComboBox.Items.Count > 1 then
    ManagersComboBox.ItemIndex := 0;
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  InitRightStatusCombo;
  SetupRightsList;
  SetVisibility;
end;

procedure TEmployeeRightsReportForm.InitRightStatusCombo;
begin
  RightStatusCombo.Items.Clear;
  RightStatusCombo.AddItem('Granted', Pointer(EmpRightGranted));
  RightStatusCombo.AddItem('Denied', Pointer(EmpRightDenied));
  RightStatusCombo.AddItem('Both', Pointer(EmpRightAll));
  RightStatusCombo.ItemIndex := 0;
end;

procedure TEmployeeRightsReportForm.SetupRightsList;
begin
  PermissionsDM.EmployeeRightsList(RightsList.Items);
  RightsList.Sorted := True;
  SetCheckListBox(RightsList, True);
end;

procedure TEmployeeRightsReportForm.SetVisibility;
begin
  EmployeeStatusCombo.Enabled := not RunForOneEmployee;
  EmployeeComboBox.Enabled := RunForOneEmployee;
end;

procedure TEmployeeRightsReportForm.ValidateParams;
var
  Rights: string;
begin
  inherited;
  SetReportID('EmployeeRights');
  SetParamDate('StartDate', ModifiedDateRange.FromDate);
  SetParamDate('EndDate', ModifiedDateRange.ToDate);
  SetParamDate('OrigGrantStartDate', OrigGrantDateRange.FromDate);
  SetParamDate('OrigGrantEndDate', OrigGrantDateRange.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamIntCombo('EmployeeStatus', EmployeeStatusCombo, True);
  if RunForOneEmployee then
    SetParamInt('EmployeeID', GetComboObjectInteger(EmployeeComboBox))
  else
    SetParamInt('EmployeeID', -1);

  SetParamIntCombo('RightStatus', RightStatusCombo, True);

  Rights := GetCheckedItemString(RightsList, True);
  if IsEmpty(Rights) then begin
    RightsList.SetFocus;
    raise EOdEntryRequired.Create('Please select the rights to report on.');
  end;

  if SelectedSubsetOfRights then
    SetParam('RightIDList', Rights)
  else
    SetParam('RightIDList', '');
end;

function TEmployeeRightsReportForm.RunForOneEmployee: Boolean;
begin
  Result := not AllCheckBox.Checked;
end;

procedure TEmployeeRightsReportForm.RepopulateEmployeeList;
begin
  if RunForOneEmployee then begin
    if ManagersComboBox.ItemIndex > -1 then begin
      EmployeeDM.EmployeeList(EmployeeComboBox.Items, GetComboObjectInteger(ManagersComboBox))
    end;
  end else
    EmployeeComboBox.Items.Clear;
end;

procedure TEmployeeRightsReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  RepopulateEmployeeList;
end;

procedure TEmployeeRightsReportForm.AllCheckBoxClick(Sender: TObject);
begin
  SetVisibility;
  RepopulateEmployeeList;
end;

procedure TEmployeeRightsReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(RightsList, True);
end;

procedure TEmployeeRightsReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(RightsList, False);
end;

function TEmployeeRightsReportForm.SelectedSubsetOfRights: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to RightsList.Count - 1 do begin
    Result := not RightsList.Checked[i];
    if Result then
      Break;
  end;
end;

end.
