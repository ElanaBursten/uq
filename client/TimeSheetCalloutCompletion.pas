unit TimeSheetCalloutCompletion;

interface                                                  

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, StdCtrls, cxTextEdit, cxMaskEdit, cxSpinEdit, cxTimeEdit,
  DateUtils, OdMiscUtils;

type
  TTSUpdates = Record
    IsUpdated: Boolean;
    COWorkDate: TDatetime;      {Date only - Work Date to finish}
    MissingCOName: string;      {FieldName that is missing - to update}
    CalcNextDay: TDatetime;     {Calculated from the last callout_start datetime}
    FormEntryMax: TDatetime;    {Time}
    FormEntryMin: TDateTime;    {Time}
    Mode: integer;              {Before Midnight (0) or Finishing Today (1)}
    OutofRange: Boolean;
    TimeStart: TDateTime;       {The start time of the abandoned callout (call-in)}
    TimeEnd: TDateTime;         {Callout Stop Time Entered}
    AddEntryCallout: TDateTime; {Additional Entry callout_stop for the prev day}
    AddEntryCallin: TDateTime;  {Additional Entry callout_start for the next day}
    EntryID: integer;           {Save the Entry ID for the update}

    {Calculated after entry has been made}
    CalcYesterdayCOTotal: Double;   {Yesterdays Total Call Out hours}

  End;

  TEntryRec = Record
    FoundProblem: Boolean;
    EntryID: integer;
    LastCallIn: TDateTime;      {Call-in Time}
    COWorkDate: TDateTime;      {Work Date checked}
    Msg: string;
    COFieldName: string;

    CoTotal: Double;
  End;


  TCalloutCompletionDlg = class(TForm)
    lblTop: TLabel;
    RBtnNextMorning: TRadioButton;
    RBtnBeforeMidnight: TRadioButton;
    CalloutTimeEdit: TcxTimeEdit;
    lblDATE: TLabel;
    btnSave: TButton;
    btnCancel: TButton;
    lblAdditionalCallout: TLabel;
    lblAdditionalCallIn: TLabel;
    lblAdditionalEntries: TLabel;
    lblFinishedAt: TLabel;
    procedure RBtnModeClick(Sender: TObject);
    procedure CalloutTimeEditPropertiesEditValueChanged(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    fEmpID: integer;
    fEmpShortName: string;
    fLastCallinEntry: TDatetime;
    fTimeEditCanTrigger: Boolean;
    procedure Init(pEmpID: Integer; pMissingEntry: TEntryRec);
    procedure ChangeMode;
    procedure AdjustDisplay;
    procedure SaveChanges;

  public
    fUpdates: TTSUpdates;
  end;

function GetHoursDifference(Start, Stop: Variant): Double;
function ExecuteCallOutCompletionDlg(pEmpID: integer; pMissingEntry: TEntryRec): TTSUpdates;
function GetAbandonedCallOutEntries(pEmpID: integer; pWorkDate: TDateTime): TEntryRec;


const
  TOPStr =               'You have an incomplete call-out on %s at %s';
  BeforeMidnightStr =    'Finished before midnight on %s';
  NextMorningStr =       'Finished the next morning on %s';
  AdditionalEntriesStr = 'These entries will also be added to the timesheet: ';
  BadBeforeMidStr =      '* Entered time must be between %s and 23:59';
  BadNextMornStr =       '* Entered time must be before %s';

  modeBeforeMidnight =    0;
  modeNextMorning =       1;

  STARTDAYTIME =         '00:02';
  ENDDAYTIME =           '23:59';
  LDATEFORMAT =          'DDD MM/DD/YYYY';
  DBDT =                 'YYYY-MM-DD HH:MM';
  DBT =                  'hh:mm';  //' hh:mm:ss'

  {Screen Sizing}
  UnderEntry =           114;
  Expanded =             150;
  ExpHeight =            218;
  ShortHeight =          183;
var
  CalloutCompletionDlg: TCalloutCompletionDlg;

implementation
uses
  DMu, LocalEmployeeDMu, DB;

{$R *.dfm}

{Freeform functions}
function GetHoursDifference(Start, Stop: Variant): Double;
begin
  if VarIsNull(Start) or VarIsNull(Stop) or (Start > Stop) then
    Result := 0
  else
    Result := FloatRound(SecondsBetween(Stop, Start) / (60 * 60), 2);
end;

function ExecuteCallOutCompletionDlg(pEmpID: integer; pMissingEntry: TEntryRec): TTSUpdates;
begin
  CalloutCompletionDlg := TCalloutCompletionDlg.Create(nil);
  try
    CalloutCompletionDlg.Init(pEmpID, pMissingEntry);
    if CalloutCompletionDlg.ShowModal = mrOK then
      CalloutCompletionDlg.fUpdates.IsUpdated := True
    else
      CalloutCompletionDlg.fUpdates.IsUpdated := False;

    Result := CalloutCompletionDlg.fUpdates;
  finally
    CalloutCompletionDlg.Free;
  end;
end;

function GetAbandonedCallOutEntries(pEmpID: integer; pWorkDate: TDateTime): TEntryRec;
var
  i: integer;
  Start, Stop: TDatetime; {the times}
  FullStartDT, FullStopDT: TDateTime;
  StartStr, StopStr: string;

const
  Max = 16;  {Number of callouts}
begin
  Result.COTotal := 0;
  Result.EntryID := 0;
  Result.Msg := '';
  Result.COFieldName := '';
  Result.FoundProblem := False;
  Result.COWorkDate := pWorkDate;

  if EmployeeDM.HasLocalTimeSheetEntriesToday(pEmpID) then begin
    Result.FoundProblem := False; {Too late with today's entries}
    Exit;
  end;

  try
    DM.qryAbandonedCallOut.ParamByName('yesterday').AsDate := pWorkDate;
    DM.qryAbandonedCallOut.ParamByName('EmpID').AsInteger := pEmpID;
    DM.qryAbandonedCallOut.Open;

    {This is only for 1 day, so just 1 record}
    if DM.qryAbandonedCallOut.EOF then
      Result.Msg := 'No record found for ' + DateToStr(pWorkDate)
    else begin

      for i := 1 to Max do begin
        StartStr :=  'Callout_start' + intToStr(i);
        StopStr :=   'Callout_stop' + intToStr(i);

        Start := DM.qryAbandonedCallOut.FieldByName(StartStr).AsDatetime;
        FullStartDT := pWorkDate + Start;
        Stop :=  DM.qryAbandonedCallOut.FieldByName(StopStr).AsDateTime;
        FullStopDT := pWorkDate + Stop;


        if (FullStartDT > pWorkDate) then
          if (DM.qryAbandonedCallOut.FieldByName(StopStr).IsNull) then begin
             Result.LastCallIn := Start;
             Result.COFieldName := StopStr;
             Result.Msg := StopStr + 'is missing';
             Result.EntryID := DM.qryAbandonedCallOut.FieldByName('entry_id').AsInteger;
             Result.FoundProblem := True;
             BREAK;
          end
          else if (DM.qryAbandonedCallOut.FieldByName(StartStr).IsNull) then BREAK
          else
            Result.COTotal := Result.COTotal + GetHoursDifference(Start, Stop);
      end;
    end;

  finally
   if DM.qryAbandonedCallOut.Active then
     DM.qryAbandonedCallOut.Close;


    {Swallow anything else}
  end;
end;


{ TCalloutCompletionDlg }
procedure TCalloutCompletionDlg.AdjustDisplay;
var
  AddCOInStr, AddCOOutStr: string;
begin
 {Bad Entries}
 if fUpdates.OutOfRange then begin
   CallOutTimeEdit.Style.Font.Color := clRed;

   if (fUpdates.Mode = modeNextMorning) then begin
     lblAdditionalEntries.Caption :=
            Format(BadNextMornStr, [formatDatetime(DBT, fUpdates.FormEntryMax)]) ;
   end
   else if (fUpdates.Mode = modeBeforeMidnight) then
     lblAdditionalEntries.Caption :=
            Format(BadBeforeMidStr, [formatDateTime(DBT, fLastCallInEntry)]);

   lblAdditionalEntries.Font.Color := clRed;
   lblAdditionalEntries.Visible := True; {Show Error}
   lblAdditionalCallout.Visible := False;
   lblAdditionalCallin.Visible := False;
 end

 {Good Entries}
 else begin
   CallOutTimeEdit.Style.Font.Color := clBlack;
   lblAdditionalEntries.Font.Color := clNavy;

   if (fUpdates.Mode = modeNextMorning) then begin
     lblAdditionalEntries.Caption := AdditionalEntriesStr;
     AddCOInStr := formatDatetime(DBT, fUpdates.AddEntryCallin);
     AddCOOutStr := formatDatetime(DBT, fUpdates.AddEntryCallout);
     lblAdditionalCallout.Caption := 'call-out: ' + AddCoOutStr + ' for ' + formatdatetime(LDATEFORMAT, fUpdates.COWorkDate);
     lblAdditionalCallin.Caption :=  'call-in: ' + AddCoInStr + ' for ' + formatdatetime(LDATEFORMAT, fUpdates.CalcNextDay);
     lblAdditionalCallin.Visible := True;
     lblAdditionalCallout.Visible := True;
     lblAdditionalEntries.Visible := True;
   end
   else if (fUpdates.Mode = modeBeforeMidnight) then begin
     lblAdditionalEntries.Caption :=
            Format(BadBeforeMidStr, [TimeToStr(fLastCallInEntry)]);
     lblAdditionalCallin.Visible := False;
     lblAdditionalCallout.Visible := False;
     lblAdditionalEntries.Visible := False;
   end;
 end;

end;

procedure TCalloutCompletionDlg.btnSaveClick(Sender: TObject);
begin
  btnSave.Enabled := False;
  SaveChanges;
end;

procedure TCalloutCompletionDlg.Button1Click(Sender: TObject);
var
  EntryInfo: TEntryRec;
begin
  EntryInfo := GetAbandonedCalloutEntries(DM.EmpID, Yesterday);
end;

procedure TCalloutCompletionDlg.CalloutTimeEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  if fTimeEditCanTrigger then begin

   {Mode: Next Morning}
   if (fUpdates.Mode = modeNextMorning) then begin

     {Next Morning: Out of range}
     if (CallOutTimeEdit.Time > fUpdates.FormEntryMax) or
        (CallOutTimeEdit.Time < fUpdates.FormEntryMin) then begin
       fUpdates.OutofRange := True;
       BtnSave.Top := UnderEntry;
       BtnCancel.Top := UnderEntry;
       Self.Height :=  ShortHeight;
     end
     {Next Morning: In range}
     else begin
       fUpdates.OutofRange := False;
       BtnSave.Top := Expanded;
       BtnCancel.Top := Expanded;
       Self.Height := ExpHeight;
     end;
     fUpdates.TimeEnd := CallOutTimeEdit.Time;
   end

   {Mode: Before Midnight}
   else if (fUpdates.Mode = modeBeforeMidnight) then begin
     BtnSave.Top := UnderEntry;
     BtnCancel.Top := UnderEntry;
     Self.Height :=  ShortHeight;
     {Before Midnight Out of Range}
     if (CallOutTimeEdit.Time > fUpdates.FormEntryMax) or
        (CallOutTimeEdit.Time < fUpdates.FormEntryMin) then begin
       fUpdates.OutofRange := True;
     end
     else
       fUpdates.OutofRange := False;

     fUpdates.TimeEnd := CallOutTimeEdit.Time;
   end;
   AdjustDisplay;
  end;
end;

procedure TCalloutCompletionDlg.ChangeMode;
var
  Showit: Boolean;
  CaptureTime: TDatetime;
begin
  ShowIt := False;

  {Reset Label Text}
  lblAdditionalEntries.Caption := '';
  lblAdditionalCallout.Caption := '';
  lblAdditionalCallIn.Caption := '';

  {Finished Next Morning}
  if (fUpdates.Mode = modeNextMorning) then begin
    fTimeEditCanTrigger := False;
    CaptureTime := TimeOf(Now);
    fUpdates.FormEntryMin := StrToTime(STARTDAYTIME);
    fUpdates.FormEntryMax :=  IncMinute(CaptureTime, 3);
    CallOutTimeEdit.Time  := CaptureTime;
    lblDate.Caption := formatdatetime(LDATEFORMAT, fUpdates.CalcNextDay);
    btnSave.Top := Expanded;
    btnCancel.Top := Expanded;
    Self.Height := ExpHeight;
    ShowIt := True;
  end
  {Finished Before Midnight}
  else begin
    fUpdates.FormEntryMin := TimeOf(IncMinute(fLastCallinEntry, 1));
    fUpdates.FormEntryMax :=  StrToTime(ENDDAYTIME);
    CallOutTimeEdit.Time := fUpdates.FormEntryMax;
    lblDate.Caption := formatdatetime(LDATEFORMAT, fUpdates.COWorkDate);
    btnSave.Top := UnderEntry;
    btnCancel.Top := UnderEntry;
    Self.Height := ShortHeight;
  end;

  {Make sure to enable fields now}
  lblFinishedAt.Enabled := True;
  lblDate.Enabled := True;
  CalloutTimeEdit.Enabled := True;
  fTimeEditCanTrigger := True;

  CalloutTimeEdit.Style.Font.Color := clBlack;

  AdjustDisplay;
end;

//procedure TCalloutCompletionDlg.Init(pEmpID: integer; pLastCallInEntry: TDateTime; pMissingCOStr: string);
procedure TCalloutCompletionDlg.Init(pEmpID: Integer; pMissingEntry: TEntryRec);
var
  DateStr, TimeStr: string;
begin
  fEmpID := pEmpID;
  fEmpShortName := EmployeeDM.GetEmployeeShortName(fEmpID);
  fTimeEditCanTrigger := False;
  fUpdates.COWorkDate := pMissingEntry.COWorkDate;
  fUpdates.IsUpdated := False;
  fUpdates.FormEntryMin := Now;
  fUpdates.FormEntryMax := IncMinute(fUpdates.FormEntryMin, 5);
  fUpdates.OutofRange := False;
  fUpdates.MissingCOName := pMissingEntry.COFieldName;
  fUpdates.EntryID := pMissingEntry.EntryID;
  fUpdates.CalcYesterdayCOTotal := pMissingEntry.CoTotal;

  {Set the incomplete call-in datetime (or callout_start)}
  fLastCallinEntry := pMissingEntry.LastCallIn;    //pLastCallinEntry;
  fUpdates.CalcNextDay := IncDay(pMissingEntry.COWorkDate + (fLastCallInEntry));

  DateStr := formatdatetime(LDATEFORMAT, pMissingEntry.COWorkDate);
  TimeStr := formatdatetime(DBT, fLastCallInEntry);//TimeToStr(fLastCallInEntry);
  lblTop.Caption := format(TopStr, [DateStr, TimeStr]);

  RBtnBeforeMidnight.Caption:= format(BeforeMidnightStr, [DateStr]);

  DateStr := formatdatetime(LDATEFORMAT, fUpdates.CalcNextDay);
  RBtnNextMorning.Caption := format(NextMorningStr, [DateStr]);
  fTimeEditCanTrigger := True;

  {Go ahead and calculate the added entries needed if Finishing Next Morning - these stay the same}
  fUpdates.AddEntryCallout := StrToTime(ENDDAYTIME);
  fUpdates.AddEntryCallin  := StrToTime(STARTDAYTIME);


  {Default starting text if the mode is modeBeforeMidnight}
  lblAdditionalEntries.Caption := ' ';
  lblAdditionalCallout.Caption := 'call-out: ';
  lblAdditionalCallin.Caption := 'call-in: ';

  DateStr := formatdatetime(LDATEFORMAT, pMissingEntry.COWorkDate);
  Self.Caption := ' Call-out Completion: '  + fEmpShortName + ' ' + DateStr;

  lblDate.Caption := formatdatetime(LDATEFORMAT, fUpdates.COWorkDate);

  RBtnBeforeMidnight.Checked := True;
end;



procedure TCalloutCompletionDlg.RBtnModeClick(Sender: TObject);
begin
  if rBtnBeforeMidnight.checked then
    fUpdates.Mode := modeBeforeMidnight
  else if rBtnNextMorning.checked then
    fUpdates.Mode := modeNextMorning;
  ChangeMode;
end;

procedure TCalloutCompletionDlg.SaveChanges;
const
  SQLUpdate = 'update timesheet_entry set %S = %S, callout_hours = %S,' +
               ' DeltaStatus=''U'''+
               ' where entry_id = %S';
var
  SQL: string;
  TimeStr: string;
  EntryStr: string;
  COTotalStr: string;
  NewCOCalculation: double;
  Yesterday1159: TDatetime;
begin
  Yesterday1159 := StrToDateTime(ENDDAYTIME);
  EntryStr := IntToStr(fUpdates.EntryID);

 // ShowMessage(DateTimeToStr(Yesterday1159)+ ' ' + DateTimeToStr(fLastCallinEntry) + ' ' + FloatToStr(NewCOCalculation));
  if fUpdates.Mode = modeBeforeMidnight then begin
    NewCOCalculation := GetHoursDifference(fLastCallinEntry, fUpdates.TimeEnd);
    fUpdates.CalcYesterdayCOTotal := fUpdates.CalcYesterdayCOTotal + NewCOCalculation;
    COTotalStr := FloatToStr(fUpdates.CalcYesterdayCOTotal);
    TimeStr := '''' + formatdatetime(DBDT, fUpdates.TimeEnd) + '''';
    SQL := format(SQLUpdate, [fUpdates.MissingCOName, TimeStr, COTotalStr, EntryStr]);
    DM.Engine.ExecQuery(SQL);
  end
  else begin
   {Add callout_stop right before midnight to close out previous day}
   NewCOCalculation := GetHoursDifference(fLastCallinEntry, Yesterday1159);
   fUpdates.CalcYesterdayCOTotal := fUpdates.CalcYesterdayCOTotal + NewCOCalculation;
   COTotalStr := FloatToStr(fUpdates.CalcYesterdayCOTotal);
   TimeStr := '''' + formatdatetime(DBDT, fUpdates.AddEntryCallout) + '''';
   SQL := format(SQLUpdate, [fUpdates.MissingCOName, TimeStr, COTotalStr, EntryStr]);
   DM.Engine.ExecQuery(SQL);
  end;
  DM.SendTimeChangesToServer;

end;

end.
