object PhoneVerifyDlg: TPhoneVerifyDlg
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Phone number verification dialog'
  ClientHeight = 94
  ClientWidth = 397
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 281
    Height = 73
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 16
    Top = 21
    Width = 90
    Height = 13
    Caption = 'Present Phone No:'
  end
  object Label2: TLabel
    Left = 32
    Top = 44
    Width = 74
    Height = 13
    Caption = 'New Phone No:'
  end
  object OKBtn: TButton
    Left = 300
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Save'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 300
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object edtNewNumber: TMaskEdit
    Left = 107
    Top = 40
    Width = 119
    Height = 21
    EditMask = '!\(999\)000-0000;1;_'
    MaxLength = 13
    TabOrder = 3
    Text = '(   )   -    '
    OnChange = edtNewNumberChange
  end
  object edtPresentPhoneNo: TEdit
    Left = 107
    Top = 17
    Width = 119
    Height = 21
    MaxLength = 13
    ReadOnly = True
    TabOrder = 2
  end
end
