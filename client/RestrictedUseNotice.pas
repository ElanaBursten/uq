unit RestrictedUseNotice;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, StdCtrls, ExtCtrls, Forms,
  OdEmbeddable;

type
  TRestrictedUseForm = class(TEmbeddableForm)
    MainPanel: TPanel;
    NoticeMemo: TMemo;
  public
    procedure LoadAndDisplayMessage(const MessageText: string);
  end;

implementation

{$R *.dfm}

{ TRestrictedUseForm }

procedure TRestrictedUseForm.LoadAndDisplayMessage(const MessageText: string);
begin
  NoticeMemo.Text := MessageText;
end;

end.
