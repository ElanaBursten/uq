unit AuditReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, ExtCtrls, StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, ComCtrls, dxCore, cxDateUtils;

type
  TAuditReportForm = class(TReportBaseForm)
    Label1: TLabel;
    Label2: TLabel;
    DateEdit: TcxDateEdit;
    CallCenterComboBox: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

{$R *.dfm}

uses
  DMu;

{ TAuditReportForm }

procedure TAuditReportForm.ValidateParams;
begin
  inherited;
  SetReportID('Audit');
  SetParamDate('date', DateEdit.Date);
  SetParam('call_center', FCallCenterList.GetCode(CallCenterComboBox.Text));
end;

procedure TAuditReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  DateEdit.Date := Date - 1;
  CallCenterComboBox.ItemIndex := 0;
end;

end.

