inherited DlgSimpleLink: TDlgSimpleLink
  Caption = 'Simple External Link'
  ClientHeight = 110
  ClientWidth = 295
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Bevel1: TBevel
    Width = 281
    Height = 65
  end
  object lblLink: TLabel [1]
    Left = 16
    Top = 16
    Width = 65
    Height = 13
    Caption = 'External Link:'
  end
  object Label1: TLabel [2]
    Left = 135
    Top = 16
    Width = 143
    Height = 13
    Caption = 'ex: http://www.utliquest.com'
  end
  inherited OKBtn: TButton
    Left = 78
    Top = 79
    OnClick = OKBtnClick
  end
  inherited CancelBtn: TButton
    Top = 79
  end
  object EdtLink: TcxTextEdit
    Left = 16
    Top = 31
    Style.Edges = []
    TabOrder = 2
    Width = 262
  end
end
