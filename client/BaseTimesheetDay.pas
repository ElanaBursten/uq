unit BaseTimesheetDay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, DB, DBISAMTb, ComCtrls, QMConst, ActnList, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxClasses;

type
  TTimeSheetPermissions = record   //QM-517 Sick Leave
  private
    {Entry Permissions}
    FShowMilesEntry: Boolean;
    FAllowPovCovEntry: Boolean;
    fVehicleUsageRequired: Boolean;
    FAllowEmpTypeEntry: Boolean;
    FAllowSickLeaveEntry: Boolean; //QM-517 Sick Leave
    fPerDiemEntry: Boolean;
    fPriorDayEntry: Boolean;
  end;

  {Contains the accrued balance including values inserted up until today or the day selected}
  TAccruedBal = record //QM-579 Need to pass some info to individual day frame
    PTO:  double;
    Sick: double;
    NoRecordFound: Boolean;
  end;

  TFinishCallOut = record   //QM-597 EB Abandoned Callout
    HasMoreDates: Boolean;       {Only if we need to add for today}
    PreviousEntriesMade: Boolean;{Lets us know if entries were made night before}
    FinishWorkDate: TDateTime;   {Date To finish callout}
    AddCallOutStart: TDatetime;  {Start time}
    AddCallOutStop: TDateTime;   {Assigned Stop time}
    SavedandDone: Boolean;       {We're done with it}
  end;

  TBaseTimesheetDayFrame = class(TFrame)
    EditedFlagLabel: TLabel;
    BelowCalloutPanel: TPanel;
    NothingAction: TAction;
    SubmitHint: TStaticText;
    TimesheetSource: TDataSource;
    TimesheetData: TDBISAMTable;
    DayActions: TActionList;
    SubmitAction: TAction;
    NothingButton: TButton;
    DefaultEditStyleController: TcxDefaultEditStyleController;
    BelowCalloutTopPanel: TPanel;
    CalloutHours: TLabel;
    RegularHours: TLabel;
    OvertimeHours: TLabel;
    DoubleTimeHours: TLabel;
    WorkHours: TLabel;
    LeaveVacPanel: TPanel;
    VacationHours: TcxDBCurrencyEdit;
    PTOPanel: TPanel;
    PTOHours: TcxDBComboBox;
    BelowCalloutBottomPanel: TPanel;
    DayHours: TLabel;
    ApprovalMemo: TMemo;
    SubmitButton: TButton;
    BereavementHours: TcxDBCurrencyEdit;
    HolidayHours: TcxDBCurrencyEdit;
    JuryDutyHours: TcxDBCurrencyEdit;
    MilesStart1: TcxDBCurrencyEdit;
    MilesStop1: TcxDBCurrencyEdit;
    FloatingHoliday: TDBCheckBox;
    VehicleUse: TcxDBComboBox;
    EmployeeType: TComboBox;
    ReasonChanged: TcxComboBox;
    PerDiem: TDBCheckBox;
    SickLeaveHours: TcxDBComboBox;
    lblDevOverride: TLabel;
    procedure TimesheetSourceDataChange(Sender: TObject; Field: TField);
    procedure MilesStart1Exit(Sender: TObject);
    procedure TimeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SubmitActionExecute(Sender: TObject); virtual;
    procedure TimesheetDataAfterInsert(DataSet: TDataSet);
    procedure FloatingHolidayClick(Sender: TObject);
    procedure EmployeeTypeSelect(Sender: TObject);
    procedure DayActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TimesheetDataBeforePost(DataSet: TDataSet);
    procedure ReasonChangedPropertiesChange(Sender: TObject);
    procedure PerDiemClick(Sender: TObject);
  private
    fPermissions: TTimeSheetPermissions;
    FReadOnly: Boolean;
    FDayHours: Double;
    FOnDataChange: TNotifyEvent;
    FIgnoreRecalc: Boolean;
    FViewTotalsMode: Boolean;
    FNeedToShutdown: Boolean;
//    fPTOOverLimit: Boolean;
//    fSLOverLimit: Boolean;

    procedure ShowErrors;
    procedure SetReadOnly(const Value: Boolean);
    procedure CheckForErrors;
    procedure AfterDisplay;
    procedure UpdateApprovalData;
    procedure SetShowApprovalData(const Value: Boolean);
    procedure SaveFieldDouble(FieldName: string; NewValue: Double);
    procedure SaveAllDays;
    procedure UpdateEditFlagLabel;
    function PromptUserForContactPhone: string;
    procedure InitPermissions;  //QM-517 Sick Leave Refactoring
  protected
    Timesheet: TDataSet;
    FWorkDate: TDateTime;
    FDuringSubmit: Boolean;
    FWorkHours: Double;
    FCalloutHours: Double;
    FFloatHolHours: Double;
    FHaveCalculatedChanges: Boolean;
    FDoubleTimePossible: Boolean;
    FEmpID: Integer;
    FMode: TTimesheetEditMode;
    FOnSaveDays: TNotifyEvent;
    FShowApprovalData: Boolean;
    FExpanded: Boolean;
    FFloatingHolidaysAvailable: Boolean;
    FWorkStartedToday : Boolean;  //QM-100 EB  - Only applicable if today
    function IsSubmitted: Boolean;
    function IsFinalApproved: Boolean;

    function DisplayingYesterday: Boolean;
    procedure RecalculateTotals;
    procedure SetDayLabel; virtual; abstract;
    procedure SetExpanded(const Value: Boolean); virtual;
    procedure NotifyDataChange;
    function IsPriorDayActive: Boolean;
    function GetCurrentTimesheetEntryID(EmpID: Integer): Integer;
    procedure DetermineReadOnly(ForceReadOnly: Boolean = False); virtual;
    procedure RefreshScreenControls; virtual;
    procedure BeforeErrorCheck; virtual;
    function IsSubmitHintShowing: Boolean;
    procedure ShowSubmitHint(Show: Boolean);
    function ConfirmContinueWithSubmit: Boolean; virtual;
    procedure SubmitTime;
    procedure DisplaySubmitMessage;
    procedure DisplayFloatingHolidayCaption; virtual;
    procedure DisplayPerDiemCaption; virtual;
  public
    AccruedBal: TAccruedBal; //QM-579 EB
    FinishCallOut: TFinishCallOut; //QM-597 EB Abandoned Callout
    function DisplayingToday: Boolean;
    procedure Display(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode); virtual;
    procedure DisplayDataSet(DataSet: TDataSet); virtual;
    procedure SaveChanges; virtual;
    procedure CancelChanges; virtual;
    function Modified: Boolean; virtual;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    property ShowApprovalData: Boolean read FShowApprovalData write SetShowApprovalData;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
    function WorkingHours: Double;
    function DayCalloutHours: Double;
    procedure SetHoursBreakdown(Regular, Overtime, DoubleTime, Callout: Double);
    property OnDataChange: TNotifyEvent read FOnDataChange write FOnDataChange;
    property OnSaveDays: TNotifyEvent read FOnSaveDays write FOnSaveDays;
    procedure SetDoubleTimePossible(Possible: Boolean);
    procedure CloseData;
    procedure ShowHideVehicleFields;
    property WorkDate: TDateTime read FWorkDate;
    property DuringSubmit: Boolean read FDuringSubmit;
    procedure Redisplay;
    procedure FocusIfPossible; virtual; abstract;
    property Expanded: Boolean read FExpanded write SetExpanded;
    property NeedToShutdown: Boolean read FNeedToShutdown write FNeedToShutdown;
    procedure SetAccruedValsFromTimeSheet(AccruedSick: double; AccruedPTO: double; NoBalanceRecord: Boolean);  //QM-579 EB
    procedure DisplayPTO(Editable: boolean); //QM-579 Sick Leave and PTO Balance - modified
    procedure DisplaySl(Editable: boolean);  //QM-579 Sick Leave and PTO Balance -new
    function FloatingHolidayEnabled: Boolean; virtual;
    property FloatingHolidaysAvailable: Boolean read FFloatingHolidaysAvailable write FFloatingHolidaysAvailable;
    procedure CleanupFutureCheckboxes;  //EB QMANTWO-733
    property WorkStartedToday: Boolean read fWorkStartedToday write fWorkStartedToday;
    property Permissions: TTimeSheetPermissions read fPermissions write fPermissions; //QM-517 Sick Leave Refactoring
  end;

const
  ShowVehicleTypes: array[0..3] of string = ('NONE', 'COV1W', 'COVRT', 'POV');
  DefaultDayHeight = 600;

  TimeFields: array [0..29] of string = ('work_start1', 'work_stop1', 'work_start2', 'work_stop2',
    'work_start3', 'work_stop4', 'work_start5', 'work_stop5', 'callout_start1', 'callout_stop1',
    'callout_start2', 'callout_stop2', 'callout_start3', 'callout_stop3', 'callout_start4',
    'callout_stop4', 'callout_start5', 'callout_stop5', 'callout_start6', 'callout_stop6',
    'leave_hours', 'vac_hours', 'br_hours', 'hol_hours', 'jury_hours', 'miles_start1', 'miles_stop1',
    'floating_holiday', 'pto_hours', 'per_diem');           //QM-1061 Timesheet fix to gap workstarts EB

implementation

uses DMu, DateUtils, OdDbUtils, OdMiscUtils, OdVclUtils, OdDBISAMUtils,
  OdExceptions, Variants, HoursDataset, Timesheet, BreakRules, OdAckMessageDlg,
  OdIsoDates, OdAckMessagePromptDlg, OdCxUtils, LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

{ TBaseTimesheetDayFrame }

constructor TBaseTimesheetDayFrame.Create(Owner: TComponent);
var
  i: Integer;
begin
  inherited;
  NeedToShutdown := False;
  VehicleUse.Properties.Items.Clear;
  for i := Low(ShowVehicleTypes) to High(ShowVehicleTypes) do
    VehicleUse.Properties.Items.Add(ShowVehicleTypes[i]);
  Timesheet := TimesheetData;
  TimesheetData.BeforePost := TimesheetDataBeforePost;
  DM.Engine.LinkEventsAutoInc(Timesheet);
  // TimesheetBeforePost calls back into the engine
  Timesheet.BeforePost := TimesheetDataBeforePost;

  ApprovalMemo.Lines.Clear;
  Height := DefaultDayHeight;

  EmployeeDM.EmpTypeList(EmployeeType.Items);
  DM.TimesheetChangeReasonList(ReasonChanged.Properties.Items);
  DM.GetRefLookupList('ptohrs', PTOHours.Properties.Items, False);   //QM-933 EB SortOrder
  DM.GetRefLookupList('sickhrs', SickLeaveHours.Properties.Items, False);  //QM-517 Sick Leave
  FWorkStartedToday := False; //QM-100
end;

destructor TBaseTimesheetDayFrame.Destroy;
begin
  inherited;
end;

procedure TBaseTimesheetDayFrame.CancelChanges;
begin
  if Timesheet.State = dsInsert then begin
    Timesheet.Cancel;
    Display(FEmpID, FWorkDate, FMode);
  end
  else
    Timesheet.Cancel;
end;

procedure TBaseTimesheetDayFrame.Display(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode);
var
  EntryID: Integer;
begin
  Timesheet.DisableControls;
  try

    TimesheetSource.DataSet := Timesheet;
    FWorkDate := WorkDate;
    FEmpID := EmpID;
    FMode := Mode;
    FViewTotalsMode := False;
    InitPermissions;  //QM-517 Sick Leave EB
    DetermineReadOnly;
    ShowSubmitHint(ReadOnly and (DisplayingToday and IsPriorDayActive) and (Mode = teNormal));

    Timesheet.Cancel;
    Timesheet.Close;
    Timesheet.Open;

    EntryID := GetCurrentTimesheetEntryID(EmpID);

    if (EntryID = -1) and ReadOnly then begin
      Timesheet.Close; // No need to insert/sync a blank record
      RecalculateTotals;
    end
    else if (EntryID = -1) then begin
      Timesheet.Append;
      if not Permissions.fVehicleUsageRequired then
        Timesheet.FieldByName('vehicle_use').AsString := 'NONE';
        
      Timesheet.FieldByName('work_emp_id').AsInteger := EmpID;
      Timesheet.FieldByName('work_date').AsDateTime := DateOf(WorkDate);
      Timesheet.FieldByName('status').AsString := TimesheetStatusActive;
      Timesheet.FieldByName('entry_by').AsInteger := DM.EmpID;
      Timesheet.FieldByName('emp_type_id').AsInteger := EmployeeDM.GetEmployeeTypeID(EmpID);

      {QM-606/QM-597 EB We go ahead and post the needed callouts here. But there are some Save issues}
      if (FinishCallOut.HasMoreDates) and (FinishCallOut.FinishWorkDate = DateOf(WorkDate)) then begin    //QM-597 EB Abandoned Callout
        TimeSheet.FieldByName('callout_start1').AsDateTime := FinishCallOut.AddCallOutStart;
        TimeSheet.FieldByName('callout_stop1').AsDateTime := FinishCallOut.AddCallOutStop;
        TimeSheet.Post;  {Go ahead and post this as it was set on Dialog screen}
      end;
    end
    else if not Timesheet.Locate('entry_id', EntryID, []) then
      raise Exception.CreateFmt('Timesheet entry %d not found', [EntryID]);
    SetDateFieldDisplayFormats(Timesheet, QTimeFormat);

    DetermineReadOnly;
    AfterDisplay;
  finally
    Timesheet.EnableControls;
  end;
end;

procedure TBaseTimesheetDayFrame.Redisplay;
begin
  Assert(((FEmpID > 0) and (FWorkDate > 1)),
    'Cannot Redisplay a day before calling Display');
  Display(FEmpID, FWorkDate, FMode);
end;

procedure TBaseTimesheetDayFrame.DisplayDataSet(DataSet: TDataSet);
begin
  Assert(Assigned(DataSet));
  Assert(DataSet.Active);
  if DataSet.Eof then
    Exit;
  TimesheetSource.DataSet := DataSet;
  Timesheet := DataSet;
  FWorkDate := DataSet.FieldByName('work_date').AsDateTime;
  FEmpID := DataSet.FieldByName('work_emp_id').AsInteger;
  FMode := teReadOnly;
  FViewTotalsMode := True;
  DetermineReadOnly(True);
  AfterDisplay;
  RecalculateTotals;
end;

function TBaseTimesheetDayFrame.Modified: Boolean;

begin
  Result := False;
  if Timesheet.State in dsEditModes then begin
    Result := True;
    if Timesheet.State = dsInsert then begin
      Result := not (AllFieldsHaveValue(Timesheet, TimeFields, Null) and    //QM-1061 Timesheet fix to gap workstarts EB
        (Timesheet.FieldByName('vehicle_use').AsString = 'NONE'));
    end;
  end;
end;

procedure TBaseTimesheetDayFrame.RecalculateTotals;

  function FloatValue(FieldName: string): Double;
  begin
    Result := 0;
    if Timesheet.Active then
      Result := Timesheet.FieldByName(FieldName).AsFloat;
  end;

  function GetDayHours: Double;
  begin
    Result := FWorkHours + FCalloutHours;
	//         + FFloatHolHours;                     //QM-614 EB removing these hours
//              FloatValue('leave_hours')  +
//              FloatValue('vac_hours') +
//              FloatValue('br_hours') +
//              FloatValue('hol_hours') +
//              FloatValue('jury_hours') +
//              FloatValue('pto_hours') +

  end;

begin
  if FIgnoreRecalc then
    Exit;

  FWorkHours := GetWorkHours(Timesheet);
  FCalloutHours := GetCalloutHours(Timesheet);

  if FWorkHours < 0 then
    raise EOdDataEntryError.Create('You can not work negative hours');
  Assert(FWorkHours >= 0);

  if FCalloutHours < 0 then
    raise EOdDataEntryError.Create('You can not work negative callout hours');
  Assert(FCalloutHours >= 0);

  FDayHours := GetDayHours;
  if (FDayHours > 0) and DisplayingToday then    //QM-100 EB UtiliSafe
    fWorkStartedToday := True;
  

  NotifyDataChange;

  WorkHours.Caption := HoursFormat(FWorkHours);
  DayHours.Caption := HoursFormat(FDayHours);

  if FViewTotalsMode then begin
    RegularHours.Caption := HoursFormat(FloatValue('reg_hours'));
    OvertimeHours.Caption := HoursFormat(FloatValue('ot_hours'));
    DoubleTimeHours.Caption := HoursFormat(FloatValue('dt_hours'));
    CalloutHours.Caption := HoursFormat(FloatValue('callout_hours'));
  end;
end;

procedure TBaseTimesheetDayFrame.SaveChanges;
begin
  // Calculated changes must be saved even if we are in ReadOnly mode so the hour totals are correct
  if ((not ReadOnly) and EditingDataSet(Timesheet)) or FHaveCalculatedChanges then begin
    Assert(FEmpID = Timesheet.FieldByName('work_emp_id').AsInteger);
    Assert(FWorkDate = Timesheet.FieldByName('work_date').AsDateTime);
    EditDataSet(Timesheet);
    BeforeErrorCheck;
    CheckForErrors;
    Timesheet.UpdateRecord;
    RecalculateTotals;
    FIgnoreRecalc := True;
    Timesheet.FieldByName('emp_type_id').AsInteger := GetComboObjectInteger(EmployeeType, True);
    if (FMode = teManager) then
      Timesheet.FieldByName('reason_changed').AsInteger := GetCxComboObjectInteger(ReasonChanged, True);
    try
      Timesheet.Post;
      FHaveCalculatedChanges := False;
    finally
      FIgnoreRecalc := False;
    end;
  end;
end;

procedure TBaseTimesheetDayFrame.BeforeErrorCheck;
begin
  // override in child objects to do anything extra before checking for errors
end;

procedure TBaseTimesheetDayFrame.CheckForErrors;

  procedure CheckMileage(const VehicleUse, FieldName: string);
  var
    Miles: TField;
  begin
    Miles := Timesheet.FieldByName(FieldName);
    if not Miles.IsNull then begin
      if Miles.AsInteger <= 0 then
        raise EOdDataEntryError.Create('Mileage numbers must be 1 or more if any are entered');
    end;
  end;

  procedure CheckBreakRules(const ProfitCenter: string);  {This only happens for today's timesheet}
  var
    Msg: string;
    i: Integer;
    StartTime: TDateTime;
    StopTime: TDateTime;
    RuleConfig: TDataset;
    BreakChecker: TBreakRulesChecker;
  begin
    // safety check   Bypass on previous day
    if (not DM.CanAccessProfitCenterCache) and
      (Timesheet.FieldByName('work_emp_id').Value <> DM.EmpID) then begin
      MessageDlg('Without timesheet approval right, work break rules are not checked ' +
        'when editing another user''s timesheet.', mtWarning, [mbOk], 0);
      Exit;
    end;

    RuleConfig := DM.Engine.OpenQuery('select * from break_rules where active and pc_code = '
      + QuotedStr(ProfitCenter) + ' and upper(rule_type) in (''EVERY'',''AFTER'')');
    try
      if RuleConfig.IsEmpty then
        Exit;

      BreakChecker := CreateBreakRuleChecker(RuleConfig.FieldByName('rule_type').AsString,
        RuleConfig.FieldByName('break_length').AsInteger,
        RuleConfig.FieldByName('break_needed_after').AsInteger);
      try
        for i := 1 to TimesheetNumWorkPeriods do begin
          StartTime := Timesheet.FieldByName('work_start' + IntToStr(i)).AsDateTime;
          StopTime := Timesheet.FieldByName('work_stop' + IntToStr(i)).AsDateTime;
          if TimeSlotIsEmpty(StartTime, StopTime) or FloatEquals(StopTime, 0) then
            Continue;
          BreakChecker.Add(StartTime, StopTime);
        end;
        for i := 1 to TimesheetNumCalloutPeriods do begin
          StartTime := Timesheet.FieldByName('callout_start' + IntToStr(i)).AsDateTime;
          StopTime := Timesheet.FieldByName('callout_stop' + IntToStr(i)).AsDateTime;
          if TimeSlotIsEmpty(StartTime, StopTime) or FloatEquals(StopTime, 0) then
            Continue;
          BreakChecker.Add(StartTime, StopTime);
        end;

        EditDataSet(Timesheet);
        if not BreakChecker.Check then begin
          Msg := RuleConfig.FieldByName('rule_message').AsString;
          if ShowMessageAckDialog(Msg, 'Acknowledge,Cancel') = 'Acknowledge' then begin
            Timesheet.FieldByName('rule_ack_date').Value := Now;
            Timesheet.FieldByName('rule_id_ack').Value := RuleConfig.FieldByName('rule_id').AsInteger;
          end
          else
            Abort;
        end else begin
          Timesheet.FieldByName('rule_ack_date').Clear;
          Timesheet.FieldByName('rule_id_ack').Clear;
        end;
      finally
        FreeAndNil(BreakChecker);
      end;
    finally
      FreeAndNil(RuleConfig);
    end;
  end;

var
  Gaps: string;
begin
  RecalculateTotals;

  CheckWorkPeriods(Timesheet, 'work_', 'Work', TimesheetNumWorkPeriods);
  CheckWorkPeriods(Timesheet, 'callout_', 'Callout', TimesheetNumCalloutPeriods);
     //  CheckHoursEntry(Timesheet, 'leave_hours', 'Sick Leave');
     //  CheckHoursEntry(Timesheet, 'vac_hours', 'Vacation');
     //  CheckHoursEntry(Timesheet, 'br_hours', 'Bereavement');
     //  CheckHoursEntry(Timesheet, 'hol_hours', 'Holiday');
     //  CheckHoursEntry(Timesheet, 'jury_hours', 'Jury duty');
  CheckOverlap(Timesheet, 'work_', 'callout_', TimesheetNumWorkPeriods, TimesheetNumCalloutPeriods);
  if DuringSubmit then begin
    CheckWorkPeriodsEnd(Timesheet, 'work', TimesheetNumWorkPeriods);
    CheckWorkPeriodsEnd(Timesheet, 'callout', TimesheetNumCalloutPeriods);
  end;

  Gaps := CheckSequentialWorkTimes(Timesheet, 'work_', TimesheetNumWorkPeriods);  //QM-1061 Timesheet fix EB
  if Gaps <> '' then
    raise EODDataEntryError.Create('You must correct the empty field gap(s) before ' + Gaps + ' prior to saving.');

  Gaps := CheckSequentialWorkTimes(Timesheet, 'callout_', TimesheetNumWorkPeriods);  //QM-1061 Timesheet fix EB
  if Gaps <> '' then
    raise EODDataEntryError.Create('You must correct the empty field gap(s) before ' + Gaps + ' prior to saving.');


  if (FDayHours > 24) then
    raise EOdDataEntryError.Create('You can not work more than 24 hours per day');
  if (FDayHours < 0) then
    raise EOdDataEntryError.Create('You can not work negative hours');

//  CheckMileage(Timesheet.FieldByName('vehicle_use').AsString, 'miles_start1');
//  CheckMileage(Timesheet.FieldByName('vehicle_use').AsString, 'miles_stop1');

//  if Timesheet.FieldByName('miles_stop1').AsInteger < Timesheet.FieldByName('miles_start1').AsInteger then
//    raise EOdDataEntryError.Create('The starting mileage must be less than the stopping mileage');

  if Permissions.fVehicleUsageRequired and (Timesheet.FieldByName('vehicle_use').AsString = '') then
    raise EOdDataEntryError.Create('You must enter vehicle usage for each day.');

  if (FMode = teNormal) and DisplayingToday then
    CheckBreakRules(DM.GetProfitCenterForEmployee(Timesheet.FieldByName('work_emp_id').Value));

//  if (Owner <> nil) and (Owner is TTimesheetForm) then begin
//    TimeForm := Owner as TTimesheetForm;
//    if (TimeForm.FloatingHolidaysYear[YearOf(WorkDate)] > TimeForm.MaxFloatingHolidaysPerYear) then
//      raise EOdDataEntryError.Create('You have exceeded the maximum number of floating Holidays for ' + IntToStr(YearOf(WorkDate)));
//  end;

//  if (Timesheet.FieldByName('vac_hours').AsFloat +        //qm-528  sr
//  Timesheet.FieldByName('leave_hours').AsFloat > 0) and      //qm-528  sr
//    (Timesheet.FieldByName('pto_hours').AsFloat > 0) then        //qm-528  sr
//    raise EOdDataEntryError.Create('You can not enter both vacation/leave and PTO hours');      //qm-528  sr

  if (FMode = teManager) and (ReasonChanged.Enabled) and (ReasonChanged.ItemIndex = -1) then begin
    ReasonChanged.SetFocus;
    raise EOdDataEntryError.Create('You must select a reason for changing another employee''s timesheet.');
  end;
end;

procedure TBaseTimesheetDayFrame.SetReadOnly(const Value: Boolean);
begin
  if Value <> FReadOnly then begin
    FReadOnly := Value;
    if Assigned(TimesheetSource) then
      TimesheetSource.AutoEdit := not Value;
  end;
end;

procedure TBaseTimesheetDayFrame.TimesheetSourceDataChange(Sender: TObject; Field: TField);
begin
  RecalculateTotals;
  ShowErrors;
end;

procedure TBaseTimesheetDayFrame.MilesStart1Exit(Sender: TObject);
begin
  if (MilesStart1.Text <> '') and (Timesheet.FieldByName('miles_stop1').AsString = '') then begin
    Timesheet.Edit;
    Timesheet.FieldByName('miles_stop1').AsString := Timesheet.FieldByName('miles_start1').AsString;
  end;
end;

function TBaseTimesheetDayFrame.GetCurrentTimesheetEntryID(EmpID: Integer): Integer;
const
  SQL = 'select entry_id from timesheet_entry where work_emp_id = %d and (status=''%s'' or status=''%s'') and work_date = %s';
var
  DataSet: TDataSet;
begin
  DataSet := DM.Engine.OpenQuery(Format(SQL, [EmpID, TimesheetStatusActive, TimesheetStatusSubmit, DateToDBISAMDate(FWorkDate)]));
  try
    Assert(DataSet.RecordCount <= 1, 'There should only be one active local timesheet');
    if DataSet.Eof then
      Result := -1
    else
      Result := DataSet.FieldByName('entry_id').AsInteger;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TBaseTimesheetDayFrame.ShowErrors;
begin
  // Highlight data entry errors here?
end;

procedure TBaseTimesheetDayFrame.TimesheetDataBeforePost(DataSet: TDataSet);
begin
  // We need both BeforePost events to run
  DM.Engine.BeforePost(DataSet);
  Timesheet.FieldByName('entry_date_local').AsDateTime := Now;
  Timesheet.FieldByName('entry_by').AsInteger := DM.EmpID;
  if not FDuringSubmit then
    Timesheet.FieldByName('status').AsString := TimesheetStatusActive;
end;

function TBaseTimesheetDayFrame.DisplayingToday: Boolean;
begin
  Result := FWorkDate = Today;
end;

function TBaseTimesheetDayFrame.DisplayingYesterday: Boolean;
begin
  Result := FWorkDate = Yesterday;
end;

procedure TBaseTimesheetDayFrame.DisplayPerDiemCaption;
begin
  if PerDiem.Checked then
    PerDiem.Caption := '$45'
  else
    PerDiem.Caption := '';

  RecalculateTotals;
  NotifyDataChange;
end;

procedure TBaseTimesheetDayFrame.AfterDisplay;
begin
  Assert(FEmpID > 0);
  Assert(FWorkDate > 1);
  SetDayLabel;
  If FWorkDate > Today then CleanupFutureCheckboxes;
  UpdateApprovalData;
  InitPermissions; // QM-517 Sick Leave EB
  ReasonChanged.ItemIndex := -1;
  if TimeSheet.Active then begin
    if Timesheet.FieldByName('emp_type_id').IsNull then
      SelectComboboxItemFromObjects(EmployeeType, EmployeeDM.GetEmployeeTypeID(FEmpID))
    else
      SelectComboboxItemFromObjects(EmployeeType, Timesheet.FieldByName('emp_type_id').AsInteger);
  end else
    EmployeeType.ItemIndex := -1;
  SizeComboDropdownToItems(EmployeeType);
  EmployeeType.DropDownCount := 12;
end;

procedure TBaseTimesheetDayFrame.SetShowApprovalData(const Value: Boolean);
begin
  FShowApprovalData := Value;
end;



procedure TBaseTimesheetDayFrame.UpdateApprovalData;
var
  Info: string;
  Name: string;
  Date: string;
begin
  Info := '';
  if ShowApprovalData and Timesheet.Active then begin
    Info := Timesheet.FieldByName('status').AsString + '  ';
    if not Timesheet.FieldByName('approve_by').IsNull then begin
      Name := EmployeeDM.GetEmployeeShortName(Timesheet.FieldByName('approve_by').AsInteger);
      Date := FormatDateTime('mmm d, h:mm', Timesheet.FieldByName('approve_date').AsDateTime);
      Info := Info + 'Aprv by ' + Name + ', ' + Date;
      if not Timesheet.FieldByName('final_approve_by').IsNull then begin
        Name := EmployeeDM.GetEmployeeShortName(Timesheet.FieldByName('final_approve_by').AsInteger);
        Date := FormatDateTime('mmm d, h:mm', Timesheet.FieldByName('final_approve_date').AsDateTime);
        Info := Info + '  ' + '  FAprv by ' + Name + ', ' + Date;
      end;
    end;
  end;
  ApprovalMemo.Lines.Text := Info;
end;

procedure TBaseTimesheetDayFrame.SubmitActionExecute(Sender: TObject);
const
  PendingUploadMsg = 'Warning, you have pending uploads which should be uploaded before submitting a timesheet. ' +
                     'Do you want to continue submitting timesheet?';

begin
  if Assigned(DM.FBackgroundUploader) and
        DM.FBackgroundUploader.InProgress then begin
        if MessageDlg(PendingUploadMsg, mtWarning, [mbYes, mbNo], 0) = mrNo then
          exit;
  end;

  if ConfirmContinueWithSubmit then
    SubmitTime;
end;

procedure TBaseTimesheetDayFrame.SubmitTime;
begin
  EditDataSet(Timesheet);
  Timesheet.FieldByName('status').AsString := TimesheetStatusSubmit;
  FDuringSubmit := True;
  try
    try
      DisplaySubmitMessage;
      SaveAllDays;
    except
      Timesheet.FieldByName('status').AsString := TimesheetStatusActive;
      raise;
    end;
  finally
    FDuringSubmit := False;
  end;
  // We would need to refresh the data to get a possibly server-edited timesheet
  ReadOnly := True;
end;

function TBaseTimesheetDayFrame.ConfirmContinueWithSubmit: Boolean;
const
  Msg = 'Once submitted, this timesheet can not be edited.  Continue?';
begin
  Result := (MessageDlg(Msg, mtInformation, [mbYes, mbNo], 0) = mrYes);
end;

function TBaseTimesheetDayFrame.IsSubmitted: Boolean;
begin
  Result := False;
  if Timesheet.Active then
    Result := (Timesheet.FieldByName('status').AsString = TimesheetStatusSubmit);
end;

procedure TBaseTimesheetDayFrame.InitPermissions;  //QM-517 Sick Leave Refactoring  EB
begin
  With Permissions do begin
    FShowMilesEntry := EmployeeDM.EmployeeHasCompanyVehicle(FEmpID);
    FAllowPovCovEntry := PermissionsDM.CanEmployee(FEmpID, RightPOVCOVEntry);
    FAllowEmpTypeEntry := PermissionsDM.CanI(RightTimesheetsEmpTypeEntry);
    fPerDiemEntry := PermissionsDM.CanI(RightTimesheetsPerDiemEntry);  // QMANTWO-789 sr
    FAllowSickLeaveEntry := PermissionsDM.CanI(RightTimesheetsViewSickLeave); // QM-517 Sick Leave EB
    fPriorDayEntry := PermissionsDM.CanI(RightTimesheetsPriorDayEntry);
    fVehicleUsageRequired := FAllowPovCovEntry and (not PermissionsDM.IsInEmployeeLimitationList(FEmpID, RightPOVCOVEntry, RightLimitationOptional));

    PerDiem.enabled := fPerDiemEntry;
  end;
end;

function TBaseTimesheetDayFrame.IsFinalApproved: Boolean;
begin
  Result := False;
  if Timesheet.Active then
    Result := (Timesheet.FieldByName('final_approve_by').AsInteger > 0);
end;

function TBaseTimesheetDayFrame.WorkingHours: Double;
begin
  Result := FWorkHours;
end;

function TBaseTimesheetDayFrame.DayCalloutHours: Double;
begin
  Result := FCalloutHours;
end;

procedure TBaseTimesheetDayFrame.SaveFieldDouble(FieldName: string; NewValue: Double);
begin
  NewValue := FloatRound(NewValue, 2);
  if not FloatEquals(Timesheet.FieldByName(FieldName).AsFloat, NewValue) then begin
    EditDataSet(Timesheet);
    Timesheet.FieldByName(FieldName).AsFloat := NewValue;
    FHaveCalculatedChanges := True;
  end;
end;

procedure TBaseTimesheetDayFrame.SetHoursBreakdown(Regular, Overtime, DoubleTime, Callout: Double);
var
  OldWorkingHours: Double;
  NewWorkingHours: Double;
begin
  OldWorkingHours := WorkingHours + DayCalloutHours;
  NewWorkingHours := Regular + Overtime + DoubleTime + Callout;

  // This logic is no longer correct
  if not FloatEquals(NewWorkingHours, OldWorkingHours) then
    raise Exception.Create('Internal error in hours breakdown calculation');

  TimesheetSource.OnDataChange := nil;
  try
    if Timesheet.Active then begin
      SaveFieldDouble('reg_hours', Regular);
      SaveFieldDouble('ot_hours', Overtime);
      SaveFieldDouble('dt_hours', DoubleTime);
      SaveFieldDouble('callout_hours', Callout);
    end;
  finally
    TimesheetSource.OnDataChange := TimesheetSourceDataChange;
  end;

  RegularHours.Caption := HoursFormat(Regular);
  OvertimeHours.Caption := HoursFormat(Overtime);
  DoubleTimeHours.Caption := HoursFormat(DoubleTime);
  CalloutHours.Caption := HoursFormat(Callout);
end;

procedure TBaseTimesheetDayFrame.NotifyDataChange;
begin
  if Assigned(FOnDataChange) then
    FOnDataChange(Self);
end;



procedure TBaseTimesheetDayFrame.SetDoubleTimePossible(Possible: Boolean);
begin
  FDoubleTimePossible := Possible;
end;

procedure TBaseTimesheetDayFrame.CleanupFutureCheckboxes;
begin
  if fWorkDate > Today then begin
    PerDiem.Checked := False;
    PerDiem.Caption := '';
  end;
end;

procedure TBaseTimesheetDayFrame.CloseData;
begin
  if EditingDataSet(Timesheet) then
    Timesheet.Cancel;

  Timesheet.Close;
end;

procedure TBaseTimesheetDayFrame.TimeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Control: TcxDBTimeEdit;
  DataSet: TDataSet;
  FieldName: string;
begin
  if (Key = VK_DELETE) and (Sender is TcxDBTimeEdit) then begin
    Key := 0;
    Control := Sender as TcxDBTimeEdit;
    FieldName := Control.DataBinding.DataField;
    Assert(Assigned(Control.DataBinding.DataSource));
    Assert(Assigned(Control.DataBinding.DataSource.DataSet));
    DataSet := Control.DataBinding.DataSource.DataSet;
    DataSet.FieldByName(FieldName).Clear;
  end;
end;


procedure TBaseTimesheetDayFrame.ShowHideVehicleFields;
begin
{QM-579: Hiding out by request}
  VehicleUse.Visible := False; //Permissions.FAllowPovCovEntry;

  MilesStart1.Visible := False; //Permissions.FShowMilesEntry;
  MilesStop1.Visible := False; //Permissions.FShowMilesEntry;
end;

function TBaseTimesheetDayFrame.IsPriorDayActive: Boolean;
const
  SQL = 'select entry_id from timesheet_entry where work_emp_id = %d ' +
    'and status = ''%s'' and work_date = %s and final_approve_by is null';
var
  DataSet: TDataSet;
  PriorDay: TDateTime;
begin
  Result := Permissions.fPriorDayEntry;
  if not Result then
    Exit;

  PriorDay := FWorkDate - 1;
  DataSet := DM.Engine.OpenQuery(Format(SQL, [FEmpID, TimesheetStatusActive, DateToDBISAMDate(PriorDay)]));
  try
    Assert(DataSet.RecordCount <= 1, 'There should only be one active local timesheet for ' + DateToStr(PriorDay));
    Result := not DataSet.IsEmpty;
  finally
    FreeAndNil(DataSet);
  end;
end;

procedure TBaseTimesheetDayFrame.DetermineReadOnly(ForceReadOnly: Boolean);
var
  EditableDay: Boolean;
begin
  case FMode of
    teReadOnly:
      ReadOnly := True;
    teNormal:
      begin
        if IsSubmitted then
          ReadOnly := True
        else begin
          if Permissions.fPriorDayEntry then
            EditableDay := (DisplayingToday and (not IsPriorDayActive)) or DisplayingYesterday
          else
            EditableDay := (FWorkDate = Today);
          ReadOnly := (not EditableDay) or IsFinalApproved;
        end;
      end;
    teManager:
      ReadOnly := ForceReadOnly or IsFinalApproved;
    teITOverride:
      ReadOnly := ForceReadOnly;
  end;
end;

procedure TBaseTimesheetDayFrame.SaveAllDays;
begin
  if not Assigned(FOnSaveDays) then
    Raise Exception.Create('FOnSaveDays not assigned');
  FOnSaveDays(Self);
end;

procedure TBaseTimesheetDayFrame.UpdateEditFlagLabel;
begin
  if Timesheet.State = dsEdit then
    EditedFlagLabel.Caption := 'e'
  else if Timesheet.State = dsInsert then
    EditedFlagLabel.Caption := 'i'
  else if not Timesheet.Active then
    EditedFlagLabel.Caption := 'n';
  EditedFlagLabel.Visible := EditingDataSet(Timesheet) and DM.UQState.DeveloperMode;
end;


procedure TBaseTimesheetDayFrame.TimesheetDataAfterInsert(DataSet: TDataSet);
begin
  TimesheetData.FieldByName('floating_holiday').AsBoolean := False;
  TimeSheetData.FieldByName('per_diem').AsBoolean := False; //QMANTWO-733 EB - Get rid of Null
end;

procedure TBaseTimesheetDayFrame.DisplayFloatingHolidayCaption;
begin
  if FloatingHoliday.Checked then begin
    FloatingHoliday.Caption := '8:00';
    FFloatHolHours := 8.0;
  end else begin
    FloatingHoliday.Caption := '';
    FFloatHolHours := 0;
  end;
  RecalculateTotals;
  NotifyDataChange;
end;

procedure TBaseTimesheetDayFrame.FloatingHolidayClick(Sender: TObject);
begin
  DisplayFloatingHolidayCaption;
end;

function TBaseTimesheetDayFrame.FloatingHolidayEnabled: Boolean;
begin
  Result := not ReadOnly and (DayOfWeek(WorkDate) in [2..6])
end;

procedure TBaseTimesheetDayFrame.DisplaySubmitMessage;
const
  SelectSubmitMsg = 'select rule_id, rule_message, message_frequency, ' +
    'coalesce(button_text, ''Acknowledge'') as button_text from break_rules ' +
    'where upper(pc_code)=upper(''%s'') and upper(rule_type)=''SUBMIT'' and active';
var
  NeedToShowMessage: Boolean;
  LastShownDate: TDateTime;
  Data: TDataSet;
  Response: string;
  Agree: Boolean;
  ResponseDate: TDateTime;
  ContactPhone: string;
begin
  Data := DM.Engine.OpenQuery(Format(SelectSubmitMsg,
    [DM.GetProfitCenterForEmployee(Timesheet.FieldByName('work_emp_id').Value)]));
  try
    NeedToShowMessage := not Data.Eof;
    if NeedToShowMessage and (Data.FieldByName('message_frequency').AsInteger > 0) then begin
      if DM.UQState.TimeSubmitMessageLastDate <> '' then
        LastShownDate := IsoStrToDate(DM.UQState.TimeSubmitMessageLastDate)
      else
        LastShownDate := 0;
      NeedToShowMessage := Today >= Trunc(LastShownDate + Data.FieldByName('message_frequency').Value);
    end;
    if NeedToShowMessage then begin
      repeat
        ContactPhone := '';
        Response := ShowMessageAckDialog(Data.FieldByName('rule_message').AsString,
          Data.FieldByName('button_text').AsString);
        Agree := (not SameText(Response, 'disagree'));
        if not Agree then
          ContactPhone := PromptUserForContactPhone;
      until Agree or (NotEmpty(ContactPhone));

      ResponseDate := Now;
      DM.AddBreakRuleResponse(Data.FieldByName('rule_id').Value, Response, ContactPhone, ResponseDate);
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTimeSubmitMsgAck, IntToStr(Data.FieldByName('rule_id').Value), ResponseDate, Response);
      DM.UQState.TimeSubmitMessageLastDate := IsoDateToStr(Date);
    end;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TBaseTimesheetDayFrame.ShowSubmitHint(Show: Boolean);
begin
  SubmitHint.Visible := Show;
  if Show then
    SubmitHint.BringToFront;
end;

procedure TBaseTimesheetDayFrame.EmployeeTypeSelect(Sender: TObject);
begin
  EditDataSet(Timesheet);
end;




procedure TBaseTimesheetDayFrame.PerDiemClick(Sender: TObject);
begin
  if PerDiem.Checked then PerDiem.Caption := '$45'  //QMANTWO-733 EB - Per Steven, hardcode as it doesn't change very often
  else PerDiem.Caption := '';
end;

function TBaseTimesheetDayFrame.PromptUserForContactPhone: string;
const
  SelectTimeSubmitDisagreeMsg = 'select rule_id, rule_message, message_frequency, ' +
    'coalesce(button_text, ''Agree'') as button_text from break_rules ' +
    'where upper(pc_code)=upper(''%s'') and upper(rule_type)=upper(''%s'') and active';
var
  Data: TDataSet;
begin
  Result := '000-000-0000';
  Data := DM.Engine.OpenQuery(Format(SelectTimeSubmitDisagreeMsg,
    [DM.GetProfitCenterForEmployee(Timesheet.FieldByName('work_emp_id').Value),
    BreakRuleTypeSubmitDisagree]));
  try
    if not Data.Eof then
      Result := ShowMessageAckPromptDialog(Data.FieldByName('rule_message').AsString,
        Data.FieldByName('button_text').AsString, 'Contact Phone Number:', '###-###-####', Result);
  finally
    FreeAndNil(Data);
  end;
end;

procedure TBaseTimesheetDayFrame.ReasonChangedPropertiesChange(
  Sender: TObject);
begin
  EditDataSet(Timesheet);
end;

procedure TBaseTimesheetDayFrame.DayActionsUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  RefreshScreenControls;
end;

procedure TBaseTimesheetDayFrame.SetExpanded(const Value: Boolean);
begin
  FExpanded := Value;
end;

procedure TBaseTimesheetDayFrame.RefreshScreenControls;
var
  SpecialHoursEditable: Boolean;
begin
  SpecialHoursEditable := not ReadOnly;
  SubmitAction.Enabled := (not IsSubmitted) and (not ReadOnly) and (DM.EmpID = FEmpID);
  SubmitAction.Visible := (not ReadOnly) and (FMode <> teReadOnly);
  ReasonChanged.Visible := (FMode in [teManager, teITOverride]);
  ReasonChanged.Enabled := (not ReadOnly) and (FMode in [teManager, teITOverride]);

  ShowHideVehicleFields;

  DoubleTimeHours.Visible := FDoubleTimePossible;
  ApprovalMemo.Visible := FShowApprovalData;

  UpdateEditFlagLabel;

    DisplayPTO(SpecialHoursEditable);
    DisplaySL(SpecialHoursEditable);


  VacationHours.Enabled := False; //QM-517 Sick Leave - turn this off totally
  VacationHours.Visible := False; //QM-579 EB Move this up here

  BereavementHours.Enabled := False; //QM-614 EB QMSpecialHoursEditable;
  HolidayHours.Enabled := False; //QM-614 EB SpecialHoursEditable;
  JuryDutyHours.Enabled := False; //QM-614 EB SpecialHoursEditable;
  VehicleUse.Enabled := not ReadOnly;
  MilesStart1.Enabled := not ReadOnly;
  MilesStop1.Enabled := not ReadOnly;
  RegularHours.Enabled := not ReadOnly;
  WorkHours.Enabled := not ReadOnly;
  OvertimeHours.Enabled := not ReadOnly;
  DoubleTimeHours.Enabled := not ReadOnly;
  CalloutHours.Enabled := not ReadOnly;
  DayHours.Enabled := not ReadOnly;
  EmployeeType.Enabled := Permissions.FAllowEmpTypeEntry and (not ReadOnly);
  if EmployeeType.Enabled then
    EmployeeType.Color := clWindow
  else
    EmployeeType.ParentColor := True;

  FloatingHoliday.Enabled := False; //QM-614 EB FloatingHolidayEnabled and FloatingHolidaysAvailable;
  PerDiem.Enabled := False; //QM-614 EB ((not ReadOnly) and (Permissions.fPerDiemEntry));  // QMANTWO-789 sr
end;

function TBaseTimesheetDayFrame.IsSubmitHintShowing: Boolean;
begin
  Result := SubmitHint.Visible;
end;

procedure TBaseTimesheetDayFrame.DisplayPTO(Editable: Boolean); //QM-579 EB Sick and PTO Bal
{  QM-517 Sick Leave EB - Remove dualing visibility issue }
begin

  PTOHours.Enabled := False; //Editable;  //QM-579 EB
  {QM-579 Replacement}
  {If they are over....doesn't matter the permissions, it will be disabled}
  {QM-579 PART 2 Code}
//  if (PTOBalance <= -40.0) then begin
//    PTOHours.Enabled := False;
//    fPTOOverLimit := True;
//  end
//  else fPTOOverLimit := False;
end;


procedure TBaseTimesheetDayFrame.DisplaySL(Editable: Boolean);  //QM-579 EB Sick and PTO Bal
begin
  LeaveVacPanel.Visible := True;

  {Shows * if it is in the DeveloperOverrideMode}
  if DM.UQState.DeveloperOverrideMode then
    lblDevOverride.Visible := True
  else
    lblDevOverride.Visible := False;

  SickLeaveHours.Enabled := False;
  {No Balance Record - Let them Edit if Edit mode}
 // if AccruedBal.NoRecordFound and Permissions.FAllowSickLeaveEntry and (Editable) then
//    SickLeaveHours.Enabled := True
//  else if DM.UQState.DeveloperOverrideMode then begin //QM-579 EB
//    SickLeaveHours.Enabled := True;
//  end
//  else if (AccruedBal.Sick <= 0.0) then
//   SickLeaveHours.Enabled := False
//  else
//    SickLeaveHours.Enabled := (Permissions.FAllowSickLeaveEntry) and (Editable);
end;


procedure TBaseTimesheetDayFrame.SetAccruedValsFromTimeSheet(AccruedSick,
  AccruedPTO: double; NoBalanceRecord: Boolean);  //QM-579 EB Sick and PTO Bal
begin
 {This lets our current day know the Accrued amount of PTO and Sickleave
  from previous days adjusted from the balancesheet}
  AccruedBal.PTO := AccruedPTO;
  AccruedBal.Sick := AccruedSick;
  AccruedBal.NoRecordFound := NoBalanceRecord;

//  ShowMessage('Sick:'+ FloatToStr(AccruedSick));
end;

end.


