unit DailySalesReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, OdRangeSelect, StdCtrls;

type
  TDailySalesReportForm = class(TReportBaseForm)
    ReportDateLabel: TLabel;
    ReportDate: TOdRangeSelectFrame;
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
  private
    { Private declarations }
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  public
    { Public declarations }
  end;

var
  DailySalesReportForm: TDailySalesReportForm;

implementation

{$R *.dfm}

{ TReportBaseForm1 }
procedure TDailySalesReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
  ReportDate.SelectDateRange(OdRangeSelect.rsLastWeek);
end;

procedure TDailySalesReportForm.ValidateParams;
begin
  inherited;
  SetReportID('DailySales');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamDate('StartDate', ReportDate.FromDate);
  SetParamDate('EndDate', ReportDate.ToDate);
end;

end.
