unit OQManagement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, dbisamtb, ActnList,
  StdCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, ExtCtrls,
  Dmu, MSXML2_TLB, WorkManageUtils, OdMiscUtils, OdIsoDates, DBClient,
  cxCheckBox, SharedDevExStyles;

type
  TOQManagementForm = class(TEmbeddableForm)
    AssetPanel: TPanel;
    OQGRid: TcxGrid;
    OQView: TcxGridDBTableView;
    ColEmpID: TcxGridDBColumn;
    ColEmpShortName: TcxGridDBColumn;
    ColFirstName: TcxGridDBColumn;
    ColLastName: TcxGridDBColumn;
    ColEmpNumber: TcxGridDBColumn;
    ColQualifiedDate: TcxGridDBColumn;
    ColExpiredDate: TcxGridDBColumn;
    ColOQDescription: TcxGridDBColumn;
    ColOQModifier: TcxGridDBColumn;
    ColOQStatus: TcxGridDBColumn;
    ColReportLevel: TcxGridDBColumn;
    ColReportTo: TcxGridDBColumn;
    OQGRidLevel: TcxGridLevel;
    OQSource: TDataSource;
    AssetActions: TActionList;
    AddAssetAction: TAction;
    RemoveAssetAction: TAction;
    SaveChangesAction: TAction;
    Headerpnl: TPanel;
    ReportingToLbl: TLabel;
    QualifiedOnlyCBx: TCheckBox;
    ColIsQualified: TcxGridDBColumn;
    MgrLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure QualifiedOnlyCBxClick(Sender: TObject);
    procedure OQViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ColOQStatusCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    ManagerID: Integer;
    UsingDefMgr: Boolean;
    Loaded: Boolean;
    procedure Init;
  public
    OQListDS: TClientDataSet;
    function LoadOQList(ManagerID: Integer): boolean;
    procedure CreateOQListTable;
    function PopulateOQDataSetfromDOM(Doc: IXMLDomDocument; TargetNode: Integer = 0): integer;  //QM-444 Part 2 EB
    procedure ActivatingNow; override;
  end;

var
  OQManagementForm: TOQManagementForm;

implementation
uses LocalEmployeeDMu, OdMSXMLUtils, UQUtils;

{$R *.dfm}

{ TEmbeddableForm1 }

function TOQManagementForm.PopulateOQDataSetfromDOM(Doc: IXMLDomDocument; TargetNode: Integer = 0): integer;  //QM-444 Part 2 EB
var
  i: integer;
  Nodes: IXMLDOMNodeList;
  Elem: IXMLDOMElement;

  {Copying from WorkManageUtils - could refactor later}
  function AttrText(const AttrName: string): string;
  var
    AttrNode: IXMLDomNode;
  begin
    AttrNode := Elem.attributes.getNamedItem(AttrName);
    if AttrNode <> nil then
      Result := AttrNode.text
    else
      Result := '-';
  end;

  function AttrInt(const AttrName: string): Integer;
  var
    AttrStr: string;
  begin
    AttrStr := AttrText(AttrName);
    try
      Result := StrToInt(AttrStr);
    except
      on E: Exception do begin
        E.Message := Format('Invalid Integer text "%s" for attribute "%s"', [AttrStr, AttrName]);
        raise;
      end;
    end;
  end;

  function AttrDateTime(const AttrName: string): Variant;
  var
    AttrStr: string;
  begin
    Result := Null;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and IsoIsValidDateTime(AttrStr) then
      Result := IsoStrToDateTime(AttrStr);
  end;

  function AttrFloat(const AttrName: string): Double;
  var
    AttrStr: string;
  begin
    Result := 0.0;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and (AttrStr <> NULL) then
      Result := StrToFloat(AttrStr);
  end;
{BEGIN}
begin
  {Get the control ready}
//  if not Assigned(CDataSet) then
//    exit;

  OQListDS.DisableControls;
  while not OQListDS.IsEmpty do
    OQListDS.Delete;

  try
    Nodes := Doc.SelectNodes('//oqList');
    {Process the records}
    for i := 0 to Nodes.Length-1 do begin
      Elem := Nodes[i] as IXMLDOMElement;

      OQListDS.Open;
      OQListDS.Append;
      {----}
      OQListDS.FieldByName('emp_id').asInteger := AttrInt('emp_id');
      OQListDS.FieldByName('short_name').AsString := AttrText('short_name');
      OQListDS.FieldByName('first_name').AsString := AttrText('first_name');
      OQListDS.FieldByName('last_name').AsString := AttrText('last_name');
      OQListDS.FieldByName('emp_number').AsString := AttrText('emp_number');
      OQListDS.FieldByName('qualified_date').AsDateTime := AttrDateTime('qualified_date');
      OQListDS.FieldByName('expired_date').AsDateTime := AttrDateTime('expired_date');
      OQListDS.FieldByName('oq_description').AsString := AttrText('oq_description');
      OQListDS.FieldByName('oq_modifier').AsString := AttrText('oq_modifier');
      OQListDS.FieldByName('oq_status').AsString := UPPERCASE(AttrText('oq_status'));
      OQListDS.FieldByName('report_level').AsInteger := AttrInt('report_level');
      OQListDS.FieldByName('report_to').AsInteger := AttrInt('report_to');
      OQListDS.FieldByName('is_qual').AsString := AttrText('is_qual');
      {----}
      OQListDS.Post;
    end;
  finally                        
    OQListDS.Open;
    OQSource.DataSet := OQListDS;
    OQListDS.EnableControls;
  end;
end;

procedure TOQManagementForm.QualifiedOnlyCBxClick(Sender: TObject);
begin
  inherited;
  if QualifiedOnlyCBx.checked then begin
    OQListDS.Filter := 'is_qual = ''1''';
    OQListDS.Filtered := True;
  end
  else begin
    OQListDS.Filter := '';
    OQListDS.Filtered := False;
  end;
end;

procedure TOQManagementForm.ActivatingNow;
var
  CanDebug: Boolean;
begin
  inherited;
  if UsingDefMgr then begin  {Tells us whether to load people under the logged in user}
    ManagerID := DM.EmpID;
  end;

  LoadOQList(ManagerID);

  {Open up the background fields so that they can be viewed in Debug}
  if DM.UQState.DeveloperMode then
    CanDebug := True
  else
    CanDebug := False;

    ColEmpID.VisibleForCustomization := CanDebug;
    ColReportTo.VisibleForCustomization := CanDebug;
    ColReportLevel.VisibleForCustomization := CanDebug;
end;



procedure TOQManagementForm.CreateOQListTable;
begin
  OQListDS := TClientDataSet.Create(Application);
  OQListDS.FieldDefs.Add('emp_id', ftInteger);
  OQListDS.FieldDefs.Add('short_name', ftString, 30);
  OQListDS.FieldDefs.Add('first_name', ftString, 30);
  OQListDS.FieldDefs.Add('last_name', ftString, 30);
  OQListDS.FieldDefs.Add('emp_number', ftString, 20);
  OQListDS.FieldDefs.Add('qualified_date', ftDateTime);
  OQListDS.FieldDefs.Add('expired_date', ftDateTime);
  OQListDS.FieldDefs.Add('oq_description', ftString, 100);
  OQListDS.FieldDefs.Add('oq_modifier', ftString, 20);
  OQListDS.FieldDefs.Add('oq_status', ftString, 20);
  OQListDS.FieldDefs.Add('report_level', ftInteger);
  OQListDS.FieldDefs.Add('report_to', ftInteger);
  OQListDS.FieldDefs.Add('is_qual', ftString, 20);
  OQListDS.CreateDataSet;

//  for i := 0 to OQListDS.FieldDefs.Count - 1 do
//    OQListDS.FieldDefs[i].Required := False;
  //OQList.CreateTable;
end;


procedure TOQManagementForm.FormCreate(Sender: TObject);
begin
  inherited;
  Init;
end;

procedure TOQManagementForm.Init;
begin
  ManagerID := 0;
  Loaded := False;
  UsingDefMgr := True; {Will reset this if coming from Work Management}
  CreateOQListTable;
end;

function TOQManagementForm.LoadOQList(ManagerID: Integer): boolean;
var
  Doc: IXMLDomDocument;
  RecCount: integer;
begin
  Doc := ParseXml(EmployeeDM.GetOQListForManager(ManagerID));
  RecCount := PopulateOQDataSetfromDOM(Doc, ManagerID);
  MgrLbl.Caption := EmployeeDM.GetEmployeeShortName(ManagerID);
end;


procedure TOQManagementForm.ColOQStatusCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  //////////
  ///
end;

procedure TOQManagementForm.OQViewCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  QualFlag, ExpFlag: TQualFlag;
  ExpDate: TDateTime;
begin
  inherited;
  {These are applied after any style changes}
    {Is the locator qualified?}
    if not AViewInfo.Selected then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[ColIsQualified.Index]) then begin
        If (AViewInfo.GridRecord.Values[ColIsQualified.Index] = '1') then begin
          QualFlag := oqfIsQualified;
          if (AViewInfo.Item.Index = ColIsQualified.Index) then
            ACanvas.Font.Style := [fsBold];
        end
        else
          QualFlag := oqfIsNotQualified;

        ACanvas.Brush.Color := GetValidColor(QualFlag);
      end;

      {Is it close to expiring? Must follow locator Qualified}
      if not VarIsNull(AViewInfo.GridRecord.Values[ColExpiredDate.Index]) and
       ((AViewInfo.Item.Index = ColExpiredDate.Index) or
         (AViewInfo.Item.Index = ColIsQualified.Index)) then begin
        ExpDate := TDateTime(AViewInfo.GridRecord.Values[ColExpiredDate.Index]);
        if (Now < ExpDate) and ((Now + 2) >= ExpDate) and (QualFlag = oqfIsQualified) then begin
          ExpFlag := oqfAboutToExpire;
          ACanvas.Brush.Color := GetValidColor(ExpFlag);
          ACanvas.Font.Color := clMaroon;
          ACanvas.Font.Style := [fsBold];
        end;
      end;
    end;
end;

end.
