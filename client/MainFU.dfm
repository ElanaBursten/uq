object MainForm: TMainForm
  Left = 216
  Top = 97
  Caption = '---'
  ClientHeight = 605
  ClientWidth = 1048
  Color = clBtnFace
  Constraints.MinHeight = 430
  Constraints.MinWidth = 640
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 148
    Height = 586
    Align = alLeft
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 73
    Caption = 'LeftToolBar'
    Color = clBtnFace
    EdgeBorders = [ebTop, ebRight]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Images = SharedImagesDM.Images
    ParentColor = False
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object TicketManButton: TToolButton
      Left = 0
      Top = 0
      Action = WorkManagementAction
      Caption = 'Manage Work'
    end
    object MyWorkButton: TToolButton
      Left = 73
      Top = 0
      Action = MyWorkAction
      Wrap = True
    end
    object NewTicketButton: TToolButton
      Left = 0
      Top = 36
      Action = NewTicketAction
    end
    object TicketDetailButton: TToolButton
      Left = 73
      Top = 36
      Action = TicketDetail
      Wrap = True
    end
    object FindTicketsButton: TToolButton
      Left = 0
      Top = 72
      Action = FindTickets
      Caption = 'Find Tkt'
    end
    object FindWOButton: TToolButton
      Left = 73
      Top = 72
      Action = FindWorkOrders
      Wrap = True
    end
    object SyncButton: TToolButton
      Left = 0
      Top = 108
      Action = Synchronize
    end
    object OptionsButton: TToolButton
      Left = 73
      Top = 108
      Action = SystemConfig
      Wrap = True
    end
    object DamageButton: TToolButton
      Left = 0
      Top = 144
      Action = Damage
    end
    object ReportsButton: TToolButton
      Left = 73
      Top = 144
      Action = Reports
      Wrap = True
    end
    object TimesheetButton: TToolButton
      Left = 0
      Top = 180
      Action = Timesheet
    end
    object ApprovalButton: TToolButton
      Left = 73
      Top = 180
      Action = Approval
      Wrap = True
    end
    object AssetsButton: TToolButton
      Left = 0
      Top = 216
      Action = Assets
    end
    object BillingButton: TToolButton
      Left = 73
      Top = 216
      Action = Billing
      Wrap = True
    end
    object MessagesButton: TToolButton
      Left = 0
      Top = 252
      Action = MessageAction
    end
    object AreaEditorButton: TToolButton
      Left = 73
      Top = 252
      Action = AreaEditorAction
      Wrap = True
    end
    object MessageAckButton: TToolButton
      Left = 0
      Top = 288
      Action = MessageAckAction
      ImageIndex = 26
    end
    object SafetyButton: TToolButton
      Left = 73
      Top = 288
      Action = SafetySiteAction
      Wrap = True
    end
    object UtilisafeButton: TToolButton
      Left = 0
      Top = 324
      Action = UtiliSafeAction
    end
    object OQButton: TToolButton
      Left = 73
      Top = 324
      Action = ViewOQList
      Wrap = True
    end
    object tbDPU: TToolButton
      Left = 0
      Top = 360
      Caption = 'DPU'
      ImageIndex = 53
      OnClick = tbDPUClick
    end
    object MarsButton: TToolButton
      Left = 73
      Top = 360
      Action = ViewMarsList
    end
  end
  object SBar: TStatusBar
    Left = 0
    Top = 586
    Width = 1048
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Panels = <
      item
        Width = 150
      end
      item
        Width = 200
      end
      item
        Width = 120
      end
      item
        Width = 300
      end
      item
        Width = 40
      end
      item
        Width = 90
      end>
    UseSystemFont = False
    OnDrawPanel = SBarDrawPanel
  end
  object RightPanel: TPanel
    Left = 148
    Top = 0
    Width = 900
    Height = 586
    Align = alClient
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 1
    object ContentLabelPanel: TPanel
      Left = 0
      Top = 0
      Width = 900
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '   ---'
      Color = clGray
      FullRepaint = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        900
        25)
      object HelpSpeedButton: TSpeedButton
        Left = -1216
        Top = 2
        Width = 23
        Height = 22
        Anchors = [akTop, akRight]
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          08000000000000010000C30E0000C30E00000001000000010000292929005252
          5200849CA5000873B500298CBD004A9CC60039A5D60084BDD60042B5F7005AC6
          F7008CD6F7009CDEF700FF00FF0073E7FF00B5EFFF00DEEFFF0094F7FF00CEF7
          FF00E7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000C0C0C0C0C0C
          0C0C0C0C0C040C0C0C0C0C0C0C0C0C0C0C0C0C0C04070C0C0C0C0C0C0C0C0C0C
          0C0C0C0312070C0C0C0C0C0C0406060606060412120F060C0C0C0C0412121212
          12121212121212060C0C0C041112111211000111121112060C0C0C0411111111
          11111111111111060C0C0C051111111111000111111111060C0C0C0611111111
          11010002111111060C0C0C060E0E0E0E0E0E0100010E0E060C0C0C080E0E0E00
          010E0E00000E0E080C0C0C080E0E0E00000E0200000E0E080C0C0C090E0E0E02
          00000000020E0E080C0C0C091010101010101010101010080C0C0C0A10101010
          10101010101010080C0C0C0C0B0D0D0D0D0D0D0D0D0D080C0C0C}
        Visible = False
        OnClick = HelpSpeedButtonClick
      end
      object Label1: TLabel
        Left = 210
        Top = 5
        Width = 50
        Height = 13
        Caption = 'Last Sync:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Tag = 546
        Left = 440
        Top = 5
        Width = 57
        Height = 13
        Caption = 'Emergency:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Tag = 546
        Left = 537
        Top = 5
        Width = 25
        Height = 13
        Caption = 'New:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Tag = 546
        Left = 600
        Top = 5
        Width = 44
        Height = 13
        Caption = 'Ongoing:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EmergencyLabel: TLabel
        Tag = 546
        Left = 504
        Top = 2
        Width = 6
        Height = 19
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object NewLabel: TLabel
        Tag = 546
        Left = 569
        Top = 2
        Width = 6
        Height = 19
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object OngoingLabel: TLabel
        Tag = 546
        Left = 650
        Top = 2
        Width = 6
        Height = 19
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LastSyncLabel: TLabel
        Left = 268
        Top = 5
        Width = 16
        Height = 13
        Caption = '----'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 362
        Top = 5
        Width = 54
        Height = 13
        Caption = 'My Tickets:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object MessageIcon: TImage
        Left = 870
        Top = 3
        Width = 16
        Height = 17
        Picture.Data = {
          07544269746D617036040000424D360400000000000036000000280000001000
          000010000000010020000000000000040000F00A0000F00A0000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000BD8181F4CC9393EEC78E8ED7BF8989A5A97A7A879A6E6E69936C
          6C4C8C6E6E2E8B6C6C1100000000000000000000000000000000000000000000
          000000000000C99898FDDBABACFFFFE2E2FFFFDADAFFFFD9D9FFFFD2D2FFECB7
          B7FFDCABABFFD1A3A4FECFA2A2F5C79B9CE2BA9394CE9F8686997A7272790000
          000000000000CD9B9BFDE6C5C7FFE4BCBDFFFFECECFFFFE4E4FFFFE0E0FFFFE1
          E1FFFFDEDEFFFFDCDCFFFFDCDCFFFFDBDBFFF3CFCFFFD5A4A4FFA48585FD0000
          000000000000CE9A9AFDFFF8F8FFE8B2B2FFF3E0E0FFFFFEFEFFFFF5F5FFFFF2
          F2FFFFEFEFFFFFEBEBFFFFEBEBFFFFEFEFFFD8C0C0FFD8A8A8FFBF9898FD0000
          000000000000D09B9BFDFFFDFDFFFFE9E9FFE3A7A7FFF6EFEFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFDBCACAFFD3A9A9FFFFE6E6FFBF9898FD0000
          000000000000D29C9CFDFFFFFFFFFFF4F4FFFFEFEFFFE9B2B1FFF7E3E1FFFFFF
          FFFFFFFFFFFFFFFFFFFFD6BBBAFFE0C3C4FFFFF1F1FFFFF9F9FFC19C9CFD0000
          000000000000D49B9BFDFFFFFFFFFFFFFFFFFFF7F5FFDAB4B7FFB2B2C5FFD9B0
          B3FFE3B9B8FFBBAAB4FFC8A4A9FFFEF0EFFFFFFFFFFFFFFFFFFFC39E9EFD0000
          000000000000D69F9FFDFFFFFFFFF0CACBFFCDB8C0FFB8EDFFFFBBEFFFFFB9EE
          FFFFBAECFFFFBBEFFFFFBAF0FFFFBDB9C6FFD5B0B0FFFFFFFFFFC8A2A1FD0000
          000000000000E19F9FFDE1A8A8FFB9C5D4FFD6F8FFFFD6F1FFFFD6F0FFFFD6F0
          FFFFD6F0FFFFD6F0FFFFD6F1FFFFD6F6FFFFCDEDFDFFB78F90FFC59696FA0000
          000000000000E98F8FC2B99D9CFFC0CBD6FFF6FFFFFFF3FEFFFFF3FFFFFFF3FF
          FFFFF3FFFFFFF3FFFFFFF3FFFFFFF5FFFFFFF0FFFFFFC1A2A2FFC18F8FA90000
          000000000000FDB4B408DD9191A6B7BDC7FFBEC3CEFFBCC0CCFFBBBECBFFBCC0
          CBFFBBBFCAFFBDC2CAFFBDC0C9FFBFC2CBFFC4C8CDFFBC9C9CA9000000010000
          000000000000000000008A979702F49C9C70DFA3A3F9EAC1C1FFF3C4C4FFF9C8
          C8FFFFCCCCFFFFCECEFFFFD2D1FFF1B8B8FDD799999400000000000000000000
          000000000000000000000000000000000000FFA4A433E9A0A0D2EFC1C1FFF4CA
          CAFFFCCECEFFFFD3D3FFE4ABABF1D79797710000000000000000000000000000
          00000000000000000000000000000000000000000000CD888804E08D8D66DD9E
          9ED5DFA3A3ECD39292AAD385851A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
        Transparent = True
        OnClick = MessageIconClick
      end
      object WarningLabel: TLabel
        Left = 139
        Top = 5
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'WarningLabel'
        Font.Charset = ANSI_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DamagesLabel: TLabel
        Tag = 546
        Left = 682
        Top = 5
        Width = 48
        Height = 13
        Caption = 'Damages:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object DamagesCountLabel: TLabel
        Tag = 546
        Left = 737
        Top = 2
        Width = 6
        Height = 19
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object WorkOrdersLabel: TLabel
        Tag = 546
        Left = 773
        Top = 5
        Width = 65
        Height = 13
        Caption = 'Work Orders:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object WorkOrdersCountLabel: TLabel
        Tag = 546
        Left = 842
        Top = 2
        Width = 6
        Height = 19
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
    end
    object UploadStatusPanel: TPanel
      Left = 0
      Top = 25
      Width = 900
      Height = 22
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '    Attachment Status:'
      Color = clMedGray
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      Visible = False
      object Label7: TLabel
        Tag = 546
        Left = 112
        Top = 4
        Width = 83
        Height = 13
        Caption = 'Pending Uploads:'
      end
      object UploadFileCountLabel: TLabel
        Tag = 546
        Left = 202
        Top = 2
        Width = 5
        Height = 16
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Tag = 546
        Left = 238
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Size:'
      end
      object UploadFileSizeLabel: TLabel
        Tag = 546
        Left = 270
        Top = 2
        Width = 5
        Height = 16
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Tag = 546
        Left = 362
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Activity:'
      end
      object UploadStatusMessageLabel: TLabel
        Tag = 546
        Left = 404
        Top = 4
        Width = 75
        Height = 13
        Caption = 'Waiting to start'
        OnDblClick = UploadStatusMessageLabelDblClick
      end
    end
    object AutoSyncStatusPanel: TPanel
      Left = 0
      Top = 47
      Width = 900
      Height = 22
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '    Auto-sync Status:'
      Color = clMedGray
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
      Visible = False
      DesignSize = (
        900
        22)
      object AutoSyncStatusLabel: TLabel
        Tag = 546
        Left = 106
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Waiting'
        OnDblClick = UploadStatusMessageLabelDblClick
      end
      object ElapsedIdleTimeLabel: TLabel
        Tag = 546
        Left = 853
        Top = 4
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Elapsed'
        Font.Charset = ANSI_CHARSET
        Font.Color = clYellow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnDblClick = UploadStatusMessageLabelDblClick
      end
    end
    object ContentPanel: TPanel
      Left = 0
      Top = 69
      Width = 900
      Height = 517
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      FullRepaint = False
      TabOrder = 0
      object ProgressBar: TcxProgressBar
        Left = 8
        Top = 489
        Constraints.MaxHeight = 18
        Constraints.MinHeight = 16
        ParentColor = False
        Properties.ShowTextStyle = cxtsText
        Properties.Text = 'ProgressBar'
        Style.BorderStyle = ebsNone
        TabOrder = 0
        Width = 121
      end
    end
  end
  object FullWindowPanel: TPanel
    Left = 199
    Top = 96
    Width = 322
    Height = 169
    BevelOuter = bvNone
    Caption = 'Full Window Panel'
    TabOrder = 3
    Visible = False
    inline TicketWorkFullFrame: TTicketWorkFrame
      Left = 0
      Top = 0
      Width = 322
      Height = 169
      Align = alClient
      TabOrder = 0
      inherited ButtonPanel: TPanel
        Top = 128
        Width = 322
        DesignSize = (
          322
          41)
        inherited CloseButton: TButton
          Font.Height = -13
        end
      end
      inherited WorkToDoBrowser: TWebBrowser
        Width = 322
        Height = 128
        ControlData = {
          4C000000482100003B0D00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  object MainMenu: TMainMenu
    Images = SharedImagesDM.Images
    Left = 104
    Top = 48
    object File1: TMenuItem
      Caption = 'File'
      object Synchronize2: TMenuItem
        Action = Synchronize
      end
      object SetWindowSizeSmall: TMenuItem
        Action = SetSmallScreenAction
      end
      object SendErrorReportTesting1: TMenuItem
        Action = SendErrorReportAction
      end
      object ApplyXMLChanges1: TMenuItem
        Action = ApplyXmlFileAction
      end
      object Messages1: TMenuItem
        Action = MessageAckAction
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object FileExitItem: TMenuItem
        Action = FileExit1
      end
    end
    object Edit1: TMenuItem
      Caption = '&Edit'
      Hint = 'Edit commands'
      object CutItem: TMenuItem
        Action = EditCut1
      end
      object CopyItem: TMenuItem
        Action = EditCopy1
      end
      object PasteItem: TMenuItem
        Action = EditPaste1
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      Hint = 'Help topics'
      object LetMeIn1: TMenuItem
        Caption = 'Let Me In'
        OnClick = LetMeIn1Click
      end
      object HelpAboutItem: TMenuItem
        Action = HelpAbout1
      end
    end
  end
  object Actions: TActionList
    Images = SharedImagesDM.Images
    Left = 104
    Top = 344
    object MyWorkAction: TAction
      Category = 'Forms'
      Caption = 'My Work'
      ImageIndex = 6
      Visible = False
      OnExecute = MyWorkActionExecute
    end
    object TicketDetail: TAction
      Category = 'Forms'
      Caption = 'Ticket Detail'
      ImageIndex = 3
      Visible = False
      OnExecute = TicketDetailExecute
    end
    object SearchTickets: TAction
      Category = 'Forms'
      Caption = 'Old Tickets'
      ImageIndex = 7
      Visible = False
      OnExecute = SearchTicketsExecute
    end
    object Reports: TAction
      Category = 'Forms'
      Caption = 'Reports'
      ImageIndex = 8
      Visible = False
      OnExecute = ReportsExecute
    end
    object SystemConfig: TAction
      Category = 'Forms'
      Caption = 'Options'
      ImageIndex = 10
      OnExecute = SystemConfigExecute
    end
    object FileExit1: TAction
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Exit application'
      OnExecute = FileExit1Execute
    end
    object EditCut1: TEditCut
      Category = 'Edit'
      Caption = 'Cu&t'
      Hint = 'Cut|Cuts the selection and puts it on the Clipboard'
      ImageIndex = 0
      ShortCut = 16472
      OnExecute = EditCut1Execute
    end
    object EditCopy1: TEditCopy
      Category = 'Edit'
      Caption = '&Copy'
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 1
      ShortCut = 16451
      OnExecute = EditCopy1Execute
    end
    object EditPaste1: TEditPaste
      Category = 'Edit'
      Caption = '&Paste'
      Hint = 'Paste|Inserts Clipboard contents'
      ImageIndex = 2
      ShortCut = 16470
      OnExecute = EditPaste1Execute
    end
    object HelpAbout1: TAction
      Category = 'Help'
      Caption = '&About...'
      Hint = 
        'About|Displays program information, version number, and copyrigh' +
        't'
      OnExecute = HelpAbout1Execute
    end
    object Damage: TAction
      Category = 'Forms'
      Caption = 'Damage'
      ImageIndex = 12
      Visible = False
      OnExecute = DamageExecute
    end
    object WorkManagementAction: TAction
      Category = 'Forms'
      Caption = 'Work Man.'
      ImageIndex = 13
      Visible = False
      OnExecute = WorkManagementActionExecute
    end
    object Synchronize: TAction
      Category = 'Forms'
      Caption = 'Sync'
      ImageIndex = 11
      ShortCut = 16467
      Visible = False
      OnExecute = SynchronizeExecute
    end
    object FindTickets: TAction
      Category = 'Forms'
      Caption = 'Find'
      ImageIndex = 7
      Visible = False
      OnExecute = FindTicketsExecute
    end
    object SetSmallScreenAction: TAction
      Caption = 'Test Small Screen'
      Visible = False
      OnExecute = SetSmallScreenActionExecute
    end
    object FindWorkOrders: TAction
      Category = 'Forms'
      Caption = 'Find WO'
      ImageIndex = 7
      Visible = False
      OnExecute = FindWorkOrdersExecute
    end
    object Assets: TAction
      Category = 'Forms'
      Caption = 'Assets'
      Enabled = False
      ImageIndex = 16
      Visible = False
      OnExecute = AssetsExecute
    end
    object SendErrorReportAction: TAction
      Caption = 'Send Error Report (Testing)'
      Visible = False
      OnExecute = SendErrorReportActionExecute
    end
    object ApplyXmlFileAction: TAction
      Caption = 'Apply XML Changes...'
      OnExecute = ApplyXmlFileActionExecute
    end
    object NewTicketAction: TAction
      Category = 'Forms'
      Caption = 'New Ticket'
      ImageIndex = 22
      Visible = False
      OnExecute = NewTicketActionExecute
    end
    object Timesheet: TAction
      Category = 'Forms'
      Caption = 'Timesheet'
      ImageIndex = 14
      Visible = False
      OnExecute = TimesheetExecute
    end
    object Approval: TAction
      Category = 'Forms'
      Caption = 'Approval'
      ImageIndex = 17
      Visible = False
      OnExecute = ApprovalExecute
    end
    object Billing: TAction
      Category = 'Forms'
      Caption = 'Billing'
      ImageIndex = 18
      Visible = False
      OnExecute = BillingExecute
    end
    object MessageAckAction: TAction
      Caption = 'Message Ack'
      OnExecute = MessageAckActionExecute
    end
    object HoursMessageAckAction: TAction
      Caption = 'Hours Message Ack'
      OnExecute = HoursMessageAckActionExecute
    end
    object MessageAction: TAction
      Category = 'Forms'
      Caption = 'Messages'
      ImageIndex = 23
      Visible = False
      OnExecute = MessageActionExecute
    end
    object AreaEditorAction: TAction
      Category = 'Forms'
      Caption = 'Area Editor'
      ImageIndex = 24
      Visible = False
      OnExecute = AreaEditorActionExecute
    end
    object SafetySiteAction: TAction
      Caption = 'Safety'
      Hint = '32'
      ImageIndex = 37
      OnExecute = SafetySiteActionExecute
    end
    object UtiliSafeAction: TAction
      Caption = 'UtiliSafe'
      Enabled = False
      ImageIndex = 37
      OnExecute = UtiliSafeActionExecute
    end
    object ViewOQList: TAction
      Caption = 'OQ List'
      ImageIndex = 47
      OnExecute = ViewOQListExecute
    end
    object ViewPlusList: TAction
      Caption = 'Emp PLUS'
      ImageIndex = 47
      OnExecute = ViewPlusListExecute
    end
    object ViewMarsList: TAction
      Caption = 'MARS'
      ImageIndex = 20
      OnExecute = ViewMarsListExecute
    end
  end
  object SyncLabelTimer: TTimer
    Interval = 5000
    OnTimer = SyncLabelTimerTimer
    Left = 224
    Top = 392
  end
  object ChooseXmlFileDialog: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'XML Update Files (*.xml)|*.xml'
    InitialDir = 'c:\'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 104
    Top = 208
  end
  object CheckUsageTimer: TTimer
    Interval = 30000
    OnTimer = CheckUsageTimerTimer
    Left = 104
    Top = 176
  end
  object ScreenshotTimer: TTimer
    Interval = 5000
    OnTimer = ScreenshotTimerTimer
    Left = 984
    Top = 56
  end
end
