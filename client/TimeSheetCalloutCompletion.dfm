object CalloutCompletionDlg: TCalloutCompletionDlg
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Call-out completion'
  ClientHeight = 181
  ClientWidth = 354
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblTop: TLabel
    Left = 5
    Top = 6
    Width = 317
    Height = 14
    Caption = 'You have an incomplete call-out on 7/4/2022 at 22:30PM'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblDATE: TLabel
    Left = 206
    Top = 76
    Width = 54
    Height = 13
    Caption = '7/4/2022'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblAdditionalCallout: TLabel
    Left = 64
    Top = 114
    Width = 271
    Height = 13
    AutoSize = False
    Caption = 'call-out: 7/4/2022 at 23:59'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lblAdditionalCallIn: TLabel
    Left = 64
    Top = 130
    Width = 118
    Height = 13
    Caption = 'call-in 7/5/2022 at 00:01'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lblAdditionalEntries: TLabel
    Left = 40
    Top = 95
    Width = 295
    Height = 13
    AutoSize = False
    Caption = 'These entries will also be added to the timesheet:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lblFinishedAt: TLabel
    Left = 39
    Top = 76
    Width = 61
    Height = 13
    Caption = 'Finished at'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object RBtnNextMorning: TRadioButton
    Left = 39
    Top = 51
    Width = 296
    Height = 17
    Caption = 'Finished the next morning on 7/5/2022'
    TabOrder = 0
    OnClick = RBtnModeClick
  end
  object RBtnBeforeMidnight: TRadioButton
    Left = 39
    Top = 30
    Width = 296
    Height = 17
    Caption = 'Finished before midnight on 7/4/2022'
    TabOrder = 1
    OnClick = RBtnModeClick
  end
  object CalloutTimeEdit: TcxTimeEdit
    Left = 106
    Top = 72
    EditValue = 0d
    Enabled = False
    ParentFont = False
    Properties.Circular = True
    Properties.ImmediatePost = True
    Properties.TimeFormat = tfHourMin
    Properties.ValidateOnEnter = False
    Properties.ValidationOptions = []
    Properties.OnEditValueChanged = CalloutTimeEditPropertiesEditValueChanged
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -12
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 2
    Width = 88
  end
  object btnSave: TButton
    Left = 52
    Top = 149
    Width = 113
    Height = 25
    Caption = 'Save to Timesheet'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = btnSaveClick
  end
  object btnCancel: TButton
    Left = 171
    Top = 149
    Width = 113
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
