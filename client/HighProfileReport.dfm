inherited HighProfileReportForm: THighProfileReportForm
  Left = 345
  Top = 195
  Caption = 'High Profile Report'
  ClientHeight = 405
  ClientWidth = 596
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 311
    Top = 8
    Width = 99
    Height = 13
    Caption = 'High profile reasons:'
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 8
    Width = 300
    Height = 84
    Caption = ' Date Range '
    TabOrder = 0
    inline RangeSelect: TOdRangeSelectFrame
      Left = 2
      Top = 14
      Width = 298
      Height = 63
      TabOrder = 0
      inherited FromLabel: TLabel
        Left = 19
        Top = 11
      end
      inherited ToLabel: TLabel
        Left = 173
        Top = 11
      end
      inherited DatesLabel: TLabel
        Left = 19
        Top = 37
      end
      inherited DatesComboBox: TComboBox
        Left = 87
        Width = 185
      end
      inherited FromDateEdit: TcxDateEdit
        Left = 87
        Width = 77
      end
      inherited ToDateEdit: TcxDateEdit
        Left = 195
        Width = 77
      end
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 0
    Top = 161
    Width = 300
    Height = 72
    Caption = ' Office '
    TabOrder = 2
    object ManagersComboBox: TComboBox
      Left = 89
      Top = 13
      Width = 185
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 1
      OnEnter = ManagersComboBoxEnter
    end
    object ManagerRadio: TRadioButton
      Left = 21
      Top = 15
      Width = 61
      Height = 17
      Caption = 'Manager'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object OfficeRadio: TRadioButton
      Left = 21
      Top = 43
      Width = 60
      Height = 17
      Caption = 'Office'
      TabOrder = 2
    end
    object OfficesComboBox: TComboBox
      Left = 89
      Top = 41
      Width = 185
      Height = 21
      Style = csDropDownList
      DropDownCount = 18
      ItemHeight = 13
      TabOrder = 3
      OnEnter = OfficesComboBoxEnter
    end
  end
  object TicketsGroup: TGroupBox [3]
    Left = 0
    Top = 99
    Width = 301
    Height = 57
    Caption = 'Tickets'
    TabOrder = 1
    object TicketsLabel: TLabel
      Left = 21
      Top = 23
      Width = 37
      Height = 13
      Caption = 'Tickets:'
    end
    object TicketsComboBox: TComboBox
      Left = 89
      Top = 20
      Width = 185
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object HighProfileReasonsCheckListBox: TCheckListBox [4]
    Left = 311
    Top = 22
    Width = 155
    Height = 183
    ItemHeight = 13
    TabOrder = 3
  end
  object SelectAllButton: TButton [5]
    Left = 311
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 4
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [6]
    Left = 391
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 5
    OnClick = ClearAllButtonClick
  end
  object CallCenterClientsBox: TGroupBox [7]
    Left = 0
    Top = 244
    Width = 547
    Height = 160
    Anchors = [akLeft, akTop, akBottom]
    Caption = ' Limit To Specific Call Centers / Clients '
    ParentShowHint = False
    ShowHint = False
    TabOrder = 6
    DesignSize = (
      547
      160)
    object Label2: TLabel
      Left = 8
      Top = 17
      Width = 105
      Height = 13
      AutoSize = False
      Caption = 'Call Centers:'
      WordWrap = True
    end
    object Label3: TLabel
      Left = 287
      Top = 17
      Width = 36
      Height = 13
      Caption = 'Clients:'
    end
    object CallCenterList: TCheckListBox
      Left = 8
      Top = 32
      Width = 275
      Height = 124
      Anchors = [akLeft, akTop, akBottom]
      ItemHeight = 13
      TabOrder = 0
      OnClick = CallCenterListClick
    end
    object ClientList: TCheckListBox
      Left = 287
      Top = 32
      Width = 183
      Height = 124
      Anchors = [akLeft, akTop, akBottom]
      Columns = 2
      ItemHeight = 13
      TabOrder = 1
    end
    object SelectAllClientsButton: TButton
      Left = 474
      Top = 32
      Width = 65
      Height = 25
      Caption = 'Select All'
      TabOrder = 2
      OnClick = SelectAllClientsButtonClick
    end
    object ClearAllClientsButton: TButton
      Left = 474
      Top = 64
      Width = 65
      Height = 25
      Caption = 'Clear All'
      TabOrder = 3
      OnClick = ClearAllClientsButtonClick
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 65535
  end
end
