unit TimeClockDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseTimesheetDay, ActnList, DB, DBISAMTb, StdCtrls,
  DBCtrls, ExtCtrls, QMConst, TimeClockEntry, TimeClockDB, TimeClockImport,
  TimesclockOverride,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, cxDBEdit,
  cxDropDownEdit, cxMaskEdit, cxStyles, 
  cxDataStorage, cxDBData, cxTimeEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxNavigator, cxCustomData, cxFilter, cxData, dxSkinsCore;

type
  TTimeClockDayFrame = class(TBaseTimesheetDayFrame)
    TimeClockSource: TDataSource;
    ClockDetailData: TDBISAMQuery;
    Bevel: TBevel;
    TimeClockStyleRepo: TcxStyleRepository;
    TimeClockGridHeaderStyle: TcxStyle;
    TimeClockGrid: TcxGrid;
    TimeClockGridDBTableView: TcxGridDBTableView;
    ColActivity: TcxGridDBColumn;
    ColActivityTime: TcxGridDBColumn;
    TimeClockGridLevel: TcxGridLevel;
    TimeClockGridHeaderBoldStyle: TcxStyle;
    TimeClockGridContentStyle: TcxStyle;
    TimeClockGridContentROStyle: TcxStyle;
    procedure DayActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ClockDetailDataBeforePost(DataSet: TDataSet);
    procedure ClockDetailDataNewRecord(DataSet: TDataSet);
    procedure ClockDetailDataBeforeInsert(DataSet: TDataSet);
    procedure ClockDetailDataBeforeDelete(DataSet: TDataSet);
    procedure TimeClockGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TimeClockGridDBTableViewEditing(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      var AAllow: Boolean);
  private
    FAnyPendingChanges: Boolean;
    FLastUsedActivity: string;
    FDuringRedisplay: Boolean;
    FAddingFromTimesheet: Boolean;
    FAddedWorkStop: Boolean;
    FWorkTimesUsed: Integer;
    FCallTimesUsed: Integer;
    fTimeOverrideReason: string;
    frmTimeSheetOverride: TfrmTimeSheetOverride;
    procedure LoadActivityTypeCombo(const LastActivityType: string);
    procedure AddClockDetailsToTranslator;
    function GetThisActivityTime(const ActivityType: string): TDateTime;
    procedure NewClockActivity;
    procedure DropTimeClockDetailTable;
    function GetLastActivityUsed: string;
    function IsGridReadOnly: Boolean;
    procedure WorkStartOverride(ThisActType: string; ThisActTime,
      oActTime: TDateTime);

  protected
    TimeClockTranslator: TTimeClockTranslator;
    DataSaver: TDbDataSaver;
    RowWriter: TGridRowWriter;
    procedure DetermineReadOnly(ForceReadOnly: Boolean); override;
    procedure RefreshScreenControls; override;
    procedure SetDayLabel; override;
    procedure BeforeErrorCheck; override;
    procedure PrepareTimeClockDetailTable;
    function ImportTimeClockActivities: Boolean;
    procedure RefreshTimeClockGrid;
    function ConfirmContinueWithSubmit: Boolean; override;
    function TimeClockTableName: string; virtual;
  public

    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Display(EmpID: Integer; WorkDate: TDateTime; Mode: TTimesheetEditMode); override;
    procedure SaveChanges; override;
    procedure CancelChanges; override;
    procedure FocusIfPossible; override;
    function Modified: Boolean; override;
    function IsLastActivityWorkStop: Boolean;
    function AutoImport:Boolean;
    function HasStartedWork: Boolean;  //QM-100 EB UtiliSafe
    property NeedToShutdown;
    property TimeOverrideReason : string read fTimeOverrideReason  write fTimeOverrideReason;

  end;

var
  newTime:TDatetime;
  overRideReason :string;

implementation

uses DMu, OdDBISAMUtils, OdDbUtils, OdMiscUtils, OdIsoDates, DateUtils, OdExceptions,
  OdAckMessageDlg, OdCxUtils, LocalPermissionsDMu, LocalEmployeeDmu;

{$R *.dfm}

{ TTimeClockDayFrame }

const
  TimeClockDayHeight = 600;

constructor TTimeClockDayFrame.Create(Owner: TComponent);
begin
  inherited;
  ClockDetailData.DatabaseName := TimesheetData.DatabaseName;
  ClockDetailData.SessionName := TimesheetData.SessionName;
  RowWriter := TGridRowWriter.Create(ClockDetailData);
  DataSaver := TDbDataSaver.Create(TimesheetData, ClockDetailData);
  TimeClockTranslator := TTimeClockTranslator.Create(DataSaver, RowWriter);
  SubmitHint.Visible := False;
  Height := TimeClockDayHeight;
  BelowCalloutPanel.Top := TimeClockGrid.Top + TimeClockGrid.Height;
  if (ColActivityTime.PropertiesClass = TcxTimeEditProperties) then
    TcxTimeEditProperties(ColActivityTime.Properties).TimeFormat := tfHourMin;
  FDuringRedisplay := False;
  FAnyPendingChanges := False;
  FAddedWorkStop := False;

end;

destructor TTimeClockDayFrame.Destroy;
begin
  FreeAndNil(TimeClockTranslator);
  DropTimeClockDetailTable;
  inherited;
end;

procedure TTimeClockDayFrame.LoadActivityTypeCombo(const LastActivityType: string);
var
  ActivityItems: TStrings;
begin
  ActivityItems := CxComboBoxItems(ColActivity);
  ActivityItems.Clear;
  if FMode in ([teManager, teITOverride]) then begin
    ActivityItems.Add(CalloutStart);
    ActivityItems.Add(CalloutStop);
    ActivityItems.Add(LunchStart);
    ActivityItems.Add(LunchStop);
    ActivityItems.Add(PersonalStart);
    ActivityItems.Add(PersonalStop);
    ActivityItems.Add(WorkStart);
    ActivityItems.Add(WorkStop);
    Exit;
  end;

  // load the valid activity types based on the last used type
  if IsEmpty(LastActivityType) or (LastActivityType = CalloutStop) then begin
    ActivityItems.Add(WorkStart);
    ActivityItems.Add(CalloutStart);
  end else if StringInArray(LastActivityType, [WorkStart, LunchStop, PersonalStop]) then begin
    ActivityItems.Add(LunchStart);
    ActivityItems.Add(PersonalStart);
    ActivityItems.Add(WorkStop);
  end else if LastActivityType = CalloutStart then
    ActivityItems.Add(CalloutStop)
  else if LastActivityType = LunchStart then
    ActivityItems.Add(LunchStop)
  else if LastActivityType = PersonalStart then begin
    ActivityItems.Add(PersonalStop);
    ActivityItems.Add(CalloutStart);
  end else if LastActivityType = WorkStop then
    ActivityItems.Add(CalloutStart);
end;

procedure TTimeClockDayFrame.Display(EmpID: Integer; WorkDate: TDateTime;
  Mode: TTimesheetEditMode);
begin
  inherited; //loads data from timesheet_entry into common controls
  NeedToShutdown := IsLastActivityWorkStop and (not PermissionsDM.CanI(RightWorkAfterWorkClockOut));
  Bevel.Visible := DayOfTheWeek(WorkDate) <> DaySunday;
  ShowSubmitHint(False);
  PrepareTimeClockDetailTable;
  RefreshTimeClockGrid;
  PerDiem.enabled := False;//QM-614 EB PermissionsDM.CanI(RightTimesheetsPerDiemEntry);  // QMANTWO-789 sr
  FAnyPendingChanges := False;
  LoadActivityTypeCombo('');
  if (FMode = teNormal) and PermissionsDM.CanI(RightTimesheetsClockImport) then begin
    //Save any changes/make sure non-edit mode because auto import will not process files when editing.
    SaveChanges;
  end
       
  else if (not ReadOnly) and (FMode = teNormal) and DisplayingToday then   //EB ......................
    // Add a new row for the current time if WorkDate is today
    NewClockActivity;

  FDuringRedisplay := False;
end;

procedure TTimeClockDayFrame.RefreshTimeClockGrid;
const
  SelectClock = 'select * from "\Memory\%s" where emp_id=%d and work_date=%s order by activity_time';
begin
  TimeClockGridDBTableView.BeginUpdate;
//  ClockDetailData.DisableControls;
  FAddingFromTimesheet := True;
  try
    TimeClockSource.DataSet := ClockDetailData;
    ClockDetailData.SQL.Text := Format(SelectClock, [TimeClockTableName, FEmpID, DateToDBISAMDate(FWorkDate)]);
    ClockDetailData.RequestLive := True;
    ClockDetailData.Open;
    SetDateFieldDisplayFormats(ClockDetailData, QTimeFormat);
    if FWorkDate = Today then
      ColActivityTime.Styles.Header := TimeClockGridHeaderBoldStyle
    else
      ColActivityTime.Styles.Header := TimeClockGridHeaderStyle;

    // converts the timesheet_entry.work_* & callout_* time spans into ClockDetailData rows
    TimeClockTranslator.Initialize(FEmpID, FWorkDate, IsSubmitted);
    if Timesheet.Active then
      TimeClockTranslator.AddTimeEntryTimes;
    TimeClockTranslator.ExportTabularForm;
    FWorkTimesUsed := TimeClockTranslator.WorkTimesUsed;
    FCallTimesUsed := TimeClockTranslator.CallTimesUsed;
//    if (FWorkDate = Today) and (ClockDetailData.FieldByName('ColActivity').AsDateTime > Today) then
//      fWorkStartedToday := True;  //QM-100 EB UtiliSafe
  finally
    FAddingFromTimesheet := False;
//    ClockDetailData.EnableControls;
    TimeClockGridDBTableView.EndUpdate;
  end;
end;

function TTimeClockDayFrame.GetThisActivityTime(const ActivityType: string): TDateTime;

  function UseFirstAvailableTime(Times: array of TDateTime): TDateTime;
  var
    i: Integer;
    LastUsedTime: TDateTime;
  begin
    Result := Time;
    if ClockDetailData.IsEmpty then
      LastUsedTime := 0
    else begin
      ClockDetailData.Last;
      LastUsedTime := TimeOf(ClockDetailData.FieldByName('activity_time').AsDateTime);
    end;

    for i := Low(Times) to High(Times) do begin
      if (not IsToday(Times[I])) or (Times[I] <= 0) then
        Continue;

      if not ClockDetailData.Locate('activity_time_key', FormatDateTime('hh:mm:ss', Times[I]), []) then begin
        Result := TimeOf(Times[I]);
        Break;
      end;
    end;
    if Result <= LastUsedTime then
      Result := Time;
  end;

var
  FirstBootTime: TDateTime;
begin
  if StringInArray(ActivityType, [WorkStart, CalloutStart]) then begin
    {If we have a PowerUpTime, use it. Otherwise, use Now}
    FirstBootTime := IsoStrToDateTimeDef(DM.UQState.PowerOnFirstDateTimeToday, Now);
    Result := UseFirstAvailableTime([FirstBootTime, Time]);
  end else
    Result := Time;
end;

function TTimeClockDayFrame.HasStartedWork : Boolean;  //QM-100 EB UtiliSafe
const
  TodaySQL = 'select entry_id, work_start1, work_start2 from timesheet_entry where (work_emp_id=%d) and (work_date=''%s'')';
var
  TempQuery: TDataSet;
  QryStr: string;
  WorkDayStr: string;
begin
  WorkDayStr := FormatDateTime('yyyy-mm-dd', Today);
  QryStr := Format(TodaySQL, [DM.EmpID, WorkDayStr]);
  TempQuery := DM.Engine.OpenQuery(QryStr);
  Result := False;
  try
    TempQuery.First;
    while not TempQuery.EOF do begin
      if TempQuery.FieldByName('work_start1').AsDatetime > 0  then
        fWorkStartedToday := True;
      TempQuery.Next;
    end;
  finally
   TempQuery.Close;
   FreeAndNil(TempQuery);
  end;
  Result := fWorkStartedToday;
end;

function TTimeClockDayFrame.GetLastActivityUsed: string;
begin
  Result := '';
  if not ClockDetailData.IsEmpty then begin
    ClockDetailData.Last;
    Result := ClockDetailData.FieldByName('activity_type').Value;
  end;
end;

procedure TTimeClockDayFrame.NewClockActivity;
var
  LastActType: string;
  ThisActType: string;
  ThisActTime: TDateTime;
  oActTime : TDateTime;    // QMANTWO-235
  response : string;      // QMANTWO-235
begin
  if FDuringRedisplay then
    Exit;

  LastActType := GetLastActivityUsed;
  LoadActivityTypeCombo(LastActType);
  if IsEmpty(LastActType) then
    ThisActType := WorkStart
  else
    ThisActType := '';
  ThisActTime := GetThisActivityTime(ThisActType);
  oActTime := ThisActTime;    // QMANTWO-235
  ClockDetailData.Insert;
  ClockDetailData.FieldByName('activity_type').AsString := ThisActType;
  ClockDetailData.FieldByName('activity_time').AsDateTime := ThisActTime;
  FAnyPendingChanges := True;
  if (FMode = teNormal) and (ThisActType = WorkStart) then begin
    response := ShowMessageAckDialog('Your work starting time was set to ' +        // QMANTWO-235
      FormatDateTime(QTimeFormat, ThisActTime), 'Yes, No', 'Set Work Start Time');
  //  fWorkStartedToday := True;  //QM-100 EB UtiliSafe
  end;
  if response = 'No' then                                            // QMANTWO-235
  begin                                                              // QMANTWO-235
    frmTimeSheetOverride:= TfrmTimeSheetOverride.Create(self);       // QMANTWO-235
    WorkStartOverride(ThisActType, ThisActTime,oActTime);            // QMANTWO-235
  end;                                                               // QMANTWO-235
end;

procedure TTimeClockDayFrame.WorkStartOverride(ThisActType:string; ThisActTime,oActTime:TDateTime);  // QMANTWO-235
var
  response : string;
begin
  try
    frmTimeSheetOverride.ShowModal;
    if frmTimeSheetOverride.ModalResult = mrOK then
    begin
      ThisActTime := TimeClockDay.newTime;                                    
      ClockDetailData.FieldByName('activity_type').AsString := ThisActType;
      ClockDetailData.FieldByName('activity_time').AsDateTime := ThisActTime;
      response := ShowMessageAckDialog('Your Work Start was changed from ' + FormatDateTime(QTimeFormat, oActTime)+' to '+    //QM-859 EB Correct Time format to match
      FormatDateTime(QTimeFormat, ThisActTime)+' Reason: '+trim(TimeClockDay.overRideReason), 'Yes, No', 'Override Work Start Time');
      if response = 'No' then
      begin
        ClockDetailData.Cancel;
        FAnyPendingChanges := false;
        FDuringRedisplay := false;
        NewClockActivity;  // let's try it again
      end
      else

      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeStartTimeOverride,              //ActivityType   TESTRTOVRD
                                  trim(TimeClockDay.overRideReason),          //Details        Reason Overriden
                                  Now,                                        //EventDateTime
                                  FormatDateTime('hh:mm ampm', oActTime)+' to '+FormatDateTime('hh:mm ampm', ThisActTime)  //ExtraDetails  new start time
                                  );
    end
    else
    begin
      ClockDetailData.Cancel;
      FAnyPendingChanges := false;
      FDuringRedisplay := false;
      NewClockActivity;  // let's try it again
    end;
  finally
    frmTimeSheetOverride.Release;
  end;
end;


function TTimeClockDayFrame.Modified: Boolean;
begin
  Result := inherited Modified or FAnyPendingChanges;
end;

procedure TTimeClockDayFrame.FocusIfPossible;
begin
  if ClockDetailData.Active and (not ReadOnly) then begin
    try
      if TimeClockGrid.CanFocus then begin
        TimeClockGrid.SetFocus;
        TimeClockGridDBTableView.Controller.FocusedColumn := ColActivity;
      end;
    except
    end;
  end;
end;

procedure TTimeClockDayFrame.AddClockDetailsToTranslator;
begin
  try
    PostDataSet(ClockDetailData);
  except
    CancelDataSet(ClockDetailData);
    raise;
  end;

  ClockDetailData.First;
  while not ClockDetailData.Eof do begin
    TimeClockTranslator.AddTimeClockTime(ClockDetailData.FieldByName('activity_time').AsDateTime,
      ClockDetailData.FieldByName('activity_type').AsString);
    ClockDetailData.Next;
  end;
end;

procedure TTimeClockDayFrame.SaveChanges;
begin
  FDuringRedisplay := True;
  if Modified then
    EditDataSet(Timesheet);
  inherited; // timesheet_entry data is saved in parent
  FAnyPendingChanges := False;
  if FAddedWorkStop then
    SubmitAction.Execute;
end;

procedure TTimeClockDayFrame.BeforeErrorCheck;
begin
  inherited;
  if Modified then begin
    ClockDetailData.DisableControls;
    try
      TimeClockTranslator.ClearTimeList;
      AddClockDetailsToTranslator;
      TimeClockTranslator.CompleteSave;
    finally
      ClockDetailData.EnableControls;
    end;
  end;
end;

procedure TTimeClockDayFrame.CancelChanges;
begin
  FDuringRedisplay := True;
  inherited; // undo anything in the timesheet_entry data
  if FAnyPendingChanges then begin
    CancelDataSet(ClockDetailData);
    RefreshTimeClockGrid;
  end;
  FAnyPendingChanges := False;
end;

procedure TTimeClockDayFrame.SetDayLabel;
begin
  ColActivityTime.Caption := FormatDateTime('ddd mmm dd', FWorkDate);
end;

procedure TTimeClockDayFrame.DayActionsUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  RefreshScreenControls;
end;

procedure TTimeClockDayFrame.RefreshScreenControls;
begin
  inherited;
  SubmitHint.Visible := False;
  TimeClockGridDBTableView.OptionsSelection.HideSelection := IsGridReadOnly;
  TimeClockGridDBTableView.OptionsSelection.HideFocusRect := IsGridReadOnly;
  TimeClockGridDBTableView.OptionsSelection.CellSelect := not IsGridReadOnly;
  if IsGridReadOnly then begin
    TimeClockGridDBTableView.Styles.Content := TimeClockGridContentROStyle;
    TimeClockGridDBTableView.Styles.Background := TimeClockGridContentROStyle;
  end else begin
    TimeClockGridDBTableView.Styles.Content := TimeClockGridContentStyle;
    TimeClockGridDBTableView.Styles.Background := TimeClockGridContentStyle;
  end;
  if (not IsGridReadOnly) and (FMode in [teManager, teITOverride]) then begin
    TimeClockGridDBTableView.OptionsData.Deleting := True;
    TimeClockGridDBTableView.OptionsData.Appending := True;
    TimeClockGridDBTableView.OptionsData.Inserting := True;
    ColActivityTime.Properties.ReadOnly := False;
  end else begin
    TimeClockGridDBTableView.OptionsData.Deleting := False;
    TimeClockGridDBTableView.OptionsData.Appending := False;
    TimeClockGridDBTableView.OptionsData.Inserting := False;
    ColActivityTime.Properties.ReadOnly := True;
  end;
  if IsGridReadOnly or (ClockDetailData.State = dsBrowse) then
    ColActivity.Options.ShowEditButtons := isebNever
  else if EditingDataSet(ClockDetailData) then
    ColActivity.Options.ShowEditButtons := isebDefault;
end;

function TTimeClockDayFrame.TimeClockTableName: string;
begin
  Result := 'time_clock_' + FormatDateTime('ddd', FWorkDate);
end;

procedure TTimeClockDayFrame.PrepareTimeClockDetailTable;
const
  // each day's activity is in its own memory table
  CreateTimeClockDetail = 'CREATE TABLE IF NOT EXISTS "\Memory\%s" (' +
    'activity_time_key CHAR(8) NOT NULL, ' +
    'emp_id INTEGER NOT NULL, ' +
    'work_date DATE NOT NULL, ' +
    'activity_time TIME NOT NULL, ' +
    'activity_type VARCHAR(15) NOT NULL, ' +
    'PRIMARY KEY (activity_time_key))';
begin
  ClockDetailData.SQL.Text := Format(CreateTimeClockDetail, [TimeClockTableName]);
  ClockDetailData.ExecSQL;
end;

procedure TTimeClockDayFrame.DropTimeClockDetailTable;
const
  DropTimeClockDetail = 'DROP TABLE IF EXISTS "\Memory\%s"';
begin
  ClockDetailData.Close;
  ClockDetailData.SQL.Text := Format(DropTimeClockDetail, [TimeClockTableName]);
  ClockDetailData.ExecSQL;
end;

procedure TTimeClockDayFrame.ClockDetailDataBeforePost(DataSet: TDataSet);
begin
  if IsEmpty(DataSet.FieldByName('activity_type').AsString) then
    raise EOdDataEntryError.Create('An activity type is required, or press ESC to cancel.');
  if DataSet.FieldByName('activity_time').IsNull then
    raise EOdDataEntryError.Create('An activity time is required, or press ESC to cancel.');
  DataSet.FieldByName('activity_time_key').AsString := FormatDateTime('hh:mm:ss',
    DataSet.FieldByName('activity_time').AsDateTime);

  if FAddingFromTimesheet then
    Exit;

  if StringInArray(DataSet.FieldByName('activity_type').AsString, [WorkStart, LunchStop, PersonalStop]) then begin
    if FWorkTimesUsed >= TimesheetNumWorkPeriods then
      raise EOdDataEntryError.CreateFmt('Exceeded maximum of %d work time spans.', [TimesheetNumWorkPeriods]);
    Inc(FWorkTimesUsed);
  end;
  if (DataSet.FieldByName('activity_type').AsString = CalloutStart) then begin
    if FCallTimesUsed >= TimesheetNumCalloutPeriods then
      raise EOdDataEntryError.CreateFmt('Exceeded maximum of %d callout time spans.', [TimesheetNumCalloutPeriods]);
    Inc(FCallTimesUsed);
  end;

  FAddedWorkStop := StringInArray(DataSet.FieldByName('activity_type').AsString, WorkStopActTypes);
  FAnyPendingChanges := True;
  NotifyDataChange;
end;

procedure TTimeClockDayFrame.ClockDetailDataNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('emp_id').Value := FEmpID;
  DataSet.FieldByName('work_date').Value := FWorkDate;
  LoadActivityTypeCombo(FLastUsedActivity);
end;

procedure TTimeClockDayFrame.DetermineReadOnly(ForceReadOnly: Boolean);
begin
  inherited;
  if (FMode = teNormal) and ReadOnly then
    ReadOnly := IsSubmitted or IsFinalApproved or (not DisplayingToday);
end;

procedure TTimeClockDayFrame.ClockDetailDataBeforeInsert(DataSet: TDataSet);
begin
  FLastUsedActivity := '';
  if FMode = teNormal then
    FLastUsedActivity := GetLastActivityUsed;
end;

procedure TTimeClockDayFrame.ClockDetailDataBeforeDelete(DataSet: TDataSet);
begin
  FAnyPendingChanges := True;
  NotifyDataChange;
end;

function TTimeClockDayFrame.IsLastActivityWorkStop: Boolean;
begin
  Result := (FMode = teNormal) and DisplayingToday and FAddedWorkStop;
end;

function TTimeClockDayFrame.ConfirmContinueWithSubmit: Boolean;
const
  SubmitMsg = 'If you are done with your day, you should Submit your time. Once ' +
    'time is submitted for the day, you will no longer be able to edit it.' + CRLF +
    'If you are done now, but may be called out later, do not Submit your time.';
  SubmitAndShutdownMsg = 'If you are done with your day, you should Submit your time and Shutdown. ' + CRLF +
    'Once time is submitted for the day, you will no longer be able to edit it.' + CRLF +
    'If you are done now, but may be called out later, you should just Shutdown.' + CRLF +
    'If you do not want to shutdown, choose Cancel Shutdown.';
var
  Response: string;
begin
  if PermissionsDM.CanI(RightTimesheetsClockImport) then begin
    Result := False;
    Exit;
  end;

  if not IsLastActivityWorkStop then begin
    Result := inherited ConfirmContinueWithSubmit;
    Exit;
  end;

  if PermissionsDM.CanI(RightWorkAfterWorkClockOut) then begin
    Response := ShowMessageAckDialog(SubmitMsg, '"Submit Time","Do not Submit"', 'Confirm Submit');
    Result := (Response = 'Submit Time');
    NeedToShutdown := False;
  end else begin
    Response := ShowMessageAckDialog(SubmitAndShutdownMsg, '"Submit && Shutdown","Save && Shutdown","Save && Cancel Shutdown"', 'Confirm Submit');
    Result := (Response = 'Submit && Shutdown');
    NeedToShutdown := not (Response = 'Save && Cancel Shutdown');
  end;
end;

function TTimeClockDayFrame.ImportTimeClockActivities: Boolean;
var
  UnableToImportMsg: string;
begin
  Result := False;
  try
    if ImportTimeClock(FWorkDate, TimeClockTranslator, (not ReadOnly)) then begin
      SaveChanges;
      Result := True;
    end;
  except
    on E: Exception do begin
      UnableToImportMsg := 'Unable to complete the time import because of this problem: ' + E.Message;
      DM.AddToQMLog(UnableToImportMsg);
      DM.ShowTransientMessage(UnableToImportMsg);
      CancelChanges;
    end;
  end;
end;

function TTimeClockDayFrame.AutoImport: Boolean;
begin
  Result := ImportTimeClockActivities;
end;

function TTimeClockDayFrame.IsGridReadOnly: Boolean;
begin
  Result := ReadOnly or
    ((FMode = teNormal) and PermissionsDM.CanI(RightTimesheetsClockImport));
end;

procedure TTimeClockDayFrame.TimeClockGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  if (FMode in [teManager, teITOverride]) then
    Exit;

  if not AViewInfo.Selected then begin
    if (not IsGridReadOnly) and ((FMode = teNormal)
      and EditingDataSet(ClockDetailData)) then
      ACanvas.Brush.Color := clWindow
    else
      ACanvas.Brush.Color := clBtnFace;
  end;
end;

procedure TTimeClockDayFrame.TimeClockGridDBTableViewEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  AAllow := (not ReadOnly)
    and ((FMode = teNormal) and (ClockDetailData.State = dsInsert))
    or ((FMode = teManager) or (FMode = teITOverride));
end;

end.
