object NotesFrame: TNotesFrame
  Left = 0
  Top = 0
  Width = 605
  Height = 298
  TabOrder = 0
  object NotesGrid: TcxGrid
    Left = 0
    Top = 105
    Width = 605
    Height = 167
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object NotesGridView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = NotesSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'notes_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      Preview.Column = NotesGridViewNote
      Preview.MaxLineCount = 1000
      Preview.Visible = True
      object NotesGridViewnotes_id1: TcxGridDBColumn
        Caption = 'Notes ID'
        DataBinding.FieldName = 'notes_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object NotesGridViewnotes_type_desc1: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'notes_type_desc'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 124
      end
      object NotesGridViewforeign_id1: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'foreign_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object NotesGridViewClaimant: TcxGridDBColumn
        Caption = 'Claimant'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 156
      end
      object NotesGridViewentry_date1: TcxGridDBColumn
        Caption = 'Entry Date'
        DataBinding.FieldName = 'entry_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Width = 107
      end
      object NotesGridViewemployee_name1: TcxGridDBColumn
        Caption = 'Entered By'
        DataBinding.FieldName = 'employee_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 94
      end
      object NotesGridViewuid1: TcxGridDBColumn
        Caption = 'UID'
        DataBinding.FieldName = 'uid'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 60
      end
      object NotesGridViewactive1: TcxGridDBColumn
        Caption = 'Active'
        DataBinding.FieldName = 'active'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = True
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        Visible = False
        MinWidth = 16
        Options.Filtering = False
        Width = 40
      end
      object NotesGridViewNote: TcxGridDBColumn
        Caption = 'Note'
        DataBinding.FieldName = 'note'
      end
      object NotesGridRestrictionCol: TcxGridDBColumn
        DataBinding.FieldName = 'Restriction'
        Width = 123
      end
    end
    object NotesGridLevel: TcxGridLevel
      GridView = NotesGridView
    end
  end
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object NoteLabel: TLabel
      Left = 4
      Top = 4
      Width = 26
      Height = 26
      Caption = 'Note Text'
      WordWrap = True
    end
    object NoteText: TMemo
      Left = 37
      Top = 4
      Width = 483
      Height = 95
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object AddNoteButton: TButton
      Left = 525
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Add Note'
      TabOrder = 1
      OnClick = AddNoteButtonClick
    end
  end
  object FooterPanel: TPanel
    Left = 0
    Top = 272
    Width = 605
    Height = 26
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object ViewAllCheck: TCheckBox
      Left = 4
      Top = 5
      Width = 97
      Height = 17
      Caption = 'View All Notes'
      TabOrder = 0
      OnClick = ViewAllCheckClick
    end
  end
  object NotesSource: TDataSource
    DataSet = Notes
    Left = 208
    Top = 184
  end
  object Notes: TDBISAMQuery
    BeforeOpen = NotesBeforeOpen
    AfterPost = NotesAfterPost
    OnCalcFields = NotesCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select * from Notes where foreign_type in (2,3,4)')
    Params = <>
    Left = 176
    Top = 184
  end
end
