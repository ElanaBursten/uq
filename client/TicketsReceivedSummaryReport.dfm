inherited TicketsReceivedSummaryReportForm: TTicketsReceivedSummaryReportForm
  Left = 246
  Top = 272
  Caption = 'TicketsReceivedSummaryReportForm'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 147
    Height = 20
    AutoSize = False
    Caption = 'Transmit Date Range:'
    WordWrap = True
  end
  inline RangeSelect: TOdRangeSelectFrame [1]
    Left = 18
    Top = 27
    Width = 248
    Height = 63
    TabOrder = 0
  end
  object IncludeBreakdown: TCheckBox
    Left = 0
    Top = 95
    Width = 193
    Height = 17
    Caption = 'Include Breakdown by Call Center'
    TabOrder = 1
    OnClick = IncludeBreakdownClick
  end
  object IncludeNoWorkTickets: TCheckBox
    Left = 0
    Top = 118
    Width = 201
    Height = 17
    Caption = 'Include tickets witih no locating work'
    Enabled = False
    TabOrder = 2
  end
  inherited SaveTSVDialog: TSaveDialog
    Top = 374
  end
end
