unit DamagesByLocatorReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect;

type
  TDamagesByLocatorReportForm = class(TReportBaseForm)
    ProfitCenterLabel: TLabel;
    LiabilityLabel: TLabel;
    RangeSelect: TOdRangeSelectFrame;
    ProfitCenter: TComboBox;
    Liability: TComboBox;
    Label1: TLabel;
    ManagerLabel: TLabel;
    ManagersComboBox: TComboBox;
    EmployeeStatusLabel: TLabel;
    EmployeeStatusCombo: TComboBox;
    EmployeeLabel: TLabel;
    LocatorComboBox: TComboBox;
    AllEmployeesCheckBox: TCheckBox;
    procedure ManagersComboBoxChange(Sender: TObject);
    procedure RepopulateLocatorList;
    function SingleEmployeeMode: Boolean;
    procedure AllEmployeesCheckBoxClick(Sender: TObject);
  private
    procedure SetControlVisibility;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses
  DMu, OdExceptions, QMConst, OdVclUtils, LocalEmployeeDmu;

{$R *.dfm}

{ TDamagesByLocatorReportForm }

procedure TDamagesByLocatorReportForm.ValidateParams;
begin
  inherited;
  if (ProfitCenter.Text = '') and (ManagersComboBox.Text = '') then
    raise EOdDataEntryError.Create('Please select either a Profit Center or a Manager');

  if SingleEmployeeMode and (LocatorComboBox.Text = '') then
    raise EOdDataEntryError.Create('Please select a Locator to report on');

  SetReportID('DamagesByLocator');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParamInt('Liability', Liability.ItemIndex);
  if SingleEmployeeMode then
    SetParamIntCombo('EmployeeID', LocatorComboBox, False)
  else
    SetParamIntCombo('EmployeeID', ManagersComboBox, False);
  SetParamBoolean('SingleEmployee', SingleEmployeeMode);
  SetParamIntCombo('EmployeeStatus', EmployeeStatusCombo, not SingleEmployeeMode);
end;

procedure TDamagesByLocatorReportForm.InitReportControls;
var
  i: TDamageLiability;
begin
  inherited;
  AllEmployeesCheckBox.Checked := True;
  SetUpManagerList(ManagersComboBox);
  if ManagersComboBox.Items.Count > 1 then
    ManagersComboBox.ItemIndex := 0;
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
  RangeSelect.SelectDateRange(rsPreviousMonth);
  DM.ProfitCenterList(ProfitCenter.Items);
  Liability.Items.Clear;
  for i := Low(TDamageLiability) to High(TDamageLiability) do
    Liability.Items.Add(DM.UQState.DamageLiabilityDescription[i]);
  Liability.ItemIndex := Liability.Items.Count-1;
end;

function TDamagesByLocatorReportForm.SingleEmployeeMode: Boolean;
begin
  Result := not AllEmployeesCheckBox.Checked;
end;

procedure TDamagesByLocatorReportForm.RepopulateLocatorList;
begin
  if SingleEmployeeMode then begin
    if ManagersComboBox.ItemIndex > -1 then begin
      EmployeeDM.EmployeeList(LocatorComboBox.Items, GetComboObjectInteger(ManagersComboBox))
    end;
  end else
    LocatorComboBox.Items.Clear;
end;

procedure TDamagesByLocatorReportForm.SetControlVisibility;
begin
  EmployeeStatusCombo.Enabled := not SingleEmployeeMode;
  LocatorComboBox.Enabled := SingleEmployeeMode;
end;

procedure TDamagesByLocatorReportForm.ManagersComboBoxChange(Sender: TObject);
begin
  RepopulateLocatorList;
end;

procedure TDamagesByLocatorReportForm.AllEmployeesCheckBoxClick(
  Sender: TObject);
begin
  SetControlVisibility;
  RepopulateLocatorList;
end;

end.
