unit DamagesPerLocatesReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect, OdVclUtils;

type
  TDamagesPerLocatesReportForm = class(TReportBaseForm)
    ProfitCenterLabel: TLabel;
    LiabilityLabel: TLabel;
    Label1: TLabel;
    RangeSelect: TOdRangeSelectFrame;
    ProfitCenter: TComboBox;
    Liability: TComboBox;
    LocateRatioLabel: TLabel;
    LocateRatio: TEdit;
    UtilityCoLabel: TLabel;
    Client: TComboBox;
    FacilityTypeLabel: TLabel;
    FacilityType: TComboBox;
    ShowLocatorDetail: TCheckBox;
    procedure ProfitCenterChange(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

var
  DamagesPerLocatesReportForm: TDamagesPerLocatesReportForm;

implementation

uses
  DMu, OdExceptions, QMConst;

{$R *.dfm}

{ TDamagesPerLocatesReportForm }

procedure TDamagesPerLocatesReportForm.ValidateParams;
var
  Ratio: Integer;
begin
  inherited;
  RequireComboBox(ProfitCenter, 'Please select a profit center');
  Ratio := StrToIntDef(LocateRatio.Text, 0);
  if Ratio < 1 then
    raise EOdDataEntryError.Create('Please enter a locate ratio of 1 or greater');

  SetReportID('DamagesPerLocates');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParam('ProfitCenter', ProfitCenter.Text);
  SetParam('FacilityType',  FacilityType.Text);
  SetParamIntCombo('ClientID', Client, False);
  SetParamInt('Liability', Liability.ItemIndex);
  SetParamInt('LocateRatio', Ratio);
  SetParamBoolean('ShowLocatorDetail', ShowLocatorDetail.Checked);
end;

procedure TDamagesPerLocatesReportForm.InitReportControls;
var
  i: TDamageLiability;
begin
  inherited;
  RangeSelect.SelectDateRange(rsYearToDate);
  DM.ProfitCenterList(ProfitCenter.Items);
  Liability.Items.Clear;
  for i := Low(TDamageLiability) to High(TDamageLiability) do
    Liability.Items.Add(DM.UQState.DamageLiabilityDescription[i]);
  Liability.ItemIndex := 0;
  LocateRatio.Text := '1000';
  DM.GetRefCodeList('factype', FacilityType.Items);
  FacilityType.Items.Insert(0, '');
  Client.Items.Clear;
end;

procedure TDamagesPerLocatesReportForm.ProfitCenterChange(Sender: TObject);
begin
  DM.ClientListForProfitCenter(ProfitCenter.Text, Client.Items);
  Client.Items.Insert(0, '');
  Client.Sorted := True;
  SizeComboDropdownToItems(Client);
end;

end.
