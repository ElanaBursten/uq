inherited OQManagementForm: TOQManagementForm
  Caption = 'OQ : QUalification Summary'
  ClientHeight = 585
  ClientWidth = 1029
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object AssetPanel: TPanel
    Left = 0
    Top = 0
    Width = 1029
    Height = 585
    Align = alClient
    TabOrder = 0
    object OQGRid: TcxGrid
      Left = 1
      Top = 33
      Width = 1027
      Height = 551
      Align = alClient
      DragMode = dmAutomatic
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object OQView: TcxGridDBTableView
        DragMode = dmAutomatic
        Navigator.Buttons.CustomButtons = <>
        Navigator.InfoPanel.Visible = True
        OnCustomDrawCell = OQViewCustomDrawCell
        DataController.DataSource = OQSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'emp_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Appending = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsSelection.InvertSelect = False
        OptionsView.ShowEditButtons = gsebForFocusedRecord
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.Indicator = True
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object ColIsQualified: TcxGridDBColumn
          Caption = 'Is Qualified'
          DataBinding.FieldName = 'is_qual'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.DisplayGrayed = 'False'
          Properties.ValueChecked = '1'
          Properties.ValueUnchecked = '0'
          Width = 60
        end
        object ColEmpID: TcxGridDBColumn
          Caption = 'Emp ID'
          DataBinding.FieldName = 'emp_id'
          Visible = False
          Options.Editing = False
          Width = 55
        end
        object ColEmpNumber: TcxGridDBColumn
          Caption = 'Emp #'
          DataBinding.FieldName = 'emp_number'
          Options.Editing = False
          Width = 63
        end
        object ColEmpShortName: TcxGridDBColumn
          Caption = 'Short Name'
          DataBinding.FieldName = 'short_name'
          BestFitMaxWidth = 1
          Options.Editing = False
          Width = 79
        end
        object ColFirstName: TcxGridDBColumn
          Caption = 'First Name'
          DataBinding.FieldName = 'first_name'
          BestFitMaxWidth = 1
          Options.Editing = False
          Width = 85
        end
        object ColLastName: TcxGridDBColumn
          Caption = 'Last Name'
          DataBinding.FieldName = 'last_name'
          BestFitMaxWidth = 1
          Options.Editing = False
          Width = 99
        end
        object ColQualifiedDate: TcxGridDBColumn
          Caption = 'Qual Date'
          DataBinding.FieldName = 'qualified_date'
          Options.Editing = False
          Width = 137
        end
        object ColExpiredDate: TcxGridDBColumn
          Caption = 'Exp Date'
          DataBinding.FieldName = 'expired_date'
          Options.Editing = False
          Styles.Header = SharedDevExStyleData.BoldMaroonFont
          Width = 143
        end
        object ColOQDescription: TcxGridDBColumn
          Caption = 'Qual For'
          DataBinding.FieldName = 'oq_description'
          Options.Editing = False
          Width = 120
        end
        object ColOQModifier: TcxGridDBColumn
          Caption = 'Code'
          DataBinding.FieldName = 'oq_modifier'
          Options.Editing = False
          Width = 54
        end
        object ColOQStatus: TcxGridDBColumn
          Caption = 'Status'
          DataBinding.FieldName = 'oq_status'
          OnCustomDrawCell = ColOQStatusCustomDrawCell
          Options.Editing = False
          Width = 105
        end
        object ColReportTo: TcxGridDBColumn
          Caption = 'Rpt To'
          DataBinding.FieldName = 'report_to'
          Visible = False
          Options.Editing = False
        end
        object ColReportLevel: TcxGridDBColumn
          Caption = 'Rpt Lvl'
          DataBinding.FieldName = 'report_level'
          Visible = False
          Options.Editing = False
        end
      end
      object OQGRidLevel: TcxGridLevel
        GridView = OQView
      end
    end
    object Headerpnl: TPanel
      Left = 1
      Top = 1
      Width = 1027
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object ReportingToLbl: TLabel
        Left = 5
        Top = 10
        Width = 76
        Height = 14
        Caption = 'Reporting To:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object MgrLbl: TLabel
        Left = 87
        Top = 10
        Width = 20
        Height = 14
        Caption = '-----'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object QualifiedOnlyCBx: TCheckBox
        Left = 304
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Qualified Only'
        TabOrder = 0
        OnClick = QualifiedOnlyCBxClick
      end
    end
  end
  object OQSource: TDataSource
    Left = 244
    Top = 168
  end
  object AssetActions: TActionList
    Left = 244
    Top = 200
    object AddAssetAction: TAction
      Caption = 'Add Asset'
    end
    object RemoveAssetAction: TAction
      Caption = 'Remove Asset'
    end
    object SaveChangesAction: TAction
      Caption = 'Save Changes'
    end
  end
end
