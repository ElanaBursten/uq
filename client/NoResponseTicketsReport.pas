unit NoResponseTicketsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TNoResponseTicketsReportForm = class(TReportBaseForm)
    TransmitDate: TOdRangeSelectFrame;
    Label3: TLabel;
    Label2: TLabel;
    CallCenterCombo: TComboBox;
  private
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  public
  end;

implementation

uses DMu;

{$R *.dfm}

{ TNoResponseTicketsReportForm }

procedure TNoResponseTicketsReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterCombo.Items, True);
end;

procedure TNoResponseTicketsReportForm.ValidateParams;
begin
  inherited;
  SetReportID('NoResponseTickets');
  RequireComboBox(CallCenterCombo, 'Please select a Call Center.');
  SetParamDate('StartDate', TransmitDate.FromDate);
  SetParamDate('EndDate', TransmitDate.ToDate);
  SetParam('CallCenterList', FCallCenterList.GetCode(CallCenterCombo.Text));
end;

end.
