unit WorkManageUtils;

interface

uses
  SysUtils, MSXML2_TLB, Classes, DB, cxTL, cxDBTL;

type
  TTicketCounts = record
    BucketName: string;
    BUCKETCOUNT: integer;  //QM-695
    Normal: integer;
    Emergency: integer;
    PastDue: integer;
    Today: integer; //QM-318 EB
    Ongoing: integer;
  end;

procedure PopulateAllOpenManagersFromServer(EmpTree: TcxDBTreeList);
function PopulateTicketListFromServer(LocatorID, NumDays: Integer; OpenOnly: Boolean; UseRouteOrder:Boolean = False): IXMLDomDocument;
function GetDamageListFromServer(LocatorID, NumDays: Integer; OpenOnly: Boolean): IXMLDomDocument;
function GetWorkOrderListFromServer(EmployeeID, NumDays: Integer; OpenOnly: Boolean): IXMLDomDocument;
function PopulateNewEmergFromServer(ManagerID, NumDays: Integer; OpenOnly: Boolean): IXMLDomDocument;
function PopulateActivitiesListFromServer(ManagerID, MaxActivityDays: Integer): IXMLDomDocument;
function PopulatePastDueFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument; //QM-133 EB Past Due
function PopulateTodayFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument;   //QM-318 EB Today
function PopulateOpenTicketsExpFromServer(ManagerID, RowLimit, MaxDays: Integer): IXMLDomDocument; //QM-695 EB Open Tickets Expanded
function PopulateOpenTicketsSumFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument; //QM-428 Eb Open Tickets
function PopulateVirtualFromServer(ManagerID, DaysBack: Integer; BucketName: string): IXMLDomDocument; //QM-486 EB Virtual Bucket
function PopulateDataSetFromDOM(Doc: IXMLDomDocument; DataSet: TDataSet; TargetNode: Integer = 0): TTicketCounts;  //QM-133 EB Need to expand nodes

function FindLocatorInTreeView(LocatorName: string; Tree: TcxDBTreeList; ManagerList: TStrings; Reset: Boolean; EmployeeStatus: Integer): TcxTreeListNode;

function NodeHasLocatorChildren(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsEmergencyNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsTicketActivityNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function ISPastDueNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;  //QM-133 EB Past Due Bucket
function IsTodayNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;    //QM-318 EB Today Bucket
function IsOpenTicketsNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean; //QM-428 Open Tickets Count
function IsVirtualBucketNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean; //QM-486 EB Virtual Bucket
function IsManagerNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsLocatorNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsEmployeeNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsClockedInNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsClockedOutNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsCalledInNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function IsTomorrowNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;  //QM-294 EB Tomorrow Bucket
function IsInActiveEmp(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;     //QM-294 EB Added Active


function EmpIDOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
function EmpNameOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;
function GetNodeName(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;  //QM-486 Virtual Bucket EB
function IsNodeUnderMe(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function ShowFutureTicketsOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
function TicketViewLimitOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
function ReportToOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
function IDOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;
function TypeOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;

function IsEmployee(EmpWorkload: TDataset): Boolean;
function IsManager(EmpWorkload: TDataset): Boolean;
function IsLocator(EmpWorkload: TDataset): Boolean;
function IsEmergency(EmpWorkload: TDataset): Boolean;
function IsTicketActivity(EmpWorkload: TDataset): Boolean;
function IsPastDue(EmpWorkload: TDataset): Boolean;  //QM-133 EB
function IsTodayDue(EmpWorkload: TDataset): Boolean; //QM-318 EB
function IsVirtualBucket(EmpWorkLoad: TDataset): Boolean; //QM-486 EB Virtual Bucket

function GetEmpPermissionFromServer(EmpID: Integer; Permission: string; var Limitation: string): Boolean; //QM-10 EB Returns Limitation if user has permission
function ShortNameForFile(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;




const
  MAGIC_NEW_EMERG = -1;
  MAGIC_NEW_ACTIVITY = -2;
  MAGIC_PAST_DUE = -3;  //QM-133 EB
  MAGIC_TODAY = -4;  //QM-318 EB
  MAGIC_OPEN_TICKETS = -5;  //QM-428 EB
  MAGIC_VIRTUAL = -6; //QM-486 EB Virtual Bucket

type
  TNodeType = (ntOrgNode, ntLocator, ntEmergency, ntTicketActivities, ntPastDue, ntOpen, ntToday, ntVirtual);  //QM-133 Past Due, QM-318 Today, QM-486 Virtual EB


implementation

uses DMu, OdIsoDates, OdMiscUtils, UQUtils, ServiceLink, OdMSXMLUtils,
  QMConst, Variants, LocalEmployeeDMu, LocalPermissionsDMu;

function FindNodeFor(Tree: TcxDBTreeList; ManagerNode: TcxTreeListNode; LocatorID: Integer): TcxTreeListNode;
var
  Child: TcxTreeListNode;
begin
  Result := nil;
  Child := ManagerNode.GetFirstChild;
  while Child <> nil do begin
    if IsEmployeeNode(Tree, Child) and (EmpIDOfNode(Tree, Child) = LocatorID) then begin
      Result := Child;
      Exit;
    end;
    Child := ManagerNode.GetNextChild(Child);
  end;
end;

function FindManagerNode(Tree: TcxDBTreeList; AEmpID: Integer): TcxTreeListNode;
var
  Node: TcxTreeListNode;
begin
  Result := nil;
  Node := Tree.TopNode;
  repeat
    if Assigned(Node) and IsManagerNode(Tree, Node) and (EmpIDOfNode(Tree, Node) = AEmpID) then begin
      Result := Node;
      Exit;
    end;
    Node := Node.GetNext;
  until Node = nil;
end;

procedure PopulateAllOpenManagersFromServer(EmpTree: TcxDBTreeList);
var
  Lst: TStringList;
  Node: TcxTreeListNode;
  LimitationStr: string;
  VBMgrID: integer;
begin
  Lst := TStringList.Create;
  try
    Node := EmpTree.TopNode;
    while Assigned(Node) do begin
      if IsNodeUnderMe(EmpTree, Node) and IsManagerNode(EmpTree, Node) and NodeHasLocatorChildren(EmpTree, Node) then
        Lst.Add(IDOfNode(EmpTree, Node));
      Node := Node.GetNext;
    end;

    if Lst.Count > 0 then begin
     {Permissions: Virtual Bucket, Today, Open & Past Due (stacked to avoid old client compatibility issues}
      If PermissionsDM.CanI(RightTicketsVirtualBucket) then begin     //QM-486 Virtual Bucket EB
        LimitationStr := PermissionsDM.GetLimitationDef(RightTicketsVirtualBucket, '0');
        try
          VBMgrID := StrToInt(LimitationStr);
          if (VBMgrID = 0) or (not EmployeeDM.EmpIsManager(VBMgrID)) then
              VBMgrID := DM.EmpID;
        except
          VBMgrID := DM.EmpID;
        end;
        DM.CallingServiceName('MultiOpenTotals6V');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6V(DM.Engine.SecurityInfo, Lst.CommaText, VBMgrID ));
        EmpTree.DataController.DataSet.Refresh;
      end else

     {Permissions: Today, Open & Past Due  - note this is primarily to avoid old client compatibility issues}
	    If PermissionsDM.CanI(RightTicketsTodayOpenPastDueTree) then begin     //QM-318 Today Tickets EB
        DM.CallingServiceName('MultiOpenTotals6TD');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6TD(DM.Engine.SecurityInfo, Lst.CommaText));
        EmpTree.DataController.DataSet.Refresh;
      end else
      {Permissions: Open & Past Due}
      if PermissionsDM.CanI(RightTicketsOpenTree)then begin    //QM-428 Open Tickets EB
        DM.CallingServiceName('MultiOpenTotals6OP');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6OP(DM.Engine.SecurityInfo, Lst.CommaText));
        EmpTree.DataController.DataSet.Refresh;
      end else
      {Permissions: Past Due}
      if PermissionsDM.CanI(RightTicketsPastDueTree) then begin     //QM-248 & QM-133 Past Due EB
        DM.CallingServiceName('MultiOpenTotals6PD');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6PD(DM.Engine.SecurityInfo, Lst.CommaText));
        EmpTree.DataController.DataSet.Refresh;
      end
      else begin
        DM.CallingServiceName('MultiOpenTotals6');
        DM.Engine.SyncTablesFromXmlString(DM.Engine.Service.MultiOpenTotals6(DM.Engine.SecurityInfo, Lst.CommaText));
        EmpTree.DataController.DataSet.Refresh;
      end;
    end;
  finally
    FreeAndNil(Lst);
  end;
end;

function PopulateTicketListFromServer(LocatorID, NumDays: Integer; OpenOnly: Boolean; UseRouteOrder: Boolean): IXMLDomDocument;
begin
  Assert(LocatorID > 0);
  if UseRouteOrder then begin  //QM-896 Route Order limitation
    GetChan.OperationName := 'GetTicketsForLocatorRO';
    Result := ParseXml(DM.Engine.Service.GetTicketsForLocatorRO(DM.Engine.SecurityInfo, LocatorID, NumDays, OpenOnly, False));
  end
  else begin
    GetChan.OperationName := 'GetTicketsForLocator';
    Result := ParseXml(DM.Engine.Service.GetTicketsForLocator(DM.Engine.SecurityInfo, LocatorID, NumDays, OpenOnly, False));
  end;
end;

function PopulateNewEmergFromServer(ManagerID, NumDays: Integer; OpenOnly: Boolean): IXMLDomDocument;
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetTicketsForLocator';
  Result := ParseXml(DM.Engine.Service.GetTicketsForLocator(DM.Engine.SecurityInfo, ManagerID, NumDays, False, True));
end;

function PopulateActivitiesListFromServer(ManagerID, MaxActivityDays: Integer): IXMLDomDocument;
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetTicketActivities';
  Result := ParseXml(DM.Engine.Service.GetTicketActivities(DM.Engine.SecurityInfo, ManagerID, MaxActivityDays));
end;

function PopulatePastDueFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument; //QM-133 EB Past Due
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetPastDueList';
  Result := ParseXml(DM.Engine.Service.GetPastDueList(DM.Engine.SecurityInfo, ManagerID, MaxDays));
end;

function PopulateTodayFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument;   //QM-318 EB Today
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetTodayList';
  Result := ParseXml(DM.Engine.Service.GetTodayList(DM.Engine.SecurityInfo, ManagerID, MaxDays));
end;

function PopulateVirtualFromServer(ManagerID, DaysBack: Integer; BucketName: string): IXMLDomDocument; //QM-486 EB Virtual Bucket
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetVirtualBucket' + BucketName;

  Result := ParseXml(DM.Engine.Service.GetVirtualBucket(DM.Engine.SecurityInfo, ManagerID, BucketName ));
end;

function PopulateOpenTicketsExpFromServer(ManagerID, RowLimit, MaxDays: Integer): IXMLDomDocument; //QM-695 EB Open Tickets Expanded
begin
   Assert(ManagerID > 0);
  GetChan.OperationName := 'GetOpenTicketExpList';
  Result := ParseXML(DM.Engine.Service.GetOpenTicketExpList(DM.Engine.SecurityInfo, ManagerID, RowLimit, MaxDays));
end;

function PopulateOpenTicketsSumFromServer(ManagerID, MaxDays: Integer): IXMLDomDocument; //QM-428 Eb Open Tickets
begin
  Assert(ManagerID > 0);
  GetChan.OperationName := 'GetOpenTicketSumList';
  Result := ParseXML(DM.Engine.Service.GetOpenTicketSumList(DM.Engine.SecurityInfo, ManagerID, MaxDays));
end;

function GetDamageListFromServer(LocatorID, NumDays: Integer; OpenOnly: Boolean): IXMLDomDocument;
begin
  Assert(LocatorID > 0);
  GetChan.OperationName := 'GetDamagesForLocator';
  Result := ParseXml(DM.Engine.Service.GetDamagesForLocator(DM.Engine.SecurityInfo,
    LocatorID, NumDays, OpenOnly));
end;

function GetWorkOrderListFromServer(EmployeeID, NumDays: Integer;
  OpenOnly: Boolean): IXMLDomDocument;
begin
  Assert(EmployeeID > 0);
  GetChan.OperationName := 'GetWorkOrdersForEmployee';
  Result := ParseXML(DM.Engine.Service.GetWorkOrdersForEmployee(
    DM.Engine.SecurityInfo, EmployeeID, NumDays, OpenOnly, False));
end;

function GetEmpPermissionFromServer(EmpID: Integer; Permission: string; var Limitation: string): Boolean;  //QM-10 EB
{Returns Limitation}
var
  TempStr: string;
begin
  if EmpID > 0 then begin

    GetChan.OperationName := 'GetEmpPermission';
    TempStr := DM.Engine.Service.GetEmpPermission(DM.Engine.SecurityInfo, EmpID, Permission);
    if TempStr = REVOKED then
      Result := False
    else begin
      Result := True;
      Limitation := TempStr;
    end;
  end
  else Result := False;
end;

function PopulateDataSetFromDOM(Doc: IXMLDomDocument; DataSet: TDataSet; TargetNode: Integer ): TTicketCounts;
var
  Nodes: IXMLDOMNodeList;
  Elem: IXMLDOMElement;
  i: Integer;

  function AttrText(const AttrName: string): string;
  var
    AttrNode: IXMLDomNode;
  begin
    AttrNode := Elem.attributes.getNamedItem(AttrName);
    if AttrNode <> nil then
      Result := AttrNode.text
    else
      Result := '-';
  end;

  function AttrInt(const AttrName: string): Integer;
  var
    AttrStr: string;
  begin
    AttrStr := AttrText(AttrName);
    try
      Result := StrToInt(AttrStr);
    except
      on E: Exception do begin
        E.Message := Format('Invalid Integer text "%s" for attribute "%s"', [AttrStr, AttrName]);
        raise;
      end;
    end;
  end;

  function AttrDateTime(const AttrName: string): Variant;
  var
    AttrStr: string;
  begin
    Result := Null;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and IsoIsValidDateTime(AttrStr) then
      Result := IsoStrToDateTime(AttrStr);
  end;

  function AttrFloat(const AttrName: string): Double;
  var
    AttrStr: string;
  begin
    Result := 0.0;
    AttrStr := AttrText(AttrName);
    if (not StrConsistsOf(AttrStr, ['-', ' '])) and (AttrStr <> NULL) then
      Result := StrToFloat(AttrStr);
  end;

var
  FieldValue: string;
  TS: TTicketStatus;
begin
  {These are the counts in the CURRENT BUCKET} //QM-274 EB Counts
  Result.BucketName := '';
  Result.BUCKETCOUNT := 0; {Number of Tickets in this bucket returned}
  Result.Normal := 0;    {kind = Normal}
  Result.Emergency := 0; {kind = Emergency}
  Result.PastDue := 0;   {tickets that are past due}
  Result.Today := 0;     {tickets that are today}
  Result.Ongoing := 0;   {Tickets that are ongoing}


  {These have to match the tname in the stored procedure exactly}
  if TargetNode = MAGIC_NEW_ACTIVITY then begin
    Nodes := Doc.SelectNodes('//activity');
    Result.BucketName := bAct;
  end
  else if TargetNode = MAGIC_PAST_DUE then begin    //QM-133 Past Due bucket EB
    Nodes := Doc.selectNodes('//pastdue');
    Result.BucketName := bPD;
  end
  else if TargetNode = MAGIC_TODAY then begin      //QM-318 Today bucket  EB
    Nodes := Doc.selectNodes('//today');
    Result.BucketName := bToday;
  end
  else if TargetNode = MAGIC_OPEN_TICKETS then begin
    Nodes := Doc.selectNodes('//opentickets'); //QM-428 Open Tickets Bucket EB    //QM-695 Open Tickets Expanded NO CHANGE HERE
    Result.BucketName := bOpen;
  end
  else if TargetNode = MAGIC_VIRTUAL then begin
    Nodes := Doc.selectNodes('//virtualbucket'); //QM-486 Virtual Bucket EB
    Result.BucketName := bVirtual;
  end
  else if TargetNode = MAGIC_NEW_EMERG then begin  //QM695 EB Open Tickets
    Nodes := Doc.SelectNodes('//ticket');
    Result.BucketName := bEmerg;
  end
  else begin
    Nodes := Doc.SelectNodes('//ticket');
    Result.BucketName := bLoc;
  end;


  // Get the control ready
  DataSet.DisableControls;
  while not DataSet.IsEmpty do
    DataSet.Delete;

  try
    // Process the records
    for i := 0 to Nodes.Length-1 do begin
      Elem := Nodes[i] as IXMLDOMElement;

      DataSet.Open;
      DataSet.Insert;
      Inc(Result.BucketCount); //QM-695 Check the bucket count

    //  if Result.BucketName = 'LOCATOR' then
//        Result.BucketName := 'LOCATOR: ' + DataSet.FieldByName('short_name').AsString;
      

      {ALT GRID - Counts only}
      {  We have to fill out the values (to prevent load errors), then we hide the invalid fields}
      if (TargetNode = MAGIC_OPEN_TICKETS) and (PermissionsDM.CanI(RightTicketsOpenExpTree)=FALSE) then begin   // QM-428 EB   //QM-695 Eb Open Ticket Expanded
        DataSet.FieldByName('short_name').AsString := AttrText('short_name');
        DataSet.FieldByName('tkt_count').AsInteger := AttrInt('tkt_count');
        DataSet.FieldByName('loc_count').AsInteger := AttrInt('loc_count');

        {mandatory stuff}
        DataSet.FieldByName('ticket_number').AsString := '--';
        DataSet.FieldByName('ticket_id').AsInteger := 0;
        DataSet.FieldByName('activity_id').AsInteger := 0;
        DataSet.FieldByName('activity_date').AsDateTime := NOW;
        DataSet.FieldByName('activity').AsString := '--';
        DataSet.FieldByName('alert').AsString := '--';
        DataSet.FieldByName('ticket_type').AsString := '--';
        DataSet.FieldByName('kind').AsString := '--'; //FieldValue;   //QM-274 EB
        DataSet.FieldByName('ticket_format').AsString := '--';
        DataSet.FieldByName('due_date').Value := NOW;
        DataSet.FieldByName('work_address_number').AsString := '--';
        DataSet.FieldByName('work_address_street').AsString := '--';
        DataSet.FieldByName('work_city').AsString := '--';
        DataSet.FieldByName('work_type').AsString := '--';
        DataSet.FieldByName('map_page').AsString := '--';
        DataSet.FieldByName('work_county').AsString := '--';
        DataSet.FieldByName('work_priority').AsString := '--';
        DataSet.FieldByName('modified_date').Value := NOW;
        DataSet.FieldByName('ticket_is_visible').AsString := '--';
        DataSet.FieldByName('est_start_time').Value := NOW; //QM-751 First Task Fix
        DataSet.FieldByName('ticket_status').AsInteger := 0; //QMANTWO-396
        DataSet.FieldByName('area_name').AsString := '--';

        DataSet.FieldByName('lon').AsFloat := 0.0; //QM-10 EB
        DataSet.FieldByName('lat').AsFloat := 0.0; //QM-10 EB
        DataSet.FIeldByName('work_state').AsString := '--';
        DataSEt.FieldByName('route_order').AsFloat := 0.0; //QM-10 EB
        DataSet.FieldByName('util').AsString := '--'; //QM-306
        DataSet.FieldByName('con_name').AsString := '--'; //QM-325 EB
        DataSet.FieldByName('company').AsString := '--'; //QM-325 EB
        DataSet.FieldByName('risk').AsString := '--';  //QM-387 EB
        DataSet.FieldByName('age_hours').AsInteger := 0; //QM-551 EB
        DataSet.FieldByName('alt_due_date').Value := NOW;    //QM-973 EB
        DataSet.FieldByName('expiration_date').Value := NOW;  //QM-973 EB

        DataSet.Post;
      end

      {TICKET GRID - includes the OPEN Tickets Expanded}
      else begin
        DataSet.FieldByName('ticket_id').AsInteger := StrToInt(AttrText('ticket_id'));

        if (TargetNode = MAGIC_NEW_ACTIVITY) then begin //QM-428 EB
          DataSet.FieldByName('activity_id').AsInteger := AttrInt('activity_id');
          DataSet.FieldByName('activity').AsString := AttrText('activity');
          DataSet.FieldByName('activity_date').AsDateTime := AttrDateTime('activity_date');
          DataSet.FieldByName('short_name').AsString := AttrText('short_name');
        end;

        if (TargetNode = MAGIC_PAST_DUE) or (TargetNode = MAGIC_TODAY)
        or (TargetNode = MAGIC_VIRTUAL) or (TargetNode = MAGIC_NEW_EMERG)
        or (TargetNode = MAGIC_OPEN_TICKETS) then begin  //QM-133 Past Due, QM-318 Today, QM-486 VIrtual EB, QM-551 EB, QM-695 EB
          DataSet.FieldByName('short_name').AsString := AttrText('short_name');
        end;

        DataSet.FieldByName('ticket_number').AsString := AttrText('ticket_number');
        FieldValue := ' ';
        if AttrText('alert') = 'A' then
          FieldValue := '!';
        DataSet.FieldByName('alert').AsString := FieldValue;


        if AttrText('kind') = TicketKindDone then
          FieldValue := TicketKindDone

        else if AttrText('kind') = TicketKindOngoing then begin
          FieldValue := 'ONGOING: ' + AttrText('ticket_type');
          inc(Result.Ongoing);   //QM-274 EB
        end

        else if AttrText('kind') = TicketKindEmergency then begin
          inc(Result.Emergency);  //QM-274 EB
          FieldValue := AttrText('ticket_type');
        end

        else if AttrText('kind') = TicketKindNormal then begin
          inc(Result.Normal);  //QM-274 EB
          FieldValue := AttrText('ticket_type');
        end

        else
          FieldValue := AttrText('ticket_type');

       if (AttrDateTime('due_date') < Now) then
          inc(Result.PastDue);    //QM-274 EB
       if DateToStr(AttrDateTime('due_date')) = DateToStr(Now) then
        inc(Result.Today);    //QM-318 EB 


        DataSet.FieldByName('ticket_type').AsString := FieldValue;
        DataSet.FieldByName('kind').AsString := AttrText('kind'); //FieldValue;   //QM-274 EB
        DataSet.FieldByName('ticket_format').AsString := AttrText('ticket_format');
        DataSet.FieldByName('due_date').Value := AttrDateTime('due_date');
        DataSet.FieldByName('work_address_number').AsString := AttrText('work_address_number');
        DataSet.FieldByName('work_address_street').AsString := AttrText('work_address_street');
        DataSet.FieldByName('work_city').AsString := AttrText('work_city');
        DataSet.FieldByName('work_type').AsString := AttrText('work_type');
        DataSet.FieldByName('map_page').AsString := AttrText('map_page');
        DataSet.FieldByName('work_county').AsString := AttrText('work_county');
        DataSet.FieldByName('work_priority').AsString := AttrText('work_priority');
        DataSet.FieldByName('ticket_id').AsString := AttrText('ticket_id');
        DataSet.FieldByName('modified_date').Value := AttrDateTime('modified_date');
        DataSet.FieldByName('ticket_is_visible').AsString := AttrText('ticket_is_visible');
        DataSet.FieldByName('est_start_time').Value := AttrDateTime('est_start_time');
        TS := GetTicketStatusFromRecord(DataSet);
        DataSet.FieldByName('ticket_status').AsInteger := Ord(Word(TS)); //QMANTWO-396
        DataSet.FieldByName('area_name').AsString := AttrText('area_name');

        DataSet.FieldByName('lon').AsFloat := AttrFloat('lon'); //QM-10 EB
        DataSet.FieldByName('lat').AsFloat := AttrFloat('lat'); //QM-10 EB
        DataSet.FIeldByName('work_state').AsString := AttrText('work_state');
        DataSEt.FieldByName('route_order').AsFloat := AttrFloat('route_order'); //QM-10 EB
        DataSet.FieldByName('util').AsString := AttrText('util'); //QM-306
        DataSet.FieldByName('con_name').AsString := AttrText('con_name'); //QM-325 EB
        DataSet.FieldByName('company').AsString := AttrText('company'); //QM-325 EB
        DataSet.FieldByName('risk').AsString := AttrText('risk');  //QM-387 EB
        if (AttrText('age_hours') <> '-') then
          DataSet.FieldByName('age_hours').AsInteger := AttrInt('age_hours');  //QM-551 EB
        DataSet.FieldByName('alt_due_date').Value := AttrDateTime('alt_due_date');  //QM-973 EB
        DataSet.FieldByName('expiration_date').Value := AttrDateTime('expiration_date');  //QM-973 EB

        DataSet.Post;
      end;
    end;
  finally
    DataSet.EnableControls;
  end;
end;






function FindLocatorInTreeView(LocatorName: string; Tree: TcxDBTreeList; ManagerList: TStrings; Reset: Boolean; EmployeeStatus: Integer): TcxTreeListNode;
var
  LocatorID: Integer;
  Node: TcxTreeListNode;
  ManagerID: Integer;
begin
  Assert(LocatorName <> '');
  Assert(Tree <> nil);
  Node := nil;
  if ManagerList.Count = 0 then begin
    Assert(Tree.DataController.NodesCount > 0, 'No tree nodes');   //QMANTWO-687 EB Replaced rowcouut with nodescount
    ManagerList.Add(IntToStr(EmpIDOfNode(Tree, Tree.Items[0])));
  end;

  while Node = nil do begin
    LocatorID := DM.FindLocatorByPartialShortName(LocatorName, ManagerList, EmployeeStatus, Reset);
    Reset := False;
    if LocatorId = 0 then
      Break;
    Node := FindManagerNode(Tree, LocatorID);
    if Node <> nil then begin
      if EmpIDOfNode(Tree, Node) = LocatorID then begin
        Result := Node;
        Exit;
      end;
    end;

    if Node = nil then begin
      ManagerID := EmployeeDM.GetManagerIDForEmp(LocatorID);
      Node := FindManagerNode(Tree, ManagerID);
    end;

    if Node <> nil then begin
      Tree.Select(Node);
      Node := FindNodeFor(Tree, Tree.Selections[0], LocatorID);
    end;
  end;
  Result := Node;
end;

function NodeHasLocatorChildren(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Child: TcxTreeListNode;
begin
  Assert(Assigned(Node));
  Result := False;
  Child := Node.GetFirstChild;
  while Child <> nil do begin
    if IsLocatorNode(Tree, Child) then begin
      Result := True;
      Exit;
    end;
    Child := Node.GetNextChild(Child);
  end;
end;

function GetValueFromNodeColumn(Tree: TcxDBTreeList; Node: TcxTreeListNode; ColName: string): Variant;
begin
  Assert(NotEmpty(ColName));
  Assert(Assigned(Tree));
  if (not Assigned(Node)) or (not Assigned(Node.TreeList)) then
    Result := Null
  else
    Result := Node.Values[Tree.ColumnByName(ColName).ItemIndex];
end;

function IDOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;
begin
  Result := VarToString(GetValueFromNodeColumn(Tree, Node, 'EmpTreeIDColumn'));
end;

function EmpIDOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
begin
  Result := VarToInt(GetValueFromNodeColumn(Tree, Node, 'EmpIDColumn'));
end;

function EmpNameOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;
begin
  Result := VarToString(GetValueFromNodeColumn(Tree, Node, 'EmployeeColumn'));
end;

function GetNodeName(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;  //QM-486 Virtual Bucket EB
begin
  Result := VarToString(GetValueFromNodeColumn(Tree, Node, 'ShortNameColumn'));

end;

function TicketViewLimitOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
begin
  Result := VarToInt(GetValueFromNodeColumn(Tree, Node, 'TicketViewLimitColumn'));
end;

function ReportToOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
begin
  Result := VarToInt(GetValueFromNodeColumn(Tree, Node, 'ReportToColumn'));
end;

function ShowFutureTicketsOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Idx: Integer;
begin
  Result := False;
  if Assigned(Node) then begin
    Idx := Tree.ColumnByName('ShowFutureTicketsColumn').ItemIndex;
    Result := VarToBoolean(Node.Values[Idx]);
  end;
end;

function TypeOfNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Integer;
begin
  Result := 0;
  if Assigned(Node) then
    Result := VarToInt(GetValueFromNodeColumn(Tree, Node, 'EmpTypeColumn'));
end;

function IsNodeUnderMe(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Idx: Integer;
begin
  Result := False;
  if Assigned(Node) then begin
    Idx := Tree.ColumnByName('UnderMeColumn').ItemIndex;
    Result := VarToBoolean(Node.Values[Idx]);
  end;
end;

function IsEmergencyNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_NEW_EMERG);
end;

function IsEmployeeNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
begin
  Result := (not IsEmergencyNode(Tree, Node)) and
    (not IsTicketActivityNode(Tree, Node));
end;



function IsInActiveEmp(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;     //QM-294 EB Added Active
var
  Idx: integer;
begin
  Result := False;
  if IsEmployeeNode(Tree, Node) then
  begin
    Idx := Tree.ColumnByName('IsActiveColumn').ItemIndex;
    Result := not (VarToBoolean(Node.Values[Idx]));
  end;

end;

function IsLocatorNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
begin
  Result := IsEmployeeNode(Tree, Node) and (not IsManagerNode(Tree, Node));
end;

function IsManagerNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Idx: Integer;
begin
  Result := False;
  if Assigned(Node) then begin
    Idx := Tree.ColumnByName('IsManagerColumn').ItemIndex;
    Result := VarToBoolean(Node.Values[Idx], False);
  end;
end;

function IsTicketActivityNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_NEW_ACTIVITY);
end;

function ISPastDueNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;  //QM-133 EB Past Due Bucket
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_PAST_DUE);
end;

function IsTodayNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean; //QM-318 EB Today Bucket
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_TODAY);
end;

function IsOpenTicketsNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean; //QM-428 Open Tickets Count
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_OPEN_TICKETS);
end;

function IsVirtualBucketNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean; //QM-486 EB Virtual Bucket
begin
  Result := (TypeOfNode(Tree, Node) = MAGIC_VIRTUAL);
end;


function IsClockedInNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Idx: Integer;
begin
  Result := False;
  if Assigned(Node) then begin
    idx := Tree.ColumnByName('WorkStatusTodayColumn').ItemIndex;
    Result := Node.Values[idx] = 'IN';
  end;
end;

function IsClockedOutNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
var
  Idx: Integer;
begin
  Result := False;
  if Assigned(Node) then begin
    idx := Tree.ColumnByName('WorkStatusTodayColumn').ItemIndex;
    Result := Node.Values[idx] = 'OUT';
  end;
end;

function IsCalledInNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;
begin
  Result := False;
  if Assigned(Node) then
    Result := Node.Values[Tree.ColumnByName('WorkStatusTodayColumn').ItemIndex] = 'CALLIN';
end;

function IsTomorrowNode(Tree: TcxDBTreeList; Node: TcxTreeListNode): Boolean;  //QM-294 EB
begin
  Result := False;
  if Assigned(Node) then
    Result := Node.Values[Tree.ColumnByName('EmpTypeColumn').ItemIndex] = EmployeeDM.EmpWorkType.TomorrowBucketRefID;
end;

function IsEmergency(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('type_id').AsInteger = MAGIC_NEW_EMERG);
end;

function IsTicketActivity(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('type_id').AsInteger = MAGIC_NEW_ACTIVITY);
end;

function IsPastDue(EmpWorkload: TDataset): Boolean;  //QM-425 EB
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('type_id').AsInteger = MAGIC_PAST_DUE);
end;

function IsTodayDue(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('type_id').AsInteger = MAGIC_TODAY);
end;

function IsVirtualBucket(EmpWorkLoad: TDataset): Boolean; //QM-486 EB Virtual Bucket
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('type_id').AsInteger = MAGIC_VIRTUAL);
end;

function IsEmployee(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := EmpWorkload.Active and (EmpWorkload.FieldByName('emp_id').AsInteger > 0);
end;

function IsManager(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := IsEmployee(EmpWorkload) and EmpWorkload.FieldByName('is_manager').AsBoolean;
end;

function IsLocator(EmpWorkload: TDataset): Boolean;
begin
  Assert(Assigned(EmpWorkload));
  Result := IsEmployee(EmpWorkload) and (not IsManager(EmpWorkload));
end;


function ShortNameForFile(Tree: TcxDBTreeList; Node: TcxTreeListNode): string;  //QM-10 EB
var
  NodeEmpID: Integer;
begin
  try
    NodeEmpID := EmpIDOfNode(Tree, Node);
    Result := EmployeeDM.GetEmployeeShortName(NodeEmpID);
    Result := StringReplace(Result, '.', ' ', [rfReplaceAll]);
    Result := MakeLegalFileName(Result, '');
  except
    Result := '';
  end;
end;




end.
