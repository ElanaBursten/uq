unit EmailStatusReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TEmailStatusReportForm = class(TReportBaseForm)
    CallCenterComboBox: TComboBox;
    CallCenterLabel: TLabel;
    QueueDate: TOdRangeSelectFrame;
    Label1: TLabel;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu;

{$R *.dfm}

procedure TEmailStatusReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  CallCenterComboBox.ItemIndex := 0;
  QueueDate.SelectDateRange(rsYesterday);
end;

procedure TEmailStatusReportForm.ValidateParams;
begin
  inherited;
  SetReportID('EmailStatus');
  SetParamDate('DateFrom', QueueDate.FromDate);
  SetParamDate('DateTo', QueueDate.ToDate);
  SetParam('CallCenter', FCallCenterList.GetCode(CallCenterComboBox.Text));
end;

end.
