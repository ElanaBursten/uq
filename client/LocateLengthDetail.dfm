object LocateLengthEditor: TLocateLengthEditor
  Left = 480
  Top = 429
  Caption = 'Locate Length Editor'
  ClientHeight = 157
  ClientWidth = 223
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LocateLengthDetailPanel: TPanel
    Left = 0
    Top = 0
    Width = 223
    Height = 157
    Align = alClient
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
    object WorkDateLabel: TLabel
      Left = 25
      Top = 9
      Width = 55
      Height = 13
      Caption = 'Work Date:'
    end
    object LenMarkedLabel: TLabel
      Left = 25
      Top = 31
      Width = 75
      Height = 13
      Caption = 'Length Marked:'
    end
    object LenTotalLabel: TLabel
      Left = 25
      Top = 53
      Width = 64
      Height = 13
      Caption = 'Length Total:'
    end
    object QtyTotalLabel: TLabel
      Left = 25
      Top = 75
      Width = 73
      Height = 13
      Caption = 'Quantity Total:'
    end
    object lblOverridetxt: TLabel
      Left = 25
      Top = 98
      Width = 172
      Height = 59
      AutoSize = False
      Caption = 'Override to be used for: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object QtyTotalEdit: TEdit
      Left = 140
      Top = 72
      Width = 57
      Height = 21
      TabStop = False
      AutoSize = False
      ParentColor = True
      ReadOnly = True
      TabOrder = 3
    end
    object QtyTotalSpinEdit: TcxSpinEdit
      Left = 140
      Top = 71
      Properties.Alignment.Horz = taRightJustify
      Properties.AssignedValues.MinValue = True
      Properties.OnEditValueChanged = QtyTotalSpinEditPropertiesEditValueChanged
      TabOrder = 6
      Width = 57
    end
    object WorkDateEdit: TcxDateEdit
      Left = 90
      Top = 4
      Enabled = False
      Properties.DateButtons = [btnToday]
      Properties.DateOnError = deToday
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 107
    end
    object LenMarkedEdit: TcxSpinEdit
      Left = 140
      Top = 28
      Properties.Alignment.Horz = taRightJustify
      Properties.AssignedValues.MinValue = True
      Properties.Increment = 5.000000000000000000
      Properties.MaxValue = 100000.000000000000000000
      Properties.OnChange = LenMarkedEditPropertiesChange
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Width = 57
    end
    object LenTotalEdit: TEdit
      Left = 140
      Top = 50
      Width = 57
      Height = 21
      TabStop = False
      AutoSize = False
      ParentColor = True
      ReadOnly = True
      TabOrder = 2
    end
    object OkButton: TButton
      Left = 59
      Top = 97
      Width = 49
      Height = 0
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 4
      TabStop = False
    end
    object CancelButton: TButton
      Left = 121
      Top = 97
      Width = 49
      Height = 0
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 5
      TabStop = False
      OnClick = CancelButtonClick
    end
  end
end
