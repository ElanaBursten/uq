unit PhoneVerifyDialog;
 //qm-388  sr
interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Mask;

type
  TPhoneVerifyDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    edtNewNumber: TMaskEdit;
    edtPresentPhoneNo: TEdit;
    procedure FormShow(Sender: TObject);
    procedure edtNewNumberChange(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    fnewNumber: string;
    fpresentNumber: string;
    { Private declarations }
  public
    { Public declarations }
    property presentNumber:string read fpresentNumber write fpresentNumber;
    property newNumber:string read fnewNumber write fnewNumber;
  end;

var
  PhoneVerifyDlg: TPhoneVerifyDlg;

implementation

{$R *.dfm}

procedure TPhoneVerifyDlg.edtNewNumberChange(Sender: TObject);
begin
  OKBtn.Enabled := true;
  OKBtn.Caption := 'Save';
end;

procedure TPhoneVerifyDlg.FormShow(Sender: TObject);
begin
  edtPresentPhoneNo.Text := presentNumber;
end;

procedure TPhoneVerifyDlg.OKBtnClick(Sender: TObject);
begin
  newNumber:=edtNewNumber.text;
end;

end.
