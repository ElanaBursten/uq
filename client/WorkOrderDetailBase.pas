unit WorkOrderDetailBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  DB, DMu, dbisamtb, StdCtrls, DBCtrls, ExtCtrls;

type
  TWODetailBaseFrame = class(TFrame)
    DetailsPanel: TPanel;
    DueDateLabel: TLabel;
    TransmitDateLabel: TLabel;
    CustomerPanel: TPanel;
    WorkAddressLabel: TLabel;
    CallerNameLabel: TLabel;
    ClientCodeLabel: TLabel;
    ClientNameLabel: TLabel;
    ClientWONumberLabel: TLabel;
    ClientOrderNumLabel: TLabel;
    FeedDetailsPanel: TPanel;
    MapPageLabel: TLabel;
    MapPageGrid: TLabel;
    EditingPanel: TPanel;
    StatusList: TDBISAMQuery;
    StatusSource: TDataSource;
    WorkOrderDS: TDataSource;
    ButtonPanel: TPanel;
    PlatAddinButton: TButton;
    TerminalGPSLabel: TLabel;
    ClosedDateLabel: TLabel;
    lblWO_ID: TLabel;
    TransmitDate: TDBText;
    DueDate: TDBText;
    ClientCode: TDBText;
    ClientName: TDBText;
    ClientWONumber: TDBText;
    ClientOrderNum: TDBText;
    WorkLong: TDBText;
    WorkLat: TDBText;
    WorkOrderID: TDBText;
    WorkAddressNumber: TDBText;
    City: TDBText;
    WorkAddressStreet: TDBText;
    State: TDBText;
    Zip: TDBText;
    CallerName: TDBText;
    MapPage: TDBText;
    MapRef: TDBText;
    ClosedDate: TDBText;
    procedure StatusListFilterRecord(DataSet: TDataSet; var Accept: Boolean); virtual;
    procedure ItemDblClick(Sender: TObject); //QMANTWO-577
  private
  protected
    FStatusedHow: string;
    FCurrentStatusOfSelectedWorkOrder: string;
    FWorkOrderSourceStr: string;  //QMANTWO-436 EB

    procedure SetInheritedFrameEvents; virtual; abstract;//Hook any dataset, etc events; inherited frames do not properly fire them.
    procedure CheckForAndLoadReferenceCodes; virtual;
    procedure AfterGotoWorkOrder; virtual;

  public
    procedure ClearSelection;
    property  StatusedHow: string read fStatusedHow write fStatusedHow;
    property  CurrentStatusOfSelectedWorkOrder: string read FCurrentStatusOfSelectedWorkOrder
              write fCurrentStatusOfSelectedWorkOrder;
    property  WorkOrderSourceStr: string read FWorkOrderSourceStr write FWorkOrderSourceStr;  //QMANTWO-436 EB
    procedure EnableEditing(Enabled: Boolean); virtual;
    procedure Load(ID:Integer); virtual;
    function ValidateWorkOrderFields: Boolean; virtual;
    procedure RaiseErrorOnFrame(const ErrorMessage: string;
              const WinControl: TWinControl); virtual;
    procedure SaveWorkOrderDetails; virtual;
    procedure CancelWorkOrder; virtual;
    procedure SetWorkOrderClosed(Value: Boolean); virtual;
    procedure Setup; virtual;
    procedure RetrieveStatus;
    procedure RefreshStatusFilter;
    procedure RefreshStatusList;
    function NeedToSave: Boolean; virtual;
    function WorkTypeisValidForWOSource(RowSrc: string): boolean;  //QMANTWO-436 EB
  end;

implementation
uses
  OdVclUtils, OdMiscUtils, OdExceptions, OdDbUtils, WorkOrderDetail;

{$R *.dfm}

{ TWODetailsBaseFrame }
procedure TWODetailBaseFrame.AfterGotoWorkOrder;
begin

end;

procedure TWODetailBaseFrame.CheckForAndLoadReferenceCodes;
begin
//Fill in on inherited frames
end;

procedure TWODetailBaseFrame.ClearSelection;
begin
  ClearSelectionsDBText(CustomerPanel);
  ClearSelectionsDBText(DetailsPanel);
  ClearSelectionsDBText(FeedDetailsPanel);
  ClearSelectionsDBText(EditingPanel);
  ClearSelectionsDBText(ButtonPanel);
  if (Self.Owner is TWorkOrderDetailForm) then
    (Self.Owner as TWorkOrderDetailForm).ClearSelection;
end;

procedure TWODetailBaseFrame.EnableEditing(Enabled: Boolean);
begin
  SetEnabledOnControlAndChildren(EditingPanel, Enabled);

  // keep these enabled so the scroll bars work
  EditingPanel.Enabled := True;
end;

procedure TWODetailBaseFrame.ItemDblClick(Sender: TObject);
begin
   ClearSelection;
    if Sender is TDBText then
    CopyDBText(Sender as TDBText)
  else if Sender is TDBMemo then
    CopyDBMemo(Sender as TDBMemo)
  else if Sender is TLabel then
    CopyLabelText(Sender as TLabel)
  else if Sender is TDBEdit then
    CopyDBEdit(Sender as TDBEdit);
end;

procedure TWODetailBaseFrame.Load(ID: Integer);
begin

end;

function TWODetailBaseFrame.NeedToSave: Boolean;
begin
  Result := EditingDataSet(WorkOrderDS.DataSet);
end;

procedure TWODetailBaseFrame.RaiseErrorOnFrame(const ErrorMessage: string;
const WinControl: TWinControl);
begin
  try
    if WinControl.CanFocus then
      WinControl.SetFocus;
  finally
  end;
  raise EOdDataEntryError.Create(ErrorMessage);
end;

procedure TWODetailBaseFrame.RefreshStatusFilter;
begin

end;

procedure TWODetailBaseFrame.RefreshStatusList;
begin
  RefreshDataSet(StatusList);
end;

procedure TWODetailBaseFrame.RetrieveStatus;
begin
  StatusList.Filtered := False;
  StatusList.Filtered := True;
end;

procedure TWODetailBaseFrame.SaveWorkOrderDetails;
begin
  if EditingDataSet(WorkOrderDS.DataSet) then begin
    WorkOrderDS.DataSet.UpdateRecord;  // Force an update in case the user did not leave the field
    EditDataSet(WorkOrderDS.DataSet);
  end;
end;

procedure TWODetailBaseFrame.CancelWorkOrder;
begin
  WorkOrderDS.DataSet.Cancel;
end;

procedure TWODetailBaseFrame.SetWorkOrderClosed(Value: Boolean);
begin
  ClosedDateLabel.Visible := Value;
  ClosedDate.Visible := Value;
end;

procedure TWODetailBaseFrame.StatusListFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  Status: string;
  ClientID: Integer;
begin
  inherited;
  Assert(Assigned(WorkOrderDS.DataSet));
  Status := DataSet.Fields[0].AsString;
  if Status = FCurrentStatusOfSelectedWorkOrder then
    Accept := True
  else begin
    ClientID := WorkOrderDS.DataSet.FieldByName('client_id').AsInteger;
    Accept := DM.ValidStatusForClient(ClientID, Status)
              and DM.ValidStatusForEmployee(Status); //and
//todo?      DM.ValidStatusForWorkOrder(FWorkOrderID, ??-> DataSet.FieldByName('meet_only').AsBoolean)

  end;
end;

function TWODetailBaseFrame.ValidateWorkOrderFields: Boolean;
begin
  Result := False;
end;

function TWODetailBaseFrame.WorkTypeisValidForWOSource(RowSrc: string): boolean;  //QMANTWO-436 EB
begin
  if (AnsiPos(WorkOrderSourceStr, RowSrc) > 0) or (RowSrc = '*') then
    Result := True
  else Result := False;
end;

procedure TWODetailBaseFrame.Setup;
begin
  SetInheritedFrameEvents;
end;

end.
