unit BaseExpandableWorkMgtGridFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableDataFrame, StdCtrls, ExtCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxClasses,
  cxCustomData, cxGridCustomTableView, cxGridLevel, cxGrid, DB, DMu, 
  cxVariants, QMConst, cxDataStorage, cxEdit, cxDBData,
  cxTextEdit, cxCheckBox, cxDropDownEdit, cxGridTableView, cxGridDBDataDefinitions,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, Menus,
  ActnList, WorkManageUtils, cxGridDBTableView, dbisamtb, DBCtrls;

type
 {QMANTWO-690 TGridState: We need a number of flags to track what the user is doing
                          while selecting, de-selecting or moving tickets. The
                          child form grid events will set these (don't set in here)}
  TGridState = record    //QMANTWO-690 EB
    CheckSelection: boolean;  {Flag to tell us to check selected itess during idle}
    LastDisplayedID: integer; {Focused item displayed at bottom - using this to stop multiple refreshes}
    LastOpenedID: integer;    {Last OPENED item}
    PrevFocusedID: integer;   {Used only for the ReselectList when returning to Work Management because LastDisplayed is reset sooner}
    FocusedRecChange: boolean;
    InitialDisplay: boolean;
    AmDragging: boolean;
    BottomItemHighlighted: boolean;
  end;

  TExpandableWorkMgtGridFrameBase = class(TExpandableDataFrameBase)
    BaseGrid: TcxGrid;
    BaseGridLevelDetail: TcxGridLevel;
    PanelDisplaySelectedItem: TPanel;
    ActionListFrame: TActionList;
    AcknowledgeAction: TAction;
    NormalPriorityAction: TAction;
    HighPriorityAction: TAction;
    ReleasePriorityAction: TAction;
    PriorityPopup: TPopupMenu;
    HighPriorityTicketMenuItem: TMenuItem;
    NormalPriorityTicketMenuItem: TMenuItem;
    ReleaseforWorkMenuItem: TMenuItem;
    ListTable: TDBISAMTable;
    DetailTable: TDBISAMTable;
    procedure ViewDblClick(Sender: TObject);
    procedure RescheduleDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure BaseGridEnter(Sender: TObject);
    procedure RescheduleDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure ItemDblClick(Sender: TObject);
  private
  protected
    FSelectedItemID: Integer;
    FPopulatingWorkSummary: Boolean;
    FShowingSummary: Boolean;
    FBlockItemUpdates: Boolean;
    FSelectedTreeNodeType: TNodeType; //What kind of node was selected in tree; i.e. Ticket Activity, Emergency, etc.
    FParentNodeReferenceName: string;
    FOnGridEnter: TNotifyEvent;
    FOnSelectItem: TItemSelectedEvent;
    FOnViewDblClick: TNotifyEvent;
    FOnRescheduleItemDragDrop: TDragDropEvent;
    FOnRescheduleItemDragOver: TDragOverEvent;
    FOnRefreshSelectedManager: TNotifyEvent; //Notify outside entities to refresh the selected manager
    FOnAfterExpand: TNotifyEvent; //Work that has to be done right after expansion; i.e. before selecting records
    function GetListView : TcxGridDBTableView;
    function GetSelectedItemID: Integer;
    procedure SetSelectedItemID(Value: Integer);
    function FocusedGridRecord: TcxCustomGridRecord;
    procedure SetGridAndViewEvents; virtual; abstract; //Necessary to programmatically hook common events in inherited frames or they won't fire
    function ItemIDColumn: TcxGridColumn; virtual; abstract;
    procedure SetSelectedGridItemID(const GridItemID: Integer); virtual;
    function SelectFocusedRecord(AGridView: TcxCustomGridTableView; Col: TcxGridDBColumn) : Boolean; //Elana - check to see if our value is correct
  public
    GridState: TGridState;
    ShowOpenItemsOnly: Boolean;
    RescheduledItemIDs: String;
    property ListView: TcxGridDBTableView read GetListView;
    property PopulatingWorkSummary: Boolean read FPopulatingWorkSummary;
    property ShowingSummary: Boolean read FShowingSummary write FShowingSummary;
    property SelectedTreeNodeType: TNodeType read FSelectedTreeNodeType write FSelectedTreeNodeType;
    property SelectedItemID: Integer read GetSelectedItemID write SetSelectedItemID;
    property ParentNodeReferenceName: string read FParentNodeReferenceName write FParentNodeReferenceName;
    property OnGridEnter: TNotifyEvent read FOnGridEnter write FOnGridEnter;
    property OnSelectItem: TItemSelectedEvent read FOnSelectItem write FOnSelectItem;
    property OnViewDblClick: TNotifyEvent read FOnViewDblClick write FOnViewDblClick;
    property OnRescheduleItemDragDrop: TDragDropEvent read FOnRescheduleItemDragDrop write FOnRescheduleItemDragDrop;
    property OnRescheduleItemDragOver: TDragOverEvent read FOnRescheduleItemDragOver write FOnRescheduleItemDragOver;
    property OnRefreshSelectedManager: TNotifyEvent read FOnRefreshSelectedManager write FOnRefreshSelectedManager;
    property OnAfterExpand: TNotifyEvent read FOnAfterExpand write FOnAfterExpand;
    procedure RefreshItem;
    procedure ClearListTable;
    procedure Setup(EntityDescription: string; ExpandHandler: TNotifyEvent); override;
    procedure Expand; override;
    procedure RefreshData; virtual;
    function GetCurrentItemID(var ItemID: Integer): Boolean; virtual;
    procedure SelectFirstGridRow; virtual;
    function HaveFocusedItem: Boolean;
    procedure RefreshCaptionCounts; virtual; abstract;
    procedure UpdateFrameData(const SourceDataSet: TDataSet); virtual; abstract;
    procedure ShowSelectedItem; virtual; abstract;
    procedure UpdateItemInCache; virtual; abstract;
    procedure CopyDBText(ADBText: TDBText);   //QMANTWO-577  EB
    procedure CopyDBMemo(ADBMemo: TDBMemo);
    procedure ClearSelectionsDBText;
  end;

var
  ExpandableWorkMgtGridFrameBase: TExpandableWorkMgtGridFrameBase;

implementation

uses OdDBUtils, OdHourglass, OdCxUtils, ClipBrd;

{$R *.dfm}

{ TExpandableGridFrameBase }

procedure TExpandableWorkMgtGridFrameBase.Setup(EntityDescription: string;
  ExpandHandler: TNotifyEvent);
begin
  inherited;
  SetGridAndViewEvents;
end;

function TExpandableWorkMgtGridFrameBase.GetCurrentItemID(var ItemID: Integer): Boolean;
begin
  Assert(Assigned(ListView));

  if ListView.DataController.RowCount > 0 then begin
    ItemID := SelectedItemID;
    Result := (ItemID <> InvalidID);
  end
  else
    raise Exception.Create('No selected item.');
end;

procedure TExpandableWorkMgtGridFrameBase.BaseGridEnter(Sender: TObject);
var
  ItemID: Integer;
begin
  inherited;
  if Assigned(FOnSelectItem) then
    if GetCurrentItemID(ItemID) then
      FOnSelectItem(Self, ItemID);
end;

function TExpandableWorkMgtGridFrameBase.FocusedGridRecord: TcxCustomGridRecord;
begin
  Result := nil;
  if (FSelectedItemID > 0) and Assigned(ListView) then
    Result := ListView.Controller.FocusedRecord;
end;

function TExpandableWorkMgtGridFrameBase.HaveFocusedItem: Boolean;
begin
  Result := False;
  if HaveFocusedGridViewRow(ListView) then
    if BaseGrid.IsFocused or ShowingSummary then
      Result := True;
end;


procedure TExpandableWorkMgtGridFrameBase.ItemDblClick(Sender: TObject);
begin
  if Sender is TDBText then
    CopyDBText(Sender as TDBText)
  else if Sender is TDBMemo then
    CopyDBMemo(Sender as TDBMemo);
  GridState.BottomItemHighlighted := True;
end;

procedure TExpandableWorkMgtGridFrameBase.CopyDBMemo(ADBMemo: TDBMemo);
begin
  ClearSelectionsDBText;
  ADBMemo.Color := HIGHLIGHTCOLOR; //clSkyBlue;
  Clipboard.AsText := ADBMemo.Text;
  GridState.BottomItemHighlighted := True;
end;

procedure TExpandableWorkMgtGridFrameBase.CopyDBText(ADBText: TDBText);
begin
  ClearSelectionsDBText;
  ADBText.Color :=  HIGHLIGHTCOLOR;  //clSkyBlue;
  ADBText.Transparent := False;
 Clipboard.AsText := ADBText.Caption;
 GridState.BottomItemHighlighted := True;
end;


function TExpandableWorkMgtGridFrameBase.SelectFocusedRecord(AGridView: TcxCustomGridTableView; Col: TcxGridDBColumn): Boolean;
{Elana - This is a correction to a grid issue when the user drags mouse down quickly
          after selecting a record.  The "focused" record is used to display the data
          at the bottom of the grid.  However, in this case, the focus changes
          but another item stays selected. This procedure changes the focused row
          to selected, so that it is obvious which record the user is viewing}
var
  FocusedTicket : string;
  OtherTicket : string;
begin
  inherited;
  //Elana - Only want to trigger this when there is a discrepancy, otherwise, it will
  //        cause a grid side-effect
  {ColTicketNumber.ID}
  Result := False;
   if AGridView.Controller.SelectedRecordCount > 0 then begin
     FocusedTicket := AGridView.Controller.FocusedRecord.Values[Col.ID];
     OtherTicket := AGridView.Controller.SelectedRecords[0].Values[Col.ID];

     //Only trigger this if they don't match
     if FocusedTicket <> OtherTicket then begin
       AGridView.Controller.FocusedRecord.Selected := True;
       Result := True;
     end;
   end;
end;

procedure TExpandableWorkMgtGridFrameBase.Expand;
begin
  inherited;
  if Assigned(FOnAfterExpand) then
    FOnAfterExpand(self);
  SelectFirstGridRow;
end;

procedure TExpandableWorkMgtGridFrameBase.SelectFirstGridRow;
var
  i: integer;
  Continue: Boolean;
begin
{EB QMANTWO-564 MultiSelect Ticket Priority -
      This has been rewritten to work more reliably with WM Ticket screen}
  Continue := True;
  ListView.Controller.ClearSelection; //EB QMANTWO-564  7/6  To make sure that residual selections are cleared when returning to WorkMgmt
  {EB - If the first one is a data row, focus it but don't select it
         Because it takes considerable time to load details section, we are
         NOT loading the first row.  This should speed up navigation between locators
         and only require the user to explicitly click the first row to get details
         displayed.  The WorkManagement Idle will not set the first row to selected
         when InitialDisplay is True}

  if (ListView.ViewData.RecordCount > 0) and (ListView.Controller.SelectedRecordCount = 0) then begin  //7/18 Testing to see if this was contributing to AV
    if ListView.ViewData.Records[0].IsData then begin
     ListView.ViewData.Records[0].Focused := True;
     if ListView.ViewData.RecordCount = 1 then
       SelectedItemId :=  ListView.ViewData.Records[0].Values[ItemIDColumn.ID];    // 7/18 EB 5/31 EB QMANTWO-564 Major change may affect WM Work Orders and WM Damages
     Continue := FALSE;
    end;
  end
  else begin
    for i := 0 to ListView.ViewData.RecordCount - 1 do begin

      if Continue then begin
        if ListView.ViewData.Records[i].Expandable then begin
          ListView.ViewData.Collapse(True);
          ListView.Controller.FocusedRow.Expanded := True;
        end;
        if ListView.ViewData.Records[i].IsData then begin
          ListView.ViewData.Records[i].Focused := True;    // QMANTWO-564 EB focused, but do not select  5/30
          SelectedItemId :=  ListView.ViewData.Records[i].Values[ItemIDColumn.ID];
          Continue := False;
        end;
          //Collapse to get rid of unrelated, expanded rows from previous employee and expand top row
        if ListView.Controller.FocusedRow.Expandable then begin
          ListView.ViewData.Collapse(True);
          ListView.Controller.FocusedRow.Expanded := True;
        end;
//
//        {EB - Reset}
//        ListView.ViewData.Records[i].Focused := True;
//        Continue := FALSE;
      end
    end;
  end;
end;


function TExpandableWorkMgtGridFrameBase.GetSelectedItemID: Integer;
begin
  Result := InvalidID;
  if (FocusedGridRecord is TcxGridGroupRow) then
    Exit;

  Result := FSelectedItemID;
end;

function TExpandableWorkMgtGridFrameBase.GetListView: TcxGridDBTableView;
begin
  Result := TcxGridDBTableView(BaseGrid.FocusedView);
end;

procedure TExpandableWorkMgtGridFrameBase.SetSelectedItemID(Value: Integer);
begin
  if Value <> FSelectedItemID then begin
    FSelectedItemID := Value;
    ShowSelectedItem;
  end;
end;

procedure TExpandableWorkMgtGridFrameBase.RefreshData;
begin
  RefreshCaptionCounts;
end;

procedure TExpandableWorkMgtGridFrameBase.RescheduleDragOver(Sender,
  Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;
  Accept := Source is TcxDragControlObject;
end;

procedure TExpandableWorkMgtGridFrameBase.RescheduleDragDrop(Sender,
  Source: TObject; X, Y: Integer);
begin
  inherited;
  if Assigned(FOnRescheduleItemDragDrop) then
    FOnRescheduleItemDragDrop(Sender, Source, X, Y);
end;

procedure TExpandableWorkMgtGridFrameBase.ViewDblClick(Sender: TObject);
begin
  inherited;
  try
  if Assigned(ListView) then     //QM-742 EB Isolated Machne and Dev Ex Upgrade (explicit)
    If HaveFocusedGridViewRow(ListView) then
      if Assigned(FOnViewDblClick) then
        FOnViewDblClick(Sender);
  except
    ShowMessage('Grid Issue detected');
  end;
end;

procedure TExpandableWorkMgtGridFrameBase.RefreshItem;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  ListTable.DisableControls;
  try
    if SelectedItemID <> 0 then begin
      DetailTable.Close;
      UpdateItemInCache;
      ShowSelectedItem;
    if Assigned(FOnRefreshSelectedManager) then
      FOnRefreshSelectedManager(Self);
    end;
  finally
    ListTable.EnableControls;
  end;
end;

procedure TExpandableWorkMgtGridFrameBase.ClearListTable;
begin
  ListTable.DisableControls;
  try
    ListTable.Close;
    if ListTable.Exists then
      ListTable.EmptyTable;
    SelectedItemID := 0;
  finally
    ListTable.EnableControls;
  end;
end;

procedure TExpandableWorkMgtGridFrameBase.ClearSelectionsDBText;     //QMANTWO-577 EB
var
  i : integer;
begin
  if GridState.BottomItemHighlighted then begin
    {Clear any other "selection"}
    for i := 0 to PanelDisplaySelectedItem.ControlCount - 1 do begin
      if PanelDisplaySelectedItem.Controls[i] is TDBText then begin
        (PanelDisplaySelectedItem.Controls[i] as TDBText).Transparent := True;
        (PanelDisplaySelectedItem.Controls[i] as TDBText).Color := PanelDisplaySelectedItem.Color;
      end
      else if PanelDisplaySelectedItem.Controls[i] is TDBMemo then begin
        (PanelDisplaySelectedItem.Controls[i] as TDBMemo).Color := PanelDisplaySelectedItem.Color;
      end;
    end;
    GridState.BottomItemHighlighted := False;
  end;
end;

procedure TExpandableWorkMgtGridFrameBase.SetSelectedGridItemID(const GridItemID: Integer);
//This method sets the selected Ticket/WorkOrder/Damage ID if not populating the list
begin
  if (not FPopulatingWorkSummary) then
    SelectedItemID := GridItemID;
end;

procedure TExpandableWorkMgtGridFrameBase.ViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if (Assigned(AFocusedRecord) and (AFocusedRecord is TcxGridDataRow) and (not FBlockItemUpdates)) then begin
    SetSelectedGridItemID(AFocusedRecord.Values[ItemIDColumn.Index]);
    ShowingSummary := HasRecords(DetailTable);
  end;
end;

end.

