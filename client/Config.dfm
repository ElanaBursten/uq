inherited ConfigForm: TConfigForm
  Left = 260
  Top = 234
  Caption = 'Configuration / Options'
  ClientHeight = 635
  ClientWidth = 1016
  Menu = MainMenu1
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 709
    Top = 6
    Width = 33
    Height = 13
    Caption = 'Errors:'
  end
  object Label3: TLabel
    Left = 749
    Top = 6
    Width = 20
    Height = 13
    Caption = '----'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object ConfigPageCtrl: TPageControl
    Left = 0
    Top = 0
    Width = 1016
    Height = 635
    ActivePage = tabGeneral
    Align = alClient
    TabOrder = 0
    object tabGeneral: TTabSheet
      Caption = 'General'
      object GeneralPages: TPageControl
        Left = 0
        Top = 0
        Width = 1008
        Height = 607
        ActivePage = tsApplication
        Align = alClient
        TabOrder = 0
        object tsApplication: TTabSheet
          Caption = 'Application'
          ImageIndex = 1
          object pnlApplication: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 579
            Align = alClient
            TabOrder = 0
            object Database: TGroupBox
              Left = 0
              Top = 0
              Width = 166
              Height = 114
              Caption = 'Application'
              TabOrder = 0
              object UpdateButton: TButton
                Left = 7
                Top = 21
                Width = 150
                Height = 25
                Caption = 'Auto &Upgrade'
                TabOrder = 0
                OnClick = UpdateButtonClick
              end
              object ChangePassswordButton: TButton
                Left = 7
                Top = 52
                Width = 150
                Height = 25
                Caption = 'Change &Password'
                TabOrder = 1
                OnClick = ChangePassswordButtonClick
              end
            end
            object grpUserInfo: TGroupBox
              Left = 172
              Top = 3
              Width = 166
              Height = 111
              Caption = 'User'
              TabOrder = 1
              object ShowRightsButton: TButton
                Left = 8
                Top = 18
                Width = 148
                Height = 25
                Caption = 'Show Rights'
                TabOrder = 0
                OnClick = ShowRightsButtonClick
              end
            end
          end
          object GetCRCButton: TButton
            Left = 7
            Top = 83
            Width = 150
            Height = 25
            Caption = 'Get CRC'
            TabOrder = 1
            Visible = False
            OnClick = GetCRCButtonClick
          end
        end
        object tsDatabaseMaint: TTabSheet
          Caption = 'Data Maintenance (DBISAM)'
          object pnlDataMaint: TPanel
            Left = 0
            Top = 117
            Width = 1000
            Height = 462
            Align = alClient
            TabOrder = 0
            object TableGrid: TcxGrid
              Left = 1
              Top = 1
              Width = 998
              Height = 460
              Align = alClient
              TabOrder = 0
              Visible = False
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              object TableView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = ViewTableSrc
                DataController.Filter.MaxValueListCount = 1000
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Filtering.ColumnPopup.MaxDropDownItemCount = 12
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.InvertSelect = False
                OptionsSelection.MultiSelect = True
                OptionsView.GroupFooters = gfVisibleWhenExpanded
                Preview.AutoHeight = False
                Preview.MaxLineCount = 2
              end
              object TableLevel: TcxGridLevel
                GridView = TableView
              end
            end
          end
          object PnlGeneralTop: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 117
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object MiscGroupBx: TGroupBox
              Left = 258
              Top = 4
              Width = 165
              Height = 113
              Caption = 'Data Refresh'
              TabOrder = 0
              object DiscardButton: TButton
                Left = 7
                Top = 21
                Width = 150
                Height = 25
                Caption = 'Delete All Local Data'
                TabOrder = 0
                Visible = False
                OnClick = DiscardButtonClick
              end
              object ReloadTicketButton: TButton
                Left = 7
                Top = 52
                Width = 150
                Height = 25
                Caption = 'Reload Ticket Data'
                TabOrder = 1
                Visible = False
                OnClick = ReloadTicketButtonClick
              end
              object btnReloadRefernce: TButton
                Left = 7
                Top = 83
                Width = 150
                Height = 25
                Caption = 'Reload Reference Data'
                TabOrder = 2
                OnClick = btnReloadRefernceClick
              end
            end
            object GetSQLButton: TButton
              Left = 175
              Top = 92
              Width = 77
              Height = 25
              Caption = 'Copy SQL'
              TabOrder = 1
              OnClick = GetSQLButtonClick
            end
            object gpbxSync: TGroupBox
              Left = 3
              Top = 7
              Width = 249
              Height = 81
              Caption = 'Sync'
              TabOrder = 2
              object lblLastSync: TLabel
                Left = 3
                Top = 18
                Width = 128
                Height = 13
                Caption = 'Last Sync (DBISAM Date): '
              end
              object lblLastSyncDate: TLabel
                Left = 48
                Top = 31
                Width = 47
                Height = 13
                Caption = '<DATE>'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object btnResetSyncStatusDates: TBitBtn
                Left = 48
                Top = 50
                Width = 126
                Height = 20
                Caption = 'Reset SyncStatus Dates'
                TabOrder = 0
                OnClick = btnResetSyncStatusDatesClick
              end
            end
          end
          object TableName: TComboBox
            Left = 3
            Top = 94
            Width = 166
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 0
            TabOrder = 2
            OnChange = TableNameChange
          end
        end
        object tsComputerInfo: TTabSheet
          Caption = 'Computer Info and Plats'
          ImageIndex = 2
          object pnlComputerInfo: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 579
            Align = alClient
            TabOrder = 0
            object lblConfigStatus: TLabel
              Left = 300
              Top = 42
              Width = 72
              Height = 13
              Caption = 'lblConfigStatus'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clMaroon
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              Visible = False
            end
            object lblPlatsUpdated: TLabel
              Left = 300
              Top = 24
              Width = 74
              Height = 13
              Caption = 'Plats Updated: '
            end
            object GrpBxComputerInfo: TGroupBox
              Left = 11
              Top = 9
              Width = 251
              Height = 142
              Caption = 'Computer Info'
              TabOrder = 0
              object lblSerialNumber: TLabel
                Left = 15
                Top = 51
                Width = 41
                Height = 13
                Caption = 'Serial #:'
              end
              object lblComputer: TLabel
                Left = 15
                Top = 69
                Width = 51
                Height = 13
                Caption = 'Computer:'
              end
              object lblSimCardNum: TLabel
                Left = 15
                Top = 88
                Width = 78
                Height = 13
                Caption = 'Sim Card ICC #:'
              end
              object lblUser: TLabel
                Left = 15
                Top = 15
                Width = 56
                Height = 13
                Caption = 'Comp User:'
              end
              object lblPhone: TLabel
                Left = 15
                Top = 33
                Width = 34
                Height = 13
                Caption = 'Phone:'
              end
              object lblOSVersion: TLabel
                Left = 15
                Top = 123
                Width = 56
                Height = 13
                Caption = 'OS Version:'
              end
              object Label1: TLabel
                Left = 15
                Top = 106
                Width = 67
                Height = 13
                Caption = 'Cell Device #:'
              end
              object edtUser: TEdit
                Left = 97
                Top = 15
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
                Text = 'edtUser'
              end
              object edtPhone: TEdit
                Left = 97
                Top = 33
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 1
                Text = 'edtPhone'
              end
              object edtSerialNumber: TEdit
                Left = 97
                Top = 51
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 2
                Text = 'edtSerialNumber'
              end
              object edtComputer: TEdit
                Left = 97
                Top = 69
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 3
                Text = 'edtComputer'
              end
              object edtSimCard: TEdit
                Left = 97
                Top = 87
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 4
                Text = 'None Found'
              end
              object edtOSVersion: TEdit
                Left = 97
                Top = 123
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 5
                Text = 'edtOSVersion'
              end
              object EdtCellDeviceID: TEdit
                Left = 97
                Top = 105
                Width = 145
                Height = 17
                Margins.Left = 10
                BorderStyle = bsNone
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 6
                Text = 'None Found'
              end
            end
            object edtPlatsUpdated: TEdit
              Left = 380
              Top = 21
              Width = 205
              Height = 21
              TabOrder = 1
              Text = 'edtPlatsUpdated'
            end
          end
        end
      end
    end
    object tabTicketColors: TTabSheet
      Caption = 'Application Colors'
      ImageIndex = 1
      TabVisible = False
      object Colors: TPageControl
        Left = 0
        Top = 0
        Width = 1008
        Height = 607
        ActivePage = GeocodeColorsTab
        Align = alClient
        Enabled = False
        TabOrder = 0
        Visible = False
        object TicketColorsTab: TTabSheet
          Caption = 'Ticket Colors'
          object AlternateRowColorButton: TButton
            Left = 12
            Top = 6
            Width = 161
            Height = 25
            Caption = 'Alternate Row Color'
            TabOrder = 0
            OnClick = AlternateRowColorButtonClick
          end
          object DueTicketColorButton: TButton
            Left = 12
            Top = 38
            Width = 161
            Height = 25
            Caption = 'Due Today Ticket Color'
            TabOrder = 1
            OnClick = DueTicketColorButtonClick
          end
          object CriticalTicketColorButton: TButton
            Left = 12
            Top = 70
            Width = 161
            Height = 25
            Caption = 'Critical Ticket Color'
            TabOrder = 2
            OnClick = CriticalTicketColorButtonClick
          end
          object DoNotMarkTicketColorButton: TButton
            Left = 12
            Top = 102
            Width = 161
            Height = 25
            Caption = 'Do Not Mark Ticket Color'
            TabOrder = 3
            OnClick = DoNotMarkTicketColorButtonClick
          end
        end
        object GeocodeColorsTab: TTabSheet
          Caption = 'Geocode Colors'
          ImageIndex = 1
          object HighestGeocodePrecisionColorButton: TButton
            Left = 12
            Top = 6
            Width = 161
            Height = 25
            Caption = 'Highest Precision Color'
            TabOrder = 0
            OnClick = HighestGeocodePrecisionColorButtonClick
          end
          object AcceptableGeocodePrecisionColorButton: TButton
            Left = 12
            Top = 38
            Width = 161
            Height = 25
            Caption = 'Acceptable Precision Color'
            TabOrder = 1
            OnClick = AcceptableGeocodePrecisionColorButtonClick
          end
          object ZeroGeocodeMatchesColorButton: TButton
            Left = 12
            Top = 70
            Width = 161
            Height = 25
            Caption = 'Zero Match Color'
            TabOrder = 2
            OnClick = ZeroGeocodeMatchesColorButtonClick
          end
        end
      end
    end
    object TabCamera: TTabSheet
      Caption = 'TabCamera'
      ImageIndex = 2
      TabVisible = False
      object DriveLettersGroup: TGroupBox
        Left = 0
        Top = 1
        Width = 178
        Height = 161
        Caption = 'Camera Drive Letters'
        TabOrder = 0
        object DriveLetter: TEdit
          Left = 8
          Top = 24
          Width = 41
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 1
          TabOrder = 0
        end
        object AddDriveLetterButton: TButton
          Left = 55
          Top = 22
          Width = 55
          Height = 25
          Caption = '&Add'
          TabOrder = 1
          OnClick = AddDriveLetterButtonClick
        end
        object RemoveDriveLetterButton: TButton
          Left = 116
          Top = 22
          Width = 55
          Height = 25
          Caption = '&Remove'
          TabOrder = 2
          OnClick = RemoveDriveLetterButtonClick
        end
        object DriveLettersListBox: TListBox
          Left = 8
          Top = 56
          Width = 162
          Height = 97
          ItemHeight = 13
          TabOrder = 3
        end
      end
    end
    object TabPlats: TTabSheet
      Caption = 'Plats'
      ImageIndex = 3
      TabVisible = False
      object pnlClient: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 572
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          1008
          572)
        object pgCtrlPlatsMgr: TcxPageControl
          Left = 0
          Top = 24
          Width = 1008
          Height = 548
          Align = alClient
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          Properties.ActivePage = tabPlatAge
          Properties.CustomButtons.Buttons = <>
          ClientRectBottom = 544
          ClientRectLeft = 4
          ClientRectRight = 1004
          ClientRectTop = 24
          object tabPlatAge: TcxTabSheet
            Caption = 'Plats on Machine'
            ImageIndex = 0
            object MyPlatGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 520
              Align = alClient
              TabOrder = 0
              object MyPlatGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = dsPlats
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsView.NoDataToDisplayInfoText = '...'
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.Indicator = True
                Styles.ContentEven = SharedDevExStyleData.AltColorEven
                Styles.Selection = SharedDevExStyleData.GridSelectionStyle
                object ColCompany: TcxGridDBColumn
                  DataBinding.FieldName = 'Company'
                  Options.Editing = False
                  Width = 54
                end
                object ColCenter: TcxGridDBColumn
                  DataBinding.FieldName = 'Center'
                  Options.Editing = False
                  Width = 42
                end
                object ColAge: TcxGridDBColumn
                  DataBinding.FieldName = 'Age'
                  Options.Editing = False
                end
                object ColLocalPath: TcxGridDBColumn
                  Caption = 'Local Path'
                  DataBinding.FieldName = 'LocalPath'
                  Options.Editing = False
                  Width = 277
                end
                object ColTopName: TcxGridDBColumn
                  Caption = 'Company Name'
                  DataBinding.FieldName = 'CompanyName'
                  Options.Editing = False
                  Width = 101
                end
                object ColLastUpdated: TcxGridDBColumn
                  Caption = 'Last Updated'
                  DataBinding.FieldName = 'LastUpdated'
                  Options.Editing = False
                  Width = 146
                end
                object ColRecIndex: TcxGridDBColumn
                  Caption = 'Idx'
                  DataBinding.FieldName = 'RecIndex'
                  Options.Editing = False
                  Width = 32
                end
              end
              object MyPlatGridLevel1: TcxGridLevel
                GridView = MyPlatGridView
              end
            end
          end
          object tabUpdateList: TcxTabSheet
            Caption = 'Update List'
            ImageIndex = 1
            object UpdateGrid: TcxGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 520
              Align = alClient
              TabOrder = 0
              object UpdateGridView: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                OnCustomDrawCell = UpdateGridViewCustomDrawCell
                DataController.DataSource = dsUpdates
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsData.CancelOnExit = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.Indicator = True
                Styles.Selection = SharedDevExStyleData.GridSelectionStyle
                object ColUpdatesChecked: TcxGridDBColumn
                  DataBinding.FieldName = 'CheckedOff'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Properties.DisplayGrayed = 'False'
                  Properties.NullStyle = nssUnchecked
                  MinWidth = 10
                  Styles.Content = SharedDevExStyleData.BoldLargeFontStyle
                  Width = 12
                  IsCaptionAssigned = True
                end
                object ColUpdatesMethod: TcxGridDBColumn
                  DataBinding.FieldName = 'Method'
                  Visible = False
                  Options.Editing = False
                  Width = 71
                end
                object ColUpdatesCompany: TcxGridDBColumn
                  DataBinding.FieldName = 'Company'
                  Options.Editing = False
                  Width = 47
                end
                object ColUpdatesCenter: TcxGridDBColumn
                  DataBinding.FieldName = 'Center'
                  Options.Editing = False
                  Width = 44
                end
                object ColUpdatesPM7Age: TcxGridDBColumn
                  Caption = 'PM7 Age'
                  DataBinding.FieldName = 'PM7Age'
                  Options.Editing = False
                  Width = 67
                end
                object ColUPdatesStatus: TcxGridDBColumn
                  DataBinding.FieldName = 'Status'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Editing = False
                  Width = 57
                end
                object ColUpdatesPM7File: TcxGridDBColumn
                  Caption = 'PM7 File'
                  DataBinding.FieldName = 'PM7File'
                  Options.Editing = False
                  Width = 266
                end
                object ColUpdatesLocalFileAge: TcxGridDBColumn
                  Caption = 'Local Plats'
                  DataBinding.FieldName = 'LocalFileAge'
                  Options.Editing = False
                  Width = 129
                end
                object ColLocInfo: TcxGridDBColumn
                  Caption = 'Local update.pm'
                  DataBinding.FieldName = 'LocalInfo'
                  Options.Editing = False
                  Width = 96
                end
                object ColCompanyName: TcxGridDBColumn
                  Caption = 'Local Co Name'
                  DataBinding.FieldName = 'CompanyName'
                  Width = 85
                end
                object ColExtractTo: TcxGridDBColumn
                  Caption = 'Extract To (default)'
                  DataBinding.FieldName = 'TargetPath'
                  Width = 183
                end
              end
              object UpdateGridLevel1: TcxGridLevel
                GridView = UpdateGridView
              end
            end
          end
          object tabUpdateLog: TcxTabSheet
            Caption = 'Update Log'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ImageIndex = 2
            ParentFont = False
            object edUpdateLog: TRichEdit
              Left = 0
              Top = 0
              Width = 1000
              Height = 520
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 24
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 1
          object lblPlatsUpdated2: TLabel
            Left = 8
            Top = 6
            Width = 97
            Height = 13
            Caption = 'Plats Last Updated: '
          end
          object edPlatsUpdated2: TLabel
            Left = 107
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblLocalNum: TLabel
            Left = 258
            Top = 6
            Width = 54
            Height = 13
            Caption = 'Local Plats:'
          end
          object edtLocalNum: TLabel
            Left = 315
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblWarnings: TLabel
            Left = 573
            Top = 6
            Width = 49
            Height = 13
            Caption = 'Warnings:'
          end
          object edtWarnings: TLabel
            Left = 627
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblNewMissing: TLabel
            Left = 338
            Top = 6
            Width = 25
            Height = 13
            Caption = 'New:'
          end
          object edtNewMissing: TLabel
            Left = 366
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblUpdates: TLabel
            Left = 391
            Top = 6
            Width = 67
            Height = 13
            Caption = 'Need Update:'
          end
          object edtUpdates: TLabel
            Left = 462
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblDuplicates: TLabel
            Left = 488
            Top = 6
            Width = 53
            Height = 13
            Caption = 'Duplicates:'
          end
          object edtDuplicates: TLabel
            Left = 547
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblErrors: TLabel
            Left = 653
            Top = 6
            Width = 52
            Height = 13
            Caption = 'File Errors:'
          end
          object EdtErrors: TLabel
            Left = 712
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object lblUpdated: TLabel
            Left = 738
            Top = 6
            Width = 51
            Height = 13
            Caption = 'Updated:'
            Color = 10930928
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object edtUpdated: TLabel
            Left = 795
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = 10930928
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object edtUpdateErrors: TLabel
            Left = 898
            Top = 6
            Width = 20
            Height = 13
            Caption = '----'
            Color = 10930928
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object lblUpdateErrors: TLabel
            Left = 821
            Top = 6
            Width = 71
            Height = 13
            Caption = 'Update Errors:'
            Color = 10930928
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
        end
        object btnClearChecks: TButton
          Left = 903
          Top = 24
          Width = 103
          Height = 20
          Anchors = [akTop, akRight]
          Caption = 'Clear Checkmarks'
          TabOrder = 2
          OnClick = btnClearChecksClick
        end
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 572
        Width = 1008
        Height = 35
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 1
        DesignSize = (
          1008
          35)
        object btnPlatAge: TButton
          Left = 5
          Top = 6
          Width = 145
          Height = 25
          Caption = '1 - How old are my plats?'
          TabOrder = 0
          OnClick = btnPlatAgeClick
        end
        object btnWhatToUpdate: TButton
          Left = 162
          Top = 6
          Width = 145
          Height = 25
          Caption = '2 - What can I update?'
          TabOrder = 1
          OnClick = btnWhatToUpdateClick
        end
        object btnUpdateThisMachine: TButton
          Left = 313
          Top = 6
          Width = 145
          Height = 25
          Caption = '3 - Update this machine'
          Enabled = False
          TabOrder = 2
          OnClick = btnUpdateThisMachineClick
        end
        object btnCancel: TButton
          Left = 898
          Top = 6
          Width = 106
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Cancel'
          Enabled = False
          TabOrder = 3
          OnClick = btnCancelClick
        end
      end
    end
  end
  object ColorDialog: TColorDialog
    Options = [cdFullOpen, cdSolidColor]
    Left = 8
    Top = 368
  end
  object ViewTableSrc: TDataSource
    DataSet = TableData
    Left = 72
    Top = 232
  end
  object TableData: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from employee_right')
    Params = <>
    ReadOnly = True
    Left = 32
    Top = 232
  end
  object ChildTable: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    ReadOnly = True
    Left = 32
    Top = 272
  end
  object MainMenu1: TMainMenu
    Left = 784
    Top = 192
  end
  object dsPlats: TDataSource
    Left = 752
    Top = 192
  end
  object dsUpdates: TDataSource
    Left = 776
    Top = 232
  end
  object QrySync: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct LastSyncDown from syncstatus')
    Params = <>
    ReadOnly = True
    Left = 432
    Top = 56
  end
end
