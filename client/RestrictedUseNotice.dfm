inherited RestrictedUseForm: TRestrictedUseForm
  Caption = 'Restricted Use'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 548
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    BevelWidth = 2
    BorderWidth = 8
    Caption = ' '
    TabOrder = 0
    object NoticeMemo: TMemo
      Left = 8
      Top = 8
      Width = 532
      Height = 384
      Align = alClient
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'NoticeMemo')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
  end
end
