object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 890
  Width = 937
  object Database1: TDBISAMDatabase
    EngineVersion = '4.44 Build 3'
    Connected = True
    DatabaseName = 'DB1'
    Directory = 'data'
    SessionName = 'Default'
    Left = 56
    Top = 16
  end
  object TicketKindCount: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct ticket_id'
      
        ' from ticket inner join locate on ticket.ticket_id=locate.ticket' +
        '_id'
      ' where locate.locator_id=:emp_id'
      '  and locate.active'
      '  and not locate.closed'
      '  and kind=:kind'
      '  and ((do_not_mark_before is NULL)'
      '    or (do_not_mark_before <= :last_sync))'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end
      item
        DataType = ftString
        Name = 'kind'
      end
      item
        DataType = ftUnknown
        Name = 'last_sync'
      end>
    Left = 213
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end
      item
        DataType = ftString
        Name = 'kind'
      end
      item
        DataType = ftUnknown
        Name = 'last_sync'
      end>
  end
  object Ticket: TDBISAMTable
    AfterOpen = TicketAfterOpen
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'ticket'
    Left = 213
    Top = 12
  end
  object Locate: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'locate'
    Left = 318
    Top = 16
  end
  object StatusList: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'statuslist'
    Left = 211
    Top = 411
  end
  object Reference: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    IndexFieldNames = 'type;sortby'
    ReadOnly = True
    TableName = 'reference'
    Left = 56
    Top = 232
  end
  object CallCenters: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select cc_code, cc_name from call_center'
      'where active'
      'order by cc_code')
    Params = <>
    Left = 136
    Top = 196
  end
  object TicketNotesData: TDBISAMQuery
    Filtered = True
    BeforeOpen = TicketNotesDataBeforeOpen
    OnCalcFields = TicketNotesDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select notes.*, e.short_name, False on_cache'
      'from notes'
      '  left join users u on u.uid = notes.uid'
      '  left join employee e on e.emp_id = u.emp_id'
      'where notes.active=1'
      'and'
      '  ('
      
        '    (notes.foreign_type = :foreign_type_ticket and notes.foreign' +
        '_id = :ticket_id)'
      '    or'
      
        '    (notes.foreign_type = :foreign_type_locate and notes.foreign' +
        '_id in (select locate_id from locate where ticket_id = :ticket_i' +
        'd))'
      '  )')
    Params = <
      item
        DataType = ftInteger
        Name = 'foreign_type_ticket'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'foreign_type_locate'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 214
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'foreign_type_ticket'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'foreign_type_locate'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object Notes: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'notes'
    Left = 56
    Top = 336
  end
  object CallCenterClients: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select client.*'
      'from client'
      'where (client.call_center = :cc)'
      '  and client.active'
      
        ' and ((client.parent_client_id IS NULL) OR (client.parent_client' +
        '_id = 0))'
      
        '  and not (client.client_id in (select client_id from locate whe' +
        're (ticket_id = :ticket_id) and (status <> '#39'-N'#39')))'
      
        '  and not (client.oc_code in (select client_code from locate whe' +
        're (ticket_id = :ticket_id) and (status <> '#39'-N'#39')))'
      'order by oc_code')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cc'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 137
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cc'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object Offices: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select office_id, office_name'
      'from office'
      'where'
      '  office_name <> '#39#39
      'order by office_name')
    Params = <>
    Left = 56
    Top = 128
  end
  object LocateHoursData: TDBISAMQuery
    OnCalcFields = LocateHoursDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select locate_hours.*'
      'from locate_hours'
      '  inner join locate on locate_hours.locate_id = locate.locate_id'
      '  inner join ticket on ticket.ticket_id = locate.ticket_id'
      'where ticket_id = :ticket_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 318
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object LocateLookup: TDBISAMTable
    BeforeOpen = LocateLookupBeforeOpen
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    ReadOnly = True
    TableName = 'locate'
    Left = 318
    Top = 408
  end
  object SaveLocateHours: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'locate_hours'
    Left = 318
    Top = 456
  end
  object FollowupTicketTypes: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from followup_ticket_type'
      'where ((call_center = '#39'*'#39')'
      '  or (call_center = :call_center))'
      '  and active'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'call_center'
      end>
    Left = 213
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'call_center'
      end>
  end
  object Attachment: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'attachment'
    Left = 506
    Top = 28
  end
  object UploadLocation: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from upload_location where location_id = :LocationID')
    Params = <
      item
        DataType = ftUnknown
        Name = 'LocationID'
      end>
    Left = 588
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'LocationID'
      end>
  end
  object AssetCount: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select count(*) as N'
      'from asset'
      
        '  inner join asset_assignment on asset.asset_id = asset_assignme' +
        'nt.asset_id'
      'where asset_assignment.emp_id = :EmpID'
      '  and asset_assignment.active')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 0
      end>
    Left = 501
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
        Value = 0
      end>
  end
  object FindRuleCode: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select code, timerule_id'
      ' from employee'
      '  left join reference'
      '   on reference.ref_id=employee.timerule_id'
      ' where emp_id=:emp_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end>
    Left = 588
    Top = 260
    ParamData = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end>
  end
  object ValidStatusQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct sgi.status from client'
      
        ' inner join status_group_item sgi on client.status_group_id=sgi.' +
        'sg_id'
      ' inner join statuslist sl on sgi.status=sl.status'
      ' where sgi.active=true'
      ' and status not in('#39'-N'#39', '#39'-P'#39')'
      ' and sl.active=true'
      ' and client.client_id=:client_id'
      ' order by 1 ')
    Params = <
      item
        DataType = ftUnknown
        Name = 'client_id'
      end>
    Left = 225
    Top = 544
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'client_id'
      end>
  end
  object CenterGroups: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from center_group'
      'where active and show_for_billing'
      'order by group_code')
    Params = <>
    Left = 137
    Top = 136
  end
  object Customers: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from customer where active'
      'order by customer_name, city, state')
    Params = <>
    Left = 137
    Top = 24
  end
  object SaveLocatePlat: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'locate_plat'
    Left = 398
    Top = 432
  end
  object LocatePlatData: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select locate_plat.*'
      'from locate_plat'
      '  inner join locate on locate_plat.locate_id = locate.locate_id'
      '  inner join ticket on ticket.ticket_id = locate.ticket_id'
      'where ticket_id = :ticket_id'
      'and locate_plat.active')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 318
    Top = 548
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object UnitTypeLookup: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'billing_unit_conversion'
    Left = 576
    Top = 552
  end
  object LocateUnitType: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select c.unit_conversion_id, uc.unit_type'
      'from client c'
      'join locate l on l.client_id = c.client_id'
      
        'join billing_unit_conversion uc on uc.unit_conversion_id = c.uni' +
        't_conversion_id'
      'where l.locate_id = :locate_id'
      '  and Coalesce(uc.first_unit_factor, 0) > 0')
    Params = <
      item
        DataType = ftInteger
        Name = 'locate_id'
      end>
    Left = 318
    Top = 600
    ParamData = <
      item
        DataType = ftInteger
        Name = 'locate_id'
      end>
  end
  object Area: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'area'
    Left = 56
    Top = 72
  end
  object BillingOutputConfig: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select customer_id, which_invoice '
      'from billing_output_config where which_invoice is not null')
    Params = <>
    Left = 591
    Top = 373
  end
  object EstimateTypeLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select code, description from reference'
      'where type='#39'esttype'#39' and active_ind')
    Params = <>
    Left = 588
    Top = 440
  end
  object MarkMessagesAcked: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'update message_dest set ack_date = current_timestamp, DeltaStatu' +
        's='#39'U'#39
      'where ack_date is null and show_date <= current_timestamp '
      'and expiration_date > current_timestamp and rule_id = :rule_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'rule_id'
      end>
    Left = 580
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rule_id'
      end>
  end
  object StatusGroup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct sgi.status'
      'from status_group_item sgi'
      '  inner join status_group sg on sg.sg_id = sgi.sg_id'
      'where sg.sg_name = :sg_name'
      '  and sg.active and sgi.active')
    Params = <
      item
        DataType = ftUnknown
        Name = 'sg_name'
      end>
    Left = 230
    Top = 483
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'sg_name'
      end>
  end
  object CenterGroup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct cgd.call_center'
      'from center_group_detail cgd'
      
        '  inner join center_group cg on cg.center_group_id = cgd.center_' +
        'group_id'
      'where cg.group_code = :group_code'
      '  and cg.active and cgd.active')
    Params = <
      item
        DataType = ftString
        Name = 'group_code'
        Value = #39#39
      end>
    Left = 137
    Top = 84
    ParamData = <
      item
        DataType = ftString
        Name = 'group_code'
        Value = #39#39
      end>
  end
  object SaveTicketInfo: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'ticket_info'
    Left = 217
    Top = 224
  end
  object LocatorQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    Left = 318
    Top = 64
  end
  object SaveJobsiteArrival: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'jobsite_arrival'
    Left = 56
    Top = 521
  end
  object LocateFacilityData: TDBISAMQuery
    BeforeEdit = LocateFacilityDataBeforeEdit
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select f.* from locate_facility f'
      'inner join locate l on f.locate_id = l.locate_id'
      'inner join ticket t on t.ticket_id = l.ticket_id'
      'where ticket_id = :ticket_id and f.active = True')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 318
    Top = 310
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object SaveLocateFacility: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'locate_facility'
    Left = 318
    Top = 360
  end
  object ClientLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select client_id, oc_code, client_name, require_locate_units'
      'from client')
    Params = <>
    Left = 137
    Top = 540
  end
  object JobsiteArrivalData: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * '
      'from jobsite_arrival'
      'where ((ticket_id = :ticket_id and ticket_id is not null)'
      '        or'
      '       (damage_id = :damage_id and damage_id is not null))'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'damage_id'
      end>
    Left = 56
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'damage_id'
      end>
  end
  object SaveBreakRuleResponse: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'break_rules_response'
    Left = 592
    Top = 313
  end
  object SaveTaskSchedule: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'task_schedule'
    Left = 588
    Top = 496
  end
  object HighlightRule: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select call_center, field_name, word_regex, '
      '  regex_modifier, which_match, highlight_bold, '
      '  reference.description as color_name,'
      '  reference.modifier as color_code, active'
      'from highlight_rule '
      'left join reference on highlight_color = ref_id'
      'where (call_center = '#39'*'#39' '
      '  or call_center = :call_center)'
      '  and active'
      '')
    Params = <
      item
        DataType = ftString
        Name = 'call_center'
      end>
    Left = 588
    Top = 140
    ParamData = <
      item
        DataType = ftString
        Name = 'call_center'
      end>
  end
  object SaveGPSPosition: TDBISAMTable
    OnPostError = SaveGPSPositionPostError
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'gps_position'
    Left = 720
    Top = 384
  end
  object WorkOrder: TDBISAMTable
    BeforeOpen = WorkOrderBeforeOpen
    BeforePost = WorkOrderBeforePost
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'work_order'
    Left = 842
    Top = 236
  end
  object OpenWorkOrders: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct wo_id '
      '  from work_order   '
      '  where work_order.assigned_to_id=:emp_id'
      '  and work_order.active'
      '  and not work_order.closed '
      '  and work_order.kind <> '#39'CANCEL'#39)
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 842
    Top = 346
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object WorkOrderNotesData: TDBISAMQuery
    Filtered = True
    BeforeOpen = TicketNotesDataBeforeOpen
    OnCalcFields = TicketNotesDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select notes.*, e.short_name, False on_cache'
      'from notes'
      '  left join users u on u.uid = notes.uid'
      '  left join employee e on e.emp_id = u.emp_id'
      'where notes.active and'
      
        '    (notes.foreign_type = :foreign_type_work_order and notes.for' +
        'eign_id = :wo_id)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'foreign_type_work_order'
      end
      item
        DataType = ftUnknown
        Name = 'wo_id'
      end>
    Left = 843
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'foreign_type_work_order'
      end
      item
        DataType = ftUnknown
        Name = 'wo_id'
      end>
  end
  object WorkOrderInspection: TDBISAMTable
    BeforePost = WorkOrderInspectionBeforePost
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'work_order_inspection'
    Left = 842
    Top = 292
  end
  object WorkOrderRemedy: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'work_order_remedy'
    Left = 842
    Top = 396
  end
  object WorkOrderCGAReasonLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select ref_id, code, description from reference where type='#39'cgar' +
        'eason'#39' and active_ind order by description')
    Params = <>
    Left = 847
    Top = 499
  end
  object LocStatusQuestions: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select qi.qi_description, qi.info_type, qi.form_field, qgd.tstat' +
        'us, qi.qi_id '
      'from question_info qi'
      'join question_group_detail qgd on qi.qi_id = qgd.qi_id'
      'join client c on qgd.qh_id = c.qh_id'
      'where '
      '(qgd.active = 1) and '
      'qi.qi_id in ('
      'select distinct qi2.qi_id '
      'from question_info qi2'
      'join question_group_detail qgd2 on qi2.qi_id = qgd2.qi_id'
      'join client c2 on qgd2.qh_id = c2.qh_id)'
      'and (qgd.tstatus =:status)'
      'and (c.client_id =:clientid)'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'clientid'
      end>
    Left = 398
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'clientid'
      end>
  end
  object LocStatusPrefill: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    Left = 398
    Top = 536
  end
  object NotesSubTypeLookup: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'Select ref_id, code, description, sortby '
      'From reference where '
      'active_ind=1 and '
      'modifier='#39'1'#39' and'
      'type='#39'notes_sub'#39' and'
      '(code like :ForeignTypeStr)'
      ' order by code, sortby')
    Params = <
      item
        DataType = ftString
        Name = 'ForeignTypeStr'
      end>
    Left = 56
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'ForeignTypeStr'
      end>
  end
  object RefDescForNotesSubType: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'Select description'
      'From reference where '
      'code=:Code and'
      'active_ind=1 and '
      'type='#39'notes_sub'#39' '
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'Code'
      end>
    Left = 56
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Code'
      end>
  end
  object GetWorkOrderNumQuery: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select wo_number'
      'from work_order'
      'where wo_id=:wo_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'wo_id'
      end>
    Left = 843
    Top = 557
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'wo_id'
      end>
  end
  object LocatesForTickets: TDBISAMQuery
    Filtered = True
    BeforeEdit = LocateDataBeforeEdit
    BeforePost = LocateDataBeforePost
    OnCalcFields = LocateDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select l.locator_id, e.short_name as locator, '
      '       l.status, l.client_code, '
      #9'   c.client_name as client, l.high_profile,'
      #9'   qty_marked,'
      #9'   l.closed, l.closed_date,'
      ' c.alert, l.alert as locatealert'
      ' from locate l'
      '  left join employee e on l.locator_id=e.emp_id'
      '  left join client c on l.client_id=c.client_id'
      ' where l.active'
      '  and locate.ticket_id=:ticket_id'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 318
    Top = 160
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    object IntegerField1: TIntegerField
      FieldName = 'locate_id'
      Origin = 'locate.locate_id'
    end
    object StringField1: TStringField
      FieldName = 'client_code'
      Origin = 'locate.client_code'
      Size = 10
    end
    object IntegerField2: TIntegerField
      FieldName = 'client_id'
      Origin = 'locate.client_id'
    end
    object StringField2: TStringField
      FieldName = 'status'
      Origin = 'locate.status'
      OnChange = LocateDatastatusChange
      Size = 5
    end
    object BooleanField1: TBooleanField
      FieldName = 'high_profile'
      Origin = 'locate.high_profile'
      OnChange = LocateDataHighProfileChange
    end
    object IntegerField3: TIntegerField
      FieldName = 'qty_marked'
      Origin = 'locate.qty_marked'
    end
    object BooleanField2: TBooleanField
      FieldName = 'closed'
      Origin = 'locate.closed'
    end
    object IntegerField4: TIntegerField
      FieldName = 'locator_id'
      Origin = 'assignment.locator_id'
    end
    object StringField3: TStringField
      FieldName = 'short_name'
      Origin = 'employee.short_name'
    end
    object IntegerField5: TIntegerField
      FieldName = 'closed_by_id'
    end
    object StringField4: TStringField
      FieldName = 'closed_how'
      Size = 10
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'closed_date'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'modified_date'
    end
    object BooleanField3: TBooleanField
      FieldName = 'active'
    end
    object StringField5: TStringField
      FieldName = 'DeltaStatus'
      Size = 1
    end
    object IntegerField6: TIntegerField
      FieldName = 'ticket_id'
    end
    object BCDField1: TBCDField
      FieldName = 'price'
      Precision = 32
    end
    object BooleanField4: TBooleanField
      FieldName = 'invoiced'
    end
    object BCDField2: TBCDField
      FieldName = 'regular_hours'
      Precision = 32
      Size = 2
    end
    object BCDField3: TBCDField
      FieldName = 'overtime_hours'
      Precision = 32
      Size = 2
    end
    object StringField6: TStringField
      FieldName = 'added_by'
      Size = 8
    end
    object BooleanField5: TBooleanField
      FieldName = 'watch_and_protect'
    end
    object IntegerField7: TIntegerField
      FieldName = 'high_profile_reason'
    end
    object StringField7: TStringField
      FieldName = 'high_profile_multi'
      Size = 30
    end
    object StringField8: TStringField
      FieldName = 'high_profile_multi_old'
      Size = 30
    end
  end
  object Holidays: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from holiday'
      'where holiday_date=:inputdate')
    Params = <
      item
        DataType = ftUnknown
        Name = 'inputdate'
      end>
    Left = 56
    Top = 576
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'inputdate'
      end>
  end
  object qryRefIDFromClient: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select ref_id, oc_code'
      'from client'
      'where client_id = :ClientID')
    Params = <
      item
        DataType = ftInteger
        Name = 'ClientID'
      end>
    Left = 137
    Top = 332
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ClientID'
      end>
  end
  object LocatorsMgr: TDBISAMQuery
    Filtered = True
    BeforeEdit = LocateDataBeforeEdit
    BeforePost = LocateDataBeforePost
    OnCalcFields = LocateDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select report_to as mgr_id'
      'from employee'
      'where emp_id=:locator_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end>
    Left = 318
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end>
    object IntegerField8: TIntegerField
      FieldName = 'locate_id'
      Origin = 'locate.locate_id'
    end
    object StringField9: TStringField
      FieldName = 'client_code'
      Origin = 'locate.client_code'
      Size = 10
    end
    object IntegerField9: TIntegerField
      FieldName = 'client_id'
      Origin = 'locate.client_id'
    end
    object StringField10: TStringField
      FieldName = 'status'
      Origin = 'locate.status'
      OnChange = LocateDatastatusChange
      Size = 5
    end
    object BooleanField6: TBooleanField
      FieldName = 'high_profile'
      Origin = 'locate.high_profile'
      OnChange = LocateDataHighProfileChange
    end
    object IntegerField10: TIntegerField
      FieldName = 'qty_marked'
      Origin = 'locate.qty_marked'
    end
    object BooleanField7: TBooleanField
      FieldName = 'closed'
      Origin = 'locate.closed'
    end
    object IntegerField11: TIntegerField
      FieldName = 'locator_id'
      Origin = 'assignment.locator_id'
    end
    object StringField11: TStringField
      FieldName = 'short_name'
      Origin = 'employee.short_name'
    end
    object IntegerField12: TIntegerField
      FieldName = 'closed_by_id'
    end
    object StringField12: TStringField
      FieldName = 'closed_how'
      Size = 10
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'closed_date'
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'modified_date'
    end
    object BooleanField8: TBooleanField
      FieldName = 'active'
    end
    object StringField13: TStringField
      FieldName = 'DeltaStatus'
      Size = 1
    end
    object IntegerField13: TIntegerField
      FieldName = 'ticket_id'
    end
    object BCDField4: TBCDField
      FieldName = 'price'
      Precision = 32
    end
    object BooleanField9: TBooleanField
      FieldName = 'invoiced'
    end
    object BCDField5: TBCDField
      FieldName = 'regular_hours'
      Precision = 32
      Size = 2
    end
    object BCDField6: TBCDField
      FieldName = 'overtime_hours'
      Precision = 32
      Size = 2
    end
    object StringField14: TStringField
      FieldName = 'added_by'
      Size = 8
    end
    object BooleanField10: TBooleanField
      FieldName = 'watch_and_protect'
    end
    object IntegerField14: TIntegerField
      FieldName = 'high_profile_reason'
    end
    object StringField15: TStringField
      FieldName = 'high_profile_multi'
      Size = 30
    end
    object StringField16: TStringField
      FieldName = 'high_profile_multi_old'
      Size = 30
    end
  end
  object ProjectClients: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select client.*'
      'from client'
      'where (client.call_center = :call_center)'
      '  and client.parent_client_id = :parent_client_id'
      '  and client.active=1'
      
        '  and not (client.client_id in (select client_id from locate whe' +
        're (ticket_id = :ticket_id) and (status <> '#39'-N'#39')))'
      
        '  and not (client.oc_code in (select client_code from locate whe' +
        're (ticket_id = :ticket_id) and (status <> '#39'-N'#39')))'
      'order by oc_code')
    Params = <
      item
        DataType = ftUnknown
        Name = 'call_center'
      end
      item
        DataType = ftUnknown
        Name = 'parent_client_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 135
    Top = 600
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'call_center'
      end
      item
        DataType = ftUnknown
        Name = 'parent_client_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object ClientHasChildClientsQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select count(*) as NumOfRecords '
      'from client'
      'where (client.parent_client_id =:parent_client_id)'
      '  and (client.active)')
    Params = <
      item
        DataType = ftInteger
        Name = 'parent_client_id'
      end>
    Left = 137
    Top = 488
    ParamData = <
      item
        DataType = ftInteger
        Name = 'parent_client_id'
      end>
  end
  object LocateData: TDBISAMQuery
    Filtered = True
    BeforeEdit = LocateDataBeforeEdit
    BeforePost = LocateDataBeforePost
    OnCalcFields = LocateDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select locate.*, locate.locator_id, employee.short_name,'
      ' '#39'                              '#39' as high_profile_multi,'
      ' '#39'                              '#39' as high_profile_multi_old,'
      
        'c1.alert, locate.alert as locatealert, c1.parent_client_id, c1.c' +
        'lient_name client_name_sort, c2.oc_code parent_code'
      ' from locate'
      '  left join employee on locate.locator_id=employee.emp_id'
      '  left join client c1 on locate.client_id=c1.client_id '
      'left join client c2 on c1.parent_client_id = c2.client_id'
      ' where locate.active'
      '  and locate.ticket_id=:ticket_id'
      '  and locate.locator_id=:my_id'
      ' order by locate.client_code,  client_name_sort')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'my_id'
      end>
    Left = 318
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftInteger
        Name = 'my_id'
      end>
    object LocateDatalocate_id: TIntegerField
      FieldName = 'locate_id'
      Origin = 'locate.locate_id'
    end
    object LocateDataclient_code: TStringField
      FieldName = 'client_code'
      Origin = 'locate.client_code'
      Size = 10
    end
    object LocateDataclient_id: TIntegerField
      FieldName = 'client_id'
      Origin = 'locate.client_id'
    end
    object LocateDatastatus: TStringField
      FieldName = 'status'
      Origin = 'locate.status'
      OnChange = LocateDatastatusChange
      Size = 5
    end
    object LocateDatahigh_profile: TBooleanField
      FieldName = 'high_profile'
      Origin = 'locate.high_profile'
      OnChange = LocateDataHighProfileChange
    end
    object LocateDataqty_marked: TIntegerField
      FieldName = 'qty_marked'
      Origin = 'locate.qty_marked'
    end
    object LocateDataclosed: TBooleanField
      FieldName = 'closed'
      Origin = 'locate.closed'
    end
    object LocateDatalocator_id: TIntegerField
      FieldName = 'locator_id'
      Origin = 'assignment.locator_id'
    end
    object LocateDatashort_name: TStringField
      FieldName = 'short_name'
      Origin = 'employee.short_name'
    end
    object LocateDataclosed_by_id: TIntegerField
      FieldName = 'closed_by_id'
    end
    object LocateDataclosed_how: TStringField
      FieldName = 'closed_how'
      Size = 10
    end
    object LocateDataclosed_date: TDateTimeField
      FieldName = 'closed_date'
    end
    object LocateDatamodified_date: TDateTimeField
      FieldName = 'modified_date'
    end
    object LocateDataactive: TBooleanField
      FieldName = 'active'
    end
    object LocateDataDeltaStatus: TStringField
      FieldName = 'DeltaStatus'
      Size = 1
    end
    object LocateDataticket_id: TIntegerField
      FieldName = 'ticket_id'
    end
    object LocateDataprice: TBCDField
      FieldName = 'price'
      Precision = 32
    end
    object LocateDatainvoiced: TBooleanField
      FieldName = 'invoiced'
    end
    object LocateDataregular_hours: TBCDField
      FieldName = 'regular_hours'
      Precision = 32
      Size = 2
    end
    object LocateDataovertime_hours: TBCDField
      FieldName = 'overtime_hours'
      Precision = 32
      Size = 2
    end
    object LocateDataadded_by: TStringField
      FieldName = 'added_by'
      Size = 8
    end
    object LocateDatawatch_and_protect: TBooleanField
      FieldName = 'watch_and_protect'
    end
    object LocateDatahigh_profile_reason: TIntegerField
      FieldName = 'high_profile_reason'
    end
    object LocateDatahigh_profile_multi: TStringField
      FieldName = 'high_profile_multi'
      Size = 30
    end
    object LocateDatahigh_profile_multi_old: TStringField
      FieldName = 'high_profile_multi_old'
      Size = 30
    end
    object LocateDataparent_code: TStringField
      FieldName = 'parent_code'
      Size = 30
    end
  end
  object QryProjectLocates: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select l.ticket_id, l.locate_id, l.status, l.closed, l.client_co' +
        'de'
      'from locate l'
      'where locate_id=:locate_id and active=1'
      'union all'
      ''
      
        'select l2.ticket_id, l2.locate_id, l2.status, l2.closed, l2.clie' +
        'nt_code'
      'from locate l2'
      'left join client c1 on l2.client_id=c1.client_id'
      '  left join client c2 on c1.parent_client_id = c2.client_id'
      'where '
      
        '((l2.active=1) and (l2.ticket_id =:ticket_id) and (c1.parent_cli' +
        'ent_id =c2.client_id)'
      
        '  and (c1.parent_client_id in (select l3.client_id from locate l' +
        '3 where locate_id=:locate_id and active=1)))'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end>
    Left = 318
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end>
  end
  object qryClientInfo: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'SELECT c.client_name'
      '      ,client_rules'
      '      ,marking_concerns'
      '      ,(e.short_name) as "Trainer"'
      '      ,(ee.short_name) as "FieldEngineer"'
      '      ,plat_info'
      '      ,plat_file'
      '      ,training_link'
      '      ,plats_online_link'
      '  FROM client_info ci'
      '  left outer join client c on c.client_id = ci.client_id'
      '  left outer join employee e on e.emp_id = ci.trainer_emp_id'
      
        '  left outer join employee ee on ee.emp_id = ci.field_engineer_e' +
        'mp_id'
      '  where ci.client_id = :clientID')
    Params = <
      item
        DataType = ftInteger
        Name = 'clientID'
      end>
    ReadOnly = True
    Left = 137
    Top = 384
    ParamData = <
      item
        DataType = ftInteger
        Name = 'clientID'
      end>
  end
  object dsClientInfo: TDataSource
    AutoEdit = False
    DataSet = qryClientInfo
    Left = 137
    Top = 432
  end
  object TicketFullAddressQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select work_address_number, work_address_street, work_city, work' +
        '_state'
      'from ticket'
      'where ticket_id=:ticket_id')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 256
    Top = 26
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object RouteOrderQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select distinct t.ticket_id, t.ticket_number, t.route_order, t.k' +
        'ind, t.status, t.transmit_date, t.due_date     '
      'from ticket t inner join locate l on t.ticket_id=l.ticket_id'
      'where (t.active=1)'
      'and NOT (t.work_priority_id=:workpriority)'
      'and (l.locator_id=:emp_id)'
      'and (t.route_order> 0)'
      'and (t.route_order<:route_order)'
      'and (l.active)'
      'and (l.status ='#39'-R'#39')'
      'and not (l.closed)'
      
        'and ((do_not_mark_before is NULL) or (do_not_mark_before <= :las' +
        't_sync)) '
      'order by t.route_order'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'workpriority'
      end
      item
        DataType = ftInteger
        Name = 'emp_id'
      end
      item
        DataType = ftFloat
        Name = 'route_order'
      end
      item
        DataType = ftUnknown
        Name = 'last_sync'
      end>
    Left = 725
    Top = 28
    ParamData = <
      item
        DataType = ftInteger
        Name = 'workpriority'
      end
      item
        DataType = ftInteger
        Name = 'emp_id'
      end
      item
        DataType = ftFloat
        Name = 'route_order'
      end
      item
        DataType = ftUnknown
        Name = 'last_sync'
      end>
  end
  object InsertRouteOrderInfo: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'Insert into ticket_info (ticket_info_id, ticket_id, info_type, i' +
        'nfo, added_by,  LocalStringKey, DeltaStatus, modified_date)'
      
        '                        values (:ticket_info_id, :ticket_id, :ro' +
        'uteord, :info, :added_by, :ticketnum, '#39'I'#39', :modified_date)'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_info_id'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftString
        Name = 'routeord'
      end
      item
        DataType = ftString
        Name = 'info'
      end
      item
        DataType = ftInteger
        Name = 'added_by'
      end
      item
        DataType = ftString
        Name = 'ticketnum'
      end
      item
        DataType = ftDateTime
        Name = 'modified_date'
      end>
    Left = 725
    Top = 76
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_info_id'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftString
        Name = 'routeord'
      end
      item
        DataType = ftString
        Name = 'info'
      end
      item
        DataType = ftInteger
        Name = 'added_by'
      end
      item
        DataType = ftString
        Name = 'ticketnum'
      end
      item
        DataType = ftDateTime
        Name = 'modified_date'
      end>
  end
  object QryTicketInfoRouteOrder: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from ticket_info'
      'where info_type='#39'ROUTEORD'#39
      'and ticket_id=:ticket_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 725
    Top = 124
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object qryUtilityCo: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select *'
      'from reference'
      'where type = '#39'dmgco'#39
      'order by description')
    Params = <>
    Left = 720
    Top = 224
  end
  object qryClient: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select oc_code, client_name, ref_id, active'
      'from client'
      'order by oc_code'
      '')
    Params = <>
    Left = 721
    Top = 292
  end
  object qryEmpPhoneCo: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'Select contact_phone, (contact_phone)+R.description as TextMailA' +
        'ddress'
      'from employee E'
      'join reference R on r.ref_id = e.phoneCo_ref'
      'where E.emp_id = :EmpID'
      'and r.type = '#39'phoneco'#39)
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
    Left = 720
    Top = 448
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
  end
  object qrySearchLocator: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'Select short_name'
      'from employee E'
      'where E.emp_id = :EmpID'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
    Left = 720
    Top = 504
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EmpID'
      end>
  end
  object ROForLocator: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select distinct t.ticket_id, t.ticket_number, t.route_order'
      'from ticket t inner join locate l on t.ticket_id=l.ticket_id'
      'where (t.active=1)'
      '  and (t.route_order > 0)'
      '  and (l.locator_id=:locator_id)'
      '  and (l.active)'
      '  and (l.status ='#39'R'#39')'
      '  and not (l.closed)'
      '  and ((do_not_mark_before is NULL) '
      ')'
      'order by route_order'
      ''
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end>
    Left = 725
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end>
  end
  object StatusListForClientQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    Left = 220
    Top = 364
  end
  object qryAbandonedCallOut: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'SELECT '
      '      entry_id'
      '      ,work_emp_id'
      '      ,work_date'
      '      ,status'
      '      ,callout_start1'
      '      ,callout_stop1'
      '      ,callout_start2'
      '      ,callout_stop2'
      '      ,callout_start3'
      '      ,callout_stop3'
      '      ,callout_start4'
      '      ,callout_stop4'
      '      ,callout_start5'
      '      ,callout_stop5'
      '      ,callout_start6'
      '      ,callout_stop6'
      '      ,callout_start7'
      '      ,callout_stop7'
      '      ,callout_start8'
      '      ,callout_stop8'
      '      ,callout_start9'
      '      ,callout_stop9'
      '      ,callout_start10'
      '      ,callout_stop10'
      '      ,callout_start11'
      '      ,callout_stop11'
      '      ,callout_start12'
      '      ,callout_stop12'
      '      ,callout_start13'
      '      ,callout_stop13'
      '      ,callout_start14'
      '      ,callout_stop14'
      '      ,callout_start15'
      '      ,callout_stop15'
      '      ,callout_start16'
      '      ,callout_stop16'
      '     ,reg_hours'
      '     ,callout_hours'
      '  FROM timesheet_entry'
      '  where work_emp_id= :empID'
      '  and  work_date = :yesterday'
      'and status = '#39'ACTIVE'#39
      'order by entry_id desc top 1')
    Params = <
      item
        DataType = ftInteger
        Name = 'empID'
      end
      item
        DataType = ftDate
        Name = 'yesterday'
      end>
    Left = 720
    Top = 560
    ParamData = <
      item
        DataType = ftInteger
        Name = 'empID'
      end
      item
        DataType = ftDate
        Name = 'yesterday'
      end>
  end
  object updNextUnusedCallOut: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'update timesheet_entry'
      'set callout_start1 = :CalloutStart,'
      'callout_stop1= :CalloutStop'
      'where entry_id = :EntryID')
    Params = <
      item
        DataType = ftDateTime
        Name = 'CalloutStart'
      end
      item
        DataType = ftDateTime
        Name = 'CalloutStop'
      end
      item
        DataType = ftInteger
        Name = 'EntryID'
        Value = '30779960'
      end>
    Left = 720
    Top = 608
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'CalloutStart'
      end
      item
        DataType = ftDateTime
        Name = 'CalloutStop'
      end
      item
        DataType = ftInteger
        Name = 'EntryID'
        Value = '30779960'
      end>
  end
  object customer_locator: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select *'
      'from customer_locator'
      'where loc_emp_id = :EmpID'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
    Left = 53
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EmpID'
      end>
  end
  object qryPLUSWhitelist: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select client_code, can_status from empPlus'
      'where client_code=:client_code')
    Params = <
      item
        DataType = ftUnknown
        Name = 'client_code'
      end>
    Left = 424
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'client_code'
      end>
  end
  object UpdateAbandonedCO: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      '')
    Params = <>
    Left = 720
    Top = 656
  end
  object MultiDueDateQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'Select count(notes_id)  as sumcount '
      'from notes '
      'where (foreign_type=1)'
      'and (foreign_id=:ticket_id)'
      'and ([note] like '#39'%modifed the due date%'#39')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
    Left = 45
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ticket_id'
      end>
  end
  object ValidStatusPlusQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select sp.status, sp.status_plus statlist from status_plus sp'
      'where sp.client_code=:client_code'
      'and sp.call_center=:call_center'
      'and sp.status=:status'
      'and sp.active')
    Params = <
      item
        DataType = ftUnknown
        Name = 'client_code'
      end
      item
        DataType = ftUnknown
        Name = 'call_center'
      end
      item
        DataType = ftUnknown
        Name = 'status'
      end>
    Left = 225
    Top = 594
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'client_code'
      end
      item
        DataType = ftUnknown
        Name = 'call_center'
      end
      item
        DataType = ftUnknown
        Name = 'status'
      end>
  end
  object TicketAlertsQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'select a.*,r.description as alert_code_desc, r.sortby, r.modifie' +
        'r from ticket_alert a'
      'LEFT OUTER join reference r on (a.alert_type_code = r.code)'
      'where (ticket_id=:ticket_id) '
      'and (r.type = '#39'alerttype'#39') '
      'and (a.active = 1)'
      'order by r.sortby')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 864
    Top = 48
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
  end
  object LocateHoursDataTotals: TDBISAMQuery
    OnCalcFields = LocateHoursDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'Select sum(units_marked) as total_length_marked'
      'from locate_hours'
      'where (locate_id =:locate_id)')
    Params = <
      item
        DataType = ftInteger
        Name = 'locate_id'
      end>
    Left = 414
    Top = 284
    ParamData = <
      item
        DataType = ftInteger
        Name = 'locate_id'
      end>
  end
  object AddLocateQry: TDBISAMQuery
    Filtered = True
    BeforeEdit = LocateDataBeforeEdit
    BeforePost = LocateDataBeforePost
    OnCalcFields = LocateDataCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      
        'Insert into locate ( locate_id, ticket_id,   locator_id,  client' +
        '_code,  client_id,  status,   high_profile,  qty_marked,   assig' +
        'ned_to,   assigned_to_id,   added_by,  active,  LocalStringKey, ' +
        ' modified_date,  entry_date, closed,  invoiced, watch_and_protec' +
        't, DeltaStatus )'
      
        '                 values (:locate_id, :ticket_id, :locator_id, :c' +
        'lient_code, :client_id, :status, :high_profile, :qty_marked,  :a' +
        'ssigned_to, :assigned_to_id,  :added_by,  1,      :LocalStringKe' +
        'y, :modified_date,  :entry_date, FALSE,  FALSE,     FALSE,      ' +
        '              '#39'I'#39')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end
      item
        DataType = ftUnknown
        Name = 'client_code'
      end
      item
        DataType = ftUnknown
        Name = 'client_id'
      end
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'high_profile'
      end
      item
        DataType = ftUnknown
        Name = 'qty_marked'
      end
      item
        DataType = ftUnknown
        Name = 'assigned_to'
      end
      item
        DataType = ftUnknown
        Name = 'assigned_to_id'
      end
      item
        DataType = ftUnknown
        Name = 'added_by'
      end
      item
        DataType = ftUnknown
        Name = 'LocalStringKey'
      end
      item
        DataType = ftUnknown
        Name = 'modified_date'
      end
      item
        DataType = ftUnknown
        Name = 'entry_date'
      end>
    Left = 382
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'locate_id'
      end
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end
      item
        DataType = ftUnknown
        Name = 'locator_id'
      end
      item
        DataType = ftUnknown
        Name = 'client_code'
      end
      item
        DataType = ftUnknown
        Name = 'client_id'
      end
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'high_profile'
      end
      item
        DataType = ftUnknown
        Name = 'qty_marked'
      end
      item
        DataType = ftUnknown
        Name = 'assigned_to'
      end
      item
        DataType = ftUnknown
        Name = 'assigned_to_id'
      end
      item
        DataType = ftUnknown
        Name = 'added_by'
      end
      item
        DataType = ftUnknown
        Name = 'LocalStringKey'
      end
      item
        DataType = ftUnknown
        Name = 'modified_date'
      end
      item
        DataType = ftUnknown
        Name = 'entry_date'
      end>
    object IntegerField15: TIntegerField
      FieldName = 'locate_id'
      Origin = 'locate.locate_id'
    end
    object StringField17: TStringField
      FieldName = 'client_code'
      Origin = 'locate.client_code'
      Size = 10
    end
    object IntegerField16: TIntegerField
      FieldName = 'client_id'
      Origin = 'locate.client_id'
    end
    object StringField18: TStringField
      FieldName = 'status'
      Origin = 'locate.status'
      OnChange = LocateDatastatusChange
      Size = 5
    end
    object BooleanField11: TBooleanField
      FieldName = 'high_profile'
      Origin = 'locate.high_profile'
      OnChange = LocateDataHighProfileChange
    end
    object IntegerField17: TIntegerField
      FieldName = 'qty_marked'
      Origin = 'locate.qty_marked'
    end
    object BooleanField12: TBooleanField
      FieldName = 'closed'
      Origin = 'locate.closed'
    end
    object IntegerField18: TIntegerField
      FieldName = 'locator_id'
      Origin = 'assignment.locator_id'
    end
    object StringField19: TStringField
      FieldName = 'short_name'
      Origin = 'employee.short_name'
    end
    object IntegerField19: TIntegerField
      FieldName = 'closed_by_id'
    end
    object StringField20: TStringField
      FieldName = 'closed_how'
      Size = 10
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'closed_date'
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'modified_date'
    end
    object BooleanField13: TBooleanField
      FieldName = 'active'
    end
    object StringField21: TStringField
      FieldName = 'DeltaStatus'
      Size = 1
    end
    object IntegerField20: TIntegerField
      FieldName = 'ticket_id'
    end
    object BCDField7: TBCDField
      FieldName = 'price'
      Precision = 32
    end
    object BooleanField14: TBooleanField
      FieldName = 'invoiced'
    end
    object BCDField8: TBCDField
      FieldName = 'regular_hours'
      Precision = 32
      Size = 2
    end
    object BCDField9: TBCDField
      FieldName = 'overtime_hours'
      Precision = 32
      Size = 2
    end
    object StringField22: TStringField
      FieldName = 'added_by'
      Size = 8
    end
    object BooleanField15: TBooleanField
      FieldName = 'watch_and_protect'
    end
    object IntegerField21: TIntegerField
      FieldName = 'high_profile_reason'
    end
    object StringField23: TStringField
      FieldName = 'high_profile_multi'
      Size = 30
    end
    object StringField24: TStringField
      FieldName = 'high_profile_multi_old'
      Size = 30
    end
    object StringField25: TStringField
      FieldName = 'parent_code'
      Size = 30
    end
  end
end
