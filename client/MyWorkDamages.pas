unit MyWorkDamages;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableDataFrame, BaseExpandableGridFrame, StdCtrls, ExtCtrls,
  cxGraphics, cxDataStorage, cxEdit, DB,
  cxDBData, cxTextEdit, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxGridLevel, DBISAMTb, DMu, UQUtils, QMConst, cxDropDownEdit,dxCore,
  cxNavigator, cxCustomData, cxFilter, cxData;

const
  UM_DAMAGES_EXPANDALL = WM_USER + $102;

type
  TMyWorkDamagesFrame = class(TExpandableGridFrameBase)
    DamageSource: TDataSource;
    OpenDamages: TDBISAMQuery;
    MyWorkViewRepository: TcxGridViewRepository;
    DamagesGridDetailsTableView: TcxGridDBBandedTableView;
    DmgDColDamagePriority: TcxGridDBBandedColumn;
    DmgDColWorkPriorityDesc: TcxGridDBBandedColumn;
    DmgDColDamageType: TcxGridDBBandedColumn;
    DmgDColDamageID: TcxGridDBBandedColumn;
    DmgDColUQDamageID: TcxGridDBBandedColumn;
    DmgDColDueDate: TcxGridDBBandedColumn;
    DmgDColDamageDate: TcxGridDBBandedColumn;
    DmgDColTicketNumber: TcxGridDBBandedColumn;
    DmgDColProfitCenter: TcxGridDBBandedColumn;
    DmgDColCompanyDamaged: TcxGridDBBandedColumn;
    DmgDColFacilityType: TcxGridDBBandedColumn;
    DmgDColFacilitySize: TcxGridDBBandedColumn;
    DmgDColExcavatorCompany: TcxGridDBBandedColumn;
    DmgDColLocation: TcxGridDBBandedColumn;
    DmgDColDetailsCity: TcxGridDBBandedColumn;
    DmgDColDetailsCounty: TcxGridDBBandedColumn;
    DmgDColDetailsState: TcxGridDBBandedColumn;
    DmgDColExport: TcxGridDBBandedColumn;
    DamagesGridSummaryTableView: TcxGridDBBandedTableView;
    DmgSColDamageType: TcxGridDBBandedColumn;
    DmgSColUQDamageID: TcxGridDBBandedColumn;
    DmgSColDamageID: TcxGridDBBandedColumn;
    DmgSColDueDate: TcxGridDBBandedColumn;
    DmgSColDamageDate: TcxGridDBBandedColumn;
    DmgSColTicketNumber: TcxGridDBBandedColumn;
    DmgSColExcavatorCompany: TcxGridDBBandedColumn;
    DmgSColLocation: TcxGridDBBandedColumn;
    DmgSColCity: TcxGridDBBandedColumn;
    DmgSColCounty: TcxGridDBBandedColumn;
    DmgSColState: TcxGridDBBandedColumn;
    DmgSColPriorityDescription: TcxGridDBBandedColumn;
    DmgSColWorkPriority: TcxGridDBBandedColumn;
    DmgSColExport: TcxGridDBBandedColumn;
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    procedure DamagesExpandAll;
    procedure UMDamagesExpandAll(var Message: TMessage); message UM_DAMAGES_EXPANDALL;
    procedure RefreshDamages;
    function DamageDueDateColumn: TcxGridColumn;
  protected
    function ItemIDColumn: TcxGridColumn; override;
    procedure SetupColumns; override;
    procedure SetGridEvents; override;
  public
    procedure SortGridByFields; override;
    function ExportMyItems: string; override;
    procedure SetAllExportBoxes(Checked: Boolean); override;
    procedure RefreshData; override;
    procedure RefreshCaptionCounts; override;
  end;

var
  MyWorkDamagesFrame: TMyWorkDamagesFrame;

implementation

{$R *.dfm}

uses OdHourglass, OdDBUtils, OdMiscUtils, OdCxUtils, OdDBISAMUtils, 
  SharedDevExStyles, LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDmu;

const
  DamageSortColumns: array[0..1] of string = ('work_priority', 'due_date');
  DamageSortDirections: array[0..1] of TcxGridSortOrder = (soDescending, soAscending);

  SelectMyDamages = 'select damage.damage_id, uq_damage_id, damage_date, due_date, damage_type,  ' +
    'ticket_id, t.ticket_number, profit_center, utility_co_damaged, location, city, county, state,  ' +
    'facility_type, facility_size, excavator_company, ' +
    'coalesce(ref.sortby, 0) work_priority, coalesce(ref.description, ''Normal'') priority_description ' +
    ', False export ' +
    'from damage ' +
    'left join reference ref on damage.work_priority_id= ref.ref_id ' +
    'left join ticket t on damage.ticket_id = t.ticket_id ' +
    'where investigator_id = :emp_id ' +
    'and damage_type in (''' + DamageTypePending + ''') ' +
    'and damage.active ' +
    'order by work_priority desc, due_date';

procedure TMyWorkDamagesFrame.RefreshCaptionCounts;
begin
  inherited;
  CountLabelSummary.Caption := IntToStr(DM.nOpenDamages);
  CountLabelDataSummary.Caption := IntToStr(DM.nOpenDamages);
end;

procedure TMyWorkDamagesFrame.SetupColumns;
begin
  DmgDColExport.Visible := PermissionsDM.CanI(RightDamagesSelectiveExport);
  DmgSColExport.Visible := PermissionsDM.CanI(RightDamagesSelectiveExport);
  SortGridByFieldNames(BaseGrid, DamageSortColumns, DamageSortDirections);
  DamagesExpandAll;
end;

procedure TMyWorkDamagesFrame.DamagesExpandAll;
begin
  PostMessage(Self.Handle, UM_DAMAGES_EXPANDALL, 0, 0);
end;

procedure TMyWorkDamagesFrame.UMDamagesExpandAll(var Message: TMessage);
begin
  FullExpandGrid(BaseGrid);
  if BaseGrid.CanFocus then
    BaseGrid.SetFocus
end;

procedure TMyWorkDamagesFrame.RefreshData;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  RefreshDamages;
end;

procedure TMyWorkDamagesFrame.RefreshDamages;
var
  Cursor: IInterface;
begin
  if not LDamageDM.HaveDamagesRights then begin
    Self.Visible := False;
    Exit;
  end;

  Cursor := ShowHourGlass;
  OpenDamages.DisableControls;
  try
    OpenDamages.Close;
    OpenDamages.SQL.Text := SelectMyDamages;
    OpenDamages.ParamByName('emp_id').AsInteger := DM.EmpID;
    OpenDamages.Open;
    SetFieldDisplayFormat(OpenDamages, 'damage_date', 'mmm d, t');
    SetFieldDisplayFormat(OpenDamages, 'due_date', 'mmm d, t');
  finally
    OpenDamages.EnableControls;
  end;

  Self.Visible := (OpenDamages.RecordCount >= 1);
  if Self.Visible then
    SetupColumns;
end;

procedure TMyWorkDamagesFrame.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  DamageStatus: TDamageStatus;
  DueDate: TDateTime;
begin
  inherited;
  if (AViewInfo.GridRecord is TcxGridGroupRow) then
    Exit;

  if not AViewInfo.Selected then begin
    if VarIsNull(AViewInfo.GridRecord.Values[DamageDueDateColumn.Index]) then
      DueDate := 0
    else
      DueDate := (AViewInfo.GridRecord.Values[DamageDueDateColumn.Index]);
    DamageStatus := GetDamageStatusFromValues(DueDate);
    ACanvas.Brush.Color := GetDamageColor(DamageStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
  end;
  ADone := False;
end;

function TMyWorkDamagesFrame.ItemIDColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = DamagesGridDetailsTableView then
    Result := DmgDColDamageID
  else
    Result := DmgSColDamageID;
end;

function TMyWorkDamagesFrame.DamageDueDateColumn: TcxGridColumn;
begin
  if BaseGrid.FocusedView = DamagesGridSummaryTableView then
    Result := DmgSColDueDate
  else
    Result := DmgDColDueDate;
end;

procedure TMyWorkDamagesFrame.SortGridByFields;
begin
  inherited;
  SortGridByFieldNames(BaseGrid, DamageSortColumns, DamageSortDirections);
end;

function TMyWorkDamagesFrame.ExportMyItems: string; // returns exported filenames
const
  SelectMyDamagesExport = 'select distinct uq_damage_id "Ticket #", ' +
    't.work_lat "Lat", t.work_long "Long", d.due_date, ' +
    'coalesce(ref.sortby, 0) "work_priority", ' +
    'd.location "Name", ' +
    'CAST(d.due_date as varchar(16)) "Name2", ' +
    'd.location "Address", d.location "Street", ' +
    'd.city "City", d.state "State", d.county "County" ' +
    'from damage d ' +
    'left join ticket t on t.ticket_id = d.ticket_id ' +
    'left join reference ref on d.work_priority_id=ref.ref_id ' +
    'where d.investigator_id = %d ' +
    'and d.active = True ' +
    'and d.damage_type in (''%s'')' +
    '%s ' + // Placeholder for 'd.damage_id in ()' criteria
    'order by work_priority desc, d.due_date, d.uq_damage_id ';
var
  Wait: IInterface;
  ExportFileName: string;
  DamageIDCriteria: string;
  SelectedDamageIDs : string;
  DamagesToExport: TDataSet;
  Select: string;
  SelectedDamageExportCount: Integer;
  CanSelectDamagesToExport: Boolean;
  GPXOptions: TGPXOptions;
begin
  Result := '';
  Wait := ShowHourGlass;
  DamageIDCriteria := '';
  CanSelectDamagesToExport := PermissionsDM.CanI(RightDamagesSelectiveExport);
  GPXOptions.IncludeRouteOrder := False; //QM-133 Past Due
  GPXOptions.IncludeEmpName := False;

  if CanSelectDamagesToExport then // limit export to selected damages
    SelectedDamageIDs := GetItemIDsForExportString(OpenDamages, 'damage_id', SelectedItemID)
  else                             // export all assigned damages
    SelectedDamageIDs := '';

  if CanSelectDamagesToExport and IsEmpty(SelectedDamageIDs) then // nothing selected to export
    Exit;

  if NotEmpty(SelectedDamageIDs) then
    DamageIDCriteria := 'and d.damage_id in (' + SelectedDamageIDs + ') ';

  Select := Format(SelectMyDamagesExport, [DM.EmpID, DamageTypePending, DamageIDCriteria]);
  DamagesToExport := DM.Engine.OpenQuery(Select);
  try
    SelectedDamageExportCount := DamagesToExport.RecordCount;
    ExportFileName := DM.CreateExportFileName('-My Damages', '.gpx');
    DM.SaveGpxToFile(DamagesToExport, ExportFileName, GPXOptions); //QM-133 Past Due (Refactoring of SaveGPXToFile)
    Result := ExportFileName;
  finally
    FreeAndNil(DamagesToExport);
  end;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageRouteExport, IntToStr(SelectedDamageExportCount));
end;

procedure TMyWorkDamagesFrame.SetAllExportBoxes(Checked: Boolean);
begin
  inherited;
  if FViewIndex = 0 then
    SetExportAll(OpenDamages, DamagesGridDetailsTableView, Checked)
  else
    SetExportAll(OpenDamages, DamagesGridSummaryTableView, Checked);
end;

procedure TMyWorkDamagesFrame.SetGridEvents;
  procedure SetViewEvents(View: TcxGridDBBandedTableView);
  begin
    View.DataController.OnCompare := GridCompare;
    View.OnCustomDrawCell := GridViewCustomDrawCell;
    View.OnDblClick := GridDblClick;
  end;
begin
  inherited;
  SetViewEvents(DamagesGridDetailsTableView);
  SetViewEvents(DamagesGridSummaryTableView);
end;

end.
