unit SharedImages;

// Don't delete images from the shared imagelist, since it changes the
// ImageIndex values.  Only replace images with better ones with the same
// meaning or add new items to the end of the list.

interface

uses
  SysUtils, Classes, ImgList, Controls;

const

  ImageIndexTree = 13;
  ImageIndexClock = 14;
  ImageIndexPaint = 19;
  ImageIndexPencil = 20;
  ImageIndexNotes = 21;
  ImageIndexGreenFlag = 25;

  ImageIndexManagerIn = 29;
  ImageIndexManagerOut = 30;
  ImageIndexManagerOff = 31;
  ImageIndexLocatorIn = 32;
  ImageIndexLocatorOut = 33;
  ImageIndexLocatorOff = 34;
  ImageIndexCalledIn = 35;
  ImageIndexOnCall = 36;
  ImageIndexRedFlag = 38;    // QM-133 EB Past Due
  ImageIndexTomorrow = 39;   // QM-294 Tomorrow Bucket
  ImageIndexInactiveEmp = 40; // QM-294 Pt 2 Inactive
  ImageIndexInactiveMgr = 41; // QM-294 Pt 2 Inactive

  ImageIndexBlueFlag = 46;  //QM-428 EB
  ImageIndexYellowFlag = 48; // QM-318 TODAY Bucket
  ImageIndexVirtualBucket = 49; //QM-486 Virtual Custom Bucket

  ImageIndexDPUdiploma= 53; //QM-486 Virtual Custom Bucket

type
  TSharedImagesDM = class(TDataModule)
    Images: TImageList;
  end;

implementation

{$R *.dfm}

end.
