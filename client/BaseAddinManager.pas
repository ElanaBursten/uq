unit BaseAddinManager;

interface

uses Windows, Forms, StdCtrls, SysUtils, Classes, IniFiles, MSXML2_TLB, DB,
  OdMiscUtils, OdMSXMLUtils, OdDbUtils, QMConst, DMu,
  Variants, Dialogs;

const
  NoAttachmentSelected = 0;

type
  { TODO -odan -c3398 : Refactor to use different classes for different types }
  TAddinType = (atDefault, atChromeApplication);

  TBaseAddinManager = class
  private
    FClickHandler: TNotifyEvent;
    function AddinsFileName: string;
    procedure InternalClickHandler(Sender: TObject);
    function NewInputDocument: IXMLDOMDocument;
    function GetQManagerXML(Doc: IXMLDOMDocument): IXMLDOMElement;
  protected
    AddinFileName: string;
    AddinURL: string;
    AddinButton: TButton;
    DM: TDM;
    DamageData: TDataSet;
    TicketData: TDataSet;
    LocateData: TDataSet;
    WorkOrderData: TDataSet;
    ActionAttachmentID: Integer;
    ActionForeignType: Integer;
    ActionAttachmentFilename: string;
    procedure PrepareAddin(const SectionID: string);
    function GenerateTicketInput: string;
    function GenerateTicketURLSearchPart: string;
    function GenerateDamageInput: string;
    function GenerateWorkOrderInput: string;
    function CreateInputFileName: string;
    function GetTicketXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetDamageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetWorkOrderXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetDamageAsTicketXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer; const IsDamageAsTicketNode: Boolean=False): IXMLDOMElement;
    function GetSketchAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer): IXMLDOMElement;
    function GetDamageOrTktAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer): IXMLDOMElement;
    function GetNewAttachmentFolderXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetTicketImageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function GetWorkOrderImageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
    function HaveSelectedAttachment: Boolean;
    function BuildAddinCommandLine(const InputFile, OutputFile: string): string; virtual;
    function RunAddin(const InputFile: string): string; virtual;
    function SelectedFileIsSketchFile: Boolean;
    function AddinType: TAddinType;
    function BrowserUserDataDir: string;
  public
    constructor Create(DM: TDM; Button: TButton; ClickHandler: TNotifyEvent); virtual;
    destructor Destroy; override;
  end;

implementation

uses
  ApplicationFiles, LocalEmployeeDMu;

{ TBaseAddinManager }
const
  AttachmentFolderNodeName = 'NewAttachmentFolder';
  FileNameNodeName = 'FileName';
  OriginalFileNameNodeName = 'OriginalFileName';
  TicketDetailNodeName = 'Detail';
  AttachmentIDAttrName = 'ID';
  AttachmentSourceAttrName = 'Source';
  AttachmentActiveAttrName = 'Active';
  FileNameID = 'FileName';
  URLID = 'URL';
  EnabledID  = 'Enabled';
  CaptionID  = 'ButtonCaption';
  // Relative to GetClientLocalDir
  ChromeRelativeUserDataDir = 'Chrome User Data';
  SelectTicket = 'select * from ticket where ticket_id = %d';
  SelectAttachment =  'select * from attachment where foreign_type = %d and foreign_id = %d ';
  SelectSketchAndBaseImageAttachments = 'select * from attachment where attachment_id = %d or orig_filename like %s';
  SelectLocate = 'select locate.*, client.client_name from locate ' +
    'inner join client on locate.client_id = client.client_id ' +
    'where ticket_id = %d and status <> %s and locate.active';
  SelectWorkOrder = 'select * from work_order where wo_id = %d';
  SKETCHEXT = '.sketch';


function TBaseAddinManager.AddinsFileName: string;
begin
  Result := GetClientAddinsFile;
end;

procedure TBaseAddinManager.PrepareAddin(const SectionID: string);
var
  IniFile: TIniFile;
begin
  if FileExists(AddinsFileName) then begin
    IniFile := TIniFile.Create(AddinsFileName);
    try
      if IniFile.SectionExists(SectionID) then begin
        if IniFile.ValueExists(SectionID, FileNameID) then begin
          AddinFileName := Trim(IniFile.ReadString(SectionID, FileNameID, ''));
          AddinURL := Trim(IniFile.ReadString(SectionID, URLID, ''));

          if FileExists(AddinFileName) then begin
            AddinButton.Enabled := IniFile.ReadBool(SectionID, EnabledID, True);
            AddinButton.Caption := IniFile.ReadString(SectionID, CaptionID, 'Attachment Addin');
            AddinButton.OnClick := InternalClickHandler;
            AddinButton.Visible := True;
          end;
        end;
      end;
    finally
      FreeAndNil(IniFile);
    end;
  end;
end;

procedure TBaseAddinManager.InternalClickHandler(Sender: TObject);
begin
  if Assigned(FClickHandler) then
    FClickHandler(Sender);
end;

constructor TBaseAddinManager.Create(DM: TDM; Button: TButton; ClickHandler: TNotifyEvent);
begin
  inherited Create;
  AddinButton := Button;
  FClickHandler := ClickHandler;
  Self.DM := DM;
  Assert(Assigned(AddinButton), 'No atttachment addin button assigned in TBaseAddinManager.Create');
  Assert(Assigned(Self.DM), 'No data module assigned in TBaseAddinManager.Create');
end;

destructor TBaseAddinManager.Destroy;
begin
  inherited;
end;

function TBaseAddinManager.GetQManagerXML(Doc: IXMLDOMDocument): IXMLDOMElement;
begin
  Result := Doc.createElement('QManager');
  Result.setAttribute('Company', DM.UQState.CompanyShortName);
  Result.setAttribute('Username', DM.UQState.UserName);
  Result.setAttribute('EmployeeNumber', EmployeeDM.GetUserEmployeeNumber);
  Result.setAttribute('EmployeeID', IntToStr(DM.EmpID));
end;

function TBaseAddinManager.NewInputDocument: IXMLDOMDocument;
begin
  Result := CoDOMDocument.Create;
  Result.async := False;
end;

function TBaseAddinManager.SelectedFileIsSketchFile: Boolean;
begin
  Result := SameText(ExtractFileExt(ActionAttachmentFilename), SKETCHEXT) or
            SameText(ExtractFileExt(ActionAttachmentFilename), '.@CV');
end;

function TBaseAddinManager.AddinType: TAddinType;
begin
  if AddinURL <> '' then
    Result := atChromeApplication
  else
    Result := atDefault;
end;

{ TODO -odan -c3398 : Move this to a descendant when refactoring }
function TBaseAddinManager.BrowserUserDataDir: string;
begin
  Result := MakePath(GetClientLocalDir, ChromeRelativeUserDataDir);
end;

function TBaseAddinManager.GenerateDamageInput: string;
var
  InputXML: IXMLDOMDocument;
  QMNode: IXMLDOMElement;
  DamageNode: IXMLDOMElement;
  DamageAsTicketNode: IXMLDOMElement;
  AttachmentNode: IXMLDOMElement;
  NewAttachmentNode: IXMLDOMElement;
  InputFile: string;

  procedure AddTicketElement(const Name, FieldName: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
    TextData: string;
  begin
    if HaveField(TicketData, FieldName) then
      TextData := TicketData.FieldByName(FieldName).AsString
    else
      raise Exception.Create('Invalid ID: ' + FieldName);

    Element := DamageNode.ownerDocument.createElement(Name);
    TextNode := DamageNode.ownerDocument.createTextNode(TextData);
    Element.appendChild(TextNode);
    DamageNode.appendChild(Element);
  end;

  procedure AddRelatedTicketData;
  var
    TicketNode: IXMLDOMElement;
    TicketAttachmentNode: IXMLDOMElement;
  begin
    LocateData := nil;
    TicketData := DM.Engine.OpenLiveQuery(Format(SelectTicket,
      [DamageData.FieldByName('ticket_id').AsInteger]));
    try
      LocateData := DM.Engine.OpenQuery(Format(SelectLocate,
        [DamageData.FieldByName('ticket_id').AsInteger,
        QuotedStr(NotAClientStatus)]));
      AddTicketElement('Latitude',  'work_lat');
      AddTicketElement('Longitude', 'work_long');
      TicketNode := GetTicketXML(InputXML);
      TicketAttachmentNode := GetAttachmentXML(InputXML, qmftTicket, DamageData.FieldByName('ticket_id').AsInteger, False);
      TicketNode.appendChild(TicketAttachmentNode);
      TicketNode.appendChild(GetTicketImageXML(InputXML));
      DamageNode.appendChild(TicketNode);
    finally
      FreeAndNil(TicketData);
      FreeAndNil(LocateData);
    end;
  end;

begin
  Assert(Assigned(DamageData), 'Damage dataset not assigned in GenerateDamageInput');

  InputXML := NewInputDocument;
  QMNode := GetQManagerXML(InputXML);
  DamageNode := GetDamageXML(InputXML);
  NewAttachmentNode := GetNewAttachmentFolderXML(InputXML);
  AttachmentNode := GetAttachmentXML(InputXML, qmftDamage, DamageData.FieldByName('damage_id').AsInteger, True);

  InputXML.appendChild(QMNode);

  // these 4 lines are only temporary until the Dycom addin app is updated & deployed
  DamageAsTicketNode := GetDamageAsTicketXML(InputXML);
  QMNode.appendChild(DamageAsTicketNode);
  DamageAsTicketNode.appendChild(NewAttachmentNode.cloneNode(True));
  DamageAsTicketNode.appendChild(AttachmentNode.cloneNode(True));
  //This attachment node call can replace the previous one when the temporary ticket node is removed
  AttachmentNode := GetAttachmentXML(InputXML, qmftDamage, DamageData.FieldByName('damage_id').AsInteger, False);

  QMNode.appendChild(DamageNode);
  DamageNode.appendChild(NewAttachmentNode);
  DamageNode.appendChild(AttachmentNode);

  InputFile := CreateInputFileName;
  if DamageData.FieldByName('ticket_id').AsInteger > 0 then
    AddRelatedTicketData; //get the sketch -or- ticket attachments for the ticket node

  Result := InputFile;
  SaveDebugFile(Result, PrettyPrintXml(InputXML, TicketDetailNodeName));
end;

function TBaseAddinManager.GenerateTicketInput: string;
var
  InputXML: IXMLDOMDocument;
  QMNode: IXMLDOMElement;
  TicketNode: IXMLDOMElement;
  NewAttachmentNode: IXMLDOMElement;
  AttachmentNode: IXMLDOMElement;
  DetailNode: IXMLDOMElement;
begin
  Assert(Assigned(TicketData), 'Ticket dataset not assigned in GenerateTicketInput');

  InputXML := NewInputDocument;
  QMNode := GetQManagerXML(InputXML);
  TicketNode := GetTicketXML(InputXML);
  NewAttachmentNode := GetNewAttachmentFolderXML(InputXML);
  AttachmentNode := GetAttachmentXML(InputXML, qmftTicket, TicketData.FieldByName('ticket_id').AsInteger);
  DetailNode := GetTicketImageXML(InputXML);

  InputXML.appendChild(QMNode);
  QMNode.appendChild(TicketNode);
  TicketNode.appendChild(NewAttachmentNode);
  TicketNode.appendChild(AttachmentNode);
  TicketNode.appendChild(DetailNode);
  Result := CreateInputFileName;
  SaveDebugFile(Result, PrettyPrintXml(InputXML, TicketDetailNodeName));
end;

function TBaseAddinManager.GenerateTicketURLSearchPart: string;
begin
  Result := 'tktStAddr=' + Trim(
    TicketData.FieldByName('work_address_number').AsString + ' ' +
    TicketData.FieldByName('work_address_street').AsString) +
    '&tktCty=' + TicketData.FieldByName('work_city').AsString +
    '&tktSt=' + TicketData.FieldByName('work_state').AsString +
    '&tktLat=' + TicketData.FieldByName('work_lat').AsString +
    '&tktLon=' + TicketData.FieldByName('work_long').AsString +
    '&tktUsrTok=' + EmployeeDM.GetUserEmployeeNumber +
    '&tktNum=' + TicketData.FieldByName('ticket_number').AsString +
    '&tktId=' + TicketData.FieldByName('ticket_id').AsString +
    '&tktDesc=' + TicketData.FieldByName('work_description').AsString +
    '&tktCo=' + DM.UQState.CompanyShortName +
    '&shwPlDlg=true';
end;

function TBaseAddinManager.GetTicketXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  TicketNode: IXMLDOMElement;
  LocatesNode: IXMLDOMElement;
  LocateNode: IXMLDOMElement;

  procedure AddTextNode(ParentNode: IXMLDOMElement; const Name, Text: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
  begin
    Element := Doc.createElement(Name);
    TextNode := Doc.createTextNode(Text);
    Element.appendChild(TextNode);
    ParentNode.appendChild(Element);
  end;

  procedure AddElement(ParentNode: IXMLDOMElement; DataSet: TDataSet; const Name, FieldName: string);
  var
    TextData: string;
  begin
    Assert(Assigned(DataSet));
    if HaveField(DataSet, FieldName) then
      TextData := DataSet.FieldByName(FieldName).AsString
    else
      raise Exception.Create('Invalid Field Name: ' + FieldName);

    AddTextNode(ParentNode, Name, TextData);
  end;

  procedure AddTicketElement(const Name, FieldName: string);
  begin
    AddElement(TicketNode, TicketData, Name, FieldName);
  end;

  procedure AddLocateElement(const Name, FieldName: string);
  begin
    AddElement(LocateNode, LocateData, Name, FieldName);
  end;

begin
  Assert(Assigned(LocateData));
  TicketNode := Doc.createElement('Ticket');
  TicketNode.setAttribute('ID', TicketData.FieldByName('ticket_id').AsString);
  TicketNode.setAttribute('Number', TicketData.FieldByName('ticket_number').AsString);
  TicketNode.setAttribute('ItemType', 'Ticket');

  AddTicketElement('Address1',  'work_address_number');
  AddTicketElement('Address2',  'work_address_number_2');
  AddTicketElement('Street',    'work_address_street');
  AddTicketElement('City',      'work_city');
  AddTicketElement('State',     'work_state');
  AddTicketElement('Latitude',  'work_lat');
  AddTicketElement('Longitude', 'work_long');
  AddTicketElement('WorkType',  'work_type');
  AddTicketElement('WorkRemarks', 'work_remarks');
  if not LocateData.IsEmpty then begin
    LocateData.First;
    LocatesNode := Doc.createElement('Locates');
    while not LocateData.Eof do begin
      LocateNode := Doc.createElement('Locate');
      LocateNode.setAttribute('ID', LocateData.FieldByName('locate_id').AsString);
      AddLocateElement('Term', 'client_code');
      AddLocateElement('Client', 'client_name');
      AddLocateElement('Status', 'status');
      AddTextNode(LocateNode, 'Utility', DM.GetClientUtilityType(LocateData.FieldByName('client_id').AsInteger));
      LocateData.Next;
      LocatesNode.appendChild(LocateNode);
    end;
    TicketNode.appendChild(LocatesNode);
  end;

  Result := TicketNode;
end;

function TBaseAddinManager.GetAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer;
  const IsDamageAsTicketNode: Boolean): IXMLDOMElement;
begin
  if IsDamageAsTicketNode then begin
    if SelectedFileIsSketchFile then
     //Only the sketch file goes in the temporary ticket node; pass 0 in the foreign type so the query knows to look by ID vs damage or ticket type
      Result := GetSketchAttachmentXML(Doc, 0,0)
    else //Here the Damage attachments go into the temporary Dycom ticket node.  So far we don't need the ticket's attachments in here too.
      Result := GetDamageOrTktAttachmentXML(Doc, ForeignType, ForeignID);
  end
  else if SelectedFileIsSketchFile then //only sketch file attachment goes in file
    Result := GetSketchAttachmentXML(Doc, ForeignType, ForeignID)
  else
    Result := GetDamageOrTktAttachmentXML(Doc, ForeignType, ForeignID);
end;

function TBaseAddinManager.GetSketchAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer): IXMLDOMElement;
var
  Attachment: TDataSet;
  AttachNode: IXMLDOMElement;
  FileName: string;
  AttachmentID: Integer;
  AttachmentActive: Boolean;

  procedure AddAttachmentWithAction(const SketchFileName, BaseImageFileName, Action: string);
  var
    ActionNode: IXMLDOMElement;
    SketchNameNode: IXMLDOMElement;
    BaseImageNameNode: IXMLDOMElement;
    TextData: IXMLDOMCharacterData;
  begin
    ActionNode := Doc.createElement('Action');
    ActionNode.setAttribute('type', Action);
    SketchNameNode := Doc.createElement('SketchFileName');
    TextData := Doc.createTextNode(SketchFileName);
    SketchNameNode.appendChild(TextData);
    ActionNode.appendChild(SketchNameNode);
    //only add the following lines if we are using the older .sketch extension
    if SameText(ExtractFileExt(FileName), SKETCHEXT) then begin
      BaseImageNameNode := Doc.createElement('BaseImageFileName');
      TextData := Doc.createTextNode(BaseImageFileName);
      BaseImageNameNode.appendChild(TextData);
      ActionNode.appendChild(BaseImageNameNode);
    end;
    AttachNode.appendChild(ActionNode);
  end;

var
  BaseImageFileName: string;
begin
  AttachNode := Doc.createElement('Attachments');

  //If a sketch file was selected, then we write the sketch and corresponding baseimage.jpg attachment.
  //Sketch and related baseimage files are placed under the first temporary ticket node as well as respective node
  // for ticket/damage the file was attached to(specified by Dycom).  Thus for the first ticket node
  //the foreign type does not matter, just find the selected attachment by id and
  // corresponding baseimage file utilizing selected sketch attachment name.
  if ForeignType = 0 then begin//this is the top ticket node
    BaseImageFileName := ExtractFileName(Copy(ActionAttachmentFilename, 1, Length(ActionAttachmentFilename)-13) + '%baseimage.jpg');
    Attachment := DM.Engine.OpenQuery(Format(SelectSketchAndBaseImageAttachments, [ActionAttachmentID, QuotedStr(BaseImageFileName)]));
  end
  else //this is for either the ticket or damage regular nodes
    Attachment := DM.Engine.OpenQuery(Format(SelectAttachment, [ForeignType, ForeignID]));

  try
    // Sketch files have a single Action element
    if HaveSelectedAttachment then begin
      if Attachment.Locate('attachment_id', ActionAttachmentID, []) then begin
        FileName := Attachment.FieldByName('orig_filename').AsString;
        if SelectedFileIsSketchFile then begin
          AttachmentID := Attachment.FieldByName('attachment_id').AsInteger;
          AttachmentActive := Attachment.FieldByName('active').AsBoolean;
          if AttachmentActive then begin
            if (Attachment.FieldByName('attachment_id').AsInteger > 0)
              and (Attachment.FieldByName('upload_date').AsDateTime > 1) then
              FileName := DM.DownloadAttachment(AttachmentID)
            else if Attachment.FieldByName('attached_by').AsInteger = DM.EmpID then
              FileName := DM.CopyAttachmentToDownloadFolder(AttachmentID);
            //the rest is for .sketch only
            if SameText(ExtractFileExt(FileName), SKETCHEXT) then begin
              BaseImageFileName := ExtractFileName(Copy(FileName, 1, Length(FileName)-13) + '%baseimage.jpg');
              Attachment.Filter:= Format('orig_filename like %s and source= %s and active', [QuotedStr(BaseImageFileName), QuotedStr(AddinAttachmentSource)]);
              Attachment.Filtered:=True;
              if Attachment.RecordCount = 1 then begin
                if Attachment.FieldByName('upload_date').AsDateTime > 1 then
                  BaseImageFileName := DM.DownloadAttachment(Attachment.FieldByName('attachment_id').AsInteger)
                else if Attachment.FieldByName('attached_by').AsInteger = DM.EmpID then
                  BaseImageFileName := DM.CopyAttachmentToDownloadFolder(Attachment.FieldByName('attachment_id').AsInteger);
              end
              else if Attachment.RecordCount > 1 then
                ShowMessageFmt('Multiple base image attachments were found with the same filename (%s) so the base image file can not be included in the eSketch XML output.', [BaseImageFileName]);
              Attachment.Filtered := False;
              Attachment.Filter := '';
            end;
            //end .sketch only section
            if not FileExists(BaseImageFileName) then
              BaseImageFileName := '';
            AddAttachmentWithAction(FileName, BaseImageFileName, 'open');
            Result := AttachNode;
            Exit;
          end;
        end;
      end
      else
        if ActionForeignType = ForeignType then
          raise Exception.Create('Cannot locate attachment to load with attachment_id ' + IntToStr(ActionAttachmentID));
    end;
  finally
    if Assigned(Attachment) then
      FreeAndNil(Attachment);
  end;

  Result := AttachNode;
end;


function TBaseAddinManager.GetDamageOrTktAttachmentXML(Doc: IXMLDOMDocument; const ForeignType, ForeignID: Integer): IXMLDOMElement;
var
  Attachment: TDataSet;
  AttachNode: IXMLDOMElement;
  Source: string;
  FileName: string;
  AttachmentID: Integer;
  AttachmentActive: Boolean;

  procedure AddAttachment(const FileName: string; ID: Integer; const Source: string; AttachmentActive: Boolean);
  var
    FileNode: IXMLDOMElement;
    NameNode: IXMLDOMElement;
    OriginalNameNode: IXMLDOMElement;
    TextData: IXMLDOMCharacterData;
  begin
    FileNode := Doc.createElement('Attachment');
    FileNode.setAttribute(AttachmentIDAttrName, IntToStr(ID));
    FileNode.setAttribute(AttachmentSourceAttrName, Source);
    if AttachmentActive then
      FileNode.setAttribute(AttachmentActiveAttrName, '1')
    else
      FileNode.setAttribute(AttachmentActiveAttrName, '0');
    NameNode := Doc.createElement(FileNameNodeName);
    OriginalNameNode := Doc.createElement(OriginalFileNameNodeName);
    TextData := Doc.createTextNode(FileName);
    NameNode.appendChild(TextData);
    TextData := Doc.createTextNode(ExtractFileName(FileName));
    OriginalNameNode.appendChild(TextData);
    if SameText(Source, AddinAttachmentSource) then
      FileNode.appendChild(NameNode);
    FileNode.appendChild(OriginalNameNode);
    AttachNode.appendChild(FileNode);
  end;

begin
  AttachNode := Doc.createElement('Attachments');

  Attachment := DM.Engine.OpenQuery(Format(SelectAttachment, [ForeignType, ForeignID]));
  try
    //Only when a sketch file is not selected - write the rest of the attachments.
    //(Necessary to check this because when an attachment is selected from damage screen-this method is
    //called twice-once for damage's attachments and once for damage's ticket's attachments)
    Attachment.First;
    while not Attachment.Eof do begin
      Source := '';
      FileName := Attachment.FieldByName('orig_filename').AsString;
      AttachmentID := Attachment.FieldByName('attachment_id').AsInteger;
      AttachmentActive := Attachment.FieldByName('active').AsBoolean;
      if SameText(Attachment.FieldByName('source').AsString, AddinAttachmentSource) then begin
        if AttachmentActive then begin
          if (Attachment.FieldByName('upload_date').IsNull) and
            (Attachment.FieldByName('attached_by').AsInteger = DM.EmpID) then
            FileName := DM.CopyAttachmentToDownloadFolder(Attachment.FieldByName('attachment_id').AsInteger);
        end;
        Source := AddinAttachmentSource;
      end;
      AddAttachment(FileName, AttachmentID, Source, AttachmentActive);
      Attachment.Next;
    end;
  finally
    if Assigned(Attachment) then
      FreeAndNil(Attachment);
  end;

  Result := AttachNode;
end;

function TBaseAddinManager.HaveSelectedAttachment: Boolean;
begin
  Result := ActionAttachmentID <> NoAttachmentSelected;
end;

function TBaseAddinManager.GetDamageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  DamageNode: IXMLDOMElement;

  procedure AddDamageElement(const Name, FieldName: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
    TextData: string;
  begin
    if HaveField(DamageData, FieldName) then
      TextData := DamageData.FieldByName(FieldName).AsString
    else
      raise Exception.Create('Invalid ID: ' + FieldName);

    Element := Doc.createElement(Name);
    TextNode := Doc.createTextNode(TextData);
    Element.appendChild(TextNode);
    DamageNode.appendChild(Element);
  end;

begin
  DamageNode := Doc.createElement('Damage');
  DamageNode.setAttribute('ID', DamageData.FieldByName('damage_id').AsString);
  DamageNode.setAttribute('Number', DamageData.FieldByName('uq_damage_id').AsString);

  AddDamageElement('Street', 'location');
  AddDamageElement('City',   'city');
  AddDamageElement('County', 'county');
  AddDamageElement('State',  'state');

  Result := DamageNode;
end;

// This is an temporary change to format the damage xml input file like the ticket input
function TBaseAddinManager.GetDamageAsTicketXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  DamageNode: IXMLDOMElement;

  procedure AddDamageElement(const Name, FieldName: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
    TextData: string;
  begin
    if HaveField(DamageData, FieldName) then
      TextData := DamageData.FieldByName(FieldName).AsString
    else
      raise Exception.Create('Invalid ID: ' + FieldName);

    Element := Doc.createElement(Name);
    TextNode := Doc.createTextNode(TextData);
    Element.appendChild(TextNode);
    DamageNode.appendChild(Element);
  end;

begin
  DamageNode := Doc.createElement('Ticket');
  DamageNode.setAttribute('ID', DamageData.FieldByName('damage_id').AsString);
  DamageNode.setAttribute('Number', DamageData.FieldByName('uq_damage_id').AsString);
  DamageNode.setAttribute('ItemType', 'Damage');

  AddDamageElement('Street', 'location');
  AddDamageElement('City',   'city');
  AddDamageElement('County', 'county');
  AddDamageElement('State',  'state');

  Result := DamageNode;
end;

function TBaseAddinManager.CreateInputFileName: string;
begin
  Result := GetWindowsTempFileName(False);
  Result := ChangeFileExt(Result, '.xml');
end;

function TBaseAddinManager.BuildAddinCommandLine(const InputFile, OutputFile: string): string;
begin
  Result := '"' + AddinFileName + '"';
end;

function TBaseAddinManager.RunAddin(const InputFile: string): string;
var
  AddinCommand: string;
  OutputFile: string;
begin
  Assert(FileExists(AddinFileName));
  DM.StopTimeImportTimer;
  DM.eSketchIsRunning := True;

  if AddinType = atDefault then
    OutputFile := ChangeFileExt(InputFile, '_out.xml');

  if (AddinType = atChromeApplication) and not ForceDirectories(
    BrowserUserDataDir) then
    raise Exception.CreateFmt('Unable to create the browser''s user data ' +
      'directory: %s.', [BrowserUserDataDir]);

  Application.Minimize;
  try
    AddinCommand := BuildAddinCommandLine(InputFile, OutputFile);
    // This call may take a long time while the user uses the addin.
    if DWORD(-1) = WinExecAndWait32V2(AddinCommand, SW_SHOWNORMAL) then
      raise Exception.CreateFmt('Unable to execute %s: %s', [AddinFileName, SysErrorMessage(GetLastError)]);
  finally
    Application.Restore;
  end;

  if AddinType = atDefault then
    Result := OutputFile
  else
    Result := '';

  DM.eSketchIsRunning := False;
  DM.StartTimeImportTimer;
end;

function TBaseAddinManager.GetNewAttachmentFolderXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  TextNode: IXMLDOMCharacterData;
  TextData: string;
begin
  TextData := DM.GetAttachmentDownloadFolder;
  Result := Doc.createElement(AttachmentFolderNodeName);
  TextNode := Doc.createTextNode(TextData);
  Result.appendChild(TextNode);
end;

function TBaseAddinManager.GetTicketImageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  ImageNode: IXMLDOMCDATASection;
  Image: string;
  TicketIsXml: Boolean;
begin
  Image := DM.GetTicketImage(TicketData, TicketIsXml);
  Result := Doc.createElement(TicketDetailNodeName);
  ImageNode := Doc.createCDATASection(Image);
  Result.appendChild(ImageNode);
end;

// The work order is formatted like the ticket xml input file
function TBaseAddinManager.GenerateWorkOrderInput: string;
var
  InputXML: IXMLDOMDocument;
  QMNode: IXMLDOMElement;
  TicketNode: IXMLDOMElement;
  NewAttachmentNode: IXMLDOMElement;
  AttachmentNode: IXMLDOMElement;
  DetailNode: IXMLDOMElement;
begin
  Assert(Assigned(WorkOrderData), 'Work Order dataset not assigned in GenerateWorkOrderInput');

  InputXML := NewInputDocument;
  QMNode := GetQManagerXML(InputXML);
  TicketNode := GetWorkOrderXML(InputXML);
  NewAttachmentNode := GetNewAttachmentFolderXML(InputXML);
  AttachmentNode := GetAttachmentXML(InputXML, qmftWorkOrder, WorkOrderData.FieldByName('wo_id').AsInteger);
  DetailNode := GetWorkOrderImageXML(InputXML);

  InputXML.appendChild(QMNode);
  QMNode.appendChild(TicketNode);
  TicketNode.appendChild(NewAttachmentNode);
  TicketNode.appendChild(AttachmentNode);
  TicketNode.appendChild(DetailNode);
  Result := CreateInputFileName;
  SaveDebugFile(Result, PrettyPrintXml(InputXML, TicketDetailNodeName));
end;

function TBaseAddinManager.GetWorkOrderXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  TicketNode: IXMLDOMElement;
  LocatesNode: IXMLDOMElement;
  LocateNode: IXMLDOMElement;

  procedure AddTextNode(ParentNode: IXMLDOMElement; const Name, Text: string);
  var
    Element: IXMLDOMElement;
    TextNode: IXMLDOMCharacterData;
  begin
    Element := Doc.createElement(Name);
    TextNode := Doc.createTextNode(Text);
    Element.appendChild(TextNode);
    ParentNode.appendChild(Element);
  end;

  procedure AddElement(ParentNode: IXMLDOMElement; DataSet: TDataSet; const Name, FieldName: string);
  var
    TextData: string;
  begin
    Assert(Assigned(DataSet));
    if HaveField(DataSet, FieldName) then
      TextData := DataSet.FieldByName(FieldName).AsString
    else
      raise Exception.Create('Invalid Field Name: ' + FieldName);

    AddTextNode(ParentNode, Name, TextData);
  end;

  procedure AddWOTickElement(const Name, FieldName: string);
  begin
    AddElement(TicketNode, WorkOrderData, Name, FieldName);
  end;

  procedure AddWOLocElement(const Name, FieldName: string);
  begin
    AddElement(LocateNode, WorkOrderData, Name, FieldName);
  end;

begin
  Assert(Assigned(WorkOrderData));
  TicketNode := Doc.createElement('Ticket');
  TicketNode.setAttribute('ID', WorkOrderData.FieldByName('wo_id').AsString);
  TicketNode.setAttribute('Number', WorkOrderData.FieldByName('wo_number').AsString);
  TicketNode.setAttribute('ItemType', 'Work Order');

  AddWOTickElement('Address1',  'work_address_number');
  AddWOTickElement('Address2',  'work_address_number_2');
  AddWOTickElement('Street',    'work_address_street');
  AddWOTickElement('City',      'work_city');
  AddWOTickElement('State',     'work_state');
  AddWOTickElement('Latitude',  'work_lat');
  AddWOTickElement('Longitude', 'work_long');
  AddWOTickElement('WorkType',  'work_type');
  AddWOTickElement('WorkRemarks', 'work_description');

  LocatesNode := Doc.createElement('Locates');
  LocateNode := Doc.createElement('Locate');
  LocateNode.setAttribute('ID', WorkOrderData.FieldByName('wo_id').AsString);
  AddWOLocElement('Term', 'oc_code');
  AddWOLocElement('Client', 'client_name');
  AddWOLocElement('Status', 'status');
  AddTextNode(LocateNode, 'Utility', DM.GetClientUtilityType(WorkOrderData.FieldByName('client_id').AsInteger));
  LocatesNode.appendChild(LocateNode);
  TicketNode.appendChild(LocatesNode);

  Result := TicketNode;
end;

function TBaseAddinManager.GetWorkOrderImageXML(Doc: IXMLDOMDocument): IXMLDOMElement;
var
  ImageNode: IXMLDOMCDATASection;
  Image: string;
  WOIsXml: Boolean;
begin
  Image := DM.GetWorkOrderImage(WorkOrderData, WOIsXml);
  Result := Doc.createElement(TicketDetailNodeName);
  ImageNode := Doc.createCDATASection(Image);
  Result.appendChild(ImageNode);
end;

end.
