unit TimeSheetCalloutAdd;
//QM-442 SR dialog to permit adding call out time to preceding day
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, StdCtrls, Buttons, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxTimeEdit;

type
  TfrmCallOutDialog = class(TForm)
    coStart: TcxTimeEdit;
    coStop: TcxTimeEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lblStart: TLabel;
    lblStop: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure coStartExit(Sender: TObject);
  private
    { Private declarations }
    lastCallOut:TDateTime;
    calloutStart, calloutStop:string;
    yesterdayEntryID:integer;
    function updateCallouts:boolean;
    function GetNextCalloutForYesterday: integer;
  public
    { Public declarations }

  end;

var
  frmCallOutDialog: TfrmCallOutDialog;
  YesterdayCallOuts:boolean=false;
implementation

uses Timesheet, DMu, dateutils, LocalEmployeeDMu;

{$R *.dfm}

procedure TfrmCallOutDialog.BitBtn1Click(Sender: TObject);
begin
  if not(coStop.Time> coStart.Time) then
  begin
    showmessage('Stop time must be greater than Start time');
    coStop.Clear;
  end
  else
    updateCallouts;
end;

procedure TfrmCallOutDialog.coStartExit(Sender: TObject);
begin
  if coStart.Time<TimeOf(lastCallOut) then
  begin
    MessageDlg('You cannot start a Call Out earlier than yesterday''s last Call Out return time.'
    +#13#13+ 'Your earliest time must be after '+TimeToStr(TimeOf(lastCallOut)), mtWarning, [mbAbort], 0);
    coStart.Time:= lastCallOut;
    coStart.SetFocus;
  end;
  
end;

procedure TfrmCallOutDialog.FormActivate(Sender: TObject);
begin
  yesterdayEntryID:= GetNextCalloutForYesterday;
  if (yesterdayEntryID=0) then showmessage('No available Call Outs for '+dateToStr(yesterday));
end;

procedure TfrmCallOutDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  action := cafree;
  YesterdayCallOuts:=false;
end;

procedure TfrmCallOutDialog.FormCreate(Sender: TObject);
begin
  self.Caption:= DateToStr(yesterday)+ ' Callout';
  YesterdayCallOuts:=true;
end;

function TfrmCallOutDialog.updateCallouts:boolean;
const UPD_CALLOUTS=
  'update timesheet_entry '+
  'set %s = :CalloutStart, '+
  '%s= :CalloutStop '+
  'where entry_id = :EntryID';
begin
  dm.updNextUnusedCallOut.sql.Text := format(UPD_CALLOUTS,[calloutStart, calloutStop]);
  dm.updNextUnusedCallOut.ParamByName('CalloutStart').Value:= coStart.time;
  dm.updNextUnusedCallOut.ParamByName('CalloutStop').Value:= coStop.Time;
  dm.updNextUnusedCallOut.ParamByName('EntryID').Value:= yesterdayEntryID;

  dm.updNextUnusedCallOut.ExecSQL;
end;

function TfrmCallOutDialog.GetNextCalloutForYesterday:integer;
var
  I : Integer;
begin
  result:= 0;
  lastCallOut:=yesterday;
  with EmployeeDM.qryAbandonedCallOut do
  begin
    ParamByName('empid').Value:=dm.EmpID;
    ParamByName('yesterday').Value:= yesterday;
    try
        Open;

        for I := 1 to 16 do
        begin
          calloutStart:=''; calloutStop:='';
          calloutStart :='callout_start' + IntToStr(I);
          calloutStop  := 'callout_stop' + IntToStr(I);

          if FieldByName('callout_start' + IntToStr(I)).IsNull and
             FieldByName('callout_stop' + IntToStr(I)).IsNull then
             begin
               result:= FieldByName('entry_id').AsInteger;
               if I>1 then
               begin
                 lastCallOut:= FieldByName('callout_stop' + IntToStr(I-1)).AsDateTime;
                 coStart.Time:= lastCallOut;
               end;
               lblStart.Caption:=calloutStart;
               lblStop.Caption :=calloutStop;
               break;
             end;
        end;
    finally
      close;
    end;
  end;
end;


end.
