object LoginForm: TLoginForm
  Left = 306
  Top = 206
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Log In'
  ClientHeight = 277
  ClientWidth = 403
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnClick = FormClick
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel
    Left = 48
    Top = 186
    Width = 49
    Height = 13
    Caption = 'User ID:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 48
    Top = 210
    Width = 59
    Height = 13
    Caption = 'Password:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object AppLabel: TLabel
    Left = 42
    Top = 110
    Width = 313
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = '[Application Label]'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 5
    Top = 255
    Width = 44
    Height = 13
    Caption = 'Server:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object ServerLabel: TLabel
    Left = 75
    Top = 255
    Width = 16
    Height = 14
    Caption = '----'
  end
  object Bevel2: TBevel
    Left = 8
    Top = 250
    Width = 389
    Height = 3
    Shape = bsTopLine
  end
  object VersionLabel: TLabel
    Left = 42
    Top = 137
    Width = 313
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Version ___'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object LogoImage: TImage
    Left = 0
    Top = 0
    Width = 403
    Height = 115
    Margins.Left = 5
    Margins.Right = 5
    Align = alTop
    Center = True
  end
  object HelpDeskContact: TLabel
    Left = 42
    Top = 155
    Width = 313
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Help Desk Phone ___'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object EditUserName: TEdit
    Left = 112
    Top = 181
    Width = 141
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object EditPassword: TEdit
    Left = 112
    Top = 207
    Width = 141
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 40
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 1
  end
  object LoginBtn: TButton
    Left = 272
    Top = 179
    Width = 65
    Height = 24
    Caption = 'Login'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = LoginBtnClick
  end
  object CancelBtn: TButton
    Left = 272
    Top = 205
    Width = 65
    Height = 24
    Cancel = True
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 4
    OnClick = CancelBtnClick
  end
  object RememberBox: TCheckBox
    Left = 112
    Top = 230
    Width = 81
    Height = 17
    Caption = 'Remember'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object ServerListCombo: TComboBox
    Left = 47
    Top = 252
    Width = 348
    Height = 19
    BevelInner = bvNone
    BevelKind = bkFlat
    BevelOuter = bvNone
    Style = csOwnerDrawFixed
    Ctl3D = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    Visible = False
    OnChange = ServerListComboChange
    OnDrawItem = ServerListComboDrawItem
  end
  object btnPWReset: TBitBtn
    Left = 7
    Top = 226
    Width = 98
    Height = 21
    Caption = 'Password Reset'
    TabOrder = 6
    Visible = False
    OnClick = btnPWResetClick
  end
end
