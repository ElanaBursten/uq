unit DamageNotes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DB, DBISAMTb, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxMaskEdit, cxTextEdit, cxCalendar,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxNavigator;

type
  TNotesFrame = class(TFrame)
    HeaderPanel: TPanel;
    NoteLabel: TLabel;
    NoteText: TMemo;
    AddNoteButton: TButton;
    FooterPanel: TPanel;
    ViewAllCheck: TCheckBox;
    NotesSource: TDataSource;
    Notes: TDBISAMQuery;
    NotesGrid: TcxGrid;
    NotesGridLevel: TcxGridLevel;
    NotesGridView: TcxGridDBTableView;
    NotesGridViewnotes_id1: TcxGridDBColumn;
    NotesGridViewnotes_type_desc1: TcxGridDBColumn;
    NotesGridViewforeign_id1: TcxGridDBColumn;
    NotesGridViewClaimant: TcxGridDBColumn;
    NotesGridViewentry_date1: TcxGridDBColumn;
    NotesGridViewemployee_name1: TcxGridDBColumn;
    NotesGridViewuid1: TcxGridDBColumn;
    NotesGridViewactive1: TcxGridDBColumn;
    NotesGridViewNote: TcxGridDBColumn;
    NotesGridRestrictionCol: TcxGridDBColumn;
    procedure AddNoteButtonClick(Sender: TObject);
    procedure ViewAllCheckClick(Sender: TObject);
    procedure NotesBeforeOpen(DataSet: TDataSet);
    procedure NotesAfterPost(DataSet: TDataSet);
    procedure NotesCalcFields(DataSet: TDataSet);
  private
    FNoteType: Integer;
    FNoteSubType: String; //Set from the controlling form. Code saved in Reference table
    FSubTypeText: String; //Text from the reference table
    FDamageID: Integer;
    FOtherID: Integer;
    FAfterNoteChange: TNotifyEvent;
    procedure FilterNotes(const NoteType: Integer);
    function AddNote(const NoteText: string; NoteType: Integer): boolean;
    procedure ViewAllNotesForDamage;
    function NotesData: TDataSet;
    procedure NoteDataChanged;
  public
    procedure SetParent(aParent: TWinControl); override;
    procedure RefreshNow;
    procedure Initialize(const NoteType, DamageID, OtherID: Integer);
    property AfterNoteChange: TNotifyEvent read FAfterNoteChange write FAfterNoteChange;
    property NoteSubType: string read fNoteSubType write fNoteSubType;
    property SubTypeText: string read fSubTypeText write fSubTypeText;
  end;

implementation

uses OdHourGlass, OdVclUtils, OdMiscUtils, OdDbUtils, OdExceptions, QMConst,
  DMu, StrUtils, LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

{ TDamageNotesFrame }

procedure TNotesFrame.SetParent(aParent: TWinControl);
begin
  inherited;
  if Assigned(aParent) then
    DM.Engine.LinkEventsAutoInc(Notes);
end;

procedure TNotesFrame.AddNoteButtonClick(Sender: TObject);
begin
  if Trim(NoteText.Text) = '' then
    raise EOdEntryRequired.Create('Please enter some note text');

  DM.VerifyNoteLength(NoteText.Text);

  If AddNote(NoteText.Text, FNoteType) then
    NoteText.Clear;
end;

function TNotesFrame.AddNote(const NoteText: string; NoteType: Integer): boolean;
var
  NoteSubTypeInt: Integer;
begin
  try
    NoteSubTypeInt := StrToInt(NoteSubType);
  except
    MessageDlg('Please select Note Restriction level', mtError, [mbOK],0);
    Result := False;
    exit;
  end;
  OpenDataSet(NotesData);
  NotesData.Insert;
  NotesData.FieldByName('entry_date').AsDateTime := RoundSQLServerTimeMS(Now);
  NotesData.FieldByName('uid').AsInteger := DM.UQState.UserID;
  NotesData.FieldByName('active').AsBoolean := True;
  NotesData.FieldByName('foreign_type').AsInteger := NoteType;
  if NoteType = qmftDamage then
    NotesData.FieldByName('foreign_id').AsInteger := FDamageID
  else
    NotesData.FieldByName('foreign_id').AsInteger := FOtherID;
  NotesData.FieldByName('note').AsString := ReplaceControlChars(NoteText);
  NotesData.FieldByName('modified_date').AsDateTime := Now;
  NotesData.FieldByName('sub_type').AsInteger := NoteSubTypeInt;

  // client only fields used to match up the foreign_type to the right esttype reference code value
  // and filtering
  NotesData.FieldByName('damage_notes_type').AsInteger := NoteType - 2;
  NotesData.FieldByName('damage_id').AsInteger := FDamageID;
  DM.AssignLocalKeys(NotesData, LeftStr(Self.Name, 3));
  try
    NotesData.Post;
    Sleep(10); // Prevent duplicate notes with the same rounded date
    DM.FlushDatabaseBuffers;
  except
    NotesData.Cancel;
    raise;
  end;
  RefreshNow;
  Result := True;
end;

procedure TNotesFrame.FilterNotes(const NoteType: Integer);
var
  Filter: string;
begin
  Assert(Assigned(NotesSource.DataSet), 'A Notes data source must be assigned first');
  NotesData.DisableControls;
  try
    CloseDataSet(NotesData);
    if NoteType = qmftDamage then
      Filter := ' and foreign_id=' + IntToStr(FDamageID)
    else
      Filter := ' and foreign_id=' + IntToStr(FOtherID);

    Filter := 'foreign_type=' + IntToStr(NoteType) + Filter;
    NotesData.Filter := Filter;
    NotesData.Filtered := True;
    NotesData.Open;
  finally
    NotesData.EnableControls;
  end;
end;

procedure TNotesFrame.Initialize(const NoteType, DamageID, OtherID: Integer);
begin
  NoteText.MaxLength := MaxNoteLength;
  FNoteType := NoteType;
  FDamageID := DamageID;
  FOtherID := OtherID;
  RefreshNow;
end;

procedure TNotesFrame.RefreshNow;
var
  EnableAdd: Boolean;
begin
  EnableAdd := (FDamageID > 0);
  if EnableAdd and (FNoteType in [qmftLitigation, qmft3rdParty]) then
    EnableAdd := PermissionsDM.CanI(RightLitigationEdit) and (FOtherID > 0);

  ViewAllCheck.Enabled := PermissionsDM.EmployeeHasLitigationRights;
  SetEnabledOnControlAndChildren(HeaderPanel, EnableAdd);
  FilterNotes(FNoteType);
end;

procedure TNotesFrame.ViewAllNotesForDamage;
begin
  NotesData.DisableControls;
  try
    CloseDataSet(NotesData);
    NotesData.Filter := 'damage_id = ' + IntToStr(FDamageID);
    NotesData.Filtered := True;
    NotesData.Open;
  finally
    NotesData.EnableControls;
  end;
end;

function TNotesFrame.NotesData: TDataSet;
begin
  Result := NotesSource.DataSet;
end;

procedure TNotesFrame.NoteDataChanged;
begin
  if Assigned(FAfterNoteChange) then
    FAfterNoteChange(Self);
end;

procedure TNotesFrame.ViewAllCheckClick(Sender: TObject);
begin
  if ViewAllCheck.Checked then
    ViewAllNotesForDamage
  else
    FilterNotes(FNoteType);
end;

procedure TNotesFrame.NotesBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('employee_name', DataSet, 'uid', EmployeeDM.UserLookup,
    'uid', 'short_name', ShortNameLenth, 'EmployeeNameLookup', 'Employee');
  AddLookupField('notes_type_desc', DataSet, 'damage_notes_type', DM.EstimateTypeLookup,
    'code', 'description', 10, 'NotesTypeDescField');
  AddCalculatedField('claimant_name', DataSet, TStringField, 25);
  AddCalculatedField('Restriction', Dataset, TStringField, 40);
  DataSet.OnCalcFields := NotesCalcFields;
end;

procedure TNotesFrame.NotesAfterPost(DataSet: TDataSet);
begin
  NoteDataChanged;
end;

procedure TNotesFrame.NotesCalcFields(DataSet: TDataSet);
begin
  if FNoteType = qmftDamage then
    DataSet.FieldByName('claimant_name').Value := ''
  else if FNoteType = qmft3rdParty then
    DataSet.FieldByName('claimant_name').Value := ''
  else if FNoteTYpe = qmftLitigation then
    DataSet.FieldByName('claimant_name').Value := ''
  else
    DataSet.FieldByName('claimant_name').Value := '-';

  if Dataset.FieldByName('sub_type').AsInteger >= 0 then
    Dataset.FieldByName('Restriction').AsString :=
        DM.GetSubTypeDescription(Dataset.FieldByName('sub_type').AsInteger);
  
  if Dataset.FieldByName('sub_type').AsInteger >= 0 then
    Dataset.FieldByName('Restriction').AsString :=
      DM.GetSubTypeDescription(Dataset.FieldByName('sub_type').AsInteger);
end;

end.
