unit DamageDetails;

interface                                              

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  OdEmbeddable, Dialogs, ComCtrls, StdCtrls, DBCtrls, Mask, DB, DBISAMTb, DMu, LocalDamageDMu,
  ExtCtrls, ActnList, DBGrids, Attachments, DamageEstimate, Invoice,
  DamageInvoiceFrame, PhotoAttachment, DamageNotes, AttachmentsAddin, Menus,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxTextEdit, cxBlobEdit, cxLabel, cxMemo, cxCalendar, cxCheckBox,
  cxGridDBTableView, cxMaskEdit, cxContainer, cxDropDownEdit, cxDBEdit,
  cxButtonEdit, cxDBExtLookupComboBox,
  cxCurrencyEdit,dxCore, cxNavigator, CodeLookupList,
  ACustomForm, ACustomFormDMu, ACustomFormConst, cxData, Grids, cxLookupEdit,
  cxDBLookupEdit;

const
  UM_UPDATEDISPLAY = WM_USER + 423;
  UM_UPDATECOMBOS = WM_USER + 424;

type
  TCustDamageRec = record   //QM-593 EB Custom Damage Form AT&T
    RefID: integer;
    UtilityCoDamaged: string;
    State: string;
    FormID: integer;
    HasCustomQuestions: boolean;
    FormData: TFormInfo;
  end;

  TDamageDetailsForm = class(TEmbeddableForm)
    ConfirmationPanel: TPanel;
    CreatedDamageNumLabel: TLabel;
    ConfirmButton: TButton;
    InvestigationReqEdit: TEdit;
    DamageDetailsPanel: TPanel;
    DamagePageControl: TPageControl;
    NewDamageTab: TTabSheet;
    MainPanel: TPanel;
    DamageDateLabel: TLabel;
    LocateMarksPresentLabel: TLabel;
    LocateRequestedLabel: TLabel;
    NotifierContactNumberLabel: TLabel;
    LocationDetailsLabel: TLabel;
    OtherRemarksLabel: TLabel;
    UtilityCoClaimNumLabel: TLabel;
    InvestigatorLabel: TLabel;
    TicketNumLabel: TLabel;
    CityLabel: TLabel;
    CountyLabel: TLabel;
    StateLabel: TLabel;
    DamageLocationLabel: TLabel;
    ExcavatorLabel: TLabel;
    GridLabel: TLabel;
    MapPageLabel: TLabel;
    OtherFacTypeSize: TLabel;
    UtilityCoDamagedLabel: TLabel;
    NotifiedByCompanyLabel: TLabel;
    NotifiedByPersonLabel: TLabel;
    NotifiedDateLabel: TLabel;
    NotifiedDate: TDBText;
    ProfitCenterLabel: TLabel;
    State: TDBComboBox;
    LocateMarksPresent: TDBComboBox;
    LocateRequested: TDBComboBox;
    NotifiedByPhone: TDBEdit;
    LocationDetails: TDBEdit;
    UtilityCoClaimNum: TDBEdit;
    ExcavatorCompany: TDBEdit;
    Grid: TDBEdit;
    Page: TDBEdit;
    City: TDBEdit;
    County: TDBEdit;
    FacilityTypeSize: TDBEdit;
    NotifiedByCompany: TDBEdit;
    NotifiedByPerson: TDBEdit;
    Remarks: TDBMemo;
    ProfitCenter: TDBComboBox;
    DetailsTab: TTabSheet;
    DateMailedLabel: TLabel;
    DateFaxedLabel: TLabel;
    SentToLabel: TLabel;
    ExcavatorTypeLabel: TLabel;
    ExcavationTypeLabel: TLabel;
    DiagramNumberLabel: TLabel;
    DamageID: TDBText;
    DamageIDLabel: TLabel;
    DueDate: TDBText;
    DueDateLabel: TLabel;
    IntelexNum: TLabel;
    ClosedDateLabel: TLabel;
    ClosedDate: TDBText;
    LocatorExperienceLabel: TLabel;
    LocateEquipmentLabel: TLabel;
    TicketDueDate: TLabel;
    TicketDueDateLabel: TLabel;
    ClaimStatusLabel: TLabel;
    ClaimStatusDateLabel: TLabel;
    ClaimStatusDate: TDBText;
    InvoiceCodeLabel: TLabel;
    SentTo: TDBEdit;
    ExcavatorType: TDBComboBox;
    ExcavationType: TDBComboBox;
    DateMailed: TDBEdit;
    DateFaxed: TDBEdit;
    DiagramNumber: TDBEdit;
    LocatorExperience: TDBComboBox;
    LocateEquipment: TDBComboBox;
    LocateWasAProject: TDBCheckBox;
    ClaimStatus: TDBComboBox;
    InvoiceCode: TDBComboBox;
    SiteTab: TTabSheet;
    UtilitiesMarkedGroup: TGroupBox;
    OtherMarkedLabel: TLabel;
    Sewer: TDBCheckBox;
    Water: TDBCheckBox;
    CableTV: TDBCheckBox;
    Gas: TDBCheckBox;
    Power: TDBCheckBox;
    Telephone: TDBCheckBox;
    Other: TDBEdit;
    ExcavationGroup: TGroupBox;
    HandDigging: TDBCheckBox;
    MechanizedEquipment: TDBCheckBox;
    EquipmentGroup: TGroupBox;
    Boring: TDBCheckBox;
    Grading: TDBCheckBox;
    OpenTrench: TDBCheckBox;
    MarkPresenceGroup: TGroupBox;
    Paint: TDBCheckBox;
    Flags: TDBCheckBox;
    ImagesGroup: TGroupBox;
    P35mm: TDBCheckBox;
    Digital: TDBCheckBox;
    Video: TDBCheckBox;
    DiscussionsTab: TTabSheet;
    RepairTechGroup: TGroupBox;
    TechniciansLabel: TLabel;
    RepairsWereLabel: TLabel;
    RepairTechNameLabel: TLabel;
    RepairTechContactLabel: TLabel;
    DiscRepairTechsWere: TDBEdit;
    DiscRepairsWere: TDBEdit;
    DiscRepairPerson: TDBEdit;
    DiscRepairContact: TDBEdit;
    DiscRepairComment: TDBMemo;
    ExcavatorsGroup: TGroupBox;
    ExcavatorsLabel: TLabel;
    ExcavatorNameLabel: TLabel;
    ExcavatorContactLabel: TLabel;
    DiscExcWere: TDBEdit;
    DiscExcPerson: TDBEdit;
    DiscExcContact: TDBEdit;
    DiscExcComment: TDBMemo;
    OtherPerson1Group: TGroupBox;
    Other1NameLabel: TLabel;
    Other1ContactLabel: TLabel;
    DiscOther1Person: TDBEdit;
    DiscOther1Contact: TDBEdit;
    DiscOther1Comment: TDBMemo;
    OtherPerson2Group: TGroupBox;
    Other2NameLabel: TLabel;
    Other2ContactLabel: TLabel;
    DiscOther2Person: TDBEdit;
    DiscOther2Contact: TDBEdit;
    DiscOther2Comment: TDBMemo;
    ResponsibilityTab: TTabSheet;
    ExcavatorGroup: TGroupBox;
    ExcRespCodeLabel: TLabel;
    ExcOtherRespDescLabel: TLabel;
    ExcavatorSourceCommentsLabel: TLabel;
    ExcResponseLabel: TLabel;
    ExcDetails: TDBMemo;
    ExcResponse: TDBMemo;
    ExcRespCode: TDBComboBox;
    UtiliQuestGroup: TGroupBox;
    UQRespCodeLabel: TLabel;
    UQDetailsLabel: TLabel;
    UQOtherDescLabel: TLabel;
    UQEssentialStepLabel: TLabel;
    UQRespDetails: TDBMemo;
    UQRespOtherDesc: TDBEdit;
    UQRespCode: TDBComboBox;
    UQRespEssential: TDBComboBox;
    Special: TGroupBox;
    UtilityRespCodeLabel: TLabel;
    UtilityRespDetailsLabel: TLabel;
    SpecialDetails: TDBMemo;
    SpecialRespCode: TDBComboBox;
    InvestigatorTab: TTabSheet;             
    InvestigatorNarrativeLabel: TLabel;
    InvestigatorNarrative: TDBMemo;
    EstimatesTab: TTabSheet;
    InvoicesTab: TTabSheet;
    HistoryTab: TTabSheet;
    AttachmentsTab: TTabSheet;
    DamageAttachmentsFrame: TAttachmentsFrame;
    DamageSource: TDataSource;
    Investigator: TDBISAMTable;
    InvestigatorSource: TDataSource;
    Client: TDBISAMTable;
    ClientSource: TDataSource;
    OfficeSource: TDataSource;
    Office: TDBISAMTable;
    Ticket: TDBISAMTable;
    TicketSource: TDataSource;
    HistorySource: TDataSource;
    History: TDBISAMQuery;
    InvoiceSource: TDataSource;
    Invoices: TDBISAMQuery;
    DamageEstimateFrame: TEstimateFrame;
    Label1: TLabel;
    AddedByName: TDBText;
    DamageBillSource: TDataSource;
    DamageBill: TDBISAMQuery;
    BillingGroup: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    BilledBy: TDBText;
    Label4: TLabel;
    BilledOnDate: TDBText;
    ButtonPanel: TPanel;
    InvestigationIsLabel: TLabel;
    DamageTypeLabel: TLabel;
    SaveButton: TButton;
    CancelButton: TButton;
    PrintButton: TButton;
    CloseButton: TButton;
    PageControl: TPageControl;
    DamageTab: TTabSheet;
    ThirdPartyTab: TTabSheet;
    LitigationTab: TTabSheet;
    Actions: TActionList;
    DamageCancelAction: TAction;
    DamageSaveAction: TAction;
    PrintAction: TAction;
    CloseAction: TAction;
    ThirdParty: TDBISAMTable;
    ThirdPartySource: TDataSource;
    ThirdPartyPageControl: TPageControl;
    ThirdPartyNotifyTab: TTabSheet;
    ThirdPartyEstimateTab: TTabSheet;
    ThirdPartyEstimateFrame: TEstimateFrame;
    TabSheet3: TTabSheet;
    ThirdPartyAttachmentsFrame: TAttachmentsFrame;
    Litigation: TDBISAMTable;
    LitigationSource: TDataSource;
    Carrier: TDBISAMTable;
    CarrierSource: TDataSource;
    CarrierLookup: TDBISAMQuery;
    CarrierLookupSource: TDataSource;
    LitigationPageControl: TPageControl;
    LitigationNotifyTab: TTabSheet;
    DamageLitigationGrid: TDBGrid;
    LitigationEstimateTab: TTabSheet;
    EstimateAmountLabel: TLabel;
    LitigationEstimateFrame: TEstimateFrame;
    LitigationInvoiceTab: TTabSheet;
    TabSheet7: TTabSheet;
    LitigationAttachmentsFrame: TAttachmentsFrame;
    LitigationSaveAction: TAction;
    LitigationCancelAction: TAction;
    LitigationAddAction: TAction;
    Label10: TLabel;
    ThirdPartyClaimant: TDBEdit;
    ThirdPartyAddress1: TDBEdit;
    Label11: TLabel;
    ThirdPartyAddress2: TDBEdit;
    Label12: TLabel;
    ThirdPartyCity: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    ThirdPartyState: TDBComboBox;
    Label15: TLabel;
    ThirdPartyZip: TDBEdit;
    ThirdPartyPhone: TDBEdit;
    Label16: TLabel;
    ThirdPartyClaimDesc: TDBMemo;
    Label17: TLabel;
    Label18: TLabel;
    ThirdPartyUQDamageID: TDBText;
    Label19: TLabel;
    ThirdPartyNotificationDate: TDBText;
    Label20: TLabel;
    Label21: TLabel;
    ThirdPartyDeductible: TDBText;
    ThirdPartyCarrierOnNotice: TDBCheckBox;
    Label22: TLabel;
    ThirdPartyDemand: TDBEdit;
    Label23: TLabel;
    ThirdPartyStatus: TDBComboBox;
    TabSheet8: TTabSheet;
    SaveThirdPartyButton: TButton;
    CancelThirdPartyButton: TButton;
    ThirdPartySaveAction: TAction;
    ThirdPartyCancelAction: TAction;
    ThirdPartyGrid: TDBGrid;
    ThirdPartyAddButton: TButton;
    ThirdPartyAddAction: TAction;
    EditThirdPartyPanel: TPanel;
    EstimateLocked: TDBCheckBox;
    MarksGroup: TGroupBox;
    ToleranceLabel: TLabel;
    ToleranceMeasurement: TDBEdit;
    ClarityOfMarksLabel: TLabel;
    ClarityOfMarks: TDBComboBox;
    UtilityGroup: TGroupBox;
    BuriedUnder: TDBComboBox;
    FacilityWasPotholed: TDBComboBox;
    SireBurriedUnderLabel: TLabel;
    FacilityWasPotholedLabel: TLabel;
    NearestFacilityMeasureLabel: TLabel;
    NearestFacilityMeasure: TDBEdit;
    ToleranceFeetInches: TLabel;
    OffsetsGroup: TGroupBox;
    OffsetQtyLabel: TLabel;
    OffsetQuantity: TDBEdit;
    ExcavatorDoingRepairsLabel: TLabel;
    ExcavatorDoingRepairs: TDBEdit;
    TracerWireIntact: TDBComboBox;
    TracerWireIntactLabel: TLabel;
    AreaMarkedInWhite: TDBComboBox;
    AreaMarkedInWhiteLabel: TLabel;
    OffsetVisible: TDBComboBox;
    OffsetVisibleLabel: TLabel;
    DamageInvoice: TDamageInvoiceFrame;
    EditLitigationPanel: TPanel;
    CaseCaptionLabel: TLabel;
    vsLabel: TLabel;
    CaseNumLabel: TLabel;
    VenueLabel: TLabel;
    Label5: TLabel;
    UQDamageID: TDBText;
    SummonsDateLabel: TLabel;
    SummonsReceivedDate: TDBText;
    ResponseDateLabel: TLabel;
    ResponseDate: TDBText;
    VenueStateLabel: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DedictibleLabel: TLabel;
    Deductible: TDBText;
    DemandLabel: TLabel;
    Label9: TLabel;
    SaveLitigationButton: TButton;
    CancelLitigationButton: TButton;
    Plaintiff: TDBEdit;
    Defendant: TDBEdit;
    FileNumber: TDBEdit;
    Venue: TDBEdit;
    LitigationVenueState: TDBComboBox;
    LitigationAttorney: TDBComboBox;
    Demand: TDBEdit;
    CarrierOnNotice: TDBCheckBox;
    LitigationStatus: TDBComboBox;
    ThirdPartyInvoiceTab: TTabSheet;
    ThirdPartyInvoiceFrame: TDamageInvoiceFrame;
    ClaimCoordinatorLabel: TLabel;
    ClaimCoordinatorSource: TDataSource;
    ClaimCoordinatorQuery: TDBISAMQuery;
    PhotoAttachmentsTab: TTabSheet;
    PhotoAttachmentFrame: TPhotoAttachmentFrame;
    LitigationInvoiceFrame: TDamageInvoiceFrame;
    AddLitigationButton: TButton;
    ThirdPartyNotesTab: TTabSheet;
    ThirdPartyHistory: TTabSheet;
    LitigationNotesTab: TTabSheet;
    ThirdPartyNotesFrame: TNotesFrame;
    LitigationNotesFrame: TNotesFrame;
    DamageNotesTab: TTabSheet;
    DamageNotesFrame: TNotesFrame;
    ThirdPartyHistoryGrid: TDBGrid;
    LitigationHistoryGrid: TDBGrid;
    InvestigatorNameLabel: TLabel;
    InvestigatorComboBox: TComboBox;
    ChooseInvButton: TButton;
    LocatorLabel: TLabel;
    LocatorNameLabel: TLabel;
    LocatorCombo: TComboBox;
    LookupLocatorButton: TButton;
    ClearLocatorButton: TButton;
    AccrualHistory: TDBISAMQuery;
    AccrualHistorySource: TDataSource;
    RequiredDocumentsTab: TTabSheet;
    AttachedDocsQuery: TDBISAMQuery;
    InvestigatorNarrativeDataSource: TDataSource;
    InvestigatorNarrativeTable: TDBISAMTable;
    RequiredDocsTable: TDBISAMTable;
    ApprovalDetailsTab: TTabSheet;
    ApprovedDateLabel: TLabel;
    ApprovedDate: TDBText;
    RespCodeExcLabel: TLabel;
    ReasonForChangeLabel: TLabel;
    ExcOtherDesc: TDBEdit;
    adExcavatorRespGroup: TGroupBox;
    ExcavatorRespCodeLabel: TLabel;
    adExcRespCodeLabel: TDBText;
    ExcavatorOtherDescLabel: TLabel;
    adExcRespDescLabel: TDBText;
    ExcavatorSourceLabel: TLabel;
    ExcavatorResponseLabel: TLabel;
    adExcRespSourceMemo: TDBMemo;
    adExcRespResponseMemo: TDBMemo;
    adUQRespGroup: TGroupBox;
    UQSTSRespCodeLabel: TLabel;
    adUQRespCodeLabel: TDBText;
    UQSTSOtherDescLabel: TLabel;
    adUQRespDescLabel: TDBText;
    UQSTSEssStepLabel: TLabel;
    adUQEssentialStepMemo: TDBText;
    UQSTSDetailsLabel: TLabel;
    adUQRespDetailsMemo: TDBMemo;
    adUtilityRespGroup: TGroupBox;
    UtilRespCodeLabel: TLabel;
    adSpecialRespCodeLabel: TDBText;
    UtilDetailsLabel: TLabel;
    adSpecialRespDetailsMemo: TDBMemo;
    ApprovalGroup: TGroupBox;
    DefaultEstLabel: TLabel;
    adEstimateLabel: TLabel;
    EstimateAgreeCheckBox: TDBCheckBox;
    adCertifyStatementMemo: TMemo;
    ApproveCheckbox: TCheckBox;
    ApprovalDateTimeLabel: TLabel;
    adApproveDateTimeDBLabel: TDBText;
    ApprovedByLabel: TLabel;
    ApprovedByDBText: TDBText;
    PrintApprovalDetailsButton: TButton;
    GroupBox1: TGroupBox;
    PrintApprovalDetailsAction: TAction;
    ArriveButtonPanel: TPanel;
    ArriveButton: TButton;
    ArriveOnSiteAction: TAction;
    ArrivePopupMenu: TPopupMenu;
    OnsiteMenuItem: TMenuItem;
    OffsiteMenuItem: TMenuItem;
    ArriveOffSiteAction: TAction;
    HistoryPanel: TPanel;
    HistoryPageControl: TPageControl;
    HistoryResponsibilityTab: TTabSheet;
    HistoryArrivalsTab: TTabSheet;
    JobsiteArrivalSource: TDataSource;
    EmployeeLookup: TDBISAMTable;
    HistoryAccrualTab: TTabSheet;
    AccrualHistoryGrid: TDBGrid;
    ResponsibilityHistoryGrid: TcxGrid;
    ResponsibilityHistoryGridView: TcxGridDBBandedTableView;
    ResponsibilityHistoryGridLevel: TcxGridLevel;
    ColModifiedDate: TcxGridDBBandedColumn;
    ColModifiedBy: TcxGridDBBandedColumn;
    ColUQRespCode: TcxGridDBBandedColumn;
    ColUqRespType: TcxGridDBBandedColumn;
    ColUQRespOtherDesc: TcxGridDBBandedColumn;
    ColUqRespEssStep: TcxGridDBBandedColumn;
    ColExcRespCode: TcxGridDBBandedColumn;
    ColExcRespType: TcxGridDBBandedColumn;
    ColExcRespOtherDesc: TcxGridDBBandedColumn;
    ColSpcRespCode: TcxGridDBBandedColumn;
    ColSpcRespType: TcxGridDBBandedColumn;
    ColProfitCenter: TcxGridDBBandedColumn;
    ColReasonChangedDesc: TcxGridDBBandedColumn;
    ColUQRespDetails: TcxGridDBBandedColumn;
    ColExcRespDetails: TcxGridDBBandedColumn;
    ColExcRespResponse: TcxGridDBBandedColumn;
    ColSPCRespDetails: TcxGridDBBandedColumn;
    ColSPCRespOtherDesc: TcxGridDBBandedColumn;
    ColModifiedByShortName: TcxGridDBBandedColumn;
    ReasonChanged: TComboBox;
    Document: TDBISAMTable;
    DocumentGrid: TcxGrid;
    DocumentDBTableView: TcxGridDBTableView;
    DocumentLevel: TcxGridLevel;
    DocumentQuery: TDBISAMQuery;
    DocumentSource: TDataSource;
    ColDocRequiredDocID: TcxGridDBColumn;
    ColDocRequiredDocName: TcxGridDBColumn;
    ColDocForeignID: TcxGridDBColumn;
    ColDocForeignType: TcxGridDBColumn;
    ColDocDocID: TcxGridDBColumn;
    ColDocComment: TcxGridDBColumn;
    ColDocAddedByID: TcxGridDBColumn;
    ColDocAddedDate: TcxGridDBColumn;
    ColDocAttachedCnt: TcxGridDBColumn;
    ColDocRequired: TcxGridDBColumn;
    ApprovalMissingDocGrid: TcxGrid;
    ApprovalMissingDocDBTableView: TcxGridDBTableView;
    ColApprovalMissingDocName: TcxGridDBColumn;
    ColApprovalMissingDocComment: TcxGridDBColumn;
    ColApprovalMissingDocRequired: TcxGridDBColumn;
    ColApprovalMissingDocAttachedCnt: TcxGridDBColumn;
    ApprovalMissingDocLevel: TcxGridLevel;
    RequiredDocsTopPanel: TPanel;
    ShowOnlyMissingDocsCheckBox: TCheckBox;
    MarksWithinTolerance: TDBComboBox;
    MarksWithinToleranceLabel: TLabel;
    DamageDate: TcxDBDateEdit;
    BillingPeriodDate: TcxDBDateEdit;
    DateOfService: TcxDBDateEdit;
    TicketNumber: TcxDBButtonEdit;
    DamageBillable: TcxDBCheckBox;
    ViewRepo: TcxGridViewRepository;
    ClientLookupView: TcxGridDBBandedTableView;
    ColClientLookupClientName: TcxGridDBBandedColumn;
    CarrierLookupView: TcxGridDBBandedTableView;
    CoordinatorLookupView: TcxGridDBBandedTableView;
    ColCoordinatorLookupShortName: TcxGridDBBandedColumn;
    ColCarrierLookupCarrierID: TcxGridDBBandedColumn;
    ColCarrierLookupName: TcxGridDBBandedColumn;
    ColCarrierLookupDeductible: TcxGridDBBandedColumn;
    ColCoordinatorLookupEmpID: TcxGridDBBandedColumn;
    ClaimCoordinator: TcxDBExtLookupComboBox;
    ThirdPartyInsurance: TcxDBExtLookupComboBox;
    LitigationCarrier: TcxDBExtLookupComboBox;
    ArrivalGrid: TcxGrid;
    ArrivalGridLevel: TcxGridLevel;
    ArrivalGridView: TcxGridDBTableView;
    ColArrivalsArrivalID: TcxGridDBColumn;
    ColArrivalsArrivalDate: TcxGridDBColumn;
    ColArrivalsEmpID: TcxGridDBColumn;
    ColArrivalsShortName: TcxGridDBColumn;
    ColArrivalsEntryMethod: TcxGridDBColumn;
    ColArrivalsAddedByName: TcxGridDBColumn;
    ColArrivalsActive: TcxGridDBColumn;
    ColArrivalsDeletedByName: TcxGridDBColumn;
    ColArrivalsDeletedDate: TcxGridDBColumn;
    ColArrivalsLocationStatus: TcxGridDBColumn;
    RestrictionPanel: TPanel;
    Label24: TLabel;
    LitSubTypeCombo: TComboBox;
    Panel1: TPanel;
    DamageRestrictionLabel: TLabel;
    DamageSubTypeCombo: TComboBox;
    TPRestrictionPanel: TPanel;
    TPRestrictionLabel: TLabel;
    TPSubTypeCombo: TComboBox;
    IntelexID: TDBEdit;
    CallCenters: TComboBox;
    ClientID: TcxDBExtLookupComboBox;
    CallCenterLabel: TLabel;
    ClientLabel: TLabel;
    ClientCodeLabel: TLabel;
    cxStyleRepository1: TcxStyleRepository;
    cxGridViewRepository1: TcxGridViewRepository;
    cxInactive: TcxStyle;
    ClientFieldsView: TcxGridDBTableView;
    ClientFieldsViewClient_id: TcxGridDBColumn;
    ClientFieldsViewClient_Name: TcxGridDBColumn;
    ClientFieldsViewActive: TcxGridDBColumn;
    UtilityCoDamaged: TEdit;
    ClientCode: TEdit;
    FacilityTypeLabel: TLabel;
    FacilitySizeLabel: TLabel;
    FacilityMaterialLabel: TLabel;
    FacilityMaterial: TDBComboBox;
    FacilityMaterial2: TDBComboBox;
    FacilityMaterial3: TDBComboBox;
    FacilitySize3: TDBComboBox;
    FacilitySize2: TDBComboBox;
    FacilitySize: TDBComboBox;
    FacilityType: TDBComboBox;
    FacilityType2: TDBComboBox;
    FacilityType3: TDBComboBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    CustomQuestionsTab: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CustomFormUpdate(Action: TBasicAction; var Handled: Boolean);  //QM-593 Custom Form (At&T Damage)
    procedure FormShow(Sender: TObject);
    procedure ConfirmButtonClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DamageCancelActionExecute(Sender: TObject);
    procedure DamageSaveActionExecute(Sender: TObject);
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ToleranceMeasurementChange(Sender: TObject);
    procedure TicketNumberButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure TicketNumberKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CloseActionExecute(Sender: TObject);
    procedure DamagePageControlChange(Sender: TObject);
    procedure YNUBControlKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PrintActionExecute(Sender: TObject);
    procedure CallCentersChange(Sender: TObject);
    procedure FacilityTypeChange(Sender: TObject);
    procedure TicketNumberChange(Sender: TObject);
    procedure PageControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure DamageTabShow(Sender: TObject);
    procedure ThirdPartyTabShow(Sender: TObject);
    procedure LitigationTabShow(Sender: TObject);
    procedure LitigationSaveActionExecute(Sender: TObject);
    procedure LitigationCancelActionExecute(Sender: TObject);
    procedure LitigationAddActionExecute(Sender: TObject);
    procedure LitigationCalcFields(DataSet: TDataSet);
    procedure LitigationAfterScroll(DataSet: TDataSet);
    procedure ThirdPartyCancelActionExecute(Sender: TObject);
    procedure ThirdPartySaveActionExecute(Sender: TObject);
    procedure ThirdPartyAddActionExecute(Sender: TObject);
    procedure ThirdPartyAfterOpen(DataSet: TDataSet);
    procedure ThirdPartyAfterScroll(DataSet: TDataSet);
    procedure LitigationAfterOpen(DataSet: TDataSet);
    procedure ThirdPartyPageControlChange(Sender: TObject);
    procedure LitigationPageControlChange(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BillingGroupEnter(Sender: TObject);
    procedure DamageBillBeforeEdit(DataSet: TDataSet);
    procedure ChooseInvButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LookupLocatorButtonClick(Sender: TObject);
    procedure ClearLocatorButtonClick(Sender: TObject);
    procedure DamageSourceStateChange(Sender: TObject);
    procedure DamageSourceUpdateData(Sender: TObject);
    procedure ReasonChangedSelectionChange(Sender: TObject);
    procedure LocationExit(Sender: TObject);
    procedure PrintApprovalDetailsActionExecute(Sender: TObject);
    procedure ProfitCenterChange(Sender: TObject);
    procedure ArriveOnSiteActionExecute(Sender: TObject);
    procedure ArriveButtonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ArriveOffSiteActionExecute(Sender: TObject);
    procedure ResponsibilityCodeChange(Sender: TObject);
    procedure DocumentQueryAfterEdit(DataSet: TDataSet);
    procedure DocumentQueryCalcFields(DataSet: TDataSet);
    procedure DocumentQueryFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure ColDocCommentGetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure DocumentDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ColDocCommentGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: String);
    procedure ShowOnlyMissingDocsCheckBoxClick(Sender: TObject);
    procedure LitSubTypeComboChange(Sender: TObject);
    procedure DamageSubTypeComboChange(Sender: TObject);
    procedure TPSubTypeComboChange(Sender: TObject);
    procedure ClientFieldsViewStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure ClientIDPropertiesCloseUp(Sender: TObject);
  protected

    FInsertMode: Boolean;
    FDamageID: Integer;
    FUQDamageID: Integer;
    FLitigationID: Integer;
    FThirdPartyID: Integer;
    OriginalInvoiceCode: string;
    FOnThirdParty: TItemSelectedEvent;
    FOnDamagePrint: TItemSelectedEvent;
    FOnLitigation: TItemSelectedEvent;
    FOnFinished: TNotifyEvent;
    FOnDamage: TItemSelectedEvent;
    FThirdPartyReadOnly: Boolean;
    FThirdPartyInsertMode: Boolean;
    FLitigationReadOnly: Boolean;
    FLitigationInsertMode: Boolean;
    FAddinManager: TAttachmentsAddinManager;
    FRequiredDocList: TStringList;
    FDamageNarrativeText: string;
    FGeneratedProfitCenter: string;
    FState: string;
    FCounty: string;
    FCity: string;
    FChangingTabsForError: Boolean;
    FResponsibilityChanged: Boolean;
    FErrorsDuringSave: Boolean;
    FCallCenterList: TCodeLookupList;
    FDamageNoteSubTypeList, FLitNoteSubTypeList, FTPNoteSubTypeList:  TCodeLookupList;  //Associate object with the combobox
  private
    fNewDamage: Boolean;
    fCustDamageRec: TCustDamageRec;  //QM-593 EB Custom Damage (AT&T)
    fCustGroupContainer: TGroupContainer; //QM-593 Custom Damage (AT&T)
    procedure SetInsertMode(const Value: Boolean); virtual;
    procedure SetDamageTypeCaption(const Value: string);
    procedure Done;
    procedure SetupDataSets;
    procedure OpenDamageDataSets;
    function CheckIfDamageCanBeClosed(var Error: string): Boolean;
    function CheckIfDamageCanBeCompleted(var Error: string): Boolean;
    procedure CheckForAndLoadReferenceCodes;
    function Damage: TDataSet;
    procedure InitializeTabs;
    procedure UpdateDependentDropdowns(UserChanged: Boolean;FacilityRow:integer);  //QMANTWO-615
    function CloseButtonCaption: string;
    function CloseButtonEnabled: Boolean;
    procedure ValidateDamageFields(const DamageType: string);
    procedure SelectCallCenterForClient;
    procedure ShowTicketDetails;
    procedure AfterGoToDamage;
    procedure UpdateDisplay;
    procedure UMUpdateDisplay(var Msg: TMessage); message UM_UPDATEDISPLAY;
    procedure UMUpdateCombos(var Msg: TMessage); message UM_UPDATECOMBOS;  //QMANTWO-615
    procedure ValidateResponsibilityAndInvoiceCodes(const DamageType: string);
    procedure SaveDamageBill;
    function DamageInvoiceCount: Integer;
    procedure FinishedInvoiceEdit(Sender: TObject);
    procedure HideDamageTabs;
    procedure ShowDamageTabs;
    procedure SetLitigationReadOnly(const Value: Boolean);
    procedure SetLitigationInsertMode(const Value: Boolean);
    procedure SetThirdPartyReadOnly(const Value: Boolean);
    procedure SetThirdPartyInsertMode(const Value: Boolean);
    procedure SetupLitigationEditing(Edit: Boolean);
    procedure SetupThirdPartyEditing(Edit: Boolean);
//    procedure PopulateNoteSubType(NoteType: Integer);
    property InsertMode: Boolean read FInsertMode  write SetInsertMode;
    property ThirdPartyInsertMode: Boolean read FThirdPartyInsertMode write SetThirdPartyInsertMode;
    property LitigationInsertMode: Boolean read FLitigationInsertMode write SetLitigationInsertMode;
    property DamageTypeCaption: string write SetDamageTypeCaption;
    property ThirdPartyReadOnly: Boolean read FThirdPartyReadOnly write SetThirdPartyReadOnly;
    property LitigationReadOnly: Boolean read FLitigationReadOnly write SetLitigationReadOnly;
    procedure SetTableFilters(const DamageID: Integer);
    procedure ClearTableFilters;
    procedure DbComboRefresh(DS: TDataSet);
    procedure LitigationDone;
    procedure ThirdPartyDone;
    function GetCurrentLitigationID: Integer;
    function GetCurrentThirdPartyID: Integer;
    procedure FinishedLitigationInvoiceEdit(Sender: TObject);
    procedure FinishedThirdPartyInvoiceEdit(Sender: TObject);
    //procedure PopulateInvestigatorList;
    procedure UpdateInvestigator;
    procedure UpdateLocator;
    procedure VerifyDamageDates;
    procedure VerifyLitigationDates;
    procedure VerifyDamageBillDates;
    procedure ValidateLitigationFields;
    procedure ManageAttachmentsButtonClick(Sender: TObject);
    procedure PrepareAttachmentsAddin;
    function HaveDamage: Boolean;
    procedure AfterAddDamageEstimate(Sender: TObject);
    procedure AfterAddLitigationEstimate(Sender: TObject);
    procedure AfterAddThirdPartyEstimate(Sender: TObject);
    procedure DamageAttachmentChange(Sender: TObject);
    procedure DamageNoteChange(Sender: TObject);
    procedure LitigationAttachmentChange(Sender: TObject);
    procedure ThirdPartyAttachmentChange(Sender: TObject);
    procedure RefreshRequiredDamageDocs;
    procedure LoadDamageDocuments;
    function ValidateRequiredDocuments(var Error: string): Boolean;
    procedure SaveDamageDocuments;
    function ValidateApprovalFields: Boolean;
    function ApprovingDamage: Boolean;
    function DamageTypeIsPendingApproval: Boolean;
    procedure AutoGenerateProfitCenter;
    function DamageHasLinkedTicket: Boolean;
    function DamageEditAllowed(const Arrived: Boolean): Boolean;
    procedure SetEnabledOnDamageControls(Enabled: Boolean);
    procedure SetArriveButtonEnabled(const Enabled: Boolean);
    procedure ArriveOnDamage(const LocationStatus: string);
    procedure RaiseErrorOnTab(const ErrorMessage: string; const TabSheet: TTabSheet);
    procedure ChangeTabForError(PageControl: TPageControl; TabSheet: TTabSheet);
    function ResponsibilityCodeChangedFromSavedValue: Boolean;
    function ResponsibilityPreviouslySet: Boolean;
    procedure SaveDamage;
    function CurrentDocumentRowAttachCnt(ARecord: TcxCustomGridRecord): Integer;
    function GetAttachedDocCount(const RequiredDocID: Integer): Integer;
    procedure SetupDocumentGrid(DocGrid: TcxGrid; HideAttached: Boolean=False);
    procedure ShowMessageForTabControl(DisplayMsg: String; PageControl: TPageControl; TabSheet: TTabSheet; WinControl: TWinControl);
    procedure ConfirmInvoiceCodeChange;
    procedure PopulateNoteSubTypes;

    {Custom Form}
    procedure ClearCustDamageRec; //QM-593 EB Damage Custom Form
    function GetDamageCustomForm: integer; //QM-593 EB Damage Custom Form
  public
    procedure AddNewDamage;
    procedure GotoDamageId(ID: Integer);
    procedure AddNewDamageLitigation(const DamageID: Integer);
    procedure GotoLitigationId(const ID: Integer);
    procedure AddNewDamageThirdParty(const DamageID: Integer);
    procedure GotoThirdPartyId(const ID: Integer);
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure ClearNotesSubType;
//    procedure UMNewDamage(var Msg: TMessage); message UM_NEW_DAMAGE;  //QMANTWO-627
    function InvestigationIsOpen: Boolean;
    property NewDamage: Boolean read fNewDamage write fNewDamage;
    property OnFinished: TNotifyEvent read FOnFinished write FOnFinished;
    property OnDamagePrint: TItemSelectedEvent read FOnDamagePrint write FOnDamagePrint;
    property OnDamage: TItemSelectedEvent read FOnDamage write FOnDamage;
    property OnLitigation: TItemSelectedEvent read FOnLitigation write FOnLitigation;
    property OnThirdParty: TItemSelectedEvent read FOnThirdParty write FOnThirdParty;
  end;

implementation

uses OdHourGlass, OdVclUtils, OdMiscUtils, OdDbUtils, OdExceptions,
  BaseSearchForm, TicketSearchHeader, 
  DamageDetailsReport, QMConst, EmployeeSearch, ODReportBase,
  DamageProfitCenterRules, OdCxUtils, 
  LocalPermissionsDMu, LocalEmployeeDMu;

{$R *.dfm}

{ TDamageDetailsForm }

procedure TDamageDetailsForm.AddNewDamage;
begin
  OpenDamageDataSets;
  Damage.Insert;
  InsertMode := True;
  ArriveButtonPanel.Visible := False;
  //PopulateInvestigatorList;
  AfterGoToDamage;
end;

procedure TDamageDetailsForm.GotoDamageId(ID: Integer);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  FDamageID := ID;
  CheckForAndLoadReferenceCodes;
  OpenDamageDataSets;
  if not Damage.Locate('damage_id', ID, []) then
    raise Exception.Create('Unable to find damage id: ' + IntToStr(ID));
  if not Damage.FieldByName('ticket_id').IsNull then begin
    DM.UpdateTicketInCache(Damage.FieldByName('ticket_id').AsInteger);
    Damage.Refresh;
  end;
  FUQDamageID := Damage.FieldByName('uq_damage_id').AsInteger;
  FState := Damage.FieldByName('state').AsString;
  FCounty := Damage.FieldByName('county').AsString;
  FCity := Damage.FieldByName('city').AsString;

  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeViewDamage, Damage.FieldByName('uq_damage_id').AsString);

  Invoices.Close;
  Invoices.ParamByName('damage_id').AsInteger := ID;
  Invoices.Open;
  DamageInvoice.GoToDamageInvoice(Invoices, ID);
  //Initialize needs GoToDamageInvoice called first so the Invoice dataset will be ready.
  DamageInvoice.Initialize(DamageSource, qminvDamage, ID, 0);

  History.Close;
  History.ParamByName('DamageID').AsInteger := ID;
  History.Open;

  TDamageDetailsReportForm.SetLastDamageParams(FUQDamageID, DamageReportOnDamage);
  InsertMode := False;
  SelectCallCenterForClient;
  if Damage.FieldByName('ticket_id').IsNull then
    DamageAttachmentsFrame.Initialize(qmftDamage, ID, True)
  else
    DamageAttachmentsFrame.Initialize(qmftDamage, ID, True, qmftTicket, Damage.FieldByName('ticket_id').AsInteger);

  DamageEstimateFrame.Initialize(qmestDamage, ID, 0);

  DamageBill.Close;
  DamageBill.ParamByName('damage_id').AsInteger := ID;
  DamageBill.Open;

  AccrualHistory.Close;
  AccrualHistory.ParamByName('DamageID').AsInteger := ID;
  AccrualHistory.Open;

  AfterGoToDamage;
  //UpdateInvestigator;
  if Damage.FieldByName('damage_id').AsInteger <> ID then
    raise Exception.CreateFmt('Expected to be showing damage ID %d, but %d is showing.', [ID, Damage.FieldByName('damage_id').AsInteger]);
  PageControl.ActivePage := DamageTab;
  DamagePageControl.ActivePage := NewDamageTab;

  ApprovalDetailsTab.TabVisible := ApprovingDamage or (Damage.FieldByName('damage_type').AsString = DamageTypeApproved);
  ApproveCheckbox.Checked := (Damage.FieldByName('damage_type').AsString = DamageTypeApproved);

  SetEnabledOnDamageControls(DamageEditAllowed(False));
  SetArriveButtonEnabled(True);

  CustomQuestionsTab.TabVisible := False;

  //Do not set ReasonChanged combo- allow reason_changed to be empty whenever a
  //damage is opened; the user must give a new reason when changing a responsibility code.
end;

procedure TDamageDetailsForm.SetInsertMode(const Value: Boolean);
begin
  if Value then
    HideDamageTabs
  else
    ShowDamageTabs;

  FInsertMode := Value;

  if InsertMode then
    ShowPageControlAsSingleTab(NewDamageTab)
  else begin
    ShowPageControlAsMultiTab(DamagePageControl);
  end;

  // Force the UtilityCoDamaged.Text property to update when the style is changed
  DbComboRefresh(Damage);

  // photo attachment is currently always disabled
  PhotoAttachmentsTab.TabVisible := False;
end;

procedure TDamageDetailsForm.FormCreate(Sender: TObject);
begin
  FRequiredDocList := TStringList.Create;
  DamageSource.DataSet := LDamageDM.Damage;
  DamageEstimateFrame.EstimateSource.DataSet := LDamageDM.DamageEstimate;
  DamageEstimateFrame.AfterAddEstimate := AfterAddDamageEstimate;
  LitigationEstimateFrame.EstimateSource.DataSet := LDamageDM.DamageEstimate;
  LitigationEstimateFrame.AfterAddEstimate := AfterAddLitigationEstimate;
  ThirdPartyEstimateFrame.EstimateSource.DataSet := LDamageDM.DamageEstimate;
  ThirdPartyEstimateFrame.AfterAddEstimate := AfterAddThirdPartyEstimate;

  DamageInvoice.InvoiceSource.DataSet := Invoices;
  DamageInvoice.InvoiceView.DataController.DataSource := InvoiceSource;
  DamageInvoice.OnFinished := FinishedInvoiceEdit;
  ThirdPartyInvoiceFrame.InvoiceSource.DataSet := Invoices;
  ThirdPartyInvoiceFrame.InvoiceView.DataController.DataSource := InvoiceSource;
  ThirdPartyInvoiceFrame.OnFinished := FinishedThirdPartyInvoiceEdit;
  LitigationInvoiceFrame.InvoiceSource.DataSet := Invoices;
  LitigationInvoiceFrame.InvoiceView.DataController.DataSource := InvoiceSource;
  LitigationInvoiceFrame.OnFinished := FinishedLitigationInvoiceEdit;

  InitializeTabs;
  SetupDataSets;
  OpenDamageDataSets;
  FCallCenterList := TCodeLookupList.Create(CallCenters.Items);
  DM.CallCenterDescriptionList(FCallCenterList, True);
  CallCenters.ClearSelection;
  ClientID.ClearSelection;
  UtiliQuestGroup.Caption := DM.UQState.CompanyShortName + ' Responsibility';
  ColUQRespCode.Caption := DM.UQState.CompanyShortName + ' Code';
  ColUqRespType.Caption := DM.UQState.CompanyShortName + ' Type';
  DamageInvoice.EmbeddedInDamage := True;
  ThirdPartyInvoiceFrame.EmbeddedInDamage := True;
  LitigationInvoiceFrame.EmbeddedInDamage := True;
  if PageControl.PageCount > 0 then
    PageControl.ActivePageIndex := 0;
  PhotoAttachmentsTab.TabVisible := False;
  DamageAttachmentsFrame.AfterAttachmentChange := DamageAttachmentChange;
  DamageNotesFrame.AfterNoteChange := DamageNoteChange;
  LitigationAttachmentsFrame.AfterAttachmentChange := LitigationAttachmentChange;
  ThirdPartyAttachmentsFrame.AfterAttachmentChange := ThirdPartyAttachmentChange;
  fDamageNoteSubTypeList := TCodeLookupList.Create(DamageSubTypeCombo.Items);
  fLitNoteSubTypeList := TCodeLookupList.Create(LitSubTypeCombo.Items);
  fTPNoteSubTypeList := TCodeLookupList.Create(TPSubTypeCombo.Items);
  PopulateNoteSubTypes;

end;

procedure TDamageDetailsForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  SaveButton.Enabled := Damage.State in dsEditModes;
end;

procedure TDamageDetailsForm.FormShow(Sender: TObject);
begin
  Assert(Assigned(DamageSource.DataSet));
  DamageDetailsPanel.Visible := True;
  CheckForAndLoadReferenceCodes;
  // Workaround to force combos to refresh
  DbComboRefresh(ThirdParty);
  DbComboRefresh(Litigation);
  DbComboRefresh(Client);  //QMANTWO-627
  SetEnabledOnDamageControls(DamageEditAllowed(False));
  FGeneratedProfitCenter := '';
  FResponsibilityChanged := False;
end;

procedure TDamageDetailsForm.Done;
begin
  if Assigned(OnFinished) then
    OnFinished(Self);
  DamagePageControl.Visible := True;
end;

procedure TDamageDetailsForm.ThirdPartyDone;
begin
  ThirdPartyPageControl.Visible := True;
  ThirdPartyTabShow(Self);
end;

procedure TDamageDetailsForm.LitigationDone;
begin
  LitigationPageControl.Visible := True;
  LitigationTabShow(Self);
end;

procedure TDamageDetailsForm.ConfirmButtonClick(Sender: TObject);
begin
  Done;
end;

procedure TDamageDetailsForm.FormResize(Sender: TObject);
begin
  CenterControlOnParent(ConfirmationPanel);
end;

procedure TDamageDetailsForm.DamageCancelActionExecute(Sender: TObject);
begin
  Damage.Cancel;
  DamageInvoice.Cancel;
  DamageBill.Cancel;
  CancelDataSet(ThirdParty);
  ThirdPartyInvoiceFrame.Cancel;
  CancelDataSet(Litigation);
  LitigationInvoiceFrame.Cancel;
  Done;
end;

procedure TDamageDetailsForm.DamageSaveActionExecute(Sender: TObject);
begin
  SaveDamage;
end;

procedure TDamageDetailsForm.SaveDamage;
var
  Cursor: IInterface;
  Time: TDateTime;
  EditedDamage: Boolean;
  EditedDamageBill: Boolean;
  EstimateCount: Integer;
begin
  Cursor := ShowHourGlass;
  Time := Now;
  if ClientID.ItemIndex < 0 then
  begin MessageDlg('Client is a required field.  '+#13+#10+
    'Selecting Client will also set the Client Code and Damage Company.', mtError, [mbOK], 0);
    ClientID.SetFocus;
    exit;
  end;

  
  EditedDamage := False;
  if SaveButton.CanFocus then
    SaveButton.SetFocus;
  if EditingDataSet(Damage) then begin
    Damage.UpdateRecord;
    ConfirmInvoiceCodeChange;
    ValidateDamageFields(Damage.FieldByName('damage_type').AsString);
    EditDataSet(Damage);
    Damage.FieldByName('LocalKey').AsDateTime := Time;
    Damage.FieldByName('modified_by').AsInteger := DM.UQState.EmpID;
  //  Damage.FieldByName('DeltaStatus').AsString := 'U';
    //Set reason changed to Null if all of the responsibility codes are the same
    // as when the damage was loaded.
    if not ResponsibilityCodeChangedFromSavedValue then begin
      Damage.FieldByName('reason_changed').Clear;
      ReasonChanged.ItemIndex := -1;
    end;
    // TODO: BeforePost isn't running automatically since we also use
    // the global BeforePost event
    LDamageDM.DamageBeforePost(Damage);
    Damage.Post;
    OriginalInvoiceCode := Damage.FieldByName('invoice_code').AsString;
    EditedDamage := True;
    FResponsibilityChanged := False; //reinitialize
  end;
  DamageInvoice.Save;
  FState := Damage.FieldByName('state').AsString;
  FCounty := Damage.FieldByName('county').AsString;
  FCity := Damage.FieldByName('city').AsString;
  EditedDamageBill := EditingDataSet(DamageBill);
  if EditedDamageBill then
    SaveDamageBill;

  SaveDamageDocuments;
  try
    FErrorsDuringSave := False;
    DM.SendChangesToServer;
  except
    on E: EOdSyncError do begin
      FErrorsDuringSave := True;
      ErrorDialog(E.Message + CRLF +
        ' Your damage updates may have been lost. Confirm your changes and Save again if needed.');
      ReasonChanged.ItemIndex := -1;
    end;
  end;

  if EditedDamage and InsertMode then begin
    if not Damage.Locate('LocalKey', Time, []) then
      raise Exception.Create('Unable to locate the newly inserted record');
    LDamageDM.UpdateDamageInCache(Damage.FieldByName('damage_id').Value);
    Damage.Refresh;
    InvestigationReqEdit.Text := Damage.FieldByName('uq_damage_id').AsString;
    DamageDetailsPanel.Visible := False;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAdd, '');
  end;
  if EditedDamage and not InsertMode then begin
    EstimateCount := LDamageDM.DamageEstimate.RecordCount;
    LDamageDM.UpdateDamageInCache(Damage.FieldByName('damage_id').Value);
    Damage.Refresh;
    LDamageDM.DamageEstimate.Refresh;
    if EstimateCount < LDamageDM.DamageEstimate.RecordCount then
      MessageDlg('A new estimate was added based on your changes.', mtInformation, [mbOk], 0);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageSaveChanges, '');
  end;

  if EditedDamage then begin
    DamageEstimateFrame.RefreshNow;
    ThirdParty.Refresh;
    Litigation.Refresh;
    RefreshDataset(History);
  end;

  if EditedDamageBill then begin
    DamageBill.Close;
    DamageBill.Open;
  end;

  if fCustDamageRec.HasCustomQuestions then begin  //QM-593 Custom Form (AT&T)
    fCustGroupContainer.Save;
  end;
  RefreshRequiredDamageDocs;
  LoadDamageDocuments;
end;

procedure TDamageDetailsForm.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  if ThirdParty.IsEmpty then
    ThirdPartyReadOnly := True
  else
    ThirdPartyReadOnly := not PermissionsDM.CanI(RightLitigationEdit);
  if Litigation.IsEmpty then
    LitigationReadOnly := True
  else
    LitigationReadOnly := not PermissionsDM.CanI(RightLitigationEdit);
  DamageSaveAction.Enabled := (EditingDataSet(Damage) or EditingDataSet(Invoices));
  CloseAction.Enabled := CloseButtonEnabled;
  CloseAction.Caption := CloseButtonCaption;
  CloseAction.Visible := not InsertMode;
  DamageTypeCaption := Damage.FieldByName('damage_type').AsString;

  LitigationSaveAction.Enabled := EditingDataSet(Litigation);
  LitigationCancelAction.Enabled := EditingDataSet(Litigation);
  LitigationAddAction.Enabled := PermissionsDM.CanI(RightLitigationEdit) and (not EditingDataSet(Litigation));

  ThirdPartySaveAction.Enabled := EditingDataSet(ThirdParty);
  ThirdPartyCancelAction.Enabled := EditingDataSet(ThirdParty);
  ThirdPartyAddAction.Enabled := PermissionsDM.CanI(RightLitigationEdit) and (not EditingDataSet(ThirdParty));
  ReasonChanged.Enabled := FResponsibilityChanged;
end;

procedure TDamageDetailsForm.ToleranceMeasurementChange(Sender: TObject);
var
  Total, Inches, Feet: Cardinal;
begin
  Feet := 0;
  Total := Abs(StrToIntDef(ToleranceMeasurement.Text, 0));
  while Total >= 12 do begin
    Dec(Total, 12);
    Inc(Feet);
  end;
  Inches := Total;
  ToleranceFeetInches.Caption := Format('(%d'' %d")', [Feet, Inches]);
end;

procedure TDamageDetailsForm.ActivatingNow;
var
  refid : integer;
  description, oc_code:string;
begin
  inherited;
  ClearCustDamageRec;  //QM-593 EB Damage Custom Form EB

  PrintAction.Enabled := PermissionsDM.CanI(RightReportScreen) or PermissionsDM.CanI('DamageDetails');
  LookupLocatorButton.Visible := PermissionsDM.CanI(RightDamagesAssignLocator);
  ClearLocatorButton.Visible := LookupLocatorButton.Visible;

  ColDocRequiredDocID.Visible := DM.UQState.DeveloperMode;
  ColDocDocID.Visible := DM.UQState.DeveloperMode;
  oc_code:='';
  refid:= -1;    //QMANTWO-627
  try
    if not(Damage.FieldByName('utility_co_damaged').IsNull) then
       UtilityCoDamaged.Text := Damage.FieldByName('utility_co_damaged').AsString;

    if not(LDamageDM.Damage.FieldByName('client_code').Isnull) then
        clientCode.text := LDamageDM.Damage.FieldByName('client_code').AsString;

    if not(LDamageDM.Damage.FieldByName('client_id').IsNull) then
    begin
      dm.qryRefIDFromClient.ParamByName('ClientID').Value := LDamageDM.Damage.FieldByName('client_id').AsInteger;
      dm.qryRefIDFromClient.open;
      refid := dm.qryRefIDFromClient.fieldbyname('ref_id').AsInteger;
      oc_code := dm.qryRefIDFromClient.fieldbyname('oc_code').AsString;
      if LDamageDM.Damage.FieldByName('client_code').Isnull then
        clientCode.text := oc_code;

      LDamageDM.RefDescForDamageCos.ParamByName('refid').Value := refid;
      LDamageDM.RefDescForDamageCos.Open;

      description := LDamageDM.RefDescForDamageCos.FieldByName('description').AsString;

      if (refID > 0)  then begin
        fCustDamageRec.RefID := refID;  //QM-593 EB
        GetDamageCustomForm;  //QM-593 EB Damage Custom Form
      end
      else  //QM-693 EB Damage Custom Form (in case of re-activation, make sure tab is hidden)
        CustomQuestionsTab.TabVisible := False;

      if not(Damage.FieldByName('utility_co_damaged').IsNull) then begin
         UtilityCoDamaged.Text := Damage.FieldByName('utility_co_damaged').AsString;
         fCustDamageRec.UtilityCoDamaged := UtilityCoDamaged.Text;  //QM-593 EB
      end
      else
        UtilityCoDamaged.Text := description;
    end
    else
    begin
      ClientID.ItemIndex := -1;
      if LDamageDM.Damage.FieldByName('client_code').Isnull then
        clientCode.text := '';
      if Damage.FieldByName('utility_co_damaged').IsNull then begin
        UtilityCoDamaged.Text :='';
        CustomQuestionsTab.TabVisible:= False;
      end;

    end;
  finally
    dm.qryRefIDFromClient.close;
    LDamageDM.RefDescForDamageCos.close;
  end;
end;

procedure TDamageDetailsForm.LeavingNow;
var
  Answer: Word;
begin
  inherited;
  if (Damage.State in dsEditModes) then begin
      Answer := MessageDlg('Save current damage?', mtConfirmation, [mbYes,mbNo, mbCancel], 0);
      case Answer of
        mrYes: SaveDamage;
        mrNo: Damage.Cancel;
        mrCancel: begin
          Damage.Edit;
          Abort;
        end;
      end;
  end;
  ClearNotesSubType;
  CallCenters.ClearSelection;
  ClientID.ClearSelection;
  if Assigned(fCustGroupContainer) then   //QM-593 EB
    FreeAndNil(fCustGroupContainer);
  //FreeAndNil(InvestigatorComboBoxController);
end;

procedure TDamageDetailsForm.TicketNumberButtonClick(Sender: TObject; AbsoluteIndex: Integer);
var
  SearchForm: TSearchForm;
  TicketID: string;
begin
  SearchForm := TSearchForm.CreateWithCriteria(nil, TTicketSearchCriteria);
  try
    if Trim(TicketNumber.Text) <> '' then
      (SearchForm.Criteria as TTicketSearchCriteria).TicketNumber.Text := Trim(TicketNumber.Text);
    if SearchForm.ShowModal = mrOK then begin
      TicketID := SearchForm.GetSelectedFieldValue('ticket_id');
      if TicketID <> '' then begin
        Damage.Edit;
        Damage.FieldByName('ticket_id').AsInteger := StrToInt(TicketID);
        DM.UpdateTicketInCache(StrToInt(TicketID));
        LDamageDM.SetDamagePropertiesFromTicketIDField;
        UpdateLocator;
      end;
    end;
  finally
    FreeAndNil(SearchForm);
  end;
  ShowTicketDetails;
end;

procedure TDamageDetailsForm.SetupDataSets;
begin
  Assert(Assigned(InvestigatorSource.DataSet));
  Assert(Assigned(OfficeSource.DataSet));
  Assert(Assigned(ClientSource.DataSet));
  Assert(Assigned(HistorySource.DataSet));
  AddLookupField('investigator_name', Damage, 'investigator_id',
    InvestigatorSource.DataSet, 'emp_id', 'short_name', ShortNameLenth, 'InvNameField');
  AddLookupField('client_name', Damage, 'client_id',
    ClientSource.DataSet, 'client_id', 'client_name', 40, 'ClientNameField');
  AddLookupField('office_name', Damage, 'office_id',
    OfficeSource.DataSet, 'office_id', 'office_name', 50, 'OfficeNameField');
  AddLookupField('ticket_number', Damage, 'ticket_id',
    TicketSource.DataSet, 'ticket_id', 'ticket_number', 30, 'TicketNumberField');
  AddLookupField('added_by_name', Damage, 'added_by',
    InvestigatorSource.Dataset, 'emp_id', 'short_name', ShortNameLenth);
  AddLookupField('claim_coordinator_name', Damage, 'claim_coordinator_id',
    ClaimCoordinatorSource.DataSet, 'emp_id', 'short_name', ShortNameLenth, 'CoordNameField');
  AddLookupField('billed_by_name', DamageBill, 'modified_by', EmployeeDM.EmployeeLookup,
    'emp_id', 'short_name', ShortNameLenth, 'BilledByNameField');
  AddLookupField('uq_damage_id', Invoices, 'damage_id',
    LDamageDM.DamageLookup, 'damage_id', 'uq_damage_id');
  AddLookupField('insur_name', Litigation, 'carrier_id',
    CarrierLookup, 'carrier_id', 'name', 50, 'NameField');
  AddLookupField('deductible', Litigation, 'carrier_id',
    CarrierLookup, 'carrier_id', 'deductible', 30, 'Deductible');
  AddLookupField('uq_damage_id', Litigation, 'damage_id',
    LDamageDM.DamageLookup, 'damage_id', 'uq_damage_id');
  AddCalculatedField('respond_by_date', Litigation, TDateTimeField, 0);
  AddLookupField('insur_name', ThirdParty, 'carrier_id',
    CarrierLookup, 'carrier_id', 'name', 50, 'NameField');
  AddLookupField('deductible', ThirdParty, 'carrier_id',
    CarrierLookup, 'carrier_id', 'deductible', 30, 'Deductible');
  AddLookupField('uq_damage_id', ThirdParty, 'damage_id',
    LDamageDM.DamageLookup, 'damage_id', 'uq_damage_id');
  AddLookupField('invoice_type_desc', Invoices, 'invoice_type',
    DM.EstimateTypeLookup, 'code', 'description', 25, 'InvoiceTypeDescField');
  AddLookupField('approved_by_name', Damage, 'approved_by_id',
    InvestigatorSource.Dataset, 'emp_id', 'short_name', ShortNameLenth);
  AddLookupField('short_name', DM.JobsiteArrivalData, 'emp_id',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
  AddLookupField('added_by_name', DM.JobsiteArrivalData, 'added_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'AddedByNameField');
  AddLookupField('ColReasonChangedDesc', History, 'reason_changed',
    LDamageDM.DamageChangeReasonLookup, 'ref_id', 'description', 100, 'ReasonChangedDescField');
  AddLookupField('ColModifiedByShortName', History, 'modified_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'HistoryNameField');

  DM.Engine.LinkEventsAutoInc(DamageBill);
  DM.Engine.LinkEventsAutoInc(ThirdParty);
  DM.Engine.LinkEventsAutoInc(Litigation);
  DM.Engine.LinkEventsAutoInc(Document);
end;

procedure TDamageDetailsForm.TicketNumberKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key in [VK_DELETE, VK_BACK] then begin
    Damage.Edit;
    Damage.FieldByName('ticket_id').Clear;
  end;
end;

procedure TDamageDetailsForm.CloseActionExecute(Sender: TObject);
const
  SureComplete = 'Are you sure you want to complete this damage investigation?';
  SureClose = 'Are you sure you want to permanently close this damage investigation?';
var
  CloseError: string;
  CloseMessage: string;
  NewValue: string;
  DamageType: string;
  ReturnedMsg: string;
begin
  DamageType := Damage.FieldByName('damage_type').AsString;
  if DamageType = DamageTypeIncoming then begin
    Damage.Edit;
    Damage.FieldByName('damage_type').AsString := DamageTypePending;
    Exit;
  end;
  if not CheckIfDamageCanBeClosed(CloseError) then begin
    ErrorDialog(CloseError);
    Exit;
  end;
  if DamageType = DamageTypeApproved then
    raise Exception.Create('This damage is already closed and approved');
  if DamageTypeIsPendingApproval then begin
    if not PermissionsDM.CanI(RightDamagesApprove) then
      raise Exception.Create('You do not have the rights to approve damages.');
    if Damage.FieldByName('damage_id').AsInteger < 0 then
      raise Exception.Create('A damage must be synced to the server before it' +
        ' can be approved.');
    CloseMessage := SureClose;
    if not ValidateApprovalFields then
      Exit;
    NewValue := DamageTypeApproved;
  end
  else begin
    Assert(DamageType = DamageTypePending);
    CloseMessage := SureComplete;
    if (not StringInArray(UpperCase(LocateRequested.Text), ['YES', 'NO'])) then begin
      raise Exception.Create('Locate Requested must be Yes or No');
    end;
    NewValue := DamageTypePendingApproval;
    ApprovalDetailsTab.TabVisible := PermissionsDM.CanI(RightDamagesApprove);
    if not CheckIfDamageCanBeCompleted(CloseError) then
      raise EOdDataEntryError.Create(CloseError);
  end;
  ValidateDamageFields(NewValue); //Resp & Invoice codes need to validate for the new type before allowing save
  try
    if MessageDlg(CloseMessage, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      Exit;
    if NewValue <> DamageTypeApproved then begin
      Damage.Edit;
      Damage.FieldByName('damage_type').AsString := NewValue;
    end;
    if NewValue = DamageTypePendingApproval then
      Damage.FieldByName('closed_date').AsDateTime := Now;

    SaveDamage;
    if FErrorsDuringSave then
      Exit;

    if (NewValue = DamageTypeApproved) and not DamageTypeIsPendingApproval then
      raise Exception.Create('The damage investigation must be ' +
        DamageTypePendingApproval + ' or ' + DamageTypeCompleted + ' to approve.');

    DM.FlushDatabaseBuffers;
    if DamageTypeIsPendingApproval and (NewValue = DamageTypePendingApproval) then
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageComplete, '')
    else if (NewValue = DamageTypeApproved) then begin
      LDamageDM.ApproveDamage(FDamageID, ReturnedMsg);
      LDamageDM.UpdateDamageInCache(FDamageID);
      Damage.Refresh;
      DM.FlushDatabaseBuffers;
      if NotEmpty(ReturnedMsg) then
        raise Exception.Create(ReturnedMsg);
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageApproved, '');
    end;
  finally
    SetEnabledOnDamageControls(DamageEditAllowed(True));
  end;
end;

function TDamageDetailsForm.CheckIfDamageCanBeClosed(var Error: string): Boolean;
var
  Est: TDataSet;
begin
  Result := False;
  Est := DamageEstimateFrame.EstimateSource.DataSet;
  Est.First;
  while not Est.Eof do begin
    if Est.FieldByName('estimate_type').asInteger = 0 then begin
      Result := True;
      Break;
    end;
    Est.Next;
  end;
  if not Result then begin
    Error := 'A damage estimate must exist before closing a damage.';
    ChangeTabForError(DamagePageControl, EstimatesTab);
    Exit;
  end;

  if not Damage.FieldByName('uq_resp_code').IsNull then begin
    if Damage.FieldByName('ticket_id').IsNull then begin
      Result := False;
      Error := 'A ticket number must be specified before closing this damage';
      ChangeTabForError(DamagePageControl, DamageTab);
      Exit;
    end;
  end;
end;

procedure TDamageDetailsForm.CheckForAndLoadReferenceCodes;
begin
  DM.GetRefDisplayList('ynub', LocateRequested.Items);
  DM.GetRefDisplayList('ynub', LocateMarksPresent.Items);
  DM.GetRefDisplayList('ynub', FacilityWasPotholed.Items);
  DM.GetRefDisplayList('yesnoblank', AreaMarkedInWhite.Items);
  DM.GetRefDisplayList('yesnoblank', TracerWireIntact.Items);
  DM.GetRefDisplayList('yesnoblank', OffsetVisible.Items);
  DM.GetRefDisplayList('factype', FacilityType.Items);
  DM.GetRefDisplayList('factype', FacilityType2.Items); //QMANTWO-615
  DM.GetRefDisplayList('factype', FacilityType3.Items); //QMANTWO-615
  DM.GetRefDisplayList('buried', BuriedUnder.Items);
  DM.GetRefDisplayList('visibility', ClarityOfMarks.Items);
  DM.GetRefDisplayList('excrtype', ExcavatorType.Items);
  DM.GetRefDisplayList('excntype', ExcavationType.Items);
  DM.GetRefDisplayList('exres', ExcRespCode.Items);
  DM.GetRefDisplayList('yesnoblank', MarksWithinTolerance.Items);
  SizeComboDropdownToItems(ExcRespCode);
  DM.GetRefDisplayList('uqres', UQRespCode.Items);
  SizeComboDropdownToItems(UQRespCode);
  DM.GetRefDisplayList('locess', UQRespEssential.Items);
  SizeComboDropdownToItems(UQRespEssential);
  DM.GetRefDisplayList('utres', SpecialRespCode.Items);
  SizeComboDropdownToItems(SpecialRespCode);
  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');
//  DM.GetRefDisplayList('dmgco', UtilityCoDamaged.Items);
//  UtilityCoDamaged.Sorted := True;
//  SizeComboDropdownToItems(UtilityCoDamaged);
  DM.GetRefDisplayList('locexp', LocatorExperience.Items);
  DM.GetRefDisplayList('locequip', LocateEquipment.Items);
  LocateEquipment.Sorted := True;
  DM.GetRefDisplayList('claimstat', ClaimStatus.Items);
  DM.GetRefDisplayList('invcode', InvoiceCode.Items);
  DM.GetRefDisplayList('state', State.Items);
  DM.GetRefDisplayList('atty', LitigationAttorney.Items);
  DM.GetRefDisplayList('state', LitigationVenueState.Items);
  DM.GetRefDisplayList('claimstat', LitigationStatus.Items);
  DM.GetRefDisplayList('state', ThirdPartyState.Items);
  DM.GetRefDisplayList('claimstat', ThirdPartyStatus.Items);
  LitigationAttorney.Sorted := True;
  SizeComboDropdownToItems(LitigationAttorney);

  LDamageDM.DamageChangeReasonList(ReasonChanged.Items, True);  //populate regular combo with ref_id + description objects
  SizeComboDropdownToItems(ReasonChanged);

  ClaimCoordinatorSource.DataSet.Close;
  ClaimCoordinatorSource.DataSet.Open;
end;

function TDamageDetailsForm.Damage: TDataSet;
begin
  Result := DamageSource.DataSet;
  Assert(Assigned(Result));
end;

procedure TDamageDetailsForm.OpenDamageDataSets;
begin
  OpenDataSet(Damage);
  OpenDataSet(InvestigatorSource.DataSet);
  OpenDataSet(ClaimCoordinatorSource.DataSet);
  OpenDataSet(OfficeSource.DataSet);
  OpenDataSet(ClientSource.DataSet);
  OpenDataSet(InvoiceSource.DataSet);
  OpenDataSet(Litigation);
  Litigation.FieldByName('damage_id').Required := True;
  OpenDataSet(Carrier);
  (Carrier.FieldByName('deductible') as TNumericField).DisplayFormat := ',#';
  OpenDataSet(CarrierLookup);
  OpenDataSet(LDamageDM.DamageLookup);
  OpenDataSet(ThirdParty);
  ThirdParty.FieldByName('damage_id').Required := True;
end;

procedure SetupComboBox(Combo: TCustomComboBox; const RefName: string);
begin
  if RefName = '' then begin
    Combo.Enabled := False;
    Combo.Items.Clear;
  end
  else begin
    Combo.Enabled := True;
    DM.GetRefDisplayList(RefName, Combo.Items);
  end;
end;

procedure TDamageDetailsForm.DamagePageControlChange(Sender: TObject);
begin
  if FChangingTabsForError then
    Exit;

  // TODO: Hack to work around a bug where csDropDownList combos
  // aren't updating .Text when switching tabs (responsibility codes, etc.)
  DbComboRefresh(Damage);

  if DamagePageControl.ActivePage = PhotoAttachmentsTab then begin
    PhotoAttachmentFrame.Initialize;
    PhotoAttachmentFrame.OnPhotoAttachment := DamageAttachmentsFrame.OnPhotoAttachment;
    PhotoAttachmentFrame.AfterDoPhotoAttachment := DamageAttachmentsFrame.AfterDoPhotoAttachment;
  end
  else if DamagePageControl.ActivePage = InvoicesTab then
    DamageInvoice.GoToDamageInvoice(Invoices, FDamageID, 0, 0)
  else if DamagePageControl.ActivePage = DamageNotesTab then
    DamageNotesFrame.Initialize(qmftDamage, FDamageID, 0)
  else if DamagePageControl.ActivePage = ApprovalDetailsTab then
    adEstimateLabel.Caption := '$' + FloatToStr(LDamageDM.GetCurrentDamageEstimateAmount(FDamageId))
  else if DamagePageControl.ActivePage = HistoryTab then begin
    if HistoryPageControl.ActivePage = HistoryArrivalsTab then
      DM.OpenJobsiteArrivalData(FDamageID, atDamage);
    HistoryAccrualTab.TabVisible := (not InsertMode) and PermissionsDM.CanI(RightDamagesViewAccrualHistory);
  end
  else if DamagePageControl.ActivePage = RequiredDocumentsTab then
    DocumentQuery.Refresh;
end;

procedure TDamageDetailsForm.InitializeTabs;
var
  i: Integer;
begin
  for i := 0 to PageControl.PageCount - 1 do
    PageControl.Pages[i].HandleNeeded;
  for i := 0 to DamagePageControl.PageCount - 1 do
    DamagePageControl.Pages[i].HandleNeeded;
  for i := 0 to ThirdPartyPageControl.PageCount - 1 do
    ThirdPartyPageControl.Pages[i].HandleNeeded;
  for i := 0 to LitigationPageControl.PageCount - 1 do
    LitigationPageControl.Pages[i].HandleNeeded;
end;

procedure TDamageDetailsForm.YNUBControlKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_DELETE) or (Key = VK_BACK) then begin
    if Sender is TDBComboBox then
      (Sender as TDBComboBox).Field.Text := '';
  end;
end;

function TDamageDetailsForm.CloseButtonCaption: string;
var
  DamageType: string;
begin
  DamageType := Damage.FieldByName('damage_type').AsString;

  if DamageType = DamageTypeIncoming then
    Result := '&Open Investigation'
  else if DamageType = DamageTypePending then
    Result := 'Comp&lete Investigation'
  else
    Result := 'Appr&ove Investigation';
end;

function TDamageDetailsForm.CloseButtonEnabled: Boolean;
var
  DamageType: string;
begin
  DamageType := Damage.FieldByName('damage_type').AsString;
  if (DamageType = DamageTypeIncoming) or (DamageType = '') or (DamageType = DamageTypePending) then
    Result := True
  else if DamageTypeIsPendingApproval then
    Result := PermissionsDM.CanI(RightDamagesApprove)
  else if (DamageType = DamageTypeApproved) then
    Result := False
  else
    raise Exception.Create('Unknown damage type ' + DamageType);
  //Do not enable if Arrive Button is enabled; but do enable if user granted RightDamagesEditWithoutArriving
  Result := Result and ( (ArriveButton.Enabled = False) or (PermissionsDM.CanI(RightDamagesEditWithoutArriving)) );
end;

procedure TDamageDetailsForm.PrintActionExecute(Sender: TObject);
begin
  if Assigned(OnDamagePrint) then
    OnDamagePrint(Self, FUQDamageID);
end;

procedure TDamageDetailsForm.PrintApprovalDetailsActionExecute(Sender: TObject);
var
  RptParams : TStringList;
begin
  inherited;
  // first try to save... if needed...
  if (DamageSource.State <> dsBrowse) then
    SaveDamage;
  RptParams := TStringList.Create;
  try
    RptParams.Values['Report'] := 'DamageApprovalDetails';
    RptParams.Values['DamageID'] := InttoStr(FDamageId);
    RptParams.Values['PasswordHash'] :=  DM.UQState.Password;
    RptParams.Values['User'] := IntToStr(DM.UQState.UserID);
    PreviewPDF(RptParams);
  finally
    FreeAndNil(RptParams);
  end;
end;

function TDamageDetailsForm.ValidateApprovalFields: Boolean;
var
  ErrorMsg : string;
begin
  ErrorMsg := '';
  Result := True;
  if (not EstimateAgreeCheckBox.Checked) then begin
    Result := FALSE;
    ErrorMsg := 'You must verify that you agree with the estimate.'#13#10;
    if EstimateAgreeCheckBox.CanFocus then
      EstimateAgreeCheckBox.SetFocus;
  end;

  if (not ApproveCheckBox.Checked) then begin
    Result := False;
    ErrorMsg := ErrorMsg + 'You must check the "Approve" check box indicating that you have ' +
      'performed all the required verifications.';
    if ApproveCheckBox.CanFocus then
      ApproveCheckBox.SetFocus;
  end;

  if (not Result) then
    RaiseErrorOnTab(ErrorMsg, ApprovalDetailsTab);
end;

procedure TDamageDetailsForm.ConfirmInvoiceCodeChange;
const
  MsgConfirmInvCodeEdit = 'You are changing the invoice code to %s - %s. Are you sure?';
  MsgInvCodeEditUndone  = 'Your changes were saved, but the invoice code edit was undone.';
begin
  Assert(EditingDataSet(Damage), 'Damage is not in edit mode');
  if ((Damage.FieldByName('invoice_code').AsString <> OriginalInvoiceCode) and
    NotEmpty(OriginalInvoiceCode)) then begin
    if MessageDlg(Format(MsgConfirmInvCodeEdit, [Damage.FieldByName('invoice_code').AsString, InvoiceCode.Text]),
      mtConfirmation, [mbYes, mbNo], 0) <> mrYes then begin
      Damage.FieldByName('invoice_code').AsString := OriginalInvoiceCode;
      MessageDlg(MsgInvCodeEditUndone, mtInformation, [mbOk], 0);
    end;
  end;
end;

procedure TDamageDetailsForm.ValidateDamageFields(const DamageType: string);
const
  MsgNeedToCloseInvestigation = 'You must close the investigation before the claim status can be closed.';
  MsgNeedClient = 'Client must be specified when %s responsibility is selected.';
  MsgNeedRespEssStep = 'Locate essential step must be specified when %s responsibility is selected.';
  MsgNeedInvoices = 'There must be at least one invoice for this damage to use the selected invoice code.';
  MsgNeedNoInvoices = 'There cannot be any invoices for this damage to use the selected invoice code.';
  MsgOffsetMustBeVisible = 'Offset visible must be Y in order to specify an offset quantity.';
  MsgProfitCenterMustBeAssigned = 'A Profit Center is required to save a damage.';
  InvoiceCodesNeedingInvoices: array[0..1] of string = ('DISPUTED','DENIED');
var
  ClaimStatus: string;
begin
  VerifyDamageDates;
  ClaimStatus := Damage.FieldByName('claim_status').AsString;
  ValidateResponsibilityAndInvoiceCodes(DamageType);
  if (ClaimStatus <> '') and SameText(DM.GetRefModifier('claimstat', ClaimStatus), 'CLOSED') then begin
    if not (DamageTypeIsPendingApproval or (Damage.FieldByName('damage_type').AsString = DamageTypeApproved)) then
      raise EOdDataEntryError.Create(MsgNeedToCloseInvestigation);
  end;
  if (not Damage.FieldByName('uq_resp_code').IsNull) then begin
    if (Damage.FieldByName('client_name').AsString = '') then
      RaiseErrorOnTab(Format(MsgNeedClient, [DM.UQState.CompanyShortName]), DetailsTab);
    if (Damage.FieldByName('uq_resp_ess_step').AsString = '') then
      RaiseErrorOnTab(Format(MsgNeedRespEssStep, [DM.UQState.CompanyShortName]), ResponsibilityTab);
  end;
  if SameText(Damage.FieldByName('invoice_code').AsString, 'NOPAY') and (DamageInvoiceCount > 0) then
    RaiseErrorOnTab(MsgNeedNoInvoices, InvoicesTab);
  if StringInArray(Damage.FieldByName('invoice_code').AsString, InvoiceCodesNeedingInvoices) and (DamageInvoiceCount < 1) then
    RaiseErrorOnTab(MsgNeedInvoices, InvoicesTab);
  if (Damage.FieldByName('offset_visible').AsString <> '1') and (not Damage.FieldByName('offset_qty').IsNull) then
    RaiseErrorOnTab(MsgOffsetMustBeVisible, SiteTab);
  if IsEmpty(Damage.FieldByName('profit_center').AsString) then
    RaiseErrorOnTab(MsgProfitCenterMustBeAssigned, DamageTab);
end;

procedure TDamageDetailsForm.CallCentersChange(Sender: TObject);
begin
  inherited;
  if CallCenters.ItemIndex < 1 then
  begin
    Client.Filter := '';
    Client.Filtered := False;
  end
  else
  begin
    Client.Filter := 'call_center = '+ QuotedStr(FCallCenterList.GetCode(CallCenters.Text));
    Client.Filtered := False;
    Client.Filtered := True;
  end;
  ClientID.ItemIndex:=-1;
  ClientCode.text := '';
  UtilityCoDamaged.text := '';
  CustomQuestionsTab.TabVisible := False;
end;

procedure TDamageDetailsForm.SelectCallCenterForClient;
var
  CallCenter: string;
begin
  CallCenter := DM.GetCallCenterForClientID(Damage.FieldByName('client_id').AsInteger);

  if (CallCenter = '') then
    CallCenters.ItemIndex := 0
  else
    CallCenters.ItemIndex := FCallCenterList.GetIndexForCode(CallCenter);

  CallCentersChange(CallCenters);
end;

procedure TDamageDetailsForm.FacilityTypeChange(Sender: TObject);
const
  FAC_TYPE_1 = 'FacilityType';
  FAC_TYPE_2 = 'FacilityType2';
  FAC_TYPE_3 = 'FacilityType3';
begin
  // The User changed it
  if TDBComboBox(Sender).Name = FAC_TYPE_1 then     //QMANTWO-615
  PostMessage(Handle, UM_UPDATECOMBOS, 1, 1)
  else
  if TDBComboBox(Sender).Name = FAC_TYPE_2 then      //QMANTWO-615
  PostMessage(Handle, UM_UPDATECOMBOS, 2, 2)
  else
  if TDBComboBox(Sender).Name = FAC_TYPE_3 then     //QMANTWO-615
  PostMessage(Handle, UM_UPDATECOMBOS, 3, 3)
end;

procedure TDamageDetailsForm.UMUpdateCombos(var Msg: TMessage);
var
  FacilityRow:integer;
begin
  FacilityRow := Msg.LParam;
  case FacilityRow of
  1:  UpdateDependentDropdowns(True, FacilityRow);  //QMANTWO-615
  2:  UpdateDependentDropdowns(True, FacilityRow);  //QMANTWO-615
  3:  UpdateDependentDropdowns(True, FacilityRow);  //QMANTWO-615
  end;
end;

procedure TDamageDetailsForm.UpdateDependentDropdowns(UserChanged: Boolean;FacilityRow:integer);  //QMANTWO-615
  procedure SetupDependentCombo(const RefType, Modifier: string; Combo: TDBComboBox);
  var
    ExpectedValue, NewValue: string;
  begin
    Damage.FieldByName(Combo.DataField).Origin := RefType + '.' + Modifier;
    ExpectedValue := Damage.FieldByName(Combo.DataField).AsString;

    if Modifier = '' then begin
      Combo.Items.Clear;
      Combo.Enabled := False;
    end else begin
      DM.GetRefDisplayList(RefType, Combo.Items, Modifier);
      //Dropdowns that are enabled but the parent container is disabled- will result
      //in the dropdown being disabled but not grayed out; thus check for parent.enabled.
      Combo.Enabled := Combo.Parent.Enabled;

      DbComboRefresh(Damage);

      // The only time we should ever change the value of a dependent field
      // is if the user changes the master field value by clicking on it
      // (NOT by loading a new record)
      // and the old value of the dependent field is incompatible with the new
      // value of the master field
      if (ExpectedValue<>'') and (Combo.Text='') then begin
        // we have data in the DB, but the lookup failed on the combo
        if UserChanged then begin
          // the user changed the master combo, so blank out this combo:
          Assert(Damage.State in dsEditModes, 'Should be in edit mode, if changing combo');
          Damage.FieldByName(Combo.DataField).AsString := '';
          ExpectedValue := ''  // we expect it to be blank
        end else if (ExpectedValue <> '--') then begin
          ShowMessage('Warning - the values for facility type and size/materials do not match; '
           + ExpectedValue +  ' is not in the dropdown list');
        end;
      end;
    end;

    // Protection from those top quality Delphi built in components:
    NewValue := Damage.FieldByName(Combo.DataField).AsString;
    Assert(ExpectedValue = NewValue,
      'Internal error, data loss in combo box handling, ' + Combo.DataField +
      ': ' + ExpectedValue + '/' + NewValue);
  end;

var
  Modifier: string;
begin
  if Damage.State in dsEditModes then
    Damage.UpdateRecord;  // to get the Fac Type in to the dataset
                          // before the disable/enable happens
  case FacilityRow of  //QMANTWO-615
    1:begin
      Modifier := DM.GetRefCodeForDisplay('factype', FacilityType.Text);
      SetupDependentCombo('facsize', Modifier, FacilitySize);
      SetupDependentCombo('facmtrl', Modifier, FacilityMaterial);
    end;
    2:begin
      Modifier := DM.GetRefCodeForDisplay('factype', FacilityType2.Text);
      SetupDependentCombo('facsize', Modifier, FacilitySize2);     //QMANTWO-615
      SetupDependentCombo('facmtrl', Modifier, FacilityMaterial2); //QMANTWO-615
    end;
    3:begin
      Modifier := DM.GetRefCodeForDisplay('factype', FacilityType3.Text);
      SetupDependentCombo('facsize', Modifier, FacilitySize3);     //QMANTWO-615
      SetupDependentCombo('facmtrl', Modifier, FacilityMaterial3); //QMANTWO-615
    end;
  end;
end;

procedure TDamageDetailsForm.ShowTicketDetails;
var
  TicketID: Integer;
  HaveTicket: Boolean;
begin
  TicketID := Damage.FieldByName('ticket_id').AsInteger;
  HaveTicket := (TicketID > 0);
  if HaveTicket and Ticket.Locate('ticket_id', TicketID, []) then
    TicketDueDate.Caption := Ticket.FieldByName('due_date').AsString
  else
    TicketDueDate.Caption := '';
end;

procedure TDamageDetailsForm.TicketNumberChange(Sender: TObject);
begin
  ShowTicketDetails;
end;

procedure TDamageDetailsForm.AfterGoToDamage;
begin
  ClaimStatus.Enabled := PermissionsDM.CanI(RightDamagesSetClaimStatus) and (PermissionsDM.GetLimitationListCount(RightDamagesSetClaimStatus) > 0);
  OriginalInvoiceCode := VarToString(Damage.FieldByName('invoice_code').AsString);
  BillingGroup.Visible := PermissionsDM.CanI(RightDamagesBillDamage);
  SetTableFilters(Damage.FieldByName('damage_id').AsInteger);
  FDamageNarrativeText := '';
  UpdateDisplay;
  PrepareAttachmentsAddin;
  RefreshRequiredDamageDocs;
  LoadDamageDocuments;
  SetupDocumentGrid(ApprovalMissingDocGrid, True);
end;

procedure TDamageDetailsForm.UpdateDisplay;
begin
  PostMessage(Handle, UM_UPDATEDISPLAY, 1, 1);
//  PostMessage(Handle, UM_UPDATEDISPLAY, 2, 2); //QMANTWO-615
//  PostMessage(Handle, UM_UPDATEDISPLAY, 3, 3); //QMANTWO-615
end;

procedure TDamageDetailsForm.UMUpdateDisplay(var Msg: TMessage);
var
  FacilityRow : integer;
begin
  FacilityRow :=   Msg.WParam;
  ShowTicketDetails;
  HandleNeeded;
  DamagePageControl.HandleNeeded;
  Application.ProcessMessages;
  DamagePageControlChange(DamagePageControl);
  UpdateDependentDropdowns(False, 1);  //QMANTWO-615
  UpdateDependentDropdowns(False, 2);  //QMANTWO-615
  UpdateDependentDropdowns(False, 3);  //QMANTWO-615
  UpdateInvestigator;
  UpdateLocator;
end;

procedure TDamageDetailsForm.ValidateResponsibilityAndInvoiceCodes(const DamageType: string);
const
  RespFields: array[0..2] of string = ('uq_resp_code', 'exc_resp_code', 'spc_resp_code');
  InvoiceFields: array[0..0] of string = ('invoice_code');
  ReasonFields: array [0..0] of string = ('reason_changed');
begin
  if StringInArray(DamageType, [DamageTypePendingApproval, DamageTypeCompleted, DamageTypeApproved]) then begin
    if not OneFieldHasAValue(Damage, InvoiceFields) then begin
      if InvoiceCode.CanFocus then
        InvoiceCode.SetFocus;
      RaiseErrorOnTab('An invoice code is required for all completed investigations', DetailsTab);
    end;
    if not OneFieldHasAValue(Damage, RespFields) then begin
      RaiseErrorOnTab('Please assign responsibility to at least one party', ResponsibilityTab);
    end;
  end;

  if not InsertMode then begin
    if FResponsibilityChanged then begin
      //Checks case of reason not changed, or changed to empty
      if (ReasonChanged.ItemIndex <= 0) then begin
        if ResponsibilityPreviouslySet then begin
          if ReasonChanged.CanFocus then
            ReasonChanged.SetFocus;
          RaiseErrorOnTab('A reason for change must be selected when changing Responsibility for all investigations.', ResponsibilityTab);
        end;
      end;
    end;
  end;
end;

procedure TDamageDetailsForm.SaveDamageBill;
begin
  if not PermissionsDM.CanI(RightDamagesBillDamage) then
    raise Exception.Create('You do not have permission to bill damages.');

  if (DamageBill.State = dsInsert) and (DamageBill.FieldByName('billable').AsBoolean = False) then begin
    CancelDataset(DamageBill);
    Exit;
  end;

  if DamageBill.FieldByName('billable').AsBoolean then begin
    if Damage.FieldByName('client_id').IsNull then
      RaiseErrorOnTab('A client must be entered for the damage before making it billable.',DetailsTab);
  end;

  VerifyDamageBillDates;
  DamageBill.FieldByName('modified_date').AsDateTime := Now;
  DamageBill.FieldByName('modified_by').AsInteger := DM.UQState.EmpID;
  PostDataset(DamageBill);
end;

function TDamageDetailsForm.DamageInvoiceCount: Integer;
begin
  if Invoices.Active then
    Result := Invoices.RecordCount
  else
    Result := 0;
end;

procedure TDamageDetailsForm.FinishedInvoiceEdit(Sender: TObject);
begin
  Invoices.Close;
  Invoices.ParamByName('damage_id').AsInteger := FDamageID;
  Invoices.Open;
  DamageInvoice.GoToDamageInvoice(Invoices, FDamageID);
end;

procedure TDamageDetailsForm.FinishedThirdPartyInvoiceEdit(Sender: TObject);
begin
  Invoices.Close;
  Invoices.ParamByName('damage_id').AsInteger := FDamageID;
  Invoices.Open;
  ThirdPartyInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, FThirdPartyID, 0);
end;

procedure TDamageDetailsForm.FinishedLitigationInvoiceEdit(Sender: TObject);
begin
  Invoices.Close;
  Invoices.ParamByName('damage_id').AsInteger := FDamageID;
  Invoices.Open;
  LitigationInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, 0, FLitigationID);
end;

procedure TDamageDetailsForm.HideDamageTabs;
begin
  ShowPageControlAsSingleTab(DamageTab);
end;

procedure TDamageDetailsForm.ShowDamageTabs;
begin
  HideDamageTabs;
  if PermissionsDM.EmployeeHasLitigationRights then
    ShowPageControlAsMultiTab(PageControl);
end;

procedure TDamageDetailsForm.SetDamageTypeCaption(const Value: string);
begin
  DamageTypeLabel.Caption := value;
end;

procedure TDamageDetailsForm.LitigationSaveActionExecute(Sender: TObject);
var
  Cursor: IInterface;
  Time: TDateTime;
  EditedLitigation: Boolean;
begin
  Cursor := ShowHourGlass;
  Time := Now;
  EditedLitigation := False;
  if SaveLitigationButton.CanFocus then
    SaveLitigationButton.SetFocus;
  if EditingDataSet(Litigation) then begin
    Litigation.UpdateRecord;
    ValidateLitigationFields;
    EditDataSet(Litigation);
    Litigation.FieldByName('modified_by').AsInteger := DM.EmpID;
    Litigation.FieldByName('LocalKey').AsDateTime := Time;
    Litigation.Post;
    DM.FlushDatabaseBuffers;
    EditedLitigation := True;
  end;
  LitigationInvoiceFrame.Save;
  DM.SendChangesToServer;
  if EditedLitigation and LitigationInsertMode then begin
    if not Litigation.Locate('LocalKey', Time, []) then
      raise Exception.Create('Unable to locate the newly inserted litigation record');
    LDamageDM.UpdateDamageInCache(Litigation.FieldByName('damage_id').Value);
    LitigationPageControl.Visible := False;
    Litigation.Refresh;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddLitigationAdd, '');
  end;
  if EditedLitigation and not LitigationInsertMode then begin
    LDamageDM.UpdateDamageInCache(Litigation.FieldByName('damage_id').Value);
    Litigation.Refresh;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddLitigationSaveChanges, '');
  end;

  if EditedLitigation then begin
    LDamageDM.DamageEstimate.Refresh;
    ThirdParty.Refresh;
    Damage.Refresh;
    LitigationDone;
  end;
end;

procedure TDamageDetailsForm.LitigationCancelActionExecute(Sender: TObject);
begin
  LitigationInvoiceFrame.Cancel;
  Litigation.Cancel;
  LitigationDone;
end;

procedure TDamageDetailsForm.LitigationAddActionExecute(Sender: TObject);
begin
  AddNewDamageLitigation(FDamageID);
end;

procedure TDamageDetailsForm.GotoLitigationId(const ID: Integer);
begin
  FLitigationID := ID;
  OpenDamageDataSets;
  CheckForAndLoadReferenceCodes;
  ClearTableFilters;
  if not Litigation.Locate('litigation_id', ID, []) then
    raise Exception.Create('Unable to find litigation id: ' + IntToStr(ID));
  if not Litigation.FieldByName('damage_id').IsNull then begin
    LDamageDM.UpdateDamageInCache(Litigation.FieldByName('damage_id').AsInteger);
    Litigation.Refresh;
    SetTableFilters(Litigation.FieldByName('damage_id').AsInteger);
    FDamageID := Litigation.FieldByName('damage_id').asInteger;
    GotoDamageID(FDamageID);
  end;
  LitigationInsertMode := False;
  LitigationEstimateFrame.Initialize(qmestLitigation, FDamageID, FLitigationID);
  LitigationAttachmentsFrame.Initialize(qmftLitigation, FLitigationID);
  LitigationInvoiceFrame.Initialize(DamageSource, qminvLitigation, FDamageID, FLitigationId);
  LitigationInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, 0, FLitigationID);
  LitigationReadOnly := not PermissionsDM.CanI(RightLitigationEdit);
  PageControl.ActivePage := LitigationTab;
  LitigationPageControl.ActivePage := LitigationNotifyTab;
end;

procedure TDamageDetailsForm.AddNewDamageLitigation(const DamageID: Integer);
begin
  LitigationInsertMode := True;
  FDamageID := DamageID;
  OpenDamageDataSets;
  CheckForAndLoadReferenceCodes;
  Litigation.Insert;
  Litigation.FieldByName('damage_id').AsInteger := FDamageID;
  Litigation.FieldByName('carrier_notified').AsBoolean := False;
  Litigation.FieldByName('summons_date').AsDateTime := Now;
  Litigation.FieldByName('claim_status').AsString := 'OPEN';

  LitigationEstimateFrame.Initialize(qmestLitigation, Litigation.FieldByName('damage_id').AsInteger,
    Litigation.FieldByName('litigation_id').AsInteger);
  LitigationAttachmentsFrame.Initialize(qmftLitigation,
    Litigation.FieldByName('litigation_id').asInteger);
  PageControl.ActivePage := LitigationTab;
end;

procedure TDamageDetailsForm.LitigationCalcFields(DataSet: TDataSet);
begin
  if not Dataset.FieldByName('summons_date').IsNull then
    Dataset.FieldByName('respond_by_date').asDateTime := Dataset.FieldByName('summons_date').AsDateTime + 15;
end;

procedure TDamageDetailsForm.LitigationAfterScroll(DataSet: TDataSet);
begin
  LitigationEstimateFrame.EstimateCompanyLabel.Caption := Litigation.FieldByName('plaintiff').AsString;
  FLitigationID := GetCurrentLitigationID;
end;

procedure TDamageDetailsForm.PageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := True;
  if (PageControl.ActivePage = DamageTab) and (EditingDataSet(Damage)) then begin
    AllowChange := False;
    //Damage Save button is on Main screen
    if SaveButton.CanFocus then
      SaveButton.SetFocus;
    ShowMessage('Please save or cancel your damage changes before switching tabs.');
  end
  else if (PageControl.ActivePage = LitigationTab) and (EditingDataSet(Litigation)) then begin
    AllowChange := False;
    ShowMessageForTabControl('Please save or cancel your litigation changes before switching tabs.',
      LitigationPageControl, LitigationNotifyTab, SaveLitigationButton);
  end
  else if (PageControl.ActivePage = ThirdPartyTab) and (EditingDataSet(ThirdParty)) then begin
    AllowChange := False;
    ShowMessageForTabControl('Please save or cancel your 3rd party changes before switching tabs.',
      ThirdPartyPageControl, ThirdPartyNotifyTab, SaveThirdPartyButton);
  end;
end;


procedure TDamageDetailsForm.PopulateNoteSubTypes;
begin
  DM.NotesSubTypeList(FDamageNoteSubTypeList, qmftDamage);
  DM.NotesSubTypeList(FTPNoteSubTypeList, qmft3rdParty);
  DM.NotesSubTypeList(FLitNoteSubTypeList, qmftLitigation);
end;

procedure TDamageDetailsForm.GotoThirdPartyId(const ID: Integer);
begin
  FThirdPartyID := ID;
  OpenDamageDataSets;
  CheckForAndLoadReferenceCodes;
  ClearTableFilters;
  if not ThirdParty.Locate('third_party_id', ID, []) then
    raise Exception.Create('Unable to find third party id: ' + IntToStr(ID));
  if not ThirdParty.FieldByName('damage_id').IsNull then begin
    LDamageDM.UpdateDamageInCache(ThirdParty.FieldByName('damage_id').AsInteger);
    ThirdParty.Refresh;
    SetTableFilters(ThirdParty.FieldByName('damage_id').AsInteger);
    FDamageID := ThirdParty.FieldByName('damage_id').asInteger;
    GotoDamageID(FDamageID);
  end;
  ThirdPartyInsertMode := False;
  ThirdPartyInvoiceFrame.Initialize(DamageSource, qminv3rdParty, FDamageID, FThirdPartyID);
  ThirdPartyEstimateFrame.Initialize(qmest3rdParty, FDamageID, FThirdPartyID);
  ThirdPartyAttachmentsFrame.Initialize(qmft3rdParty, FThirdPartyID);
  ThirdPartyReadOnly := not PermissionsDM.CanI(RightLitigationEdit);
  ThirdPartyInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, FThirdPartyID, 0);
  PageControl.ActivePage := ThirdPartyTab;
  ThirdPartyPageControl.ActivePage := ThirdPartyNotifyTab;
end;

procedure TDamageDetailsForm.ThirdPartyCancelActionExecute(Sender: TObject);
begin
  ThirdPartyInvoiceFrame.Cancel;
  ThirdParty.Cancel;
  ThirdPartyDone;
end;

procedure TDamageDetailsForm.ThirdPartySaveActionExecute(Sender: TObject);
var
  Cursor: IInterface;
  Time: TDateTime;
  EditedThirdParty: Boolean;
begin
  Cursor := ShowHourGlass;
  Time := Now;
  EditedThirdParty := False;
  if SaveThirdPartyButton.CanFocus then
    SaveThirdPartyButton.SetFocus;
  if EditingDataSet(ThirdParty) then begin
    ThirdParty.UpdateRecord;
    // ValidateThirdPartyFields;
    EditDataSet(ThirdParty);
    ThirdParty.FieldByName('LocalKey').AsDateTime := Time;
    ThirdParty.FieldByName('modified_by').AsInteger := DM.EmpID;
    ThirdParty.Post;
    DM.FlushDatabaseBuffers;
    EditedThirdParty := True;
  end;
  ThirdPartyInvoiceFrame.Save;
  DM.SendChangesToServer;
  if EditedThirdParty and ThirdPartyInsertMode then begin
    if not ThirdParty.Locate('LocalKey', Time, []) then
      raise Exception.Create('Unable to locate the newly inserted third party record');
    LDamageDM.UpdateDamageInCache(ThirdParty.FieldByName('damage_id').Value);
    ThirdPartyPageControl.Visible := False;
    ThirdParty.Refresh;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageThirdPartyAdd, '');
  end;
  if EditedThirdParty and not ThirdPartyInsertMode then begin
    LDamageDM.UpdateDamageInCache(ThirdParty.FieldByName('damage_id').Value);
    ThirdParty.Refresh;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddThirdPartySaveChanges, '');
  end;

  if EditedThirdParty then begin
    LDamageDM.DamageEstimate.Refresh;
    Litigation.Refresh;
    Damage.Refresh;
    ThirdPartyDone;
  end;
end;

procedure TDamageDetailsForm.ThirdPartyAddActionExecute(Sender: TObject);
begin
  AddNewDamageThirdParty(FDamageID);
end;

procedure TDamageDetailsForm.AddNewDamageThirdParty(const DamageID: Integer);
begin
  ThirdPartyInsertMode := True;
  FDamageID := DamageID;
  OpenDamageDataSets;
  CheckForAndLoadReferenceCodes;
  ThirdParty.Insert;
  ThirdParty.FieldByName('damage_id').AsInteger := FDamageID;
  ThirdParty.FieldByName('carrier_notified').AsBoolean := False;
  ThirdParty.FieldByName('uq_notified_date').AsDateTime := RoundSQLServerTimeMS(Now);
  ThirdParty.FieldByName('claim_status').AsString := 'OPEN';
  ThirdPartyStatus.ItemIndex := ThirdPartyStatus.Items.IndexOf('Open');
  ThirdParty.FieldByName('added_by').AsInteger := DM.EmpID;
  ThirdPartyEstimateFrame.Initialize(qmest3rdParty, DamageID, ThirdParty.FieldByName('third_party_id').AsInteger);
  ThirdPartyAttachmentsFrame.Initialize(qmft3rdParty, ThirdParty.FieldByName('third_party_id').asInteger);
  SetEnabledOnControlAndChildren(EditThirdPartyPanel, True);
  PageControl.ActivePage := ThirdPartyTab;
end;

procedure TDamageDetailsForm.SetTableFilters(const DamageID: Integer);
begin
  Litigation.Filter := 'damage_id=' + IntToStr(DamageID);
  Litigation.Filtered := True;
  ThirdParty.Filter := 'damage_id=' + IntToStr(DamageID);
  ThirdParty.Filtered := True;
end;

procedure TDamageDetailsForm.ClearTableFilters;
begin
  Litigation.Filtered := False;
  Litigation.Filter := '';
  ThirdParty.Filtered := False;
  ThirdParty.Filter := '';
end;

procedure TDamageDetailsForm.ClientFieldsViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin  //QMANTWO-627
  inherited;
  If not VarToBoolean(ARecord.Values[ClientFieldsViewActive.Index]) then
    AStyle := cxInactive;
end;




procedure TDamageDetailsForm.ClientIDPropertiesCloseUp(Sender: TObject);    //QMANTWO-627
var
  refid : integer;
  iclientID : integer;
  description, oc_code:string;

begin
  refid:= -1;
  oc_code:='';
  iclientID:=-1;

  if not(Client.FieldByName('ref_id').IsNull) then
    refid := Client.FieldByName('ref_id').AsInteger;
  if not(Client.FieldByName('oc_code').IsNull) then
    oc_code := Client.FieldByName('oc_code').AsString;
  if not(Client.FieldByName('client_ID').IsNull) then
    iclientID:=Client.FieldByName('client_ID').asinteger;
  clientCode.text := oc_code;
  LDamageDM.RefDescForDamageCos.ParamByName('refid').Value := refid;
  try
    if iclientID<>-1 then
    begin
      LDamageDM.RefDescForDamageCos.Open;
      description := LDamageDM.RefDescForDamageCos.FieldByName('description').AsString;
      UtilityCoDamaged.Text := description;

      if (refID > 0) and not InsertMode then begin //QM-693 EB Damage Custom Form
        fCustDamageRec.RefID := refID;  //QM-593 EB
        GetDamageCustomForm;  //QM-593 EB Damage Custom Form
      end
      else  //QM-693 EB Damage Custom Form (hide pre-existing tab if there is no refID for change)
        CustomQuestionsTab.TabVisible := False;
    end;
    if not(DamageSource.State in [dsEdit,dsInsert]) then
    Damage.Edit;
    if UtilityCoDamaged.Text<> '' then
    begin
      Damage.FieldByName('utility_co_damaged').asString := utilityCoDamaged.Text;
      Damage.FieldByName('client_code').asString :=  oc_code;
    end;
    UtilityCoDamaged.refresh;
  finally
    LDamageDM.RefDescForDamageCos.close;
    clientCode.Refresh;
  end;

end;

procedure TDamageDetailsForm.DamageTabShow(Sender: TObject);
begin
  DamageInvoice.Initialize(DamageSource, qminvDamage, FDamageID, -1);
  DamageEstimateFrame.Initialize(qmestDamage, FDamageID, -1);
  if Damage.FieldByName('ticket_id').IsNull then
    DamageAttachmentsFrame.Initialize(qmftDamage, FDamageID, True)
  else
    DamageAttachmentsFrame.Initialize(qmftDamage, FDamageID, True, qmftTicket,
      Damage.FieldByName('ticket_id').AsInteger);

end;

procedure TDamageDetailsForm.SetEnabledOnDamageControls(Enabled: Boolean);
var
  i,j : Integer;
  AllowedEditWhenApproved: Boolean;
begin
  //Damage tab
  AllowedEditWhenApproved := PermissionsDM.CanI(RightDamagesEditWhenApproved);

  for i:=0 to DamagePageControl.PageCount - 1 do begin
    if ((DamagePageControl.Pages[i] = NewDamageTab) and InsertMode) then
      SetEnabledOnControlAndChildren(DamagePageControl.Pages[i], True)
    else if (DamagePageControl.Pages[i] = HistoryTab) then begin
      for j:= 0 to HistoryPageControl.PageCount - 1 do //Allow history subtabs to be enabled
        SetEnabledOnControlAndChildren(HistoryPageControl.Pages[j], Enabled);
    end //Damage tab, Responsibility and Estimates on APPROVED damages cannot be edited by users w/o RightDamageEditWhenApproved
    else if (Damage.FieldByName('damage_type').AsString = DamageTypeApproved) and
      ((DamagePageControl.Pages[i] = NewDamageTab ) or
      (DamagePageControl.Pages[i] = ResponsibilityTab) or
       (DamagePageControl.Pages[i] = EstimatesTab)) then
      SetEnabledOnControlAndChildren(DamagePageControl.Pages[i], (Enabled and AllowedEditWhenApproved))
    else
      SetEnabledOnControlAndChildren(DamagePageControl.Pages[i], Enabled);
  end;

  SaveButton.Enabled := (Enabled and (Damage.State in dsEditModes)) or InsertMode;
  PrintButton.Enabled := Enabled or InsertMode;
  CloseButton.Enabled := Enabled and CloseButtonEnabled;
  //Dropdowns that are enabled but the parent container is disabled- will result
  //in the dropdown being disabled but not grayed out; thus check for parent.enabled.
  ProfitCenter.Enabled := (Enabled and PermissionsDM.CanI(RightDamagesAssignProfitCenter)
    and ProfitCenter.Parent.Enabled) or InsertMode;

  //Third Party tab
  for i:=0 to ThirdPartyPageControl.PageCount - 1 do
    SetEnabledOnControlAndChildren(ThirdPartyPageControl.Pages[i], (PermissionsDM.CanI(RightLitigationEdit) and Enabled));
  SetupThirdPartyEditing((not ThirdPartyReadOnly) and Enabled);
  SaveThirdPartyButton.Enabled := Enabled and (ThirdPartySaveAction.Enabled or (ThirdPartyInsertMode and EditingDataSet(ThirdParty) ));
  CancelThirdPartyButton.Enabled := Enabled and (ThirdPartyCancelAction.Enabled or (ThirdPartyInsertMode and EditingDataSet(ThirdParty) ));

  //Litigation tab
  for i:=0 to LitigationPageControl.PageCount - 1 do
    SetEnabledOnControlAndChildren(LitigationPageControl.Pages[i], (PermissionsDM.CanI(RightLitigationEdit) and Enabled));
  SetupLitigationEditing((not LitigationReadOnly) and Enabled);
  SaveLitigationButton.Enabled := Enabled and (LitigationSaveAction.Enabled or (LitigationInsertMode and EditingDataSet(Litigation) ));
  CancelLitigationButton.Enabled := Enabled and (LitigationCancelAction.Enabled or (LitigationInsertMode and EditingDataSet(Litigation) ));

  //Other enabling rules that should be applied after Arrival enabling rules
  if Enabled then begin
    if AllowedEditWhenApproved then
      DamageEstimateFrame.RefreshNow;
    DamageAttachmentsFrame.RefreshNow;
    DamageInvoice.RefreshNow;
    //if the ThirdParty or Lit invoice tabs are active, need to call RefreshNow to reflect
    //the frame's enabled state-otherwise Initialize call on tab change takes care of it.
    if ((PageControl.ActivePage = ThirdPartyTab) and
      (ThirdPartyPageControl.ActivePage = ThirdPartyInvoiceTab)) then
      ThirdPartyInvoiceFrame.RefreshNow
    else if ((PageControl.ActivePage = LitigationTab) and
      (LitigationPageControl.ActivePage = LitigationInvoiceTab)) then
      LitigationInvoiceFrame.RefreshNow;
    ClaimStatus.Enabled := PermissionsDM.CanI(RightDamagesSetClaimStatus) and (PermissionsDM.GetLimitationListCount(RightDamagesSetClaimStatus) > 0);
    BillingGroup.Visible := PermissionsDM.CanI(RightDamagesBillDamage);
    //Once a Damage is Approved, it cannot be un-Approved
    ApproveCheckbox.Enabled := not (Damage.FieldByName('damage_type').AsString = DamageTypeApproved);
    EstimateAgreeCheckBox.Enabled := not (Damage.FieldByName('damage_type').AsString = DamageTypeApproved);
  end;
end;

procedure TDamageDetailsForm.ThirdPartyTabShow(Sender: TObject);
begin
  ThirdPartyInsertMode := ThirdParty.IsEmpty;
  FThirdPartyID := GetCurrentThirdPartyID;
  ThirdPartyEstimateFrame.Initialize(qmest3rdParty, FDamageID, FThirdPartyId);
  ThirdPartyInvoiceFrame.Initialize(DamageSource, qminv3rdParty, FDamageID, FThirdPartyId);
  ThirdPartyAttachmentsFrame.Initialize(qmft3rdParty, FThirdPartyId);
  if not ThirdPartyInsertMode then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageThirdPartyView, Damage.FieldByName('uq_damage_id').AsString);
end;

procedure TDamageDetailsForm.LitigationTabShow(Sender: TObject);
begin
  LitigationInsertMode := Litigation.IsEmpty;
  FLitigationID := GetCurrentLitigationID;
  LitigationInvoiceFrame.Initialize(DamageSource, qminvLitigation, FDamageID, FLitigationId);
  LitigationEstimateFrame.Initialize(qmestLitigation, FDamageID, FLitigationId);
  LitigationAttachmentsFrame.Initialize(qmftLitigation, FLitigationId);
  if not LitigationInsertMode then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeDamageAddLitigationView, Damage.FieldByName('uq_damage_id').AsString);
end;

procedure TDamageDetailsForm.ThirdPartyAfterScroll(DataSet: TDataSet);
begin
  ThirdPartyEstimateFrame.EstimateCompanyLabel.Caption := ThirdParty.FieldByName('claimant').AsString;
  FThirdPartyID := GetCurrentThirdPartyID;
end;

procedure TDamageDetailsForm.SetLitigationReadOnly(const Value: Boolean);
begin
  if Value <> FLitigationReadOnly then begin
    FLitigationReadOnly := Value;
    SetupLitigationEditing(not LitigationReadOnly);
  end;
end;

procedure TDamageDetailsForm.SetThirdPartyReadOnly(const Value: Boolean);
begin
  if Value <> FThirdPartyReadOnly then begin
    FThirdPartyReadOnly := Value;
    SetupThirdPartyEditing(not ThirdPartyReadOnly);
  end;
end;

procedure TDamageDetailsForm.SetupLitigationEditing(Edit: Boolean);
begin
  SetEnabledOnControlAndChildren(EditLitigationPanel, Edit);
  LitigationAddAction.Enabled := PermissionsDM.CanI(RightLitigationEdit);
end;

procedure TDamageDetailsForm.SetupThirdPartyEditing(Edit: Boolean);
begin
  SetEnabledOnControlAndChildren(EditThirdPartyPanel, Edit);
  ThirdPartyAddAction.Enabled := PermissionsDM.CanI(RightLitigationEdit);
end;

procedure TDamageDetailsForm.ThirdPartyAfterOpen(DataSet: TDataSet);
const
  ThirdPartyRequired: array[0..8] of string =
    ('claimant', 'uq_notified_date', 'address1', 'city', 'state', 'phone',
    'claim_desc', 'demand', 'claim_status');
begin
  SetRequiredFields(DataSet, ThirdPartyRequired);
  DM.SetDateFieldGetTextEvents(DataSet);
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('state'), 'state');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('claim_status'), 'claimstat');
end;

procedure TDamageDetailsForm.LitigationAfterOpen(DataSet: TDataSet);
const
  LitigationRequired: array[0..4] of string =
    ('plaintiff', 'defendant', 'file_number', 'demand', 'claim_status');
begin
  SetRequiredFields(DataSet, LitigationRequired);
  DM.SetDateFieldGetTextEvents(DataSet);
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('venue_state'), 'state');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('attorney'), 'atty');
  DM.SetRefFieldGetSetTextEvents(DataSet.FieldByName('claim_status'), 'claimstat');
end;

procedure TDamageDetailsForm.DbComboRefresh(DS: TDataSet);
begin
  // Workaround that Erik discovered for broken DBComboBox:
  DS.DisableControls;
  DS.EnableControls;
end;

procedure TDamageDetailsForm.SetLitigationInsertMode(const Value: Boolean);
begin
  FLitigationInsertMode := Value;
  LitigationPageControl.Visible := False;
  if LitigationInsertMode then
    ShowPageControlAsSingleTab(LitigationNotifyTab)
  else
    ShowPageControlAsMultiTab(LitigationPageControl);
  LitigationPageControl.Visible := True;
end;

procedure TDamageDetailsForm.SetThirdPartyInsertMode(const Value: Boolean);
begin
  FThirdPartyInsertMode := Value;
  ThirdPartyPageControl.Visible := False;
  if ThirdPartyInsertMode then
    ShowPageControlAsSingleTab(ThirdPartyNotifyTab)
  else
    ShowPageControlAsMultiTab(ThirdPartyPageControl);
  ThirdPartyPageControl.Visible := True;
end;

procedure TDamageDetailsForm.ThirdPartyPageControlChange(Sender: TObject);
begin
  if ThirdPartyPageControl.ActivePage = ThirdPartyInvoiceTab then
    ThirdPartyInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, FThirdPartyID, 0);
  ThirdPartyNotesFrame.Initialize(qmft3rdParty, FDamageID, FThirdPartyID);
end;

procedure TDamageDetailsForm.LitigationPageControlChange(Sender: TObject);
begin
  if LitigationPageControl.ActivePage = LitigationInvoiceTab then
    LitigationInvoiceFrame.GoToDamageInvoice(Invoices, FDamageID, 0, FLitigationID);
  LitigationNotesFrame.Initialize(qmftLitigation, FDamageID, FLitigationID);
end;

function TDamageDetailsForm.GetCurrentThirdPartyID: Integer;
begin
  if ThirdParty.FieldByName('third_party_id').IsNull then
    Result := 0
  else
    Result := ThirdParty.FieldByName('third_party_id').AsInteger;
end;


function TDamageDetailsForm.GetDamageCustomForm: integer;
var
  lFormID: integer;
  lState: string;
begin
  Result  := 0;
  fCustDamageRec.HasCustomQuestions := False;
  CustomQuestionstab.TabVisible := False;

  fCustDamageRec.FormData:= ACustomFormDM.GetFormIDByRef(fCustDamageRec.RefID);
  lFormID := fCustDamageRec.FormData.FormID;
  lState := fCustDamageRec.FormData.LinkModifier;


  if lFormID > 0 then begin
    fCustDamageRec.HasCustomQuestions := True;
    Result := lFormID;
    fCustGroupContainer := ACustomForm.TGroupContainer.Create(lFormID, '',
                                                              CustomQuestionsTab, DM.EmpID,
                                                              FDamageID, fCustDamageRec.FormData);
    fCustGroupContainer.AnswersForeignID := FDamageID; //IS this the right one? Do we need this now
//    fCustGroupContainer.DisplayNoteforOtherExistingAnswers;
    if UtilityCoDamaged.Text <> '' then
      CustomQuestionsTab.Caption := TabCharsOnly(UtilityCoDamaged.Text)
    else
      CustomQuestionsTab.Caption := 'Utility Co. Questions';

    CustomQuestionsTab.TabVisible := True;
  end;
end;


function TDamageDetailsForm.GetCurrentLitigationID: Integer;
begin
  if Litigation.FieldByName('litigation_id').IsNull then
    Result := 0
  else
    Result := Litigation.FieldByName('litigation_id').AsInteger;
end;

procedure TDamageDetailsForm.PageControlChange(Sender: TObject);
var
  ReportMode: string;
begin
  case PageControl.ActivePageIndex of
    0: begin
      DamagePageControl.ActivePageIndex := 0;
      ReportMode := DamageReportOnDamage;

    end;
    1: begin
      ThirdPartyPageControl.ActivePageIndex := 0;
      ReportMode := DamageReportOnThirdParty;
    end;
    2: begin
      LitigationPageControl.ActivePageIndex := 0;
      ReportMode := DamageReportOnLitigation;
    end;
  end;
  TDamageDetailsReportForm.SetLastDamageParams(FUQDamageID, ReportMode);
end;

procedure TDamageDetailsForm.UpdateInvestigator;
begin
  if (not Damage.Active) or (Damage.FieldByName('investigator_id').AsInteger <= 0) then
    InvestigatorNameLabel.Caption := 'Select Investigator... -->'
  else
    InvestigatorNameLabel.Caption := EmployeeDM.GetEmployeeShortName(Damage.FieldByName('investigator_id').AsInteger);
end;

procedure TDamageDetailsForm.BillingGroupEnter(Sender: TObject);
begin
  Assert(DamageBill.Active);
  if DamageBill.IsEmpty then begin
    EditDataSet(Damage);
    DamageBill.Insert;
    DamageBill.FieldByName('damage_id').AsInteger := Damage.FieldByName('damage_id').AsInteger;
    DamageBill.FieldByName('billing_period_date').AsDateTime := Date;
  end;
end;

procedure TDamageDetailsForm.DamageBillBeforeEdit(DataSet: TDataSet);
begin
  EditDataSet(Damage);
end;

procedure TDamageDetailsForm.ValidateLitigationFields;
begin
  VerifyLitigationDates;
end;

procedure TDamageDetailsForm.VerifyDamageDates;
const
  DateFields: array[0..5] of string = ('damage_date','date_mailed','date_faxed',
    'investigator_arrival','investigator_departure', 'investigator_est_damage_time');
var
  i: Integer;
begin
  for i := 0 to Length(DateFields)-1 do
    VerifyMinimumSQLServerDate(Damage.FieldByName(DateFields[i]));
end;

procedure TDamageDetailsForm.VerifyDamageBillDates;
begin
  VerifyMinimumSQLServerDate(DamageBill.FieldByName('billing_period_date'));
end;

procedure TDamageDetailsForm.VerifyLitigationDates;
begin
  VerifyMinimumSQLServerDate(Litigation.FieldByName('service_date'));
end;

procedure TDamageDetailsForm.ChooseInvButtonClick(Sender: TObject);
var
  SelectedID: Variant;
begin
  if TEmployeeSearchForm.Execute(esAll, SelectedID) then begin
    EditDataSet(Damage);
    Damage.FieldByName('investigator_id').AsInteger := SelectedID;
    UpdateInvestigator;
  end;
end;

procedure TDamageDetailsForm.PrepareAttachmentsAddin;
begin
  if not Assigned(FAddinManager) then
    FAddinManager := TAttachmentsAddinManager.Create(DM, DamageAttachmentsFrame.AttachmentAddinButton,
      ManageAttachmentsButtonClick);

  Assert(Assigned(FAddinManager), 'Attachment addin manager is not assigned');
  FAddinManager.PrepareDamageAttachmentsAddin(HaveDamage);
end;

procedure TDamageDetailsForm.ManageAttachmentsButtonClick(Sender: TObject);
var
  Cursor: IInterface;
begin
  if not HaveDamage then
    raise EOdDataEntryError.Create('Please select a damage.');

  Cursor := ShowHourGlass;
  if FAddinManager.ExecuteForDamage(Damage.FieldByName('damage_id').AsInteger, DamageAttachmentsFrame.SelectedAttachmentID, DamageAttachmentsFrame.SelectedForeignType, DamageAttachmentsFrame.SelectedAttachmentFilename) then
    DamageAttachmentsFrame.RefreshNow;
end;

procedure TDamageDetailsForm.LitSubTypeComboChange(Sender: TObject);
begin
  inherited;
  LitigationNotesFrame.NoteSubType := FLitNoteSubTypeList.GetCode(LitSubTypeCombo.Text);
  LitigationNotesFrame.SubTypeText := LitSubTypeCombo.Text;
end;

procedure TDamageDetailsForm.DamageSubTypeComboChange(Sender: TObject);
begin
  inherited;
  DamageNotesFrame.NoteSubType := FDamageNoteSubTypeList.GetCode(DamageSubTypeCombo.Text);
  DamageNotesFrame.SubTypeText := DamageSubTypeCombo.Text;
end;

procedure TDamageDetailsForm.TPSubTypeComboChange(Sender: TObject);
begin
  inherited;
  ThirdPartyNotesFrame.NoteSubType := FTPNoteSubTypeList.GetCode(TPSubTypeCombo.Text);
  ThirdPartyNotesFrame.SubTypeText := TPSubTypeCombo.Text;
end;

function TDamageDetailsForm.HaveDamage: Boolean;
begin
  Result := Assigned(DamageSource.DataSet) and Damage.Active and (not Damage.IsEmpty);
end;

procedure TDamageDetailsForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FCallCenterList);
  FreeAndNil(FAddinManager);
  FreeAndNil(FRequiredDocList);
  inherited;
end;

procedure TDamageDetailsForm.LookupLocatorButtonClick(Sender: TObject);
var
  SelectedID: Variant;
  RootMgr: Integer;
begin
  RootMgr := StrToIntDef(PermissionsDM.GetLimitationDef(RightDamagesAssignLocator, IntToStr(DM.EmpID)), DM.EmpID);
  if TEmployeeSearchForm.Execute(esAllEmployeesUnderManager, SelectedID, '', RootMgr) then begin
    EditDataSet(Damage);
    Damage.FieldByName('locator_id').AsInteger := SelectedID;
    UpdateLocator;
  end;
end;

procedure TDamageDetailsForm.UpdateLocator;
begin
  if (not Damage.Active) or (Damage.FieldByName('locator_id').AsInteger <= 0) then begin
    if LookupLocatorButton.Visible then
      LocatorNameLabel.Caption := 'Select Locator... -->'
    else
      LocatorNameLabel.Caption := 'unassigned';
  end
  else
    LocatorNameLabel.Caption := EmployeeDM.GetEmployeeShortName(Damage.FieldByName('locator_id').AsInteger);
  ClearLocatorButton.Enabled := Damage.Active and (Damage.FieldByName('locator_id').AsInteger > 0);
end;

procedure TDamageDetailsForm.ClearCustDamageRec;
begin
   fCustDamageRec.RefID := 0;
   fCustDamageRec.UtilityCoDamaged := '';
   fCustDamageRec.State := '';
   fCustDamageRec.FormID := 0;
end;

procedure TDamageDetailsForm.ClearLocatorButtonClick(Sender: TObject);
begin
  EditDataSet(Damage);
  Damage.FieldByName('locator_id').Clear;
  UpdateLocator;
end;

procedure TDamageDetailsForm.ClearNotesSubType;
begin
  TPSubTypeCombo.ClearSelection;
  DamageSubTypeCombo.ClearSelection;
  LitSubTypeCombo.ClearSelection;
  ThirdPartyNotesFrame.NoteSubType := '';
  ThirdPartyNotesFrame.SubTypeText := '';
  LitigationNotesFrame.NoteSubType := '';
  LitigationNotesFrame.SubTypeText := '';
  DamageNotesFrame.NoteSubType := '';
  DamageNotesFrame.SubTypeText := '';
end;

procedure TDamageDetailsForm.AfterAddDamageEstimate(Sender: TObject);
begin
  RefreshRequiredDamageDocs;
  EditDataSet(Damage);
end;

procedure TDamageDetailsForm.AfterAddLitigationEstimate(Sender: TObject);
begin
  EditDataSet(Litigation);
end;

procedure TDamageDetailsForm.AfterAddThirdPartyEstimate(Sender: TObject);
begin
  EditDataSet(ThirdParty);
end;

procedure TDamageDetailsForm.DamageAttachmentChange(Sender: TObject);
begin
  RefreshRequiredDamageDocs;
  EditDataSet(Damage);
end;

procedure TDamageDetailsForm.DamageNoteChange(Sender: TObject);
begin
  EditDataSet(Damage);
end;

procedure TDamageDetailsForm.LitigationAttachmentChange(Sender: TObject);
begin
  EditDataSet(Litigation);
end;

procedure TDamageDetailsForm.ThirdPartyAttachmentChange(Sender: TObject);
begin
  EditDataSet(ThirdParty);
end;

procedure TDamageDetailsForm.RefreshRequiredDamageDocs;
const
  AttachedDocsSQL = 'select rd.required_doc_id, count(*) as attached_cnt ' +
    'from required_document rd ' +
    'inner join required_document_type rdt on (rd.required_doc_id = rdt.required_doc_id) ' +
    'inner join attachment a on (a.doc_type = rdt.doc_type) ' +
    'where ((a.foreign_id = :damage_id and a.foreign_type = :foreign_type) ' +
    '%s and a.active) ' + // this will have the LinkedTicketClause if needed
    'and rd.foreign_type = :foreign_type and rd.active and rd.required_doc_id in (%s) ' +
    'and rdt.active ' +
    'group by rd.required_doc_id, rd.required_doc_id';
  LinkedTicketClause = 'or (a.foreign_id = :ticket_id and a.foreign_type = :linked_foreign_type) ';
var
  RequiredDocIDs: string;
begin
  if not HaveDamage then
    Exit;

  LDamageDM.GetRequiredDamageDocumentList(FRequiredDocList,
    (Damage.FieldByName('locate_requested').AsString='Y'),
    DamageEstimateFrame.CurrentEstimateAmount);

  RequiredDocIDs := DM.GetIDListFromObjects(FRequiredDocList);
  if DamageHasLinkedTicket then begin
    AttachedDocsQuery.SQL.Text := Format(AttachedDocsSql, [LinkedTicketClause, RequiredDocIDs]);
    AttachedDocsQuery.Params.ParamValues['ticket_id'] := Damage.FieldByName('ticket_id').AsInteger;
    AttachedDocsQuery.Params.ParamValues['linked_foreign_type'] := qmftTicket;
  end else
    AttachedDocsQuery.SQL.Text := Format(AttachedDocsSql, ['', RequiredDocIDs]);
  AttachedDocsQuery.Params.ParamValues['foreign_type'] := qmftDamage;
  AttachedDocsQuery.Params.ParamValues['damage_id'] := FDamageId;
  AttachedDocsQuery.Open;
end;

procedure TDamageDetailsForm.LoadDamageDocuments;
const
  DocumentSQL = 'select rd.*, %d as foreign_id, d.doc_id, d.comment, ' +
    'd.added_by_id, d.added_date from required_document rd ' +
    'left join document d on (rd.required_doc_id = d.required_doc_id ' +
    'and rd.foreign_type = d.foreign_type and foreign_id = %d and d.active) ' +
    'where rd.foreign_type = %d and rd.active';
begin
  if (not HaveDamage) or (FRequiredDocList.Count = 0) then
    Exit;

  DocumentQuery.SQL.Text := Format(DocumentSQL, [FDamageID, FDamageID, qmftDamage]);
  DocumentQuery.OnFilterRecord := DocumentQueryFilterRecord;
  DocumentQuery.Filtered := True;
  AddCalculatedField('required', DocumentQuery, TBooleanField, 0);
  AddcalculatedField('attached_cnt', DocumentQuery, TIntegerField, 0);
  DocumentQuery.Open;
  SetupDocumentGrid(DocumentGrid, False);
end;

function TDamageDetailsForm.CheckIfDamageCanBeCompleted(
  var Error: string): Boolean;
begin
  if IsEmpty(InvestigatorNarrative.Text) then begin
    Result := False;
    Error := 'You must enter an Investigator Narrative before completing this investigation.';
    ChangeTabForError(DamagePageControl, InvestigatorTab);
    Exit;
  end;

  if StringInArray(Damage.FieldByName('locate_requested').Value, ['Y', 'N']) then
    Result := ValidateRequiredDocuments(Error)
  else begin
    Result := False;
    Error := 'Locate Requested must be Yes or No to complete a damage claim investigation.';
    ChangeTabForError(DamagePageControl, DamageTab);
    Exit;
  end;
end;

function TDamageDetailsForm.ValidateRequiredDocuments(var Error: string): Boolean;
const
  MissingAttachmentOrComment = 'Enter a comment for each required document that is not attached to this damage.';
begin
  DocumentQuery.DisableControls;
  Result := True;
  try
    DocumentQuery.First;
    while not DocumentQuery.Eof do begin
      if DocumentQuery.FieldByName('required').AsBoolean and
        (DocumentQuery.FieldByName('attached_cnt').AsInteger = 0) and
        IsEmpty(DocumentQuery.FieldByName('comment').AsString) then begin
        Result := False;
        Break;
      end;
      DocumentQuery.Next;
    end;
  finally
    DocumentQuery.EnableControls;
    FocusFirstGridRow(DocumentGrid);
  end;
  if not Result then begin
    Error := MissingAttachmentOrComment;
    ChangeTabForError(DamagePageControl, RequiredDocumentsTab);
  end;
end;

procedure TDamageDetailsForm.SaveDamageDocuments;

  function AnyFieldsDiffer: Boolean;
  begin
    Result := (DocumentQuery.FieldByName('comment').AsString <> Document.FieldByName('comment').AsString);
  end;

var
  RequiredDocID: Integer;
  AttachedCnt: Integer;
  Required: Boolean;
begin
  // DBISAM does not support updatable joins, so manually apply changes to the table.
  PostDataSet(DocumentQuery);
  DocumentQuery.DisableControls;
  try
    Document.Open;
    DocumentQuery.First;
    while not DocumentQuery.EOF do begin
      RequiredDocID := DocumentQuery.FieldByName('required_doc_id').AsInteger;
      Required := DocumentQuery.FieldByName('required').AsBoolean;
      AttachedCnt := DocumentQuery.FieldByName('attached_cnt').AsInteger;
      if Document.Locate('foreign_type;foreign_id;required_doc_id;active',
        VarArrayOf([qmftDamage, FDamageID, RequiredDocID, True]), []) then begin
        if Required and AnyFieldsDiffer then begin
          Document.Edit;
          Document.FieldByName('comment').Value := DocumentQuery.FieldByName('comment').Value;
          Document.Post;
        end else if (not Required) or (AttachedCnt > 0) then begin
          Document.Edit;
          Document.FieldByName('active').Value := False;
          Document.Post;
        end;
      end else if Required and (AttachedCnt < 1) then begin
        Document.Append;
        Document.FieldByName('foreign_type').AsInteger := DocumentQuery.FieldByName('foreign_type').AsInteger;
        Document.FieldByName('foreign_id').AsInteger := DocumentQuery.FieldByName('foreign_id').AsInteger;
        Document.FieldByName('required_doc_id').AsInteger := RequiredDocID;
        Document.FieldByName('added_by_id').AsInteger := DM.EmpID;
        Document.FieldByName('added_date').AsDateTime := Now;
        Document.FieldByName('comment').Value := DocumentQuery.FieldByName('comment').Value;
        Document.FieldByName('active').AsBoolean := True;
        Document.Post;
      end;
      DocumentQuery.Next;
    end;

    Document.Close;
  finally
    DocumentQuery.EnableControls;
    FocusFirstGridRow(DocumentGrid);
  end;
end;

procedure TDamageDetailsForm.DamageSourceStateChange(Sender: TObject);
begin
  if DamageSource.State = dsEdit then
    FDamageNarrativeText := DamageSource.DataSet.FieldByName('investigator_narrative').AsString;
end;

procedure TDamageDetailsForm.DamageSourceUpdateData(Sender: TObject);
begin
  if NotEmpty(FDamageNarrativeText) and
    (DamageSource.DataSet.FieldByName('investigator_narrative').AsString <> FDamageNarrativeText) then begin
    LDamageDM.ArchiveDamageInvestigatorNarrative(FDamageID, FDamageNarrativeText);
    FDamageNarrativeText := '';
  end;
end;



function TDamageDetailsForm.ApprovingDamage: Boolean;
begin
  Result :=  PermissionsDM.CanI(RightDamagesApprove) and DamageTypeIsPendingApproval;
end;

function TDamageDetailsForm.DamageTypeIsPendingApproval: Boolean;
{This function is introduced with Mantis 2401; DamageTypeCompleted is in the
process of being replaced by DamageTypePendingApproval.  We are supporting both
in the code until the database and clients can all be updated and then
DamageTypeCompleted can be removed.}
begin
  Result := StringInArray(Damage.FieldByName('damage_type').AsString, [DamageTypePendingApproval,DamageTypeCompleted]);
end;

procedure TDamageDetailsForm.ReasonChangedSelectionChange(Sender: TObject);
var
  ReasonChangedRefID : Integer;
begin
  EditDataSet(Damage);
  if ReasonChanged.ItemIndex <> -1 then
    ReasonChangedRefID := GetComboObjectInteger(ReasonChanged)
  else
    ReasonChangedRefID := 0;
  if ReasonChangedRefID = 0 then
    Damage.FieldByName('reason_changed').Clear
  else
    Damage.FieldByName('reason_changed').AsInteger := ReasonChangedRefID;
end;

function TDamageDetailsForm.DamageHasLinkedTicket: Boolean;
begin
  Result := not Damage.FieldByName('ticket_id').IsNull;
end;

function TDamageDetailsForm.InvestigationIsOpen: Boolean;
begin
  Result := StringInArray(Damage.FieldByName('damage_type').AsString,
    [DamageTypeIncoming, DamageTypePending]) or DamageTypeIsPendingApproval;
end;

procedure TDamageDetailsForm.LocationExit(Sender: TObject);
begin
  inherited;
  //if Profit Center has ever been manually set, we never auto-determine Profit Center again.
  if Damage.FieldByName('user_changed_pc').AsBoolean = False then begin
    //Only autogenerate profit center if current location values vary from saved location values, or the PC is empty
    if not (SameText(Damage.FieldByName('state').AsString, FState) and
        SameText(Damage.FieldByName('county').AsString, FCounty) and
        SameText(Damage.FieldByName('city').AsString, FCity)) or
        IsEmpty(Damage.FieldByName('profit_center').AsString) then
    AutoGenerateProfitCenter;
  end;
end;

procedure TDamageDetailsForm.ProfitCenterChange(Sender: TObject);
begin
  inherited;
  EditDataSet(Damage);
  //During damage creation the user gets a "free pass" no matter how the PC is set;
  //i.e. user_changed_pc is always 0 for a newly inserted damage.
  if not InsertMode then
    if ProfitCenter.Text <> FGeneratedProfitCenter then
      Damage.FieldByName('user_changed_pc').AsBoolean := True;
end;

procedure TDamageDetailsForm.AutoGenerateProfitCenter;
begin
  EditDataSet(Damage);
  FGeneratedProfitCenter := AutoDetermineDamageProfitCenter(Damage.FieldByName('state').AsString,
    Damage.FieldByName('county').AsString, Damage.FieldByName('city').AsString, LDamageDM.DamageProfitCenterRules);
  if not (IsEmpty(FGeneratedProfitCenter)) then //otherwise keep original PC
    Damage.FieldByName('profit_center').AsString := FGeneratedProfitCenter;
end;

function TDamageDetailsForm.DamageEditAllowed(const Arrived: Boolean): Boolean;
begin
  if Arrived then
    Result := Arrived
  else
    Result := PermissionsDM.CanI(RightDamagesEditWithoutArriving);
end;

procedure TDamageDetailsForm.SetArriveButtonEnabled(const Enabled: Boolean);
begin
  ArriveButton.Enabled := Enabled;

  if ArriveButton.Enabled then begin
    ArriveButtonPanel.ParentBackground := False;
    ArriveButtonPanel.Visible := True;
    ArriveButtonPanel.Color := clRed;
  end else
    ArriveButtonPanel.ParentBackground := True;
end;

procedure TDamageDetailsForm.ArriveButtonMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Point: TPoint;
begin
  inherited;
  Point.X := ArriveButton.Left + ArriveButtonPanel.Left;
  Point.Y := ArriveButton.Top + ArriveButton.Height + ArriveButtonPanel.Top;
  Point := ClientToScreen(Point);
  ArrivePopupMenu.Popup(Point.X, Point.Y);
end;

procedure TDamageDetailsForm.ArriveOnSiteActionExecute(Sender: TObject);
begin
  inherited;
  ArriveOnDamage(ONSITE);
end;

procedure TDamageDetailsForm.ArriveOffSiteActionExecute(Sender: TObject);
begin
  inherited;
  ArriveOnDamage(OFFSITE);
end;

procedure TDamageDetailsForm.ArriveOnDamage(const LocationStatus: string);
var
  Resp: Integer;
  Cursor: IInterface;
begin
  if not LDamageDM.IsDamageAssignedToLocator(FDamageID, DM.UQState.EmpID) then begin
    Resp := MessageDlgFmt('You are not the investigator assigned to this damage. Are you sure you want to arrive?', [],
      mtConfirmation, [mbYes, mbNo], mrNo);
    if Resp <> mrYes then
      Abort;
  end;
  Cursor := ShowHourGlass;

  DM.CreateArrival(FDamageID, 'CLOCK', DM.EmpID, Now, atDamage, LocationStatus);
  if FDamageID > 0 then
    DM.SaveArrivalsToServer(FDamageID, atDamage);

  SetEnabledOnDamageControls(DamageEditAllowed(True));
  SetArriveButtonEnabled(False);
  Damage.Edit;
end;

procedure TDamageDetailsForm.ChangeTabForError(PageControl: TPageControl; TabSheet: TTabSheet);
begin
  FChangingTabsForError := True;
  try
    PageControl.ActivePage := TabSheet;
  finally
    FChangingTabsForError := False;
  end;
end;

procedure TDamageDetailsForm.RaiseErrorOnTab(const ErrorMessage: string; const TabSheet: TTabSheet);
begin
  ChangeTabForError(DamagePageControl, TabSheet);
  raise EOdDataEntryError.Create(ErrorMessage);
end;

procedure TDamageDetailsForm.ShowMessageForTabControl(DisplayMsg: String;
  PageControl: TPageControl; TabSheet: TTabSheet; WinControl: TWinControl);
begin
  ChangeTabForError(PageControl, TabSheet);
  if WinControl.CanFocus then
    WinControl.SetFocus;
  ShowMessage(DisplayMsg);
end;

procedure TDamageDetailsForm.ResponsibilityCodeChange(Sender: TObject);
begin
  inherited;
  FResponsibilityChanged := ResponsibilityCodeChangedFromSavedValue;
end;

function TDamageDetailsForm.ResponsibilityCodeChangedFromSavedValue: Boolean;
//Compare current selected values against previously saved Resp Code values
begin
  Result := ( (DM.LastSavedExcRespCode <> DM.GetRefCodeForDisplay('exres', ExcRespCode.Text)) or
    (DM.LastSavedUQRespCode <> DM.GetRefCodeForDisplay('uqres', UQRespCode.Text)) or
    (DM.LastSavedSpecialRespCode <> DM.GetRefCodeForDisplay('utres', SpecialRespCode.Text)) );
end;

function TDamageDetailsForm.ResponsibilityPreviouslySet: Boolean;
var
  HistoryCount: Integer;
//Determine if their was a responsibility code previously set
//Looks to see if there is a history entry that has one of UQRespCode,ExcRespCode or SpecialRespCode
begin
  HistoryCount := LDamageDM.GetCurrentDamageResponsibilityCount(FDamageID);
  Result := HistoryCount > 0;
end;

function TDamageDetailsForm.GetAttachedDocCount(const RequiredDocID: Integer): Integer;
begin
  Result := 0;
  if AttachedDocsQuery.Locate('required_doc_id', RequiredDocID, []) then
    Result := AttachedDocsQuery.FieldByName('attached_cnt').AsInteger;
end;

procedure TDamageDetailsForm.DocumentQueryAfterEdit(DataSet: TDataSet);
begin
  inherited;
  EditDataSet(LDamageDM.Damage);
end;

procedure TDamageDetailsForm.DocumentQueryCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('required').AsBoolean := (FRequiredDocList.IndexOfObject(
    TObject(DataSet.FieldByName('required_doc_id').AsInteger)) > -1);
  DataSet.FieldByName('attached_cnt').AsInteger := GetAttachedDocCount(DataSet.FieldByName('required_doc_id').AsInteger);
end;

procedure TDamageDetailsForm.DocumentQueryFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := FRequiredDocList.IndexOfObject(TObject(DataSet.FieldByName('required_doc_id').AsInteger)) > -1;
end;

procedure TDamageDetailsForm.ColDocCommentGetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  AProperties.ReadOnly := (CurrentDocumentRowAttachCnt(ARecord) > 0);
end;

function TDamageDetailsForm.CurrentDocumentRowAttachCnt(ARecord: TcxCustomGridRecord): Integer;
begin
  Result := VarToInt(ARecord.Values[ColDocAttachedCnt.Index], 0);
end;

procedure TDamageDetailsForm.CustomFormUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  SaveButton.Enabled := Damage.State in dsEditModes;
end;

procedure TDamageDetailsForm.DocumentDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  AttachCnt: Integer;
  Comment: string;
begin
  // highlight rows needing a comment.
  if AViewInfo.Editing or AViewInfo.Selected then
    Exit;

  Comment := VarToString(AViewInfo.GridRecord.Values[ColDocComment.Index], '');
  AttachCnt := CurrentDocumentRowAttachCnt(AViewInfo.GridRecord);

  if IsEmpty(Comment) and (AttachCnt < 1) then
    ACanvas.Font.Color := clRed
  else if (AttachCnt > 0) and (AViewInfo.Item = ColDocComment) then begin
    ACanvas.Font.Color := clGrayText;
    ACanvas.Font.Size := 8;
  end else
    ACanvas.Font.Color := clDefault;
end;

procedure TDamageDetailsForm.ColDocCommentGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: String);
begin
  if CurrentDocumentRowAttachCnt(ARecord) > 0 then
    AText := '- no comment required -';
end;

procedure TDamageDetailsForm.SetupDocumentGrid(DocGrid: TcxGrid; HideAttached: Boolean);
var
  View: TcxGridDBTableView;
begin
  Assert(Assigned(DocGrid));
  Assert(Assigned(DocGrid.ActiveView));
  Assert(DocGrid.ActiveView is TcxGridDBTableView);

  DocGrid.BeginUpdate;
  try
    View := DocGrid.ActiveView as TcxGridDBTableView;
    View.DataController.Filter.Clear;
    if HideAttached then begin
      // only show required documents that have no attachment
      View.DataController.Filter.AddItem(nil, View.GetColumnByFieldName('required'), foEqual, True, 'True');
      View.DataController.Filter.AddItem(nil, View.GetColumnByFieldName('attached_cnt'), foEqual, 0, '0');
      View.DataController.Filter.Active := True;
    end;
    SortGridByFieldNames(DocGrid, ['attached_cnt', 'name'], [soAscending, soAscending]);
    FocusFirstGridRow(DocGrid);
  finally
    DocGrid.EndUpdate;
  end;
end;

procedure TDamageDetailsForm.ShowOnlyMissingDocsCheckBoxClick(Sender: TObject);
begin
  SetupDocumentGrid(DocumentGrid, ShowOnlyMissingDocsCheckBox.Checked);
end;
end.
