unit LocatesCompletedSummaryReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst, OdRangeSelect;

type
  TLocatesCompletedSummaryReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    WorkDatesLabel: TLabel;
    WorkDateRange: TOdRangeSelectFrame;
    EmployeeTypeList: TCheckListBox;
    EmpTypesLabel: TLabel;
    SelectAllButton: TButton;
    SelectNoneButton: TButton;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure SelectNoneButtonClick(Sender: TObject);
  private
    function GetSelectedEmployeeTypes: string;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu, OdVclUtils, OdExceptions, LocalEmployeeDMu;

procedure TLocatesCompletedSummaryReportForm.InitReportControls;
begin
  inherited;
  EmployeeDM.EmpTypeList(EmployeeTypeList.Items);
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
  WorkDateRange.SelectDateRange(rsLastWeek);
end;

procedure TLocatesCompletedSummaryReportForm.ValidateParams;
begin
  inherited;
  SetReportID('LocatesCompletedSummary');
  SetParamDate('StartDate', WorkDateRange.FromDate, True);
  SetParamDate('EndDate', WorkDateRange.ToDate, True);
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParam('EmployeeTypes', GetSelectedEmployeeTypes);
end;

procedure TLocatesCompletedSummaryReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(EmployeeTypeList, True);
end;

procedure TLocatesCompletedSummaryReportForm.SelectNoneButtonClick(Sender: TObject);
begin
  SetCheckListBox(EmployeeTypeList, False);
end;

function TLocatesCompletedSummaryReportForm.GetSelectedEmployeeTypes: string;
var
  I: Integer;
  CheckedCount: Integer;
begin
  Result := '';
  CheckedCount := 0;

  for I := 0 to EmployeeTypeList.Count - 1 do begin
    if EmployeeTypeList.Checked[I] then begin
      Inc(CheckedCount);
      Result := Result + IntToStr(Integer(EmployeeTypeList.Items.Objects[I])) + ',';
    end;
  end;

  if CheckedCount = 0 then
    raise EOdDataEntryError.Create('Please select at least one employee type to report on.')
  else if EmployeeTypeList.Count = CheckedCount then
    Result := '*'
  else
    Delete(Result, Length(Result), 1);
end;

end.
