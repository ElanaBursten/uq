inherited DamagesPerLocatesReportForm: TDamagesPerLocatesReportForm
  Caption = 'Damages Per Locates Report'
  ClientHeight = 252
  ClientWidth = 395
  PixelsPerInch = 96
  TextHeight = 13
  object ProfitCenterLabel: TLabel [0]
    Left = 0
    Top = 88
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Profit Center:'
  end
  object LiabilityLabel: TLabel [1]
    Left = 0
    Top = 166
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Liability:'
  end
  object Label1: TLabel [2]
    Left = 0
    Top = 8
    Width = 100
    Height = 13
    Caption = 'Activity Date Range:'
  end
  object LocateRatioLabel: TLabel [3]
    Left = 0
    Top = 193
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Locate Ratio:'
  end
  object UtilityCoLabel: TLabel [4]
    Left = 0
    Top = 113
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Client:'
  end
  object FacilityTypeLabel: TLabel [5]
    Left = 0
    Top = 139
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Facility Type:'
  end
  inline RangeSelect: TOdRangeSelectFrame [6]
    Left = 35
    Top = 24
    Width = 231
    Height = 58
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 5
      Alignment = taRightJustify
    end
    inherited ToLabel: TLabel
      Left = 125
    end
    inherited DatesLabel: TLabel
      Left = 5
      Top = 35
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 41
      Top = 32
      Width = 185
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 41
      Top = 7
      Width = 79
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 145
      Top = 7
      Width = 80
    end
  end
  object ProfitCenter: TComboBox
    Left = 76
    Top = 85
    Width = 112
    Height = 21
    CharCase = ecUpperCase
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
    OnChange = ProfitCenterChange
  end
  object Liability: TComboBox
    Left = 76
    Top = 163
    Width = 231
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 4
  end
  object LocateRatio: TEdit
    Left = 76
    Top = 190
    Width = 57
    Height = 21
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 5
    Text = '1000'
  end
  object Client: TComboBox
    Left = 76
    Top = 110
    Width = 181
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 2
  end
  object FacilityType: TComboBox
    Left = 76
    Top = 136
    Width = 181
    Height = 21
    Style = csDropDownList
    DropDownCount = 14
    ItemHeight = 13
    TabOrder = 3
  end
  object ShowLocatorDetail: TCheckBox
    Left = 181
    Top = 192
    Width = 122
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Show Locator Detail'
    TabOrder = 6
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 331
    Top = 192
  end
end
