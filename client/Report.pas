unit Report;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OdEmbeddable, StdCtrls, ExtCtrls, ComCtrls, OdReportBase, DB, DBISAMTb, Contnrs,
  DBGrids, Grids;

type
  ReportClass = class of TReportBaseForm;

  TReportForm = class(TEmbeddableForm)
    ReportStatusPanel: TPanel;
    UpperPanel: TPanel;
    ReportSelectionPanel: TPanel;
    ReportListLabel: TLabel;
    ReportList: TListBox;
    Splitter1: TSplitter;
    ReportPanel: TPanel;
    RepConfigPanel: TPanel;
    ReportButtonPanel: TPanel;
    ReportNameLabel: TLabel;
    PreviewBtn: TButton;
    ExportButton: TButton;
    CopyConfigButton: TButton;
    ReportFooterPanel: TPanel;
    ReportTimeLabel: TLabel;
    ReportQueueGrid: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    ReportQueueDataSet: TDBISAMTable;
    ReportQueueDS: TDataSource;
    StatusCheckTimer: TTimer;
    ReportQueueDataSetrequest_id: TStringField;
    ReportQueueDataSetreport_name: TStringField;
    ReportQueueDataSetrequest_date: TDateTimeField;
    ReportQueueDataSetstatus: TStringField;
    ReportQueueDataSetupdate_count: TIntegerField;
    ReportQueueDataSetnext_update: TDateTimeField;
    ReportQueueDataSetElapsed: TStringField;
    ReportQueueDataSetDisplayStatus: TStringField;
    procedure PreviewBtnClick(Sender: TObject);
    procedure ReportListClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ExportButtonClick(Sender: TObject);
    procedure CopyConfigButtonClick(Sender: TObject);
    procedure ReportQueueDataSetCalcFields(DataSet: TDataSet);
    procedure StatusCheckTimerTimer(Sender: TObject);
    procedure ReportQueueGridDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    RepForm: TReportBaseForm;
    FStartTime: TDateTime;
    procedure ShowRepForm;
    procedure InitReportControls;
    procedure LoadSelectedReportForm;
    procedure SetUpReportList;
    procedure StartTiming;
    procedure ShowElapsedTime;
  public
    procedure ShowReportName(ReportName: string);
    procedure ActivatingNow; override;
    procedure UpdateReportStatusDisplay;
  end;

procedure RegisterReportForm(Name: string; ImplClass: ReportClass; ReportID: string);

implementation

uses DMu, 
  SyncReport, LoadReport, AuditReport,
  DailyClientClosingReport, LateTicketReport,
  TicketsDueReport, NoConflictReport, HighProfileReport,
  AfterHoursReport, ProductivityReport, ClosedTicketDetailReport,
  InitialStatusReport, DamageLiabilityChangesReport, NewTicketDetailReport,
  LoadSheetReport, TicketsReceivedReport, TimesheetReport,
  DamagesByLocatorReport, DamageDetailsReport, TimesheetExceptionReport,
  OnTimeCompletionReport, OldOpenTicketsReport, NoResponseTicketsReport,
  LocateStatusActivityReport, FaxStatusReport, EmailStatusReport, EmailedHPTicketsReport,
  AssetReport, TimesheetAuditReport, DamageListReport, 
  TimesheetRuleCheckReport, TimesheetSummaryReport,
  ProductivityClientReport, BlastingTicketsReport,
  TimesheetExportsReport, StandardPayrollExceptionReport,
  AdHocPayrollExceptionReport, StatusGroupReport, PayrollSummaryReport,
  InvoiceIDExceptionReport, EmployeeReport, DailySalesReport,
  BillingExceptionReport, DamageIDExceptionReport, DamageDefaultEstimateReport,
  DamageInvoiceReport, OdDbUtils, BillableDamagesReport,
  DamageProductivityReport, DamagesPerLocatesReport, EmployeeActivityReport,
  QMConst, OdVclUtils, RestrictedUseExemptionReport, TimesheetEmpSummaryReport,
  ResponseLogReport, 
  HighLevelProductivityReport, EmployeeCovStatusReport, LocatesCompletedSummaryReport,
  EmployeesInTrainingReport, BreakExceptionReport, TicketActivityReport,
  EmployeeRightsReport, TimesheetRuleResponseReport, WorkOrderDetailReport,
  WorkOrderStatusActivityReport, ResponderStatusTranslationsReport, LocalPermissionsDMu,
  TicketsDueExport;

{$R *.DFM}

type
  TReportClassHolder = class
    RepName: string;
    RepClass: ReportClass;
    ReportID: string;
  end;

var
  Reports: TObjectList;

procedure RegisterReportForm(Name: string; ImplClass: ReportClass; ReportID: string);
var
  Holder: TReportClassHolder;
begin
  Assert(ReportID <> '');
  if not Assigned(Reports) then
    Reports := TObjectList.Create;

  Holder := TReportClassHolder.Create;
  Holder.RepName := Name;
  Holder.RepClass := ImplClass;
  Holder.ReportID := ReportID;
  Reports.Add(Holder);
end;

function ReportCompare(Item1, Item2: Pointer): Integer;
begin
  Result := CompareStr(TReportClassHolder(Item1).RepName,
                       TReportClassHolder(Item2).RepName);
end;

procedure TReportForm.SetUpReportList;
var
  I: Integer;
  Holder: TReportClassHolder;
  CanRunReports: Boolean;
begin
  Assert(Assigned(Reports));
  Reports.Sort(ReportCompare);
  ReportList.Clear;
  CanRunReports := PermissionsDM.CanI(RightReportScreen);
  for I := 0 to Reports.Count-1 do begin
    Holder := Reports[I] as TReportClassHolder;
    if PermissionsDM.CanI(Holder.ReportID) or CanRunReports then
      ReportList.AddItem(Holder.RepName, Holder);
  end;
end;

procedure TReportForm.LoadSelectedReportForm;
var
  Holder: TReportClassHolder;
  OldReport: TReportBaseForm;
begin
  OldReport := RepForm;

  Holder := ReportList.Items.Objects[ReportList.ItemIndex] as TReportClassHolder;
  ReportNameLabel.Caption := ReportList.Items[ReportList.ItemIndex];
  RepForm := CreateEmbeddedForm(Holder.RepClass, nil, RepConfigPanel) as TReportBaseForm;
  // I need to set the ReportID before initializing the controls in the report
  // so can look the limitation before this initialization
  RepForm.ReportID := Holder.ReportID;
  DM.CurrentReportID := RepForm.ReportID;
  RepForm.Init;

  if Assigned(OldReport) then
    OldReport.LeavingNow;

  ShowRepForm;

  FreeAndNil(OldReport);
end;

procedure TReportForm.ReportListClick(Sender: TObject);
begin
  inherited;
  LoadSelectedReportForm;
end;

procedure TReportForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(RepForm);
end;

procedure TReportForm.PreviewBtnClick(Sender: TObject);
begin
  if Assigned(RepForm) then begin
    DM.StopTimeImportTimer;
    StartTiming;
    RepForm.Preview;
    ShowElapsedTime;
    DM.StartTimeImportTimer;
  end;
end;

procedure TReportForm.ShowRepForm;
begin
  PreviewBtn.Enabled := RepForm.CanPreview;
  if RepForm.ReportID = 'TicketsDueExport' then //QMANTWO-784   sr
   PreviewBtn.Enabled := False;
  ExportButton.Enabled := RepForm.CanExport;
  CopyConfigButton.Enabled := True;
  RepForm.SetDropDownCounts(18);
  RepForm.SetGlyphs;
  RepForm.Show;
end;

procedure TReportForm.ExportButtonClick(Sender: TObject);
begin
  if Assigned(RepForm) then begin
    DM.StopTimeImportTimer;
    StartTiming;
    RepForm.ExportData;
    ShowElapsedTime;
    DM.StartTimeImportTimer;
  end;
end;

procedure TReportForm.InitReportControls;
begin
  PreviewBtn.Enabled := False;
  ExportButton.Enabled := False;
  CopyConfigButton.Enabled := False;

  ReportNameLabel.Caption := '----';

  CopyConfigButton.Visible := DM.UQState.DeveloperMode;

  SetUpReportList;
end;

procedure TReportForm.ShowReportName(ReportName: string);
var
  Index: Integer;
begin
  Index := ReportList.Items.IndexOf(ReportName);
  if Index < 0 then
    raise Exception.CreateFmt('Report %s not found', [ReportName]);
  ReportList.ItemIndex := Index;
  LoadSelectedReportForm;
end;

procedure TReportForm.CopyConfigButtonClick(Sender: TObject);
begin
  inherited;
  if Assigned(RepForm) then
    RepForm.ShowConfigString;
end;

procedure TReportForm.StartTiming;
begin
  FStartTime := Now;
end;

procedure TReportForm.ShowElapsedTime;
var
  Elapsed: TDateTime;
  ElapsedSec: single;
begin
  if DM.UQState.DeveloperMode then begin
    ReportFooterPanel.Visible := True;
    Elapsed := Now - FStartTime;
    ElapsedSec := Elapsed * 24 * 60 * 60;
    ReportTimeLabel.Caption := Format('%4.1f seconds', [ElapsedSec]);
  end;
end;

procedure TReportForm.ActivatingNow;
begin
  inherited;
  ReportQueueDataSet.Open;
  InitReportControls;
end;

procedure TReportForm.UpdateReportStatusDisplay;
begin
  RefreshDataSet(ReportQueueDataSet);
end;

procedure TReportForm.ReportQueueDataSetCalcFields(DataSet: TDataSet);
var
  Status, DisplayStatus, Elapsed: string;
  ElapsedTime: TDateTime;
begin
  inherited;
  DisplayStatus := '-';
  Elapsed := '-';

  Status := DataSet.FieldByName('Status').AsString;

  if Status = 'queued' then begin
    DisplayStatus := 'Running';
    ElapsedTime := Now-DataSet.FieldByName('request_date').AsDateTime;
    Elapsed := FormatDateTime('m:ss', ElapsedTime);
  end;

  DataSet.FieldByName('DisplayStatus').AsString := DisplayStatus;
  DataSet.FieldByName('Elapsed').AsString := Elapsed;
end;

procedure TReportForm.StatusCheckTimerTimer(Sender: TObject);
begin
  inherited;
  UpdateReportStatusDisplay;
end;

procedure TReportForm.ReportQueueGridDblClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TReportForm.FormCreate(Sender: TObject);
begin
  inherited;
  SetDefaultFont(Self);
  SetFontBold(ReportNameLabel);
  SetFontBold(ReportListLabel);
end;

initialization
  RegisterReportForm('Locate Status Activity', TLocateStatusActivityReportForm, 'LocateStatusActivity');
  RegisterReportForm('Sync Summary (Last Sync)', TSyncReportForm, 'SyncSummary');
  RegisterReportForm('Current Locator Load', TLoadReportForm, 'LocatorLoad');
  RegisterReportForm('Receive Audit', TAuditReportForm, 'Audit');
  RegisterReportForm('Daily Client Locates', TDailyClientLocatesReportForm, 'DailyClientClosing');
  RegisterReportForm('Late Tickets', TLateTicketReportForm, 'LateTicket');
  RegisterReportForm('Tickets Due', TTicketsDueReportForm, 'TicketsDue');
  RegisterReportForm('Tickets Due Export', TTicketsDueExportForm, 'TicketsDueExport');
  RegisterReportForm('No Conflict FAXes', TNoConflictReportForm, 'NoConflict');
  RegisterReportForm('High Profile', THighProfileReportForm, 'HighProfile');
  RegisterReportForm('Productivity After Hours', TAfterHoursReportForm, 'AfterHours');
  RegisterReportForm('Productivity', TProductivityReportForm, 'Productivity');
  RegisterReportForm('Ticket Detail', TClosedTicketDetailReportForm, 'ClosedTicketDetail'); // This actually calls the new TicketDetail report
  RegisterReportForm('Initial Status / Arrival', TInitialStatusReportForm, 'InitialStatus');
  RegisterReportForm('Damage Details', TDamageDetailsReportForm, 'DamageDetails');
  RegisterReportForm('Damage Liability Changes', TDamageLiabilityChangesReportForm, 'DamageLiabilityChanges');
  RegisterReportForm('Damage by Locator', TDamagesByLocatorReportForm, 'DamagesByLocator');
  RegisterReportForm('Damage List', TDamageListReportForm, 'DamageList');
  RegisterReportForm('No Laptops - New Ticket', TNewTicketReportForm, 'NewTicketDetail');
  RegisterReportForm('No Laptops - Load Sheet', TLoadSheetReportForm, 'LoadSheet');
  RegisterReportForm('Tickets Received', TTicketsReceivedReportForm, 'TicketsReceived');
  RegisterReportForm('Timesheets', TTimesheetReportForm, 'TimesheetDetail');
  RegisterReportForm('Timesheet Summary', TTimesheetSummaryReportForm, 'TimesheetSummary');
  RegisterReportForm('Timesheet Export History', TTimesheetExportsReportForm, 'TimeExport');
  RegisterReportForm('Timesheet Exception', TTimesheetExceptionReportForm, 'TimesheetException');
  RegisterReportForm('Timesheet Rule Check', TTimesheetRuleCheckReportForm, 'TimeRuleCheck');
  RegisterReportForm('Timesheet Audit History', TTimesheetAuditReportForm, 'TimesheetAudit');
  RegisterReportForm('On Time Completion', TOnTimeCompletionReportForm, 'OnTimeCompletion');
  RegisterReportForm('Old Open Tickets', TOldOpenTicketsReportForm, 'OldOpenTickets');
  RegisterReportForm('No Response Tickets', TNoResponseTicketsReportForm, 'NoResponseTickets');
  RegisterReportForm('Fax Status', TFaxStatusReportForm, 'FaxStatus');
  RegisterReportForm('Email Status', TEmailStatusReportForm, 'EmailStatus');
  RegisterReportForm('Assets', TAssetReportForm, 'Assets');
  RegisterReportForm('Emailed High Profile Tickets', TEmailedHPTicketsReportForm, 'EmailedHPTickets');
  RegisterReportForm('Productivity by Client', TProductivityClientReportForm, 'ProductivityClient');
  RegisterReportForm('Blasting Tickets', TBlastingTicketsReportForm, 'BlastingTickets');
  RegisterReportForm('Payroll Exception - Standard', TStandardPayrollExceptionReportForm, 'PayrollExceptionStandard');
  RegisterReportForm('Payroll Exception - Ad Hoc', TAdHocPayrollExceptionReportForm, 'PayrollExceptionAdHoc');
  RegisterReportForm('Status Groups', TStatusGroupReportForm, 'StatusGroups');
  RegisterReportForm('Payroll Summary', TPayrollSummaryReportForm, 'PayrollSummary');
  RegisterReportForm('Missing Sequential Invoice Numbers', TInvoiceIDExceptionReportForm, 'InvoiceIDException');
  RegisterReportForm('Employees', TEmployeeReportForm, 'Employees');
  RegisterReportForm('Daily Sales', TDailySalesReportForm, 'DailySales');
  RegisterReportForm('Billing Exception', TBillingExceptionReportForm, 'BillingException');
  RegisterReportForm('Missing Sequential Damage IDs', TDamageIDExceptionReportForm, 'DamageIDException');
  RegisterReportForm('Damage Default Estimates', TDamageDefaultEstimateReportForm, 'DamageDefaultEstimate');
  RegisterReportForm('Damage Invoices', TDamageInvoiceReportForm, 'DamageInvoice');
  RegisterReportForm('Billable Damages', TBillableDamagesReportForm, 'BillableDamages');
  RegisterReportForm('Investigation Productivity', TDamageProductivityReportForm, 'DamageProductivity');
  RegisterReportForm('Damages Per Locates', TDamagesPerLocatesReportForm, 'DamagesPerLocates');
  RegisterReportForm('Employee Activities', TEmployeeActivityReportForm, 'EmployeeActivity');
  RegisterReportForm('Restricted Use Exemptions', TRestrictedUseExemptionReportForm, 'RestrictedUseExemption');
  RegisterReportForm('Timesheet Single Employee', TTimesheetEmpSummaryReportForm, 'TimesheetEmpSummary');
  RegisterReportForm('Response Log', TResponseLogReportForm, 'ResponseLog');
  RegisterReportForm('High Level Productivity', THighLevelProductivityReportForm, 'HighLevelProductivity');
  RegisterReportForm('Employee COV Status', TEmployeeCovStatusReportForm, 'EmployeeCovStatus');
  RegisterReportForm('Locates Completed Summary', TLocatesCompletedSummaryReportForm, 'LocatesCompletedSummary');
  RegisterReportForm('Employees in Training', TEmployeesInTrainingReportForm, 'EmployeesInTraining');
  RegisterReportForm('Payroll Exception - Breaks', TBreakExceptionReportForm, 'PayrollExceptionBreaks');
  RegisterReportForm('Ticket Activity', TTicketActivityReportForm, 'TicketActivity');
  RegisterReportForm('Employee Permissions', TEmployeeRightsReportForm, 'EmployeeRights');
  RegisterReportForm('Timesheet Rule Response', TTimesheetRuleResponseReportForm, 'TimesheetRuleResponse');
  RegisterReportForm('Work Order Detail', TWorkOrderDetailReportForm, 'WorkOrderDetail');
  RegisterReportForm('Work Order Status Activity', TWorkOrderStatusActivityReportForm, 'WorkOrderStatusActivity');
  RegisterReportForm('Responder Status Translations', TResponderStatusTranslationsReportForm, 'ResponderStatusTranslations');

finalization
  FreeAndNil(Reports);

end.

