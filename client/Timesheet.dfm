inherited TimesheetForm: TTimesheetForm
  Left = 299
  Top = 0
  Caption = 'Timesheet'
  ClientHeight = 711
  ClientWidth = 1137
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 1137
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Name: TLabel
      Left = 3
      Top = 4
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object EmpNum: TLabel
      Left = 3
      Top = 19
      Width = 35
      Height = 13
      Caption = 'Emp #:'
    end
    object NameLabel: TLabel
      Left = 44
      Top = 4
      Width = 155
      Height = 13
      AutoSize = False
      Caption = 'NameLabel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EmpNumLabel: TLabel
      Left = 43
      Top = 19
      Width = 156
      Height = 13
      AutoSize = False
      Caption = 'EmpNumLabel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SickLeave: TLabel
      Left = 736
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Sick Leave:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object SickLeaveTopLabel: TLabel
      Left = 802
      Top = 4
      Width = 167
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'No Leave Balance Found...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object PTO: TLabel
      Left = 736
      Top = 19
      Width = 24
      Height = 13
      Caption = 'PTO:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object PTOTopLabel: TLabel
      Left = 802
      Top = 19
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'NO PTO Balance Found...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblReportLink: TLabel
      Left = 992
      Top = 11
      Width = 44
      Height = 13
      Cursor = crHandPoint
      Caption = 'REPORT'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      ParentFont = False
      Visible = False
      OnClick = lblReportLinkClick
    end
    object SaveButton: TButton
      Left = 466
      Top = 6
      Width = 80
      Height = 25
      Action = SaveAction
      TabOrder = 2
    end
    object PreviousButton: TButton
      Left = 200
      Top = 6
      Width = 105
      Height = 25
      Action = PreviousWeekAction
      TabOrder = 0
    end
    object NextButton: TButton
      Left = 316
      Top = 6
      Width = 105
      Height = 25
      Action = NextWeekAction
      TabOrder = 1
    end
    object CancelButton: TButton
      Left = 552
      Top = 6
      Width = 80
      Height = 25
      Action = CancelAction
      TabOrder = 3
    end
    object SaveStateButton: TButton
      Left = 644
      Top = 6
      Width = 80
      Height = 25
      Caption = 'Save State'
      TabOrder = 4
      OnClick = SaveStateButtonClick
    end
  end
  object TimesheetData: TScrollBox
    Left = 0
    Top = 35
    Width = 1137
    Height = 676
    HorzScrollBar.Smooth = True
    HorzScrollBar.Tracking = True
    HorzScrollBar.Visible = False
    VertScrollBar.Smooth = True
    VertScrollBar.Tracking = True
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    TabOrder = 1
    object ScrollbarPanel: TPanel
      Left = 8
      Top = 0
      Width = 77
      Height = 409
      Caption = 'ScrollbarPanel'
      TabOrder = 2
    end
    object LeftPanel: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 676
      Align = alLeft
      BevelOuter = bvNone
      FullRepaint = False
      TabOrder = 0
      object JuryDutyHoursLabel: TLabel
        Left = 5
        Top = 328
        Width = 82
        Height = 13
        Caption = 'Jury Duty Hours:'
      end
      object HolidayHoursLabel: TLabel
        Left = 5
        Top = 306
        Width = 70
        Height = 13
        Caption = 'Holiday Hours:'
      end
      object VacationHoursLabel: TLabel
        Left = 3
        Top = 204
        Width = 76
        Height = 13
        Caption = 'Vacation Hours:'
      end
      object CalloutHoursLabel: TLabel
        Left = 3
        Top = 136
        Width = 68
        Height = 13
        Caption = 'Callout Hours:'
      end
      object SickLeaveHoursLabel: TLabel
        Left = 6
        Top = 267
        Width = 85
        Height = 13
        Caption = 'Sick Leave Hours:'
      end
      object BereavementHoursLabel: TLabel
        Left = 5
        Top = 284
        Width = 87
        Height = 13
        Caption = 'Bereavement Hrs:'
      end
      object MilesStop1Label: TLabel
        Left = 5
        Top = 441
        Width = 84
        Height = 13
        Caption = 'Vehicle Miles End:'
      end
      object MilesStart1Label: TLabel
        Left = 5
        Top = 422
        Width = 90
        Height = 13
        Caption = 'Vehicle Miles Start:'
      end
      object DayHoursLabel: TLabel
        Left = 5
        Top = 349
        Width = 59
        Height = 13
        Caption = 'Total Hours:'
      end
      object RegularHoursLabel: TLabel
        Left = 3
        Top = 152
        Width = 72
        Height = 13
        Caption = 'Regular Hours:'
      end
      object OvertimeHoursLabel: TLabel
        Left = 3
        Top = 169
        Width = 79
        Height = 13
        Caption = 'Overtime Hours:'
      end
      object DoubletimeHoursLabel: TLabel
        Left = 3
        Top = 186
        Width = 88
        Height = 13
        Caption = 'Doubletime Hours:'
      end
      object VehicleUseLabel: TLabel
        Left = 5
        Top = 459
        Width = 70
        Height = 13
        Caption = 'Vehicle Usage:'
      end
      object SubmitButtonLabel: TLabel
        Left = 5
        Top = 475
        Width = 74
        Height = 13
        Caption = 'California Rules'
        Font.Charset = ANSI_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object FloatingHolidayLabel: TLabel
        Left = 5
        Top = 387
        Width = 80
        Height = 13
        Caption = 'Floating Holiday:'
      end
      object EmployeeTypeLabel: TLabel
        Left = 5
        Top = 369
        Width = 77
        Height = 13
        Caption = 'Employee Type:'
      end
      object ReasonChangedLabel: TLabel
        Left = 5
        Top = 403
        Width = 86
        Height = 13
        Caption = 'Reason Changed:'
        Visible = False
      end
      object WorkHoursLabel: TLabel
        Left = 3
        Top = 164
        Width = 60
        Height = 13
        Caption = 'Work Hours:'
      end
      object PTOHoursLabel: TLabel
        Left = 5
        Top = 248
        Width = 55
        Height = 13
        Caption = 'PTO Hours:'
      end
      object PerDiemLabel: TLabel
        Left = 5
        Top = 403
        Width = 46
        Height = 13
        Caption = 'Per Diem:'
      end
      object TopLeftClockPanel: TPanel
        Left = 0
        Top = 130
        Width = 97
        Height = 112
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
      end
      object TopLeftEntryPanel: TPanel
        Left = 0
        Top = 0
        Width = 97
        Height = 130
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object WorkStart1Label: TLabel
          Left = 3
          Top = 9
          Width = 63
          Height = 13
          Caption = 'Work Periods'
        end
        object WorkStart2Label: TLabel
          Left = 3
          Top = 27
          Width = 62
          Height = 13
          Caption = '(Begin / End,'
        end
        object HourFormatLabel: TLabel
          Left = 3
          Top = 41
          Width = 79
          Height = 13
          Caption = ' 24 hour format)'
        end
        object CalloutStart1Label: TLabel
          Left = 3
          Top = 97
          Width = 71
          Height = 13
          Caption = 'Callout Periods'
        end
        object ShowCalloutsButton: TButton
          Left = 4
          Top = 110
          Width = 70
          Height = 19
          Action = ShowCalloutsAction
          TabOrder = 0
        end
      end
    end
    object DaysData: TScrollBox
      Left = 97
      Top = 0
      Width = 1040
      Height = 676
      HorzScrollBar.Range = 541
      HorzScrollBar.Tracking = True
      VertScrollBar.Range = 440
      VertScrollBar.Visible = False
      Align = alClient
      AutoScroll = False
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 1
      object TotalsPanel: TPanel
        Left = 512
        Top = 2
        Width = 45
        Height = 440
        BevelOuter = bvNone
        TabOrder = 0
        object TotalsLabel: TLabel
          Left = 4
          Top = 16
          Width = 29
          Height = 13
          Caption = 'Totals'
        end
        object CalloutHoursLabel1: TLabel
          Left = 5
          Top = 122
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '00.00'
        end
        object WorkHoursLabel1: TLabel
          Left = 5
          Top = 136
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '00.00'
        end
        object RegularHoursLabel1: TLabel
          Left = 5
          Top = 152
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '00.00'
        end
        object OvertimeHoursLabel1: TLabel
          Left = 5
          Top = 169
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '00.00'
        end
        object DoubletimeHoursLabel1: TLabel
          Left = 5
          Top = 186
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '00.00'
        end
        object WeekLabel: TLabel
          Left = 4
          Top = 5
          Width = 27
          Height = 13
          Caption = 'Week'
        end
      end
    end
  end
  object TimesheetActions: TActionList
    OnUpdate = TimesheetActionsUpdate
    Left = 128
    Top = 64
    object SaveAction: TAction
      Caption = 'Save'
      OnExecute = SaveActionExecute
    end
    object PreviousWeekAction: TAction
      Caption = '<- Previous Week'
      OnExecute = PreviousWeekActionExecute
    end
    object NextWeekAction: TAction
      Caption = 'Next Week ->'
      OnExecute = NextWeekActionExecute
    end
    object CancelAction: TAction
      Caption = 'Cancel'
      OnExecute = CancelActionExecute
    end
    object ShowCalloutsAction: TAction
      Caption = 'Show More'
      OnExecute = ShowCalloutsActionExecute
    end
  end
end
