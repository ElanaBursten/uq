unit PlatAddin;

interface

uses Classes, SysUtils, DB, DMu, BaseAddinManager;

type
  TPlatAddinManager = class (TBaseAddinManager)
  protected
    function BuildAddinCommandLine(const InputFile, OutputFile: string): string; override;
    function RunAddin(const InputFile: string): string; override;   
    procedure Prepare(const ReadyForAddin: Boolean; const SectionID: string);
  public
    procedure PrepareForTicket(const HaveTicket: Boolean);    
    procedure PrepareForWorkOrder(const HaveWorkOrder: Boolean);
    function ExecuteForTicket(Ticket, Locates: TDataSet): Boolean;   
    function ExecuteForWorkOrder(WorkOrder: TDataSet): Boolean;
  end;
          
  const
    TicketSectionID  = 'TicketPlats';
    WorkOrderSectionID = 'WorkOrderPlats';

implementation

uses
  QMConst, OdMiscUtils, LocalEmployeeDMu;

{ TPlatAddinManager }

procedure TPlatAddinManager.Prepare(const ReadyForAddin: Boolean;
  const SectionID: string);
begin
  AddinButton.Visible := False;
  if ReadyForAddin then
    PrepareAddin(SectionID);
end;

procedure TPlatAddinManager.PrepareForTicket(const HaveTicket: Boolean);
begin
  Prepare(HaveTicket, TicketSectionID);
end;

procedure TPlatAddinManager.PrepareForWorkOrder(const HaveWorkOrder: Boolean);
begin
  Prepare(HaveWorkOrder, WorkOrderSectionID);
end;

function TPlatAddinManager.ExecuteForTicket(Ticket, Locates: TDataSet): Boolean;
var
  InputFile: string;
begin
  Assert(Assigned(Ticket));
  Assert(Assigned(Locates));
  LocateData := Locates; // This has the current (even unsaved) changes in it
  TicketData := Ticket;
  try
    Assert(not TicketData.IsEmpty, 'Ticket not present');

    ActionAttachmentID := NoAttachmentSelected;
    InputFile := GenerateTicketInput;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketPlatAddinStart, TicketData.FieldByName('ticket_id').AsString);
    RunAddin(InputFile);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeTicketPlatAddinEnd, TicketData.FieldByName('ticket_id').AsString);
    Result := True;
  finally
    TicketData := nil;
    LocateData := nil;
  end;
end;

function TPlatAddinManager.ExecuteForWorkOrder(WorkOrder: TDataSet): Boolean;
var
  InputFile: string;
begin
  Assert(Assigned(WorkOrder));
  WorkOrderData := WorkOrder;
  try
    Assert(not WorkOrderData.IsEmpty, 'Work order not present');

    ActionAttachmentID := NoAttachmentSelected;
    InputFile := GenerateWorkOrderInput;
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWorkOrderPlatAddinStart, WorkOrderData.FieldByName('wo_id').AsString);
    RunAddin(InputFile);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWorkOrderPlatAddinEnd, WorkOrderData.FieldByName('wo_id').AsString);
    Result := True;
  finally
    TicketData := nil;
    LocateData := nil;
  end;
end;

function TPlatAddinManager.BuildAddinCommandLine(const InputFile, OutputFile: string): string;
begin
  Result := Format(' -i "%s" -u "%s" -p "%s" -e "%s" -d "%d"',
      [InputFile, DM.UQState.UserName, DM.UQState.Password,
      EmployeeDM.GetUserEmployeeNumber, DM.EmpID]);
end;

function TPlatAddinManager.RunAddin(const InputFile: string): string;
var
  AddinParams: string;
begin
  Result := '';
  Assert(FileExists(AddinFileName));
  AddinParams := BuildAddinCommandLine(InputFile, '');
  // run the plat viewer asynchronously without waiting for it to close
  OdShellExecute(AddinFileName, AddinParams);
end;

end.
