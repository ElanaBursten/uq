unit AfterHoursReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect;

type
  TAfterHoursReportForm = class(TReportBaseForm)
    RangeSelect: TOdRangeSelectFrame;
    Label2: TLabel;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    Label6: TLabel;
    EmployeeStatusCombo: TComboBox;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, LocalEmployeeDMu;

{$R *.dfm}

{ TAfterHoursReportForm }

procedure TAfterHoursReportForm.ValidateParams;
begin
  inherited;
  SetReportID('AfterHours');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParamIntCombo('ManagerID', ManagersComboBox, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
end;

procedure TAfterHoursReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagersComboBox);
  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);
end;

end.

