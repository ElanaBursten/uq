unit DamageThirdPartySearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls, OdRangeSelect, OdXMLToDataSet;

type
  TDamageThirdPartySearchCriteria = class(TSearchCriteria)
    Label1: TLabel;
    UQDamageID: TEdit;
    Label3: TLabel;
    ThirdPartyID: TEdit;
    Label5: TLabel;
    CarrierSearch: TComboBox;
    Label2: TLabel;
    Claimant: TEdit;
    ProfitCenterLabel: TLabel;
    ProfitCenter: TComboBox;
    ClaimStatus: TComboBox;
    ClaimStatusLabel: TLabel;
    UtilityCoDamagedLabel: TLabel;
    UtilityCoDamaged: TEdit;
    AddressLabel: TLabel;
    Address: TEdit;
    CityLabel: TLabel;
    City: TEdit;
    StateLabel: TLabel;
    State: TComboBox;
    NotifiedDate: TOdRangeSelectFrame;
  private
    function GetClaimStatusCode: string;
    function GetCarrierID: Integer;
  public
    procedure BeforeItemSelected(ID: Variant); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure InitializeSearchCriteria; override;
    procedure Showing; override;
  end;

implementation

{$R *.dfm}

uses DMu, OdVCLUtils, SyncEngineU, DB, OdExceptions, LocalDamageDMu;

procedure TDamageThirdPartySearchCriteria.BeforeItemSelected(ID: Variant);
begin
  inherited;
  LDamageDM.UpdateDamageThirdPartyInCache(ID);
end;

procedure TDamageThirdPartySearchCriteria.DefineColumns;
begin
  inherited;
  with Defs do begin
    SetDefaultTable('damage_third_party');
    AddColumn('Third Party ID', 70, 'third_party_id', ftUnknown, '', 0, True);
    AddColumn('Damage ID', 70, 'uq_damage_id');
    AddColumn('Claimant', 110, 'claimant');
    AddColumn('Notified Date', 50, 'uq_notified_date');
    AddColumn('Address', 110, 'address1');
    AddColumn('City', 50, 'city');
    AddColumn('State', 30, 'state');
    AddColumn('Insurance', 110, 'carrier');
    AddColumn('Profit Ctr', 30, 'profit_center');
    AddColumn('Utility Co Damaged', 70, 'utility_co_damaged');
  end;
end;

function TDamageThirdPartySearchCriteria.GetXmlString: string;
begin
  DM.CallingServiceName('DamageThirdPartySearch');
  Result := DM.Engine.Service.DamageThirdPartySearch(DM.Engine.SecurityInfo,
    StrToIntDef(UQDamageID.Text, 0),
    StrToIntDef(ThirdPartyID.Text, 0),
    GetCarrierID,
    Claimant.Text,
    Address.Text,
    City.Text,
    State.Text,
    GetClaimStatusCode,
    ProfitCenter.Text,
    UtilityCoDamaged.Text,
    NotifiedDate.FromDate,
    NotifiedDate.ToDate);
end;

procedure TDamageThirdPartySearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.PopulateCarrierList(CarrierSearch.Items);
  CarrierSearch.Items.Insert(0, '');
  SizeComboDropdownToItems(CarrierSearch);

  DM.ProfitCenterList(ProfitCenter.Items);
  ProfitCenter.Items.Insert(0, '');

  DM.GetRefDisplayList('state', State.Items);
  State.Items.Insert(0, '');

  DM.GetRefDisplayList('claimstat', ClaimStatus.Items);
  ClaimStatus.Items.Insert(0, '');
  ClaimStatus.ItemIndex := 0;
  RestoreCriteria;
end;

function TDamageThirdPartySearchCriteria.GetCarrierID: Integer;
begin
  Result := 0;
  if CarrierSearch.ItemIndex > 0 then // 0 is blank
    Result := GetComboObjectInteger(CarrierSearch);
end;

function TDamageThirdPartySearchCriteria.GetClaimStatusCode: string;
begin
  Result := '';
  if Trim(ClaimStatus.Text) <> '' then
    Result := DM.GetRefCodeForDisplay('claimstat', ClaimStatus.Text);
end;

procedure TDamageThirdPartySearchCriteria.InitializeSearchCriteria;
begin
  inherited;
  NotifiedDate.SelectDateRange(rsYesterday);
end;

end.
