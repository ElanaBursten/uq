unit FaxStatusReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, OdRangeSelect;

type
  TFaxStatusReportForm = class(TReportBaseForm)
    CallCenterComboBox: TComboBox;
    CallCenterLabel: TLabel;
    QueueDate: TOdRangeSelectFrame;
    Label1: TLabel;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu;

{$R *.dfm}

{ TReportBaseForm1 }

procedure TFaxStatusReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterComboBox.Items, True);
  CallCenterComboBox.ItemIndex := 0;
  QueueDate.SelectDateRange(rsYesterday);
end;

procedure TFaxStatusReportForm.ValidateParams;
begin
  inherited;
  SetReportID('FaxStatus');
  SetParamDate('DateFrom', QueueDate.FromDate);
  SetParamDate('DateTo', QueueDate.ToDate);
  SetParam('CallCenter', FCallCenterList.GetCode(CallCenterComboBox.Text));
end;

end.
