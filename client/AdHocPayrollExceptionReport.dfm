inherited AdHocPayrollExceptionReportForm: TAdHocPayrollExceptionReportForm
  Left = 192
  Top = 186
  Caption = 'AdHocPayrollExceptionReportForm'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 0
    Top = 8
    Width = 115
    Height = 13
    Caption = 'Work Date Date Range:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 85
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label2: TLabel [2]
    Left = 0
    Top = 110
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object Label3: TLabel [3]
    Left = 0
    Top = 136
    Width = 59
    Height = 13
    Caption = 'Hours Type:'
  end
  object Label5: TLabel [4]
    Left = 0
    Top = 162
    Width = 42
    Height = 13
    Caption = 'Filtering:'
  end
  object Label6: TLabel [5]
    Left = 0
    Top = 188
    Width = 57
    Height = 13
    Caption = 'Filter Value:'
  end
  inline WorkDateRange: TOdRangeSelectFrame [6]
    Left = 40
    Top = 22
    Width = 271
    Height = 63
    TabOrder = 0
    inherited ToLabel: TLabel
      Left = 151
    end
    inherited DatesComboBox: TComboBox
      Left = 60
      Width = 198
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 60
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 172
    end
  end
  object ManagersComboBox: TComboBox
    Left = 100
    Top = 82
    Width = 179
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 0
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox
    Left = 100
    Top = 107
    Width = 129
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 2
  end
  object FieldCombo: TComboBox
    Left = 100
    Top = 133
    Width = 129
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 3
  end
  object FilterTypeCombo: TComboBox
    Left = 100
    Top = 159
    Width = 65
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    Items.Strings = (
      '='
      '>'
      '>='
      '<'
      '<=')
  end
  object FilterValueEdit: TEdit
    Left = 100
    Top = 185
    Width = 65
    Height = 21
    MaxLength = 3
    TabOrder = 5
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 2
  end
end
