unit QManager_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 8291 $
// File generated on 10/2/2017 12:48:22 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\TRUNK1\client\QManager.tlb (1)
// LIBID: {FE8785C1-A551-42BF-B22A-11A0EA6A53B0}
// LCID: 0
// Helpfile: 
// HelpString: QManager Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, Classes, Graphics, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  QManagerMajorVersion = 1;
  QManagerMinorVersion = 0;

  LIBID_QManager: TGUID = '{FE8785C1-A551-42BF-B22A-11A0EA6A53B0}';

  IID_IMyExternal: TGUID = '{CE7E6BD1-6C81-4F05-807E-0BAB9746EE5B}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMyExternal = interface;
  IMyExternalDisp = dispinterface;

// *********************************************************************//
// Interface: IMyExternal
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CE7E6BD1-6C81-4F05-807E-0BAB9746EE5B}
// *********************************************************************//
  IMyExternal = interface(IDispatch)
    ['{CE7E6BD1-6C81-4F05-807E-0BAB9746EE5B}']
    procedure AckMsg(const Ack: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  IMyExternalDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CE7E6BD1-6C81-4F05-807E-0BAB9746EE5B}
// *********************************************************************//
  IMyExternalDisp = dispinterface
    ['{CE7E6BD1-6C81-4F05-807E-0BAB9746EE5B}']
    procedure AckMsg(const Ack: WideString); dispid 201;
  end;

implementation

uses ComObj;

end.
