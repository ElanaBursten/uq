unit DamageDetailsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst;

type
  TDamageDetailsReportForm = class(TReportBaseForm)
    EditUQDamageID: TEdit;
    Label1: TLabel;
    StyleRadioGroup: TRadioGroup;
    SectionsGroup: TGroupBox;
    IncludeNarrative: TCheckBox;
    IncludeDiscussion: TCheckBox;
    IncludeConclusion: TCheckBox;
    IncludeDamageAttachments: TCheckBox;
    IncludeNotes: TCheckBox;
    IncludeThirdParty: TCheckBox;
    IncludeLitigation: TCheckBox;
    IncludeTicket: TCheckBox;
    IncludeDamage: TCheckBox;
    IncludeHistory: TCheckBox;
    IncludeTicketAttachments: TCheckBox;
    LabelSelectDamageImages: TLabel;
    DamageAttachmentList: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    LabelSelectTicketImages: TLabel;
    TicketAttachmentList: TCheckListBox;
    SelectAllButton2: TButton;
    ClearAllButton2: TButton;
    UpdateListButton: TButton;
    procedure EditUQDamageIDKeyPress(Sender: TObject; var Key: Char);
    procedure StyleRadioGroupClick(Sender: TObject);
    procedure IncludeDamageClick(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure SelectAllButton2Click(Sender: TObject);
    procedure ClearAllButton2Click(Sender: TObject);
    procedure IncludeTicketClick(Sender: TObject);
    procedure IncludeDamageAttachmentsClick(Sender: TObject);
    procedure IncludeTicketAttachmentsClick(Sender: TObject);
    procedure UpdateListButtonClick(Sender: TObject);
    procedure EditUQDamageIDChange(Sender: TObject);
  protected
    FDamageID: Integer;
    FTicketID: Integer;
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    procedure InitTicketControls(Enabled: Boolean);
    procedure AdjustTicketControls;
    procedure UpdateAttachmentList;
  public
    class procedure SetLastUQDamageID(UQDamageID: Integer);
    class procedure SetLastDamageParams(UQDamageID: Integer; ReportMode: string);
  private
    function ShortMode: Boolean;
    procedure ConfigureForStyle;
  end;

implementation

{$R *.dfm}

uses OdExceptions, DMu, QMConst, OdVclUtils, OdMiscUtils, LocalPermissionsDMu, LocalDamageDmu;

var
  FLastUQDamageID: Integer;
  FShowThirdParty: Boolean;
  FShowLitigation: Boolean;
  FShowDamage: Boolean;

{ TDamageDetailsReportForm }

class procedure TDamageDetailsReportForm.SetLastUQDamageID(UQDamageID: Integer);
begin
  FLastUQDamageID := UQDamageID;
  FShowDamage := True;
  FShowThirdParty := False;
  FShowLitigation := False;
end;

class procedure TDamageDetailsReportForm.SetLastDamageParams(UQDamageID: Integer;
  ReportMode: string);
begin
  FLastUQDamageID := UQDamageID;
  FShowDamage := SameText(ReportMode, DamageReportOnDamage);
  FShowThirdParty := SameText(ReportMode, DamageReportOnThirdParty);
  FShowLitigation := SameText(ReportMode, DamageReportOnLitigation);
end;

procedure TDamageDetailsReportForm.EditUQDamageIDKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_BACK then
    Exit;
  if (IsCharAlphaNumeric(Key) and (not IsCharAlpha(Key))) then begin
    // This is a valid numeric key
  end
  else
    Key := #0;
end;

procedure TDamageDetailsReportForm.ValidateParams;
var
  ImageList: string;
begin
  inherited;
  if StrToIntDef(EditUQDamageID.Text, -1) = -1 then begin
    EditUQDamageID.SetFocus;
    raise EOdEntryRequired.Create('Please enter a valid damage ID');
  end;

  if ShortMode then
    SetReportID('DamageShort')
  else
    SetReportID('DamageDetails');

  IncludeDamageAttachments.Checked := not IsEmpty(GetCheckedItemString(DamageAttachmentList, True));
  IncludeTicketAttachments.Checked := not IsEmpty(GetCheckedItemString(TicketAttachmentList, True));

  // This is a user entered value, so it is a UQDamageId, NOT a Damage ID.
  // Damage ID are internal only, a user would never know one.
  SetParamInt('UQDamageID', StrToInt(EditUQDamageID.Text));

  // Specify which regions of the report should be visible
  SetParamBoolean('ShowTicket', IncludeTicket.Checked);
  SetParamBoolean('ShowNarrative', IncludeNarrative.Checked);
  SetParamBoolean('ShowDiscussion', IncludeDiscussion.Checked);
  SetParamBoolean('ShowConclusions', IncludeConclusion.Checked);
  SetParamBoolean('ShowNotes', IncludeNotes.Checked);
  SetParamBoolean('ShowHistory', IncludeHistory.Checked);
  SetParamBoolean('ShowDamage', IncludeDamage.Checked);
  SetParamBoolean('ShowThirdParty', IncludeThirdParty.Checked);
  SetParamBoolean('ShowLitigation', IncludeLitigation.Checked);

  if IncludeDamageAttachments.Checked then begin
    ImageList := GetCheckedItemString(DamageAttachmentList, True);
    if not IsEmpty(ImageList) then
      SetParam('DamageImageList', ImageList);
  end;

  if IncludeTicketAttachments.Checked then begin
    ImageList := GetCheckedItemString(TicketAttachmentList, True);
    if not IsEmpty(ImageList) then
      SetParam('TicketImageList', ImageList);
  end;
end;

procedure TDamageDetailsReportForm.InitReportControls;
begin
  inherited;
  if FLastUQDamageID > 0 then begin
    EditUQDamageID.Text := IntToStr(FLastUQDamageID);
    FDamageID := LDamageDM.GetDamageIDForUQDamageID(FLastUQDamageID);
    FTicketID := LDamageDM.GetTicketIDForDamageID(FDamageID);
    InitTicketControls(FTicketID > 0);
  end;

  IncludeThirdParty.Enabled := PermissionsDM.EmployeeHasLitigationRights;
  IncludeLitigation.Enabled := PermissionsDM.EmployeeHasLitigationRights;
  IncludeThirdParty.Checked := IncludeThirdParty.Enabled and FShowThirdParty;
  IncludeLitigation.Checked := IncludeLitigation.Enabled and FShowLitigation;
  IncludeDamage.Checked := FShowDamage or (not PermissionsDM.EmployeeHasLitigationRights);

  if FLastUQDamageID > 0 then
    UpdateAttachmentList;

  FLastUQDamageID := 0;
  ConfigureForStyle;
end;

procedure TDamageDetailsReportForm.InitTicketControls(Enabled: Boolean);
begin
  IncludeTicket.Enabled := Enabled;
  IncludeTicket.Checked := IncludeTicket.Enabled;
  AdjustTicketControls;
end;

procedure TDamageDetailsReportForm.AdjustTicketControls;
begin
  IncludeTicketAttachments.Enabled := IncludeTicket.Checked;

  if (not IncludeTicketAttachments.Enabled) or (not IncludeTicketAttachments.Checked) then
    IncludeTicketAttachments.Checked := False;

  SetCheckListBox(TicketAttachmentList, IncludeTicketAttachments.Checked);
  TicketAttachmentList.Enabled := IncludeTicketAttachments.Enabled and IncludeTicketAttachments.Checked;
  SelectAllButton2.Enabled := TicketAttachmentList.Enabled;
  ClearAllButton2.Enabled := TicketAttachmentList.Enabled;
  LabelSelectTicketImages.Enabled := TicketAttachmentList.Enabled;
end;

procedure TDamageDetailsReportForm.ConfigureForStyle;
begin
  IncludeNarrative.Enabled  := ShortMode and IncludeDamage.Checked;
  IncludeDiscussion.Enabled := ShortMode and IncludeDamage.Checked;
  IncludeConclusion.Enabled := ShortMode and IncludeDamage.Checked;
  IncludeTicket.Checked := IncludeDamage.Checked and IncludeTicket.Enabled;
  IncludeDamageAttachments.Checked := (IncludeDamageAttachments.Checked) and (not ShortMode);

  if not ShortMode then begin
    IncludeNarrative.Checked  := IncludeDamage.Checked;
    IncludeDiscussion.Checked := IncludeDamage.Checked;
    IncludeConclusion.Checked := IncludeDamage.Checked;
    IncludeDamageAttachments.Checked := IncludeDamage.Checked;
  end;
end;

function TDamageDetailsReportForm.ShortMode: Boolean;
begin
  Result := StyleRadioGroup.ItemIndex = 1;
end;

procedure TDamageDetailsReportForm.StyleRadioGroupClick(Sender: TObject);
begin
  ConfigureForStyle;
end;

procedure TDamageDetailsReportForm.IncludeDamageClick(Sender: TObject);
begin
  ConfigureForStyle;
end;

procedure TDamageDetailsReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(DamageAttachmentList, True);
end;

procedure TDamageDetailsReportForm.ClearAllButtonClick(Sender: TObject);
begin
  SetCheckListBox(DamageAttachmentList, False);
end;

procedure TDamageDetailsReportForm.SelectAllButton2Click(Sender: TObject);
begin
  SetCheckListBox(TicketAttachmentList, True);
end;

procedure TDamageDetailsReportForm.ClearAllButton2Click(Sender: TObject);
begin
  SetCheckListBox(TicketAttachmentList, False);
end;

procedure TDamageDetailsReportForm.IncludeTicketClick(Sender: TObject);
begin
  AdjustTicketControls;
end;

procedure TDamageDetailsReportForm.IncludeDamageAttachmentsClick(
  Sender: TObject);
begin
  SelectAllButton.Enabled := IncludeDamageAttachments.Checked;
  ClearAllButton.Enabled := IncludeDamageAttachments.Checked;
  DamageAttachmentList.Enabled := IncludeDamageAttachments.Checked;
  LabelSelectDamageImages.Enabled := IncludeDamageAttachments.Checked;
  SetCheckListBox(DamageAttachmentList, IncludeDamageAttachments.Checked);

  UpdateListButton.Enabled := IncludeDamageAttachments.Checked or IncludeTicketAttachments.Checked
end;

procedure TDamageDetailsReportForm.IncludeTicketAttachmentsClick(
  Sender: TObject);
begin
  AdjustTicketControls;
  UpdateListButton.Enabled := IncludeDamageAttachments.Checked or IncludeTicketAttachments.Checked
end;

procedure TDamageDetailsReportForm.UpdateAttachmentList;
begin
  if IsEmpty(EditUQDamageID.Text) then
    Exit;

  if FDamageID = 0 then begin
    FDamageID := LDamageDM.GetDamageIDForUQDamageID(StrToInt(EditUQDamageID.Text));
    if FDamageID = 0 then
      raise Exception.Create('Q Manager could not find a damage for the UQ Damage ID value supplied.');
  end;

  if FDamageID <> 0 then
    FTicketID := LDamageDM.GetTicketIDForDamageID(FDamageID);

  DamageAttachmentList.Clear;
  if FDamageID > 0 then begin
    DM.GetPrintableAttachmentList(qmftDamage, FDamageID, DamageAttachmentList.Items);
    SetCheckListBox(DamageAttachmentList, IncludeDamageAttachments.Checked);
  end;

  TicketAttachmentList.Clear;
  if FTicketID > 0 then begin
    DM.GetPrintableAttachmentList(qmftTicket, FTicketID, TicketAttachmentList.Items);
    SetCheckListBox(TicketAttachmentList, IncludeTicketAttachments.Checked);
  end;

  FDamageID := 0;
  FTicketID := 0;

  ShowCheckListboxHorzScrollbars(DamageAttachmentList);
  ShowCheckListboxHorzScrollbars(TicketAttachmentList);
end;

procedure TDamageDetailsReportForm.UpdateListButtonClick(Sender: TObject);
begin
  UpdateAttachmentList;
end;

procedure TDamageDetailsReportForm.EditUQDamageIDChange(Sender: TObject);
begin
  DamageAttachmentList.Clear;
  TicketAttachmentList.Clear;
end;

end.

