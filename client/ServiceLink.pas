unit ServiceLink;

{ SERVER_IN_CLIENT compiles the service modules in to the client... this is
  quite useful for testing, as it lets you debug the server code.  Obviously,
  never deploy a production build with SERVER_IN_CLIENT!
}

{$IFNDEF CUSTOMER_BUILD}
  {$DEFINE SERVER_IN_CLIENT} // Reads connection info from local client\QMServer.ini if defined
{$ENDIF not CUSTOMER_BUILD}

interface

uses
  QMServerLibrary_Intf, OdHttpROChannel;

function CreateQMService(BaseURL: string): QMService;
function GetChan: TOdHttpROChannel;
function ServerInClient: Boolean;

implementation

uses
  uROEncryption,
  uROBINMessage,
{$IFDEF SERVER_IN_CLIENT}
  QMService_Impl, LogicDMu;
{$ELSE}
  uROClient, uROProxy;
{$ENDIF}

var
  Chan: TOdHttpROChannel;  // Keep this ref so we can turn off the HTTP dialog. Note: This ends up as a dangling pointer.

function GetChan: TOdHttpROChannel;
begin
  Assert(Assigned(Chan), 'TOdHttpROChannel global Chan is null');
  Result := Chan;
end;

function CreateQMService(BaseURL: string): QMService;
{$IFNDEF SERVER_IN_CLIENT}
var
  BinMsg: TROBINMessage;
{$ENDIF}
begin
  Chan := TOdHttpROChannel.Create(nil);
  Chan.Url := BaseURL + 'app/QMLogic.dll/BIN';
  Chan.ShowProgress := True;
  Chan.Encryption.EncryptionMethod := tetRijndael;
  Chan.Encryption.EncryptionRecvKey := 'B2E3056082D3D01A158BE07937FC9452';
  Chan.Encryption.EncryptionSendKey := '7A2B4FFB71F59204A247382D98BE0C08';

{$IFDEF SERVER_IN_CLIENT}
  LogicDmIniName := '.\QMServer.ini';
  Result := TQMService.Create;
{$ELSE}
  BinMsg := TROBINMessage.Create;
  Result := CoQMService.Create(BinMsg, Chan);
{$ENDIF}
end;

function ServerInClient: Boolean;
begin
{$IFDEF SERVER_IN_CLIENT}
  Result := True;
{$ELSE}
  Result := False;
{$ENDIF}
end;

end.
