unit AckAllTicketActivity;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TAckAllTicketActivityForm = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    Bevel1: TBevel;
    CutoffDate: TcxDateEdit;
    Label2: TLabel;
    InstructionsLabel: TLabel;
    AffectedRecsLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CutoffDateExit(Sender: TObject);
    procedure CutoffDateChange(Sender: TObject);
  private
    FCutoffDateChanged: Boolean;
  public
    ManagerName: string;
    ManagerID: Integer;
    AffectedRecCount: Integer;
  end;

  function AcknowledgeActivityForManager(const ManagerID: Integer; const ManagerName: string): Integer;

implementation

uses DMu, OdHourglass;

{$R *.dfm}

function AcknowledgeActivityForManager(const ManagerID: Integer; const ManagerName: string): Integer;
var
  Dlg: TAckAllTicketActivityForm;
begin
  Result := 0;
  Dlg := TAckAllTicketActivityForm.Create(nil);
  try
    Dlg.ManagerName := ManagerName;
    Dlg.ManagerID := ManagerID;
    if Dlg.ShowModal = mrOk then
      Result := Dlg.AffectedRecCount;
  finally
    FreeAndNil(Dlg);
  end;
end;

procedure TAckAllTicketActivityForm.FormShow(Sender: TObject);
begin
  InstructionsLabel.Caption := Format('This will acknowledge all ticket activities ' +
    'for %s occurring before the selected Cutoff Date.', [ManagerName]);
  CutoffDate.Clear;
  CutoffDate.SetFocus;
  AffectedRecsLabel.Caption := 'Select a Cutoff Date && press Tab to get an activity count.';
  OKButton.Enabled := False;
end;

procedure TAckAllTicketActivityForm.OKButtonClick(Sender: TObject);
begin
  AffectedRecCount := DM.Engine.AckTicketActivitiesForManager(ManagerID, CutoffDate.Date);
  ModalResult := mrOk;
end;

procedure TAckAllTicketActivityForm.CutoffDateExit(Sender: TObject);
var
  ActivityCount: Integer;
  Accept: Boolean;
  Cursor: IInterface;
begin
  if not FCutoffDateChanged then
    Exit;

  Cursor := ShowHourGlass;
  ActivityCount := DM.Engine.GetUnackedTicketActivityCountForManager(ManagerID, CutoffDate.Date);
  Accept := (ActivityCount > 0);
  if ActivityCount > 1 then
    AffectedRecsLabel.Caption := Format('Click OK to acknowledge %d unacknowledged activities.', [ActivityCount])
  else if ActivityCount = 1 then
    AffectedRecsLabel.Caption := 'Click OK to acknowledge 1 unacknowledged activity.'
  else
    AffectedRecsLabel.Caption := 'No unacknowledged activities were found.';
  OKButton.Enabled := Accept;
  FCutoffDateChanged := False;
end;

procedure TAckAllTicketActivityForm.CutoffDateChange(Sender: TObject);
begin
  FCutoffDateChanged := True;
  AffectedRecsLabel.Caption := 'Select a Cutoff Date && press Tab to get an activity count.';
end;

end.
