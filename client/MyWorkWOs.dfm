inherited MyWorkWOsFrame: TMyWorkWOsFrame
  Height = 338
  inherited SummaryPanel: TPanel
    Caption = 'Work Orders'
  end
  inherited DataPanel: TPanel
    inherited DataSummaryPanel: TPanel
      Caption = 'Work Orders'
    end
    inherited BaseGrid: TcxGrid
      inherited BaseGridLevelDetail: TcxGridLevel
        GridView = WorkOrdersGridDetailsTableView
      end
      inherited BaseGridLevelSummary: TcxGridLevel
        GridView = WorkOrdersGridSummaryTableView
      end
    end
  end
  object OpenWorkOrders: TDBISAMQuery
    AutoCalcFields = False
    FilterOptions = [foCaseInsensitive]
    BeforeOpen = OpenWorkOrdersBeforeOpen
    AfterScroll = OpenWorkOrdersAfterScroll
    OnCalcFields = OpenWorkOrdersCalcFields
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * '
      'from work_order'
      'where assigned_to_id = :emp_id'
      '  and active = 1'
      '  and closed = 0'
      'order by '
      '  kind, due_date, wo_number')
    Params = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
    Left = 721
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emp_id'
      end>
  end
  object WorkOrdersSource: TDataSource
    DataSet = OpenWorkOrders
    Left = 756
    Top = 5
  end
  object MyWorkViewRepository: TcxGridViewRepository
    Left = 646
    Top = 8
    object WorkOrdersGridDetailsTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = WorkOrdersSource
      DataController.KeyFieldNames = 'wo_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.RowSeparatorWidth = 1
      OptionsView.BandHeaders = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'WORK ORDERS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object WoDColWorkOrderID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'wo_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object WoDColWorkOrderNumber: TcxGridDBBandedColumn
        Caption = 'Work Order #'
        DataBinding.FieldName = 'wo_number'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object WoDColClientID: TcxGridDBBandedColumn
        Caption = 'Client'
        DataBinding.FieldName = 'client_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.ReadOnly = True
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 132
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object WoDColWOSource: TcxGridDBBandedColumn
        Caption = 'Call Center'
        DataBinding.FieldName = 'wo_source'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object WoDColKind: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 33
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object WoDColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'work_order_status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 48
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object WoDColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object WoDColMapRef: TcxGridDBBandedColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object WoDColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Options.VertSizing = False
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object WoDColWorkType: TcxGridDBBandedColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 624
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object WoDColWorkDescription: TcxGridDBBandedColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 275
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 2
      end
      object WoDColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_Number'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 50
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object WoDColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 156
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object WoDColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 105
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object WoDColWorkCity: TcxGridDBBandedColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 62
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object WoDColWorkState: TcxGridDBBandedColumn
        Caption = 'State'
        DataBinding.FieldName = 'work_state'
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 35
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object WoDColWorkZip: TcxGridDBBandedColumn
        Caption = 'Zip'
        DataBinding.FieldName = 'work_zip'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object WoDColWorkLat: TcxGridDBBandedColumn
        Caption = 'Lat'
        DataBinding.FieldName = 'work_lat'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object WoDColWorkLong: TcxGridDBBandedColumn
        Caption = 'Long'
        DataBinding.FieldName = 'work_long'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object WoDColCallerName: TcxGridDBBandedColumn
        Caption = 'Caller Name'
        DataBinding.FieldName = 'caller_name'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 97
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object WoDColCallerPhone: TcxGridDBBandedColumn
        Caption = 'Caller Phone'
        DataBinding.FieldName = 'caller_phone'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object WoDColCallerCellular: TcxGridDBBandedColumn
        Caption = 'Caller Cell'
        DataBinding.FieldName = 'caller_cellular'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object WoDColClientTransmitDate: TcxGridDBBandedColumn
        Caption = 'Transmit Date'
        DataBinding.FieldName = 'transmit_date'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Options.VertSizing = False
        Width = 93
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object WoDColJobNumber: TcxGridDBBandedColumn
        Caption = 'Job #'
        DataBinding.FieldName = 'job_number'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 49
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object WoDColWireCenter: TcxGridDBBandedColumn
        Caption = 'Wire Center'
        DataBinding.FieldName = 'wire_center'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 2
      end
      object WoDColWorkCenter: TcxGridDBBandedColumn
        Caption = 'Work Center'
        DataBinding.FieldName = 'work_center'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 2
      end
      object WoDColCentralOffice: TcxGridDBBandedColumn
        Caption = 'C.O.'
        DataBinding.FieldName = 'central_office'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 27
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 2
      end
      object WoDColServingTerminal: TcxGridDBBandedColumn
        Caption = 'Srv Term'
        DataBinding.FieldName = 'serving_terminal'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 2
      end
      object WoDColCircuitNumber: TcxGridDBBandedColumn
        Caption = 'Circuit #'
        DataBinding.FieldName = 'circuit_number'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 142
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object WoDColF2Cable: TcxGridDBBandedColumn
        Caption = 'F2 Cable'
        DataBinding.FieldName = 'f2_cable'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 47
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 2
      end
      object WoDColTerminalPort: TcxGridDBBandedColumn
        Caption = 'Term. Port'
        DataBinding.FieldName = 'terminal_port'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 2
      end
      object WoDColF2Pair: TcxGridDBBandedColumn
        Caption = 'F2 Pair'
        DataBinding.FieldName = 'f2_pair'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 56
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 2
      end
      object WoDColStateHwyROW: TcxGridDBBandedColumn
        Caption = 'St. ROW'
        DataBinding.FieldName = 'state_hwy_row'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 77
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 2
      end
      object WoDColRoadBoreCount: TcxGridDBBandedColumn
        Caption = 'Rd. Bores'
        DataBinding.FieldName = 'road_bore_count'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 2
      end
      object WoDColDrivewayBoreCount: TcxGridDBBandedColumn
        Caption = 'Dr. Bores'
        DataBinding.FieldName = 'driveway_bore_count'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Options.VertSizing = False
        Width = 68
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 2
      end
      object WoDColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
    end
    object WorkOrdersGridSummaryTableView: TcxGridDBBandedTableView
      OnDblClick = GridDblClick
      Navigator.Buttons.CustomButtons = <>
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = WorkOrdersSource
      DataController.KeyFieldNames = 'wo_id'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnCompare = GridCompare
      OptionsCustomize.BandSizing = False
      OptionsCustomize.ColumnVertSizing = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ShowEditButtons = gsebForFocusedRecord
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Styles.Group = SharedDevExStyleData.NavyGridGroupSummary
      Bands = <
        item
          Caption = 'WORK ORDERS'
          HeaderAlignmentHorz = taLeftJustify
          Styles.Header = SharedDevExStyleData.BoldLargeFontStyle
        end>
      object WoSColWorkOrderID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'wo_id'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object WoSColWorkOrderNumber: TcxGridDBBandedColumn
        Caption = 'Work Order #'
        DataBinding.FieldName = 'wo_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 118
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object WoSColDueDate: TcxGridDBBandedColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        Options.Editing = False
        Options.Filtering = False
        Options.ShowEditButtons = isebNever
        Width = 211
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object WoSColKind: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kind'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soDescending
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object WoSColStatus: TcxGridDBBandedColumn
        Caption = 'Status'
        DataBinding.FieldName = 'work_order_status'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object WoSColWorkAddressNumber: TcxGridDBBandedColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object WoSColWorkAddressStreet: TcxGridDBBandedColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object WoSColWorkCounty: TcxGridDBBandedColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object WoSColMapPage: TcxGridDBBandedColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object WoSColMapRef: TcxGridDBBandedColumn
        Caption = 'Map Ref'
        DataBinding.FieldName = 'map_ref'
        Options.Editing = False
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object WoSColExport: TcxGridDBBandedColumn
        Caption = 'Export?'
        DataBinding.FieldName = 'export'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
    end
  end
end
