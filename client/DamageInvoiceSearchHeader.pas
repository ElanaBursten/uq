unit DamageInvoiceSearchHeader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SearchHeader, StdCtrls, OdRangeSelect, OdXMLToDataSet;

type
  TDamageInvoiceSearchCriteria = class(TSearchCriteria)
    Company: TEdit;
    CompanyLabel: TLabel;
    DamageCityLabel: TLabel;
    DamageCity: TEdit;
    DamageState: TComboBox;
    DamageStateLabel: TLabel;
    UQDamageID: TEdit;
    DamageIDLabel: TLabel;
    InvoiceNumber: TEdit;
    InvoiceNumberLabel: TLabel;
    AmountLabel: TLabel;
    CommentLabel: TLabel;
    Amount: TEdit;
    Comment: TEdit;
    ReceivedDate: TOdRangeSelectFrame;
    InvoiceDate: TOdRangeSelectFrame;
  public
    procedure BeforeItemSelected(ID: Variant); override;
    function GetXmlString: string; override;
    procedure DefineColumns; override;
    procedure Showing; override;
  end;

implementation

uses DB, DMu, QMServerLibrary_Intf;

{$R *.dfm}

{ TDamageInvoiceSearchCriteria }

procedure TDamageInvoiceSearchCriteria.BeforeItemSelected(ID: Variant);
begin
  inherited;
  DM.Engine.GetInvoiceDataFromServer(ID);
end;

procedure TDamageInvoiceSearchCriteria.DefineColumns;
begin
  inherited;
  with Defs do begin
    SetDefaultTable('damage_invoice');
    AddColumn('', 0, 'invoice_id', ftUnknown, '', 0, True, False);
    AddColumn('Invoice #', 70, 'invoice_num');
    AddColumn('Company', 90, 'company');
    AddColumn('Recv Date', 75, 'received_date', ftDateTime);
    AddColumn('Inv Date', 75, 'cust_invoice_date', ftDateTime);
    AddColumn('Amount', 60, 'amount');
    AddColumn('City', 90, 'damage_city');
    AddColumn('State', 40, 'damage_state');
    AddColumn('Damage ID', 75, 'uq_damage_id');
    AddColumn('Comment', 145, 'comment');
  end;
end;

procedure TDamageInvoiceSearchCriteria.Showing;
begin
  inherited;
  SaveCriteria;
  DM.GetRefDisplayList('state', DamageState.Items);
  RestoreCriteria;
end;

function TDamageInvoiceSearchCriteria.GetXmlString: string;
begin
  DM.CallingServiceName('DamageInvoiceSearch3');
  Result := DM.Engine.Service.DamageInvoiceSearch3(
    DM.Engine.SecurityInfo,
    InvoiceNumber.Text,
    Company.Text,
    DamageCity.Text,
    DamageState.Text,
    StrToIntDef(UQDamageID.Text, 0),
    StrToCurrDef(Amount.Text, 0),
    Comment.Text,
    ReceivedDate.FromDate, ReceivedDate.ToDate,
    InvoiceDate.FromDate, InvoiceDate.ToDate,
    ''
    );
end;

end.

