unit TicketDetail;

interface
                                                
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OdEmbeddable, DBCtrls, StdCtrls, Db, ExtCtrls, ComCtrls, Buttons, 
  DBISAMTb, Mask, DMu, LocateHours, Attachments, CheckLst, PhotoAttachment,
  QMConst, Spin, OdStillImage, EmployeeSelectFrame, ActnList,
  AttachmentsAddin, LocateLengthDetail, OdMiscUtils, PlatAddin, TicketWorkView,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxCurrencyEdit, cxTextEdit, cxSpinEdit, cxGridBandedTableView,
  cxGridDBBandedTableView, cxCheckBox, cxContainer, cxTimeEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxDBExtLookupComboBox, cxImage, cxButtonEdit,
  BaseNotes, cxNavigator, dxCore, cxDateUtils, CodeLookupList, cxSplitter,
  cxHyperLinkEdit, cxMemo, cxRichEdit, AddProjectLocate, OdContainer, cxBlobEdit,
  cxDBEdit, Menus, cxCheckGroup, cxCheckComboBox,
  RouteOrderOverride, RouteOrderMgt,TicketAlertsFrameU, //dxSkinsCore,
  cxCustomData, cxFilter, cxData, dxSkinsCore;   //QMANTWO-775 RESURRECTION

const
  WM_DISPLAYLOCATEHOURS = WM_USER + 487;

type
  TFollowupTicketState = (fsNotAllowed, fsCanCreate, fsNeedToVerify,
    fsReadyToCreate, fsCreated);
                                                                       
  TRouteOrderSkip = Record
    Skipped: Boolean;
    OrderNum: integer;
    TicketID: integer;
  end;

  TClientStatusRequires = record  //QM-282 EB
    LocateID: Integer;
    ClientID: integer;
    ClientName: string;
    Status: string;
    ReqPictures: boolean;
    ReqNotes: boolean;
    Changed: boolean;
  end;

  TFormStatus = record //QM-577
    FormReady: boolean;
    DataLoaded: boolean;
    NoteSourceLoaded: boolean;
  end;


  TCENButtonMode = (cbNone, cbCEN, cbTEMA);   // QM-53 EB

  TLocatorAssignments = class(TObject)
  public
    LocatorIDList: TIntegerList;
    LocateIDList: TIntegerList;
    Constructor Create;
    Destructor Destroy;
    function Add(LocatorID, LocateID: Integer): integer;
  end;

  TTicketDetailForm = class(TEmbeddableForm)
    TicketPages: TPageControl;
    DetailsTab: TTabSheet;
    ImageTab: TTabSheet;
    NotesTab: TTabSheet;
    HeaderPanel: TPanel;
    TicketNum: TLabel;
    TicketImageMemo: TMemo;
    TicketDS: TDataSource;
    DetailsPanel: TPanel;
    CrossStreetLabel: TLabel;
    ContractorLabel: TLabel;
    WorkDoneForLabel: TLabel;
    ContactLabel: TLabel;
    AltContactLabel: TLabel;
    WorkAddressLabel: TLabel;
    WorkToBeDone: TLabel;
    DoneButton: TButton;
    FooterPanel: TPanel;
    MapPageLabel: TLabel;
    WorkDateLabel: TLabel;
    TicketReceivedLabel: TLabel;
    LocateDS: TDataSource;
    Company: TDBText;
    CallerAltContact: TDBText;
    CallDate: TDBText;
    Address: TDBText;
    ConName: TDBText;
    CallerContact: TDBText;
    CallerPhone: TDBText;
    CallerAltPhone: TDBText;
    MapPage: TDBText;
    WorkCross: TDBText;
    City: TDBText;
    DueDate: TDBText;
    State: TDBText;
    County: TDBText;
    Kind: TDBText;
    AddLocateButton: TButton;
    WorkAddressNumber: TDBText;
    WorkType: TDBText;
    WorkDate: TDBText;
    Due: TLabel;
    FunctionKeyPanel: TPanel;
    F5Label: TLabel;
    F6Label: TLabel;
    F7Label: TLabel;
    F8Label: TLabel;
    F9Label: TLabel;
    LatitudeLabel: TLabel;
    LongitudeLabel: TLabel;
    Latitude: TDBText;
    Longitude: TDBText;
    IDLabel: TLabel;
    TicketType: TDBText;
    HistoryTab: TTabSheet;
    HistPanel: TPanel;
    HistoryPages: TPageControl;
    LocateStatusTab: TTabSheet;
    AssignmentsTab: TTabSheet;
    ResponsesTab: TTabSheet;
    BillingTab: TTabSheet;
    TicketVersionsTab: TTabSheet;
    LocateStatusPanel: TPanel;
    TicketVersions: TDBISAMQuery;
    Assignments: TDBISAMTable;
    Responses: TDBISAMTable;
    LocateStatus: TDBISAMTable;
    LocateStatusSource: TDataSource;
    TicketVersionSource: TDataSource;
    BillingSource: TDataSource;
    ResponseSource: TDataSource;
    AssignmentSource: TDataSource;
    AssignmentsPanel: TPanel;
    ResponsesPanel: TPanel;
    BillingPanel: TPanel;
    VersionsPanel: TPanel;
    LocateLookup: TDBISAMTable;
    ClientLookup: TDBISAMTable;
    EmployeeLookup: TDBISAMTable;
    HoursUnitsButton: TButton;
    HPReason: TDBISAMQuery;
    ReferenceLookup: TDBISAMTable;
    PrintButton: TButton;
    StatusAsAssignedLocatorBox: TCheckBox;
    StatusList: TDBISAMQuery;
    StatusSource: TDataSource;
    AttachmentsTab: TTabSheet;
    AttachmentsFrame: TAttachmentsFrame;
    Acknowledged: TDBISAMTable;
    AcknowledgeSource: TDataSource;
    AcknowledgementTab: TTabSheet;
    AcknowledgementPanel: TPanel;
    TicketVersionImageMemo: TDBMemo;
    AreaNameLabel: TLabel;
    AreaName: TDBText;
    TicketID: TDBEdit;
    CallCenter: TDBEdit;
    CenterLabel: TLabel;
    Billing: TDBISAMQuery;
    ReasonCheckListPanel: TPanel;
    HpReasonCheckList: TCheckListBox;
    HpMultiReasonQuery: TDBISAMQuery;
    PlatButton: TButton;
    FTTPLabel: TLabel;
    RouteAreaID: TComboBox;
    PhotoAttachmentsTab: TTabSheet;
    PhotoAttachmentFrame: TPhotoAttachmentFrame;
    TicketInfoTab: TTabSheet;
    TicketInfo: TDBISAMTable;
    TicketInfoSource: TDataSource;
    TicketInfoPanel: TPanel;
    JobsiteArrivalTab: TTabSheet;
    JobsiteArrivalSource: TDataSource;
    ArriveButtonPanel: TPanel;
    ArriveButton: TButton;
    FacilityTab: TTabSheet;
    TopFacilityPanel: TPanel;
    TicketFacilitiesPanel: TPanel;
    LocateFacilitySource: TDataSource;
    LocateFacility: TDBISAMQuery;
    LocateFacilityDS: TDataSource;
    LocateFacilityTab: TTabSheet;
    AddFacilityPanel: TPanel;
    Label5: TLabel;
    FacilityClient: TComboBox;
    Label2: TLabel;
    FacilityType: TComboBox;
    Label3: TLabel;
    FacilityMaterial: TComboBox;
    SizeLabel: TLabel;
    FacilitySize: TComboBox;
    Label6: TLabel;
    FacilityPressure: TComboBox;
    Label4: TLabel;
    AddHowMany: TSpinEdit;
    AddFacilityButton: TButton;
    DeleteFacilityButton: TButton;
    NoFacilityEditLabel: TLabel;
    PanelArrivalButtons: TPanel;
    ButtonDeleteArrival: TButton;
    ButtonAddNewArrival: TButton;
    PanelArrivalModify: TPanel;
    Label1: TLabel;
    ButtonNewArrivalOK: TButton;
    ButtonNewArrivalCancel: TButton;
    LabelLocInv: TLabel;
    LocatorBox: TEmployeeSelect;
    Actions: TActionList;
    DeleteArrivalAction: TAction;
    TicketSummaryTab: TTabSheet;
    Label7: TLabel;
    SummaryWorkAddressNumber: TDBText;
    SummaryWorkAddressStreet: TDBText;
    Label8: TLabel;
    SummaryWorkCross: TDBText;
    SummaryMapPage: TDBText;
    Label12: TLabel;
    SummaryWorkCity: TDBText;
    Label9: TLabel;
    Label10: TLabel;
    SummaryWorkState: TDBText;
    SummaryWorkCounty: TDBText;
    Label11: TLabel;
    ArriveForMoreDetailsLabel: TLabel;
    Label14: TLabel;
    SummaryCallerContact: TDBText;
    SummaryCallerPhone: TDBText;
    Label15: TLabel;
    SummaryCallerAltContact: TDBText;
    SummaryCallerAltPhone: TDBText;
    Label16: TLabel;
    SummaryDueDate: TDBText;
    SummaryWorkLatLabel: TLabel;
    SummaryWorkLat: TDBText;
    SummaryWorkLongLabel: TLabel;
    SummaryWorkLong: TDBText;
    EmailPanel: TPanel;
    ResponsesHeader: TPanel;
    EmailHeader: TPanel;
    ResponsesSplitter: TSplitter;
    EmailResults: TDBISAMTable;
    EmailResultsSource: TDataSource;
    TicketAlertLabel: TLabel;
    PlatAddinButton: TButton;
    WorkDescriptionTab: TTabSheet;
    TicketWorkTabFrame: TTicketWorkFrame;
    FollowupButton: TBitBtn;
    FollowupPanel: TPanel;
    FollowupTransmitDate: TLabel;
    FollowupDueDateLabel: TLabel;
    FollowupTransmitDateLabel: TLabel;
    FollowupTicketTypeLabel: TLabel;
    FollowupTicketType: TComboBox;
    FollowupPanelButtonPanel: TPanel;
    FollowupPanelButtonPanelBevel: TBevel;
    CreateFollowupButton: TButton;
    CancelFollowupButton: TButton;
    FollowupPanelTopPanel: TPanel;
    DetailFollowupTicketTypeLabel: TLabel;
    DetailFollowupTicketType: TLabel;
    AssignmentsGrid: TcxGrid;
    AssignmentsGridView: TcxGridDBTableView;
    AssignColAssignmentID: TcxGridDBColumn;
    AssignColLocateID: TcxGridDBColumn;
    AssignColClientName: TcxGridDBColumn;
    AssignColClientCode: TcxGridDBColumn;
    AssignColLocatorID: TcxGridDBColumn;
    AssignColInsertDate: TcxGridDBColumn;
    AssignColAddedBy: TcxGridDBColumn;
    AssignColActive: TcxGridDBColumn;
    AssignmentsGridLevel: TcxGridLevel;
    AssignColstatus: TcxGridDBColumn;
    AssignColDownloadDate: TcxGridDBColumn;
    AssignColShortName: TcxGridDBColumn;
    ResponsesGrid: TcxGrid;
    ResponsesGridView: TcxGridDBTableView;
    RespColResponseID: TcxGridDBColumn;
    RespColLocateID: TcxGridDBColumn;
    RespColClientCode: TcxGridDBColumn;
    RespColClientName: TcxGridDBColumn;
    RespColStatus: TcxGridDBColumn;
    RespColResponseSent: TcxGridDBColumn;
    RespColSuccess: TcxGridDBColumn;
    RespColReply: TcxGridDBColumn;
    ResponsesGridLevel: TcxGridLevel;
    RespColResponseDate: TcxGridDBColumn;
    RespColCallCenter: TcxGridDBColumn;
    EmailsGrid: TcxGrid;
    EmailsGridView: TcxGridDBTableView;
    ColEmailEqrID: TcxGridDBColumn;
    ColEmailTicketID: TcxGridDBColumn;
    ColEmailSentDate: TcxGridDBColumn;
    ColEmailAddress: TcxGridDBColumn;
    ColEmailStatus: TcxGridDBColumn;
    ColEmailErrorDetails: TcxGridDBColumn;
    EmailsGridLevel: TcxGridLevel;
    LocateStatusGrid: TcxGrid;
    LocateStatusGridView: TcxGridDBTableView;
    colLocStLocateID: TcxGridDBColumn;
    colLocStClientCode: TcxGridDBColumn;
    colLocStClientName: TcxGridDBColumn;
    colLocStStatusDate: TcxGridDBColumn;
    colLocStStatus: TcxGridDBColumn;
    colLocStMarkType: TcxGridDBColumn;
    colLocStHighProfile: TcxGridDBColumn;
    colLocStQtyMarked: TcxGridDBColumn;
    colLocStStatusedBy: TcxGridDBColumn;
    colLocStShortName: TcxGridDBColumn;
    colLocStStatusedHow: TcxGridDBColumn;
    LocateStatusGridLevel: TcxGridLevel;
    colLocStEntryDate: TcxGridDBColumn;
    colLocStInsertDate: TcxGridDBColumn;
    colLocStRegularHours: TcxGridDBColumn;
    colLocStOvertimeHours: TcxGridDBColumn;
    BillingGrid: TcxGrid;
    BillingGridView: TcxGridDBTableView;
    ColBillBill: TcxGridDBColumn;
    ColBillLocateID: TcxGridDBColumn;
    ColBillClientCode: TcxGridDBColumn;
    ColBillClientName: TcxGridDBColumn;
    ColBillBillingCC: TcxGridDBColumn;
    ColBillStatus: TcxGridDBColumn;
    ColBillQtyMarked: TcxGridDBColumn;
    ColBillClosedDate: TcxGridDBColumn;
    ColBillInitialStatusDate: TcxGridDBColumn;
    ColBillBillCode: TcxGridDBColumn;
    BillingGridLevel: TcxGridLevel;
    ColBillBucket: TcxGridDBColumn;
    ColBillPrice: TcxGridDBColumn;
    ColBillRate: TcxGridDBColumn;
    ColBillInvoiceID: TcxGridDBColumn;
    ColBillBillID: TcxGridDBColumn;
    TicketVersionsGrid: TcxGrid;
    TicketVersionsGridView: TcxGridDBTableView;
    ColTktVerCallCenter: TcxGridDBColumn;
    ColTktVerActive: TcxGridDBColumn;
    ColTktVerVersionID: TcxGridDBColumn;
    ColTktVerTicketRevision: TcxGridDBColumn;
    ColTktVerTicketType: TcxGridDBColumn;
    ColTktVerTransmitDate: TcxGridDBColumn;
    ColTktVerArrivalDate: TcxGridDBColumn;
    ColTktVerProcessedDate: TcxGridDBColumn;
    TicketVersionsGridLevel: TcxGridLevel;
    cxGrid1: TcxGrid;
    AcknowledgmentGridView: TcxGridDBTableView;
    ColAckHasAck: TcxGridDBColumn;
    ColAckShortName: TcxGridDBColumn;
    ColAckTicketID: TcxGridDBColumn;                   
    ColAckInsertDate: TcxGridDBColumn;
    ColAckAckDate: TcxGridDBColumn;
    ColAckAckEmpID: TcxGridDBColumn;
    ColAckDeltaStatus: TcxGridDBColumn;
    ColAckLocakKey: TcxGridDBColumn;
    ColAckLocalStringKey: TcxGridDBColumn;
    AcknowledgmentGridLevel: TcxGridLevel;
    TicketInfoGrid: TcxGrid;
    TicketInfoGridView: TcxGridDBTableView;
    TicketInfoGridLevel: TcxGridLevel;
    ArrivalGrid: TcxGrid;
    ArrivalGridView: TcxGridDBTableView;
    ColArrArrivalID: TcxGridDBColumn;
    ColArrArrivalDate: TcxGridDBColumn;
    ColArrEmpID: TcxGridDBColumn;
    ColArrShortName: TcxGridDBColumn;
    ColArrEntryMethod: TcxGridDBColumn;
    ColArrAddedByName: TcxGridDBColumn;
    ArrivalGridLevel: TcxGridLevel;
    ColArrActive: TcxGridDBColumn;
    ColArrDeletedBy: TcxGridDBColumn;
    ColArrDeletedDate: TcxGridDBColumn;
    FacilityHistoryGrid: TcxGrid;
    FacilityHistoryGridView: TcxGridDBTableView;
    ColFacHistLocateFacilityID: TcxGridDBColumn;
    ColFacHistLocateID: TcxGridDBColumn;
    ColFacHistClientName: TcxGridDBColumn;
    ColFacHistTypeDesc: TcxGridDBColumn;
    ColFacHistMaterialDesc: TcxGridDBColumn;
    ColFacHistSizeDesc: TcxGridDBColumn;
    ColFacHistPressureDesc: TcxGridDBColumn;
    ColFacHistOffset: TcxGridDBColumn;
    ColFacHistAddedDate: TcxGridDBColumn;
    FacilityHistoryGridLevel: TcxGridLevel;
    ColFacHistAddedByName: TcxGridDBColumn;
    ColFacHistDeletedByName: TcxGridDBColumn;
    ColFacHistDeletedDate: TcxGridDBColumn;
    ColFacHistActive: TcxGridDBColumn;
    FacilityGrid: TcxGrid;
    FacilityGridView: TcxGridDBTableView;
    ColFacClientName: TcxGridDBColumn;
    ColFacFacilityType: TcxGridDBColumn;
    ColFacMaterial: TcxGridDBColumn;
    ColFacFacilitySize: TcxGridDBColumn;
    ColFacFacilityPressure: TcxGridDBColumn;
    ColFacOffset: TcxGridDBColumn;
    ColFacAddedDate: TcxGridDBColumn;
    ColFacLocateFacilityID: TcxGridDBColumn;
    ColFacLocateID: TcxGridDBColumn;
    ColFacAddedBy: TcxGridDBColumn;
    ColFacDeletedByName: TcxGridDBColumn;
    ColFacDeletedDate: TcxGridDBColumn;
    ColFacActive: TcxGridDBColumn;
    FacilityGridLevel: TcxGridLevel;
    LocateGrid: TcxGrid;
    LocateGridLevel: TcxGridLevel;
    LocateGridView: TcxGridDBBandedTableView;
    ColLocAlert2: TcxGridDBBandedColumn;
    ColLocClientCode: TcxGridDBBandedColumn;
    ColLocClientName: TcxGridDBBandedColumn;
    ColLocMarkType: TcxGridDBBandedColumn;
    ColLocQtyMarked: TcxGridDBBandedColumn;
    ColLocStatus: TcxGridDBBandedColumn;
    ColLocLengthMarked: TcxGridDBBandedColumn;
    ColLocLengthTotal: TcxGridDBBandedColumn;
    ColLocClosed: TcxGridDBBandedColumn;
    ColLocClosedDate: TcxGridDBBandedColumn;
    ColLocShortName: TcxGridDBBandedColumn;
    ColLocHighProfile: TcxGridDBBandedColumn;
    ColLocHighProfileReason: TcxGridDBBandedColumn;
    ColLocHighProfilePopup: TcxGridDBBandedColumn;
    ColLocLocatePlats: TcxGridDBBandedColumn;
    ColLocLocateID: TcxGridDBBandedColumn;
    ArrivalDate: TcxDateEdit;
    ArrivalTime: TcxTimeEdit;
    cxGridViewRepository: TcxGridViewRepository;
    HPReasonView: TcxGridDBTableView;
    StatusListView: TcxGridDBTableView;
    ColHPReasRefID: TcxGridDBColumn;
    ColHPReasCode: TcxGridDBColumn;
    ColHPReasDescription: TcxGridDBColumn;
    ColHPReasSortBy: TcxGridDBColumn;
    ColStatusListStatus: TcxGridDBColumn;
    ColStatusListStatusName: TcxGridDBColumn;
    FollowupPopupEdit: TcxPopupEdit;
    FollowupDueDate: TcxDateEdit;
    FollowupDueTime: TcxTimeEdit;
    HPReasonSource: TDataSource;
    FTTPStaticLabel: TLabel;
    NotesPanel: TPanel;
    TicketNotesFrame: TBaseNotesFrame;
    NoteHeaderPanel: TPanel;
    EnterNoteLabel: TLabel;
    NoteSource: TComboBox;
    CENButtonPanel: TPanel;
    CENButton: TButton;
    DateColumn: TcxGridDBColumn;
    DescriptionColumn: TcxGridDBColumn;
    AnswerColumn: TcxGridDBColumn;
    LastSMDSource: TDataSource;
    LastSMD: TDBISAMQuery;
    lblLastScheduledDate: TLabel;
    dbLastSMD: TDBText;
    NoteSubTypeCombo: TComboBox;
    Label17: TLabel;
    lblEnterNoteFor: TLabel;
    btnReturnToWO: TButton;
    EFDButtonPanel: TPanel;
    btnEfdColl: TButton;
    eprGrid: TcxGrid;
    eprRespView: TcxGridDBTableView;
    eprGridLevel: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    qryEprHistory: TDBISAMQuery;
    dsEprHistory: TDataSource;
    eprRespViewHistID: TcxGridDBColumn;
    eprRespViewSentDate: TcxGridDBColumn;
    eprRespViewEmail: TcxGridDBColumn;
    eprRespViewStatus: TcxGridDBColumn;
    eprRespViewLink: TcxGridDBColumn;
    btnTktSafety: TBitBtn;
    btnProjectLocate: TButton;
    ColProject: TcxGridDBBandedColumn;
    ColParentClientCode: TcxGridDBBandedColumn;
    lblProjectLocatesText: TLabel;
    CombinedAddressLbl: TLabel;
    TicketNumber: TDBText;
    btnServiceCard: TBitBtn;
    btnMap: TBitBtn;
    btnAddr: TBitBtn;
    cxHyperLinkEdit1: TcxHyperLinkEdit;
    ColEmailBody: TcxGridDBColumn;
    EdtDueDate: TcxDBDateEdit;
    lblRS: TLabel;
    lblLocateMessage: TLabel;
    Label13: TLabel;
    RiskScore: TDBText;
    TicketRiskScoreTab: TTabSheet;
    RiskScoreGrid: TcxGrid;
    RiskScoreView: TcxGridDBTableView;
    ColTicketRiskID: TcxGridDBColumn;
    ColTicketVersionID: TcxGridDBColumn;
    ColLocateClient: TcxGridDBColumn;
    ColTicketRevision: TcxGridDBColumn;
    ColRiskSource: TcxGridDBColumn;
    ColRiskScoreType: TcxGridDBColumn;
    ColRiskScore: TcxGridDBColumn;
    ColTransmitDate: TcxGridDBColumn;
    RiskScoreLevel: TcxGridLevel;
    TicketRiskQry: TDBISAMQuery;
    TicketRiskDS: TDataSource;
    UpdatedTicketsLabel: TLabel;
    updTicketPopupMenu: TPopupMenu;
    CopyTicket1: TMenuItem;
    UpdateRelatedTickets: TDBText;
    btnReturn: TBitBtn;
    PreserveClosedDateCBox: TCheckBox;
    FlowPanel1: TPanel;
    btnWaldoAddr: TSpeedButton;
    btnWaldoLatLng: TSpeedButton;
    Label18: TLabel;
    Label19: TLabel;
    mnuPrintTktVer: TPopupMenu;
    PrintTicketVersionImage1: TMenuItem;
    btnPelican: TButton;   //qm-651 sr
    statuslist_sp: TDBISAMQuery;  //QM-635 EB Stsatus Plus
    StatusSource_sp: TDataSource;
    TicketAlertsTab: TTabSheet;
    TicketAlertsFrame: TTicketAlertsFrame;
    colLocStStatusChangedBy: TcxGridDBColumn;
    colLocStChangedByNAME: TcxGridDBColumn;
    StreetLabel: TLabel;
    VersionSplitter: TSplitter; //QM-635 EB Stsatus Plus
    procedure DoneButtonClick(Sender: TObject);
    procedure AddLocateButtonClick(Sender: TObject);
    procedure ColLocStatusPropertiesCloseUp(Sender: TObject);
    procedure ColLocStatusPropertiesKeyPress(Sender: TObject; var Key: Char);  //QM-703 EB
    procedure TicketPagesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HoursUnitsButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AssignColAddedByGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: String);
    procedure PrintButtonClick(Sender: TObject);
    function RecordIsReadOnly(ARecord: TcxCustomGridRecord): Boolean;
    procedure StatusListFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure LocateDSDataChange(Sender: TObject; Field: TField);
    procedure LocatePlatButtonClick(Sender: TObject);
    procedure ArriveButtonClick(Sender: TObject);
    procedure DeleteFacilityButtonClick(Sender: TObject);
    procedure FacilityTypeChange(Sender: TObject);
    procedure FacilityTabShow(Sender: TObject);
    procedure AddFacilityButtonClick(Sender: TObject);
    procedure LocateLookupBeforeOpen(DataSet: TDataSet);
    procedure FacilityClientSelect(Sender: TObject);
    procedure ButtonNewArrivalOKClick(Sender: TObject);
    procedure ButtonNewArrivalCancelClick(Sender: TObject);
    procedure ButtonAddNewArrivalClick(Sender: TObject);
    procedure DeleteArrivalActionExecute(Sender: TObject);
    procedure ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ManageAddinButtonClick(Sender: TObject);
    procedure TicketVersionsCalcFields(DataSet: TDataSet);
    procedure ResponsesTabResize(Sender: TObject);
    procedure TicketPagesChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure FollowupButtonClick(Sender: TObject);
    procedure CreateFollowupButtonClick(Sender: TObject);
    procedure CancelFollowupButtonClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure LocateGridViewFocusedItemChanged(
      Sender: TcxCustomGridTableView; APrevFocusedItem,
      AFocusedItem: TcxCustomGridTableItem);
    procedure LocateGridViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure LocateGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure LocateGridViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure LocateGridViewEditValueChanged(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure FollowupPopupEditPropertiesCloseUp(Sender: TObject);
    procedure FollowupPopupEditPropertiesCloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure ColLocHighProfilePopupPropertiesCloseUp(Sender: TObject);
    procedure ColLocHighProfilePopupPropertiesInitPopup(Sender: TObject);
    procedure ColLocLocatePlatsPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ColLocLengthMarkedPropertiesInitPopup(Sender: TObject);
    procedure ColLocAlert2CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ColLocLengthMarkedPropertiesCloseUp(Sender: TObject);
    procedure ColLocHighProfilePropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure ColFacAddedDateGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: String);
    procedure ColArrActiveGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: String);
    procedure TicketNotesFrameAddNoteButtonClick(Sender: TObject);
    procedure HistoryPagesResize(Sender: TObject);
    procedure CENButtonClick(Sender: TObject);

    procedure NoteSourceChange(Sender: TObject);
    procedure NoteSubTypeComboChange(Sender: TObject);
    procedure ColLocLengthMarkedPropertiesChange(Sender: TObject);
    procedure btnReturnToWOClick(Sender: TObject);
    procedure btnEfdCollClick(Sender: TObject);
    procedure eprRespViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btnTktSafetyClick(Sender: TObject);   //QMANTWO-297
    procedure btnProjectLocateClick(Sender: TObject);
    procedure LocateGridViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);   //QMANTWO-297
    procedure ItemDblClick(Sender: TObject);
    procedure btnServiceCardClick(Sender: TObject);
    procedure btnMapClick(Sender: TObject);
    procedure EmailsGridViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure TicketNotesFrameClick(Sender: TObject);
    procedure EdtDueDatePropertiesCloseUp(Sender: TObject);
    procedure UpdateRelatedTicketsDblClick(Sender: TObject);
    procedure btnReturnClick(Sender: TObject);
    procedure btnWaldoAddrClick(Sender: TObject);
    procedure CopyTicket1Click(Sender: TObject);
    procedure PrintTicketVersionImage1Click(Sender: TObject);
    procedure IDCopyGridViewCellClick(Sender: TcxCustomGridTableView; //QM-621 EB Copy IDs on History
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btnPelicanClick(Sender: TObject);   //qm-651 sr
    procedure ColLocStatusPropertiesInitPopup(Sender: TObject);
    procedure StatusAsAssignedLocatorBoxClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);  //QM-635 EB Status Plus

  private
    FormStatus: TFormStatus; //QM-577 EB
    FOnDoneWithTicket: TNotifyEvent;
    FOnShowTicketWork: TNotifyEvent;
    FTicketID: Integer;
    FAssociatedWOID: Integer;
    FLocateWorkForm: TLocateWorkForm;
    FReadOnlyCache: TList;
    FCurrentStatusOfSelectedLocate: string;
    FMultiReasonMode: Boolean;
    FReasonCodes: TStrings;
    FAttachingFilesNow: Boolean;
    FStatusedHow: string;
    FOnTicketPrint: TItemSelectedEvent;
    FAttachmentAddin: TAttachmentsAddinManager;
    FPlatAddin: TPlatAddinManager;
    FTicketHistoryDownloaded: Boolean;
    FLocateLengthEditor: TLocateLengthEditor;
    FMarkableUnitStatusCodes: TStringArray;
    FSavedTicketTabIndex: Integer;
    FFollowupTransmitDate: TDateTime;
    FFollowupTicketState: TFollowupTicketState;
    FCENButtonMode : TCENButtonMode;
    FNoteSubTypeList :TCodeLookupList;  //Associate object with the combobox
    //Ken Funk 2016-01-14 Following two variables needed for Facilities Check
    FCallCentersRequireFacilitiesTab: TStringList;
    FLocationMarkStatusList: TStringList;
    overRideClients : string;   //QMANTWO-208
    fProjectLocateForm : TAddProjectLocateForm;  //QMANTWO-616 EB Designing of Multi-locator mode
    fProjLocatorAssignments : TLocatorAssignments;  //QMANTWO-616 EB Designing of Multi-locator mode
    fProjParents : TStringList;
 //   fRouteOrderSkip: TRouteOrderSkip; //QMANTWO-775 EB - This will be defaulted to Skipped = False
    fLocateChanged: Boolean; //QMANTWO-775 EB - Need to flag any other changes in order to determine whether to display route_order override form

    fCanEditTicketDueDateStatus: string;  //QM-216 Editable Due Date
    fCanEditTicketDueDate: boolean;
    fOldDueDateValue: TDateTime;
    fDueDateChanged: boolean;
    fInitialDueDateLoaded: boolean;
    fCurClientStatusRequires: TClientStatusRequires; //QM-282 EB
    fRecentStatusChanges: Array of TClientStatusRequires; //QM-282 EB
    fretFromUpdateTktID:integer;  //qm-397  ticketID of ticket that has Update_of  sr
    ShowWaldo:Boolean;
    fRestrictedStatusus: string;  //qm-449  sr
    procedure RefreshStatusList;
    procedure RefreshMarkableUnitStatusCodes;
    procedure ShowTicketImage;
    procedure ShowTicketHistory;
    procedure ShowTicketAlerts;   //QM-771 Ticket Alerts EB
    function GetLocateFilterString: string;
    procedure SetupDataSets;
    procedure HideGridIDColumns;
    procedure EnableDisableControls;
    function SelectedLocateStatus: string;
    procedure DisplayWorkDialog(Sender: TObject; TicketID, LocateID: Integer;
      CanAddHours, CanAddUnits: Boolean);
    procedure UpdateHoursButtonVisibility;
    function HaveAUnitsOrHourlyStatusLocate: Boolean;
    function HaveUnsavedLocate: Boolean;
    procedure SetHighProfileState;
    procedure UpdateDetailFollowupTicketType;
    procedure UpdateFollowupButton;
    procedure ResetFollowupPopupControls;
    procedure SetupFollowupTicket;
    procedure SaveFollowupTicketState;
    procedure RefreshLocateReadOnlyCache;
    procedure UpdateCurrentGridRow;
    procedure OpenMultiReasonQueryToCenter;
    procedure VerifyReasonEntered;
    procedure VerifyMarkTypeEntered;
    procedure ShowLocatePlatEditor;
    procedure SetDefaultMarkType;
    procedure SetupAreaEditControl;
    procedure VerifyJobsiteArrival;
    procedure ValidateFollowupTicket;
    procedure VerifyFollowupTicketCreated;
    procedure SetupFacilitiesTab;
    procedure SetupAttachmentsTab;  //QM-255 EB
    procedure UpdateDependantFacilityCombos;
    procedure ResetFacilityControls;
    procedure PopulateNoteSourceCombo;
    procedure SetNotesPermissions; //QM-188 EB
    procedure GetNoteSource;
    procedure WMStiLaunchNotification(var Message: TMessage); message WM_STI_LAUNCH;
    procedure SetupArrivalControls;
    procedure PrepareTicketAddins;
    function HaveTicket: Boolean;
    function StatusAllowsUnits(const Status: string): Boolean;
    procedure SetupLengthColumn;
    procedure ShowTicketSummary;
    procedure HideTicketSummary;
    procedure EnableNotes(const Enabled: Boolean);
    function ShowingTicketSummary: Boolean;
    procedure SetupImageMemoDisplay(Memo: TCustomMemo; const TicketIsXml: Boolean);
    procedure SetArriveButtonEnabled(const Enabled: Boolean);
    procedure ShowTicketWorkDescription;
    procedure CreateFollowupTicket;
    procedure HideShowCENorTEMABtn; //QM-53  EB
    procedure OpenTicketInfo(TicketID: integer);
    procedure PopulateNoteSubType(NoteType: Integer);
    //Ken Funk 2016-01-13 Following two procedures added to fill vars
    procedure PopulateCallCentersRequireFacilitiesTab;
    procedure PopulateLocationMarkStatusList;
    function VerifyFacilitiesTab: Boolean;
    procedure ClearAssociatedWO;
    procedure HideEFDButtonForNonEFDColl; //QMANTWO-297
    function IsLocateProjectable: boolean; //QMANTWO-616 EB
    function IsProjectTicket: boolean;
    function InitLoadParentClients(ClientCode: string): integer;  //QMANTWO-616 EB
    function IsAParentLocate(Rec: TcxCustomGridRecord ): boolean; overload;//QMANTWO-616 EB
    function IsAParentLocate(ClientCode: string): boolean; overload;
    procedure SearchForURL;    //QMANTWO-616 EB
//    procedure RunCEN;  QM-700
    procedure RunTEMA;
    procedure EnableEditDueDate(IsEditable: Boolean); //QM-216 EB
    procedure SetDMDueDateChanged(Value: Boolean); //QM-216 EB
    procedure CheckDueDate;
    function  HasTicketDueDateOverride(var pCanEdit: boolean; var pRemainingAttempts: integer): Boolean;
    procedure InitStatusChangeArray;  //QM-282 EB
    procedure AddLocate_toStatusChangeArray; //QM-282 EB
    procedure CreateNoteForStatusChange(pTicketID: integer);
    procedure SetFormStatus(Value: Boolean = False); //QM-577

  public
    LeavingTicket: boolean;
    procedure DoneWithTicket(StayOnScreen: Boolean = False);
    procedure GoToTicket(ID: Integer; ComingFromManagementForm: Boolean; WorkOrderID: integer = 0; retFromUpdateTktID: integer = 0);   //QMANTWO-296 Added Default Parameter for WorkOrderID

    property CanEditTicketDueDate: boolean   read fCanEditTicketDueDate write EnableEditDueDate; //fCanEditTicketDueDate; //QM-216  SR
    property CanEditTicketDueDateStatus:string  read fCanEditTicketDueDateStatus write fCanEditTicketDueDateStatus;  //QM-216  SR
    property DueDateChanged: boolean read fDueDateChanged write SetDMDueDateChanged;   //QM-216 EB
    property OnDoneWithTicket: TNotifyEvent read FOnDoneWithTicket write FOnDoneWithTicket;
    procedure LeavingNow; override;
    procedure ShowTicketOnMessage(var Msg: TMessage); message WM_DISPLAYLOCATEHOURS;
    procedure ActivatingNow; override;
    function ExecuteAttachmentProcess(Mode: TAttachmentMode; const Param: string = ''): Boolean;
    function OpenProjectMultiLocatorList: boolean;

    property OnTicketPrint: TItemSelectedEvent read FOnTicketPrint write FOnTicketPrint;
    procedure UpdateCameraStatus;
    property OnShowTicketWork: TNotifyEvent read FOnShowTicketWork write FOnShowTicketWork;
    property RestrictedStatusus:string read fRestrictedStatusus write fRestrictedStatusus;
    function CheckNotesStatus: boolean; //QM-191 EB Unsaved Notes
    procedure LoadRequirements(Index: integer; CurRowStatus: string);  //QM-282 EB
    function RequiredItemsNeededBeforeDone(var InfoMessageTxt: string): boolean;  //QM-282 EB

    {QM-635 Status Plus EB}
    function CheckForStatusPlus: boolean;

    function CheckRouteOrder:boolean;  //QM-775 RESURRECTION  EB
//    property RouteOrderSkip: TRouteOrderSkip read fRouteOrderSkip write fRouteOrderSkip;   //QMANTWO-775
  end;

implementation

uses
  {$IF CompilerVersion <= 20}
  ControlResizeBugFix,
  {$IFEND}
  Variants, AddLocate, OdHourglass, OdDBUtils, OdExceptions, ClipBrd,
  OdVclUtils, SharedImages, LocatePlats, DBISAMLb,
  OdDeviceNotification, StrUtils,
  OdDBISAMUtils, OdCxUtils, DateUtils, Types, Hashes, ShellAPI, MainFU,
  ClientInfo, LocalPermissionsDMu, LocalEmployeeDmu,
  xqFadeBox, eMailBodyViewer;


{$R *.DFM}

procedure TTicketDetailForm.GoToTicket(ID: Integer; ComingFromManagementForm: Boolean; WorkOrderID,retFromUpdateTktID: Integer);
var //qm-397 added retFromUpdateTktID  this is the ticket that has the Update_of  sr
  Geocoded: Boolean;
  GeocodeColor: TColor;
  FullAddress: string;

  ICanEditDD: Boolean;            //QM-641 EB
  RemainingAttemptsDD: Integer;   //QM-641 EB
  PreviousDDOverride: Boolean;    //QM-641 EB
  CombinedStreetNum: String;      //QM-997 EB
begin
  fretFromUpdateTktID :=0;
  CombinedStreetNum := '';        //QM-997 EB
  CanEditTicketDueDate := False;
  DM.TicketDueDateChanged := False; //EB Force twice
  fLocateChanged := False; //QMANTWO-775
  lblProjectLocatesText.Visible := False;
  FTicketID := ID;
  FAssociatedWOID := WorkOrderID;  //QMANTWO-296
  FStatusedHow := 'Detail';
  if ComingFromManagementForm then
    FStatusedHow := 'Ticket Man';
  if ID > 0 then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeViewTicket, IntToStr(ID))
  else
    // Viewing an unsynced manual ticket, so no valid id to log
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeViewTicket, '');

  TicketDS.DataSet := DM.Ticket;
  LocateDS.DataSet := DM.LocateData;


  LocateFacilityDS.DataSet := DM.LocateFacilityData;

  DM.GoToTicket(ID);
  {QM-997 EB Combine the work address numbers and street }
  if (TicketDS.DataSet.FieldByName('work_address_number').AsString <> '') and
     (TicketDS.DataSet.FieldByName('work_address_number_2').AsString <> '') then
    CombinedStreetNum := TicketDS.DataSet.FieldByName('work_address_number').AsString + '-' +
                         TicketDS.DataSet.FieldByName('work_address_number_2').AsString
  else if (TicketDS.DataSet.FieldByName('work_address_number').AsString <> '') then
    CombinedStreetNum := TicketDS.DataSet.FieldByName('work_address_number').AsString
  else
    CombinedStreetNum := TicketDS.DataSet.FieldByName('work_address_number_2').AsString;
    
  StreetLabel.Caption := CombinedStreetNum + ' ' + TicketDS.DataSet.FieldByName('work_address_street').AsString;


  FormStatus.DataLoaded := True;  //QM-860 EB Flagging when we have DM activated
  fOldDueDateValue := TicketDS.DataSet.FieldByName('due_date').AsDateTime;  //QM-216 EB trial

  PreviousDDOverride := HasTicketDueDateOverride(ICanEditDD, RemainingAttemptsDD);  //QM-641 EB
  if ICanEditDD and PreviousDDOverride then
    CanEditTicketDueDate := True;

  FUllAddress := DM.GetFullTicketAddress(ID);
  CombinedAddressLbl.Caption := FullAddress;
  if FAssociatedWOID > 0 then begin  //QMANTWO-296
    btnReturnToWO.Caption := 'Return to WO#: ' + DM.GetWorkOrderNum(fAssociatedWOID);
    btnReturnToWO.Visible := True;
  end
  else
    btnReturnToWO.Visible := False;

  if retFromUpdateTktID > 0 then
  begin  //QM-397
    fretFromUpdateTktID:= retFromUpdateTktID;
    UpdateRelatedTickets.Visible:=false;
    btnReturn.Visible := True;
    btnReturn.BringToFront;
  end
  else
  begin
    fretFromUpdateTktID:=0;
    btnReturn.Visible := False;
    btnReturn.SendToBack;
  end;

  LockWindowUpdate(TicketPages.Handle);
  try
    if DM.IsTicketDetailHidden then
      ShowTicketSummary
    else
      HideTicketSummary;
  finally
    LockWindowUpdate(0);
  end;

  // := ColLocStatusPropertiesKeyPress;  //QM-703 Status Box Tweak EB
  RefreshStatusList;
  RefreshMarkableUnitStatusCodes;
  RefreshDataSet(HPReason);
  SetupFollowupTicket;

  OpenMultiReasonQueryToCenter;
  FMultiReasonMode := not HpMultiReasonQuery.EOF;
  ColLocHighProfileReason.Visible := not FMultiReasonMode;
  ColLocHighProfilePopup.Visible := FMultiReasonMode;

  StatusAsAssignedLocatorBox.Visible := PermissionsDM.CanI(RightStatusLocatesAsAssigned, False);
  StatusAsAssignedLocatorBox.Checked := StatusAsAssignedLocatorBox.Visible; //QM-804 Update - If they have the permission, default it to checked

  PreserveClosedDateCBox.Visible := PermissionsDM.CanI(RightTicketsPreserveClosedDate); //QM-463 Preserve Closed Date
  PreserveClosedDateCBox.Checked := False;

  SetupAreaEditControl;

  // Highlight the notes tab if we have any notes for this ticket
  if DM.TicketNotesData.IsEmpty then
    NotesTab.ImageIndex := -1
  else
    NotesTab.ImageIndex := ImageIndexNotes;

  TicketNotesFrame.NoteText.Clear;
  //ShowMessage('871: TicketDetail - Before ShowTicketImage');
  ShowTicketImage;

  if StrContains('FTTP', TicketType.Caption) then
    FTTPLabel.Caption := 'Yes'
  else
    FTTPLabel.Caption := 'No';

  TicketAlertLabel.Visible := (DM.Ticket.FieldByName('alert').Value = 'A');
  DM.GetRefCodeList('marktype', TcxComboBoxProperties(ColLocMarkType.Properties).Items);
  AttachmentsFrame.Initialize(qmftTicket, ID);
  TicketNotesFrame.Initialize(qmftTicket, ID, DM.TicketNotesData);

  if DM.Ticket.FieldByName('geocode_precision').IsNull or
    (DM.Ticket.FieldByName('geocode_precision').AsInteger < 0) then begin
    Geocoded := False;
    GeocodeColor := clBtnFace;
  end else begin
    Geocoded := True;
    if DM.Ticket.FieldByName('geocode_precision').AsInteger = 0 then
      GeocodeColor := DM.UQState.ZeroGeocodeMatchesColor
    else if DM.Ticket.FieldByName('geocode_precision').AsInteger <
      HighestGeocodePrecision then
      GeocodeColor := DM.UQState.AcceptableGeocodePrecisionColor
    else
      GeocodeColor := DM.UQState.HighestGeocodePrecisionColor;
  end;

  Latitude.Color := GeocodeColor;
  Longitude.Color := GeocodeColor;
  Latitude.Transparent := not Geocoded;
  Longitude.Transparent := not Geocoded;

  FTicketHistoryDownloaded := False;

  SetupFacilitiesTab;
  SetupAttachmentsTab;
  EnableDisableControls;
  PopulateNoteSourceCombo;
  SetNotesPermissions;
  PopulateNoteSubType(qmftTicket);    //QM-576  sr
  PrepareTicketAddins;
  OpenTicketInfo(FTicketID);
 // OpenAlerts(fTicketID);
  SetArriveButtonEnabled(DM.TicketIsOpen);

  TicketWorkTabFrame.Prepare(FTicketID);
  ShowWaldo :=false;  //qm-449  sr
  InitStatusChangeArray;  //QM-832 EB
  btnWaldoAddr.Visible:=ShowWaldo;  //qm-449  sr
  btnWaldoLatLng.Visible:=ShowWaldo;  //qm-449  sr
  FlowPanel1.Visible:=ShowWaldo;  //qm-449  sr
  HideShowCENorTEMABtn;   //QM-53 EB
  HideEFDButtonForNonEFDColl;
  btnServiceCard.visible := (DM.Ticket.FieldByName('work_state').asString ='IN');  //QMANTWO-753
  TicketImageMemo.Lines.BeginUpdate;    //QM-116  SR
  ShowTicketAlerts; //QM-771 Ticket Alerts EB
  try
    SearchForURL;  //QR-13  SR
    TicketImageMemo.SelStart := 0;    //QM-116  SR
    TicketImageMemo.SelLength := 0;    //QM-116  SR
    TicketImageMemo.Perform(EM_SCROLLCARET, 0,0); //QM-116  SR

  finally
    TicketImageMemo.Lines.EndUpdate;   //QM-116  SR
  end;
  FormStatus.FormReady := True; //QM-860 EB Completing this flaq
end;

procedure TTicketDetailForm.btnReturnClick(Sender: TObject);  //qm-397  sr
var
  Cursor: IInterface; //qm-460 sr
begin
  inherited;
  Cursor := ShowHourGlass; //qm-460 sr
  GoToTicket(fRetFromUpdateTktID,false,0,0);
  btnReturn.SendToBack;
  btnReturn.Visible:=false;
  UpdateRelatedTickets.BringToFront;
  UpdateRelatedTickets.Visible:=true;
end;


procedure TTicketDetailForm.SetupAreaEditControl;
var
  CurrentArea: string;
  CurrentAreaID: Integer;
begin
  RouteAreaID.Visible := False;
  AreaName.Visible := True;  //QM-234 EB
  if not PermissionsDM.CanI(RightTicketsAssignArea) then
    Exit;

  DM.AreaList(RouteAreaID.Items);
  CurrentArea := DM.Ticket.FieldByName('route_area_name').AsString;
  CurrentAreaID := DM.Ticket.FieldByName('route_area_id').AsInteger;
  if (CurrentArea <> '') and (RouteAreaID.Items.IndexOf(CurrentArea) < 0) then
    RouteAreaID.AddItem(CurrentArea, TObject(CurrentAreaID));
  if CurrentAreaID > 0 then
    SelectComboBoxItemFromObjects(RouteAreaID, CurrentAreaID)
  else
    RouteAreaID.Text := '';

  RouteAreaID.Visible := True;
  AreaName.Visible := False;     //QM-234 EB
end;

procedure TTicketDetailForm.DoneButtonClick(Sender: TObject);
var
  Cursor: IInterface;

begin
  Cursor := ShowHourGlass;
  DoneWithTicket;
end;

procedure TTicketDetailForm.AddLocateButtonClick(Sender: TObject);
var
  AltEmpID, LocateClientID: integer;
begin
  AltEmpID := DM.EmpID;

  {If User has permissions to add DIFFERENT locator to a locate}
  if (PermissionsDM.CanI(RightTicketsAddLocatesWithLocator, False)) then begin  //QM-142  EB
    try
      AltEmpID := StrToInt(PermissionsDM.GetLimitation(RightTicketsAddLocatesWithLocator));
      if not(AltEmpID > 0) then
        AltEmpID := DM.EmpID;
    except
      AltEmpID := DM.EmpID;
    end;

    If ExecuteAddLocateWLocatorDialog(DM.Ticket.FieldByName('ticket_format').AsString, FTicketID, AltEmpID, LocateClientID) then begin     //QM-142  EB
     DM.MyClients.Add(IntToStr(LocateClientID)); //QM-282 EB
     AddLocate_toStatusChangeArray;
     RefreshStatusList;  //QM-282 EB
     LocateGrid.Refresh;
     PopulateNoteSourceCombo;
    end;
  end

  {User does not have permission to add DIFFERENT locator to a locate}
  else if ExecuteAddLocateDialog(DM.Ticket.FieldByName('ticket_format').AsString, FTicketID, LocateClientID) then begin
    DM.MyClients.Add(IntToStr(LocateClientID)); //QM-282 EB
    AddLocate_toStatusChangeArray;
    RefreshStatusList;  //QM-282 EB
    LocateGrid.Refresh;
    PopulateNoteSourceCombo;
  end;
  RefreshLocateReadOnlyCache;
end;

procedure TTicketDetailForm.DoneWithTicket(StayOnScreen: Boolean);
var
  LocateEdited: Boolean;
  BeforeExitMsg: string;
  lHasAutoAttach: boolean;
begin
  lHasAutoAttach := False;
  LeavingTicket := True;
  If not CheckNotesStatus then EXIT;  //QM-191 EB Notes left open  (this should always come first as a way to cancel Done action}

  {moved RequiredItemsNeededBeforeDone}
  if RequiredItemsNeededBeforeDone(BeforeExitMsg) then begin
      MessageDlg(BeforeExitMsg, mtInformation, [mbOK], 0);
      EXIT;  //QM-282
  end;

  Assert(Assigned(LocateDS.DataSet));
  if LocateDS.DataSet.State in dsEditModes then begin
    LocateDS.DataSet.UpdateRecord;  // Force an update in case the user did not leave the field
    LocateEdited := True;
  end;

  if (CheckRouteOrder) then
  begin
   {Brings up Route Order Override. If answered, it will return FALSE}
    exit;  //QMANTWO-775 RESURRECTION EB  qm-723   sr
  end;

  DM.SaveTicketDueDate(FTicketID, EdtDueDate.Date, fOldDueDateValue);  //QM-216 EB
  CanEditTicketDueDate := False; //QM-216

  fOldDueDateValue := 0;
  VerifyReasonEntered; // QM-360  SR
  //Ken Funk 2016-01-14
  if VerifyFacilitiesTab then begin
    VerifyMarkTypeEntered;
    VerifyJobsiteArrival;
    CreateFollowupTicket;
    UpdateFollowupButton;

    TicketNotesFrame.SaveCachedNotes;
    CreateNoteForStatusChange(FTIcketID); //QM-463

    if PermissionsDM.CanI(RightAutoAttach) then begin
      Assert(Assigned(TicketDS.DataSet));
      Assert(TicketDS.DataSet.FieldByName('ticket_id').AsInteger = FTicketID);
      lHasAutoAttach := DM.AddAutoAttachments(qmftTicket, FTicketID, TicketDS.DataSet.FieldByName(
        'ticket_number').AsString);
     // ShowMessage('lHasAutoAttach: '  + boolToStr(lHasAutoAttach));
    end;

    DM.SaveTicket(FStatusedHow, StatusAsAssignedLocatorBox.Checked, PreserveClosedDateCBox.Checked); //QM-463 EB

    DM.SaveHP_TicketType(FTicketID); //qm-373 SR
    fInitialDueDateLoaded := False;  //QM-216 EB Reset this otherwise it will infinitely loop
    if PermissionsDM.CanI(RightTicketsAssignArea, False) then
      DM.SetTicketRouteArea(GetComboObjectInteger(RouteAreaID, False));

    if Assigned(FOnDoneWithTicket) and (not StayOnScreen) then
      FOnDoneWithTicket(Self);
  end else begin
    ShowMessage('This Call Center Requires entries to be added to the Facilities Tab.');
    TicketPages.ActivePage := FacilityTab;
  end;
end;

procedure TTicketDetailForm.RefreshStatusList;
  //qm-559 SR make an instance variable or property
CONST
  STATUS_RESTRICTION='TicketDetailStatus';
var
  StatusLimit:string;
  StatusListLimits:TStrings;      //qm-559 SR
  lStatusPlusList: TStringlist;  //QM-635 EB Status Plus
begin
  if not FormStatus.DataLoaded then exit; //QM-861  Fix for empty client error

  try
    StatusListLimits:= TStringList.Create;
    lStatusPlusList := TStringList.Create;  //QM-635 EB Status Plus
    StatusListLimits.Delimiter:=',';
    StatusListLimits.Add('''-N''');
    StatusListLimits.Add('''-P''');


      if CheckForStatusPlus then begin //QM-635 EB Status Plus
        lblProjectLocatesText.Visible := False;
        RefreshDataSet(StatusList);   //QM-703 EB Force the standard statuses to refresh EB
      end
      else begin
        if PermissionsDM.CanI(STATUS_RESTRICTION, False) then  //qm-559 SR
        begin //returned restrictions must look like '
          StatusLimit:= PermissionsDM.GetLimitation(STATUS_RESTRICTION);
          StatusListLimits.DelimitedText:= StatusLimit;
         end;

         {If no clients, then we can't run statuslist}  //QM-860 Fixing Exceptions with Project tickets
         if (trim(DM.MyClients.Text) = '') then begin
           lblLocateMessage.Caption := 'Waiting for Routing. Please sync and check in 2 min';
           lblLocateMessage.Visible := True;
         end
         else begin
           Statuslist.SQL.text:= Format(STATUS_LIST_FOR_CLIENTS,[StatusListLimits.DelimitedText, DM.MyClients.DelimitedText]);
 //        ShowMessage(StatusList.SQL.Text);
           RefreshDataSet(StatusList);
         end;
       end;

  finally
     StatusListLimits.Free;
     FreeAndNil(lStatusPlusList);  //QM-635 EB Status Plus
  end;
end;

function TTicketDetailForm.RequiredItemsNeededBeforeDone(
  var InfoMessageTxt: string): boolean;  //QM-282 EB
const
  ReqNoteMsg = 'Notes are required for this ticket before saving.';     //QM-966 EB Note Picture Req Fix
  ReqPicMsg =  'Pictures are required for this ticket before saving.';   //QM-966 EB Note Picture Req Fix
  ReqBothMsg = 'Pictures and notes are required for this ticket before saving.'; //QM-966 EB Note Picture Req Fix
var
  i: integer;
  PicturesRequired, NotesRequired: boolean;
  StillNeedPicture, StillNeedNote: boolean;  {Still need after check}
  lowercasepic: string;
  NoteCount: integer;
  CurNote: string;
  lNoteCat: integer;
  lAutoAttach: string;
begin
  lAutoAttach := '';
  NoteCount := 0;
  PicturesRequired := False;
  NotesRequired := False;
  StillNeedPicture := False;    //QM-966 EB Note Picture Req Fix
  StillNeedNote := False;    //QM-966 EB Note Picture Req Fix

  {Check all statuses for locate picture and note requirements}
  for i := 0 to (length(fRecentStatusChanges) - 1) do begin
    if fRecentStatusChanges[i].ReqPictures then
      PicturesRequired := True;
    if fRecentStatusChanges[i].ReqNotes then
      NotesRequired := True;
  end;
  {Initialize starting values}
  StillNeedNote := NotesRequired;
  StillneedPicture := PicturesRequired;

  {if nothing is required no need to do anything else...}
  if (not NotesRequired) and (not PicturesRequired) then begin
    Result := False;
    InfoMessageTxt := '';
    EXIT;
  end;
  {Notes (required)}
  if NotesRequired then begin
    {If Notes are required and no ticket notes, then we still need them}
    if DM.TicketNotesData.IsEmpty then begin
      StillNeedNote := True;
    end

    {Filter out automated notes}
    else begin
      Dm.TicketNotesData.First;
      NoteCount := DM.TicketNotesData.RecordCount;
      if NoteCount > 0 then begin
        {filter out automated notes}
        while not DM.TicketNotesData.EOF do begin
          CurNote := DM.TicketNotesData.FieldByName('note').asString;
          {If the note is not automated, then we no longer need a note}
          if not DM.IsAutomatedNoteCat(CurNote, lNoteCat) then begin
            StillNeedNote := False;  {Found a note}
            Break;
          end;
          DM.TicketNotesData.Next;
        end;
      end;  {else filter out automated notes}
    end;
  end; {End the Notes part}

  if PicturesRequired then begin
    If (AttachmentsFrame.Attachment.RecordCount = 0) then begin
      StillNeedPicture := True;
      {Look in Auto Attach folder}
      if DM.HasPendingAutoAttachments then
        StillNeedPicture := False;
    end
    {Filter out everything but the camera attachments}
    else begin
      AttachmentsFrame.Attachment.First;
      While not AttachmentsFrame.Attachment.EOF do begin
        lowercasepic := lowercase(AttachmentsFrame.Attachment.FieldByName('orig_filename').AsString);
        if (ansipos(picPrefix, lowercasepic) > 0 ) or (ansipos(movPrefix, lowercasepic) > 0) then begin
          StillNeedPicture := False;  {Found something}
          Break;
        end;
        AttachmentsFrame.Attachment.Next;
      end;
    end
  end;

  if StillNeedNote and StillNeedPicture then begin
    Result := True;
    InfoMessageTxt := ReqBothMsg;
  end
  else if StillNeedNote then begin
    Result := True;
    InfoMessageTxt := ReqNoteMsg;
  end
  else if StillNeedPicture then begin
    Result := True;
    InfoMessageTxt := ReqPicMsg;
  end
  else begin
    Result := False;
    InfoMessageTxt := '';
  end;

end;

procedure TTicketDetailForm.ShowTicketImage;
var
  TicketIsXml: Boolean;
begin
  TicketImageMemo.Text := DM.GetTicketImage(DM.Ticket, TicketIsXml);
  SetupImageMemoDisplay(TicketImageMemo, TicketIsXml);
end;

procedure TTicketDetailForm.SearchForURL;  //QM-13  SR
const
  SSL_URL='https://';   //qm-569 sr
var
  LineNo, CharNo: integer;
  FoundLine: String;
  w:integer;
  addHttps:boolean;   //qm-569 sr

 Function GetWidthText(const Text:string; Font:TFont):integer;
 var
   LBmp: TBitmap;
 begin
   LBmp:= TBitmap.Create;
   try
     LBmp.Canvas.Font := Font;
     Result := LBmp.Canvas.TextWidth(Text);
   finally
     LBmp.free;
   end;
 end;


begin
  LineNo := 0;
  CharNo := 0;
  addHttps := false;
  cxHyperLinkEdit1.Visible:=false;
  cxHyperLinkEdit1.Enabled:=false;
  while ((PosEx('http', TicketImageMemo.Lines[LineNo]) = 0)
        and (LineNo<TicketImageMemo.Lines.Count)
        or (PosEx('AllClear', TicketImageMemo.Lines[LineNo]) <> 0)  //special \carve out for OR
        or (PosEx('pos_res', TicketImageMemo.Lines[LineNo]) <> 0)  //QM-103 special carve out for NCA/SCA
        or (PosEx('org_pos', TicketImageMemo.Lines[LineNo]) <> 0)  //QM-97 special carve out for NCA/SCA
        or (PosEx('ICS_URL: [', TicketImageMemo.Lines[LineNo]) <> 0)  //qm-569 sr
        ) do //special \carve out for OR
    inc(LineNo);

  CharNo := PosEx('http', TicketImageMemo.Lines[LineNo]);
  if (CharNo = 0) then
  begin
    CharNo := PosEx('ICS_URL: [', TicketImageMemo.Lines[LineNo]);    //qm-569 sr
    addHttps:=true;
  end;
  

  if CharNo > 0 then
  begin
    if addHttps then
    FoundLine := SSL_URL+copy(TicketImageMemo.Lines[LineNo], CharNo,(Length(TicketImageMemo.Lines[LineNo]) - CharNo+1))   //qm-569 sr
    else
    FoundLine := copy(TicketImageMemo.Lines[LineNo], CharNo,(Length(TicketImageMemo.Lines[LineNo]) - CharNo+1));

    cxHyperLinkEdit1.Enabled:=true;
    w:= GetWidthText( trim(FoundLine), TicketPages.Font);
    cxHyperLinkEdit1.Width:= w+8;
    cxHyperLinkEdit1.top:=    TicketPages.Height -(cxHyperLinkEdit1.Height*2)-6;

    TicketImageMemo.Lines.Add('');
    TicketImageMemo.Lines.Add('');
    cxHyperLinkEdit1.Visible:= true;
    cxHyperLinkEdit1.Text:= FoundLine;
  end;
end;

procedure TTicketDetailForm.SetupImageMemoDisplay(Memo: TCustomMemo; const TicketIsXml: Boolean);
begin
  if Memo is TMemo then begin
    TMemo(Memo).WordWrap := TicketIsXml;
    if TicketIsXml then
      TMemo(Memo).ScrollBars := ssVertical
    else
      TMemo(Memo).ScrollBars := ssBoth;
  end else if Memo is TDBMemo then begin
    TDBMemo(Memo).WordWrap := TicketIsXml;
    if TicketIsXml then
      TDBMemo(Memo).ScrollBars := ssVertical
    else
      TDBMemo(Memo).ScrollBars := ssBoth;
  end;
end;

function TTicketDetailForm.IsAParentLocate(Rec: TcxCustomGridRecord): boolean;
var
 i: integer;
begin
  {QMANTWO-616 EB can only be called after grid is loaded}
  if (fProjParents.Count > 0) and (fProjParents.Find(Rec.Values[ColLocClientCode.Index], i)) then
    Result := True
  else
    Result := False;
end;

function TTicketDetailForm.IsAParentLocate(ClientCode: string): boolean;
var
  i: integer;
begin
  if (fProjParents.Count > 0) and (fProjParents.Find(ClientCode, i)) then
    Result := True
  else
    Result := False;
end;


procedure TTicketDetailForm.LocateGridViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  Status: string;
begin
  inherited;
  Status := LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index];
  if LocateGridView.Controller.FocusedItem = ColLocLengthMarked then
    //QMANTWO-616 EB: Lock down if it is a project parent
    AAllow := (StatusAllowsUnits(Status)); // and (not IsAParentLocate(LocateGridView.Controller.FocusedRecord));
  if LocateGridView.Controller.FocusedItem = ColLocQtyMarked then
    AAllow := False;
end;

procedure TTicketDetailForm.SetFormStatus(Value: Boolean);   //QM-577 EB
begin
  FormStatus.FormReady := Value;
  FormStatus.DataLoaded := Value;
  FormStatus.NoteSourceLoaded := Value;
end;

function TTicketDetailForm.InitLoadParentClients(ClientCode: string): integer;
//QMANTWO-616 EB Project Tickets Initial load
begin
  Result := 0;
  if (ClientCode <> '') and (not fProjParents.Find(ClientCode, Result)) then
    fProjParents.Add(ClientCode);    
end;


procedure TTicketDetailForm.InitStatusChangeArray;  //QM-282 EB
var
  i : integer;
const
  WALDO_TERMS : array[0..3] of string=('ATTDSOUTH','PACBEL','ATTDNORCAL','BSCA');  //qm-449 770 sr
begin
  if DM.LocateData.Active then
    SetLength(fRecentStatusChanges, DM.LocateData.RecordCount);

  DM.LocateData.First;
  i:= 0;
  while (not DM.LocateData.EOF) and (i < length(fRecentStatusChanges)) do begin
    fRecentStatusChanges[i].LocateID := DM.LocateData.FieldByName('locate_id').AsInteger;
    fRecentStatusChanges[i].ClientID := DM.LocateData.FieldByName('client_id').AsInteger;
    fRecentStatusChanges[i].ClientName := DM.LocateData.FieldByName('client_name').AsString;

    if pos('ATTDSOUTH', DM.LocateData.FieldByName('client_name').AsString)>0 then ShowWaldo:=true;    //qm-449 sr
    if pos('PACBEL', DM.LocateData.FieldByName('client_name').AsString)>0  then ShowWaldo:=true;     //qm-449 sr
    if pos('ATTDNORCAL', DM.LocateData.FieldByName('client_name').AsString)>0  then ShowWaldo:=true;     //qm-449 770 sr
    if pos('NEVBEL', DM.LocateData.FieldByName('client_name').AsString)>0  then ShowWaldo:=true; //QM-685 sr
    if pos('ATTDNEVADA', DM.LocateData.FieldByName('client_name').AsString)>0  then ShowWaldo:=true; //QM-685 770 sr


    if pos('BS', DM.LocateData.FieldByName('client_name').AsString)>0 then ShowWaldo:=true;      //qm-449 sr
    if uppercase(DM.Ticket.FieldByName('ticket_format').AsString) ='FPL1'  then
      ShowWaldo:=true;   //QM-680 sr

    if pos('ARK',DM.Ticket.FieldByName('ticket_format').AsString)>0  then ShowWaldo:=true;   //QM-826 sr

    fRecentStatusChanges[i].Status := DM.LocateData.FieldByName('status').AsString;
    fRecentStatusChanges[i].ReqPictures := False;  {We do not care about previous status settings}
    fRecentStatusChanges[i].ReqNotes := False;
    fRecentStatusChanges[i].Changed := False;
    DM.LocateData.Next;
    inc(i);
  end;
  DM.LocateData.First;
end;

procedure TTicketDetailForm.AddLocate_toStatusChangeArray;
var
  i : integer;
begin
  if DM.LocateData.Active then
    SetLength(fRecentStatusChanges, DM.LocateData.RecordCount);

    i := DM.LocateData.RecordCount - 1;
  fRecentStatusChanges[i].LocateID := DM.LocateData.FieldByName('locate_id').AsInteger;
    fRecentStatusChanges[i].ClientID := DM.LocateData.FieldByName('client_id').AsInteger;
    fRecentStatusChanges[i].ClientName := DM.LocateData.FieldByName('client_name').AsString;
    fRecentStatusChanges[i].Status := DM.LocateData.FieldByName('status').AsString;
    fRecentStatusChanges[i].ReqPictures := False;  {We do not care about previous status settings}
    fRecentStatusChanges[i].ReqNotes := False;
    fRecentStatusChanges[i].Changed := False;
end;

procedure TTicketDetailForm.LocateGridViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
var
  lclientCode: string;
  i: integer;
begin
  inherited;
  if LeavingTicket or (not FormStatus.DataLoaded) then  //QM-814 EB IDE Grid Errors
    exit;
  if (AFocusedRecord <> nil) then begin   //QM-814 EB IDE Grid Errors
    UpdateCurrentGridRow;
    {QMANTWO-616 - Initially coming into screen, we need to know if there are any project tickets}
    lclientCode := DM.LocateData.FieldByName('client_code').AsString;  //QMANTWO-616 EB
    i := InitLoadParentClients(DM.LocateData.FieldByName('parent_code').AsString);
    btnProjectLocate.Visible := IsLocateProjectable;   //QMANTWO-616 EB
    CheckForStatusPlus;   {QM-635 Status Plus EB}
  end;
end;

function TTicketDetailForm.CheckForStatusPlus: boolean;
var
  lCurCallCenter: string;
  lCurClientID : integer;
  lCurClientCode: string;
  lCurStatus: string;
begin
{QM-635 Status Plus Status List}
  try
    if not LocateGridView.Controller.HasFocusedControls then begin   //QM-860 EB Can't do anything if there is no focus
      Result := False;
      Exit;
    end;

    if not LeavingTicket then begin    //QM-814 EB IDE Grid Errors - marking this location
      lCurStatus :=     LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index];         //QM-635 for status plus
      lCurCallCenter := CallCenter.Text;
      lCurClientID   := LocateDS.DataSet.FieldByName('client_id').AsInteger;
      lCurClientCode := DM.LocateData.FieldByName('client_code').AsString;
      if  DM.UseStatusPlus(lCurStatus,  lCurClientID, lCurClientCode, lCurCallCenter, StatusList_sp) then begin
          StatusListView.DataController.DataSource := StatusSource_sp;
          Result := True;
      end
      else begin
        StatusListView.DataController.DataSource := StatusSource;
        StatusList.Filter := 'client_id = ' + intTostr(lCurClientID); //QM-635
        StatusList.Filtered := True;
        Result := False;
      end;
    end
    else Result := False;  //QM-876 Eb Code Cleanup
  except
    Result := False;  //QM-860 EB  Cleanup for saving locate/ticket
  end;
end;

procedure TTicketDetailForm.UpdateCurrentGridRow;
begin
  if not Assigned(LocateDS.DataSet) then
    Exit;
  if LocateGridView.Controller.FocusedRecord = nil then
    Exit;

  if (LocateDS.DataSet.State in dsEditModes) or (not RecordIsReadOnly(LocateGridView.Controller.FocusedRecord)) then begin
    LocateGridView.OptionsData.Editing := True;
    LocateGridView.OptionsView.ShowEditButtons := gsebForFocusedRecord;
  end else begin
    LocateGridView.OptionsData.Editing := False;
    LocateGridView.OptionsView.ShowEditButtons := gsebNever;
  end;
  SetHighProfileState;
  SetDefaultMarkType;
  SetupLengthColumn;
  ColLocLengthMarked.Properties.ReadOnly := True;
end;

procedure TTicketDetailForm.LocateGridViewFocusedItemChanged(
  Sender: TcxCustomGridTableView; APrevFocusedItem,
  AFocusedItem: TcxCustomGridTableItem);
begin
  inherited;
 // if not LeavingTicket then begin  //QM-814 EB DevEx Grid IDE
  SetHighProfileState;
  SetDefaultMarkType;
  SetupLengthColumn;
//  end;
end;

procedure TTicketDetailForm.LeavingNow;
begin
  lblLocateMessage.Visible := False;
  DM.CloseTicketDataSets;
  CanEditTicketDueDate := False;
end;

procedure TTicketDetailForm.ColLocStatusPropertiesCloseUp(Sender: TObject);
var
  pCurRowStatus: string;      // QM-161 EB
  pLocateID: integer;
  pCurClientID: integer;
  pCurClientName: string;
  pCurClientCode: string;
  ICanEditDD: boolean;
  i: integer;
  RemainingAttempts: integer;  // QM-641 EB    {Attempts left}
begin
  inherited;
  ICanEditDD := False;

  EnableDisableControls;
  //QM-161 EB - Code has been removed
  pCurRowStatus := LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index];
  pLocateID := LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index];
  pCurClientName := LocateGridView.Controller.FocusedRecord.Values[ColLocClientName.Index];
  pCurClientCode := LocateGridView.Controller.FocusedRecord.Values[ColLocClientCode.Index];
  pCurClientID := DM.LocateData.FieldByName('client_id').AsInteger;


  for i := 0 to (length(fRecentStatusChanges) - 1) do begin
    if (pLocateID = fRecentStatusChanges[i].LocateID) then begin
      LoadRequirements(i, pCurRowStatus);
      Break;
    end;
  end;


  If fCurClientStatusRequires.ReqNotes and fCurClientStatusRequires.ReqPictures then begin //HasNoteRequirement(pCurRowStatus) then begin                                       //QM-161 EB
    lblLocateMessage.Caption := 'Status ' + '"' + pCurRowStatus + '"' + ' for client ' + pCurClientCode + ' requires pictures and note.';
    lblLocateMessage.Visible := True;
  end
  else If fCurClientStatusRequires.ReqNotes then begin //HasNoteRequirement(pCurRowStatus) then begin
    lblLocateMessage.Caption := 'Status ' + '"' + pCurRowStatus + '"' + ' for client ' + pCurClientCode + ' requires a note.';
    lblLocateMessage.Visible := True;
  end
  else If fCurClientStatusRequires.ReqPictures then begin //HasNoteRequirement(pCurRowStatus) then begin
    lblLocateMessage.Caption := 'Status ' + '"' + pCurRowStatus + '"' + ' for client ' + pCurClientCode + ' requires pictures.';
    lblLocateMessage.Visible := True;
  end
  else begin
    lblLocateMessage.Caption := '';
    lblLocateMessage.Visible := False;
  end;

    HasTicketDueDateOverride(ICanEditDD, RemainingAttempts);
    ICanEditDD := DM.IsDueDateStatus(pCurRowStatus, pLocateID ) and ICanEditDD;
    CanEditTicketDueDate := ICanEditDD; //QM-216  - Is the status applicable
end;

procedure TTicketDetailForm.ColLocStatusPropertiesInitPopup(Sender: TObject);
begin
  inherited;
  CheckForStatusPlus;
end;

procedure TTicketDetailForm.ColLocStatusPropertiesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  key := #0;
end;

procedure TTicketDetailForm.CopyTicket1Click(Sender: TObject);
begin
  inherited;     //qm-471
  Clipboard.AsText :=  dm.Ticket.FieldByName('update_of').AsString;      //qm-471  sr
end;

procedure TTicketDetailForm.TicketPagesChange(Sender: TObject);
begin
  if TicketPages.ActivePage = HistoryTab then begin
    BillingTab.TabVisible := PermissionsDM.CanI(RightViewTicketBilling) or DM.UQState.DeveloperMode;
    if FTicketID = 0 then
      Exit;
    ShowTicketHistory;
  end else if TicketPages.ActivePage = PhotoAttachmentsTab then begin
    PhotoAttachmentFrame.Initialize;
    PhotoAttachmentFrame.OnPhotoAttachment := AttachmentsFrame.OnPhotoAttachment;
    PhotoAttachmentFrame.AfterDoPhotoAttachment := AttachmentsFrame.AfterDoPhotoAttachment;
  end else if TicketPages.ActivePage = AttachmentsTab then begin
    UpdateCameraStatus
  end else if TicketPages.ActivePage = WorkDescriptionTab then
    ShowTicketWorkDescription
//  else if (TicketPages.ActivePage = TicketAlertsTab) then          //QM-771 Ticket Alerts EB
//    ShowTicketAlerts;
end;

procedure TTicketDetailForm.ShowTicketAlerts;    //QM-771 Ticket Alerts EB
begin
{Ticket Alerts}
      TicketAlertsFrame.OpenAndGetAlerts(fTicketID);
      TicketAlertsFrame.InitAlertsFrame(True);
      if TicketAlertsFrame.HasAlerts then
        TicketAlertsTab.TabVisible := True
      else
        TicketAlertsTab.TabVisible := False;
end;

procedure TTicketDetailForm.ShowTicketHistory;
  procedure FilterOnLocates(Table: TDBISAMDataSet; const Suffix: string = '');
  begin
    Table.Filter := GetLocateFilterString + Suffix;
    Table.Filtered := True;
    Table.Open;
  end;

  procedure FilterOnTicketId(Table: TDBISAMTable);
  begin
    Table.Filter := 'ticket_id = ' + IntToStr(FTicketID);
    Table.Filtered := True;
    Table.Open;
  end;

  procedure OpenTicketVersionsDataset;
  const
    SQLMaxArrivalDate =
      'select max(arrival_date) as arrival_date from ticket_version where ticket_id = %d';
    SQLTicketVersions =
      'select tv.*, coalesce(cc.cc_name, ''"'' || tv.source || ''" is not a ' +
      'Call Center'') as call_center, if(tv.arrival_date = ''%s'' then ''*'' ' +
      'else '' '') as active ' +
      'from ticket_version tv left join call_center cc on tv.source = cc.cc_code where tv.ticket_id = %d';
  var
    DataSet: TDataSet;
  begin
    DataSet := DM.Engine.OpenQuery(Format(SQLMaxArrivalDate, [FTicketID]));
    try
      TicketVersions.SQL.Text := Format(SQLTicketVersions, [AnsiDateTimeToStr(Dataset.FieldByName('arrival_date').AsDateTime, False), FTicketID]);
      AddCalculatedField('image_display', TicketVersions, TStringField, 7000); //QM-256  sr
      TicketVersions.Open;
    finally
      Dataset.Close;
      Dataset.Free;
    end;
  end;

  procedure OpenLocateFacilityDataset;
  const
    SelectLocateFacility = 'select fac.*, ft.description as type_desc, ' +
      'fm.description as material_desc, fs.description as size_desc, ' +
      'fp.description as pressure_desc, l.client_code, c.client_name, ' +
      'e1.short_name as added_by_name, e2.short_name as deleted_by_name ' +
      'from locate_facility fac ' +
      'inner join locate l on fac.locate_id=l.locate_id ' +
      'inner join ticket t on l.ticket_id=t.ticket_id ' +
      'left join reference ft on (fac.facility_type=ft.code and ft.type=''factype'') ' +
      'left join reference fm on (fac.facility_material=fm.code and fm.type=''facmtrl'' and fm.modifier=facility_type) ' +
      'left join reference fs on (fac.facility_size=fs.code and fs.type=''facsize'' and fs.modifier=facility_type) ' +
      'left join reference fp on (fac.facility_pressure=fp.code and fp.type=''facpres'' and fs.modifier=facility_type) ' +
      'left join employee e1 on fac.added_by=e1.emp_id ' +
      'left join employee e2 on fac.deleted_by=e2.emp_id ' +
      'left join client c on c.client_id=l.client_id ' +
      'where t.ticket_id = %d';
  begin
    LocateFacility.SQL.Text := Format(SelectLocateFacility, [FTicketID]);
    LocateFacility.Open;
  end;

  procedure OpenJobsiteArrivalDataset;
  begin
    DM.OpenJobsiteArrivalData(FTicketID, atTicket);
  end;

  procedure OpenTicketInfoDataSet;
  begin
  {Do NOT close and re-open ticket here}
  end;

  procedure OpenEPRHistoryDataSet;
  begin
    qryEPRHistory.ParamByName('TicketID').asInteger := FTicketID;
    qryEPRHistory.Open;
  end;

  procedure OpenTicketRisk;  //QM-387 part 3 EB
  begin
    TicketRiskQry.ParamByName('ticket_id').AsInteger := FTicketID;
    TicketRiskQry.Open;
  end;


var
  Cursor: IInterface;
  TestStr: string;
begin
  if FTicketHistoryDownloaded then
    Exit;

  Cursor := ShowHourGlass;
  Application.ProcessMessages;
  LocateStatus.Close;
  Assignments.Close;
  Responses.Close;
  EmailResults.Close;
  Billing.Close;
  TicketVersions.Close;
  TicketInfo.Close;
  qryEPRHistory.Close;
  
  if FTicketID > 0 then
    DM.GetTicketHistory(FTicketID);

  FilterOnLocates(LocateStatus);
  FilterOnLocates(Assignments);
  FilterOnLocates(Responses);
  // DBISAM does not do subselects or where-correlated joins
  //QMANTWO-743 EB: Some of the information is dropping out of Billing History, try without filter
  FilterOnLocates(Billing, ' and ((client_customer_id = invoice_customer_id) or (invoice_customer_id is null))');
  TestStr := Billing.Filter;
  OpenTicketVersionsDataset;
  FilterOnTicketID(Acknowledged);
  FilterOnTicketID(TicketInfo);
  FilterOnTicketID(EmailResults);
  OpenTicketRisk;    //QM-387 part 3 EB
  OpenLocateFacilityDataset;
  BillingGridView.DataController.Groups.FullExpand;

  OpenJobsiteArrivalDataset;
  OpenTicketInfoDataSet;
  OpenEPRHistoryDataSet;   //QMANTWO-338 EB

  SetupArrivalControls;

  FTicketHistoryDownloaded := True;
end;


function TTicketDetailForm.GetLocateFilterString: string;
var
  Locates: TDataSet;
begin
  Result := '';
  Locates := DM.Engine.OpenQuery('select locate_id from locate where ticket_id = '
    + IntToStr(FTicketID));
  try
    while not Locates.Eof do begin
      if Result = '' then
        Result := '(locate_id = ' + IntToStr(Locates.FieldByName('locate_id').AsInteger) +')'
      else
        Result := Result + ' or (locate_id = ' + IntToStr(Locates.FieldByName('locate_id').AsInteger) +')';
      Locates.Next;
    end;
    // Just in case this is a ticket without any locates, show nothing
    if Result = '' then
      Result := 'locate_id = -0';
    Result := '(' + Result + ')';
  finally
    FreeAndNil(Locates);
  end;
end;

procedure TTicketDetailForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  LeavingTicket := True;  //QM-814 EB Grid Exceptions 
  inherited;
end;

procedure TTicketDetailForm.FormCreate(Sender: TObject);
begin
  inherited;  //qm-577  sr
  SetFormStatus(False);   {Our form has not been loaded}
  FNoteSubTypeList := TCodeLookupList.Create(NoteSubTypeCombo.Items);   //qm-577  sr
  fInitialDueDateLoaded := False; //QM-216
  fDueDateChanged := False; //QM-216
  fProjParents := TStringList.Create; //QMANTWO-616 EB
  fProjLocatorAssignments := TLocatorAssignments.Create; //QMANTWO-616  EB
  ImageTab.PageIndex := 0;   // Put the image tab first
  TicketPages.ActivePage := ImageTab;
  FReadOnlyCache := TList.Create;
  HistoryPages.ActivePage := LocateStatusTab;
  SetupDataSets;
  if not DM.UQState.DeveloperMode then
    HideGridIDColumns;
  overRideClients := '';         //QMANTWO-208
  DM.OnDisplayLocateHours := DisplayWorkDialog;
  overRideClients := DM.GetConfigurationDataValue('override_units');      //QMANTWO-208

  EnableDisableControls;
  FReasonCodes := TStringList.Create;

  //Ken Funk 2016-01-14
  FCallCentersRequireFacilitiesTab := TStringList.Create;
  FLocationMarkStatusList := TStringList.Create;
  PopulateCallCentersRequireFacilitiesTab;
  PopulateLocationMarkStatusList;

  PhotoAttachmentsTab.TabVisible := False;
  PrepareTicketAddins;
  FLocateLengthEditor := TLocateLengthEditor.CreateWithClients(LocateGrid, overRideClients);       //QMANTWO-208
  TicketSummaryTab.TabVisible := False;
  TicketNotesFrame.MakeVisible(False);
  FormStatus.FormReady := True;
end;

procedure TTicketDetailForm.SetupDataSets;
begin
  AddLookupField('client_code', LocateStatus, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 15, 'ClientCodeField');
  AddLookupField('short_name', LocateStatus, 'statused_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
  AddLookupField('client_name', LocateStatus, 'locate_id',
    LocateLookup, 'locate_id', 'client_name');
  AddLookupField('changedby_name', LocateStatus, 'status_changed_by_id',          //QM-965 EB Addomg history fields
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ChgByShortNameField');

  AddLookupField('client_code', Assignments, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 15, 'ClientCodeField');
  AddLookupField('short_name', Assignments, 'locator_id',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
  AddLookupField('client_name', Assignments, 'locate_id',
    LocateLookup, 'locate_id', 'client_name');

  AddLookupField('client_code', Responses, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 15, 'ClientCodeField');//found misspelled as CientCodeField  --sr 3/23/22
  AddLookupField('client_name', Responses, 'locate_id',
    LocateLookup, 'locate_id', 'client_name');

  AddLookupField('client_code', Billing, 'locate_id',
    LocateLookup, 'locate_id', 'client_code', 15, 'ClientCodeField');
  AddLookupField('client_name', Billing, 'client_id',
    LocateLookup, 'locate_id', 'client_name');

  AddLookupField('short_name', Acknowledged, 'ack_emp_id',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');

  AddLookupField('short_name', DM.JobsiteArrivalData, 'emp_id',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'ShortNameField');
  AddLookupField('deleted_by_name', DM.JobsiteArrivalData, 'deleted_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'DeletedByNameField');
  AddLookupField('added_by_name', DM.JobsiteArrivalData, 'added_by',
    EmployeeLookup, 'emp_id', 'short_name', ShortNameLenth, 'AddedByNameField');
end;

procedure TTicketDetailForm.HideShowCENorTEMABtn;    //QM-53 EB   QM-154 SR
var
  ws, TF: string;  //QM-154 SR
begin
  ws := DM.Ticket.FieldByName('work_state').AsString;     //QM-154 SR
  TF := DM.Ticket.FieldByName('ticket_format').AsString;

   if (ws = 'CA') or (TF = 'LWA1') or (TF = 'LOR1') or (TF = 'NJN')or (ws = 'NJ')  then   //qm-341 647 sr
   begin    //QM-154 SR
     CENButtonPanel.Visible := True;
     CENButton.Caption := 'TEMA';
     fCENButtonMode := cbTEMA;
   end;
   if (TF = 'NCA1') or  (TF='NVA1') or  (TF='LCA1')   then    //qm-651 sr  qm-667 sr
   begin
     btnPelican.Visible:= true;
     btnPelican.BringToFront;
   end
   else begin
     CENButtonPanel.Visible := True;
     btnPelican.Visible:= False;
     fCENButtonMode := cbTEMA;
     btnPelican.SendToBack;
  end;
end;

procedure TTicketDetailForm.HideEFDButtonForNonEFDColl;
const
  EFD = 'ELECTRONIC FIELD DATA COLLECTION';
begin
  if  DM.Ticket.FieldByName('ticket_type').AsString <> EFD then
    EFDButtonPanel.Visible := False
  else        //QMANTWO-297
    EFDButtonPanel.Visible := True;
end;


procedure TTicketDetailForm.HideGridIDColumns;
begin
  colLocStLocateID.Visible := False;
  colLocStStatusedBy.Visible := False;
  AssignColAssignmentID.Visible := False;
  AssignColLocateID.Visible := False;
  AssignColLocatorID.Visible := False;
  RespColResponseID.Visible := False;
  RespColLocateID.Visible := False;
  ColBillLocateID.Visible := False;
  ColTktVerVersionID.Visible := False;
  ColEmailEqrID.Visible := False;
  ColEmailTicketID.Visible := False;
end;

function TTicketDetailForm.StatusAllowsUnits(const Status: string): Boolean;
begin
  Result := StringInArray(Status, FMarkableUnitStatusCodes);
end;

procedure TTicketDetailForm.StatusAsAssignedLocatorBoxClick(Sender: TObject);
begin
  inherited;
  RefreshLocateReadOnlyCache;          //QM-804 EB Disabling/Enabling other locator's locates
  if StatusAsAssignedLocatorBox.Checked then
    ColLocLengthMarked.Properties.ReadOnly := False;    {If they check it, let them change it}

end;

procedure TTicketDetailForm.HoursUnitsButtonClick(Sender: TObject);
var
  LocateID: Integer;
  CanAddHours: Boolean;
  CanAddUnits: Boolean;
begin
  Assert(Assigned(LocateDS.DataSet));
  if LocateGridView.Controller.FocusedRecord = nil then
    raise EOdNonLoggedException.Create('Please select a locate first');
  if HaveUnsavedLocate then
    raise Exception.Create('Please save and sync the newly added locate before adding hours/units');

  LocateID := LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index];

  if LocateDS.DataSet.State in dsEditModes then
    LocateDS.DataSet.Post;

  CanAddHours := (SelectedLocateStatus = 'OH');
  CanAddUnits := StatusAllowsUnits(SelectedLocateStatus);
  DisplayWorkDialog(Self, FTicketID, LocateID, CanAddHours, CanAddUnits);
end;

function TTicketDetailForm.IsLocateProjectable: boolean;  //QMANTWO-616 EB
var
  ClientID: integer;
  LocateID: integer;
begin
  //QMANTWO-616 EB 1) Check Permissions 2) Check that locate is open 3) Check that it has children
  try
    Result := PermissionsDM.CanI(RightMultiLocatorsForProjTicket, False);
    LocateID := DM.LocateData.FieldByName('locate_id').AsInteger;
    ClientID := DM.LocateData.FieldByName('client_id').AsInteger;

    Result := Result and DM.ClientHasSubClients(ClientID); //(DM.LocateData.FieldByName('client_id').AsInteger);
    if Result then //Don't run this is if there is no reason
      Result := Result and DM.IsLocateEditable(LocateID); //(DM.LocateData.FieldByName('locate_id').AsInteger);
  except
    Result := False;
  end;
end;

function TTicketDetailForm.IsProjectTicket: boolean;
begin
  Result := lblProjectLocatesText.Visible;
end;

//function TTicketDetailForm.IsValidStatus(pStatus: string;  pTicketID: integer; pTktType: string;
//  pClientID: integer; pMeetOnly: boolean; pTTExclusion: string): Boolean;
//begin
//  {consolidating for use with possible exit check}  //
//  Result :=  DM.ValidStatusForTicket(pTicketID, pMeetOnly)
//              and DM.ValidStatusForEmployee(pStatus)
//              and DM.ValidExclusionTicketType(pClientID, pStatus, pTktType, pTTExclusion);
//end;

procedure TTicketDetailForm.ItemDblClick(Sender: TObject);     //QMANTWO-577  EB
begin
  ClearSelectionsDBText(HeaderPanel);
  ClearSelectionsDBText(DetailsPanel);
  if Sender is TDBText then
    CopyDBText(Sender as TDBText)
  else if Sender is TDBMemo then
    CopyDBMemo(Sender as TDBMemo)
  else if Sender is TLabel then
    CopyLabelText(Sender as TLabel)
  else if Sender is TDBEdit then
    CopyDBEdit(Sender as TDBEdit);
  TFadeBox.Execute((TComponent(Sender).Name+' has been copied to the clipboard'));//SR
end;

procedure TTicketDetailForm.EdtDueDatePropertiesCloseUp(Sender: TObject);
begin
  inherited;
  CheckDueDate;  //QM-216 EB
end;

procedure TTicketDetailForm.EmailsGridViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  mailBody:string;  //QM-146
begin
  inherited;
  mailBody:='';
  if ACellViewInfo.Item = ColEmailBody then
  begin
    mailBody:= EmailResults.fieldByName('email_body').AsString;  //QM-146  SR
    try
      frmEmailBody:= TfrmEmailBody.Create(nil);
      frmEmailBody.mailBodyMemo.lines.add(mailBody);
      frmEmailBody.ShowModal;
    finally
      frmEmailBody.Release;
    end;
  end;
end;

procedure TTicketDetailForm.EnableDisableControls;
var
  ShowingTicket: Boolean;
  AnyNewLocates: Boolean;
  iValue:integer;    //qm-577 sr
begin
  AddLocateButton.Enabled := DM.TicketIsOpen and PermissionsDM.CanI(RightTicketsAddLocates)  and (TryStrToInt(TicketID.Text,iValue));  //QMANTWO-716  qm-577 sr
  UpdateHoursButtonVisibility;
  RefreshLocateReadOnlyCache;

  ShowingTicket := Assigned(TicketDS.DataSet) and TicketDS.DataSet.Active;
  PrintButton.Enabled := ShowingTicket and DM.CanIPrintTickets;
  DoneButton.Enabled := ShowingTicket;
  PlatButton.Enabled := (ShowingTicket and (TryStrToInt(TicketID.Text,iValue))); //QMANTWO-716  qm-577 sr
  AnyNewLocates := HaveUnsavedLocate;
  SetEnabledOnControlAndChildren(AddFacilityPanel, (ShowingTicket and
    DM.TicketIsOpen and (AnyNewLocates = False)));
  ColFacOffset.Properties.ReadOnly := not AddFacilityPanel.Enabled;
  NoFacilityEditLabel.Visible := AnyNewLocates;
end;

procedure TTicketDetailForm.EnableEditDueDate(IsEditable: Boolean); //QM-216
begin
  EdtDueDate.Properties.ReadOnly := not IsEditable;
  EdtDueDate.Enabled := IsEditable;
  EdtDueDate.Visible := IsEditable;

  if IsEditable then
    EdtDueDate.Style.Color := clWebLemonChiffon  //clCream

  else
    EdtDueDate.Style.Color := DetailsPanel.Color;

end;

procedure TTicketDetailForm.SetArriveButtonEnabled(const Enabled: Boolean);
begin
  ArriveButton.Enabled := Enabled;
  if Enabled then begin
    ArriveButtonPanel.ParentBackground := False;
    ArriveButtonPanel.Color := clRed;
  end
  else
    ArriveButtonPanel.ParentBackground := True;
end;



function TTicketDetailForm.SelectedLocateStatus: string;
begin
  Result := '';
  if LocateGridView.Controller.FocusedRecord = nil then
    Exit;
  Result := LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index];
end;

procedure TTicketDetailForm.DisplayWorkDialog(Sender: TObject; TicketID,
  LocateID: Integer; CanAddHours, CanAddUnits: Boolean);
begin
  if not Assigned(FLocateWorkForm) then
    FLocateWorkForm := TLocateWorkForm.Create(nil);           //QM-299 sr
  FLocateWorkForm.TotalHoursLabel.Caption :='------             -------';  //QM-299 sr
  FLocateWorkForm.SetupDialog(TicketID, LocateID, CanAddHours, CanAddUnits);

  PostMessage(Handle, WM_DISPLAYLOCATEHOURS, 0, 0);
end;

// This is done on a message delay to allow the automatic popup when setting
// the H/OH status to exit the DM code before showing this dialog.
procedure TTicketDetailForm.ShowTicketOnMessage(var Msg: TMessage);
begin
  FLocateWorkForm.ShowDialog;
end;

procedure TTicketDetailForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FReadOnlyCache);
  FreeAndNil(FLocateWorkForm);
  FreeAndNil(FReasonCodes);
  FreeAndNil(FAttachmentAddin);
  FreeAndNil(FPlatAddin);
  FreeAndNil(FLocateLengthEditor);
  FreeAndNil(FNoteSubTypeList);

  //Ken Funk 2016-01-14 Free vars
  FreeAndNil(FCallCentersRequireFacilitiesTab);
  FreeAndNil(FLocationMarkStatusList);
  //QMANTWO-616 EB Project Locates
  FreeAndNil(fProjParents);
  FreeAndNil(fProjLocatorAssignments);

  
  inherited;
end;

procedure TTicketDetailForm.UpdateHoursButtonVisibility;
begin
  HoursUnitsButton.Enabled := HaveAUnitsOrHourlyStatusLocate and (StrToInt(TicketID.Text)>0); //QMANTWO-716 --sr
end;

procedure TTicketDetailForm.UpdateRelatedTicketsDblClick(Sender: TObject);
var
  upDateTktID: integer;
  Cursor: IInterface; //qm-460 sr
begin
  inherited;
  Cursor := ShowHourGlass; //qm-460 sr
  upDateTktID := DM.GetTicketID(dm.Ticket.FieldByName('update_of').AsString, dm.Ticket.FieldByName('ticket_format').AsString, 0.0, 0.0);  //qm-397 sr
  DM.UpdateTicketInCache(upDateTktID, Now);   //qm-397 sr
  MainForm.ShowTicketUpdateOf(Sender,upDateTktID, dm.Ticket.FieldByName('ticket_id').AsInteger);   //qm-397 sr
end;

function TTicketDetailForm.HaveAUnitsOrHourlyStatusLocate: Boolean;
var
  i: Integer;
  Status: string;
begin
  Result := False;
  for i := 0 to LocateGridView.ViewData.RecordCount - 1 do begin
    Status := LocateGridView.ViewData.Records[i].Values[ColLocStatus.Index];
    if StatusAllowsUnits(Status) or (Status = 'OH') or (Status = 'H') then begin
      Result := True;
      Break;
    end;
  end;
end;

function TTicketDetailForm.HaveUnsavedLocate: Boolean;
var
  i: Integer;
  LocateID: Integer;
begin
  Result := False;
  for i := 0 to LocateGridView.ViewData.RecordCount - 1 do begin
    LocateID := LocateGridView.ViewData.Records[i].Values[ColLocLocateID.Index];
    if LocateID < 0 then begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TTicketDetailForm.AssignColAddedByGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: String);
begin
  inherited;
  if AText = '' then
    AText := AppName
  else if StrToIntDef(AText, -1) > -1 then
    AText := EmployeeDM.GetEmployeeShortName(StrToInt(AText));
end;

procedure TTicketDetailForm.btnMapClick(Sender: TObject);
const                  //QM-12 SR
  GOOGLE_MAPS_URL = 'http://maps.google.com/?q=%s,%s';
  GOOGLE_MAPS =  'http://maps.google.com/?q=%s,%s,%s';
Var
  Link:string;
begin
  inherited;
  With DM.Ticket do
  begin
    if TComponent(Sender).Name= 'btnMap' then
    Link := Format(GOOGLE_MAPS_URL,[FieldByName('Work_Lat').AsString,FieldByName('Work_Long').AsString])
    else
    if TComponent(Sender).Name= 'btnAddr' then
    Link := Format(GOOGLE_MAPS,[FieldByName('work_address_number').AsString+' '+
            FieldByName('work_address_street').AsString,
            FieldByName('work_city').AsString,
            FieldByName('work_state').AsString]);
  End;

  ShellExecute(0,'open',PAnsiChar(Link),nil,nil, SW_SHOWNORMAL);
end;

procedure TTicketDetailForm.btnTktSafetyClick(Sender: TObject);
var     //QMANTWO-644
  HashString : string;
  TicketIDStr, LocateIDStr, EmpIDStr : string;
  Pepper, LPepper, RPepper, BaseURL, Link: string;
const
  TID  = 'tid=';
  LID  = 'Lid=';
  EID  = 'Eid=';
  FM   = 'FM=';
  CODE = 'code=';
  AMP  = '&';
  FORMNAME = 'JSA';
begin
  inherited;

  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  BaseURL := DM.GetConfigurationDataValue('JSASafetyTktURL');
  TicketIDStr := IntToStr(FTicketID);

  LocateIDStr := IntToStr(0);
  EmpIDStr := IntToStr(DM.EmpID);
  HashString := LPepper +
                TicketIDStr +
                LocateIDStr +
                EmpIDStr +
                FORMNAME +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);
  Link :=  BaseURL +
                    TID + TicketIDStr + AMP +
                    LID + LocateIDStr + AMP +
                    EID + EmpIDStr + AMP +
                    FM + FORMNAME + AMP +
                    CODE + HashString;
  try
    ShellExecute(0,'open',PChar(Link),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+Link +'  '+e.message);
  end;
end;


procedure TTicketDetailForm.btnWaldoAddrClick(Sender: TObject);
const  //qm-449 sr
  WALDO_PATH = 'C:\Users\Public\AT&T\WALDO\';
  WALDO_PROG= 'waldo_ui_loader.exe';
  WALDO_PROG2='waldo_ui.exe';       {EB - Adding this in case it takes the window}
var
 // ReceiverHandle: THandle;   {EB - This section replaced with GetHWndByProgramName}
  WaldoHandle, WaldoHandle2: THandle;
  myWorkAddress:string;
  myLatLng:string;
  WI: TWndInfo;
begin
  inherited;
  myWorkAddress :='';
  With DM.Ticket do
  begin
    myWorkAddress :=  FieldByName('work_address_number').AsString+' '+
                      FieldByName('work_address_street').AsString+', '+
                      FieldByName('work_city').AsString+' ,'+
                      FieldByName('work_state').AsString;

   myLatLng       :=  FieldByName('Work_Lat').AsString+' '+FieldByName('Work_Long').AsString;

  end;

  if TSpeedButton(Sender).Name = 'btnWaldoLatLng' then
    ClipBoard.AsText := myLatLng
  else
    ClipBoard.AsText := myWorkAddress;
   {EB - This section replaced with GetHWndByProgramName QM-954}
//  ReceiverHandle := 0;
//  ReceiverHandle :=  FindWindow('Qt5152QWindowOwnDCIcon', nil);
  try
    {EB - This section replaced with GetHWndByProgramName QM-954}
//  if ReceiverHandle>0 then
//   ShowWindow(ReceiverHandle,SW_SHOWNORMAL)
//  else begin

    {QM-954 EB Waldo - new method to see if it is running}
    WaldoHandle := GetHWndByProgramName(WALDO_PATH + WALDO_PROG);  //QM-954 EB Waldo suppress multiple windows
    WaldoHandle2 := GetHWndByProgramName(WALDO_PATH + WALDO_PROG2);
    if WaldoHandle > 0 then  //QM-954 EB
      ShowWindow(WaldoHandle,SW_SHOWNORMAL)
    else if WaldoHandle2 > 0 then
        ShowWindow(WaldoHandle2,SW_SHOWNORMAL)
    else
      ShellExecute(Handle,'open',PChar(WALDO_PATH + WALDO_PROG),nil,nil, SW_SHOWNORMAL);   //QM-954 EB
  except
  on e:exception do
    ShowMessage('Critical error at '+ WALDO_PATH + WALDO_PROG + '  ' + e.message);
  end;
end;

procedure TTicketDetailForm.btnEfdCollClick(Sender: TObject);
var     //QMANTWO-297
  HashString : string;
  TicketIDStr, LocateIDStr, EmpIDStr : string;
  Pepper, LPepper, RPepper, BaseURL, Link: string;
const
  TID  = 'tid=';
  LID  = 'Lid=';
  EID  = 'Eid=';
  FM   = 'FM=';
  CODE = 'code=';
  AMP  = '&';
  FORMNAME = 'scanna';
begin
  inherited;

  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  BaseURL := DM.GetConfigurationDataValue('SCANA_BaseURL');
  TicketIDStr := IntToStr(FTicketID);

  LocateIDStr := IntToStr(LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index]);
  EmpIDStr := IntToStr(DM.EmpID);
  HashString := LPepper +
                TicketIDStr +
                LocateIDStr +
                EmpIDStr +
                FORMNAME +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);
  Link :=  BaseURL +
                    TID + TicketIDStr + AMP +
                    LID + LocateIDStr + AMP +
                    EID + EmpIDStr + AMP +
                    FM + FORMNAME + AMP +
                    CODE + HashString;
  try
    ShellExecute(0,'open',PChar(Link),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+Link +'  '+e.message);
  end;
end;


procedure TTicketDetailForm.btnReturnToWOClick(Sender: TObject);
begin
  inherited;
  if FAssociatedWOID > 0 then
    MainForm.ShowWorkOrder(Sender, FAssociatedWOID );
end;

procedure TTicketDetailForm.btnServiceCardClick(Sender: TObject);
var     //QMANTWO-753
  HashString : string;
  TicketIDStr, LocateIDStr, EmpIDStr : string;
  Pepper, LPepper, RPepper, BaseURL, Link: string;
const
  TID  = 'tid=';
  LID  = 'Lid=';
  EID  = 'Eid=';
  FM   = 'FM=';
  CODE = 'code=';
  AMP  = '&';
  FORMNAME = 'NIPSCO';
begin
  inherited;

  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  BaseURL := DM.GetConfigurationDataValue('IN_QCard');
  TicketIDStr := IntToStr(FTicketID);

  LocateIDStr := IntToStr(0);
  EmpIDStr := IntToStr(DM.EmpID);
  HashString := LPepper +
                TicketIDStr +
                LocateIDStr +
                EmpIDStr +
                FORMNAME +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);
  Link :=  BaseURL +
                    TID + TicketIDStr + AMP +
                    LID + LocateIDStr + AMP +
                    EID + EmpIDStr + AMP +
                    FM + FORMNAME + AMP +
                    CODE + HashString;
  try
    ShellExecute(0,'open',PChar('Iexplore.exe'),PChar(Link),nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+Link +'  '+e.message);
  end;
end;

procedure TTicketDetailForm.SetHighProfileState;
var
  IsHighProfile: Boolean;
begin
  if not Assigned(LocateGridView.Controller.FocusedRecord) then
    Exit;
  if VarIsNull(LocateGridView.Controller.FocusedRecord.Values[ColLocHighProfile.Index]) then
    IsHighProfile := False
  else
    IsHighProfile := LocateGridView.Controller.FocusedRecord.Values[ColLocHighProfile.Index];
  if IsHighProfile then begin
    ColLocHighProfileReason.Options.Editing := True;
    ColLocHighProfilePopup.Options.Editing := True;
  end
  else begin
    ColLocHighProfileReason.Options.Editing := False;
    ColLocHighProfilePopup.Options.Editing := False;
  end;
end;

procedure TTicketDetailForm.SetNotesPermissions;   //QM-188 EB Delete Note Permission
var
  limStr: string;
  DelNotesLimitation : integer;
  permDelete, permRestriction: boolean;
begin
 {Delete}
  permDelete := PermissionsDM.CanI(RightNotesDeleteNote, False);
  TicketNotesFrame.NotePermissions.CanDeleteNote := permDelete;
  TicketNotesFrame.DeleteNoteAction.Visible := permDelete;

  {Change Restriction}
  permRestriction := PermissionsDM.CanI(RightNotesChangeRestriction, False);
  TicketNotesFrame.NotePermissions.CanChangeNoteToPrivate := permRestriction;
  TicketNotesFrame.ChangeToPrivateAction.Visible := permRestriction;
  TicketNotesFrame.NotePermissions.CanChangeNoteToPublic := permRestriction;
  TicketNotesFrame.ChangeToPublicAction.Visible := permRestriction;

  {Change Note Text}
  TicketNotesFrame.NotePermissions.CanEditNote := PermissionsDM.CanI(RightNotesChangeNoteText, False);


  limStr := PermissionsDM.GetLimitation(RightNotesDeleteNote);    //QM-188 EB Not used right now
  limStr := trim(limStr);  //QM-742 Isolated Machine/Dev ex  Prevent exception

  try
    if limStr <> '' then
      DelNotesLimitation := StrToInt(limStr)
    else
      DelNotesLimitation := 0;
  except
    DelNotesLimitation := 0;
  end;

  if True then
  
end;


procedure TTicketDetailForm.ColLocHighProfilePropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  if not (Sender is TcxCheckBox) then
    Exit;
  if (Sender as TcxCheckBox).Checked = False then begin
    if ColLocHighProfileReason.Visible then
      if LocateGridView.Controller.FocusedRecord.Values[ColLocHighProfileReason.Index] <> Null then
        LocateGridView.Controller.FocusedRecord.Values[ColLocHighProfileReason.Index] := Null;
    if ColLocHighProfilePopup.Visible then
      if LocateGridView.Controller.FocusedRecord.Values[ColLocHighProfilePopup.Index] <> Null then begin
        LocateDS.Edit;
        LocateDS.DataSet.FieldByName('high_profile_multi').Clear;
      end;
  end;
end;

procedure TTicketDetailForm.SetupFacilitiesTab;
begin
  DM.GetLocateListForTicket(FTicketID, FacilityClient.Items);
  DM.GetRefDisplayList('factype', FacilityType.Items, '', True);
  ResetFacilityControls;
end;

procedure TTicketDetailForm.UpdateDependantFacilityCombos;

  procedure SetupDependentCombo(const RefType, Modifier: string; Combo: TComboBox);
  begin
    if Modifier = '' then begin
      Combo.Items.Clear;
      Combo.Enabled := False;
    end else begin
      try
        DM.GetRefDisplayList(RefType, Combo.Items, Modifier);
        Combo.Enabled := True;
      except
        on EOdReferenceMissing do
          Combo.Enabled := False;
      end;
    end;
  end;

var
  Modifier: string;
begin
  Modifier := DM.GetRefCodeForDisplay('factype', FacilityType.Text);
  SetupDependentCombo('facsize', Modifier, FacilitySize);
  SetupDependentCombo('facmtrl', Modifier, FacilityMaterial);
  SetupDependentCombo('facpres', Modifier, FacilityPressure);
end;

procedure TTicketDetailForm.UpdateDetailFollowupTicketType;
begin
  if FFollowupTicketState = fsNotAllowed then
    DetailFollowupTicketTypeLabel.Visible := False
  else
    DetailFollowupTicketTypeLabel.Visible := True;

  if (FFollowupTicketState in [fsReadyToCreate, fsCreated]) and
    (FollowupTicketType.ItemIndex >= 0) then
    DetailFollowupTicketType.Caption := FollowupTicketType.Text
  else
    DetailFollowupTicketType.Caption := '';
end;

procedure TTicketDetailForm.UpdateFollowupButton;
begin
  if FFollowupTicketState in [fsCanCreate, fsNeedToVerify, fsReadyToCreate]
    then begin
    FollowupButton.Visible := True;
 //   ButtonSeparator.Visible := True;

    if ShowingTicketSummary then
      FollowupButton.Enabled := False
    else
      FollowupButton.Enabled := True;
  end
  else begin
    FollowupButton.Visible := False;
    FollowupButton.Enabled := False;
//    ButtonSeparator.Visible := False;
  end;
  //The new devex popup control requires visibility before it will show popup;
  //so it is made visible but hidden behind the button.
  FollowupPopupEdit.Visible := FollowupButton.Visible;
end;

procedure TTicketDetailForm.ResetFollowupPopupControls;
begin
  FollowupTicketType.ItemIndex := -1;
  FFollowupTransmitDate := 0;
  FollowupTransmitDate.Caption := '';
  FollowupDueDate.Date := NullDate;
  FollowupDueTime.Time := 0;
end;

procedure TTicketDetailForm.SetupFollowupTicket;
begin
  DM.GetFollowupTicketTypesForCallCenter(DM.Ticket.FieldByName('ticket_format').AsString, FollowupTicketType.Items);
  ResetFollowupPopupControls;
  FFollowupTicketState := fsNotAllowed;

  //A followup ticket can now be created from any kind of ticket (including another followup ticket),
  //and a ticket can have multiple followup tickets created from it.
  if PermissionsDM.CanI(RightTicketsCreateFollowUp, False) then begin
    if FollowupTicketType.Items.Count > 0 then
      FFollowupTicketState := fsCanCreate;
  end;

  UpdateDetailFollowupTicketType;
  UpdateFollowupButton;
end;

procedure TTicketDetailForm.SaveFollowupTicketState;
var
  FollowupID: Integer;
  FPDueDate: TDateTime;
begin
  if FFollowupTicketState = fsReadyToCreate then begin
    //update ticket locally
    FollowupID := GetComboObjectInteger(FollowupTicketType);
    FPDueDate := DateOf(FollowupDueDate.Date) + TimeOf(FollowupDueTime.Time);
    DM.SetTicketFollowupType(FollowupID, FFollowupTransmitDate, FPDueDate);
  end;
end;


procedure TTicketDetailForm.CreateFollowupTicket;
begin
  if FFollowupTicketState = fsReadyToCreate then begin
    //Create followup ticket via server op
    DM.CallingServiceName('CreateFollowupTicket');
    DM.Engine.Service.CreateFollowupTicket(DM.Engine.SecurityInfo, FTicketID,
      DM.Ticket.FieldByName('followup_type_id').AsInteger,
      DM.Ticket.FieldByName('followup_due_date').AsDateTime,
      DM.Ticket.FieldByName('followup_transmit_date').AsDateTime);
    FFollowupTicketState := fsCreated;
    DM.SetTicketFollowupType(0, 0, 0);
  end;
end;

procedure TTicketDetailForm.CreateNoteForStatusChange(pTicketID: integer);  //QM-463 EB
const
  TktSubTypeInt = ((qmftTicket * 100) + 1);
var
  i: integer;
  ClientStr, Msg: string;
  NeedMsg: Boolean;
  ChangeEmpName: string;
begin
  ClientStr := '';
  Msg := '';
  NeedMsg := False;
  ChangeEmpName := EmployeeDM.GetUserShortName;
{Check for statuses changed and create note for Mgmt changes}
  if PreserveClosedDateCBox.Checked then begin   // QM-463 EB
    for i := 0 to (length(fRecentStatusChanges) - 1) do begin
      if fRecentStatusChanges[i].Changed then begin
        NeedMsg := True;
        if ClientStr <> '' then
          ClientStr := ClientStr + '';

        ClientStr := ClientStr + ' ' +
                     fRecentStatusChanges[i].ClientName + '(' + fRecentStatusChanges[i].Status + ') ';
      end;
    end;

    if NeedMsg then begin
      Msg := 'Status Change(s) made by ' + ChangeEmpName + ' preserved the closed date: ' + ClientStr;
      DM.SaveBaseNoteToCache(Msg, qmftTicket, pTicketID, DM.TicketNotesData, TktSubTypeInt, IntToStr(TktSubTypeInt));
    end;
  end;
end;

procedure TTicketDetailForm.PrintButtonClick(Sender: TObject);
begin
  if Assigned(OnTicketPrint) then
    OnTicketPrint(Self, FTicketID);
end;


procedure TTicketDetailForm.PrintTicketVersionImage1Click(Sender: TObject);  //qm-530 sr
var //calls a small exe to run a PDF report for ticket version
  TicketVersionID:string;
  dbIsamDir:string;
  parameters:string;
Const
  DBL_QUOTES='"';    //QM-555 sp
begin
  inherited;
  TicketVersionID:= TicketVersions.FieldByName('Ticket_Version_ID').AsString;
  dbIsamDir:=DM.Database1.Directory;
  parameters:=  DBL_QUOTES+dbIsamDir+DBL_QUOTES +' '+DBL_QUOTES+TicketVersionID+DBL_QUOTES; //QM-555
  shellexecute(0, 'Open', PChar('Print2PDF.exe'), PChar(parameters), nil, SW_SHOW);
end;

procedure TTicketDetailForm.LocateGridViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
  var
  ClientID : integer;
begin
  inherited;
  AHandled := True;  //qM-585 PLUS Whitelist - Steven looked up error and said to do this
  ClientID := LocateDS.DataSet.FieldByName('client_id').AsInteger;
  frmClientInfo:= TfrmClientInfo.Create(self, ClientID);
  frmClientInfo.ShowModal;
end;


procedure TTicketDetailForm.LocateGridViewCustomDrawCell( //replaces OnCustomDrawCell
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  ThisCC: string;
  UnauthorizedMsgFlag, PlusRestricted: Boolean;
  LocShortName: string;  //QM-804 EB Disable other locator's locates
const
  MSGNotAuth = '* Not authorized to status some locates';
begin
  inherited;
  UnauthorizedMsgFlag := FALSE;
  PlusRestricted := False;  //QM-921 Restriction Message not being displayed

  if not AViewInfo.Focused then begin
    if RecordIsReadOnly(AViewInfo.GridRecord) then
      ACanvas.Brush.Color := clBtnFace; //Silver;
  end;

  if not VarIsNull(AViewInfo.GridRecord.Values[ColParentClientCode.Index]) then begin    //QMANTWO-616
    ACanvas.Font.Color := clBlue;
    lblProjectLocatesText.Visible := True;
  end
  else begin
    ACanvas.Font.Color := clBlack;
  end;

  {This Locate is restricted to the current locator -StatusPlus}
   if not VarIsNull(AviewInfo.GridRecord.Values[ColLocClientCode.Index]) then begin    //QM-585 EB PLUS WHITELIST
     ThisCC := AviewInfo.GridRecord.Values[ColLocClientCode.Index];
     if DM.IsRestricted(ThisCC) then begin
       ACanvas.Font.Color := clMaroon;
       UnauthorizedMsgFlag := True;
       PlusRestricted := True;
     end;
  end;

  if not VarIsNull(AviewInfo.GridRecord.Values[ColLocShortName.Index]) then begin  //QM-804 EB Disable Other locator
    LocShortName := AviewInfo.GridRecord.Values[ColLocShortName.Index];

    {Locate is not assigned to this locator, but he/she is allowed to status it}
    if (LocShortName <> EmployeeDM.GetEmployeeShortName(DM.EmpID))  then
     If (StatusAsAssignedLocatorBox.Checked) and
        (StatusAsAssignedLocatorBox.Visible) and
        (PlusRestricted = False) then begin   //QM-921 EB Message not being shown
      ACanvas.Font.Color := clNavy;
      UnauthorizedMsgFlag := False;
    end
    else if (PlusRestricted = False) then begin {Different Locator's locate - show as disabled}
      ACanvas.Font.Color := clGrayText;
      UnauthorizedMsgFlag := True;
//      if PermissionsDM.CanI(RightStatusLocatesAsAssigned, False) then

    end;
  end;

  if UnauthorizedMsgFlag then begin
       lblLocateMessage.Caption := MSGNotAuth;
       lblLocateMessage.Visible := True;
  end
end;

procedure TTicketDetailForm.LocateGridViewEditValueChanged( //replaces OnEdited event; closest equivalent in new grid
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
var
  ACol: TcxGridDBColumn;
  FieldName: string;
begin
  inherited;
  //The new TcxGrid does not support the OnEdited event; so this event is used as
  //the alternative.  Post the data to simulate the OnEdited event;  and then call
  // the RefreshLocateReadOnlyCache method that refreshes the list and updates the grid.
  Sender.DataController.PostEditingData;
  RefreshLocateReadOnlyCache;
  ACol := TcxGridDBColumn(AItem);
  FieldName := ACol.DataBinding.FieldName;
  if FieldName = 'status' then begin
    fLocateChanged := True;   //QMANTWO-775 EB for seeing if we need to trigger route_order

  end;
end;

procedure TTicketDetailForm.RefreshLocateReadOnlyCache;
var
  ARecord: TcxCustomGridRecord;
  LocateID: Integer;
  CC: string;  //QM-585
  i: Integer;
  LocShortName: string;
  AllowOtherLocates: boolean;
begin
  LocShortName := '';
  FReadOnlyCache.Clear;
  AllowOtherLocates := PermissionsDM.CanI(RightStatusLocatesAsAssigned, FALSE);    //QM-804 EB Disable other locators locates

  {If user has permissions and the Status box is checked}
  If (not AllowOtherLocates) or (not StatusAsAssignedLocatorBox.Checked) then
    AllowOtherLocates := False;

  for i := 0 to LocateGridView.ViewData.RecordCount - 1 do begin
    ARecord := LocateGridView.ViewData.Records[i];
    LocateID := ARecord.Values[ColLocLocateID.Index];
    CC := ARecord.Values[ColLocClientCode.Index];
    if (ARecord.Values[ColLocShortName.Index]) <> NULL then    //QM-860 EB Find that freaking empty grid value
	    LocShortName := trim(ARecord.Values[ColLocShortName.Index]);  //QM-804 EB Disable other locators
    if (not DM.IsLocateEditable(LocateID)) {or (DM.LocateHasChildLocates(fTicketID, LocateID))} then   //QMANTWO-616 EB
      FReadOnlyCache.Add(Pointer(LocateID))
    else if (LocShortName <> (EmployeeDM.GetEmployeeShortName(DM.EmpID))) and (not AllowOtherLocates) then  //QM-804 EB Disable other locators
      FReadOnlyCache.Add(Pointer(LocateID))
    else if DM.IsRestricted(CC) then
      FReadOnlyCache.Add(Pointer(LocateID));
  end;
  UpdateCurrentGridRow; //QM-804 EB
end;

function TTicketDetailForm.RecordIsReadOnly(ARecord: TcxCustomGridRecord): Boolean;    //qm-804 EB just marking location
var
  LocateID: Integer;
begin
  Assert(Assigned(ARecord));
  LocateID := ARecord.Values[ColLocLocateID.Index];
  Result := FReadOnlyCache.IndexOf(Pointer(LocateID)) > -1;
end;

procedure TTicketDetailForm.ActivatingNow;
begin
  inherited;
  LeavingTicket := False; //QM-635 EB StatusPlus
  HistoryTab.TabVisible := PermissionsDM.CanI(RightViewTicketHistory) or PermissionsDM.IsTicketManager;
  PrepareTicketAddins;
  ClearAssociatedWO;    //QMANTWO-296
end;

procedure TTicketDetailForm.StatusListFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
  Status: string;
  LocClientID: Integer;
  tktTicketType:string;
  dsMeetOnly: boolean;
  dsTicketTypeEx: string; begin
  inherited;
  {QM-635 Status Plus EB - Note: StatusPlus does not use this filter see statuslist_sp}
  Assert(Assigned(LocateDS.DataSet));
  if DM.Ticket.Active then begin   //QM-282 Only filter if we are staying on screen
    Status := trim(DataSet.Fields[0].AsString);
    tktTicketType := dm.Ticket.FieldByName('ticket_type').asString;
    LocClientID := LocateDS.DataSet.FieldByName('client_id').AsInteger;
    dsMeetOnly := DataSet.FieldByName('meet_only').AsBoolean;
    dsTicketTypeEx := DataSet.fieldByName('TicketTypeExclusion').asString;
    Accept := false;   //QM-265  sr

    if DataSet.FieldByName('client_id').AsInteger = LocClientID then
    begin
      Accept := true;
      Accept := Accept and DM.IsValidStatusAll(Status, FTicketID, tktTicketType, LocClientID, dsMeetOnly, dsTicketTypeEx); //qm-265 sr   QM-703 Status Dropdown EB
    end;
  end;
end;

procedure TTicketDetailForm.LoadRequirements(Index: integer; CurRowStatus: string);     //QM-282 I THINK WE CAN DELETE
begin
  If fRecentStatusChanges[Index].Status <> CurRowStatus then begin
    fRecentStatusChanges[Index].Changed := True;
    fRecentStatusChanges[Index].Status := CurRowStatus;
    if StatusList.Locate('client_id; status', VarArrayOf([fRecentStatusChanges[Index].ClientID, CurRowStatus]), [loCaseInsensitive]) then begin
      fRecentStatusChanges[Index].ReqPictures := StatusList.FieldByName('PicturesRequired').AsBoolean;
      fRecentStatusChanges[Index].ReqNotes := StatusList.FieldByName('NotesRequired').AsBoolean;
    end;
  end;

  fCurClientStatusRequires := fRecentStatusChanges[Index];  {change our pointer}
end;

procedure TTicketDetailForm.LocateDSDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Field = nil then begin
    FCurrentStatusOfSelectedLocate := LocateDS.DataSet.FieldByName('Status').AsString;
    StatusList.Filtered := False;
  end
end;

procedure TTicketDetailForm.ColLocHighProfilePopupPropertiesInitPopup(Sender: TObject);
var
  ReasonSL: TStringList;
  Code: string;
begin
  inherited;
  ReasonSL := TStringList.Create;
  try
    ReasonSL.CommaText := LocateDS.DataSet.FieldByName('high_profile_multi').AsString;
    HpReasonCheckList.Items.Clear;
    FReasonCodes.Clear;
    HpMultiReasonQuery.First;
    while not HpMultiReasonQuery.Eof do begin
      HpReasonCheckList.Items.Add(HpMultiReasonQuery.FieldValues['reason']);
      Code := HpMultiReasonQuery.FieldValues['code'];
      FReasonCodes.Add(Code);
      if ReasonSL.IndexOf(Code) > -1 then
         HpReasonCheckList.Checked[HpReasonCheckList.Count-1] := True;
      HpMultiReasonQuery.Next;
    end;
  finally
    FreeAndNil(ReasonSL);
  end;
  SizeCheckListPopupToItems(Sender as TcxPopupEdit, HpReasonCheckList);
end;

procedure TTicketDetailForm.ColLocHighProfilePopupPropertiesCloseUp(Sender: TObject);
var
  I: Integer;
  ReasonsToDisplay: String;
begin
  inherited;
  ReasonsToDisplay := '';
  for I := 0 to FReasonCodes.Count - 1 do
    if HpReasonCheckList.Checked[I] then begin
      if ReasonsToDisplay <> '' then
        ReasonsToDisplay := ReasonsToDisplay + ',';
      ReasonsToDisplay := ReasonsToDisplay + FReasonCodes[I];
    end;
  LocateDS.Edit;
  LocateDS.DataSet.FieldByName('high_profile_multi').AsString := ReasonsToDisplay;
end;

procedure TTicketDetailForm.OpenMultiReasonQueryToCenter;
var
  CC: string;
begin
  CC := TicketDS.DataSet.FieldByName('ticket_format').AsString;
  HpMultiReasonQuery.Close;
  HpMultiReasonQuery.Params[0].AsString := CC;
  HpMultiReasonQuery.Open;
end;

procedure TTicketDetailForm.OpenTicketInfo(TicketID: integer);
begin
//   if TicketInfoQuery.Active then TicketInfoQuery.Close;
//   TicketInfoQuery.ParamByName('TicketID').AsInteger := TicketID;
//   TicketInfoQuery.Open;

    if LastSMD.Active then LastSMD.close;
    LastSMD.ParamByName('TicketID').AsInteger := FTicketID;
    LastSMD.Open;

    if LastSMD.EOF then begin                           //QM-216 EB - Hide for CA (or when there is no value)
      lblLastScheduledDate.Visible := False;
      dbLastSMD.Visible := False;
    end
    else begin
      lblLastScheduledDate.Visible := True;
      dbLastSMD.Visible := True;
    end;


end;

procedure TTicketDetailForm.VerifyReasonEntered;

  function IsHighProfile: Boolean;
  begin
    Result := LocateDS.DataSet.FieldByName('high_profile').AsBoolean;
  end;

  function SingleHPReason: Boolean;
  begin
    Result := not LocateDS.DataSet.FieldByName('high_profile_reason').IsNull;
  end;

  function MultiHPReason: Boolean;
  begin
    Result := not LocateDS.DataSet.FieldByName('high_profile_multi').IsNull;
  end;

begin
  LocateDS.DataSet.First;
  while not LocateDS.DataSet.EOF do begin
    if IsHighProfile then begin
      if FMultiReasonMode then begin
        if not MultiHPReason then
          raise EOdEntryRequired.Create('You must specify at least one High Profile Reason');
      end else begin
        if not SingleHPReason then
          raise EOdEntryRequired.Create('You must specify a High Profile Reason');
      end;
    end;
    LocateDS.DataSet.Next;
  end;
end;

procedure TTicketDetailForm.ShowLocatePlatEditor;
var
  LocateID: Integer;
  Client: string;
begin
  Assert(Assigned(LocateDS.DataSet));
  if LocateGridView.Controller.FocusedRecord = nil then
    raise EOdNonLoggedException.Create('Please select a locate first');
  if HaveUnsavedLocate then
    raise Exception.Create('Please save and sync the newly added locate before adding plats');

  LocateID := LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index];
  Client := LocateGridView.Controller.FocusedRecord.Values[ColLocClientCode.Index];
  if LocateDS.DataSet.State in dsEditModes then
    LocateDS.DataSet.Post;
  EditLocatePlats(LocateID, Client);
  LocateDS.DataSet.Refresh;
end;

procedure TTicketDetailForm.LocatePlatButtonClick(Sender: TObject);
begin
  ShowLocatePlatEditor;
end;



procedure TTicketDetailForm.IDCopyGridViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  TheId: integer;
begin
  inherited;
  TheID:= 0;
  try
  {QM-621 EB Copy IDs In History Tabs for Research Purposes -> Developer Mode only}
  if DM.UQState.DeveloperMode then begin
    {Locate Status Grid}
    if (ACellViewInfo.Item = colLocStLocateID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[colLocStLocateID.Index])) then
      TheId:= ACellViewInfo.GridRecord.Values[colLocStLocateID.Index]

    {Assignments Grid}
    else if (ACellViewInfo.Item = AssignColLocateID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[AssignColLocateID.Index])) then
      TheId:= ACellViewInfo.GridRecord.Values[AssignColLocateID.Index]
    else if (ACellViewInfo.Item = AssignColAssignmentID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[AssignColAssignmentID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[AssignColAssignmentID.Index]
    else if (ACellViewInfo.Item = AssignColLocatorID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[AssignColLocatorID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[AssignColLocatorID.Index]

    {Responses Grid}
    else if (ACellViewInfo.Item = RespColResponseID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[RespColResponseID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[RespColResponseID.Index]
    else if (ACellViewInfo.Item = RespColLocateID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[RespColLocateID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[RespColLocateID.Index]

    {Billing Grid}
    else if (ACellViewInfo.Item = ColBillLocateID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColBillLocateID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColBillLocateID.Index]
    else if (ACellViewInfo.Item = ColBillInvoiceID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColBillInvoiceID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColBillInvoiceID.Index]

    {Version/Revision Grid}
    else if (ACellViewInfo.Item = ColTktVerVersionID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColTktVerVersionID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColTktVerVersionID.Index]
    else if (ACellViewInfo.Item = ColTktVerTicketRevision) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColTktVerTicketRevision.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColTktVerTicketRevision.Index]

    {Acknowledgements Grid}
     else if (ACellViewInfo.Item = ColAckTicketID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColAckTicketID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColAckTicketID.Index]
    else if (ACellViewInfo.Item = ColAckAckEmpID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColAckAckEmpID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColAckAckEmpID.Index]

    {Arrivals Grid}
    else if (ACellViewInfo.Item = ColArrArrivalID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColArrArrivalID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColArrArrivalID.Index]
    else if (ACellViewInfo.Item = ColArrEmpID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColArrEmpID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColArrEmpID.Index]

    {Facilities Grid}
    else if (ACellViewInfo.Item = ColFacHistLocateFacilityID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColFacHistLocateFacilityID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColFacHistLocateFacilityID.Index]
    else if (ACellViewInfo.Item = ColFacHistLocateID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColFacHistLocateID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColFacHistLocateID.Index]

    {Risk Scores}
    else if (ACellViewInfo.Item = ColTicketRiskID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColTicketRiskID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColTicketRiskID.Index]
    else if (ACellViewInfo.Item = ColTicketVersionID) and
    (not VarIsNull(ACellViewInfo.GridRecord.Values[ColTicketVersionID.Index])) then
      TheID:=ACellViewInfo.GridRecord.Values[ColTicketVersionID.Index];

    if TheID > 0 then
      Clipboard.AsText := IntToStr(TheId);
    {else it will leave whatever is in the clipboard alone}
  end;
  except
    {Swallow - If VM gets weird about clipboard, hopefully, this will bypass it}

  end;
end;

procedure TTicketDetailForm.ColLocLocatePlatsPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  ShowLocatePlatEditor;
end;

procedure TTicketDetailForm.VerifyMarkTypeEntered;

  function IsMarkTypeRequired: Boolean;
  begin
    if StatusList.Locate('status', LocateDS.Dataset.FieldByName('status').AsString, []) then
      Result := StatusList.FieldByName('mark_type_required').AsBoolean
    else
      Result := False;
  end;

  function IsMarkTypeSelected: Boolean;
  begin
    Result := not LocateDS.DataSet.FieldByName('mark_type').IsNull;
  end;

begin
  LocateDS.DataSet.First;
  while not LocateDS.DataSet.EOF do begin
    if IsMarkTypeRequired and (not IsMarkTypeSelected) then
      raise EOdEntryRequired.Create('A mark type is required for the selected status.');

    LocateDS.DataSet.Next;
  end;
end;

procedure TTicketDetailForm.VerifyJobsiteArrival;
var
  Resp: Integer;
begin
  if DM.TicketNeedsArrivalDate(FTicketID) then begin
    Resp := MessageDlgFmt('Are you sure you want to status this ticket without arriving?', [],
      mtConfirmation, [mbYes, mbNo], mrNo);
    if Resp <> mrYes then
      Abort;
  end;
end;

procedure TTicketDetailForm.ValidateFollowupTicket;
begin
  if FollowupTicketType.ItemIndex < 0 then
    raise EOdEntryRequired.Create('A ticket type is required for creating a new followup ticket.');

  if FFollowupTransmitDate <= 0 then
    { A manual entry should never raise this exception, since the transmit date
      should have been automatically set to a value > 0 }
    raise EOdEntryRequired.Create('A transmit date and time are required for creating a new ' +
      'followup ticket.');

  if (FollowupDueDate.Date = NullDate) or (FollowupDueDate.Text = '') then
    raise EOdEntryRequired.Create('A due date and time are required for creating a new ' +
      'followup ticket.');

  if not (CompareDateTime(DateOf(FollowupDueDate.Date) + TimeOf(FollowupDueTime.Time),
    FFollowupTransmitDate) = GreaterThanValue) then
    raise EOdDataEntryError.Create('The followup ticket''s due date/time must be later than its ' +
      'transmit date/time.');

  FFollowupTicketState := fsReadyToCreate;
end;



function TTicketDetailForm.VerifyFacilitiesTab: Boolean;
var HasMarked: Boolean;
begin
  //Ken Funk 2016-01-14
  result := True;
  HasMarked := False;
  //Check if required by Call Center
  if FCallCentersRequireFacilitiesTab.IndexOf(TicketDS.DataSet.FieldByName('ticket_format').AsString) > -1 then begin
    LocateDS.DataSet.First;
    while not(LocateDS.DataSet.Eof) and not(HasMarked) do begin
      if FLocationMarkStatusList.IndexOf(LocateDS.DataSet.FieldByName('status').AsString) > -1 then begin
         HasMarked := True;
      end;
      LocateDS.DataSet.Next;
    end;
  end;
  if HasMarked then begin
    result := not(LocateFacilityDS.DataSet.IsEmpty);
  end;
end;

procedure TTicketDetailForm.VerifyFollowupTicketCreated;
begin
  if FFollowupTicketState in [fsNeedToVerify, fsReadyToCreate] then
    ValidateFollowupTicket;
end;

procedure TTicketDetailForm.SetDefaultMarkType;

  function DefaultMarkType: string;
  begin
    Result := StatusList.FieldByName('default_mark_type').AsString;
  end;

begin
  if not Assigned(LocateGridView.Controller.FocusedRecord) then
    Exit;
  if not (LocateDS.DataSet.State in dsEditModes) then
    Exit;
  if not LocateDS.DataSet.FieldByName('mark_type').IsNull then
    Exit;
  if not StatusList.Locate('status', LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index], []) then
    Exit;
  LocateDS.DataSet.FieldByName('mark_type').AsString := DefaultMarkType;
end;

procedure TTicketDetailForm.SetDMDueDateChanged(Value: boolean);  //QM-216 EB Due Date
begin
{Only want to set DM checks here}
  fDueDateChanged := Value;
  DM.TicketDueDateChanged := fDueDateChanged;
end;

function TTicketDetailForm.ExecuteAttachmentProcess(Mode: TAttachmentMode; const Param: string): Boolean;
begin
  Result := False;
  if FAttachingFilesNow then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    FAttachingFilesNow := True;
    TicketPages.ActivePage := AttachmentsTab;
    if Mode = amStillImage then
      AttachmentsFrame.DoWIAAttachment
    else if Mode = amDeviceNotification then
      AttachmentsFrame.DoDefaultAttachment(Param);
    Result := AttachmentsFrame.FilesWereAttached;
  finally
    FAttachingFilesNow := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TTicketDetailForm.ArriveButtonClick(Sender: TObject);
var
  Resp: Integer;
  Cursor: IInterface;
begin
  if not DM.IsTicketAssignedToLocator(FTicketID, DM.UQState.EmpID) then begin
    Resp := MessageDlgFmt('You are not assigned to this ticket. Are you sure you want to arrive?', [],
      mtConfirmation, [mbYes, mbNo], mrNo);
    if Resp <> mrYes then
      Abort;
  end;
  Cursor := ShowHourGlass;

  DM.CreateArrival(FTicketID, 'CLOCK', DM.EmpID, Now, atTicket);
  if (FTicketID > 0) and PermissionsDM.CanI(RightTicketsImmediateSync) then
    DM.SaveArrivalsToServer(FTicketID, atTicket);

  EnableDisableControls;
  if ShowingTicketSummary then
    HideTicketSummary;
  SetArriveButtonEnabled(False);

  DM.ExportQMDataAsXML;
  ShowTicketWorkDescription;
end;

procedure TTicketDetailForm.FacilityTypeChange(Sender: TObject);
begin
  UpdateDependantFacilityCombos;
end;

procedure TTicketDetailForm.FacilityTabShow(Sender: TObject);
begin
  if LocateDS.DataSet.Active then
    SelectComboBoxItemFromObjects(FacilityClient, LocateDS.DataSet.FieldByName('locate_id').AsInteger)
  else
    FacilityClient.ItemIndex := -1;
  ResetFacilityControls;
end;

procedure TTicketDetailForm.AddFacilityButtonClick(Sender: TObject);

  procedure AssignNonBlankRefCodeFromCombo(Field: TField; Combo: TCombobox; RefType, Modifier: string);
  var
    RefCode: string;
  begin
    RefCode := DM.GetRefCodeForDisplay(RefType, Combo.Text, Modifier);
    if NotEmpty(RefCode) then
      Field.AsString := RefCode;
  end;

var
  i: Integer;
  LocateID: Integer;
  Facility: TDataSet;
  Cursor: IInterface;
  FacType: string;
begin
  Cursor := ShowHourGlass;
  Facility := LocateFacilityDS.DataSet;
  LocateID := GetComboObjectInteger(FacilityClient);
  FacType := DM.GetRefCodeForDisplay('factype', FacilityType.Text, '');
  if FacType = '' then begin
    FacilityType.SetFocus;
    raise EOdDataEntryError.Create('You must select a Utility Type before adding a facility.');
  end;

  for i := 1 to AddHowMany.Value do begin
    Facility.Append;
    Facility.FieldByName('locate_id').AsInteger := LocateID;
    Facility.FieldByName('active').AsBoolean := True;
    Facility.FieldByName('added_date').AsDateTime := RoundSQLServerTimeMS(Now);
    Facility.FieldByName('added_by').AsInteger := DM.EmpID;
    Facility.FieldByName('facility_type').AsString := FacType;
    AssignNonBlankRefCodeFromCombo(Facility.FieldByName('facility_material'), FacilityMaterial, 'facmtrl', FacType);
    AssignNonBlankRefCodeFromCombo(Facility.FieldByName('facility_size'), FacilitySize, 'facsize', FacType);
    AssignNonBlankRefCodeFromCombo(Facility.FieldByName('facility_pressure'), FacilityPressure, 'facpres', FacType);
    Facility.Post;
    Sleep(10); // Ensure no two facilities get added within a 3 ms span (the resolution of a datetime field)
  end;
  FacilityGrid.Refresh;
  ResetFacilityControls;
end;

procedure TTicketDetailForm.DeleteFacilityButtonClick(Sender: TObject);
var
  Facility: TDataSet;
begin
  Facility := LocateFacilityDS.DataSet;
  if not HasRecords(Facility) then
    raise EOdDataEntryError.Create('There are no facilities to delete.');

  if not DM.IsLocateFacilityEditable(Facility.FieldByName('locate_facility_id').Value) then
    raise EOdDataEntryError.Create('You can only delete facilities you add.');

  if YesNoDialogFmt('Are you sure you want to delete the selected facility for %s?',
    [Facility.FieldByName('client_code').AsString]) then begin
    EditDataSet(Facility);
    Facility.FieldByName('deleted_by').Value := DM.EmpID;
    Facility.FieldByName('deleted_date').Value := Now;
    Facility.FieldByName('active').Value := False;
    Facility.FieldByName('modified_by').Value := DM.EmpID;
    Facility.FieldByName('DeltaStatus').Value := 'U';

    PostDataSet(Facility);
    // Save the ticket after deletes to flush the updates so the grid will refresh.
    DM.SaveLocateFacilityChanges;
    RefreshDataSet(Facility);
  end;
end;

procedure TTicketDetailForm.ResetFacilityControls;
var
  LocateID: Integer;
  FacType: string;
begin
  LocateID := GetComboObjectInteger(FacilityClient, False);
  FacType := DM.GetLocateFacilityTypeDesc(LocateID);
  FacilityType.ItemIndex := FacilityType.Items.IndexOf(FacType);
  UpdateDependantFacilityCombos;
  AddHowMany.Value := 1;
end;

procedure TTicketDetailForm.LocateLookupBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('client_name', DataSet, 'client_id', DM.ClientLookup, 'client_id', 'client_name');
end;

procedure TTicketDetailForm.FacilityClientSelect(Sender: TObject);
begin
  ResetFacilityControls;
end;

procedure TTicketDetailForm.PopulateCallCentersRequireFacilitiesTab;
begin
  //Ken Funk 2016-01-14 Build List
  FCallCentersRequireFacilitiesTab.Clear;
  FCallCentersRequireFacilitiesTab.Delimiter := '|';
  FCallCentersRequireFacilitiesTab.StrictDelimiter := True;
  //Configuration_Data is Hard Coded in Sync_6.
  //Cannot Dynamically pull down values until changed.
  //FCallCentersRequireFacilitiesTab.DelimitedText :=
  //  Dm.GetConfigurationDataValue('QM_CCentersReqFacilitiesT');
  FCallCentersRequireFacilitiesTab.DelimitedText := 'FCO1';
end;


procedure TTicketDetailForm.PopulateLocationMarkStatusList;
begin
  //Ken Funk 2016-01-14
  DM.GetLocationMarkStatusList(FLocationMarkStatusList);
end;

procedure TTicketDetailForm.PopulateNoteSourceCombo;
begin
  DM.TicketAndLocatesList(DM.Ticket.FieldByName('ticket_number').AsString, NoteSource.Items);

  SizeComboDropdownToItems(NoteSource);
  if NoteSource.Items.Count > 0 then
    NoteSource.ItemIndex := 0;
  FormStatus.NoteSourceLoaded := True;  //QM-577 EB
end;



procedure TTicketDetailForm.PopulateNoteSubType(NoteType: integer);
begin
  DM.NotesSubTypeList(FNoteSubTypeList, NoteType);   //QM-576  sr  step 2
end;


procedure TTicketDetailForm.GetNoteSource;
begin
  if NoteSource.ItemIndex = -1 then
    raise EOdDataEntryError.Create('Please select the ticket or locate you are adding the note to.');

  if NoteSource.ItemIndex = 0 then begin // The ticket is always the first in the list
    TicketNotesFrame.NoteType := qmftTicket;
    TicketNotesFrame.ForeignId := FTicketID;
  end
  else begin
    TicketNotesFrame.NoteType := qmftLocate;
    TicketNotesFrame.ForeignId := GetComboObjectInteger(NoteSource, True);
  end;
  TicketNotesFrame.NoteSubType := FNoteSubTypeList.GetCode(NoteSubTypeCombo.Text);
  TicketNotesFrame.SubTypeText := NoteSubTypeCombo.Text;
end;

// This is temporary until we sort out the duplicate index errors
procedure TTicketDetailForm.ColFacAddedDateGetDataText(
  Sender: TcxCustomGridTableItem; ARecordIndex: Integer;
  var AText: String);
var
  AddedDate: Variant;
  AddedDateTime: TDateTime;
begin
  inherited;
  AddedDate := FacilityGridView.DataController.Values[ARecordIndex, (Sender as TcxCustomGridTableItem).Index];
  if not VarIsNull(AddedDate) then begin
    AddedDateTime := VarToDateTime(AddedDate);
    AText := FormatDateTime(ShortDateFormat + ' ' + 'h:m:s.z', AddedDateTime);
  end
  else
    AText := '';
end;

procedure TTicketDetailForm.UpdateCameraStatus;
begin
  AttachmentsFrame.UpdateCameraStatus;
end;

procedure TTicketDetailForm.WMStiLaunchNotification(var Message: TMessage);
begin
  if (Owner is TForm) then
    PostMessage(TForm(Owner).Handle, WM_STI_LAUNCH, 0, 0);
end;

procedure TTicketDetailForm.SetupArrivalControls;
begin
  PanelArrivalButtons.Visible := PermissionsDM.CanI(RightTicketsEditArrivals);
  PanelArrivalModify.Visible := False;
end;

procedure TTicketDetailForm.SetupAttachmentsTab;
begin
  if (DM.TicketIsOpen = FALSE) and PermissionsDM.CanI(RightTicketsHideAttachmentsForClosed) then
    AttachmentsTab.TabVisible := FALSE
  else
    AttachmentsTab.TabVisible := TRUE
end;

procedure TTicketDetailForm.DeleteArrivalActionExecute(Sender: TObject);
begin
  if YesNoDialog('Do you really want to delete the selected arrival?') then begin
    DM.DeleteJobsiteArrival;
    EnableDisableControls;
  end;
end;

procedure TTicketDetailForm.ButtonNewArrivalCancelClick(Sender: TObject);
begin
  PanelArrivalButtons.Visible := True;
  PanelArrivalModify.Visible := False;
end;


function TTicketDetailForm.OpenProjectMultiLocatorList: boolean;   //QMANTWO-616 EB
const
  TktSubTypeInt = ((qmftTicket * 100) + 1);
var
 SelectedMultiLocatorList: TStringList;
 PermissionsMgrID: integer;
 Limitation: string;
 LimitationID: integer;
 TicketID, LocateID, LocatorID : integer;
 NewLocateData: TLocateClientData;
 TktSubTypeStr: String;
 CurrentMarkedAmt: integer;
begin
  {EB - When/If Project tickets are implemented, need to follow permission scheme and
        make sure the PermissionsMgrID is always set.}
  TktSubTypeStr := intToStr(TktSubTypeInt);
  Limitation := '';
  PermissionsMgrID := 0;
  if (PermissionsDM.CanI(RightMultiLocatorsForProjTicket, False)) then begin
    PermissionsMgrID := EmployeeDM.GetManagerIDForEmp(DM.LocateData.FieldByName('locator_id').AsInteger);
    Limitation := PermissionsDM.GetLimitation(RightMultiLocatorsForProjTicket);
    if (Limitation <> '') and (tryStrToInt(Limitation, LimitationID)) then
      PermissionsMgrID := LimitationID
    else
      PermissionsMgrID := DM.EmpID;
  end
  else
    ShowMessage('You do not have permissions to create Project Locates');

  TicketID := DM.Ticket.FieldByName('ticket_id').AsInteger;
  LocateID := DM.LocateData.FieldByName('locate_id').AsInteger;
  LocatorID := DM.LocateData.FieldByName('assigned_to').AsInteger;
//  if DM.LocateData.FieldByName('status').AsString <> '-R' then
  If VarIsNull(LocateGridView.Controller.FocusedRecord.Values[ColLocLengthTotal.Index]) then
    CurrentMarkedAmt := 0
  else
    CurrentMarkedAmt := LocateGridView.Controller.FocusedRecord.Values[ColLocLengthTotal.Index];

  NewLocateData := ExecuteAddProjectLocateDialog(TicketID, LocateID, PermissionsMgrID, CurrentMarkedAmt);
  if NewLocateData.Active then begin
    fProjParents.Add(NewLocateData.ParentClient);
    fProjLocatorAssignments.Add(NewLocateData.AssignedToID, NewLocateData.LocateID);

    //Add a note
    DM.SaveBaseNoteToCache(ReplaceControlChars(NewLocateData.ShortNote), qmftTicket, TicketID, DM.TicketNotesData, TktSubTypeInt, TktSubTypeStr);
  end;
end;

procedure TTicketDetailForm.btnPelicanClick(Sender: TObject);
var    //qm-651 sr
  PelicanURL:string;
  Rev:string;

begin
  inherited;
  if uppercase(DM.Ticket.FieldByName('ticket_format').AsString) ='NCA1'  then
    PelicanURL:=DM.GetConfigurationDataValue('OneCallMapURL_NCA1','',True)
	else
  if uppercase(DM.Ticket.FieldByName('ticket_format').AsString) ='NVA1'  then
  PelicanURL:=DM.GetConfigurationDataValue('OneCallMapURL_NVA1','',True)
  else
  if uppercase(DM.Ticket.FieldByName('ticket_format').AsString) ='LCA1'  then
  PelicanURL:=DM.GetConfigurationDataValue('OneCallMapURL_LCA1','',True);

  PelicanURL := PelicanURL+TicketDS.DataSet.FieldByName('ticket_number').AsString;

  Rev:= TicketDS.DataSet.FieldByName('Revision').AsString;
  if Rev='' then Rev:='000';

  PelicanURL := PelicanURL+'-'+Rev;

  try
    ShellExecute(0,'open',PChar(PelicanURL),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+PelicanURL +'  '+e.message);
  end;
end;

procedure TTicketDetailForm.btnProjectLocateClick(Sender: TObject);  //QMANTWO-616 EB Designing of Multi-locator mode
begin
  OpenProjectMultiLocatorList;
  btnProjectLocate.Visible := IsLocateProjectable;
  LocateGrid.Refresh;
end;

procedure TTicketDetailForm.ButtonAddNewArrivalClick(Sender: TObject);
var
  Limitation: string;
begin
  LocatorBox.EmployeeCombo.Clear;
  Limitation := PermissionsDM.GetLimitation(RightTicketsEditArrivals);
  if IsEmpty(Limitation) then
    Limitation := IntToStr(DM.EmpID);

  LocatorBox.Initialize(esAllEmployeesUnderManager, DM.EmpID, StrToIntDef(Limitation, DM.EmpID));
  ArrivalDate.Date := Date;
  ArrivalTime.Time := Time;
  PanelArrivalButtons.Visible := False;
  PanelArrivalModify.Visible := True;
end;

procedure TTicketDetailForm.ButtonNewArrivalOKClick(Sender: TObject);
var
  Resp: Integer;
  EmpID: Integer;
  ArrivalDateTime: TDatetime;
begin
  EmpID := GetComboObjectInteger(LocatorBox.EmployeeCombo);
  if not DM.IsTicketAssignedToLocator(FTicketID, EmpID) then begin
    Resp := MessageDlgFmt('The selected employee is not assigned to this ticket. Are you sure you want to add the arrival?', [],
      mtConfirmation, [mbYes, mbNo], mrNo);
    if Resp <> mrYes then
      Abort;
  end;
  PanelArrivalButtons.Visible := True;
  PanelArrivalModify.Visible := False;
  ArrivalDateTime := ArrivalDate.Date + ArrivalTime.Time;
  DM.CreateArrival(FTicketID, 'ENTRY', EmpID, ArrivalDateTime, atTicket);
  EnableDisableControls;
end;

procedure TTicketDetailForm.PrepareTicketAddins;
begin
  if not Assigned(FAttachmentAddin) then
    FAttachmentAddin := TAttachmentsAddinManager.Create(DM, AttachmentsFrame.AttachmentAddinButton,
      ManageAddinButtonClick);

  Assert(Assigned(FAttachmentAddin), 'Attachment addin manager is not assigned');
  FAttachmentAddin.PrepareTicketAttachmentsAddin(HaveTicket);

  if not Assigned(FPlatAddin) then
    FPlatAddin := TPlatAddinManager.Create(DM, PlatAddinButton, ManageAddinButtonClick);
  FPlatAddin.PrepareForTicket(HaveTicket);
end;

procedure TTicketDetailForm.ManageAddinButtonClick(Sender: TObject);
var
  Cursor: IInterface;
  Ticket: TDataSet;
begin
  if not HaveTicket then
    raise EOdDataEntryError.Create('Please select a ticket.');

  Cursor := ShowHourGlass;
  Ticket := TicketDS.DataSet;
  Assert(Assigned(Ticket));

  if Sender = AttachmentsFrame.AttachmentAddinButton then begin
    if FAttachmentAddin.ExecuteForTicket(Ticket, LocateDS.DataSet, AttachmentsFrame.SelectedAttachmentID, qmftTicket, AttachmentsFrame.SelectedAttachmentFilename) then
      AttachmentsFrame.RefreshNow;
  end else if Sender = PlatAddinButton then begin
    FPlatAddin.ExecuteForTicket(Ticket, LocateDS.DataSet);
  end;
end;

procedure TTicketDetailForm.NoteSourceChange(Sender: TObject);
begin
  inherited;
  if FormStatus.NoteSourceLoaded then begin   //QM-577 EB
    if NoteSource.ItemIndex = 0 then
      PopulateNoteSubType(qmftTicket)
    else if NoteSource.ItemIndex > 0 then
      PopulateNoteSubType(qmftLocate);  //QM-576  sr  step 1
  end;
end;

procedure TTicketDetailForm.NoteSubTypeComboChange(Sender: TObject);
begin
  inherited;
    if NoteSubTypeCombo.Text <> '' then
    TicketNotesFrame.MakeVisible(True)
  else
    TicketNotesFrame.MakeVisible(False);

//  TicketNotesFrame.NoteSubType := FNoteSubTypeList.GetCode(NoteSubTypeCombo.Text);
//  TicketNotesFrame.SubTypeText := NoteSubTypeCombo.Text;
end;

function TTicketDetailForm.HaveTicket: Boolean;
begin
  Result := Assigned(DM) and Assigned(DM.Ticket) and DM.Ticket.Active and (not DM.Ticket.IsEmpty);
end;

procedure TTicketDetailForm.ColArrActiveGetDataText(Sender: TcxCustomGridTableItem;
  ARecordIndex: Integer; var AText: String);
begin
  inherited;
  if SameText(AText, 'True') then
    AText := 'Yes'
  else if SameText(AText, 'False') then
    AText := 'No';
end;

procedure TTicketDetailForm.ActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  DeleteArrivalAction.Enabled := DM.JobsiteArrivalData.Active and
    (not DM.JobsiteArrivalData.IsEmpty) and (DM.JobsiteArrivalData.FieldByName('active').AsBoolean);
end;

procedure TTicketDetailForm.ColLocLengthMarkedPropertiesInitPopup(Sender: TObject);
begin
  inherited;
  FLocateLengthEditor.SetupEditor(LocateDS.DataSet.FieldByName('locate_id').AsInteger,
    Date, LocateDS.DataSet.FieldByName('length_marked').AsInteger,
    LocateDS.DataSet.FieldByName('length_total').AsInteger,
    LocateDS.DataSet.FieldByName('qty_marked').AsInteger, LocateDS.DataSet.FieldByName('client_code').AsString);

    //Elana - Custom sizing
 // TcxPopupEdit(ColLocLengthMarked.Properties).PopupWindow.PopupHeight := 300;     Doesn't work
   TcxPopupEditProperties(ColLocLengthMarked.Properties).PopupHeight := fLocateLengthEditor.PopupHeight;
   TcxPopupEditProperties(ColLocLengthMarked.Properties).PopupWidth := fLocateLengthEditor.PopupWidth;
//****************************************************
end;

procedure TTicketDetailForm.ColLocLengthMarkedPropertiesChange(Sender: TObject);
begin
  inherited;
  TcxPopupEditProperties(ColLocLengthMarked.Properties).PopupHeight := fLocateLengthEditor.PopupHeight;
  TcxPopupEditProperties(ColLocLengthMarked.Properties).PopupWidth := fLocateLengthEditor.PopupWidth;
end;

procedure TTicketDetailForm.ColLocLengthMarkedPropertiesCloseUp(Sender: TObject);
begin
  inherited;
  FLocateLengthEditor.ValidateLocateLength;
end;

procedure TTicketDetailForm.RefreshMarkableUnitStatusCodes;
begin
  FMarkableUnitStatusCodes := DM.GetMarkableUnitStatusCodes;
end;

procedure TTicketDetailForm.SetupLengthColumn;
var
  Status: string;
begin
  if not Assigned(LocateGridView.Controller.FocusedRecord) then
    Exit;

  if VarIsNull(LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index]) then
    Status := ''
  else
    Status := LocateGridView.Controller.FocusedRecord.Values[ColLocStatus.Index];
  SetGridPopupColumnEnabled(ColLocLengthMarked, StatusAllowsUnits(Status));
end;

procedure TTicketDetailForm.ShowTicketSummary;
var
  i: Integer;
begin
  for i := 0 to TicketPages.PageCount-1 do begin
    TicketPages.Pages[i].TabVisible := (TicketPages.Pages[i] = TicketSummaryTab) or
      (TicketPages.Pages[i] = ImageTab) or (TicketPages.Pages[i] = NotesTab);
    EnableNotes(False);//Show notes tab but do not permit editing
  end;
  UpdateFollowupButton;
end;

procedure TTicketDetailForm.HideTicketSummary;
var
  i: Integer;
begin
  for i := 0 to TicketPages.PageCount-1 do begin
    if (TicketPages.Pages[i] = TicketSummaryTab) or
      (TicketPages.Pages[i] = PhotoAttachmentsTab) then
      TicketPages.Pages[i].TabVisible := False
    else if TicketPages.Pages[i] = HistoryTab then
      HistoryTab.TabVisible := PermissionsDM.CanI(RightViewTicketHistory) or PermissionsDM.IsTicketManager
    else
      TicketPages.Pages[i].TabVisible := True;
  end;
  TicketPages.ActivePage := ImageTab;
  EnableNotes(True);
  UpdateFollowupButton;
end;

procedure TTicketDetailForm.HistoryPagesResize(Sender: TObject);
begin
  inherited;
  ForceControlsGridsToResize(HistoryPages);
end;

procedure TTicketDetailForm.EnableNotes(const Enabled: Boolean);
begin
  SetEnabledOnControlAndChildren(NotesPanel, Enabled);
end;

procedure TTicketDetailForm.eprRespViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  ShellExecute(0,'open',PChar(qryEprHistory.FieldByName('link').AsString),nil,nil, SW_SHOW);  //QMANTWO-338
end;

function TTicketDetailForm.ShowingTicketSummary: Boolean;
begin
  Result := (TicketSummaryTab.TabVisible);
end;

procedure TTicketDetailForm.TicketVersionsCalcFields(DataSet: TDataSet);
var
  TicketIsXml: Boolean;
begin
  DataSet.FieldByName('image_display').Value := DM.GetTicketImage(DataSet, TicketIsXml);
  SetupImageMemoDisplay(TicketVersionImageMemo, TicketIsXml);
end;

procedure TTicketDetailForm.ResponsesTabResize(Sender: TObject);
begin
  EmailPanel.Height := Trunc((ResponsesTab.ClientHeight - ResponsesSplitter.Height) / 2);
end;

//procedure TTicketDetailForm.RunCEN;    //QM-700
//var
//  HashString : string;
//  TicketIDStr, LocateIDStr, EmpIDStr : string;
//  Pepper, LPepper, RPepper, BaseURL, Link: string;
//const
//  TID  = 'tid=';
//  LID  = 'Lid=';
//  EID  = 'Eid=';
//  FM   = 'FM=';
//  CODE = 'code=';
//  AMP  = '&';
//  FORMNAME = 'xcelcen';
//begin
//  inherited;
//  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
//  LPepper := Copy(Pepper, 1, 16);
//  RPepper := Copy(Pepper,17, 32);
//  BaseURL := DM.GetConfigurationDataValue('CenBaseUrl');
//  TicketIDStr := IntToStr(FTicketID);
//
//  LocateIDStr := IntToStr(LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index]);
//  EmpIDStr := IntToStr(DM.EmpID);
//  HashString := LPepper +
//                TicketIDStr +
//                LocateIDStr +
//                EmpIDStr +
//                FORMNAME +
//                RPepper;
//  HashString := CalcHash2(HashString, haSHA1);
//  Link :=  BaseURL +
//                    TID + TicketIDStr + AMP +
//                    LID + LocateIDStr + AMP +
//                    EID + EmpIDStr + AMP +
//                    FM + FORMNAME + AMP +
//                    CODE + HashString;
//  try
//    ShellExecute(0,'open',PChar(Link),nil,nil, SW_SHOW);
//  except
//  on e:exception do
//    ShowMessage('Critical error at '+Link +'  '+e.message);
//  end;
//end;

procedure TTicketDetailForm.RunTEMA;   //QM-53  EB
var
  aURL, BaseURL, FormName : string;
  TicketIDStr, LocateIDStr, EmpIDStr,
  HashStr, Pepper, LPepper, RPepper : string;
begin
  FormName := 'tema';
  TicketIDStr := IntToStr(FTicketID);
  LocateIDStr := IntToStr(LocateGridView.Controller.FocusedRecord.Values[ColLocLocateID.Index]);
  EmpIDStr := IntToStr(DM.EmpID);
  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  BaseURL := DM.GetConfigurationDataValue('TEMABaseUrl', '', True);
  HashStr :=    LPepper +
                TicketIDStr +
                LocateIDStr +
                EmpIDStr +
                FormName +
                RPepper;
  HashStr := UpperCase(CalcHash2(HashStr, haSHA1));
  aURL := BaseURL +
          'tid=' + TicketIDStr + '&' +
          'Lid=' + LocateIDStr + '&' +
          'Eid=' + EmpIDStr + '&' +
          'FM=tema&code=' + HashStr;

  try
    ShellExecute(0,'open',PChar(aURL),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+aURL +'  '+e.message);
  end;
end;

procedure TTicketDetailForm.ShowTicketWorkDescription;
begin
  if Assigned(FOnShowTicketWork) and (PermissionsDM.CanI(RightTicketsShowFullWorkScreen)) then begin
    FOnShowTicketWork(Self); //Expand full screen
    TicketPages.ActivePageIndex := FSavedTicketTabIndex;
  end;
end;

procedure TTicketDetailForm.TicketPagesChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  FSavedTicketTabIndex := TicketPages.TabIndex;
end;

procedure TTicketDetailForm.FollowupButtonClick(Sender: TObject);
begin  //QM-287 Default follow up ticket Due date to be one week after Transmit date  sr
  inherited;

  case FFollowupTicketState of
    fsCanCreate: begin
        if FollowupTicketType.Items.Count > 0 then
          FollowupTicketType.ItemIndex := 0;
        FFollowupTransmitDate := CurrentYDMHMS;
        FollowupTransmitDate.Caption := DateTimeToStr(FFollowupTransmitDate);
        FollowupDueDate.Date := DateOf(FFollowupTransmitDate) + 7; //QM-287 sr
        FollowupDueTime.Time := TimeOf(FFollowupTransmitDate);
      end;
    fsReadyToCreate: FFollowupTicketState := fsNeedToVerify;
  end;

  FollowupButton.Enabled := False;
  FollowupPopupEdit.DroppedDown := True;
end;

procedure TTicketDetailForm.FollowupPopupEditPropertiesCloseUp(Sender: TObject);
begin
  inherited;
  UpdateDetailFollowupTicketType;
  UpdateFollowupButton;
end;

procedure TTicketDetailForm.CreateFollowupButtonClick(Sender: TObject);
begin
  inherited;
  FFollowupTicketState := fsNeedToVerify;
  VerifyFollowupTicketCreated;
  SaveFollowupTicketState;
end;

procedure TTicketDetailForm.CancelFollowupButtonClick(Sender: TObject);
begin
  inherited;
  ResetFollowupPopupControls;
  FFollowupTicketState := fsCanCreate;
  DM.SetTicketFollowupType(0, 0, 0);
  FollowupPopupEdit.DroppedDown := False;
end;

//EB: Probably needs to be refactored
procedure TTicketDetailForm.CENButtonClick(Sender: TObject);
begin
  case fCENButtonMode of
//    cbCEN: RunCEN;   //qm-700 sr
    cbTEMA: RunTEMA;
  end;
end;



procedure TTicketDetailForm.CheckDueDate;
var
  NewValue: string;
  OldValue: string;
begin
  inherited;

    OldValue := DateTimeToStr(fOldDueDateValue);
    NewValue := EdtDueDate.Text;
    if (OldValue <> NewValue) then
      DueDateChanged := True;
end;



function TTicketDetailForm.HasTicketDueDateOverride(var pCanEdit: boolean; var pRemainingAttempts: integer): boolean;  //QM-216 EB Ticket Due Date
{Returns TRUE if overrides exist, returns RemainingAttempts as out variable}
var
  TktType: string;
  HasPermission: boolean;
  ChangeLimit: integer;         {Number of times  permitted}
  NumOfUpdatesMade: integer;    {How many updates already made}
begin
  Result := False;
  NumOfUpdatesMade := 0;
  ChangeLimit := 0;
  try
    If (PermissionsDM.CanI(RightEditTicketDueDate)) then begin      //QM-641 EB
      HasPermission := True;
      ChangeLimit :=1;
      TryStrToInt(PermissionsDM.GetLimitation(RightEditTicketDueDate), ChangeLimit); //QM-641 EB
    end
    else HasPermission := False;  //Maint 1/2024 EB

    TktType := TicketDS.DataSet.FieldByName('ticket_type').AsString;

    {If already an -RS ticket, then highlightand return TRUE}
    if ansipos(TicketTypeResched, TktType) > 0 then begin     {-RS}
      DueDate.Transparent := False;
      DueDate.Color :=  clWebLemonChiffon;
      Result := True;
      lblRS.Visible := True;
      {Check how many times it has updated}  //QM-641 EB
      DM.MultiDueDateQuery.ParamByName('ticket_id').AsInteger := TicketDS.DataSet.FieldByName('ticket_id').AsInteger;
      DM.MultiDueDateQuery.Open;
      NumOfUpdatesMade := DM.MultiDueDateQuery.FieldByName('SumCount').AsInteger;
      pRemainingAttempts := ChangeLimit - NumOfUpdatesMade;
      if pRemainingAttempts > 0 then
        pCanEdit := True;
    end
    else begin
      {Not editable- yet}
      DueDate.Color := clBtnFace;
      DueDate.Transparent := True;
      Result := False;   {No previous edits}
      lblRs.Visible := False;
      if HasPermission then begin
        pCanEdit := True;
        pRemainingAttempts := ChangeLimit;
      end;
    end;
  finally
    DM.MultiDueDateQuery.Close;
  end;
end;



function TTicketDetailForm.CheckNotesStatus: boolean; //QM-191
const
  UnsavedMsg = 'You have an unsaved note';
  SaveQuestion = 'Do you want to save it';
  AsMsg = ' as ';
  P = '. ';
  QM = '? ';
  UnspecSubType = ' with an unspecified Restriction/SubType';
var
  SaveIt: Boolean;
  Answer: integer; {mrYes, mrNo, mrCancel...etc}
  PrevSubType: string;
begin
  SaveIt := False;
  Answer := mrNo;
  if TicketNotesFrame.NoteText.GetTextLen() > 0 then begin
    {Switch tabs so user can see}
    TicketPages.ActivePage := NotesTab;

    GetNoteSource;
    PrevSubType := NoteSubTypeCombo.Text;

    {Comboboxes contain values}
    if (NoteSource.Text <> '') and (PrevSubType <> '') then
      Answer := (MessageDlg(UnsavedMsg + P + SaveQuestion + QM,
                    mtConfirmation, [mbYes, mbNo, mbCancel], 0))

    {NoteSubType is missing}
    else if (NoteSource.Text <> '') and (NoteSubTypeCombo.Text = '') then begin
      try
        NoteSubTypeCombo.ItemIndex := 0;
      except
        //do nothing, lease as is...just don't error
      end;
      if NoteSubTypeCombo.Text <> '' then begin
        Answer := MessageDlg(UnsavedMsg + UnSpecSubType + P + SaveQuestion + AsMsg  + NoteSubTypeCombo.Text + QM,
                    mtConfirmation, [mbYes, mbNo, mbCancel], 0);
        GetNoteSource;  //QM-191 EB Will need to do this again with the default subtype.
  
      end;
    end;
    case Answer of
        mrYes: SaveIt := True;
        mrNo:  begin
            SaveIt := False;
            TicketNotesFrame.NoteText.Clear;
            Result := True; {we want it to exit out, just not save the note}
        end;
        mrCancel: Result := False  {alert that we are going to cancel the Done action}
       else begin
         SaveIt := False;
         ShowMessage('There was a problem with an unsaved Note. Please correct or clear and try again.');
         Result := False;
       end;
    end;

    If SaveIt then begin
      TicketNotesFrame.AddNote;
      Result := True;
    end;
  end
  else Result := True;
end;


function TTicketDetailForm.CheckRouteOrder: boolean;  //QM-775 RESURRECTION EB //qm-773  sr
var
  Cursor: IInterface;
  CurRouteOrder: double;
  EarlierRouteArray: TRouteArray;
  ReturnCount : integer;
  lTicketNum: string;
begin
  Result := FALSE;  {All Tickets (unless RO skipped)}
  Cursor := ShowHourGlass;
  If (fLocateChanged or DM.TicketChanged) and (PermissionsDM.CanI(RightTicketsUseRouteOrder)) then begin
    CurRouteOrder := DM.Ticket.FieldByName('route_order').AsFloat;
    EarlierRouteArray := DM.GetEarlierRouteOrderArray(FTicketID, CurRouteOrder, ReturnCount);  //QMANTWO-775 EB Route_Order

    if (ReturnCount > 0) then begin
      Result := True;
      try
        lTicketNum:= Dm.Ticket.FieldByName('ticket_number').AsString;
        RouteOrderOverrideFrm := TRouteOrderOverrideFrm.Create(self);
        RouteOrderOverrideFrm.LoadTicketInfo(fTicketID, lTicketNum,ReturnCount, EarlierRouteArray);
        if RouteOrderOVerrideFrm.ShowModal = mrOK then
          Result := False //We no longer need it   QM-773 EB
        else
          Result := True //still needed  QM-773 EB
 //       result :=RouteOrderOverrideFrm.ShowModal<>mrCancel; //qm-773  sr
      finally
        FreeAndNil(RouteOrderOverrideFrm);
      end;
    end;
  end;
end;

procedure TTicketDetailForm.ClearAssociatedWO;
begin
  fAssociatedWOID := 0;
  btnReturnToWO.Caption := 'Return to';
  btnReturnToWO.Visible := False;
end;


procedure TTicketDetailForm.FollowupPopupEditPropertiesCloseQuery(
  Sender: TObject; var CanClose: Boolean);
begin
  inherited;
  try
    CanClose := False;
    ValidateFollowupTicket;
    CanClose := True;
  except
    on E: EOdNonLoggedException do
      ErrorDialog(E.Message);
  end;
end;

procedure TTicketDetailForm.FormActivate(Sender: TObject);
begin
  inherited;
  LeavingTicket := False;  //QM-814 EB DevEx Grid IDE error
  TicketNotesFrame.MakeVisible(False);
  if FFollowupTicketState = fsNeedToVerify then begin
    try
      ValidateFollowupTicket;
      FFollowupTicketState := fsReadyToCreate;
    except
      on E: EOdNonLoggedException do begin
        ErrorDialog(E.Message);
        FollowupButton.Click;
      end;
    end;
    UpdateDetailFollowupTicketType;
  end;

//  if dm.Ticket.FieldByName('update_of').IsNull then   //qm-397  sr only show if update ticket
//  begin
//    UpdateRelatedTickets.Visible:=false;
//    UpdatedTicketsLabel.Visible:=false;
//  end
//  else
//  begin
//    UpdateRelatedTickets.Visible:=true;
//    UpdatedTicketsLabel.Visible:=true;
//  end;


end;

procedure TTicketDetailForm.ColLocAlert2CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Color := clRed;
  ACanvas.Font.Style := [fsBold];
end;

procedure TTicketDetailForm.TicketNotesFrameAddNoteButtonClick(Sender: TObject);
begin
  GetNoteSource;
  TicketNotesFrame.AddNoteButtonClick(Sender);
end;

procedure TTicketDetailForm.TicketNotesFrameClick(Sender: TObject);
begin
  inherited;

end;

{ TLocatorAssignments }

function TLocatorAssignments.Add(LocatorID, LocateID: Integer): integer;
begin
  LocatorIDList.Add(LocatorID);
  LocateIDList.Add(LocateID);
end;

constructor TLocatorAssignments.Create;
begin
  LocatorIDList := TIntegerList.Create;
  LocateIDList := TIntegerList.Create;
end;

destructor TLocatorAssignments.Destroy;
begin
  FreeAndNil(LocatorIDList);
  FreeAndNil(LocateIDList);
end;



end.
