unit WorkManagementTickets;
                                                            
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableWorkMgtGridFrame, cxGraphics, cxControls, cxLookAndFeels,
  cxGridTableView, cxLookAndFeelPainters, cxStyles, cxClasses, cxGridLevel, cxGrid,
  StdCtrls, ExtCtrls, cxCustomData, 
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView, WorkManageUtils,
  cxGridDBTableView, cxGridCustomView, cxMaskEdit, dbisamtb, OdDbUtils,
  cxInplaceContainer, cxNavigator, cxCalendar, cxTextEdit, cxMemo, cxCheckBox,
  ComCtrls, DBCtrls, ActnList, Menus, OdContainer, dxGDIPlusClasses, cxLabel, 
  cxImage, Dmu, cxContainer, ImgList, cxHyperLinkEdit, cxFilter,
  cxData, dxSkinsCore, cxButtons;

type
 TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
 TcxPainterAccess = class(TcxGridTableDataCellPainter);


 {The Last NODE loaded in the grid}
  TLastLoaded = record
    SelectedLocatorID,
    ManagerID,
    NumTicketDays,
    MaxActivityDays: Integer;
    NodeName: string;             //QM-486 Virtual Bucket EB
    NodeIsUnderMe, ShowOpenWorkOnly: Boolean;
    GridLoaded: Boolean;
    NodeType: TNodeType;//QM-710 Only using this for AdjustTicketListColumns
  end;



  TWorkMgtTicketsFrame = class(TExpandableWorkMgtGridFrameBase)
    TicketGridViewRepository: TcxGridViewRepository;
    TicketView: TcxGridDBTableView;
    TicketListSource: TDataSource;
    ColActivityID: TcxGridDBColumn;
    ColActivity: TcxGridDBColumn;
    ColActivityDate: TcxGridDBColumn;
    ColActivityEmp: TcxGridDBColumn;
    ColTicketNumber: TcxGridDBColumn;
    ColAlert: TcxGridDBColumn;
    ColKind: TcxGridDBColumn;
    ColTicketType: TcxGridDBColumn;
    ColDueDate: TcxGridDBColumn;
    ColWorkAddressNumber: TcxGridDBColumn;
    ColWorkAddressStreet: TcxGridDBColumn;
    ColWorkCity: TcxGridDBColumn;
    ColWorkType: TcxGridDBColumn;
    ColMapPage: TcxGridDBColumn;
    ColWorkCounty: TcxGridDBColumn;
    ColWorkPriority: TcxGridDBColumn;
    ColEstStartTime: TcxGridDBColumn;
    ColTicketID: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColTicketStatus: TcxGridDBColumn;
    ColTicketIsVisible: TcxGridDBColumn;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ticket_number: TDBText;
    kind: TDBText;
    status: TDBText;
    work_address_number: TDBText;
    work_address_street: TDBText;
    Label11: TLabel;
    map_page: TDBText;
    Label3: TLabel;
    company: TDBText;
    Label9: TLabel;
    caller_contact: TDBText;
    Label12: TLabel;
    caller_altcontact: TDBText;
    caller_altphone: TDBText;
    caller_phone: TDBText;
    Label13: TLabel;
    do_not_mark_before: TDBText;
    Label1: TLabel;
    route_area_name: TDBText;
    EstStartLabel: TLabel;
    ContactPhoneLabel: TLabel;
    AtlPhoneLabel: TLabel;
    EstStartText: TStaticText;
    work_type: TDBMemo;
    RefreshTicketButton: TButton;
    DetailPageControl: TPageControl;
    LocateTab: TTabSheet;
    LocateGrid: TcxGrid;
    LocateGridDBTableView: TcxGridDBTableView;
    LocateGridclient_code: TcxGridDBColumn;
    LocateGridAlert: TcxGridDBColumn;
    LocateGridstatus: TcxGridDBColumn;
    LocateGridqty_marked: TcxGridDBColumn;
    LocateGridshort_name: TcxGridDBColumn;
    LocateGridclosed_date: TcxGridDBColumn;
    LocateGridLevel: TcxGridLevel;
    ResponseTab: TTabSheet;
    ResponsesGrid: TcxGrid;
    ResponsesGridDBTableView: TcxGridDBTableView;
    ResponsesGridclient_code: TcxGridDBColumn;
    ResponsesGridstatus: TcxGridDBColumn;
    ResponsesGridresponse_date: TcxGridDBColumn;
    ResponsesGridresponse_sent: TcxGridDBColumn;
    ResponsesGridreply: TcxGridDBColumn;
    ResponsesGridLevel: TcxGridLevel;
    NotesTab: TTabSheet;
    NotesGrid: TcxGrid;
    NotesGridDBTableView: TcxGridDBTableView;
    NoteDateColumn: TcxGridDBColumn;
    NoteColumn: TcxGridDBColumn;
    NotesGridLevel: TcxGridLevel;
    AttachmentsTab: TTabSheet;
    AttachmentsGrid: TcxGrid;
    AttachmentsGridTableView: TcxGridDBTableView;
    ColAttachmentID: TcxGridDBColumn;
    ColOrigFileName: TcxGridDBColumn;
    ColSize: TcxGridDBColumn;
    ColAttachDate: TcxGridDBColumn;
    ColUploadDate: TcxGridDBColumn;
    ColDocumentType: TcxGridDBColumn;
    ColComment: TcxGridDBColumn;
    ColFileName: TcxGridDBColumn;
    ColForeignType: TcxGridDBColumn;
    ColForeignID: TcxGridDBColumn;
    ColActive: TcxGridDBColumn;
    ColShortName: TcxGridDBColumn;
    ColAttachedBy: TcxGridDBColumn;
    ColSource: TcxGridDBColumn;
    ColUploadMachineName: TcxGridDBColumn;
    AttachmentsGridLevel: TcxGridLevel;
    AckButton: TButton;
    TicketPriorityCombo: TComboBox;
    TicketDS: TDataSource;
    LocateData: TDBISAMQuery;
    LocateDS: TDataSource;
    Responses: TDBISAMQuery;
    ResponseDS: TDataSource;
    Notes: TDBISAMQuery;
    NotesDS: TDataSource;
    Attachments: TDBISAMQuery;
    AttachmentsDS: TDataSource;
    cxStyleRepoTickets: TcxStyleRepository;
    BottomGridStyle: TcxStyle;
    ColArea: TcxGridDBColumn;
    PriorityLbl: TLabel;
    FocusedvsSelectedBtn: TButton;
    GridSelectionStyle: TcxStyle;
    UnfocusedGridSelStyle: TcxStyle;
    ColLat: TcxGridDBColumn;
    ColLng: TcxGridDBColumn;
    ColRouteOrder: TcxGridDBColumn;
    cxStyle1: TcxStyle;
    N1: TMenuItem;
    Label6: TLabel;
    lblROMessage: TLabel;
  	ColName: TcxGridDBColumn;
    ColUtilities: TcxGridDBColumn;
    UtilityDots: TImageList;
    ColCompany: TcxGridDBColumn;
    ColCon_Name: TcxGridDBColumn;
    ColRiskScore: TcxGridDBColumn;
    tabRisk: TTabSheet;
    RiskGrid: TcxGrid;
    RiskView: TcxGridDBTableView;
    ColRiskViewTicketVersionID: TcxGridDBColumn;
    ColRiskViewRevision: TcxGridDBColumn;
    RiskGridLevel: TcxGridLevel;
    RiskScoreHistory: TDBISAMQuery;
    RiskScoreHistoryDS: TDataSource;
    ColRiskViewRiskID: TcxGridDBColumn;
    ColRiskViewTicketID: TcxGridDBColumn;
    ColRiskViewLocateClient: TcxGridDBColumn;
    ColRiskViewSource: TcxGridDBColumn;
    ColRiskViewkScoreType: TcxGridDBColumn;
    ColRiskViewScore: TcxGridDBColumn;
    ColRiskViewTransmitDate: TcxGridDBColumn;
    ColTktCount: TcxGridDBColumn;
    ColLocCount: TcxGridDBColumn;
    WorkDescTab: TTabSheet;
    work_description: TDBMemo;
    cxHyperLinkEdit1: TcxHyperLinkEdit;
    ColTicketAgeHours: TcxGridDBColumn;
    DispatchingPriorityAction: TAction;
    Dispatching1: TMenuItem;
    OptimizeRouteFromTkt1: TMenuItem;
    Loads: TLabel;
    lblAllCounts: TLabel;
    btnOptimizeRO: TcxButton;
    ExportButton: TcxButton;
    btnBulkDueDate: TcxButton;
    ColAltDueDate: TcxGridDBColumn;
    ColExpirationDate: TcxGridDBColumn;
    procedure TicketViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure TicketViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure RescheduleDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure TicketViewStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure RefreshTicketButtonClick(Sender: TObject);
    procedure TicketPriorityComboChange(Sender: TObject);
    procedure LocateGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure BaseGridEnter(Sender: TObject);
    procedure PriorityPopupPopup(Sender: TObject);
    procedure HighPriorityActionExecute(Sender: TObject);
    procedure ReleasePriorityActionExecute(Sender: TObject);
    procedure NormalPriorityActionExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure AcknowledgeActionExecute(Sender: TObject);
    procedure ItemListTableAfterOpen(DataSet: TDataSet);
    procedure ItemDetailTableCalcFields(DataSet: TDataSet);
    procedure TicketListSourceDataChange(Sender: TObject; Field: TField);
    procedure TicketViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TicketViewSelectionChanged(Sender: TcxCustomGridTableView);
    procedure FocusedvsSelectedBtnClick(Sender: TObject);
    procedure BaseGridFocusedViewChanged(Sender: TcxCustomGrid;
      APrevFocusedView, AFocusedView: TcxCustomGridView);
    procedure TicketViewCustomDrawIndicatorCell(Sender: TcxGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxCustomGridIndicatorItemViewInfo;
      var ADone: Boolean);
    procedure TicketViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ExportButtonClick(Sender: TObject);
    procedure ColUtilitiesCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TicketViewFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure DispatchingPriorityActionExecute(Sender: TObject);

    {Route Order}
    procedure btnOptimizeROClick(Sender: TObject);
    procedure OptimizeRouteFromTkt1Click(Sender: TObject);
    procedure TicketPriorityComboKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton1Click(Sender: TObject);
    procedure btnBulkDueDateClick(Sender: TObject);  //QM-702 Route Optimization EB


  private
    LastLoaded: TLastLoaded; //QMANTWO-564 EB
    FOnRemoveTicketFromList: TNotifyEvent;
    procedure AcknowledgeCurrentTicket;
    procedure AcknowledgeTicketActivity;
    function IsDamageTicket: Boolean;
    procedure AcknowledgeDamageTicket;
    function CanWeAcknowledge: Boolean;
    function CanWeSetPriority: Boolean; //QMANTWO-564 (this is for single and multi-selected tickets}
    function GetTicketGridIniPath: string;
    procedure SearchForURL;  //QM-461  SR
  protected
    function ItemIDColumn: TcxGridColumn; override;   
    procedure SetGridAndViewEvents; override;
    procedure SetSelectedGridItemID(const GridItemID: Integer); override;//This method sets the ID and triggers an additional notification event used by MainFu.
    procedure SetTicketPriority;
  public
    fgridLoads: integer; //QM-695 EB
    ForceReload: boolean; //QM-695
    PrevSelList: TIntegerList;        //QMANTWO-690 EB    {Row Index}
    PrevSelTicketList: TIntegerList;  //QMANTWO-690 EB    {Ticket ID for re-selecting}
    PlaceHolderTicketID: Integer;     //QMANTWO-752 EB    {Keeps track of the next ticket after group selection - when tickets moved, it will return to focus this ticket}
    FocusedRecChange: boolean;

    property OnRemoveTicketFromList: TNotifyEvent read FOnRemoveTicketFromList write FOnRemoveTicketFromList;
    procedure RefreshCaptionCounts; override;
    procedure RefreshTicketCounts(TktCounts : TTicketCounts);
    procedure ReassignSelectedTickets(SelectedLocatorID, LocatorID: Integer);
    procedure CreateTicketListTable;
    procedure AdjustTicketListColumns(NodeType: TNodeType); //QM-133 EB Past Due bucket
    procedure SelectAndShowTicketAtRow(const RowIdx: Integer);
    function LoadTicketList(SelectedLocatorID, ManagerID,
      NumTicketDays, MaxActivityDays: Integer; NodeName: string; NodeIsUnderMe, ShowOpenWorkOnly: Boolean): Boolean;   //QMANTWO-690 EB Sel/Focus Tweak
    procedure UpdateFrameData(const SourceDataSet: TDataSet); override;
    procedure ShowSelectedItem; override;

    procedure ShowTicketSummary;   {Summary Panel Show}
    procedure SetPriorityComboBox(PriorityCombo: TComboBox; PriorityID: Variant);
    procedure MarkPriority(PriorityRefID: Integer);
    procedure MarkMultiPriorities(PriorityRefID: Integer); //QMANTWO-564 EB
    procedure UpdateItemInCache; override;
    function ChangeAllLocStatus(TicketID: Integer; NewStatus: String): Boolean;
    procedure SetGridCursors(NewCursor: TCursor);
    function IsMultiSelect: boolean;
    procedure ShowMultiSelDetail(IsOn: Boolean);    //QMANTWO-564 EB

    {Multi-Select QMANTWO-690 EB}
    procedure CreateStructures;    //QMANTWO-690 EB
    procedure DestroyStructures;   //QMANTWO-690 EB
    procedure LoadSelList;         //QMANTWO-690 EB
    procedure ReSelectList;        //QMANTWO-690 EB Sel/Focus Tweak
    procedure ClearSelectList;     //QMANTWO-690 EB Return to previous list tweak
    function HasPrevSelectedItems(CurEmpID: Integer): boolean; //QMANTWO-690 EB When we return to Work Management Tickets Panel

    procedure MoveToTicketID(GoToTicketID: Integer); //QMANTWO-690 EB
    procedure MoveFocusToTicketByRow(Row: integer); //QMANTWO-690 EB When returning to WM screen

    {Export}
    function ExportLocatorItems: string;
    function ExportEmergencyorActivityItems: string;

    function SelLocatorUsesRouteOrder: Boolean;

    {Saving Grid positions in INI: QM_TicketGridSettings.ini}
    procedure SaveTicketGridSettings;   //QM-338 EB new grid INI
    procedure RestoreTicketGridSettings; //QM-338 EB new grid INI

    procedure ChangeViewEmergencies(FilterOn: boolean);
    procedure ToggleOptimizeRouteOn(pOn: boolean);

    function HasSelectedTickets: boolean;
  end;

const
  QMTicketIni = 'QM_TicketGridSettings.ini';   //QM-338 Saving Grid Settings EB

  EM_GETSCROLLPOS = $04DD;  //QM-419 sr


var
  WorkMgtTicketsFrame: TWorkMgtTicketsFrame;
  ScrollPtr:Integer;  // QM-419  sr
  AfterMoveRec: Integer;   // QM-419 EB   Marks which ticket to view after move


implementation
uses QMServerLibrary_Intf,  QMConst, MSXML2_TLB, UQUtils, OdMiscUtils, SharedDevExStyles,
  ServiceLink, OdHourglass, OdVclUtils,  OdExceptions, AckDamage, OdDBISAMUtils, StrUtils,  //qm-461 sr
  LocateStatusQuestions, LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDMu,
  BulkDueDateDlg, DateUtils; //QM-959 EB Add Bulk Due Date Update

{$R *.dfm}

{ TWorkMgtTicketsFrame }

procedure TWorkMgtTicketsFrame.RefreshCaptionCounts;
begin
  ///Leaving in for now.  It is called, but for event order
end;

procedure TWorkMgtTicketsFrame.SearchForURL;  //QM-461  SR
var
  LineNo, CharNo: integer;
  FoundLine: String;

 Function GetWidthText(const Text:string; Font:TFont):integer;
 var
   LBmp: TBitmap;
 begin
   LBmp:= TBitmap.Create;
   try
     LBmp.Canvas.Font := Font;
     Result := LBmp.Canvas.TextWidth(Text);
   finally
     LBmp.free;
   end;
 end;


begin
  LineNo := 0;
  CharNo := 0;
  cxHyperLinkEdit1.Visible:=false;
  cxHyperLinkEdit1.Enabled:=false;
  while (PosEx('http', work_description.Lines[LineNo]) = 0)
          and (LineNo<work_description.Lines.Count) do
    inc(LineNo);

  CharNo := PosEx('http', work_description.Lines[LineNo]);

  if CharNo > 0 then
  begin
    FoundLine := copy(work_description.Lines[LineNo], CharNo,(Length(work_description.Lines[LineNo]) - CharNo+1));
    cxHyperLinkEdit1.Enabled:=true;
    cxHyperLinkEdit1.Visible:= true;
    cxHyperLinkEdit1.Text:= FoundLine;
  end;
end;



procedure TWorkMgtTicketsFrame.UpdateItemInCache;
begin
  if (SelectedItemID > 0) then   //QM-417
    DM.UpdateTicketInCache(SelectedItemID);  // Forces refresh of the ticket
end;



procedure TWorkMgtTicketsFrame.RescheduleDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;
  if Accept then
    Accept := TcxGridSite(TcxDragControlObject(Source).Control).GridView = TicketView;
end;



procedure TWorkMgtTicketsFrame.SetGridAndViewEvents;
begin
  inherited;
  TicketView.OnDblClick := ViewDblClick;
  TicketView.OnFocusedRecordChanged := ViewFocusedRecordChanged;
  BaseGrid.OnEnter := BaseGridEnter;

  TicketPriorityCombo.OnChange := TicketPriorityComboChange;
  ActionListFrame.OnUpdate := ActionListUpdate;
  AckButton.Action := AcknowledgeAction;
  AcknowledgeAction.OnExecute := AcknowledgeActionExecute;
  RefreshTicketButton.OnClick := RefreshTicketButtonClick;
end;

procedure TWorkMgtTicketsFrame.SetGridCursors(NewCursor: TCursor);
begin
  BaseGrid.Cursor := NewCursor;
  DetailPageControl.Cursor := NewCursor;
  TicketPriorityCombo.Cursor := NewCursor;
  Screen.Cursor := NewCursor;
end;

function TWorkMgtTicketsFrame.ItemIDColumn: TcxGridColumn;
begin
  Result := ColTicketID;
end;

procedure TWorkMgtTicketsFrame.AdjustTicketListColumns(NodeType: TNodeType);   //QM-133 EB Past Due

  procedure ShowSumFieldsOnly(CanShow: Boolean);
  begin
    {Sum Fields}
    ColName.Visible := CanShow;
    ColTktCount.Visible := CanShow;
    ColTktCount.VisibleForCustomization := CanShow;
    ColLocCount.Visible := CanShow;
    ColLocCount.VisibleForCustomization := CanShow;
    ColActivityID.Visible := CanShow;

    {All other fields}
    ColUtilities.Visible := not CanShow;
    ColUtilities.VisibleForCustomization := not CanShow;
    ColTicketNumber.Visible := not CanShow;
    ColTicketNumber.VisibleForCustomization := Not CanShow;
    ColTicketID.Visible := not CanShow;
    ColTicketID.VisibleForCustomization := not CanShow;
    ColAlert.Visible := not CanShow;
    ColAlert.VisibleForCustomization := not CanShow;
    ColKind.Visible := not CanShow;
    ColKind.VisibleForCustomization := not CanShow;
    ColCompany.Visible := not CanShow;
    ColCompany.VisibleForCustomization := not CanShow;
    ColCon_Name.Visible := not CanShow;
    ColCon_Name.VisibleForCustomization := not CanShow;
    ColTicketType.Visible := not CanShow;
    ColTicketType.VisibleForCustomization := not CanShow;
    ColDueDate.Visible := not CanShow;
    ColDueDate.VisibleForCustomization := not CanShow;
    ColWorkAddressNumber.Visible := not CanShow;
    ColWorkAddressNumber.VisibleForCustomization := not CanShow;
    ColWorkAddressStreet.Visible := not CanShow;
    ColWorkAddressStreet.VisibleForCustomization := not CanShow;
    ColWorkCity.Visible := not CanShow;
    ColWorkCity.VisibleForCustomization := not CanShow;
    ColWorkType.Visible := not CanShow;
    ColWorkType.VisibleForCustomization := not CanShow;
    ColArea.Visible := not CanShow;
    ColArea.VisibleForCustomization := not CanShow;
    ColMapPage.Visible := not CanShow;
    ColMapPage.VisibleForCustomization := not CanShow;
    ColWorkCOunty.Visible := not CanShow;
    ColWorkCOunty.VisibleForCustomization := not CanShow;
    ColWorkPriority.Visible := not CanShow;
    ColWorkPriority.VisibleForCustomization := not CanShow;
    ColEstStartTime.Visible := not CanShow;
    ColEstStartTime.VisibleForCustomization := not CanShow;
    ColRouteOrder.Visible := not CanShow;
    ColRouteOrder.VisibleForCustomization := not CanShow;
    ColLat.Visible := not CanShow;
    ColLat.VisibleForCustomization := not CanShow;
    ColLng.Visible := not CanShow;
    ColLng.VisibleForCustomization := not CanShow;
    ColTicketStatus.Visible := not CanShow;
    ColTicketStatus.VisibleForCustomization := not CanShow;
    ColModifiedDate.Visible := not CanShow;
    ColModifiedDate.VisibleForCustomization := not CanShow;
    ColTicketIsVisible.Visible := not CanShow;
    ColTicketIsVisible.VisibleForCustomization := not CanShow;
    ColRiskScore.Visible := not CanShow;
    ColRiskScore.VisibleForCustomization := not CanShow;
    ColActivity.Visible := not CanShow;
    ColActivity.VisibleForCustomization := not CanShow;
    ColActivityDate.Visible := not CanShow;
    ColActivityDate.VisibleForCustomization := not CanShow;
    ColActivityEmp.Visible := not CanShow;
    ColActivityEmp.VisibleForCustomization := not CanShow;
    ColUtilities.Visible := not CanShow;
    ColUtilities.VisibleForCustomization := not CanShow;
    ColTicketAgeHours.Visible := not CanShow;
    ColTicketAgeHours.VisibleForCustomization := not CanShow;
  end;


begin
  {If it is the same type of node as the last time, don't adjust}
  if NodeType = LastLoaded.NodeType then   //QM-710 EB
    exit;
  
  BaseGrid.BeginUpdate;
  LastLoaded.NodeType := NodeType;     //QM-710 EB
  try
    {reset}
    ShowSumFieldsOnly(False);

    ColActivity.Visible := False;
    ColActivityDate.Visible := False;
    ColActivityEmp.Visible := False;
    ColName.Visible := False;
    ColName.VisibleForCustomization := False;
    ColTicketStatus.Visible := False;

//    {Utility Icons}
//    if PermissionsDM.CanI(RightTicketsViewUtilIcons) then begin
//      ColUtilities.Visible := True;
//      ColUtilities.VisibleForCustomization := True;
//    end
//    else begin
//      ColUtilities.Visible := False;
//      ColUtilities.VisibleForCustomization := False;
//    end;

    {Activity}
    if NodeType = ntTicketActivities then begin
      ColActivityID.Visible := False;
      ColActivity.Visible := True;
      ColActivityDate.Visible := True;
      ColActivityEmp.Visible := True;
    end

    {Column Name: Past Due, Today, Virtual, Emergency}    //QM-551 EB Show Name on Emergency as well
    else if (NodeType = ntPastDue) or
            (NodeType = ntToday) or
            (NodeType = ntVirtual) or
            (NodeType = ntEmergency) then begin   //QM-133, QM-486
      ColName.Visible := True;
      ColName.VisibleForCustomization := True;
    end

    {Open}
    else if NodeType = ntOpen then begin   //QM-695 EB Open Ticket Expanded
      If (PermissionsDM.CanI(RightTicketsOpenExpTree)=FALSE) then
        ShowSumFieldsOnly(True)
      else begin
        ColName.Visible := True;
        ColName.VisibleForCustomization := True;
      end;
    end;

    {Emergency - Age Hours}
    if (NodeType = ntEmergency) then begin  //QM-551 EB
      ColTicketAgeHours.Visible := True;
    end
    else begin
      ColTicketAgeHours.Visible := False;
      ColTicketAgeHours.VisibleForCustomization := False;
    end;

    {Today}
    if NodeType = ntToday then begin          //QM-551 EB
      ColTicketAgeHours.Visible := False;
      ColTicketAgeHours.VisibleForCustomization := False;
    end;

  finally
    BaseGrid.EndUpdate;
  end;
end;

procedure TWorkMgtTicketsFrame.BaseGridEnter(Sender: TObject);
begin
  inherited;
  //  ShowSelectedItem; EB QMANTWO-564 - Moved to prevent delay on grid scrolling
  try
    if (DM.UQState.SecondaryDebugOn) and (DM.UQState.DebugWorkMgmtDebug) then begin  //QM-805 EB  //QM-10 EB Moving button visibility to QManagerDebugConfig.ini
      FocusedvsSelectedBtn.Visible := True;
      Loads.Visible := True;
    end;
  except
    // EB - Purposefully, swallow as this is Debug
  end;
  GridState.InitialDisplay := False;  //EB 7/18
end;

procedure TWorkMgtTicketsFrame.BaseGridFocusedViewChanged(Sender: TcxCustomGrid;
  APrevFocusedView, AFocusedView: TcxCustomGridView);
begin
  inherited;
  {EB QMANTWO-564:  Tried using focus events to test whether all focused items were
      were also selected.  Code has been moved to WorkManagement.pas using
      ApplicationEvents1.OnIdle event.}
end;



procedure TWorkMgtTicketsFrame.btnBulkDueDateClick(Sender: TObject);
var
  i, lTicketID: integer;
  lTicketDisplayList, lTicketIDList: TStringlist;
  lTicketIDStr: String;
  lSelRecord: TcxCustomGridRecord;
  lTicketNum: string;
begin
    lTicketDisplayList := TStringList.Create; {For Displaying tickets}
    lTicketIDList:= TStringList.Create; {For Queries, Objects attached}
    lTicketDisplayList.Delimiter := ',';
    lTicketDisplayList.StrictDelimiter := True;
    lTicketIDList.Delimiter := ',';
    lTicketIDList.StrictDelimiter := True;
    lTicketDisplayList.Clear;
    lTicketIDList.Clear;
    try
      for i := 0 to TicketView.Controller.SelectedRecordCount - 1 do begin
        lSelRecord := TicketView.Controller.SelectedRecords[i];
        if lSelRecord.IsData then begin
          lTicketID := lSelRecord.Values[ColTicketID.Index];
          lTicketNum := lSelRecord.Values[ColTicketNumber.Index];
          if lTicketID > 0 then begin
            lTicketIDStr := intToStr(lTicketID);
            lTicketDisplayList.Add(lTicketNum + '(' + lTicketIDStr + ')');
            lTicketIDList.AddObject(lTicketIDStr, TObject(lTicketNum));
          end;
        end;
      end;
    TfrmBulkDueDateEdit.Execute(lTicketDisplayList,lTicketIDList);
  finally
   // for i := 0 to lTicketIDList.Count-1 do
   //  lTicketIDList.Objects[i].Free;

    FreeAndNil(lTicketIDList);
    FreeAndNil(lTicketDisplayList);
  end;
end;

procedure TWorkMgtTicketsFrame.btnOptimizeROClick(Sender: TObject);
var
  lSubmit: boolean;
begin
  inherited;
  lSubmit := EmployeeDM.AddROOptimizationRequest(LastLoaded.SelectedLocatorID, DM.EmpID, 0);
  if lSubmit then begin
    ShowMessage('An optimization has been requested for ' + EmployeeDM.GetEmployeeShortName(LastLoaded.SelectedLocatorID));
    ToggleOptimizeRouteOn(False);
  end;
end;




procedure TWorkMgtTicketsFrame.FocusedvsSelectedBtnClick(Sender: TObject);  //EB QMANTWO-564 Test Selected vs Focused
var
  MessageStr: string;
  i: integer;
  SelRecord: TcxCustomGridRecord;
begin
  {EB QMANTWO-564 Multi-Select
      This is a test button to compare selected records to focused records.
      * Focused - Highlighted rows and there is only one
      * Selected - multiple
      TEST BUTTON ONLY - Make button visible/enable when needed for testing.
      Leaving code in here for testing purposes}
  inherited;

  MessageStr := 'Selected Records: ' + #13#10;

    for i := 0 to TicketView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := TicketView.Controller.SelectedRecords[i];
      MessageStr := MessageStr + ' *' + String(SelRecord.Values[ColTicketNumber.Index]) + #13#10;
    end;
    MessageStr := MessageStr + #13#10 + 'Focused Rec Index: ' + IntToStr(TicketView.Controller.FocusedRecordIndex) + #13#10;
    MessageStr := MessageStr + ListView.Controller.FocusedRecord.Values[ColTicketNumber.Index];
  ShowMessage(MessageStr);

end;


function TWorkMgtTicketsFrame.GetTicketGridIniPath: string;  //QM-338
begin
  Result := ExtractFilePath(Application.ExeName)+ QMTicketIni;
end;


procedure TWorkMgtTicketsFrame.LoadSelList;
var
  SelRecord: TcxCustomGridRecord;
  i : integer;
  SelTicketID: integer;
  NextRecIndex: integer;
begin
  {EB QMANTWO-690 Multi-Select Tweak}
    PrevSelList.Clear;
    PrevSelTicketList.Clear;

    GridState.PrevFocusedID := -1;    //QM-710 EB

    for i := 0 to TicketView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := TicketView.Controller.SelectedRecords[i];
      SelTicketID := SelRecord.Values[ColTicketID.Index];
      NextRecIndex := SelRecord.Index + 1;
      PrevSelList.Add(SelRecord.Index);   {List of indexes}      //QM-56 EB - Removed conditional which skipped first row as prev selected
      PrevSelTicketList.Add(SelTicketID); {List of Ticket IDs}
    end;
    GridState.PrevFocusedID := ListView.Controller.FocusedRecord.Values[ColTicketID.Index];
    {Note the record after the record selections}

    if NextRecIndex < ListView.DataController.RecordCount then {QMANTWO-752}
      PlaceHolderTicketID := TicketView.DataController.Values[NextRecIndex, ColTicketID.Index]
    else
      PlaceHolderTicketID := -1;  //QM-710  EB
end;

procedure TWorkMgtTicketsFrame.ReSelectList;
var
  i: integer;
  CurTicketID: integer;
begin
  //QMANTWO-690 EB Sel/Focus Tweak
  TicketView.Controller.ClearSelection;

  for i := 0 to TicketView.DataController.RowCount - 1 do begin
    CurTicketID := TicketView.DataController.Values[I, ColTicketID.Index];

    if CurTicketID = GridState.PrevFocusedID then
      TicketView.ViewData.Rows[i].Focused := True
    else
      TicketView.ViewData.Rows[i].Focused := False;   //QM-710 EB Explicity set

    If PrevSelTicketList.Has(CurTicketID) then
      TicketView.ViewData.Rows[I].Selected := True
    else
      TicketView.ViewData.Rows[I].Selected := False;  //QMANTWO-752 In Win7, returning to screen nothing is selected. But for optimized code, make this explicit
  end;
end;





function TWorkMgtTicketsFrame.ChangeAllLocStatus(TicketID: Integer;
  NewStatus: String): Boolean;
var
  Changed: Boolean;
begin
  If TicketID <> LocateData.FieldByName('ticket_id').AsInteger then begin
    DM.GoToTicketAck(TicketID);
  end;
  //Need to allow editing
  if LocateData.Active then
    LocateData.Close;
  LocateData.ReadOnly := False;
  LocateData.Open;
  Changed := False;

  LocateData.First;
  LocateGrid.Cursor :=  crHourGlass;
  try
    while not LocateData.EOF do begin
      LocateData.Edit;
      LocateData.FieldByName('Status').AsString := NewStatus;
      LocateData.Post;
      Changed := True;
      LocateData.Next;
    end;
  finally
    if LocateData.Active then
      LocateData.Close;
    LocateData.ReadOnly := True;
    LocateData.Open;
    LocateGrid.Cursor := crDefault;
  end;

  if not Changed then
    ShowMessage('There were no locate status changes (Status = ' + NewStatus + ')');

  Result := Changed;
end;

procedure TWorkMgtTicketsFrame.ChangeViewEmergencies(FilterOn: boolean);
begin
  ListTable.Filter := 'kind like ''%EMERG%''';
  ListTable.Filtered := FilterOn;
end;





procedure TWorkMgtTicketsFrame.ClearSelectList;
begin
  PrevSelList.Clear;
end;

procedure TWorkMgtTicketsFrame.ColUtilitiesCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  {QM-306 EB}
const
  GAS = 0;
  ELEC = 1;
  WATER = 2;
  SEWER = 3;
  COMM = 4;
  PHONE = 5;
  PIPL = 6;
  FUEL = 7;
  SGNL = 8;
var
  APainter: TcxPainterAccess;
  UStr: string;
  NumOfImages: integer;
  IndentWidth: Integer;
  //R: TRect;
  RowStatus: TTicketStatus;
  BackgroundColor: TColor;
begin
  inherited;
  if (TicketView.DataController.RecordCount = 0) or (not (PermissionsDM.CanI(RightTicketsViewUtilIcons))) then
    exit;   //QM-338 EB

{Color Utility Dots}
  if (AViewInfo.Item.Index = ColUtilities.Index) and
     (not VarIsNull(AViewInfo.GridRecord.Values[ColUtilities.Index])) then begin

   APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
   with APainter do begin
     try
       NumOfImages := 0;
       BackgroundColor := clNavy;

       {Set Background to ticket status color}
       if not AViewInfo.Selected then begin
         RowStatus := TTicketStatus(word(AViewInfo.GridRecord.Values[ColTicketStatus.Index])); //QMANTWO-396
         BackgroundColor := GetTicketColor(RowStatus, (AViewInfo.GridRecord.Index mod 2) = 1);
       end
       else {unless, selected row}
         BackgroundColor := GridSelectionStyle.Color;

       ACanvas.FillRect(AViewInfo.ClientBounds, BackgroundColor);  {same background as the rest of the row}
       ACanvas.Canvas.Brush.Color := clWhite;  {Puts the white behind the icon - makes it brighter}
       IndentWidth := UtilityDots.Width;

        with AViewInfo.ClientBounds do begin
          UStr := AViewInfo.GridRecord.Values[ColUtilities.Index];
          if ansipos('catv', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, COMM);
            Inc(NumOfImages);
          end;
          if ansipos('elec', UStr) > 0 then begin
           IndentWidth := UtilityDots.Width * NumOfImages;
           UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, ELEC);
           Inc(NumOfImages);
          end;
          if ansipos('fuel', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, FUEL);
            Inc(NumOfImages);
          end;
          if ansipos('ugas', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, GAS);
            Inc(NumOfImages);
          end;
          if ansipos('pipl', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, PIPL);
            Inc(NumOfImages);
          end;
          if ansipos('phon', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, PHONE);
            Inc(NumOfImages);
          end;
          if ansipos('sewr', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, SEWER);
            Inc(NumOfImages);
          end;
          if ansipos('sgnl', UStr) > 0 then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDots.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, SGNL, True);
            Inc(NumOfImages);
          end;
          if (ansipos('water', UStr) > 0) or (ansipos('watr', UStr) > 0) then begin
            IndentWidth := UtilityDots.Width * NumOfImages;
            UtilityDOts.Draw(ACanvas.Canvas, Left + IndentWidth, Top + 1, WATER);
            Inc(NumOfImages);
          end;
        end;
      finally
        APainter.Free;
      end;
    end;  {with..}
  end;  {if col...}
  ADone := True;
end;

procedure TWorkMgtTicketsFrame.CreateStructures;   //QMANTWO-690 EB
begin
  PrevSelList := TIntegerList.Create;
  PrevSelTicketList := TIntegerList.Create;
  GridState.LastOpenedID := 0;
end;

procedure TWorkMgtTicketsFrame.CreateTicketListTable;
var
  i: Integer;
begin
  // Copy over all current ticket fields, and add special fields for the grid
  DetailTable.FieldDefs.Update;
  ListTable.FieldDefs.Assign(DetailTable.FieldDefs);
  ListTable.FieldDefs.Add('activity_id', ftInteger);
  ListTable.FieldDefs.Add('activity', ftString, 100);
  ListTable.FieldDefs.Add('activity_date', ftDateTime);
  ListTable.FieldDefs.Add('short_name', ftString, 100);
  ListTable.FieldDefs.Add('ticket_is_visible', ftstring, 20);
  ListTable.FieldDefs.Add('ticket_status', ftInteger);
  ListTable.FieldDefs.Add('work_priority', ftString, 100);
  ListTable.FieldDefs.Add('est_start_time', ftDateTime);
  ListTable.FieldDefs.Add('area_name', ftString, 30);  //QM-381  SR 
  ListTable.FieldDefs.Add('lat', ftFloat, 0, false);  //QM-10 EB For Export
  ListTable.FieldDefs.Add('lon', ftFloat, 0, false); //QM-10 EB For Export
  ListTable.FieldDefs.Add('assigned_to', ftString, 30);  //QM-133 EB Past Due
  ListTable.FieldDefs.Add('util', ftString, 150); //QM-306
  ListTable.FieldDefs.Add('risk', ftString, 100); //QM-387
  ListTable.FieldDefs.Add('tkt_count', ftInteger); //QM-428 EB
  ListTable.FieldDefs.Add('loc_count', ftInteger); //QM-428 EB
  ListTable.FieldDefs.Add('Age_hours', ftFloat, 0, false); //QM-551 EB Ticket Age in Hours   QM-796 Type mismatch corrected
 // ListTable.FieldDefs.Add('alt_due_date', ftDatetime); //QM-973 EB
 // ListTable.FieldDefs.Add('expiration_date', ftDatetime); //QM-973 EB
  for i := 0 to ListTable.FieldDefs.Count - 1 do
    ListTable.FieldDefs[i].Required := False;

  ListTable.CreateTable;

  if (DM.UQState.SecondaryDebugOn) and (DM.UQState.DebugWorkMgmtDebug) then               //QM-131 EB - Fields Available to view in Debug Mode
    ColTicketID.VisibleForCustomization := True;
end;

procedure TWorkMgtTicketsFrame.cxButton1Click(Sender: TObject);
var
  lSubmit: boolean;
begin
  inherited;
  lSubmit := EmployeeDM.AddROOptimizationRequest(LastLoaded.SelectedLocatorID, DM.EmpID, 0);
  if lSubmit then begin
    ShowMessage('An optimization has been requested for ' + EmployeeDM.GetEmployeeShortName(LastLoaded.SelectedLocatorID));
    ToggleOptimizeRouteOn(False);
  end;
end;

procedure TWorkMgtTicketsFrame.DestroyStructures; //QMANTWO-690 EB
begin
  FreeAndNil(PrevSelList);
  FreeAndNil(PrevSelTicketList);
end;


procedure TWorkMgtTicketsFrame.DispatchingPriorityActionExecute(Sender: TObject);
begin
  inherited;
    inherited;
  TicketPriorityCombo.ItemIndex := TicketPriorityCombo.Items.IndexOf('Dispatching');
  MarkPriority(GetComboObjectInteger(TicketPriorityCombo, True));
end;

//QM-133 EB Need
procedure TWorkMgtTicketsFrame.ExportButtonClick(Sender: TObject);
var
  ExportedFileName: string;
begin
  inherited;
  if (TicketView.ViewData.RecordCount = 0) then begin
    MessageDlg('There are no tickets to export.', mtInformation, [mbOK], 0);
    exit;
  end;
  if (SelectedTreeNodeType = ntEmergency) or
     (SelectedTreeNodeType = ntTicketActivities) or
     (SelectedTreeNodeType = ntPastDue) or  //QM-133 EB Past Due
     (SelectedTreeNodeType = ntToday) or  //QM-318 Today EB
     (SelectedTreeNodeType = ntVirtual) then //QM-486 Virtual EB
    ExportedFileName := ExportEmergencyorActivityItems     //QM-10 EB Emergency bucket export

  else
    {QM-10 EB Export what is marked}
    ExportedFileName := ExportLocatorItems;
end;

function TWorkMgtTicketsFrame.ExportEmergencyorActivityItems: string;  //QM-10 EB
var
  TicketsToExport: TDataSet;
  TicketExportCount: integer;
  ExportFileName: string;
  GPXOptions: TGPXOptions;
begin
  Result := ParentNodeReferenceName;
  {Note: For Emergencies, we can cut to the chase}
  TicketsToExport := ListTable;
  GPXOptions.IncludeEmpName := True;
  try
    TicketExportCount := TicketsToExport.RecordCount;
    if SelectedTreeNodeType = ntEmergency then
      ExportFileName := DM.CreateExportFileName('-' + 'Emergency Ticket List', '.gpx', ParentNodeReferenceName)
    else if SelectedTreeNodeType = ntPastDue then     //QM-133 EB Past Due
      ExportFileName := DM.CreateExportFileName('-' + 'Past Due List', '.gpx', ParentNodeReferenceName)     
     else if SelectedTreeNodeType = ntToday then       //QM-318 EB Today
      ExportFileName := DM.CreateExportFileName('-' + 'Today List', '.gpx', ParentNodeReferenceName)
    else
      ExportFileName := DM.CreateExportFileName('-' + 'Ticket Activity List', '.gpx',  ParentNodeReferenceName);
    DM.SaveGpxToFile(TicketsToExport, ExportFileName, GPXOptions);
    Result := ExportFileName;
    MessageDlg('Export created this file: ' + #13#10 + Result, mtInformation, [mbOk], 0);
  except
   {Swallow exception at this point}
  end;
end;

function TWorkMgtTicketsFrame.SelLocatorUsesRouteOrder: Boolean;  //EB - Clean up
var
  EmpLimitation: string;
begin
  Result := GetEmpPermissionFromServer(LastLoaded.SelectedLocatorID, RightTicketsUseRouteOrder, EmpLimitation);
end;

function TWorkMgtTicketsFrame.ExportLocatorItems: string; //QM-10 Eb
var
  ExportFileName: string;
  TicketsToExport: TDataSet;
  TicketExportCount: integer;
  FN,LN, EmpName: string;
  GPXOptions: TGPXOptions;
begin
  EmployeeDM.GetEmployeeFullName(LastLoaded.SelectedLocatorID, FN, LN);
  EmpName:= FN + LN;
  EmpName := EmployeeDM.GetEmployeeShortName(LastLoaded.SelectedLocatorID);
  TicketsToExport := ListTable;
  GPXOptions.IncludeRouteOrder := SelLocatorUsesRouteOrder;
  GPXOptions.IncludeEmpName := False;
  try
    TicketExportCount := TicketsToExport.RecordCount;
    if TicketExportCount > 0 then begin
      ExportFileName := DM.CreateExportFileName('-' + ' Ticket List', '.gpx', EmpName);
      DM.SaveGpxToFile(TicketsToExport, ExportFileName, GPXOptions);
      Result := ExportFileName;
      MessageDlg('Export created this file: ' + #13#10 + Result, mtInformation, [mbOk], 0);
    end
  except
   {Swallow exception at this point}
  end;
end;



procedure TWorkMgtTicketsFrame.SelectAndShowTicketAtRow(const RowIdx: Integer);
var
  Idx: Integer;
begin
  if (TicketView.ViewData.RowCount > 0) and (RowIdx > -1) then begin
    if (TicketView.ViewData.RowCount <= RowIdx) then
      Idx := TicketView.ViewData.RowCount - 1
    else
      Idx := RowIdx;

    SetSelectedGridItemID(TicketView.ViewData.Rows[Idx].Values[ColTicketID.Index]);
  end else
    SelectedItemID := 0;

end;



procedure TWorkMgtTicketsFrame.TicketViewSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  inherited;
  if (not GridState.InitialDisplay) and (GridState.LastDisplayedID <> GetSelectedItemID) then    //QMANTWO-690
    ShowSelectedItem;
  {QMANTWO-564 EB - This will trigger a visual effect when multiple tickets are selected}
  if isMultiSelect then
    ShowMultiSelDetail(True)
  else
    ShowMultiSelDetail(False);

  lblROMessage.Caption := '';
  SearchForURL;  //qm-461 SR

  btnBulkDueDate.Enabled := (PermissionsDM.CanI(RightWorkManagementBulkDueDate)) and
                            (HasSelectedTickets);  //QM-959 EB Bulk Ticket Due Date Change

end;

procedure TWorkMgtTicketsFrame.TicketViewStartDrag(Sender: TObject;
  var DragObject: TDragObject);
//Figure out which items are being dragged.
var
  I: Integer;
  SelectedTicketID: string;
  SelectedItemIDList: TStringList;
  FirstTicket: Boolean;

begin
  inherited;
  ScrollPtr:=0;
  AfterMoveRec := 0;    // QM-419 EB
  FirstTicket := True;
  ScrollPtr:=TcxGridSite(Sender).VScrollBar.Position; //qm-419  sr
  GridState.AmDragging := True;
  RescheduledItemIDs := '';
  SelectedItemIDList := TStringList.Create;
  try
    with TcxGridDBTableView(TcxGridSite(Sender).GridView) do

      {Iterating through Selected Tickets}
      for I := 0 to Controller.SelectedRecordCount - 1 do begin
        SelectedTicketID := Controller.SelectedRecords[I].Values[ItemIDColumn.Index];

        if FirstTicket then begin  // QM-419 EB
          AfterMoveRec := Controller.SelectedRecords[i].Index - 1;
          if (AfterMoveRec < 0) then AfterMoveRec := 0;
          FirstTicket := False;
        end;

        if not IsEmpty(SelectedTicketID) then
          SelectedItemIDList.Add(SelectedTicketID);
      end;

    RescheduledItemIDs := SelectedItemIDList.CommaText;
  finally
    SelectedItemIDList.Free;
  end;
end;

procedure TWorkMgtTicketsFrame.ToggleOptimizeRouteOn(pOn: boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    ListTable.DisableControls;
    btnOptimizeRO.Enabled := pOn;
    OptimizeRouteFromTkt1.Enabled := pOn;
  finally
    ListTable.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TWorkMgtTicketsFrame.SetSelectedGridItemID(const GridItemID: Integer);
//This method sets the ID and triggers an additional notification event used by MainFu.
begin
  inherited;
  if (not FPopulatingWorkSummary) then
    if Assigned(FOnSelectItem) then
      FOnSelectItem(Self, GridItemID);
end;

procedure TWorkMgtTicketsFrame.SetTicketPriority;
begin
    if StrContainsText('Release', TicketPriorityCombo.Text) then begin
    ReleasePriorityAction.Execute;
    if not PermissionsDM.CanI(RightTicketsReleaseForWork) then
        SetPriorityComboBox(TicketPriorityCombo, DetailTable.FieldByName('work_priority_id').Value);
  end else
  //QMANTWO-564 EB - Different action for multi-select
  if  IsMultiSelect then begin
    MarkMultiPriorities(GetComboObjectInteger(TicketPriorityCombo, True));
  end
  else //EB - Single Select
    MarkPriority(GetComboObjectInteger(TicketPriorityCombo, True));
end;

procedure TWorkMgtTicketsFrame.ShowMultiSelDetail(IsOn: Boolean);
{QMANTWO-564 EB :  Disables/Hides detail information when Multi-Select}
var
  FontColor: Integer;
  i: integer;
begin
  if IsOn then
     FontColor := clGray
   else
     FontColor := clWindowText;
  
  for i := 0 to PanelDisplaySelectedItem.ControlCount - 1 do begin
    if PanelDisplaySelectedItem.Controls[i] is TLabel then begin
      TLabel(PanelDisplaySelectedItem.Controls[i]).Font.Color := FontColor;
      if (TLabel(PanelDisplaySelectedItem.Controls[i]).Name = 'lblROMessage') then
        TLabel(PanelDisplaySelectedItem.Controls[i]).Font.Color := clMaroon;
    end
    else if PanelDisplaySelectedItem.Controls[i] is TDBText then
      TDBText(PanelDisplaySelectedItem.Controls[i]).Font.Color := FontColor
    else if PanelDisplaySelectedItem.Controls[i] is TDBMemo then
      TDBMemo(PanelDisplaySelectedItem.Controls[i]).Font.Color := FontColor
    else if PanelDisplaySelectedItem.Controls[i] is TcxGrid then
      TcxGrid(PanelDisplaySelectedItem.Controls[i]).Font.Color := FontColor;
  end;


  
  {EB - Multi-select on and user is allowed to set priorities}
  if IsOn and (PermissionsDM.CanI(RightMultiSelPriorityChange)) then begin
    TicketPriorityCombo.Color := 	clCream;
    PriorityLbl.Font.Color := clMaroon;
    TicketPriorityCombo.Font.Color := clWindowText;
    BottomGridStyle.TextColor := clInactiveCaptionText;
    BottomGridStyle.Color := clBtnFace;
  end

  {EB - Multi-select on...but user does NOT have permissions}
  else if IsON and (not PermissionsDM.CanI(RightMultiSelPriorityChange)) then begin
    TicketPriorityCombo.Font.Color := clInactiveCaptionText;
    TicketPriorityCombo.Color := clBtnFace;
    TicketPriorityCombo.Enabled := False;
    RefreshTicketButton.SetFocus;
    PriorityLbl.Font.Color := clInactiveCaptionText;
    BottomGridStyle.TextColor := clInactiveCaptionText;
    BottomGridStyle.Color := clBtnFace;
  end

  else begin
    TicketPriorityCombo.Color := clWhite;
//    DetailPageControl.Visible := True;
    PriorityLbl.Font.Color := clWindowText;
    TicketPriorityCombo.Font.Color := clWindowText;
    TicketPriorityCombo.Color := clWindow;
    BottomGridStyle.TextColor := clWindowText;
    BottomGridStyle.Color := clWindow;
  end;

  TicketPriorityCombo.Enabled := CanWeSetPriority;
end;


procedure TWorkMgtTicketsFrame.ShowSelectedItem;
var
  Cursor: IInterface;
begin
  inherited;
  Cursor := ShowHourGlass;
  ShowTicketSummary;
  if HaveFocusedItem then    //QMANTWO-719  Before setting flag, need to make that we are ready to load it
    GridState.LastDisplayedID := SelectedItemID;   //QMANTWO-690  EB
  ClearSelectionsDBText;  //QMANTWO-577 EB
end;

procedure TWorkMgtTicketsFrame.ShowTicketSummary;
//This method shows the data on the summary panel at bottom right and is called
//when the selected ticket changes. It also passes on any ticket data changes to
//the frame's memory dataset.
var
  Count: integer;
begin
  if (BaseGrid.Dragging) then
    Exit;
  AckButton.Enabled := False; //Elana - Safety Net
  LocateData.DisableControls;
  Responses.DisableControls;
  DetailTable.DisableControls;
  Notes.DisableControls;
  Attachments.DisableControls;
  RiskScoreHistory.DisableControls; //QM-387 EB
  try
    LocateData.Close;
    Responses.Close;
    Notes.Close;
    Attachments.Close;
    RiskScoreHistory.Close;        //QM-387 EB

    if SelectedItemID <> 0 then begin
      if HaveFocusedItem then begin
        // Hack: Use a RO call without a progress dialog to allow drag and drop without focus loss
        GetChan.ShowProgress := False;
        try                                                            
          UpdateItemInCache;
        finally
          GetChan.ShowProgress := True;
        end;
      end;

      AddCalculatedField('work_priority', DetailTable, TStringField, 20);
      DetailTable.Open;
      DetailTable.Refresh; //Prevent DBISAM 8708 error (for change detection)
      DetailTable.SetRange([SelectedItemID], [SelectedItemID]);
      LocateData.Params[0].AsInteger := SelectedItemID;
      AddCalculatedField('alert2', LocateData, TStringField, 2);
      LocateData.Open;
      SetFieldDisplayFormat(LocateData, 'closed_date', 'mmm d, t');
      Responses.ParamByName('ticket_id').AsInteger := SelectedItemID;
      Responses.Open;
      SetFieldDisplayFormat(Responses, 'response_date', 'mmm d, t');

      {Notes}
      Notes.ParamByName('ticket_id').AsInteger := SelectedItemID;
      Notes.ParamByName('foreign_type_ticket').AsInteger := qmftTicket;
      Notes.ParamByName('foreign_type_locate').AsInteger := qmftLocate;
      Notes.Open;
      SetFieldDisplayFormat(Notes, 'entry_date', 'mmm d, t');
      if Notes.IsEmpty then
        NotesTab.ImageIndex := 1
      else
        NotesTab.ImageIndex := 2; //ImageIndexNotes;

      {Attachments}
      Attachments.ParamByName('foreign_type').AsInteger := qmftTicket;
      Attachments.ParamByName('foreign_id').AsInteger := SelectedItemID;
      Attachments.Open;


      {Risk Score History}
      RiskScoreHistory.ParamByName('ticket_id').AsInteger := SelectedItemID; //QM-387 EB pt 3
      RiskScoreHistory.Open;
      Count := RiskScoreHistory.RecordCount;

      {EB - On the first call this is called before the row has focus}
      if HaveFocusedItem then begin
        UpdateFrameData(DetailTable);
        SetPriorityComboBox(TicketPriorityCombo, DetailTable.FieldByName('work_priority_id').Value);


      end;
      if VarIsNull(ListTable.FieldByName('est_start_time').Value) then
        EstStartText.Caption := ''
      else
        EstStartText.Caption := FormatDateTime('ddd mmm d, t', ListTable.FieldByName('est_start_time').AsDateTime);



    end;

  finally
    DetailTable.EnableControls;
    LocateData.EnableControls;
    Responses.EnableControls;
    Notes.EnableControls;
    Attachments.EnableControls;
    RiskScoreHistory.EnableControls;
    AckButton.Enabled := CanWeAcknowledge;
    TicketPriorityCombo.Enabled := CanWeSetPriority;   //QMANTWO-564 EB Broken out from CanWeAcknowledge
  end;
end;

procedure TWorkMgtTicketsFrame.ReassignSelectedTickets(SelectedLocatorID, LocatorID: Integer);
var
  SelRecord: TcxCustomGridRecord;
  i: Integer;
  TicketID: Integer;
  IDs: TStringList;
begin
  // TODO: This proc needs to be changed to use a Thrift command struct
  IDs := TStringList.Create;
  try
    for i := 0 to TicketView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := TicketView.Controller.SelectedRecords[i];
      if SelRecord.IsData then begin
        TicketID := SelRecord.Values[ColTicketID.Index];
        if TicketID > 0 then
          IDs.AddInteger('', TicketID);
      end;
    end;

    DM.MoveTheseTickets(IDs, SelectedLocatorID, LocatorID); //QMANTWO-573  EB Remove QMServer2 reference

    if IDs.Count > 0 then
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeMoveTicket, IntToStr(IDs.Count) + ' moved');
  finally
    FreeAndNil(IDs);
  end;
  RefreshCaptionCounts;
end;

procedure TWorkMgtTicketsFrame.ItemDetailTableCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('work_priority').AsString := DM.GetTicketPriority(DataSet.FieldByName('ticket_id').AsInteger);

  end;

procedure TWorkMgtTicketsFrame.ItemListTableAfterOpen(DataSet: TDataSet);
begin
  SetFieldDisplayFormat(ListTable, 'due_date', 'mmm d, t');
  SetFieldDisplayFormat(ListTable, 'activity_date', 'mmm d, t');
  SetFieldDisplayFormat(ListTable, 'est_start_time', 'mmm d, t');
end;

procedure TWorkMgtTicketsFrame.TicketListSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
 // Placeholder - hit the datachange event
end;

procedure TWorkMgtTicketsFrame.TicketPriorityComboChange(Sender: TObject);
begin
  inherited;
  SetTicketPriority;   //QM-710 Manage Work Clean up EB
end;

procedure TWorkMgtTicketsFrame.TicketPriorityComboKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  key := #0;    //QM-710 EB - Prevent changes by typing (which can cause an error)
  
end;

procedure TWorkMgtTicketsFrame.TicketViewCellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  //The reason Showing[Ticket|WO|Damage]Summary checks are there is to prevent
  //excessive fetching of the ticket|damage|wo details from the server. If the
  //summary is already showing the selected row, then there is no need to sync
  //those details down again.

  if (not ShowingSummary) and (Assigned(Sender.Controller)) then
    ViewFocusedRecordChanged(Sender, Sender.Controller.FocusedRecord, Sender.Controller.FocusedRecord, False);

  AHandled := False;
  GridState.InitialDisplay := False;  //QMANTWO-564 5/31 Turn off InitialDisplay so that focused records are automatically selected
  Application.HandleMessage;
end;

procedure TWorkMgtTicketsFrame.TicketViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  Status: TTicketStatus;
  InvertFlag: boolean;
  TTStr: string;
  RO: integer;
  ExpValue: TDateTime;   ExpValueStr: string;
  CC: string;
begin
  try
  {This happens after the style changes, so be careful not to reset from a style change}
  {Init}                  //QMANTWO-690 EB Refactored
  ACanvas.Font.Style := [];
  InvertFlag := False;
  ExpValue := 0;
  CC := '';

  {Alerts - QM-62 EB Alert flag - This is moved to the end so it is not overwritten}

    {NOT Selected Row}
    if not AViewInfo.Selected then begin
      ACanvas.Font.Style := [];
      ACanvas.Font.Color := clWindowText;
    end
    {Focused}                                            //QM-751 EB First Task Fix - moved   QMANTWO-690 Tweaking Manage Work Ticket Grid
    else if AViewInfo.GridRecord.Focused then begin
      if (not AViewInfo.Selected) and (AViewInfo.GridRecord.Index = 0) and (not ShowingSummary) then  //QM-56 EB
        ACanvas.Font.Style := []
      else
        ACanvas.Font.Style := [fsBold];
    end;


    {Age_Hours > 2}           //QM-551
    if (not VarIsNull(AViewInfo.GridRecord.Values[ColTicketAgeHours.Index])) and (AViewInfo.Item.Index = ColTicketAgeHours.Index)  then
      if (AViewInfo.GridRecord.Values[ColTicketAgeHours.Index] > 2) and (not AViewInfo.Selected) then begin
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
      end;



    {Alt Due Date}   //QM-973 EB
    if (not VarIsNull(AViewInfo.GridRecord.Values[ColAltDueDate.Index])) and (AViewInfo.Item.Index = ColAltDueDate.Index)  then begin
      if(AviewInfo.GridRecord.Values[ColAltDueDate.Index] < Now) then begin
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
        ACanvas.Brush.Color := clWebLightYellow;
      end
      else if (AviewInfo.GridRecord.Values[ColAltDueDate.Index] < (Today + 2)) then begin
                ACanvas.Font.Color := clNavy;
        ACanvas.Font.Style := [fsBold];
        ACanvas.Brush.Color := clWebLightYellow;
      end;
    end;

    {Status}
    if not AViewInfo.Selected then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[ColTicketStatus.Index]) then
        Status := TTicketStatus(word(AViewInfo.GridRecord.Values[ColTicketStatus.Index])) //QMANTWO-396
      else
        Status := [];
      ACanvas.Brush.Color := GetTicketColor(Status, (AViewInfo.GridRecord.Index mod 2) = 1);
    end;

    {N}
    if (ShowOpenItemsOnly) and (AViewInfo.GridRecord.Values[ColTicketIsVisible.Index] = 'N') then
      ACanvas.Font.Color := clGray;

    {Alerts}
    if (AViewInfo.Item.Index = ColAlert.Index) then begin        //QM-62 EB Alert flag
      {Alert field - has to be after status so it is not overwritten}
      if not VarIsNull(AViewInfo.GridRecord.Values[ColAlert.Index]) then begin
        ACanvas.Brush.Color := cl9SofterYellow;
        ACanvas.Font.Color := clRed;
        ACanvas.Font.Style := [fsBold];
      end;
    end;

    {Est_Start_Time - First Task}  //QM-751 EB First Task fix - moved after Alerts
    if not VarIsNull(AViewInfo.GridRecord.Values[ColEstStartTime.Index]) then begin
      if AViewInfo.GridRecord.Values[ColEstStartTime.Index] > Now then begin  //EB QM-93 First Task - Only show the ones that are not past start time
        ACanvas.Font.Style := [fsBold];
        if ((AViewInfo.Item.Index = ColTicketNumber.Index) or
            (AViewInfo.Item.Index = ColEstStartTime.Index)) then begin
            ACanvas.Font.Color := cl9LightMaroon;
            ACanvas.Brush.Color := clCream;
        end;
      end;
    end;

    {Route Order = 1}
    if (AViewInfo.Item.Index = ColRouteOrder.Index) then begin  //QM-916 Route Order limitation cleanup
      RO := 0;
      if not VarIsNull(AViewInfo.GridRecord.Values[ColRouteOrder.Index]) then begin
        RO :=  AViewInfo.GridRecord.Values[ColRouteOrder.Index];
        {First one}
        if RO = 1 then begin
          ACanvas.Brush.Color := cl9SofterYellow;
          ACanvas.Font.Color := cl9LightMaroon;  // QM-973 EB - just quick fix
        end
        else if RO < RouteOrderNull then begin    //QM-916 Route Order Limitation  (Null values are turned into 9999 for ordering purposes)
          ACanvas.Font.Color := cl9LightMaroon;
          ACanvas.Font.Style := [fsBold];
        end
        else
          ACanvas.Font.Color := clGray;
      end;
    end;

    {N}
    if (ShowOpenItemsOnly) and (AViewInfo.GridRecord.Values[ColTicketIsVisible.Index] = 'N') then
      ACanvas.Font.Color := clGray;

  {Due Date colors}
    if not VarIsNull(AViewInfo.GridRecord.Values[ColTicketType.Index]) then begin    //QM-604 EB  QM-417
      {Due Date was edited (CA) -RS}
      if (AViewInfo.Item.Index = ColDueDate.Index) then begin
        TTStr := AViewInfo.GridRecord.Values[ColTicketType.Index];
        If AnsiPos('-RS', TTStr) > 0 then begin
          ACanvas.Brush.Color := cl9SofterYellow;
          ACanvas.Font.Color := cl9LightMaroon;
          ACanvas.Font.Style := [fsBold];
        end
        {Extended Time (OCC2) -ET+}
        else if AnsiPos('-ET+', TTStr) > 0 then begin     //QM-888 EB Extended Due Date
          ACanvas.Brush.Color := clWebDarkgreen;
          ACanvas.Font.Color := cl9SofterYellow;
          ACanvas.Font.Style := [fsBold];
        end;
      end;
    end;


    {Route Order}
    if (AViewInfo.Item.Index = ColRouteOrder.Index) then begin
      if not VarIsNull(AViewInfo.GridRecord.Values[ColRouteOrder.Index]) then begin   //QM-417
        if (AviewInfo.GridRecord.Values[ColRouteOrder.Index] = 0) then begin
          ACanvas.Font.Color := ACanvas.Brush.Color;
        end
        else if (AViewInfo.GridRecord.Values[ColRouteOrder.Index] > 0) then
          ACanvas.Font.Style := [fsBold];
        end;
      end;


      {Expiration Date - whole row}   //QM-991 EB
      {Color for the whole row - only expired}
    if not VarIsNull(AViewInfo.GridRecord.Values[ColExpirationDate.Index]) then begin
      If (AViewInfo.GridRecord.Values[ColExpirationDate.Index] < Now) then begin
        ACanvas.Brush.Color := clWebSaddleBrown;
        ACanvas.Font.Color := clCream;
      end;
    end;

    {Expiration Date - Cell Color}  //QM-991 EB
    if (not VarIsNull(AViewInfo.GridRecord.Values[ColExpirationDate.Index]))
     and (AViewInfo.Item.Index = ColExpirationDate.Index)  then begin
      ExpValue := AViewInfo.GridRecord.Values[ColExpirationDate.Index];
      ExpValueStr := DateTimeToStr(ExpValue);
      {Ticket has expired}
      if(ExpValue < Now) and (ExpValue > 0) then begin
        ACanvas.Font.Color := cl9LightMaroon;
        ACanvas.Font.Style := [fsBold];
        ACanvas.Brush.Color := cl9SofterYellow;
      end
      {Ticket is about to expire}
      else if (ExpValue <= (GetNextBusDay(Today) + 2)) then begin
        ACanvas.Font.Color := clNavy;
        ACanvas.Font.Style := [fsBold];
        ACanvas.Brush.Color := cl9SofterYellow;
      end;
    end;

      ADone := False;
  except
    ADone := False;
    {Swallow - no color, just display grid field normally}
  end;

end;

procedure TWorkMgtTicketsFrame.TicketViewCustomDrawIndicatorCell(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
  {QMANTWO-690  Tweaks on Manage Work Ticket Screen}
begin
  try
    {Grid Indicator highlighting}
   {Est_Start_Time - First Task}   //QM-751 Eb First Task fix
   if (AViewInfo is TcxGridIndicatorRowItemViewInfo) then begin   //QM-814 EB Grid Errors - exclude calls from custom
    if not VarIsNull(TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Values[ColEstStartTime.Index]) then begin
      if (TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Values[ColEstStartTime.Index] > Now) then   //EB QM-93 First Task - Only show the ones that are not past start time
         if (TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Selected) then
           ACanvas.Brush.Color := clYellow
         else
           ACanvas.Brush.Color := clWebLightSalmon;
    end
    else
    if (AViewInfo is TcxGridIndicatorRowItemViewInfo) then begin
       If TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.Selected then begin
          ACanvas.Brush.Color := clYellow;
       end
    end
    else if TcxGridIndicatorRowItemViewInfo(AViewInfo).GridRecord.focused then
      ACanvas.Brush.Color := clSkyBlue;
   end;

  except
    {swallow - no color assigned}
  end;
end;

procedure TWorkMgtTicketsFrame.TicketViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  SearchForURL;  //qm-461 SR
end;

procedure TWorkMgtTicketsFrame.TicketViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
{  var
     ThisGrid : TcxGrid;
     HitTest: TcxCustomGridHitTest;
}
begin
  {HitTest:
   if ((ssCtrl in Shift) or (ssShift in Shift)) then begin
    HitTest := TicketView.GetHitTest(X,Y);
    if (HitTest.HitTestCode = htRecord)....
  end;}
end;



procedure TWorkMgtTicketsFrame.TicketViewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  FocusedRec: TcxCustomGridRecord;
  ShouldBeSelected: boolean;

begin
  {Elana - Do no call this explicitly (it should only be called when the mouse is clicked on Grid)}
 // inherited;
 ShouldBeSelected := False;
 if not GridState.AmDragging then begin
   if HaveFocusedItem then begin
     FocusedRec := ListView.Controller.FocusedRecord;
     If not PrevSelList.Has(FocusedRec.Index) then  {If it was not previously selected}
       ShouldBeSelected := True  //QMANTWO-690 EB
     else
       ShouldBeSelected := False;  //QMANTWO-752 EB Explicit setting
   end;

   {Additional selection or Deselecting}   //QMANTWO-752/690
  if ((ssCtrl in Shift) or (ssShift in Shift)) then begin   //QMANTWO-690
    If (not TicketView.Controller.FocusedRecord.Selected) and ShouldBeSelected then      //QMANTWO-690 EB
      TicketView.Controller.FocusedRecord.Selected := True
         
  end
  else begin
    TicketView.Controller.ClearSelection;
    TicketView.Controller.FocusedRecord.Selected := True;
  end;
  GridState.CheckSelection := True;
 end;
 GridState.AmDragging := False;
end;


function TWorkMgtTicketsFrame.LoadTicketList(SelectedLocatorID, ManagerID,
  NumTicketDays, MaxActivityDays: Integer; NodeName: string; NodeIsUnderMe, ShowOpenWorkOnly: Boolean ): Boolean;
var
  Doc: IXMLDomDocument;
  TicketCountsForLoc : TTicketCounts;
  NodeBucketName: string; //QM-486 Virtual Bucket EB
  OpenRowLimit: integer; //QM-695 Open Expanded EB
begin
  {Previously Loaded}
  if (LastLoaded.SelectedLocatorID = SelectedLocatorID) and (not ForceReload) then begin  //QM-695 EB
    inc(fGridLoads);
    loads.Caption := '#Loads: ' + intToStr(fGridLoads) + ',  ID: ' + intToStr(SelectedLocatorID);  //tracking repeat loads
    exit;
  end;

  
  FPopulatingWorkSummary := True;
  BaseGrid.Enabled := True;
  lblAllCounts.Visible := False;
  try
    LastLoaded.SelectedLocatorID := SelectedLocatorID;
    LastLoaded.ManagerID := ManagerID;
    LastLoaded.NumTicketDays := NumTicketDays;
    LastLoaded.MaxActivityDays := MaxActivityDays;
    LastLoaded.NodeName := NodeName;  //QM-486 Virtual Bucket EB
    LastLoaded.NodeIsUnderMe := NodeisUnderMe;
    LastLoaded.ShowOpenWorkOnly := ShowOpenWorkOnly;

    if (SelectedLocatorID = 0) or not NodeIsUnderMe then begin
      ClearListTable;
      LastLoaded.GridLoaded := False;
    end else begin
      if SelectedLocatorID = MAGIC_NEW_EMERG then
        Doc := PopulateNewEmergFromServer(ManagerID, NumTicketDays, ShowOpenWorkOnly)
      else if SelectedLocatorID = MAGIC_NEW_ACTIVITY then
        Doc := PopulateActivitiesListFromServer(ManagerID, MaxActivityDays)
      else if SelectedLocatorID = MAGIC_PAST_DUE then       //QM-133 Past Due EB
        Doc := PopulatePastDueFromServer(ManagerID, NumTicketDays)
	    else if SelectedLocatorID = MAGIC_TODAY then          //QM-318 Today EB
        Doc := PopulateTodayFromServer(ManagerID, NumTicketDays)
      else if SelectedLocatorID = MAGIC_VIRTUAL then begin       //QM-486 Virtual Bucket EB
        NodeBucketName := LastLoaded.NodeName;
        Doc := PopulateVirtualFromServer(ManagerID, NumTicketDays, LastLoaded.NodeName)
      end
      else if SelectedLocatorID = MAGIC_OPEN_TICKETS then begin
        if PermissionsDM.CanI(RightTicketsOpenExpTree) then begin
          OpenRowLimit := DM.GetConfigurationDataInt('OpenTicketsLimit', 5000);
          Doc := PopulateOpenTicketsExpFromServer(ManagerID, OpenRowLimit, NumTicketDays) //QM-695 Open EB
        end
        else begin
          Doc := PopulateOpenTicketsSumFromServer(ManagerID, NumTicketDays);  //QM-428 Open EB
          BaseGrid.Enabled := False;
        end;
      end
      else begin
        Doc := PopulateTicketListFromServer(SelectedLocatorID, NumTicketDays, ShowOpenWorkOnly, SelLocatorUsesRouteOrder);   //QM-896 EB Route Order Limit
        LastLoaded.GridLoaded := True;
      end;

      BaseGrid.BeginUpdate;
      try
      {Load ListTable (TicketList) & init Route Order Array}
        TicketCountsForLoc := PopulateDataSetFromDOM(Doc, ListTable, SelectedLocatorID);  //QM-274 EB Counts & QM-133 EB Past Due (change structure of call params)
        RefreshTicketCounts(TicketCountsForLoc);


        //QMANTWO-690 EB Sel/Focus Tweak - Do not reset the selected item when this is set to True
          if (SelectedItemID = 0) then
            TicketView.DataController.GotoFirst
          else
            TicketView.DataController.LocateByKey(SelectedItemID);

      finally
        FBlockItemUpdates := True;
        fGridLoads := 1;
        try
          BaseGrid.EndUpdate;
        finally
          FBlockItemUpdates := False;
        end;
      end;
    end;

  finally
    FPopulatingWorkSummary := False;
    if TicketView.DataController.RecordCount > 0 then begin   //QM-338 EB
      TicketView.OptionsCustomize.ColumnsQuickCustomization := True;
      BaseGrid.Enabled := True;
    end
    else begin
      TicketView.OptionsCustomize.ColumnsQuickCustomization := False;    //QM-338 EB
      BaseGrid.Enabled := False;
    end;
  end;

  //Close the detail dataset in preparation for selecting first row
  LocateData.Close;
  DetailTable.Close;

  Result := HasRecords(ListTable);
  if Result then begin
    SelectFirstGridRow;
    GridState.InitialDisplay := True;
  end;
end;

procedure TWorkMgtTicketsFrame.LocateGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  if AViewInfo.Item.Index = LocateGridAlert.Index then begin
    ACanvas.Font.Color := clRed;
    ACanvas.Font.Style := [fsBold];
  end;
  ADone := False;
end;

procedure TWorkMgtTicketsFrame.AcknowledgeActionExecute(Sender: TObject);

begin
  inherited;
  try
    if SelectedTreeNodeType = ntEmergency then begin
      Screen.Cursor := crHourGlass;
      AcknowledgeCurrentTicket; // EB - comment out to test so you don't "eat up" test tickets

    end
    else if SelectedTreeNodeType = ntTicketActivities then
      AcknowledgeTicketActivity;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TWorkMgtTicketsFrame.AcknowledgeCurrentTicket;
const
  AckQuestion = 'Are you sure you want to acknowledge ticket %s?';
var
  RowIdx: Integer;
  CurTicketID,
  CurClientID,
  CurLocatorID: Integer;
  OldStatus, AdjStatus : string;   //QM-604 EB
  CallCenter: string;
  UseEmergencyAckStatus: boolean;
  CanAckStatus: boolean;  //Only use this in the cases where a statused ticket was left in the Emergency bucket and needs to be moved
  AckAddress : string;
  begin
  Assert(SelectedTreeNodeType = ntEmergency, 'Expecting an emergency node in AcknowledgeCurrentTicket');
  if not HaveFocusedItem then
    Exit;
  if IsDamageTicket then // Damage ticket for FCO1, 6001
    AcknowledgeDamageTicket
  else begin
    AckAddress := 'ADDRESS: ' + #13#10 +
                  DetailTable.FieldByName('work_address_number').AsString + ' ' +
                  DetailTable.FieldByName('work_address_street').AsString;
    If trim(DetailTable.FieldByName('work_city').AsString) <> '' then begin
      AckAddress := AckAddress + #13#10  + DetailTable.FieldByName('work_city').AsString;
      if trim(DetailTable.FieldByName('work_state').AsString) <> '' then
        AckAddress := AckAddress + ', ' + DetailTable.FieldByName('work_state').AsString;
    end;
    AckAddress := Trim(AckAddress);
    if AckAddress <> '' then
      AckAddress := #13#10 + #13#10 + AckAddress;
    if MessageDlg(Format(AckQuestion + AckAddress, [ColTicketNumber.DataBinding.Field.AsString]),
      mtConfirmation, [mbYes,mbNo], 0) = mrYes then begin
      try
        CurTicketID := DetailTable.FieldByName('Ticket_id').AsInteger;
        CurClientID := LocateData.FieldByName('client_id').AsInteger; //grab the first one or whatever is selected
        OldStatus := LocateData.FieldByName('status').AsString;
        CurLocatorID := LocateData.FieldByName('locator_id').AsInteger;
        CallCenter := DetailTable.FieldByName('ticket_format').AsString;
        DM.GoToTicketAck(CurTicketID);
        //CO - Questionnaire will only trigger if the client has that status defined in question tables
        AdjStatus := CO_Acknowledge_Status; //QM-604 EB Nipsco - had to change to var param
        If TLocStatusQuestions.GetStatusQuestions(CurTicketID, CurClientID, CurLocatorID, OldStatus, AdjStatus, CallCenter)= SQTRUE then
        begin
          UseEmergencyAckStatus := True;
          Application.ProcessMessages;
        end
        else
          UseEmergencyAckStatus := DM.MatchesCenterGroup('CallCentersThatAck', CallCenter);

        {If we use the 'ACK" status for this Call Center, then change it and make sure the ticket is set to modified}
        If UseEmergencyAckStatus then begin
            CanAckStatus := DM.ChangeAllLocStatus(CurTicketID, EmergencyAckStatus);
            DM.SetTicketModified;
            if DM.Engine.HavePendingTicketChanges then
              DM.SendChangesToServer;
        end;

        RefreshItem;
        RefreshCaptionCounts;
        RowIdx := TicketView.Controller.FocusedRowIndex;
        DM.SetTicketAcknowledged(SelectedItemID); {Takes it out of the bucket}
        if Assigned(FOnRemoveTicketFromList) then
          FOnRemoveTicketFromList(self);
        SelectAndShowTicketAtRow(RowIdx);
        EmployeeDM.AddEmployeeActivityEvent(ActivityTypeACKEmergTicket, IntToStr(CurTicketID));   //QM-838 EB Emerg Ack Emp Activity
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;





function TWorkMgtTicketsFrame.IsDamageTicket: Boolean;
var
  TicketFormat: string;
  TicketType: string;
begin
  Result := False;
  TicketFormat := DetailTable.FieldByName('ticket_format').AsString;
  TicketType := DetailTable.FieldByName('ticket_type').AsString;

  if TicketFormat = 'FCO1' then
    Result := StrContains('EMER', TicketType) and StrContains('DAMG', TicketType)
  else if TicketFormat = '6001' then
    Result := StrContains('DIG-UP', TicketType) or StrContains('DIG UP', TicketType);
end;



function TWorkMgtTicketsFrame.IsMultiSelect: boolean;
begin
  if TicketView.Controller.SelectedRecordCount > 1 then
    Result := True
  else
    Result := False;
  
end;

procedure TWorkMgtTicketsFrame.AcknowledgeDamageTicket;
var
  AckDialog: TAckDamageDialog;
  InvestigatorID: Integer;
  RowIdx: Integer;
begin
  Assert(HaveFocusedItem);
  AckDialog := TAckDamageDialog.Create(nil);
  try
    if AckDialog.ShowModal = mrOK then begin
      RowIdx := TicketView.Controller.FocusedRowIndex;
      if AckDialog.CreateDamageCheckbox.Checked then begin
        InvestigatorID := AckDialog.GetInvestigatorID;
        LDamageDM.SetDamageTicketAcknowledged(SelectedItemID, InvestigatorID);
      end
      else
        DM.SetTicketAcknowledged(SelectedItemID);
      ListTable.Delete;
      SelectAndShowTicketAtRow(RowIdx);
    end;
  finally
    FreeAndNil(AckDialog);
  end;
end;

procedure TWorkMgtTicketsFrame.AcknowledgeTicketActivity;
var
  TicketActAckIDs: TIntegerArray;
  TicketActAckID: Integer;
  RowIdx: Integer;
begin
  Assert(SelectedTreeNodeType = ntTicketActivities, 'Expecting an activity node in AcknowledgeTicketActivity');
  if not HaveFocusedItem then
    Exit;

  // Current specs require single activity acknowledgement, but it is setup to handle multiples
  if isMultiSelect then
    raise EOdDataEntryError.Create('You can only acknowledge one activity at a time.');

  SetLength(TicketActAckIDs, 1);
  TicketActAckID := ColActivityID.DataBinding.Field.AsInteger;
  TicketActAckIDs[0] := TicketActAckID;

  RowIdx := TicketView.Controller.FocusedRowIndex;
  DM.SetTicketActivitiesAcknowledged(TicketActAckIDs);

  if Assigned(FOnRemoveTicketFromList) then
    FOnRemoveTicketFromList(self);

  SelectAndShowTicketAtRow(RowIdx);
end;

function TWorkMgtTicketsFrame.CanWeAcknowledge: Boolean;
begin
  AcknowledgeAction.Enabled := ((SelectedTreeNodeType = ntEmergency) or
                                (SelectedTreeNodeType = ntTicketActivities)) and
                                HaveFocusedItem and
                               (TicketView.Controller.SelectedRecordCount = 1);

{QMANTWO-564 EB Removed, made setting more explicit}
//  TicketPriorityCombo.Enabled := (SelectedTreeNodeType <> ntTicketActivities) and HaveFocusedItem;
  Result := AcknowledgeAction.Enabled;
end;


function TWorkMgtTicketsFrame.CanWeSetPriority: Boolean;
begin
  if (SelectedTreeNodeType = ntOpen) then
    Result := False // QM-428 EB {no grid functionality}
  else if not isMultiSelect then
    Result := (SelectedTreeNodeType <> ntTicketActivities) and HaveFocusedItem
  else
    Result := PermissionsDM.CanI(RightMultiSelPriorityChange);
end;

procedure TWorkMgtTicketsFrame.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  CanWeAcknowledge;
  TicketPriorityCombo.Enabled := CanWeSetPriority;
end;

procedure TWorkMgtTicketsFrame.SetPriorityComboBox(PriorityCombo: TComboBox;
  PriorityID: Variant);
begin
  Assert(Assigned(PriorityCombo));
  DM.GetRefLookupList('tkpriority', PriorityCombo.Items, True);  //QM-933 EB SortBy

  if PriorityID = Null then
    PriorityCombo.ItemIndex := PriorityCombo.Items.IndexOf('Normal')
  else
    SelectComboBoxItemFromObjects(PriorityCombo, PriorityID);
end;

procedure TWorkMgtTicketsFrame.MarkPriority(PriorityRefID: Integer);
begin
  if SelectedItemID <> 0 then begin
    DM.SetTicketPriority(SelectedItemID, PriorityRefID);
    RefreshItem;
  end else
    raise EOdDataEntryError.Create('Please select a ticket before changing the priority.');
end;


procedure TWorkMgtTicketsFrame.MoveFocusToTicketByRow(Row: integer);
begin
  {QMANTWO-690 EB Allows us to move back to the last opened ticket}
  TicketView.ViewData.Rows[Row].Focused := True;
end;


procedure TWorkMgtTicketsFrame.MoveToTicketID(GoToTicketID: Integer);
var
  i: integer;
begin
  {QMANTWO-690 EB Allows us to move back to the last opened ticket}
  for i := 0 to TicketView.ViewData.RecordCount - 1 do
    If (GoToTicketID = TicketView.ViewData.Records[i].Values[ItemIDColumn.ID]) then begin
      TicketView.ViewData.Records[i].Focused := True;
      TicketView.ViewData.Records[i].Selected := True;
      Exit;
    end;
end;

procedure TWorkMgtTicketsFrame.MarkMultiPriorities(PriorityRefID: Integer);
{QMANTWO-564 EB - Multi Select Ticket Priorities}
var
  SelRecord: TcxCustomGridRecord;
  i: integer;
  StashSelItemID, TicketID: integer;
begin
  {EB - DBL check that they have rights (we also disable combo if they don't have
        rights)}
  if not PermissionsDM.CanI(RightMultiSelPriorityChange) then
    EXIT;
  
  try
    StashSelItemID := SelectedItemID;
    for i := 0 to TicketView.Controller.SelectedRecordCount - 1 do begin
      SelRecord := TicketView.Controller.SelectedRecords[i];
      if SelRecord.IsData then begin
        TicketID := SelRecord.Values[ColTicketID.Index];
        if TicketID > 0 then begin
          //EB - Send the change straight up to server!
          DM.SetTicketPriority(TicketID, PriorityRefID );
          FBlockItemUpdates := True;
          //EB - Update the grid so the user sees the change
           SelRecord.Values[ColWorkPriority.Index] := TicketPriorityCombo.Text;
           FBlockItemUpdates := False;
        end;
      end;
    end;
  finally
    LoadSelList;
    LoadTicketList(LastLoaded.SelectedLocatorID, LastLoaded.ManagerID,           //QMANTWO-690 EB - Cory Complaint
                   LastLoaded.NumTicketDays, LastLoaded.MaxActivityDays, LastLoaded.NodeName, //QM-486 Virtual Bucket EB
                   LastLoaded.NodeIsUnderMe, LastLoaded.ShowOpenWorkOnly);

    ReSelectList;
  end;
end;

function TWorkMgtTicketsFrame.HasPrevSelectedItems(CurEmpID: integer): boolean;
begin
  {If it isn't set, then we are going to ignore it}
  if (CurEmpID = -1) then begin
    CurEmpID := LastLoaded.SelectedLocatorID;
    Result := False;                         //QM-710 EB (Can we just set to false if the Emp ID is not set)
  end
  else
    Result := (LastLoaded.GridLoaded) and (PrevSelTicketList.Count > 0) and (CurEmpID = LastLoaded.SelectedLocatorID);
end;



function TWorkMgtTicketsFrame.HasSelectedTickets: boolean;   //QM-959 EB Bulk Ticket Due Date
begin
  Result := False;
  Result := (TicketView.Controller.SelectedRecordCount > 0);
  //    (SelectedLocatorID > 0) and
 //   (TicketFrame.TicketView.DataController.RecordCount > 0) ;
end;

procedure TWorkMgtTicketsFrame.HighPriorityActionExecute(Sender: TObject);
begin
  inherited;
  TicketPriorityCombo.ItemIndex := TicketPriorityCombo.Items.IndexOf('High');
  MarkPriority(GetComboObjectInteger(TicketPriorityCombo, True));
end;


procedure TWorkMgtTicketsFrame.SaveTicketGridSettings;  //QM-338 EB new grid INI
var
  QMIniFile: string;
begin
  QMIniFile := GetTicketGridIniPath;
  TicketView.StoreToIniFile(QMIniFile);
end;

procedure TWorkMgtTicketsFrame.RestoreTicketGridSettings; //QM-338 EB new grid INI
var
  QMIniFile: string;
begin
  QMIniFile := GetTicketGridIniPath;
  if FileExists(QMIniFile) then
    TIcketView.RestoreFromIniFile(QMIniFile);
end;



procedure TWorkMgtTicketsFrame.NormalPriorityActionExecute(Sender: TObject);
begin
  inherited;
  TicketPriorityCombo.ItemIndex := TicketPriorityCombo.Items.IndexOf('Normal');
  if isMultiSelect then
    MarkMultiPriorities(GetComboObjectInteger(TicketPriorityCombo, True))
  else
    MarkPriority(GetComboObjectInteger(TicketPriorityCombo, True));
end;


procedure TWorkMgtTicketsFrame.OptimizeRouteFromTkt1Click(Sender: TObject);  //QM-702 Route Optimization EB
var
  FocusedRec: TcxCustomGridRecord;
  NextTicketID: integer;
  DisplayTicketNum: string;
  lSubmit: boolean;
begin
  inherited;
  NextTicketID := 0;

  If HaveFocusedItem then begin
    FocusedRec := TicketView.Controller.FocusedRecord;
    NextTicketID := FocusedRec.Values[ColTicketID.Index];
    DisplayTicketNum := FocusedRec.Values[ColTicketNumber.Index];
    lSubmit := EmployeeDM.AddROOptimizationRequest(LastLoaded.SelectedLocatorID, DM.EmpID, NextTicketID);
    ShowMessage('An optimization has been requested for ' + EmployeeDM.GetEmployeeShortName(LastLoaded.SelectedLocatorID) + ' with 1st Ticket of ' + DisplayTicketNum);
    ToggleOptimizeRouteOn(False);
  end
  else
    ShowMessage('There is no ticket focused in grid. Please select and try again');
end;




procedure TWorkMgtTicketsFrame.ReleasePriorityActionExecute(Sender: TObject);
begin
  inherited;
  if not PermissionsDM.CanI(RightTicketsReleaseForWork) then begin
    MessageDlg('You do not have sufficient rights to release a ticket for work.', mtWarning, [mbOk], 0);
    Exit;
  end;

  TicketPriorityCombo.ItemIndex := TicketPriorityCombo.Items.IndexOf('Release for work');
  if isMultiSelect then
    MarkMultiPriorities(GetComboObjectInteger(TicketPriorityCombo, True))
  else
    MarkPriority(GetComboObjectInteger(TicketPriorityCombo, True));
end;


procedure TWorkMgtTicketsFrame.PriorityPopupPopup(Sender: TObject);
var
  I: Integer;
begin
  inherited;

  for I := 0 to PriorityPopup.Items.Count - 1 do begin
    if Assigned(PriorityPopup.Items[I].Action) then
      (PriorityPopup.Items[I].Action as TAction).Enabled := False;
  end;

  if SelectedTreeNodeType = ntTicketActivities then
    Exit;

  if HaveFocusedItem then begin
    if (not ShowingSummary) then
      ViewFocusedRecordChanged(TicketView, TicketView.Controller.FocusedRecord,
        TicketView.Controller.FocusedRecord, False);
    HighPriorityAction.Enabled := True;
    NormalPriorityAction.Enabled := True;
    ReleasePriorityAction.Enabled := True;
    DispatchingPriorityAction.Enabled := True;
  end;
end;

procedure TWorkMgtTicketsFrame.UpdateFrameData(const SourceDataSet: TDataSet);

  procedure SetTicketType(const TicketType: string);
  begin
    Assert(not ListTable.IsEmpty);
    ListTable.FieldByName('ticket_type').AsString := TicketType;
  end;

begin
  FBlockItemUpdates := True;
  // Update the GUI/grid in case any values just changed
  EditDataSet(ListTable);
  if SourceDataSet.FieldByName('kind').AsString = TicketKindDone then
    SetTicketType(TicketKindDone)
  else if SourceDataSet.FieldByName('kind').AsString = TicketKindOngoing then
    SetTicketType('ONGOING: ' + SourceDataSet.FieldByName('ticket_type').AsString)
  else
    SetTicketType(SourceDataSet.FieldByName('ticket_type').AsString);
  ListTable.FieldByName('work_priority').AsString := SourceDataSet.FieldByName('work_priority').AsString;
  ListTable.Post; // Saves to memory only
  FBlockItemUpdates := False;
end;

procedure TWorkMgtTicketsFrame.RefreshTicketButtonClick(Sender: TObject);
begin
  inherited;
  RefreshItem;

end;

procedure TWorkMgtTicketsFrame.RefreshTicketCounts(TktCounts : TTicketCounts);
//QM-274 EB Counts
var
  NeedComma: Boolean;
  lstr: string;
  lbucket:string;
begin
  NeedComma := False;
  lstr := '';
  lbucket := '';

  lblAllCounts.Caption := '';
  if not (ListTable.Active) then
    exit;

  if (TktCounts.BucketName = 'LOCATOR') then
    TktCounts.BucketName :=  EmployeeDM.GetEmployeeShortName(LastLoaded.SelectedLocatorID);

  lbucket := TktCounts.BucketName + ': ' + IntToStr(TktCounts.BucketCount) + ' displayed ';


  if TktCounts.Emergency > 0 then begin
    lstr := lstr + 'Emerg: ' + IntToStr(TktCounts.Emergency);
    NeedComma := True;
  end;

  if TktCounts.Normal > 0 then begin
    if NeedComma then lstr := lstr + ', ';
    lstr := lstr + 'Norm: ' + IntToStr(TktCounts.Normal);
    NeedComma := True;
  end;

  if TktCounts.Ongoing > 0 then begin
    if NeedComma then lstr := lstr + ', ';
    lstr := lstr + 'Ongo: ' + IntToStr(TktCounts.Ongoing);
    NeedComma := True;
  end;

  if (TktCounts.PastDue > 0) and (TktCounts.BucketName <> bPD) then begin
    if NeedComma then lstr := lstr + ', ';
    lstr := lstr +  ' Past: ' + IntToStr(TktCounts.PastDue);
    NeedComma := True;
  end;

  if (tktCounts.Today > 0) and (TktCounts.BucketName <> bToday) then begin
    if NeedComma then lstr := lstr + ', ';
    lstr := lstr + 'Today: ' + IntToStr(TktCounts.Today);
    NeedComma := True;
  end;

  if lstr <> '' then
    lstr := '(' + trim(lstr) + ')';

 lblAllCounts.Caption := lbucket  + lstr;

  
  

  lblAllCounts.Visible := True;


end;


//  NormalCountD.Caption := IntToStr(TktCounts.Normal);
//  OngoingCountD.Caption := IntToStr(TktCounts.Ongoing);
//  PastDueCountD.Caption := IntToStr(TktCounts.PastDue);
//  EmergencyCountD.Caption := IntToStr(TktCOunts.Emergency);
//  TodayCountD.Caption := IntToStr(TktCounts.Today);
//
//  Show := ((TktCounts.Normal + TktCounts.Ongoing + TktCounts.PastDue + TktCounts.Emergency) > 0);
//    NormalCountD.Visible := Show;
//    OngoingCountD.Visible := Show;
//    PastDueCountD.Visible := Show;
//    EmergencyCountD.Visible := Show;
//    TodayCountD.Visible := Show;
//    lblNormalD.Visible := Show;
//    lblOngoingD.Visible := Show;
//    lblPastDueD.Visible := Show;
//    lblEmergencyD.Visible := Show;
//    lblTodayD.Visible := Show;
//end;

end.
