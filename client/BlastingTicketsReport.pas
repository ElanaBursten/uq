unit BlastingTicketsReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, CheckLst, OdRangeSelect;

type
  TBlastingTicketsReportForm = class(TReportBaseForm)
    Label2: TLabel;
    Label3: TLabel;
    ClientList: TCheckListBox;
    Label1: TLabel;
    ManagersComboBox: TComboBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    CallCenterList: TCheckListBox;
    LimitManagerBox: TCheckBox;
    TransmitRange: TOdRangeSelectFrame;
    Label4: TLabel;
    LimitClients: TCheckBox;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure CallCenterListClick(Sender: TObject);
    procedure LimitManagerBoxClick(Sender: TObject);
    procedure LimitClientsClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, OdExceptions;

{$R *.dfm}

procedure TBlastingTicketsReportForm.ValidateParams;
var
  CheckedItems: string;
begin
  inherited;
  SetReportID('BlastingTickets');
  SetParamDate('DateFrom', TransmitRange.FromDate);
  SetParamDate('DateTo', TransmitRange.ToDate);

  if LimitManagerBox.Checked then
    SetParamIntCombo('ManagerID', ManagersComboBox, True)
  else
    SetParamInt('ManagerID', -1);

  if LimitClients.Checked then begin
    CheckedItems := GetCheckedItemString(ClientList, True);
    if CheckedItems = '' then begin
      ClientList.SetFocus;
      raise EOdEntryRequired.Create('Please select the clients to report on.');
    end;
    SetParam('ClientIds', CheckedItems);
  end;
end;

procedure TBlastingTicketsReportForm.InitReportControls;
begin
  inherited;
  SetupCallCenterList(CallCenterList.Items, False);
  TransmitRange.SelectDateRange(rsThisWeek);
  SetUpManagerList(ManagersComboBox);
  LimitManagerBox.Checked := False;
  LimitClients.Checked := False;
  ManagersComboBox.Enabled := False;
  CallCenterList.Enabled := False;
  ClientList.Enabled := False;
end;

procedure TBlastingTicketsReportForm.SelectAllButtonClick(
  Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, True);
end;

procedure TBlastingTicketsReportForm.ClearAllButtonClick(Sender: TObject);
begin
  inherited;
  SetCheckListBox(ClientList, False);
end;

procedure TBlastingTicketsReportForm.CallCenterListClick(Sender: TObject);
var
  CallCenters: string;
begin
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure TBlastingTicketsReportForm.LimitManagerBoxClick(
  Sender: TObject);
begin
  inherited;
  ManagersComboBox.Enabled := LimitManagerBox.Checked;
end;

procedure TBlastingTicketsReportForm.LimitClientsClick(Sender: TObject);
begin
  inherited;
  CallCenterList.Enabled := LimitClients.Checked;
  ClientList.Enabled := LimitClients.Checked;
end;

end.

