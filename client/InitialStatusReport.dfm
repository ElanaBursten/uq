inherited InitialStatusReportForm: TInitialStatusReportForm
  Left = 715
  Top = 252
  Caption = 'InitialStatusReportForm'
  ClientHeight = 375
  ClientWidth = 403
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 83
    Height = 13
    Caption = 'Due Date Range:'
  end
  object Label3: TLabel [1]
    Left = 1
    Top = 90
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object Label4: TLabel [2]
    Left = 0
    Top = 138
    Width = 65
    Height = 20
    AutoSize = False
    Caption = 'Call Centers:'
    WordWrap = True
  end
  inline RangeSelect: TOdRangeSelectFrame [3]
    Left = 30
    Top = 23
    Width = 264
    Height = 63
    TabOrder = 0
    inherited ToLabel: TLabel
      Left = 146
    end
    inherited DatesComboBox: TComboBox
      Width = 215
    end
    inherited FromDateEdit: TcxDateEdit
      Width = 91
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 168
      Width = 91
    end
  end
  object ManagersComboBox: TComboBox
    Left = 74
    Top = 87
    Width = 196
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object Arrival: TCheckBox
    Left = 74
    Top = 116
    Width = 217
    Height = 17
    Caption = 'Use Initial Arrival Date for first activity'
    TabOrder = 2
  end
  object CallCenterList: TCheckListBox
    Left = 73
    Top = 138
    Width = 275
    Height = 233
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    TabOrder = 3
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 25
    Top = 67
  end
end
