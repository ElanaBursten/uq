unit MessageDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, DB, DBISAMTb, DBCtrls,
  OleCtrls, SHDocVw, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxDataStorage, cxEdit, cxDBData,
  cxMaskEdit, cxCalendar, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxNavigator, cxCustomData, cxFilter, cxData, Mask;

type
  TMessageDetailForm = class(TEmbeddableForm)
    MessageRecipSource: TDataSource;
    ButtonPanel: TPanel;
    DoneButton: TButton;
    MessagePanel: TPanel;
    Label1: TLabel;
    Destination: TDBEdit;
    Label2: TLabel;
    MessageNumber: TDBEdit;
    Label3: TLabel;
    Subject: TDBEdit;
    MessageTable: TDBISAMTable;
    MessageSource: TDataSource;
    MessageRecipTable: TDBISAMTable;
    MessageActive: TDBCheckBox;
    pnlName: TPanel;
    MessageRecipGrid: TcxGrid;
    MessageRecipView: TcxGridDBTableView;
    MessageRecipViewmessage_dest_id1: TcxGridDBColumn;
    MessageRecipViewhas_acked1: TcxGridDBColumn;
    MessageRecipViewemp_id1: TcxGridDBColumn;
    MessageRecipViewrecipient_name1: TcxGridDBColumn;
    MessageRecipViewack_date1: TcxGridDBColumn;
    MessageRecipViewread_date1: TcxGridDBColumn;
    MessageRecipLevel: TcxGridLevel;
    Splitter1: TSplitter;
    pnlMsg: TPanel;
    MessageAckQry: TDBISAMQuery;
    Label6: TLabel;
    ShowDate: TDBEdit;
    ExpirationDate: TDBEdit;
    lblExpires: TLabel;
    lblMessagesSentCap: TLabel;
    lblNumSent: TLabel;
    Label4: TLabel;
    lblNumAck: TLabel;
    ScrollBoxMsgBody: TScrollBox;
    WebBrowserMsgBody: TWebBrowser;
    procedure DoneButtonClick(Sender: TObject);
    procedure MessageRecipTableBeforeOpen(DataSet: TDataSet);
    procedure MessageRecipTableCalcFields(DataSet: TDataSet);
    procedure WebBrowserMsgBodyBeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
  private
    FOnDone: TNotifyEvent;
    fMessageLoaded: Boolean; //QM-1020 EB Mesage Review
    procedure LoadMessage(const Body: string);
    procedure LoadAckStats(pMessageID: Integer);  //QM-1020 Adding Stats
  public
    property OnDone: TNotifyEvent read FOnDone write FOnDone;
    procedure ShowMessageDetail(MessageID: Integer);
    procedure ActivatingNow; override;  //QM-1020 EB message Review
    procedure LeavingNow; override;
    procedure OpenDataSets; override;
    procedure CloseDataSets; override;
  end;

implementation

{$R *.dfm}

uses OdBrowserUtils, OdMiscUtils, DMu, OdCxUtils, OdDbUtils, QMConst, LocalPermissionsDMu, LocalEmployeeDMu;

procedure TMessageDetailForm.DoneButtonClick(Sender: TObject);
begin
  if MessageTable.State = dsEdit then begin
    MessageTable.Post;
    DM.SetMessageActive(MessageTable.FieldByName('message_id').AsInteger, MessageTable.FieldByName('active').AsBoolean);
  end;
  if Assigned(FOnDOne) then
    FOnDone(Sender);
end;

procedure TMessageDetailForm.ShowMessageDetail(MessageID: Integer);
begin
  RefreshNow;
  MessageRecipGrid.BeginUpdate;
  try
    MessageTable.Locate('message_id', MessageID, []);
    Assert(MessageTable.FieldByName('message_id').AsInteger = MessageID,
      'Expecting message ID ' + IntToStr(MessageID) + '; got ' +
      MessageTable.FieldByName('message_id').AsString);
    MessageRecipView.DataController.Groups.FullExpand;
    FocusFirstGridRow(MessageRecipGrid, False);
    LoadMessage(MessageTable.FieldByName('body').AsString);
    MessageActive.Enabled := MessageTable.FieldByName('active').AsBoolean and
      (PermissionsDM.CanI(RightMessagesDeleteAny) or
      (MessageTable.FieldByName('from_emp_id').AsInteger = DM.UQState.EmpID));
    LoadAckStats(MessageID);   //QM-1020 EB  Message Ack Qry
  finally
    MessageRecipGrid.EndUpdate;
  end;
  fMessageLoaded := True; //QM-1020 EB Message Review
  Show;
end;

procedure TMessageDetailForm.WebBrowserMsgBodyBeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);
begin
  inherited;
  if not fMessageLoaded then exit;

    Cancel := True;
    if URL <> '' then begin
      BrowseURLSpecified(URL, 'chrome.exe'); //QM-1008 & QM-1014 EB HTML fix
    end;
end;

procedure TMessageDetailForm.MessageRecipTableBeforeOpen(DataSet: TDataSet);
begin
  AddLookupField('recipient_name', DataSet, 'emp_id', EmployeeDM.EmployeeLookup, 'emp_id',
    'short_name', 50, 'recipient_name', 'Recipient');
  AddCalculatedField('has_acked', DataSet, TStringField, 3);
end;

procedure TMessageDetailForm.MessageRecipTableCalcFields(DataSet: TDataSet);
begin
  if DataSet.FieldByName('ack_date').IsNull then
    DataSet.FieldByName('has_acked').Value := 'No'
  else
    DataSet.FieldByName('has_acked').Value := 'Yes';
end;

procedure TMessageDetailForm.ActivatingNow;
begin
  inherited;
  fMessageLoaded := False;
end;

procedure TMessageDetailForm.CloseDatasets;
begin
  MessageRecipTable.Close;
  MessageTable.Close;
  MessageAckQry.Close;
  inherited;
end;

procedure TMessageDetailForm.OpenDatasets;
begin
  inherited;
  OpenDataset(MessageTable);
  OpenDataset(MessageRecipTable);
end;

procedure TMessageDetailForm.LeavingNow;
begin
  CloseDatasets;
  inherited;
end;

procedure TMessageDetailForm.LoadAckStats(pMessageID: integer);

begin
  {QM-1020 EB Make message larger to view
   Moved panels around to increase size of message and make it easier to read}
   MessageAckQry.Close;
   MessageAckQry.ParamByName('Message_Id').AsInteger := pMessageID;
   MessageAckQry.Open;

     lblNumSent.Caption := IntToStr(MessageRecipTable.RecordCOunt);
     lblNumAck.Caption :=  IntToStr(MessageAckQry.fieldByName('MsgCount').AsInteger);


end;

procedure TMessageDetailForm.LoadMessage(const Body: string);
var
  HTML: TStringList;
begin
  HTML := TStringList.Create;
  try
    HTML.Add('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">');
    HTML.Add('<html>');
    HTML.Add('<head>');
    HTML.Add('<title></title>');
    HTML.Add('<STYLE TYPE="text/css">');
    HTML.Add('body {');
    HTML.Add('font-family: Tahoma, sans-serif;');
    HTML.Add('font-size: x-small ;');
    HTML.Add('}');
    HTML.Add('</STYLE>');
    HTML.Add('</head>');
    HTML.Add('<body>');
    HTML.Add(Body);
    HTML.Add('</body>');
    HTML.Add('</html>');
    LoadHtml(WebBrowserMsgBody, HTML.Text);
  finally
    FreeAndNil(HTML);
  end;
end;

end.
