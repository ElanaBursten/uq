unit LocalAttachmentDMu;

interface

uses
  Windows, Classes, SysUtils, DBISAMTb, DB, IdFtp, IdComponent, OdUqInternet,
  BaseAttachmentDMu, Variants, AttachmentUploadStatistics,
  SyncObjs;

type
  TDataRequestEvent = procedure(Sender: TObject; UploadFileItem: TUploadFileItem) of object;

  TLocalAttachment = class(TBaseAttachment)
    Attachment: TDBISAMTable;
    PendingUploads: TDBISAMQuery;
    TempQuery: TDBISAMQuery;
    AttachmentDB: TDBISAMDatabase;
    AttachmentSession: TDBISAMSession;
    UpdateAttachment: TDBISAMQuery;
    AWSCredentialsQry: TDBISAMQuery;
    AWSConstantsQry: TDBISAMQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure FTPProgressUpdate(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
  private
    FUploadInProgress: Boolean;  //QMANTWO-851 EB Sync Interrupting Uploads
    FPendingUploadCount: Integer;
    FPendingUploadSize: Integer;
    FUploadSessionStartDate: TDateTime;
    FCurrentFileSize: Integer;
    FCurrentFileName: string;
    FStopRequested: Boolean;
    FStopping: Boolean;
    FOnPendingFilesChanged: TNotifyEvent;
    FLongestFileWaitSeconds: Integer;
    FInBackground: Boolean;
    FAttachSummary: TAttachmentUploadSummary;
    FOnNeedDataFromServer: TDataRequestEvent;
    function GetNextFileToUpload(FileItem: TUploadFileItem): Boolean;
    function MarkFileUploaded(FileItem: TUploadFileItem; CheckUploadDate:boolean = true): Boolean;
    procedure MarkFileUploadAttempted(const AttachmentID: Integer);
    procedure RequestParentDataFromServer(FileItem: TUploadFileItem);
  protected
    function OpenQuery(const SQL: string): TDataSet; override;
    function GetDownloadLocationSQL: string; override;
    function GetUploadFileTypesSQL: string; override;
    function GetDuplicateAttachmentCheckSQL: string; override;
    function GetAttachmentParentSQL: string; override;
    procedure FlushDatabaseBuffers;
    procedure LockEnter; override;
    procedure LockExit; override;
  public
    constructor Create(const AppName: string; DatabaseName: string; const EmpID: Integer; const InBackground: Boolean); reintroduce;
    function AttachFileToRecord(const ForeignType, ForeignID: Integer; const FileName, Source: string; const CanAttachAnything: Boolean): string; override;
    function UploadPendingAttachments(const Location: Integer): Boolean;
    function DownloadAttachment(const AttachmentID: Integer): string; override;
    procedure RemoveAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string); override;
    procedure Stop;
    function HaveFilesToUpload: Boolean;
    function IsForeignIDInCache(const ForeignType, ForeignID: Integer): Boolean;
    function GetAttachmentDownloadFolder: string; override;
    function GetAttachmentFolder: string; override;
    function IsStopping: Boolean;
    function GetAWSCredentialsAndConstants(var pAttachmentData: TAttachmentData): boolean;

    property OnPendingFilesChanged: TNotifyEvent read FOnPendingFilesChanged write FOnPendingFilesChanged;
    property OnNeedDataFromServer: TDataRequestEvent read FOnNeedDataFromServer write FOnNeedDataFromServer;
    property PendingUploadCount: Integer read FPendingUploadCount;
    property PendingUploadSize: Integer read FPendingUploadSize;
    property LongestFileWaitSeconds: Integer read FLongestFileWaitSeconds;
    property UploadInProgress: Boolean read FUploadInProgress write FUploadInProgress;  //QMANTWO-851 EB Sync interrupting attachment uploads
  end;

var
  // Use this object to coordinate access to the Attachments table between threads.
  AttachmentDataCS: TCriticalSection;

implementation

uses
  OdMiscUtils, OdDbUtils, QMConst, DateUtils,
  ApplicationFiles, IdFTPCommon, JclSysInfo, Dialogs, DMu;

{$R *.dfm}

function TLocalAttachment.HaveFilesToUpload: Boolean;
begin
  LockEnter;
  try
    FAttachSummary.GetAttachmentUploadSummary;
  finally
    LockExit;
  end;
  FPendingUploadCount := FAttachSummary.PendingUploadCount;
  FPendingUploadSize := FAttachSummary.PendingUploadSize;
  FLongestFileWaitSeconds := FAttachSummary.LongestFileWaitSeconds;
  Result := PendingUploadCount > 0;
  if Assigned(FOnPendingFilesChanged) then
    FOnPendingFilesChanged(Self);
end;

function TLocalAttachment.UploadPendingAttachments(const Location: Integer): Boolean;
var
  ErrorList: TStringList;
  ParentDataRequestList: TStringList;
  FTP: TIdFTP;

  procedure LogError(const Msg: string);
  begin
    Log('ERROR:' + Msg);
    ErrorList.Add(' ' + Msg);
  end;

  procedure FreeFTP;
  begin
    try
      if Assigned(FTP) and (FTP.Connected) then
        FTP.Disconnect;
    finally
      FreeAndNil(FTP);
    end;
  end;

  procedure ValidateUploadFolder(const FTP: TIdFTP; const RootDir: string;
    const AttachmentData: TAttachmentData; const AttachID: Integer);
  var
    CurrentDir: string;
  begin
    CurrentDir := FTP.RetrieveCurrentDir;
    if not CurrentDirIsAttachmentFolder(FTP, RootDir, AttachmentData) then
      raise Exception.CreateFmt('%s is not the right upload folder for attachment %d.',
        [CurrentDir, AttachID]);
  end;

  procedure AddToParentDataRequestList(UploadFileItem: TUploadFileItem);
  var
    Key: string;
  begin
    // only want to request each ForeignType/ID combo once
    Key := Format('%d|%d', [UploadFileItem.ForeignType, UploadFileItem.ForeignID]);
    if ParentDataRequestList.IndexOf(Key) < 0 then
      ParentDataRequestList.AddObject(Key, UploadFileItem);
  end;

  procedure RequestMissingParentData;
  var
    i: Integer;
  begin
    Log('Requesting missing parent data from the server');
    for i := 0 to ParentDataRequestList.Count - 1 do
      RequestParentDataFromServer(ParentDataRequestList.Objects[i] as TUploadFileItem);
  end;

var
  AttachFile: string;
  OriginalFile: string;
  UploadedFile: Boolean;
  FileCount: Integer;
  FilePctComplete: Integer;
  UploadFileItem: TUploadFileItem;
  ProcessStep: string;
  AttachmentData : TAttachmentData;

  procedure SetUploadDateAndCleanUpLocalFile(CheckUploadDate:boolean);
  begin
//    UploadFileItem.UploadDate := Now;
    Log('Uploaded ' + OriginalFile + ' (stored as ' + AttachFile + ')');
    MinorProgress('Upload Complete', 100);
    ProcessStep := 'MarkFileUploaded';
    Log('Marking as uploaded ' + OriginalFile);
    if MarkFileUploaded(UploadFileItem,CheckUploadDate) then begin
      UploadedFile := True;
      Log('Deleting temporary files');
      ProcessStep := 'DeleteUploadedFile';
      OdDeleteFile(ExtractFilePath(AttachmentData.LocalFilename),    //QMANTWO-572  EB
        ExtractFileName(AttachmentData.LocalFilename), -1, False, False);
    end;
  end;

begin
  FStopRequested := False;
  FStopping := False;
  FUploadSessionStartDate := Now;
  FileCount := 0;
  Assert(Location > 0);
  FTP := nil;
  ErrorList := nil;
  ParentDataRequestList := nil;
  UploadedFile := False;
  Result := False;

  UploadFileItem := TUploadFileItem.Create;
  try
    ErrorList := TStringList.Create;
    ParentDataRequestList := TStringList.Create;
    FTP := TIdFtp.Create(nil);
    FTP.OnWork := FTPProgressUpdate;
    FTP.TransferType := ftBinary;
    ProcessStep := 'Setup';
    while HaveFilesToUpload and not FStopRequested do begin
      try
        UploadInProgress := True;    //QMANTWO-851 EB
        Assert(PendingUploadCount > 0, 'HaveFilesToUpload was true, but PendingUploadCount <=0');
        ProcessStep := 'GetNextFileToUpload';
        if not GetNextFileToUpload(UploadFileItem) then
          Break;

        ProcessStep := 'MarkFileUploadAttempted';
        MarkFileUploadAttempted(UploadFileItem.AttachmentID);
        Inc(FileCount);

        ProcessStep := 'ComputePctComplete';
        FilePctComplete := Trunc(FileCount / PendingUploadCount * 100);
        MajorProgress(Format('Preparing to upload %s', [UploadFileItem.OriginalFile]), FilePctComplete);

        ProcessStep := 'IsForeignIDInCache';
        if not IsForeignIDInCache(UploadFileItem.ForeignType, UploadFileItem.ForeignID) then begin
          if UploadFileItem.ForeignID < 0 then
            raise Exception.CreateFmt('Missing local only parent data type %d, parent id %d',
              [UploadFileItem.ForeignType, UploadFileItem.ForeignID]);
          ProcessStep := 'AddToParentDataRequestList';
          AddToParentDataRequestList(UploadFileItem);
          raise Exception.CreateFmt('Missing parent data for parent type %d, parent id %d',
            [UploadFileItem.ForeignType, UploadFileItem.ForeignID]);
        end;

{$IFNDEF FTPUPLOAD}      
        AttachmentData.OrigFilename := UploadFileItem.OriginalFile;
        AttachmentData.ServerFilename := UploadFileItem.AttachFile;
        AttachmentData.LocalFilename := UploadFileItem.LocalAttachFilename;
        AttachmentData.ForeignID := UploadFileItem.ForeignID;
        AttachmentData.ForeignTypeID := UploadFileItem.ForeignType;
        AttachmentData.EmpID := UploadFileItem.EmpID;
        AttachmentData.AttachmentID := UploadFileItem.AttachmentID;
//        If not GetAWSCredentials(AttachmentData.AWSKey, AttachmentData.AWSSecret) then   //AWS QMANTWO-432 QMANTWO-552 EB
//          raise Exception.Create('Unable to upload attachment due to expired Key. Please sync and try again.')
//        else
//          GetAWSConstants(AttachmentData.Constants);
         GetAWSCredentialsAndConstants(AttachmentData);  //QMANTWO-552 EB
         {If SecondaryDebugOn, then don't try and uplaod - EB}
         if (DM.UQState.SecondaryDebugOn = FALSE) then begin
           if not Upload_Attachment(AttachmentData, self) then
            raise Exception.Create(Format('AWS file %d/%s/%s failed to upload.',
              [AttachmentData.AttachmentID, AttachmentData.OrigFilename, AttachmentData.LocalFilename]));
         end;
        UploadFileItem.UploadDate := Now;
        SetUploadDateAndCleanUpLocalFile(false);
{$ELSE}
        ProcessStep := 'PrepareFTPServer';
        Log('Connecting to FTP server directory');
        PrepareFTPServer(FTP, Location, RootDir);
        Log('Resetting to FTP root');
        FTP.ChangeDir(RootDir);

        OriginalFile := UploadFileItem.OriginalFile;
        AttachFile := UploadFileItem.AttachFile;
        InProgressName := AttachFile + '.inprogress';
        LocalAttachFilename := UploadFileItem.LocalAttachFilename;
        ForeignType := UploadFileItem.ForeignType;
        ForeignID := UploadFileItem.ForeignID;

        AttachmentData.ServerFilename := InProgressName;

        if not FileExists(LocalAttachFilename) then
          raise Exception.CreateFmt('File no longer exists to upload: %s', [LocalAttachFilename]);

        Assert(FTP.RetrieveCurrentDir = RootDir, 'FTP connection is not set to the correct starting dir.');
        ProcessStep := 'GetUploadFolder';
        GetDirectoryNamesForAttachment(ForeignType, ForeignID, AttachmentData.ForeignTypeDir,
          AttachmentData.YearDir, AttachmentData.DateDir, AttachmentData.IDDir);
        ProcessStep := 'ChangeToOrMakeForeignTypeDir';
        ChangeToOrMakeDir(FTP, AttachmentData.ForeignTypeDir);
        ProcessStep := 'ChangeToOrMakeYearDir';
        ChangeToOrMakeDir(FTP, AttachmentData.YearDir);
        ProcessStep := 'ChangeToOrMakeForeignDateDir';
        ChangeToOrMakeDir(FTP, AttachmentData.DateDir);
        ProcessStep := 'ChangeToOrMakeIDDir';
        ChangeToOrMakeDir(FTP, AttachmentData.IDDir);
        ProcessStep := 'ValidateUploadFolder';
        ValidateUploadFolder(FTP, RootDir, AttachmentData, UploadFileItem.AttachmentID);

        // does the non-inprog file exist?
        // if so, delete it
        ProcessStep := 'CheckingForDuplicateFile';
        if FileExistsOnServer(FTP, AttachFile) then
          FTP.Delete(AttachFile);

        FCurrentFileName := OriginalFile;
        FCurrentFileSize := GetFileSize(LocalAttachFilename);
        MajorProgress('Uploading ' + OriginalFile, FilePctComplete);
        ProcessStep := 'UploadFile';

        FTP.Put(LocalAttachFilename, InProgressName);

        if FStopRequested then
          Break;

        FileCompareResult := CheckFileSizeOnFTPServer(FTP, FCurrentFileSize, AttachmentData, True);
        if FileCompareResult <> fcFilesHaveSameSize then
          raise Exception.Create(Format('Server file (%s) size (%d) and Local file (%s) size (%d) did not match.',
            [AttachFile, FTP.Size(InProgressName), LocalAttachFilename, FCurrentFileSize]));

        FTP.Rename(InProgressName, AttachFile);
        UploadFileItem.UploadDate := Now;
        SetUploadDateAndCleanUpLocalFile(true);
{$ENDIF}        
      except
        on E: Exception do
          LogError(E.Message + ' attachment_id = ' + IntToStr(UploadFileItem.AttachmentID) + ' during ' + ProcessStep);
      end;
      Sleep(0); // let any other threads do their work
    end;

    if not FStopRequested then begin
      if UploadedFile then begin
        Log('Total Files Uploaded: ' + IntToStr(FileCount));
        MajorProgress(DateTimeToStr(Now) + ' Upload done', 100);
        Result := True;
      end else
        Log('No files uploaded');

      if ParentDataRequestList.Count > 0 then
        RequestMissingParentData;
      if ErrorList.Count > 0 then
        DisplayTransientError(Format('QM background uploader problems detected:%s%s', [sLineBreak, ErrorList.Text]));
    end;

  finally
    FStopping := False;
    FreeAndNil(UploadFileItem);
    FreeAndNil(ErrorList);
    FreeAndNil(ParentDataRequestList);
    FreeFTP;
    if FStopRequested then
      Log(DateTimeToStr(Now) + ' Upload stopped');
    UploadInProgress := False;  //QMANTWO-851 EB
  end;
end;

function TLocalAttachment.AttachFileToRecord(const ForeignType, ForeignID: Integer;
  const FileName, Source: string; const CanAttachAnything: Boolean): string;
var
  NewFileName: string;
begin
  CanIAttachAnything := CanAttachAnything;
  NewFileName := PrepareFileForAttachment(ForeignType, ForeignID, FileName, Source);
  Result := NewFileName;

  LockEnter;
  try
    OpenDataSet(Attachment);
    Attachment.Insert;
    try
      AssignAttachmentFields(Attachment, Source, NewFileName, FileName, ForeignType, ForeignID);
      // assign temp id & mark as an Insert in the local data cache so syncing works
      Attachment.FieldByName('attachment_id').AsInteger := -Random(1000000) - 10000;
      Attachment.FieldByName('DeltaStatus').AsString := 'I';
      Attachment.FieldByName('need_to_upload').AsBoolean := True;
      Attachment.FieldByName('LocalKey').AsDateTime := Now;
      Attachment.FieldByName('LocalStringKey').AsString := Format('%s %s', [Source, GetLocalUserName]);
      Attachment.FieldByName('upload_machine_name').AsString := GetLocalComputerName;
      Attachment.Post;
    except
      on E: Exception do begin
        Attachment.Cancel;
        raise;
      end;
    end;
  finally
    LockExit;
  end;
  Assert(Attachment.FieldByName('attachment_id').AsInteger <> 0);
end;

procedure TLocalAttachment.Stop;
begin
  FStopRequested := True;
  FStopping := False;
end;

procedure TLocalAttachment.FlushDatabaseBuffers;
var
  i: Integer;
begin
  for i := 0 to ComponentCount-1 do begin
    if (Components[i] is TDBISAMDBDataSet) and TDBISAMDBDataSet(Components[i]).Active then
      TDBISAMDBDataSet(Components[i]).FlushBuffers;
  end;
end;

constructor TLocalAttachment.Create(const AppName: string; DatabaseName: string;
  const EmpID: Integer; const InBackground: Boolean);
var
  i: Integer;
begin
  inherited Create(nil);
  ApplicationName := AppName;
  AttachedByEmpID := EmpID;
  FInBackground := InBackground;
  FUploadInProgress := False;    //QMANTWO-851 EB Sync Interrupting Uploads

  if InBackground then begin     // Use the Attachment session
    Assert(DatabaseName = '', ' Do not specify a DB Name for background operation');
    DatabaseName := 'AttachmentDB';
    // Note that AutoSessionName automatically sets SessionName for all
    // components with the same Owner (on the same DM).
    AttachmentSession.PrivateDir := DBISAMTb.Session.PrivateDir;
    AttachmentSession.AutoSessionName := True;
    AttachmentDB.Directory := GetClientDataLocalDir;
    //Have to set the Attachment DB's DatabaseName here vs in the component or get error saying DB already exists. Refer to:
    //http://www.elevatesoft.com/forums?action=view&category=dbisam&id=dbisam_general&msg=61176&start=1&keywords=TDBISAMDatabase%20shared%20TDBISAMSession&searchbody=True&forum=DBISAM_Announce&forum=DBISAM_General&forum=DBISAM_SQL&forum=DBISAM_CS&forum=DBISAM_ODBC&forum=DBISAM_Beta&forum=DBISAM_Linux&forum=DBISAM_Binaries&forum=DBISAM_Suggestions&forum=DBISAM_ThirdParty&forum=DBISAM_Discussion#61176
    AttachmentDB.DatabaseName := DatabaseName;
  end else begin                 // Use main (default) session
    Assert(DatabaseName <> '', ' Must specify a DB Name for foreground operation');
    // AttachmentDB is not used for foreground operation
  end;

  for i := 0 to ComponentCount-1 do
    if Components[i] is TDBISAMDBDataSet then
      TDBISAMDBDataSet(Components[i]).DatabaseName := DatabaseName;

  FAttachSummary := TAttachmentUploadSummary.Create(Attachment.DatabaseName, Attachment.SessionName, EmpID);
end;

procedure TLocalAttachment.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FAttachSummary);
  Attachment.Close;
  TempQuery.Close;
end;

function TLocalAttachment.OpenQuery(const SQL: string): TDataSet;
begin
  // Note this uses a shared query component, so don't destroy the result or nest usage.
  TempQuery.Close;
  TempQuery.SQL.Text := SQL;
  TempQuery.Open;
  Result := TempQuery;
end;

function TLocalAttachment.GetDownloadLocationSQL: string;
begin
  Result := 'select * from upload_location where active and description = ''Download''';
end;

function TLocalAttachment.GetUploadFileTypesSQL: string;
begin
  Result := 'select * from upload_file_type where active';
end;

function TLocalAttachment.GetAttachmentParentSQL: string;
begin
  Result := 'select * from %s where %s = %d %s top 1';
end;




function TLocalAttachment.GetAWSCredentialsAndConstants(
  var pAttachmentData: TAttachmentData): boolean;
begin
  {Fill in the AWS Credentials}
  if AWSCredentialsQry.Active then
    AWSCredentialsQry.Close;

  AWSCredentialsQry.Open;
  if AWSCredentialsQry.RecordCount > 0 then begin
    pAttachmentData.AWSKey := AWSCredentialsQry.FieldByName('enc_access_key').AsWideString;
    pAttachmentData.AWSSecret := AWSCredentialsQry.FieldByName('enc_secret_key').AsWideString;
    if (pAttachmentData.AWSKey <> '') and (pAttachmentData.AWSSecret <> '') then
      Result := True
    else
      Result := False
  end
  else begin
    pAttachmentData.AWSKey := '';
    pAttachmentData.AWSSecret := '';
    Result := False;
    Exit;
  end;

  {Fill in the AWS Constants}
  if AWSConstantsQry.Active then
      AWSConstantsQry.Close;
   AWSConstantsQry.Open;
   if AWSConstantsQry.RecordCount > 0 then begin
     PAttachmentData.Constants.Host := AWSConstantsQry.FieldByName('host').AsString;
     PAttachmentData.Constants.RestURL :=  AWSConstantsQry.FieldByName('rest_url').AsString;
     PAttachmentData.Constants.Service := AWSConstantsQry.FieldByName('service').AsString;
     PAttachmentData.Constants.StagingBucket := AWSConstantsQry.FieldByName('bucket').AsString;
     PAttachmentData.Constants.StagingFolder := AWSConstantsQry.FieldByName('bucket_folder').AsString;
     PAttachmentData.Constants.Region := AWSConstantsQry.FieldByName('region').AsString;
     PAttachmentData.Constants.Algorithm := AWSConstantsQry.FieldByName('algorithm').AsString;
     PAttachmentData.Constants.SignedHeaders := AWSConstantsQry.FieldByName('signed_headers').AsString;
     PAttachmentData.Constants.ContentType := AWSConstantsQry.FieldByName('content_type').AsString;
     PAttachmentData.Constants.AcceptedValues := AWSConstantsQry.FieldByName('accepted_values').AsString;
     if PAttachmentData.Constants.Host <> '' then
       Result := True
     else
       Result := False
   end
   else
     Result := False;
end;

function TLocalAttachment.GetDuplicateAttachmentCheckSQL: string;
begin
  Result := 'select count(*) from attachment where foreign_type = %d ' +
    'and foreign_id = %d and orig_filename = %s and active';
end;

function TLocalAttachment.DownloadAttachment(const AttachmentID: Integer): string;
var
  DestDir: string;
  ForeignType: Integer;
  ForeignID: Integer;
  FileSize: Integer;
  ServerFileName: string;
  OriginalFileName: string;
  FileHash: string;
  lAttachmentData: TAttachmentData;
begin
  GetAttachmentDataFromAttachmentID(AttachmentID, ForeignType, ForeignID,
    FileSize, ServerFileName, OriginalFileName, FileHash);

  DestDir := GetAttachmentDownloadFolder;
  DestDir := DestDir + IncludeTrailingPathDelimiter(GetForeignTypeDirFromForeignType(ForeignType)) + IntToStr(ForeignID);
  if not DirectoryExists(DestDir) then
    OdForceDirectories(DestDir);
  Result := IncludeTrailingPathDelimiter(DestDir) + OriginalFileName;

	{$IFDEF USEFTP}
    Result := DownloadAttachmentToFile(AttachmentID, Result);
  {$ELSE}
    lAttachmentData.AttachmentID := AttachmentID;
    lAttachmentData.OrigFilename := OriginalFileName;
    lAttachmentData.ServerFilename := ServerFileName;
    If GetAWSCredentialsAndConstants(lAttachmentData) then begin
      DownloadAttachmentToFileAWS(lAttachmentData, Result);
      if (lAttachmentData.AWSRetMessage <> AWSSuccess) and (lAttachmentData.AWSRetMessage <> '') then
        MessageDlg(lAttachmentData.AWSRetMessage, mtError, [mbOK], 0);
    end;
  {$ENDIF}

end;

procedure TLocalAttachment.RemoveAttachment(const ForeignType, ForeignID: Integer; const FileName, Source: string);
const
  SQL = 'update attachment set active = False, DeltaStatus=''U'' where foreign_type=%d ' +
    'and foreign_id=%d and orig_filename = %s and coalesce(source, '''') = %s ' +
    'and active';
begin
  inherited;
  Assert(not TempQuery.Active, 'Unsafe reuse of TempQuery in InactivateExistingAttachment');
  LockEnter;
  try
    TempQuery.SQL.Text := Format(SQL, [ForeignType, ForeignID, QuotedStr(FileName), QuotedStr(Source)]);
    TempQuery.ExecSQL;
  finally
    LockExit;
  end;
end;

procedure TLocalAttachment.FTPProgressUpdate(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCount: Int64);
var
  PctComplete: Integer;
begin
  if FStopRequested and (not FStopping) and (ASender is TIdFTP) and TIdFTP(ASender).Connected then begin
    FStopping := True; // to prevent recursion
    Log('FTP upload interrupted by stop request');
    TIdFTP(ASender).Abort;
    Exit;
  end;

  if AWorkMode = wmWrite then begin
    if FCurrentFileSize > 0 then
      PctComplete := Trunc(AWorkCount / FCurrentFileSize * 100)
    else
      PctComplete := 0;
    MinorProgress(' Uploading ' + FCurrentFileName, PctComplete);
    Sleep(0); // let any other threads do their work
  end;
end;

function TLocalAttachment.GetNextFileToUpload(FileItem: TUploadFileItem): Boolean;
begin
  LockEnter;
  try
    // find the next attachment to upload that hasn't already been tried in this cycle
    PendingUploads.ParamByName('emp_id').Value := AttachedByEmpID;
    PendingUploads.ParamByName('upload_start_date').Value := FUploadSessionStartDate;
    PendingUploads.ParamByName('ignore_before_date').AsDateTime := Today - 60;
    PendingUploads.Open;
    Result := not PendingUploads.IsEmpty;
    if Result then begin
      FileItem.EmpID := AttachedByEmpID;
      FileItem.AttachmentID := PendingUploads.FieldByName('attachment_id').AsInteger;
      FileItem.OriginalFile := PendingUploads.FieldByName('orig_filename').AsString;
      FileItem.AttachFile := PendingUploads.FieldByName('filename').AsString;
      FileItem.LocalAttachFilename := IncludeTrailingPathDelimiter(GetAttachmentFolder) + FileItem.AttachFile;
      FileItem.ForeignType := PendingUploads.FieldByName('foreign_type').AsInteger;
      FileItem.ForeignID := PendingUploads.FieldByName('foreign_id').AsInteger;
      FileItem.UploadDate := Null;
      Assert(FileItem.ForeignType > 0, 'Invalid foreign type in attachment record: ' + IntToStr(FileItem.ForeignType));
      Assert(FileItem.ForeignID > 0, 'Invalid foreign ID in attachment record: ' + IntToStr(FileItem.ForeignID));
    end;
  finally
    PendingUploads.Close;
    LockExit;
  end;
end;

procedure TLocalAttachment.MarkFileUploadAttempted(const AttachmentID: Integer);
const
  // upload_try_date is only a client-side field, so no need to update DeltaStatus
  Update = 'update attachment set upload_try_date=:upload_try_date where attachment_id=:attachment_id';
begin
  LockEnter;
  try
    UpdateAttachment.SQL.Text := Update;
    UpdateAttachment.ParamByName('attachment_id').Value := AttachmentID;
    UpdateAttachment.ParamByName('upload_try_date').AsDateTime := FUploadSessionStartDate;
    UpdateAttachment.ExecSQL;
    Assert(UpdateAttachment.RowsAffected = 1, 'No attachment with id = ' + IntToStr(AttachmentID));
  finally
    LockExit;
  end;
end;

function TLocalAttachment.MarkFileUploaded(FileItem: TUploadFileItem;CheckUploadDate:boolean): Boolean;
const
  Update = 'update attachment set upload_date = :upload_date, ' +
    'background_upload = :background_upload, need_to_upload = False, ' +
    'upload_machine_name = :upload_machine_name, DeltaStatus = ''U'' ' +
    'where attachment_id = :attachment_id';
  UpdateWithoutUploaddate = 'update attachment set upload_date = NULL, ' +
    'background_upload = :background_upload, need_to_upload = False, ' +
    'upload_machine_name = :upload_machine_name, DeltaStatus = ''U'' ' +
    'where attachment_id = :attachment_id';
begin
  Result := False;
  if CheckUploadDate and (FileItem.UploadDate = Null) then
    Exit;

  LockEnter;
  try
    // If a sync starts & completes while an upload is in progress, this update fails & the upload retried
    if FileItem.UploadDate = Null then
      UpdateAttachment.SQL.Text := UpdateWithoutUploaddate
    else
      UpdateAttachment.SQL.Text := Update;
    UpdateAttachment.ParamByName('attachment_id').Value := FileItem.AttachmentID;
    if FileItem.UploadDate <> null then
      UpdateAttachment.ParamByName('upload_date').AsDateTime := FileItem.UploadDate;
    UpdateAttachment.ParamByName('background_upload').Value := FInBackground;
    UpdateAttachment.ParamByName('upload_machine_name').Value :=
      GetLocalComputerName;
    UpdateAttachment.ExecSQL;
    Assert(UpdateAttachment.RowsAffected < 2, 'Multiple attachments found with id = ' + IntToStr(FileItem.AttachmentID));
    Result := (UpdateAttachment.RowsAffected = 1);
  finally
    LockExit;
  end;
end;

function TLocalAttachment.IsForeignIDInCache(const ForeignType, ForeignID: Integer): Boolean;
const
  Select = 'select count(*) N from %s where %s = %d';
var
  Table: string;
  IDField: string;
  ParentData: TDataSet;
begin
  case ForeignType of
    qmftTicket: begin
      Table := 'ticket';
      IDField := 'ticket_id';
    end;
    qmftDamage: begin
      Table := 'damage';
      IDField := 'damage_id';
    end;
    qmft3rdParty: begin
      Table := 'damage_third_party';
      IDField := 'third_party_id';
    end;
    qmftLitigation: begin
      Table := 'damage_litigation';
      IDField := 'litigation_id';
    end;
    qmftWorkOrder: begin
      Table := 'work_order';
      IDField := 'wo_id';
    end;
  else
    raise Exception.CreateFmt('Unexpected ForeignType %d in IsForeignIDInCache', [ForeignType]);
  end;
  LockEnter;
  try
    ParentData := OpenQuery(Format(Select, [Table, IDField, ForeignID]));
  finally
    LockExit;
  end;
  try
    Result := ParentData.FieldByName('N').AsInteger > 0;
  finally
    ParentData.Close;
  end;
end;

function TLocalAttachment.IsStopping: Boolean;
begin
  Result := FStopping or FStopRequested;
end;

procedure TLocalAttachment.RequestParentDataFromServer(FileItem: TUploadFileItem);
begin
  Assert(Assigned(FileItem));
  if Assigned(FOnNeedDataFromServer) then
    FOnNeedDataFromServer(Self, FileItem);
end;

function TLocalAttachment.GetAttachmentDownloadFolder: string;
begin
  Result := GetClientAttachmentsDownloadDir(ApplicationName);
end;

function TLocalAttachment.GetAttachmentFolder: string;
begin
  Result := GetClientAttachmentsLocalDir;
  if not DirectoryExists(Result) then
    if not CreateDir(Result) then
      raise Exception.Create('Unable to create directory: ' + Result);
end;

procedure TLocalAttachment.LockEnter;
begin
  inherited;
  AttachmentDataCS.Enter;
end;

procedure TLocalAttachment.LockExit;
begin
  inherited;
  AttachmentDataCS.Leave;
end;

initialization
  AttachmentDataCS := TCriticalSection.Create;

finalization
  FreeAndNil(AttachmentDataCS);

end.


