unit AssetManagement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  OdEmbeddable, StdCtrls, ExtCtrls, ComCtrls, DB, DBISAMTb, 
  ActnList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, 
  cxDataStorage, cxEdit, cxDBData, cxDropDownEdit, cxMaskEdit, cxCalendar,
  cxCurrencyEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxClasses, cxGridLevel, cxGrid,
  cxNavigator, cxCustomData, cxFilter, cxData;

type
  TAssetManagementForm = class(TEmbeddableForm)
    Splitter: TSplitter;
    AssetPanel: TPanel;
    ActionPanel: TPanel;
    AddAssetButton: TButton;
    RemoveAssetButton: TButton;
    AssetSource: TDataSource;
    AssetQuery: TDBISAMQuery;
    SaveChangesButton: TButton;
    EmpList: TListBox;
    AssetActions: TActionList;
    AddAssetAction: TAction;
    RemoveAssetAction: TAction;
    SaveChangesAction: TAction;
    AssetGrid: TcxGrid;
    AssetGridLevel: TcxGridLevel;
    AssetGridView: TcxGridDBTableView;
    ColAssetCode: TcxGridDBColumn;
    ColAssetNumber: TcxGridDBColumn;
    ColAssetID: TcxGridDBColumn;
    ColModifiedDate: TcxGridDBColumn;
    ColSerialNum: TcxGridDBColumn;
    ColCondition: TcxGridDBColumn;
    ColDescription: TcxGridDBColumn;
    ColSolomonProfitCenter: TcxGridDBColumn;
    ColComment: TcxGridDBColumn;
    ColCost: TcxGridDBColumn;
    procedure EmpListClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AssetActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure AddAssetActionExecute(Sender: TObject);
    procedure RemoveAssetActionExecute(Sender: TObject);
    procedure SaveChangesActionExecute(Sender: TObject);
    procedure EditAssetActionExecute(Sender: TObject);
    procedure AssetQueryAfterEdit(DataSet: TDataSet);
    procedure EmpListDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure EmpListDragDrop(Sender, Source: TObject; X, Y: Integer);
    function GetEmpIDAtListPoint(X, Y: Integer): Integer;
    function IsValidReassignEmpID(EmpID: Integer): Boolean;
  private
    FSelectedEmpID: Integer;
    FModified: Boolean;
    FCanEditAllAddAndDelete: Boolean;
    function SelectedEmpID: Integer;
    function RecordAssetCode: string;
    function RecordAssetNumber: string;
    function HaveSelectedAsset: Boolean;
    procedure EnsureSelectedAsset;
    procedure GetAndShowAssetsForEmployee(EmpID: Integer);
    procedure MarkModified(Value: Boolean = True);
    procedure PromptToSave;
    procedure ValidateData;
    procedure PopulateEmployeeList;
    procedure UpdateAssets;
  public
    procedure ActivatingNow; override;
    procedure RefreshNow; override;
  end;

implementation

{$R *.dfm}

uses DMu, Dialogs, OdHourglass, OdExceptions, QMConst, OdMiscUtils, OdVclUtils,
  OdCxUtils, LocalPermissionsDMu, LocalEmployeeDMu;

{ TAssetManagementForm }

procedure TAssetManagementForm.ActivatingNow;
begin
  RefreshNow;
  inherited;
end;

procedure TAssetManagementForm.RefreshNow;
begin
  inherited;
  FSelectedEmpID := -1;
  PopulateEmployeeList;
  AssetQuery.Close;

  DM.GetAssetTypes(GetGridComboItems(ColAssetCode));
  DM.GetRefCodeList('acond', GetGridComboItems(ColCondition));
  DM.SolomonProfitCenterList(GetGridComboItems(ColSolomonProfitCenter));
  GetGridComboItems(ColSolomonProfitCenter).Insert(0, '');
  FCanEditAllAddAndDelete := PermissionsDM.CanI(RightEditAssets);
  ColAssetCode.Options.Editing := FCanEditAllAddAndDelete;
  ColAssetNumber.Options.Editing := FCanEditAllAddAndDelete;
  ColAssetID.Options.Editing := FCanEditAllAddAndDelete;
  ColModifiedDate.Options.Editing := FCanEditAllAddAndDelete;
  ColSerialNum.Options.Editing := FCanEditAllAddAndDelete;
  ColDescription.Options.Editing := FCanEditAllAddAndDelete;
  ColSolomonProfitCenter.Options.Editing := FCanEditAllAddAndDelete;
  ColCost.Options.Editing := FCanEditAllAddAndDelete;
  // Anyone that can see them can edit these two fields
  ColCondition.Options.Editing := True;
  ColComment.Options.Editing := True;
end;

procedure TAssetManagementForm.EmpListClick(Sender: TObject);
begin
  UpdateAssets;
end;

procedure TAssetManagementForm.FormCreate(Sender: TObject);
begin
  inherited;
  FSelectedEmpID := -1;
end;

procedure TAssetManagementForm.AssetActionsUpdate(Action: TBasicAction; var Handled: Boolean);
var
  HaveSelectedEmp: Boolean;
begin
  HaveSelectedEmp := FSelectedEmpID > 0;
  AddAssetAction.Enabled := HaveSelectedEmp and FCanEditAllAddAndDelete;
  RemoveAssetAction.Enabled := HaveSelectedAsset and HaveSelectedEmp and FCanEditAllAddAndDelete;
  SaveChangesAction.Enabled := HaveSelectedEmp and FModified;
  AssetGrid.Enabled := HaveSelectedAsset;
end;

procedure TAssetManagementForm.AddAssetActionExecute(Sender: TObject);
begin
  AssetQuery.Insert;
  AssetQuery.FieldByName('emp_id').AsInteger := SelectedEmpID;
  AssetQuery.FieldByName('modified_date').AsDateTime := Now;
  AssetQuery.Post;
  MarkModified;
end;

procedure TAssetManagementForm.RemoveAssetActionExecute(Sender: TObject);
var
  Question: string;
begin
  if RecordAssetCode = '' then
    AssetQuery.Delete
  else begin
    Question := Format('Remove "%s" asset number "%s"?', [RecordAssetCode, RecordAssetNumber]);
    if (MessageDlg(Question, mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
      MarkModified;
      AssetQuery.Delete;
    end;
  end;
end;

procedure TAssetManagementForm.SaveChangesActionExecute(Sender: TObject);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if AssetQuery.State in dsEditModes then
    AssetQuery.UpdateRecord;
  ValidateData;
  DM.CallingServiceName('SaveEmployeeAssets2');
  DM.Engine.SaveAssetsForEmployee(SelectedEmpID, AssetQuery);
  MarkModified(False);
end;

function TAssetManagementForm.SelectedEmpID: Integer;
begin
  if FSelectedEmpID < 0 then
    raise EOdEntryRequired.Create('Please select an employee');
  Result := FSelectedEmpID;
end;

procedure TAssetManagementForm.EditAssetActionExecute(Sender: TObject);
var
  AssetNumber: string;
  SelectedAssetCode: string;
begin
  if not HaveSelectedAsset then
    Exit;
  AssetNumber := AssetQuery.FieldByName('asset_number').AsString;
  SelectedAssetCode := AssetQuery.FieldByName('asset_code').AsString;
  if InputQuery('Edit Asset', SelectedAssetCode + ' Asset Number', AssetNumber) then begin
    MarkModified;
    AssetQuery.Edit;
    AssetQuery.FieldByName('asset_number').AsString := AssetNumber;
    AssetQuery.FieldByName('modified_date').AsDateTime := Now;
    AssetQuery.Post;
  end;
end;

function TAssetManagementForm.RecordAssetCode: string;
begin
  EnsureSelectedAsset;
  Result := AssetQuery.FieldByName('asset_code').AsString;
end;

function TAssetManagementForm.RecordAssetNumber: string;
begin
  EnsureSelectedAsset;
  Result := AssetQuery.FieldByName('asset_number').AsString;
end;

function TAssetManagementForm.HaveSelectedAsset: Boolean;
begin
  Result := False;
  if AssetQuery.Active then
    Result := AssetQuery.RecordCount > 0;
end;

procedure TAssetManagementForm.EnsureSelectedAsset;
begin
  if not HaveSelectedAsset then
    raise EOdEntryRequired.Create('Please select an asset');
end;

procedure TAssetManagementForm.GetAndShowAssetsForEmployee(EmpID: Integer);
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  PromptToSave;
  FSelectedEmpID := EmpID;
  DM.Engine.GetAssetsFromServer(EmpID);
  AssetQuery.DisableControls;
  try
    AssetQuery.ParamByName('EmpID').AsInteger := FSelectedEmpID;
    AssetQuery.Close;
    AssetQuery.Open;
    MarkModified(False);
  finally
    AssetQuery.EnableControls;
  end;
end;

procedure TAssetManagementForm.MarkModified(Value: Boolean);
begin
  FModified := Value;
end;

procedure TAssetManagementForm.PromptToSave;
begin
  if FModified then
    if (MessageDlg('Save changes?', mtWarning, [mbYes,mbNo], 0) = mrYes) then
      SaveChangesAction.Execute;
end;

procedure TAssetManagementForm.ValidateData;
var
  CurrentCode: string;
begin
  AssetQuery.DisableControls;
  try
    AssetQuery.First;
    while not AssetQuery.Eof do begin
      CurrentCode := AssetQuery.FieldByName('asset_code').AsString;
      if Trim(CurrentCode) = '' then
        raise EOdEntryRequired.Create('An asset code is required for each asset assignment');
      if Trim(AssetQuery.FieldByName('asset_number').AsString) = '' then
        raise EOdEntryRequired.Create('An asset number is required for each asset assignment');
      AssetQuery.Next;
    end;
  finally
    AssetQuery.EnableControls;
  end;
end;

procedure TAssetManagementForm.AssetQueryAfterEdit(DataSet: TDataSet);
begin
  MarkModified;
end;

procedure TAssetManagementForm.PopulateEmployeeList;
const
  GroupSep = '--------------------';
var
  EmpIDs: TStringList;
  Emps: TStringList;
  i: Integer;
begin
  EmpList.Items.BeginUpdate;
  try
    EmpList.Clear;
    Emps := nil;
    EmpIDs := TStringList.Create;
    try
      Emps := TStringList.Create;
      PermissionsDM.GetLimitationList(RightManageAssets, EmpIDs);
      if EmpIDs.Count = 0 then
        EmpIDs.Add(IntToStr(DM.UQState.EmpID));
      for i := 0 to EmpIDs.Count - 1 do begin
        EmployeeDM.EmployeeList(Emps, StrToIntDef(EmpIDs[i], 0));
        if Emps.Count > 0 then
          Emps.AddObject(GroupSep, TObject(0));
        ConcatStringLists(EmpList.Items, Emps);
      end;
    finally
      FreeAndNil(EmpIDs);
      FreeAndNil(Emps);
    end;
    if (EmpList.Count > 0) and (EmpList.Items[EmpList.Count - 1] = GroupSep) then
      EmpList.Items.Delete(EmpList.Items.Count - 1);
  finally
    EmpList.Items.EndUpdate;
  end;
end;

procedure TAssetManagementForm.EmpListDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := IsValidReassignEmpID(GetEmpIDAtListPoint(X, Y));
end;

procedure TAssetManagementForm.EmpListDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  AssetNum: string;
  AssetCode: string;
  AssetID: Integer;
  DestEmpID: Integer;
  Cursor: IInterface;
  SourceControl: TControl;
  FRecord: TcxCustomGridRecord;
begin
  Cursor := ShowHourGlass;

  if Source is TDragControlObject then begin
    DestEmpID := GetEmpIDAtListPoint(X, Y);
    if IsValidReassignEmpID(DestEmpID) then begin
      if SaveChangesAction.Enabled then
        SaveChangesAction.Execute;
      SourceControl := TDragControlObject(Source).Control;
      FRecord := TcxCustomGridTableView(TcxGridSite(SourceControl).GridView).Controller.FocusedRecord;
      AssetNum := FRecord.Values[ColAssetNumber.Index];
      AssetCode := FRecord.Values[ColAssetCode.Index];
      AssetID :=  FRecord.Values[ColAssetID.Index];
      DM.CallingServiceName('ReassignAssets');
      DM.Engine.ReassignAsset(AssetNum, AssetCode, AssetID, DestEmpID);
      UpdateAssets;
    end
    else
      MessageBeep(0);
  end
  else
    MessageBeep(0);
end;

function TAssetManagementForm.GetEmpIDAtListPoint(X, Y: Integer): Integer;
begin
  Result := EmpList.ItemAtPos(Point(X, Y), True);
  if Result > -1 then
    Result := GetListBoxObjectInteger(EmpList, Result);
end;

function TAssetManagementForm.IsValidReassignEmpID(EmpID: Integer): Boolean;
begin
   Result := (EmpID > -1) and (EmpID <> SelectedEmpID);
end;

procedure TAssetManagementForm.UpdateAssets;
var
  Cursor: IInterface;
begin
  Cursor := ShowHourGlass;
  if EmpList.ItemIndex < 0 then
    Exit;
  GetAndShowAssetsForEmployee(Integer(EmpList.Items.Objects[EmpList.ItemIndex]));
end;

end.
