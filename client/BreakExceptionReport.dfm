inherited BreakExceptionReportForm: TBreakExceptionReportForm
  Left = 460
  Top = 204
  Caption = 'Breaks Less Than 30 Minutes'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 13
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 40
    Width = 89
    Height = 13
    Caption = 'Work Date Range:'
  end
  object Label2: TLabel [2]
    Left = 0
    Top = 126
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object Label3: TLabel [3]
    Left = 0
    Top = 154
    Width = 94
    Height = 39
    Caption = 'Exclude Employees that have these Time Rules:'
    WordWrap = True
  end
  object ManagerCombo: TComboBox [4]
    Left = 104
    Top = 10
    Width = 222
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  inline WorkDateRange: TOdRangeSelectFrame [5]
    Left = 48
    Top = 54
    Width = 278
    Height = 53
    TabOrder = 1
    inherited FromLabel: TLabel
      Left = 7
      Top = 10
    end
    inherited ToLabel: TLabel
      Left = 148
      Top = 10
    end
    inherited DatesLabel: TLabel
      Left = 7
      Top = 35
    end
    inherited DatesComboBox: TComboBox
      Left = 56
      Top = 32
      Width = 200
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 56
      Top = 7
      Width = 88
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 164
      Top = 7
      Width = 92
    end
  end
  object EmployeeStatusCombo: TComboBox
    Left = 104
    Top = 123
    Width = 198
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object TimeRulesCheckListBox: TCheckListBox
    Left = 104
    Top = 154
    Width = 198
    Height = 111
    ItemHeight = 13
    TabOrder = 3
  end
  object SelectAllButton: TButton
    Left = 121
    Top = 271
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 4
    OnClick = SelectAllButtonClick
  end
  object SelectNoneButton: TButton
    Left = 205
    Top = 271
    Width = 75
    Height = 25
    Caption = 'Select None'
    TabOrder = 5
    OnClick = SelectNoneButtonClick
  end
end
