unit BaseExpandableGridFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseExpandableDataFrame, StdCtrls, ExtCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxClasses,
  cxCustomData, cxGridCustomTableView, cxGridLevel, cxGrid, DB, DMu, cxVariants,
  QMConst, cxDataStorage, cxEdit, cxDBData,
  cxTextEdit, cxCheckBox, cxDropDownEdit, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,dxCore;

type
  TExpandableGridFrameBase = class(TExpandableDataFrameBase)
    BaseGrid: TcxGrid;
    BaseGridLevelDetail: TcxGridLevel;
    BaseGridLevelSummary: TcxGridLevel;
    CountLabelSummary: TLabel;
    CountLabelDataSummary: TLabel;
    OpenLabelSummary: TLabel;
    OpenLabelDataSummary: TLabel;
    procedure GridCompare(ADataController: TcxCustomDataController; ARecordIndex1,
      ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);
    procedure GridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure GridDblClick(Sender: TObject);
  private
  protected
    FViewIndex: Integer;
    FOnItemSelected: TItemSelectedEvent;
    function ItemIDColumn: TcxGridColumn; virtual; abstract;
    procedure SetupColumns; virtual; abstract;
    function GetItemIDsForExportString(Dataset: TDataset;
      ItemFieldName: string; CurrentSelectedItemID: integer): string;
    procedure SetExportAll(DataSet: TDataSet;
      AView: TcxCustomGridTableView; Checked: Boolean);
    function GetCurrentItemID(var ItemID: Integer): Boolean; virtual;
    function SelectedItemID: Integer;
    function FocusedGridRecord: TcxCustomGridRecord;
    procedure SetGridEvents; virtual; abstract; //necessary to programmatically hook common events in inherited frames or they won't fire
  public
    property OnItemSelected: TItemSelectedEvent read FOnItemSelected write FOnItemSelected;
    procedure Setup(EntityDescription: string; ExpandHandler: TNotifyEvent); override;
    function ExportMyItems: string; virtual; abstract;
    procedure SortGridByFields; virtual; abstract;
    procedure SetAllExportBoxes(Checked: Boolean); virtual; abstract;
    procedure RefreshData; virtual;
    procedure RefreshCaptionCounts; virtual;
    procedure ChangeView(ViewIndex: Integer); virtual;
  end;

var
  ExpandableGridFrameBase: TExpandableGridFrameBase;

implementation

{$R *.dfm}

uses OdHourglass, OdDBUtils, OdMiscUtils;

{ TExpandableGridFrameBase }

procedure TExpandableGridFrameBase.Setup(EntityDescription: string;
  ExpandHandler: TNotifyEvent);
begin
  inherited;
  SetGridEvents;
end;

procedure TExpandableGridFrameBase.GridCompare(ADataController: TcxCustomDataController;
  ARecordIndex1, ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant;
  var Compare: Integer);
const
  SortValueA: array [Boolean] of Integer = (-1, 1);
begin
  if VarType(V1) = varNull then begin
    if VarType(V2) = varNull then
      Compare := 0
    else
      Compare := SortValueA[ADataController.GetItemSortOrder(AItemIndex) = soAscending];
  end else begin
    if VarType(V2) = varNull then
      Compare := SortValueA[ADataController.GetItemSortOrder(AItemIndex) = soDescending]
    else
      Compare := VarCompare(V1, V2);
  end;
end;

function TExpandableGridFrameBase.GetItemIDsForExportString(Dataset: TDataset;
  ItemFieldName: string; CurrentSelectedItemID: integer): string;
var
  Cursor: IInterface;
begin
  Result := '';
  Cursor := ShowHourGlass;
  Dataset.DisableControls;
  try
  {EB - Will only export the tickets where checkbox has been checked}
    Dataset.Filtered := False;
    Dataset.Filter := 'export = True';
    Dataset.Filtered := True;

    Result := GetDataSetFieldPlainString(Dataset, ItemFieldName, -1, ',');

    Dataset.Filtered := False;
  finally
    Dataset.Locate(ItemFieldName,CurrentSelectedItemID,[]);
    Dataset.EnableControls;
  end;
end;

procedure TExpandableGridFrameBase.SetExportAll(DataSet: TDataSet; AView: TcxCustomGridTableView; Checked: Boolean);
var
  Cursor: IInterface;
  CurrentFocusedRecord: Integer;
begin
  Cursor := ShowHourGlass;

  CurrentFocusedRecord := AView.DataController.FocusedRecordIndex;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do begin
      DataSet.Edit;
      DataSet.FieldByName('Export').AsBoolean := Checked;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.EnableControls;
    AView.DataController.FocusedRecordIndex := CurrentFocusedRecord;
  end;
end;

procedure TExpandableGridFrameBase.ChangeView(ViewIndex: Integer);
begin
  FViewIndex := ViewIndex;
  BaseGrid.Levels[ViewIndex].MakeVisible;
  SetupColumns;
end;

procedure TExpandableGridFrameBase.GridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
 //implement in inherited class
end;

procedure TExpandableGridFrameBase.GridDblClick(Sender: TObject);
var
  ItemID: Integer;
begin
  if Assigned(FOnItemSelected) then
    if GetCurrentItemID(ItemID) then
      FOnItemSelected(Self, ItemID);
end;

function TExpandableGridFrameBase.GetCurrentItemID(var ItemID: Integer): Boolean;
begin
  Result := True;
  ItemID := SelectedItemID;
  if ItemID = InvalidID then
    Result := False;
end;

function TExpandableGridFrameBase.FocusedGridRecord: TcxCustomGridRecord;
var
  View : TcxGridTableView;
begin
  Result := nil;
  View := TcxGridTableView(BaseGrid.FocusedView);
  if Assigned(View) then
    Result := View.Controller.FocusedRecord;
end;

function TExpandableGridFrameBase.SelectedItemID: Integer;
begin
  Result := InvalidID;
  if (FocusedGridRecord is TcxGridGroupRow) then
    Exit;

  if FocusedGridRecord <> nil then begin
    if not VarIsNull(FocusedGridRecord.Values[ItemIDColumn.Index]) then
      Result := FocusedGridRecord.Values[ItemIDColumn.Index];
  end;
end;

procedure TExpandableGridFrameBase.RefreshData;
begin
  RefreshCaptionCounts;
end;

procedure TExpandableGridFrameBase.RefreshCaptionCounts;
begin
  DM.UpdateCounts;
end;

end.
