inherited BillingAdjustmentCriteria: TBillingAdjustmentCriteria
  Caption = 'Billing Adjustments'
  ClientHeight = 203
  ClientWidth = 733
  Font.Charset = ANSI_CHARSET
  Font.Name = 'Tahoma'
  PixelsPerInch = 96
  TextHeight = 13
  inherited RecordsLabel: TLabel
    Left = 439
    Top = 175
    Width = 113
  end
  object CallCenterLabel: TLabel [1]
    Left = 311
    Top = 119
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Call Centers:'
    Visible = False
  end
  object CustomerLabel: TLabel [2]
    Left = 46
    Top = 11
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Customer:'
  end
  object TypeLabel: TLabel [3]
    Left = 68
    Top = 63
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Type:'
  end
  object DescriptionLabel: TLabel [4]
    Left = 39
    Top = 91
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Description:'
  end
  object AmountLabel: TLabel [5]
    Left = 55
    Top = 119
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Amount:'
  end
  object AdjustmentDateLabel: TLabel [6]
    Left = 311
    Top = 72
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Added Date'
  end
  object AddedDateLabel: TLabel [7]
    Left = 311
    Top = 11
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Adjustment Date'
  end
  object Label1: TLabel [8]
    Left = 25
    Top = 37
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Which Invoice:'
  end
  object GLCodeLabel: TLabel [9]
    Left = 52
    Top = 145
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'GL Code:'
  end
  inherited SearchButton: TButton
    Left = 107
    Top = 169
    TabOrder = 9
  end
  inherited CancelButton: TButton
    Left = 427
    Top = 169
    TabOrder = 12
  end
  inherited CopyButton: TButton
    Left = 187
    Top = 169
    TabOrder = 10
  end
  inherited ExportButton: TButton
    Left = 267
    Top = 169
    TabOrder = 11
  end
  inherited PrintButton: TButton
    Left = 428
    Top = 139
    Width = 74
    TabOrder = 14
  end
  inherited ClearButton: TButton
    Left = 347
    Top = 169
    TabOrder = 15
  end
  inline AdjustmentDate: TOdRangeSelectFrame
    Left = 396
    Top = -1
    Width = 248
    Height = 67
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  inline AddedDate: TOdRangeSelectFrame
    Left = 395
    Top = 60
    Width = 248
    Height = 56
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object CallCenters: TComboBox
    Left = 385
    Top = 115
    Width = 29
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 0
    Visible = False
  end
  object Description: TEdit
    Left = 108
    Top = 87
    Width = 175
    Height = 21
    TabOrder = 4
  end
  object Amount: TEdit
    Left = 108
    Top = 115
    Width = 175
    Height = 21
    TabOrder = 5
  end
  object Customer: TComboBox
    Left = 108
    Top = 7
    Width = 175
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 1
  end
  object AdjustmentType: TComboBox
    Left = 108
    Top = 61
    Width = 175
    Height = 21
    Style = csDropDownList
    DropDownCount = 16
    ItemHeight = 13
    TabOrder = 3
  end
  object WidthPanel: TPanel
    Left = 664
    Top = 39
    Width = 69
    Height = 41
    BevelOuter = bvNone
    TabOrder = 13
  end
  object WhichInvoice: TComboBox
    Left = 108
    Top = 33
    Width = 175
    Height = 21
    ItemHeight = 13
    TabOrder = 2
  end
  object GLCode: TEdit
    Left = 108
    Top = 141
    Width = 175
    Height = 21
    TabOrder = 6
  end
end
