unit Timesheet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdEmbeddable, StdCtrls, ExtCtrls, ActnList, DB, Buttons, HoursCalc,
	QMConst, BaseTimesheetDay, TimesheetDay, TimeClockDay, 
  TimeSheetCalloutCompletion, //TimeSheetCalloutAdd,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxTextEdit, cxHyperLinkEdit;

const
  UM_FOCUSDAY = WM_USER + 85;

type
  TSickPTOBal = record   {QM-579 Sick and PTO values  SR/EB}
    InsertDate        :TDateTime;
    EmploymentStatus  :string;
    FirstName         :string;
    LastName          :string;
    SickOption        :string;
    SickBalance       :double;
    PtoOption         :string;
    PtoBalance        :double;
    PtoAccrual        :double;
    WeeksRemaining    :double;
    AccrualToEOY      :double;
    EstBalAtEOY       :double;
    PTOLimit          :double; //QM-579 EB
    NoRecord          :Boolean;
  end;


  TTimesheetForm = class(TEmbeddableForm)
    TopPanel: TPanel;
    SaveButton: TButton;
    TimesheetData: TScrollBox;
    LeftPanel: TPanel;
    DaysData: TScrollBox;
    Name: TLabel;
    EmpNum: TLabel;
    NameLabel: TLabel;
    EmpNumLabel: TLabel;
    PreviousButton: TButton;
    NextButton: TButton;
    TimesheetActions: TActionList;
    SaveAction: TAction;
    PreviousWeekAction: TAction;
    NextWeekAction: TAction;
    CancelAction: TAction;
    WorkStart1Label: TLabel;
    JuryDutyHoursLabel: TLabel;
    HolidayHoursLabel: TLabel;
    VacationHoursLabel: TLabel;
    CalloutHoursLabel: TLabel;
    SickLeaveHoursLabel: TLabel;
    CalloutStart1Label: TLabel;
    BereavementHoursLabel: TLabel;
    MilesStop1Label: TLabel;
    MilesStart1Label: TLabel;
    DayHoursLabel: TLabel;
    RegularHoursLabel: TLabel;
    OvertimeHoursLabel: TLabel;
    DoubletimeHoursLabel: TLabel;
    VehicleUseLabel: TLabel;
    WorkStart2Label: TLabel;
    ScrollbarPanel: TPanel;
    CancelButton: TButton;
    HourFormatLabel: TLabel;
    SubmitButtonLabel: TLabel;
    ShowCalloutsButton: TButton;
    ShowCalloutsAction: TAction;
    TotalsPanel: TPanel;
    TotalsLabel: TLabel;
    CalloutHoursLabel1: TLabel;
    WorkHoursLabel1: TLabel;
    RegularHoursLabel1: TLabel;
    OvertimeHoursLabel1: TLabel;
    DoubletimeHoursLabel1: TLabel;
    WeekLabel: TLabel;
    SaveStateButton: TButton;
    FloatingHolidayLabel: TLabel;
    EmployeeTypeLabel: TLabel;
    ReasonChangedLabel: TLabel;
    TopLeftEntryPanel: TPanel;
    TopLeftClockPanel: TPanel;
    WorkHoursLabel: TLabel;
    PTOHoursLabel: TLabel;
    PerDiemLabel: TLabel;
    SickLeave: TLabel;
    SickLeaveTopLabel: TLabel;
    PTO: TLabel;
    PTOTopLabel: TLabel;
    lblReportLink: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TimesheetActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure SaveActionExecute(Sender: TObject);
    procedure PreviousWeekActionExecute(Sender: TObject);
    procedure NextWeekActionExecute(Sender: TObject);
    procedure CancelActionExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ShowCalloutsActionExecute(Sender: TObject);
    procedure SaveStateButtonClick(Sender: TObject);
    procedure lblReportLinkClick(Sender: TObject);
  private
    Days: array of TBaseTimesheetDayFrame;
    FWeekStartDate: TDateTime;
    HC: TBaseHoursCalculator;
    FSingleWeek: Boolean;
    FEmpID: Integer;
    FTimesheetEditMode: TTimesheetEditMode;
    FEditDay: TDateTime;
    FExpanded: Boolean;
    FFloatingHolidaysYear: TStringList;
    FFloatingHolidaysWeek: Integer;
    FMaxFloatingHolidaysPerYear: Integer;
    FAfterTimesheetSave: TNotifyEvent;
    FNeedToShutdown: Boolean;
    FirstPass: Boolean;
    fShowUtiliSafeButton: Boolean;   //QM-100 EB
    NewEntryForToday: Boolean;  //QM-100 EB
    FSickPTOBal: TSickPTOBal;
    fAbandonedCO: Boolean; //QM-597 EB
    fCOUpdates: TTSUpdates; //QM-597 EB

    procedure AlignLabels;
    procedure UMFocusDay(var Msg: TMessage); message UM_FOCUSDAY;
    function IsTimesheetModified: Boolean;
    procedure DayDataChanged(Sender: TObject);
    procedure SaveAllDays(Sender: TObject);
    procedure CreateAppropriateHoursCalculator;
    procedure SetUpDoubleTime;
    procedure SetEmpID(Value: Integer);
    procedure SetBevelHeights(NewHeight: Integer);
    procedure VerifyTimesheetCompatibility;
    procedure DoShowUtliSafeButton(const Value: Boolean);  //QM-100 EB
    property SingleWeek: Boolean read FSingleWeek write FSingleWeek;
    procedure SetupDaysExpanded;
    procedure CalculateWeek;
    function FloatingHolidaysYear_Get(const Year:Integer):Integer;
    function LocalEmployeeFloatingHolidays(const EmpID: Integer; const Year: Integer; const ExcludeFrom: TDateTime; const ExcludeTo: TDateTime): Integer;
    procedure RefreshControls;
    procedure CreateDayFrames;
    function GetNeedToShutdown: Boolean;
    function DisplayingAsTimeClock: Boolean;
    function AutoTimeImportAllowed: Boolean;
    procedure ShowTimeClockInstructions(AMode: TTimesheetEditMode);
    procedure AutoImportTime;
    procedure TimesheetSaveDone;
    function CheckSLandPTOBalanceOnSave: string;
    procedure ClearSickPTOBal;
    procedure CheckForAbandonedCalloutEntries;
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure ShowWeek(AEmpID: Integer; AEditDay: TDateTime; AMode: TTimesheetEditMode);
    procedure LeavingNow; override;
    procedure ActivatingNow; override;
    procedure ShowTodaysTimesheet;
    procedure PerformTimeImport;
    property MaxFloatingHolidaysPerYear: Integer read FMaxFloatingHolidaysPerYear;
    property FloatingHolidaysYear[const Year: Integer]: Integer read FloatingHolidaysYear_Get;
    property AfterTimesheetSave: TNotifyEvent read FAfterTimesheetSave write FAfterTimesheetSave;
    property NeedToShutdown: Boolean read GetNeedToShutdown;
    property ShowUtiliSafeButton: Boolean read fShowUtiliSafeButton write DoShowUtliSafeButton; //QM-100 EB

  end;

implementation

{$R *.dfm}

uses DMu, OdMiscUtils, DateUtils, OdExceptions, OdIsoDates, ShellAPI, //QM-579 EB
LocalPermissionsDMu, LocalEmployeeDMu;

const
  WeekDays: array [0..6] of string = ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
  DefaultBevelHeight = 425;

procedure TTimesheetForm.FormCreate(Sender: TObject);
begin
  SetEmpID(DM.EmpID);
  DM.KillTask('StreetMapProj.exe'); //qm-764 sr
  NameLabel.Caption := EmployeeDM.GetUserShortName;
  EmpNumLabel.Caption := EmployeeDM.GetUserEmployeeNumber;
  TopLeftClockPanel.Height := TopLeftEntryPanel.Height;
  SetLength(Days, Length(WeekDays));
  FirstPass := True;

end;

{Days - Adjust width}
procedure TTimesheetForm.CreateDayFrames;
const
  DayWidth = 82;
  ClockWidth = 131;
  DaySpacing = 8;
  ClockSpacing = 0;
  BufferSpacing = 10;
var
  ShowAsTimeClock: Boolean;
  i: Integer;
  FrameWidth: Integer;
  Bevel: TBevel;
  Spacing: Integer;
begin
  LockWindowUpdate(Self.Handle);
  try
    DaysData.Visible := False;  // Hide while we set everything up
    for i := Low(Days) to High(Days) do
      FreeAndNil(Days[i]); // also frees the TBevels owned by each day frame

    ShowAsTimeClock := DisplayingAsTimeClock;

    if ShowAsTimeClock then begin
      TopLeftEntryPanel.Visible := False;
      TopLeftClockPanel.Visible := True;
    end else begin
      TopLeftClockPanel.Visible := False;
      TopLeftEntryPanel.Visible := True;
    end;

    for i := Low(WeekDays) to High(WeekDays) do begin
      if ShowAsTimeClock then begin
        Days[i] := TTimeClockDayFrame.Create(Self);
        FrameWidth := ClockWidth;
        Spacing := ClockSpacing;
      end
      else begin
        Days[i] := TTimesheetDayFrame.Create(Self);
        FrameWidth := DayWidth;
        Spacing := DaySpacing;
      end;
      Days[i].Name := 'DayFrame' + IntToStr(i);
      Days[i].Top := 0;
      Days[i].Left := i * FrameWidth;
      Days[i].Width := FrameWidth - Spacing;
      Days[i].Parent := DaysData;
      Days[i].OnDataChange := DayDataChanged;
      Days[i].OnSaveDays := SaveAllDays;
      Days[i].Tag := i;

      if (not ShowAsTimeClock) and (i < High(WeekDays)) then begin
        Bevel := TBevel.Create(Days[i]);
        Bevel.SetBounds(Days[i].Left + (FrameWidth - 6), 7, 3, DefaultBevelHeight);
        Bevel.Parent := DaysData;
      end;
    end;
    TotalsPanel.Left := BufferSpacing + ((FrameWidth * (High(WeekDays) + 1)) - Spacing);
    DaysData.HorzScrollBar.Range := TotalsPanel.Left + TotalsPanel.Width + Spacing;
    DaysData.HorzScrollBar.Visible := True;
    ScrollbarPanel.Height := Days[0].Height;
    if ShowAsTimeClock then
      RefreshControls
    else
      SetupDaysExpanded;  // this aligns the labels too
  finally
    LockWindowUpdate(0);
  end;
end;

procedure TTimesheetForm.TimesheetActionsUpdate(Action: TBasicAction; var Handled: Boolean);
var
  Modified: Boolean;
begin
  Modified := IsTimesheetModified;
  SaveAction.Enabled := Modified;
  SaveButton.Enabled := Modified;
  CancelAction.Enabled := Modified;
  NextWeekAction.Enabled := (Today > EndingOfTheWeek(FWeekStartDate)) and (not SingleWeek);
  PreviousWeekAction.Enabled := (FWeekStartDate > (Today - 31)) and (not SingleWeek);
  //CalculateWeek;
end;

procedure TTimesheetForm.SaveActionExecute(Sender: TObject);
var
  i: Integer;
  TodayIdx, TodayIdxToo: Integer;
  RedisplayToday: Boolean;
  IWorked: Boolean;
  PTOSLMessage: string;
begin
  FNeedToShutdown := False;
  RedisplayToday := False;
  TodayIdxToo := -1;
  NewEntryForToday := False;
  {QM-833 Timesheet TSE: Note that we do not set the TSE limitation on the client side
    it is done on the Server side when saving the timesheet}
  for i := Low(Days) to High(Days) do begin
    if Days[i].DisplayingToday then begin
      TodayIdx := i;
      if Days[i].WorkStartedToday then  //QM-100 EB UtiliSafe
        NewEntryForToday := True;
    end;
    if (i + FWeekStartDate) = Yesterday then
      RedisplayToday := Days[i].DuringSubmit or Days[i].Modified;

    {QM-579 EB Sickleave/PTO Balance - if there is a message, halt the save}
    PTOSLMessage := CheckSLandPTOBalanceOnSave;
    if PTOSLMessage <> '' then begin
      MessageDlg(PTOSLMessage, mtError, [mbOK], 0);
      Exit;
    end;
    
    Days[i].SaveChanges;
    if (Days[i] is TTimeClockDayFrame) and (Days[i] as TTimeClockDayFrame).NeedToShutdown then
      FNeedToShutdown := True;
  end;
  // submitting yesterday enables today
  if RedisplayToday and (TodayIdx in [Low(Days)..High(Days)]) then
    // this is safe because ShowWeek has called Days[].Display
    Days[TodayIdx].Redisplay;

  TimesheetSaveDone;
  try
    if FEmpID = DM.EmpID then begin {Only if this is my timesheet}
      IWorked := DM.AmIClockedIn;   //QM-100 EB
      if ((Days[TodayIdx].WorkStartedToday) or DM.AmIClockedIn) and PermissionsDM.CanI(RightTimesheetsUtiliSafe) then //EB QM-100 UtiliSafe
        ShowUtiliSafeButton := True;
    end;
  except
    {Swallow}
  end;
end;

procedure TTimesheetForm.TimesheetSaveDone;
var
  i: Integer;
begin
 {QM-597 QM-606 EB - Mark where Timesheet is SAVED}
  if Assigned(FAfterTimesheetSave) then begin
    FAfterTimesheetSave(Self);
    for i := Low(Days) to High(Days) do
      Days[i].Redisplay;
  end;

end;

procedure TTimesheetForm.CancelActionExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := Low(Days) to High(Days) do
    Days[i].CancelChanges;

end;

procedure TTimesheetForm.CheckForAbandonedCalloutEntries;    //QM-597 EB Abandoned Callouts
var
  MissingEntry: TEntryRec;
begin
  if fEMPID = DM.EmpID then begin
    MissingEntry := GetAbandonedCalloutEntries(fEMPID, Yesterday);
    if (MissingEntry.FoundProblem) then begin
      fCOUpdates := ExecuteCalloutCompletionDlg(fEmpID, MissingEntry);
      if fCOUpdates.IsUpdated then
        fAbandonedCO := True  //QM-597 EB Abandoned Callouts
      else
        fAbandonedCO := False;  //QM-597 EB Going to Ignore it
    end;
  end;


end;

function TTimesheetForm.CheckSLandPTOBalanceOnSave: string;
var
  i: integer;
  AccruedPTO: double;
  AccruedSL: double;
begin
 Result := '';

 AccruedPTO := FSickPTOBal.PtoBalance;
 AccruedSL :=  fSickPTOBal.SickBalance;
 for i := Low(Days) to High(Days) do begin
   try
      if (Days[i].PTOHours.Text <> '') then
        AccruedPTO := AccruedPTO - StrToFloat(Days[i].PTOHours.Text);
      if (Days[i].SickLeaveHours.Text <>'') then
        AccruedSL := AccruedSL - StrToFloat(Days[i].SickLeaveHours.Text);
    except
      {Swallow - keep previous accumulated balance}
    End;
 end;

// if (AccruedPTO <= -40) and (FSickPTOBal.PtoBalance > AccruedPTO) then begin
//   Result := 'PTO taken has exceeded the allowable limit';
// end;
 if (AccruedSL < 0) and (fSickPTOBal.SickBalance > AccruedSL) then begin
//   if Result <> '' then Result := Result + ' and ';
   Result := Result + 'Sick Leave taken has exceeded the allowable amount.';
 end;
end;

procedure TTimesheetForm.ClearSickPTOBal;
begin
  fSickPTOBal.InsertDate := 0;
  fSickPTOBal.EmploymentStatus := '';
  fSickPTOBal.FirstName := '';
  fSickPTOBal.LastName := '';
  fSickPTOBal.SickOption := '';
  fSickPTOBal.SickBalance := 0.0;
  fSickPTOBal.PtoOption := '';
  fSickPTOBal.PTOBalance := 0.0;
  FSickPTOBal.EmploymentStatus  :='';
  FSickPTOBal.PtoAccrual := 0.0;
  FSickPTOBal.WeeksRemaining := 0.0;
  FSickPTOBal.AccrualToEOY := 0.0;
  FSickPTOBal.EstBalAtEOY  := 0.0;
  FSickPTOBal.NoRecord := FALSE;
end;

procedure TTimesheetForm.ShowWeek(AEmpID: Integer; AEditDay: TDateTime; AMode: TTimesheetEditMode);
const
  StartDateConfigName = 'PTOHoursEntryStartDate';
  BadDate = 900000; //on invalid PTO start date, set the value far in the future
var
  i: Integer;
  DisplayDay, PTOStartDate: TDateTime;
  ShowMileageLabels: Boolean;
  PTODateString: string;
  UseMode: TTimesheetEditMode;
//  UsePTO, PartialPTOWeek: Boolean;
  AccruedPTOBal, AccruedSLBal : double;
begin
  Assert(FFloatingHolidaysYear <> nil);
  FFloatingHolidaysYear.Clear;
  FloatingHolidayLabel.Visible := False;

  SetEmpID(AEmpID);

//  btnAddCallOut.Visible :=PermissionsDM.CanEmployee(DM.EmpID, (RightTimesheetsAddYesterdayCallout));  //QM-442 SR
  FWeekStartDate := BeginningOfTheWeek(AEditDay);
  VerifyTimesheetCompatibility;
  SingleWeek := AMode <> teNormal; // Managers/IT might only have the current week's data locally
  FEditDay := AEditDay;
  CreateAppropriateHoursCalculator;  // In case they synced and it changed

  {QM-579: Hiding out by request}
  ShowMileageLabels := False; //EmployeeDM.EmployeeHasCompanyVehicle(FEmpID);
  MilesStart1Label.Visible := ShowMileageLabels;
  MilesStop1Label.Visible := ShowMileageLabels;
  VehicleUseLabel.Visible := False; //PermissionsDM.CanEmployee(AEmpID, RightPOVCOVEntry);
  ReasonChangedLabel.Visible := (AMode in [teManager, teITOverride]);

  PTODateString := DM.GetConfigurationDataValue(StartDateConfigName, '');
  PTOStartDate := IsoStrToDateDef(PTODateString, BadDate);
  if NotEmpty(PTODateString) and (PTOStartDate = BadDate) and FirstPass then begin
    MessageDlg(Format('PTO Hours entry is disabled. ' + CRLF + 'The %s configuration data ' +
      'of %s is not in the expected format of yyyy-mm-dd. ' +
      CRLF + 'After a system administrator fixes the entry with QManagerAdmin, sync and try again.',
      [StartDateConfigName, PTODateString]), mtWarning, [mbOk], 0);
    FirstPass := False;
  end;

//  UsePTO := (PTOStartDate <= FWeekStartDate + 6);
//  PartialPTOWeek := EmpHasPartialPTOWeek(PTOStartDate) or
//    (UsePTO and (PTOStartDate > FWeekStartDate));

  PTOHoursLabel.Visible := True;//  SickLeaveHoursLabel.Visible := PermissionsDM.CanI(RightTimesheetsViewSickLeave); //QM-549 Set back to visible --QM-517 Sick Leave EB

  VacationHoursLabel.Visible := False;//QM-517 Sick Leave - turn this completely off

  AccruedPTOBal :=0.0;
  AccruedSLBal := 0.0;

  {Loop through DAYS}
  for i := Low(Days) to High(Days) do begin
    DisplayDay := FWeekStartDate + i;
    UseMode := AMode;
    if (AMode in [teManager, teITOverride]) and (DisplayDay <> AEditDay) then
      UseMode := teReadOnly;

    {Call-out Completed Next Morning}
    If (fCOUpdates.IsUpdated) and (fCOUpdates.Mode = modeNextMorning) and
    (DateOf(DisplayDay) = Today) then begin   //QM-614 EB  {Leave this out, it messes up Next Day Entries: and (Dateof(fCOUpdates.COWorkDate) = DateOf(DisplayDay))}                        //QM-597 EB Abandoned CallOut
      Days[i].FinishCallOut.HasMoreDates := True;
      Days[i].FinishCallOut.FinishWorkDate := DateOf(Today);
      Days[i].FinishCallOut.AddCallOutStart := fCOUpdates.AddEntryCallin;
      Days[i].FinishCallOut.AddCallOutStop := fCOUpdates.TimeEnd;
      Days[i].FinishCallOut.PreviousEntriesMade := True;
      Days[i].FinishCallOut.SavedandDone := False;
      fCOUpdates.IsUpdated := False; {We don't want to cycle through again}
    end

    {No Call-out}
    else begin {If before Midnight, we don't have anything to complete}
     Days[i].FinishCallOut.HasMoreDates := False;
     Days[i].FinishCallOut.PreviousEntriesMade := False;
    end;

    Days[i].Display(FEmpID, DisplayDay, UseMode);

    {Not Used anymore, but just disable it}

    Days[i].FloatingHoliday.Visible := false; //MaxFloatingHolidaysPerYear > 0;
    Days[i].FloatingHoliday.Caption := '';
    Days[i].FloatingHoliday.Checked := False;

    Try
      if (Days[i].PTOHours.Text <> '') then
        AccruedPTOBal := AccruedPTOBal + StrToFloat(Days[i].PTOHours.Text);
      if (Days[i].SickLeaveHours.Text <>'') then
        AccruedSLBal := AccruedSLBal + StrToFloat(Days[i].SickLeaveHours.Text);

      Days[i].SetAccruedValsFromTimeSheet((FSickPTOBal.SickBalance - AccruedSLBal),
                                          (FSickPTOBal.PtoBalance - AccruedPTOBal),
                                           FSickPTOBal.NoRecord );  //QM-579 EB Sets all accrued values on individual days
    except
      {Swallow - keep previous accumulated balance}
    End;

    //(UsePTO and (PTOStartDate <= DisplayDay), PartialPTOWeek); QM-579 EB
    if Days[i].DisplayingToday then begin
      //.....
      Days[i].WorkStartedToday := TTimeClockDayFrame(Days[i]).HasStartedWork;   //QM-100 EB UtiliSafe
      if Days[i].WorkStartedToday and PermissionsDM.CanI(RightTimesheetsUtiliSafe) then begin //EB QM-100 UtiliSafe
        ShowUtiliSafeButton := True;
        NewEntryForToday := True;
      end;
    end;
  end;
  AlignLabels;
  ShowTimeClockInstructions(AMode);

  DaysData.Visible := True;  // Show, if it was hidden
  PostMessage(Handle, UM_FOCUSDAY, 0, 0);
  SaveStateButton.Visible := DM.UQState.DeveloperMode;
  //CalculateWeek;
  DayDataChanged(nil);


end;

{Algin Labels to controls}
procedure TTimesheetForm.AlignLabels;
const
  LabelLeft = 3;

  procedure AlignPanelLabels(Panel: TPanel);
  var
    AlignLabel: TLabel;
    AlignComponent: TComponent;
    AlignControl, ParentControl: TControl;
    ControlCenter: Integer;
    ComponentName: string;
    i, ParentTop: Integer;
  begin
    for i := 0 to Panel.ControlCount - 1 do begin
      if Panel.Controls[i] is TLabel then begin
        AlignLabel := Panel.Controls[i]as TLabel;
        AlignControl := nil;
        if StrEndswith('toplabel', AlignLabel.Name) then begin {we can just ignore this code}
          ComponentName := 'xxxxx';
        end

        {Cycle through the labels on the left side and align with their counterparts in the frames}
        else if StrEndsWith('Label1', AlignLabel.Name) then
          ComponentName := Copy(AlignLabel.Name, 1, Length(AlignLabel.Name) - Length('Label1'))
        else
          ComponentName := Copy(AlignLabel.Name, 1, Length(AlignLabel.Name) - Length('Label'));
        AlignComponent := Days[0].FindComponent(ComponentName);

        if Assigned(AlignComponent) and (AlignComponent is TControl) then
          AlignControl := AlignComponent as TControl;
        if Assigned(AlignControl) then begin
          ControlCenter := AlignControl.Top + Round(AlignControl.Height / 2);
          if Panel = LeftPanel then
            AlignLabel.Left := LabelLeft;
          if AlignControl.Parent <> Days[0] then begin
            ParentControl := AlignControl.Parent;
            ParentTop := 0;
            repeat
              ParentTop := ParentTop + ParentControl.Top;
              ParentControl := ParentControl.Parent;
            until ParentControl = Days[0];
            AlignLabel.Top := ControlCenter - Round(AlignLabel.Height / 2) + ParentTop;
          end else
            AlignLabel.Top := ControlCenter - Round(AlignLabel.Height / 2);
        end;
      end;
    end;
  end;

begin
  AlignPanelLabels(LeftPanel);
  AlignPanelLabels(TotalsPanel);
  ShowCalloutsButton.Top := CalloutStart1Label.Top + CalloutStart1Label.Height + 1;
  ShowCalloutsButton.Left := LabelLeft;
end;

procedure TTimesheetForm.PreviousWeekActionExecute(Sender: TObject);
begin
  Assert(not SingleWeek);
  ShowWeek(FEmpID, FWeekStartDate - 7, FTimesheetEditMode);
end;

procedure TTimesheetForm.NextWeekActionExecute(Sender: TObject);
begin
  Assert(not SingleWeek);
  ShowWeek(FEmpID, FWeekStartDate + 7, FTimesheetEditMode);
end;

procedure TTimesheetForm.UMFocusDay(var Msg: TMessage);
var
  i: Integer;
  FocusDate: TDateTime;
begin
  if FTimesheetEditMode = teNormal then
    FocusDate := Today
  else
    FocusDate := FEditDay;

  if DM.EmpID = FEmpID then begin
    for i := Low(Days) to High(Days) do
      if Days[i].WorkDate = FocusDate then begin
        Days[i].FocusIfPossible;
        Break;
      end;
  end;
end;


procedure TTimesheetForm.LeavingNow;
var
  i: Integer;
  SaveAfterCO: Boolean;
begin
  inherited;
  SaveAfterCO := False;
  for i := Low(Days) to High(Days) do begin     //QM-597 EB Fix
   // if (Days[i].DisplayingToday) and
//      (Days[i].FinishCallOut.PreviousEntriesMade) and (not Days[i].FinishCallOut.SavedandDone) then begin
//      Days[i].SaveChanges;
//      Days[i].CloseData;
//      SaveAfterCO :=True;
//      Days[i].FinishCallOut.SavedandDone := True;
//    end
//    else if (Days[i].FinishCallOut.PreviousEntriesMade) and (not Days[i].FinishCallOut.SavedandDone) then begin
//      Days[i].CloseData;
//    end

//    else begin
      Days[i].CancelChanges;
      Days[i].CloseData;
//    end;
  end;

//  if SaveAfterCO then begin
//    DM.SendTimeChangesToServer;
//  end;
end;


function TTimesheetForm.IsTimesheetModified: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(Days) to High(Days) do begin
    Result := Assigned(Days[i]) and Days[i].Modified;
    if Result then
      Break;
  end;
end;

procedure TTimesheetForm.lblReportLinkClick(Sender: TObject);
const
  URLLink= 'https://uqview.utiliquest.com/UQDashboard/Home/DynamicView?vid=59';
begin
  inherited;
  {QM-579 EB}
   try
    ShellExecute(0,'open',PChar(URLLink),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Error, cannot open link '+URLLink +'  '+e.message);
  end;
end;

procedure TTimesheetForm.ShowTodaysTimesheet;
begin
  fAbandonedCO := False;
  CheckForAbandonedCalloutEntries;
  ShowWeek(DM.EmpID, Now, teNormal);
end;

procedure TTimesheetForm.CalculateWeek;
var
  i: Integer;
begin
  Assert(Assigned(HC), 'CalculateWeek called before HC set up');

  for i := Low(Days) to High(Days) do begin
    HC.Worked[i] := Days[i].WorkingHours;
    HC.Callout[i] := Days[i].DayCalloutHours;
  end;

  HC.Calculate;

  WorkHoursLabel1.Caption := HoursFormat(TotalWeek(HC.Worked));
  // Note that we don't show a total for HC.Callout, which is the input CO time

  RegularHoursLabel1.Caption := HoursFormat(TotalWeek(HC.Regular));
  OvertimeHoursLabel1.Caption := HoursFormat(TotalWeek(HC.Overtime));
  DoubletimeHoursLabel1.Caption := HoursFormat(TotalWeek(HC.DoubleTime));
  CalloutHoursLabel1.Caption := HoursFormat(TotalWeek(HC.CalloutCalc));
end;

procedure TTimesheetForm.DayDataChanged(Sender: TObject);
var
  i: Integer;
begin
  CalculateWeek;

  // Save calculated week back to the days:
  FFloatingHolidaysWeek := 0;
  for i := Low(Days) to High(Days) do begin
    Days[i].SetHoursBreakdown(HC.Regular[i], HC.Overtime[i], HC.DoubleTime[i], HC.CalloutCalc[I]);
    if (Days[i].FloatingHoliday.Checked)
      then Inc(FFloatingHolidaysWeek);
  end;

  for i := Low(Days) to High(Days) do begin
    if not Days[i].TimesheetData.Active
      then Continue;
    Days[i].FloatingHolidaysAvailable := (FloatingHolidaysYear[YearOf(Days[i].WorkDate)] + FFloatingHolidaysWeek < MaxFloatingHolidaysPerYear) or
      (Days[i].FloatingHoliday.Checked);
    Days[i].FloatingHoliday.Enabled := Days[i].FloatingHolidayEnabled and Days[i].FloatingHolidaysAvailable;
  end;
end;

procedure TTimesheetForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(HC);
end;

procedure TTimesheetForm.CreateAppropriateHoursCalculator;
begin
  FreeAndNil(HC);
  HC := DM.CreateHoursCalculatorForEmployee(FEmpID);

  SubmitButtonLabel.Caption := HC.Identity;
  SetUpDoubleTime;
end;

procedure TTimesheetForm.SetUpDoubleTime;
var
  i: Integer;
begin
  for i := Low(Days) to High(Days) do
    Days[i].SetDoubleTimePossible(HC.DoubleTimePossible);
  DoubletimeHoursLabel.Visible := HC.DoubleTimePossible;
end;

procedure TTimesheetForm.ActivatingNow;
begin
  FNeedToShutdown := False;
  CreateDayFrames;
end;

procedure TTimesheetForm.PerformTimeImport; //formerly called by timeimport monitor; now called via MainFu
begin
  if AutoTimeImportAllowed then
    AutoImportTime
  else
    ShowTimeClockInstructions(FTimesheetEditMode);
end;

function TTimesheetForm.AutoTimeImportAllowed: Boolean;
begin
  Result := (not IsTimesheetModified) and (FTimesheetEditMode = teNormal) and PermissionsDM.CanI(RightTimesheetsClockImport);
end;

procedure TTimesheetForm.AutoImportTime;
var
  i: Integer;
begin
  for i := Low(Days) to High(Days) do begin
    if ((Days[i].WorkDate = Today) or
        (PermissionsDM.CanI(RightTimesheetsPriorDayEntry) and (Days[i].WorkDate = Today-1))) then begin
      Assert(Days[i] is TTimeClockDayFrame, 'Internal error in TTimesheetForm.AutoImportTime');
      if TTimeClockDayFrame(Days[i]).AutoImport then
        SaveActionExecute(Self);
      //reset the flag; if import fails then the files are moved to failed folder
      ShowTimeClockInstructions(FTimesheetEditMode);
    end;
  end;
end;

procedure TTimesheetForm.SetEmpID(Value: Integer);
const
  Select = 'select lc.floating_holidays_per_year from locating_company lc ' +
    'join employee e on (lc.company_id = e.company_id) where (e.emp_id = %d)';

  SICKBAL = '%n hrs as of %s';
  PTOBAL =  '%n hrs as of %s';
var
  Data: TDataSet;
begin
  ClearSickPTOBal;
  FEmpID := Value;
  NameLabel.Caption := EmployeeDM.GetEmployeeShortName(Value);
  EmpNumLabel.Caption := EmployeeDM.GetEmployeeNumber(Value);
  EmployeeDM.ClearEmpBalRecord;  //QM-579  sr
  if fEmpID <> DM.EmpID then begin  //QM-579 EB:if we are looking at someone else's timesheet
    EmployeeDM.LoadEmployeeSickPTOfromServer(FEmpID);
    //lblReportLink.Visible := TRUE;  //QM-803 EB Hide PTO and report labels on timesheet
  end
  else
    lblReportLink.Visible := FALSE;
  
  if EmployeeDM.GetEmployeeBalance(FEmpID) then  //QM-579  sr
  begin
    FSickPTOBal.InsertDate        :=EmployeeDM.InsertDate;
    FSickPTOBal.EmploymentStatus  :=EmployeeDM.EmploymentStatus;
    FSickPTOBal.SickOption        :=EmployeeDM.SickOption;
    FSickPTOBal.SickBalance       :=EmployeeDM.SickBalance;
    FSickPTOBal.PtoOption         :=EmployeeDM.PtoOption;
    FSickPTOBal.PtoBalance        :=EmployeeDM.PtoBalance;
    FSickPTOBal.PtoAccrual        :=EmployeeDM.PtoAccrual;
    FSickPTOBal.WeeksRemaining    :=EmployeeDM.WeeksRemaining;
    FSickPTOBal.AccrualToEOY      :=EmployeeDM.AccrualToEOY;
    FSickPTOBal.EstBalAtEOY       :=EmployeeDM.EstBalAtEOY;
    FSickPTOBal.FirstName         :=EmployeeDM.FirstName;
    FSickPTOBal.LastName          :=EmployeeDM.LastName;
  //  FSickPTOBal.PTOLimit          :=DM.GetConfigurationDataValue('OverPTOLimit');


    SickLeavetopLabel.Caption := '';
    {re-align these}
    SickLeaveTopLabel.Width := 168;
    PTOTopLabel.Width := SickleaveTopLabel.Width;
    PTOTopLabel.Left := SickLeaveTopLabel.Left;


    if FSickPTOBal.InsertDate > StrToDate('5/18/2022') then begin
      SickLeavetopLabel.Caption := format(SICKBAL,[FSickPTOBal.SickBalance, DateToStr(FSickPTOBal.InsertDate)]);   //QM-579  sr
      PTOtopLabel.Caption := format(PTOBAL, [FSickPTOBal.PtoBalance, DateToStr(FSickPTOBal.InsertDate)]);  //QM-579 EB
      SickLeaveTopLabel.Alignment := taRightJustify;
      PTOTopLabel.Alignment := taRightJustify;

      if fSickPTOBal.SickBalance < 0.0 then begin      //QM-579 EB Change colors
        SickLeaveTopLabel.Font.Color := clMaroon;
        SickLeave.Font.Color := clMaroon;
      end
      else begin
        SickLeaveTopLabel.Font.Color := clNavy;
        SickLeave.Font.Color := clNavy;
      end;

      if FSickPTOBal.PtoBalance < 0.0 then begin
        PTOTopLabel.Font.Color := clMaroon;
        PTO.Font.Color := clMaroon;
      end
      else begin
        PTOTopLabel.Font.Color := clNavy;
        PTO.Font.Color := clNavy;
      end;

    end;
    EmployeeDM.ClearEmpBalRecord;  //QM-579  sr
  end
  else
  begin
    SickLeavetopLabel.Caption := 'No leave balance found'; //+Trim(FSickPTOBal.FirstName)+ ' '+Trim(FSickPTOBal.LastName);
    PtoTopLabel.Caption := 'No PTO balance found';
    SickLeaveTopLabel.Alignment := taLeftJustify;
    PTOTopLabel.Alignment := taLeftJustify;
    fSickPTOBal.NoRecord := True;   //QM-579 EB
    PTOTopLabel.Font.Color := clNavy;
    PTO.Font.Color := clNavy;
    SickLeaveTopLabel.Font.Color := clNavy;
    SickLeave.Font.Color := clNavy;
  end;
  FMaxFloatingHolidaysPerYear := 0;

  Data := DM.Engine.OpenQuery(Format(Select, [FEmpID]));
  try
    if not Data.IsEmpty then
      FMaxFloatingHolidaysPerYear := Data.FieldByName('floating_holidays_per_year').AsInteger;
  finally
    FreeAndNil(Data);
  end;

  FloatingHolidayLabel.Visible := False; //(MaxFloatingHolidaysPerYear > 0);
end;

procedure TTimesheetForm.SaveAllDays(Sender: TObject);
begin
  SaveActionExecute(Sender);
end;

procedure TTimesheetForm.ShowCalloutsActionExecute(Sender: TObject);
begin
  FExpanded := not FExpanded;

  SetupDaysExpanded;
end;

procedure TTimesheetForm.SetupDaysExpanded;
var
  i: Integer;
begin
  for i := Low(Days) to High(Days) do
    Days[i].Expanded := FExpanded;

  if FExpanded then begin
    ShowCalloutsButton.Caption := 'Show Fewer';
    SetBevelHeights(DefaultBevelHeight + TTimesheetDayFrame(Days[0]).MoreCalloutsPanel.Height);
  end else begin
    ShowCalloutsButton.Caption := 'Show More';
    SetBevelHeights(DefaultBevelHeight);
  end;

  RefreshControls;
end;

procedure TTimesheetForm.RefreshControls;
begin
  ScrollbarPanel.Height := Days[0].Height;
  AlignLabels;
end;

procedure TTimesheetForm.SetBevelHeights(NewHeight: Integer);
var
  i: Integer;
begin
  for i := 0 to DaysData.ControlCount - 1 do
    if DaysData.Controls[i] is TBevel then
      (DaysData.Controls[i] as TBevel).Height := NewHeight;
end;

procedure TTimesheetForm.SaveStateButtonClick(Sender: TObject);
const
  FileName = 'time-calc-state.txt';
begin
  inherited;
  HC.WriteStateToFile(FileName);
  ShowMessage('State saved to ' + fileName);
end;

constructor TTimesheetForm.Create(AOwner: TComponent);
begin
  inherited;
  FFloatingHolidaysYear := TStringList.Create;
end;

destructor TTimesheetForm.Destroy;
begin
  FreeAndNil(FFloatingHolidaysYear);
  inherited;
end;

function TTimesheetForm.FloatingHolidaysYear_Get(const Year: Integer): Integer;
var
  FloatingHolidaysInTheYear: string;
begin
  Assert(FFloatingHolidaysYear <> nil);
  FloatingHolidaysInTheYear := FFloatingHolidaysYear.Values[IntToStr(Year)];
  if (FloatingHolidaysInTheYear = '') then begin
    if (FEmpID = DM.EmpID) then begin
      // Editing timesheet records for logged in employee.
      // All records for the past year that have floating_holiday
      // set to true should have been synced to the local database.
      Result := LocalEmployeeFloatingHolidays(FEmpID, Year, FWeekStartDate, FWeekStartDate + (A_Day * 6));
    end
    else begin
      // Manager/supervisor is editing another employee's timesheet records.
      // Server must be called to determine how many floating holiday days
      // the employee has taken for any given year since that data will
      // not exist in the local database.
      DM.CallingServiceName('EmployeeFloatingHolidays');
      // TODO: Fix this by downloading the timesheets before this and remove this special case
      Result := DM.Engine.Service.GetEmployeeFloatingHolidays(DM.Engine.SecurityInfo, FEmpID, Year, FWeekStartDate, FWeekStartDate + (A_Day * 6));
    end;

    FFloatingHolidaysYear.Values[IntToStr(Year)] := IntToStr(Result);
  end
  else
    Result := StrToInt(FloatingHolidaysInTheYear);
end;

function TTimesheetForm.LocalEmployeeFloatingHolidays(const EmpID,
  Year: Integer; const ExcludeFrom, ExcludeTo: TDateTime): Integer;
const
  Select = 'select Count(*) as CountDays from timesheet_entry ' +
    'where (floating_holiday = True) and (work_emp_id = %d) and status <> ''OLD''';
var
  SQL: string;
  Data: TDataSet;
begin
  SQL := Format(Select, [EmpID]);
  if (ExcludeFrom < 1) or (ExcludeTo < 1)  then
    // Search all records for the indicated year
    SQL := SQL + ' and (work_date >= ''' + IntToStr(Year) + '-01-01'') and (work_date < ''' + IntToStr(Year + 1) + '-01-01'')'
  else begin
    // Search records for the indicated year, excluding the specified date range
    SQL := SQL + ' and (';
    SQL := SQL + ' ( (work_date >= ''' + IntToStr(Year) + '-01-01'') and (work_date < ''' + FormatDateTime('yyyy-mm-dd',Trunc(ExcludeFrom) + A_Day) + ''') )';
    SQL := SQL + ' or ( (work_date >= ''' + FormatDateTime('yyyy-mm-dd',Trunc(ExcludeTo) + A_Day) + ''') and (work_date < ''' + IntToStr(Year + 1) + '-01-01'') )';
    SQL := SQL + ' )';
  end;

  // Execute the SQL select query and return the result
  Data := DM.Engine.OpenQuery(SQL);
  try
    Result := Data.FieldByName('CountDays').AsInteger;
  finally
    FreeAndNil(Data);
  end;
end;

procedure TTimesheetForm.ShowTimeClockInstructions(AMode: TTimesheetEditMode);
const
  ManagerHelp = CRLF + 'Change activities or times as needed.' + CRLF +
    'Insert key adds more time.' + CRLF + 'Ctrl-Delete removes time.';
  UsageHelp = CRLF + 'Select a clock in or out activity for the current time and Save.' + CRLF +
    'Use Escape or Cancel to clear.';
  ImportWaitingHelp = CRLF + 'Time activities are waiting for import.' + CRLF +
    'They will be imported after saving or canceling.';
  ImportHelp = CRLF + 'No time activities to import.';
begin    //qm-579  sr removed while locating space  --  need to see if we can move into Help Dialog
//  InstructionLabel.Visible := False;
//  if not DisplayingAsTimeClock then
//    Exit;
//
//  if (AMode in [teManager, teITOverride]) then
//    InstructionLabel.Caption := ManagerHelp
//  else if ((FTimesheetEditMode = teNormal)
//    and TimeFileMonitorDM.AnyPendingTimeImportFiles ) then
//    InstructionLabel.Caption := ImportWaitingHelp
//  else if ((FTimesheetEditMode = teNormal) and PermissionsDM.CanI(RightTimesheetsClockImport)) then
//    InstructionLabel.Caption := ImportHelp
//  else
//    InstructionLabel.Caption := UsageHelp;
//  InstructionLabel.Visible := True;
end;

function TTimesheetForm.DisplayingAsTimeClock: Boolean;
begin
  Result := PermissionsDM.CanI(RightTimesheetsClockEntry) or PermissionsDM.CanI(RightTimesheetsClockImport);
end;



procedure TTimesheetForm.DoShowUtliSafeButton(const Value: Boolean);   //QM-100 EB
begin
  fShowUtiliSafeButton := Value;
  if fShowUtiliSafeButton then begin
    if (EmployeeDM.HasRecordedEmployeeActivity(ActivityTypeUtiliSafe)) or
      (EmployeeDM.AlreadyHasActivityToday(ActivityTypeUtiliSafe, DM.EmpID)) then
      exit
    else if (Owner is TForm) then  {Send Message to Main form that work has started}
      PostMessage(TForm(Owner).Handle, WM_USER + 1100, 0, 0);
  end;
end;

function TTimesheetForm.GetNeedToShutdown: Boolean;
begin
  Result := FNeedToShutdown and (not PermissionsDM.CanI(RightWorkAfterWorkClockOut));
end;

procedure TTimesheetForm.VerifyTimesheetCompatibility;
begin
  if (not DisplayingAsTimeClock) and (DM.WeekHasAdditionalWorkTimes(FEmpID, FWeekStartDate)) then
    raise EOdDataEntryError.Create('Timesheets - Time Clock permission is required ' +
      'to access the requested timesheet because it has additional work or callout time spans.');
end;

//function TTimesheetForm.EmpHasPartialPTOWeek(const PTOStartDate: TDateTime): Boolean;
//const
//  SQL = 'select Sum(Coalesce(pto_hours,0)) pto_hrs, ' +
//    'Sum(Coalesce(vac_hours,0) + Coalesce(leave_hours,0)) vac_leave_hrs ' +
//    'from timesheet_entry where work_emp_id = %d ' +
//    'and work_date >= %s and work_date < %s and status <> ''OLD''';
//var
//  D: TDataSet;
//  PTOHrs, VacLeaveHrs: Double;
//begin
//  Assert(DayOfTheWeek(FWeekStartDate) = DaySunday, 'WeekStartDate must be a Sunday.');
//  D := DM.Engine.OpenQuery(Format(SQL, [FEmpID, IsoDateToStrQuoted(FWeekStartDate),
//    IsoDateToStrQuoted(FWeekStartDate + 7)]));
//  try
//    PTOHrs := D.FieldByName('pto_hrs').AsFloat;
//    VacLeaveHrs := D.FieldByName('vac_leave_hrs').AsFloat;
//    Result := ((PTOHrs > 0) and (VacLeaveHrs > 0)) or
//      ((PTOHrs > 0) and (PTOStartDate > FWeekStartDate + 6)) or
//      ((VacLeaveHrs > 0) and (PTOStartDate < FWeekStartDate));
//  finally
//    D.Close;
//    FreeAndNil(D);
//  end;
//end;

end.
