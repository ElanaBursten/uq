inherited OldTicketsForm: TOldTicketsForm
  Caption = 'Old Tickets'
  ClientWidth = 801
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 29
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object RefreshButton: TButton
      Left = 3
      Top = 4
      Width = 86
      Height = 23
      Caption = 'Refresh'
      TabOrder = 0
      OnClick = RefreshButtonClick
    end
    object btnWhatRow: TButton
      Left = 672
      Top = 2
      Width = 75
      Height = 25
      Caption = 'What Row?'
      TabOrder = 1
      Visible = False
      OnClick = btnWhatRowClick
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 29
    Width = 801
    Height = 371
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    object GridView: TcxGridDBTableView
      OnDblClick = GridDblClick
      OnKeyDown = GridKeyDown
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      FilterBox.Position = fpTop
      FilterBox.Visible = fvNever
      OnCustomDrawCell = GridViewCustomDrawCell
      DataController.DataSource = TicketSource
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'ticket_id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnGroupingChanged = GridViewDataControllerGroupingChanged
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = ColTicketNumber
      OptionsCustomize.ColumnHiding = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.GroupBySorting = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.ContentEven = SharedDevExStyleData.OldTicketsEven
      Styles.ContentOdd = SharedDevExStyleData.OldTicketsOdd
      Styles.IncSearch = SharedDevExStyleData.IncSearchHighlight
      Styles.Selection = SharedDevExStyleData.UnfocusedGridSelStyle
      Styles.Group = SharedDevExStyleData.OldTicketsGroupTitle
      OnColumnHeaderClick = GridViewColumnHeaderClick
      object ColTicketID: TcxGridDBColumn
        Caption = 'Ticket ID'
        DataBinding.FieldName = 'ticket_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        HeaderAlignmentHorz = taRightJustify
        Options.Filtering = False
        Width = 260
      end
      object ColTicketNumber: TcxGridDBColumn
        Caption = 'Ticket #'
        DataBinding.FieldName = 'ticket_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.LookupItemsSorted = True
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Grouping = False
        Width = 134
      end
      object ColKind: TcxGridDBColumn
        Caption = 'Kind'
        DataBinding.FieldName = 'kind'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 244
      end
      object ColDueDate: TcxGridDBColumn
        Caption = 'Due Date'
        DataBinding.FieldName = 'due_date'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        OnCustomDrawCell = ColCustomDrawCell
        Width = 452
      end
      object ColTicketType: TcxGridDBColumn
        Caption = 'Type'
        DataBinding.FieldName = 'ticket_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Width = 130
      end
      object ColWorkDescription: TcxGridDBColumn
        Caption = 'Work Description'
        DataBinding.FieldName = 'work_description'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Options.Grouping = False
        Width = 359
      end
      object ColCompany: TcxGridDBColumn
        Caption = 'Company'
        DataBinding.FieldName = 'company'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Width = 134
      end
      object ColPriority: TcxGridDBColumn
        Caption = 'Priority'
        DataBinding.FieldName = 'priority'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 990
      end
      object ColWorkType: TcxGridDBColumn
        Caption = 'Work Type'
        DataBinding.FieldName = 'work_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 2209
      end
      object ColWorkAddressNumber: TcxGridDBColumn
        Caption = 'Street #'
        DataBinding.FieldName = 'work_address_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Options.Grouping = False
        Width = 76
      end
      object ColWorkAddressStreet: TcxGridDBColumn
        Caption = 'Street'
        DataBinding.FieldName = 'work_address_street'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Options.Grouping = False
        Width = 200
      end
      object ColLegalGoodThru: TcxGridDBColumn
        Caption = 'Legal Thru'
        DataBinding.FieldName = 'legal_good_thru'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 452
      end
      object ColWorkCity: TcxGridDBColumn
        Caption = 'City'
        DataBinding.FieldName = 'work_city'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Width = 152
      end
      object ColMapPage: TcxGridDBColumn
        Caption = 'Map Page'
        DataBinding.FieldName = 'map_page'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 503
      end
      object ColWorkCounty: TcxGridDBColumn
        Caption = 'County'
        DataBinding.FieldName = 'work_county'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        Options.Filtering = False
        Width = 118
      end
      object ColTicketFormat: TcxGridDBColumn
        Caption = 'Ticket Format'
        DataBinding.FieldName = 'ticket_format'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 503
      end
      object ColLocates: TcxGridDBColumn
        Caption = 'Locates'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        OnCustomDrawCell = ColCustomDrawCell
        OnGetDisplayText = ColLocatesGetDisplayText
        Options.Filtering = False
        Options.Grouping = False
        Width = 124
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object BtnFind: TcxButtonEdit
    Left = 233
    Top = 4
    Properties.Buttons = <
      item
        Action = SearchAction
        Default = True
        Kind = bkText
      end
      item
        Action = ClearSearchAction
        Kind = bkText
      end>
    TabOrder = 2
    OnEditing = BtnFindEditing
    Width = 241
  end
  object OldTickets: TDBISAMQuery
    AutoCalcFields = False
    AfterScroll = OldTicketsAfterScroll
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ParamCheck = False
    SQL.Strings = (
      
        'select ticket_id, ticket_number, kind, due_date, ticket_type, co' +
        'mpany,'
      
        '  priority, work_type, work_city, work_address_street, work_coun' +
        'ty,'
      '  work_description, legal_good_thru, map_page, ticket_format, '
      '  work_address_number'
      'from ticket where ticket_id in'
      '(select distinct locate.ticket_id'
      ' from locate where locate.locator_id=:id'
      '  and locate.active'
      '  and locate.closed)'
      'order by due_date desc')
    Params = <
      item
        DataType = ftInteger
        Name = 'id'
      end>
    ReadOnly = True
    Left = 16
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id'
      end>
  end
  object TicketLocates: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    IndexName = 'ticket_id'
    MasterFields = 'ticket_id'
    TableName = 'locate'
    Left = 56
    Top = 72
  end
  object TicketSource: TDataSource
    DataSet = OldTickets
    Left = 16
    Top = 104
  end
  object OldTicketsActionList: TActionList
    Left = 504
    Top = 8
    object SearchAction: TAction
      Caption = 'Search'
      OnExecute = SearchActionExecute
    end
    object ClearSearchAction: TAction
      Caption = 'Clear'
      OnExecute = ClearSearchActionExecute
    end
  end
end
