inherited WOOHMFrame: TWOOHMFrame
  Width = 742
  Height = 601
  inherited DetailsPanel: TPanel
    Width = 742
    Height = 147
    inherited DueDateLabel: TLabel
      Top = 47
      Width = 49
      Caption = 'Due Date:'
    end
    inherited TransmitDateLabel: TLabel
      Top = 63
      Width = 86
      Caption = 'Transaction Date:'
    end
    object AccountNumber: TDBText [2]
      Left = 512
      Top = 63
      Width = 93
      Height = 15
      DataField = 'account_number'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object ComplianceDueDate: TDBText [3]
      Left = 112
      Top = 31
      Width = 114
      Height = 13
      AutoSize = True
      DataField = 'compliance_due_date'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    inherited TransmitDate: TDBText
      Left = 112
      Top = 63
    end
    object AssignedToLabel: TLabel [5]
      Left = 3
      Top = 0
      Width = 62
      Height = 13
      Caption = 'Assigned To:'
      Transparent = True
    end
    object AssignedTo: TDBText [6]
      Left = 112
      Top = 0
      Width = 65
      Height = 13
      AutoSize = True
      DataField = 'employee_name'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object Label4: TLabel [7]
      Left = 406
      Top = 63
      Width = 83
      Height = 13
      Caption = 'Account Number:'
      Transparent = True
    end
    object PremiseIDLabel: TLabel [8]
      Left = 406
      Top = 79
      Width = 55
      Height = 13
      Caption = 'Premise ID:'
      Transparent = True
    end
    object PremiseID: TDBText [9]
      Left = 512
      Top = 79
      Width = 93
      Height = 17
      DataField = 'premise_id'
      DataSource = WorkOrderInspectionDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = ItemDblClick
    end
    object ComplianceDueDateLabel: TLabel [10]
      Left = 3
      Top = 31
      Width = 106
      Height = 13
      Caption = 'Compliance Due Date:'
      Transparent = True
    end
    inherited DueDate: TDBText
      Left = 112
      Top = 47
    end
    inherited ClientCode: TDBText
      Left = 512
      Top = 15
    end
    inherited ClientCodeLabel: TLabel
      Left = 406
      Top = 15
    end
    inherited ClientNameLabel: TLabel
      Left = 406
      Top = 31
    end
    inherited ClientName: TDBText
      Left = 512
      Top = 31
    end
    inherited ClientWONumberLabel: TLabel
      Left = 406
      Top = 47
      Visible = False
    end
    inherited ClientWONumber: TDBText
      Left = 512
      Top = 47
      Visible = False
    end
    inherited ClientOrderNumLabel: TLabel
      Left = 406
    end
    inherited ClientOrderNum: TDBText
      Left = 512
    end
    inherited WorkLong: TDBText
      Left = 192
      Top = 15
    end
    inherited WorkLat: TDBText
      Left = 112
      Top = 15
    end
    inherited TerminalGPSLabel: TLabel
      Top = 15
      Width = 51
      Caption = 'Lat   Long:'
    end
    inherited lblWO_ID: TLabel
      Left = 603
      Top = 15
    end
    inherited WorkOrderID: TDBText
      Left = 646
      Top = 15
    end
    inherited CustomerPanel: TPanel
      Top = 92
      Width = 742
      Height = 55
      object CallerPhoneLabel: TLabel [0]
        Left = 443
        Top = 20
        Width = 34
        Height = 13
        Caption = 'Phone:'
        Transparent = True
      end
      object AtlPhoneLabel: TLabel [1]
        Left = 443
        Top = 36
        Width = 50
        Height = 13
        Caption = 'Alt Phone:'
        Transparent = True
      end
      object Phone: TDBText [2]
        Left = 512
        Top = 20
        Width = 93
        Height = 17
        DataField = 'caller_phone'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object AltPhone: TDBText [3]
        Left = 512
        Top = 36
        Width = 93
        Height = 17
        DataField = 'caller_altphone'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      inherited WorkAddressLabel: TLabel
        Top = 20
      end
      inherited WorkAddressNumber: TDBText
        Left = 112
        Top = 20
        Width = 88
      end
      inherited City: TDBText
        Left = 112
        Top = 39
      end
      inherited WorkAddressStreet: TDBText
        Left = 206
        Top = 20
        Width = 231
      end
      inherited State: TDBText
        Left = 285
        Top = 36
        Width = 40
      end
      inherited Zip: TDBText
        Left = 346
        Top = 36
      end
      inherited CallerNameLabel: TLabel
        Top = 2
      end
      inherited CallerName: TDBText
        Left = 112
        Top = 2
      end
    end
  end
  inherited FeedDetailsPanel: TPanel
    Top = 147
    Width = 742
    Height = 150
    inherited MapPageLabel: TLabel
      Left = 376
      Top = -2
      Visible = False
    end
    inherited MapPage: TDBText
      Left = 411
      Top = -2
      Visible = False
    end
    inherited MapRef: TDBText
      Left = 587
      Top = -2
      Visible = False
    end
    inherited MapPageGrid: TLabel
      Left = 528
      Top = -2
      Width = 53
      Caption = 'Quad Map:'
      Visible = False
    end
    object pcOHM: TPageControl
      Left = 0
      Top = 0
      Width = 742
      Height = 150
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      ActivePage = tsPremise
      Align = alClient
      TabOrder = 0
      object tsGasMain: TTabSheet
        Caption = 'Gas Main'
        object pnlGasMain: TPanel
          Left = 0
          Top = 0
          Width = 734
          Height = 122
          Align = alClient
          BevelEdges = []
          BevelOuter = bvNone
          TabOrder = 0
          object Label17: TLabel
            Left = 303
            Top = 39
            Width = 42
            Height = 13
            Caption = 'Material:'
          end
          object Label19: TLabel
            Left = 303
            Top = 13
            Width = 87
            Height = 13
            Caption = 'Measured Length:'
          end
          object Label20: TLabel
            Left = 10
            Top = 42
            Width = 68
            Height = 13
            Caption = 'Coating Type:'
          end
          object Label21: TLabel
            Left = 10
            Top = 68
            Width = 59
            Height = 13
            Caption = 'Install Date:'
          end
          object Label22: TLabel
            Left = 10
            Top = 15
            Width = 87
            Height = 13
            Caption = 'Nominal Diameter:'
          end
          object COATINGTYP: TcxDBTextEdit
            Left = 105
            Top = 37
            DataBinding.DataField = 'COATINGTYP'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 0
            Width = 176
          end
          object INSTALLEDD: TcxDBTextEdit
            Left = 105
            Top = 64
            DataBinding.DataField = 'INSTALLEDD'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 1
            Width = 176
          end
          object NOMINALDIA: TcxDBTextEdit
            Left = 105
            Top = 10
            DataBinding.DataField = 'NOMINALDIA'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 2
            Width = 176
          end
          object MATERIAL: TcxDBTextEdit
            Left = 398
            Top = 35
            DataBinding.DataField = 'MATERIAL'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 3
            Width = 176
          end
          object MEASUREDLE: TcxDBTextEdit
            Left = 398
            Top = 10
            DataBinding.DataField = 'MEASUREDLE'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 4
            Width = 176
          end
        end
      end
      object tsPremise: TTabSheet
        Caption = 'Premise'
        ImageIndex = 1
        object pnlPremise: TPanel
          Left = 0
          Top = 0
          Width = 734
          Height = 122
          Align = alClient
          BevelEdges = []
          BevelOuter = bvNone
          TabOrder = 0
          object Label16: TLabel
            Left = 6
            Top = 80
            Width = 75
            Height = 13
            Caption = 'Meter Location:'
          end
          object Label15: TLabel
            Left = 405
            Top = 34
            Width = 122
            Height = 13
            Caption = 'Main Reference Location:'
          end
          object Label14: TLabel
            Left = 206
            Top = 57
            Width = 67
            Height = 13
            Caption = 'Main Material:'
          end
          object Label13: TLabel
            Left = 206
            Top = 34
            Width = 71
            Height = 13
            Caption = 'Main Pressure:'
          end
          object Label12: TLabel
            Left = 206
            Top = 11
            Width = 84
            Height = 13
            Caption = 'Service Pressure:'
          end
          object Label11: TLabel
            Left = 405
            Top = 57
            Width = 91
            Height = 13
            Caption = 'Customer Material:'
          end
          object Label10: TLabel
            Left = 206
            Top = 80
            Width = 95
            Height = 13
            Caption = 'Customer Pipe Size:'
          end
          object Label9: TLabel
            Left = 6
            Top = 102
            Width = 59
            Height = 13
            Caption = 'Install Date:'
          end
          object Label8: TLabel
            Left = 405
            Top = 80
            Width = 123
            Height = 13
            Caption = 'Company Service Length:'
          end
          object Label7: TLabel
            Left = 405
            Top = 11
            Width = 128
            Height = 13
            Caption = 'Company Service Material:'
          end
          object Label6: TLabel
            Left = 6
            Top = 34
            Width = 94
            Height = 13
            Caption = 'Company Pipe Size:'
          end
          object Label5: TLabel
            Left = 6
            Top = 57
            Width = 72
            Height = 13
            Caption = 'Meter Number:'
          end
          object Label3: TLabel
            Left = 6
            Top = 11
            Width = 91
            Height = 13
            Caption = 'Curb Box Location:'
          end
          object Label23: TLabel
            Left = 404
            Top = 102
            Width = 100
            Height = 13
            Caption = 'Curb Valve Found:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object mai_1: TcxDBTextEdit
            Left = 309
            Top = 54
            DataBinding.DataField = 'mai_1'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 0
            Width = 83
          end
          object sl_pi: TcxDBTextEdit
            Left = 309
            Top = 77
            DataBinding.DataField = 'sl_pi'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 1
            Width = 83
          end
          object slin_s: TcxDBTextEdit
            Left = 110
            Top = 100
            DataBinding.DataField = 'slin_s'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 2
            Width = 83
          end
          object slp_2: TcxDBTextEdit
            Left = 539
            Top = 77
            DataBinding.DataField = 'slp_2'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 3
            Width = 86
          end
          object slp_1: TcxDBTextEdit
            Left = 539
            Top = 8
            DataBinding.DataField = 'slp_1'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 4
            Width = 86
          end
          object slpi_p: TcxDBTextEdit
            Left = 110
            Top = 31
            DataBinding.DataField = 'slpi_p'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 5
            Width = 83
          end
          object meter_1: TcxDBTextEdit
            Left = 110
            Top = 54
            DataBinding.DataField = 'meter_1'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 6
            Width = 83
          end
          object curb_b: TcxDBTextEdit
            Left = 110
            Top = 8
            DataBinding.DataField = 'curb_b'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 7
            Width = 83
          end
          object meter_2: TcxDBTextEdit
            Left = 110
            Top = 77
            DataBinding.DataField = 'meter_2'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 8
            Width = 83
          end
          object main_r: TcxDBTextEdit
            Left = 539
            Top = 31
            DataBinding.DataField = 'main_r'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 9
            Width = 86
          end
          object main_p: TcxDBTextEdit
            Left = 309
            Top = 31
            DataBinding.DataField = 'main_p'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 10
            Width = 83
          end
          object slpre: TcxDBTextEdit
            Left = 309
            Top = 8
            DataBinding.DataField = 'slpre'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 11
            Width = 83
          end
          object slp_3: TcxDBTextEdit
            Left = 539
            Top = 54
            DataBinding.DataField = 'slp_3'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            Style.Color = clBtnFace
            TabOrder = 12
            Width = 86
          end
          object dbCkBox_has_c: TcxDBCheckBox
            Left = 199
            Top = 99
            Caption = 'Has curb valve'
            DataBinding.DataField = 'has_c'
            DataBinding.DataSource = dsWorkOrderOHMdetails
            ParentBackground = False
            ParentColor = False
            Properties.Alignment = taLeftJustify
            TabOrder = 13
            Width = 106
          end
          object cmbxCurbValueFound: TcxComboBox
            Left = 539
            Top = 99
            Properties.Items.Strings = (
              '--'
              'YES'
              'NO')
            TabOrder = 14
            Text = 'cbCurbValueFound'
            Width = 86
          end
        end
      end
    end
  end
  inherited EditingPanel: TPanel
    Top = 297
    Width = 742
    Height = 274
    Align = alTop
    object AOCInspectionLabel: TLabel
      Left = 4
      Top = 55
      Width = 79
      Height = 13
      Caption = 'AOC Inspection:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 334
      Top = 157
      Width = 86
      Height = 13
      Caption = 'Potential Hazard: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 423
      Top = 156
      Width = 180
      Height = 13
      Caption = ' Immediate Communication Required  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      ParentFont = False
      Transparent = True
    end
    object CustInspectionLabel: TLabel
      Left = 334
      Top = 55
      Width = 171
      Height = 13
      Caption = 'Customer Inspection: (Orange Tag)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object InitialInspectionPanel: TPanel
      Left = 0
      Top = 0
      Width = 742
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object InitialInspectionLabel: TLabel
        Left = 3
        Top = 0
        Width = 83
        Height = 13
        Caption = 'Initial Inspection:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
      end
      object DateOfVisitLabel: TLabel
        Left = 3
        Top = 19
        Width = 62
        Height = 13
        Caption = 'Date of Visit:'
      end
      object CGACountLabel: TLabel
        Left = 3
        Top = 34
        Width = 57
        Height = 13
        Caption = 'CGA Count:'
      end
      object CompletionCodeLabel: TLabel
        Left = 191
        Top = 20
        Width = 85
        Height = 13
        Caption = 'Completion Code:'
      end
      object DateOfVisit: TDBText
        Left = 75
        Top = 20
        Width = 63
        Height = 13
        AutoSize = True
        DataField = 'status_date'
        DataSource = WorkOrderDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object CGACount: TDBText
        Left = 75
        Top = 34
        Width = 65
        Height = 19
        DataField = 'cga_visits'
        DataSource = WorkOrderInspectionDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = ItemDblClick
      end
      object CGAReasonLabel: TLabel
        Left = 423
        Top = 20
        Width = 67
        Height = 13
        Caption = 'CGA Reason: '
      end
      object CompletionCodeCombobox: TDBLookupComboBox
        Left = 282
        Top = 17
        Width = 118
        Height = 21
        BiDiMode = bdLeftToRight
        DataField = 'status'
        DataSource = WorkOrderDS
        DropDownWidth = 200
        KeyField = 'status'
        ListField = 'status;status_name'
        ListSource = StatusSource
        ParentBiDiMode = False
        TabOrder = 0
      end
      object CGAReasonCombo: TcxDBExtLookupComboBox
        Left = 500
        Top = 17
        DataBinding.DataField = 'cga_reason'
        DataBinding.DataSource = WorkOrderInspectionDS
        Properties.DropDownAutoSize = True
        Properties.View = CGAReasonView
        Properties.KeyFieldNames = 'code'
        Properties.ListFieldItem = CGAReasonDescription
        TabOrder = 1
        OnEditing = ComboBoxEditing
        Width = 118
      end
    end
    object PotentialHazardCheckList: TcxCheckListBox
      Left = 334
      Top = 171
      Width = 272
      Height = 78
      Items = <>
      TabOrder = 7
      OnClickCheck = RemedyCheckListClickCheck
    end
    object AOCInspectionCheckList: TcxCheckListBox
      Left = 4
      Top = 102
      Width = 294
      Height = 147
      Items = <
        item
          Text = 'Item 1'
        end
        item
          Text = 'Item 2'
        end
        item
          Text = 'Item 3'
        end>
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      OnClickCheck = RemedyCheckListClickCheck
    end
    object CustomerInspectionCheckList: TcxCheckListBox
      Left = 334
      Top = 70
      Width = 272
      Height = 81
      Items = <>
      TabOrder = 6
      OnClickCheck = RemedyCheckListClickCheck
    end
    object VentClearanceDistCombobox: TcxDBExtLookupComboBox
      Left = 234
      Top = 61
      DataBinding.DataField = 'vent_clearance_dist'
      DataBinding.DataSource = WorkOrderInspectionDS
      Enabled = False
      Properties.DropDownAutoSize = True
      Properties.View = VentClearanceView
      Properties.KeyFieldNames = 'code'
      Properties.ListFieldItem = VentClearanceDescription
      TabOrder = 2
      OnEditing = VentClearanceDistComboboxEditing
      Width = 64
    end
    object VentIgnitionDistComboBox: TcxDBExtLookupComboBox
      Left = 234
      Top = 82
      DataBinding.DataField = 'vent_ignition_dist'
      DataBinding.DataSource = WorkOrderInspectionDS
      Enabled = False
      Properties.DropDownAutoSize = True
      Properties.View = VentIgnitionView
      Properties.KeyFieldNames = 'code'
      Properties.ListFieldItem = VentIgnitionDescription
      TabOrder = 4
      OnEditing = VentClearanceDistComboboxEditing
      Width = 64
    end
    object InadequateVentClearance: TcxCheckBox
      Left = 2
      Top = 66
      Caption = 'Inadequate Vent Clearance'
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      OnClick = InadequateVentClearanceClick
      Width = 228
    end
    object InadequateVentClearanceIg: TcxCheckBox
      Left = 2
      Top = 81
      Caption = 'Inadequate Vent Clearance Ignition'
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      OnClick = ItemDblClick
      Width = 228
    end
  end
  inherited ButtonPanel: TPanel
    Top = 571
    Width = 742
    Height = 30
    Align = alClient
    inherited ClosedDateLabel: TLabel
      Left = 315
    end
    inherited ClosedDate: TDBText
      Left = 386
    end
    inherited PlatAddinButton: TButton
      Left = 476
      Top = 3
    end
  end
  inherited StatusList: TDBISAMQuery
    OnFilterRecord = nil
    Left = 510
    Top = 488
  end
  inherited StatusSource: TDataSource
    DataSet = StatusList
    Left = 525
    Top = 472
  end
  inherited WorkOrderDS: TDataSource
    Left = 518
    Top = 512
  end
  object WorkOrderInspectionDS: TDataSource
    DataSet = DM.WorkOrderInspection
    Left = 366
    Top = 8
  end
  object Remedies: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'select wt.work_type, wt.work_description, wt.flag_color, wt.aler' +
        't, wor.*'
      'from'
      'work_order_work_type wt'
      'JOIN work_order_remedy wor'
      'on wor.work_type_id= wt.work_type_id'
      'where (wor.wo_id=:WOID)'
      '  and wor.active')
    Params = <
      item
        DataType = ftUnknown
        Name = 'WOID'
      end>
    ReadOnly = True
    Left = 254
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'WOID'
      end>
  end
  object WorkOrderRemedyDS: TDataSource
    AutoEdit = False
    DataSet = DM.WorkOrderRemedy
    Left = 125
    Top = 528
  end
  object WorkTypes: TDBISAMQuery
    Tag = 999
    Filtered = True
    OnFilterRecord = WorkTypesFilterRecord
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'Select * '
      'from work_order_work_type'
      'where active = True'
      'order by work_type_id'
      '')
    Params = <>
    ReadOnly = True
    Left = 518
    Top = 424
  end
  object ViewRepo: TcxGridViewRepository
    Left = 352
    Top = 80
    object BuildingView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = BuildingRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object BuildingDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object MapStatusView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = MapStatusRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object MapStatusDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object MeterLocationView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = MeterLocationRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object MeterLocationDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object YnubView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = YnubRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object YnubDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object VentClearanceView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = VentClearanceRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object VentClearanceDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object VentIgnitionView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = VentIgnitionRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnVertSizing = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object VentIgnitionDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object CGAReasonView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = CGAReasonRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object CGAReasonDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object GasLightView: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = GasLightRefSource
      DataController.KeyFieldNames = 'code'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLineColor = clBtnShadow
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      object GasLightDescription: TcxGridDBBandedColumn
        DataBinding.FieldName = 'description'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
  end
  object BuildingRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'bldtype'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 560
  end
  object BuildingRefSource: TDataSource
    AutoEdit = False
    DataSet = BuildingRef
    Left = 205
    Top = 560
  end
  object MapStatusRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'mapstat'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 528
  end
  object MapStatusRefSource: TDataSource
    AutoEdit = False
    DataSet = MapStatusRef
    Left = 205
    Top = 528
  end
  object MeterLocationRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'metrloc'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 496
  end
  object MeterLocationRefSource: TDataSource
    AutoEdit = False
    DataSet = MeterLocationRef
    Left = 205
    Top = 496
  end
  object YnubRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ynub'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 464
  end
  object YnubRefSource: TDataSource
    AutoEdit = False
    DataSet = YnubRef
    Left = 205
    Top = 464
  end
  object VentClearanceRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ventclr'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 432
  end
  object VentClearanceRefSource: TDataSource
    AutoEdit = False
    DataSet = VentClearanceRef
    Left = 205
    Top = 432
  end
  object VentIgnitionRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'ventign'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 174
    Top = 400
  end
  object VentIgnitionRefSource: TDataSource
    AutoEdit = False
    DataSet = VentIgnitionRef
    Left = 205
    Top = 400
  end
  object UpdateWORemedy: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'update work_order_remedy'
      'set active=:active, DeltaStatus='#39'U'#39
      'where wo_id=:wo_id and work_type_id=:work_type_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'active'
      end
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end>
    ReadOnly = True
    Left = 358
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'active'
      end
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end>
  end
  object InsertWORemedy: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'insert into work_order_remedy(wo_id, work_type_id, active, modif' +
        'ied_date)'
      'values(:wo_id, :work_type_id, 1, :current)')
    Params = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end
      item
        DataType = ftDateTime
        Name = 'current'
      end>
    ReadOnly = True
    Left = 406
    Top = 472
    ParamData = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end
      item
        DataType = ftInteger
        Name = 'work_type_id'
      end
      item
        DataType = ftDateTime
        Name = 'current'
      end>
  end
  object CGAReasonRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'cgareason'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 246
    Top = 520
  end
  object CGAReasonRefSource: TDataSource
    AutoEdit = False
    DataSet = CGAReasonRef
    Left = 277
    Top = 520
  end
  object GasLightRef: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      'select * from reference'
      'where type='#39'gaslight'#39
      'and active_ind=1'
      'order by sortby')
    Params = <>
    ReadOnly = True
    Left = 246
    Top = 488
  end
  object GasLightRefSource: TDataSource
    AutoEdit = False
    DataSet = GasLightRef
    Left = 277
    Top = 488
  end
  object WorkOrderOHMdetails: TDBISAMTable
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    ReadOnly = True
    TableName = 'work_order_OHM_details'
    Left = 612
    Top = 492
  end
  object dsWorkOrderOHMdetails: TDataSource
    DataSet = WorkOrderOHMdetails
    Left = 616
    Top = 536
  end
end
