inherited WOLAM01Frame: TWOLAM01Frame
  Width = 748
  Height = 552
  OnEnter = FrameEnter
  inherited DetailsPanel: TPanel
    Width = 748
    Height = 129
    inherited DueDateLabel: TLabel
      Left = 4
    end
    inherited TransmitDateLabel: TLabel
      Left = 4
    end
    object ClientMasterOrderNum: TDBText [2]
      Left = 479
      Top = 17
      Width = 129
      Height = 13
      AutoSize = True
      DataField = 'client_master_order_num'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object ClientMasterOrderNumLabel: TLabel [3]
      Left = 374
      Top = 17
      Width = 57
      Height = 13
      Caption = 'Client MON:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object JobNumberLabel: TLabel [5]
      Left = 4
      Top = 0
      Width = 35
      Height = 13
      Caption = 'Job ID:'
      Transparent = True
    end
    object JobNumber: TDBText [6]
      Left = 94
      Top = 0
      Width = 64
      Height = 13
      AutoSize = True
      DataField = 'job_number'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    inherited ClientCode: TDBText
      Left = 479
    end
    inherited ClientCodeLabel: TLabel
      Left = 374
    end
    inherited ClientNameLabel: TLabel
      Left = 374
    end
    inherited ClientName: TDBText
      Left = 479
    end
    inherited ClientWONumberLabel: TLabel
      Left = 374
    end
    inherited ClientWONumber: TDBText
      Left = 479
    end
    inherited ClientOrderNumLabel: TLabel
      Left = 374
    end
    inherited ClientOrderNum: TDBText
      Left = 479
    end
    inherited WorkLong: TDBText
      Left = 179
      Top = 56
    end
    inherited WorkLat: TDBText
      Top = 55
    end
    inherited TerminalGPSLabel: TLabel
      Left = 4
      Top = 56
    end
    inherited lblWO_ID: TLabel
      Left = 593
      OnDblClick = ItemDblClick
    end
    inherited CustomerPanel: TPanel
      Top = 78
      Width = 748
      Height = 47
      Align = alNone
      inherited WorkAddressLabel: TLabel
        Left = 4
      end
      inherited WorkAddressNumber: TDBText
        Left = 94
      end
      inherited City: TDBText
        Left = 94
      end
      inherited WorkAddressStreet: TDBText
        Left = 172
      end
      inherited State: TDBText
        Left = 218
      end
      inherited Zip: TDBText
        Left = 307
      end
      inherited CallerNameLabel: TLabel
        Left = 4
      end
      inherited CallerName: TDBText
        Left = 94
      end
    end
  end
  inherited FeedDetailsPanel: TPanel
    Top = 129
    Width = 748
    inherited MapPageLabel: TLabel
      Left = 479
    end
    inherited MapPage: TDBText
      Left = 509
    end
    inherited MapRef: TDBText
      Left = 509
    end
    inherited MapPageGrid: TLabel
      Left = 479
    end
    object ServingTerminalLabel: TLabel
      Left = 3
      Top = 10
      Width = 83
      Height = 13
      Caption = 'Serving Terminal:'
      Transparent = True
    end
    object ServingTerminal: TDBText
      Left = 94
      Top = 10
      Width = 93
      Height = 13
      AutoSize = True
      DataField = 'serving_terminal'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object CircuitNumberLabel: TLabel
      Left = 3
      Top = 44
      Width = 48
      Height = 13
      Caption = 'Circuit ID:'
      Transparent = True
    end
    object CircuitNumber: TDBText
      Left = 94
      Top = 44
      Width = 80
      Height = 13
      AutoSize = True
      DataField = 'circuit_number'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object WorkTypeLabel: TLabel
      Left = 3
      Top = 61
      Width = 85
      Height = 13
      Caption = 'Work to be Done:'
      Transparent = True
    end
    object WorkType: TDBText
      Left = 94
      Top = 61
      Width = 665
      Height = 13
      DataField = 'work_type'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      WordWrap = True
      OnDblClick = ItemDblClick
    end
    object F2CableLabel: TLabel
      Left = 306
      Top = 10
      Width = 46
      Height = 13
      Caption = 'F2 Cable:'
      Transparent = True
    end
    object F2Cable: TDBText
      Left = 375
      Top = 10
      Width = 44
      Height = 13
      AutoSize = True
      DataField = 'f2_cable'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object TerminalPort: TDBText
      Left = 375
      Top = 27
      Width = 74
      Height = 13
      AutoSize = True
      DataField = 'terminal_port'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object TerminalPortLabel: TLabel
      Left = 306
      Top = 27
      Width = 67
      Height = 13
      Caption = 'Terminal Port:'
      Transparent = True
    end
    object F2PairLabel: TLabel
      Left = 306
      Top = 44
      Width = 37
      Height = 13
      Caption = 'F2 Pair:'
      Transparent = True
    end
    object F2Pair: TDBText
      Left = 375
      Top = 44
      Width = 35
      Height = 13
      AutoSize = True
      DataField = 'f2_pair'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
  end
  inherited EditingPanel: TPanel
    Top = 209
    Width = 748
    Height = 303
    object WorkDescriptionLabel: TLabel
      Left = 3
      Top = 5
      Width = 75
      Height = 13
      Caption = 'Extent of work:'
      Transparent = True
    end
    object DrivewayBoresLabel: TLabel
      Left = 3
      Top = 101
      Width = 82
      Height = 13
      Caption = 'Driveway Bores: '
    end
    object RoadwayBoresLabel: TLabel
      Left = 153
      Top = 101
      Width = 79
      Height = 13
      Caption = 'Roadway Bores:'
    end
    object WorkCrossLabel: TLabel
      Left = 3
      Top = 76
      Width = 64
      Height = 13
      Caption = 'Cross Street:'
      Transparent = True
    end
    object StatusLabel: TLabel
      Left = 3
      Top = 126
      Width = 35
      Height = 13
      Caption = 'Status:'
    end
    object DiggingWithinStateROWLabel: TLabel
      Left = 305
      Top = 101
      Width = 176
      Height = 13
      Caption = 'Digging within 15'#39' of State Highway: '
    end
    object RelatedTicketsLabel: TLabel
      Left = 458
      Top = 5
      Width = 40
      Height = 26
      Alignment = taRightJustify
      Caption = 'Related Tickets:'
      Transparent = True
      WordWrap = True
    end
    object SourceSentAttachment: TLabel
      Left = 94
      Top = 147
      Width = 111
      Height = 13
      Caption = 'SourceSentAttachment'
    end
    object AssignedToLabel: TLabel
      Left = 480
      Top = 126
      Width = 62
      Height = 13
      Caption = 'Assigned To:'
      Transparent = True
    end
    object AssignedTo: TDBText
      Left = 548
      Top = 126
      Width = 65
      Height = 13
      AutoSize = True
      DataField = 'employee_name'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clBtnText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnDblClick = ItemDblClick
    end
    object Label1: TLabel
      Left = 94
      Top = 168
      Width = 225
      Height = 16
      Caption = 'Locates associated with selected ticket:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dbTikNoLabel: TDBText
      Left = 338
      Top = 168
      Width = 115
      Height = 17
      DataField = 'ticket_number'
      DataSource = TicketsDS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object WorkDescription: TDBMemo
      Left = 94
      Top = 2
      Width = 365
      Height = 66
      DataField = 'work_description'
      DataSource = WorkOrderDS
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object WorkCross: TDBEdit
      Left = 94
      Top = 72
      Width = 178
      Height = 21
      CharCase = ecUpperCase
      Ctl3D = True
      DataField = 'work_cross'
      DataSource = WorkOrderDS
      ParentCtl3D = False
      TabOrder = 1
    end
    object DiggingWithinStateROW: TDBComboBox
      Left = 480
      Top = 97
      Width = 82
      Height = 21
      Style = csDropDownList
      DataField = 'state_hwy_row'
      DataSource = WorkOrderDS
      ItemHeight = 13
      TabOrder = 2
    end
    object DrivewayBores: TcxDBSpinEdit
      Left = 94
      Top = 97
      DataBinding.DataField = 'driveway_bore_count'
      DataBinding.DataSource = WorkOrderDS
      Properties.AssignedValues.MinValue = True
      Properties.EditFormat = '####0'
      TabOrder = 3
      Width = 56
    end
    object RoadwayBores: TcxDBSpinEdit
      Left = 234
      Top = 97
      DataBinding.DataField = 'road_bore_count'
      DataBinding.DataSource = WorkOrderDS
      Properties.AssignedValues.MinValue = True
      Properties.EditFormat = '####0'
      Properties.OnChange = RoadwayBoresPropertiesChange
      Properties.OnValidate = RoadwayBoresPropertiesValidate
      TabOrder = 4
      Width = 56
    end
    object RelatedTickets: TDBGrid
      Left = 504
      Top = 0
      Width = 145
      Height = 91
      TabStop = False
      DataSource = TicketsDS
      Options = [dgTitles, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      PopupMenu = TicketPopupMenu
      ReadOnly = True
      TabOrder = 5
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = RelatedTicketsDblClick
      Columns = <
        item
          ButtonStyle = cbsEllipsis
          Expanded = False
          FieldName = 'ticket_number'
          Title.Caption = 'Ticket #'
          Width = 141
          Visible = True
        end>
    end
    object StatusCombo: TDBLookupComboBox
      Left = 94
      Top = 122
      Width = 178
      Height = 21
      DataField = 'status'
      DataSource = WorkOrderDS
      KeyField = 'status'
      ListField = 'status;status_name'
      ListSource = StatusSource
      TabOrder = 6
      OnCloseUp = StatusComboCloseUp
    end
    object DBGrid1: TDBGrid
      Left = 94
      Top = 184
      Width = 567
      Height = 119
      DataSource = LocatorWO_Ticket_DS
      TabOrder = 7
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'client_code'
          Title.Caption = 'Client'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'status'
          Title.Caption = 'Status'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'short_name'
          Title.Caption = 'Assigned'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'closed'
          Title.Caption = 'Closed'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'closed_how'
          Title.Caption = 'How closed'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'closed_date'
          Title.Caption = 'Closed Date'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'active'
          Title.Caption = 'Active'
          Visible = True
        end>
    end
  end
  inherited ButtonPanel: TPanel
    Top = 512
    Width = 748
    inherited ClosedDateLabel: TLabel
      Left = 270
    end
    inherited ClosedDate: TDBText
      Left = 336
      Width = 138
    end
  end
  inherited StatusList: TDBISAMQuery
    OnFilterRecord = nil
  end
  inherited StatusSource: TDataSource
    DataSet = StatusList
  end
  inherited WorkOrderDS: TDataSource
    Left = 334
    Top = 8
  end
  object Tickets: TDBISAMQuery
    Tag = 999
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    RequestLive = True
    SQL.Strings = (
      'select t.ticket_number,  t. ticket_id'
      'from ticket t'
      'join work_order_ticket w on w.ticket_id = t. ticket_id'
      'where'
      '  w.wo_id = :wo_id and'
      '  w.active')
    Params = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end>
    Left = 46
    Top = 408
    ParamData = <
      item
        DataType = ftInteger
        Name = 'wo_id'
      end>
  end
  object TicketsDS: TDataSource
    DataSet = Tickets
    OnDataChange = TicketsDSDataChange
    Left = 13
    Top = 408
  end
  object TicketPopupMenu: TPopupMenu
    OnPopup = TicketPopupMenuPopup
    Left = 664
    Top = 208
    object CopyTicket1: TMenuItem
      Caption = 'Copy Ticket #'
      OnClick = CopyTicket1Click
    end
  end
  object LocatorWO_Ticket_DS: TDataSource
    DataSet = LocatorWO_Ticket
    Left = 13
    Top = 443
  end
  object LocatorWO_Ticket: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DB1'
    EngineVersion = '4.34 Build 7'
    SQL.Strings = (
      
        'select locate.*, locate.locator_id, employee.short_name, locate.' +
        'ticket_id'
      ' from locate'
      '  left join employee on locate.locator_id=employee.emp_id'
      ' where locate.active'
      'and locate.ticket_id = :ticket_id')
    Params = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    Left = 44
    Top = 444
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ticket_id'
      end>
    object StringField1: TStringField
      FieldName = 'client_code'
      Origin = 'locate.client_code'
      Size = 10
    end
    object StringField2: TStringField
      FieldName = 'status'
      Origin = 'locate.status'
      Size = 5
    end
    object StringField3: TStringField
      FieldName = 'short_name'
      Origin = 'employee.short_name'
    end
    object BooleanField2: TBooleanField
      FieldName = 'closed'
      Origin = 'locate.closed'
    end
    object StringField4: TStringField
      FieldName = 'closed_how'
      Size = 10
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'closed_date'
    end
    object BooleanField3: TBooleanField
      FieldName = 'active'
    end
  end
end
