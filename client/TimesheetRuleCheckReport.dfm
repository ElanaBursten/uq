inherited TimesheetRuleCheckReportForm: TTimesheetRuleCheckReportForm
  Left = 344
  Top = 276
  Caption = 'TimesheetRuleCheckReportForm'
  ClientHeight = 173
  ClientWidth = 324
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 66
    Height = 13
    Caption = 'Week Ending:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 41
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object DepthLabel: TLabel [2]
    Left = 0
    Top = 71
    Width = 82
    Height = 13
    Caption = 'Hierarchy Depth:'
  end
  object Label6: TLabel [3]
    Left = 0
    Top = 101
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [4]
    Left = 93
    Top = 38
    Width = 187
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox [5]
    Left = 93
    Top = 98
    Width = 187
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  object ChooseDate: TcxDateEdit [6]
    Left = 93
    Top = 8
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Properties.OnEditValueChanged = ChooseDateChange
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 88
  end
  object Depth: TSpinEdit [7]
    Left = 93
    Top = 68
    Width = 57
    Height = 22
    MaxValue = 200
    MinValue = 1
    TabOrder = 2
    Value = 99
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 1
  end
end
