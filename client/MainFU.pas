unit MainFU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, ComCtrls, OdEmbeddable, StdActns, ActnList,
  Buttons, QMConst, WinSessionNotify, OdStillImage, OdTimeKeeper,
  OdDeviceNotification, UploadThread, OdIdleTimer, TicketWorkView, GPSThread,
  BackgroundWorker, cxGraphics, cxControls, cxLookAndFeels,  ShellAPI, Hashes,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxProgressBar,
  ToolWin;

{$IFOPT B+}
  QManager should never be compiled with "Complete Boolean Evaluation" on
  Please turn it off in the Project Options dialog
{$ENDIF}

type

  TCopyDataType = (cdtString=0, cdtImage=1, cdtRecord=2);     //QMANTWO-711

  TGPSData = packed record    //QMANTWO-711
    Lng:string[13];
    Lat:string[13];
    dateSend: TDateTime;
  end;

const
  UM_SET_FROM_MAP = WM_USER + $104;    //QMANTWO-711
  UM_SET_EMP_ACT_MAP = WM_USER + $106;  //QMANTWO-711
  UM_UTILISAFE = WM_USER + 1100;  //QM-100 UtiliSafe EB
  UM_PLATSSTART = WM_USER + 1200;  //QM-164 Plats Start
  UM_PLATSDONE = WM_USER + 1250;
type
  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    File1: TMenuItem;
    FileExitItem: TMenuItem;
    Edit1: TMenuItem;
    CutItem: TMenuItem;
    CopyItem: TMenuItem;
    PasteItem: TMenuItem;
    Help1: TMenuItem;
    HelpAboutItem: TMenuItem;
    Actions: TActionList;
    FileExit1: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    HelpAbout1: TAction;
    ToolBar: TToolBar;
    MyWorkButton: TToolButton;
    FindTicketsButton: TToolButton;
    TicketDetail: TAction;
    SearchTickets: TAction;
    MyWorkAction: TAction;
    SBar: TStatusBar;
    RightPanel: TPanel;
    ContentPanel: TPanel;
    ContentLabelPanel: TPanel;
    DamageButton: TToolButton;
    HelpSpeedButton: TSpeedButton;
    SystemConfig: TAction;
    Reports: TAction;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EmergencyLabel: TLabel;
    NewLabel: TLabel;
    OngoingLabel: TLabel;
    LastSyncLabel: TLabel;
    ReportsButton: TToolButton;
    WorkManagementAction: TAction;
    TicketManButton: TToolButton;
    SyncLabelTimer: TTimer;
    Synchronize: TAction;
    OptionsButton: TToolButton;
    N1: TMenuItem;
    Synchronize2: TMenuItem;
    TimesheetButton: TToolButton;
    FindTickets: TAction;
    SyncButton: TToolButton;
    Label5: TLabel;
    SetWindowSizeSmall: TMenuItem;
    SetSmallScreenAction: TAction;
    AssetsButton: TToolButton;
    NewTicketButton: TToolButton;
    Assets: TAction;
    SendErrorReportAction: TAction;
    SendErrorReportTesting1: TMenuItem;
    ApplyXmlFileAction: TAction;
    ApplyXMLChanges1: TMenuItem;
    ChooseXmlFileDialog: TOpenDialog;
    Timesheet: TAction;
    ApprovalButton: TToolButton;
    Approval: TAction;
    TicketDetailButton: TToolButton;
    NewTicketAction: TAction;
    BillingButton: TToolButton;
    Billing: TAction;
    Damage: TAction;
    Messages1: TMenuItem;
    MessageAckAction: TAction;
    MessagesButton: TToolButton;
    MessageAction: TAction;
    MessageIcon: TImage;
    WarningLabel: TLabel;
    CheckUsageTimer: TTimer;
    AreaEditorButton: TToolButton;
    AreaEditorAction: TAction;
    HoursMessageAckAction: TAction;
    UploadStatusPanel: TPanel;
    Label7: TLabel;
    UploadFileCountLabel: TLabel;
    Label9: TLabel;
    UploadFileSizeLabel: TLabel;
    Label11: TLabel;
    UploadStatusMessageLabel: TLabel;
    AutoSyncStatusPanel: TPanel;
    AutoSyncStatusLabel: TLabel;
    ElapsedIdleTimeLabel: TLabel;
    FullWindowPanel: TPanel;
    TicketWorkFullFrame: TTicketWorkFrame;
    MessageAckButton: TToolButton;
    DamagesLabel: TLabel;
    DamagesCountLabel: TLabel;
    WorkOrdersLabel: TLabel;
    WorkOrdersCountLabel: TLabel;
    FindWorkOrders: TAction;
    FindWOButton: TToolButton;
    ProgressBar: TcxProgressBar;
    ScreenshotTimer: TTimer;
    SafetyButton: TToolButton;
    SafetySiteAction: TAction;
    UtilisafeButton: TToolButton;
    UtiliSafeAction: TAction;
    OQButton: TToolButton;
    ViewOQList: TAction;
    LetMeIn1: TMenuItem;
    ViewPlusList: TAction;
    MarsButton: TToolButton;
    ViewMarsList: TAction;
    tbDPU: TToolButton;    //QM-671 EB MARS Bulk Status
    procedure FileExit1Execute(Sender: TObject);
    procedure HelpAbout1Execute(Sender: TObject);
    procedure EditCut1Execute(Sender: TObject);
    procedure EditCopy1Execute(Sender: TObject);
    procedure EditPaste1Execute(Sender: TObject);
    procedure TicketDetailExecute(Sender: TObject);
    procedure SearchTicketsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HelpSpeedButtonClick(Sender: TObject);
    procedure SystemConfigExecute(Sender: TObject);
    procedure MyWorkActionExecute(Sender: TObject);
    procedure ReportsExecute(Sender: TObject);
    procedure WorkManagementActionExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SyncLabelTimerTimer(Sender: TObject);
    procedure SynchronizeExecute(Sender: TObject);
    procedure FindTicketsExecute(Sender: TObject);
    procedure FindWorkOrdersExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SetSmallScreenActionExecute(Sender: TObject);
    procedure AssetsExecute(Sender: TObject);
    procedure SendErrorReportActionExecute(Sender: TObject);
    procedure ApplyXmlFileActionExecute(Sender: TObject);
    procedure TimesheetExecute(Sender: TObject);
    procedure ApprovalExecute(Sender: TObject);
    procedure NewTicketActionExecute(Sender: TObject);
    procedure BillingExecute(Sender: TObject);
    procedure DamageExecute(Sender: TObject);
    procedure MessageAckActionExecute(Sender: TObject);
    procedure MessageActionExecute(Sender: TObject);
    procedure MessageIconClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckUsageTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AreaEditorActionExecute(Sender: TObject);
    procedure HoursMessageAckActionExecute(Sender: TObject);
    procedure UploadStatusMessageLabelDblClick(Sender: TObject);
    procedure SBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure TimeImportTimerTimer(Sender: TObject);
    procedure ScreenshotTimerTimer(Sender: TObject);
    procedure ShowTicket(Sender: TObject; ID: Integer);
    procedure ShowTicketFromWO(Sender: TObject; ID: Integer; WOID: Integer); //QMANTWO-296
    procedure ShowTicketUpdateOf(Sender: TObject; ID, CallingTktID: Integer);  //QM-397 sr
    procedure ShowWorkOrder(Sender: TObject; ID: Integer);
    procedure SafetySiteActionExecute(Sender: TObject);
    procedure UtiliSafeActionExecute(Sender: TObject);
    procedure ViewOQListExecute(Sender: TObject);
    procedure LetMeIn1Click(Sender: TObject);
    procedure ViewPlusListExecute(Sender: TObject);
    procedure ViewMarsListExecute(Sender: TObject);
    procedure tbDPUClick(Sender: TObject);
  private
    CanShutDown: boolean;  //QMANTWO-803
    EmbeddedForms: TStringList;
    FActiveEmbeddedForm: TEmbeddableForm;
    FCurrentTicketID: Integer;
    ComingFromManagementForm: Boolean;
    ComingFromSearchForm: Boolean;
    FMessagesAvailable: Boolean;
    FRestrictedUse: Boolean;
    FWorkLockout: Boolean;
    FLunchLockout: Boolean;
    TimeKeeper: TTimeKeeper;
    FReadyToAttachFiles: Boolean;
    FAlteredHoursMessagesAvailable: Boolean;
    IdleTimer: TOdIdleTimer;
    ComingFromApprovalForm: Boolean;
    FCompSerialNumMismatch: Boolean;
    fUtiliSafeAutoOpened: Boolean;  //QM-100 EB UtiliSafe
   procedure SetTicketFromMap(var Message: TMessage); message UM_SET_FROM_MAP; //QMANTWO-711
   procedure AddActivityFromMap(var Message: TMessage); message UM_SET_EMP_ACT_MAP; //QMANTWO-711
    function CreateForm(const FormID: string): TEmbeddableForm;
    procedure AdjustCaption;
//    procedure SendRecord(cdt: TcopyDataType);
    procedure PrintDamage(Sender: TObject; ID: Integer);
    procedure PrintTicket(Sender: TObject; ID: Integer);
    procedure PrintWorkOrder(Sender: TObject; ID: Integer);
    procedure ShowDamageThirdParty(Sender: TObject; ID: Integer);
    procedure SetSummaryVisible(AVisible: Boolean);
    procedure UpdateLastSyncLabel;
    procedure UpdateCountLabelsNotify(Sender: TObject);
    procedure SyncDone(Sender: TObject);
    procedure SyncStart(Sender: TObject);
    procedure TimesheetOverride(Sender: TObject);
    procedure LunchLockoutExpired(Sender: TObject);
    procedure CurrentTicketId(Sender: TObject; ID: Integer);
    procedure ShowLastSummaryScreen;
    procedure EditTimesheet(EmpID: Integer; EditDay: TDateTime; Mode: TTimesheetEditMode);
    procedure ShowUpdatedReportStatus(Sender: TObject);
    function GetRegisteredForm(const FormID: string): TEmbeddableForm;
    procedure NewDamage(Sender: TObject);
    procedure ShowDamageSearch(Sender: TObject);
    procedure ShowDamage(Sender: TObject; ID: Integer);
    procedure ShowDamageInvoice(Sender: TObject; ID: Integer);
    procedure ShowDamageLitigation(Sender: TObject; ID: Integer);
    procedure DamageInvoiceSearch(Sender: TObject);
    procedure DamageThirdPartySearch(Sender: TObject);
    procedure DamageLitigationSearch(Sender: TObject);
    procedure MessageAcked(Sender: TObject);
    procedure CheckForMessages;
    procedure StartWarningMessage(Message: string);
    procedure StopWarningMessage;
    procedure StartSyncReminder;
    procedure StopSyncReminder;
    procedure SetWarningMessage(Message: string);
    function MustSyncNow: Boolean;
    procedure SetupAction(Action: TCustomAction; Usable: Boolean; AlwaysUsable: Boolean=False); //QM-100 EB Moved
    procedure EnableAction(Action: TCustomAction; IsEnabled: Boolean);
    // Windows Broadcast Information Message
    procedure WMQueryEndSession(var Msg: TWMQueryEndSession); message WM_QueryEndSession; //QMANTWO-803
    procedure WMUserChanged(var Msg: TMessage); message WM_USERCHANGED;
    procedure WMStiLaunchNotification(var Message: TMessage); message WM_STI_LAUNCH;
    procedure WmDeviceChange(var Message: TWMDeviceChange); message WM_DEVICECHANGE;
    procedure OnWorkStarted(var Msg: TMessage); message UM_UTILISAFE;  //QM-100 UtiliSafe EB
    procedure OnPlatsUpdating(var Msg: TMessage); message UM_PLATSSTART;
    procedure OnPlatsFinished(Var Msg: TMessage); message UM_PLATSDONE;
    procedure StartAttachmentProcess(Mode: TAttachmentMode; const Param: string = '');
    procedure OnDeviceConnected(Sender: TObject);
    procedure OnDeviceDisconnected(Sender: TObject);
    procedure ValidateUsageTime;
    procedure SetRestrictedUse(Value: Boolean);
    procedure StartUsageTimeCheck(CheckNow: Boolean=False);
    procedure StopUsageTimeCheck;
    procedure UpdateUsageStatus(const MessageText: string);
    procedure ProcessBreakRuleResponse(Sender: TObject; const MessageDestID, RuleID: Integer; const Response: string; var OkToClose: Boolean);
    procedure HandleWorkerStatus(var Msg: TMessage); message UM_WORKER_STATUS;
    procedure ShowUploadStatus(var Msg: TMessage);
    procedure SetupBackgroundUploader;
    procedure UpdateUploadStatusPanelColors(Status: TUploadStatus);
    procedure AutoForegroundSync(const Source: string);
    procedure ApplicationIdleTimeElapsed(Sender: TObject);
    procedure SetupAutoForegroundSync;
    procedure AfterTimesheetSave(Sender: TObject);
    procedure StopIdleTimer;
    procedure RefreshAutoSyncStatus(const Msg: string);
    procedure ProcessAttachmentDataRequest(var Msg: TMessage); message UM_UPLOAD_NEED_DATA;
    procedure ShowTicketWorkFullScreen(Sender: TObject);
    procedure TicketWorkFullScreenClosed(Sender: TObject);
    function CheckIsClockedOut: Boolean;
    procedure CheckLockouts;
    procedure SetupGPS;
    procedure ShowGPSStatus(var Msg: TMessage); message UM_GPS_STATUS;
    procedure DoneWithDamage(Sender: TObject);
    procedure ShowLastDamageSummaryScreen;
    procedure ShowWorkerThreadStatus(var Msg: TMessage);

    procedure ShowLastWorkOrderSummaryScreen;
    procedure ShowProgressBar(Sender: TObject; const ProgressMessage: string;
      const PctComplete: Integer);
    procedure HideProgressBar(Sender: TObject);
    procedure CheckScreenShot;
    procedure WMEndSession(var Msg: TMessage);
    procedure WMPowerBroadcast(var Msg: TMessage);
    procedure WMSessionChange(var Msg: TMessage);
    function CheckUtiliSafeStatus: boolean; //QM-100 EB UtiliSafe 2
    procedure GoToUtiliSafeURL; //QM-100 EB
    procedure SetOldTicketsButtonEnabled;    //qm-754 sr

  protected
    procedure Loaded; override;
  public
    function ActivateForm(const FormID: string; CallActivatingNow: Boolean = True): TEmbeddableForm;
    function CreateOrGetFormByID(const FormID: string): TEmbeddableForm;
    procedure UpdateCountLabels;
    procedure DoneWithTicket(Sender: TObject);
    procedure DoneWithWorkOrder(Sender: TObject);
    procedure LeavingAndSaveWorkOrder(Sender: TObject);
    procedure SetVisibilityBasedOnRights;
    procedure TurnAllOtherActionsOff(RulingAction: TCustomAction); //QM-100 EB
    procedure ShowStartingScreen;

  end;

const
  frmDamageDetail = 'Damage Detail';
  frmDamageSearch = 'Damage Search';
  frmInvoiceDetail = 'Damage Invoice Detail';
  frmInvoiceSearch = 'Damage Invoice Search';
  frmThirdPartyDetail = 'Damage Third Party Detail';
  frmThirdPartySearch = 'Damage Third Party Search';
  frmLitigationDetail = 'Damage Litigation Detail';
  frmLitigationSearch = 'Damage Litigation Search';
  frmWorkOrderDetail = 'Work Order Detail';
  frmWorkOrderSearch = 'Work Order Search';
  frmTimesheet = 'Timesheet';

var
  MainForm: TMainForm;
  shellHandle: THandle;
implementation

uses DMu, TicketDetail, Report, Config,
  WorkManagement, OdMiscUtils, StateMgr, Sync, About, DateUtils,
  OldTickets, DamageList, DamageInvoiceForm, BaseSearchForm, NewTicket,
  TicketSearchHeader, DamageSearchHeader, OdExceptions, DamageInvoiceSearchHeader,
  IniFiles, OdVclUtils, DamageLitigationSearchHeader,
  MyAssets, AssetManagement, Timesheet,
  TimesheetApproval, BillingMain, DamageDetails,
  DamageThirdPartySearchHeader, MessageAck, MessageMain, Terminology,
  OdAutoSyncTimerDialog, OdWiaManager, RestrictedUseNotice,
  ClosedTicketDetailReport, DamageDetailsReport, OdAckMessagePromptDlg, OdIsoDates,
  WorkOrderDetail, WorkOrderSearchHeader,
  WorkOrderDetailReport, MyWork, TimeFileMonitor, TlHelp32, OdWmi,
  LocalPermissionsDMu, LocalEmployeeDMu, LocalDamageDMu, 
  OQManagement, PlusMgrList, MARSBulkStatus;

{$R *.DFM}

const
  PBT_APMQUERYSUSPEND           = $0000;
  {$EXTERNALSYM PBT_APMQUERYSUSPEND}
  PBT_APMQUERYSTANDBY           = $0001;
  {$EXTERNALSYM PBT_APMQUERYSTANDBY}

  PBT_APMQUERYSUSPENDFAILED     = $0002;
  {$EXTERNALSYM PBT_APMQUERYSUSPENDFAILED}
  PBT_APMQUERYSTANDBYFAILED     = $0003;
  {$EXTERNALSYM PBT_APMQUERYSTANDBYFAILED}

  PBT_APMSUSPEND                = $0004;
  {$EXTERNALSYM PBT_APMSUSPEND}
  PBT_APMSTANDBY                = $0005;
  {$EXTERNALSYM PBT_APMSTANDBY}

  PBT_APMRESUMECRITICAL         = $0006;
  {$EXTERNALSYM PBT_APMRESUMECRITICAL}
  PBT_APMRESUMESUSPEND          = $0007;
  {$EXTERNALSYM PBT_APMRESUMESUSPEND}
  PBT_APMRESUMESTANDBY          = $0008;
  {$EXTERNALSYM PBT_APMRESUMESTANDBY}

  PBTF_APMRESUMEFROMFAILURE     = $00000001;
  {$EXTERNALSYM PBTF_APMRESUMEFROMFAILURE}

  PBT_APMBATTERYLOW             = $0009;
  {$EXTERNALSYM PBT_APMBATTERYLOW}
  PBT_APMPOWERSTATUSCHANGE      = $000A;
  {$EXTERNALSYM PBT_APMPOWERSTATUSCHANGE}

  PBT_APMOEMEVENT               = $000B;
  {$EXTERNALSYM PBT_APMOEMEVENT}
  PBT_APMRESUMEAUTOMATIC        = $0012;
  {$EXTERNALSYM PBT_APMRESUMEAUTOMATIC}

  // Statusbar panel indexes:
  UserStatusPanelIdx = 0;
  EmployeeStatusPanelIdx = 1;
  VersionStatusPanelIdx = 2;
  ServerStatusPanelIdx = 3;
  UsageStatusPanelIdx = 4;
  GPSStatusPanelIdx = 5;
  ProgressBarStatusPanelIdx = EmployeeStatusPanelIdx;

procedure TMainForm.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
begin
  Caption := AppName;
  DM.OnUpdateCountLabels := UpdateCountLabelsNotify;
  DM.OnEditTimesheet := EditTimesheet;
  DM.OnReportStatusChange := ShowUpdatedReportStatus;

  if DM.AttachmentManager <> nil then begin
    DM.AttachmentManager.OnDownloadProgress := ShowProgressBar;
    DM.AttachmentManager.OnDownloadDone := HideProgressBar;
  end;
  ProgressBar.Parent := SBar;

  IniFile := DM.UQState.CreateINIFile;
  try
    RestoreFormState(Self, IniFile, 'UI', 'Main');
  finally
    FreeAndNil(IniFile);
  end;
  MessageIcon.Visible := False;
  MessageAckAction.Visible := False;
  ToolBar.Realign;  {workaround Delphi bug http://qc.embarcadero.com/wc/qcmain.aspx?d=3791}

  RegisterSessionNotification(Handle, NOTIFY_FOR_THIS_SESSION);
  if Assigned(WiaManager) then begin
    WiaManager.OnDeviceConnected := OnDeviceConnected;
    WiaManager.OnDeviceDisconnected := OnDeviceDisconnected;
  end;
  SetParentBackgroundValue(ContentLabelPanel, False);
  SetParentBackgroundValue(AutoSyncStatusPanel, False);
  SetParentBackgroundValue(UploadStatusPanel, False);
  SetParentBackgroundValue(ContentLabelPanel, False);
  TimeKeeper := TTimeKeeper.Create(DM.UQState.EmpID, DM.Engine);
  FRestrictedUse := False;
  FWorkLockout:= False;
  FLunchLockout:= False;
  DeviceNotification.InitializeDeviceNotification;
  dm.RunRoboCopy(DM.UQState.EmpID);   //qm-436  sr
  IdleTimer := TOdIdleTimer.Create(Self);
  IdleTimer.OnIdleExecute := ApplicationIdleTimeElapsed;
  DM.InitializeHttpServer(IdleTimer);
  TimeFileMonitorDM.TimeImportTimer.OnTimer := TimeImportTimerTimer;
  SetOldTicketsButtonEnabled;  //qm-754 sr
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DM.ExportQMDataAsXML;
  if FActiveEmbeddedForm is TWorkManagementForm then
        (FActiveEmbeddedForm as TWorkManagementForm).TicketFrame.SaveTicketGridSettings;  //QM-338 EB Grid Settings Ini
  UnRegisterSessionNotification(Handle);
  DeviceNotification.FinalizeDeviceNotification;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  SetupGPS;

  if DM.UQState.DeveloperMode then
    SBar.Panels[UserStatusPanelIdx].Text := 'User: ' + DM.UQState.UserName + '  ID: ' + IntToStr(DM.UQState.EmpID)
  else
    SBar.Panels[UserStatusPanelIdx].Text := 'User: ' + DM.UQState.UserName;
  SBar.Panels[EmployeeStatusPanelIdx].Text := DM.UQState.DisplayName;
  SBar.Panels[VersionStatusPanelIdx].Text := 'Version: ' + AppVersion + ' ' + AppVersionMarker;
  if DM.ServerInClient then begin
    SBar.Panels[ServerStatusPanelIdx].Text := 'Server: SERVER_IN_CLIENT mode, see client\QMServer.ini';
    ContentLabelPanel.Color := clMaroon;  //QM-805 EB Refactor Debug Ini
    ContentLabelPanel.Font.Color := clWhite;  //QM-805 EB Refactor Debug Ini
  end
  else
  begin     //QMANTWO-320
    if StrContainsText('QM-Dev', DM.UQState.ServerURL) then begin  //QM-155 SR
      SBar.Panels[ServerStatusPanelIdx].Text := 'Connected to Test Database';
      ContentLabelPanel.Color := clMaroon; //QM-805 EB Refactor Debug Ini
    end
    else
    if StrContainsText('QMCluster', DM.UQState.ServerURL) then
      SBar.Panels[ServerStatusPanelIdx].Text := 'Connected to Production Database'
    else
     SBar.Panels[ServerStatusPanelIdx].Text := 'Server: ' + DM.UQState.ServerURL;
  end;

  UpdateUsageStatus('');

  MyWorkAction.Visible := True;   // Managers can also see this
  SearchTickets.Visible := True;   // Managers can also see this
  SafetySiteAction.Visible := True; //QMANTWO-348
  SetSmallScreenAction.Visible := DM.UQState.DeveloperMode;
  SendErrorReportAction.Visible := DM.UQState.DeveloperMode;
  ApplyXmlFileAction.Visible := DM.UQState.DeveloperMode;
  MessageAckAction.Visible := DM.UQState.DeveloperMode;
  ToolBar.Realign;
  DM.RefreshMyAllowedStatusCodes;


  StopSyncReminder;
  SetSummaryVisible(True);    // Managers can also see this
  UpdateLastSyncLabel;
  ValidateUsageTime;
  DM.UpdateCounts;
  DM.CreateBackgroundUploader;
  SetupBackgroundUploader;

  ShowStartingScreen;         // This sometimes runs a sync
  if MustSyncNow then
    Synchronize.Execute;

  if LaunchedBySTI then
    StartAttachmentProcess(amStillImage);
  SetupAutoForegroundSync;
//  SetupGPS;
  DM.StartTimeImportTimer;
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeStart, '');
end;

{ Design note:

  Normally, using magic strings like 'customer' to identify an object would be
  inferior to using a variable that references it.  However, the embedded form
  system can support Quickbooks-style interfaces in which the list of
  available forms is dynamic.  It would be possible to have 'Customer 123' for
  example.
}

function TMainForm.CreateForm(const FormID: string): TEmbeddableForm;

  function CreateFormOfClass(FormClass: TEmbeddableFormClass): TEmbeddableForm;
  begin
    Result := CreateEmbeddedForm(FormClass, Self, ContentPanel);
  end;

begin
  // This method maps from the FormID to the class, and also sets up
  // event handlers as needed.  It is only called the first time each form
  // is needed.
  Result := nil;

  if FormID = 'My Work' then begin
    Result := CreateFormOfClass(TMyWorkForm);
    with Result as TMyWorkForm do begin
      MyWorkTicketsFrame.OnItemSelected := ShowTicket;
      MyWorkTicketsFrame.OnSelectTicket := CurrentTicketId;
      MyWorkDamagesFrame.OnItemSelected := ShowDamage;
      MyWorkWOsFrame.OnItemSelected := ShowWorkOrder;
    end;
  end;

  if FormID = frmWorkOrderDetail then begin
    Result := CreateFormOfClass(TWorkOrderDetailForm);
    with Result as TWorkOrderDetailForm do begin
      OnDoneWithWorkOrder := Self.DoneWithWorkOrder;
      OnWorkOrderPrint := Self.PrintWorkOrder;
      OnLeavingAndSaveWorkOrder := LeavingAndSaveWorkOrder;
    end;
  end;

  if FormID = 'Ticket Detail' then begin
    Result := CreateFormOfClass(TTicketDetailForm);
    with Result as TTicketDetailForm do begin
      OnDoneWithTicket := Self.DoneWithTicket;
      OnTicketPrint := Self.PrintTicket;
      OnShowTicketWork := ShowTicketWorkFullScreen;
    end;
  end;

  if FormID = 'Search Tickets' then begin
    Result := CreateFormOfClass(TOldTicketsForm);
    with Result as TOldTicketsForm do begin
      OnItemSelected := ShowTicket;
      OnSelectTicket := CurrentTicketId;
    end;
  end;

  if FormID = 'Reports' then begin
    Result := CreateFormOfClass(TReportForm);
  end;

  if FormID = 'Configuration' then begin
    Result := CreateFormOfClass(TConfigForm);
  end;

  if FormID = 'Ticket Management' then begin
    Result := CreateFormOfClass(TWorkManagementForm);
    with Result as TWorkManagementForm do begin
      OnTicketItemSelected := ShowTicket;
      OnDamageItemSelected := ShowDamage;
      OnWorkOrderItemSelected := ShowWorkOrder;
      OnSelectTicket := CurrentTicketId;
    end;
  end;

  if FormID = 'Find Tickets' then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TTicketSearchCriteria;
      OnItemSelected := ShowTicket;
      OnItemPrint    := PrintTicket;
      ShowTicketLocates := True;    //QMANTWO-422
    end;
  end;

  if FormID = 'Find Work Orders' then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TWorkOrderSearchCriteria;
      OnItemSelected := ShowWorkOrder;
      OnItemPrint    := PrintWorkOrder;
    end;
  end;

  if FormID = frmInvoiceSearch then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TDamageInvoiceSearchCriteria;
      OnItemSelected := ShowDamageInvoice;
    end;
  end;

  if FormID = 'Synchronize' then begin
    Result := CreateFormOfClass(TSyncForm);
    with Result as TSyncForm do begin
      OnSyncStart := SyncStart;
      OnSyncDone := SyncDone;
      OnShowTimesheet := TimesheetOverride;
      OnLunchLockoutExpired := LunchLockoutExpired;
    end;
  end;

  if FormID = 'Damages' then begin
    Result := CreateFormOfClass(TDamageListForm);
    with Result as TDamageListForm do
    begin
      OnDamagePrint := PrintDamage;
      OnNewDamage := NewDamage;
      OnDamageSearch := ShowDamageSearch;
      OnInvoiceSearch := DamageInvoiceSearch;
      OnLitigationSearch := DamageLitigationSearch;
      OnThirdPartySearch := DamageThirdPartySearch;
    end;
  end;

  if FormID = frmDamageSearch then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TDamageSearchCriteria;
      OnItemSelected := ShowDamage;
    end;
  end;

  if FormID = 'Timesheet' then begin
    Result := CreateFormOfClass(TTimesheetForm);
    with Result as TTimesheetForm do
      AfterTimesheetSave := Self.AfterTimesheetSave;
  end;

  if FormID = 'Timesheet Approval' then begin
    Result := CreateFormOfClass(TTimesheetApprovalForm);
  end;

  if FormID = frmLitigationSearch then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TDamageLitigationSearchCriteria;
      OnItemSelected := ShowDamageLitigation;
    end;
  end;

  if FormID = 'Damage Third Party Search' then begin
    Result := CreateFormOfClass(TSearchForm);
    with Result as TSearchForm do begin
      CriteriaClass := TDamageThirdPartySearchCriteria;
      OnItemSelected := ShowDamageThirdParty;
    end;
  end;

  if FormID = frmDamageDetail then begin
    Result := CreateFormOfClass(TDamageDetailsForm);
    with Result as TDamageDetailsForm do begin
      OnFinished := ShowDamageSearch;
      OnDamagePrint := PrintDamage;
    end;
  end;

  if FormID = 'My Assets' then begin
    Result := CreateFormOfClass(TMyAssetsForm);
  end;

  if FormID = 'Asset Management' then begin
    Result := CreateFormOfClass(TAssetManagementForm);
  end;

  if FormID = 'New Ticket' then begin
    Result := CreateFormOfClass(TNewTicketForm);
    with Result as TNewTicketForm do begin
      OnGoToTicket := ShowTicket;
    end;
  end;

  if FormID = 'Billing' then begin
    Result := CreateFormOfClass(TBillingMainForm);
  end;

  if FormID = 'MessageAck' then begin
    Result := CreateFormOfClass(TMessageAckForm);
    with Result as TMessageAckForm do begin
      OnAcked := MessageAcked;
      OnCheckResponse := nil;
    end;
  end;

  if FormID = 'HoursMessageAck' then begin
    Result := CreateFormOfClass(TMessageAckForm);
    with Result as TMessageAckForm do begin
      ShowBreakRuleMessages(BreakRuleTypeManagerAlteredTime);
      OnAcked := MessageAcked;
      OnCheckResponse := ProcessBreakRuleResponse;
    end;
  end;

  if FormID = 'Messages' then begin
    Result := CreateFormOfClass(TMessageMainForm);
  end;

  if FormID = frmInvoiceDetail then begin
    Result := CreateFormOfClass(TDamageInvoiceForm);
    with Result as TDamageInvoiceForm do begin
      OnFinished := ShowDamageSearch;
      OnGoToDamage := ShowDamage;
    end;
  end;

  if FormID = 'RestrictedUse' then
    Result := CreateFormOfClass(TRestrictedUseForm);

  if FormID = 'OQ List' then
    Result := CreateFormOfClass(TOQManagementForm);  //QM-444 Part 2 EB

  if FormID = 'Employee Plus List' then
   Result := CreateFormOfClass(TPlusMgrListForm);   //QM-585 Emp Plus Whitelist Part 3 EB

  if FormID = 'MARS Bulk Status' then               //QM-671 EB MARS Bulk Status
    Result := CreateFormOfClass(TfrmMARSBulkStatus);

  if Result = nil then
    Result := GetRegisteredForm(FormID);

  if Result = nil then
    raise Exception.Create('ERROR: Don''t know how to create form: ' + FormID);
end;

// **************** Action handlers
// All user action code in this unit should be in action handlers, not in
// toolbutton handlers, to allow for changing to a different toolbar component
// later, if needed.

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
begin
  About.ShowAboutBox;
end;

procedure TMainForm.EditCut1Execute(Sender: TObject);
begin
  ActiveControl.Perform(WM_CUT, 0, 0);
end;

procedure TMainForm.EditCopy1Execute(Sender: TObject);
begin
  ActiveControl.Perform(WM_COPY, 0, 0);
end;

procedure TMainForm.EditPaste1Execute(Sender: TObject);
begin
  ActiveControl.Perform(WM_PASTE, 0, 0);
end;

procedure TMainForm.TicketDetailExecute(Sender: TObject);
begin
  if FCurrentTicketID > 0 then
    ShowTicket(Self, FCurrentTicketID)
  else
    MessageDlg('There is no current ticket in memory.', mtInformation, [mbOK],  0);
end;

procedure TMainForm.SearchTicketsExecute(Sender: TObject);
begin
  ActivateForm('Search Tickets');
end;

procedure TMainForm.HelpSpeedButtonClick(Sender: TObject);
var
  Url: string;
begin
  Url := DM.UQState.BaseHelpURL + FActiveEmbeddedForm.ClassName + '.html';
  OdShellExecute(Url);
end;

procedure TMainForm.SystemConfigExecute(Sender: TObject);
begin
  ActivateForm('Configuration');
end;

procedure TMainForm.MyWorkActionExecute(Sender: TObject);
begin
  ComingFromManagementForm := False;
  ComingFromSearchForm := False;
  ActivateForm('My Work');
end;

procedure TMainForm.ReportsExecute(Sender: TObject);
begin
  ActivateForm('Reports');
end;

procedure TMainForm.DamageExecute(Sender: TObject);
begin
  ActivateForm('Damages');
end;

procedure TMainForm.WorkManagementActionExecute(Sender: TObject);
begin
  ComingFromManagementForm := True;
  ComingFromSearchForm := False;
  //TODO: Changes to the temporary Tickets - Management New right don't take effect w/o a QM restart.
  ActivateForm('Ticket Management');
end;

procedure TMainForm.SynchronizeExecute(Sender: TObject);
begin
  //Work lockout never applies to sync
  ActivateForm('Synchronize');
end;

procedure TMainForm.FindTicketsExecute(Sender: TObject);
begin
  ActivateForm('Find Tickets');
end;

procedure TMainForm.FindWorkOrdersExecute(Sender: TObject);
begin
  ComingFromSearchForm := True;
  ComingFromManagementForm := False;
  ActivateForm('Find Work Orders');
end;

procedure TMainForm.NewDamage(Sender: TObject);
var
  Form: TDamageDetailsForm;
begin
  Form := CreateOrGetFormByID(frmDamageDetail) as TDamageDetailsForm;
  Form.AddNewDamage;
  ActivateForm(frmDamageDetail);
end;

procedure TMainForm.ShowDamageSearch(Sender: TObject);
begin
  ComingFromSearchForm := True;
  ComingFromManagementForm := False;
  ActivateForm(frmDamageSearch);
end;

// **********************************************************************
// Embedded form management code needs to moved in to the embedded form
// utility module and base class... right now it is copy-and-pasted to each
// project that uses it.  Ugh.  -Kyle

procedure TMainForm.AddActivityFromMap(var Message: TMessage);  //QMANTWO-711
var                 {receives messages from TktStreetMap}
  maMapActivity:TmaMapActivity;
  matMapActType:TmatMapActType;
  
  ActivityType, ActivityDetails,ActivityExtraDetails: string;
begin
  maMapActivity := TmaMapActivity(Message.WParam);
  matMapActType := TmatMapActType(Message.LParam);
  case matMapActType of
        matUser : ActivityExtraDetails:= 'User select';
        matSystem : ActivityExtraDetails:= 'System select';
  end;


  case maMapActivity of
      maMapPlot: begin
                   ActivityType:= PLOT;
                   ActivityDetails :='Map tickets plotted';
                 end;
      maWazeLookup: begin
                   ActivityType:= WAZE;
                   ActivityDetails :='Map tickets plotted';
                 end;
      maDriveTimeDist: begin
                   ActivityType:= TIME_DIST;
                   ActivityDetails :='Time/Distance';
                 end;
      maViewTicketDetails: begin
                   ActivityType:= VIEW_DET;
                   ActivityDetails :='Ticket details viewed';
                 end;
      maMapOpen: begin
                   ActivityType:= MAP_SHOW;
                   ActivityDetails :='Map is shown';
                 end;
  end;


  EmployeeDM.AddEmployeeActivityEvent(ActivityType,ActivityDetails, 0, ActivityExtraDetails);
end;

procedure TMainForm.AdjustCaption;
begin
  if Assigned(FActiveEmbeddedForm) then
    ContentLabelPanel.Caption := '   ' + FActiveEmbeddedForm.Caption;
end;

function TMainForm.CheckIsClockedOut: Boolean;
//returns true if User is clocked out;
//returns false if user is clocked in
//returns false if user has the RightWorkBeforeClockIn or RightWorkDuringLunch granted
begin
  CheckLockouts;
  if FLunchLockout then
    Synchronize.Execute
  else if FWorkLockout then
    TimesheetOverride(Self);

  SetVisibilityBasedOnRights;
  Result := FWorkLockout or FLunchLockout;
end;

procedure TMainForm.CheckLockouts;
var
  ClockedIn: Boolean;
begin
  ClockedIn := DM.AmIClockedIn;
  if not ClockedIn then begin
    FWorkLockout := not PermissionsDM.CanI(RightWorkBeforeClockIn);
    if DM.IsLunchLockout then
      FLunchLockout := not PermissionsDM.CanI(RightWorkDuringLunch)
    else
      FLunchLockout := False;
  end else begin
    FWorkLockout := False;
    FLunchLockout := False;
  end;
end;

procedure TMainForm.CheckScreenShot;
  function ProcessExists(exeFileName: string): Boolean;
  var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
    ProcessExtFileName, IniExtFileName : string;
  begin
    FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
    ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
    Result := False;
    while ContinueLoop do begin
      ProcessExtFileName := UpperCase(ExtractFileName(FProcessEntry32.szExeFile));
      IniExtFileName := UpperCase(ExtractFileName(exeFileName));
      if (ProcessExtFileName = IniExtFileName) or
         (UpperCase(FProcessEntry32.szExeFile) = exeFileName) then begin
        Result := True;
        ContinueLoop := False;
      end else
        ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
  end;

const
  SSErrorMessage = 'Screen Shot failed to start';
begin
  if DM.UQState.ScreenShotPath = '' then
    exit;
  if not ProcessExists(DM.UQState.ScreenShotPath) then
  begin
    OdShellEx(DM.UQState.ScreenShotPath, SSErrorMessage);
  end;
end;

function TMainForm.ActivateForm(const FormID: string; CallActivatingNow: Boolean): TEmbeddableForm;
var
  Form: TEmbeddableForm;
  Index: Integer;
  FormToHide: TEmbeddableForm;
begin
  // This method actives the named form, and also caches them so that only
  // one instance per name is created.  This supports a form class being
  // instantiated more than once under different names.

  Form := CreateOrGetFormByID(FormID);
  if Form <> nil then begin
    if Form.RequireSync and not DM.HaveSyncedData then
      raise EOdNonLoggedException.Create('You must sync before opening ' + FormID);
  end;

  if not Form.Visible then begin
    for Index := 0 to EmbeddedForms.Count-1 do begin   // hide all others
      FormToHide := EmbeddedForms.Objects[Index] as TEmbeddableForm;
      if FormToHide.Visible then begin
        FormToHide.LeavingNow;
        FormToHide.Hide;
      end;
    end;
    FActiveEmbeddedForm := Form;
    Form.Show;
    // This is just a hack to ensure the embedded alClient form actually
    // resizes when the parent window was resized since the last time this
    // embedded form.  I can't find another way to force a realign that works.
    Form.Width := Form.Width + 1;
  end;
  if CallActivatingNow then begin
    Form.ActivatingNow;
    PostMessage(Form.Handle, UM_ACTIVATED, 0, 0);
  end;

  AdjustCaption;
  Result := Form;
end;



procedure TMainForm.ShowTicket(Sender: TObject; ID: Integer);
begin
  with ActivateForm('Ticket Detail') as TTicketDetailForm do
    GoToTicket(ID, ComingFromManagementForm);
  fCurrentTicketID := ID;
end;

procedure TMainForm.ShowTicketUpdateOf(Sender: TObject; ID, CallingTktID: Integer);  //qm-397  sr
begin
  ComingFromManagementForm:=false;
  with ActivateForm('Ticket Detail') as TTicketDetailForm do
    GoToTicket(ID, ComingFromManagementForm, 0, CallingTktID);    //qm-397  sr
  fCurrentTicketID := ID;
end;

procedure TMainForm.ShowTicketFromWO(Sender: TObject; ID, WOID: Integer);   //QMANTWO-296
begin
  with ActivateForm('Ticket Detail') as TTicketDetailForm do
    GoToTicket(ID, ComingFromManagementForm, WOID);
  fCurrentTicketID := ID;
end;

procedure TMainForm.UpdateCountLabels;
var
  DamageCountVisible: Boolean;
begin
  EmergencyLabel.Caption := IntToStr(DM.nEmergency);
  NewLabel.Caption := IntToStr(DM.nNew);
  OngoingLabel.Caption := IntToStr(DM.nOngoing);
  DamageCountVisible := LDamageDM.HaveDamagesRights;
  DamagesLabel.Visible := DamageCountVisible;
  DamagesCountLabel.Visible := DamageCountVisible;
  if DamageCountVisible then
    DamagesCountLabel.Caption := IntToStr(DM.nOpenDamages);
  WorkOrdersCountLabel.Caption := IntToStr(DM.nOpenWorkOrders);
  UpdateLastSyncLabel;
end;

procedure TMainForm.SetSummaryVisible(AVisible: Boolean);
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i].Tag = 546 then
      if Components[i] is TLabel then
        (Components[i] as TLabel).Visible := AVisible;
end;

procedure TMainForm.SetTicketFromMap(var Message: TMessage); //QMANTWO-711
var
  TicketID:integer;
  MyWorkHandle: HWnd;
begin
  try
    TicketID:= Message.LParam;
  except
    TicketID := 0;
  end;
  if TicketID > 0 then begin  //QM-742 Isolated Machine/Dev Ex - avoid handled exception (delphi issue)
    ShowTicket(nil,TicketID);
    MyWorkHandle := FindWindow('TfrmMapShell', nil);
    ShowWindow(MyWorkHandle, SW_SHOWMINIMIZED);
  end;
end;

procedure TMainForm.DoneWithTicket(Sender: TObject);
begin
  ShowLastSummaryScreen;

  if ComingFromManagementForm then begin
    if DM.Engine.HavePendingTicketChanges then begin
      DM.SendChangesToServer;
      if FActiveEmbeddedForm is TWorkManagementForm then
        (FActiveEmbeddedForm as TWorkManagementForm).TicketFrame.RefreshItem;
    end;
  end else if PermissionsDM.CanI(RightSyncAfterTicketSave) then
    AutoForegroundSync('TICKSAVE')
  else if PermissionsDM.CanI(RightTicketsImmediateSync) and DM.Engine.HavePendingTicketChanges then
    DM.SafeSendChangesToServer;

  DM.ExportQMDataAsXML;
  if FActiveEmbeddedForm is TWorkManagementForm then     //QMANTWO-690 EB
        (FActiveEmbeddedForm as TWorkManagementForm).TicketFrame.MoveToTicketID((FActiveEmbeddedForm as TWorkManagementForm).TicketFrame.GridState.LastOpenedID);
end;

procedure TMainForm.DoneWithWorkOrder(Sender: TObject);
begin
  ShowLastWorkOrderSummaryScreen;

  LeavingAndSaveWorkOrder(Sender);
   if FActiveEmbeddedForm is TWorkManagementForm then     //QMANTWO-690 EB
        (FActiveEmbeddedForm as TWorkManagementForm).WorkOrderFrame.MoveToWOID((FActiveEmbeddedForm as TWorkManagementForm).WorkOrderFrame.GridState.LastOpenedID);
end;

procedure TMainForm.LeavingAndSaveWorkOrder(Sender: TObject);
begin
  if ComingFromManagementForm then begin
    if DM.Engine.HavePendingWorkOrderChanges then begin
      DM.SendChangesToServer;
      if FActiveEmbeddedForm is TWorkManagementForm then
        (FActiveEmbeddedForm as TWorkManagementForm).WorkOrderFrame.RefreshItem;
    end;
  end else if PermissionsDM.CanI(RightWorkOrdersImmediateSync) and DM.Engine.HavePendingWorkOrderChanges then begin
    DM.SafeSendChangesToServer;
    DM.WorkOrder.Refresh;
  end;
end;

procedure TMainForm.LetMeIn1Click(Sender: TObject);  //qm-493  sr
CONST
  LETMEIN='https://secure.logmeinrescue.com/Customer/Code.aspx';
begin
    ShellExecute(0,'open', PChar(LETMEIN), nil, nil, SW_SHOW);
end;

procedure TMainForm.ShowLastWorkOrderSummaryScreen;
begin
  if ComingFromManagementForm then
    WorkManagementAction.Execute
  else if ComingFromSearchForm then
    FindWorkOrders.Execute
  else
    MyWorkAction.Execute;
end;

procedure TMainForm.ShowLastDamageSummaryScreen;
begin
  if ComingFromManagementForm then
    WorkManagementAction.Execute
  else if ComingFromSearchForm then
    ShowDamageSearch(Self)
  else
    MyWorkAction.Execute;
end;

procedure TMainForm.DoneWithDamage(Sender: TObject);
begin
  ShowLastDamageSummaryScreen;
  if ComingFromManagementForm then begin
    if FActiveEmbeddedForm is TWorkManagementForm then
      (FActiveEmbeddedForm as TWorkManagementForm).DamageFrame.RefreshItem;
      (FActiveEmbeddedForm as TWorkManagementForm).DamageFrame.MoveToDamageID((FActiveEmbeddedForm as TWorkManagementForm).DamageFrame.GridState.LastOpenedID);
  end;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_F12) then begin
    if FActiveEmbeddedForm is TTicketDetailForm then begin
      (FActiveEmbeddedForm as TTicketDetailForm).DoneButton.SetFocus;  // to leave the grid
      (FActiveEmbeddedForm as TTicketDetailForm).DoneWithTicket;
    end
    else if FActiveEmbeddedForm is TWorkOrderDetailForm then begin
      (FActiveEmbeddedForm as TWorkOrderDetailForm).DoneButton.SetFocus;  // to leave the grid
      (FActiveEmbeddedForm as TWorkOrderDetailForm).DoneWithWorkOrder;
    end;
  end;
  if (Key = VK_F3) then begin
    if FActiveEmbeddedForm is TWorkManagementForm then
      (FActiveEmbeddedForm as TWorkManagementForm).DoEmployeeSearch(False);
  end;
  if (ssCtrl in Shift) and (Key = Ord('F')) then begin // Ctrl+F
    if FActiveEmbeddedForm is TWorkManagementForm then
      (FActiveEmbeddedForm as TWorkManagementForm).DoEmployeeSearch(True);
  end;
end;

procedure TMainForm.SyncLabelTimerTimer(Sender: TObject);
begin
  UpdateLastSyncLabel;
end;

procedure TMainForm.UpdateLastSyncLabel;
var
  LastSync: TDateTime;
begin
  // If the last sync was in the future, it means the clock was somehow off,
  // so set it back to Now.
  if DM.UQState.LastLocalSyncTime > Now then
    DM.UQState.LastLocalSyncTime := Now;

  LastSync := DM.UQState.LastLocalSyncTime;
  if Now - LastSync > 200 then    // more than 200 days since last sync
    LastSyncLabel.Caption := 'None'
  else
    LastSyncLabel.Caption := SecondsToTime(SecondsBetween(Now, LastSync));

  if MustSyncNow then
    StartSyncReminder;
  if DM.UQState.DeveloperMode then
    RefreshAutoSyncStatus(AutoSyncStatusLabel.Caption);
end;

procedure TMainForm.UpdateCountLabelsNotify(Sender: TObject);
begin
  UpdateCountLabels;
end;

procedure TMainForm.ShowDamage(Sender: TObject; ID: Integer);
var
  Form: TDamageDetailsForm;
begin
  Form := CreateOrGetFormByID(frmDamageDetail) as TDamageDetailsForm;
  Form.OnFinished := DoneWithDamage;
  Form.GotoDamageId(ID);
  ActivateForm(frmDamageDetail);
end;

procedure TMainForm.ShowDamageInvoice(Sender: TObject; ID: Integer);
var
  Form: TDamageInvoiceForm;
begin
  Form := CreateOrGetFormByID(frmInvoiceDetail) as TDamageInvoiceForm;
  Form.GoToDamageInvoice(ID);
  ActivateForm(frmInvoiceDetail);
end;

procedure TMainForm.DamageInvoiceSearch(Sender: TObject);
begin
  ActivateForm(frmInvoiceSearch);
end;

function TMainForm.CreateOrGetFormByID(const FormID: string): TEmbeddableForm;
var
  Index: Integer;
begin
  if EmbeddedForms = nil then
    EmbeddedForms := TStringList.Create;

  Index := EmbeddedForms.IndexOf(FormID);
  if Index >= 0 then begin
    Result := EmbeddedForms.Objects[Index] as TEmbeddableForm;
  end else begin
    Result := CreateForm(FormID);
    EmbeddedForms.AddObject(FormID, Result);
  end;
end;

procedure TMainForm.CurrentTicketId(Sender: TObject; ID: Integer);
begin
  FCurrentTicketID := ID;
end;

procedure TMainForm.TimesheetExecute(Sender: TObject);
begin
  //User's w/o the right 'RightWorkBeforeClockIn' should not be able to clock
  //in until lunchtime elapsed or overridden
  DM.KillTask('StreetMapProj.exe');
  if not CheckIsClockedOut then begin
    ComingFromApprovalForm := False;
    (ActivateForm('Timesheet') as TTimesheetForm).ShowTodaysTimesheet;
  end;
end;

procedure TMainForm.TimesheetOverride(Sender: TObject);
begin
  CheckLockouts;
  SetVisibilityBasedOnRights;
  ComingFromApprovalForm := False;
  (ActivateForm('Timesheet') as TTimesheetForm).ShowTodaysTimesheet;
end;

procedure TMainForm.tbDPUClick(Sender: TObject);      //qm-822
const
  URL='https://sites.google.com/utiliquest.com/new-hire-remote-training/local-training';
begin
    ShellExecute(0,'open', PChar(URL), nil, nil, SW_SHOW);    //qm-822
end;

procedure TMainForm.LunchLockoutExpired(Sender: TObject);
begin
  CheckLockouts;
  SetVisibilityBasedOnRights;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  SBar.Invalidate;
  MyWorkButton.AutoSize := True;
  MyWorkButton.AutoSize := False;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  IniFile: TIniFile;
begin
  CanClose := True;

  IniFile := DM.UQState.CreateIniFile;
  try
    SaveFormState(Self, IniFile, 'UI', 'Main');
  finally
    FreeAndNil(IniFile);
  end;

  if Assigned(DM.FBackgroundUploader) and
      DM.FBackgroundUploader.InProgress then begin
    CanClose := MessageDlg('Background uploads are currently active. Are you sure you want to exit?',
      mtConfirmation, [mbYes, mbNo], 0) = mrYes;

    if CanClose then
      DM.KillBackgroundUploader;
  end;

  if CanClose then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeQuit, '');
end;

procedure TMainForm.PrintDamage(Sender: TObject; ID: Integer);
begin
  Reports.Execute;
  TDamageDetailsReportForm.SetLastUQDamageID(ID);
  (FActiveEmbeddedForm as TReportForm).ShowReportName('Damage Details');
end;

procedure TMainForm.PrintTicket(Sender: TObject; ID: Integer);
begin
  Reports.Execute;
  TClosedTicketDetailReportForm.SetTicketID(ID);
  (FActiveEmbeddedForm as TReportForm).ShowReportName('Ticket Detail');
end;

procedure TMainForm.PrintWorkOrder(Sender: TObject; ID: Integer);
begin
  Reports.Execute;
  TWorkOrderDetailReportForm.SetWorkOrderID(ID);
  (FActiveEmbeddedForm as TReportForm).ShowReportName('Work Order Detail');
end;

procedure TMainForm.DamageLitigationSearch(Sender: TObject);
begin
  ActivateForm(frmLitigationSearch);
end;

procedure TMainForm.DamageThirdPartySearch(Sender: TObject);
begin
  ActivateForm('Damage Third Party Search');
end;

procedure TMainForm.ShowDamageLitigation(Sender: TObject; ID: Integer);
var
  Form: TDamageDetailsForm;
begin
  Form := CreateOrGetFormByID(frmDamageDetail) as TDamageDetailsForm;
  Form.GotoLitigationId(ID);
  ActivateForm(frmDamageDetail);
end;

procedure TMainForm.ShowLastSummaryScreen;
begin
  if ComingFromManagementForm then
    WorkManagementAction.Execute
  else
    MyWorkAction.Execute;
end;

procedure TMainForm.SetSmallScreenActionExecute(Sender: TObject);
begin
  Width := 800;
  Height := 560;
end;

procedure TMainForm.SetupAction(Action: TCustomAction; Usable: Boolean; AlwaysUsable: Boolean=False);
  begin
    Action.Enabled := Usable and not FMessagesAvailable and not FRestrictedUse
                     and not FWorkLockout and not FLunchLockout or AlwaysUsable;
    Action.Visible := Usable;
  end;

procedure TMainForm.EnableAction(Action: TCustomAction; IsEnabled: Boolean);
begin
  Action.Enabled := IsEnabled;
end;

procedure TMainForm.SetVisibilityBasedOnRights;
var
  SerialMessage: string;
  SerialMessage2: string; //QMANTWO-521


procedure MakeVisible(Action: TCustomAction; IsVisible: Boolean);
begin
  Action.Visible := IsVisible;
end;

begin
  CheckForMessages;
  FCompSerialNumMismatch := not(PermissionsDM.CanIUseComputer(DM.UQState.EmpID)); //QMANTWO-521
  if not(FCompSerialNumMismatch) and (DM.AD_Active) then begin
    SetupAction(WorkManagementAction, PermissionsDM.IsTicketManager);
    SetupAction(Reports, PermissionsDM.CanIRunReports);
    SetupAction(FindTickets, PermissionsDM.CanI(RightSearchTickets));
    SetupAction(FindWorkOrders, PermissionsDM.CanI(RightSearchWorkOrders));
    SetupAction(Damage, LDamageDM.HaveDamagesRights);
    SetupAction(Timesheet, PermissionsDM.CanI(RightTimesheetsEntry), (not FLunchLockout) );
    SetupAction(Approval, PermissionsDM.CanI(RightTimesheetsApprove) or PermissionsDM.CanI(RightTimesheetsFinalApprove));
    SetupAction(Assets, True);
    SetupAction(NewTicketAction, PermissionsDM.CanI(RightCreateNewTicket));
    SetupAction(Billing, PermissionsDM.EmployeeHasBillingRights);
    SetupAction(MessageAction, PermissionsDM.CanI(RightMessagesSend) or PermissionsDM.CanI(RightMessagesReview));
    SetupAction(MyWorkAction, True);
    SetupAction(TicketDetail, True);
    SetupAction(Synchronize, True, (not FMessagesAvailable));
    SetupAction(SystemConfig, True);
    SetupAction(SearchTickets, True);
    SetupAction(AreaEditorAction, PermissionsDM.CanI(RightRoutingEditAreas));
    if PermissionsDM.CanI(RightEmpPlus) then
      OQButton.Action := ViewPlusList
    else
      OQButton.Action := ViewOQList;

    SetupAction(ViewPlusList, PermissionsDM.CanI(RightEmpPlus));  //QM-585 PLUS Whitelist Part 3 EB
    SetupAction(ViewOQList, PermissionsDM.CanI(RightOQList)); //QM-444 Part 2 EB



    SetupAction(ViewMarsList, PermissionsDM.CanI(RightTicketsMARSBulkStatusMgmt)); //QM-671 EB MARS
    MakeVisible(UtiliSafeAction, PermissionsDM.CanI(RightTimesheetsUtiliSafe)); //QM-100 EB UtiliSafe
//    SetupAction(SafetySiteAction, StrContainsText('180.', DM.UQState.DisplayName));  //QMANTWO-348  QMANTWO-657 always show
     { TODO -osr -cneeded : Retrieve the permission from server.  This is effectly hardcoded }
  end
  else
  begin  {User is not authorized for this computer}
    SetupAction(Synchronize, True, (not FMessagesAvailable));
    EnableAction(WorkManagementAction, False);
    EnableAction(Reports, False);
    EnableAction(FindTickets, False);
    EnableAction(FindWorkOrders, False);
    EnableAction(Damage, False);
    EnableAction(Timesheet, False );
    EnableAction(Approval, False);
    EnableAction(Assets, False);
    EnableAction(NewTicketAction, False);
    EnableAction(Billing, False);
    EnableAction(MessageAction, False);
    EnableAction(MyWorkAction, False);
    EnableAction(TicketDetail, False);
    EnableAction(Synchronize, True);
    EnableAction(SystemConfig, False);
    EnableAction(SearchTickets, False);
    EnableAction(AreaEditorAction, False);
//    EnableAction(SafetySiteAction, StrContainsText('180.', DM.UQState.DisplayName));    //QMANTWO-348  QMANTWO-657 always show

    if not(DM.AD_Active) then   //QMANTWO-521
      SerialMessage2 :=  'The Active Directory associated with this login is not valid '
    else
      SerialMessage2 :='';

    {If it is already True, then we have already displayed a message}
    if FCompSerialNumMismatch then
    begin
//      FCompSerialNumMismatch := False;  {Keep this at the end so that we don't pop up twenty messages}
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeSerialMismatch, '');
      SerialMessage := 'Device Serial Number Mismatch: ' + #13#10 +// #13#10 +
                       'The device serial number does not match the one assigned to the logged in user.' +
                       #13#10 + '  [  ' + 'S# ' + GetLocalComputerSerial + '  ]';
      SerialMessage := wraptext(SerialMessage, 35);
    end
    else
      SerialMessage := '';

    if ((Not DM.AD_Active) or (FCompSerialNumMismatch=True))  then
      MessageDlg(SerialMessage2+ #13#10 + SerialMessage + #13#10+ #13#10 +'Please call ' +DMu.HelpDeskContact,mtError,[mbOK],0);

  end;  //  {User is not authorized for this computer}
  ToolBar.Realign;
end;

procedure TMainForm.TurnAllOtherActionsOff(RulingAction: TCustomAction);
begin
  SetupAction(Synchronize, True, (not FMessagesAvailable));
  EnableAction(WorkManagementAction, False);
  EnableAction(Reports, False);
  EnableAction(FindTickets, False);
  EnableAction(FindWorkOrders, False);
  EnableAction(Damage, False);
  EnableAction(Timesheet, False );
  EnableAction(Approval, False);
  EnableAction(Assets, False);
  EnableAction(NewTicketAction, False);
  EnableAction(Billing, False);
  EnableAction(MessageAction, False);
  EnableAction(MyWorkAction, False);
  EnableAction(TicketDetail, False);
  EnableAction(Synchronize, True);
  EnableAction(SystemConfig, False);
  EnableAction(SearchTickets, False);
  EnableAction(AreaEditorAction, False);
end;

procedure TMainForm.SyncDone(Sender: TObject);
begin
  TimeKeeper.Initialize(DM.UQState.EmpID, DM.Engine);
  ValidateUsageTime;
  CheckLockouts;
  SetVisibilityBasedOnRights;
  StartUsageTimeCheck(True);
  StopSyncReminder;
  SetupGPS;
  DM.CurrentReportID := '';
  SetupBackgroundUploader;
  SetupAutoForegroundSync;
  DM.RefreshMyAllowedStatusCodes;
  DM.UQState.DisplayName := EmployeeDM.GetUserShortName;
  DM.UQState.ProfitCenter := DM.GetUserProfitCenter;
  DM.UQState.SaveToIni;
  SBar.Panels[EmployeeStatusPanelIdx].Text := DM.UQState.DisplayName;
  DM.StartTimeImportTimer;
  DM.InitializeHttpServer(IdleTimer);
  CanShutDown:=true;  //QMANTWO-803
end;

procedure TMainForm.AssetsExecute(Sender: TObject);
begin
  if PermissionsDM.CanI(RightManageAssets) then
    ActivateForm('Asset Management')
  else
    ActivateForm('My Assets');
end;

procedure TMainForm.SendErrorReportActionExecute(Sender: TObject);
var
  ErrText: string;
begin
  ErrText := InputBox('Error Report Testing', 'Enter your error report text:', '');
  if ErrText <> '' then
    DM.Engine.AddErrorReport(DM.EmpID, Now, ErrText, '');
end;

procedure TMainForm.ApplyXmlFileActionExecute(Sender: TObject);
var
  SL: TStringList;
begin
  if ChooseXmlFileDialog.Execute then begin
    SL := TStringList.Create;
    try
      SL.LoadFromFile(ChooseXmlFileDialog.FileName);
      DM.Engine.SyncTablesFromXmlString(SL.Text);
    finally
      FreeAndNil(SL);
    end;
  end;
end;

procedure TMainForm.ApprovalExecute(Sender: TObject);
begin
  ComingFromApprovalForm := True;
  ActivateForm('Timesheet Approval');
end;

procedure TMainForm.EditTimesheet(EmpID: Integer; EditDay: TDateTime; Mode: TTimesheetEditMode);
begin
  (ActivateForm('Timesheet') as TTimesheetForm).ShowWeek(EmpID, EditDay, Mode);
  If (FActiveEmbeddedForm is TTimesheetForm) then begin                   //Don't think this is needed
    if TTimesheetForm(FactiveEmbeddedForm).ShowUtiliSafeButton then  //QM-100 EB UtiliSafe
      UtiliSafeButton.Enabled := True;
  end;

end;

procedure TMainForm.SetOldTicketsButtonEnabled; //qm-754 sr
begin
//  OldTicketsButton.visible :=false;
//  OldTicketsButton.visible := PermissionsDM.CanI(RightShowOldTickets, False); //qm-754 sr
//  OldTicketsButton.Refresh;
end;

procedure TMainForm.ShowUpdatedReportStatus(Sender: TObject);
var
  Form: TReportForm;
begin
  Form := CreateOrGetFormByID('Reports') as TReportForm;
  Form.UpdateReportStatusDisplay;
end;

procedure TMainForm.NewTicketActionExecute(Sender: TObject);
begin
  ActivateForm('New Ticket');
end;

procedure TMainForm.BillingExecute(Sender: TObject);
begin
  ActivateForm('Billing');
  if FActiveEmbeddedForm is TBillingMainForm then
    TBillingMainForm(FActiveEmbeddedForm).Initialize;
end;

function TMainForm.GetRegisteredForm(const FormID: string): TEmbeddableForm;
var
  FormClass: TEmbeddableFormClass;
begin
  Result := nil;
  FormClass := DM.GetRegisteredFormClass(FormID);
  if Assigned(FormClass) then
    Result := CreateEmbeddedForm(FormClass, Self, ContentPanel);
end;


procedure TMainForm.GoToUtiliSafeURL;
var    //QM-100 EB UtiliSafe
  BaseURL,
  HashString : string;
  EmpIDStr : string;
  Pepper, LPepper, RPepper,  Link: string;
  EpochTimeStr:string;
const
  EID  = 'empid=';
  SC   = '&source=qm&code=';
  FormName = 'qm';
  EPOCH_TIME='&et='; //SR
  BASE_URL='UtilisafeBaseURL';  //SR
begin
  BaseURL := DM.GetConfigurationDataValue(UpperCase(BASE_URL),'',True);  //SR  was failing due to case mismatch
  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  EpochTimeStr := IntToStr(DateTimeToUnix(Now)); //SR
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);
  EmpIDStr := IntToStr(DM.EmpID);
  HashString := LPepper +
                EmpIDStr +
                FormName +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);

  Link := BaseURL +
          EID + EmpIDStr +
          SC +
          HashString+EPOCH_TIME+EpochTimeStr;  //SR

  if BaseURL = '' then begin
    ShowMessage('No URL entry in configuration_data. Could not open Utlisafe link.');
    Exit;
  end;
  try
    ShellExecute(0,'open', PChar(Link), nil, nil, SW_SHOW);
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeUtiliSafe, '', NOW);
    PostMessage(Self.Handle, UM_UTILISAFE, 0, 0);
  except
  on e:exception do
    ShowMessage('Critical error at ' + Link + '  ' + e.message);
  end;
end;

procedure TMainForm.ShowDamageThirdParty(Sender: TObject; ID: Integer);
var
  Form: TDamageDetailsForm;
begin
  Form := CreateOrGetFormByID(frmDamageDetail) as TDamageDetailsForm;
  Form.GotoThirdPartyId(ID);
  ActivateForm(frmDamageDetail);
end;

procedure TMainForm.MessageAckActionExecute(Sender: TObject);
begin
  if DM.UnackedBreakRuleMessageCount > 0 then
    HoursMessageAckAction.Execute
  else
    ActivateForm('MessageAck');
end;

procedure TMainForm.MessageAcked(Sender: TObject);
begin
  CheckForMessages;
  SetVisibilityBasedOnRights;
end;

procedure TMainForm.ProcessBreakRuleResponse(Sender: TObject; const MessageDestID, RuleID: Integer; const Response: string; var OkToClose: Boolean);
var
  Accept: Boolean;
  ContactPhone: string;
const
  MessageText = 'You have indicated there is a discrepancy with your recorded working ' +
    'hours for this week. Please enter a contact phone number below and a ' +
    'representative from the Human Resources department will contact you to ' +
    'discuss the matter. If you selected this operation by error, please ' +
    'press Cancel to return to the acknowledgment screen.';
begin
  ContactPhone := '';
  Accept := (SameText(Response, 'accept'));

  if not Accept then
    ContactPhone := ShowMessageAckPromptDialog(MessageText, 'Ok,Cancel', 'Contact Phone Number:', '', '');

  OkToClose := Accept or (NotEmpty(ContactPhone));
  if OkToClose then
    DM.AddBreakRuleResponse(RuleID, Response, ContactPhone, Now, MessageDestID);
end;

procedure TMainForm.CheckForMessages;
begin
  FAlteredHoursMessagesAvailable := DM.UnackedBreakRuleMessageCount > 0;
  FMessagesAvailable := (DM.UnackedMessageCount > 0) or FAlteredHoursMessagesAvailable;
  MessageIcon.Visible := FMessagesAvailable;
  MessageAckAction.Visible := FMessagesAvailable;
  ToolBar.Realign;
end;

procedure TMainForm.ShowStartingScreen;
begin
  SetVisibilityBasedOnRights;
  fUtiliSafeAutoOpened := False;  //QM-100 EB Only used for UtiliSafe
  if FRestrictedUse then
    Exit
  else if DM.HaveSyncedData = False then
    Synchronize.Execute
  else if CheckIsClockedOut = False then begin
    if PermissionsDM.CanI(RightTimesheetsClockEntry) or PermissionsDM.CanI(RightTimesheetsClockImport) then
      TimesheetExecute(Self)
    else if FAlteredHoursMessagesAvailable then
      HoursMessageAckAction.Execute
    else if FMessagesAvailable then
      MessageAckAction.Execute
    else
      MyWorkAction.Execute;
  end;
end;

procedure TMainForm.MessageIconClick(Sender: TObject);
begin
  MessageAckAction.Execute;
end;

procedure TMainForm.MessageActionExecute(Sender: TObject);
begin
  ActivateForm('Messages');
end;

procedure TMainForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

procedure TMainForm.SetWarningMessage(Message: string);
begin
  WarningLabel.Caption := Message;
end;

procedure TMainForm.StartWarningMessage(Message: string);
begin
  SetWarningMessage(Message);
  WarningLabel.Visible := True;
end;

procedure TMainForm.StopWarningMessage;
begin
  SetWarningMessage('');
  WarningLabel.Visible := False;
end;

procedure TMainForm.StartSyncReminder;
begin
  StartWarningMessage('Sync Now');
end;

procedure TMainForm.StopSyncReminder;
begin
  StopWarningMessage;
end;

procedure TMainForm.SyncStart(Sender: TObject);
begin
  CanShutDown:=false;  // //QMANTWO-803
  DM.CheckForApproachingEOD; //QM-370  sr
  DM.StopTimeImportTimer;
  StopIdleTimer;
  StopSyncReminder;
  StopUsageTimeCheck;
  Update;
end;

function TMainForm.MustSyncNow: Boolean;
var
  SyncReminderInterval: Integer;
begin
  SyncReminderInterval := DM.GetSyncReminderInterval;
  Result := (SyncReminderInterval>0) and ((SecondsBetween(Now, DM.UQState.LastLocalSyncTime))>SyncReminderInterval);
end;

procedure TMainForm.WMEndSession(var Msg: TMessage);
begin
  if (Msg.lParam and ENDSESSION_LOGOFF) = ENDSESSION_LOGOFF then
    EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinLogOff, '');
end;

procedure TMainForm.WMPowerBroadcast(var Msg: TMessage);
begin
  case Msg.WParam of
    PBT_APMSuspend          : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCSleep,'Suspend');
    PBT_APMStandby          : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCSleep,'Standby');
    PBT_APMResumeCritical   : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCWake,'Critical');
    PBT_APMResumeSuspend    : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCWake,'Suspend');
    PBT_APMResumeAutomatic  : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCWake,'Auto');
    PBT_APMResumeStandby    : EmployeeDM.AddEmployeeActivityEvent(ActivityTypePCWake,'Standby');
  end;
end;

procedure TMainForm.WMQueryEndSession(var Msg: TWMQueryEndSession);
begin
  //test for Sync state here  QMANTWO-803
  if CanShutDown then begin
    Msg.Result:= 0;
    DM.AddToQMLog('Windows shutdown delayed until Sync complete');
  end
  else Msg.Result:= 1
end;

procedure TMainForm.WMSessionChange(var Msg: TMessage);
begin
  case Msg.WParam of
    WTS_CONSOLE_CONNECT    : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinConnect,'Local');
    WTS_CONSOLE_DISCONNECT : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinDisconnect,'Local');
    WTS_REMOTE_CONNECT     : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinConnect,'Remote');
    WTS_REMOTE_DISCONNECT  : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinDisconnect,'Remote');
    WTS_SESSION_LOGON      : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinLogOn,'');
    WTS_SESSION_LOGOFF     : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinLogOff,'');
    WTS_SESSION_LOCK       : EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinLock,'');
    WTS_SESSION_UNLOCK     : begin
      DM.UQState.LastWindowsUnlockDateTime := IsoDateTimeToStr(Now);
      EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinUnlock, '');
    end;
  end;
end;

procedure TMainForm.WMUserChanged(var Msg: TMessage);
begin
  EmployeeDM.AddEmployeeActivityEvent(ActivityTypeWinUserChange,'')
end;

procedure TMainForm.StartAttachmentProcess(Mode: TAttachmentMode; const Param: string);
begin
  if (FActiveEmbeddedForm is TTicketDetailForm) then begin
    with FActiveEmbeddedForm as TTicketDetailForm do begin
      if ExecuteAttachmentProcess(Mode, Param) then
        if PermissionsDM.CanI(RightAutoSyncAndUpload) then
          with TAutoSyncTimerDialogForm.Create(Self) do begin
            if Execute(AppName, 'Auto sync and upload files to:', SecondsToAutoSyncDialog) then
              if Synchronize.Enabled then
                Synchronize.Execute;
            Free;
          end;
    end;
  end;
end;

procedure TMainForm.WMStiLaunchNotification(var Message: TMessage);
begin
  StartAttachmentProcess(amStillImage);
end;

procedure TMainForm.OnDeviceConnected(Sender: TObject);
begin
  if (FActiveEmbeddedForm is TTicketDetailForm) then
    (FActiveEmbeddedForm as TTicketDetailForm).UpdateCameraStatus;
  StartAttachmentProcess(amStillImage);
end;

procedure TMainForm.OnDeviceDisconnected(Sender: TObject);
begin
  if (FActiveEmbeddedForm is TTicketDetailForm) then
    (FActiveEmbeddedForm as TTicketDetailForm).UpdateCameraStatus;
end;

procedure TMainForm.OnPlatsFinished(var Msg: TMessage);
begin
  //QM-164 - Plats Update Unfreeze after done
  IdleTimer.Enabled := False;
  ToolBar.Enabled := True;
end;

procedure TMainForm.OnPlatsUpdating(var Msg: TMessage);
begin
  //QM-164 - Plats Update Must freeze any other actions while processing
  StopIdleTimer;
  Toolbar.Enabled := False;
end;

procedure TMainForm.OnWorkStarted(var Msg: TMessage);  //QM-100 EB UtiliSafe
begin
  if fUtiliSafeAutoOpened then
    exit;
  If CheckUtiliSafeStatus or (not fUtilisafeAutoOpened) then
    GoToUtiliSafeURL;
  fUtiliSafeAutoOpened := True;  //EB - Set this no matter what
end;

procedure TMainForm.ValidateUsageTime;
var
  MessageText: string;
  TimeLeft: Integer;
begin
  StopUsageTimeCheck;
  try
    UpdateUsageStatus('');
    if DM.UserHasLimitedHours then begin
      TimeLeft := TimeKeeper.SecondsRemaining;
      UpdateUsageStatus('Left: ' + SecondsToHM(TimeLeft, True));
      MessageText := TimeKeeper.NextMessage;
      if Length(MessageText) > 0 then
        MessageDlg(MessageText, mtWarning, [mbOk], 0);
      SetRestrictedUse(TimeLeft <= 0);
    end;
  finally
    StartUsageTimeCheck(False);
  end;
end;

procedure TMainForm.ViewMarsListExecute(Sender: TObject);
begin
  ActivateForm('MARS Bulk Status') as TfrmMARSBulkStatus;  //QM-671 EB MARS Bulk Status
 // ActivateForm('Employee Plus List') as TPlusMgrListForm;
end;

procedure TMainForm.ViewOQListExecute(Sender: TObject);
begin
  ActivateForm('OQ List') as TOQManagementForm;  //QM-444 OQ Part 2 EB
end;

procedure TMainForm.ViewPlusListExecute(Sender: TObject);
begin
  ActivateForm('Employee Plus List') as TPlusMgrListForm;   //QM-585 Part 3 EB
end;

procedure TMainForm.SetRestrictedUse(Value: Boolean);
begin
  if Value <> FRestrictedUse then begin
    FRestrictedUse := Value;
    SetVisibilityBasedOnRights;
  end;

  if FRestrictedUse and not (FActiveEmbeddedForm is TRestrictedUseForm) then
    (ActivateForm('RestrictedUse') as TRestrictedUseForm).LoadAndDisplayMessage(TimeKeeper.DescriptionOfTimeLimits)
  else if (not FRestrictedUse) and (FActiveEmbeddedForm is TRestrictedUseForm) then
    ShowStartingScreen;
end;

procedure TMainForm.UpdateUsageStatus(const MessageText: string);
begin
  SBar.Panels.Items[UsageStatusPanelIdx].Text := MessageText;
end;

procedure TMainForm.StartUsageTimeCheck(CheckNow: Boolean);
begin
  if CheckNow and DM.UserHasLimitedHours then
    ValidateUsageTime;
  CheckUsageTimer.Enabled := DM.UserHasLimitedHours;
end;

procedure TMainForm.StopUsageTimeCheck;
begin
  CheckUsageTimer.Enabled := False;
end;

procedure TMainForm.CheckUsageTimerTimer(Sender: TObject);
begin
  ValidateUsageTime;
end;

function TMainForm.CheckUtiliSafeStatus: boolean;  //QM-100 EB UtiliSafe 2
begin
  Result := False;
  if PermissionsDM.CanI(RightTimesheetsUtiliSafe) then begin
    UtilisafeAction.Enabled := True;
    UtiliSafeButton.Visible := True;
    UtiliSafeButton.Enabled := True;
    if (EmployeeDM.HasRecordedEmployeeActivity(ActivityTypeUtiliSafe)) or
    (EmployeeDM.AlreadyHasActivityToday(ActivityTypeUtiliSafe, DM.EmpID)) then begin
      SetVisibilityBasedOnRights;
      Result := True;
    end;
  end
  else begin
    SetVisibilityBasedOnRights;
    UtilisafeButton.Visible := False;
  end;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(TimeKeeper);
  FreeAndNil(IdleTimer);
end;

procedure TMainForm.WmDeviceChange(var Message: TWMDeviceChange);
begin
  case Message.Event of
    DBT_DEVICEARRIVAL:
      begin
        DeviceNotification.DeviceConnected(Message.dwData);
      end;
    DBT_DEVNODES_CHANGED:
      if (FReadyToAttachFiles) and DeviceNotification.HasDeviceConnected and
         (DeviceNotification.DeviceDriveLetter <> '') and DM.IsAttachmentDriveLetter(DeviceNotification.DeviceDriveLetter) then begin
        FReadyToAttachFiles := False;
        StartAttachmentProcess(amDeviceNotification, DeviceNotification.DeviceDriveLetter);
      end;
    DBT_DEVICEREMOVECOMPLETE:
      begin
        DeviceNotification.DeviceDisconnected;
        FReadyToAttachFiles := True;
      end;
  end;
end;

procedure TMainForm.AreaEditorActionExecute(Sender: TObject);
begin
  ActivateForm('AreaEditor');
end;

procedure TMainForm.HoursMessageAckActionExecute(Sender: TObject);
begin
  ActivateForm('HoursMessageAck');
end;

procedure TMainForm.SetupBackgroundUploader;
begin
  UploadStatusPanel.Visible := DM.CanBackgroundUpload;
  DM.BackgroundUploaderStart;
end;

procedure TMainForm.HandleWorkerStatus(var Msg: TMessage);
begin
  if TObject(Msg.LParam) is TUploadStatus then
    ShowUploadStatus(Msg)
  else if TObject(Msg.LParam) is TGPSStatus then
    ShowGPSStatus(Msg)
  else if TObject(Msg.LParam) is TWorkerStatus then
    ShowWorkerThreadStatus(Msg)
  else
    raise Exception.Create('Unexpected worker thread status message');
end;

procedure TMainForm.ShowWorkerThreadStatus(var Msg: TMessage);
var
  Status: TWorkerStatus;
begin
  Status := TWorkerStatus(Msg.LParam);
  Assert(TObject(Msg.LParam) is TWorkerStatus, 'ShowWorkerStatus needs a TWorkerStatus object parameter.');
  try
    DM.ShowTransientMessage(Status.MessageText);
  finally
    FreeAndNil(Status);
  end;
end;

procedure TMainForm.ShowUploadStatus(var Msg: TMessage);
var
  Status: TUploadStatus;
begin
  Assert(TObject(Msg.LParam) is TUploadStatus, 'ShowUploadStatus needs a TUploadStatus object parameter.');

  Status := TUploadStatus(Msg.LParam);
  try
    UploadStatusMessageLabel.Caption := Status.MessageText;
    if Status.PendingUploadCount > -1 then
      UploadFileCountLabel.Caption := IntToStr(Status.PendingUploadCount);
    if Status.PendingUploadSize > -1 then begin
      if Status.PendingUploadSize > 0 then
        UploadFileSizeLabel.Caption := IntToStr(Status.PendingUploadSize) + 'KB'
      else
        UploadFileSizeLabel.Caption := '-';
    end;
    if Status.MessageType = umtError then begin
      DM.ShowTransientMessage(Status.MessageText);
      DM.AddToQMLog(Status.MessageText);
    end;
    if DM.FBackgroundUploader.InProgress then
      UploadStatusPanel.Visible := True;
    UpdateUploadStatusPanelColors(Status);
  finally
    FreeAndNil(Status);
  end;
end;

procedure TMainForm.UploadStatusMessageLabelDblClick(Sender: TObject);
begin
  if DM.UQState.DeveloperMode then
    DM.BackgroundUploaderStart;
end;

procedure TMainForm.UtiliSafeActionExecute(Sender: TObject);
begin
  GoToUtiliSafeURL;
end;

procedure TMainForm.UpdateUploadStatusPanelColors(Status: TUploadStatus);
var
  MaxPendingCount: Integer;
  MaxPendingSize: Integer;
  MaxWaitSeconds: Integer;
begin
  if Status.PendingUploadCount < 0 then
    Exit;

  MaxPendingCount := DM.GetConfigurationDataInt('AttachWarningMaxFiles', 100);
  MaxPendingSize := DM.GetConfigurationDataInt('AttachWarningMaxSizeKb', 10000);
  MaxWaitSeconds := DM.GetConfigurationDataInt('AttachWarningMaxWaitHrs', 24) * 60 * 60;
  if ((Status.PendingUploadSize >= MaxPendingSize) or
    (Status.PendingUploadCount >= MaxPendingCount) or
    (Status.LongestFileWaitSeconds >= MaxWaitSeconds)) then
    UploadStatusPanel.Font.Color := clYellow
  else
    UploadStatusPanel.Font.Color := clWhite;
  UploadFileCountLabel.Font.Color := UploadStatusPanel.Font.Color;
  UploadFileSizeLabel.Font.Color := UploadStatusPanel.Font.Color;
end;

procedure TMainForm.ApplicationIdleTimeElapsed(Sender: TObject);
begin
  if PermissionsDM.CanI(RightSyncAfterIdle) then
    AutoForegroundSync('IDLE');
end;

procedure TMainForm.SetupAutoForegroundSync;
var
  DelaySeconds: Integer;
  DelaySecondsMinimized: Integer;
  SecondsBetween: Integer;
begin
  if PermissionsDM.CanI(RightSyncAfterIdle) or PermissionsDM.CanI(RightTimesheetsClockImport) then begin
    DM.GetAutoSyncIdleLimits(DelaySeconds, DelaySecondsMinimized, SecondsBetween);
    IdleTimer.IdleDelaySeconds := DelaySeconds;
    IdleTimer.IdleDelaySecondsMinimized := DelaySecondsMinimized;
    IdleTimer.SecondsBetweenActivity := SecondsBetween;
    IdleTimer.LastActiveTime := DM.UQState.LastLocalSyncTime;
    IdleTimer.Enabled := True;
    AutoSyncStatusPanel.Visible := DM.UQState.DeveloperMode;
    RefreshAutoSyncStatus(Format('Config: Delay Min=%d, Minimized Delay Min=%d, ' +
      'Min Between=%d; Last Sync=%s',
      [DelaySeconds div 60, DelaySecondsMinimized div 60,
      SecondsBetween div 60, DateTimeToStr(IdleTimer.LastActiveTime)]));
  end else begin
    IdleTimer.Enabled := False;
    AutoSyncStatusPanel.Visible := False;
  end;
end;

procedure TMainForm.StopIdleTimer;
begin
  IdleTimer.Enabled := False;
  RefreshAutoSyncStatus(TimeToStr(Now) + ' Idle sync timer paused.');
end;

procedure TMainForm.AutoForegroundSync(const Source: string);

  function OkToSyncNow: Boolean;
  var
    CannotSyncReason: string;
  begin
    if not Synchronize.Enabled then begin
      RefreshAutoSyncStatus(TimeToStr(Now) + ' Cannot sync because Synchronize action is disabled');
      Result := False;
    end else if (FActiveEmbeddedForm is TReportForm) then begin
      RefreshAutoSyncStatus(TimeToStr(Now) + ' Cannot sync from a report params form');
      Result := False;
    end else if not DM.SafeToSyncNow(CannotSyncReason) then begin
      RefreshAutoSyncStatus(TimeToStr(Now) + ' Not safe to sync because ' + CannotSyncReason);
      Result := False;
    end else
      Result := True;
  end;

begin
  if OkToSyncNow then begin
    DM.SyncRequestSource := Source;
    Synchronize.Execute;
  end;
end;

procedure TMainForm.RefreshAutoSyncStatus(const Msg: string);
begin
  AutoSyncStatusLabel.Caption := Msg;
  if IdleTimer.IsApplicationIdle then begin
    ElapsedIdleTimeLabel.Caption := ' Idle: ' + SecondsToTime(IdleTimer.SecondsIdle);
    if IdleTimer.SecondsIdle >= IdleTimer.SecondsBetweenActivity then
      ElapsedIdleTimeLabel.Font.Color := clYellow
    else
      ElapsedIdleTimeLabel.ParentFont := True;
  end else
    ElapsedIdleTimeLabel.Caption := '';
end;

procedure TMainForm.AfterTimesheetSave(Sender: TObject);
begin
//  DebugSleep := DM.UQState.DeveloperSyncWait1;
  if ComingFromApprovalForm then begin
    Sleep(100);  //QM-614 EB Let local timesheet changes settle
//    Sleep(DebugSleep);

    DM.SendTimeChangesToServer;
  end
  else begin
    if PermissionsDM.CanI(RightSyncAfterTimesheetSave) then
      AutoForegroundSync('TIMESAVE');
    Assert(Sender is TTimesheetForm);
    if (Sender as TTimesheetForm).NeedToShutdown then
      DM.ShutdownWindows;
  end;

  CheckUtiliSafeStatus;     //QM-100 EB UtiliSafe Modified
  CheckIsClockedOut;
end;

procedure TMainForm.ProcessAttachmentDataRequest(var Msg: TMessage);
var
  DataRequest: TParentDataRequest;
begin
  Assert(TObject(Msg.LParam) is TParentDataRequest, 'ProcessAttachmentDataRequest needs a TParentDataRequest object parameter.');

  DataRequest := TParentDataRequest(Msg.LParam);
  try
    DM.GetAttachmentParentData(DataRequest.ForeignType, DataRequest.ForeignID);
  finally
    FreeAndNil(DataRequest);
  end;
end;

procedure TMainForm.ShowTicketWorkFullScreen(Sender: TObject);
var
  AutoTimeoutSecs: Integer;
begin
  AutoTimeoutSecs := StrToInt(PermissionsDM.GetLimitationDef(RightTicketsShowFullWorkScreen, '120'));
  TicketWorkFullFrame.Prepare(DM.Ticket.FieldByName('ticket_id').AsInteger, True, AutoTimeoutSecs);
  TicketWorkFullFrame.OnCloseClick := TicketWorkFullScreenClosed;
  FullWindowPanel.BringToFront;
  FullWindowPanel.Align := alClient;
  FullWindowPanel.Visible := True;
  Toolbar.Visible := False;
end;

procedure TMainForm.TicketWorkFullScreenClosed(Sender: TObject);
begin
  Toolbar.Visible := True;
  FullWindowPanel.Visible := False;
  FullWindowPanel.SendToBack;
end;

procedure TMainForm.SetupGPS;

begin
//  if DM.CanI(RightTrackGPSPosition) then begin
    DM.BackgroundGPSStart;
//  end else begin
//    DM.BackgroundGPSStop;
//    SBar.Panels[GPSStatusPanelIdx].Text:= '';
//  end;
end;

procedure TMainForm.ShowGPSStatus(var Msg: TMessage);
const
  DegreesSymbol = #176;
var
  Status: TGPSStatus;
  GPSData : TGPSData;
  mapHandle:THandle;
  SndHandle:THandle;
  copyDataStruct: TcopyDataStruct;
  ret:integer;
  PosErrorMsg: string;
begin
  try
    try
    Assert(TObject(Msg.LParam) is TGPSStatus, 'ShowGPSStatus needs a TGPSStatus object parameter.');
    if GPSStatusPanelIdx >= SBar.Panels.Count then
      Exit;

    Status := TGPSStatus(Msg.LParam);
//    if DM.CanI(RightTrackGPSPosition) then begin
      if NotEmpty(Status.MessageText) then begin
        DM.ShowTransientMessage(Status.MessageText);
        DM.AddToQMLog(Status.MessageText);
      end;
      if Status.HavePosition then begin
        PosErrorMsg := 'GPS Position found';
        DM.CurrentGPSFixTime := Now;
        DM.CurrentGPSLatitude := 0.0;    //...sr       qmantwo-203
        DM.CurrentGPSLongitude := 0.0;   //...sr       qmantwo-203
        DM.CurrentGPSLatitude := Status.Latitude;
        DM.CurrentGPSLongitude := Status.Longitude;
        GPSData.Lat:=  FloatToStr(DM.CurrentGPSLatitude); //QMANTWO-711
        GPSData.Lng:=  FloatToStr(DM.CurrentGPSLongitude);//QMANTWO-711
        GPSData.dateSend := Now;                           //QMANTWO-711
        copyDataStruct.dwData := integer(cdtRecord);   //QMANTWO-711
        copyDataStruct.cbData := sizeof(GPSData);     //QMANTWO-711
        copyDataStruct.lpData := @GPSData;            //QMANTWO-711

        mapHandle:=FindWindow('TfrmStreetMapTickets',NIL); //qm-686 sr
        if mapHandle <> 0 then                      //qm-686 sr
          ret:=SendMessage(mapHandle, WM_COPYDATA, integer(SndHandle), integer(@copyDataStruct));    //qm-686 sr

        DM.CurrentGPSHDOP := Status.HDOP;
        PosErrorMsg := 'GPS Pos: ' + floattostr(Status.Latitude) + ' : ' + floattostr(Status.Longitude);
        SBar.Panels[GPSStatusPanelIdx].Text := Format('%.3f%s,%.3f%s',
          [Status.Latitude, DegreesSymbol, Status.Longitude, DegreesSymbol]);
      end
      else begin
        PosErrorMsg := 'GPS not Found';
        DM.CurrentGPSFixTime := 0.0;
        DM.CurrentGPSLatitude := 0.0;
        DM.CurrentGPSLongitude := 0.0;
        DM.CurrentGPSHDOP := -1;
        SBar.Panels[GPSStatusPanelIdx].Text := 'No GPS';
      end;
//    end;
    except
      raise Exception.Create('GPS format Error: ' + PosErrorMsg);
    end;
  finally
    FreeAndNil(Status);
  end;
end;

//procedure TMainForm.SendRecord(cdt: TcopyDataType);
//begin
////
//end;

procedure TMainForm.ShowWorkOrder(Sender: TObject; ID: Integer);
begin
  with ActivateForm(frmWorkOrderDetail) as TWorkOrderDetailForm do
    GotoWorkOrder(ID, ComingFromManagementForm);
end;

procedure TMainForm.SafetySiteActionExecute(Sender: TObject);
var    //QMANTWO-348
  HashString : string;
  EmpIDStr : string;
  Pepper, LPepper, RPepper, BaseURL, Link: string;
  Safety_Link : string;  //QMANTWO-591  SR
const
  TID  = 'tid=0';
  LID  = 'Lid=0';
  EID  = 'Eid=';
  FM   = 'FM=';
  CODE = 'code=';
  AMP  = '&';
  FORMNAME = 'JobSafety';

begin
  Safety_Link := DM.GetConfigurationDataValue('JobSafetyURL');  //QMANTWO-591  SR
  Pepper := DM.GetConfigurationDataValue('EPR_Pepper');
  BaseURL := SAFETY_LINK;  //DM.GetConfigurationDataValue('JobSafetyURL');
  LPepper := Copy(Pepper, 1, 16);
  RPepper := Copy(Pepper,17, 32);

  EmpIDStr := IntToStr(DM.EmpID);   //DM.UQState.EmpID
  HashString := LPepper +
                '0' +
                '0' +
                EmpIDStr +
                FORMNAME +
                RPepper;
  HashString := CalcHash2(HashString, haSHA1);
  Link :=  BaseURL +
                    TID + AMP +
                    LID + AMP +
                    EID + EmpIDStr + AMP +
                    FM + FORMNAME + AMP +
                    CODE + HashString;

  try
    ShellExecute(0,'open',PChar(LINK),nil,nil, SW_SHOW);
  except
  on e:exception do
    ShowMessage('Critical error at '+LINK +'  '+e.message);
  end;

end;

procedure TMainForm.SBarDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
  if (Panel = SBar.Panels[ProgressBarStatusPanelIdx]) then
  with ProgressBar do begin
    Top := Rect.Top - 2;
    Left := Rect.Left - 2;
    Width := Rect.Right - Rect.Left + 4;
    Height := Rect.Bottom - Rect.Top;
  end;
end;

procedure TMainForm.ScreenshotTimerTimer(Sender: TObject);
begin
  ScreenshotTimer.Enabled := False;
  CheckScreenShot;
  ScreenshotTimer.Enabled := True;
end;

procedure TMainForm.HideProgressBar(Sender: TObject);
begin
  SBar.Panels[ProgressBarStatusPanelIdx].Style := psText;
  ProgressBar.Visible := False;
  ProgressBar.Position := 0;
  ProgressBar.Properties.Text := '';
  SBar.Repaint;
end;

procedure TMainForm.ShowProgressBar(Sender: TObject;
  const ProgressMessage: string; const PctComplete: Integer);
begin
  if not ProgressBar.Visible then
    ProgressBar.Visible := True;
  if SBar.Panels[ProgressBarStatusPanelIdx].Style <> psOwnerDraw then
    SBar.Panels[ProgressBarStatusPanelIdx].Style := psOwnerDraw;
  ProgressBar.Position := PctComplete;
  ProgressBar.Properties.Text := ProgressMessage;
  SBar.Repaint;
end;

procedure TMainForm.TimeImportTimerTimer(Sender: TObject);
//Switch to the timesheet, close any dialogs, then import time.
//ImportTimeMonitor shortens the TimeImportTimer's interval when files are
//detected via the shell; then TimeImportTimer does the actual time import
begin
  if not TimeFileMonitorDM.AnyPendingTimeImportFiles then
    TimeFileMonitorDM.TimeImportTimer.Interval := 30000;//30 secs

  if not TimeFileMonitorDM.AnyImportableFiles then
    Exit;

  DM.StopTimeImportTimer;
  try
    DM.PrepareToImportTime; //cancel all dataset changes in progress

    //Close any message dialogs that the user has left hanging around
    if (Screen.ActiveCustomForm <> nil) and
      (Screen.ActiveCustomForm.ClassName = 'TMessageForm') then
      Screen.ActiveCustomForm.Close;

    if not (FActiveEmbeddedForm is TTimesheetForm) then begin
      TimeFileMonitorDM.NavigatingToTimesheetForImport := True;
      (ActivateForm(frmTimesheet) as TTimesheetForm).ShowTodaysTimesheet;
    end;
    (FActiveEmbeddedForm as TTimesheetForm).PerformTimeImport;
  finally
    TimeFileMonitorDM.NavigatingToTimesheetForImport := False;
    DM.StartTimeImportTimer;
  end;
end;

end.
