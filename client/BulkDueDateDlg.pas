unit BulkDueDateDlg;    //EB QM-959 Bulk Due Date Update
{This dialog is for changing due dates on a bulk selection}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ComCtrls, dxCore, dxSkinsCore, StdCtrls,
  cxDateUtils, Dmu, Clipbrd, DateUtils, QMConst,
  cxMemo, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfrmBulkDueDateEdit = class(TForm)
    DtEdNewDueDate: TcxDateEdit;
    lblNewDueDate: TLabel;
    cxmTicketList: TcxMemo;
    btnUpdateDueDate: TButton;
    btnClose: TButton;
    btnCopyResults: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnUpdateDueDateClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnCopyResultsClick(Sender: TObject);
    procedure DtEdNewDueDatePropertiesEditValueChanged(Sender: TObject);
  private
    fTicketsUpdated: boolean;
    fIncomingTicketIDList: TStringList;
    fIncomingDisplayList: TStringList;
  public
    property IncomingTicketList: TStringList read fIncomingTicketIDList;
    property IncomingDisplayList: TStringList read fIncomingDisplayList;
    property TicketsUpdated: boolean read fTicketsUpdated;
    procedure SetTicketLists(pTicketDisplay, pTickets: TStrings);
    class function Execute(pTicketDisplayList, pTicketIDList: TStrings): boolean;

  end;


var
  frmBulkDueDateEdit: TfrmBulkDueDateEdit;

implementation

{$R *.dfm}

procedure TfrmBulkDueDateEdit.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmBulkDueDateEdit.btnCopyResultsClick(Sender: TObject);
begin
  Clipboard.AsText := cxmTicketList.Text;
end;

procedure TfrmBulkDueDateEdit.btnUpdateDueDateClick(Sender: TObject);
var
  lTicketStr: string;
  lNewDueDate: TDateTime;
  MsgStr: string;
  MsgStrList: TStringList;
  i: integer;
begin
  MsgStr:= '';
  MsgStrList := TStringList.Create;
  MsgStrList.Delimiter := ';';
  MsgStrList.StrictDelimiter := True;
  try
    lNewDueDate := DtEdNewDueDate.Date;
    if (lNewDueDate > (Today)) and (fIncomingTicketIDList.DelimitedText <> '') then begin
      MsgStr := DM.BulkTicketDueDatesToServer(fIncomingTicketIDList.DelimitedText, lNewDueDate);
      MsgStrList.DelimitedText := MsgStr;
      fTicketsUpdated := True;
      for i := 0 to MsgStrList.Count - 1 do
        cxmTicketList.Lines.Add(MsgStrList[i]);
    end;
  finally
    FreeAndNil(MsgStrList);
  end;

end;


procedure TfrmBulkDueDateEdit.DtEdNewDueDatePropertiesEditValueChanged(
  Sender: TObject);
const
  MSG = 'New Due Date is invalid. Date must be greater than ';
begin
  if (DtEdNewDueDate.Date > Today) then
    btnUpdateDueDate.Enabled := True
  else begin
    MessageDlg(MSG + DateTimeToStr(Today),mtError, [mbOK], 0);
    btnUpdateDueDate.Enabled := False;
  end;
end;

class function TfrmBulkDueDateEdit.Execute(pTicketDisplayList, pTicketIDList: TStrings): boolean;
const
  MSG = 'No Tickets were selected for update.';
var
  DefDT: TDateTime;
begin
  frmBulkDueDateEdit := TfrmBulkDueDateEdit.Create(nil);
  DefDT := Tomorrow + StrToTime('5PM');
  try
    if pTicketIDList.Count = 0 then begin
      MessageDlg(MSG,mtError, [mbOK], 0);
      Exit;
    end;
    With frmBulkDueDateEdit do begin
      SetTicketLists(pTicketDisplayList, pTicketIDList);
      DtEdNewDueDate.Date := DefDT;

      cxmTicketList.Lines.Clear;

      {Using a different delimiter for the memo so that we can return the results text
       and then have it force line feeds in the memobox}
      cxmTicketList.Lines.Delimiter := ';';
      cxmTicketList.Lines.StrictDelimiter := True;
      cxmTicketList.Lines.Add('TICKETS SELECTED ' + 'Count= ' + IntToStr(pTicketDisplayList.Count) + ' Tickets: ' + pTicketDisplayList.DelimitedText);
      cxmTicketList.Lines.Add(DashLineSep);
    end;

    if (frmBulkDueDateEdit.ShowModal = mrOK) then begin
      Result := frmBulkDueDateEdit.TicketsUpdated;
    end;

  finally
    FreeAndNil(frmBulkDueDateEdit);
  end;
end;

procedure TfrmBulkDueDateEdit.FormCreate(Sender: TObject);
begin
  inherited;
  fIncomingTicketIDList := TStringList.Create;  {Objects}
  fIncomingDisplayList:= TStringList.Create;
end;

procedure TfrmBulkDueDateEdit.FormDestroy(Sender: TObject);
var
  i: integer;
begin
  {Note: Attached TObjects are strings and clear with the TStringlist}
  if assigned(fIncomingTicketIDList) then
    FreeAndNil(fIncomingTicketIDList);
  if assigned(fIncomingDisplayList) then
    FreeAndNil(fIncomingDisplayList);
  inherited;
end;

procedure TfrmBulkDueDateEdit.SetTicketLists(pTicketDisplay, pTickets: TStrings);
var
  i: integer;
  lTicketIDStr, lTicketNum: string;
begin
{ See comments about why this was done long hand}
  fIncomingTicketIDList.Delimiter := ',';
  fIncomingTicketIDList.StrictDelimiter := True;
  fIncomingDisplayList.Delimiter := ',';
  fIncomingDisplayList.StrictDelimiter := True;
//  fIncomingTicketIDList.DelimitedText := pTickets.DelimitedText;
  for i := 0 to pTickets.Count - 1 do begin
    lTicketIDStr := pTickets.Strings[i];
    lTicketNum := String(pTickets.Objects[i]);
    fIncomingTicketIDList.AddObject(lTicketIDStr, pTickets.Objects[i]);
  end;

  fIncomingDisplayList.DelimitedText := pTicketDisplay.DelimitedText;
end;

end.
