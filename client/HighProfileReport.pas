unit HighProfileReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect, CheckLst;

type
  THighProfileReportForm = class(TReportBaseForm)
    GroupBox1: TGroupBox;
    RangeSelect: TOdRangeSelectFrame;
    GroupBox2: TGroupBox;
    ManagersComboBox: TComboBox;
    ManagerRadio: TRadioButton;
    OfficeRadio: TRadioButton;
    OfficesComboBox: TComboBox;
    TicketsGroup: TGroupBox;
    TicketsComboBox: TComboBox;
    TicketsLabel: TLabel;
    HighProfileReasonsCheckListBox: TCheckListBox;
    Label1: TLabel;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    CallCenterClientsBox: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    CallCenterList: TCheckListBox;
    ClientList: TCheckListBox;
    SelectAllClientsButton: TButton;
    ClearAllClientsButton: TButton;
    procedure ManagersComboBoxEnter(Sender: TObject);
    procedure OfficesComboBoxEnter(Sender: TObject);
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure CallCenterListClick(Sender: TObject);
    procedure SelectAllClientsButtonClick(Sender: TObject);
    procedure ClearAllClientsButtonClick(Sender: TObject);
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
    function GetManagerID: Integer;
    function GetOfficeID: Integer;
    procedure SelectAllReasons;
    procedure ClearAllReasons;
    function AllReasonsSelected: Boolean;
  end;

implementation

uses
  DMu, OdExceptions, OdVclUtils, QMConst;

{$R *.dfm}

{ THighProfileReportForm }

procedure THighProfileReportForm.ValidateParams;
var
  ReasonString: string;
  I: Integer;
  Obj: TObject;
  CallCenters: string;
  Clients: string;
begin
  inherited;
  if OfficeRadio.Checked then begin
    if GetOfficeID = -1 then begin
      OfficesComboBox.SetFocus;
      raise EOdEntryRequired.Create('Please enter a valid office ID.');
    end;
  end
  else if ManagerRadio.Checked then begin
    if GetManagerID = -1 then begin
      ManagersComboBox.SetFocus;
      raise EOdEntryRequired.Create('Please select a manager.');
    end;
  end
  else
    Assert(False);

  if AllReasonsSelected then
    ReasonString := ''    // server interprets this as ALL
  else begin
    for I := 0 to HighProfileReasonsCheckListBox.Items.Count - 1 do begin
      if HighProfileReasonsCheckListBox.Checked[I] then begin
        Obj := HighProfileReasonsCheckListBox.Items.Objects[I]; // get ref id
        if ReasonString = '' then begin
          ReasonString := IntToStr(Integer(Obj));
        end else begin
          ReasonString := ReasonString + ',' + IntToStr(Integer(Obj));
        end;
      end;
    end;
    if ReasonString = ''
      then raise EOdEntryRequired.Create('You must select at least one high profile reason.');
  end;

  SetReportID('HighProfile');
  SetParamDate('DateFrom', RangeSelect.FromDate);
  SetParamDate('DateTo', RangeSelect.ToDate);
  SetParamInt('OfficeId', GetOfficeID);
  SetParamInt('ManagerId', GetManagerID);
  SetParamInt('Kind', TicketsComboBox.ItemIndex);
  SetParam('HighProfileIDs', ReasonString);

  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters<>'' then begin
    Clients := GetCheckedItemString(ClientList, True);
    if Clients = '' then begin
      ClientList.SetFocus;
      raise EOdEntryRequired.Create('Please select the clients to report on.');
    end;
    SetParam('ClientIDs', Clients);
  end;
end;

procedure THighProfileReportForm.InitReportControls;
var
  i: TOpenClosedCriteria;
begin
  inherited;

  SetUpManagerList(ManagersComboBox);
  DM.OfficeList(OfficesComboBox.Items);
  DM.GetHighProfileReasons(HighProfileReasonsCheckListBox.Items);
  SelectAllReasons;

  for i := Low(TOpenClosedCriteria) to High(TOpenClosedCriteria) do
    TicketsComboBox.Items.Add(OpenClosedDescription[i]);
  TicketsComboBox.ItemIndex := 0;

  SetupCallCenterList(CallCenterList.Items, False);
end;

function THighProfileReportForm.GetManagerID: Integer;
begin
  Result := -1;
  if ManagerRadio.Checked then
    if ManagersComboBox.ItemIndex > -1 then
      Result := GetComboObjectInteger(ManagersComboBox);
end;

function THighProfileReportForm.GetOfficeID: Integer;
begin
  Result := -1;
  if OfficeRadio.Checked then
    if OfficesComboBox.ItemIndex > -1 then
      Result := GetComboObjectInteger(OfficesComboBox);
end;

procedure THighProfileReportForm.ManagersComboBoxEnter(Sender: TObject);
begin
  ManagerRadio.Checked := True;
end;

procedure THighProfileReportForm.OfficesComboBoxEnter(Sender: TObject);
begin
  OfficeRadio.Checked := True;
end;

procedure THighProfileReportForm.SelectAllReasons;
begin
  SetCheckListBox(HighProfileReasonsCheckListBox, True);
end;

procedure THighProfileReportForm.ClearAllReasons;
begin
  SetCheckListBox(HighProfileReasonsCheckListBox, False);
end;

procedure THighProfileReportForm.SelectAllButtonClick(Sender: TObject);
begin
  SelectAllReasons;
end;

procedure THighProfileReportForm.ClearAllButtonClick(Sender: TObject);
begin
  ClearAllReasons;
end;

function THighProfileReportForm.AllReasonsSelected: Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := 0 to HighProfileReasonsCheckListBox.Items.Count - 1 do begin
    if not HighProfileReasonsCheckListBox.Checked[I] then begin
      Result := False;
      Break;
    end;
  end;
end;

procedure THighProfileReportForm.CallCenterListClick(Sender: TObject);
var
  CallCenters: string;
begin
  CallCenters := GetCheckedItemCodesString(CallCenterList);
  if CallCenters <> '' then
    DM.ClientList(CallCenters, ClientList.Items)
  else
    ClientList.Clear;
end;

procedure THighProfileReportForm.SelectAllClientsButtonClick(
  Sender: TObject);
begin
  SetCheckListBox(ClientList, True);
end;

procedure THighProfileReportForm.ClearAllClientsButtonClick(
  Sender: TObject);
begin
  SetCheckListBox(ClientList, False);
end;

end.
