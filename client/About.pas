unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TAboutForm = class(TForm)
    LogoImage: TImage;
    AppLabel: TLabel;
    VersionLabel: TLabel;
    OkButton: TButton;
    HelpDeskContact: TLabel;
    lblServer: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OkButtonClick(Sender: TObject);
  protected
    procedure Loaded; override;
  end;

  procedure ShowAboutBox;

implementation

{$R *.dfm}

uses DMu, OdVclUtils, Terminology, OdMiscUtils;

procedure TAboutForm.FormCreate(Sender: TObject);
begin
  if DM.UQState.BrandLocatorsInc then
    LoadBitmapResource(LogoImage, 'LOC_LOGO')
  else
    LoadBitmapResource(LogoImage, 'LOGO');

  AppLabel.Caption := DMu.AppName;
  VersionLabel.Caption := 'Version ' + AppVersionShort + ' ' + DMu.AppVersionMarker;
  HelpDeskContact.Caption := DMu.HelpDeskContact;
  lblServer.Caption := lblServer.Caption+DM.UQState.ServerURL; //QMANTWO-320
end;

procedure ShowAboutBox;
begin
  TAboutForm.Create(nil).Show;
end;

procedure TAboutForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;  // release myself
end;

procedure TAboutForm.OkButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TAboutForm.Loaded;
begin
  inherited Loaded;
  // Automatically search for and replace industry specific terminology
  if (AppTerminology <> nil) and (AppTerminology.Terminology <> '')
    then AppTerminology.ReplaceVCL(Self);
end;

end.

