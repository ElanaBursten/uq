inherited NoResponseTicketsReportForm: TNoResponseTicketsReportForm
  Left = 559
  Top = 317
  Caption = 'No Response Tickets'
  ClientHeight = 191
  ClientWidth = 406
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 0
    Top = 8
    Width = 105
    Height = 13
    Caption = 'Transmit Date Range:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 100
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  inline TransmitDate: TOdRangeSelectFrame [2]
    Left = 40
    Top = 24
    Width = 248
    Height = 59
    TabOrder = 0
    inherited FromLabel: TLabel
      Left = 4
      Alignment = taRightJustify
    end
    inherited DatesLabel: TLabel
      Left = 4
      Alignment = taRightJustify
    end
    inherited DatesComboBox: TComboBox
      Left = 42
      Width = 203
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 42
    end
    inherited ToDateEdit: TcxDateEdit
      Width = 89
    end
  end
  object CallCenterCombo: TComboBox
    Left = 82
    Top = 97
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
end
