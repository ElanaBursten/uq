object frmBulkDueDateEdit: TfrmBulkDueDateEdit
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Bulk Due Date Edit'
  ClientHeight = 269
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    324
    269)
  PixelsPerInch = 96
  TextHeight = 13
  object lblNewDueDate: TLabel
    Left = 8
    Top = 3
    Width = 69
    Height = 13
    Caption = 'New Due Date'
  end
  object DtEdNewDueDate: TcxDateEdit
    Left = 8
    Top = 17
    Properties.DateButtons = [btnClear]
    Properties.Kind = ckDateTime
    Properties.OnEditValueChanged = DtEdNewDueDatePropertiesEditValueChanged
    TabOrder = 0
    Width = 137
  end
  object cxmTicketList: TcxMemo
    Left = 9
    Top = 44
    Anchors = [akLeft, akTop, akRight]
    Properties.ReadOnly = True
    Properties.ScrollBars = ssVertical
    Style.Color = clMenu
    TabOrder = 1
    Height = 187
    Width = 307
  end
  object btnUpdateDueDate: TButton
    Left = 154
    Top = 16
    Width = 134
    Height = 22
    Caption = 'Update Ticket Due Dates'
    Enabled = False
    TabOrder = 2
    OnClick = btnUpdateDueDateClick
  end
  object btnClose: TButton
    Left = 240
    Top = 237
    Width = 76
    Height = 24
    Anchors = [akTop, akRight]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 3
    OnClick = btnCloseClick
  end
  object btnCopyResults: TButton
    Left = 8
    Top = 236
    Width = 75
    Height = 25
    Caption = 'Copy Results'
    TabOrder = 4
    OnClick = btnCopyResultsClick
  end
end
