inherited DailyClientLocatesReportForm: TDailyClientLocatesReportForm
  Left = 357
  Top = 153
  Caption = 'DailyClientLocatesReportForm'
  ClientHeight = 295
  ClientWidth = 364
  PixelsPerInch = 96
  TextHeight = 13
  object CallCenterLabel: TLabel [0]
    Left = 0
    Top = 185
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object ClientCodeLabel: TLabel [1]
    Left = 0
    Top = 212
    Width = 59
    Height = 13
    Caption = 'Client Code:'
  end
  object LocatesClosedLabel: TLabel [2]
    Left = 0
    Top = 238
    Width = 41
    Height = 13
    Caption = 'Locates:'
  end
  object SyncDateRangeLabel: TLabel [3]
    Left = 0
    Top = 7
    Width = 87
    Height = 13
    Caption = 'Sync Date Range:'
  end
  object TransmitDateRangeLabel: TLabel [4]
    Left = 0
    Top = 89
    Width = 105
    Height = 13
    Caption = 'Transmit Date Range:'
  end
  object EditClientCode: TEdit [5]
    Left = 67
    Top = 209
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object CallCenterComboBox: TComboBox [6]
    Left = 67
    Top = 182
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object LocatesClosed: TComboBox [7]
    Left = 67
    Top = 235
    Width = 121
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 2
  end
  object PrintImagesBox: TCheckBox [8]
    Left = 67
    Top = 264
    Width = 193
    Height = 17
    Caption = 'Print Ticket Images (one per page)'
    TabOrder = 3
  end
  inline SyncDate: TOdRangeSelectFrame [9]
    Left = 23
    Top = 21
    Width = 249
    Height = 57
    TabOrder = 4
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesLabel: TLabel
      Top = 37
    end
  end
  inline TransmitDate: TOdRangeSelectFrame
    Left = 20
    Top = 102
    Width = 249
    Height = 56
    TabOrder = 5
    inherited FromLabel: TLabel
      Left = 11
      Top = 11
    end
    inherited ToLabel: TLabel
      Left = 138
      Top = 11
    end
    inherited DatesLabel: TLabel
      Left = 11
      Top = 37
    end
    inherited DatesComboBox: TComboBox
      Left = 47
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 47
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 159
    end
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 2
  end
end
