inherited TicketActivityReportForm: TTicketActivityReportForm
  Left = 460
  Top = 204
  Caption = 'Ticket Activity'
  PixelsPerInch = 96
  TextHeight = 13
  object ManagerLabel: TLabel [0]
    Left = 0
    Top = 11
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Manager:'
  end
  object Label1: TLabel [1]
    Left = 0
    Top = 48
    Width = 100
    Height = 13
    Caption = 'Activity Date Range:'
  end
  object Label2: TLabel [2]
    Left = 0
    Top = 124
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagerCombo: TComboBox [3]
    Left = 96
    Top = 8
    Width = 184
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  inline ActivityDateRange: TOdRangeSelectFrame [4]
    Left = 50
    Top = 62
    Width = 268
    Height = 53
    TabOrder = 1
    inherited FromLabel: TLabel
      Left = 7
    end
    inherited ToLabel: TLabel
      Left = 140
    end
    inherited DatesLabel: TLabel
      Left = 7
      Top = 35
    end
    inherited DatesComboBox: TComboBox
      Left = 46
      Top = 32
      Width = 203
    end
    inherited FromDateEdit: TcxDateEdit
      Left = 46
      Top = 7
      Width = 88
    end
    inherited ToDateEdit: TcxDateEdit
      Left = 160
      Top = 7
      Width = 88
    end
  end
  object EmployeeStatusCombo: TComboBox
    Left = 96
    Top = 123
    Width = 202
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
end
