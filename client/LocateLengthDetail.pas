unit LocateLengthDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, LengthConverter,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxSpinEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  ComCtrls, dxCore, cxDateUtils;

type
  TLocateLengthEditor = class(TForm)
    LocateLengthDetailPanel: TPanel;
    WorkDateLabel: TLabel;
    WorkDateEdit: TcxDateEdit;
    LenMarkedLabel: TLabel;
    LenMarkedEdit: TcxSpinEdit;
    LenTotalLabel: TLabel;
    QtyTotalLabel: TLabel;
    LenTotalEdit: TEdit;
    QtyTotalEdit: TEdit;
    OkButton: TButton;
    CancelButton: TButton;
    QtyTotalSpinEdit: TcxSpinEdit;
    lblOverridetxt: TLabel;
    procedure LenMarkedEditPropertiesChange(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure QtyTotalSpinEditPropertiesEditValueChanged(Sender: TObject);
  private

    function GetLengthMarked: Integer;
    procedure SetLengthMarked(const Value: Integer);
  private
    FPopupWidth, FPopupHeight: integer;  //QMANTWO-208 use this to set the ideal size when there is a message
    FOriginalLengthTotal: Integer;
    FOriginalLengthMarked: Integer;
    FConversionCalc: TLengthConversionCalc;
    FLocateID: Integer;
    FEntryCancelled: Boolean;
    FUserChangedSpinner : Boolean;
    function ComputeQuantityMarked: Integer;
    function Locates: TDataSet;
    function GetLengthTotal: Integer;
    function GetQuantityTotal: Integer;
    procedure SetLengthTotal(const Value: Integer);
    procedure SetQuantityTotal(const Value: Integer);
  public

    FovrClients: string;  //QMANTWO-208
    useOverRide: boolean; //QMANTWO-208
    property LengthMarked: Integer read GetLengthMarked write SetLengthMarked;
    property LengthTotal: Integer read GetLengthTotal write SetLengthTotal;
    property QuantityTotal: Integer read GetQuantityTotal write SetQuantityTotal;
    property PopupWidth: integer read fPopupWidth write fPopupWidth;
    property PopupHeight: integer read fPopupHeight write fPopupHeight;
    procedure SetupEditor(const LocateID: Integer; const WorkDate: TDateTime; const LenMarked, LenTotal,
          QtyTotal:Integer; client_code:string; lbl:string='');
    procedure SetupEditorLocateParent(const LocateID: Integer; const WorkDate: TDateTime; const LenMarked, LenTotal,
          QtyTotal: Integer;client_code:string);
    function ValidateLocateLength: Boolean;

    destructor Destroy; override;
    class function CreateWithClients(AOwner: TComponent; ovrClients: string): TLocateLengthEditor;
  end;

implementation

uses DMu, OdDBUtils, OdMiscUtils, OdExceptions;

{$R *.dfm}

function TLocateLengthEditor.Locates: TDataSet;
begin
  Result := DM.LocateData;
end;

procedure TLocateLengthEditor.QtyTotalSpinEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  FUserChangedSpinner := True;
end;

procedure TLocateLengthEditor.SetupEditor(const LocateID: Integer;
  const WorkDate: TDateTime; const LenMarked, LenTotal, QtyTotal: Integer; client_code:String; lbl:string='');
const
  LblStatement = 'Override: To be used for %s only under certain situations';
var
  OverrideClients: string;
begin
  if Lbl = '' then Lbl := LblStatement; //QMANTWO-616 EB Need to be able to customize for Project Locate
  
  FreeAndNil(FConversionCalc);
  FConversionCalc := TLengthConversionCalc.Create(Locates);
  LenMarkedLabel.Caption := FConversionCalc.UnitType + ' Marked:';

  WorkDateEdit.Date := WorkDate;
  LengthMarked := LenMarked;
  LengthTotal := LenTotal;
  QuantityTotal := QtyTotal;
  FLocateID := LocateID;
  FOriginalLengthMarked := LenMarked;
  FOriginalLengthTotal := LenTotal;
  FEntryCancelled := False;

 FovrClients := StringReplace(FovrClients, '|', ',', [rfReplaceAll]);
  OverrideClients := UPPERCASE(fOvrClients);
  client_code := UPPERCASE(client_code);

  if (AnsiPos(client_code, OverrideClients) > 0) then  //QMANTWO-208
  begin
    useOverRide := true;
    QtyTotalSpinEdit.BringToFront;
    QtyTotalEdit.SendToBack;
    lblOverridetxt.Caption := format(Lbl, [client_code]);
    lblOverridetxt.Visible := True;
    FPopupWidth := 208;
    FPopupHeight := 165;
  end
  else
  begin
    useOverRide := false;
    QtyTotalSpinEdit.SendToBack;
    QtyTotalEdit.BringToFront;
    lblOverridetxt.Visible := False;
    FPopupWidth := 200;
    FPopupHeight := 150;
  end

end;

procedure TLocateLengthEditor.SetupEditorLocateParent(const LocateID: Integer;
  const WorkDate: TDateTime; const LenMarked, LenTotal, QtyTotal: Integer;
  client_code: string);
var
  LblStatement: string;
begin
  LblStatement :=  'Length Total for is calculated for all locates assigned to %s';
  LblStatement := format(LblStatement, [client_code]);
  SetupEditor(LocateID, WorkDate, LenMarked, LenTotal, QtyTotal, client_code, LblStatement);

end;

procedure TLocateLengthEditor.LenMarkedEditPropertiesChange(Sender: TObject);
begin
  LengthTotal := FOriginalLengthTotal - FOriginalLengthMarked + LengthMarked;
  QuantityTotal := ComputeQuantityMarked;
end;

function TLocateLengthEditor.ComputeQuantityMarked: Integer;
begin
  Assert(Assigned(FConversionCalc), 'No length conversion calculator assigned');
  Result := FConversionCalc.LengthAsBillingUnits(LengthTotal);
end;


class function TLocateLengthEditor.CreateWithClients(AOwner: TComponent; ovrClients: string): TLocateLengthEditor;
begin
  Result := TLocateLengthEditor.Create(AOwner);
  Result.FovrClients := '';
  Result.useOverRide := false;
  Result.FovrClients := ovrClients;
  Result.PopupWidth := 205;
  Result.PopupHeight := 160;
end;

function TLocateLengthEditor.ValidateLocateLength: Boolean;

  function SomeLengthEntered: Boolean;
  begin
    Result := ((FEntryCancelled = False) and (FOriginalLengthMarked <> LengthMarked)) or (FUserChangedSpinner);
  end;

begin
  Assert(Locates.FieldByName('locate_id').AsInteger = FLocateID);

  Result := False;
  if SomeLengthEntered then begin
    if (WorkDateEdit.Date < (BeginningOfTheWeek(Now) - 7)) or (WorkDateEdit.Date > EndingOfTheWeek(Now)) then
      raise EOdDataEntryError.Create('The work date must be during this week or last week.');

    if LengthMarked < 0 then
      raise EOdDataEntryError.Create('The length must be greater than or equal 0.');

    if LengthMarked > LenMarkedEdit.Properties.MaxValue then
      raise EOdDataEntryError.Create('The length can not be greater than ' +
                 FloatToStr(LenMarkedEdit.Properties.MaxValue) + '.');

    EditDataSet(DM.LocateData);
    DM.LocateData.FieldByName('length_marked').AsInteger := LengthMarked;
    DM.LocateData.FieldByName('length_total').AsInteger := LengthTotal;
    DM.LocateData.FieldByName('qty_marked').AsInteger := QuantityTotal;

    Result := True;
  end;
end;

function TLocateLengthEditor.GetLengthMarked: Integer;
begin
  // LenMarkedEdit's edit mask insures numbers only, but .Text may be ''
  Result := StrToIntDef(LenMarkedEdit.Text, 0);
end;

function TLocateLengthEditor.GetLengthTotal: Integer;
begin
  Result := StrToIntDef(LenTotalEdit.Text, 0);
end;

function TLocateLengthEditor.GetQuantityTotal: Integer;
begin
  if useOverRide = true then         //QMANTWO-208
    Result := QtyTotalSpinEdit.value
  else
    Result := StrToIntDef(QtyTotalEdit.Text, 0);
end;

procedure TLocateLengthEditor.SetLengthMarked(const Value: Integer);
begin
  LenMarkedEdit.EditValue := Value;
end;

procedure TLocateLengthEditor.SetLengthTotal(const Value: Integer);
begin
  LenTotalEdit.Text := IntToStr(Value);
end;

procedure TLocateLengthEditor.SetQuantityTotal(const Value: Integer);
begin
  if useOverRide = True then
    QtyTotalSpinEdit.Value := Value
  else
    QtyTotalEdit.Text := IntToStr(Value);
end;

destructor TLocateLengthEditor.Destroy;
begin
  FreeAndNil(FConversionCalc);
  inherited;
end;

procedure TLocateLengthEditor.CancelButtonClick(Sender: TObject);
begin
  FEntryCancelled := True;
end;


end.
