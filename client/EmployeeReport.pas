unit EmployeeReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, CheckLst;

type
  TEmployeeReportForm = class(TReportBaseForm)
    ManagerLabel: TLabel;
    ManagerCombo: TComboBox;
    EmployeeStatusLabel: TLabel;
    EmployeeStatusCombo: TComboBox;
    LabelSortBy: TLabel;
    SortByCombo: TComboBox;
    LabelEmployeeType: TLabel;
    EmployeeTypeList: TCheckListBox;
  protected
    procedure InitReportControls; override;
    procedure PopulateSortByCombo;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu, OdVclUtils, LocalEmployeeDMu;

procedure TEmployeeReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;

  EmployeeDM.InitEmployeeStatusCombo(EmployeeStatusCombo);

  EmployeeDM.EmpTypeList(EmployeeTypeList.Items);
  SetCheckListBox(EmployeeTypeList, True);

  PopulateSortByCombo;
  if SortByCombo.Items.Count > 1 then
    SortByCombo.ItemIndex := 0;
end;

procedure TEmployeeReportForm.PopulateSortByCombo;
var
  List: TStrings;
begin
  List := SortByCombo.Items;
  List.Clear;
  List.Add('Employee Last Name');
  List.Add('Employee Number');
  List.Add('Payroll Profit Center');
end;

procedure TEmployeeReportForm.ValidateParams;
begin
  inherited;
  SetReportID('Employees');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
  SetParamInt('EmployeeStatus', GetComboObjectInteger(EmployeeStatusCombo));
  SetParam('EmployeeTypeList', GetCheckedItemString(EmployeeTypeList, True));

  if SortByCombo.ItemIndex <> -1 then
    SetParamInt('SortBy', SortByCombo.ItemIndex)
  else
    SetParamInt('SortBy', 0);
end;

end.
