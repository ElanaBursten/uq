inherited ClosedTicketDetailReportForm: TClosedTicketDetailReportForm
  Left = 357
  Top = 229
  Caption = 'ClosedTicketDetailReportForm'
  ClientHeight = 506
  ClientWidth = 625
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 72
    Height = 13
    Caption = 'Ticket Number:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 42
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object Label3: TLabel [2]
    Left = 0
    Top = 73
    Width = 71
    Height = 13
    Caption = 'Transmit Date:'
  end
  object LabelSelectImages: TLabel [3]
    Left = 90
    Top = 177
    Width = 261
    Height = 13
    Caption = 'Select attachments to print: (limited to 160 per report)'
  end
  object Label4: TLabel [4]
    Left = 264
    Top = 5
    Width = 123
    Height = 26
    Caption = 'Use the button to search for a ticket to print.'
    WordWrap = True
  end
  object CallCenterEdit: TEdit [5]
    Left = 90
    Top = 39
    Width = 166
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 1
  end
  object IncludeNotes: TCheckBox [6]
    Left = 90
    Top = 98
    Width = 118
    Height = 17
    Caption = 'Include Ticket Notes'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object IncludeAttachments: TCheckBox [7]
    Left = 90
    Top = 147
    Width = 174
    Height = 17
    Caption = 'Include Ticket Attachments'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = IncludeAttachmentsClick
  end
  object IncludeFacilities: TCheckBox [8]
    Left = 90
    Top = 122
    Width = 142
    Height = 17
    Caption = 'Include Ticket Facilities'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object AttachmentList: TCheckListBox [9]
    Left = 90
    Top = 194
    Width = 285
    Height = 217
    OnClickCheck = AttachmentListClickCheck
    ItemHeight = 13
    TabOrder = 6
  end
  object SelectAllButton: TButton [10]
    Left = 385
    Top = 194
    Width = 75
    Height = 25
    Caption = 'Select All'
    TabOrder = 7
    OnClick = SelectAllButtonClick
  end
  object ClearAllButton: TButton [11]
    Left = 385
    Top = 226
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 8
    OnClick = ClearAllButtonClick
  end
  object TransmitDateEdit: TEdit [12]
    Left = 90
    Top = 70
    Width = 166
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 2
  end
  object TicketNumberEdit: TcxButtonEdit [13]
    Left = 90
    Top = 8
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.ReadOnly = True
    Properties.OnButtonClick = TicketNumberEditButtonClick
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 166
  end
  inherited SaveTSVDialog: TSaveDialog
    Left = 0
    Top = 318
  end
end
