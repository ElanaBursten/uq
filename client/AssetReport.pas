unit AssetReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls;

type
  TAssetReportForm = class(TReportBaseForm)
    ManagerCombo: TComboBox;
    ManagerLabel: TLabel;
  protected
    procedure InitReportControls; override;
    procedure ValidateParams; override;
  end;

implementation

{$R *.dfm}

uses DMu;

{ TAssetReportForm }

procedure TAssetReportForm.InitReportControls;
begin
  inherited;
  SetUpManagerList(ManagerCombo);
  if ManagerCombo.Items.Count > 1 then
    ManagerCombo.ItemIndex := 0;
end;

procedure TAssetReportForm.ValidateParams;
begin
  inherited;
  SetReportID('Assets');
  SetParamIntCombo('ManagerID', ManagerCombo, True);
end;

end.

