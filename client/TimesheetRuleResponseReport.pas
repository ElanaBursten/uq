unit TimesheetRuleResponseReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OdReportBase, StdCtrls, ExtCtrls, OdRangeSelect, EmployeeSelectFrame,
  ActnList;

type
  TTimesheetRuleResponseReportForm = class(TReportBaseForm)
    ResponseDateRange: TOdRangeSelectFrame;
    IncludeNoticeGroup: TRadioGroup;
    ResponseDateRangeLabel: TLabel;
    IncludeResponseGroup: TRadioGroup;
  protected
    procedure ValidateParams; override;
    procedure InitReportControls; override;
  end;

implementation

uses DMu, OdVclUtils, QMConst;

{$R *.dfm}

{ TTimesheetRuleResponseReportForm }

procedure TTimesheetRuleResponseReportForm.InitReportControls;
begin
  inherited;
  ResponseDateRange.SelectDateRange(rsThisWeek);
  IncludeResponseGroup.ItemIndex := 1;
  IncludeNoticeGroup.ItemIndex := 2;
end;

procedure TTimesheetRuleResponseReportForm.ValidateParams;
begin
  inherited;
  SetReportID('TimeSubmitMessageResponse');
  SetParamDate('DateStart', ResponseDateRange.FromDate, True);
  SetParamDate('DateEnd', ResponseDateRange.ToDate, True);
  case IncludeNoticeGroup.ItemIndex of
    0: SetParam('RuleType', 'SUBMIT');
    1: SetParam('RuleType', 'MGRALTER');
    else SetParam('RuleType', '');
  end;
  if IncludeResponseGroup.ItemIndex = 2 then
    SetParam('Response', '')
  else
    SetParam('Response', IncludeResponseGroup.Items[IncludeResponseGroup.ItemIndex]);
end;

end.


