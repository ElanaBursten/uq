unit BaseExpandableDataFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TExpandableDataFrameBase = class(TFrame)
    SummaryPanel: TPanel;
    DataPanel: TPanel;
    ExpandLabel: TLabel;
    DataSummaryPanel: TPanel;
    procedure SummaryPanelClick(Sender: TObject);
  private
    FExpandHandler: TNotifyEvent;
  public
    procedure Setup(EntityDescription: string; ExpandHandler: TNotifyEvent); virtual;
    procedure Contract(AAlign: TAlign);
    procedure Expand; virtual;
  end;

implementation

{$R *.dfm}

{ TExpandableDataFrameBase }

procedure TExpandableDataFrameBase.Contract(AAlign: TAlign);
begin
  Align := AAlign;
  Height := 41;
  SummaryPanel.Visible := True;
  DataPanel.Visible := False;
end;

procedure TExpandableDataFrameBase.Expand;
begin
  Align := alClient;
  SummaryPanel.Visible := False;
  DataPanel.Visible := True;
end;

procedure TExpandableDataFrameBase.Setup(EntityDescription: string; ExpandHandler: TNotifyEvent);
begin
  SummaryPanel.Align := alClient;
  DataPanel.Align := alClient;
  SummaryPanel.Caption := EntityDescription;
  DataSummaryPanel.Caption := EntityDescription;
  FExpandHandler := ExpandHandler;
  Contract(alTop);
end;

procedure TExpandableDataFrameBase.SummaryPanelClick(Sender: TObject);
begin
  FExpandHandler(Self);
end;

end.
