object LocatePlatForm: TLocatePlatForm
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Locate Plats'
  ClientHeight = 202
  ClientWidth = 312
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PlatEntryPanel: TPanel
    Left = 0
    Top = 0
    Width = 312
    Height = 202
    Align = alClient
    BevelOuter = bvNone
    Caption = ' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LocatePlatLabel: TLabel
      Left = 8
      Top = 8
      Width = 297
      Height = 41
      AutoSize = False
      Caption = 'Plats used when locating Client'
      WordWrap = True
    end
    object HowToLabel: TLabel
      Left = 8
      Top = 144
      Width = 263
      Height = 13
      Caption = 'If multiple plats were used, list each one on a new line.'
    end
    object PlatsMemo: TMemo
      Left = 7
      Top = 56
      Width = 297
      Height = 80
      Lines.Strings = (
        'PlatsMemo')
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object OKButton: TButton
      Left = 79
      Top = 170
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
    object CancelButton: TButton
      Left = 159
      Top = 170
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
end
