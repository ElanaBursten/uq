inherited TimesheetExceptionReportForm: TTimesheetExceptionReportForm
  Left = 501
  Top = 301
  Caption = 'TimesheetExceptionReportForm'
  ClientHeight = 192
  ClientWidth = 308
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 11
    Width = 27
    Height = 13
    Caption = 'Date:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 37
    Width = 46
    Height = 13
    Caption = 'Manager:'
  end
  object lbIncludeEmployee: TLabel [2]
    Left = 0
    Top = 65
    Width = 84
    Height = 13
    Caption = 'Employee Status:'
  end
  object ManagersComboBox: TComboBox [3]
    Left = 90
    Top = 34
    Width = 188
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object EmployeeStatusCombo: TComboBox [4]
    Left = 90
    Top = 62
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object DateEdit: TcxDateEdit [5]
    Left = 90
    Top = 8
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 88
  end
end
