inherited AuditReportForm: TAuditReportForm
  Left = 271
  Top = 301
  Caption = 'Receive Audit Report'
  ClientHeight = 60
  ClientWidth = 429
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 0
    Top = 8
    Width = 27
    Height = 13
    Caption = 'Date:'
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 38
    Width = 57
    Height = 13
    Caption = 'Call Center:'
  end
  object CallCenterComboBox: TComboBox [2]
    Left = 64
    Top = 35
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 18
    ItemHeight = 13
    TabOrder = 1
  end
  object DateEdit: TcxDateEdit [3]
    Left = 64
    Top = 5
    EditValue = 36892d
    Properties.DateButtons = [btnToday]
    Properties.SaveTime = False
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
    Width = 85
  end
end
