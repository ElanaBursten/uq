inherited TimesheetEmpSummaryReportForm: TTimesheetEmpSummaryReportForm
  Left = 395
  Top = 174
  Caption = 'TimesheetEmpSummaryReportForm'
  ClientHeight = 160
  ClientWidth = 379
  PixelsPerInch = 96
  TextHeight = 13
  object ReportDateLabel: TLabel [0]
    Left = 0
    Top = 43
    Width = 61
    Height = 13
    Caption = 'Date Range:'
  end
  object EmployeeLabel: TLabel [1]
    Left = 0
    Top = 11
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Employee:'
  end
  inline ReportDate: TOdRangeSelectFrame [2]
    Left = 44
    Top = 58
    Width = 248
    Height = 63
    TabOrder = 0
    inherited FromLabel: TLabel
      Top = 11
    end
    inherited ToLabel: TLabel
      Top = 11
    end
    inherited DatesComboBox: TComboBox
      Width = 201
    end
    inherited ToDateEdit: TcxDateEdit
      Width = 89
    end
  end
  inline EmployeeBox: TEmployeeSelect
    Left = 65
    Top = 0
    Width = 264
    Height = 36
    TabOrder = 1
    inherited EmployeeCombo: TComboBox
      Left = 22
      Top = 8
      Width = 203
    end
  end
end
