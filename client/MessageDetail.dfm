inherited MessageDetailForm: TMessageDetailForm
  Left = 323
  Top = 144
  Caption = 'Message Detail'
  ClientHeight = 491
  ClientWidth = 689
  Font.Charset = ANSI_CHARSET
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 241
    Top = 137
    Width = 4
    Height = 354
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 0
    Width = 689
    Height = 32
    Align = alTop
    Caption = ' '
    TabOrder = 0
    object DoneButton: TButton
      Left = 3
      Top = 3
      Width = 102
      Height = 25
      Caption = 'Done'
      TabOrder = 0
      OnClick = DoneButtonClick
    end
  end
  object MessagePanel: TPanel
    Left = 0
    Top = 32
    Width = 689
    Height = 105
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 5
      Top = 31
      Width = 58
      Height = 13
      Caption = 'Destination:'
    end
    object Label2: TLabel
      Left = 5
      Top = 8
      Width = 86
      Height = 13
      Caption = 'Message Number:'
    end
    object Label3: TLabel
      Left = 5
      Top = 54
      Width = 40
      Height = 13
      Caption = 'Subject:'
    end
    object Label6: TLabel
      Left = 420
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Show On:'
    end
    object lblExpires: TLabel
      Left = 420
      Top = 31
      Width = 39
      Height = 13
      Caption = 'Expires:'
    end
    object lblMessagesSentCap: TLabel
      Left = 420
      Top = 54
      Width = 76
      Height = 13
      Caption = 'Mesasges Sent:'
    end
    object lblNumSent: TLabel
      Left = 558
      Top = 54
      Width = 53
      Height = 13
      Caption = 'lblNumSent'
    end
    object Label4: TLabel
      Left = 420
      Top = 75
      Width = 123
      Height = 13
      Caption = 'Mesasges Acknowledged:'
    end
    object lblNumAck: TLabel
      Left = 558
      Top = 75
      Width = 48
      Height = 13
      Caption = 'lblNumAck'
    end
    object Destination: TDBEdit
      Left = 103
      Top = 28
      Width = 300
      Height = 21
      DataField = 'destination'
      DataSource = MessageSource
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
    object MessageNumber: TDBEdit
      Left = 103
      Top = 5
      Width = 300
      Height = 21
      DataField = 'message_number'
      DataSource = MessageSource
      ParentColor = True
      ReadOnly = True
      TabOrder = 1
    end
    object Subject: TDBEdit
      Left = 103
      Top = 51
      Width = 300
      Height = 21
      DataField = 'subject'
      DataSource = MessageSource
      ParentColor = True
      ReadOnly = True
      TabOrder = 2
    end
    object MessageActive: TDBCheckBox
      Left = 103
      Top = 74
      Width = 113
      Height = 17
      Caption = 'Active'
      DataField = 'active'
      DataSource = MessageSource
      TabOrder = 3
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object ShowDate: TDBEdit
      Left = 485
      Top = 5
      Width = 115
      Height = 21
      DataField = 'show_date'
      DataSource = MessageSource
      ParentColor = True
      ReadOnly = True
      TabOrder = 4
    end
    object ExpirationDate: TDBEdit
      Left = 486
      Top = 28
      Width = 114
      Height = 21
      DataField = 'expiration_date'
      DataSource = MessageSource
      ParentColor = True
      ReadOnly = True
      TabOrder = 5
    end
  end
  object pnlName: TPanel
    Left = 0
    Top = 137
    Width = 241
    Height = 354
    Align = alLeft
    TabOrder = 2
    object MessageRecipGrid: TcxGrid
      Left = 1
      Top = 1
      Width = 239
      Height = 352
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      object MessageRecipView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = MessageRecipSource
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'message_dest_id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = 'Count is 0'
            Kind = skCount
            Column = MessageRecipViewhas_acked1
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsBehavior.ImmediateEditor = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        object MessageRecipViewmessage_dest_id1: TcxGridDBColumn
          DataBinding.FieldName = 'message_dest_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 134
        end
        object MessageRecipViewhas_acked1: TcxGridDBColumn
          Caption = 'Read?'
          DataBinding.FieldName = 'has_acked'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          GroupIndex = 0
          MinWidth = 16
          Options.Filtering = False
          Width = 63
        end
        object MessageRecipViewemp_id1: TcxGridDBColumn
          DataBinding.FieldName = 'emp_id'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 61
        end
        object MessageRecipViewrecipient_name1: TcxGridDBColumn
          Caption = 'Recipient'
          DataBinding.FieldName = 'recipient_name'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          MinWidth = 120
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 150
        end
        object MessageRecipViewack_date1: TcxGridDBColumn
          Caption = 'Ack Date'
          DataBinding.FieldName = 'ack_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Options.Filtering = False
          Width = 90
        end
        object MessageRecipViewread_date1: TcxGridDBColumn
          DataBinding.FieldName = 'read_date'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.DateButtons = [btnClear, btnToday]
          Properties.DateOnError = deToday
          Properties.InputKind = ikRegExpr
          Visible = False
          Options.Filtering = False
          Width = 82
        end
      end
      object MessageRecipLevel: TcxGridLevel
        GridView = MessageRecipView
      end
    end
  end
  object pnlMsg: TPanel
    Left = 245
    Top = 137
    Width = 444
    Height = 354
    Align = alClient
    TabOrder = 3
    object ScrollBoxMsgBody: TScrollBox
      Left = 1
      Top = 1
      Width = 442
      Height = 352
      VertScrollBar.Style = ssFlat
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelKind = bkSoft
      BorderStyle = bsNone
      TabOrder = 0
      object WebBrowserMsgBody: TWebBrowser
        Left = 0
        Top = 0
        Width = 440
        Height = 350
        Align = alClient
        TabOrder = 0
        OnBeforeNavigate2 = WebBrowserMsgBodyBeforeNavigate2
        ControlData = {
          4C0000007A2D00002C2400000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  object MessageRecipTable: TDBISAMTable
    BeforeOpen = MessageRecipTableBeforeOpen
    OnCalcFields = MessageRecipTableCalcFields
    DatabaseName = 'DB1'
    SessionName = 'Default'
    EngineVersion = '4.44 Build 3'
    IndexName = 'message_id'
    MasterFields = 'message_id'
    MasterSource = MessageSource
    TableName = 'message_recip'
    Left = 216
    Top = 192
  end
  object MessageRecipSource: TDataSource
    DataSet = MessageRecipTable
    Left = 248
    Top = 192
  end
  object MessageTable: TDBISAMTable
    DatabaseName = 'DB1'
    SessionName = 'Default'
    EngineVersion = '4.44 Build 3'
    TableName = 'message'
    Left = 216
    Top = 159
  end
  object MessageSource: TDataSource
    DataSet = MessageTable
    Left = 248
    Top = 159
  end
  object MessageAckQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select count(*) MsgCount from Message_recip'
      'WHERE message_id = :Message_ID and'
      'ack_date <> NULL')
    Params = <
      item
        DataType = ftUnknown
        Name = 'Message_ID'
      end>
    Left = 232
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Message_ID'
      end>
  end
end
