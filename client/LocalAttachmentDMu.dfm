inherited LocalAttachment: TLocalAttachment
  OldCreateOrder = True
  Height = 250
  Width = 233
  object Attachment: TDBISAMTable
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    TableName = 'attachment'
    Left = 139
    Top = 13
  end
  object PendingUploads: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from attachment'
      'where upload_date is null'
      '  and need_to_upload'
      '  and attachment_id > 0'
      '  and active'
      '  and attached_by = :emp_id'
      '  and attach_date > :ignore_before_date'
      '  and (upload_try_date < :upload_start_date'
      '  or upload_try_date is null)'
      ' order by upload_try_date, attach_date')
    Params = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end
      item
        DataType = ftDateTime
        Name = 'ignore_before_date'
      end
      item
        DataType = ftDateTime
        Name = 'upload_start_date'
      end>
    Left = 140
    Top = 60
    ParamData = <
      item
        DataType = ftInteger
        Name = 'emp_id'
      end
      item
        DataType = ftDateTime
        Name = 'ignore_before_date'
      end
      item
        DataType = ftDateTime
        Name = 'upload_start_date'
      end>
  end
  object TempQuery: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    Params = <>
    Left = 46
    Top = 114
  end
  object AttachmentDB: TDBISAMDatabase
    EngineVersion = '4.44 Build 3'
    KeepConnection = False
    SessionName = 'Default'
    Left = 48
    Top = 16
  end
  object AttachmentSession: TDBISAMSession
    EngineVersion = '4.44 Build 3'
    KeepConnections = False
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 48
    Top = 64
  end
  object UpdateAttachment: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    RequestLive = True
    SQL.Strings = (
      '-- set at runtime')
    Params = <>
    Left = 145
    Top = 116
  end
  object AWSCredentialsQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from api_storage_credentials'
      'where (api_storage_constants_id = 1)')
    Params = <>
    Left = 46
    Top = 186
  end
  object AWSConstantsQry: TDBISAMQuery
    DatabaseName = 'DB1'
    EngineVersion = '4.44 Build 3'
    SQL.Strings = (
      'select * from api_storage_constants'
      'where (id = 1)')
    Params = <>
    Left = 142
    Top = 186
  end
end
