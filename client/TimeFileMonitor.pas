unit TimeFileMonitor;

interface

uses
  SysUtils, Classes, ExtCtrls, FileDetector;
{$WARN SYMBOL_PLATFORM OFF}

type
  TTimeFileMonitorDM = class(TDataModule)
    TimeImportTimer: TTimer;
    procedure FileDetectorOnFileCreate(Sender: TObject; Path: String);
    procedure DataModuleCreate(Sender: TObject);
  private
    FileDetector : TFileDetector;
  public
    TimeFilesWereImported: Boolean;
    NavigatingToTimesheetForImport: Boolean;
    function AnyPendingTimeImportFiles: Boolean;
    function AnyImportableFiles: Boolean;
    function ValidateFileForImportDate(Filename: String): Boolean;
  end;

var
  TimeFileMonitorDM: TTimeFileMonitorDM;

implementation

uses
  TimeClockImport, JCLFileUtils, OdMiscUtils, DMu, StrUtils, DateUtils, QMConst,
  LocalPermissionsDMu;

{$R *.dfm}

{ TTimeFileMonitor }

procedure TTimeFileMonitorDM.FileDetectorOnFileCreate(Sender: TObject; Path: String);
begin
  //make certain to only catch the event for the import dir, not subdirs
  if SameText(ExtractFilePath(Path), IncludeTrailingPathDelimiter(GetTimeImportFolderName)) = True then
    TimeImportTimer.Interval := 1000; //1 second
end;

procedure TTimeFileMonitorDM.DataModuleCreate(Sender: TObject);
begin
  FileDetector := TFileDetector.create(self);
  FileDetector.OnFileCreate := FileDetectorOnFileCreate;
  FileDetector.WatchSubTree := False;
  FileDetector.Path := ExcludeTrailingPathDelimiter(GetTimeImportFolderName);
  FileDetector.Active := True;
end;

function TTimeFileMonitorDM.AnyPendingTimeImportFiles: Boolean;
var
  TimeImportFolder: string;
  FilePattern: string;
  FS: TSearchRec;
begin
  Result := False;
  TimeImportFolder := GetTimeImportFolderName;
  if not DirectoryExists(TimeImportFolder) then
    Exit;

  FilePattern := TimeImportFolder + '*.xml';
  try
    Result := (FindFirst(FilePattern, faAnyFile, FS) = 0);
  finally
    FindClose(FS);
  end;
end;

function TTimeFileMonitorDM.AnyImportableFiles: Boolean;
//Returns true if any file in the import folder can get exclusive access (i.e. is not being written to)
var
  TimeImportFolder: string;
  FilePattern: string;
  FileList: TStringList;
  FileName: string;
  i: Integer;
begin
  Result := False;
  TimeImportFolder := GetTimeImportFolderName;
  if not DirectoryExists(TimeImportFolder) then
    Exit;

  FileList := TStringList.Create;
  try
    FilePattern := TimeImportFolder + '*.xml';
    AdvBuildFileList(FilePattern, faArchive, FileList, amSubSetOf);
      for i := 0 to FileList.Count-1 do begin
        FileName := FileList[i];
        if ValidateFileForImportDate(FileName) then begin
          if not IsFileInUse(TimeImportFolder + FileName) then begin
            Result := True;
            Exit;
          end;
        end;
      end;
  finally
    FreeAndNil(FileList);
  end;
end;

{QMANTWO-624 EB - TimeImportFolder}
function TTimeFileMonitorDM.ValidateFileForImportDate(Filename: String): Boolean;
var
  TimeImportFolder: string;
  FailedImportFolder: string;
  BadFileMsg: string;
  CanImportYesterday: Boolean;
begin
  Result := False;
  TimeImportFolder := GetTimeImportFolderName;
  FailedImportFolder := IncludeTrailingPathDelimiter(TimeImportFolder + 'Failed');
  CanImportYesterday := PermissionsDM.CanI(RightTimesheetsPriorDayEntry);

  if AnsiContainsText(FileName, FormatDateTime('yyyy-mm-dd_', Today)) or
    (CanImportYesterday and AnsiContainsText(FileName, FormatDateTime('yyyy-mm-dd_', Today-1)) ) then
    Result := True
  else begin
    BadFileMsg := 'Time in ' + FileName + ' was not imported. Only time entries for today' +
      IfThen(CanImportYesterday, ' or yesterday', '') + ' are allowed.';
    if FileMove(TimeImportFolder + FileName, FailedImportFolder + FileName, True) then
      BadFileMsg := BadFileMsg + ' The file was moved to the failed folder.'
    else
      BadFileMsg := BadFileMsg + ' The file was not moved to the failed folder.';

    DM.AddToQMLog(BadFileMsg);
    DM.ShowTransientMessage(BadFileMsg);
  end;
end;


end.
