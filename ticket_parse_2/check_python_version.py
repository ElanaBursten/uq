# check_python_version.py

import string
import sys
#
import errorhandling2

class PythonVersionError(Exception): pass

MIN_VERSION = (2, 7, 0)

def fmt(tuple):
    return string.join([str(x) for x in tuple], ".")

# XXX show clearer message
def check_python_version(log=None):
    if sys.version_info[:3] < MIN_VERSION:
        try:
            raise PythonVersionError(
             "Python version %s expected; got %s instead" % (
             fmt(MIN_VERSION), fmt(sys.version_info[:3])))
        except:
            if log:
                ep = errorhandling2.errorpacket()
                log.log(ep, send=1, write=1, dump=1)
            raise

#
# can be used as command line tool for quick checking

if __name__ == "__main__":

    check_python_version()

