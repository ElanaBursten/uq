"""
polygon.py

Determine if a point falls inside a polygon.
A polygon is informally defined as a list of coordinates (x, y) or (long, lat).

See: http://www.faqs.org/faqs/graphics/algorithms-faq/, section 2.03
"""

"""
    Original C code (from the FAQ mentioned above):

    int pnpoly(int npol, float *xp, float *yp, float x, float y)
    {
      int i, j, c = 0;
      for (i = 0, j = npol-1; i < npol; j = i++) {
        if ((((yp[i]<=y) && (y<yp[j])) ||
             ((yp[j]<=y) && (y<yp[i]))) &&
            (x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i]))

          c = !c;
      }
      return c;
    }
"""

def inside((x, y), points):
    """ Return true if point (x, y) falls inside of the polygon represented
        by <points>, which is a list of 2-tuples (px, py) as well. """

    # ensure that the last point is the same as the first
    points = points[:]
    if points[0] != points[-1]:
        points.append(points[0])

    # convert to floats, so / does not do integer division
    x = float(x)
    y = float(y)

    c = 0
    for i in range(len(points)):
        if i == 0:
            j = len(points) - 1
        else:
            j = i - 1

        #print "Comparing:", "i =", i, "j =", j, "points:", points[i], points[j]

        a1 = ((points[i][1] <= y and y < points[j][1]) \
          or (points[j][1] <= y and y < points[i][1]))
        q = a1 and (x < (points[j][0] - points[i][0]) * (y - points[i][1])
                 / (points[j][1] - points[i][1]) + points[i][0])

        if q:
            c = not c

    return c

