# imagedump.py
# Quick script to dump the image of a ticket (or multiple tickets) to file.
# Created: 2002.06.24 HN

import getopt
import sys
import traceback
#
import config
import dbinterface_ado

__usage__ = """\
imagedump.py [options] ticket_id1 [ticket_id2 ... ticket_idN] filename

Options:
    -s server
    -d database
    -l login
    -p password
    -v               Use ticket_version table instead (with ticket_version_id)
"""

SEPARATOR = chr(12) # Ctrl-L

def imagedump(idlist, filename, server=None, database=None, login=None,
 password=None, use_ticket_version=False):
    cfg = config.getConfiguration() # default configuration
    if server:
        cfg.ado_database["host"] = server
    if database:
        cfg.ado_database["database"] = database
    if login:
        cfg.ado_database["login"] = login
    if password:
        cfg.ado_database["password"] = password
    print server, database, login, password
    print cfg.ado_database

    dbado = dbinterface_ado.DBInterfaceADO()

    tickets = []

    if use_ticket_version:
        sqltemp = """
         select ticket_version_id, image
         from ticket_version
         where ticket_version_id = %s
        """
    else:
        sqltemp = """
         select ticket_id, image
         from ticket where ticket_id = '%s'
        """

    for id in idlist:
        try:
            sql = sqltemp % (id,)
            rows = dbado.runsql_result(sql)
            if rows:
                row = rows[0]
            else:
                raise ValueError, "Ticket id %s not found" % (id,)
            print "Ticket", id, "retrieved"
        except:
            print "Error when retrieving ticket", id
            traceback.print_exc()
            continue
        tickets.append(row)

    print "Writing file", filename, "...",
    f = open(filename, "w")
    for row in tickets:
        #f.write(ticket.image)
        f.write(row["image"])
        f.write(SEPARATOR)
    f.close()
    print "OK"


if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "s:d:l:p:v?", ["data="])

    server = database = login = password = None
    use_ticket_version = False

    for o, a in opts:
        if o == "-s":
            server = a
        elif o == "-d":
            database = a
        elif o == "-l":
            login = a
        elif o == "-p":
            password = a
        elif o == '-v':
            use_ticket_version = True
        elif o == "-?":
            print >> sys.stderr, __usage__
            raise SystemExit
        elif o == "--data":
            print "Data dir:", a

    idlist, filename = args[:-1], args[-1]

    imagedump(idlist, filename, server=server, database=database, login=login,
              password=password, use_ticket_version=use_ticket_version)
