# work_order.py
# Placeholder for WorkOrder class.

import sqlmap
import static_tables

class WorkOrder(sqlmap.SQLMap):

    __table__ = "work_order"
    __key__ = "wo_id"
    __fields__ = static_tables.work_order

    def __init__(self):
        sqlmap.SQLMap.__init__(self)

        # some default values
        self.status = "-P"
        self.closed = 0

    @classmethod
    def find(cls, tdb, wo_center, wo_number):
        """ Find an existing work order record from the given center with the
            given WO number. Number may or may not have 'LAM' prefix.
        """
        sql = """
         select top 1 * from work_order wo
         where wo.wo_source = '%s'
         and (wo.wo_number = '%s' or wo.client_wo_number = '%s'
           or wo.wo_number = 'LAM%s' or wo.client_wo_number = 'LAM%s')
         and wo.active = 1
         order by transmit_date desc
        """ % (wo_center, wo_number, wo_number, wo_number, wo_number)
        rows = tdb.runsql_result(sql)
        if rows:
            # create WorkOrder object and return it
            return cls.load_from_row(rows[0])
        else:
            raise ValueError("Work order not found: %s/%s" % (
                  wo_center, wo_number))

