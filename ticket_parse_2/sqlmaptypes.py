# sqlmaptypes.py
# Types for SQLMap __field__ definitions.

import adoconstants

class SQLMapType(object):
    def __init__(self, tag, **kwargs):
        self.tag = tag
        self.__dict__.update(kwargs)
    def __repr__(self):
        s = self.__class__.__name__
        if not s.endswith(")"): s += "()"
        return s
    def make_ado_parameter(self):
        raise NotImplementedError

class SQLInt(SQLMapType):
    def __init__(self):
        self.tag = 'int'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adInteger,
            adoconstants.adParamInput)
        return p
sqlint = SQLInt()

class SQLTinyInt(SQLMapType):
    def __init__(self):
        self.tag = 'tinyint'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adTinyInt,
            adoconstants.adParamInput)
        return p
sqltinyint = SQLTinyInt()

class SQLDateTime(SQLMapType):
    def __init__(self):
        self.tag = 'datetime'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adDBTimeStamp,
            adoconstants.adParamInput)
        return p
sqldatetime = SQLDateTime()

class SQLBool(SQLMapType):
    def __init__(self):
        self.tag = 'bool'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adBoolean,
            adoconstants.adParamInput)
        return p
sqlbool = SQLBool()

class SQLCurrency(SQLMapType):
    def __init__(self):
        self.tag = 'currency'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adCurrency,
            adoconstants.adParamInput)
        return p
sqlcurrency = SQLCurrency()

class SQLChar(SQLMapType):
    def __init__(self):
        self.tag = 'char'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adchar,
            adoconstants.adParamInput)
        return p
sqlchar = SQLChar()

class SQLBinary(SQLMapType):
    def __init__(self):
        self.tag = 'binary'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adBinary,
            adoconstants.adParamInput) # needs width?
        return p
sqlbinary = SQLBinary()

class SQLText(SQLMapType):
    def __init__(self):
        self.tag = 'text'
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adChar,
            adoconstants.adParamInput, -1)
            # -1 seems to work since SQL Server type 'text' doesn't really
            # have a fixed length
        return p
sqltext = SQLText()

class SQLString(SQLMapType):
    def __init__(self, width):
        self.tag = 'string'
        self.width = width
    def __repr__(self):
        return "SQLString(%s)" % (self.width,)
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adVarChar,
            adoconstants.adParamInput, self.width)
        return p
def sqlstring(width):
    return SQLString(width)

class SQLDecimal(SQLMapType):
    def __init__(self, precision, scale):
        self.tag = 'decimal'
        self.precision = precision
        self.scale = scale
    def __repr__(self):
        return "SQLDecimal(%s,%s)" % (self.precision, self.scale)
    def make_ado_parameter(self, value, adocommand):
        p = adocommand.CreateParameter(value, adoconstants.adDecimal,
            adoconstants.adParamInput)#, self.precision, self.scale)
        # this has to be set separately... another ADO quirk... (sigh)
        p.Precision = self.precision
        p.NumericScale = self.scale
        return p
def sqldecimal(precision, scale):
    return SQLDecimal(precision, scale)


if __name__ == "__main__":
    print sqlint, sqlstring(40), sqldatetime, sqlbool
    print sqldecimal(9,6)

