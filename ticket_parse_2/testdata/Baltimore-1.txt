
NOTICE OF INTENT TO EXCAVATE      SHORT NOTICE
Ticket No: 00356468
Transmit      Date: 05/14/02      Time: 09:01    Op: janice
Original Call Date: 05/14/02      Time: 08:55    Op: janice
Work to Begin Date: 05/15/02      Time: 09AM

Place: ANNAPOLIS
Address: 115         Street: WEST ST
Nearest Intersecting Street: LAYFAYETTE AV

Type of Work: INSTALL STORM DRAINS
Extent of Work:   LOC 200FT IN BOTH DIRECTIONS FROM THE INTER & INCLUDING
: THE ENTIRE PROP OF 115 WEST ST
Remarks: CREW IS ON SITE   SHORT NOTICE
:                                                        Fax: (410)224-2177

Company     : JONES OF ANNAPOLIS INC
Contact Name: WENDY SPENCER         Contact Phone: (410)224-2095
Alt. Contact: WANDA OR DAVID        Alt. Phone   :
Work Being Done For: BROWN CONTRACTING
State: MD              County: AA
Map: AA    Page: 020   Grid Cells: J10
Explosives: N
CAPW01     JTV01      TAN01
Send To: BGE   09  Seq No: 0121   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356448
Transmit      Date: 05/14/02      Time: 09:01    Op: geneva
Original Call Date: 05/14/02      Time: 08:53    Op: geneva
Work to Begin Date: 05/16/02      Time: 09AM

Place: ELLICOTT CITY
Address:             Street: ILCHESTER RD  MONTGOMERY RD
Nearest Intersecting Street: ILCHESTTER RD MONTGOMERY RD

Type of Work: INSTALLING STATE CONDUIT
Extent of Work:   LOCATE ENTIRE ABOVE INTER 400FT IN EVERY DIRECTION
: INCLUDING RDWY.
Remarks: MARK WITH FLAGS & PAINT.
:                                                        Fax: (410)590-9043

Company     : J P ENTERPRISES
Contact Name: BRITTANY MORRIS       Contact Phone: (410)590-9040
Alt. Contact: JASON                 Alt. Phone   : (443)324-4065
Work Being Done For: STATE OF MD
State: MD              County: HWD
Map: HWD   Page: 016   Grid Cells: H04
Explosives: N
ABS01      HCU01      HTV01      TBW04
Send To: BGE   05  Seq No: 0094   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356458
Transmit      Date: 05/14/02      Time: 09:01    Op: janel
Original Call Date: 05/14/02      Time: 08:54    Op: janel
Work to Begin Date: 05/16/02      Time: 09AM

Place: BURTONSVILLE
Address:             Street: WEXHALL TERR
Nearest Intersecting Street: AIRDIRE CT

Type of Work: INST A FENCE
Extent of Work:   LOC ENTIRE PROPS OF ADDRESSES 14503 THRU 14509 WEXHALL
: TERR, THIS IS ONE SIDE OF BLDG #5
Remarks:
:                                                        Fax: (301)874-5706

Company     : LONG FENCE
Contact Name: MARIE BAGENT          Contact Phone: (301)428-9040
Alt. Contact: MARY SHAFFER          Alt. Phone   :
Work Being Done For: VANGUARD MANAGEMENT
State: MD              County: MONT
Map: MONT  Page: 032   Grid Cells: E04
Explosives: N
ATM01      TMT02      TRU02      WGL06      WSS01
Send To: BGE   05  Seq No: 0095   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356487
Transmit      Date: 05/14/02      Time: 09:01    Op: john
Original Call Date: 05/14/02      Time: 08:57    Op: john
Work to Begin Date: 05/16/02      Time: 09AM

Place: BEL AIR
Address: 204         Street: PLUMB TREE RD
Nearest Intersecting Street: RT 24

Type of Work: INST TEMPORARY SIGN
Extent of Work:   LOC 30FT IN FROM CORNER OF INTER
Remarks:
:                                                        Fax: (703)368-7059

Company     : METRO SIGN & DESIGN
Contact Name: PHIL GARDUNO          Contact Phone: (703)631-1866
Alt. Contact: MICHELLE              Alt. Phone   : (703)368-1986
Work Being Done For: LONG AND FOSTER
State: MD              County: HARFORD
Map: HARF  Page: 023   Grid Cells: H02
Explosives: N
HCPW01     MLV01
Send To: BGE   63  Seq No: 0032   Map Ref:
         TBE   05          0031            LOC#37



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356488               Update Of: 00318715
Transmit      Date: 05/14/02      Time: 09:01    Op: patty.
Original Call Date: 05/14/02      Time: 08:57    Op: patty.
Work to Begin Date: 05/16/02      Time: 09AM

Place: CLARKSVILLE
Address:             Street: WAKE FOREST RD
Nearest Intersecting Street: RT 108

Type of Work: INST SWR SVC
Extent of Work:   LOC WAKE FOREST RD 300FT E OF RT 108 R.O.W. TO R.O.W.
: CALL PAT FINNINAGAN AT 443-324-0202 FOR FURTHER LOC INSTRUCTIONS
Remarks: BEST INFORMATION CALLER COULD PROVIDE ON UPDATE
:                                                        Fax: (410)893-2695

Company     : T C SIMON
Contact Name: DAVID BAILEY          Contact Phone: (410)879-3055
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: HWD COUNTY
State: MD              County: HWD
Map: HWD   Page: 014   Grid Cells: E08
Explosives: N
HCU01      HTV01      HTV02      TBW04
Send To: BGE   05  Seq No: 0096   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356491
Transmit      Date: 05/14/02      Time: 09:01    Op: candac
Original Call Date: 05/14/02      Time: 08:57    Op: candac
Work to Begin Date: 05/16/02      Time: 09AM

Place: COLUMBIA
Address: 6392        Street: MORNINGTIME LA
Nearest Intersecting Street: WARM SUNSHINE PATH

Type of Work: INST PATIO
Extent of Work:   LOC ENTIRE REAR OF PROP
Remarks:

Company     : BROOKEVILLE DESIGN BUILD
Contact Name: MAUREEN LAMPE         Contact Phone: (301)924-2603
Alt. Contact: WAYNE                 Alt. Phone   :
Work Being Done For: BELAMY
State: MD              County: HWD
Map: HWD   Page: 014   Grid Cells: H10
Explosives: N
HCU01      HTV01      TBW04
Send To: BGE   05  Seq No: 0097   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356493               Update Of: 00318220
Transmit      Date: 05/14/02      Time: 09:01    Op: geneva
Original Call Date: 05/14/02      Time: 08:58    Op: geneva
Work to Begin Date: 05/16/02      Time: 09AM

Place: SAVAGE
Address: 8800        Street: WASHINGTON BLVD
Nearest Intersecting Street: GUILFORD RD

Type of Work: INST FIBER OPTICS
Extent of Work:   LOC IN FRONT OF BURGER KING FROM P#309542 AT ABOVE
: ADDRESS 110FT S TO EXISTING CONDUIT
Remarks:
:                                                        Fax: (410)590-9043

Company     : J P ENTERPRISES
Contact Name: BRITNEY MORRIS        Contact Phone: (410)590-9040
Alt. Contact: JASON                 Alt. Phone   : (443)324-4065
Work Being Done For: COMCAST
State: MD              County: HWD
Map: HWD   Page: 020   Grid Cells: E06
Explosives: N
ACSI01     HCU01      HTV01      HTV02      MCI01      TAN03
Send To: BGE   05  Seq No: 0098   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356496               Update Of: 00318722
Transmit      Date: 05/14/02      Time: 09:01    Op: patty.
Original Call Date: 05/14/02      Time: 08:58    Op: patty.
Work to Begin Date: 05/16/02      Time: 09AM

Place: SCAGGSVILLE
Address:             Street: GRANT AV
Nearest Intersecting Street: SCAGGSVILLE RD

Type of Work: INSTALLING SILT FENCE, WTR & SWR MAINS
Extent of Work:   LOC FROM DEAD END OF GRANT AV 200 FT OUT
Remarks: CALL MARK SLADE FOR FURTHER INSTRUCTIONS AT 443-506-2522
:                                                        Fax: (410)893-2695

Company     : T C SIMON
Contact Name: DAVID BAILEY          Contact Phone: (410)879-3055
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: HWD CTY
State: MD              County: HWD
Map: HWD   Page: 019   Grid Cells: H10
Explosives: N
HCU01      HTV01      TAN03
Send To: BGE   05  Seq No: 0099   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356499
Transmit      Date: 05/14/02      Time: 09:01    Op: janel
Original Call Date: 05/14/02      Time: 08:58    Op: janel
Work to Begin Date: 05/16/02      Time: 09AM

Place: BURTONSVILLE
Address:             Street: AIRDIRE CT
Nearest Intersecting Street: WEXHALL TERR

Type of Work: INST A FENCE
Extent of Work:   LOC ENTIRE PROPS OF ADDRESSES 3717 THRU 3723 AIRDIRE CT
: THIS IS THE OTHER SIDE OF BLDG #5
Remarks: THIS TKT CORRESPONDS WITH TKT 356458
:                                                        Fax: (301)874-5706

Company     : LONG FENCE
Contact Name: MARIE BAGENT          Contact Phone: (301)428-9040
Alt. Contact: MARY SHAFFER          Alt. Phone   :
Work Being Done For: VANGUARD MANAGEMENT
State: MD              County: MONT
Map: MONT  Page: 032   Grid Cells: E04
Explosives: N
ATM01      TMT02      TRU02      WGL06      WSS01
Send To: BGE   05  Seq No: 0100   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356500               Update Of: 00318216
Transmit      Date: 05/14/02      Time: 09:01    Op: geneva
Original Call Date: 05/14/02      Time: 08:59    Op: geneva
Work to Begin Date: 05/16/02      Time: 09AM

Place: SAVAGE
Address: 10350       Street: GUILFORD RD
Nearest Intersecting Street: STAYTON DR

Type of Work: INST FIBER OPTICS
Extent of Work:   LOC THE W SIDE OF GUILFORD RD FROM 10350 TO
: OPPOSITE 10401
Remarks:
:                                                        Fax: (410)590-9043

Company     : J P ENTERPRISES
Contact Name: BRITNEY MORRIS        Contact Phone: (410)590-9040
Alt. Contact: JASON                 Alt. Phone   : (443)324-4065
Work Being Done For: COMCAST
State: MD              County: HWD
Map: HWD   Page: 020   Grid Cells: E07,E06
Explosives: N
ACSI01     HCU01      HTV01      HTV02      MCI01      TAN03
Send To: BGE   05  Seq No: 0101   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356501               Update Of: 00318728
Transmit      Date: 05/14/02      Time: 09:01    Op: patty.
Original Call Date: 05/14/02      Time: 08:59    Op: patty.
Work to Begin Date: 05/16/02      Time: 09AM

Place: SCAGGSVILLE
Address:             Street: GROSS AV
Nearest Intersecting Street: SCAGGSVILLE RD

Type of Work: INSTALLING SILT FENCE, WTR & SWR MAINS
Extent of Work:   LOC FROM DEAD END OF GROSS AV 200 FT OUT R.O.W. TO R.O.W.
Remarks: CALL MARK SLADE FOR FURTHER INSTRUCIONS AT 443-506-2522
:                                                        Fax: (410)893-2695

Company     : T C SIMON
Contact Name: DAVID  BAILEY         Contact Phone: (410)879-3055
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: HWD CTY
State: MD              County: HWD
Map: HWD   Page: 019   Grid Cells: H10
Explosives: N
HCU01      HTV01      TAN03
Send To: BGE   05  Seq No: 0102   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356503               Update Of: 00318732
Transmit      Date: 05/14/02      Time: 09:01    Op: patty.
Original Call Date: 05/14/02      Time: 08:59    Op: patty.
Work to Begin Date: 05/16/02      Time: 09AM

Place: ARNOLD
Address: 1336        Street: JONES STATION RD
Nearest Intersecting Street: WOODARD RD

Type of Work: DIGGING FOR PLANT MAINTENANCE
Extent of Work:   LOC FRONT OF PROP, MARKING FROM R.O.W. TO R.O.W.,  FROM
: 1ST ENTRANCE TO END OF FENCE, //CALL JOE KELLY FOR BETTER LOC
: INSTRUCTIONS AT 410 365 7099  /
Remarks: BEST INFORMATION CALLER COULD PROVIDE
:                                                        Fax: (410)893-2695

Company     : T C SIMON
Contact Name: DAVID  BAILEY         Contact Phone: (410)879-3055
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: AA COUNTY
State: MD              County: AA
Map: AA    Page: 015   Grid Cells: J12,K12
Explosives: N
AAU01      ACT01      JTV01      TAN01
Send To: BGE   09  Seq No: 0122   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356508               Update Of: 00318212
Transmit      Date: 05/14/02      Time: 09:01    Op: geneva
Original Call Date: 05/14/02      Time: 08:59    Op: geneva
Work to Begin Date: 05/16/02      Time: 09AM

Place: SAVAGE
Address: 10401       Street: GUILFORD RD
Nearest Intersecting Street: STAYTON DR

Type of Work: INST FIBER OPTICS
Extent of Work:   LOC THE W SIDE OF GUILFORD RD FROM OPPOSITE 10401 TO
: OPPOSITE 10421
Remarks:
:                                                        Fax: (410)590-9043

Company     : J P ENTERPRISES
Contact Name: BRITNEY MORRIS        Contact Phone: (410)590-9040
Alt. Contact: JASON                 Alt. Phone   : (443)324-4065
Work Being Done For: COMCAST
State: MD              County: HWD
Map: HWD   Page: 020   Grid Cells: E07,E06
Explosives: N
ACSI01     HCU01      HTV01      HTV02      MCI01      TAN03
Send To: BGE   05  Seq No: 0103   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356510               Update Of: 00318741
Transmit      Date: 05/14/02      Time: 09:02    Op: patty.
Original Call Date: 05/14/02      Time: 08:59    Op: patty.
Work to Begin Date: 05/16/02      Time: 09AM

Place: ROCK SPRING
Address:             Street: WILLRICH CIR
Nearest Intersecting Street: MARDIC DR

Type of Work: REPAIRING WTR MAIN VALVE N/E
Extent of Work:   APPROX 300FT E OF ABOVE INTER, LOC WILLRICH CIR, FROM
: BEGINNING OF GUARD RAIL TO END OF GUARD RAIL, R.O.W. TO R.O.W.
: //CALL PAUL WORTHINGTON AT 410 365 1611 FOR BETTER LOCATE INSTRUCTIONS//
Remarks: BEST INFORMATION CALLER COULD PROVIDE ON UPDATE
:                                                        Fax: (410)893-2695

Company     : T C SIMON
Contact Name: DAVID BAILEY          Contact Phone: (410)879-3055
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: T C SIMON
State: MD              County: HARFORD
Map: HARF  Page: 017   Grid Cells: B02,C02
Explosives: N
HCPW01     MLV01
Send To: BGE   63  Seq No: 0033   Map Ref:
         TBE   05          0032            LOC#37



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356520               Update Of: 00318200
Transmit      Date: 05/14/02      Time: 09:02    Op: geneva
Original Call Date: 05/14/02      Time: 09:00    Op: geneva
Work to Begin Date: 05/16/02      Time: 09AM

Place: SAVAGE
Address: 10421       Street: GUILFORD RD
Nearest Intersecting Street: STAYTON DR

Type of Work: INST FIBER OPTICS
Extent of Work:   LOC THE W SIDE OF GUILFORD RD FROM OPPOSITE 10421 TO
: OPPOSITE 10545
Remarks:
:                                                        Fax: (410)590-9043

Company     : J P ENTERPRISES
Contact Name: BRITNEY MORRIS        Contact Phone: (410)590-9040
Alt. Contact: JASON                 Alt. Phone   : (443)324-4065
Work Being Done For: COMCAST
State: MD              County: HWD
Map: HWD   Page: 020   Grid Cells: E07,E06
Explosives: N
ACSI01     HCU01      HTV01      HTV02      MCI01      TAN03
Send To: BGE   05  Seq No: 0104   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356524
Transmit      Date: 05/14/02      Time: 09:02    Op: janel
Original Call Date: 05/14/02      Time: 09:01    Op: janel
Work to Begin Date: 05/16/02      Time: 09AM

Place: BURTONSVILLE
Address:             Street: AIRDIRE CT
Nearest Intersecting Street: WEXHALL TERR

Type of Work: INST A FENCE
Extent of Work:   LOC ENTIRE PROPS OF ADDRESSES 3735 THRU 3741 AIRDIRE CT
: THIS IS ONE SIDE OF BLDG #6
Remarks:
:                                                        Fax: (301)874-5706

Company     : LONG FENCE
Contact Name: MARIE BAGENT          Contact Phone: (301)428-9040
Alt. Contact: MARY SHAFFER          Alt. Phone   :
Work Being Done For: VANGUARD MANAGEMENT
State: MD              County: MONT
Map: MONT  Page: 032   Grid Cells: E04
Explosives: N
ATM01      TMT02      TRU02      WGL06      WSS01
Send To: BGE   05  Seq No: 0105   Map Ref:



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356502
Transmit      Date: 05/14/02      Time: 09:03    Op: sylvia
Original Call Date: 05/14/02      Time: 08:59    Op: sylvia
Work to Begin Date: 05/16/02      Time: 09AM

Place: REISTERSTOWN
Address: 4209        Street: OSBORN RD
Nearest Intersecting Street: DOVER RD

Type of Work: INSTALLING ELEC SVC & MAINS
Extent of Work:   LOC FROM P#179364 E TO AND INCL THE ENTIRE PROP OF APPROX
: 400 FT.  POLE IS LOC ON RIGHT SIDE OF HOUSE AS FACING.
Remarks: WMS#701751
:                                                        Fax: (410)638-1840

Company     : R NORTON COMPANY
Contact Name: LAURA BLEVINS         Contact Phone: (410)879-7920
Alt. Contact: MATT ADAMS            Alt. Phone   : (410)218-2009
Work Being Done For: BGE
State: MD              County: BALT
Map: BALT  Page: 010   Grid Cells: E09,F09
Explosives: N
CPM01
Send To: BGE   62  Seq No: 0089   Map Ref:
         TBW   08          0060            LOC42



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356517
Transmit      Date: 05/14/02      Time: 09:03    Op: barbar
Original Call Date: 05/14/02      Time: 09:00    Op: barbar
Work to Begin Date: 05/16/02      Time: 09AM

Place: WOODBINE
Address: 1919        Street: HIDDEN SPRINGS CT
Nearest Intersecting Street: FARMINGTON LANE

Type of Work: BURY PROPANE GAS LINE
Extent of Work:   LOCATE THE ENTIRE RIGHT SIDE OF PROP AS FACING IN THE
: FRONT
Remarks:
:                                                        Fax: (410)861-8435

Company     : SUBURBAN PROPANE
Contact Name: BRIAN GERBER          Contact Phone: (410)833-1400
Alt. Contact: TODD RILL             Alt. Phone   : (443)506-4457
Work Being Done For: CATHY MILLER
State: MD              County: CARROLL
Map: CARR  Page: 028   Grid Cells: G10
Explosives: N

Send To: BGE   61  Seq No: 0052   Map Ref:
         TBW   05          0051            LOC41



NOTICE OF INTENT TO EXCAVATE
Ticket No: 00356537               Update Of: 00316490
Transmit      Date: 05/14/02      Time: 09:03    Op: kevin
Original Call Date: 05/14/02      Time: 09:03    Op: kevin
Work to Begin Date: 05/16/02      Time: 09AM

Place: HAGERSTOWN
Address:             Street: EASTERN BLVD
Nearest Intersecting Street: SECURITY RD

Type of Work: RD WIDENING
Extent of Work:   LOC FROM THE ABOVE INTER MARK BOTH SIDES & MIDDLE OF
: EASTERN BLVD 500FT S & 200FT N PASS THE R/R TRACKS
Remarks:
:                                                        Fax: (301)733-0812

Company     : C WILLIAM HETZER
Contact Name: LARA BOLLINGER        Contact Phone: (301)733-7300
Alt. Contact: STEVE                 Alt. Phone   :
Work Being Done For: WASHINGTON COUNTY ENGINEERING
State: MD              County: WASHINGTON
Map: WASH  Page: 021   Grid Cells: H04
Explosives: N
CGM01      COHWSL01
Send To: PTE   01  Seq No: 0008   Map Ref:
         TFH   02          0076



au;}'G7#j~
