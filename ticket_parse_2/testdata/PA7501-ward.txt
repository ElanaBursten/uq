JOB 3285921  S.T.S., Inc.  12/01/2004  13:05:52
KAD    00006 POCS 12/01/04 13:04:41 3285921-002 RNOT XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3285921]-[002] Channel#--[130111 ][0145]

Message Type--[RENOTIFY][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[40]
Work Site--[THEODORE ST]
     Nearest Intersection--[S 69TH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
     Location Information--
     [WORKING NEAR  6918 THEODORE ST.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.916652/-75.236533,39.917490/-75.235336,39.917230/-75.234920
      39.916432/-75.236221]

Type of Work--[INSTL MONITORING WELLS]                       Depth--[35FT]
Extent of Excavation--[6IN DIA]         Method of Excavation--[AUGER]
Street--[X] Sidewalk--[ ] Pub Prop--[X] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[MWH]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[MONTGOMERY WATSON HARZA]        Homeowner/Business--[B]
Address--[335 PHOENIXVILLE PIKE]
City--[MALVERN]                      State--[PA] Zip--[19355]

Caller--[MATT VAN HORN]                    Phone--[610-993-0800] Ext--[]
FAX--[610-993-0252]  Email--[MATTHEW.D.VANHORN@MWHGLOBAL.COM]
Person to Contact--[MATT VAN HORN]             Phone--[610-407-7924] Ext--[]
Best Time to Call--[0900-1700]

Prepared--[23-NOV-04] at [1203] by [LEICA HUMPHREYS]

Remarks--
     [CALLER WOULD LIKE TO MEET LOCATORS ON SITE IF POSSIBLE.  REQUESTING FIELD
      MARKING AND PRINTS.
      ******=== RENOTIFY 3285921-002 --12/1/2004 1301 DAM 11===******

      PER MATT VANHORN. ATTN PECO, PHILA CITY WTR, PHILA GAS WORKS AND VERIZON,
      PLEASE RESPOND TO MARK OR RESPOND CLEAR ASAP TO THE KARL SYSTEM.]

KAD0  KAD=PECO DESIGN      PD 0  PD =PHILA C WTR DPT  PZ 0  PZ =PGW PHLA
YA 0  YA =VERIZON PA INC-

Serial Number--[3285921]-[002]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========
JOB 3365805  S.T.S., Inc.  12/01/2004  11:16:45
KAD    00001 POCS 12/01/04 11:16:07 3365805-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3365805]-[000] Channel#--[110421 ][0663]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[52]
Work Site--[N 50TH ST]
     Nearest Intersection--[LEIDY AVE]
     Second Intersection--[W JEFFERSON ST]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [THE PROP IS BORDERED BY THE 3 ABOVE INTERSECTING STREETS, INCLUDING 52ND
      ST.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.977645/-75.226656,39.981438/-75.224102,39.977725/-75.213682
      39.973493/-75.212483,39.972934/-75.218058]

Type of Work--[SURVEY]                                       Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[X] Other--[]
Owner/Work Being Done for--[LANGAN ENGINEERING]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[LANGAN ENGINEERING]             Homeowner/Business--[B]
Address--[30 S 17TH ST SUITE 1500]
City--[PHILADELPHIA]                 State--[PA] Zip--[19103]

Caller--[SHAUN HIGGINS]                    Phone--[215-864-0640] Ext--[]
FAX--[215-864-0671]  Email--[shiggins@langan.com]
Person to Contact--[SHAUN HIGGINS]             Phone--[215-864-0640] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[01-DEC-04] at [1115] by [PATTI MCINTOSH]

Remarks--
     [CALLER IS REQUESTING PLANS]

ATM0  ATM=AT&T ATLANTA     CAV0  CAV=CITY SIGNAL COM  EI 0  EI =AT&T LOCAL SVCS
ID 0  ID =BUCKEYE PL DELA  KAD0  KAD=PECO DESIGN      LKC0  LKC=LEVEL 3 COMM
MI 0  MI =MCI              MMF0  MMF=ABOVENET COMM    PD 0  PD =PHILA C WTR DPT
PZ 0  PZ =PGW PHLA         RU 0  RU =URBAN CABLEWORK  SEP0  SEP=SEPTA
WCS0  WCS=WILTEL COMM LLC  YA 0  YA =VERIZON PA INC-

Serial Number--[3365805]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========
JOB 3365975  S.T.S., Inc.  12/01/2004  11:49:39
KAD    00002 POCS 12/01/04 11:48:58 3365975-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3365975]-[000] Channel#--[114324 ][0318]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[13]
Work Site--[W LOUDEN ST]
     Nearest Intersection--[WINDRIM AVE]
     Second Intersection--[BELFIELD AVE]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [BTWN THE ABOVE 2 INTER STREETS]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [40.026855/-75.151909,40.026775/-75.150761,40.026356/-75.150970
      40.026575/-75.152378]

Type of Work--[SWR RECONSTRUCTION WORK]                      Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[PHILA CITY WTR DEPT]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[PHILADELPHIA CITY WATER DEPT]   Homeowner/Business--[B]
Address--[1101 MARKET ST 2ND FL]
City--[PHILADELPHIA]                 State--[PA] Zip--[19107]

Caller--[TENA BOLTON]                      Phone--[215-685-6284] Ext--[]
FAX--[215-685-6312]  Email--[tena.bolton@phila.gov]
Person to Contact--[TENA BOLTON]               Phone--[215-685-6284] Ext--[]
Best Time to Call--[0730-1530]

Prepared--[01-DEC-04] at [1148] by [JANET COPE]

Remarks--
     [REQUESTING PRINTS    PROJECT NUMBER IS 40241.]

ATM0  ATM=AT&T ATLANTA     EI 0  EI =AT&T LOCAL SVCS  KAD0  KAD=PECO DESIGN
LKC0  LKC=LEVEL 3 COMM     MMF0  MMF=ABOVENET COMM    PD 0  PD =PHILA C WTR DPT
PZ 0  PZ =PGW PHLA         RU 0  RU =URBAN CABLEWORK  YB 0  YB =VERIZON PA INC

Serial Number--[3365975]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========

JOB 3365978  S.T.S., Inc.  12/01/2004  11:50:44
KAD    00003 POCS 12/01/04 11:49:55 3365978-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3365978]-[000] Channel#--[114824 ][0062]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[21]
Work Site--[LAWNTON ST]
     Nearest Intersection--[GORGAS LN]
     Second Intersection--[FOUNTAIN ST]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [BTWN THE ABOVE 2 INTER STREETS]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.041279/-75.221401,40.041239/-75.219497]

Type of Work--[SWR RECONSTRUCTION WORK]                      Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[PHILA CITY WTR DEPT]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[PHILADELPHIA CITY WATER DEPT]   Homeowner/Business--[B]
Address--[1101 MARKET ST 2ND FL]
City--[PHILADELPHIA]                 State--[PA] Zip--[19107]

Caller--[TENA BOLTON]                      Phone--[215-685-6284] Ext--[]
FAX--[215-685-6312]  Email--[tena.bolton@phila.gov]
Person to Contact--[TENA BOLTON]               Phone--[215-685-6284] Ext--[]
Best Time to Call--[0730-1530]

Prepared--[01-DEC-04] at [1149] by [JANET COPE]

Remarks--
     [REQUESTING PRINTS    PROJECT NUMBER IS 40241.]

KAD0  KAD=PECO DESIGN      PCL0  PCL=PHILA UNIV       PD 0  PD =PHILA C WTR DPT
PZ 0  PZ =PGW PHLA         RU 0  RU =URBAN CABLEWORK  YB 0  YB =VERIZON PA INC

Serial Number--[3365978]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========
JOB 3365988  S.T.S., Inc.  12/01/2004  11:54:02
KAD    00004 POCS 12/01/04 11:52:40 3365988-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3365988]-[000] Channel#--[114924 ][0122]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[12]
Work Site--[BELFIELD AVE]
     Nearest Intersection--[E PENN ST]
     Second Intersection--[E CHURCH LN]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [BTWN THE ABOVE 2 INTER STREETS & THE INTERSECTIONS.]
     Caller Lat/Lon--[]
     Mapped Type--[L] Mapped Lat/Lon--
     [40.037356/-75.168186,40.037156/-75.168369,40.037396/-75.167013
      40.037176/-75.165422,40.036857/-75.164275]

Type of Work--[SWR RECONSTRUCTION WORK]                      Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[PHILA CITY WTR DEPT]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[PHILADELPHIA CITY WATER DEPT]   Homeowner/Business--[B]
Address--[1101 MARKET ST 2ND FL]
City--[PHILADELPHIA]                 State--[PA] Zip--[19107]

Caller--[TENA BOLTON]                      Phone--[215-685-6284] Ext--[]
FAX--[215-685-6312]  Email--[tena.bolton@phila.gov]
Person to Contact--[TENA BOLTON]               Phone--[215-685-6284] Ext--[]
Best Time to Call--[0730-1530]

Prepared--[01-DEC-04] at [1151] by [JANET COPE]

Remarks--
     [REQUESTING PRINTS    PROJECT NUMBER IS 40241.]

EI 0  EI =AT&T LOCAL SVCS  KAD0  KAD=PECO DESIGN      PD 0  PD =PHILA C WTR DPT
PZ 0  PZ =PGW PHLA         RU 0  RU =URBAN CABLEWORK  YB 0  YB =VERIZON PA INC

Serial Number--[3365988]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========

JOB 3366138  S.T.S., Inc.  12/01/2004  12:29:56
KAD    00005 POCS 12/01/04 12:29:25 3366138-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3366138]-[000] Channel#--[122524 ][0236]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[15]
Work Site--[719-725 N 24TH ST]
     Nearest Intersection--[OLIVE ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [ALSO KNOWN AS 2344 PEROT ST.      WORKING ON THE OLIVE ST SIDE OF THE BLDG
      N SIDEWALK.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.967756/-75.177217,39.968196/-75.177139,39.968116/-75.176149
      39.967557/-75.176331,39.967537/-75.177034]

Type of Work--[DRIVEWAY CURB CUT]                            Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[CAL 20 ASSOCIATES]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[CAL 20 ASSOCIATES]              Homeowner/Business--[B]
Address--[266 S 23RD ST UNIT 16A]
City--[PHILADELPHIA]                 State--[PA] Zip--[19103]

Caller--[JOHN TORI]                        Phone--[215-735-3020] Ext--[]
FAX--[215-735-1467]  Email--[taney3@aol.com]
Person to Contact--[JOHN TORI]                 Phone--[215-735-3020] Ext--[]
Best Time to Call--[0900-1700]

Prepared--[01-DEC-04] at [1229] by [JANET COPE]

Remarks--
     [REQUESTING PRINTS & PLANS]

ATM0  ATM=AT&T ATLANTA     BL 0  BL =COMCAST PHILA 1  KAD0  KAD=PECO DESIGN
MI 0  MI =MCI              PD 0  PD =PHILA C WTR DPT  PZ 0  PZ =PGW PHLA
SEP0  SEP=SEPTA            TQ 0  TQ =TRIGEN PHILA     YB 0  YB =VERIZON PA INC

Serial Number--[3366138]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========
JOB 3366301  S.T.S., Inc.  12/01/2004  13:29:11
KAD    00007 POCS 12/01/04 13:28:11 3366301-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3366301]-[000] Channel#--[132014 ][0434]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[05]
Work Site--[926-928 RACE ST]
     Nearest Intersection--[N 9TH ST]
     Second Intersection--[N 10TH ST]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [MARK THE FRONT OF THE PRIVATE PROPERTY.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.955609/-75.155973,39.955390/-75.153863,39.954851/-75.154020
      39.955090/-75.156207]

Type of Work--[SWR LINE CONNECTION]                          Depth--[]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[X] Other--[]
Owner/Work Being Done for--[TC LEI]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[JG PARK & ASSOCIATES]           Homeowner/Business--[B]
Address--[1084 TAYLORSVILLE RD  PO BOX 518]
City--[WASHINGTON CROSSING]          State--[PA] Zip--[18977]

Caller--[MOONLIGHT TONG]                   Phone--[215-493-5546] Ext--[]
FAX--[215-493-5663]  Email--[jgp518@aol.com]
Person to Contact--[MOONLIGHT TONG]            Phone--[215-493-5546] Ext--[]
Best Time to Call--[0900-1600]

Prepared--[01-DEC-04] at [1327] by [IRMA BODA]

Remarks--
     [PLEASE SEND MAPS AND PLANS. CALLER IS ALSO REQUESTING VERBAL CONFIRMATION.]

ATM0  ATM=AT&T ATLANTA     BL 0  BL =COMCAST PHILA 1  CAV0  CAV=CITY SIGNAL COM
CNV0  CNV=CAVALIER TEL     EI 0  EI =AT&T LOCAL SVCS  IHP0  IHP=INDEP NATL PARK
KAD0  KAD=PECO DESIGN      LKC0  LKC=LEVEL 3 COMM     MI 0  MI =MCI
MMF0  MMF=ABOVENET COMM    PD 0  PD =PHILA C WTR DPT  PLC0  PLC=XO COMM
PZ 0  PZ =PGW PHLA         RCN0  RCN=RCN TLCOM PHILA  SEP0  SEP=SEPTA
SRC0  SRC=SUNGARD REC SVC  TQ 0  TQ =TRIGEN PHILA     WCS0  WCS=WILTEL COMM LLC
YA 0  YA =VERIZON PA INC-  YB 0  YB =VERIZON PA INC

Serial Number--[3366301]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========

JOB 3366383  S.T.S., Inc.  12/01/2004  13:53:38
KAD    00008 POCS 12/01/04 13:53:00 3366383-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[3366383]-[000] Channel#--[134721 ][0312]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]         Ward--[08]
Work Site--[ARCH ST]
     Nearest Intersection--[N 20TH ST]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[N]
     Location Information--
     [130FT E OF THE INTER OF ARCH ST & 20TH ST ON ARCH ST.]
     Caller Lat/Lon--[]
     Mapped Type--[P] Mapped Lat/Lon--
     [39.955059/-75.173250,39.956137/-75.172885,39.955818/-75.170671
      39.954819/-75.171088]

Type of Work--[TELECOMMUNICATIONS]                           Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]
Owner/Work Being Done for--[SUNESYS]

               Lawful Start Dates--[         ] Through [         ]
    Proposed Dig Date--[DESIGN    ]

Contractor--[SUNESYS]                        Homeowner/Business--[B]
Address--[202 TITUS AVE]
City--[WARRINGTON]                   State--[PA] Zip--[18976]

Caller--[DAVE HAYWARD]                     Phone--[267-927-2000] Ext--[]
FAX--[267-927-2090]  Email--[david.hayward@infrasourceinc.com]
Person to Contact--[DAVE HAYWARD]              Phone--[267-927-2000] Ext--[]
Best Time to Call--[0800-1600]

Prepared--[01-DEC-04] at [1352] by [PATTI MCINTOSH]

Remarks--
     [CALLER IS REQUESTING PRINTS.]

ATM0  ATM=AT&T ATLANTA     BL 0  BL =COMCAST PHILA 1  CAV0  CAV=CITY SIGNAL COM
CNV0  CNV=CAVALIER TEL     EI 0  EI =AT&T LOCAL SVCS  KAD0  KAD=PECO DESIGN
LKC0  LKC=LEVEL 3 COMM     MI 0  MI =MCI              MMF0  MMF=ABOVENET COMM
PD 0  PD =PHILA C WTR DPT  PLC0  PLC=XO COMM          PZ 0  PZ =PGW PHLA
QS 0  QS =QWEST COMM       SEP0  SEP=SEPTA            TQ 0  TQ =TRIGEN PHILA
WCS0  WCS=WILTEL COMM LLC  YA 0  YA =VERIZON PA INC-

Serial Number--[3366383]-[000]

========== Copyright (c) 2004 by Pennsylvania One Call System, Inc. ==========

