
					
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 31260115
Transmit      Date: 02/13/04      Time: 12:39    Op: sonia
Original Call Date: 02/13/04      Time: 12:06    Op: sonia
Work to Begin Date: 02/13/04      Time: 12PM

Place: HARRINGTON
Address: 101A        Street: CLUCKEY DR
Nearest Intersecting Street: NEARESTING INTERSECTING ST

Type of Work: TYPE OF WORK FIELD - THIS IS A TEST
Extent of Work: NEW TKT FORMAT TESTING.  PAGE IS NO LONGER PADDED WITH
: ZEROS. 1ST LINE OF MPG IS OP. INPUT & CAN BE BLANK.  2ND LINE IS MAPPING
: OUTPUT. BELOW THAT WILL BE LATITUDE & LONGITUDE DATA.  1ST LINE IS CALLER
: INPUT AND 2ND LINE IS MAP OUTPUT.  VERIFY WHETHER OR NOT YOU RECEIVED
Remarks: FORMAT WITHOUT PROBLEM.  CALL SONIA RUSH AT 410-782-2024 OR EMAIL
: AT SONIA@MISSUTILITY.NET ASAP.

Company      : MISS UTILITY / ONE CALL
Contact Name : SONIA RUSH                     Fax          : (410)782-2052
Contact Phone: (410)712-0056                  Ext          : 2008
Alt. Contact : DIRECT LINE                    Alt. Phone   : (410)782-2024
Work Being Done For: WORK BEING DONE FOR - TEST
State: DE              County: KENT
Map:       Page:       Grid Cells:
Map: *MORE Page: 36    Grid Cells: A10,A11

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 38.909123 Lon: -77.052209  SE Lat: 38.909123 Lon: -77.052209
Explosives: N
CC01       CHOP01     CSC01      CUDE14     DECO26     DECO27     DPCV29
 DPCV30     DPML12     DPSA15     DPSA16     DPU104     HARR01     KCDE01
 LTC04      MFN05      PFNT04     SCED01     SDHY24     TIDE03     TMC01
 TOB01      TOBB01     TOFB01     TOG01      TOL01      TOPR01     TOWD01
 TRM01      TUNN01     UNCA28     UNCA29     UOFD02     UTSY39     VBE01
 VKC01      WSUB34     ZMISSU
Send To: CNTV01    Seq No: 0003   Map Ref:
         CNTV02            0001
         DSTS45            0001
         KCBA01            0001
         TNC01             0001

				
					
NOTICE OF INTENT TO EXCAVATE      EMERGENCY
Ticket No: 31260120
Transmit      Date: 02/13/04      Time: 12:39    Op: sonia
Original Call Date: 02/13/04      Time: 12:28    Op: sonia
Work to Begin Date: 02/13/04      Time: 12PM

Place: HARRINGTON
Address: 101A        Street: CLUCKEY DR
Nearest Intersecting Street: NEARESTING INTERSECTING ST

Type of Work: TYPE OF WORK FIELD - THIS IS A TEST
Extent of Work: NEW TKT FORMAT TESTING.  PAGE IS NO LONGER PADDED WITH
: ZEROS. 1ST LINE OF MPG IS OP. INPUT & CAN BE BLANK.  2ND LINE IS MAPPING
: OUTPUT. BELOW THAT WILL BE LATITUDE & LONGITUDE DATA.  1ST LINE IS CALLER
: INPUT AND 2ND LINE IS MAP OUTPUT.  VERIFY WHETHER OR NOT YOU RECEIVED
Remarks: FORMAT WITHOUT PROBLEM.  CALL SONIA RUSH AT 410-782-2024 OR EMAIL
: AT SONIA@MISSUTILITY.NET ASAP.

Company      : MISS UTILITY / ONE CALL
Contact Name : SONIA RUSH                     Fax          : (410)782-2052
Contact Phone: (410)712-0056                  Ext          : 2008
Alt. Contact : DIRECT LINE                    Alt. Phone   : (410)782-2024
Work Being Done For: WORK BEING DONE FOR - TEST
State: DE              County: KENT
Map: NEWC  Page: 35    Grid Cells: K10
Map: *MORE Page: 35    Grid Cells: K10,K11

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 38.914943 Lon: -77.037123  SE Lat: 38.914943 Lon: -77.037123
Explosives: N
CC01       CHOP01     CSC01      CUDE14     DECO26     DECO27     DPCV29
 DPCV30     DPML12     DPSA15     DPSA16     DPU104     DPUG07     HARR01
 KCDE01     LTC04      MFN05      PFNT04     SCED01     SDHY24     TMC01
 TOFB01     TOPR01     TOWD01     TRM01      TUNN01     UNCA28     UNCA29
 UOFD02     UTSY39     VBE01      VKC01      WSUB34     ZMISSU
Send To: CNTV01    Seq No: 0004   Map Ref:
         CNTV02            0002
         DSTS45            0002
         KCBA01            0002
         TNC01             0002

				