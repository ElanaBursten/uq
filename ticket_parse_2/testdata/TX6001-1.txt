
SEQUENCE NUMBER 0019   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENERGY ALLIANCE  TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941930

Prepared By ALECIA STANLEY       On 12-JUL-04  At 1107

MapRef :                            Grid: 325430097173A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4136 BOLEN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 11:07 am

Excavation Type : ELECTRIC                      
Nature of Work  : INSTL UG ELEC                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SANDY JONES
Company Name   : TEXAS STATE UTILITIES, INC.
Work by TEXAS STATE UTILITIES, INC.      For TRI COUNTY CO-OP

Person to Contact : SANDY JONES

Phone No.  ( 817 )665-9000 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )665-9005
Email:     SR@TSU1.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LACEY 
MAPSCO:21,V  LOCATE THE ENTIRE PROP WITHIN A 50' PERIMTER AROUND THE P
ROP, INCLUDING ALL MAINS,TAPS, DROPS AND PRIMARIES FOR ALL CO'S. ALONG
 WITH ALL GAS LINES AND T'S.                                          
                                                                      
                                                                      
                                                                      
                                                                      
SR@TSU1.COM

Map Cross Reference : MAPSCO 21,V                         

FaxBack Requested ? YES    Lone Star Xref: 


041941930 to EMAIL ADDRESS at 11:11:10 on MON, 07/12/04 for FN3 #0019

SEQUENCE NUMBER 0020   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     SM&P-F         
TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941934

Prepared By APRIL GONZALES       On 12-JUL-04  At 1107

MapRef :                            Grid: 325630097153A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 SALINAS ST 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 07:31 am

Excavation Type : TELEPHONE                     
Nature of Work  : INSTL U/G PHONE/FIBER         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JESSE  BLANKENSHIP
Company Name   : HENKLES AND MCCOY
Work by HENKLES AND MCCOY      For VERIZON

Person to Contact : JESSE  BLANKENSHIP

Phone No.  ( 972 )512-2900 /( 469 )644-0666 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )512-2957
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SONOMA DR 
UPDATE AND REMARK -041880395  STARTING AT THE INTER, LOC BOTH SIDES OF
 SALINAS ST GOING N TO LOS ALTOS RD.  MAPSCO 22,D  (BORING,TRENCHING,B
ACKHOE)                                                               
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 9,W                          

FaxBack Requested ? YES    Lone Star Xref: 


041941934 to EMAIL ADDRESS at 11:11:13 on MON, 07/12/04 for FN3 #0020

SEQUENCE NUMBER 0025   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENERGY ALLIANCE  TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941964

Prepared By ALECIA STANLEY       On 12-JUL-04  At 1109

MapRef :                            Grid: 325430097173A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4129 DUNCAN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 11:09 am

Excavation Type : ELECTRIC                      
Nature of Work  : INSTL UG ELEC                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SANDY JONES
Company Name   : TEXAS STATE UTILITIES, INC.
Work by TEXAS STATE UTILITIES, INC.      For TRI COUNTY CO-OP

Person to Contact : SANDY JONES

Phone No.  ( 817 )665-9000 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )665-9005
Email:     SR@TSU1.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LACEY 
MAPSCO:21,V  LOCATE THE ENTIRE PROP WITHIN A 50' PERIMTER AROUND THE P
ROP, INCLUDING ALL MAINS,TAPS, DROPS AND PRIMARIES FOR ALL CO'S. ALONG
 WITH ALL GAS LINES AND T'S.                                          
                                                                      
                                                                      
                                                                      
                                                                      
SR@TSU1.COM

Map Cross Reference : MAPSCO 21,V                         

FaxBack Requested ? YES    Lone Star Xref: 


041941964 to EMAIL ADDRESS at 11:13:32 on MON, 07/12/04 for FN3 #0025

SEQUENCE NUMBER 0021   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENERGY ALLIANCE  TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941944

Prepared By ALECIA STANLEY       On 12-JUL-04  At 1107

MapRef :                            Grid: 325430097173A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4120 BOLEN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 11:08 am

Excavation Type : ELECTRIC                      
Nature of Work  : INSTL UG ELEC                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SANDY JONES
Company Name   : TEXAS STATE UTILITIES, INC.
Work by TEXAS STATE UTILITIES, INC.      For TRI COUNTY CO-OP

Person to Contact : SANDY JONES

Phone No.  ( 817 )665-9000 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )665-9005
Email:     SR@TSU1.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LACEY 
MAPSCO:21,V  LOCATE THE ENTIRE PROP WITHIN A 50' PERIMTER AROUND THE P
ROP, INCLUDING ALL MAINS,TAPS, DROPS AND PRIMARIES FOR ALL CO'S. ALONG
 WITH ALL GAS LINES AND T'S.                                          
                                                                      
                                                                      
                                                                      
                                                                      
SR@TSU1.COM

Map Cross Reference : MAPSCO 21,V                         

FaxBack Requested ? YES    Lone Star Xref: 


041941944 to EMAIL ADDRESS at 11:13:10 on MON, 07/12/04 for FN3 #0021

SEQUENCE NUMBER 0022   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     MILLENNIUM TEL 
SM&P-F           TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941950

Prepared By APRIL GONZALES       On 12-JUL-04  At 1108

MapRef :                            Grid: 325630097160A  Footprint: D15

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 EMMERYVILLE LN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 07:31 am

Excavation Type : TELEPHONE                     
Nature of Work  : INSTL U/G PHONE/FIBER         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JESSE  BLANKENSHIP
Company Name   : HENKLES AND MCCOY
Work by HENKLES AND MCCOY      For VERIZON

Person to Contact : JESSE  BLANKENSHIP

Phone No.  ( 972 )512-2900 /( 469 )644-0666 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )512-2957
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: COSTA MESA DR 
UPDATE AND REMARK - 041880386  FROM THE INTER, LOC BOTH SIDES OF EMMER
YVILLE GOING S TO 5344 LOS ALTOS.   MAPSCO 22,C  (BORING,TRENCHING,BAC
KHOE)                                                                 
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 8,Z                          

FaxBack Requested ? YES    Lone Star Xref: 


041941950 to EMAIL ADDRESS at 11:13:15 on MON, 07/12/04 for FN3 #0022

SEQUENCE NUMBER 0023   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENERGY ALLIANCE  TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941953

Prepared By ALECIA STANLEY       On 12-JUL-04  At 1108

MapRef :                            Grid: 325430097173A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4121 BOLEN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 11:08 am

Excavation Type : ELECTRIC                      
Nature of Work  : INSTL UG ELEC                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SANDY JONES
Company Name   : TEXAS STATE UTILITIES, INC.
Work by TEXAS STATE UTILITIES, INC.      For TRI COUNTY CO-OP

Person to Contact : SANDY JONES

Phone No.  ( 817 )665-9000 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )665-9005
Email:     SR@TSU1.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LACEY 
MAPSCO:21,V  LOCATE THE ENTIRE PROP WITHIN A 50' PERIMTER AROUND THE P
ROP, INCLUDING ALL MAINS,TAPS, DROPS AND PRIMARIES FOR ALL CO'S. ALONG
 WITH ALL GAS LINES AND T'S.                                          
                                                                      
                                                                      
                                                                      
                                                                      
SR@TSU1.COM

Map Cross Reference : MAPSCO 21,V                         

FaxBack Requested ? YES    Lone Star Xref: 


041941953 to EMAIL ADDRESS at 11:13:22 on MON, 07/12/04 for FN3 #0023

SEQUENCE NUMBER 0024   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     SM&P-F         
TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941962

Prepared By APRIL GONZALES       On 12-JUL-04  At 1109

MapRef :                            Grid: 325630097160A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 SONOMA DR 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 07:31 am

Excavation Type : TELEPHONE                     
Nature of Work  : INSTL U/G PHONE/FIBER         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JESSE  BLANKENSHIP
Company Name   : HENKLES AND MCCOY
Work by HENKLES AND MCCOY      For VERIZON

Person to Contact : JESSE  BLANKENSHIP

Phone No.  ( 972 )512-2900 /( 469 )644-0666 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )512-2957
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SALINAS ST 
UPDATE AND REMARK - 041880367   FROM THE INTER , LOC BOTH SIDES OF SON
OMA DR, GOING W TO END OF THE ST.. MAPSCO 22,C  (BORING,TRENCHING,BACK
HOE)                                                                  
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 8,Y                          

FaxBack Requested ? YES    Lone Star Xref: 


041941962 to EMAIL ADDRESS at 11:13:26 on MON, 07/12/04 for FN3 #0024

SEQUENCE NUMBER 0026   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      CHARTER COMM     SM&P-F           LONE STAR XCHG

Locate Request No. 041941979

Prepared By CHRIS JOHNSON        On 12-JUL-04  At 1110

MapRef :                            Grid: 324730097213A  Footprint: D03

Location:     County: TARRANT  Town: FOREST HILL

             Address: 00 W LORAINE AVE 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date          at 

Excavation Type : POLE/SIGN INSTALLATION        
Nature of Work  : POLE AND ANCHOR INSTL         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : DAVE VOLCIK
Company Name   : RED SIMPSON
Work by RED SIMPSON      For TXU ELEC

Person to Contact : DAVE VOLCIK

Phone No.  ( 817 )907-3569 /( 817 )669-0500 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )227-0955
Email:     DVOLCIK@EV1.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: ROOSEVELT AVE 
MAPSCO 62,A - FROM INTER GO E APPX 250' ON LORAINE TO DEAD END, TURN N
 APPX 450' ALONG W SIDE OF CREEK TO AREA MARKED W/2 STAKES AND RED RIB
BON                                                                   
                                                                      
                                                                      
                                                                      
                                                                      
DVOLCIK@EV1.NET

Map Cross Reference : MAPSCO 62,A                         

FaxBack Requested ? NO     Lone Star Xref: 


041941979 to EMAIL ADDRESS at 11:15:02 on MON, 07/12/04 for FN3 #0026

SEQUENCE NUMBER 0027   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     SM&P-F         
TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941983

Prepared By APRIL GONZALES       On 12-JUL-04  At 1111

MapRef :                            Grid: 325630097153A  Footprint: D12

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 SONOMA DR 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 11:11 am

Excavation Type : TELEPHONE                     
Nature of Work  : INSTL U/G PHONE/FIBER         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JESSE  BLANKENSHIP
Company Name   : HENKLES AND MCCOY
Work by HENKLES AND MCCOY      For VERIZON

Person to Contact : JESSE  BLANKENSHIP

Phone No.  ( 972 )512-2900 /( 469 )644-0666 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )512-2957
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SALINAS ST 
UPDATE AND REMARK - 041880379  STARTING AT THE ABOVE INTER, LOC BOTH S
IDES OF SONOMA DR GOING E TO THE END OF THE CUL-DE-SAC. MAPSCO 22,D  (
BORING,TRENCHING,BACKHOE)                                             
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 8,Z                          

FaxBack Requested ? YES    Lone Star Xref: 


041941983 to EMAIL ADDRESS at 11:15:06 on MON, 07/12/04 for FN3 #0027

SEQUENCE NUMBER 0028   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     MOBIL OIL      
MILLENNIUM TEL   SM&P-F           TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041941976

Prepared By APRIL GONZALES       On 12-JUL-04  At 1110

MapRef :                            Grid: 325400097160A  Footprint: D08

Location:     County: TARRANT  Town: FORT WORTH

             Address: 0 EMMERYVILLE LN 

Beginning Work Date 07/14/04 Time of Day: 11:15 am   Duration: 10 DAYS 

Fax-A-Locate Date 07/12/04 at 07:31 am

Excavation Type : TELEPHONE                     
Nature of Work  : INSTL U/G PHONE/FIBER         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : JESSE  BLANKENSHIP
Company Name   : HENKLES AND MCCOY
Work by HENKLES AND MCCOY      For VERIZON

Person to Contact : JESSE  BLANKENSHIP

Phone No.  ( 972 )512-2900 /( 469 )644-0666 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 972 )512-2957
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: COSTA MESA DR 
UPDATE AND REMARK - 041880372  FROM THE INTER, LOC GOING N TO NW ON BO
TH SIDES OF EMMERYVILLE TO THE INTER OF MURRIERA WAY. MAPSCO 36,C  (BO
RING,TRENCHING,BACKHOE)                                               
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 22,Y                         

FaxBack Requested ? YES    Lone Star Xref: 


041941976 to EMAIL ADDRESS at 11:15:09 on MON, 07/12/04 for FN3 #0028

SEQUENCE NUMBER 0119   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
AT&T LOCAL       TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   PANTHER P/L NTX  SM&P-F           TR COUNTY NE     
LONE STAR XCHG

Locate Request No. 041942799

Prepared By MELANIE RIVERA       On 12-JUL-04  At 1254

MapRef :                            Grid: 325630097163A  Footprint: D16

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4845 CRUMBCAKE 

Beginning Work Date 07/14/04 Time of Day: 01:00 pm   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCING                       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : BRIDGETTE
Company Name   : CORRAL FENCE CO. INC.
Work by CORRAL FENCE CO. INC.      For LENNAR

Person to Contact : BRIDGETTE

Phone No.  ( 817 )654-1555 /(    )     ( Hours: 10:00 am/10:00 am )
Fax No.    ( 817 )429-7159
Email:     CORRALFENCE@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: KELLER HICKS & ALTA VI 
WORK DATE: 07-19-2004 08:00 AM  MAPSCO: 22-B  GRIDS:   LAT/LONG:     1
72343.XML                                                             
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CORRALFENCE@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 22,B                         

FaxBack Requested ? YES    Lone Star Xref: 


041942799 to EMAIL ADDRESS at 12:58:27 on MON, 07/12/04 for FN3 #0119

SEQUENCE NUMBER 0120   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     MILLENNIUM TEL 
SM&P-F           TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041942801

Prepared By RC_LASITER           On 12-JUL-04  At 1254

MapRef :                            Grid: 325700097130A  Footprint: D12

Location:     County: TARRANT  Town: KELLER

             Address: 1413 LIZZY CT

Beginning Work Date 07/14/04   Time of Day: 01:00 pm   Duration: 03 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : PLUMBING ROUGH                

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SHANNON BOWLING
Company Name   : LASITER AND LASITER PLUMBING
Work by LASITER AND LASITER PLUMBING      For BUILDER

Person to Contact : SHANNON BOWLING

Phone No.  ( 817 )831-4245 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )831-3268
Email:     CALLER DID NOT PROVIDE

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: UNKNOWN 
LOCATE ENTIRE PROPERTY   MARK ALL UNDERGROUND FACILTIIES PAINT & FLAG 
EVERY 10 TO 12 FEET LOT 13 BLOCK 5 MAPSCO 9 Z SUBDIVISION OVERTON RIDG
E                                                                     
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE

Map Cross Reference : MAPSCO 9,Z                          

FaxBack Requested ? YES    Lone Star Xref: 


041942801 to EMAIL ADDRESS at 12:58:30 on MON, 07/12/04 for FN3 #0120

SEQUENCE NUMBER 0122   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM     MILLENNIUM TEL 
SM&P-F           TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041942825

Prepared By RC_LASITER           On 12-JUL-04  At 1257

MapRef :                            Grid: 325700097130A  Footprint: D16

Location:     County: TARRANT  Town: KELLER

             Address: 1420 LIZZY CT

Beginning Work Date 07/14/04   Time of Day: 01:00 pm   Duration: 03 DAYS 

Fax-A-Locate Date          at 

Excavation Type : OTHER                         
Nature of Work  : PLUMBING ROUGH                

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : SHANNON BOWLING
Company Name   : LASITER AND LASITER PLUMBING
Work by LASITER AND LASITER PLUMBING      For BUILDER

Person to Contact : SHANNON BOWLING

Phone No.  ( 817 )831-4245 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )831-3268
Email:     CALLER DID NOT PROVIDE

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: UNKNOWN 
LOCATE ENTIRE PROPERTY   MARK ALL UNDERGROUND FACILITIES PAINT & FLAG 
EVERY 10 TO 12 FEET LOT 10 BLOCK 5 MAPSCO 9 Z SUBDIVISION OVERTON RIDG
E                                                                     
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE

Map Cross Reference : MAPSCO 9,V                          

FaxBack Requested ? YES    Lone Star Xref: 


041942825 to EMAIL ADDRESS at 13:01:07 on MON, 07/12/04 for FN3 #0122

SEQUENCE NUMBER 0121   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      MOBIL OIL        MILLENNIUM TEL   SM&P-F           
LONE STAR XCHG

Locate Request No. 041942821

Prepared By S_ELLISGIBBS         On 12-JUL-04  At 1256

MapRef :                            Grid: 325130097140C  Footprint: D02

Location:     County: TARRANT  Town: NORTH RICHLAND HILLS

             Address: 6701 MID CITIES BLVD 

Beginning Work Date 07/14/04 Time of Day: 01:00 pm   Duration: 06 HOURS

Fax-A-Locate Date          at 

Excavation Type : WATER                         
Nature of Work  : INSTALLING A WATER LINE       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : CHUCK JAMES
Company Name   : CITY OF NORTH RICHLAND HILLS
Work by CITY OF NORTH RICHLAND HILLS      For CITY OF NORTH RICHLAND HILLS

Person to Contact : JOHN

Phone No.  ( 817 )427-6440 /(    )     ( Hours: 08:00 am/08:30 am )
Fax No.    ( 817 )427-6444
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SUNSET RD 
LOCATE IN FRONT OF THE PROPERTY BEHIND THE CURB.                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 37,Y                         

FaxBack Requested ? NO     Lone Star Xref: 


041942821 to EMAIL ADDRESS at 13:01:01 on MON, 07/12/04 for FN3 #0121

SEQUENCE NUMBER 0123   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
ENERGY ALLIANCE  TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MOBIL OIL        MILLENNIUM TEL   TR COUNTY NE     LONE STAR XCHG

Locate Request No. 041942881

Prepared By PRISCILLA CEBALLOS   On 12-JUL-04  At 1304

MapRef :                            Grid: 325430097170A  Footprint: D09

Location:     County: TARRANT  Town: FORT WORTH

             Address: 4553 HICKORY MEADOWS LN 

Beginning Work Date 07/14/04 Time of Day: 01:00 pm   Duration: 60 DAYS 

Fax-A-Locate Date          at 

Excavation Type : SWIMMING POOLS                
Nature of Work  : DIGGING SWIMMING POOL         

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : JEFF PEZNELL
Company Name   : ACCENT CONSTRUCTION
Work by ACCENT CONSTRUCTION      For TINA ADAMS

Person to Contact : JEFF PEZNELL

Phone No.  ( 817 )808-9255 /( 817 )808-9255 ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )299-9261
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: FIR DR 
WORK WILL BE IN THE BACKYARD AND E SIDE OF PROP                       
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 22,S                         

FaxBack Requested ? YES    Lone Star Xref: 


041942881 to EMAIL ADDRESS at 13:08:34 on MON, 07/12/04 for FN3 #0123

SEQUENCE NUMBER 0124   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      TXU-E/G-STS      VERIZ-KG- METRO  CHARTER COMM   
MILLENNIUM TEL   QWEST COMM.      SM&P-F           TR COUNTY NE     
LONE STAR XCHG

Locate Request No. 041942889

Prepared By LATASHIA MILES       On 12-JUL-04  At 1306

MapRef :                            Grid: 325500097113A  Footprint: D12

Location:     County: TARRANT  Town: SOUTHLAKE

             Address: 0 PEARSON 

Beginning Work Date 07/14/04 Time of Day: 01:15 pm   Duration: 07 DAYS 

Fax-A-Locate Date          at 

Excavation Type : TELEPHONE                     
Nature of Work  : INSTALL TELEPHONE LINE        

Blasting ? NO           48 Hr Notice ? YES  
White Line ? YES        Digging Deeper Than 16 Inches ? YES    

Person Calling : DALE HARALSON
Company Name   : PARKER LANE
Work by PARKER LANE      For VERIZON

Person to Contact : DALE HARALSON

Phone No.  ( 817 )235-4050 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )318-7772
Email:     PARKERLANE@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: CHESAPEAKE 
LOCATE FROM THE ABOVE INTERSECTION GOING S ON THE E SIDE OF PEARSON AP
X 500'.  MAPSCO 24,Q                                                  
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
PARKERLANE@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 24,Q                         

FaxBack Requested ? YES    Lone Star Xref: 


041942889 to EMAIL ADDRESS at 13:09:51 on MON, 07/12/04 for FN3 #0124

SEQUENCE NUMBER 0125   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      CHARTER COMM     SM&P-F           TR COUNTY NE     
LONE STAR XCHG

Locate Request No. 041942916

Prepared By ROGER RODRIGUEZ      On 12-JUL-04  At 1310

MapRef :                            Grid: 325330097113A  Footprint: D08

Location:     County: TARRANT  Town: NORTH RICHLAND HILLS

             Address: 8321 WESTWIND 

Beginning Work Date 07/14/04 Time of Day: 01:15 pm   Duration: 01 DAY  

Fax-A-Locate Date          at 

Excavation Type : IRRIGATION                    
Nature of Work  : INSTAL SPRINKLERS             

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : TAYLOR PEARCE
Company Name   : PRECISION IRRIGATION
Work by PRECISION IRRIGATION      For HOMEOWNER

Person to Contact : TAYLOR PEARCE

Phone No.  ( 817 )825-7752 /(    )     ( Hours: 08:00 am/08:00 am )
Fax No.    ( 817 )421-2670
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: SHADYWOOD 
LOC THE ENTIRE PROP, MAPSCO 38-C                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 38,C                         

FaxBack Requested ? YES    Lone Star Xref: 


041942916 to EMAIL ADDRESS at 13:14:19 on MON, 07/12/04 for FN3 #0125

SEQUENCE NUMBER 0127   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-FUEL-STS     TXU-E/G-STS      CHARTER COMM     SBC-F          
SM&P-F           LONE STAR XCHG

Locate Request No. 041942954

Prepared By PENNY SCHUTZA        On 12-JUL-04  At 1316

MapRef : HP                         Grid: 325000097220A  Footprint: D16

Location:     County: TARRANT  Town: FORT WORTH

             Address: 717 WESTERN STAR 

Beginning Work Date 07/16/04 Time of Day: 08:00 am   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCING                       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : BRIDGETTE
Company Name   : CORRAL FENCE CO. INC.
Work by CORRAL FENCE CO. INC.      For PIONEER HOMES

Person to Contact : BRIDGETTE

Phone No.  ( 817 )654-1555 /(    )     ( Hours: 11:00 am/11:00 am )
Fax No.    ( 817 )429-7159
Email:     CORRALFENCE@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: FOX HUNT 
WORK DATE: 07-16-2004 08:00 AM  MAPSCO: 47-H  GRIDS:   LAT/LONG:   JOC
KEY CLUB & FOX HUNT  172365.XML                                       
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CORRALFENCE@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 47,H                         

FaxBack Requested ? YES    Lone Star Xref: 


041942954 to EMAIL ADDRESS at 13:20:18 on MON, 07/12/04 for FN3 #0127

SEQUENCE NUMBER 0126   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-E/G-STS      CHARTER COMM     SBC-F            LONE STAR XCHG

Locate Request No. 041942952

Prepared By RO_CJOHNSON          On 12-JUL-04  At 1316

MapRef : HP                         Grid: 325030097223A  Footprint: D02

Location:     County: TARRANT  Town: FORT WORTH

             Address: 6040 BRONZE RIVER 

Beginning Work Date 07/14/04 Time of Day: 01:15 pm   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCING                       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : BRIDGETTE
Company Name   : CORRAL FENCE CO. INC.
Work by CORRAL FENCE CO. INC.      For PIONEER HOMES

Person to Contact : BRIDGETTE

Phone No.  ( 817 )654-1555 /(    )     ( Hours: 11:00 am/11:00 am )
Fax No.    ( 817 )429-7159
Email:     CORRALFENCE@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LONGHORN AND GRAND CHA 
WORK DATE: 07-16-2004 08:00 AM  MAPSCO: 47-H  GRIDS:   LAT/LONG:     1
72367.XML                                                             
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CORRALFENCE@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 47,H                         

FaxBack Requested ? YES    Lone Star Xref: 


041942952 to EMAIL ADDRESS at 13:20:16 on MON, 07/12/04 for FN3 #0126

SEQUENCE NUMBER 0128   CDC = FN3
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
TXU-FUEL-STS     TXU-E/G-STS      CHARTER COMM     SBC-F          
SBC-F            LONE STAR XCHG

Locate Request No. 041942961

Prepared By ALECIA STANLEY       On 12-JUL-04  At 1317

MapRef : HP                         Grid: 325030097223A  Footprint: D16

Location:     County: TARRANT  Town: FORT WORTH

             Address: 1056 BREEDERS CUP 

Beginning Work Date 07/16/04 Time of Day: 08:00 am   Duration: 02 DAYS 

Fax-A-Locate Date          at 

Excavation Type : FENCES                        
Nature of Work  : FENCING                       

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? YES    

Person Calling : BRIDGETTE
Company Name   : CORRAL FENCE CO. INC.
Work by CORRAL FENCE CO. INC.      For PIONEER HOMES

Person to Contact : BRIDGETTE

Phone No.  ( 817 )654-1555 /(    )     ( Hours: 11:00 am/11:00 am )
Fax No.    ( 817 )429-7159
Email:     CORRALFENCE@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: LONGHORN AND QUEENS PL 
WORK DATE: 07-16-2004 08:00 AM  MAPSCO: 47-C  GRIDS:   LAT/LONG:     1
72368.XML                                                             
                                                                      
                                                                      
                                                                      
                                                                      
                                                                      
CORRALFENCE@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 47,C                         

FaxBack Requested ? YES    Lone Star Xref: 


041942961 to EMAIL ADDRESS at 13:21:25 on MON, 07/12/04 for FN3 #0128
