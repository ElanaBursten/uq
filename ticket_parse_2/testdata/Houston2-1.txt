NOTICE OF INTENT TO EXCAVATE      EMER-EMERGENCY                
Ticket No: 29405312               Update Of: 
Transmit      Date: 10/21/02      Time: 09:06PM  Op: gwen  
Original Call Date: 10/21/02      Time: 08:58PM  Op: gwen  
Work to Begin Date: 10/21/02      Time: 09:00PM

Place: HUMBLE CITY
Address:    527      Street: CHARLESTON SQ
Nearest Intersecting Street: INTERCONTINENTAL VLG

Type of Work: EMERGENCY REPAIR OF WATER MAIN
Extent of Work: MARK ENTIRE FRONT EASEMENT.
Remarks: CALLER STATES CREW IS ON SITE NOW

Company     : CITY OF HUMBLE
Contact Name: GORDON M              Contact Phone: (281)446-2327
Alt. Contact: TONIGHT               Alt. Phone   : (281)831-5779
Work Being Done For: CITY OF HUMBLE
State: TX              County: HARRIS
Map: TX    Page: 336   Grid Cells: W
Map:     Page:       Grid Cells: 
Ex. Coord NW Lat: 29.991915 Lon: -95.247189  SE Lat: 29.991488 Lon: -95.245869

Explosives: N
SCENT04    TWC20
HLP-URD    
Send To: RELIAN01  Seq No: 1268

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29405158               Update Of: 
Transmit      Date: 10/21/02      Time: 06:25PM  Op: txcaro
Original Call Date: 10/21/02      Time: 06:21PM  Op: txcaro
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: LEAGUE CITY CITY
Address:    710      Street: W BAY AREA BLVD
Nearest Intersecting Street: ORANGE BLOSSOM

Type of Work: INSTALLING UNDERGROUND GAS SERVICE
Extent of Work: 710, 702, 508, 504, 808, 1004 W BAY AREA BLVD. MARK THE
              : ENTIRE PROPERTY, SIDE AND REAR EASEMENTS AND THE ROAD RIGHT OF WAY ON
              : BOTH SIDES OF THE ROAD.
Remarks: FAXED LOCATE REQUEST JOB: 34497663
       :  FAX TO LONE STAR 10/21

Company     : DIGCO
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: CALL FIRST            Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY ENTEX
State: TX              County: GALVESTON
Map: TX    Page: 657   Grid Cells: V
Map: *M    Page: 657   Grid Cells: R,V
Ex. Coord NW Lat: 29.487060 Lon: -95.150577  SE Lat: 29.481748 Lon: -95.142880

Explosives: N
TWC40      XNHP01
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1199

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29404853               Update Of: 
Transmit      Date: 10/21/02      Time: 06:28PM  Op: reissa
Original Call Date: 10/21/02      Time: 05:10PM  Op: reissa
Work to Begin Date: 10/24/02      Time: 08:00AM

Place: HOUSTON
Address:   6635      Street: CALICO WOODS LN
Nearest Intersecting Street: NOT GIVEN

Type of Work: IRRIGATION SYSTEM-IRRIGATION
Extent of Work: MAPSCO 408,V  LOCATE WHOLE PROPERTY. KEYMAP 408,V
              : FAXBACKREQ: N
Remarks: WK TO BEGIN DT 10/24/02 08:00:00
       :  TESSTOC: 022943343 SEQ: 3793 ON 21-OCT-02 AT 1704 RCD: 10/21/02 17:10:20

Company     : CHOATE IRRIGATION
Contact Name: STACEY R              Contact Phone: (972)306-0755
Alt. Contact: STACEY R              Alt. Phone   : none
Work Being Done For: NOT GIVEN
State: TX              County: HARRIS
Map: TX    Page: 408   Grid Cells: V
Map: TX    Page: 408   Grid Cells: V
Ex. Coord NW Lat: 29.874775 Lon: -95.612081  SE Lat: 29.863770 Lon: -95.599390

Explosives: N
TVMAX01    TWC10
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1204

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29405177               Update Of: 
Transmit      Date: 10/21/02      Time: 06:40PM  Op: fxtrue
Original Call Date: 10/21/02      Time: 06:29PM  Op: fxtrue
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: LAKE JACKSON CITY
Address:     91      Street: LAKE RD
Nearest Intersecting Street: SOUTH OAK DRIVE

Type of Work: PLACE MAIN CABLE
Extent of Work: LOCATE ALL CABLE AND UTILITIES START AT DRIVEWAY BEFORE
              : ENTRANCE TO RECREATION CENTER LOCATE APPROX 700FT FROM DRIVEWAY TO
              : DRIVEWAY.
Remarks: FAXED LOCATE REQUEST     PRE APPR#LS2511U

Company     : TEXAS INTERGRATED OPTICS
Contact Name: CASEY                 Contact Phone: (281)446-0253
Alt. Contact: SAME                  Alt. Phone   : none
Work Being Done For: TIME WARNER CABLE
State: TX              County: BRAZORIA
Map: TX    Page: 883   Grid Cells: U
Map:       Page:       Grid Cells: 
Ex. Coord NW Lat: 29.040301 Lon: -95.461527  SE Lat: 29.028296 Lon: -95.443729

Explosives: N
TWC40      XCPOT01
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1210

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  CORRECTION
Ticket No: 29405042               Update Of: 
Transmit      Date: 10/21/02      Time: 07:31PM  Op: fxtrue
Original Call Date: 10/21/02      Time: 05:57PM  Op: fxtrue
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: LAKE JACKSON CITY
Address:    125      Street: PERSIMMON
Nearest Intersecting Street: WILLOW DR

Type of Work: INSTALL POWER SUPPLY
Extent of Work: 125 1/5 PERSIMMON. LOCATE LINES CABLE UTILITIES-E ON
              : OYSTER CREEK DR TO PERSIMMON LN. P/S LOCATED ON LEFT BEFORE CLOVER. POLE
              : LOCATED NE REAR EASEMENT OF 175 LOCATE FROM PLS TO 2ND BEHINE WEST 52FT.
Remarks: FAXED LOCATE REQUEST     PRE APPR#T 13009-C
       :  COR PER L.O.W

Company     : TEXAS INTERGRATED OPTICS
Contact Name: LISA                  Contact Phone: (281)446-0253
Alt. Contact: SAME                  Alt. Phone   : none
Work Being Done For: TIME WARNER CABLE
State: TX              County: BRAZORIA
Map: TX    Page: 884   Grid Cells: J
Map:       Page:       Grid Cells: 
Ex. Coord NW Lat: 29.042872 Lon: -95.436455  SE Lat: 29.038868 Lon: -95.423130

Explosives: N
TWC40      XCPOT01
6INCHGAS   ENTX-MN    HLP-S/L    ENTX-SV    
Send To: RELIAN01  Seq No: 1247

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29405291               Update Of: 
Transmit      Date: 10/21/02      Time: 07:31PM  Op: fxtrue
Original Call Date: 10/21/02      Time: 07:22PM  Op: fxtrue
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: DICKINSON CITY
Address:   2412      Street: NICHOLS
Nearest Intersecting Street: 27TH ST

Type of Work: INSTALLING UNDERGROUND GAS SERVICE
Extent of Work: 2412,2421 NICHOLS. MARK THE ENTIRE PROPERTY,SIDE AND REAR
              : EASEMENTS AND THE ROAD RIGHT OF WAY ON BOTH SIDES OF THE ROAD.
Remarks: FAXED LOCATE REQUEST JOB: 34500587
       :  FAX TO LONE STAR 10/21

Company     : DIGCO
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: CALL FIRST            Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY ENTEX
State: TX              County: GALVESTON
Map: TX    Page: 659   Grid Cells: Y
Map: TX    Page: 659   Grid Cells: Y,Z,U
Ex. Coord NW Lat: 29.476884 Lon: -95.060610  SE Lat: 29.470319 Lon: -95.054157

Explosives: N
XNHP01
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1250

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29405292               Update Of: 
Transmit      Date: 10/21/02      Time: 07:31PM  Op: fxtrue
Original Call Date: 10/21/02      Time: 07:23PM  Op: fxtrue
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: DICKINSON CITY
Address:   2412      Street: HILL
Nearest Intersecting Street: 27TH ST

Type of Work: INSTALLING UNDERGROUND GAS SERVICE
Extent of Work: 2412,2421 HILL. MARK THE ENTIRE PROPERTY,SIDE AND REAR
              : EASEMENTS AND THE ROAD RIGHT OF WAY ON BOTH SIDES OF THE ROAD.
Remarks: FAXED LOCATE REQUEST JOB: 34500587
       :  FAX TO LONE STAR 10/21

Company     : DIGCO
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: CALL FIRST            Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY ENTEX
State: TX              County: GALVESTON
Map: TX    Page: 659   Grid Cells: Y
Map: TX    Page: 659   Grid Cells: Y,Z
Ex. Coord NW Lat: 29.474712 Lon: -95.059874  SE Lat: 29.472312 Lon: -95.057499

Explosives: N
XNHP01
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1251

NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE                  
Ticket No: 29405293               Update Of: 
Transmit      Date: 10/21/02      Time: 07:31PM  Op: fxtrue
Original Call Date: 10/21/02      Time: 07:24PM  Op: fxtrue
Work to Begin Date: 10/24/02      Time: 07:00AM

Place: CROSBY
Address:    123      Street: LE BLANC
Nearest Intersecting Street: BLANCHARD

Type of Work: INSTALLING UNDERGROUND GAS SERVICE
Extent of Work: MARK THE ENTIRE PROPERTY,SIDE AND REAR EASEMENTS AND THE
              : ROAD RIGHT OF WAY ON BOTH SIDES OF THE ROAD.
Remarks: FAXED LOCATE REQUEST JOB: 34470549
       :  FAX TO LONE STAR 10/21

Company     : DIGCO
Contact Name: PHILLIP TAMBORELLO    Contact Phone: (713)691-3616
Alt. Contact: CALL FIRST            Alt. Phone   : (713)333-2134
Work Being Done For: RELIANT ENERGY ENTEX
State: TX              County: HARRIS
Map: TX    Page: 419   Grid Cells: Q
Map: TX    Page: 419   Grid Cells: Q,R,U,V
Ex. Coord NW Lat: 29.882689 Lon: -95.062880  SE Lat: 29.881808 Lon: -95.059895

Explosives: N
CITGO01    HCMUD02    XNHP01
ENTX-MN    ENTX-SV    
Send To: RELIAN01  Seq No: 1252
