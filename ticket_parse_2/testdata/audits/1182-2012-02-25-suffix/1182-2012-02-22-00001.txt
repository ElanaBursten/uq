
KORTERRA AUDIT UTILIQUEST_WV-SMTP-1 *EOD AUDIT* FOR TICKETS SENT ON 2/21/2012

Ticket Number			Seq #
---------------------------------------------------------
WV1205217019PEB			1
WV1205217022PEB			2
WV1205217040PEB			3
WV1205217051PEB			4
WV1205217077PE			5
WV1205217091PEB			6
WV1205217099PEB			7
WV1205217102PEB			8
WV1205217113PE			9
WV1205217117PEB			10
WV1205217139PEB			11
WV1205217150PE			12
WV113410013PE			13
WV113410001PEB			14
WV1205217187PEB			15
WV1205217207PE			16
WV1205217222PEB			17


Total Sent = 17

End Of Report

-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.