
FROM ALOCS
UTILIQUEST
BYER04
AUDIT FOR 4/27/2011

FOR CODE BYER04
TYPE  SEQ#  TICKET                    STATUS                                
----  ----  ------------------------  ------------------------------------- 
!     0001  111170018                 DELIVERED
!     0002  111170019                 DELIVERED
!     0003  111170021                 DELIVERED
      0004  111170032                 DELIVERED
      0005  111170036                 DELIVERED
      0006  111170039                 DELIVERED
      0007  111170041                 DELIVERED
      0008  111170043                 DELIVERED
      0009  111170045                 DELIVERED
      0010  111170048                 DELIVERED
      0011  111170053                 DELIVERED
      0012  111170055                 DELIVERED
      0013  111170056                 DELIVERED
      0014  111170073                 DELIVERED
!     0015  111170088                 DELIVERED
      0016  111170090                 DELIVERED
      0017  111170091                 DELIVERED
      0018  111170092                 DELIVERED
      0019  111170107                 DELIVERED
      0020  111170110                 DELIVERED
      0021  111170118                 DELIVERED
      0022  111170119                 DELIVERED
      0023  111170122                 DELIVERED
      0024  111170123                 DELIVERED
      0025  111170126                 DELIVERED
      0026  111170128                 DELIVERED
      0027  111170129                 DELIVERED
      0028  111170131                 DELIVERED
      0029  111170132                 DELIVERED
      0030  111170135                 DELIVERED
      0031  111170137                 DELIVERED
      0032  111170138                 DELIVERED
      0033  111170139                 DELIVERED
      0034  111170141                 DELIVERED
      0035  111170143                 DELIVERED
      0036  111170145                 DELIVERED
      0037  111170146                 DELIVERED
      0038  111170148                 DELIVERED
      0039  111170150                 DELIVERED
      0040  111170153                 DELIVERED
      0041  111170156                 DELIVERED
      0042  111170161                 DELIVERED
      0043  111170177                 DELIVERED
!     0044  111170184                 DELIVERED
      0045  111170205                 DELIVERED
!     0046  111170207                 DELIVERED
      0047  111170209                 DELIVERED
      0048  111170212                 DELIVERED
      0049  111170217                 DELIVERED
!     0050  111170224                 DELIVERED
      0051  111170235                 DELIVERED
      0052  111170237                 DELIVERED
      0053  111170248                 DELIVERED
      0054  111170269                 DELIVERED
      0055  111170284                 DELIVERED
      0056  111170286                 DELIVERED
      0057  111170287                 DELIVERED
      0058  111170289                 DELIVERED
      0059  111170297                 DELIVERED
      0060  111170313                 DELIVERED
      0061  111170322                 DELIVERED
      0062  111170330                 DELIVERED
      0063  111170333                 DELIVERED
      0064  111170349                 DELIVERED
      0065  111170364                 DELIVERED
      0066  111170382                 DELIVERED
!     0067  111170413                 DELIVERED
      0068  111170431                 DELIVERED
      0069  111170432                 DELIVERED
      0070  111170444                 DELIVERED
      0071  111170454                 DELIVERED
      0072  111170470                 DELIVERED
      0073  111170471                 DELIVERED
      0074  111170473                 DELIVERED
!     0075  111170476                 DELIVERED
!     0076  111170477                 DELIVERED
      0077  111170493                 DELIVERED
      0078  111170501                 DELIVERED
      0079  111170513                 DELIVERED
      0080  111170515                 DELIVERED
      0081  111170517                 DELIVERED
      0082  111170518                 DELIVERED
      0083  111170521                 DELIVERED
      0084  111170525                 DELIVERED
      0085  111170527                 DELIVERED
      0086  111170531                 DELIVERED
      0087  111170533                 DELIVERED
      0088  111170534                 DELIVERED
      0089  111170536                 DELIVERED
      0090  111170538                 DELIVERED
      0091  111170540                 DELIVERED
      0092  111170541                 DELIVERED
      0093  111170546                 DELIVERED
      0094  111170547                 DELIVERED
      0095  111170548                 DELIVERED
      0096  111170551                 DELIVERED
      0097  111170552                 DELIVERED
      0098  111170554                 DELIVERED
      0099  111170557                 DELIVERED
      0100  111170559                 DELIVERED
      0101  111170560                 DELIVERED
      0102  111170563                 DELIVERED
      0103  111170564                 DELIVERED
      0104  111170567                 DELIVERED
      0105  111170568                 DELIVERED
      0106  111170571                 DELIVERED
      0107  111170572                 DELIVERED
      0108  111170578                 DELIVERED
      0109  111170591                 DELIVERED
      0110  111170593                 DELIVERED
      0111  111170596                 DELIVERED
!     0112  111170614                 DELIVERED
      0113  111170617                 DELIVERED
      0114  111170625                 DELIVERED
      0115  111170629                 DELIVERED
      0116  111170637                 DELIVERED
      0117  111170646                 DELIVERED
      0118  111170661                 DELIVERED
      0119  111170675                 DELIVERED
!     0120  111170690                 DELIVERED
!     0121  111170692                 DELIVERED
      0122  111170693                 DELIVERED
      0123  111170699                 DELIVERED
      0124  111170701                 DELIVERED
      0125  111170704                 DELIVERED
      0126  111170705                 DELIVERED
      0127  111170707                 DELIVERED
      0128  111170708                 DELIVERED
      0129  111170709                 DELIVERED
      0130  111170710                 DELIVERED
      0131  111170711                 DELIVERED
      0132  111170713                 DELIVERED
      0133  111170714                 DELIVERED
      0134  111170715                 DELIVERED
      0135  111170716                 DELIVERED
      0136  111170717                 DELIVERED
      0137  111170718                 DELIVERED
      0138  111170721                 DELIVERED
      0139  111170723                 DELIVERED
      0140  111170729                 DELIVERED
      0141  111170734                 DELIVERED
      0142  111170735                 DELIVERED
      0143  111170739                 DELIVERED
      0144  111170742                 DELIVERED
      0145  111170746                 DELIVERED
      0146  111170748                 DELIVERED
      0147  111170751                 DELIVERED
      0148  111170757                 DELIVERED
      0149  111170761                 DELIVERED
      0150  111170764                 DELIVERED
!     0151  111170766                 DELIVERED
      0152  111170775                 DELIVERED
      0153  111170791                 DELIVERED
      0154  111170797                 DELIVERED
      0155  111170829                 DELIVERED
      0156  111170832                 DELIVERED
      0157  111170833                 DELIVERED
      0158  111170837                 DELIVERED
      0159  111170839                 DELIVERED
      0160  111170845                 DELIVERED
      0161  111170848                 DELIVERED
      0162  111170849                 DELIVERED
      0163  111170850                 DELIVERED
      0164  111170851                 DELIVERED
      0165  111170852                 DELIVERED
      0166  111170853                 DELIVERED
      0167  111170855                 DELIVERED
      0168  111170856                 DELIVERED
      0169  111170857                 DELIVERED
      0170  111170858                 DELIVERED
!     0171  111170861                 DELIVERED
      0172  111170866                 DELIVERED
      0173  111170872                 DELIVERED
      0174  111170874                 DELIVERED
      0175  111170879                 DELIVERED
      0176  111170880                 DELIVERED
      0177  111170883                 DELIVERED
      0178  111170889                 DELIVERED
      0179  111170892                 DELIVERED
      0180  111170902                 DELIVERED
      0181  111170923                 DELIVERED
      0182  111170953                 DELIVERED
      0183  111170973                 DELIVERED
      0184  111170985                 DELIVERED
      0185  111170992                 DELIVERED
      0186  111170995                 DELIVERED
      0187  111171034                 DELIVERED
      0188  111171038                 DELIVERED
      0189  111171041                 DELIVERED
      0190  111171046                 DELIVERED
      0191  111171052                 DELIVERED
      0192  111171054                 DELIVERED
      0193  111171057                 DELIVERED
      0194  111171060                 DELIVERED
      0195  111171061                 DELIVERED
      0196  111171062                 DELIVERED
      0197  111171077                 DELIVERED
      0198  111171078                 DELIVERED
      0199  111171079                 DELIVERED
      0200  111171080                 DELIVERED
      0201  111171081                 DELIVERED
      0202  111171082                 DELIVERED
      0203  111171085                 DELIVERED

NORMAL    : 188
RESEND    : 0
EMERGENCY : 15
FAILED    : 0
TOTAL FOR BYER04: 203



LEGEND
-----------------------
  - NORMAL
* - RESEND
! - EMERGENCY

