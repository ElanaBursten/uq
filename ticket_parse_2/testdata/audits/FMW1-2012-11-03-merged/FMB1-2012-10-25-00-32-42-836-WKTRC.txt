
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: PRM01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570028-48HR|  21  12570994-48HR|  40  12571460-48HR|  59  12572192-48HR|
   2  12570156-48HR|  22  12571002-48HR|  41  12571474-48HR|  60  12572193-48HR|
   3  12570220-ETKT|  23  12571038-48HR|  42  12571567-48HR|  61  12572195-48HR|
   4  12570314-48HR|  24  12571099-48HR|  43  12571594-48HR|  62  12572196-48HR|
   5  12570327-UPDT|  25  12571126-48HR|  44  12571634-48HR|  63  12572203-48HR|
   6  12570346-48HR|  26  12571136-48HR|  45  12571638-48HR|  64  12572216-48HR|
   7  12570296-UPDT|  27  12571168-UPDT|  46  12571769-48HR|  65  12572274-48HR|
   8  12570304-UPDT|  28  12571180-48HR|  47* 12571772-ETKT|  66  12572261-ETKT|
   9  12570309-UPDT|  29  12571232-48HR|  48  12571780-48HR|  67  12572293-48HR|
  10  12570377-UPDT|  30  12571349-UPDT|  49  12571802-48HR|  68  12572307-48HR|
  11  12570386-UPDT|  31  12571359-UPDT|  50  12571772-CANC|  69  12572284-48HR|
  12  12570317-UPDT|  32  12571366-UPDT|  51  12571895-48HR|  70  12572387-48HR|
  13  12570388-UPDT|  33  12571361-UPDT|  52  12571922-48HR|  71  12572506-48HR|
  14  12570335-UPDT|  34  12571380-48HR|  53  12571934-UPDT|  72  12572530-48HR|
  15  12570570-48HR|  35  12571387-48HR|  54  12571980-48HR|  73  12572541-48HR|
  16  12570832-UPDT|  36  12571393-48HR|  55  12572080-48HR|  74  12572638-ETKT|
  17  12570858-48HR|  37  12571404-48HR|  56  12572160-48HR|  75  12572643-48HR|
  18  12570922-48HR|  38  12571408-48HR|  57  12572180-48HR|  76  12572745-ETKT|
  19  12570983-UPDT|  39  12571428-48HR|  58  12572191-48HR|  77  12572838-48HR|
  20  12570946-48HR|

* indicates ticket # is repeated

Total Tickets: 77

48HR - STANDARD                       |   54

ETKT - EMERGENCY                      |    5

UPDT - UPDATE                         |   17

CANC - CANCELLATION                   |    1


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


