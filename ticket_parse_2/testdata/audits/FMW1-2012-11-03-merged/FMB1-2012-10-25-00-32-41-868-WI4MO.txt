
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: JTV01

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570132-ETKT|  74  12570890-48HR| 146  12571350-UPDT| 218  12572128-48HR|
   2  12570148-48HR|  75  12570873-48HR| 147  12571356-UPDT| 219  12572173-48HR|
   3  12570151-ETKT|  76  12570918-UPDT| 148  12567403-CANC| 220  12572189-48HR|
   4  12570176-48HR|  77  12570926-UPDT| 149  12571368-FIOS| 221  12572199-FIOS|
   5  12570180-48HR|  78  12570931-UPDT| 150  12571364-48HR| 222  12572200-48HR|
   6  12570183-48HR|  79  12570948-ETKT| 151  12571373-FIOS| 223  12572208-FIOS|
   7  12570190-48HR|  80  12570966-FIOS| 152  12571385-48HR| 224  12572247-48HR|
   8  12570195-48HR|  81  12570970-UPDT| 153  12571316-48HR| 225  12572234-48HR|
   9  12570198-48HR|  82  12570973-UPDT| 154  12571502-48HR| 226  12572239-48HR|
  10  12570193-ETKT|  83  12570976-UPDT| 155  12571513-48HR| 227  12572263-48HR|
  11  12570199-FIOS|  84  12570979-UPDT| 156  12571487-48HR| 228  12572232-48HR|
  12  12570214-48HR|  85  12571004-ETKT| 157  12571521-48HR| 229  12572266-48HR|
  13  12570226-48HR|  86  12571027-FIOS| 158  12571532-48HR| 230  12572329-FIOS|
  14  12570252-UPDT|  87  12571041-48HR| 159  12571542-48HR| 231  12572339-FIOS|
  15  12570254-UPDT|  88  12571048-48HR| 160  12571551-48HR| 232* 12572353-ETKT|
  16  12570308-48HR|  89  12571056-FIOS| 161  12571554-48HR| 233* 12572407-ETKT|
  17  12570282-ETKT|  90  12571058-48HR| 162  12571571-48HR| 234  12565869-REMK|
  18  12570334-UPDT|  91  12571069-FIOS| 163  12571578-48HR| 235  12572353-CANC|
  19  12570338-UPDT|  92  12571105-48HR| 164  12571633-48HR| 236  12572407-CANC|
  20  12570357-48HR|  93  12571044-48HR| 165  12571637-48HR| 237  12572444-48HR|
  21  12570289-UPDT|  94  12571057-48HR| 166  12571640-48HR| 238  12572447-ETKT|
  22  12570372-48HR|  95  12571112-48HR| 167  12571642-48HR| 239  12572439-ETKT|
  23  12570427-ETKT|  96  12571120-48HR| 168  12571646-48HR| 240  12572464-48HR|
  24  12570438-ETKT|  97  12571121-48HR| 169  12571649-48HR| 241  12572479-48HR|
  25  12570406-48HR|  98  12571125-48HR| 170  12571655-UPDT| 242  12572489-48HR|
  26  12570417-48HR|  99* 12571131-UPDT| 171  12571658-UPDT| 243  12572487-48HR|
  27  12570437-48HR| 100  12571133-48HR| 172  12571663-UPDT| 244  12572482-48HR|
  28  12570446-48HR| 101  12571135-48HR| 173  12571665-UPDT| 245  12572494-UPDT|
  29* 12570456-ETKT| 102  12571131-CANC| 174  12571668-UPDT| 246  12572499-48HR|
  30  12570461-48HR| 103  12571137-48HR| 175  12571713-48HR| 247  12572510-48HR|
  31  12570456-CORR| 104  12569860-CANC| 176  12571722-ETKT| 248  12564054-CANC|
  32  12570466-48HR| 105  12569862-CANC| 177  12571785-48HR| 249  12572519-ETKT|
  33  12570566-UPDT| 106  12569863-CANC| 178  12571804-FIOS| 250  12572516-48HR|
  34  12570574-UPDT| 107  12571145-48HR| 179  12571809-ETKT| 251  12572528-UPDT|
  35  12570577-UPDT| 108  12571148-FIOS| 180  12571810-48HR| 252  12572537-48HR|
  36  12570582-UPDT| 109  12571149-48HR| 181  12571826-48HR| 253  12572536-48HR|
  37  12570586-UPDT| 110  12571153-UPDT| 182  12571831-UPDT| 254  12572544-48HR|
  38  12570590-48HR| 111  12571154-48HR| 183  12571814-48HR| 255  12572534-48HR|
  39  12570593-UPDT| 112  12571155-48HR| 184  12564478-CANC| 256  12572573-UPDT|
  40  12570597-UPDT| 113  12571158-UPDT| 185  12571843-48HR| 257  12572574-UPDT|
  41  12570601-UPDT| 114  12571152-UPDT| 186  12571860-UPDT| 258  12572575-UPDT|
  42  12570575-ETKT| 115  12571159-48HR| 187  12571834-48HR| 259  12572579-48HR|
  43  12570600-48HR| 116  12571163-UPDT| 188  12571865-48HR| 260  12572584-48HR|
  44  12570613-48HR| 117  12571146-48HR| 189  12571866-UPDT| 261  12572598-ETKT|
  45  12570616-48HR| 118  12571165-48HR| 190  12571868-UPDT| 262  12572608-48HR|
  46  12570627-48HR| 119  12571166-UPDT| 191  12571871-UPDT| 263  12572644-48HR|
  47  12570604-ETKT| 120  12571170-48HR| 192  12571873-UPDT| 264  12572646-48HR|
  48  12570662-48HR| 121  12571172-UPDT| 193  12571875-UPDT| 265  12572650-48HR|
  49  12570647-ETKT| 122  12571173-48HR| 194  12571867-UPDT| 266  12572652-48HR|
  50  12570681-48HR| 123  12571174-UPDT| 195  12571844-ETKT| 267  12572653-48HR|
  51  12551561-REMK| 124  12571138-48HR| 196  12571877-UPDT| 268  12572660-48HR|
  52  12570770-48HR| 125  12571178-48HR| 197  12571878-UPDT| 269  12572704-48HR|
  53  12570782-48HR| 126  12571184-ETKT| 198  12571883-UPDT| 270  12572707-48HR|
  54  12570790-UPDT| 127  12571185-48HR| 199  12571894-48HR| 271  12572716-48HR|
  55  12569653-CANC| 128  12571189-FIOS| 200  12571897-48HR| 272  12572717-48HR|
  56  12570794-48HR| 129  12571190-48HR| 201  12571900-48HR| 273  12572723-ETKT|
  57  12570810-48HR| 130  12571192-UPDT| 202  12571909-FIOS| 274  12572722-48HR|
  58  12570843-UPDT| 131  12571193-48HR| 203  12571910-FIOS| 275  12572727-48HR|
  59  12570845-UPDT| 132  12571194-48HR| 204  12571912-ETKT| 276  12572741-ETKT|
  60  12570848-48HR| 133  12571197-FIOS| 205* 12571933-UPDT| 277  12572747-FIOS|
  61  12570827-48HR| 134  12571199-48HR| 206  12571933-CANC| 278  12572752-48HR|
  62  12570846-48HR| 135  12571201-UPDT| 207  12571932-48HR| 279  12572767-FIOS|
  63* 12570863-48HR| 136  12571204-48HR| 208  12571971-48HR| 280  12572812-FIOS|
  64* 12570875-48HR| 137  12571211-48HR| 209  12571979-UPDT| 281  12572814-FIOS|
  65  12570882-UPDT| 138  12571213-FIOS| 210  12571975-ETKT| 282  12572816-FIOS|
  66  12570902-UPDT| 139  12571250-FIOS| 211  12572007-UPDT| 283  12572819-FIOS|
  67  12570904-ETKT| 140  12571253-48HR| 212  12572013-48HR| 284  12572826-FIOS|
  68  12570863-CANC| 141  12571313-48HR| 213  12572023-48HR| 285  12572828-FIOS|
  69  12554886-REMK| 142  12571320-UPDT| 214  12572055-48HR| 286  12572835-FIOS|
  70  12570898-UPDT| 143  12571322-48HR| 215  12572105-48HR| 287  12572926-ETKT|
  71  12570875-CANC| 144  12571315-ETKT| 216  12572123-48HR| 288  12572921-48HR|
  72  12570912-UPDT| 145  12571344-UPDT| 217  12572118-48HR| 289  12572927-FIOS|
  73  12570885-48HR|

* indicates ticket # is repeated

Total Tickets: 289

ETKT - EMERGENCY                      |   29

48HR - STANDARD                       |  149

FIOS - FIOS                           |   29

UPDT - UPDATE                         |   65

CORR - CORRECTION                     |    1

REMK - REMARK                         |    3

CANC - CANCELLATION                   |   13


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


