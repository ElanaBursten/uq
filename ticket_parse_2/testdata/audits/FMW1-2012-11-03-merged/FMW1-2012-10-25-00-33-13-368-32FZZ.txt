
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: JICT02

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570134-48HR|  14  12571605-48HR|  27  12572145-48HR|  39  12572793-FIOS|
   2  12570166-UPDT|  15  12571606-FIOS|  28  12572168-48HR|  40  12572794-FIOS|
   3  12570167-UPDT|  16  12571607-48HR|  29  12572262-48HR|  41  12572795-FIOS|
   4  12570458-48HR|  17  12571609-48HR|  30  12572265-48HR|  42  12572804-FIOS|
   5  12570467-UPDT|  18  12571621-48HR|  31  12572325-UPDT|  43  12572823-FIOS|
   6  12570471-UPDT|  19  12550629-REMK|  32  12572431-48HR|  44  12572833-48HR|
   7  12570478-UPDT|  20  12571832-FIOS|  33  12572432-48HR|  45  12572834-48HR|
   8  12570480-UPDT|  21  12571850-UPDT|  34  12572434-48HR|  46  12572839-48HR|
   9  12570796-ETKT|  22  12571935-48HR|  35  12572469-48HR|  47  12572911-FIOS|
  10  12569624-CANC|  23  12571996-48HR|  36  12572529-48HR|  48  12572912-FIOS|
  11  12571089-FIOS|  24  12572027-48HR|  37  12572538-48HR|  49  12572913-FIOS|
  12  12571596-FIOS|  25  12572153-UPDT|  38  12572792-FIOS|  50  12572916-FIOS|
  13  12571602-48HR|  26  12572156-UPDT|

* indicates ticket # is repeated

Total Tickets: 50

48HR - STANDARD                       |   23

UPDT - UPDATE                         |   10

ETKT - EMERGENCY                      |    1

CANC - CANCELLATION                   |    1

FIOS - FIOS                           |   14

REMK - REMARK                         |    1


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


