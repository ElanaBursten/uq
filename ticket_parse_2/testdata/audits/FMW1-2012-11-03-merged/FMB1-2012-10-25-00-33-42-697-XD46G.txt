
MISSU Miss Utility
SUMM
DAILY AUDIT OF TICKETS SENT ON 10/24/12

Date/Time: 10/25/2012 12:05:45 AM
Receiving Terminal: VHF

Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    Seq #   Ticket #    
----- ------------  ----- ------------  ----- ------------  ----- ------------  
   1  12570192-ETKT|  36  12571047-FIOS|  71  12571787-FIOS| 106  12572291-48HR|
   2  12570191-ETKT|  37  12571062-UPDT|  72  12571794-UPDT| 107  12572297-48HR|
   3  12570200-ETKT|  38  12571066-UPDT|  73  12570969-REMK| 108  12572345-UPDT|
   4  12570333-ETKT|  39  12571071-UPDT|  74  12571798-ETKT| 109  12572362-UPDT|
   5  12570459-48HR|  40  12571075-UPDT|  75  12571861-FIOS| 110  12572370-UPDT|
   6  12570500-48HR|  41  12571080-UPDT|  76  12571898-48HR| 111  12572372-UPDT|
   7  12570487-48HR|  42  12571084-UPDT|  77  12571903-48HR| 112  12572375-UPDT|
   8  12570490-ETKT|  43  12571028-48HR|  78  12571955-FTTP| 113  12572399-48HR|
   9  12570503-48HR|  44  12571118-UPDT|  79  12571962-UPDT| 114  12572400-48HR|
  10  12570502-48HR|  45  12571132-48HR|  80  12571964-UPDT| 115  12572416-48HR|
  11  12570547-48HR|  46  12571183-UPDT|  81  12571968-UPDT| 116  12572417-48HR|
  12  12570558-UPDT|  47  12571208-FIOS|  82  12571970-UPDT| 117  12572512-48HR|
  13  12570564-48HR|  48  12571280-48HR|  83  12571972-UPDT| 118  12572526-UPDT|
  14  12570583-48HR|  49  12571293-48HR|  84  12571974-UPDT| 119  12572524-48HR|
  15  12570518-48HR|  50  12571306-48HR|  85  12571976-UPDT| 120  12572531-48HR|
  16  12570531-UPDT|  51  12571307-48HR|  86  12571958-STKT| 121  12572592-48HR|
  17  12570629-ETKT|  52  12571308-48HR|  87  12571978-UPDT| 122  12572659-48HR|
  18  12570751-ETKT|  53  12571312-48HR|  88  12571981-UPDT| 123  12572664-48HR|
  19  12570749-UPDT|  54  12571325-48HR|  89  12571977-STKT| 124  12572669-48HR|
  20  12570753-UPDT|  55  12571339-FIOS|  90  12571983-UPDT| 125  12572670-48HR|
  21* 12570762-ETKT|  56  12571367-48HR|  91  12571994-UPDT| 126  12572672-48HR|
  22  12570775-48HR|  57  12571549-UPDT|  92  12571997-FIOS| 127  12572679-48HR|
  23  12570803-48HR|  58  12571580-UPDT|  93  12572001-UPDT| 128  12572718-48HR|
  24  12570853-48HR|  59  12571583-UPDT|  94  12572014-UPDT| 129  12572719-48HR|
  25  12570869-UPDT|  60  12571604-UPDT|  95  12572016-UPDT| 130  12572720-48HR|
  26  12570874-UPDT|  61  12571608-UPDT|  96  12572019-UPDT| 131  12572724-48HR|
  27  12570878-UPDT|  62  12571612-UPDT|  97  12572021-UPDT| 132  12572725-48HR|
  28  12570880-ETKT|  63  12571617-UPDT|  98  12572040-UPDT| 133  12572728-48HR|
  29  12570871-UPDT|  64  12571625-UPDT|  99  12572043-UPDT| 134  12572731-48HR|
  30  12570888-UPDT|  65  12571630-UPDT| 100  12572046-UPDT| 135  12572733-48HR|
  31  12570892-UPDT|  66  12571588-48HR| 101  12572069-UPDT| 136  12572734-48HR|
  32  12570897-UPDT|  67  12571656-48HR| 102  12572204-FIOS| 137  12572749-48HR|
  33* 12570969-ETKT|  68  12571669-48HR| 103  12572170-48HR| 138  12572756-FIOS|
  34  12570975-48HR|  69  12571671-48HR| 104  12572264-UPDT| 139  12572807-FIOS|
  35  12570762-CANC|  70  12571732-48HR| 105  12572286-48HR|

* indicates ticket # is repeated

Total Tickets: 139

ETKT - EMERGENCY                      |   11

48HR - STANDARD                       |   58

UPDT - UPDATE                         |   56

CANC - CANCELLATION                   |    1

FIOS - FIOS                           |    9

REMK - REMARK                         |    1

FTTP - FTTP                           |    1

STKT - SHORT NOTICE                   |    2


Please call (410) 712-0056 if this data does
not match the tickets you received on 10/24/12


GOOD MORNING - Miss Utility Subscribers of Maryland, DC and Delmarva
Revised on: October 24, 2012 
***********************************************************************
IMPORTANT MESSAGES:

**Ticket Check upgrade for FTP and Web users - As of 2/10/11, we are happy to
report that all owner-members/locators who status tickets via FTP or
through www.managetickets.com can now post comments for all Maryland 
Ticket Check status codes. This enhancement will give locators the
ability to enter a message of up to 200 characters within the comments
box.

If you FTP your status to Ticket Check and need the file format that
includes the Status Comment field, please contact support@managetickets.com
for the updated file format requirements.

Excavators and homeowners will receive the comments on their Ticket Check
fax backs or email confirmations and they will be able to view them on 
Search and Status. 

***********************************************************************
	    MISS UTILITY 2012 HOLIDAY SCHEDULE

New Years Day Obs...January 2
Martin Luther King Jr...January 16
Lincoln's Birthday Obs (MD only)...February 13
President's Day...February 20
Maryland Day Obs (MD only)...March 26
Good Friday (MD & DE only)...April 6
Emancipation Day (DC only)...April 16
Memorial Day... May 28
Independence Day...July 4
Labor Day...September 3
Defender's Day (MD only)...September 12
Columbus Day...October 8
Election Day (MD & DE only)...November 6
Veteran's Day Obs...November 12
Thanksgiving Day...November 22
Day After Thanksgiving (MD and DE only)...November 23
Christmas Eve (DE only)...December 24
Christmas Day...December 25
New Years Eve (DE only)...December 31

The Call center will not include the above holidays when calculating the
start date of tickets respective of State holidays.

As a benefit to all members, your district code will be statused as closed
for the following holidays; New Years Day, Memorial Day, Independence Day,
Labor Day, Thanksgiving Day, the day after Thanksgiving and Christmas Day.

**************************************************************************
			Contact Information

Changes to district code databases should be forwarded to our Member 
Database Administrator, Shannon Stultz at shannonstultz@missutility.net. 
Changes may also be faxed to Miss Utility at 410-712-0062. These changes
include but are not limited to; underground plant notification area, 
contact/address revisions, office open and/or close times and ticket 
receiver modifications.

Regarding temporary office open/close district code changes, we will 
acknowledge your notice within 2 hours of receipt. If you do NOT receive 
our confirmation, please call our Help Desk immediately at 410-712-0056 or
email us at helpdesk@missutility.net. To ensure we are modifying the 
appropriate district codes, a list of all affected codes must be provided 
with the open/close change notice. Additionally, Miss Utility will contact
you when we have completed the district code programming as this affects 
Emergency ticket notification - programming can take up to one hour 
depending on the amount of affected district codes.

Daily Tickets/Audits: If you need a copy of your daily ticket audit or
ticket retransmits contact the retransmit line by calling 410-712-0056.
If you are experiencing problems receiving tickets, call the Help Desk at 
410-712-0056. Please be sure to have your district codes(s) available.

***********************************************************************
                            UPCOMING MEETINGS

The Maryland/DC Damage Prevention Meeting is held the fourth Tuesday of each
month (excluding December) at Miss Utility Center, 7223 Parkway Drive, Suite
100, Hanover, MD. Refreshments at 9am; Meeting at 9:30.

The Maryland/DC Subscribers Meeting time will be held December 12, 2012 at 
the Miss Utility Center, 7223 Parkway Dr, Hanover, MD, from 9:30am to 
12:00pm followed by a holiday luncheon. For questions, contact Dora Parks 
at 410-782-2026.

The Delmarva Membership Meeting is held the third Thursday of each month;
8:45am Coffee, 9:00am Presentations, 9:15am-10am Concerns, 10am-11am
Business Meeting, 11am-Noon Board Meeting. For more information, please
visit www.missutilitydelmarva.com. Meeting Minutes are available via
www.missutlitydelmarva.com/documents.asp.

Have a pleasant day.


