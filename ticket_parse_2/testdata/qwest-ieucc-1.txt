IEUCC4069963-00  509924  2004/03/31 00:24:10  00001

/

  IEUCC
Ticket No:  4069963               =CREW ON SITE=
Send To: USWEST34   Seq No:   15  Map Ref:

Transmit      Date:  3/22/04   Time:  7:41 am    Op: bertha
Original Call Date:  3/22/04   Time:  7:31 am    Op: bertha
Work to Begin Date:  3/22/04   Time:  7:45 am

State: WA            County: SPOKANE                 Place: VERA
Address: 4216        Street: S SULLIVAN ROAD
Nearest Intersecting Street: S BALL ROAD

Twp: 25N   Rng: 44E   Sect-Qtr: 35-SE
Twp: 25N   Rng: 44E   Sect-Qtr: 36-SW,35-SE
Legal Given:

Type of Work: REPAIRING WATERMAIN LEAK
Location of Work: ADD IS APX 100FT N OF INTER, ON W SIDE OF S SULLIVAN
: ROAD. MARK AREA IN FRONT OF ABV ADD. CREW ON SITE.

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
: ==CALLER REQUESTS AREA MARKED A.S.A.P==  DANGER TO PROPERTY        OH: N

Company     : VERA WATER AND POWER
Contact Name: TODD HENRY                       Phone: (509)924-3800
Alt. Contact: TODD HARMEN                      Phone:
Contact Fax : (509)922-3929
Work Being Done For: VERA WATER AND POWER
Additional Members:
ATTCBL21   AVISTA11   ELCLT02    INLND02    LOCINC31   SPENG01    SPENG02
 VERA01     ZZZZZZ99IEUCC4050321-00  509924	 2004/03/31 00:24:10  00005

/

  IEUCC
Ticket No:  4050321               =CREW ON SITE=
Send To: USWEST34   Seq No:    2  Map Ref:
Update Of:  4040339
Transmit      Date:  3/04/04   Time:  5:41 am    Op: caroly
Original Call Date:  3/04/04   Time:  5:37 am    Op: caroly
Work to Begin Date:  3/04/04   Time:  7:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: N NEVADA ST
Nearest Intersecting Street: E HAWTHORN

Twp: 26N   Rng: 43E   Sect-Qtr: 17
Twp: 26N   Rng: 43E   Sect-Qtr: 17-SE-NE
Legal Given:

Type of Work: INSTALL GUARD POLES
Location of Work: MARK AREA MARKED WITH ORANGE STAKES AND YELLOW RIBBON ON
: BOTH SIDES OF ROAD APX 100YDS N OF ABV INTER, APX 100YDS TOTAL. ==UPDATE
: CALLER REQUESTS AREA REMARKED WITH FLAGS DUE TO SNOW COVERING EXISTING
: MARKS.CREW IS ON SITE.MARKS ASAP NO GUAR.SVC OUTAGE==

Remarks:
: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO                          OH: Y

Company     : HENKELS AND MCCOY
Contact Name: ROGER ALMAN                      Phone: (509)725-6500
Alt. Contact: CELL                             Phone: (509)631-0653
Contact Fax :
Work Being Done For: BONNEVILLE POWER ASSOC.
Additional Members:
ATTCBL21   AVISTA08   ELCLT02    LOCINC31   SPENG01    SPOKAN01   SPOKAN02
 SPOKAN03   WHIT01     WLMSP06    XO01       ZZZZZZ99
IEUCC4077474-00	 509924	 2004/03/31 00:24:10  00006

/

  IEUCC
Ticket No:  4077474               ==EMERGENCY==
Send To: USWEST34   Seq No:    5  Map Ref:

Transmit      Date:  3/26/04   Time:  8:17 am    Op: elane
Original Call Date:  3/26/04   Time:  7:28 am    Op: elane
Work to Begin Date:  3/26/04   Time:  7:30 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 408         Street: E 7TH AVE
Nearest Intersecting Street: SHERMAN

Twp: 25N   Rng: 43E   Sect-Qtr: 20-SE
Twp: 25N   Rng: 43E   Sect-Qtr: 20-SW-NW
Legal Given:

Type of Work: REPAIR WATER SRV
Location of Work: ADD APX 199FT W OF ABV INTER. MARK AREA IN WHITE AT ABV
: ADD, CREW ON SITE FOR FURTHER MARKING INSTRUCTION

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
: ==CALLER REQUESTS AREA MARKED A.S.A.P==                            OH: ?

Company     : CITY OF SPOKANE WATER DEPT
Contact Name: LINN                             Phone: (509)625-7800
Alt. Contact: LYNN CLOTHER                     Phone:
Contact Fax :
Work Being Done For: CITY OF SPOKANE WATER DEPT
Additional Members:
ATTCBL21   AVISTA09   FBRLNK01   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03
 TWT01      XO01       ZZZZZZ99IEUCC4056114-00	 509924	 2004/03/31 00:24:10  00007

/

  IEUCC
Ticket No:  4056114               ==EMERGENCY==     CANCELLATION
Send To: USWEST34   Seq No:   85  Map Ref:

Transmit      Date:  3/09/04   Time:  2:47 pm    Op: adrian
Original Call Date:  3/09/04   Time:  2:25 pm    Op: michae
Work to Begin Date:  3/09/04   Time:  2:30 pm

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: FRANCIS AVE
Nearest Intersecting Street: EVERGREEN ROAD

Twp: 26N   Rng: 44E   Sect-Qtr: 27
Twp: 26N   Rng: 44E   Sect-Qtr: 26-SW,27-SE,34-NE,35-NW
Legal Given:

Type of Work: REPAIR TELE MAIN
Location of Work: MARK SE CORNER OF INTER.  SEE CREW ON SITE WITH
: QUESTIONS.

Remarks: PROB SOLVED - NO NEED FOR EXCAVATION
: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO                          OH: N

Company     : W.C. CONNER EXCAVATING
Contact Name: STACEY HOLYOAKE                  Phone: (509)483-1123
Alt. Contact:                                  Phone:
Contact Fax : (509)483-1007
Work Being Done For: QWEST
Additional Members:
AVISTA11   AVISTA12   INLND02    LOCINC31   SPENG01    TRENT01    WLMSP06
 ZZZZZZ99IEUCC4040876-00	 509924	 2004/03/31 00:24:10  00008

/

  IEUCC
Ticket No:  4040876               ==EMERGENCY==     CORRECTION
Send To: USWEST34   Seq No:    6  Map Ref:

Transmit      Date:  2/24/04   Time:  8:17 am    Op: elane
Original Call Date:  2/24/04   Time:  7:08 am    Op: elane
Work to Begin Date:  2/24/04   Time:  7:15 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 1803        Street: E ILLINOIS
Nearest Intersecting Street: MAGNOLIA

Twp: 25N   Rng: 43E   Sect-Qtr: 9-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: REPAIR SWR SRV
Location of Work: ADD LOC AT ABV INTER. MARK ENTIRE REAR OF PROP AT ABV
: ADD.

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P==
: CORRECTED TKT - REMOVE GUARANTEES                                  OH: Y

Company     :
Contact Name: DANIEL LAGUE                     Phone: (509)484-5057
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: DANIEL LAGUE
Additional Members:
ATT05      ATTCBL21   AVISTA09   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03
 XO01       ZZZZZZ99IEUCC4065336-00	 509924	 2004/03/31 00:24:10  00010

/

  IEUCC
Ticket No:  4065336               ==EMERGENCY==
Send To: USWEST34   Seq No:   15  Map Ref:
Update Of:  4048573
Transmit      Date:  3/17/04   Time:  8:55 am    Op: kelly
Original Call Date:  3/17/04   Time:  8:49 am    Op: kelly
Work to Begin Date:  3/17/04   Time:  9:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 2215        Street: S HAYFORD ROAD
Nearest Intersecting Street: DENO

Twp: 25N   Rng: 41E   Sect-Qtr: 12
Twp:  *MORERng: 41E   Sect-Qtr: 13-NE,12-SE-NE
Legal Given:

Type of Work: REPAIR SEWER
Location of Work: ADD IS APX 1/4 MILE N OF INTER, MARK N SIDE OF NEW AUTO
: BODY SHOP, AREA MARKED IN WHITE

Remarks: 2ND UPDATE: REMOVE MOST INSTRUCTIONS/CHANGE TYPE OF WORK.. REQUEST
: REMARKS ASAP! BLUE IS ON SITE WITH BACKHOE (CALL BLUE 370-8579 IF ?OH: N

Company     : VISTA CONSTRUCTION/DEVELOPMENT
Contact Name: CONNIE HILL                      Phone: (509)926-2243
Alt. Contact: DAN HILL                         Phone: (509)926-2243
Contact Fax :
Work Being Done For: AUTO AUCTION
Additional Members:
AIRHT01    ATT05      AVISTA07   CHEV05     DAVISC01   INLND02    LOCINC31
 SPENG01    TOCHAM02   ZZZZZZ99
IEUCC4056971-00	 509924	 2004/03/31 00:24:10  00012

/

  IEUCC
Ticket No:  4056971               =MEET TIME=
Send To: USWEST34   Seq No:   22  Map Ref:

Transmit      Date:  3/10/04   Time:  9:40 am    Op: zach
Original Call Date:  3/10/04   Time:  9:22 am    Op: zach
Work to Begin Date:  3/10/04   Time: 10:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: HAWTHORNE ROAD
Nearest Intersecting Street: NEVADA

Twp: 26N   Rng: 43E   Sect-Qtr: 18,17
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: BUILDING POWER TOWER FOUNDATIONS
Location of Work: FROM INTER, MARK W APX 2MI ALONG POWERLINE R.O.W. TO
: WAIKIKI. PLEASE MEET DON AT HAWTHORNE/WAIKIKI INTERSECTION AT 10AM ON
: 3/15/03 FOR SPECIFIC INSTRUCTIONS. CALL DON IF UNABLE TO MEET.

Remarks:
:                                                                    OH: N

Company     : HENKELS AND MCCOY
Contact Name: DON MCCART                       Phone: (509)725-6500
Alt. Contact: DON MCCART                       Phone: (503)706-8578
Contact Fax :
Work Being Done For: BONNEVILLE POWER
Additional Members:
ATTCBL21   AVISTA08   ELCLT02    FBRLNK01   LOCINC31   SPENG01    SPENG02
 SPENG03    SPOKAN01   SPOKAN02   SPOKAN03   WHIT01     WLMSP06    WSDOT14
 XO01       ZZZZZZ99
IEUCC4061792-00	 509924	 2004/03/31 00:24:10  00014

/

IEUCC
Ticket No:  4061792               =MEET TIME=       CANCELLATION
Send To: USWEST34   Seq No:   21  Map Ref:

Transmit      Date:  3/16/04   Time:  8:45 am    Op: michae
Original Call Date:  3/15/04   Time:  9:42 am    Op: judy
Work to Begin Date:  3/26/04   Time: 10:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 12611       Street: E 4TH AVE
Nearest Intersecting Street: VERCLER ST

Twp: 25N   Rng: 44E   Sect-Qtr: 22-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: PLANTING ORCHARD, INSTALL FENCE
Location of Work: ADD IS AT ABV INTER.  MARK ENTIRE PASTURE AT ABV ADD.
: SITE IS APX 80FT BY 272FT.  CALLER REQUESTS MEETING WITH LOCATORS AT ABV
: ADD SO HOMEOWNER CAN PUT DOGS IN HOUSE AND GET ACCESS TO LOCKED SITE.
: MEETING IS FOR A WEEK FROM THIS FRIDAY.  BEST INFO

Remarks: CANCELED TICKET CLR NEEDS A PRIVATE LOCATOR
: PLEASE CONTACT CALLER IF UNABLE TO ATTEND MEETING                  OH: N

Company     :
Contact Name: MARIA ADAMS                      Phone: (509)926-4455
Alt. Contact: SAME                             Phone:
Contact Fax :
Work Being Done For: MARIA ADAMS
Additional Members:
ATTCBL21   AVISTA11   LOCINC31   MEW01      SPENG01    SPENG02    SPKVLY01
 TWT01      XO01       ZZZZZZ99
IEUCC4038513-00	 509924	 2004/03/31 00:24:10  00013

/

  IEUCC
Ticket No:  4038513               =MEET TIME=       CORRECTION
Send To: USWEST34   Seq No:   15  Map Ref:
Update Of:  4035004
Transmit      Date:  2/20/04   Time: 10:47 am    Op: elane
Original Call Date:  2/20/04   Time: 10:37 am    Op: elane
Work to Begin Date:  2/24/04   Time:  8:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: E 2210      Street: HANGMAN VALLEY ROAD

Twp: 24N   Rng: 43E   Sect-Qtr: 28
Twp: 24N   Rng: 43E   Sect-Qtr: 27-SW,28-SW-SE-NE
Legal Given:

Type of Work: INSTALL GUARDRAIL
Location of Work: MARK IN CLUB HOUSE AREA AT ABV ADD, MEET AT HANGMAN
: VALLEY ROAD AND TROON, EXIT GATE OF CLUBHOUSE AT ABV ADD, IF UNABLE TO
: MEET PLS CALL - CALLER STATES VIDEO WAVE TV NEED NOT RESPOND -
: CORRECT TKT TO: UPDATE TKT TO CHANGE MARKING INSTRUCTIONS SET MT TIME

Remarks: CALLER REQUESTS A MEETING ON 02/24/2004 AT 08:00 AM
: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO                          OH: N

Company     : HANGMAN VALLEY GOLF COURSE
Contact Name: MIKE BARBER                      Phone: (509)448-9737
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: HANGMAN VALLEY GOLF COURSE
Additional Members:
AVISTA07   HGMNWD01   INLND02    LOCINC31   SPENG01    SPENG02    VTV01
 ZZZZZZ99
IEUCC4076278-00	 509924	 2004/03/31 00:24:10  00016

/

  IEUCC
Ticket No:  4076278               =MEET TIME=
Send To: USWEST34   Seq No:   26  Map Ref:
Update Of:  4069218
Transmit      Date:  3/25/04   Time:  9:34 am    Op: jared
Original Call Date:  3/25/04   Time:  9:29 am    Op: jared
Work to Begin Date:  3/25/04   Time: 12:00 pm

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: SEVEN MILE RD
Nearest Intersecting Street: HWY 291

Twp: 26N   Rng: 42E   Sect-Qtr: 26
Twp: 26N   Rng: 42E   Sect-Qtr: 34-NE,28-NE,35-NW-NE,27,26-SW-NW-SE
Legal Given:

Type of Work: INSTALL TOWERS FOR POWER LINES-UPDATED NO MARKS
Location of Work: SITE IS APX 1.1 MI W OF ABV INTER MARK AROUND 5 TOWER
: LOCATIONS THAT ARE STAKED APX 1 1/4 MI W ON E SIDE OF HWY 291 TO INDIAN
: TRAILS ROAD. CALLER STATES SITE WILL BE A TRIANGULAR AREA MARKING ELE ROW
: IN THE AREA OF HWY 291 TO THE S, NINE MILE RD TO THE W, INDIAN TR RD TO

Remarks: THE EAST: MEET MARK AT ABV INTER 3-25-04 AT 12:00 NO GUARANTE
: OR CALL FOR SPECIFICS AT 503-704-7531/7 MILE / 9 MILE RD MERGE     OH: N

Company     : HENKELS AND MCCOY
Contact Name: MARK GILBERT                     Phone: (509)725-6500
Alt. Contact:                                  Phone: (503)704-7531
Contact Fax :
Work Being Done For: BONNEVILLE POWER AUTHORITY
Additional Members:
ATTCBL21   AVISTA08   CHEV05     LOCINC31   SPENG01    SPOKAN01   SPOKAN02
 SPOKAN03   XO01       ZZZZZZ99
IEUCC4076976-00	 509924	 2004/03/31 00:24:10  00018

/

  IEUCC
Ticket No:  4076976               =NON-EMERGENCY=
Send To: USWEST34   Seq No:   67  Map Ref:

Transmit      Date:  3/25/04   Time:  2:16 pm    Op: kelly
Original Call Date:  3/25/04   Time:  2:10 pm    Op: kelly
Work to Begin Date:  3/25/04   Time:  2:15 pm

State: WA            County: SPOKANE                 Place: SPOKANE VALLEY
Address: 523         Street: S DISHMAN MICA ROAD
Nearest Intersecting Street: 8TH

Twp: 25N   Rng: 44E   Sect-Qtr: 19-NE,20-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL FENCE?
Location of Work: ADD IS APX 3BLKS N OF ABV INTER, MARK ENTIRE S END OF
: PARKING LOT AT ABV ADD

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P== NO GUAR
: BEST INFORMATION AVAILABLE                                         OH: N

Company     : SPOKANE DISCOUNT
Contact Name: ROBERT                           Phone: (509)487-3386
Alt. Contact: STEVE WACO                       Phone:
Contact Fax :
Work Being Done For: SPOKANE DISCOUNT
Additional Members:
ATTCBL21   AVISTA09   ELCLT02    ESWD101    LOCINC31   MEW01      SPENG01
 SPENG02    SPENG03    SPKVLY01   SPWD01     TWT01      XO01       ZZZZZZ99IEUCC4077534-00	 509924	 2004/03/31 00:24:10  00019

/

  IEUCC
Ticket No:  4077534               =NON-EMERGENCY=   CANCELLATION
Send To: USWEST34   Seq No:    1  Map Ref:

Transmit      Date:  3/27/04   Time:  9:01 am    Op: mndebb
Original Call Date:  3/26/04   Time:  8:01 am    Op: julie
Work to Begin Date:  3/26/04   Time:  8:15 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 3423        Street: S GRAND
Nearest Intersecting Street: 35TH AVE

Twp: 25N   Rng: 43E   Sect-Qtr: 32-SW-NW-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: PLANT TREE
Location of Work: ADD APX 1 LOT N FROM INTER.MARK APX 3FT DIAMETER OF STAKE
: LOC IN FRONT YARD NEAR S PROP LINE AT ABV ADD.

Remarks: CANCEL PER NANCY, ALREADY HAS THE LINES MARKED
:                                                                    OH: N

Company     :
Contact Name: NANCY KLINGMAN                   Phone: (509)624-6285
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: NANCY KLINGMAN
Additional Members:
ATTCBL21   AVISTA07   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03   ZZZZZZ99
IEUCC4061330-00	 509924	 2004/03/31 00:24:10  00020

/

  IEUCC
Ticket No:  4061330               =NON-EMERGENCY=   CORRECTION
Send To: USWEST34   Seq No:   13  Map Ref:

Transmit      Date:  3/15/04   Time:  8:23 am    Op: crysta
Original Call Date:  3/15/04   Time:  7:45 am    Op: crysta
Work to Begin Date:  3/15/04   Time:  8:00 am

State: WA            County: SPOKANE                 Place: SPOKANE VALLEY
Address: 6715        Street: E 8TH
Nearest Intersecting Street: THIERMAN

Twp: 25N   Rng: 43E   Sect-Qtr: 24
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL SEWER SERV
Location of Work: SITE IS 1 LOT W OF ABV INTER. MARK FRONT 1/2 OF LOT AT
: ABV ADD. BEST INFO AVIAL. ==CORR TO CHANGE WORK FOR HOMEOWNER AND ADD
: BEST INFO===

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P==NO GUAR
: CENTER FOUND E 8TH AVE AND S THIERMAN ST                           OH: N

Company     : A ADVANCED PLUMBING
Contact Name: STEVEN JOHNSON                   Phone: (509)325-6111
Alt. Contact: CHARLES DELEON                   Phone:
Contact Fax :
Work Being Done For: HOMEOWNER
Additional Members:
ATTCBL21   AVISTA09   ESWD101    LOCINC31   SPENG01    SPENG02    SPKVLY01
 XO01       YELCN07    ZZZZZZ99
IEUCC4078393-00	 509924	 2004/03/31 00:24:10  00022

/

  IEUCC
Ticket No:  4078393               =NON-EMERGENCY=
Send To: USWEST34   Seq No:   59  Map Ref:
Update Of:  4071057
Transmit      Date:  3/26/04   Time:  1:55 pm    Op: adrian
Original Call Date:  3/26/04   Time:  1:44 pm    Op: adrian
Work to Begin Date:  3/26/04   Time:  1:45 pm

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 5504        Street: E 21ST AVE
Nearest Intersecting Street: GLENROSE

Twp: 25N   Rng: 43E   Sect-Qtr: 25-SW-NW,26
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: TRANSPLANTING TREES
Location of Work: ADD IS APX 500FT FROM INTER. MARK S SIDE OF PROP.

Remarks: ==CALLER REQUESTS AREA MARKED A.S.A.P==
: UPDTD: CLR STATES TEL/CATV NOT MARKED - TEL/CATV PLS RESPOND       OH: N

Company     :
Contact Name: BRYAN ELLSWORTH                  Phone: (509)535-2744
Alt. Contact: BRYAN CELL                       Phone: (509)939-9300
Contact Fax :
Work Being Done For: BRYAN ELLSWORTH
Additional Members:
ATTCBL21   AVISTA09   LOCINC31   SPENG01    SPENG02    SPKVLY01   SPOKAN02
 SPOKAN03   YELCN07    ZZZZZZ99
IEUCC4073382-00	 509924	 2004/03/31 00:24:10  00023

/

IEUCC
Ticket No:  4073382               PRE-SURVEY
Send To: USWEST34   Seq No:   94  Map Ref:

Transmit      Date:  3/23/04   Time:  4:33 pm    Op: bertha
Original Call Date:  3/23/04   Time:  3:25 pm    Op: michae
Work to Begin Date:  3/26/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: HAMILTON ST
Nearest Intersecting Street: SPRAGUE AVE

Twp: 26N   Rng: 44E   Sect-Qtr: 36-SW-SE
Twp: *MORE Rng: 46E   Sect-Qtr: 30-NW,19
Legal Given:

Type of Work: EXPANDING  RR OPERATIONS
Location of Work: SITE IS AT R.O.W. OF MAINLINE RR CORRIDOR OF BNSF RR.
: FROM N OF ABV INTER, APX 10-17 MI E TO IDAHO BORDER.

Remarks: CALLER REQUEST MAPS SENT HDR ENGINEERING 500 108TH AVE NE SUITE
: 1200 BELLUVUE WA 98004    ATTN : CHAD OXFORD                       OH: N

Company     : H D R ENGINEERING
Contact Name: CHAD OXFORD                      Phone: (425)453-1523
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: BNSF RR
Additional Members:
ATT05      ATTCBL21   AVISTA09   AVISTA11   BNSF01     CIRR01     ELCLT02
 EVSD01     FBRLNK01   INLND02    IRVIN01    LOCINC31   MEW01      MILLWD01
 MOAB01     ORCHRD01   PIONER01   SPENG01    SPENG02    SPENG03    SPIN01
 SPKVLY01   SPOKAN01   SPOKAN02   SPOKAN03   SPRINT02   SPUR01     SPWD01
 TMBLN01    TOCHAM02   TRENT01    TWT01      WLMSP06    WSDOT14    XO01
 YELCN07    ZZZZZZ99
IEUCC4073382-00	 509924	 2004/03/31 00:24:10  00026

/

IEUCC
Ticket No:  4073382               PRE-SURVEY        DUPLICATION
Send To: USWEST34   Seq No:   51  Map Ref:

Transmit      Date:  3/24/04   Time: 11:27 am    Op: bertha
Original Call Date:  3/23/04   Time:  3:25 pm    Op: michae
Work to Begin Date:  3/26/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address:             Street: HAMILTON ST
Nearest Intersecting Street: SPRAGUE AVE

Twp: 26N   Rng: 44E   Sect-Qtr: 36-SW-SE
Twp: *MORE Rng: 46E   Sect-Qtr: 30-NW,19
Legal Given:

Type of Work: EXPANDING  RR OPERATIONS
Location of Work: SITE IS AT R.O.W. OF MAINLINE RR CORRIDOR OF BNSF RR.
: FROM N OF ABV INTER, APX 10-17 MI E TO IDAHO BORDER.

Remarks: CALLER REQUEST MAPS SENT HDR ENGINEERING 500 108TH AVE NE SUITE
: 1200 BELLUVUE WA 98004    ATTN : CHAD OXFORD                       OH: N

Company     : H D R ENGINEERING
Contact Name: CHAD OXFORD                      Phone: (425)453-1523
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: BNSF RR
Additional Members:
ATT05      ATTCBL21   AVISTA09   AVISTA11   BNSF01     CIRR01     ELCLT02
 EVSD01     FBRLNK01   INLND02    IRVIN01    LOCINC31   MEW01      MILLWD01
 MOAB01     ORCHRD01   PIONER01   SPENG01    SPENG02    SPENG03    SPIN01
 SPKVLY01   SPOKAN01   SPOKAN02   SPOKAN03   SPRINT02   SPUR01     SPWD01
 TMBLN01    TOCHAM02   TRENT01    TWT01      WLMSP06    WSDOT14    XO01
 YELCN07    ZZZZZZ99IEUCC4074854-00	 509924	 2004/03/31 00:24:10  00028

/

IEUCC
Ticket No:  4074854               PRIORITY
Send To: USWEST34   Seq No:   55  Map Ref:

Transmit      Date:  3/24/04   Time: 11:59 am    Op: rose
Original Call Date:  3/24/04   Time: 11:44 am    Op: rose
Work to Begin Date:  3/26/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 1011        Street: E 30TH

Twp: 25N   Rng: 43E   Sect-Qtr: 28-SW,29-SE,32-NE,33-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: SEWER PIPE BURST
Location of Work: MARK ALONG RT MARKED IN WHITE FROM ABV ADD TO ADD 2915 S
: ARTHER ALONG ROW.    SITE IS BTWN 30TH AND 29TH THEN BTWN ARTHUR AND
: PERRY

Remarks: CALLER REQUESTS MARKS DONE BY FRIDAY 3/26/04 MORNING SO THEY MAY
: START WORK THAT MORNING    MADE NO GUARANTEES                      OH: Y

Company     : MR ROOTER PLUMBING
Contact Name: RICK MASHTARE                    Phone: (208)772-3091
Alt. Contact: SCOTT                            Phone: (208)929-2396
Contact Fax :
Work Being Done For: WILFORD EDWARD
Additional Members:
ATTCBL21   AVISTA07   AVISTA09   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03
 ZZZZZZ99
IEUCC4061914-00	 509924	 2004/03/31 00:24:10  00032

/

IEUCC
Ticket No:  4061914               PRIORITY
Send To: USWEST34   Seq No:   40  Map Ref:
Update Of:  4050696
Transmit      Date:  3/15/04   Time: 10:24 am    Op: kevin
Original Call Date:  3/15/04   Time: 10:19 am    Op: kevin
Work to Begin Date:  3/16/04   Time:  7:30 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 1621        Street: W PINECREST
Nearest Intersecting Street: MARC DR

Twp: 26N   Rng: 43E   Sect-Qtr: 7-SW
Twp: 26N   Rng: 42E   Sect-Qtr: 12-SE
Legal Given:

Type of Work: BUILD NEW HOUSE
Location of Work: ADD IS APX 500FT FROM INTER, MARK ENTIRE PROP AT ABV ADD
: INCL FROM AVISTA TRANS (LOC 2 LOTS W FROM ABV ADD) PAST ABV ADD TO FAR
: SIDE OF NEXT LOT E.

Remarks: = 2ND UPDATE TO ADD/CHANGE MARKING INSTRUCTION PLEASE REMARK=
: CREW WILL BE ON SITE ON 03/16/2004 AT 07:30 AM NO GUAR             OH: N

Company     :
Contact Name: PAUL JOHNSON                     Phone: (509)362-2688
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: PAUL JOHNSON
Additional Members:
ATTCBL21   AVISTA08   LOCINC31   SPENG01    SPENG02    WHIT01     WLMSP06
 ZZZZZZ99
IEUCC4060841-00	 509924	 2004/03/31 00:24:10  00033

/

IEUCC
Ticket No:  4060841               2 FULL BUSINESS
Send To: USWEST34   Seq No:   54  Map Ref:

Transmit      Date:  3/12/04   Time:  3:36 pm    Op: kevin
Original Call Date:  3/12/04   Time:  3:30 pm    Op: kevin
Work to Begin Date:  3/17/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 2607        Street: S CORBIN CIR
Nearest Intersecting Street: GALAXY DR

Twp: 25N   Rng: 45E   Sect-Qtr: 20-SW
Twp: 25N   Rng: 45E   Sect-Qtr: 20-SW
Legal Given:

Type of Work: INSTALL UTILITY
Location of Work: ADD IS APX 300FT S OF ABV INTER ON E SIDE OF ROAD. MARK
: ENTIRE PROP

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
:                                                                    OH: N

Company     : NORTHWEST EXCAVATORS, INC.
Contact Name: DAN CLAWSON                      Phone: (509)466-7574
Alt. Contact: DAN CLAWSON - CELL               Phone: (509)217-5511
Contact Fax : (509)466-7574
Work Being Done For: NORTHWEST EXCAVATORS, INC.
Additional Members:
ATTCBL21   AVISTA11   CIRR01     LOCINC31   PGETNW01   SPENG01    SPENG02
 SPKVLY01   VERA01     ZZZZZZ99
IEUCC4059921-00	 509924	 2004/03/31 00:24:10  00034

/

IEUCC
Ticket No:  4059921               2 FULL BUSINESS   CANCELLATION
Send To: USWEST34   Seq No:   16  Map Ref:

Transmit      Date:  3/15/04   Time:  8:32 am    Op: bobby
Original Call Date:  3/12/04   Time:  8:24 am    Op: bobby
Work to Begin Date:  3/17/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: MEAD
Address:             Street: MARKET ST
Nearest Intersecting Street: GRACE AVE

Twp: 26N   Rng: 43E   Sect-Qtr: 10-SW
Twp: 25N   Rng: 43E   Sect-Qtr: 10-NW
Legal Given:

Type of Work: INSTALL SIGN - NO COMPRESSION BRAKES
Location of Work: 30FT W OF ABV INTER, MARK N SIDE OF ROAD IN WHITE PAINT

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO
: CANCEL TICKET - PREVIOUS TICKET WAS CORRECT                        OH: ?

Company     : SPOKANE COUNTY ENGINEERS
Contact Name: BRENDA COLLINS                   Phone: (509)477-3600
Alt. Contact: JOHN BURKS                       Phone:
Contact Fax : (509)477-7655
Work Being Done For: SPOKANE COUNTY
Additional Members:
ATTCBL21   AVISTA09   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03   ZSPKG05
 ZZZZZZ99
IEUCC4060537-00	 509924	 2004/03/31 00:24:10  00035

/

IEUCC
Ticket No:  4060537               2 FULL BUSINESS   CORRECTION
Send To: USWEST34   Seq No:   72  Map Ref:
Update Of:  4059683
Transmit      Date:  3/12/04   Time:  5:52 pm    Op: tesa
Original Call Date:  3/12/04   Time:  1:24 pm    Op: kevin
Work to Begin Date:  3/17/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 4818        Street: W HOFFMAN PL
Nearest Intersecting Street: ROYAL CT

Twp: 26N   Rng: 42E   Sect-Qtr: 34-SW-SE
Twp: 25N   Rng: 42E   Sect-Qtr: 3-NW-NE
Legal Given:

Type of Work: INSTALL FENCE
Location of Work: ADD IS BTWN ABV INTER AND HARTLEY.  MARK FROM FENCE LINE
: ON N SIDE OF PROP AT ABV ADD TO ROAD, TO INCLUDE DRIVEWAY AT ABV ADD UP
: TO EACH SIDE OF PROP LI

Remarks: ==UPDT TO CHANGE MARKING INSTRUCTIONS.==  CORR-ADD REMARK
: CALLER STATES AREA TO BE MARKED IS ON SIDE OF FENCE BORDERING WELLEOH: N

Company     :
Contact Name: JANET BLUMER                     Phone: (509)325-1949
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: JANET BLUMER
Additional Members:
ATTCBL21   AVISTA08   AVISTA10   DAVISC01   LOCINC31   SPENG01    SPOKAN01
 SPOKAN02   SPOKAN03   ZZZZZZ99
IEUCC4064738-00	 509924	 2004/03/31 00:24:10  00036

/

IEUCC
Ticket No:  4064738               2 FULL BUSINESS   DUPLICATION
Send To: USWEST34   Seq No:  103  Map Ref:

Transmit      Date:  3/16/04   Time:  4:35 pm    Op: bobby
Original Call Date:  3/16/04   Time:  3:23 pm    Op: bobby
Work to Begin Date:  3/19/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 3029        Street: E BOONE AVE
Nearest Intersecting Street: FISKE

Twp: 25N   Rng: 43E   Sect-Qtr: 15-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: UNDERGROUND SPRINKLERS AND LANDSCAPING
Location of Work: NW CORNER OF ABV INTER, MARK ENTIRE YARD

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 25 43 15-NE
:                                                                    OH: N

Company     : DEW DROP SPRINKLERS/LANDSCAPE
Contact Name: DOUG                             Phone: (509)922-7168
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: JIM ELMER
Additional Members:
ATTCBL21   AVISTA09   BNSF01     LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03
 XO01       ZDEWD00    ZZZZZZ99
IEUCC4079948-00	 509924	 2004/03/31 00:24:10  00037

/

IEUCC
Ticket No:  4079948               2 FULL BUSINESS
Send To: USWEST34   Seq No:   51  Map Ref:
Update Of:  4007926
Transmit      Date:  3/29/04   Time: 11:15 am    Op: paulw
Original Call Date:  3/29/04   Time: 11:12 am    Op: paulw
Work to Begin Date:  4/01/04   Time: 12:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 11801       Street: E MISSION AVE
Nearest Intersecting Street: N PINES ROAD

Twp: 25N   Rng: 44E   Sect-Qtr: 9-SW-SE,16-NW-NE
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL SEWER SVC
Location of Work: ADD IS APX 4-5BLKS W FROM ABV INTER. MARK ENTIRE PROP AT
: ABV ADD.

Remarks: 2NDUPDT TICKET -- LOCATES HAVE EXPIRED -- NEEDS REMARKS
:                                                                    OH: N

Company     : ACME EXCAVATING
Contact Name: MIKE ORRINO                      Phone: (509)251-8058
Alt. Contact: HOME                             Phone: (509)228-0691
Contact Fax :
Work Being Done For: DARREN NELSON
Additional Members:
ATTCBL21   AVISTA09   INLND02    LOCINC31   MEW01      SPENG01    SPENG02
 SPENG03    SPKVLY01   TWT01      XO01       ZZZZZZ99
IEUCC4079720-00	 509924	 2004/03/31 00:24:10  00038

/

IEUCC
Ticket No:  4079720               SHORT NOTICE
Send To: USWEST34   Seq No:   39  Map Ref:

Transmit      Date:  3/29/04   Time: 10:25 am    Op: elane
Original Call Date:  3/29/04   Time: 10:17 am    Op: elane
Work to Begin Date:  3/31/04   Time:  9:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 6200        Street: E JAMIESON ROAD
Nearest Intersecting Street: BEN BURR

Twp: 24N   Rng: 43E   Sect-Qtr: 11-SE-NE,12-SW-NW
Twp:       Rng:       Sect-Qtr:
Legal Given:

Type of Work: INSTALL TREES
Location of Work: ADD APX 1/2-3/4MI E OF ABV INTER. MARK ENTIRE FRONT,SW
: AND W SIDE OF HOUSE AND ALONG S SIDE OF HOUSE AT ABV ADD.

Remarks: CALLER REQUESTS MARKS BY 03/31/2004 BY 09:00 AM NO GUARANTEES
:                                                                    OH: N

Company     :
Contact Name: LINDA WARNER BROWN               Phone: (509)448-3442
Alt. Contact:                                  Phone:
Contact Fax :
Work Being Done For: LINDA WARNER BROWN
Additional Members:
AVISTA07   INLND02    LOCINC31   SPENG01    ZZZZZZ99
IEUCC4052653-00	 509924	 2004/03/31 00:24:10  00042

/

IEUCC
Ticket No:  4052653               SHORT NOTICE
Send To: USWEST34   Seq No:   52  Map Ref:
Update Of:  4021568
Transmit      Date:  3/05/04   Time:  2:18 pm    Op: judy
Original Call Date:  3/05/04   Time:  2:14 pm    Op: judy
Work to Begin Date:  3/08/04   Time: 10:00 am

State: WA            County: SPOKANE                 Place: SPOKANE
Address: 4500        Street: N A ST
Nearest Intersecting Street: PRINCETON ST

Twp: 25N   Rng: 42E   Sect-Qtr: 2
Twp:  *MORERng: 42E   Sect-Qtr: 36-SW,35-SE
Legal Given:

Type of Work: INSTALL AN ANCHOR
Location of Work: ADD IS E OF ABV INTER, MARK 25FT RADIUS OF POLE ON E SIDE
: OF A STREET WHERE PRINCETON INTERSECTS.  CALLER GAVE LEGALS.

Remarks: UPDT TCKT- LOCATES HAVE EXPIRED - REQ REMARKS 3/8/04 BY 10:00 A.M.
: CREW WILL BE ON SITE ON 03/08/2004 AT 10:00 AM                     OH: Y

Company     : AVISTA UTILITIES
Contact Name: KATY                             Phone: (509)495-4152
Alt. Contact:                                  Phone:
Contact Fax : (509)534-2463
Work Being Done For: AVISTA  JOB 50141175
Additional Members:
ATTCBL21   AVISTA10   LOCINC31   SPOKAN01   SPOKAN02   SPOKAN03   ZAVST05
 ZZZZZZ99

