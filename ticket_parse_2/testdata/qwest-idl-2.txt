IDL2004050300368        208743  2004/05/03 15:25:00     00870


Idaho Dig Line
NOTICE OF INTENT TO EXCAVATE                   Header Code: STANDARD LOCATE
Ticket No:    2004190426 Seq. No: 18
Update of:
Original Call Date:     05/03/2004     Time:      02:16:20 PM  OP: 186
Transmit Date:          05/03/2004     Time:      02:17:44 PM
Work to Begin Date:     05/05/2004     Time:      01:16:00 PM

Company:           WILLIAMS PLUMBING
Contact Name:      LOWELL WILLIAMS            Contact Phone:   (208)983-2005
Alternate Contact:                            Alternate Phone:
Best Time to Call:                            Fax No:
Cell Phone:        (208)983-6159              Pager No:

State: ID       County: IDAHO           City: GRANGEVILLE
Address:    202   , N A ST
To Address:
Nearest Intersecting Street: W NORTH ST
2nd Intersecting Street:
Subdivision:
Town: 30N  Ran: 03E  Sect 1/4: 19 NE       Town: 30N  Ran: 03E  Sect 1/4: 18 SE
Location of Work: LOC ON NE CORNER OF BUILDING AREA MARKED IN GREEN




Remarks:


Type of Work: REPLACE SEWER LINE
Private Property: Y    Street:                Overhead Lines:     Blasting:
Easement:              Mechanical Boring:     Premarked:      Y
Work Being Done For: SYRINGA GENERAL HOSPITAL
Sending to: (listing of utilities tkt sent to)
*USWLEW         AVISTA25        USAMED25        QLNID25


                                 FOR MEMBER USE ONLY
Located by____________________________________Date of Location________________
Remarks:______________________________________________________________________
______________________________________________________________________________
______________________________________________________________________________
Excavator Notified (Not located)__________________ Who Notified_______________
Notified by:__________________________________Date:______________Time:________


