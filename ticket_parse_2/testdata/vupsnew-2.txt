TEST05 00078 VUPSa 01/23/04 13:25:41 A402000792-00A          RETRANSMIT

Ticket No:  A402000792-00A                        NEW  GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:25 PM   Op: 1MLF
Call            Date: 01/20/04   Time: 09:23 AM
Requested By    Date: 01/23/04   Time: 07:00 AM
Update By       Date: 02/06/04   Time: 12:00 AM
Due By          Date: 01/23/04   Time: 07:00 AM
Expires         Date: 02/11/04   Time: 07:00 AM
Old Tkt No: A402000792
Original Call   Date: 01/20/04   Time: 09:23 AM   Op: 1MLF

City/Co:NORTHAMPTON           Place:CAPE CHARLES                        State:VA
Subdivision: BAY CREEK
Address:     8                Street: BURFORD COURT
Cross 1:     BLUE HEAVEN                                       Intersection: N

Type of Work:   PLANTING TREES
Work Done For:  BAYMARK CONST
Location:       MARK FROM THE CURB IN FRONT OF ABOVE ADDRESS TO 10FT INTO
                PROPERTY
Instructions:   CALLER MAP REF 3715A7559A
                THIS IS A NEW STREET GRIDED WITH CALLERS GRID.

Whitelined: N   Blasting: N   Boring: N

Company:        TREE TRANSPLANTERS OF VA                      Type: CONT
Co. Address:    600 CARTER ROAD
City:           CHESAPEAKE  State:VA  Zip:23322
Company Phone:  757-482-8183
Contact Name:   CONNIE WILKINS #            Contact Phone:757-377-3396
Call Back Time: 9AM-5PM                Contact Fax  :757-481-2257
Field Contact:  JACK WILKINS

Mapbook:
Grids:    3715A7559A     3715B7559A

Members:
CCH901 = TOWN OF CAPE CHARLES         DPE911 = CONECTIV POWER DELIVERY
VZN905 = VERIZON

Seq No:   78 A

[Originally sent as sequence number 00001 at 01/23/04 07:00]

TEST05 00079 VUPSa 01/23/04 13:25:42 A402000809-00A          RETRANSMIT

Ticket No:  A402000809-00A                        NEW  GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:25 PM   Op: 1MLF
Call            Date: 01/20/04   Time: 09:28 AM
Requested By    Date: 01/23/04   Time: 07:00 AM
Update By       Date: 02/06/04   Time: 12:00 AM
Due By          Date: 01/23/04   Time: 07:00 AM
Expires         Date: 02/11/04   Time: 07:00 AM
Old Tkt No: A402000809
Original Call   Date: 01/20/04   Time: 09:28 AM   Op: 1MLF

City/Co:NORTHAMPTON           Place:CAPE CHARLES                        State:VA
Subdivision: BAY CREEK
Address:     12               Street: BURFORD COURT
Cross 1:     BLUE HEAVEN                                       Intersection: N

Type of Work:   PLANTING TREES
Work Done For:  BAYMARK CONST.
Location:       MARK FROM THE CURB IN FRONT OF ABOVE ADDRESS TO 10FT INTO
                PROPERTY.
Instructions:   CALLER MAP REF 3715A7559A
                THIS IS A NEW STREET GRIDED WITH CALLERS GRID.

Whitelined: N   Blasting: N   Boring: N

Company:        TREE TRANSPLANTERS OF VA                      Type: CONT
Co. Address:    600 CARTER ROAD
City:           CHESAPEAKE  State:VA  Zip:23322
Company Phone:  757-482-8183
Contact Name:   CONNIE WILKINS #            Contact Phone:757-377-3396
Call Back Time: 9AM-5PM                Contact Fax  :757-481-2257
Field Contact:  JACK WILKINS

Mapbook:
Grids:    3715A7559A     3715B7559A

Members:
CCH901 = TOWN OF CAPE CHARLES         DPE911 = CONECTIV POWER DELIVERY
VZN905 = VERIZON

Seq No:   79 A

[Originally sent as sequence number 00002 at 01/23/04 07:00]

TEST05 00080 VUPSa 01/23/04 13:25:42 A402000811-00A          RETRANSMIT

Ticket No:  A402000811-00A                        NEW  GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:25 PM   Op: 1MLF
Call            Date: 01/20/04   Time: 09:29 AM
Requested By    Date: 01/23/04   Time: 07:00 AM
Update By       Date: 02/06/04   Time: 12:00 AM
Due By          Date: 01/23/04   Time: 07:00 AM
Expires         Date: 02/11/04   Time: 07:00 AM
Old Tkt No: A402000811
Original Call   Date: 01/20/04   Time: 09:29 AM   Op: 1MLF

City/Co:NORTHAMPTON           Place:CAPE CHARLES                        State:VA
Subdivision: BAY CREEK
Address:     14               Street: BURFORD COURT
Cross 1:     BLUE HEAVEN                                       Intersection: N

Type of Work:   PLANTING TREES
Work Done For:  BAYMARK CONST
Location:       MARK FROM THE CURB IN FRONT OF ABOVE ADDRESS TO 10FT INTO
                PROPERTY.
Instructions:   CALLER MAP REF 3715A7559A
                THIS IS A NEW STREET GRIDED WITH CALLERS GRID.

Whitelined: N   Blasting: N   Boring: N

Company:        TREE TRANSPLANTERS OF VA                      Type: CONT
Co. Address:    600 CARTER ROAD
City:           CHESAPEAKE  State:VA  Zip:23322
Company Phone:  757-482-8183
Contact Name:   CONNIE WILKINS #            Contact Phone:757-377-3396
Call Back Time: 9AM-5PM                Contact Fax  :757-481-2257
Field Contact:  JACK WILKINS

Mapbook:
Grids:    3715A7559A     3715B7559A

Members:
CCH901 = TOWN OF CAPE CHARLES         DPE911 = CONECTIV POWER DELIVERY
VZN905 = VERIZON

Seq No:   80 A

[Originally sent as sequence number 00003 at 01/23/04 07:00]

TEST05 00086 VUPSa 01/23/04 13:42:34 A402300684-00A          RETRANSMIT

Ticket No:  A402300684-00A                        NEW  GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:42 PM   Op: 1BKT
Call            Date: 01/23/04   Time: 09:49 AM
Requested By    Date: 01/28/04   Time: 07:00 AM
Update By       Date: 02/11/04   Time: 12:00 AM
Due By          Date: 01/28/04   Time: 07:00 AM
Expires         Date: 02/17/04   Time: 07:00 AM
Old Tkt No: A402300684
Original Call   Date: 01/23/04   Time: 09:49 AM   Op: 1BKT

City/Co:KING AND QUEEN        Place:SHACKLEFORDS                        State:VA
Address:     1379             Street: CENTERVILLE RD
Cross 1:     THE TRAIL                                         Intersection: N

Type of Work:   REPLACING SEWER SERVICE LINE
Work Done For:  HO/WILLIAMS
Location:       MARK REAR OF PROPERTY
Instructions:   CALLER MAP REF (NONE)

Whitelined: N   Blasting: N   Boring: N

Company:        BRUMFIELD PLUMBING                            Type: CONT
Co. Address:    6317 ARK RD
City:           GLOUCESTER  State:VA  Zip:23061
Company Phone:  804-694-0116
Contact Name:   KIM WILLIAMS #              Contact Phone:804-785-3303
Call Back Time: 9AM-5PM
Field Contact:  NORMAN BRUMFIELD
Fld. Contact Phone:804-694-0116

Mapbook:
Grids:    3732A7644C     3732A7644D     3733D7644C     3733D7644D

Members:
COX468 = COX COMMUNICATIONS           DMC040 = DOMINION VIRGINIA POWER
GTWA48 = VERIZON-WARSAW

Seq No:   86 A

[Originally sent as sequence number 00003 at 01/23/04 10:00]

TEST05 00085 VUPSa 01/23/04 13:42:33 A402300430-00A          RETRANSMIT

Ticket No:  A402300430-00A                        NEW  GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:42 PM   Op: 1BKT
Call            Date: 01/23/04   Time: 09:00 AM
Requested By    Date: 01/28/04   Time: 07:00 AM
Update By       Date: 02/11/04   Time: 12:00 AM
Due By          Date: 01/28/04   Time: 07:00 AM
Expires         Date: 02/17/04   Time: 07:00 AM
Old Tkt No: A402300430
Original Call   Date: 01/23/04   Time: 09:00 AM   Op: 1BKT

City/Co:MATHEWS               Place:                                    State:VA
Address:     147              Street: TWIGG FERRY RD
Cross 1:     STATE HWY 198                                     Intersection: N

Type of Work:   TRENCHING FOR UNDERGROUND ELECTRIC SERVICE LINE
Work Done For:  HO/HUDGINS/804-725-5364
Location:       MARK FROM THE NEW GARAGE OUT TO THE GUEST BUNGELOW TOWARDS THE
                MAIN HOUSE.
Instructions:   CALLER MAP REF (NONE)

Whitelined: N   Blasting: N   Boring: N

Company:        BAY CONSTRUCTION                              Type: CONT
Co. Address:    PO BOX 1317
City:           MATHEWS COUNTY  State:VA  Zip:23109
Company Phone:  804-725-9893
Contact Name:   CHARLY CARTER #             Contact Phone:804-695-4589
Call Back Time: 8AM-6PM                Contact Fax  :804-725-7763

Mapbook:  30C10
Grids:    3730C7625C     3730C7625D

Members:
DMC040 = DOMINION VIRGINIA POWER      GTGL49 = VERIZON-GLOUCESTER

Seq No:   85 A

[Originally sent as sequence number 00002 at 01/23/04 09:12]

TEST05 00084 VUPSa 01/23/04 13:42:32 A402300304-00A          RETRANSMIT

Ticket No:  A402300304-00A                        UPDT GRID NORM LREQ RSND
Transmit        Date: 01/23/04   Time: 01:42 PM   Op: WLLIPSCOMB
Call            Date: 01/23/04   Time: 08:41 AM
Requested By    Date: 01/28/04   Time: 07:00 AM
Update By       Date: 02/11/04   Time: 12:00 AM
Due By          Date: 01/28/04   Time: 07:00 AM
Expires         Date: 02/17/04   Time: 07:00 AM
Old Tkt No: A400500447
Original Call   Date: 12/11/03   Time: 07:31 AM   Op: 1JGP

City/Co:KING WILLIAM          Place:WEST POINT                          State:VA
Address:                      Street: 7TH ST
Cross 1:     LEE ST                                            Intersection: N
Cross 2:     MATTAPONI RIVER

Type of Work:   CLEANING UP TREES, STUMPS & BRICKS/OFFICE RECONSTRUCTION/REP
Work Done For:  MASSEY OIL
Location:       LOCATE THE ENTIRE FENCED IN AREA, FENCED AREA IS AT THE DEAD END
                OF 7TH STREET
Instructions:   CALLER MAP REF (NONE)
                <<<<< UPDATE OF TICKET #A400500447 >>>>>

Whitelined: N   Blasting: N   Boring: N

Company:        J. SANDERS CONSTRUCTION                       Type: CONT
Co. Address:    3240 KING WILLIAM AVE
City:           WEST POINT  State:VA  Zip:23181
Company Phone:  804-843-4700
Contact Name:   LAURIE LIPSCOMB #           Contact Phone:804-843-4700
Call Back Time: 7AM-5PM                Contact Fax  :804-843-4251

Mapbook:
Grids:    3731A7647B-01  3731A7647B-02  3731A7647B-03  3731A7647B-11
Grids:    3731A7647B-12  3731A7647B-13  3732D7647B-41  3732D7647B-42
Grids:    3732D7647B-43

Members:
BAR283 = VERIZON COMM.-UTILIQUEST     COX469 = COX COMMUNICATIONS
DMC040 = DOMINION VIRGINIA POWER      HRSD89 = HAMPTON ROADS SANITATION

Seq No:   84 A

[Originally sent as sequence number 00001 at 01/23/04 08:41]


UTIL12 00072 VUPSb 01/28/04 12:58:13 B402800328-00B          NORMAL

Ticket No:  B402800328-00B                        NEW  GRID NORM LREQ
Transmit        Date: 01/28/04   Time: 12:58 PM   Op: 1KDV
Call            Date: 01/28/04   Time: 12:55 PM
Requested By    Date: 02/02/04   Time: 07:00 AM
Update By       Date: 02/17/04   Time: 12:00 AM
Due By          Date: 02/02/04   Time: 07:00 AM
Expires         Date: 02/20/04   Time: 07:00 AM
Old Tkt No: B402800328
Original Call   Date: 01/28/04   Time: 12:55 PM   Op: 1KDV

City/Co:CHESAPEAKE CITY       Place:                                    State:VA
Address:     416              Street: REDBRICK DR
Cross 1:     LLOYD DR                                          Intersection: N

Type of Work:   INSTALL NEW GAS SERVICES
Work Done For:  VA NATURAL GAS
Location:       LOCATE ENTIRE PROPERTY
Instructions:   CALLER MAP REF 247A3

Whitelined: N   Blasting: N   Boring: N

Company:        ROSS & SONS UTILITY CONTRACTOR                Type: CONT
Co. Address:    913 VENTURES WAY
City:           CHESAPEAKE  State:VA  Zip:23320
Company Phone:  757-436-2018
Contact Name:   ELIZABETH ROSS #            Contact Phone:757-436-2018
Call Back Time: 9AM-5PM
Field Contact:  BOBBY ROSS
Fld. Contact Phone:757-436-2018

Mapbook:  247A3
Grids:    3647A7614A-03  3647A7614A-04  3647A7614B-00  3647A7614B-10

Members:
CICP46 = CITY OF CHESAPEAKE           CINW39 = CITY OF NORFOLK - WATER
CPNO36 = VERIZON-NORFOLK              CXCA38 = COX COMMUNICATIONS-HAMPTON R
DME200 = DOMINION VIRGINIA POWER      VNG12D = VIRGINIA NATURAL GAS

Seq No:   72 B



UTIL16 01036 VUPSa 03/15/04 22:05:40 A407504236-00A          NORMAL

Ticket No:  A407504236-00A                        NEW  CNTY NORM LREQ
Transmit        Date: 03/15/04   Time: 10:05 PM   Op: 1BSS
Call            Date: 03/15/04   Time: 09:59 PM
Requested By    Date: 03/18/04   Time: 07:00 AM
Update By       Date: 04/01/04   Time: 12:00 AM
Due By          Date: 03/18/04   Time: 07:00 AM
Expires         Date: 04/06/04   Time: 07:00 AM
Old Tkt No: A407504236
Original Call   Date: 03/15/04   Time: 09:59 PM   Op: 1BSS

City/Co:FAIRFAX               Place:                                    State:VA
Address:     7451             Street: CROSS GATE LN
Cross 1:     KINGSTOWNE PKY                                    Intersection: N

Type of Work:   INSTALL FENCE
Work Done For:  WESTON
Location:       MARK LEFT SIDE OF PROPERTY
Instructions:   CALLER MAP REF 29C1

Whitelined: N   Blasting: N   Boring: N

Company:        ACCOKEEK FENCE                                Type: CONT
Co. Address:    5410 VINE ST
City:           ALEXANDRIA  State:VA  Zip:22310
Company Phone:  703-971-0660
Contact Name:   ANN RYAN #                  Contact Phone:703-971-0660
Call Back Time: 7AM-4PM                Contact Fax  :703-971-0662

Mapbook:
Grids:

Members:
ATT392 = AT&T - ATLANTA               ATT901 = AT & T
CGC901 = COGENT COMMUNICATIONS, INC   CGT901 = COLUMBIA GAS TRANSMISSION
CGT908 = COLUMBIA GAS TRANSMISSION    CGV903 = COLUMBIA GAS OF VIRGINIA
CMC901 = COMCAST CABLE                CMC902 = COMCAST CABLE
CMC904 = COMCAST CABLE                COX901 = COX COMMUNICATIONS
COX902 = COX COMMUNICATIONS           CPL901 = COLONIAL PIPELINE
CVL901 = CAVALIER TELEPHONE, LLC      DMT901 = DOMINION TRANSMISSION CORP
DMT902 = DOMINION TRANSMISSION CORP   DOM400 = DOMINION VIRGINIA POWER
DTI901 = DOMINION TELECOM, INC        EMP231 = CITY OF EMPORIA
FAXCFM = FAX CONFIRM QUEUE            FCH901 = CITY OF FALLS CHURCH
FCU901 = FAIRFAX COUNTY DPW           FCU902 = FAIRFAX COUNTY DIT/TID
FCW902 = FAIRFAX COUNTY WATER AUTHORI FFX901 = CITY OF FAIRFAX
FSU496 = FIRST SOUTH UTILITY - AT&T V HRN901 = TOWN OF HERNDON
IAD902 = DULLES AIRPORT               LGN903 = LOOKING GLASS NETWORKS OF VA
LTC903 = LEVEL 3 COMMUNICATION        MCII81 = MCI
MCS901 = MICHIGAN COGENERATION SYSTEM MFN902 = MFN OF VA, LLC
MIT901 = THE MITRE CORPORATION        NVE901 = NORTHERN VIRGINIA ELEC COOP
OFC901 = ONFIBER COMMUNICATIONS, INC  PFN901 = P. F. NET AT&T VIRGINIA
PPR901 = PLANTATION PIPELINE COMPANY  QGS901 = QWEST GOVERNMENT SERVICES
QGS906 = QWEST GOVERNMENT SERVICES    QGS907 = QWEST GOVERNMENT SERVICES
QWES05 = QWEST COMMUNICATIONS         SLT132 = SPRINT GSD - FORT BELVOIR
STC333 = SHENANDOAH TELECOMMUNICATION TCP902 = WILLIAMS GAS PIPELINE/TRANSC
UOS901 = UPPER OCCOQUAN SEWAGE AUTH   USSP22 = SPRINT - LONG DISTANCE
VNN901 = TOWN OF VIENNA               VZN902 = VERIZON
VZN906 = VERIZON                      VZN907 = VERIZON
WCM103 = WILTEL COMMUNICATIONS, LLC   WGL904 = WASHINGTON GAS
XOC902 = XO COMMUNICATIONS, INC       XSP902 = XSPEDIUS COMMUNICATIONS
YPS902 = YIPES ENTERPRISES SERVICES,I

Seq No:   1036 A



