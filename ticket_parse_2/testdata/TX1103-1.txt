SEQUENCE NUMBER 0001   CDC = VTG
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows :
XSPEDIUS MNG CO  AT&T             EL PASO ELEC CO  EL PASO WATER
LEVEL 3 COMM     MCI              QWEST COMM.      TX GAS-EL PASO
SPRINT           VALLEY TELECOMM  SM&P-W           LONE STAR XCHG

Locate Request No. 052100991

Prepared By DANIEL OLSON         On 29-JUL-05  At 0940

MapRef :                            Grid: 314630106300A  Footprint: D01

Location:     County: EL PASO  Town: EL PASO

             Address: 3333 SUN BOWL DR

Beginning Work Date 08/02/05 Time of Day: 09:45 am   Duration: 14 DAYS

Fax-A-Locate Date          at

Excavation Type : ELECTRIC
Nature of Work  : INSTL ELECT LINE

Blasting ? NO           48 Hr Notice ? YES
White Line ? NO         Digging Deeper Than 16 Inches ? YES

Person Calling : EMILY CHACON
Company Name   : AC ELECTRIC
Work by AC ELECTRIC      For HIGH DESERT COMMUNICATIONS

Person to Contact : ALBERT CHACON

Phone No.  ( 915 )857-0267 /( 915 )474-2457 ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 915 )857-8639
Email:     ACELECTRIC123@ELP.RR.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: MESA
WORK WILL BE INSIDE THE FENCED IN AREA.






ACELECTRIC123@ELP.RR.COM

Map Cross Reference : MAPSCO 734,H

FaxBack Requested ? YES    Lone Star Xref:


052100991 to 917706401859 at 10:14:06 on FRI, 07/29/05 for VTG #0001


SEQUENCE NUMBER 0001   CDC = VTG
Texas Excavation Safety System
*  NO RESPONSE  * MESSAGES Sent to  Office(s) as follows :
VALLEY TELECOMM  LONE STAR XCHG

Locate Request No. 052090393

Prepared By JOSEPH SALINAS       On 28-JUL-05  At 0816

MapRef :                            Grid: 314830106340A  Footprint: D02

Location:     County: EL PASO  Town: EL PASO

             Address: 0 FRONTERA RD

Beginning Work Date 07/28/05 Time of Day: 08:30 am   Duration: 30 DAYS

Fax-A-Locate Date          at

Excavation Type : SEWER
Nature of Work  : SEWER INSTALLATION

Blasting ? NO           48 Hr Notice ? NO
White Line ? NO         Digging Deeper Than 16 Inches ? YES

Person Calling : KEVIN MONTES
Company Name   : SKYLINE ENGINEERING
Work by SKYLINE ENGINEERING      For SUNWEST DEVELOPMENT

Person to Contact : KEVIN MONTES

Phone No.  ( 915 )490-4772 /( 505 )589-5481 ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 520 )826-1082
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY -->NoResponseTo 052073129
Near Intersection: BOY SCOUT LN
NO RESPONSE-051993167 - WORK WILL BE DONE ON BOTH SIDES OF FRONTERA RD
 AT BOY SCOUT LN.





CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 703,S

FaxBack Requested ? YES    Lone Star Xref:


052090393 to 917706401859 at 13:58:17 on THU, 07/28/05 for VTG #0001
SEQUENCE NUMBER 0002   CDC = VTG
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows :
XSPEDIUS MNG CO  AT&T             EL PASO ELEC CO  EL PASO WATER
MCI              QWEST COMM.      TX GAS-EL PASO   SPRINT
VALLEY TELECOMM  SM&P-W           LONE STAR XCHG

Locate Request No. 052090575

Prepared By HERBERT DAVIS        On 28-JUL-05  At 0838

MapRef :                            Grid: 314900106310A  Footprint: D02

Location:     County: EL PASO  Town: EL PASO

             Address: 135 S MESA HILLS DR

Beginning Work Date 08/01/05 Time of Day: 08:45 am   Duration: 03 DAYS

Fax-A-Locate Date          at

Excavation Type : ELECTRIC
Nature of Work  : TRENCHING AND REPL ELECTRIAL C

Blasting ? NO           48 Hr Notice ? YES
White Line ? NO         Digging Deeper Than 16 Inches ? YES

Person Calling : ANDY BISHOP
Company Name   : DESERT SAND UTILITIES
Work by DESERT SAND UTILITIES      For EL PASO ELEC

Person to Contact : ANDY BISHOP

Phone No.  ( 915 )726-0282 /( 915 )921-6459 ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 915 )921-6462
Email:     CALLER DID NOT PROVIDE EMAIL

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: MESA
WORKING ON ENTIRE PROPERTY TO INCLUDE THE FRONTAGE OF THE PROP. AREA M
ARKED WITH RED PAINT. MAPSCO 704,P





CALLER DID NOT PROVIDE EMAIL

Map Cross Reference : MAPSCO 704,P

FaxBack Requested ? YES    Lone Star Xref:


052090575 to 917706401859 at 13:58:20 on THU, 07/28/05 for VTG #0002
SEQUENCE NUMBER 0003   CDC = VTG
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows :
MCI              SPRINT           AT&T             EL PASO ELEC CO
EL PASO WATER    BROADWING        LEVEL 3 COMM     QWEST COMM.
TX GAS-EL PASO   VALLEY TELECOMM  SM&P-W           TIME WARNER COM
XSPEDIUS MNG CO  LONE STAR XCHG

Locate Request No. 052098198

Prepared By TARAH LEBSACK        On 28-JUL-05  At 1045

MapRef :                            Grid: 314600106293A  Footprint: D07

Location:     County: EL PASO  Town: EL PASO

             Address: 2401 FLORENCE

Beginning Work Date 08/01/05 Time of Day: 11:00 am   Duration: 20 WEEKS

Fax-A-Locate Date          at

Excavation Type : LEAD REMEDIATION
Nature of Work  : LEAD REMEDIATION

Blasting ? NO           48 Hr Notice ? YES
White Line ? NO         Digging Deeper Than 16 Inches ? YES

Person Calling : CHRIS NELSON
Company Name   : ENTACT
Work by ENTACT      For EPA

Person to Contact : BOB WINTERS

Phone No.  ( 915 )351-3863 /(    )     ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 915 )351-9634
Email:     RWINTERS@ENTACT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: KERBEY AVE
MARK ENTIRE YARD AND ALL EASEMENTS.





FAX #: 915-351-9634
RWINTERS@ENTACT.COM

Map Cross Reference : MAPSCO 735,J

FaxBack Requested ? NO     Lone Star Xref:


052098198 to 917706401859 at 13:58:23 on THU, 07/28/05 for VTG #0003
SEQUENCE NUMBER 0004   CDC = VTG
Texas Excavation Safety System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows :
AT&T             EL PASO ELEC CO  EL PASO WATER    LEVEL 3 COMM
MCI              TX GAS-EL PASO   VALLEY TELECOMM  SM&P-W
LONE STAR XCHG

Locate Request No. 052091846

Prepared By PENNY SCHUTZA        On 28-JUL-05  At 1111

MapRef :                            Grid: 314730106300C  Footprint: D02

Location:     County: EL PASO  Town: EL PASO

             Address: 4204 WALLINGTON

Beginning Work Date 08/01/05 Time of Day: 08:00 am   Duration: 01 DAYS

Fax-A-Locate Date          at

Excavation Type : OTHER
Nature of Work  : INSTALLING AN ANCHOR

Blasting ? NO           48 Hr Notice ? YES
White Line ? NO         Digging Deeper Than 16 Inches ? YES

Person Calling : KATHY
Company Name   : R.E.R. UTILITY CONTRACTOR, INC.
Work by R.E.R. UTILITY CONTRACTOR, INC.      For EL PASO ELEC. CO.

Person to Contact : KATHY

Phone No.  ( 915 )598-7163 /(    )     ( Hours: 08:00 am/05:00 pm )
Fax No.    ( 915 )598-1846
Email:     KATHY200@SBCGLOBAL.NET

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: BRENTWOOD
WORK DATE: 08-01-2005 08:00 AM  MAPSCO: 734 D  GRIDS:   LAT/LONG:   RE
PLACING AN ANCHOR BEHIND 4204 WALLINGTON. PLEASE LOCATE AREA AROUND EX
ISTING ANCHOR.  281851.XML




KATHY200@SBCGLOBAL.NET

Map Cross Reference : MAPSCO 734,D

FaxBack Requested ? YES    Lone Star Xref:


052091846 to 917706401859 at 13:58:26 on THU, 07/28/05 for VTG #0004



