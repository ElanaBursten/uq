
~EMAIL 00001 USAN 01/16/06 09:03:29 0014832 NORMAL NOTICE RENEWAL

Message Number: 0014832 Received by USAN at 11:11 on 01/13/06 by CMB

Work Begins:    01/18/06 at 11:30   Notice: 020 hrs      Priority: 2

Caller:         CHARLIE PROA
Company:        FREHNER CONSTRUCTION
Address:        4040 FREHNER RD, NORTH LAS VEGAS
City:           NORTH LAS VEGAS               State: NV Zip: 89030
Telephone:      702-346-1537                  Fax: 702-649-8834
Alt #(s):       
Nature of Work: EXC FOR ST OVERLAY
Done for:       CITY OF MESQUITE              Explosives: NO
Foreman:        ARON WATSON                   Area Marked in White Paint
Permit Type:    NO                            
Location: 
    WRK ON ALL/O W. MESQUITE BL FR INT/O THISTLE ST TO INT/O N. WILLOW
    ST (INCL 50' BEH SI/WLK B/SI MESQUITE)

Place: MESQUITE                     County: CLARK                State: NV

Map Book: 
Page Grid
0000 000
Long/Lat Long: -114.081787 Lat:  36.802204 Long: -114.064133 Lat:  36.805626 
State Grid  E:  0 N:  0 E:   0 N:  0

R E M A R K S: 
RENEWAL OF TICKET RN#412663 ORIG DATE 10/26/05-CMB 01/13/06

Sent to:
ATTNEV = ZZ AT&T COMM NEVADA          CHACOM = CHARTER COMMUNICATIONS       
MOAPA  = ZE MOAPA VLY TELEPHONE       OVRPWR = ZE OVERTON POWER DIST#5      
RVITEL = ZE RIO VIRGIN TELEPHONE      VIRWTR = ZE VIRGIN VALLEY WATER       


