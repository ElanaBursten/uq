
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 11180740 
Transmit      Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Original Call Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Work to Begin Date: 04/15/11      Time: 12:01 AM 
Expiration    Date: 04/29/11      Time: 11:59 PM 

Place: FOREST HILL 
Address: 909         Street: DELRAY DR 
Nearest Intersecting Street: JEAN COURT 

Type of Work: VZN:E7170570/27340/5845C 
Extent of Work: PLEASE LOCATE FROM F909 DELRAY DR THROUGH RIGHT OF WAY TO
: PROPERTY. PLEASE MARK ALL SIDES OF PROPERTY. PLEASE LOCATE MAIN LINE, DROPS,
: FOR GROUND RODS AND FIOS. 
Remarks: BSW WO#V0009256 

Company      : ARMSTRONG CONSTRUCTION 
Contact Name : MR ARMSTRONG                   Fax          :  
Contact Phone: (410)940-2032                  Ext          :  
Caller Address: 9435 WASHINGTON BLVD STE M & N 
                LAUREL, MD 20723 
Email Address:  armstrongconstructionfios@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: VERIZON 
State: MD              County: HARFORD 
MPG:  Y 
Caller    Provided:  Map Name: HARF  Map#: 4238 Grid Cells: H10 
Computer Generated:  Map Name: HARF  Map#: 4238 Grid Cells: H10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.5687693Lon: -76.3937716 SE Lat: 39.5625027Lon: -76.3875122 
Explosives: N 
ATC01      HCPW01     MLV01      VHF 
Send To: BGEHR     Seq No: 0097   Map Ref:  
         BGEHRG            0097 
