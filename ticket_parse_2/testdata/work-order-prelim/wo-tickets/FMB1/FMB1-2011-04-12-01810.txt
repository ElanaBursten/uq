
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 11180739 
Transmit      Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Original Call Date: 04/12/11      Time: 01:52 PM           Op: webusr 
Work to Begin Date: 04/15/11      Time: 12:01 AM 
Expiration    Date: 04/29/11      Time: 11:59 PM 

Place: BEL AIR 
Address: 1001        Street: GORSUCH GARTH 
Nearest Intersecting Street: NORTH TOLLGATE ROAD 

Type of Work: VZN:E7170200/27340/5845C 
Extent of Work: PLEASE LOCATE FROM HH F 1001 GORSUCH GARTH THROUGH RIGHT OF WAY
: TO PROPERTY. PLEASE MARK ALL SIDES OF PROPERTY. PLEASE LOCATE MAIN LINE,
: DROPS, FOR GROUND RODS AND FIOS. 
Remarks: BSW WO#V0009228 

Company      : ARMSTRONG CONSTRUCTION 
Contact Name : MR ARMSTRONG                   Fax          :  
Contact Phone: (410)940-2032                  Ext          :  
Caller Address: 9435 WASHINGTON BLVD STE M & N 
                LAUREL, MD 20723 
Email Address:  armstrongconstructionfios@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: VERIZON 
State: MD              County: HARFORD 
MPG:  Y 
Caller    Provided:  Map Name: HARF  Map#: 4350 Grid Cells: J6 
Computer Generated:  Map Name: HARF  Map#: 4350 Grid Cells: J6 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.5312523Lon: -76.3875072 SE Lat: 39.5250027Lon: -76.3812445 
Explosives: N 
BGEHR      BGEHRG     VHF 
Send To: MLV01     Seq No: 0094   Map Ref:  
