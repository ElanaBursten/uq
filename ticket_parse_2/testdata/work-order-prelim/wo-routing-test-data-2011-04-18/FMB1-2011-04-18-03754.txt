
NOTICE OF INTENT TO EXCAVATE      FIOS 
Ticket No: 11191354 
Transmit      Date: 04/18/11      Time: 12:15 PM           Op: webusr 
Original Call Date: 04/18/11      Time: 12:14 PM           Op: webusr 
Work to Begin Date: 04/21/11      Time: 12:01 AM 
Expiration    Date: 05/05/11      Time: 11:59 PM 

Place: ESSEX 
Address: 9           Street: OLD MAPLE CT 
Nearest Intersecting Street: HIGH VILLA RD 

Type of Work: VZN:E7188460/25350/5845C 
Extent of Work: LOCATE FROM ST HH IN FRONT OF 5 OLD MAPLE CT UP TO INCLUDE
: ENTIRE PROPERTY OF 9 OLD MAPLE CT. LOCATE MAIN LINES FOR DROPS, GROUNDS AND
: FIOS.   PLEASE CONTACT LAMBERTS CABLE WITH ANY QUESTIONS @ (301)725-7082. 
Remarks: BSW WO#V0010441 

Company      : CABLE UTILITY MARTINEZ 
Contact Name : LEONEO MARTINEZ                Fax          :  
Contact Phone: (443)810-4659                  Ext          :  
Caller Address: 9435 WASHINGTON BLVD STE M & N 
                LAUREL, MD 20723 
Email Address:  cableutilitymartinezfios@yahoo.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: VERIZON 
State: MD              County: BALTIMORE 
MPG:  Y 
Caller    Provided:  Map Name: BALT  Map#: 4701 Grid Cells: F7 
Computer Generated:  Map Name: BALT  Map#: 4701 Grid Cells: F7 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 39.3375346Lon: -76.4688065 SE Lat: 39.3312789Lon: -76.4625160 
Explosives: N 
BGEBA      BGEBAG     CBW04      QWESTM01   VBT 
Send To: CTV01     Seq No: 0337   Map Ref:  
