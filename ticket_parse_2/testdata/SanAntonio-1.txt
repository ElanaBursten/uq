SEQUENCE NUMBER 0002   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     CITY OF COTULLA  AEP-CP&L-UTLQST  SW BELL TELE   
LONE STAR XCHG

Locate Request No. 023085079

Prepared By SHANNON GYURE        On 04-NOV-02  At 0758

MapRef : LA SALLE                   Grid: 282600099143C  Footprint: D01

Location:     County: LA SALLE  Town: COTULLA

             Address: 1401 TILDEN ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: CHEROKEE 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085079 to 912106847245 at 08:31:43 on MON, 11/04/02 for CWD #0002




SEQUENCE NUMBER 0003   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     AEP-CP&L-UTLQST  SW BELL TELE     TCI OF TEXAS   
WEST TEXAS GAS   LONE STAR XCHG

Locate Request No. 023085082

Prepared By SHANNON GYURE        On 04-NOV-02  At 0759

MapRef : ZAVALA                     Grid: 284100099490B  Footprint: D01

Location:     County: ZAVALA  Town: CRYSTAL CITY

             Address: 1312 E BEXAR ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: 7TH ST 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085082 to 912106847245 at 08:31:52 on MON, 11/04/02 for CWD #0003




SEQUENCE NUMBER 0004   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     AEP-CP&L-UTLQST  SW BELL TELE     TCI OF TEXAS   
WEST TEXAS GAS   LONE STAR XCHG

Locate Request No. 023085085

Prepared By SHANNON GYURE        On 04-NOV-02  At 0800

MapRef : ZAVALA                     Grid: 284030099490D  Footprint: D01

Location:     County: ZAVALA  Town: CRYSTAL CITY

             Address: 310 S JUAREZ ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: ZAVALA 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085085 to 912106847245 at 08:32:02 on MON, 11/04/02 for CWD #0004




SEQUENCE NUMBER 0005   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
MCIWORLDCOM      QWEST COMM.      WLMS COMM/WCLLC  AEP-CP&L-UTLQST
SW BELL TELE     SW TEXAS TELE    CITY OF UVALDE   LONE STAR XCHG

Locate Request No. 023085091

Prepared By SHANNON GYURE        On 04-NOV-02  At 0803

MapRef : UVALDE                     Grid: 291400099470C  Footprint: D01

Location:     County: UVALDE  Town: UVALDE

             Address: 1708 N CAMP ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: STUDER 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085091 to 912106847245 at 08:32:11 on MON, 11/04/02 for CWD #0005




SEQUENCE NUMBER 0006   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     AEP-CP&L-UTLQST  SW BELL TELE     CITY OF UVALDE 
LONE STAR XCHG

Locate Request No. 023085094

Prepared By SHANNON GYURE        On 04-NOV-02  At 0804

MapRef : UVALDE                     Grid: 291230099473A  Footprint: D02

Location:     County: UVALDE  Town: UVALDE

             Address: 511 W LEONA ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: HIGH ST 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085094 to 912106847245 at 08:32:20 on MON, 11/04/02 for CWD #0006




SEQUENCE NUMBER 0007   CDC = CWD
Texas One Call System
*  ROUTINE  * MESSAGES Sent to  Office(s) as follows : 
NO UTILITIES     AEP-CP&L-UTLQST  SW BELL TELE     TCI - UVALDE   
CITY OF UVALDE   LONE STAR XCHG

Locate Request No. 023085095

Prepared By SHANNON GYURE        On 04-NOV-02  At 0805

MapRef : UVALDE                     Grid: 291300099463C  Footprint: D01

Location:     County: UVALDE  Town: UVALDE

             Address: 513 E LEONA ST 

Beginning Work Date 11/06/02 Time of Day: 08:15 AM   Duration: 01 HOUR 

Fax-A-Locate Date          at 

Excavation Type : BURY SVC LINE                 
Nature of Work  : BURY SVC LINE                 

Blasting ? NO           48 Hr Notice ? YES  
White Line ? NO         Digging Deeper Than 16 Inches ? NO     

Person Calling : MIKE VARA
Company Name   : VARA'S TRENCHING
Work by VARA'S TRENCHING      For SWBT

Person to Contact : MIKE VARA

Phone No.  ( 830 )278-7408 /( 830 )503-5545 PAGER ( Hours: 08:00 AM/05:00 PM )
Fax No.    ( 830 )278-6996
Email:     VARATREN@HILLCONNECT.COM

Remarks : MARK ALL UNDERGROUND FACILITIES AS NECESSARY
Near Intersection: CAMP 
TEMP LINE IS LAYING ON GROUND                                         
LOCATE ENTIRE PROP, ST & ALLEY R/W                                    
                                                                      
                                                                      
                                                                      
                                                                      
FAX #: 830-278-6996                                                   
VARATREN@HILLCONNECT.COM

Map Cross Reference : NONE

FaxBack Requested ? NO     Lone Star Xref: 


023085095 to 912106847245 at 08:32:28 on MON, 11/04/02 for CWD #0007






