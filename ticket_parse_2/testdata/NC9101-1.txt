
STS01  00057 NCOCc 01/28/05 09:49:55 C0027445-00C NORM NEW STRT

North Carolina One Call

Ticket : C0027445 Date: 01/28/05 Time: 09:42 Oper: DDH Chan:999
Old Tkt: C0027445 Date: 01/28/05 Time: 09:49 Oper: DDH Rev :00C

State: NC Cnty: DARE Place: NAGS HEAD In/Out: B
Subdivision: NORTHBANK

Address :
Street  : W WOODHILL DR   Intersection: Y
Cross 1 : S CROATAN HWY
Location: CORNER LOT  LOCATE THE PROPERTY LINE BETWEEN PRIATES QUAY AND THE
ENTIRE N SIDE OF THE ADDRESS 102 AND 110 W WOODHILL DR  <SIDE BY SIDE> PER
CALLER  THIS IS ALL THE INFO CALLER HAS   PLEASE CALL TONY AT 252-305-1225 IF
HAVE ANY QUESTIONS
:
Grids   : 3558B7538A    3558B7538B    3558B7538C    3558C7538A    3558C7538B

Work type:INSTALL SEPTIC SYSTEM
Work date:02/01/05  Time: 09:42  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 2 WEEKS  Done for: NORTHBANK PROPERTIES

Company : ROBERTSON REPAIR  Type: CONT
Co addr : PO BOX 1783
City    : MANTIO State: NC Zip: 27954
Caller  : TONY ROBERTSON Phone: 252-305-1225
BestTime:

Remarks : THIS IS ALL THE INFO I COULD GET OUT OF THIS CALLER
:
Submitted date: 01/28/05 Time: 09:49
Members: CSR01  CTT13* OBC01* STS01  TNH01  VEP02*

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=C002744500C&OPR=10xthZQA0qTG7tb


STS01  00374 NCOCa 01/28/05 09:52:26 A0061555-00A NORM NEW STRT

North Carolina One Call

Ticket : A0061555 Date: 01/28/05 Time: 09:50 Oper: LJS Chan:999
Old Tkt: A0061555 Date: 01/28/05 Time: 09:51 Oper: LJS Rev :00A

State: NC Cnty: CAMDEN Place: CAMDEN In/Out: B
Subdivision:

Address : 300
Street  : N RIVER RD   Intersection: N
Cross 1 : IVY NECK RD
Location: HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET? WITHIN 1/4 MILE
LOCATE: ENTIRE PROPERTY
:
Grids   : 3620B7606C    3620B7606D    3620C7606C    3620C7606D

Work type:WATER SERV
Work date:02/01/05  Time: 09:50  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 2HRS  Done for: SAME

Company : CAMDEN COUNTY  Type: OTHR
Co addr : 117 N HWY343
City    : CAMDEN State: NC
Caller  : BETTY GRIFFINS Phone: 252-338-1919
BestTime:

Submitted date: 01/28/05 Time: 09:51
Members: CHC01* CSR01  CSR02  CTT13* MTV01  SCW01  STS01

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006155500A&OPR=igdZTIAroeJ3wgI


STS01  00371 NCOCa 01/28/05 09:50:33 A0061543-00A NORM NEW STRT

North Carolina One Call

Ticket : A0061543 Date: 01/28/05 Time: 09:48 Oper: LJS Chan:999
Old Tkt: A0061543 Date: 01/28/05 Time: 09:50 Oper: LJS Rev :00A

State: NC Cnty: CAMDEN Place: CAMDEN In/Out: B
Subdivision:

Address : 192
Street  : HWY158   Intersection: N
Cross 1 : BELCROSS RD
Location: HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET? WITHIN 1/4 MILE
LOCATE: ENTIRE PROPERTY
:
Grids   : 3618A7610A    3618A7610B    3619D7610A    3619D7610B    3620C7609A
Grids   : 3620C7609B    3620D7609A    3620D7609B

Work type:WATER SERV
Work date:02/01/05  Time: 09:47  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: Y     Emergency: N
Duration: 2HRS  Done for: SAME

Company : CAMDEN COUNTY  Type: OTHR
Co addr : 117 N HWY343
City    : CAMDEN State: NC
Caller  : BETTY GRIFFINS Phone: 252-338-1919
BestTime:

Submitted date: 01/28/05 Time: 09:50
Members: CHC01* CSR01  CSR02  CTT04* ENG01  MTV01  SCW01  STS01  VEP04*

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006154300A&OPR=TPK91qfUL8naN4o


STS01  00375 NCOCa 01/28/05 09:52:27 A0061556-00A NORM NEW GRID

North Carolina One Call

Ticket : A0061556 Date: 01/28/05 Time: 09:51 Oper: SST Chan:999
Old Tkt: A0061556 Date: 01/28/05 Time: 09:51 Oper: SST Rev :00A

State: NC Cnty: PERQUIMANS Place: HERTFORD In/Out: B
Subdivision: NONE

Address : 414
Street  : W DOBBS ST   Intersection: N
Cross 1 : E RAILROAD AVE
Location: HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET? CROSS STREET IS
W/IN 1/4 MILE              LOCATE THE ENTIRE PROPERTY AND BOTH SIDES OF ROAD
POSS ROAD BORE    PAINT AND FLAG             WORK WILL START AFTER 48HR NOTICE
:
Grids   : 3611C7628B    3611C7628C    3611D7628B    3611D7628C

Work type:INSTALL NAT GAS
Work date:02/01/05  Time: 09:51  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: Y  Railroad: Y     Emergency: N
Duration: 3 DAYS  Done for: PIEDMONT NAT GAS

Company : W E CURLING INC  Type: OTHR
Co addr : 4125 S MILATARY HWY
City    : CHESAPEAKE State: VA Zip: 23321
Caller  : CHRISTINA LANGER Phone: 757-485-8703
Contact : SAME Phone:
BestTime:

Submitted date: 01/28/05 Time: 09:51
Members: CSR01  CTT04* ENG01  MTV01  STS01  VEP04*

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006155600A&OPR=hhgWOBzngWBylXF


STS01  00376 NCOCa 01/28/05 09:52:28 A0061561-00A NORM NEW GRID

North Carolina One Call

Ticket : A0061561 Date: 01/28/05 Time: 09:51 Oper: ARB Chan:999
Old Tkt: A0061561 Date: 01/28/05 Time: 09:52 Oper: ARB Rev :00A

State: NC Cnty: NEW HANOVER Place: WILMINGTON In/Out: B
Subdivision: NO

Address :
Street  : MILITARY CUTOFF RD   Intersection: N
Cross 1 : GORDON RD
Cross 2 : MARKET ST
Location: STARTING AT INTER LOCATE BOTH SIDES OF MILITARY CUTOFF RD TO MARKET ST

  CALLER STATES NO OTHER INTER RDS    INTER ON SEPARATE TKT    DISTANCE UNKNOWN
WORK WILL BEGIN AFTER THE FORTY EIGHT HOUR NOTICE
:
Grids   : 3415A7749B    3415A7749C    3415B7749B

Work type:ROAD WIDENING
Work date:02/01/05  Time: 09:51  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 30 MONTHS  Done for: NCDOT

Company : APAC  Type: CONT
Co addr : BURNT MILL DR
City    : WILMINGTON State: NC Zip: 00000
Caller  : TERRY WHALEY Phone: 910-254-1313
Contact : TERRY (CELL#) Phone: 910-231-1131
BestTime:
Fax     : 910-254-1235

Remarks : --SEE ORIGINAL TICKET#: A0618231-00A
--SEE ORIGINAL TICKET#: A0665244-00A
--SEE ORIGINAL TICKET#: A0710676-00A
--SEE ORIGINAL TICKET#: C0307916-00C
--SEE ORIGINAL TICKET#: A0801778-00A
--SEE ORIGINAL TICKET#: A0842642-00A
--SEE ORIGINAL TICKET#: C0008556-00C
:
Submitted date: 01/28/05 Time: 09:52
Members: ATT01  CLS01  CPL40* CSR01  CSR02  DOT01  NCN04  NHC01  SBT15* STS01
Members: UTV01* VCW01* WIL01

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006156100A&OPR=PLG3xmZQH2hUHzh


STS01  00377 NCOCa 01/28/05 09:52:28 A0061564-00A NORM NEW STRT

North Carolina One Call

Ticket : A0061564 Date: 01/28/05 Time: 09:48 Oper: RBS Chan:WEB
Old Tkt: A0061564 Date: 01/28/05 Time: 09:52 Oper: RBS Rev :00A

State: NC Cnty: CUMBERLAND Place: FAYETTEVILLE In/Out: I
Subdivision: BRONCO SQUARE

Address : 1047
Street  : MURCHISON RD   Intersection: Y
Cross 1 : FILTER PLANT DR
Location: HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET?    CROSS ST
WITHIN 1/4 MILE     LOCATE  ENTIRE PROPERTY AT THE BACK OF BLDG WE WILL RUN TWO
GAS SERVICE LINE ON THE BACK OF BLDG
:
Grids   : 3459*7851*   3459A7852D   3500*7851*   3500*7852*   3501*7851*
Grids   : 3501*7852*   3501A7853D   3502*7852*   3502*7853D   3503A7853*
Grids   : 3504*7853*   3507*7856*

Work type:INSTALLING GAS
Work date:02/01/05  Time: 10:20  Hours notice: 97/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: Y  Railroad: N     Emergency: N
Duration: 15 DAYS  Done for: NCNG

Company : DISTRIBUTION CONSTRUCTION CO  Type: CONT
Co addr : 720 HILLSBORO ST
City    : FAYETTEVILLE State: NC Zip: 28301
Caller  : RICHARD STARLING Phone: 910-484-4440
Contact : RICHARD STARLING Phone: 910-484-4440
BestTime:

Submitted date: 01/28/05 Time: 09:52
Members: ASI01  ATT01  CLS01  COF01* CPL40* CSR01  CSR02  CTT05* FPW01  FPW02
Members: KMC03* NCN01  STS01  SUS01*

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006156400A&OPR=ecZRL9ymdVCxmWC


STS01  00378 NCOCa 01/28/05 09:53:08 A0061566-00A NORM NEW GRID

North Carolina One Call

Ticket : A0061566 Date: 01/28/05 Time: 09:52 Oper: ARB Chan:999
Old Tkt: A0061566 Date: 01/28/05 Time: 09:52 Oper: ARB Rev :00A

State: NC Cnty: NEW HANOVER Place: WILMINGTON In/Out: B
Subdivision: NO

Address :
Street  : MILITARY CUTOFF RD   Intersection: Y
Cross 1 : MARKET ST
Location: LOCATE ENTIRE INTERSECTION FOR 500FT IN ALL DIRECTIONS BOTH SIDES OF
THE RD
NO OTHER INTERSECTING ROADS
:
Grids   : 3415A7749B    3415A7749C    3416D7749B    3416D7749C

Work type:ROAD WIDENING
Work date:02/01/05  Time: 09:52  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 30 MONTHS  Done for: NCDOT

Company : APAC  Type: CONT
Co addr : BURNT MILL DR
City    : WILMINGTON State: NC Zip: 00000
Caller  : TERRY WHALEY Phone: 910-254-1313
Contact : TERRY (CELL#) Phone: 910-231-1131
BestTime:
Fax     : 910-254-1235

Remarks : --SEE ORIGINAL TICKET#: A0618234-00A
--SEE ORIGINAL TICKET#: A0665257-00A
--SEE ORIGINAL TICKET#: A0710687-00A
--SEE ORIGINAL TICKET#: C0307920-00C
--SEE ORIGINAL TICKET#: A0801779-00A
--SEE ORIGINAL TICKET#: A0842648-00A
--SEE ORIGINAL TICKET#: C0008559-00C
:
Submitted date: 01/28/05 Time: 09:52
Members: ATT01  CLS01  CPL40* CSR01  CSR02  DOT01  NCN04  NHC01  SBT15* STS01
Members: UTV01* VCW01* WIL01

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006156600A&OPR=gebTNCzngWBylXF


STS01  00379 NCOCa 01/28/05 09:53:17 A0061568-00A NORM NEW STRT

North Carolina One Call

Ticket : A0061568 Date: 01/28/05 Time: 09:49 Oper: PAL Chan:999
Old Tkt: A0061568 Date: 01/28/05 Time: 09:53 Oper: PAL Rev :00A

State: NC Cnty: ALAMANCE Place: MEBANE In/Out: B
Subdivision: GRACE LANDING

Address :
Street  : GRACE LANDING DR   Intersection: Y
Cross 1 : OLD HILLSBOROUGH RD
Location: HOW FAR IS THE JOB SITE FROM THE CLOSEST CROSS STREET? AT THE
INTERSECTIOIN
LOCATE A 100FT RADIUS IN ALL DIRECTIONS FOR BOTH SIDES OF BOTH ROADS
:
Grids   : 3603*7915*   3603*7916*   3603*7917*   3603*7918*

Work type:MAIN LINE CABLE
Work date:02/01/05  Time: 09:48  Hours notice: 96/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: Y  Railroad: N     Emergency: N
Duration: 15 DAYS  Done for: TW

Company : CABLENET CONSTRUCTION  Type: CONT
Co addr : 8663 HWY150
City    : CLEMMONS State: NC Zip: 00000
Caller  : STEVE HARRIS Phone: 336-345-8433
Contact : SAME Phone:
BestTime:

Remarks : THIS IS ALL LOCATE INFORMATION CALLER HAS   ANY QUESTIONS PLEASE CALL
48HRS PLEASE
:
Submitted date: 01/28/05 Time: 09:53
Members: CAC01* CLS01  CSR01  DPC11* MEB01* PEM01* PRO02  PSG07  SBT12* STS01

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006156800A&OPR=igdVPE0kbT9wjVD


STS01  00380 NCOCa 01/28/05 09:53:28 A0061573-00A RUSH NEW GRID

North Carolina One Call

Ticket : A0061573 Date: 01/28/05 Time: 09:52 Oper: TB1 Chan:WEB
Old Tkt: A0061573 Date: 01/28/05 Time: 09:53 Oper: TB1 Rev :00A

State: NC Cnty: CURRITUCK Place: POWELLS POINT In/Out: I
Subdivision:

Address :
Street  : W SIDE LN   Intersection: N
Location: AREA FROM 350' NORTH OF (SR 1112)WEST SIDE LANE TO 1200' NORTH AND
FROM EAST SIDE OF EXISTING ELECTRIC TRANSMISSION LINE 750'WEST
:
Grids   : 3620C7549C    3620C7549D    3620D7549C    3620D7549D

Work type:EXCAVATION, GRADING, PAVING, STORM DRAIN
Work date:01/28/05  Time: 10:00  Hours notice: 1/0  Priority: RUSH
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: 8 MONTHS  Done for: R LAWSON CONSTRUCTION

Company : RPC CONTRACTING  Type: CONT
Co addr : PO BOX 333
City    : KITTY HAWK State: NC Zip: 27949
Caller  : TRACI BRADY Phone: 252-261-3336
BestTime:

Remarks : SEE ORGINIAL TICKET # A0292131  SEE ORIGINAL TKT# A663573
SEE ORIGINAL TKT# C122830       SEE ORIGINAL TKT A751113
SEE ORIGINAL TKT# A397188       SEE ORIGINAL TKT C324367
SEE ORIGINAL TKT# A441878       SEE ORIGINAL TKT C842414
SEE ORIGINAL TKT# A601903       SEE ORIGINAL TKT A0020235
:
Submitted date: 01/28/05 Time: 09:53
Members: CSR01  CTT13* MTV01  OBC01* STS01  VEP02*

View map at:
http://www.ncocc.org/ocars/apps/web_map_gis_tkt.asp?Operation=MAPTKT&TRG=A006157300A&OPR=USPLF5rgXTEvoY9


