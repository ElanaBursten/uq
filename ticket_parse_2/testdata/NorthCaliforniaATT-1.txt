
                                                                                     
 PBTHAN 00021 USAN 01/31/07 07:42:39 0037524 NORMAL NOTICE                           
                                                                                     
 Message Number: 0037524 Received by USAN at 07:40 on 01/31/07 by JBM                
                                                                                     
 Work Begins:    02/02/07 at 08:00   Notice: 020 hrs      Priority: 2                
                                                                                     
 Expires: 02/28/07 at 17:00   Update By: 02/26/07 at 16:59                           
                                                                                     
 Caller:         LANCE DOWN                                                          
 Company:        THE GAS COMPANY                                                     
 Address:        1700 INYO ST                                                        
 City:           MOJAVE                        State: CA Zip: 93501                  
 Business Tel:   661-824-0819                  Fax:                                  
 Email Address:                                                                      
                                                                                     
 Nature of Work: DIG & TR TO REPL GAS SVC                                            
 Done for:       SAME                          Explosives: N                         
 Foreman:        UNKNOWN                                                             
 Field Tel:                                    Cell Tel:                             
 Area Premarked: Y   Premark Method: OTHER, WHITE PAINT                              
 Permit Type:    CITY                          Number: PENDING                       
 Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N        
 Excavation Enters Into Street Or Sidewalk Area: N                                   
                                                                                     
 Location:                                                                           
 Street Address:         303  E H ST                                                 
   Cross Street:         N DAVIS ST                                                  
     WRK IN THE AL BEH ADDR                                                          
                                                                                     
 Place: TEHACHAPI                    County: KERN                 State: CA          
                                                                                     
 Long/Lat Long: -118.448883 Lat:  35.131172 Long: -118.443100 Lat:  35.133862        
                                                                                     
 Sent to:                                                                            
 BHNBFD = BRIGHT HOUSE NETWORKS        BHNTEH = BRIGHT HOUSE NETWORKS                
 CTYTHA = CITY TEHACHAPI               MOJGAS = MOJAVE PIPELINE                      
 PBTHAN = PACIFIC BELL HANFORD         SCEVIC = SO CAL EDISON VICTORVILL             
 SCGMOJ = SO CAL GAS MOJAVE                                                          
                                                                                     
                                                                                     
 At&t Ticket Id: 102          clli code:   THCHCA01


                                                                                     
 PBTSAC 00031 USAN 01/31/07 07:43:37 0412177 NORMAL NOTICE EXTENSION                 
                                                                                     
 Message Number: 0412177 Received by USAN at 07:43 on 01/31/07 by INTERNET           
                                                                                     
 Work Begins:    11/15/06 at 07:00   Notice: 000 hrs      Priority: 2                
                                                                                     
 Expires: 03/02/07 at 17:00   Update By: 02/28/07 at 16:59                           
                                                                                     
 Caller:         BRIAN FOSTER                                                        
 Company:        TEICHERT CONSTRUCTION                                               
 Address:        8811 KIEFER BLVD                                                    
 City:           SACRAMENTO                    State: CA Zip: 95851                  
 Business Tel:   916-386-5868                  Fax: 916-386-2392                     
 Email Address:  BFOSTER@TEICHERT.COM                                                
                                                                                     
 Nature of Work: TR FOR STORM DRAIN                                                  
 Done for:       P/O KAMILOS                   Explosives: N                         
 Foreman:        DENNIS DEW                                                          
 Field Tel:                                    Cell Tel: 916-825-8718                
 Area Premarked: Y   Premark Method: WHITE PAINT                                     
 Permit Type:    NO                                                                  
 Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: Y        
 Excavation Enters Into Street Or Sidewalk Area: Y                                   
                                                                                     
 Location:                                                                           
 Street Address:         4134  BUCHANAN DR                                           
   Cross Street:         WINDING WAY                                                 
     WRK IN ST IN FRT/O PROP (IN A 15' BY 45' AREA)                                  
                                                                                     
 Place: FAIR OAKS, CO AREA           County: SACRAMENTO           State: CA          
                                                                                     
 Long/Lat Long: -121.249695 Lat:  38.640121 Long: -121.244720 Lat:  38.645004        
                                                                                     
 Excavator Requests Operator(s) To Re-mark Their Facilities: N                       
 Comments:                                                                           
 #1 EXTEND TO 01/05/07 RE-MARK NO ORIG DATE 11/10/06-IDEM 12/06/06                   
 #2 EXTEND TO 02/02/07 RE-MARK NO ORIG DATE 11/10/06-IDEM 01/03/07                   
 #3 EXTEND TO 03/02/07 RE-MARK NO ORIG DATE 11/10/06-IDEM 01/31/07                   
                                                                                     
 Sent to:                                                                            
 COMSAC = COMCAST-SACRAMENTO           COSAC1 = COUNTY SACRAMENTO - ADMI             
 COSAC3 = COUNTY SACRAMENTO TRF SI     COSAC4 = COUNTY SACRAMENTO SEWER              
 FOAWTR = FAIR OAKS WTR DIST           PBTSAC = PACIFIC BELL SACRAMENTO              
 PGESAC = PGE DISTR SACRAMENTO         SMUDSO = SMUD                                 
                                                                                     
 At&t Ticket Id: 92           clli code:   FROKCA11

