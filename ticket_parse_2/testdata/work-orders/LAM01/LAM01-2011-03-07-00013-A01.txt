﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0002840</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>E7018210</JOBID>
  <ORDERNUMBER>BMDH32O00</ORDERNUMBER>
  <DUEDATE>3/12/2011 11:00:00 PM</DUEDATE>
  <MON>MD00091750362</MON>
  <ISSUEDATE>2/28/2011 6:44:00 PM</ISSUEDATE>
  <CUSTOMERNAME>DAVID MARKOWITZ</CUSTOMERNAME>
  <CUSTOMERADDRESS>13519 HAYWORTH DR</CUSTOMERADDRESS>
  <CUSTOMERCITY>ROCKVILLE</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>20854</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>(240) 678-9490</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>(301) 251-2821</CUSTOMERBTN>
  <WIRECENTER>301251</WIRECENTER>
  <WORKCENTER>22</WORKCENTER>
  <CO>1222</CO>
  <SERVINGTERMINAL>R 13517 HAYWORTH DR</SERVINGTERMINAL>
  <TERMINALGPS>39.080610 / -77.217500</TERMINALGPS>
  <MAP>5163</MAP>
  <GRID>F8</GRID>
  <CIRCUITID>11/BURY/H32O00/ /VZMD</CIRCUITID>
  <F2CABLE>H4081</F2CABLE>
  <TERMINALPORT>2</TERMINALPORT>
  <F2PAIR>166</F2PAIR>
</XML>