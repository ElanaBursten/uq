﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0010937</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>M787549</JOBID>
  <ORDERNUMBER>CH02G9JR</ORDERNUMBER>
  <DUEDATE>3/10/2011 12:00:00 AM</DUEDATE>
  <MON />
  <ISSUEDATE>4/19/2011 12:00:00 AM</ISSUEDATE>
  <CUSTOMERNAME>HARVEY J BRADY &amp; SON</CUSTOMERNAME>
  <CUSTOMERADDRESS>2853 THORNBROOK ROAD</CUSTOMERADDRESS>
  <CUSTOMERCOUNTY>HOWARD</CUSTOMERCOUNTY>
  <CUSTOMERCITY>ELICOTT CITY</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>21042</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER />
  <CUSTOMERBTN>1410-750-2680</CUSTOMERBTN>
  <WIRECENTER>410461</WIRECENTER>
  <WORKCENTER />
  <CO />
  <SERVINGTERMINAL>PED F2857 THORNBROOK RD</SERVINGTERMINAL>
  <TERMINALGPS />
  <MAP>4815</MAP>
  <GRID>F1</GRID>
  <CIRCUITID />
  <F2CABLE>9450FR</F2CABLE>
  <TERMINALPORT />
  <F2PAIR>0750</F2PAIR>
</XML>