﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0002934</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>E7020640</JOBID>
  <ORDERNUMBER>BMDRA2O00</ORDERNUMBER>
  <DUEDATE>3/30/2011 11:00:00 PM</DUEDATE>
  <MON>MD00091506169</MON>
  <ISSUEDATE>3/1/2011 12:50:00 PM</ISSUEDATE>
  <CUSTOMERNAME>GARY HORNBERGER</CUSTOMERNAME>
  <CUSTOMERADDRESS>25 CHINS CT</CUSTOMERADDRESS>
  <CUSTOMERCOUNTY>BALTIMORE</CUSTOMERCOUNTY>
  <CUSTOMERCITY>OWINGS MILLS</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>21117</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>(410) 375-3170</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>(A03) 865-9065</CUSTOMERBTN>
  <WIRECENTER>410356</WIRECENTER>
  <WORKCENTER>22</WORKCENTER>
  <CO>1222</CO>
  <SERVINGTERMINAL>HH 25 CHINS CT</SERVINGTERMINAL>
  <TERMINALGPS>39.421821 / -76.800588</TERMINALGPS>
  <MAP>4576</MAP>
  <GRID>B3</GRID>
  <CIRCUITID>11/BURY/RA2O00/ /VZMD</CIRCUITID>
  <F2CABLE>H1024</F2CABLE>
  <TERMINALPORT>4</TERMINALPORT>
  <F2PAIR>163</F2PAIR>
</XML>