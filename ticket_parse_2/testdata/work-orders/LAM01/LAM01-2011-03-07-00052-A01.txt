﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XML Type="WorkOrder">
  <INTERNALNUMBER>V0002984</INTERNALNUMBER>
  <TYPE>NORMAL</TYPE>
  <JOBID>E7022530</JOBID>
  <ORDERNUMBER>BMDMG2O00</ORDERNUMBER>
  <DUEDATE>3/8/2011 11:00:00 PM</DUEDATE>
  <MON>MD00091521821</MON>
  <ISSUEDATE>3/1/2011 5:03:00 PM</ISSUEDATE>
  <CUSTOMERNAME>MARJORIE JONES</CUSTOMERNAME>
  <CUSTOMERADDRESS>2135 NORTH ANVIL LN</CUSTOMERADDRESS>
  <CUSTOMERCITY>TEMPLE HILLS</CUSTOMERCITY>
  <CUSTOMERSTATE>MD</CUSTOMERSTATE>
  <CUSTOMERZIP>20748</CUSTOMERZIP>
  <CUSTOMERCONTACTNUMBER>(240) 701-9005</CUSTOMERCONTACTNUMBER>
  <CUSTOMERBTN>(301) 630-0974</CUSTOMERBTN>
  <WIRECENTER>301238</WIRECENTER>
  <WORKCENTER>22</WORKCENTER>
  <CO>1222</CO>
  <SERVINGTERMINAL>R2137 N ANVIL LN</SERVINGTERMINAL>
  <TERMINALGPS>38.839263 / -76.970524</TERMINALGPS>
  <MAP>5649</MAP>
  <GRID>E6</GRID>
  <CIRCUITID>11/BURY/MG2O00/ /VZMD</CIRCUITID>
  <F2CABLE>H1034</F2CABLE>
  <TERMINALPORT>3</TERMINALPORT>
  <F2PAIR>131</F2PAIR>
</XML>