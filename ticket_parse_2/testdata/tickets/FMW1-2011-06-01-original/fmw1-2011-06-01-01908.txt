
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11278787 
Transmit      Date: 06/01/11      Time: 01:23 PM           Op: wibrit 
Original Call Date: 06/01/11      Time: 01:11 PM           Op: wibrit 
Work to Begin Date: 06/04/11      Time: 12:01 AM 
Expiration    Date: 06/17/11      Time: 11:59 PM 

Place: UPPER MARLBORO 
Address: 4916        Street: TROTTERS GLEN DR 
Nearest Intersecting Street: CHARLES HILL BLVD 

Type of Work: INSTALL UNDERGROUND SPRINKLER SYSTEM 
Extent of Work: MARK THE ENTIRE PROPERTY 
Remarks:  

Company      : AUTOMATIC SPRINKLER SYSTEM 
Contact Name : EDWIN REYES                    Fax          :  
Contact Phone: (301)768-6898                  Ext          :  
Caller Address: PO BOX 599 
                LANHAM, MD 20706 
Email Address:  redwin03@gmail.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: HAROLD VAUGHT 
State: MD              County: PRINCE GEORGES 
MPG:  N 
Caller    Provided:  Map Name: PG    Map#: 5652 Grid Cells: A9,A10,B9,B10 
Computer Generated:  Map Name: PG    Map#: 5652 Grid Cells: A9,A10,B9,B10 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 38.8250493Lon: -76.8124996 SE Lat: 38.8124999Lon: -76.8000398 
Explosives: N 
PEPCOPG    VPG        WGL06      WSS01 
Send To: JTV02     Seq No: 0313   Map Ref:  
