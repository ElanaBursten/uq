
NOTICE OF INTENT TO EXCAVATE        
Ticket No: 11277275 
Transmit      Date: 06/01/11      Time: 08:09 AM           Op: webusr 
Original Call Date: 06/01/11      Time: 07:52 AM           Op: webusr 
Work to Begin Date: 06/04/11      Time: 12:01 AM 
Expiration    Date: 06/17/11      Time: 11:59 PM 

Place: CLINTON 
Address: 8009        Street: WOODYARD ROAD 
Nearest Intersecting Street: COLONIAL LANE 

Type of Work: ABANDON GAS SERVICE 
Extent of Work: ABANDON GAS SERVICE / MARK ENTIRE PROPERTY / BRANCHED WITH
: #8013 / MARK SERVICE LINE TO METERS FOR BOTH #8009 AND #8013 
Remarks:  

Company      : WASHINGTON GAS 
Contact Name : EDWARD HOPPE                   Fax          :  
Contact Phone: (571)327-4520                  Ext          :  
Caller Address: 4000 FORESTVILLE RD 
                FORESTVILLE, MD 20747 
Email Address:  ehoppe@washgas.com 
Alt. Contact :                                Alt. Phone   :  
Work Being Done For: WASHINGTON GAS 
State: MD              County: PRINCE GEORGES 
MPG:  Y 
Caller    Provided:  Map Name: PG    Map#: 5767 Grid Cells: C6 
Computer Generated:  Map Name: PG    Map#: 5767 Grid Cells: C6 

Lat:               Lon:                        Zone:  
Ex. Coord NW Lat: 38.7812806Lon: -76.8625571 SE Lat: 38.7750341Lon: -76.8562875 
Explosives: N 
PEPCOPG    VPG        WGL06      WSS01 
Send To: JTV02     Seq No: 0101   Map Ref:  
