
BGAWM  00002 GAUPC 01/11/11 06:24:12 01031-400-061-001 LPEXCA
Large Project Excavation Notification
Notice : 01031-400-061 Date: 01/03/11  Time: 15:30  Revision: 001 

State : GA County: HOUSTON       Place: WARNER ROBINS                           
Addr  : From:        To:        Name:    CORDER                         RD      
Near  : Name:    PINEVIEW                       DR  

Subdivision:                                         
Locate: FROM PINEVIEW DR LOC THE R/O/W ON BOTH SIDES CORDER RD TO RUSSELL PKWY 
      :  --FROM CORDER RD LOC THE R/O/W ON BOTH SIDES OF THE FOLLOWING ROADS FOR
      :  200FT DOWN EACH RD FOR OAK DR, PEACOCK DR, SPRUCE DR, LA CLAIRE DR, WOO
      :  DLAND DR E & W, SKYWAY DR E & W, LINDA K CT, SPRING HILL DR, LAVERN DR,
      :  STORY DR, DUSA AVE, NELSON DR, PINEDALE DR, OAKDALE DR                 

Grids       : 3235A8338A 3235A8339A 3235A8339B 3235A8339C 3235A8339D 
Grids       : 3235B8338A 3235B8339A 3235B8339B 3235B8339C 3235B8339D 
Grids       : 3235C8338A 3235C8339A 3235C8339B 3235C8339C 3235C8339D 
Grids       : 3236A8338A 3236A8338B 3236A8339B 3236A8339C 3236A8339D 
Grids       : 3236B8338A 3236B8338B 3236B8339A 3236B8339B 3236B8339C 
Grids       : 3236B8339D 3236C8338A 3236C8339A 3236C8339B 3236C8339C 
Grids       : 3236C8339D 3236D8338A 3236D8339A 3236D8339B 3236D8339C 
Grids       : 3236D8339D 3237D8338A 3237D8339D 
Work type   : CLEARING & GRADING, WIDENING RD & INSTL STORM DRAINS                

ScopeOfWork : CLEARING & GRADING, WIDENING RD & INSTL STORM DRAINS             

Meeting Date      : 01/06/11 Time: 12:00 AM
Meeting Respond By: 01/05/11 Time: 23:59
Meeting Location  : **THIS IS A RESTAKE OF LARGE PROJECT 09290-400-042--CALLER 
        : MISSED RESTAKE DATE, BUT TICKET IS STILL VALID--PLS MARK AC
        : CORDING TO THE CURRENT MARKING AGREEMENT**                 
LP Contact        : DANNY HARRISON            Phone   : 4789609879 Ext:  
Contact Email     : DANOHARRISON@AOL.COM                                       

Start date: 01/19/11 Time: 00:00 Hrs notc : 000
Legal day : 01/14/11 Time: 07:00 Good thru: 04/11/11 Restake by: 04/06/11
RespondBy : 01/10/11 Time: 23:59 Duration : 18 MONTHS  Priority: 1
Done for  : HOUSTON COUNTY                          
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : **THIS IS A RESTAKE OF LARGE PROJECT 09290-400-042--CALLER MISSED RE
        : STAKE DATE, BUT TICKET IS STILL VALID--PLS MARK ACCORDING TO THE CUR
        : RENT MARKING AGREEMENT**                                            
        : OVERHEAD WORK BEGIN DATE: 
        : OVERHEAD WORK COMPLETION DATE: 

Company : GEORGIA ASPHALT                           Type: CONT                
Co addr : PO BOX 7261                              
City    : MACON                           State   : GA Zip: 31209              
Caller  : DANNY HARRISON                  Phone   :  478-960-9879              
Fax     :                                 Alt. Ph.:  478-960-9879              
Email   : DANOHARRISON@AOL.COM                                                
Contact :                                                           

Submitted date: 01/03/11  Time: 15:30  Oper: 211
Mbrs : BGAWM CCM01 CNT50 CNT51 GAUPC HOU01 JONG01 FLI71 WSMDVL 
-------------------------------------------------------------------------------

