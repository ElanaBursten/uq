LONESTAR 811
------------------------------------------------------------------------------
NOTICE OF INTENT TO EXCAVATE      NOTI-ROUTINE          RETRANSMIT
Ticket No: 561966025

Send To: ERVIN01   Seq No:   27   Map Ref:

Transmit      Date: 08/29/16      Time: 05:31 PM        Type: PORTAL TICKET
Original Call Date: 08/24/16      Time: 03:29 PM
Work to Begin Date: 08/26/16      Time: 02:45 PM

------------------------------ Work Information ------------------------------
County : BEXAR        State: TX
Place  : SAN ANTONIO
Address: 3500        Street: WISEMAN BLVD
Nearest Intersecting Street: WESTOVER HILLS BLVD

Explosives     : N           Duration: 6 MONTHS
Marked in White: N           Digging deeper than 16in.: Y
Type of Work   : INSTALL UTILITY POLE
Work Done For  : CPS ENERGY
Extent of Work : WORK DATE: 8/26/2016 2:45:00 PM POLE LOCATION IS MARKED WITH
: A RED TOP WOOD LATH NEXT TO A RED TOP STAKE WITH A RED ARROW PAINTED IN THE
: STREET AND A RED P NEXT TO IT.-NEED A 6' RADIUS MARKED AROUND STAKE-WR#
: 1176103 POLE IS LOCATED ON THE W SIDE ON WISEMAN RD. IT IS ABOUT 400' S OF
: THE ABOVE ADDRESS. POLE LOCATION IS 14' FROM THE EDGE OF THE PAVEMENT.
: EXCAVATOR PHONE: (210)496-3200   ALT CONTACT EMAIL:
: jbrookover@trcsolutions.com  MAPBOOK: 578-F6
Remarks        : INSTALL UTILITY POLE
: TESS: 1673728504 Seq: 7192 Rcd: 08/24/16 15:32 WTBD: 08/26/16 14:45

------------------------------ Caller Information ----------------------------
Company     : TRC SOLUTIONS                  Type: EXCAVATOR
Contact Name: JACK BROOKOVER                Phone: (210)245-3748
Address     : 975 W BITTERS RD
              SAN ANTONIO, TX 78216
Alt. Contact: JACK BROOKOVER                Phone: (210)245-3748
Best Time   : 0800-170                        Fax:
Email       : jbrookover@trcsolutions.com

------------------------------ Location Information --------------------------
Excavation Coordinates for # Polygons: 1
Poly 1: NW Lat: 29.474407 Lon: -98.685076    SE Lat: 29.465691 Lon: -98.673735
Map        Page:          Grids:

--------------------------Additional Members Notified-------------------------


Code       Company                       Code       Company
ERVIN01    ERVIN CABLE CONSTRUCTION      L3CTX01    LEVEL 3 COMMUNICATIONS