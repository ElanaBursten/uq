

From: IRTHNet  At: 03/13/13 04:00 PM  Seq No: 234

OUNC 
Ticket No: 13046623               48 HOUR NOTICE 
Send To: QLNOR15    Seq No:   26  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:30 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:26 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: LANE                    Place: EUGENE 
Address: 2746        Street: POTTER ST 
Nearest Intersecting Street: E 27TH AVE 

Twp: 18S   Rng: 03W   Sect-Qtr: 05 
Twp: 18S   Rng: 3W    Sect-Qtr: 5-SW-SE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.0287810 Lon:-123.0778140 SE Lat: 44.0271060 Lon:-123.0750950 

Type of Work: INSTALL WATER SERVICE 
Location of Work: IN FRONT OF ABOVE ADDRESS APPROX 264FT SOUTH OF E 27TH AVE
: ON POTTER ST. MARK THE WEST ROW. AREA MARKED IN WHITE. 

Remarks:  
:  

Company     : EWEB WATER OPERATIONS 
Contact Name: DENA SANDER                      Phone: (541)685-7000 
Cont. Email : DENA.SANDER@EWEB.ORG 
Alt. Contact: KYLE HENGLER- CALL 1             Phone: (541)953-3834 
Contact Fax :  
Work Being Done For: EWEB 
Additional Members:  
CMCST02    EUGENE01   EWEB01     NWN01
