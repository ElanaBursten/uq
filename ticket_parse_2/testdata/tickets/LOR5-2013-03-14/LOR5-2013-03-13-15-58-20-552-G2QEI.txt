

From: IRTHNet  At: 03/13/13 02:57 PM  Seq No: 209

OUNC 
Ticket No: 13046583               48 HOUR NOTICE 
Send To: QLNOR17    Seq No:   66  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:14 am    Op: ortyni 
Original Call Date:  3/13/13   Time: 10:08 am    Op: ortyni 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address: 261         Street: SW KINGSTON AVE 
Nearest Intersecting Street: FAIRVIEW 

Twp: 1N    Rng: 1E    Sect-Qtr: 32-SE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.5221944 Lon:-122.7075789 SE Lat: 45.5209037 Lon:-122.7062078 

Type of Work: REPL WATER SRVC LINE 
Location of Work: ADD IS ON NW CORNER OF ABV INTER. MARK FROM THE SIDE OF THE
: HOUSE AT ABV ADD CLOSEST TO KINGSTON AVE GOING TO THE WATER METER LOCATED AT
: KINGSTON AVE. 

Remarks:  
:  

Company     : BOYD BROWN AND SONS INC 
Contact Name: ERIC BROWN                       Phone: (503)781-2857 
Cont. Email : bbsinco@comcast.net 
Alt. Contact:                                  Phone:  
Contact Fax : (503)636-0909 
Work Being Done For: LARRY THOMAS 
Additional Members:  
ATT01      CMCST01    NWN01      PGE01      PTLD03
