

From: IRTHNet  At: 03/13/13 04:00 PM  Seq No: 231

OUNC 
Ticket No: 13046617               48 HOUR NOTICE 
Send To: QLNOR23    Seq No:   13  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:25 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:23 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: CLACKAMAS               Place: OREGON CITY 
Address: 19183       Street: CLAIRMONT WAY 
Nearest Intersecting Street: WHITNEY LANE 

Twp: 3S    Rng: 2E    Sect-Qtr: 8-NW,7-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.3294145 Lon:-122.6008890 SE Lat: 45.3270290 Lon:-122.5981820 

Type of Work: INSTALL CATV SERVICE 
Location of Work: ADDRESS IS LOCATED ON THE SOUTH SIDE OF CLAIRMONT WAY. MARK
: FOLLOWING THE WHITE LINE FROM PED TO HOUSE, LOCATED SOUTH OF THE PROPERTY TO
: THE WEST SIDE OF THE HOUSE AT THE ABOVE LOCATION. 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 656 F7 
:  

Company     : FISK COMMUNICATIONS 
Contact Name: JOHN HALL                        Phone: (360)772-3815 
Cont. Email : JOHN@DROPBURY.COM 
Alt. Contact:                                  Phone: (360)772-3815 
Contact Fax :  
Work Being Done For: COMCAST 
Additional Members:  
BCT01      CCDOT01    CCDOT05    CMCST01    COC01      NWN01      PGE01
