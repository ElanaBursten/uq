

From: IRTHNet  At: 03/13/13 03:07 PM  Seq No: 213

OUNC 
Ticket No: 13046593               48 HOUR NOTICE 
Send To: QLNOR29    Seq No:    3  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:17 am    Op: orcbvj 
Original Call Date:  3/13/13   Time: 10:13 am    Op: orcbvj 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: MALHEUR                 Place: ONTARIO 
Address: 1500        Street: CREST WAY 
Nearest Intersecting Street: VERDE DR 

Twp: 18S   Rng: 47E   Sect-Qtr: 5-NE,4-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.0369199 Lon:-116.9883970 SE Lat: 44.0357069 Lon:-116.9852413 

Type of Work: INSTALL POWER LINE 
Location of Work: EXCAVATION SITE IS ON THE S SIDE OF THE ROAD. 
: MARK FROM ABV INTER  APX 360 FT TO HOUSE AT ABV ADD. AREA MARKED IN WHITE 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : JAY HARTLEY EXCAVATION 
Contact Name: JAY HARTLEY                      Phone: (541)889-3304 
Cont. Email :  
Alt. Contact: JACKIE HARTLEY                   Phone: (208)739-8996 
Contact Fax :  
Work Being Done For: BRUCE  ERELBACK 
Additional Members:  
CC02       CNG07      COFO01     IPC01      OID01
