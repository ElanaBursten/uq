

From: IRTHNet  At: 03/13/13 03:44 PM  Seq No: 227

OUNC 
Ticket No: 13046562               48 HOUR NOTICE 
Send To: QLNOR15    Seq No:   25  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:24 am    Op: orjean 
Original Call Date:  3/13/13   Time:  9:57 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:00 am 

State: OR            County: LANE                    Place: EUGENE 
Address:             Street: ROYAL AVE 
Nearest Intersecting Street: WILLIAMS ST 

Twp: 17S   Rng: 4W    Sect-Qtr: 21-SW-SE,28-NW-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.0705816 Lon:-123.1776232 SE Lat: 44.0694864 Lon:-123.1760988 

Type of Work: CURB INLET REMOVE/REPLACE 
Location of Work: ON THE NORTH WEST CORNER OF ABOVE INTERSECTION 

Remarks: MARKED IN WHITE PAINT 
:  

Company     : CITY OF EUGENE 
Contact Name: ROB BROOKS                       Phone: (541)682-4937 
Cont. Email : ROBIN.L.BROOKS@CI.EUGENE.OR.US 
Alt. Contact: CHAD MICKELSON                   Phone: (541)682-4990 
Contact Fax :  
Work Being Done For: CITY OF EUGENE 
Additional Members:  
CMCST02    EUGENE01   EWEB01     NWN01
