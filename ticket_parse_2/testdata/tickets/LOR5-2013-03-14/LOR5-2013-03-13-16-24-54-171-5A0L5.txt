

From: IRTHNet  At: 03/13/13 03:24 PM  Seq No: 217

OUNC 
Ticket No: 13046613               48 HOUR NOTICE 
Send To: QLNOR23    Seq No:   12  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:22 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:04 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: CLACKAMAS               Place: LAKE OSWEGO 
Address: 1115        Street: HALLINAN CIR 
Nearest Intersecting Street: HALLINAN ST 

Twp: 2S    Rng: 1E    Sect-Qtr: 10-SE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.4081675 Lon:-122.6682980 SE Lat: 45.4058640 Lon:-122.6661130 

Type of Work: INSTALL CATV SERVICE 
Location of Work: ADDRESS IS LOCATED ON THE WEST SIDE OF HALLINAN CIR. MARK
: FOLLOWING THE WHITE LINE FROM POLE #1113 TO HOUSE, LOCATED NORTHWEST OF THE
: PROPERTY TO THE SOUTHWEST SIDE OF THE HOUSE AT THE ABOVE LOCATION. POLE
: #1113. 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 656 F7 
:  

Company     : FISK COMMUNICATIONS 
Contact Name: JOHN HALL                        Phone: (360)772-3815 
Cont. Email : JOHN@DROPBURY.COM 
Alt. Contact:                                  Phone: (360)772-3815 
Contact Fax :  
Work Being Done For: COMCAST 
Additional Members:  
CLO01      CMCST01    NWN01      PGE01
