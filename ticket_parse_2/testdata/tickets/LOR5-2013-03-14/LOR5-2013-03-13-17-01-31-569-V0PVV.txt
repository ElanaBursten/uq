

From: IRTHNet  At: 03/13/13 04:00 PM  Seq No: 232

OUNC 
Ticket No: 13046592               48 HOUR NOTICE 
Send To: QLNOR26    Seq No:   13  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:27 am    Op: orchri 
Original Call Date:  3/13/13   Time: 10:12 am    Op: orcbtl 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: JACKSON                 Place: ASHLAND 
Address: 1001        Street: EUREKA ST 

Twp: 39S   Rng: 1E    Sect-Qtr: 9-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 42.1953179 Lon:-122.7025882 SE Lat: 42.1944888 Lon:-122.7010869 

Type of Work: INSTALL FENCE 
Location of Work: ADD LIES BETWEEN B ST AND C ST AND IS THE ONLY HOUSE WHERE
: THE FRONT DOOR FACES EUREKA ST.  MARK  IF LOOKING AT HOUSE FROM THE STREET
: START ON THE RIGHT SIDE BETWEEN HEAT PUMP AND THE PORCH EXTEND ALOG THE
: PROPERTY LINE TO THE BACK PROPERTY LINE THEN ACROSS TO THE DRIVEWAY THEN TO
: THE END OF THE HOUSE. 

Remarks:  
:  

Company     : HOMEOWNER 
Contact Name: CAROL SMITH                      Phone: (541)846-1300 
Cont. Email :  
Alt. Contact: CAROL CELL                       Phone: (541)659-2017 
Contact Fax :  
Work Being Done For: HOMEOWNER 
Additional Members:  
ASH01      TCI18      WPNG03
