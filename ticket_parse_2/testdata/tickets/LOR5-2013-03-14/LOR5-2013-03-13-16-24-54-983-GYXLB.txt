

From: IRTHNet  At: 03/13/13 03:24 PM  Seq No: 220

OUNC 
Ticket No: 13046610               48 HOUR NOTICE 
Send To: QLNOR14    Seq No:   21  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:22 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:19 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: MARION                  Place: SALEM 
Address: 4115        Street: HAWTHORNE AVE NE 
Nearest Intersecting Street: FELINA AVE NE 

Twp: 7S    Rng: 2W    Sect-Qtr: 12-SE 
Twp: 7S    Rng: 3W    Sect-Qtr: 12-SE-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.9794325 Lon:-122.9936627 SE Lat: 44.9774925 Lon:-122.9913253 

Type of Work: RELOCATING FIRE HYDRANT 
Location of Work: LOCATE AND MARK THE AREA MARKED IN WHITE PAINT 300 FT NORTH
: OF THE INTERSECTION ON THE WEST SIDE OF THE STREET.  WORK ZONE IS 15 FT X 15
: FT. 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO 
:  

Company     : CITY OF SALEM 
Contact Name: PAM POLVI                        Phone: (503)588-6333 
Cont. Email : SERVICE@CITYOFSALEM.NET 
Alt. Contact: RANDY REISTERER                  Phone: (503)385-7450 
Contact Fax : (503)584-4656 
Work Being Done For: CITY OF SALEM - WATER DEPT 
Additional Members:  
CMCST02    NWN01      PGE04      SALEM01
