

From: IRTHNet  At: 03/13/13 02:57 PM  Seq No: 204

OUNC 
Ticket No: 13046591               48 HOUR NOTICE 
Send To: QLNOR07    Seq No:   13  Map Ref:  
Update Of: 12229874 
Transmit      Date:  3/13/13   Time: 10:14 am    Op: orjack 
Original Call Date:  3/13/13   Time: 10:12 am    Op: orjack 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: DESCHUTES               Place: BEND 
Address: 2329        Street: NW VARDON CT 
Nearest Intersecting Street: NW BRAID DR 

Twp: 17S   Rng: 11E   Sect-Qtr: 24-SW-SE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.0861294 Lon:-121.3517621 SE Lat: 44.0835367 Lon:-121.3473728 

Type of Work: REPLACE CATV SRVC 
Location of Work: EXCAVATION SITE IS ON THE S SIDE OF THE ROAD. 
: ADD APX 500 FT E OF ABV INTER. MARK FROM ELEC METER, GOING APX 100 FT N,
: ACROSS ST TO CATV PED. AREA MARKED IN WHITE. 

Remarks:  
: UPDT TICKET -- MARKS ARE GONE -- NEEDS REMARKS 

Company     : SCOTT HARRIN DESIGN/CONST 
Contact Name: MIKE MCCALLISTER                 Phone: (541)350-7433 
Cont. Email : HARRIN17@BENDCABLE.COM 
Alt. Contact: MIKE MCCALLISTER-CEL             Phone: (541)848-7342 
Contact Fax : (541)318-4527 
Work Being Done For: BEND BROADBAND 
Additional Members:  
BCC01      BEND01     CNG03      PPL20
