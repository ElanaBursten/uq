

From: IRTHNet  At: 03/13/13 03:38 PM  Seq No: 223

OUNC 
Ticket No: 13046814               48 HOUR NOTICE 
Send To: QLNOR26    Seq No:   27  Map Ref:  
Update Of: 13043079 
Transmit      Date:  3/13/13   Time: 12:44 pm    Op: webusr 
Original Call Date:  3/13/13   Time: 12:42 pm    Op: webusr 
Work to Begin Date:  3/15/13   Time: 12:45 pm 

State: OR            County: JACKSON                 Place: MEDFORD 
Address: 2781        Street: HOWARD AVE 
Nearest Intersecting Street: MACE RD 

Twp: 37S   Rng: 2W    Sect-Qtr: 14-NE,13-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 42.3583050 Lon:-122.8945953 SE Lat: 42.3555610 Lon:-122.8927157 

Type of Work: MOVE SVC 
Location of Work: MARKED WHITE--- 165 FT S C/L  OF MACE RD
: EXPAND LOCATES TO BACK OF SIDEWALK IN DRIVEWAY. 

Remarks: MOVE SCV APPROX 8 FT FROM ENCLOSURE 
: L SIDE OF HOUSE 

Company     : NPL CONSTRUCTION 
Contact Name: LINDSEY ACREY                    Phone: (541)690-1845 
Cont. Email : LACREY@GONPL.COM 
Alt. Contact: MIKE SMITH                       Phone: (541)941-4046 
Contact Fax :  
Work Being Done For: AVISTA 
Additional Members:  
MEDFRD01   MSD01      MWC01      PPL25      RRVID01    TCI18      WPNG03
