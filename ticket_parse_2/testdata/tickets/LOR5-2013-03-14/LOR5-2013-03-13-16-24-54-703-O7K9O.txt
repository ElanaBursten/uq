

From: IRTHNet  At: 03/13/13 03:24 PM  Seq No: 218

OUNC 
Ticket No: 13046558               48 HOUR NOTICE 
Send To: QLNOR23    Seq No:   11  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:22 am    Op: orjean 
Original Call Date:  3/13/13   Time:  9:56 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 10:00 am 

State: OR            County: CLACKAMAS               Place: SANDY 
Address: 18187       Street: MEADOW AVE 
Nearest Intersecting Street: BUCK ST 

Twp: 2S    Rng: 5E    Sect-Qtr: 18-SW 
Twp: 2S    Rng: 5E    Sect-Qtr: 19-NW,18-SW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.3929955 Lon:-122.2478771 SE Lat: 45.3896502 Lon:-122.2445083 

Type of Work: INSTALLING GAS SERVICE 
Location of Work: MARK LEFT OF HOUSE 10 FT BACK OUT TO INCLDUING THE ST; LOT
: 73 DEER POINTE 2, LOT IS MARKED AS 18107 ON MAP IN ERROR. 

Remarks: MARK FACING PROPERTY FROM ST. 
:  

Company     : LOY CLARK PIPELINE 
Contact Name: HEATHER WYCKOFF                  Phone: (503)783-1188 
Cont. Email : HEATHER.WYCKOFF@LOYCLARK.COM 
Alt. Contact: ROBIN JUNOR EXT 1177             Phone: (503)644-2137 
Contact Fax : (503)520-8763 
Work Being Done For: NW NATURAL 
Additional Members:  
GTC16      NWN01      PGE01      SANDY01    WAVE01
