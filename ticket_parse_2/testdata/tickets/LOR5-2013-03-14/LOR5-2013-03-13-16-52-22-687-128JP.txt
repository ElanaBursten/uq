

From: IRTHNet  At: 03/13/13 03:51 PM  Seq No: 228

OUNC 
Ticket No: 13046911               +NON-EMERGENCY+ 
Send To: QLNOR14    Seq No:   39  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:51 pm    Op: orshan 
Original Call Date:  3/13/13   Time:  1:45 pm    Op: orshan 
Work to Begin Date:  3/13/13   Time:  2:00 pm 

State: OR            County: MARION                  Place: SALEM 
Address: 1830        Street: ANUNSEN ST NE 
Nearest Intersecting Street: BRADY CT NE 

Twp: 7S    Rng: 3W    Sect-Qtr: 11-SE,12-SW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 44.9733465 Lon:-123.0095780 SE Lat: 44.9720436 Lon:-123.0076184 

Type of Work: PLANTING TREES 
Location of Work: EXCAVATION SITE IS ON THE N SIDE OF THE ROAD. 
: SITE IS AT THE NW CORNER OF THE ABV INTER. MARK THE PARKING STRIPS ON THE
: ANUNSEN ST NE AND BRADY CT SIDE OF PROP. THERE ARE 9 AREAS MARKED IN WHITE 

Remarks: ++CALLER REQUESTS AREA MARKED A.S.A.P!++ 
: MADE AWARE OF STATE LAW/NO GUARANTEE/MUST WAIT FOR MARKINGS 

Company     :  
Contact Name: MARC MCCABE                      Phone: (503)362-1808 
Cont. Email :  
Alt. Contact: MARC CELL                        Phone: (503)559-0305 
Contact Fax :  
Work Being Done For: MARC MCCABE 
Additional Members:  
CMCST02    L3C01      NWN01      PGE04      QWEST02    SALEM01    SELEC01
