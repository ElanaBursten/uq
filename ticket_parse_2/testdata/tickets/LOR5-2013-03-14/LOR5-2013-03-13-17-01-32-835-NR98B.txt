

From: IRTHNet  At: 03/13/13 04:00 PM  Seq No: 233

OUNC 
Ticket No: 13046589               48 HOUR NOTICE 
Send To: QLNOR26    Seq No:   12  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:25 am    Op: orchri 
Original Call Date:  3/13/13   Time: 10:11 am    Op: orcbjh 
Work to Begin Date:  3/15/13   Time: 10:15 am 

State: OR            County: JACKSON                 Place: MEDFORD 
Address: 1717        Street: MAGNOLIA AVE 
Nearest Intersecting Street: LILAC ST 

Twp: 37S   Rng: 2W    Sect-Qtr: 34-SE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 42.3048199 Lon:-122.9189517 SE Lat: 42.3037465 Lon:-122.9175429 

Type of Work: INSTALL SPRINKLER SYSTEM , TRENCHING 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: ADD IS APX 1 BLK E OF THE ABV INTER. MARK FRONT YARD AT THE ABV ADD. 

Remarks:  
:  

Company     : LANDMARK LANDSCAPE 
Contact Name: GREG STANFIELD                   Phone: (541)608-7471 
Cont. Email :  
Alt. Contact: CHERIE STANFIELD                 Phone:  
Contact Fax :  
Work Being Done For: PETE HALE 
Additional Members:  
BCVSA01    MID01      MWC01      PPL25      TCI18      WPNG03
