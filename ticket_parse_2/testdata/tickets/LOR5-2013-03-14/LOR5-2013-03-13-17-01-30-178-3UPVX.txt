

From: IRTHNet  At: 03/13/13 04:00 PM  Seq No: 229

OUNC 
Ticket No: 13046603               48 HOUR NOTICE 
Send To: QLNOR23    Seq No:   14  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:28 am    Op: orruth 
Original Call Date:  3/13/13   Time: 10:18 am    Op: orruth 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: CLACKAMAS               Place: HUBBARD 
Address: 30050       Street: S BARLOW ROAD 
Nearest Intersecting Street: BARNARDS 

Twp: 4S    Rng: 1E    Sect-Qtr: 32-SW,31-SE 
Twp: 5S    Rng: 1E    Sect-Qtr: 6-NE,5-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.1728447 Lon:-122.7230421 SE Lat: 45.1708890 Lon:-122.7206047 

Type of Work: INSTALL FENCE 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: ADD APX 200FT S/2ND DRIVEWAY S OF ABV INTER.  MARK APX 264FT ALONG PROP
: ALONG S BARLOW ROAD AT ABV ADD.  POST AREAS ARE MARKED WITH WHITE PAINT.
: THIS IS UNDER THE SEQUOIA TREES. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : HOMEOWNER 
Contact Name: DANIEL EKENBARGER                Phone: (503)951-1847 
Cont. Email :  
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: HOMEOWNER 
Additional Members:  
CCDOT01    CTA01      NWN01      PGE01      WAVE01
