

From: IRTHNet  At: 03/13/13 03:24 PM  Seq No: 219

OUNC 
Ticket No: 13046598               48 HOUR NOTICE 
Send To: QLNOR26    Seq No:   11  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:21 am    Op: orjon 
Original Call Date:  3/13/13   Time: 10:16 am    Op: orjon 
Work to Begin Date:  3/15/13   Time: 10:30 am 

State: OR            County: JACKSON                 Place: ASHLAND 
Address: 640         Street: TOLMAN CREEK ROAD 

Twp: 39S   Rng: 1E    Sect-Qtr: 14-NW-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 42.1839133 Lon:-122.6724133 SE Lat: 42.1823861 Lon:-122.6691791 

Type of Work: INSTALL BOLLARDS 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: MARK PROP ON S SIDE OF BLDG AT ABV ADD. 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : BATZER INC 
Contact Name: ROGER FISHER                     Phone: (541)773-7553 
Cont. Email :  
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: INDEPENDENT PRINTING 
Additional Members:  
ASH01      QWEST02    TCI18      TID01      WPNG03
