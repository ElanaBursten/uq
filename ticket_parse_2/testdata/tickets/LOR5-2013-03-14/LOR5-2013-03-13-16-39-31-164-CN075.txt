

From: IRTHNet  At: 03/13/13 03:38 PM  Seq No: 224

OUNC 
Ticket No: 13046739               48 HOUR NOTICE 
Send To: QLNOR26    Seq No:   25  Map Ref:  
Update Of: 13042833 
Transmit      Date:  3/13/13   Time: 11:55 am    Op: orsusa 
Original Call Date:  3/13/13   Time: 11:34 am    Op: webusr 
Work to Begin Date:  3/15/13   Time: 11:45 am 

State: OR            County: JACKSON                 Place: MEDFORD 
Address: 20          Street: GROVELAND 
Nearest Intersecting Street: HILLCREST 

Twp: 37S   Rng: 1W    Sect-Qtr: 29-NW 
Twp: 37S   Rng: 1W    Sect-Qtr: 29-NW-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 42.3296761 Lon:-122.8452902 SE Lat: 42.3286255 Lon:-122.8434514 

Type of Work: LEAK 
Location of Work: MARKED WHITE--- L HAND SIDE OF HOUSE TO STREET 

Remarks: MARK FROM METER TO STREET 
: WRONG ADDRESS- NEW IS 20 GROVELAND 

Company     : NPL CONSTRUCTION 
Contact Name: LINDSEY ACREY                    Phone: (541)690-1845 
Cont. Email : LACREY@GONPL.COM 
Alt. Contact: STEVE MARCI                      Phone: (541)210-2648 
Contact Fax :  
Work Being Done For: AVISTA 
Additional Members:  
MEDFRD01   MID01      MWC01      PPL25      TCI18      WPNG03
