
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2012-01-12T14:04:30</transmitted>
    <seq_num>274</seq_num>
    <ticket>A201201202</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A201201202</ticket>
    <revision>00A</revision>
    <started>2012-01-12T14:04:16</started>
    <account>WEMOSS</account>
    <original_ticket>B135600826</original_ticket>
    <original_date>2011-12-22T11:21:55</original_date>
    <original_account>WEMOSS</original_account>
    <replace_by_date>2012-02-02T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>UPDT</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>UPDATE</derived_type>
    <response_required>Y</response_required>
    <response_due>2012-01-19T07:00:00</response_due>
    <expires>2012-02-07T07:00:00</expires>
    <state>VA</state>
    <county>LOUDOUN</county>
    <place>ASHBURN</place>
    <subdivision>STONEGATE</subdivision>
    <lot>156-161</lot>
    <street>FOXTHOM TERRACE</street>
    <cross1>HYRST GROVE TERRACE</cross1>
    <work_type>ELECTRIC PRIMARY - INSTALL</work_type>
    <done_for>DOMINION POWER</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>LEO CONSTRUCTION CO</name>
    <address1>PO BOX 966</address1>
    <city>STERLING</city>
    <cstate>VA</cstate>
    <zip>20167</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7034436608</phone>
    <caller>EMMA MOSS</caller>
    <caller_phone>7034436608</caller_phone>
    <email>leoconstruct@verizon.net</email>
    <contact>KEVIN HENRY</contact>
    <contact_phone>5407718154</contact_phone>
    <map_url>http://newtina.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&amp;TRG=A20120120200A&amp;OPR=C7yzmgeUL1kYBwhFt</map_url>
    <map_reference>5279F6</map_reference>
    <centroid>
      <coordinate>
        <latitude>39.026857</latitude>
        <longitude>-77.463911</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>39.026897</latitude>
        <longitude>-77.466525</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.029472</latitude>
        <longitude>-77.463882</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.026817</latitude>
        <longitude>-77.461296</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.024242</latitude>
        <longitude>-77.463939</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >ENTIRE PROP &amp; COMMON AREA TO INCLUDE THE ROAD CROSSING - PLEASE USE BOTH FLAGS
&amp; PAINT</location>
    <remarks xml:space="preserve" ></remarks>
    <polygon>
      <coordinate>
        <latitude>39.026817</latitude>
        <longitude>-77.461296</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.027542</latitude>
        <longitude>-77.462036</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.028072</latitude>
        <longitude>-77.463104</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.028042</latitude>
        <longitude>-77.464806</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.027908</latitude>
        <longitude>-77.465439</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.027695</latitude>
        <longitude>-77.465706</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.026634</latitude>
        <longitude>-77.465828</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.025845</latitude>
        <longitude>-77.465500</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.025013</latitude>
        <longitude>-77.464394</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.024937</latitude>
        <longitude>-77.463226</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3901B7727A-11</grid>
      <grid>3901B7727A-12</grid>
      <grid>3901B7727A-13</grid>
      <grid>3901B7727A-14</grid>
      <grid>3901B7727A-21</grid>
      <grid>3901B7727A-22</grid>
      <grid>3901B7727A-23</grid>
      <grid>3901B7727A-24</grid>
      <grid>3901B7727A-31</grid>
      <grid>3901B7727A-32</grid>
      <grid>3901B7727A-33</grid>
      <grid>3901B7727A-34</grid>
      <grid>3901B7727A-41</grid>
      <grid>3901B7727A-42</grid>
      <grid>3901B7727A-43</grid>
      <grid>3901B7727A-44</grid>
      <grid>3901B7727B-10</grid>
      <grid>3901B7727B-20</grid>
      <grid>3901B7727B-21</grid>
      <grid>3901B7727B-30</grid>
      <grid>3901B7727B-31</grid>
      <grid>3901B7727B-40</grid>
      <grid>3901C7727A-02</grid>
      <grid>3901C7727A-03</grid>
      <grid>3901C7727A-04</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>ATT392</member>
        <group_code>ATT</group_code>
        <description>AT&amp;T</description>
      </memberitem>
      <memberitem>
        <member>CMC703</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>LWS901</member>
        <group_code>LWS</group_code>
        <description>LOUDOUN WATER</description>
      </memberitem>
      <memberitem>
        <member>MCII81</member>
        <group_code>MCI</group_code>
        <description>MCI</description>
      </memberitem>
      <memberitem>
        <member>MFN902</member>
        <group_code>MFN</group_code>
        <description>ABOVENET COMMUNICATIONS INC</description>
      </memberitem>
      <memberitem>
        <member>OBV901</member>
        <group_code>OBV</group_code>
        <description>OPENBAND OF VIRGINIA LLC</description>
      </memberitem>
      <memberitem>
        <member>PAE901</member>
        <group_code>PAE</group_code>
        <description>PAETEC COMMUNICATIONS INC</description>
      </memberitem>
      <memberitem>
        <member>QWE901</member>
        <group_code>QWE</group_code>
        <description>QWEST COMMUNICATIONS</description>
      </memberitem>
      <memberitem>
        <member>VZN212</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WCC307</member>
        <group_code>WCC</group_code>
        <description>WOODLAWN COMMUNICATION LLC</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
