
PEHZ51 31 SC811 Voice 08/04/2014 11:08:00 AM 1408041257 Resend 

Notice Number:    1408041257        Old Notice:      1407311986        
Sequence:         31                Created By:      AKR               

Created:          08/04/14 11:10 AM                                     
Work Date:        08/05/14 11:59 PM                                     
Update on:                          Good Through:                      

Caller Information:
ATLAS SURVEYING
49 BROWNS COVE RD
RIDGELAND, SC 29936
Company Fax:                        Type:            Contractor        
Caller:           KEITH BURNS         Phone:         (843) 304-3976 Ext:
Caller Email:     kburns@atlassurveying.com                             

Site Contact Information:
KEITH BURNS                              Phone:(843) 304-3976 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  BEAUFORT             Place:  HILTON HEAD ISLAND
Street:  LIGHTHOUSE RD              Address In Instructions:false             
Intersection:  LIGHTHOUSE LN                                         
Subdivision:   SEA PINES PLANTATIONS                                 

Lat/Long: 32.136978,-80.812999
Second:  32.141598,-80.807419

Explosives:  N Premark:  N Drilling/Boring:  N Near Railroad:  N

Work Type:     DEMOLITION                                              
Work Done By:  ATLAS SURVEYING      Duration:        APPROX 1 WEEK     
Work Done For:                                                       

Instructions:
BLDGS F & G //THE CALLER STATES THESE TWO ARE SIDE BY SIDE //MARK THE FRONT OF
THE PROPERTY INSIDE THE PARKING LOT IN FRONT OF EACH BLDG                     

Directions:
THIS IS AT THE SCHOONER COURT CONDOS 

THIS WILL BE AT THE END OF LIGHTHOUSE  
RD                                                                            

Remarks:
RESENDING DUE TO : THESE CONDOS ARE UNITS 731 THUR 744// THESE BUILDING DO NOT
HAVE LETTERS ONLY NUMBERS// THEY ARE THE SAME BUILDING LISTED**               

Member Utilities Notified:
HRGZ57 SPSD29 PEHZ51 TWBZ51 TSPRZ84 

