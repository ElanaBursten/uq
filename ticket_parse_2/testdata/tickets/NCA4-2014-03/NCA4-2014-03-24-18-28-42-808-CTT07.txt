
CVINET 00050 USAN 03/24/14 11:50:45 0111781 NORMAL NOTICE 

Message Number: 0111781 Received by USAN at 11:45 on 03/24/14 by ANW

Work Begins:    03/27/14 at 07:00   Notice: 025 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 04/21/14 at 23:59   Update By: 04/17/14 at 16:59

Caller:         MATT CARTER              
Company:        DURHAM CONSTRUCTION                
Address:        1025 HOLLAND AVE                        
City:           CLOVIS                        State: CA Zip: 93612
Business Tel:   559-288-2016                  Fax:                             
Email Address:  MATT@DURHAM-CONSTRUCTION.COM                                

Nature of Work: TR TO INST ELEC CON                     
Done for:       THE LIGHT HOUSE FOR CHILDR    Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 559-288-2016           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: 142193                   
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         2405 TULARE ST
  Cross Street:         O ST

    WRK SW/SI/O THE PROP 

Place: FRESNO                       County: FRESNO               State: CA

Long/Lat Long: -119.786391 Lat:  36.736962 Long: -119.785234 Lat:  36.737845 


Sent to:
ATTCAL = AT&T TRANSMISSION CAL        CTYFNO = CITY FRESNO                  
COMFNO = COMCAST-FRESNO               COMMED = COMMUNITY MEDICAL CENTER     
CVINET = CVIN LLC                     MCIWSA = MCI WORLDCOM                 
PACBEL = PACIFIC BELL                 PGEFNO = PGE DISTR FRESNO             
TWTFRE = TW TELECOM - FRESNO MARK     TWLNGH = TW TELECOM - LONG HAUL