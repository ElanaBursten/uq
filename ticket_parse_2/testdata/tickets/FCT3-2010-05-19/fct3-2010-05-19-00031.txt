
BEC03  00029 NCOCa 05/19/10 09:03:08 A101390434-00A NORM NEW PLCE MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390434 Date: 05/19/10 Time: 09:02 Oper: SSB Chan:777
Old Tkt: A101390434 Date: 05/19/10 Time: 09:02 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SR1320   Intersection: N
Cross 1 : HWY294
Location: ST: RIVER HILL

LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET TO END OF RD
:
Grids   :

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNK  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 09:02
Members: BEC03  GTE01* PRO02  SCB01*
