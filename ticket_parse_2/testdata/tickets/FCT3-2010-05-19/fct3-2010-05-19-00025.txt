
BEC03  00023 NCOCa 05/19/10 08:59:04 A101390413-00A NORM NEW GRID MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390413 Date: 05/19/10 Time: 08:58 Oper: SSB Chan:777
Old Tkt: A101390413 Date: 05/19/10 Time: 08:59 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SR1158   Intersection: N
Cross 1 : SR1150
Cross 2 : SR1147
Location: ST: LIBERTY RD
XST: CANDY MOUNTAIN
ST2: WEHUTTY

LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET TO CROSS STREET
:
Grids   : 3505A8417A    3505B8417A    3505A8418D    3505B8418D    3506A8417A
Grids   : 3506A8417B    3506B8417A    3506B8417B    3506C8417A    3506C8417B
Grids   : 3506D8417A    3506D8417B    3507C8417B    3507C8417C    3507D8417A
Grids   : 3507D8417B

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNKNOWN  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 08:59
Members: BEC03  SCB01*

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10139041300A&OPR=mdgVN6ztjWLFyXDu
