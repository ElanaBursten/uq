
BEC03  00022 NCOCa 05/19/10 08:58:45 A101390411-00A NORM NEW GRID MOWR

North Carolina 811

MOWING TICKET

Ticket : A101390411 Date: 05/19/10 Time: 08:58 Oper: SSB Chan:777
Old Tkt: A101390411 Date: 05/19/10 Time: 08:58 Oper: SSB Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address :
Street  : SR1160   Intersection: N
Cross 1 : SR1158
Location: ST: WILLARD BORING
XST : LIBERTY RD

LOCATE ALL PEDS ON BOTH SIDES OF THE ROAD FROM CROSS STREET TO END OF ROAD
:
Grids   : 3506B8417A    3506B8417B    3506C8417A    3506C8417B    3506B8418D
Grids   : 3506C8418C    3506C8418D

Work type:MOWING
Work date:05/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: UNK  Done for: SAME

Company : NCDOT  Type: GOVT
Co addr : 5426 HWY141
City    : MARBLE State: NC Zip: 28905
Caller  : NANCY WOODARD-NO EMAIL Phone: 828-837-2742
Contact : SAME Phone:
BestTime:

Submitted date: 05/19/10 Time: 08:58
Members: BEC03  SCB01*

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10139041100A&OPR=kbeTL2vpfUJDwVBs
