
INLEMPUTA 00013B USAS 04/22/13 11:27:29 B31120103-00B SHRT NEW GRID

Ticket : B31120103  Date: 04/22/13 Time: 11:24 Oper: MYRA18 Chan: WEB
Old Tkt: B31120103  Date: 04/22/13 Time: 11:27 Oper: MYRA18 Revision: 00B

Company: CITY OF ONTARIO                Caller: MONTE FARRELL
Co Addr: 1425 S BON VIEW AV
City&St: ONTARIO, CA                    Zip: 91761      Fax: 909-395-2601
Phone: 909-395-2678 Ext:      Call back: 7A-4P
Formn: MONTE FARRELL        Phone: 909-376-4947
Email: MSANCHEZ@CI.ONTARIO.CA.US

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1505        Street:E OLIVE ST
X/ST 1 : N LEEDS AV
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0602F041
Lat/Long  : 34.090679/-117.624219 34.090645/-117.620374
          : 34.089861/-117.624226 34.089828/-117.620381
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : RELOCATE WATER SERVICE LINE
Wkend: N  Night: N
Work date: 04/24/13 Time: 11:19 Hrs notc: 047 Work hrs: 047 Priority: 1
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : CITY OF ONTARIO

Tkt Exp: 05/20/13

Mbrs : CUC01  INLEMPUTA     MWD04  ONTWTRSWR     SCG1OD SCG28T UPW01  UROCMGA
USCE34 UTWCONT       UVZRCC
