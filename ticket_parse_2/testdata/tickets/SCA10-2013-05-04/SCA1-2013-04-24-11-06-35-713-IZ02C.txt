
INLEMPUTA 00003A USAS 04/24/13 08:05:55 A31140147-00A NORM NEW GRID

Ticket : A31140147  Date: 04/24/13 Time: 08:04 Oper: MLC Chan: 100
Old Tkt: A31140147  Date: 04/24/13 Time: 08:05 Oper: MLC Revision: 00A

Company: VERIZON                        Caller: CARLO
Co Addr: 1400 E PHILLIPS BLVD
City&St: POMONA, CA                     Zip: 91768
Phone: 909-469-6387 Ext:      Call back: 7AM-5PM
Formn: MIKE HERMS

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1608        Street:N HACIENDA DR
X/ST 1 : N ISADORA WAY
MPM 1:             MPM 2:
Locat: MARK CHALK PAINT ONLY - DO NOT MARK ON STREET UTILIQUEST DOES NOT NEED TO
     : RESPOND FOR VERIZON **ALSO WTR DOES NOT RESPOND **

Excav Enters Into St/Sidewalk: N

Grids: 0602H043
Lat/Long  : 34.087079/-117.605877 34.087634/-117.604657
          : 34.086173/-117.605465 34.086729/-117.604245
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL FIBER OPTICS
Wkend: N  Night: N
Work date: 04/26/13 Time: 08:15 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : VERIZON    WO #OC 3854658

Tkt Exp: 05/22/13

Mbrs : CUC01  INLEMPUTA     MWD04  SCG1OD UCHRTCM       UROCMGA       USCE34
UTWCONT       UVZRCC
