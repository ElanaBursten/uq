
INLEMPUTA 00014A USAS 04/23/13 14:47:01 A30651029-02A NORM UPDT GRID

Ticket : A30651029  Date: 04/23/13 Time: 14:46 Oper: JMC Chan: 100
Old Tkt: A30651029  Date: 03/06/13 Time: 14:41 Oper: CAC Revision: 02A

Company: SECC                           Caller: JIM MONTGOMERY
Co Addr: 183 BUSINESS CENTER DR
City&St: CORONA, CA                     Zip: 92880      Fax: 951-734-4878
Phone: 805-310-8272 Ext:      Call back: 7AM-3PM
Formn: JIM                  Phone: 909-747-4858
Email: DAN@SECC-CORP.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1337        Street:N MOUNTAIN AVE
X/ST 1 : W HAWTHORNE ST
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 0601J0524
Lat/Long  : 34.083718/-117.670607 34.083725/-117.669622
          : 34.081114/-117.670588 34.081121/-117.669604
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : PLACING CONDUIT FOR INSTALLATION FOR NEW PHONE PAD
Wkend: N  Night: N
Work date: 04/23/13 Time: 14:46 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT AVAILABLE
Done for : VERIZON

Tkt Exp: 05/21/13

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER JIM MONTGOMERY--[MLC 03/27/13 14:02]
**RESEND**UPDATE ONLY-WORK CONT PER JIM--[JMC 04/23/13 14:46]

Mbrs : INLEMPUTA     MWD04  ONTRAF ONTWTRSWR     SCG1OD UPW01  USCE34 UTWCONT
UVZRCC
