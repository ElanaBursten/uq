
INLEMPUTA 00009A USAS 04/22/13 14:06:35 A31121064-01A RUSH CNCL GRID

Ticket : A31121064  Date: 04/22/13 Time: 14:05 Oper: TP2DBH Chan: WEB
Old Tkt: A31121064  Date: 04/22/13 Time: 13:54 Oper: TP2DBH Revision: 01A

Company: SOUTHERN CALIFORNIA GAS COMPANY (L) Caller: DUANE
Co Addr: 1981 W. LUGONIA AVE
City&St: REDLANDS, CA                   Zip: 92211
Phone: 800-423-1391 Ext:      Call back: 630-300
Formn: MICHAEL LEVARIO      Phone: 909-353-9517
Email: DHEISER@SEMPRAUTILITIES.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1687        Street:E LA DENEY DR
X/ST 1 : N BAKER AVE
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0602F0512    0602G051
Lat/Long  : 34.084390/-117.620319 34.084381/-117.615022
          : 34.083571/-117.620321 34.083563/-117.615024
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR MAIN GAS LEAK
Wkend: N  Night: N
Work date: 04/24/13 Time: 13:53 Hrs notc: 047 Work hrs: 047 Priority: 0
Instruct : CANCEL                         Permit: NOT REQUIRED
Done for : SCG 52-455429

Tkt Exp: 05/20/13

                                  COMMENTS

**RESEND**CANCEL TICKET PER DUANE--[TP2DBH 04/22/13 14:06]

Mbrs : CUC01  INLEMPUTA     MWD04  ONTRAF ONTWTRSWR     SCG1OD UPW01  USCE34
UTWCONT       UVZRCC
