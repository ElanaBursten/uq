
INLEMPUTA 00001B USAS 04/24/13 09:58:54 B31140053-00B NORM NEW GRID

Ticket : B31140053  Date: 04/24/13 Time: 09:57 Oper: SME Chan: WEB
Old Tkt: B31140053  Date: 04/24/13 Time: 09:57 Oper: DIGEXPRESS Revision: 00B

Company: JONES COVEY GROUP, INC.        Caller: APRIL WEEMS
Co Addr: 9595 LUCAS RANCH RD., SUITE 100
City&St: RANCHO CUCAMONGA, CA           Zip: 91730
Phone: 888-972-7581
Formn: TONY AGUILAR         Phone: 909-963-9841
Email: AWEEMS@JONESCOVEY.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 4325        Street:E GUASTI RD
X/ST 1 : N MILLIKEN AVE
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 0603D0724    0603E0713
Lat/Long  : 34.066951/-117.558083 34.067233/-117.552041
          : 34.064011/-117.557946 34.064293/-117.551903
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE TRANSITION SUMP/PIPING AT FUEL STATION
Wkend: N  Night: N
Work date: 04/29/13 Time: 07:00 Hrs notc: 117 Work hrs: 069 Priority: 2
Instruct : "MARK BY"                      Permit: NOT REQUIRED
Done for : TRAVEL CENTERS OF AMERICA

Tkt Exp: 05/22/13

Mbrs : ATTDSOUTH     CUC01  INLEMPUTA     KINDER4B00    LVL3CM MCISOCAL
ONTRAF ONTWTRSWR     PACPIPK       SCE17  SCG1OD TWTLH  UACRIV USCE34 UTWCONT
UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org