
INLEMPUTA 00010A USAS 04/22/13 15:04:01 A31121205-00A NORM NEW GRID

Ticket : A31121205  Date: 04/22/13 Time: 14:54 Oper: EMB Chan: 100
Old Tkt: A31121205  Date: 04/22/13 Time: 15:03 Oper: EMB Revision: 00A

Company: ROJAS BROADBAND COMMUNICATIONS Caller: ISIDRO ROJAS
Co Addr: 18515 SANTA ANA AVE
City&St: BLOOMINGTON, CA                Zip: 92316      Fax: 909-258-2735
Phone: 909-820-7003 Ext:      Call back: ANYTIME
Formn: ISIDRO               Phone: 909-368-8410
Email: ISIDROROJAS.RBC1@YAHOO.COM

State: CA County: SAN BERNARDINO  Place: ALTA LOMA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:BASELINE RD
X/ST 1 : HAVEN AVE
X/ST 2 : HERMOSA AVE
MPM 1:             MPM 2:
Locat: FRM FRONT OF SPACE # 9 AND 8 CONT N/FOR APPROXIMATELY 60FT INTO PROERTY
     : TO THE REAR OF PROPERTY ; FRM FRONT OF SPACE # 9 CONT E/TO THE S/E/COR OF
     : SPACE # 4 THEN CONT N/INTO PROPERTY FOR APPROX 60FT ALONG THE N/SIDE OF
     : THE  PROPERTY OF SPACE # 4;  BTWN SPACE # 6 & 7 FRM THE FRONT OF PROPERTY
     : CONT N/FOR APPROXIMATELY 60FT INTO THE REAR OF PROPERTY;  BTWN SPACE # 5
     : & 4 FRM THE FRONT OF PROPERTY CONT N/FOR APPROXIMATELY 60FT INTO THE REAR
     : OF PROPERTY; ALL WORK IS INSIDE THE ALTA LAGUNA MOBILE HOME PARK AT 10210
     : BASELINE RD ( CALLER STATES MHP IS LOC ON THE N/SIDE OF BASELINE RD BTWN
     : HERMOSA AVE AND HAVEN AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0573A06      0573B0613
Lat/Long  : 34.125936/-117.585121 34.125903/-117.575677
          : 34.121059/-117.585138 34.121026/-117.575695
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL FIBER OPTIC CONDUIT
Wkend: N  Night: N
Work date: 04/24/13 Time: 14:55 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT AVAILABLE
Done for : VERIZON WO# 5197P0A0CC

Tkt Exp: 05/20/13

Mbrs : CUC01  INLEMPUTA     NEXTGLAVEN    REDFLEXTRF    SCG1OD SGV01TRNS
SPRINT1       UCHRTCM       UROCMGA       USCE06 USCE83RIV     UVZPOM UVZRCC
