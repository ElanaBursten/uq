
INLEMPUTA 00015A USAS 04/24/13 15:55:18 A31141133-00A NORM NEW GRID

Ticket : A31141133  Date: 04/24/13 Time: 15:46 Oper: MMD Chan: 200
Old Tkt: A31141133  Date: 04/24/13 Time: 15:55 Oper: MMD Revision: 00A

Company: M J K CONSTRUCTION             Caller: MICHAEL
Co Addr: 2766 POMONA BLVD
City&St: POMONA, CA                     Zip: 91768
Phone: 909-721-2817 Ext: CELL Call back: ANYTIME
Formn: MICHAEL
Email: MKISSICK@MJKCONSTRUCTION.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 2161        Street:E AVION DR
X/ST 1 : S VINEYARD AVE
MPM 1:             MPM 2:
Locat: LOC IS E/OF THE X/ST -- WITHIN THE AIRPORT PROPERTY AT THE ADDRESS
     : (PROPERTY IS ON N/SIDE OF THE ST)  (E AVION ST AKA E AVION DR IN TG)

Excav Enters Into St/Sidewalk: N

Grids: 0642G0124    0642G0224    0642H01      0642H02
Grids: 0642J01      0642J02
Lat/Long  : 34.062435/-117.611664 34.062798/-117.593071
          : 34.048648/-117.611395 34.049010/-117.592802
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REMOVE UNDERGROUND FUEL TANK FARM
Wkend: N  Night: N
Work date: 05/01/13 Time: 07:00 Hrs notc: 159 Work hrs: 111 Priority: 2
Instruct : MARK BY                        Permit: 011713-01
Done for : ATLANTIC AVIATION

Tkt Exp: 05/22/13

Mbrs : INLEMPUTA     KINDER4B00    LVL3CM MCISOCAL      ONTWTRSWR     SCG1OD
USCE34 USCE83RIV     UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org