
INLEMPUTA 00004A USAS 04/23/13 07:55:58 A31130137-00A NORM NEW GRID

Ticket : A31130137  Date: 04/23/13 Time: 07:54 Oper: KRI Chan: 200
Old Tkt: A31130137  Date: 04/23/13 Time: 07:55 Oper: KRI Revision: 00A

Company: VERIZON                        Caller: ALEX BANUCHI
Co Addr: 1400 E PHILLIPS BLVD
City&St: POMONA, CA                     Zip: 91766
Phone: 909-469-6387 Ext:      Call back: 7AM-4PM
Formn: MIKE HERMS
Email: MICHAEL.C.HERMS@VERIZON.COM

State: CA County: SAN BERNARDINO  Place: ONTARIO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1730        Street:TRINITY LOOP
X/ST 1 : E 7TH ST
MPM 1:             MPM 2:
Locat: DO NOT MARK IN STREET. USE CHALK BASE PAINT ONLY. REQUESTING FOR GAS,
     : CATV & ELECT ONLY TO RESPOND

Excav Enters Into St/Sidewalk: N

Grids: 0602G0412
Lat/Long  : 34.089674/-117.612184 34.089869/-117.610913
          : 34.088688/-117.612034 34.088882/-117.610762
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : TO INSTALL FIBER OPTIC
Wkend: N  Night: N
Work date: 04/25/13 Time: 07:55 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : VERIZON-WORK ORDER # OC3852107

Tkt Exp: 05/21/13

Mbrs : CUC01  INLEMPUTA     MWD04  ONTRAF ONTWTRSWR     SCG1OD SCG28T TWTLH
UCHRTCM       UROCMGA       USCE34 UTWCONT       UVZRCC
