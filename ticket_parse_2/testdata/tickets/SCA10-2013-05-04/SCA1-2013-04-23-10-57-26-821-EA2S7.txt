
INLEMPUTA 00003A USAS 04/23/13 07:54:57 A31130134-00A NORM NEW GRID

Ticket : A31130134  Date: 04/23/13 Time: 07:44 Oper: KAW Chan: 201
Old Tkt: A31130134  Date: 04/23/13 Time: 07:54 Oper: KAW Revision: 00A

Company: DYNAMIC PLUMBING SYSTEMS       Caller: BOB BORGES
Co Addr: 5290 WINTERHAVEN AVE
City&St: RIVERSIDE, CA                  Zip: 92504      Fax: 951-343-1098
Phone: 951-343-1200 Ext:      Call back: 7AM-4PM
Formn: MATT                 Phone: 661-916-9716
Email: BBORGES@DYNAMIC.COM

State: CA County: SAN BERNARDINO  Place: CHINO
Delineated: Y
Delineated Method: STAKES
Address:             Street:APPALACHIAN ST
X/ST 1 : NORFOLK AVE
MPM 1:             MPM 2:
Locat: LOT #37 & 38 APPALACHIAN ST  X/ST NORFOLK AVE; LOT #39 THROUGH 44 NORFOLK
     : AVE  X/ST APPALACHIAN ST (NEW STS NOT SHOWN ON MAP LOC IN AN AREA BOUNDED
     : W/BY RICE AVE, N/BY PEIDMONT ST, E/BY BAYLOR AVE & S/BY EDINBORO ST)

Excav Enters Into St/Sidewalk: N

Grids: 0682B0412
Lat/Long  : 33.989826/-117.658514 33.989806/-117.654532
          : 33.989009/-117.658518 33.988988/-117.654536
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL SEWER & WATER SERVICE
Wkend: N  Night: N
Work date: 04/25/13 Time: 07:45 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : STANDARD PACIFIC

Tkt Exp: 05/21/13

Mbrs : CHI01  INLEMPUTA     SCG1OD USCE34 UTWCCHNO      UVZPOM




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org