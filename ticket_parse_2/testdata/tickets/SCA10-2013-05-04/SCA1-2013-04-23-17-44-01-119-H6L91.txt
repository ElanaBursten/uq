
INLEMPUTA 00013A USAS 04/23/13 14:43:47 A31130950-00A NORM NEW GRID

Ticket : A31130950  Date: 04/23/13 Time: 14:41 Oper: NHR Chan: 100
Old Tkt: A31130950  Date: 04/23/13 Time: 14:43 Oper: NHR Revision: 00A

Company: CUCAMONGA VALLEY WATER DISTRICT Caller: JAMES
Co Addr: 10440 ASHFORD ST
City&St: RANCHO CUCAMONGA, CA           Zip: 91730      Fax: 909-941-8069
Phone: 909-987-2591 Ext:      Call back: 730-530PM
Formn: PATRICK MILROY

State: CA County: SAN BERNARDINO  Place: RANCHO CUCAMONGA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 7250        Street:TRAVIS PL
X/ST 1 : PROVINCE ST
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0573G064     0573H063
Lat/Long  : 34.122253/-117.528462 34.122245/-117.527176
          : 34.121311/-117.528468 34.121303/-117.527183
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPLACE WATER SERVICE
Wkend: N  Night: N
Work date: 04/25/13 Time: 14:41 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : CUCAMONGA VALLEY WATER DISTRICT

Tkt Exp: 05/21/13

Mbrs : CUC01  INLEMPUTA     MCISOCAL      NEXTGLAVEN    SCG1OD SGV01TRNS
UCHRTCM       UROCMGA       USCE06 UVZRCC




DigAlert Group



,  
Tel: (909) 993-
Fax: 
Email: digalert@ieua.org
http://www.ieua.org