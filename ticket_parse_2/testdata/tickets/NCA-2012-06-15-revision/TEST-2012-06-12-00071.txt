
PBTHAN 00379 USAN 02/08/12 12:20:55 0044850 NORMAL NOTICE 

Message Number: 0044850 Received by USAN at 12:19 on 02/08/12 by DWS

Work Begins:    02/10/12 at 12:30   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: Y

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         JUSTIN PRYOR             
Company:        PRYOR                              
Address:        1013 KERRY DR                           
City:           SAN LUIS OBISPO               State: CA Zip: 96045
Business Tel:   805-440-9970                  Fax:                             
Email Address:                                                              

Nature of Work: VERTICAL BORING W/AUGER FOR FENCE POST  
Done for:       P/O CLARKE                    Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 805-440-9970           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         1017 KERRY DR
  Cross Street:         KAREN DR

    WRK LOC BK YD AREA EXT INTO PROP APP 25' FR ST 

Place: SAN LUIS OBISPO              County: SAN LUIS OBISPO      State: CA

Long/Lat Long: -120.701436 Lat:  35.259691 Long: -120.696904 Lat:  35.262648 


Sent to:
ATTCAL = AT&T TRANSMISSION CAL        CHASLO = CHARTER COMM SLO             
CTYSLO = CITY SAN LUIS OBISPO         COPSLO = CONOCO PHILLIPS - SAN LU     
LEVCAL = LEVEL 3 COMM - CALIF         MCIWSA = MCI WORLDCOM                 
PBTHAN = PACIFIC BELL HANFORD         PGESLO = PGE DISTR SAN LUIS OBISP     
SPTTEL = QWEST COMM (CA)              SLOUSD = SLO UNIFIED SCH DIST         
SCGSLO = SO CAL GAS SAN LUIS OBIS     SPRINT = SPRINT                       




At&t Ticket Id: 46313   clli code: SNLOCA01   OCC Tkt No: 0044850-000