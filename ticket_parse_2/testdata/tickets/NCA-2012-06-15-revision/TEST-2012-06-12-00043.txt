
PBTSAL 00057 USAN 02/08/12 13:31:03 0045040 NORMAL NOTICE 

Message Number: 0045040 Received by USAN at 13:29 on 02/08/12 by TRS

Work Begins:    02/10/12 at 13:45   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         TREVOR FAULKNER          
Company:        KEN SMALL INDUSTRIES               
Address:        6205 DISTRICT BLVD                      
City:           BAKERSFIELD                   State: CA Zip: 93313
Business Tel:   661-979-2401                  Fax:                             
Email Address:  TLFAULKNER@KSILP.COM                                        

Nature of Work: BACKHO TO UNCOVER WELLS                 
Done for:       AREA ENERGY                   Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 661-979-2401           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    AT A POINT 1.82MI SE AND 0.23MI E/O INT DEAD MANS GULCH RD AND SARGENTS RD
    WELL# OR5184U-2(WRK TO INCL A 150' RAD AT PT) 

Place: SAN ARDO, CO AREA            County: MONTEREY             State: CA

Long/Lat Long: -120.855862 Lat:  35.956479 Long: -120.847756 Lat:  35.96307  


Sent to:
AERA05 = AERA ENERGY 5(FORMERLY S     EXXMO2 = EXXON MOBILE PIPELINE CO     
PBTSAL = PACIFIC BELL SALINAS         PGESAL = PGE DISTR SALINAS            
TEXSAR = TEXACO EXP&PRO S ARDO        




At&t Ticket Id: 46362   clli code: SNARCA11   OCC Tkt No: 0045040-000