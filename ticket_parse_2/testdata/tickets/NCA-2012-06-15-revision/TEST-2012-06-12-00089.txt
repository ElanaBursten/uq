
PBTSJ2 00029 USAN 02/09/12 08:04:44 0016835 NORMAL NOTICE EXTENSION

Message Number: 0016835 Received by USAN at 08:04 on 02/09/12 by SGS

Work Begins:    01/19/12 at 08:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/13/12 at 23:59   Update By: 03/09/12 at 16:59

Caller:         JOHN ELDRIDGE            
Company:        PROEX EXCAVATING                   
Address:        9737 MANZANITA AVE                      
City:           BEN LOMOND                    State: CA Zip: 95005
Business Tel:   831-246-3653                  Fax: 831-336-5539                
Email Address:  PROEXCON@COMCAST.NET                                        

Nature of Work: BKHO TO INST WTR MAIN, HYDRANT, SVCS    
Done for:       SAN JOSE WATER                Explosives: N
Foreman:        SAL GUSTO                
Field Tel:                                    Cell Tel: 408-371-5510           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: ENC2011-0016             
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Entire Intersection of: HERBERT LN
         And:           PATIO DR
 ON B/SI/O HERBERT LN

    GO 420' S

Place: CAMPBELL                     County: SANTA CLARA          State: CA

Long/Lat Long: -121.926877 Lat:  37.286827 Long: -121.925491 Lat:  37.289104 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 03/13/2012 RE-MARK NO ORIG DATE 01/17/2012-SGS 02/09/2012


Sent to:
ABNECO = ABOVENET COMMUNICATIONS      CTYCAM = CITY CAMPBELL                
CTYSJO = CITY SAN JOSE                COMSJO = COMCAST-SAN JOSE             
COSCL2 = COUNTY SANTA CLARA 2         LEVCAL = LEVEL 3 COMM - CALIF         
MCIWSA = MCI WORLDCOM                 PBTSJ3 = PACIFIC BELL SAN JOSE 3      
PGECUP = PGE DISTR CUPERTINO          SJOWT2 = SAN JOSE WATER CO. 2         
SCLVLY = SANTA CLARA VALLEY WTR       WVASAN = WEST VLY SANI DIST SCLA      




At&t Ticket Id: 43957   clli code: SNJSCA14   OCC Tkt No: 0016835-001