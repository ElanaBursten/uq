
PBTPAL 00119 USAN 02/08/12 12:16:00 0044836 NORMAL NOTICE 

Message Number: 0044836 Received by USAN at 12:12 on 02/08/12 by MHS

Work Begins:    02/10/12 at 12:30   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/07/12 at 23:59   Update By: 03/05/12 at 16:59

Caller:         KATHE DEMPSEY            
Company:        LANGLEY HILL QUARRY                
Address:        4388 ALPINE RD STE "A", PORTOLA VALLEY  
City:           PORTOLA VALLEY                State: CA Zip: 94028
Business Tel:   650-851-0126                  Fax: 650-851-4126                
Email Address:  KATHE@LANGLEYHILL.COM                                       

Nature of Work: DIG FOR SEPTIC INVESTIGATION            
Done for:       VINCE CARINO                  Explosives: N
Foreman:        ROBERTO MUNOZ            
Field Tel:                                    Cell Tel: 650-464-1971           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         140 FARM RD
  Cross Street:         HIDDEN VALLEY LN

    A 100 SQUARE FOOT AREA ON LEFT SIDE/O DR/WAY--PER CALLER  

Place: WOODSIDE                     County: SAN MATEO            State: CA

Long/Lat Long: -122.2319   Lat:  37.386136 Long: -122.229933 Lat:  37.387574 


Sent to:
CWSATH = CALIF WTR SVC-ATHERTON       COMPAL = COMCAST-PALO ALTO            
COSMTO = COUNTY SAN MATEO             PBTPAL = PACIFIC BELL PALO ALTO       
PGEBEL = PGE DISTR BELMONT            WBASAN = WEST BAY SANITARY            




At&t Ticket Id: 46184   clli code: MNPKCA11   OCC Tkt No: 0044836-000