
PBTCHI 00008 USAN 02/09/12 08:08:38 0017185 NORMAL NOTICE EXTENSION

Message Number: 0017185 Received by USAN at 08:07 on 02/09/12 by CAP

Work Begins:    01/23/12 at 08:00   Notice: 038 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 03/13/12 at 23:59   Update By: 03/09/12 at 16:59

Caller:         BILL WARMBRODT           
Company:        PG&E                               
Address:        3600 MEADOWVIEW DR, \                   
City:           REDDING                       State: CA Zip: 96002
Business Tel:   530-941-1197                  Fax:                             
Email Address:  WDW8@PGE.COM                                                

Nature of Work: DIG TO REP GAS PIPE                     
Done for:       SAME PM:41360507              Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 530-941-1197           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location: 
Entire Intersection of: BALLS FERRY RD
         And:           EAST ST


    WRK ENT NW/PORTION/O THE INT EXT 25'INTO PROP ON B/SI/O THE INT

Place: ANDERSON                     County: SHASTA               State: CA

Long/Lat Long: -122.295531 Lat:  40.448265 Long: -122.293913 Lat:  40.449501 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 03/13/2012 RE-MARK NO ORIG DATE 01/17/2012-CAP 02/09/2012
PER BILL

Sent to:
ANDIRR = ANDERSON-COTTONWOOD IRR      CTYAND = CITY ANDERSON                
FALRED = FALCON CTV REDDING           FRNCO2 = FRONTIER COMM. NORTH         
PBTCHI = PACIFIC BELL CHICO           PGERED = PGE DISTR REDDING            




At&t Ticket Id: 43819   clli code: ARSNCA11   OCC Tkt No: 0017185-001