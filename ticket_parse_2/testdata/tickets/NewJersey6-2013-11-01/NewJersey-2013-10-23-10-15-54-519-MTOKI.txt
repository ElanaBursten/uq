
UtiliQ-NJ  NJ132350391  10/23/2013 10:15:57  00008

Assigned to ORUUNJ for 08/23/2013


New Jersey One Call System        SEQUENCE NUMBER 0009    CDC = REC 


Transmit:  Date: 08/23/13   At: 09:32 


*** R O U T I N E         *** Request No.: 132350391 


Operators Notified: 
BAN     = VERIZON                       PSOK    = PUBLIC SERVICE ELECTRIC &      
REC     = ROCKLAND ELECTRIC COMPANY     TG1     = TENNESSEE GAS PIPELINE CO      
UW4     = UWNJ-NE                        


Start Date/Time:    08/29/13   At 00:15  Expiration Date: 10/29/13 


Location Information: 
County: BERGEN                 Municipality: UPPER SADDLE RIVER 
Subdivision/Community:  
Street:               70 OAK DR 
Nearest Intersection: FERN DELL 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL POLE(S) 
Block:                Lot:                Depth: 8FT 
Extent of Work: 25FT RADIUS OF POLE 56911/38409. 
Remarks:  
  Working For Contact:  JOE BOULAY 


Working For: ORANGE &ROCKLAND 
Address:     390 W RT59 
City:        SPRING VALLEY, NY  10977 
Phone:       845-422-0803   Ext:  


Excavator Information: 
Caller:      JOSEPH BOULAY 
Phone:       845-422-0803   Ext:  


Excavator:   ORANGE & ROCKLAND UTILITIES 
Address:     390 W RT.59 
City:        SPRING VALLEY, NY  10977 
Phone:       845-422-0803   Ext:          Fax:  845-577-3549 
Cellular:    845-422-0803 
Email:       boulayj@oru.com 
End Request
