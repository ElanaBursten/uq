
UtiliQ-NJ  NJ132350125  10/23/2013 10:15:57  00003

Assigned to ORUUNJ for 08/23/2013


New Jersey One Call System        SEQUENCE NUMBER 0004    CDC = REC 


Transmit:  Date: 08/23/13   At: 07:51 


*** E M E R G E N C Y     *** Request No.: 132350125 


Operators Notified: 
BAN     = VERIZON                       EPC     = EASTERN PROPANE                
GP0     = JERSEY CENTRAL POWER & LI     PSOK    = PUBLIC SERVICE ELECTRIC &      
PVW     = PASSAIC VALLEY WATER COMM     REC     = ROCKLAND ELECTRIC COMPANY      
TK5     = CABLEVISION OF NJ              


Start Date/Time:    08/23/13   At 08:00  Expiration Date:  


Location Information: 
County: PASSAIC                Municipality: WEST MILFORD 
Subdivision/Community:  
Street:               0 MORSETOWN ROAD 
Nearest Intersection: BELLEAU GATEWAY 
Other Intersection:    
Lat/Lon: 
Type of Work: EMERGENCY - REPAIR/REPLACE BROKEN POLE 
Block:                Lot:                Depth: 8FT 
Extent of Work: M/O LOCATED 5FT E OF C/L OF INTERSECTION.  30FT RADIUS OF
  POLE NUMBER 49253/41127. 
Remarks:  
  SAFETY HAZARD 
  Working For Contact:  MICHAEL EKIZIAN 


Working For: ORANGE AND ROCKLAND UTILITIES 
Address:     390 W ROUTE 59 
City:        SPRING VALLEY, NY  10977 
Phone:       845-577-3432   Ext:  


Excavator Information: 
Caller:      MICHAEL EKIZIAN 
Phone:       845-577-3432   Ext:  


Excavator:   ORANGE AND ROCKLAND UTILITIES 
Address:     390 W ROUTE 59 
City:        SPRING VALLEY, NY  10977 
Phone:       845-577-3432   Ext:          Fax:  845-577-3089 
Cellular:    845-577-3432 
Email:       ekizianm@oru.com 
End Request
