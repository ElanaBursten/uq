
UtiliQ-NJ  NJ132350168  10/23/2013 10:15:57  00004

Assigned to ORUUNJ for 08/23/2013


New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = REC 


Transmit:  Date: 08/23/13   At: 08:11 


*** R O U T I N E         *** Request No.: 132350168 


Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
BCD     = BERGEN COUNTY D.P.W.          MAH     = TOWNSHIP OF MAHWAH             
PSOK    = PUBLIC SERVICE ELECTRIC &     REC     = ROCKLAND ELECTRIC COMPANY      
TK4     = CABLEVISION OF ROCKLAND/R      


Start Date/Time:    08/29/13   At 07:00  Expiration Date: 10/29/13 


Location Information: 
County: BERGEN                 Municipality: MAHWAH 
Subdivision/Community:  
Street:               2 AIRMONT AVE 
Nearest Intersection: GRENADIER DR 
Other Intersection:    
Lat/Lon: 
Type of Work: REPAIR SEPTIC SYSTEM 
Block: 80             Lot:  4             Depth: 10FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  MARK SELFON 


Working For: HOMEOWNER 
Address:     2 AIRMONT AVE 
City:        MAHWAH, NJ  07430 
Phone:       917-881-1593   Ext:  


Excavator Information: 
Caller:      EDWARD BOWER 
Phone:       800-428-6166   Ext: 112 


Excavator:   ALL COUNTY RESOURCE MGMT 
Address:     99 MAPLE GRANGE ROAD 
City:        VERNON, NJ  07462 
Phone:       800-428-6166   Ext:          Fax:  973-764-6404 
Cellular:    800-428-6166 
Email:       ebower@wrenvironmental.com 
End Request
