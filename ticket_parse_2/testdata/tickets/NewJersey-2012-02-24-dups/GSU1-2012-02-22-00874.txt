
New Jersey One Call System        SEQUENCE NUMBER 0052    CDC = GSC 

Transmit:  Date: 02/22/12   At: 11:12 

*** U P D A T E           *** Request No.: 120530732  Of Request No.: 120381131 
                          *** No Response Of Request No.:  120381131 

Operators Notified: 
BAN     = VERIZON                       GSC     = COMCAST CABLEVISION OF GA      
MOO     = MOORESTOWN TOWNSHIP           P34     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    02/11/12   At 08:00  Expiration Date: 04/12/12 

Location Information: 
County: BURLINGTON             Municipality: MOORESTOWN 
Subdivision/Community:  
Street:               5 SHELTER ROCK PL 
Nearest Intersection: SENTINEL ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: STUMP GRINDING 
Block:                Lot:                Depth: 3FT 
Extent of Work: CURB TO 4FT BEHIND CURB. 
Remarks:  
  CALLER STATES ONLY ELECTRIC COMPANY RESPONDED FOR MARKOUT 
  Working For Contact:  SUSAN EPPOLITE 

Working For: MOORESTOWN DEPT OF PUBLIC WORK 
Address:     601 E 3RD ST 
City:        MOORESTOWN, NJ  08057 
Phone:       856-235-3520   Ext:  

Excavator Information: 
Caller:      SUSAN EPPOLITE 
Phone:       856-235-3520   Ext:  

Excavator:   MOORESTOWN DEPT OF PUBLIC WORK 
Address:     601 E 3RD ST 
City:        MOORESTOWN, NJ  08057 
Phone:       856-235-3520   Ext:          Fax:  856-231-1514 
Cellular:    856-235-3520 
Email:       seppolite@moorestown.nj.us 
End Request 
