
New Jersey One Call System        SEQUENCE NUMBER 0030    CDC = EG1 

Transmit:  Date: 02/22/12   At: 10:09 

*** U P D A T E           *** Request No.: 120530498  Of Request No.: 120530392 
                          *** Cancellation Of Request No.: 120530392 

Operators Notified: 
AGT     = ALGON/TEXAS EASTERN GAS T     BAN     = VERIZON                        
BUK     = BUCKEYE PARTNERS              CC7     = COMCAST CABLEVISION OF NE      
EG1     = ELIZABETHTOWN GAS COMPANY     EW1     = NEW JERSEY AMERICAN WATER      
P35     = PUBLIC SERVICE ELECTRIC &     SU2     = SUNOCO PIPELINE                

Start Date/Time:    02/28/12   At 00:15  Expiration Date: 04/26/12 

Location Information: 
County: UNION                  Municipality: LINDEN 
Subdivision/Community:  
Street:               0 RANGE ROAD 
Nearest Intersection: LOWER ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL WELL(S) 
Block:                Lot:                Depth: 50FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS S FOR 1050FT
  ON RANGE RD.  CURB TO 270FT BEHIND W CURB.
  CANCEL 02/22/12 10:07 am: CANCEL DUE TO INCORRECT INFORMATION 
Remarks:  
  CANCEL DUE TO INCORRECT INFORMATION ON TICKET 
  Working For Contact:  MARTY WARD 

Working For: TRC 
Address:     57 E WILLOW ST 
City:        MILLBURN, NJ  07041 
Phone:       973-615-6190   Ext:  

Excavator Information: 
Caller:      JAMIE WYCKOFF 
Phone:       631-874-2112   Ext:  

Excavator:   LAND,AIR,WATER ENVIRONMENTAL S 
Address:     32 CHICHESTER AVE 
City:        CENTER MORICHES, NY  11934 
Phone:       631-874-2112   Ext:          Fax:  631-874-4547 
Cellular:     
Email:       mainoffice@LAWES.org 
End Request 
