
New Jersey One Call System        SEQUENCE NUMBER 0009    CDC = CAM 

Transmit:  Date: 02/23/12   At: 08:50 

*** U P D A T E           *** Request No.: 120540243  Of Request No.: 120440961 

Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
CAM     = CABLEVISION OF MONMOUTH       GPP     = JERSEY CENTRAL POWER & LI      
MAQ     = BOROUGH OF MANASQUAN          MCI     = MCI                            
MMF     = ZAYO GROUP LLC                NJN     = NEW JERSEY NATURAL GAS CO      
NJR     = NEW JERSEY TRANSIT            NWS     = NEW JERSEY WATER SUPPLY A      
SOM     = SOUTH MONMOUTH REGIONAL S     TYC     = TYCOM (US) INC.                
USS     = SPRINT                         

Start Date/Time:    02/17/12   At 00:15  Expiration Date: 04/18/12 

Location Information: 
County: MONMOUTH               Municipality: MANASQUAN 
Subdivision/Community:  
Street:               0 STATE RTE 71 
Nearest Intersection: STOCKTON LAKE BLVD 
Other Intersection:   BLAKEY AVE 
Lat/Lon: 
Type of Work: INSTALL GAS MAIN 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O ENTIRE LENGTH OF STATE RT 71 FROM C/L OF BLAKEY AVENUE
  TO C/L OF STOCKLAND LAKE BLVD INCLUDING ALL INTERSECTIONS AND 100FT IN
  ALL DIRECTIONS FROM C/L OF INTERSECTIONS AND EXTENDS 400FT S FROM C/L
  OF STOCKLAND LAKE BLVD AND RT 71.  CURB TO 10FT BEHIND ALL CURBS.  CURB
  TO CURB. 
Remarks:  
  CALLER STATES WATER AND ELECTRIC COMPANIES MISMARKED 
  Working For Contact:  RICK HOFFMAN 

Working For: NJNG 
Address:     1415 WYCKOFF ROAD 
City:        WALL, NJ  07719 
Phone:       732-919-8156   Ext:  

Excavator Information: 
Caller:      NICOLE COLANDRO 
Phone:       732-222-4400   Ext: 243 

Excavator:   J F KIELY CONSTRUCTION COMPANY 
Address:     700 MCCLELLAN ST 
City:        LONG BRANCH, NJ  07740 
Phone:       732-222-4400   Ext:          Fax:  732-229-2353 
Cellular:     
Email:       ncolandro@jfkiely.com 
End Request 
