
New Jersey One Call System        SEQUENCE NUMBER 0027    CDC = CAM 

Transmit:  Date: 02/23/12   At: 10:14 

*** U P D A T E           *** Request No.: 120540556  Of Request No.: 120411373 
                          *** No Response Of Request No.:  120411373 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
GPE     = JERSEY CENTRAL POWER & LI     NJN     = NEW JERSEY NATURAL GAS CO      
SCNJ4   = NJ AMERICAN WATER              

Start Date/Time:    02/16/12   At 07:15  Expiration Date: 04/17/12 

Location Information: 
County: OCEAN                  Municipality: LAKEWOOD 
Subdivision/Community:  
Street:               50 - 56 BUCHANAN ST 
Nearest Intersection: VAN BUREN AVE 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL PHONE SERVICE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO 20FT BEHIND CURB. 
Remarks:  
  CONSECUTIVE EVEN 
  CALLER STATES NO RESPONSE FROM CABLE,GAS JCP&L AND WATER COMPANY 
  Working For Contact:  BILL ROTH 

Working For: VERIZON FIOS 
Address:     183 BROAD ST 
City:        RED BANK, NJ  07701 
Phone:       732-530-6760   Ext:  

Excavator Information: 
Caller:      BRANDY BLAZER 
Phone:       609-548-0021   Ext:  

Excavator:   J FLETCHER CREAMER & SON 
Address:     PO BOX 617 
City:        HAMMONTON, NJ  08037 
Phone:       609-548-0021   Ext:          Fax:  609-561-6507 
Cellular:    609-548-0021 
Email:       tcorcoran@jfcson.com 
End Request 
