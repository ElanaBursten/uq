
New Jersey One Call System        SEQUENCE NUMBER 0080    CDC = CC7 

Transmit:  Date: 02/22/12   At: 11:06 

*** U P D A T E           *** Request No.: 120530713  Of Request No.: 120530681 
                          *** Cancellation Of Request No.: 120530681 

Operators Notified: 
BAN     = VERIZON                       CC7     = COMCAST CABLEVISION OF NE      
PS6     = PUBLIC SERVICE ELECTRIC &     SCNJ6   = NJ AMERICAN WATER              

Start Date/Time:    02/28/12   At 07:30  Expiration Date: 04/26/12 

Location Information: 
County: ESSEX                  Municipality: WEST ORANGE 
Subdivision/Community: CRYSTAL WOODS 
Street:               240 CLARKEN DR 
Nearest Intersection: DOCKERY DR 
Other Intersection:    
Lat/Lon: 
Type of Work: REPAIR DRAINAGE 
Block:                Lot:                Depth: 2FT 
Extent of Work: CURB TO 200FT BEHIND CURB.
  CANCEL 02/22/12 11:06 am: MARKED AREA IN WHITE. 
Remarks:  
  Working For Contact:  GALE BRAUNSTEIN 

Working For: TAYLOR MANAGEMENT 
Address:     80 S JEFFERSON ROAD 
City:        WHIPPANY, NJ  07981 
Phone:       973-267-9000   Ext:  

Excavator Information: 
Caller:      DONNA PECCI 
Phone:       973-515-9577   Ext:  

Excavator:   ALL SEASONS MAINTENANCE 
Address:     9 BEHRENS DR 
City:        WHIPPANY, NJ  07981 
Phone:       973-515-9577   Ext:          Fax:  973-515-4447 
Cellular:    973-390-4219 
Email:       ALLSEASONSMAINT@AOL.COM 
End Request 
