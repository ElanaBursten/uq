
TEST01 2 ALOC 08/21/2012 10:06:00 122340003 Normal NOGRID

=========// ALOC LOCATE REQUEST //=========

TICKET NUMBER--[122340003]
OLD TICKET NUM-[]

MESSAGE TYPE--[Normal] LEAD TIME--[48]
PREPARED------[08/21/12] AT  [10:24] BY [ru.test01]

CONTRACTOR--[ALABAMA 811] CALLER--[JENNIFER LEE]
ADDRESS-----[3104 BATES LANE]
CITY--------[FULTONDALE] STATE--[AL] ZIP--[35068]
CALL BACK---[] PHONE--[(205) 731-3210]
CONTACT-----[JENNIFER LEE] PHONE--[(205) 731-3200]
CONTACT FAX-[] EMAIL--[]

WORK TO BEGIN--[08/23/12] AT  [10:15] STATE--[AL]
COUNTY---[JEFFERSON] PLACE--[FULTONDALE]

ADDRESS--[3104 BATES LN]
NEAREST INTERSECTION--------[CENTRAL PKWY]
LATITUDE--[33.64468]  LONGITUDE--[-86.816049]
SECONDARY LATITUDE--[33.64651]  SECONDARY LONGITUDE--[-86.813709]

ADDITIONAL ADDRESS IN LOCATION--[N]

DIRECTIONS--[LOCATE THE ENTIRE LOT]
REMARKS-----[]

WORK TYPE--[Pole, Instl] DONE FOR--[ALABAMA 811]
EXTENT-----[]

EXPLOSIVES--[N]  WHITE PAINT--[N] AT INTERSECTION--[false]

SUBDIVISION--[]

UTILITIES NOTIFIED--
CODE        NAME                                                    
--------------------------------------------------------------------
APC1CL      Alabama Power - Birmingham Division               
BHWT01      Birmingham Water Works                            
SCBH01      ATT/D - Birmingham - (334) 850-7761               
CHAL01      Charter Communications of Alabama                 
FULG01      Fultondale Gas Board                              
TEST01      Alabama 811 TEST                                  



