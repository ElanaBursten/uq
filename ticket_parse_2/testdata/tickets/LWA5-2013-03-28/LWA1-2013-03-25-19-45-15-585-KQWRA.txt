


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Monday, March 25, 2013 4:43 PM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13067184

Ticket No: 13067184               2 FULL BUSINESS DAYS      UPDATE 
Send To: NOANT01    Seq No:    2  Map Ref: -NOANT01 2ND EDIT,             
Update Of: 13063115 
Transmit      Date:  3/25/13   Time:  4:43 PM    Op: orbert 
Original Call Date:  3/25/13   Time:  4:39 PM    Op: orbert 
Work to Begin Date:  3/28/13   Time: 12:00 AM 

State: WA            County: GRANT                   Place: QUINCY 
Address:             Street: ROAD 11 NW 
Nearest Intersecting Street: ROAD Q NW 

Twp: 20N     Rng: 24E     Sect-Qtr: 7,8 
Twp: 20N     Rng: 24E     Sect-Qtr: 3-SW,6-SE,5-SW-SE,4-SW-SE,7-NE,10-NW,9-NW-N* 

Type of Work: INSTALL FIBER OPTICS MAINLINE Location of Work: SITE STARTS AT SE CORNER OF ABV INTER. MARK E ALONG RD 11
: APX 8800FT ON S SIDE AS MARKED IN WHITE AT SITE    CLR GAVE TRSQ     CALLER
: STATES   MARKS WERE APX 1000FT SHORT   CALLER STATES  GRANT CO PUD,
: FRONTIER,  LEVEL 3,  360 NETWORKS, NOANET,  AND ZAYO  NEED ONLY RESPOND
: PLEASE CALL STEVE BEFORE LOCATING FOR SPECIFICS. 

Remarks: BEST INFO AVAIL 
: UPDATE FOR INCOMPLETE MARKS            NEED MARKS 

Company     : UNDERGROUND SPECIALTIES          Best Time:   
Contact Name: STEVE SMITH                      Phone: (509)767-0521 
Alt. Contact: STEVE-CELL -  1ST CA             Phone: (425)239-3938 
Contact Fax :  
Work Being Done For: QWEST
Additional Members:  
360NET03   CNG12      GRAPUD04   GTE07      LEVL301    QCBI01     QUINCY01 
 ZAYO02 


