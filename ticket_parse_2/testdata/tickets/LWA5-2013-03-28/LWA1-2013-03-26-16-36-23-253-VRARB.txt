


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Tuesday, March 26, 2013 1:33 PM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13068105

Ticket No: 13068105               2 FULL BUSINESS DAYS 
Send To: NOANT01    Seq No:    2  Map Ref:  

Transmit      Date:  3/26/13   Time:  1:32 PM    Op: orbert 
Original Call Date:  3/26/13   Time:  1:27 PM    Op: orbert 
Work to Begin Date:  3/29/13   Time: 12:00 AM 

State: WA            County: ADAMS                   Place: OTHELLO 
Address:    818      Street: S GRACE LANE 
Nearest Intersecting Street: BENCH RD 

Twp: 15N     Rng: 29E     Sect-Qtr: 14 
Twp: 15N     Rng: 29E     Sect-Qtr: 12-SW,11-SW-SE,10-SE,22-NE,15-SE-NE,24-NW,2* 

Type of Work: PLACING BURIED SERVICE WIRE 
Location of Work: MARK  AREA  THAT HAS BEEN WHITELINED     AT THE ABOVE
: ADDRESS   GOING OUT TO TERMINAL   AT BENCH RD   APPROX 500FT 

Remarks: BEST INFO AVAIL      2ND OF 3 LOCATES   USED TRSQ TO MAP 
: UNABLE TO PINPOINT 

Company     : SPOKANE DITCH AND CABLE          Best Time:   
Contact Name: VICKI TURNER                     Phone: (509)483-1272 
Alt. Contact:                                  Phone:  
Contact Fax : (509)483-2525 
Work Being Done For: CENTURY LINK 
Additional Members:  
AC01       AVISTA03   BIGBND01   ECBI01     QLNWA01    WSDOT09 


