


-----Original Message-----
From: wa@occinc.com [mailto:wa@occinc.com] 
Sent: Tuesday, March 26, 2013 1:20 PM
To: onecall@noanet.net
Subject: 2 FULL BUSINESS DAYS 13068025

Ticket No: 13068025               2 FULL BUSINESS DAYS 
Send To: NOANT02    Seq No:    7  Map Ref:  

Transmit      Date:  3/26/13   Time:  1:20 PM    Op: orbert 
Original Call Date:  3/26/13   Time:  1:17 PM    Op: orbert 
Work to Begin Date:  3/29/13   Time: 12:00 AM 

State: WA            County: LINCOLN                 Place: ODESSA 
Address:             Street: SR 21 
Nearest Intersecting Street: HOPP RD 

Twp: 21N     Rng: 33E     Sect-Qtr: 17,20,19 
Twp:  *MORE  Rng: 32E     Sect-Qtr: 13-SE,25-NE,24-SE-NE 

Type of Work: UNDERGROUND TELEPHONE LINE CONSTRUCTION 
Location of Work: 500FT NORTH FROM INTERSECTING ROAD.    MARK FROM PHONE PED
: 175550   ON THE EAST SIDE OF SR 21  SOUTH FOR 700FT   THIS JOB IS A TOTAL OF
: 3600FT  TO PHONE PED   204950   LOCATE IS MARKED WITH WHITE PAINT   CENTURY
: LINK NEED NOT RESPOND   WO 1982 372501 

Remarks: USED TRSQ TO MAP
:  

Company     : STRONG UTILITY, INC              Best Time:  FAX 
Contact Name: MARK STRONG                      Phone: (208)659-2188 
Alt. Contact:                                  Phone:  
Contact Fax : (208)687-8529
Work Being Done For: CENTURY LINK & POTELCO INC Additional Members:  
AVISTA15   INLND03    PTI36      WAPOW04    YELCN08 


