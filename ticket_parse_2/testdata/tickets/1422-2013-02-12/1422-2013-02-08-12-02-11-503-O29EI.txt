
SCEDZ05 6 SC811 Voice 02/08/2013 11:58:00 AM 1302080050 Extraord Circ 

Notice Number:    1302080050        Old Notice:                        
Sequence:         6                 Created By:      RGO               

Created:          02/08/13 12:01 PM                                     
Work Date:        02/13/13 11:59 PM                                     
Update on:        03/04/2013        Good Through:    03/07/2013        

Caller Information:
SC811
810 DUTCH SQ BLVD
COLUMBIA, SC 29210
Company Fax:                        Type:            Other             
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:0
Caller Email:     RDOTMAN@HOTMAIL.COM                                   

Site Contact Information:
RANDI OLSEN                              Phone:(803) 939-1117 Ext:0
Site Contact Email:  frontdesk@sc1pups.org                                 
CallBack:  8am to 5pm

Excavation Information:
SC    County:  SUMTER               Place:  SUMTER            
Street:  TOMLINSON RD               Address In Instructions:true              
Intersection:  NORWOOD                                               
Subdivision:   TESTING                                               

Lat/Long: 33.962794,-79.948551
Second:  0, 0

Explosives:  Y Premark:  N Drilling/Boring:  N Near Railroad:  Y

Work Type:     SEE INSTRUCTIONS                                        
Work Done By:  SC811                Duration:        DAY               
Work Done For: SC811-1                                               

Instructions:
THIS IS ONLY A TEST                                                           

Directions:
THIS IS ONLY A TEST                                                           

Remarks:
THIS IS ONLY A TEST                                                           

Member Utilities Notified:
FTCZ80 CAM60* CNNG15* BERZ20* ATT09* BAT64* CPLZ05 
HOT21* QWC42* EMBZ11* SCEDZ05* SRWS80* JMW83* 11TCH49* 


