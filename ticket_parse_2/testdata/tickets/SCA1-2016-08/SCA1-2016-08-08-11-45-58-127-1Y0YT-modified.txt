
UQSTSO 00410A USAS 08/08/16 08:45:33 A62210229-00A SHRT NEW GRID

Ticket : A62210229  Date: 08/08/16 Time: 08:34 Oper: CLA Chan: 100
Old Tkt: A62210229  Date: 08/08/16 Time: 08:45 Oper: CLA Revision: 00A

Company: H/O VAHIDI, CARRIE             Caller: CARRIE VAHIDI
Co Addr: 17603 THISTLE HILL CT
City&St: RIVERSIDE, CA                  Zip: 92504
Phone: 609-458-1181 Ext:      Call back: ANYTIME
Formn: CARRIE VAHIDI
Email: CVAHIDI@HOTMAIL.COM

State: CA County: RIVERSIDE       Place: RIVERSIDE
Delineated: N
Address: 17603       Street:THISTLE HILL CT
X/ST 1 : LOG HILL RD
MPM 1:             MPM 2:
Locat: IN FRONT OF ADDRESS

Excav Enters Into St/Sidewalk: N

Grids: 0746A064     0746A072
Lat/Long  : 33.862929/-117.355926 33.862886/-117.354486
          : 33.861273/-117.355975 33.861231/-117.354534
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : AUGER INSTALL FENCE POST & GATE - EMERGENCY
Wkend: Y  Night: Y
Work date: 08/10/16 Time: 08:30 Hrs notc: 047 Work hrs: 047 Priority: 1
Instruct : MARK BY                        Permit: NOT REQUIRED
Wrk order: ABC-123-000 XYZ
Done for : H/O VAHIDI, CARRIE

Tkt Exp: 09/05/16

Mbrs : ATTDSOUTH     SCG1OK USCE77 UTWCNRIV      WMW01
