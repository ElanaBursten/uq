
UQSTSO 00078A USAS 08/08/16 07:15:11 A62210059-00A EMER NEW GRID

Ticket : A62210059  Date: 08/08/16 Time: 07:11 Oper: LAC Chan: 100
Old Tkt: A62210059  Date: 08/08/16 Time: 07:15 Oper: LAC Revision: 00A

Company: SOUTHERN CALIFORNIA GAS        Caller: LESLIE BATTLES
Co Addr: 9400 OAKDALE AVE
City&St: CHATSWORTH, CA                 Zip: 91313
Phone: 800-427-1919 Ext:      Call back: ANYTIME
Formn: RYAN LITTLE          Phone: 818-701-3333
Email: LBATTLES@SEMPRAUTILITIES.COM

State: CA County: SANTA BARBARA   Place: SOLVANG
Delineated: Y
Delineated Method: WHITEPAINT
Address: 1555        Street:DOVE MEADOW RD
X/ST 1 : SAN MIL RD
MPM 1:             MPM 2:
Locat: X/ST PROVIDED BY USA  **CREW ENROUTE**

Excav Enters Into St/Sidewalk: Y

Grids: 0920H0324    0920J0313
Lat/Long  : 34.628944/-120.111525 34.628886/-120.106092
          : 34.626311/-120.111553 34.626253/-120.106120
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : EMERGENCY REPAIR GAS LEAK OVER SERVICE
Wkend: N  Night: N
Work date: 08/08/16 Time: 07:11 Hrs notc: 000 Work hrs: 000 Priority: 9
Instruct : NOW                            Permit: NOT REQUIRED
Done for : SOUTHERN CALIFORNIA GAS

Tkt Exp: 09/05/16

Mbrs : CMCDISTFO     PGE01  SCG4UK SYR01DIST     UCMC09 UVZSTABAR
