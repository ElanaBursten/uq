
XYZZY     00000 USAN 02/01/10 16:09:25 0000037 NORMAL NOTICE 

Message Number: 0000037 Received by USAN at 14:26 on 01/27/10 by INTERNET

Work Begins:    02/01/10 at 14:45   Notice: 039 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 02/24/10 at 17:00   Update By: 02/22/10 at 16:59

Caller:         JOSEPH WEST              
Company:        USA NORTH                          
Address:        4090 NELSON AVE, STE A                  
City:           CONCORD                       State: CA Zip: 94520
Business Tel:   925-798-9504                  Fax:                             
Email Address:  JWEST@USAN.ORG                                              

Nature of Work: DIG TO REP WTR VALVE                    
Done for:       SAME                          Explosives: N
Foreman:        DAN JONES                
Field Tel:                                    Cell Tel: 925-798-9504           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    WRK AREA BOUNDED BY: PORT CHICAGO HWY ON W, 1/2MI N/O BATES AVE ON S,
    3/4MI N/O BATES AVE ON N, & 3/4MI E/O PORT CHICAGO HWY ON E (NO STS IN
    WORK AREA)

Place: CONCORD, CO AREA             County: CONTRA COSTA         State: CA

Long/Lat Long: -122.031045 Lat:  38.021353 Long: -122.022505 Lat:  38.026036 


Sent to:
ASTBRO = ASTOUND BROADBAND            CENTRL = CENTRAL SANITARY             
CTYCND = CITY CONCORD                 COMCND = COMCAST CONCORD              
CCOWTR = CONTRA COSTA WATER           CPNPIP = CPN PIPELINE CO.             
CPNPI2 = CPN PIPELINE CO. 2           CPNPI3 = CPN PIPELINE CO. 3           
EBWWWC = EAST BAY WATER WALNUT CR     PBTWAL = PACIFIC BELL WALNUT CRK      
PGECND = PGE DISTR CONCORD            EQUTRA = SHELL PIPELINE CO, LP -      
TESREM = TESORO REFINING & MARKET     



