
SF     00015 POCS 01/20/10 13:00:41 20100201072-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201072]-[000] Channel#--[1255015][0302]

Message Type--[NEW][EXCAVATION][ROUTINE]

County--[BUCKS]           Municipality--[WARMINSTER TWP]
Work Site--[1189 LYNDA LN]
     Nearest Intersection--[SLIGHT RD]
     Second Intersection--[WEBER RD]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [BTWN SLIGHT RD & WEBER RD.  EXCAVATING STREET TO HOUSE SIDEWALK.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.227896/-75.101100,40.224181/-75.101180,40.224547/-75.098946,
      40.226739/-75.098946]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201072]

Type of Work--[REPLACE SEWER LINE]                           Depth--[4-6FT]
Extent of Excavation--[]                Method of Excavation--[BH]
Street--[ ] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[X] Other--[]

              Lawful Start Dates--[27-Jan-10] Through [03-Feb-10]
    Scheduled Excavation Date--[27-Jan-10] Dig Time--[0900] Duration--[1 DAY]
                         Response Due Date--[26-Jan-10]

Caller--[SUSIE BASKIN]                     Phone--[215-257-1087] Ext--[]
Excavator--[BASKIN & SON PLUMBING]          Homeowner/Business--[B]
Address--[307 TOWER RD]
City--[SELLERSVILLE]                 State--[PA] Zip--[18960]
FAX--[]              Email--[plumberz88@aol.com]
Work Being Done For--[DODOVIDIO]

Person to Contact--[SUSIE BASKIN]              Phone--[215-257-1087] Ext--[]
Best Time to Call--[ANYTIME]

Prepared--[20-Jan-10] at [1300] by [DEBRA BOOTH]

Remarks--
     []

KD 0  KD =PECO WRTR        SF 0  SF =COMCAST IVYLAND  TC 0  TC =TRANSCO GAS PL 
WA 0  WA =WARMINSTER TMA   WRM0  WRM=WARMINSTER TWP   YI 0  YI =VERIZON HRSM

Serial Number--[20100201072]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========