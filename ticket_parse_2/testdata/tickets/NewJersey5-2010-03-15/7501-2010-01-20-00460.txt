
KB     00044 POCS 01/20/10 13:04:13 20100201084-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201084]-[000] Channel#--[1301001][0168]

Message Type--[NEW][EXCAVATION][ROUTINE]

County--[PHILADELPHIA]    Municipality--[PHILADELPHIA CITY]          Ward--[20]
Work Site--[11TH ST]
     Nearest Intersection--[W MONTGOMERY AVE]
     Second Intersection--[]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [WORKING AT POLE # 74106D WHICH IS APPX 50FT-200FT N OF W MONTGOMERY ST IN
      THE SIDEWALK ON THE E SIDE OF 11TH ST.]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [39.979610/-75.152114,39.981320/-75.151780,39.981202/-75.150780,
      39.979374/-75.151241]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201084]

Type of Work--[REPL EXISTING POLE]                           Depth--[6FT]
Extent of Excavation--[24IN DIA]        Method of Excavation--[AUGER]
Street--[ ] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[ ] Other--[]

              Lawful Start Dates--[25-Jan-10] Through [03-Feb-10]
      Scheduled Excavation Date--[25-Jan-10] Dig Time--[0800] Duration--[]
                         Response Due Date--[22-Jan-10]

Caller--[ED CONVERY]                       Phone--[215-519-3066] Ext--[]
Excavator--[PECO ENERGY]                    Homeowner/Business--[B]
Address--[4000 G ST]
City--[PHILADELPHIA]                 State--[PA] Zip--[19124]
FAX--[215-965-8889]  Email--[none]
Work Being Done For--[PECO]

Person to Contact--[ED CONVERY]                Phone--[215-519-3066] Ext--[]
Best Time to Call--[0800-1600]

Prepared--[20-Jan-10] at [1304] by [MARY ANNE YURTAL]

Job Number--[06818421]
Remarks--
     []

BL 0  BL =COMCAST CABLE    KB 0  KB =PECO DNGN        LKC0  LKC=LEVEL 3 COMM   
PD 0  PD =PHILA C WTR DPT  PSD0  PSD=PHILADELPHIA ST  PZ 0  PZ =PGW PHLA       
SEP0  SEP=SEPTA            YB 0  YB =VERIZON PA INC

Serial Number--[20100201084]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========