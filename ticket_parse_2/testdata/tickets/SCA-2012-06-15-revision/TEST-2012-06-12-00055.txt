
ATTD18LAN 00014A USAS 02/29/12 08:07:32 A20600123-00A NORM NEW GRID

Ticket : A20600123  Date: 02/29/12 Time: 08:03 Oper: CLA Chan: 100
Old Tkt: A20600123  Date: 02/29/12 Time: 08:07 Oper: CLA Revision: 00A

Company: KORMX INC                      Caller: DANIEL
Co Addr: 19314 AVENIDA DEL SOL
City&St: WALNUT, CA                     Zip: 91789      Fax: 909-595-9086
Phone: 909-771-7079 Ext:      Call back: ANYTIME
Formn: FRANK URIBE
Email: FRANK@KORMXINC.COM

State: CA County: LOS ANGELES     Place: LA CANADA FLINTRIDGE
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:VINETA AVE
X/ST 1 : HOUSEMAN ST
MPM 1:             MPM 2:
Locat: 4713, 4644, 4633, 4636 VINETA AVE      S/OF HOUSEMAN ST
     : IF CALLING BACK ASK FOR FRANK URIBE

Excav Enters Into St/Sidewalk: Y

Grids: 0535D033     0535D0413
Lat/Long  : 34.204927/-118.188425 34.204991/-118.187276
          : 34.200050/-118.188153 34.200115/-118.187003
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPL CURB, GUTTER, SIDEWALK
Wkend: Y  Night: N
Work date: 03/05/12 Time: 07:00 Hrs notc: 118 Work hrs: 070 Priority: 2
Instruct : MARK BY                        Permit: NOT AVAILABLE
Done for : C/OF LA CANADA FLINTRIDGE

Tkt Exp: 03/28/12

Mbrs : ATTD18LAN     CHAMPBRBND    FMW01  LACOTS LCI01  MCR01  SCE13  SCG4QH
UCHRCOM       USCE27 UVZIRWN       UVZSFERN      VWC98

At&t Ticket Id: 40944   clli code: LACRCA11   OCC Tkt No: A20600123-00A