
ATTD124OR 00034A USAS 02/08/12 10:02:57 A20390404-00A NORM NEW GRID

Ticket : A20390404  Date: 02/08/12 Time: 09:58 Oper: ZIM Chan: 100
Old Tkt: A20390404  Date: 02/08/12 Time: 10:02 Oper: ZIM Revision: 00A

Company: SW ADMINISTRATORS              Caller: MIKE MALIK - SC GAS
Co Addr:
City&St:                                Zip:  Fax: 714-634-7227
Phone: 949-448-8513 Ext:      Call back: 6AM-8AM
Formn: UNK
Email: MMALIK@SEMPRAUTILITIES.COM

State: CA County: ORANGE          Place: LAGUNA NIGUEL
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:RIDGEVIEW DR
X/ST 1 : HIGHLANDS AVE
MPM 1:             MPM 2:
Locat: S/MOST INTER/OF RIDGEVIEW DR AND HIGHLANDS AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0951E0334
Lat/Long  : 33.538256/-117.723373 33.537823/-117.722597
          : 33.537548/-117.723768 33.537115/-117.722992
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : POSSIBLE BORING; REPL GAS MAIN
Wkend: N  Night: N
Work date: 02/10/12 Time: 10:00 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: PENDING
Done for : SC GAS W R # 2069895

Tkt Exp: 03/07/12

Mbrs : ATTD124OR     COXRSM LAGN01 MNW01  SCG2XK USCE04 UVZHTB

At&t Ticket Id: 44066   clli code: LGNGCA12   OCC Tkt No: A20390404-00A