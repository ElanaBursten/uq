
ATTD19LAN 00032A USAS 02/08/12 10:43:48 A20390492-00A NORM NEW GRID

Ticket : A20390492  Date: 02/08/12 Time: 10:39 Oper: NHR Chan: 100
Old Tkt: A20390492  Date: 02/08/12 Time: 10:43 Oper: NHR Revision: 00A

Company: C/OF EL MONTE                  Caller: JOE HAZLETT
Co Addr: PO BOX 6008
City&St: EL MONTE, CA                   Zip: 91734      Fax: 626-580-2253
Phone: 626-580-2250 Ext:      Call back: ANYTIME
Formn: JOE HAZLETT          Phone: 626-580-2250

State: CA County: LOS ANGELES     Place: EL MONTE
Delineated: N
Address: 2326        Street:SILVERBAY AVE
X/ST 1 : RAMER ST
MPM 1:             MPM 2:
Locat: WORK IS IN THE PARKWAY   *AREA WILL BE DELINEATED*

Excav Enters Into St/Sidewalk: N

Grids: 0637E044     0637F043
Lat/Long  : 34.043869/-118.018684 34.044474/-118.017787
          : 34.042885/-118.018020 34.043489/-118.017124
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : STUMP GRINDING
Wkend: N  Night: N
Work date: 02/14/12 Time: 07:00 Hrs notc: 140 Work hrs: 092 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF EL MONTE

Tkt Exp: 03/07/12

Mbrs : ATTD19LAN     CDW01  CSDSGBL       ELM01  ELM02  SCE13  SCG4PW SCG4QB
SGV30  USCE07 USCE08 UUCT01 UVZSFS

At&t Ticket Id: 44414   clli code: ELMNCA01   OCC Tkt No: A20390492-00A