
ATTD01OR 00001B USAS 02/29/12 08:33:45 A10210154-16B NORM UPDT GRID

Ticket : A10210154  Date: 02/29/12 Time: 08:33 Oper: SIALIC Chan: WEB
Old Tkt: A10210154  Date: 01/21/11 Time: 08:42 Oper: LAC Revision: 16B

Company: SHAWNAN                        Caller: KIM
Co Addr: 12240 WOODRUFF AVE
City&St: DOWNEY, CA                     Zip: 90241      Fax: 562-803-9955
Phone: 562-803-9977 Ext:      Call back: ANYTIME
Formn: ART                  Phone: 562-760-0110 Ext: CELL
Email: ACCOUNTING@SHAWNAN.COM

State: CA County: LOS ANGELES     Place: LA MIRADA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:VALLEY VIEW AVE
X/ST 1 : ALONDRA BLVD
MPM 1:             MPM 2:
Locat: INTER OF VALLEY VIEW AVE & ALONDRA BLVD; BOTH SIDES OF  VALLEY VIEW AVE
     : FRM ALONDRA BLVD N/FOR APPROX 100FT & S/FOR  APPROX 500FT; BOTH SIDES OF
     : ALONDRA BLVD FRM VALLEY VIEW AVE W/FOR APPROX 100FT & E/TO APPROX 100FT
     : E/OF CANARY AVE;  MARKING BACK OF SIDEWALK TO BACK OF SIDEWALK FOR ENTIRE
     : DISTANCES   * MARK WITH CHALK BASE PAINT ONLY *

Excav Enters Into St/Sidewalk: Y

Grids: 0737E05123
Lat/Long  : 33.888447/-118.029930 33.890088/-118.023851
          : 33.886137/-118.029307 33.887779/-118.023227
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REMOVE & REPL ASPHALT & BASE
Wkend: N  Night: N
Work date: 02/29/12 Time: 08:33 Hrs notc: 000 Work hrs: 001 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : CITY OF LA MIRADA

Tkt Exp: 03/28/12

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 02/09/11 13:52]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 03/07/11 11:31]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 04/04/11 14:30]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 04/28/11 15:55]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 05/24/11 13:34]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 06/20/11 14:57]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 07/15/11 15:07]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 08/11/11 14:32]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 09/06/11 07:57]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 10/03/11 16:27]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 10/27/11 14:19]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 11/22/11 14:03]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 12/15/11 10:10]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 01/10/12 09:14]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 02/03/12 07:46]
**RESEND**UPDATE ONLY-WORK CONT PER KYM--[SIALIC 02/29/12 08:33]

Mbrs : ATTD01OR      CER86DIST     CHELA  CRMSNPIP      LACOTS PARADYNE
SCG2WV SCG2XD SFSDIST       SWSWHIT       UCHRCLB       USCE46 USCE47 UVZCERR
UVZSFS XOCOMMLA

At&t Ticket Id: 42473   clli code: BNPKCA11   OCC Tkt No: A10210154-16B