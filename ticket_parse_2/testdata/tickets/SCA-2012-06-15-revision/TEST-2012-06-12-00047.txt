
ATTD22LAS 00027A USAS 02/21/12 09:16:31 A20520436-00A RUSH NEW GRID

Ticket : A20520436  Date: 02/21/12 Time: 09:12 Oper: EVA Chan: 100
Old Tkt: A20520436  Date: 02/21/12 Time: 09:16 Oper: EVA Revision: 00A

Company: SCW2M - GOLDEN STATE WATER     Caller: ELISEO QUINTANILLA
Co Addr: 16245 BROADWAY
City&St: GARDENA, CA                    Zip: 90248      Fax: 310-811-8476
Phone: 310-660-0320 Ext: 108  Call back: 7:30AM - 4PM M-F
Formn: ELISEO               Phone: 310-237-3485

State: CA County: LOS ANGELES     Place: HAWTHORNE
Delineated: Y
Delineated Method: WHITEPAINT
Address: 13629       Street:CERISE AVE
X/ST 1 : W 135TH ST
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 0733F031
Lat/Long  : 33.907757/-118.334088 33.907776/-118.332595
          : 33.906713/-118.334074 33.906732/-118.332581
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR WATER SERVICE
Wkend: N  Night: N
Work date: 02/21/12 Time: 09:15 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : VALID PERMIT ONLY              Permit: NOT AVAILABLE
Done for : SCW2M - GOLDEN STATE WATER

Tkt Exp: 03/20/12

Mbrs : ATTD22LAS     LACOTS SCG32F SCW2M  USCE44 UTWCTOR

At&t Ticket Id: 63948   clli code: HWTHCA01   OCC Tkt No: A20520436-00A