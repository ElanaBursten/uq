
ATTD28SD 00332A USAS 02/08/12 16:54:54 A20391318-00A NORM NEW GRID

Ticket : A20391318  Date: 02/08/12 Time: 16:50 Oper: JMC Chan: 100
Old Tkt: A20391318  Date: 02/08/12 Time: 16:54 Oper: JMC Revision: 00A

Company: PREMIER POOLS AND SPAS         Caller: CINDY LAZERSON
Co Addr: 1945 CAMINO VIDA ROBLE, STE C
City&St: CARLSBAD, CA                   Zip: 92008      Fax: 760-476-0049
Phone: 760-476-0008 Ext:      Call back: 9-5
Formn: ROGER                Phone: 619-277-4010
Email: CLAZERSON@PPAS.COM

State: CA County: SAN DIEGO       Place: FALLBROOK
Delineated: N
Address: 2369        Street:VERN DR
X/ST 1 : CALLE CANONERO
MPM 1:             MPM 2:
Locat: WILL BE DELINEATED 2/13/10 BY 7AM

Excav Enters Into St/Sidewalk: N

Grids: 1028F0624    1028G0613
Lat/Long  : 33.353353/-117.173220 33.353357/-117.171063
          : 33.351020/-117.173215 33.351024/-117.171058
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : DIG FOR POOL AND SPA
Wkend: N  Night: N
Work date: 02/13/12 Time: 07:00 Hrs notc: 110 Work hrs: 062 Priority: 2
Instruct : MARK BY                        Permit: PENDING
Done for : H/O- TIEHEN

Tkt Exp: 03/07/12

Mbrs : ATTD28SD      RAI88  SDG01  UTWCCARL

At&t Ticket Id: 46207   clli code: FLBKCA12   OCC Tkt No: A20391318-00A