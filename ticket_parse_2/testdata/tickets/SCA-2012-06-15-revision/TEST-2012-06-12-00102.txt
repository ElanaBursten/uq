
ATTD13OR 00011B USAS 02/08/12 09:17:17 A20110203-01B NORM UPDT GRID

Ticket : A20110203  Date: 02/08/12 Time: 09:17 Oper: SKYMUN Chan: WEB
Old Tkt: A20110203  Date: 01/11/12 Time: 08:39 Oper: KRI Revision: 01B

Company: CITY OF CORONA DWP             Caller: BRANDI RUVALCABA
Co Addr: 755 CORPORATION YARD WAY
City&St: CORONA, CA                     Zip: 92880
Phone: 951-736-2234 Ext:      Call back: 7A-5P
Formn: HECTOR URIAS         Phone: 951-712-0458 Ext: CELL
Email: BRANDI.RUVALCABA@CI.CORONA.CA.US

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:RIMPAU AVE
X/ST 1 : TABER ST
MPM 1:             MPM 2:
Locat: S/W SIDE OF RIMPAU AVE AT APPROX 40 YARDS S/W OF TABER ST

Excav Enters Into St/Sidewalk: Y

Grids: 0773G023     0773G031
Lat/Long  : 33.845066/-117.543495 33.844630/-117.542717
          : 33.844358/-117.543892 33.843922/-117.543114
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL CATHODIC PROTECTION
Wkend: N  Night: N
Work date: 02/08/12 Time: 09:17 Hrs notc: 000 Work hrs: 001 Priority: 2
Instruct : WORK CONTINUING                Permit: NOT REQUIRED
Done for : CITY OF CORONA DWP

Tkt Exp: 03/07/12

                                  COMMENTS

**RESEND**UPDATE ONLY-WORK CONT PER SKYLER--[SKYMUN 02/08/12 09:17]

Mbrs : ATTD13OR      COR19  EVW37  GSUSUNESY     SCG1CO TWCWRIV       USCE34
UVZMENIF      UVZPERS

At&t Ticket Id: 43767   clli code: CORNCA11   OCC Tkt No: A20110203-01B