
UTIL14 00005 VUPSb 02/24/11 12:15:01 B105500744-00B          NORMAL

Ticket No:  B105500744-00B                        NEW  GRID NORM LREQ
Transmit        Date: 02/24/11   Time: 12:15 PM   Op: WKDUER
Call            Date: 02/24/11   Time: 12:03 PM
Due By          Date: 03/01/11   Time: 07:00 AM
Update By       Date: 03/15/11   Time: 11:59 PM
Expires         Date: 03/18/11   Time: 07:00 AM
Old Tkt No: B105500744
Original Call   Date: 02/24/11   Time: 12:03 PM   Op: WKDUER

City/Co:ACCOMACK              Place:MELFA                               State:VA
Subdivision:                                           Lot: 7
Address:                      Street: BOBTOWN RD
Cross 1:     DINGLEYS MILL RD

Type of Work:   ELECTRIC SECONDARY - REPAIR OR REPLACE
Work Done For:  ANEC
Excavation area:LOCATE FROM POLE 41690/67819 GOING VERTICALLY ACROSS BOBTOWN RD
                WHERE A NEW POLE WILL BE SET AND THEN FROM NEW POLE TO METER
                SOCKET ON HOUSE. LOCATED 0.5 MILES DOWN BOBTOWN RD ON THE LEFT.
Instructions:

Whitelined: N   Blasting: N   Boring: N

Company:        R&P LUCAS UNDERGROUND UTILTIES INC        Type: CONT
Co. Address:    PO BOX 90  First Time: N
City:           MELFA  State:VA  Zip:23410
Company Phone:  757-787-3510
Contact Name:   KRISTIN DUER                Contact Phone:757-787-2525
Field Contact:  BURTON ROGERS
Fld. Contact Phone:757-710-0621

Mapbook:
Grids:    3739A7546A     3739A7546B     3739A7546C     3739A7546D
Grids:    3740C7545A     3740D7545A     3740C7546C     3740C7546D
Grids:    3740D7546A     3740D7546B     3740D7546C     3740D7546D
Grids:

Members:
ANE901 = A & N ELECTRIC COOPERATIV(ANE) CHC901 = CHARTER COMMUNICATIONS (CHC)
VZN905 = VERIZON (VZN)

Seq No:   5 B
