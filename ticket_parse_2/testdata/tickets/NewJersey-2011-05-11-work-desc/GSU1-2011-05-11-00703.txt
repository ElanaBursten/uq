
New Jersey One Call System        SEQUENCE NUMBER 0013    CDC = CC3 

Transmit:  Date: 05/11/11   At: 10:00 

*** R O U T I N E         *** Request No.: 111310587 

Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
CC3     = COMCAST CABLEVISION OF CE     GPS     = JERSEY CENTRAL POWER & LI      
MOT     = MONROE TOWNSHIP UTIL. DEP     P29     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    05/17/11   At 08:15  Expiration Date: 07/15/11 

Location Information: 
County: MIDDLESEX              Municipality: MONROE 
Subdivision/Community: ROSSMOOR 
Street:               0 OLD NASSAU ROAD 
Nearest Intersection: MIDDLEBURY LANE 
Other Intersection:    
Lat/Lon: 
Type of Work: REPAIR ELECTRIC 
Block:                Lot:                Depth: 6FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 265FT N.  CURB
  TO 30FT BEHIND E CURB. 
Remarks:  
  Working For Contact:  TONY BENANTI 

Working For: JCP&L 
Address:     1345 ENGLISHTOWN RD 
City:        OLD BRIDGE, NJ  08857 
Phone:       732-723-6600   Ext:  

Excavator Information: 
Caller:      TONY BENANTI 
Phone:       732-723-6600   Ext:  

Excavator:   JERSEY CENTRAL POWER & LIGHT 
Address:     1345 ENGLISHTOWN ROAD 
City:        OLD BRIDGE, NJ  08857 
Phone:       732-723-6600   Ext:          Fax:  732-723-6633 
Cellular:    908-415-3359 
Email:       jfahoury@firstenergycorp.com 
End Request 
