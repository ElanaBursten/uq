
New Jersey One Call System        SEQUENCE NUMBER 0129    CDC = NJN 

Transmit:  Date: 05/11/11   At: 10:01 

*** R O U T I N E         *** Request No.: 111310619 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
GPE     = JERSEY CENTRAL POWER & LI     JTM     = JACKSON TOWNSHIP M.U.A.        
NJN     = NEW JERSEY NATURAL GAS CO      

Start Date/Time:    05/17/11   At 09:00  Expiration Date: 07/15/11 

Location Information: 
County: OCEAN                  Municipality: JACKSON 
Subdivision/Community: WEST GATE RETIREMENT COMMUNITY 
Street:               29 PINE VALLEY ROAD 
Nearest Intersection: CYPRESS POINT LANE 
Other Intersection:    
Lat/Lon: 
Type of Work: TERMITE TREATMENT 
Block:                Lot:                Depth: 1FT 
Extent of Work: 1FT PERIMETER OF FOUNDATION. 
Remarks:  
  Working For Contact:  RICH PIACENTINO 

Working For: RICH PIACENTINO 
Address:     29 PINE VALLEY ROAD 
City:        JACKSON, NJ  08527 
Phone:       732-961-6953   Ext:  

Excavator Information: 
Caller:      RAYMOND TRIMBLE 
Phone:       732-240-6249   Ext:  

Excavator:   FAST TERMITE & PEST CONTROL 
Address:     428 NIXON AVE 
City:        BAYVILLE, NJ  08721 
Phone:       732-240-6249   Ext:          Fax:  
Cellular:    732-684-0607 
Email:       fastpest@verizon.net 
End Request 
