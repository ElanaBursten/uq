
New Jersey One Call System        SEQUENCE NUMBER 0131    CDC = NJN 

Transmit:  Date: 05/11/11   At: 10:02 

*** R O U T I N E         *** Request No.: 111310638 

Operators Notified: 
ADC     = COMCAST CABLEVISION OF TO     BAN     = VERIZON                        
DVM     = TOMS RIVER MUNICIPAL UTIL     GPC     = JERSEY CENTRAL POWER & LI      
NJN     = NEW JERSEY NATURAL GAS CO     UW7     = /UWTR-TOMS RIVER               

Start Date/Time:    05/17/11   At 00:15  Expiration Date: 07/15/11 

Location Information: 
County: OCEAN                  Municipality: TOMS RIVER 
Subdivision/Community:  
Street:               27 HEATHER ROAD 
Nearest Intersection: S SHORE DR 
Other Intersection:    
Lat/Lon: 
Type of Work: REMOVE OIL TANK(S) 
Block: 442.01         Lot:  16            Depth: 30FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  JEANNE CAMPBELL 

Working For: JEAN KABA 
Address:     27 HEATHER ROAD 
City:        SILVERTONI, NJ  08753 
Phone:       732-647-4579   Ext:  

Excavator Information: 
Caller:      KRISTIN SCICUTELLA 
Phone:       732-659-1000   Ext:  

Excavator:   RENOVA ENVIRONMENTAL SVCS 
Address:     1181 SYCAMORE AVE 
City:        TINTON FALLS, NJ  07724 
Phone:       732-659-1000   Ext:          Fax:  732-659-1034 
Cellular:     
Email:       kscicutella@NJenvironmental.com 
End Request 
