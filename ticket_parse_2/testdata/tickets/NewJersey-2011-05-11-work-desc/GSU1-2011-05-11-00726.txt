
New Jersey One Call System        SEQUENCE NUMBER 0130    CDC = NJN 

Transmit:  Date: 05/11/11   At: 10:02 

*** R O U T I N E         *** Request No.: 111310616 

Operators Notified: 
BAN     = VERIZON                       BIT     = BRICK TOWNSHIP MUNICIPAL       
CC4     = COMCAST CABLEVISION OF NE     GPP     = JERSEY CENTRAL POWER & LI      
NJN     = NEW JERSEY NATURAL GAS CO     OCE     = OCEAN COUNTY ENGINEERING       

Start Date/Time:    05/17/11   At 08:00  Expiration Date: 07/15/11 

Location Information: 
County: OCEAN                  Municipality: BRICK 
Subdivision/Community: BURNT TAVERN MANOR 
Street:               24 CENTRAL BLVD E 
Nearest Intersection: BRANDYWINE CT 
Other Intersection:    
Lat/Lon: 
Type of Work: REPLACE DRAINAGE 
Block:                Lot:                Depth: 6FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  JOE ABBATO 

Working For: BRICK TWP D.P.W. 
Address:     836 RIDGE ROAD 
City:        BRICK, NJ  08724 
Phone:       732-451-4060   Ext:  

Excavator Information: 
Caller:      JOE ABBATO 
Phone:       732-451-4060   Ext:  

Excavator:   BRICK TWP D.P.W. 
Address:     836 RIDGE ROAD 
City:        BRICK, NJ  08724 
Phone:       732-451-4060   Ext:          Fax:  732-458-0757 
Cellular:    732-262-1087 
Email:       loliveri@twp.brick.nj.us 
End Request 
