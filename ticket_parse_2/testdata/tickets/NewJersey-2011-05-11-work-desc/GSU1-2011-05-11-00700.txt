
New Jersey One Call System        SEQUENCE NUMBER 0095    CDC = SJG 

Transmit:  Date: 05/11/11   At: 09:59 

*** R O U T I N E         *** Request No.: 111310623 

Operators Notified: 
ACT     = ATLANTIC COUNTY DEPARTMEN     AE1     = ATLANTIC CITY ELECTRIC         
ATC     = ATLANTIC CITY MUNICIPAL U     BAN     = VERIZON                        
NJ3     = NEW JERSEY AMERICAN WATER     SCV     = COMCAST CABLE (VINELAND)       
SJG     = SOUTH JERSEY GAS COMPANY      USS     = SPRINT                         

Start Date/Time:    05/17/11   At 08:00  Expiration Date: 07/15/11 

Location Information: 
County: ATLANTIC               Municipality: ABSECON 
Subdivision/Community:  
Street:               100 HIGHLAND BLVD 
Nearest Intersection: AMBASSADOR DR 
Other Intersection:   SEASIDE AVE 
Lat/Lon: 
Type of Work: INSTALL DRIVEWAY 
Block: 199            Lot:  1             Depth: 2FT 
Extent of Work: CURB TO CURB.  CURB TO ENTIRE PROPERTY.  CORNER LOT: WORKING
  ON BOTH STREETS. 
Remarks:  
  Working For Contact:  CHARLES SABATINI 

Working For: HOMEOWNER 
Address:     100 HIGHLAND BLVD 
City:        ABSECON, NJ  08201 
Phone:       609-485-0536   Ext:  

Excavator Information: 
Caller:      CHARLES SABATINI 
Phone:       609-485-0536   Ext:  

Excavator:   CHARLES SABATINI 
Address:     100 HIGHLAND BLVD 
City:        ABSECON, NJ  08201 
Phone:       609-485-0536   Ext:          Fax:  
Cellular:    609-412-4195 
Email:       charles.sabatini@rve.com 
End Request 
