
KD     00035 POCS 01/20/10 13:03:39 20100201079-000 NEW  XCAV DSGN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============

Serial Number--[20100201079]-[000] Channel#--[1301WEB][0131]

Message Type--[NEW][EXCAVATION][DESIGN]

County--[BUCKS]           Municipality--[WARMINSTER TWP]
Work Site--[ROUTE 263]
     Nearest Intersection--[OLD YORK RD]
     Second Intersection--[BRISTOL RD]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [RT 263 - TOTAL ROADWAY FROM OLD YORK ROAD IN WARMINSTER TWP THRU WARWICK
      TWP TO APPROX 1000 FT INTO BUCKINGHAM TWP]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.228422/-75.095590,40.226628/-75.092425,40.224358/-75.095638,
      40.224944/-75.096932]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20100201079]

Type of Work--[ROADWAY-DRAINAGE-SIGNAL RECONSTRUCTION]       Depth--[]
Extent of Excavation--[]                Method of Excavation--[]
Street--[X] Sidewalk--[X] Pub Prop--[X] Pvt Prop--[ ] Other--[R/W-HWY]

              Lawful Start Dates--[         ] Through [         ]
                      Scheduled Excavation Date--[DESIGN]
                         Response Due Date--[03-Feb-10]

Caller--[KEVIN NEHF]                       Phone--[717-691-1340] Ext--[]
Excavator--[KCI TECHNOLOGIES INC]           Homeowner/Business--[B]
Address--[5001 LOUISE DR STE 201]
City--[MECHANICSBURG]                State--[PA] Zip--[17055]
FAX--[717-691-3470]  Email--[kevin.nehf@kci.com]
Work Being Done For--[PENN DOT]

Person to Contact--[SCOTT RABOCI]              Phone--[717-691-1340] Ext--[3103]
Best Time to Call--[0800-1600]

Prepared--[20-Jan-10] at [1303] by [KEVIN NEHF]

Remarks--
     [THIS IS ROADWAY AND DRAINAGE AND SIGNAL RECONSTRUCTION OF RT 263 FORM OLD
      YORK RD IN WARMINSTER TWP THRU WARWICK TWP AND APPROX 1000 FT INTO
      BUCKINGHAM TWP. PLEASE MARK UTILITIES IN THE FIELD AND SEND APPLICABLE
      PLANS.]

KD 0  KD =PECO WRTR        NO 0  NO =N WALES WA       SF 0  SF =COMCAST IVYLAND
TC 0  TC =TRANSCO GAS PL   WA 0  WA =WARMINSTER TMA   WRM0  WRM=WARMINSTER TWP 
YI 0  YI =VERIZON HRSM

Serial Number--[20100201079]-[000]

========== Copyright (c) 2010 by Pennsylvania One Call System, Inc. ==========