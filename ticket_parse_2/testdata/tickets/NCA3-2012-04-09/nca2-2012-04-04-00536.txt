
PBTPAL 00137 USAN 04/03/12 14:53:07 0112594 NORMAL NOTICE 

Message Number: 0112594 Received by USAN at 14:47 on 04/03/12 by SMV

Work Begins:    04/05/12 at 15:00   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 05/01/12 at 23:59   Update By: 04/27/12 at 16:59

Caller:         JEFF CALL                
Company:        ROYAL POOLS                        
Address:        2258 CAMDEN AVE, SAN JOSE               
City:           SAN JOSE                      State: CA Zip: 95124
Business Tel:   408-371-8000                  Fax: 408-377-3341                
Email Address:                                                              

Nature of Work: EXC TO INST POOL                        
Done for:       P/O FISK                      Explosives: N
Foreman:        CALLER                   
Field Tel:                                    Cell Tel: 408-371-8000           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: PENDING                  
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         266 SANTANDER CT
  Cross Street:         MARGARITA CT

    BK/O PROP

Place: LOS ALTOS                    County: SANTA CLARA          State: CA

Long/Lat Long: -122.120377 Lat:  37.400321 Long: -122.119961 Lat:  37.400738 


Sent to:
CWSLAL = CALIF WTR SVC-LOS ALTOS      CTYLAL = CITY LOS ALTOS               
CTYMTN = CITY MOUNTAIN VIEW           CTYPAL = CITY PALO ALTO               
COMPAL = COMCAST-PALO ALTO            PBTPAL = PACIFIC BELL PALO ALTO       
PGECUP = PGE DISTR CUPERTINO          SCLVLY = SANTA CLARA VALLEY WTR       
SFOWTR = SF PUC WTR SUPPLY/TRTMNT     




At&t Ticket Id: 25146130   clli code: LSATCA11