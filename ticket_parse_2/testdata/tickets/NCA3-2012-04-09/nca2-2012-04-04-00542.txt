
PBTNPA 00053 USAN 04/03/12 14:55:28 0112595 NORMAL NOTICE 

Message Number: 0112595 Received by USAN at 14:46 on 04/03/12 by VLERMA

Work Begins:    04/16/12 at 08:00   Notice: 083 hrs      Priority: 2
Night Work: N    Weekend Work: N

Expires: 05/01/12 at 23:59   Update By: 04/27/12 at 16:59

Caller:         JOHN FRITZ               
Company:        SWAN POOLS                         
Address:        1295 BOULEVARD WAY STE "C"              
City:           WALNUT CREEK                  State: CA Zip: 94595
Business Tel:   925-934-7926                  Fax: 925-934-5845                
Email Address:  JFRITZ@SWANNORCAL.COM                                       

Nature of Work: EXC FOR BK YD POOL                      
Done for:       P/O BECKER                    Explosives: N
Foreman:        MIKE MOREY               
Field Tel:                                    Cell Tel: 925-766-8249           
Area Premarked: Y   Premark Method: WHITE PAINT                                
Permit Type:    CITY                          Number: 12BLD00189               
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 
Street Address:         762 MARY CT
  Cross Street:         KEARNEY ST

    WRK AT THE BK YD/O THE PROP 

Place: BENICIA                      County: SOLANO               State: CA

Long/Lat Long: -122.148261 Lat:  38.087288 Long: -122.145736 Lat:  38.089784 


Sent to:
CTYBEN = CITY BENICIA                 COMVJO = COMCAST-VALLEJO              
PBTNPA = PACIFIC BELL NAPA            PGEVJO = PGE DISTR VALLEJO            
TERDEX = TERRADEX INC.                




At&t Ticket Id: 25146143   clli code: BNCICA11