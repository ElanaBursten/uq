
PEH51 2 SC811 Voice 12/30/2013 02:37:00 PM 1312301807 Emerg-Resend 

Notice Number:    1312301807        Old Notice:      1312301004        
Sequence:         2                 Created By:      JJR               

Created:          12/30/13 02:38 PM                                     
Work Date:        12/30/13 02:37 PM                                     
Update on:                          Good Through:                      

Caller Information:
PALMETTO ELECTIC
111 MATHEWS DRIVE
HILTON HEAD ISLAND, SC 29925
Company Fax:                        Type:            Excavator         
Caller:           JOYCE GRANT         Phone:         (843) 681-0078 Ext:
Caller Email:     jgrant@palmetto.coop                                  

Site Contact Information:
DAVID WHITE                              Phone:(843) 384-3104 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  BEAUFORT             Place:  HILTON HEAD ISLAND
Street:  25 CALIBOGUE CAY RD        Address In Instructions:false             
Intersection:  PLANTATION DR                                         
Subdivision:   SEA PINES                                             

Lat/Long: 32.14314,-80.79754
Second:  0, 0

Explosives:  N Premark:  N Drilling/Boring:  N Near Railroad:  N

Work Type:     ELECTRIC, REPLACE SERVICE                                   
Work Done By:  PALMETTO ELECTIC     Duration:        APPROX 2-5 DAYS   
Work Done For:                                                       

Instructions:
MARK STARTING AT FEED CAN #76631011 LOCATED IN FRONT OF THE PROPERTY TO       
TRANSFORMER #76632013 //

**EMERGENCY SITUATION DUE TO: INTERRUPTION OF       
ESSENTIAL UTILITY SERVICE // CREW IS ON THE SITE // MARK ASAP**               

Directions:
ADD RD: N/A                                                                   

Remarks:
**EMERG RESENDING DUE TO// ADDING CONTRACTOR INFRATECH AS ALSO RESPONSIBLE    
DIGGING // THANK YOU                                                          

Member Utilities Notified:
HRGZ57 SPSD29 PEH51 TWBZ51 

