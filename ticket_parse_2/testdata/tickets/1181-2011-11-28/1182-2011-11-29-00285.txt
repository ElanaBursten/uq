
KORTERRA JOB UTILIQUEST_WV-SMTP-1 WV1133300454 Seq: 284 11/29/2011 11:05:17
Locate Request For PEB
----------------------------------------------------------------------------
Ticket Number:    1133300454        Old Ticket:
Source:           Voice             Sequence:         3
Type:             Normal            Date:             11/29/11 08:21

Company Information
----------------------------------------------------------------------------
DAVID LUTMAN                        Type:             Resident
610 N MILDRED ST                    Company Phone:    (304) 702-2500
RANSON, WV 25438                    Company Fax:
Caller Name:      DAVID LUTMAN      Caller Phone:     (304) 702-2500
Contact Name:     DAVID LUTMAN      Contact Phone:    (304) 702-2500
Caller Email:
Contact Email:

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        12/01/11 08:15
County:           JEFFERSON         Done For:         DAVID LUTMAN
Place:            CHARLES TOWN      Depth:            1FT
Street:           MAPLE AVE
Intersection:     W LIBERTY ST
Nature of Work:
Explosives:       No                Boring:           No
White Lined:      No                Length:

Driving Directions
----------------------------------------------------------------------------
PROPERTY IS ON NORTH WEST CORNER OF ABOVE INTER.


Remarks
----------------------------------------------------------------------------
WORKING ON RIGHT SIDE OF HOUSE (IF FACING HOUSE) IN THE FENCED IN AREA. LOCATE
ENTIRE RIGHT SIDE OF HOUSE.


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually
----------------------------------------------------------------------------
PEB   Firstenergy Corp                                  No
GAN   Frontier - A Citizens Communications Company      No
CTC   Charles Town Utility Board                        No
DTC   Elantic Telecom Inc                               No


Location
----------------------------------------------------------------------------
Latitude:         39.286596         Longitude:        -77.86984
Second Latitude:  39.288835         Second Longitude: -77.867795

Grids
----------------------------------------------------------------------------
39170077520B 39172077520A 39172077520C 39172077520D




-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.