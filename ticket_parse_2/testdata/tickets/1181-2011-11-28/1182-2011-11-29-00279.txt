
KORTERRA JOB UTILIQUEST_WV-SMTP-1 WV1133300459 Seq: 277 11/29/2011 11:04:21
Locate Request For PEB
----------------------------------------------------------------------------
Ticket Number:    1133300459        Old Ticket:
Source:           Email             Sequence:         4
Type:             Normal            Date:             11/29/11 08:31

Company Information
----------------------------------------------------------------------------
ALL-LINK COMMUNICATIONS LLC         Type:             Contractor
P.O BOX 209                         Company Phone:    (434) 985-4933
RUCKERSVILLE, VA 22968              Company Fax:      (434) 465-6850
Caller Name:      SANDRA KINDER     Caller Phone:     (434) 985-4933
Contact Name:     CORINNA REILLY    Contact Phone:    (434) 985-4933
Caller Email:     ZONE2LOCATES@ALLLINKCOMM.COM
Contact Email:    ZONE2LOCATES@ALLLINKCOMM.COM

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        12/01/11 08:30
County:           Berkeley          Done For:         COMCAST
Place:            MARTINSBURG       Depth:            2 FT
Street:           MOSSY LN
Intersection:     MCMILLAN CT
Nature of Work:
Explosives:       No                Boring:           No
White Lined:      No                Length:           3 HOURS

Driving Directions
----------------------------------------------------------------------------


Remarks
----------------------------------------------------------------------------
Lat/Long: 3949181 7796204
PLEASE LOCATE FROM THE NEAREST PED/TAP/POLE TO
INCLUDE EASEMENTS, HOMES AND BUILDINGS TO THE POWER METER BASE LOCATED ON THE
ABOVE MENTIONED LOCATION

2ND CROSSSTREET---EDWIN MILLER BLVD
Original File:
18a0dfa4_20111129073712.xml



Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually
----------------------------------------------------------------------------
BKC   Beckley County PSD                                No
PEB   Firstenergy Corp                                  No
AX    Frontier Communications(Formerly Verizon)         No
LEV   Level 3 Communications                            No
MI    MCI                                               No
CEM   Mountaineer Gas Company                           No
WCM   Comcast                                           No
BKC   Berkeley County PSD                               No


Location
----------------------------------------------------------------------------
Latitude:         39.489384         Longitude:        -77.962807
Second Latitude:  39.492468         Second Longitude: -77.959621

Grids
----------------------------------------------------------------------------
39292077574A 39292077576A 39292077576B 39294077574C 39294077576A
39294077576B 39294077576C 39294077576D




-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.