
KORTERRA JOB UTILIQUEST_WV-SMTP-1 WV1133300470 Seq: 281 11/29/2011 11:04:32
Locate Request For PE
----------------------------------------------------------------------------
Ticket Number:    1133300470        Old Ticket:
Source:           Fax               Sequence:         1
Type:             Normal            Date:             11/29/11 08:54

Company Information
----------------------------------------------------------------------------
WV DIVISION OF HIGHWAYS             Type:             Contractor
HC 64 BOX 232                       Company Phone:    (304) 822-4167
ROMNEY, WV 26757                    Company Fax:      (304) 822-4264
Caller Name:      JULIE GREENE      Caller Phone:     (304) 822-4167
Contact Name:     JULIE GREENE      Contact Phone:    (304) 822-4167
Caller Email:     JULIE.G.GREENE@WV.GOV
Contact Email:    JULIE.G.GREENE@WV.GOV

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        12/01/11 08:45
County:           HAMPSHIRE         Done For:         WV DIVISION OF HIG
Place:            CAPON BRIDGE      Depth:            03 FT
Street:           CO RT 50/27
Intersection:     US 50
Nature of Work:
Explosives:       No                Boring:           No
White Lined:      Yes               Length:           50 FT

Driving Directions
----------------------------------------------------------------------------
FROM THE INTER GO SOUTH ON CO RT 50/27 (FROG-EYE SIRBAUGH RD) FOR APPX 0.3
MILE TO THE LOCATION.


Remarks
----------------------------------------------------------------------------
REPLACING CULVERT.

PLEASE MARK ENTIRE AREA OF CULVERT AND 20 FT EITHER
SIDE.

AREA IS MARKED IN WHITE.

CONTACT: ROGER HAINES (304) 813-2323 IF MORE
INFORMATION IS NEEDED.


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually
----------------------------------------------------------------------------
PE    Firstenergy Corp                                  No
GAN   Frontier - A Citizens Communications Company      No


Location
----------------------------------------------------------------------------
Latitude:         39.281108         Longitude:        -78.405783
Second Latitude:  39.287938         Second Longitude: -78.397468

Grids
----------------------------------------------------------------------------
39168078238A 39168078240A 39168078240B 39168078240C 39168078240D
39168078242A 39168078242B 39170078238A 39170078238B 39170078238C
39170078238D 39170078240A 39170078240B 39170078240C 39170078240D
39170078242B 39170078242C 39170078242D 39172078238C 39172078240C
39172078240D




-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.