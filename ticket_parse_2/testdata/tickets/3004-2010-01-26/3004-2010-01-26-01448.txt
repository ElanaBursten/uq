
AGL112  01919 GAUPC 01/26/10 15:52:09 01260-301-225-000 NORMAL
Underground Notification             
Notice : 01260-301-225 Date: 01/26/10  Time: 15:50  Revision: 000 

State : GA County: PAULDING      Place: ACWORTH                                 
Addr  : From: 402    To:        Name:    BENTLEAF                       DR      
Near  : Name:    SEVEN HILLS                    RD  

Subdivision: SEVEN HILLS                             
Locate: PLEASE LOCATE ALL UTILITIES  20FT EACH DIRECTION OF TRANSFORMER AND STR
      :  EET LIGHT POLE                                                         

Grids       : 3401C8447D 
Work type   : REPAIR UG STREET LIGHT WIRE                                         

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : COBB EMC                                
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : COBB EMC                                  Type: MEMB                
Co addr : 1000 EMC PARKWAY                         
City    : MARIETTA                        State   : GA Zip: 30061              
Caller  : JULIE FOWLER                    Phone   :  770-429-3432              
Fax     :  770-429-3498                   Alt. Ph.:                            
Email   : JULIE.FOWLER@COBBENERGY.COM                                         
Contact :                                                           

Submitted date: 01/26/10  Time: 15:50  Oper: 192
Mbrs : AGL112 BSNW COB70 COMNOR COMNR2 GAUPC GRS70 PLD01 
-------------------------------------------------------------------------------
