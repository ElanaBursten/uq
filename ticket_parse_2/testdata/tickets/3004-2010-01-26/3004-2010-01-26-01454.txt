
AGL108  01929 GAUPC 01/26/10 15:56:03 01260-301-233-000 NORMAL
Underground Notification             
Notice : 01260-301-233 Date: 01/26/10  Time: 15:52  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 5788   To:        Name:    HARRIER                        LN      
Near  : Name:    PITTMAN                        RD  

Subdivision:                                         
Locate: J. BUGGS 773-350-0502  LOCATE UTILTIIES FRONT LEFT AND RIGHT QUADRANTS 
      :  TO METER AND TO INCLUDE THE RIGHT OF WAY. PLEASE MARK ALL UTILITIES AND
      :  ANY CONFLICTS. FRONT AND BOTH SIDES OF PROPERTY                        

Grids       : 3338A8434A 3338A8434B 3338B8434A 3338B8434B 
Work type   : BURYING CATV SERVICE DROPS                                          

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : COMCAST                                 
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Driveway & Sidewalk           
        : *** DrivingInstructions : S ON STONEWALLTELL RD LEFT ON PITTMAN RD 1ST ROAD ON RIGHT          

Company : SPECTRUM TECHNOLOGY SOLUTIONS             Type: CONT                
Co addr : 20 GILL LN                               
City    : STOCKBRIDGE                     State   : GA Zip: 30281              
Caller  : REBECCA RIGGINS                 Phone   :  404-558-8704              
Fax     :                                 Alt. Ph.:  678-773-5676              
Email   : RIGGINSBECA@CHARTER.NET                                             
Contact :                                                           

Submitted date: 01/26/10  Time: 15:52  Oper: 202
Mbrs : AGL108 ATL02 BSCA COMCEN FUL03 GAUPC GP131 GRS70 
-------------------------------------------------------------------------------
