
BSZT29 32 SC811 Voice 09/26/2012 11:11:00 AM 1209260059 Emerg-Remark 

Notice Number:    1209260059        Old Notice:      1209260058        
Sequence:         32                Created By:      RGO               

Created:          09/26/12 11:11 AM                                     
Work Date:        09/26/12 11:11 AM                                     
Update on:                          Good Through:                      

Caller Information:
SC811
810 DUTCH SQUARE BLVD; SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Other             
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:0
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
CHARLEIGH ELEBASH                              Phone:(800) 290-2783 Ext:0
Site Contact Email:  CELEBASH@sc1pups.org                                  
CallBack:  8AM TO 5PM MON-FRI

Excavation Information:
SC    County:  LAURENS              Place:  FOUNTAIN INN      
Street:        541 S MAIN ST                                         
Intersection:  MILTON DR                                             
Subdivision:   ST JAMES ESTATES                                      

Lat/Long: 34.68265,-82.18895
Second:  34.68265,-82.18895

Explosives:  N Premark:  Y Drilling/Boring:  Y Near Railroad:  Y

Work Type:     SEE INSTRUCTIONS                                        
Work Done By:  SC811                Duration:        2 HOURS           

Instructions:
LOCATE ENTIRE PROPERTY AND BOTH SIDES OF THE STREET FOR POSSIBLE ROAD BORE    

Directions:
FROM HWY 1 TO DEER ROAD, LEFT ON HOWELL STREET, THEN IT'S ON THE RIGH         

Remarks:
**TEST**                                                                      

Member Utilities Notified:
LCPW25 BSZT29 DPCZ02 FNG44 CCMZ41 CCL20* 

