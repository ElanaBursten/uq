
BSZT29 37 SC811 Voice 09/26/2012 11:14:00 AM 1209260061 Normal 

Notice Number:    1209260061        Old Notice:                        
Sequence:         37                Created By:      RGO               

Created:          09/26/12 11:14 AM                                     
Work Date:        10/01/12 11:59 PM                                     
Update on:                          Good Through:                      

Caller Information:
SC811
810 DUTCH SQUARE BLVD; SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Other             
Caller:           RHONDA DOTMAN       Phone:         (803) 939-1117 Ext:0
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
CHARLEIGH ELEBASH                              Phone:(800) 290-2783 Ext:0
Site Contact Email:  CELEBASH@sc1pups.org                                  
CallBack:  8AM TO 5PM MON-FRI

Excavation Information:
SC    County:  UNION                Place:  UNION             
Street:        541 E MAIN ST                                         
Intersection:  MILTON DR                                             
Subdivision:   ST JAMES ESTATES                                      

Lat/Long: 34.715712,-81.615487
Second:  34.715712,-81.615487

Explosives:  N Premark:  Y Drilling/Boring:  Y Near Railroad:  Y

Work Type:     SEE INSTRUCTIONS                                        
Work Done By:  SC811                Duration:        2 HOURS           

Instructions:
LOCATE ENTIRE PROPERTY AND BOTH SIDES OF THE STREET FOR POSSIBLE ROAD BORE    

Directions:
FROM HWY 1 TO DEER ROAD, LEFT ON HOWELL STREET, THEN IT'S ON THE RIGH         

Remarks:
**TEST**                                                                      

Member Utilities Notified:
BSZT29 COU17 

