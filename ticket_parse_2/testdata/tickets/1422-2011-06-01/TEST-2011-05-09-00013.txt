
From: IRTHNet  At: 05/09/11 03:43 PM  Seq No: 13

SCA88 57 PUPS Web 05/09/2011 03:42:00 PM 1105092607 Normal 

Ticket Number: 1105092607
Old Ticket Number: 
Created By: LRP
Seq Number: 57

Created Date: 05/09/2011 03:42:49 PM
Work Date/Time: 05/12/2011 03:45:14 PM
Update: 05/31/2011 Good Through: 06/03/2011

Excavation Information:
State: SC     County: CHARLESTON
Place: MT PLEASANT
Address Number: 618
Street: PELZER DR
Inters St: W. COLEMAN BLVD
Subd: COOPER ESTATES

Type of Work: IRRIGATION, INSTALL SYSTEM
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PRESTIGE LANDSCAPING

Remarks/Instructions: LOCATE ENTIRE PROPERTY                                  


Caller Information: 
Name: CINNAMON LEIGH                        PRESTIGE LANDSCAPING                  
Address: 115 HICKORY TRACE DR
City: GOOSE CREEK State: SC Zip: 29445
Phone: (843) 572-9370 Ext:  Type: Business
Fax:  Caller Email: PRESTIGE.LANDSCAPING@COMCAST.NET

Contact Information:
Contact: Email: PRESTIGE.LANDSCAPING@COMCAST.NET
Call Back: Fax: 

Grids: 
Lat/Long: 32.7968370555205, -79.882537342791
Secondary: 32.7944022729104, -79.8791259715516
Lat/Long Caller Supplied: N

Members Involved: BSZN42 COMZ41 KNO45 MPT96 SCA88 SCEDZ95                     


Map Link: (NEEDS DEVELOPMENT)