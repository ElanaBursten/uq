
From: IRTHNet  At: 05/09/11 02:58 PM  Seq No: 10

SCA88 54 PUPS Voice 05/09/2011 02:52:00 PM 1105092465 Normal 

Ticket Number: 1105092465
Old Ticket Number: 
Created By: WGS
Seq Number: 54

Created Date: 05/09/2011 02:57:53 PM
Work Date/Time: 05/12/2011 03:00:01 PM
Update: 05/31/2011 Good Through: 06/03/2011

Excavation Information:
State: SC     County: LEXINGTON
Place: CHAPIN
Address Number: 15
Street: COLUMBIA AVE
Inters St: ROLAND SHEALY COURT
Subd: 

Type of Work: ELECTRIC, INSTALL POLE(S) & ANCHOR(S)
Duration: A COUPLE OF DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: WATTS BROTHERS CABLE CONSTRUCTION

Remarks/Instructions: MARK A 75 FT RADIUS AROUND POLE #11553 LOCATED IN REAR  
OF PROPERTY//ADDITIONAL PEAK ST, PARK ST AND E BOUNDARY//POSSIBLE DRILLING    
                                                                              
                                                                              
                                                                              
JOB #19D23050N                                                                


Caller Information: 
Name: DEBBIE MITCHUM                        WATTS BROTHERS CABLE CONSTRUCTION     
Address: 105 CORPORATE BLVD
City: W. COLUMBIA State: SC Zip: 29169
Phone: (803) 978-2846 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:DEBBIE MITCHUM Email: WATTSBROS@YAHOO.COM
Call Back: Fax: 

Grids: 
Lat/Long: 34.1697372193207, -81.351534871065
Secondary: 34.1674897276806, -81.3329127974756
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCA88 SCEDZ08 SCGZ05 TWCZ40                    


Map Link: (NEEDS DEVELOPMENT)