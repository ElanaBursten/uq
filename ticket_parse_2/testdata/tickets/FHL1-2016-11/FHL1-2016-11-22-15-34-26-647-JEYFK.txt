
LONE STAR 811 Locate Request 
------------------------------------------------------------------------------  
NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE           NEW 
Ticket No: 562717435              Orig. Tkt: 562717253 

Send To: ERVIN01   Seq No:   17   Map Ref:  

Transmit      Date: 11/22/16      Time: 02:34 PM        Type: VOICE 
Original Call Date: 11/22/16      Time: 01:58 PM 
Work to Begin Date: 11/25/16      Time: 02:00 PM 

------------------------------ Work Information ------------------------------ 
County : BEXAR        State: TX 
Place  : SAN ANTONIO 
Address:             Street: I-410 
Nearest Intersecting Street: HWY 151 

Explosives     : N           Duration: 2 YEARS 
Marked in White: N           Digging deeper than 16in.: Y 
Type of Work   : HIGHWAY CONSTRUCTION 
Work Done For  : TEXAS DEPARTMENT OF TRANSPORTA 
Extent of Work : RECALL-1682792998 -DUE TO CHANGE THE CONTACT INFO AND CALLER
: EMAIL- -LONESTAR TICKET: 552305445 ORIGINAL FILE: XOCSX1_552305445R1_PUL7MO
: WORK DATE: 10/22/2015 9:00:00 AM ALL UTILITES MUST RESPOND AND REMARK, ,
: MARK ALL 4 QUADRANTS OF THE INTERSECTION AND FROM THE INTERSECTION OF I 410
: AND STATE HWY 151, TRAVEL GOING N FOR APPROX 2.7 MILES FROM INTERSECTION,
: AND TRAVEL S APPROX 1600FT FROM INTERSECTION. MARKING FROM LAKE SIDE PARKWAY
: ON THE S TO INGAM RD ON THE N MARKING BOTH SIDES OF THE HWY.-UP DATE:
: 09/17/15 09:27 AM: CALLER REQUESTS LOCATERS CALL BEFORE LOCATING CALLER
: REQUESTS UTILITIES PLEASE CALL BEFORE LOCATING ALT CONTACT: MAC QUALLS
: (210)669-5915-EXCAVATOR PHONE: (210)669-5915 ALT CONTACT EMAIL:
: MQUALLS@WBCTX.COM UPDATE: 10/20/15 08:51 AM: MARKING INSTRUCTIONS. CALLER
: REQUESTS UTILITIES PLEASE CALL BEFORE LOCATING ALT CONTACT: MAC QUALLS
: (210)288-8462
: DRIVING DIRECTIONS: 
: ADDITIONAL INFORMATION: 
: EXCAVATOR PHONE: (210)669-5915   ALT CONTACT EMAIL: HLEHRMAN@WBCTX.COM
Remarks        : HIGHWAY CONSTRUCTION 
: TESS: 1682793158 Seq: 6435 Rcd: 11/22/16 14:02 WTBD: 11/25/16 14:00 

------------------------------ Caller Information ---------------------------- 
Company     : WILLIAMS BROS. CONST. CO.      Type: CONTRACTOR 
Contact Name: LYNETTE BIRDSONG              Phone: (210)669-5915 
Address     : 18952 -4 REDLAND RD 
              SAN ANTONIO, TX 78259 
Alt. Contact: HUNTER LEHRMAN                Phone: (512)845-2155 
Best Time   : 0800-170                        Fax:  
Email       : HLEHRMAN@WBCTX.COM 

------------------------------ Location Information -------------------------- 
Excavation Coordinates for # Polygons: 1 
Poly 1: NW Lat: 29.471745 Lon: -98.665745    SE Lat: 29.422549 Lon: -98.609908 
Map        Page:          Grids:  

--------------------------Additional Members Notified------------------------- 
Code       Company                       Code       Company 
ERVIN01    ERVIN CABLE CONSTRUCTION      L3CTX01    LEVEL 3 COMMUNICATIONS         
