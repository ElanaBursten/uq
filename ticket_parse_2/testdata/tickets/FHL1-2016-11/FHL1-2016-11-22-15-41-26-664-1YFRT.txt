
LONE STAR 811 Locate Request 
------------------------------------------------------------------------------  
NOTICE OF INTENT TO EXCAVATE      NOTI-UPDATE           NEW 
Ticket No: 562718018              Orig. Tkt: 562653721 

Send To: ERVIN01   Seq No:   18   Map Ref:  

Transmit      Date: 11/22/16      Time: 02:39 PM        Type: PORTAL UPDATE 
Original Call Date: 11/22/16      Time: 02:33 PM 
Work to Begin Date: 11/25/16      Time: 02:45 PM 

------------------------------ Work Information ------------------------------ 
County : BEXAR        State: TX 
Place  : SAN ANTONIO 
Address:             Street: WESTCREEK OAKS BLVD 
Nearest Intersecting Street: WISEMAN BLVD 

Explosives     : N           Duration: 12 MONTHS 
Marked in White: N           Digging deeper than 16in.: Y 
Type of Work   : RESIDENTIAL DEVELOPMENT 
Work Done For  : WPE VENTURES LLC 
Extent of Work : UPDATE & REMARK-1682028186 UPDATE & REMARK --CHANGED EAST TO
: WEST PER DIRECTIONS.-- WORK DATE: 11/12/2015 6:15:00 PM CUL-DE-SAC IS
: NORTHEAST CORNER OF PROJECT LIMITS. AREA REQUIRING LOCATES STARTS AT
: CUL-DE-SAC AND EXTENDS APPROXIMATELY 1300 WEST & 1450 FT SOUTHWEST TO THE
: ENTERPRISE GAS LINE EASEMENT, AND 1000 FT SOUTH AND 200 FT EAST TO THE
: CURBLINE OF WESTCREEK OAKS BLVD.
: DRIVING DIRECTIONS: 
: FROM LOOP 1604, HEAD WEST ON WISEMAN BLVD (OUTSIDE THE LOOP) FOR
: APPROXIMATELY 1.5 MILES TO THE INTERSECTION OF WISEMAN AND WESTCREEK OAKS.
: TURN LEFT, HEADING SOUTH ON WESTCREEK OAKS FOR .3 MILES TO INTERSECTION OF
: UPTON PARK. TURN RIGHT ON, HEADING WEST ON UPTON PARK FOR .15 MILES TO THE
: INTERSECTION OF BAILEY HEIGHTS AND UPTON PARK. TURN LEFT, HEADING SOUTH ON
: BAILEY HEIGHTS TO INTERSECTION OF MERRITT VILLA. TURN LEFT, HEADING EAST ON
: MERRITT VILLA FOR 600 FT TO INTERSECTION OF MERRRIT VILLA AND BAILEY FOREST.
: TURN RIGHT ON BAILEY FOREST AND HEAD SOUTH TO END OF STREET.
: ADDITIONAL INFORMATION: 
: EXCAVATOR PHONE: (210)490-6700   ALT CONTACT EMAIL:
: jmccourt@dntconstruction.com
Remarks        : RESIDENTIAL DEVELOPMENT 
: TESS: 1682793685 Seq: 6867 Rcd: 11/22/16 14:37 WTBD: 11/25/16 14:45 

------------------------------ Caller Information ---------------------------- 
Company     : DNT CONSTRUCTION               Type: EXCAVATOR 
Contact Name: DARLENE ALLEN                 Phone: (210)490-6700 
Address     : 18952 REDLAND RD #2 
              SAN ANTONIO, TX 78259 
Alt. Contact: NICK AHEARN                   Phone: (210)760-4955 
Best Time   : 0000-2400                       Fax:  
Email       : jmccourt@dntconstruction.com 

------------------------------ Location Information -------------------------- 
Excavation Coordinates for # Polygons: 1 
Poly 1: NW Lat: 29.481402 Lon: -98.702197    SE Lat: 29.459418 Lon: -98.679657 
Map        Page:          Grids:  

--------------------------Additional Members Notified------------------------- 
Code       Company                       Code       Company 
ABOVE03    ZAYO                          ERVIN01    ERVIN CABLE CONSTRUCTION       
L3CTX01    LEVEL 3 COMMUNICATIONS        NEXT01     NEXTERA FIBERNET               
