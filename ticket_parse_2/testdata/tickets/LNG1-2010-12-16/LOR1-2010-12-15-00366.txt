
Ticket No: 10230201               ++EMERGENCY++ 
Send To: PPL02      Seq No:    2  Map Ref:  

Transmit      Date: 12/15/10   Time: 10:10 AM    Op: orcrys 
Original Call Date: 12/15/10   Time: 10:07 AM    Op: orcrys 
Work to Begin Date: 12/15/10   Time: 10:15 AM 

State: OR            County: HOOD RIVER              Place: HOOD RIVER 
Address:   1515      Street: SHERMAN AVE 
Nearest Intersecting Street: 16TH ST 

Twp:       Rng:       Sect-Qtr:  
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.7075399Lon: -121.5286625SE Lat: 45.7067818Lon: -121.526486 

Type of Work: REPLACE SEWER SERVICE 
Location of Work: ADD ON SE CORNER OF ABV INTER.  MARK ENTIRE FRONT YARD ON N
: SIDE OF HOUSE, INCLUDING TO CENTER LINE OF SHREMAN AVE AT ABV ADD.
: +CUSTOMER OUT OF SERVICE, CREW EN ROUTE+ 

Remarks: BEST INFORMATION AVAILABLE 
: ++CALLER REQUESTS AREA MARKED A.S.A.P!++ 

Company     : DEHART EXCAVATION INC            Best Time:   
Contact Name: TONY DEHART                      Phone: (541)386-3290 
Email Address:  excavation@gorge.net 
Alt. Contact: TONY DEHART - CELL               Phone: (541)308-6610 
Contact Fax :  
Work Being Done For: FAYE DORBEN 
Additional Members:  
COHR01     EMBOR01    FALCON03   HREC01     NWN01 
