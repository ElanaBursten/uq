
BSES  00963 GAUPC 04/26/12 10:30:48 04262-300-477-000 NORMAL
Underground Notification             
Notice : 04262-300-477 Date: 04/26/12  Time: 10:29  Revision: 000 

State : GA County: BRYAN         Place: ELLABELL                                
Addr  : From: 6804   To:        Name:    US HWY 280                             
Near  : Name:    TONI BRANCH                    RD  

Subdivision:                                         
Locate:  FRONT AND BOTH SIDES OF PROPERTY. LOCATE FROM THE FIBER PED TO THE POW
      :  ER SIDE OF THE HOUSE                                                   

Grids       : 
Work type   : REPLACING PHONE SERVICE                                             

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : PTC/ZIMMERMAN                           
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : PEMBROKE TELEPHONE COMPANY                Type: MEMB                
Co addr : 185 E BACON ST                           
City    : PEMBROKE                        State   : GA Zip: 31321              
Caller  : JENNY MILLER                    Phone   :  912-653-4794              
Fax     :  912-653-2209                   Alt. Ph.:                            
Email   : JENNYM@PEMTELCO.COM                                                 
Contact :                                                           

Submitted date: 04/26/12  Time: 10:29  Oper: 150
Mbrs : BSES CAN70 CAS01 CLX92 GAUPC PMT01 
-------------------------------------------------------------------------------

