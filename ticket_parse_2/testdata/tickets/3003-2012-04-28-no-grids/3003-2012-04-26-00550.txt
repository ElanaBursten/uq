
BSNE  00557 GAUPC 04/26/12 09:18:15 04262-300-268-000 NORMAL
Underground Notification             
Notice : 04262-300-268 Date: 04/26/12  Time: 09:17  Revision: 000 

State : GA County: GWINNETT      Place: DACULA                                  
Addr  : From: 2600   To:        Name:    DROWNING CREEK                 RD      
Near  : Name:    DACULA HARBINS                 RD  

Subdivision:                                         
Locate: LOCATE ENTIRE PROPERTY INCLUDING BOTH SIDE OF THE RIGHT-OF-WAY FOR ADDR
      :  ESS 2600 DROWNING CREEK ROAD.                                          

Grids       : 
Work type   : WORK INSTALL GAS SERVICE                                            

Start date: 05/01/12 Time: 07:00 Hrs notc : 000
Legal day : 05/01/12 Time: 07:00 Good thru: 05/17/12 Restake by: 05/14/12
RespondBy : 04/30/12 Time: 23:59 Duration : 2 DAYS     Priority: 3
Done for  : CITY OF BUFORD                          
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : THIS IS THE CITY OF DACULA MAINT. FACILITY                          

Company : CEDS CONSTRUCTION                         Type: CONT                
Co addr : 1470  DAHLONEGA HWY                      
City    : CUMMING                         State   : GA Zip: 30040              
Caller  : JOHN WALKER                     Phone   :  770-889-2361              
Fax     :  770-889-7138                   Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 04/26/12  Time: 09:17  Oper: 225
Mbrs : BENCH1 BSNE BUF50 CBL01 CPL81 GAUPC GP240 GWI90 GWI91 
-------------------------------------------------------------------------------

