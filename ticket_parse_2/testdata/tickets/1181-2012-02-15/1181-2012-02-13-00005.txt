
Locate Request For PE
----------------------------------------------------------------------------
Ticket Number:    1204415338        Old Ticket:                         
Source:           Voice             Sequence:         1                 
Type:             Emergency         Date:             02/13/12 04:57    

Company Information
----------------------------------------------------------------------------
POTOMAC EDISON                      Type:             Member            
800 Cabin Hill Dr                   Company Phone:    (304) 496-2226    
Greensburg, PA 15601                Company Fax:                        
Caller Name:      DARREN PUFFINDURGERCaller Phone:     (304) 359-0593    
Contact Name:     DARREN PUFFINDURGERContact Phone:    (304) 359-0593    
Caller Email:     DPUFFIN@ALLEGHENYPOWER.COM                            
Contact Email:    DPUFFIN@ALLEGHENYPOWER.COM                            

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        02/13/12 04:45    
County:           HARDY             Done For:         POTOMAC EDISON    
Place:            BAKER             Depth:            6 FEET            
Street:           0 GROVER SMITH RD                                     
Intersection:     HWY 259                                               
Nature of Work:   EMER REPLACE POLE                                     
Explosives:       No                Boring:           No                
White Lined:      Yes               Length:                             

Driving Directions
----------------------------------------------------------------------------
FROM THE NORTHERNNMOST INTER OF 259 & GROVER SMITH RD, GO WEST ON GROVER SMITH
RD APPX 0.75MI TO DIGSITE.                                                    


Remarks
----------------------------------------------------------------------------
EMERGENCY REPLACE BROKEN POLE.  CREW IS ENROUTE.  CUSTOMERS ARE WITHOUT       
SERVICE.  WORKING 100' OFF THE RD.  
N39.026794 W78.773653                    


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
PE    Firstenergy Corp                                  No                  
HTC   Hardy Telecommunications Inc.                     No                  


Location
----------------------------------------------------------------------------
Latitude:         39.019998         Longitude:        -78.780048        
Second Latitude:  39.030048         Second Longitude: -78.764915        

