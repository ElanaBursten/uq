
BP     00043 POCS 07/14/16 14:18:45 20161891238-003 RNOT DAMG EMER

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============
Serial Number--[20161891238]-[003] Channel#--[1415025][0199]
Message Type--[RENOTIFY][DAMAGE][EMERGENCY]

County--[MONTGOMERY]      Municipality--[WHITPAIN TWP]
Work Site--[1740 CASSELL DR]
     Nearest Intersection--[DOVER LN]
     Second Intersection--[TOWNSHIP LINE RD]
     Subdivision--[]                              Site Marked in White--[Y]
Location Information--
     [SIDEWALK IS PARALLEL WITH THE STREET AT FRONT OF PROPERTY.
      SECOND INTERSECTION ABOVE IS A NEARBY ROAD
      PRIVATE PROPERTY LOCATION:IN FRONT, TO LEFT]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.156353/-75.317226,40.155980/-75.317659,40.155549/-75.316931,
      40.155951/-75.316495]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20161891238]
Type of Work--[EXPOSED-CABLE TV]                             Depth--[2IN]
Extent of Excavation--[3/8IN AROUND]    Method of Excavation--[HAND DIGGING]
Street--[ ] Sidewalk--[X] Pub Prop--[ ] Pvt Prop--[X] Other--[]

Lawful Start Dates--[         ] thru [         ] Response Due Date--[14-Jul-16]
      Scheduled Excavation Date--[07-Jul-16] Dig Time--[1100] Duration--[]

Caller--[JIM DORAZIO]
Caller Phone--[610-639-1227]              Caller Ext--[]
Excavator--[DORAZIO CONTRACTING]
Address--[209 W 7TH AVE]
City--[CONSHOHOCKEN]                               State--[PA] Zip--[19428]
FAX--[610-397-0550]              Caller Type--[B]
Email--[doraziocntrctng@aol.com]
Work For--[NEIL ARMSTRONG]

Person to Contact--[JIM DORAZIO]
Contact Phone--[484-250-9391]              Contact Ext--[]
Best Time to Call--[ANYTIME]
Job Number--[NEIL]

Prepared--[14-Jul-16] at [1418] by [COLLEEN TRAINER]
Remarks--
     [CALLER EXPOSED VERIZON LINE APPX 2IN UNDER SIDEWALK.   HE IS UNABLE TO SAW
      CUT.   PLEASE RESPOND TO THE SITE ASAP.
      TICKET IS FOR EXPOSED LINE
      FACILITY TYPE: CABLE TV-VERIZON
      EXCAVATION EQUIPMENT: HAND TOOLS
      HAZARDOUS RELEASE: NO
      ****=== RENOTIFY 20161891238-003 --7/14/2016 1418 CTR 25===****
      RENOTIFY REQUESTED BY: JIM DORAZIO
      ATTN ALL COMMUNICATION COMPANIES - ALL HAVE RESPONDED CLEAR. CALLER
      STATES THE EXPOSED LINE INVOLVED WAS MARKED IN ORANGE. PLEASE REVERIFY
      AND GO TO SITE ASAP. WORK IS BEING DELAYED. CALLER STATES THEY NEED TO
      COMPLETE THE JOB AND THE LINE IS INVOLVED IN THE AREA. ALSO PLEASE CALL
      JIM DORAZIO @ 484-250-9391.]

BP 0  BP =COMCAST PLMG     HRA0  HRA=COMCAST-FIBER    PLC0  PLC=XO COMM        
YI 0  YI =VERIZON EASTERN

Serial Number--[20161891238]-[003]
========== Copyright (c) 2016 by Pennsylvania One Call System, Inc. ==========