
New Jersey One Call System        SEQUENCE NUMBER 0001    CDC = GPC 

Transmit:  Date: 01/06/14   At: 05:26 

*** R O U T I N E         *** Request No.: 140060006 

Operators Notified: 
ADC     = COMCAST-TOMS RIVER OCEAN      BAN     = VERIZON                        
DVM     = TOMS RIVER MUNICIPAL UTIL     GPC     = JERSEY CENTRAL POWER & LI      
NJN     = NEW JERSEY NATURAL GAS CO     UW7     = UWTR-TOMS RIVER                

Start Date/Time:    01/10/14   At 00:15  Expiration Date: 03/12/14 

Location Information: 
County: OCEAN                  Municipality: TOMS RIVER 
Subdivision/Community:  
Street:               45 MODIFIED DREW PL 
Nearest Intersection: JAY ST 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40.19759   Lon: -74.028701
Type of Work: INSTALL PHONE SERVICE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  BILL ROTH 

Working For: VERIZON SERVICE 
Address:     183 BROAD ST 
City:        RED BANK, NJ  07701 
Phone:       732-530-6760   Ext:  

Excavator Information: 
Caller:      ANTHONY IACOVELLI 
Phone:       201-481-0174   Ext:  

Excavator:   J FLETCHER CREAMER & SONS 
Address:     1701 E LINDEN AVE 
City:        LINDEN, NJ  07036 
Phone:       201-481-0174   Ext:          Fax:  908-587-3236 
Cellular:    201-481-0174 
Email:       AIACOVELLI@JFCSON.COM 
End Request 
