
New Jersey One Call System        SEQUENCE NUMBER 0011    CDC = ADC 

Transmit:  Date: 01/13/14   At: 09:03 

*** R O U T I N E         *** Request No.: 140130371 

Operators Notified: 
ADC     = COMCAST-TOMS RIVER OCEAN      BAN     = VERIZON                        
DVM     = TOMS RIVER MUNICIPAL UTIL     GPC     = JERSEY CENTRAL POWER & LI      
GPMF4   = G4S TECHNOLOGY                NJN     = NEW JERSEY NATURAL GAS CO      
OCE     = OCEAN COUNTY ENGINEERING      UW7     = UWTR-TOMS RIVER                

Start Date/Time:    01/17/14   At 07:00  Expiration Date: 03/19/14 

Location Information: 
County: OCEAN                  Municipality: TOMS RIVER 
Subdivision/Community:  
Street:               0 OLD FREEHOLD RD 
Nearest Intersection: WHITTY RD 
Other Intersection:   WHITTY RD 
Lat/Lon: 
Type of Work: INSTALL POLE(S) 
Block:                Lot:                Depth: 100FT 
Extent of Work: M/O ENTIRE LENGTH OF OLD FREEHOLD ROAD FROM C/L OF WHITTY
  ROAD TO C/L OF WHITTY ROAD INCLUDING ALL INTERSECTIONS.  CURB TO 50FT
  BEHIND BOTH CURBS.  CURB TO CURB. 
Remarks:  
  Working For Contact:  ANDY MCCONNELL 

Working For: NJ TURNPIKE AUTHORITY 
Address:     0 P.O. BOX 504 
City:        WOODBRIDGE, NJ  07095 
Phone:       732-750-5300   Ext:  

Excavator Information: 
Caller:      DEB SLATER 
Phone:       732-938-4004   Ext:  

Excavator:   GEORGE HARMS CONSTRUCTION CO 
Address:     PO BOX 817 
City:        FARMINGDALE, NJ  07727 
Phone:       732-938-4004   Ext:          Fax:  732-938-2782 
Cellular:     
Email:       dslater@ghcci.com 
End Request 
