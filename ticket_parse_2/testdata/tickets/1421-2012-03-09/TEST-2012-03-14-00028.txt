
SCGZ02 45 SC811 Voice 03/14/2012 10:17:00 AM 1203140172 Emerg-Cancel 

Ticket Number:    1203140172        Old Ticket:      1203140171        
Sequence:         45                Created By:      RMD               

Created:          03/14/12 10:17 AM                                     
Work Date:        03/14/12 10:30 AM                                     
Update on:                          Good Through:                      

Caller Information:
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:1
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
JOHN BLACK                              Phone:(803) 123-1234 Ext:1
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        901 BROOKWOOD DR                                      
Intersection:  GARLAND ST                                            
Subdivision:                                                         

Lat/Log: 33.966584,-81.038132
Second:  33.978234,-81.011714

Explosives:  N White Lined:  Y Drilling/Boring:  Y Near Railroad:  

Work Type:     LAGOON, DIG                                             
Work Done By:  SC811                Duration:        about 1 day       

Instructions:
MARK ENTIRE PROPERTY                                                          

Directions:
**ADDITIONAL ST:OAKDALE DR**                                                  

Remarks:
THIS IS A TEST LOCATE FOR OUR MEMBERS                                         

Member Utilities Notified:
COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 SCG02 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 SCG02 SCGZ02 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 SCG02 SCGZ02 SCGT02 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 SCG02 SCGZ02 SCGT02 WCR44* 

COC82 DPT77 BSZB45 USC42 ATT09* LC393 MCI18 QWC42 STG10 TWCZ40 TWTZ26 TWTZ27 SCAZ88 SCEJZ40 SCG02 SCGZ02 SCGT02 WCR44* 


