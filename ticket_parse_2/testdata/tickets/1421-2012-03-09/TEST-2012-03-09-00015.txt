
SCGZ02/SCGZ65 13 SC811 Voice 03/09/2012 12:05:00 PM 1203090068 Normal 

Ticket Number:    1203090068        Old Ticket:                        
Sequence:         13                Created By:      rmd               

Created:          03/09/12 12:07                                        
Work Date:        03/14/12 12:15                                        
Update on:        03/30/2012        Good Through:    04/04/2012        

Caller Information
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:2
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
RHONDA DOTMAN                              Phone:(803) 939-1117 Ext:2
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  best time to reach is 7am to 4am Mon-Fri

Excavation Information:
SC    County:  RICHLAND             Place:  ST ANDREWS        
Street:        810 DUTCH SQUARE BLVD                                 
Intersection:  N ARROWWOOD LN                                        
Subdivision:   NONE                                                  

Lat/Log: 32.05354,-81.755955
Second:  34.80754,-78.551455

Explosives:  Y White Lined:  N Drilling/Boring:  Y Near Railroad:  Y

Work Type:     GARAGE, REPAIR                                          
Work Done By:  SC811                Duration:        ABOUT 10 DAYS     

Instructions:
PLEASE EMAIL ME IF ANY QUESTIONS                                              

Directions:
THIS IS A TEST TICKET FOR OUR MEMBERS TO TEST THE FORMAT                      

Remarks:
GOOD MORNING, THIS IS TO TEST THE FORMAT FOR OUR MEMBERS WITH OUR SYSTEM      
UPGRADE// THANK YOU // RHONDA                                                 

Member Utilities Notified:
CBN90 CEC54 CFB24 COC82 COJ34 HMT85 DPT77 DXP53 
FAE07 FTCZ81 FTCZ80 GEO97 KNO45 TEB25 BSZB45 BSZC46 
BSZN42 BSZU45 HARZ12 HRG58 HRGZ57 CAM60 CGC22 KIU06 
ODPU53 OLC56 BERZ20 BERZ24 SCC21 CCRZ62 HHP61 HRHZ01 
HRRZ02 CMB20 COMZ17 COMZ42 COMZ41 XXX09 IPA61 HTC47 
LRE34 MPT96 BRD11 CPLZ05 CPWZ69 DIX54 KIC07 JAI79 
HOT21 LC393 MCI18 XXX05 XXX06 PEB52 PEH51 PER53 
PRTZ20 PBT43 PDEZ40 QWC42 XXX10 STG11 EMBZ11 STJ55 
TWCZ40 TWOZ73 TWTZ27 NWT74 PETZ78 PETZ80 PETZ82 
PETZ86 PETZ79 PETZ85 RCU73 SAN40 SANZ30 SAN21 SAN22 
SCAZ88 TWUZ46 TCE12 COMZ78 WTC54 SXCZ30 RWDG90 ITCC76 
COG40 FLG76 DFS38 CRCZ22 SCGZ02 SCGZ65 SBI10 TOH11 
HCG73 WISZ52 MUSC83 CCI41 TWBZ51 WCWS93 SRWS80 CGT63 
CGT60 CGT61 CGT62 ABB64 TWDZ11 CXTZ18 LRWS40 JMW83 
SST56 MCT10 HRBZ22 TWQZ64 SBT77 TOK68 FEDZ55 THE55 
EEC51 GWS93 HEC35 FTRZ06 DWS32 MEC28 TOR26 CAS82 



