
SCGZ02 50 SC811 Voice 03/15/2012 10:46:00 AM 1203150427 Normal 

Ticket Number:    1203150427        Old Ticket:                        
Sequence:         50                Created By:      rmd               

Created:          03/15/12 10:47 AM                                     
Work Date:        03/20/12 11:00 AM                                     
Update on:        04/05/2012        Good Through:    04/10/2012        

Caller Information:
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:1
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
JOHN BLACK                              Phone:(803) 123-1234 Ext:1
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  

Excavation Information:
SC    County:  RICHLAND             Place:  COLUMBIA          
Street:        412 KNEECE RD                                         
Intersection:  DECKER PARK ROAD                                      
Subdivision:                                                         

Lat/Log: 33.928182,-81.265428
Second:  34.131626,-80.788941

Explosives:  N White Lined:  Y Drilling/Boring:  Y Near Railroad:  

Work Type:     GRADING, LOWER ROAD BANK(S)                                   
Work Done By:  SC811                Duration:        ABOUT 1 DAY       

Instructions:
IF FACING, MARK THE REAR AND RIGHT SIDE OF THE PROPERTY // MARKING            
INSTRUCTIONS HERE                                                             

Directions:
DIRECTIONS TO THE SITE WILL GO HERE // ADDITIONAL CROSS STREETS AND LANDMARK. 

Remarks:
THIS IS A TEST LOCATE FOR OUR MEMBERS TO TEST THE NEW TICKET FORMAT // PLEASE 
EMAIL ME IF ANY QUESTIONS. THANK YOU RHONDA rdotman@sc1pups.org               

Member Utilities Notified:
COC82 CWC18 DPT77 FAE07 TWN40 BSZB45 USC42 ATT09 

XXX09 CHT11* DIX54 LC393 MCI18 MID55 PBT43 QWC42 

STG10 TWCZ40 TWTZ26 TWTZ27 PETZ79 RCU73 SCAZ88 SCEDZ05 

SCEDZ08 SCEKZ42 SCEKZ82 SCEJZ40 SCTDZ01 TLX24 TCE12 

SXCZ30 ITC73 RSD02 SCG02 SCGZ02 SCGZ05 SCGT02 SCGT05 

WINZ08 CGT63 JMW83 11CIC56 


