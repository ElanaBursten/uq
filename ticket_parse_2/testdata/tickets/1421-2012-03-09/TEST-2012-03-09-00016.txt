
SCEDZ65/SCETZ03/SCGZ02/SCGZ65 14 SC811 Voice 03/09/2012 12:07:00 PM 1203090072 Normal 

Ticket Number:    1203090072        Old Ticket:                        
Sequence:         14                Created By:      rmd               

Created:          03/09/12 12:08                                        
Work Date:        03/14/12 12:15                                        
Update on:        03/30/2012        Good Through:    04/04/2012        

Caller Information
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:2
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
RHONDA DOTMAN                              Phone:(803) 939-1117 Ext:2
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  best time to reach is 7am to 4am Mon-Fri

Excavation Information:
SC    County:  RICHLAND             Place:  ST ANDREWS        
Street:        810 DUTCH SQUARE BLVD                                 
Intersection:  N ARROWWOOD LN                                        
Subdivision:   NONE                                                  

Lat/Log: 31.988236,-82.736788
Second:  34.521236,-78.979788

Explosives:  Y White Lined:  N Drilling/Boring:  Y Near Railroad:  Y

Work Type:     GARAGE, REPAIR                                          
Work Done By:  SC811                Duration:        ABOUT 10 DAYS     

Instructions:
PLEASE EMAIL ME IF ANY QUESTIONS                                              

Directions:
THIS IS A TEST TICKET FOR OUR MEMBERS TO TEST THE FORMAT                      

Remarks:
GOOD MORNING, THIS IS TO TEST THE FORMAT FOR OUR MEMBERS WITH OUR SYSTEM      
UPGRADE// THANK YOU // RHONDA                                                 

Member Utilities Notified:
BFW67 BHW66 CEC54 CFB24 COC82 COJ34 CPC24 CWC18 
DPT77 EDG30 FAE07 FTCZ81 FTCZ80 GEO97 KNO45 LCPW25 
NSC49 TWN40 WCC72 TEB25 BSZB45 BSZC46 BSZN42 BSZT29 
BSZU45 HARZ12 HRGZ57 CNW04 BJW49 BJW50 CAM60 CGC22 
CNNG15 CNNG18 KIU06 ODPU53 BERZ50 SCC21 USC42 SPSD29 
CCRZ62 HRHZ01 HRRZ02 ATT09 CMMZ48 COMZ17 COMZ42 
COMZ41 DPCZ02 DPCZ07 DPCZ04 FFC64 XXX09 GSW89 IPA61 
HTC47 MCPW21 MPT96 BRD11 CPLZ05 CPWZ69 CHT11 CCNZ23 
DIX54 DPL21 KIC07 JAI79 HOT21 LAU27 LC393 MCI18 
MID55 XXX05 XXX06 PEB52 PEH51 PER53 PRTZ20 PBT43 
PDEZ40 PNAZ80 QWC42 SCWS44 XXX10 STG11 STG10 SNG67 
SNG66 EMBZ11 EMBZ12 STJ55 TWCZ40 TWOZ73 TWTZ26 TWTZ27 
NCT22 NWT74 PETZ78 PETZ84 PETZ81 PETZ82 PETZ86 PETZ79 
PETZ85 RCU73 SAN40 SANZ30 SAN21 SAN22 SCAZ88 SCED10 
SCED12 SCED14 SCED52 SCED20 SCED42 SCED53 SCED36 
SCED23 SCED62 SCEDZ05 SCEDZ53 SCEDZ60 SCEDZ70 SCEDZ80 
SCEVZ03 SCED11 SCED71 SCEDZ08 SCEDZ65 SCEDZ81 SCEDZ95 
SCEKZ42 SCEKZ82 SCETZ03 SCETZ09 SCEWZ80 SCEJZ40 
SCER09 SIP61 SCTDZ01 SCTDZ06 SPR80 TWUZ46 TLX24 
TCE12 COMZ78 WTC54 SXCZ30 RWDG90 ITCC76 ITC73 CCMZ41 
RSD02 COG40 FLG76 DFS38 MMP70 CRCZ22 SCGT12 SCGT60 
SCGT76 SCGT83 SCGT88 SCGT90 SCGS87 SCG10 SCG11 SCG14 
SCG35 SCG50 SCG55 SCG60 SCG71 SCG76 SCG88 SCG89 
SCG02 SCG20 SCG23 SCG42 SCG52 SCG54 SCG83 SCG94 
SCG12 SCGZ02 SCGZ05 SCGZ20 SCGZ70 SCGZ80 SCGZ90 
SCGT10 SCGT14 SCGT55 SCGZ65 SCGT54 SCGT42 SCGT65 
SCGT02 SCGT05 SCGT11 SCGT20 SCGT35 SCGT52 SCGT70 
SCGT89 SBI10 GWC38 TCF44 TOH11 WINZ08 WISZ52 MUSC83 
CCI41 SECZ05 TWBZ51 WCWS93 SRWS80 CGT63 CGT60 CGT61 
CGT62 ABB64 TOW18 CXTZ18 JMW83 SST56 MCT10 TWQZ64 
CAB21 WCR44 GMD22 SBT77 PDT68 LWS68 TOK68 FEDZ55 
THE55 CCL20 SPIZ34 EEC51 TOM58 BPW40 GSD51 GWS93 
HEC35 AMG85 BHPW74 FTRZ06 DWS32 PXA29 TCI95 TOR26 
CAS82 KCC99 DWPW85 


