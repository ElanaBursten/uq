
SCEDZ65/SCGZ65 16 SC811 Voice 03/09/2012 12:13:00 PM 1203090082 No Show 

Ticket Number:    1203090082        Old Ticket:                        
Sequence:         16                Created By:      HD                

Created:          03/09/12 12:13                                        
Work Date:        03/14/12 12:15                                        
Update on:                          Good Through:                      

Caller Information
SC811
810 DUTCH SQUARE BLVD SUITE 320
COLUMBIA, SC 29210
Company Fax:                        Type:            Member            
Caller:           RHONDA DOTMAN     Phone:           (803) 939-1117 Ext:2
Caller Email:     rdotman@sc1pups.org                                   

Site Contact Information:
RHONDA DOTMAN                              Phone:(803) 939-1117 Ext:2
Site Contact Email:  hdinglejr@sc1pups.org                                 
CallBack:  best time to reach is 7am to 4am Mon-Fri

Excavation Information:
SC    County:  JASPER               Place:  RIDGELAND         
Street:        107 FIRST ave                                         
Intersection:  GREAT SWAMP RD                                        
Subdivision:   PUPS PHASE 3                                          

Lat/Log: 32.058973,-81.153691
Second:  32.593162,-80.765244

Explosives:  N White Lined:  Y Drilling/Boring:  N Near Railroad:  Y

Work Type:     LAGOON, DIG                                             
Work Done By:  SC811                Duration:        ABOUT 10 DAYS     

Instructions:
PLEASE EMAIL ME IF ANY QUESTIONS                                              

Directions:
THIS IS A TEST TICKET FOR OUR MEMBERS TO TEST THE FORMAT                      

Remarks:
GOOD MORNING, THIS IS TO TEST THE FORMAT FOR OUR MEMBERS WITH OUR SYSTEM      
UPGRADE// THANK YOU // RHONDA                                                 

Member Utilities Notified:
HARZ12 HRG58 HRGZ57 BJW49 BJW50 SPSD29 CCRZ62 HHP61 
HRHZ01 HRRZ02 BCT31 COMZ17 PEB52 PEH51 PER53 EMBZ11 
SAN22 SCAZ88 SCED42 SCED62 SCEDZ60 SCEDZ80 SCEDZ65 
SCTDZ06 ITCC76 FLG76 SCG60 SCGZ65 SCGT65 TWBZ51 
CGT60 CXTZ18 HRBZ22 TWQZ64 TOR26 


