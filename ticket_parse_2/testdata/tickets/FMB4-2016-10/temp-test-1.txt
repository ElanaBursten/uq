
KORTERRA JOB FABER-SMTP-1 MD16689310 Seq: 1 10/21/2016 08:40:19

NOTICE OF INTENT TO EXCAVATE                                    REMARK
Ticket No: 16689310
Transmit   Date: 10/20/16         Time: 11:21 AM           Op: mdsher
Release    Date: 10/11/16         Time: 10:19 AM           Op: mdsher
Response Due By: 10/13/16         Time: 11:59 PM
Expiration Date: 10/27/16         Time: 11:59 PM

State      : MD      County: CHARLES
Place      : WALDORF
Subdivision:
Address: 4200        Street: OLD WASHINGTON RD
Nearest Intersecting Street: BILLINGSLEY RD

Type of Work: INST ELEC SVC
Extent of Work: LOC THE FIRST BUILDING TO YOUR LEFT, AND MARK FROM THE ELEC
: POLE IN FRONT OF BUILDING TO THE ELEC METER ON THE LEFT CORNER OF BUILDIN
: LOC BETWEEN TRANSFORMER AND BUILDING ON THE MOST SOUTHERN SIDE FROM THE
: BILLINGSLEY RD ENTRANCE.
: CLICK HERE TO SEE THE TICKET MAPPING FOR CP02:
: http://md.itic.occinc.com/7VR2-ADA-7Z2-CZX
Comments: RE-MARK: 2ND REQUEST. PLEASE MARK ASAP.  CREW IS ON SITE. COMCAST
: VERIZON, AND WASHINGTON GAS HAS NOT MARKED

Company      : WEISMAN ELECTRIC COMPANY
Contact Name : EDWIN EVERITT                            Fax: (410)266-3971
Contact Phone: (410)974-6564                            Ext:
Caller Address: 42 HUDSON ST SUITE 102
                ANNAPOLIS, MD 21401
Email Address:  eeveritt@weismanelectric.com
Job Site Contact: EDWIN                            Job Site Phone: (443)871
Work Being Done For: NEHMIA GROUP

Caller Lat      :            Lon:              Zone:
Ex. Coord NW Lat: 38.6000000 Lon:-76.9400000 SE Lat: 38.5933333 Lon:-76.933
Permit#:                              Contract Job#:
Explosives: N
ATM01      ATS02      CHAS01     DOIT01     JICT02     VCH        WGL06
 WP02       WP03       WP05

Send To: CPW01     Seq No: 0006   Map Ref:




