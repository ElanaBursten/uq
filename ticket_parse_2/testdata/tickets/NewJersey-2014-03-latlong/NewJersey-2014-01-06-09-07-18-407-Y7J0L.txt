
New Jersey One Call System        SEQUENCE NUMBER 0057    CDC = SJG 

Transmit:  Date: 01/06/14   At: 09:06 

*** R O U T I N E         *** Request No.: 140060292 

Operators Notified: 
ACT     = ATLANTIC COUNTY DPW           AE1     = ATLANTIC CITY ELECTRIC         
BAN     = VERIZON                       MG3     = SUBURBAN PROPANE COMPANY       
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/13/14   At 07:00  Expiration Date: 03/12/14 

Location Information: 
County: ATLANTIC               Municipality: ESTELL MANOR 
Subdivision/Community:  
Street:               0 CAPE MAY AVE 
Nearest Intersection: AETNA DR 
Other Intersection:   STATE RTE 49 
Lat/Lon:  Nad: 83    Lat: 39 18 42.8 Lon: -74 49 21. 
Type of Work: INSTALL SIGN(S) 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O LOCATED 150FT N OF C/L OF INTERSECTION.  CURB TO 10FT
  BEHIND E CURB. 
Remarks:  
  Working For Contact:  HENRY PEACOCK 

Working For: ATLANTIC COUNTY UTILTIES AUTHO 
Address:     6700 DELILAH ROAD 
City:        EGG HARBOR TWP, NJ  08234 
Phone:       609-272-6904   Ext:  

Excavator Information: 
Caller:      HENRY PEACOCK 
Phone:       609-272-6904   Ext:  

Excavator:   ATLANTIC COUNTY UTILTIES AUTHO 
Address:     6700 DELILAH ROAD 
City:        EGG HARBOR TWP, NJ  08234 
Phone:       609-272-6904   Ext: 6985     Fax:  609-569-7331 
Cellular:    609-385-8353 
Email:       hpeacock@acua.com 
End Request 
