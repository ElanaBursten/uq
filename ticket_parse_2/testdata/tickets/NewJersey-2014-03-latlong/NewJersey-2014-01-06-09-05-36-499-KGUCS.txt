
New Jersey One Call System        SEQUENCE NUMBER 0055    CDC = SJG 

Transmit:  Date: 01/06/14   At: 09:03 

*** R O U T I N E         *** Request No.: 140060278 

Operators Notified: 
ACT     = ATLANTIC COUNTY DPW           AE1     = ATLANTIC CITY ELECTRIC         
BAN     = VERIZON                       MG3     = SUBURBAN PROPANE COMPANY       
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/13/14   At 07:00  Expiration Date: 03/12/14 

Location Information: 
County: ATLANTIC               Municipality: ESTELL MANOR 
Subdivision/Community:  
Street:               0 CAPE MAY AVE 
Nearest Intersection: 5TH AVE 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 39 22 06   Lon: -74 19 14 
Type of Work: INSTALL SIGN(S) 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O LOCATED 150FT S OF C/L OF INTERSECTION.  CURB TO 10FT
  BEHIND W CURB. 
Remarks:  
  Working For Contact:  HENRY PEACOCK 

Working For: ATLANTIC COUNTY UTILTIES AUTHO 
Address:     6700 DELILAH ROAD 
City:        EGG HARBOR TWP, NJ  08234 
Phone:       609-272-6904   Ext:  

Excavator Information: 
Caller:      HENRY PEACOCK 
Phone:       609-272-6904   Ext:  

Excavator:   ATLANTIC COUNTY UTILTIES AUTHO 
Address:     6700 DELILAH ROAD 
City:        EGG HARBOR TWP, NJ  08234 
Phone:       609-272-6904   Ext: 6985     Fax:  609-569-7331 
Cellular:    609-385-8353 
Email:       hpeacock@acua.com 
End Request 
