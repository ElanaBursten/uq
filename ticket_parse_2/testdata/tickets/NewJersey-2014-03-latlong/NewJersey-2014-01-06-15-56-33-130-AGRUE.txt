
New Jersey One Call System        SEQUENCE NUMBER 0123    CDC = SCV 

Transmit:  Date: 01/06/14   At: 15:55 

*** R O U T I N E         *** Request No.: 140061484 

Operators Notified: 
AE1     = ATLANTIC CITY ELECTRIC        BAN     = VERIZON                        
CMC     = CAPE MAY COUNTY MUNICIPAL     GPMF4   = G4S TECHNOLOGY                 
MDT     = MIDDLE TOWNSHIP               MG3     = SUBURBAN PROPANE COMPANY       
NJH     = GARDEN STATE PARKWAY          SCV     = COMCAST CABLE (VINELAND)       
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/10/14   At 07:00  Expiration Date: 03/12/14 

Location Information: 
County: CAPE MAY               Municipality: MIDDLE TWP 
Subdivision/Community: CLERMON/GSP MAINTENANCE YARD LOT 
Street:               0 GARDEN STATE PKWY 
Nearest Intersection: AVALON BLVD 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 39.130152  Lon: -74.771588 
Type of Work: PAVING RESTORATION 
Block:                Lot:                Depth: 3FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 1500FT N.
  CURB TO 500FT BEHIND W CURB. 
Remarks:  
  Working For Contact:  DON WALSH 

Working For: JPC GROUP INC 
Address:     228 BLACKWOOD BARNSBORO RD 
City:        BLACKWOOD, NJ  08012 
Phone:       856-232-0400   Ext:  

Excavator Information: 
Caller:      WALTER NOLL 
Phone:       609-641-2781   Ext: 213 

Excavator:   A.E. STONE, INCORPORATED 
Address:     1435 DOUGHTY RD 
City:        EGG HARBOR TOWNSHIP, NJ  08234 
Phone:       609-641-2781   Ext: 213      Fax:  609-641-0374 
Cellular:    609-435-4464 
Email:       wnoll@aestone.com 
End Request 
