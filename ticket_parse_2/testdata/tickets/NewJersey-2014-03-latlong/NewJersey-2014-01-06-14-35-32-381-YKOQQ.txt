
New Jersey One Call System        SEQUENCE NUMBER 0074    CDC = EG1 

Transmit:  Date: 01/06/14   At: 14:32 

*** R O U T I N E         *** Request No.: 140061216 

Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
BUK     = BUCKEYE PARTNERS              CP2     = COLONIAL PIPELINE COMPANY      
EG1     = ELIZABETHTOWN GAS COMPANY     ELC     = ELIZABETH CITY                 
JOM     = JOINT MEETING OF ESSEX &      MCI     = MCI                            
POR     = PORT AUTHORITY OF NY & NJ     PSCE    = PUBLIC SERVICE ELECTRIC &      
SCLWC   = NJ AMERICAN WATER             TGP     = TRANSCONTINENTAL GAS PIPE      
TK7     = CABLEVISION OF NJ              

Start Date/Time:    01/10/14   At 10:00  Expiration Date: 03/12/14 

Location Information: 
County: UNION                  Municipality: ELIZABETH 
Subdivision/Community:  
Street:               0 S 1ST ST 
Nearest Intersection: 3RD AVE 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40 38 24   Lon: -74 11 59 
Type of Work: REMOVE POLE(S) 
Block:                Lot:                Depth: 10FT 
Extent of Work: M/O LOCATED 1850FT S OF C/L OF INTERSECTION.  10FT RADIUS OF
  POLE # 68885.  M/O LOCATED 1FT BEHIND E CURB. 
Remarks:  
  Working For Contact:  TORY THOMPSON 

Working For: VERIZON COMMUNICATIONS 
Address:     663 FLORIDA GROVE ROAD 
City:        WOODBRIDGE, NJ  08861 
Phone:       732-977-7901   Ext:  

Excavator Information: 
Caller:      TORY THOMPSON 
Phone:       732-977-7901   Ext:  

Excavator:   VERIZON COMMUNICATIONS 
Address:     663 FLORIDA GROVE ROAD 
City:        WOODBRIDGE, NJ  08861 
Phone:       732-977-7901   Ext:          Fax:  732-634-2512 
Cellular:    201-602-0736 
Email:        
End Request 
