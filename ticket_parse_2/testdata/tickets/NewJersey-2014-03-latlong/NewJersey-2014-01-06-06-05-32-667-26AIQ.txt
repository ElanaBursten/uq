
New Jersey One Call System        SEQUENCE NUMBER 0002    CDC = CAM 

Transmit:  Date: 01/06/14   At: 06:04 

*** R O U T I N E         *** Request No.: 140060012 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
GPE     = JERSEY CENTRAL POWER & LI     LKW     = LAKEWOOD TOWNSHIP M.U.A.       
NJN     = NEW JERSEY NATURAL GAS CO      

Start Date/Time:    01/10/14   At 00:15  Expiration Date: 03/12/14 

Location Information: 
County: OCEAN                  Municipality: LAKEWOOD 
Subdivision/Community:  
Street:               1 RIVKA LN 
Nearest Intersection: BOULDER WAY 
Other Intersection:    
Lat/Lon:  Nad: 27    Lat: 40.087309  Lon: -74.180939 
Type of Work: INSTALL PHONE SERVICE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO ENTIRE PROPERTY.  NEW STREET 
Remarks:  
  Working For Contact:  BILL ROTH 

Working For: VERIZON SERVICE 
Address:     183 BROAD ST 
City:        RED BANK, NJ  07701 
Phone:       732-530-6760   Ext:  

Excavator Information: 
Caller:      ANTHONY IACOVELLI 
Phone:       201-481-0174   Ext:  

Excavator:   J FLETCHER CREAMER & SONS 
Address:     1701 E LINDEN AVE 
City:        LINDEN, NJ  07036 
Phone:       201-481-0174   Ext:          Fax:  908-587-3236 
Cellular:    201-481-0174 
Email:       AIACOVELLI@JFCSON.COM 
End Request 
