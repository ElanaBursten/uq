
New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = CC3 

Transmit:  Date: 01/07/14   At: 08:27 

*** E M E R G E N C Y     *** Request No.: 140070164 

Operators Notified: 
AM2     = AT&T CORP                     BAN     = VERIZON                        
CC3     = COMCAST CABLEVISION OF CE     GPS     = JERSEY CENTRAL POWER & LI      
MOT     = MONROE TOWNSHIP UTIL. DEP     PSNB    = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    01/07/14   At 08:30  Expiration Date:  

Location Information: 
County: MIDDLESEX              Municipality: MONROE 
Subdivision/Community:  
Street:               0 FORSGATE DR 
Nearest Intersection: CENTRE DR 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40.341410  Lon: -74.457641 
Type of Work: EMERGENCY - REPAIR/REPLACE BROKEN POLE 
Block:                Lot:                Depth: 7FT 
Extent of Work: M/O LOCATED 1235FT E OF C/L OF INTERSECTION.  50FT RADIUS OF
  POLE JC183MNT. 
Remarks:  
  PUBLIC SAFETY HAZARD 
  Working For Contact:  ANTHONY BENANTI 

Working For: JERSEY CENTRAL POWER AND LIGHT 
Address:     1345 ENGLISHTOWN ROAD 
City:        OLD BRIDGE, NJ  08857 
Phone:       732-723-6657   Ext:  

Excavator Information: 
Caller:      ANTHONY BENANTI 
Phone:       732-723-6657   Ext:  

Excavator:   JERSEY CENTRAL POWER AND LIGHT 
Address:     1345 ENGLISHTOWN ROAD 
City:        OLD BRIDGE, NJ  08857 
Phone:       732-723-6657   Ext:          Fax:  732-723-6633 
Cellular:    732-723-6657 
Email:       abenanti@firstenergycorp.com 
End Request 
