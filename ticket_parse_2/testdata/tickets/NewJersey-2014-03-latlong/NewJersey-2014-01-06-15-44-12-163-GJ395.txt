
New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = CC2 

Transmit:  Date: 01/06/14   At: 15:43 

*** E M E R G E N C Y     *** Request No.: 140061466 

Operators Notified: 
BAN     = VERIZON                       CC2     = COMCAST CABLEVISION OF BU      
GSF     = COMCAST CABLEVISION OF GA     PSBU    = PUBLIC SERVICE ELECTRIC &      
WIL     = WILLINGBORO MUNICIPAL UTI      

Start Date/Time:    01/06/14   At 15:45  Expiration Date:  

Location Information: 
County: BURLINGTON             Municipality: WILLINGBORO 
Subdivision/Community:  
Street:               22 PARISH LN 
Nearest Intersection: PAGEANT LN 
Other Intersection:   PRIMROSE LN 
Lat/Lon:  Nad: 83    Lat: 40.030300  Lon: -74.907000 
Type of Work: EMERGENCY - REPAIR/REPLACE SEWER FACILITY 
Block: 304            Lot:  7             Depth: 6FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  CUSTOMER IS WITHOUT SERVICE 
  Working For Contact:  JOHN LIVEZEY 

Working For: HOMEOWNER 
Address:     22 PARISH LN 
City:        WILLINGBORO, NJ  08046 
Phone:       609-871-6833   Ext:  

Excavator Information: 
Caller:      NILKA GUALTIERI 
Phone:       856-547-4404   Ext:  

Excavator:   RAPID ROOTER 
Address:     412 S WHITE HORSE PIKE 
City:        LINDENWOLD, NJ  08021 
Phone:       856-547-4404   Ext:          Fax:  609-878-3298 
Cellular:    856-547-4404 
Email:       info@rapidrooternewjersey.com 
End Request 
