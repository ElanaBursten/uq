
New Jersey One Call System        SEQUENCE NUMBER 0033    CDC = SJG 

Transmit:  Date: 01/06/14   At: 08:38 

*** E M E R G E N C Y     *** Request No.: 140060211 

Operators Notified: 
ACS     = ATLANTIC CITY SEWERAGE CO     ACY     = ATLANTIC CITY, CITY OF         
AE1     = ATLANTIC CITY ELECTRIC        ATC     = ATLANTIC CITY MUNICIPAL U      
BAN     = VERIZON                       CAT     = COMCAST CABLEVISION OF SO      
FTN     = FIBER TECHNOLOGY NETWORKS     SJG     = SOUTH JERSEY GAS COMPANY       
TLE     = CONECTIV THERMAL SYSTEMS       

Start Date/Time:    01/06/14   At 08:45  Expiration Date:  

Location Information: 
County: ATLANTIC               Municipality: ATLANTIC CITY 
Subdivision/Community:  
Street:               0 PACIFIC AVE 
Nearest Intersection: S STATES AVE 
Other Intersection:   MARYLAND AVE 
Lat/Lon:  Nad: 83    Lat: 39 21 47.7 Lon: -74 25 13. 
Type of Work: EMERGENCY - REPAIR/REPLACE WATER FACILITY 
Block:                Lot:                Depth: 5FT 
Extent of Work: M/O ENTIRE LENGTH OF PACIFIC AVE FROM C/L OF S STATES AVE TO
  C/L OF MARYLAND AVE INCLUDING ALL INTERSECTIONS AND 100FT IN ALL
  DIRECTIONS FROM C/L OF INTERSECTIONS.  CURB TO 5FT BEHIND ALL CURBS.
  CURB TO CURB. 
Remarks:  
  Working For Contact:  KEVIN JORDAN 

Working For: ATLANTIC CITY MUA 
Address:     401 N VIRGINIA AVE 
City:        ATLANTIC CITY, NJ  08401 
Phone:       609-345-3883   Ext: 229 

Excavator Information: 
Caller:      KEVIN JORDAN 
Phone:       609-345-3883   Ext: 229 

Excavator:   ATLANTIC CITY MUA 
Address:     401 N VIRGINIA AVE 
City:        ATLANTIC CITY, NJ  08401 
Phone:       609-345-3883   Ext: 229      Fax:  609-348-1710 
Cellular:    609-412-3897 
Email:       wnorman@ACMUA.ORG 
End Request 
