
New Jersey One Call System        SEQUENCE NUMBER 0097    CDC = EG1 

Transmit:  Date: 01/06/14   At: 16:44 

*** R O U T I N E         *** Request No.: 140061627 

Operators Notified: 
BAN     = VERIZON                       CGD     = NEON TRANSCOM                  
EG1     = ELIZABETHTOWN GAS COMPANY     MFR     = ZAYO GROUP                     
PSCE    = PUBLIC SERVICE ELECTRIC &     SCEW1   = NJ AMERICAN WATER              

Start Date/Time:    01/13/14   At 08:00  Expiration Date: 03/12/14 

Location Information: 
County: UNION                  Municipality: WESTFIELD 
Subdivision/Community:  
Street:               560 NORTH AVE E 
Nearest Intersection: 4TH AVE 
Other Intersection:   HILLCREST AVE 
Lat/Lon:  Nad: 83    Lat: 40.651536  Lon: -74.335299 
Type of Work: INSTALL MONITORING WELL(S) 
Block: 1.03,1.04      Lot:  3305          Depth: 120FT 
Extent of Work: CURB TO 260FT BEHIND CURB. 
Remarks:  
  Working For Contact:  LANA ANTONETTI 

Working For: LANGAN ENGINEERING 
Address:     30 S 17TH ST STE1300 
City:        PHILADELPHIA, PA  19103 
Phone:       215-446-2927   Ext:  

Excavator Information: 
Caller:      DAN SPONSELLER 
Phone:       717-766-4800   Ext: 3105 

Excavator:   EICHELBERGERS INC 
Address:     107 TEXACO ROAD 
City:        MECHANICSBURG, PA  17050 
Phone:       717-766-4800   Ext: 3105     Fax:  717-691-6069 
Cellular:    717-649-0206 
Email:       dan@eichelbergers.com 
End Request 
