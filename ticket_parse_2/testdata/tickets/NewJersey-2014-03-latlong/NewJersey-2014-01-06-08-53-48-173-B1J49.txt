
New Jersey One Call System        SEQUENCE NUMBER 0052    CDC = SJG 

Transmit:  Date: 01/06/14   At: 08:53 

*** R O U T I N E         *** Request No.: 140060262 

Operators Notified: 
BAN     = VERIZON                       LSA     = THE LANDIS SEWERAGE AUTHO      
SCV     = COMCAST CABLE (VINELAND)      SJG     = SOUTH JERSEY GAS COMPANY       
VIN     = VINELAND, CITY OF              

Start Date/Time:    01/10/14   At 08:00  Expiration Date: 03/12/14 

Location Information: 
County: CUMBERLAND             Municipality: VINELAND 
Subdivision/Community:  
Street:               2881 S DELSEA DR 
Nearest Intersection: W SHERMAN AVE 
Other Intersection:   W BUTLER AVE 
Lat/Lon:  Nad: 83    Lat: 39 26 41.8 Lon: -75 2 23.4 
Type of Work: SOIL BORINGS 
Block: 7002           Lot:  50            Depth: 40FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  BRYAN HALL 

Working For: THE PETTIT GROUP, LLC 
Address:     497 CENTER STREET 
City:        SEWELL, NJ  08080 
Phone:       856-464-9600   Ext:  

Excavator Information: 
Caller:      STEVE NERNEY 
Phone:       856-809-1202   Ext:  

Excavator:   ACER ASSOCIATES, LLC 
Address:     1012 INDUSTRIAL DR 
City:        WEST BERLIN, NJ  08091 
Phone:       856-809-1202   Ext:          Fax:  856-809-1203 
Cellular:     
Email:       stevenerney@acerassociates.com 
End Request 
