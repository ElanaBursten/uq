
New Jersey One Call System        SEQUENCE NUMBER 0053    CDC = SJG 

Transmit:  Date: 01/06/14   At: 08:53 

*** E M E R G E N C Y     *** Request No.: 140060258 

Operators Notified: 
ACS     = ATLANTIC CITY SEWERAGE CO     ACY     = ATLANTIC CITY, CITY OF         
AE1     = ATLANTIC CITY ELECTRIC        ATC     = ATLANTIC CITY MUNICIPAL U      
BAN     = VERIZON                       CAT     = COMCAST CABLEVISION OF SO      
SJG     = SOUTH JERSEY GAS COMPANY       

Start Date/Time:    01/06/14   At 09:00  Expiration Date:  

Location Information: 
County: ATLANTIC               Municipality: ATLANTIC CITY 
Subdivision/Community:  
Street:               0 N HARRISBURG AVE 
Nearest Intersection: PORTER AVE 
Other Intersection:   FILBERT AVE 
Lat/Lon:  Nad: 83    Lat: 39 21 02.4 Lon: -74 27 38. 
Type of Work: EMERGENCY - REPAIR/REPLACE WATER FACILITY 
Block:                Lot:                Depth: 5FT 
Extent of Work: M/O ENTIRE LENGTH OF N HARRISBURG AVE FROM C/L OF PORTER AVE
  TO C/L OF FILBERT AVE INCLUDING BOTH INTERSECTIONS AND 100FT IN ALL
  DIRECTIONS FROM C/L OF INTERSECTIONS.  CURB TO 5FT BEHIND ALL CURBS.
  CURB TO CURB. 
Remarks:  
  Working For Contact:  KEVIN JORDAN 

Working For: ATLANTIC CITY MUA 
Address:     401 N VIRGINIA AVE 
City:        ATLANTIC CITY, NJ  08401 
Phone:       609-345-3883   Ext: 229 

Excavator Information: 
Caller:      KEVIN JORDAN 
Phone:       609-345-3883   Ext: 229 

Excavator:   ATLANTIC CITY MUA 
Address:     401 N VIRGINIA AVE 
City:        ATLANTIC CITY, NJ  08401 
Phone:       609-345-3883   Ext: 229      Fax:  609-348-1710 
Cellular:    609-412-3897 
Email:       wnorman@ACMUA.ORG 
End Request 
