
New Jersey One Call System        SEQUENCE NUMBER 0058    CDC = CTC 

Transmit:  Date: 01/06/14   At: 21:12 

*** R O U T I N E         *** Request No.: 140061748 

Operators Notified: 
BAN     = VERIZON                       CTC     = COMCAST OF SOMERSET            
EW2     = NEW JERSEY AMERICAN WATER     PSTN    = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    01/10/14   At 00:15  Expiration Date: 03/12/14 

Location Information: 
County: SOMERSET               Municipality: MONTGOMERY 
Subdivision/Community:  
Street:               16 MCGOVERN LN 
Nearest Intersection: ACADIA LN 
Other Intersection:    
Lat/Lon:  Nad: 27    Lat: 40.409999  Lon: -74.654250 
Type of Work: INSTALL PHONE SERVICE 
Block:                Lot:                Depth: 5FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  BILL ROTH 

Working For: VERIZON SERVICE 
Address:     183 BROAD ST 
City:        RED BANK, NJ  07701 
Phone:       732-530-6760   Ext:  

Excavator Information: 
Caller:      PAUL MOSER 
Phone:       201-937-3570   Ext:  

Excavator:   J. FLETCHER CREAMER & SON 
Address:     1701 E LINDEN AVE 
City:        LINDEN, NJ  07036 
Phone:       201-937-3570   Ext:          Fax:  908-587-3236 
Cellular:     
Email:       pmoser@jfcson.com 
End Request 
