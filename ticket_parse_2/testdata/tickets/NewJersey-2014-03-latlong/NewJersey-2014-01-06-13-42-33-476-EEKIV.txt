
New Jersey One Call System        SEQUENCE NUMBER 0068    CDC = EG1 

Transmit:  Date: 01/06/14   At: 13:41 

*** R O U T I N E         *** Request No.: 140061063 

Operators Notified: 
AGT     = ALGON/TEXAS EASTERN GAS T     BAN     = VERIZON                        
BUK     = BUCKEYE PARTNERS              CIT     = CITGO PETROLEUM LINDEN TE      
CP1     = IMTT - PIPELINE               EG1     = ELIZABETHTOWN GAS COMPANY      
GAT     = KINDER MORGAN LIQUIDS TER     NLT     = NUSTAR TERMINALS OPERATIO      
PRX     = PRAXAIR, INC.                 PSCE    = PUBLIC SERVICE ELECTRIC &      
SCEW1   = NJ AMERICAN WATER             SU2     = SUNOCO LOGISTICS               
TRE     = PHILLIPS 66 PIPELINE LLC       

Start Date/Time:    01/13/14   At 07:00  Expiration Date: 03/12/14 

Location Information: 
County: UNION                  Municipality: LINDEN 
Subdivision/Community:  
Street:               710 LOWER RD 
Nearest Intersection: PARKWAY AVE 
Other Intersection:    
Lat/Lon:  Nad: 83    Lat: 40 36 44.7 Lon: -74 14 3.5 
Type of Work: INSTALL ANODE(S) 
Block:                Lot:                Depth: 80FT 
Extent of Work: CURB TO 200FT BEHIND CURB. 
Remarks:  
  Working For Contact:  JOE ABRAHAMS 

Working For: NEW STAR ENERGY 
Address:     4501 TREMLEY POINT RD 
City:        LINDEN, NJ  07036 
Phone:       908-986-5572   Ext:  

Excavator Information: 
Caller:      SCOTT HEDDY 
Phone:       973-616-4501   Ext: 20 

Excavator:   P C A ENGINEERING 
Address:     57 CANNONBALL ROAD 
City:        POMPTON LAKES, NJ  07442 
Phone:       973-616-4501   Ext: 23       Fax:  973-616-4451 
Cellular:    973-703-8262 
Email:       sheddy@cpbypca.com 
End Request 
