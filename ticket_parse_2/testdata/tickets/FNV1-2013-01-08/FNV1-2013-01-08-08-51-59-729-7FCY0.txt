
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085126                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        39                
Prepared By:   Jillian.kirby               Taken Date:     01/08/13 07:36    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     HIGHWAYS INC                Excavator Phone:(615) 768-4500    
Address:       5605 MURFREESBORO RD        Caller:         AMY HOLLAND       
City, St, Zip: LAVERGNE, TN 37086          Caller Phone:   (615) 768-4500    
Contact Fax:                               Contact:        AMY HOLLAND       
Contact Email:                             Contact Phone:  (615) 768-4500    
Call Back:     (615) 768-4500                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       AIRPORT CENTER DR                                     
Intersection:  ROYAL PKWY                                            

Latitude:      36.142629                   Longitude:      -86.664481        
Secondary Lat: 36.145589                   Secondary Long: -86.662981        

Work Type:     GAS LINE, INSTL             Explosives: No       WhitePaint: No       
Done For:      PIEDMONT NATURAL GAS        Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER...                                                                 

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
 MARK GOING E/SE APPX 1000FT ON BOTH SIDES OF RD....                          

--------------------------------------------------------------------------------
Grids:   [108A ] [108H ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
MPW             Metro Public Works                                No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


