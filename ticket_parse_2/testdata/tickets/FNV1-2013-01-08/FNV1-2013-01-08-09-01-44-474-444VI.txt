
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080090                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        54                
Prepared By:   gr.amy.goldschmidt          Taken Date:     01/08/13 07:55    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STANSELL ELECTRIC           Excavator Phone:(615) 329-4944    
Address:       860 VISCO DRIVE             Caller:         AMY GOLDSCHMIDT   
City, St, Zip: NASHVILLE, TN 37210         Caller Phone:   (615) 207-6283    
Contact Fax:   (615) 320-5236              Contact:        AMY GOLDSCHMIDT   
Contact Email:                             Contact Phone:  (615) 207-6283    
Call Back:     615-329-4944 or cell 615-207-6283                     

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:00 
County:        DAVIDSON                    Update Date:                      
Place:         NASHVILLE                   Expire Date:                      

Address:       LAFAYETTE ST                                          
Intersection:  6TH AVE S                                             

Latitude:      36.1542310211639            Longitude:      -86.7767586826366 
Secondary Lat: 36.152466976219             Secondary Long: -86.7745519410623 

Work Type:     TRAFFIC SIGNALS, INSTL      Explosives: No       WhitePaint: No       
Done For:      METRO PUBLIC WORKS          Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK A 200 FT RADIUS OF THE CENTER OF INTERS... AREA MARKED IN WHITE          
PAINT...UPDATE / REMARK PER AMY GOLDSCHMIDT TKT 123590065                     

--------------------------------------------------------------------------------
Grids:   [93J  ] [93O  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NESNET          Nashville Electric Service Network - NESNET       No                  
NES             Nashville Electric Svc (utiliquest) - NES         No                  
MPW             Metro Public Works                                No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  
LVT3            Level 3 Communications - LVT3                     No                  


