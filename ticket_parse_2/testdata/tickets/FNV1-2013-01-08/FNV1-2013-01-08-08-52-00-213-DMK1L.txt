
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080080                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        50                
Prepared By:   gr.amy.goldschmidt          Taken Date:     01/08/13 07:48    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STANSELL ELECTRIC           Excavator Phone:(615) 329-4944    
Address:       860 VISCO DRIVE             Caller:         SARA ZIMMERMAN    
City, St, Zip: NASHVILLE, TN 37210         Caller Phone:   (615) 329-4944    
Contact Fax:   (615) 320-5236              Contact:        JOSH COUTS        
Contact Email:                             Contact Phone:  (615) 329-4944    
Call Back:     615-394-5138 CELL                                     

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        SUMNER                      Update Date:                      
Place:         GOODLETTSVILLE              Expire Date:                      

Address:       SPRINGFIELD HWY                                       
Intersection:  DAVIDSON /SUMNER COUNTY LINE                          

Latitude:      36.3405546147823            Longitude:      -86.7185041447843 
Secondary Lat: 36.3387894124488            Secondary Long: -86.717821377844  

Work Type:     POLE(S), INSTL AND/OR REPL  Explosives: No       WhitePaint: No       
Done For:      RAY BELL ASSOC              Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
FROM INTER MARK 250FT N ALONG W SIDE OF SPRINGFIELD HWY...INTER IS AT THE     
DAVIDSON/SUMNER COUNTY LINE...  ....UPDATE / REMARK PER AMY GOLDSCHMIDT TKT   
123590052                                                                     

--------------------------------------------------------------------------------
Grids:   [142D ] [142E ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
MCI             MCI - MCI                                         No                  
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
GDL             Goodlettsville Sewer Dept                         No                  
MWS             Metro Water & Sewer                               No                  
WHU             White House Utility District                      No                  
LVT3            Level 3 Communications - LVT3                     No                  


