
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085326                   OLD TICKET NUM: 123520952         

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        84                
Prepared By:   Stacy.rainey                Taken Date:     01/08/13 08:30    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     VINSON GROUP                Excavator Phone:(615) 874-9703    
Address:       POB 1784                    Caller:         TROY PELKEY       
City, St, Zip: MADISON, TN 37116           Caller Phone:   (615) 874-9703    
Contact Fax:                               Contact:        TROY PELKEY       
Contact Email:                             Contact Phone:  (615) 874-9703    
Call Back:     (615) 484-9291                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:30 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       CLEGHORN AVE                                          
Intersection:  ABBOTT MARTIN RD                                      

Latitude:      36.106334                   Longitude:      -86.819814        
Secondary Lat: 36.107287                   Secondary Long: -86.819301        

Work Type:     OTHER                       Explosives: No       WhitePaint: No       
Done For:      ROCKFORD GROUP              Directional Boring: No                
Extent:        MG 11:03AM                  ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
WORK: DIGGING UP SWR LINE & INSTALLING A SWR CLEANOUT....FROM WESTERN MOST    
INTER GO N APPX 120FT TO ORANGE MARKINGS ON BOTH SIDES OF RD...MARK AT THESE  
ORANGE MARKINGS...UPDATE / REMARK PER SAM VINSON                              

--------------------------------------------------------------------------------
Grids:   [117O ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
MPW             Metro Public Works                                No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


