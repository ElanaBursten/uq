
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085057                   OLD TICKET NUM: 130071637         

Message Type:  Cancel Request              For Code:       NES               
Hours Notice:  72                          Seq Num:        20                
Prepared By:   Jillian.kirby               Taken Date:     01/08/13 06:52    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STAR CONSTRUCTION           Excavator Phone:(615) 385-1205    
Address:       700 FESSEY PARK RD          Caller:         WADE REED         
City, St, Zip: NASHVILLE, TN 37204         Caller Phone:   (615) 385-1205    
Contact Fax:                               Contact:        WADE REED         
Contact Email:                             Contact Phone:  (615) 385-1205    
Call Back:     (931) 215-2143                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:00 
County:        WILLIAMSON                  Update Date:    01/23/13 AT 00:00 
Place:         BRENTWOOD                   Expire Date:    01/26/13 AT 00:00 

Address:       205 POWELL DR                                         
Intersection:  MARYLAND WAY                                          

Latitude:      36.032253                   Longitude:      -86.816182        
Secondary Lat: 36.037536                   Secondary Long: -86.810359        

Work Type:     TELEPHONE CABLE, BURYING    Explosives: No       WhitePaint: Yes      
Done For:      ATT                         Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
CANCELATION OF TKT...WORK NOT BEING DONE  BY COMPANY...WHEN FACING FRONT OF   
PROP FROM STREET, MARK LEFT SIDE OF PROP, APPX 300FT. AREA IN WHITE PAINT.    
PROP APPX 400FT S OF INTER....PER WADE REED TKT 130071637                     

--------------------------------------------------------------------------------
Grids:   [12C  ] [12D  ] [12E  ] 
[8M   ] [8N   ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
ATT             AT&T / T                                          No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
ICG             TW Telecom                                        No                  
MWS             Metro Water & Sewer                               No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  
LVT3            Level 3 Communications - LVT3                     No                  
AFS             Zayo (American Fiber Systems)                     No                  
BRENT           Brentwood, City of                                No                  
BWS             Brentwood Water Services                          No                  


