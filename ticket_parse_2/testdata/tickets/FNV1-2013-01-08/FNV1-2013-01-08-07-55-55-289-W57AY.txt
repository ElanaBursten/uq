
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080018                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       CEMC              
Hours Notice:  72                          Seq Num:        18                
Prepared By:   gr.rita.c                   Taken Date:     01/08/13 06:52    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AT&T                        Excavator Phone:(423) 875-4346    
Address:       4500 PINNACLE LN            Caller:         RITA COOK         
City, St, Zip: CHATTANOOGA, TN 37415       Caller Phone:   (423) 875-2514    
Contact Fax:   (423) 875-2727              Contact:        RITA COOK         
Contact Email:                             Contact Phone:  (423) 875-2514    
Call Back:     423 875-2514                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:00 
County:        MONTGOMERY                  Update Date:                      
Place:         CLARKSVILLE                 Expire Date:                      

Address:       174 DEERWOOD RD                                       
Intersection:  HWY 41A S                                             

Latitude:      36.4895183934596            Longitude:      -87.2084264078044 
Secondary Lat: 0                           Secondary Long: 0                 

Work Type:     TELEPHONE DROPS, BURYING    Explosives: No       WhitePaint: No       
Done For:      AT&T UTILITY OPERATIONS     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROPERTY AT ADDRESS AND BOTH SIDES OF ROAD ADJACENT TO ADDRESS.   
ALSO MARK PROPERTY ACROSS ROAD 100 FT IN EACH DIRECTION... ATT JOB TC1300031  

--------------------------------------------------------------------------------
Grids:   [87C  ] [87F  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
CCC             Charter Communications (Cencom Cable) - CCC       No                  
CLK             Clarksville Gas, Water, and Sewer Dept            No                  
EMU             East Montgomery Utility                           No                  
CEMC            Cumberland Electric Membership Coop               No                  


