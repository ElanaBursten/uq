
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085346                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        90                
Prepared By:   Sam.bussell                 Taken Date:     01/08/13 08:37    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STAR CONSTRUCTION           Excavator Phone:(615) 385-1205    
Address:       700 FESSEY PARK RD          Caller:         RANDY FINLEY      
City, St, Zip: NASHVILLE, TN 37204         Caller Phone:   (931) 379-0305    
Contact Fax:                               Contact:        WADE REED         
Contact Email:                             Contact Phone:  (615) 385-1205    
Call Back:     931-215-2143                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:45 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       HORNER AVE                                            
Intersection:  FRANKLIN PK                                           

Latitude:      36.121853                   Longitude:      -86.779506        
Secondary Lat: 36.122473                   Secondary Long: -86.775746        

Work Type:     ANCHOR(S), INSTL            Explosives: No       WhitePaint: Yes      
Done For:      ATT                         Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER GO W APPX 200 FT TO POLE 10612127 ON S SIDE OF RD...               

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK 25 FT RADIUS OF POLE 10612127...AREA MARKED W/ WHITE...                  

--------------------------------------------------------------------------------
Grids:   [118B ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
MPW             Metro Public Works                                No                  
ATT             AT&T / T                                          No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
MWS             Metro Water & Sewer                               No                  


