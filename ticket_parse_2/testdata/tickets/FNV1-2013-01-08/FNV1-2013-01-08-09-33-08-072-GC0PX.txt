
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085318                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       CLKSEL            
Hours Notice:  72                          Seq Num:        83                
Prepared By:   Stacy.rainey                Taken Date:     01/08/13 08:28    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     JD WATSON AND ASSOCIATES    Excavator Phone:(276) 669-4622    
Address:       2601 OSBORNE ST             Caller:         JOHNNY GRUB       
City, St, Zip: BRISTOL , VA 24201          Caller Phone:   (423) 340-1337    
Contact Fax:   276649619                   Contact:        JOEY WATSON       
Contact Email:                             Contact Phone:  (423) 340-1337    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:30 
County:        MONTGOMERY                  Update Date:    01/23/13 AT 00:00 
Place:         CLARKSVILLE                 Expire Date:    01/26/13 AT 00:00 

Address:       2701 WILMA RUDOLPH BVD                                
Intersection:  DUNLOP LN                                             

Latitude:      36.583139                   Longitude:      -87.297332        
Secondary Lat: 36.587218                   Secondary Long: -87.292438        

Work Type:     GRADING                     Explosives: No       WhitePaint: No       
Done For:      GREER LAND CO.              Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROP IS AT INTER                                                              

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
ALSO WATER AND SEWER INSTALL...MARK 400FT EACH DIRECTION ON PROP SIDE OF RD   
FROM INTER AND  ENTIRE PROP...THIS IS FOR CHEDDARS...**NOTE: CALLER STATES A  
WATER LINE DID NOT MARKED ON A PREVIOUS TKT AND WANTS TO BE SURE IT DOES THIS 
TIME**                                                                        

--------------------------------------------------------------------------------
Grids:   [32L  ] [32M  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
CLK             Clarksville Gas, Water, and Sewer Dept            No                  
CLKSEL          Clarksville Dept Of Electric                      No                  


