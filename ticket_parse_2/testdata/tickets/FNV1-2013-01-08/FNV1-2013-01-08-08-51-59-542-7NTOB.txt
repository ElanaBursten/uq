
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085168                   OLD TICKET NUM: 123560376         

Message Type:  Normal                      For Code:       MTEMFR            
Hours Notice:  72                          Seq Num:        49                
Prepared By:   Melody.goodman              Taken Date:     01/08/13 07:45    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     DIXIE EARTH MOVERS          Excavator Phone:(615) 242-1203    
Address:       9775 CONCORD RD             Caller:         LISA STINSON      
City, St, Zip: BRENTWOOD, TN 37027         Caller Phone:   (615) 242-1203    
Contact Fax:                               Contact:        LISA STINSON      
Contact Email:                             Contact Phone:  (615) 242-1203    
Call Back:     615-242-1203 OR PHILLIP STINSON AT 615-478-1203       

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        WILLIAMSON                  Update Date:    01/23/13 AT 00:00 
Place:         BRENTWOOD                   Expire Date:    01/26/13 AT 00:00 

Address:       BOUCHAINE PASS                                        
Intersection:  SONOMA TRCE                                           

Latitude:      35.948885                   Longitude:      -86.736471        
Secondary Lat: 35.949301                   Secondary Long: -86.735644        

Work Type:     WATER, SEWER & ELEC, INSTL  Explosives: No       WhitePaint: No       
Done For:      TURNBERRY HOMES             Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
THIS IS SONOMA SUBD. LOT 8...FROM THE INTER GO SW 900FT TO THE PROP...MARK    
ENTIRE LOT...UPDATE / REMARK PER LISA STINSON                                 

--------------------------------------------------------------------------------
Grids:   [55O  ] [60B  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
MTEMFR          Middle Tenn Electric Membership Coop - Franklin...No                  
MWS             Metro Water & Sewer                               No                  
U01             Atmos Energy (United Cities Gas) - Murfreesboro...No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  
BWS             Brentwood Water Services                          No                  


