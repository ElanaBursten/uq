
                  ========//  TNOCS LOCATE REQUEST  //======== Emergency =====

TICKET NUMBER: 130085030                   OLD TICKET NUM: 130085025         

Message Type:  Emergency                   For Code:       CEMC              
Hours Notice:  0                           Seq Num:        9                 
Prepared By:   Jillian.kirby               Taken Date:     01/08/13 06:26    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     SHEARON CONSTRUCTION        Excavator Phone:(615) 505-6492    
Address:       4922 OAKS RD                Caller:         GAINES SHEARON    
City, St, Zip: CHAPMANSBORO, TN 37035      Caller Phone:   (615) 505-6492    
Contact Fax:                               Contact:        GAINES SHEARON    
Contact Email: RGSHEARON@AOL.COM           Contact Phone:  (615) 207-7659    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/08/13 AT 06:30 
County:        CHEATHAM                    Update Date:    01/17/13 AT 00:00 
Place:         PLEASANT VIEW               Expire Date:    01/23/13 AT 00:00 

Address:       1116 GOLF COURSE LN                                   
Intersection:  FOX HILL RD                                           

Latitude:      36.338802                   Longitude:      -87.048882        
Secondary Lat: 36.338802                   Secondary Long: -87.048882        

Work Type:     WATER LINE, INSTL           Explosives: No       WhitePaint: No       
Done For:      SHEARON CONSTRUCTION        Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER GO S APPX 200FT TO PROP...                                         

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
EMERGENCY - EACH UTIL IS REQUIRED BY LAW TO MARK U/G FACILITIES WITHIN 2 HOURS
OF NOTIFICATION...EMERGENCY TKT CREW EN ROUTE...MARK ENTIRE                   
PROP....***OVERWRITE TO ADD REMARKS TO TKT*****                               

--------------------------------------------------------------------------------
Grids:   [36D  ] [36E  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
PVU             Pleasant View Util                                No                  
CEMC            Cumberland Electric Membership Coop               No                  


