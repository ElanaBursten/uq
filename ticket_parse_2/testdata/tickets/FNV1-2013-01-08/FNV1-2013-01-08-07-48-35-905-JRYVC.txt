
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080011                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        12                
Prepared By:   gr.rita.c                   Taken Date:     01/08/13 06:46    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     AT&T                        Excavator Phone:(423) 875-4346    
Address:       4500 PINNACLE LN            Caller:         RITA COOK         
City, St, Zip: CHATTANOOGA, TN 37415       Caller Phone:   (423) 875-2514    
Contact Fax:   (423) 875-2727              Contact:        RITA COOK         
Contact Email:                             Contact Phone:  (423) 875-2514    
Call Back:     423 875-2514                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 06:45 
County:        DAVIDSON                    Update Date:                      
Place:         NASHVILLE                   Expire Date:                      

Address:       2428 ROBBIE CT                                        
Intersection:  KIMBERLY DR                                           

Latitude:      36.2078783697858            Longitude:      -86.6874336921422 
Secondary Lat: 0                           Secondary Long: 0                 

Work Type:     TELEPHONE DROPS, BURYING    Explosives: No       WhitePaint: No       
Done For:      AT&T UTILITY OPERATIONS     Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: Yes               

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROPERTY AT ADDRESS AND BOTH SIDES OF ROAD ADJACENT TO ADDRESS.   
ALSO MARK PROPERTY ACROSS ROAD 100 FT IN EACH DIRECTION... ATT JOB TH1300041  

--------------------------------------------------------------------------------
Grids:   [73C  ] [73F  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


