
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085248                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        67                
Prepared By:   Jennie.ayers                Taken Date:     01/08/13 08:07    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     OUTDOOR WOOD WORDS          Excavator Phone:(615) 865-6900    
Address:       1124 MYATT BLVD             Caller:         CHUCK GRIFFITH    
City, St, Zip: MADISON, TN 37115           Caller Phone:   (615) 865-6900    
Contact Fax:                               Contact:        CHUCK GRIFFITH    
Contact Email: CECF491@YAHOO.COM           Contact Phone:  (615) 865-6900    
Call Back:     615-207-4493                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       609 CHILDRESS XING                                    
Intersection:  JORDAN RIDGE DR                                       

Latitude:      36.219147                   Longitude:      -86.875258        
Secondary Lat: 36.224647                   Secondary Long: -86.870218        

Work Type:     FENCE, INSTL                Explosives: No       WhitePaint: No       
Done For:      MICHAEL STANSTILL           Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM CATO RD GO APPX 1/8 OF A MILE N ON JORDAN RIDGE DR TO CHILDRESS XING ON  
RIGHT/ E SIDE OF RD...THEN GO APPX 150FT E TO PROP....                        

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK REAR & BOTH SIDES OF PROP...                                             

--------------------------------------------------------------------------------
Grids:   [57E  ] [57L  ] [58H  ] 
[58I  ] [58P  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


