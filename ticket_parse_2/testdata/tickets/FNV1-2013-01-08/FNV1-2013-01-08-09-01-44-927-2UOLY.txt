
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085213                   OLD TICKET NUM: 123590189         

Message Type:  Normal                      For Code:       MTEMFR            
Hours Notice:  72                          Seq Num:        56                
Prepared By:   Jennie.ayers                Taken Date:     01/08/13 07:57    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     CARBINE CO                  Excavator Phone:(931) 698-4772    
Address:       621 BRADLEY CT              Caller:         JESSE HUGHES      
City, St, Zip: FRANKLIN, TN 37067          Caller Phone:   (931) 698-4772    
Contact Fax:                               Contact:        JESSE HUGHES      
Contact Email:                             Contact Phone:  (931) 698-4772    
Call Back:     931-698-4772                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:00 
County:        WILLIAMSON                  Update Date:    01/23/13 AT 00:00 
Place:         THOMPSONS STATION           Expire Date:    01/26/13 AT 00:00 

Address:       WAREHAM DR                                            
Intersection:  COLEBROOK DR                                          

Latitude:      35.828981                   Longitude:      -86.907788        
Secondary Lat: 35.839331                   Secondary Long: -86.890523        

Work Type:     WATER, SEWER, ELEC, GAS & TEL, INSTLExplosives: No       WhitePaint: No       
Done For:      DOCKSTREET COMMUNITY        Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
FROM INTER, GO APPX 450FT NE TO THE END OF PAVEMENT...                        

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
FROM HERE MARK BOTH SIDES OF THE GRAVEL RD APPX 400FT NW, MARKING 50FT INTO   
PROPS ON BOTH SIDES OF THE RD. UPDATE / REMARK PER JESSE HUGHES TKT 123590189 

--------------------------------------------------------------------------------
Grids:   [118P ] [119M ] [131D ] 
[131E ] [131L ] [132A ] 
[132H ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
HBT             HB and TS Utility Dist                            No                  
MTEMFR          Middle Tenn Electric Membership Coop - Franklin...No                  
U09             Atmos Energy (United Cities Gas) - Franklin - U...No                  
INTMCWH         Comcast - Williamson/Hickman - INTMCWH            No                  
THST            Thompsons Station, Town of                        No                  


