
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085294                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       ME/INTRU          
Hours Notice:  72                          Seq Num:        77                
Prepared By:   Kathy.jordan                Taken Date:     01/08/13 08:18    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     THOMAS REED                 Excavator Phone:(615) 293-3295    
Address:       1634 WEXFORD DR             Caller:         MARY REED         
City, St, Zip: MURFREESBORO, TN 37129      Caller Phone:   (615) 293-3295    
Contact Fax:                               Contact:        MARY REED         
Contact Email:                             Contact Phone:  (615) 293-3295    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        RUTHERFORD                  Update Date:    01/23/13 AT 00:00 
Place:         MURFREESBORO                Expire Date:    01/26/13 AT 00:00 

Address:       1634 WEXFORD DR                                       
Intersection:  LONGFORD DR                                           

Latitude:      35.904071                   Longitude:      -86.412509        
Secondary Lat: 35.904071                   Secondary Long: -86.412509        

Work Type:     TREES, INSTL / REPL / PLANTINGExplosives: No       WhitePaint: No       
Done For:      THOMAS REED                 Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROPERTY IS LOCATED AT THE CORNER.                                            

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK FRONT LEFT SIDE OF PROPERTY...THERE ARE SOME SMALL TREES IN THIS AREA.   

--------------------------------------------------------------------------------
Grids:   [69B  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
ME              Murfreesboro Electric (utiliquest)                No                  
MRU             Murfreesboro Water & Sewer                        No                  
U01             Atmos Energy (United Cities Gas) - Murfreesboro...No                  
INTRU           Comcast- Rutherford - INTRU                       No                  


