
                  ========//  TNOCS LOCATE REQUEST  //======== Emergency =====

TICKET NUMBER: 130085117                   OLD TICKET NUM: 130072019         

Message Type:  Emergency                   For Code:       INTSU/CEMC        
Hours Notice:  0                           Seq Num:        33                
Prepared By:   Jillian.kirby               Taken Date:     01/08/13 07:31    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     CITY OF PORTLAND            Excavator Phone:(615) 325-5338    
Address:       100 S RUSSELL STREET        Caller:         JIMMY TIDWELL     
City, St, Zip: PORTLAND, TN 03714          Caller Phone:   (615) 325-5338    
Contact Fax:                               Contact:        JIMMY TIDWELL     
Contact Email:                             Contact Phone:  (615) 325-5338    
Call Back:     (615) 325-5338                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/08/13 AT 07:30 
County:        SUMNER                      Update Date:    01/17/13 AT 00:00 
Place:         PORTLAND                    Expire Date:    01/23/13 AT 00:00 

Address:       113 RIDGEWOOD DR                                      
Intersection:  E LONGVIEW DR                                         

Latitude:      36.56148                    Longitude:      -86.502189        
Secondary Lat: 36.56148                    Secondary Long: -86.502189        

Work Type:     WATER LEAK, REPAIR          Explosives: No       WhitePaint: Yes      
Done For:      CITY OF PORTLAND            Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
EMERGENCY - EACH UTIL IS REQUIRED BY LAW TO MARK U/G FACILITIES WITHIN 2 HOURS
OF NOTIFICATION...EMERGENCY - EACH UTIL IS REQUIRED BY LAW TO MARK U/G        
FACILITIES WITHIN 2 HOURS OF NOTIFICATION. CREW ON SITE. MARK FRONT OF PROP   
AND BOTH SIDES OF RD. AREA IN WHITE. PROP APPX 100FT N OF INTER.***OVERWRITE  
TO ADD MARKING TO TKT PER JANICE LANE TKT 130072019                           

--------------------------------------------------------------------------------
Grids:   [39B  ] [39G  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
QWST            Qwest Communications                              No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
PG              Portland Utilities Gas - PG                       No                  
PS              Portland Utility Sewer - PS                       No                  
PW              Portland Utilities Water - PW                     No                  
INTSU           Comcast - Gallatin - INTSU                        No                  
CEMC            Cumberland Electric Membership Coop               No                  


