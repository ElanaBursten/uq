
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130080077                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        43                
Prepared By:   gr.amy.goldschmidt          Taken Date:     01/08/13 07:38    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     STANSELL ELECTRIC           Excavator Phone:(615) 329-4944    
Address:       860 VISCO DRIVE             Caller:         DARLENE DWYER     
City, St, Zip: NASHVILLE, TN 37210         Caller Phone:   (615) 329-4944    
Contact Fax:   (615) 320-5236              Contact:        ANTHONY SOUSA     
Contact Email:                             Contact Phone:  (615) 329-4944    
Call Back:     615-947-8615 CELL                                     

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 07:45 
County:        DAVIDSON                    Update Date:                      
Place:         NASHVILLE                   Expire Date:                      

Address:       UNNAMED ST                                            
Intersection:  EASTSIDE RD                                           

Latitude:      36.1688006461652            Longitude:      -86.7333800499381 
Secondary Lat: 36.1638191646809            Secondary Long: -86.7271207906752 

Work Type:     CONDUIT, INSTL              Explosives: No       WhitePaint: Yes      
Done For:      BARON & DOWDLE              Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
THIS IS SHELBY PARK...FROM INTER OF 20TH AVE S GO E/SE ON  EASTSIDE AVE APPX  
1700FT TO UNNMAED ST...GO NE ON UNNAMED ST APPX 140FT & START MARKING NE APPX 
1210FT ON N SIDE OF RD...TURN & CONT MAKING S OFF RD S/SW APPX 750FT...       
MARKING INSIDE AREA MARKED W/ STAKES...UPDATE / REMARK PER AMY GOLDSCHMIDT TKT
123590047                                                                     

--------------------------------------------------------------------------------
Grids:   [94B  ] [94C  ] [94F  ] 
[94G  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
MCI             MCI - MCI                                         No                  
QWST            Qwest Communications                              No                  
NES             Nashville Electric Svc (utiliquest) - NES         No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
CP1             Colonial Pipeline - Nashville - CP1               No                  
MWS             Metro Water & Sewer                               No                  


