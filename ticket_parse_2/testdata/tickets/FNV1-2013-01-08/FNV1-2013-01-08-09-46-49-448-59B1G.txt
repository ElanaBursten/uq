
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085367                   OLD TICKET NUM: 123491105         

Message Type:  Normal                      For Code:       MTEMLE            
Hours Notice:  72                          Seq Num:        93                
Prepared By:   Melody.goodman              Taken Date:     01/08/13 08:44    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     MIDDLE TN ELECTRIC          Excavator Phone:(615) 444-1323    
Address:       201 MADDOX SIMPSON PKWY     Caller:         CASEY MOORE       
City, St, Zip: LEBANON, TN 37087           Caller Phone:   (615) 444-1323    
Contact Fax:                               Contact:        CASEY MOORE       
Contact Email:                             Contact Phone:  (615) 444-1323    
Call Back:     (615) 289-6906                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:45 
County:        WILSON                      Update Date:    01/23/13 AT 00:00 
Place:         LEBANON                     Expire Date:    01/26/13 AT 00:00 

Address:       ROCKY VALLEY RD                                       
Intersection:  CLEMMONS LN                                           

Latitude:      36.119365                   Longitude:      -86.30758         
Secondary Lat: 36.13087                    Secondary Long: -86.296681        

Work Type:     POLE(S) & ANCHOR(S), INSTL  Explosives: No       WhitePaint: Yes      
Done For:      MIDDLE TN ELECTRIC          Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
WORK--INSTALL   4 POLES/ 5 ANCHORS  ..LOCATION IS ACROSS FROM    2200 ROCKY   
VALLEY RD ....FROM INTER GO APPX 4600 FT E ,AND   FROM THIS POINT, GO APPX    
200 FT N, MARK 30 FT RADIUS OF POLE STAKE WITH ORANGE RIBBON--FROM THIS POINT 
CONT N AND MARK 30 FT RADIUS OF STAKE WITH ORANGE   RIBBON--NOW GO BACK TO THE
RD, AND GO S APPX 100 FT--MARK 30 FT RADIUS OF STAKE WITH ORANGE RIBBON--FROM 
THIS POINT GO S APPX 300 FT AND MARK A 30 FT RADIUS OF STAKE WITH ORANGE      
RIBBON--FROM THIS POINT GO 300 FT AND MARK 30 FT RADIUS OF STAKE WITH ORANGE  
RIBBON  ...WHITE MARKINGS ON ROAD....UPDATE / REMARK PER CASEY MOORE          

--------------------------------------------------------------------------------
Grids:   [103M ] [103N ] [103O ] 
[114B ] [114C ] [114D ] 
[114E ] [114F ] [114G ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
MTEMLE          Middle Tenn Electric Membership Coop - Lebanon ...No                  
WWA             Wilson County Water and Wastewater                No                  


