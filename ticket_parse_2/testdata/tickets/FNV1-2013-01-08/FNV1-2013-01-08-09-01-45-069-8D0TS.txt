
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085219                   OLD TICKET NUM:                   

Message Type:  Normal                      For Code:       MTEMLE            
Hours Notice:  72                          Seq Num:        60                
Prepared By:   Stacy.rainey                Taken Date:     01/08/13 07:59    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     PINE ENTERPRISE             Excavator Phone:(770) 614-9664    
Address:       796 BROGDON RD              Caller:         LAUREN RUSSELL    
City, St, Zip: SEWANNEE, GA 30024          Caller Phone:   (770) 614-9664    
Contact Fax:                               Contact:        LAUREN RUSSELL    
Contact Email: LAUREN@PINENTERPRISES.COM   Contact Phone:  (770) 614-9664    
Call Back:                                                           

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:00 
County:        WILSON                      Update Date:    01/23/13 AT 00:00 
Place:         MOUNT JULIET                Expire Date:    01/26/13 AT 00:00 

Address:       306 PATRIOTIC WAY                                     
Intersection:  REZEILLE BLVD                                         

Latitude:      36.158767                   Longitude:      -86.476795        
Secondary Lat: 36.162207                   Secondary Long: -86.470895        

Work Type:     WATER & SEWER, INSTL        Explosives: No       WhitePaint: No       
Done For:      NORTHWEST PLUMBING          Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
LAKE PROVIDENCE AT HERMITAGE SUBD -- LOT 787...FROM INTER OF SOUTHERN WAY BLVD
AND DEL WEBB BLVD GO S APPX 250FT AND REZEILLE BLVD IS ON E SIDE OF ST...GO SE
APPX 450FT TO PATRIOTIC WAY...GO S ON PATRIOTIC WAY APPX 150FT TO PROP...     

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK ENTIRE PROP                                                              

--------------------------------------------------------------------------------
Grids:   [95H  ] [95I  ] [96E  ] 
[96L  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
MTEMLE          Middle Tenn Electric Membership Coop - Lebanon ...No                  
MWS             Metro Water & Sewer                               No                  
TT1             Tennessee Telephone - Mt Juliet - TT1             No                  
WWILS           West Wilson Utility District                      No                  
MTJUL           Mt Juliet, City of                                No                  


