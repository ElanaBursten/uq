
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085002                   OLD TICKET NUM: 123550721         

Message Type:  Normal                      For Code:       ME/MTEMMU/INTRU   
Hours Notice:  72                          Seq Num:        3                 
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 05:21    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     LINK UTILITY                Excavator Phone:(270) 392-6783    
Address:       6446 RED RIVER SCHOOL RD    Caller:         STEVE LINK        
City, St, Zip: PORTLAND, TN 37148          Caller Phone:   (270) 392-6783    
Contact Fax:                               Contact:        STEVE LINK        
Contact Email:                             Contact Phone:  (270) 392-6783    
Call Back:     (270) 392-6783                                        

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 05:15 
County:        RUTHERFORD                  Update Date:    01/23/13 AT 00:00 
Place:         MURFREESBORO                Expire Date:    01/26/13 AT 00:00 

Address:       CHERRY LN                                             
Intersection:  CAVALIER DR                                           

Latitude:      35.925494                   Longitude:      -86.390481        
Secondary Lat: 35.925929                   Secondary Long: -86.386447        

Work Type:     POLE(S), ANCHOR(S), REPL    Explosives: No       WhitePaint: Yes      
Done For:      MID TN ELECTRIC             Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
MARK AT PL LOCATED ON THE NE CORNER OF THE INTER.. FROM HERE CROSS THE ST TO  
PL MARKED WITH WHITE . ALSO FROM THE NE CORNER OF INTER GO E APPX. 300FT TO   
THE PL MARKED IN WHITE ON THE N SIDE OF THE RD..THEN FROM HERE GO E APPX.     
625FT TO PL MARKED IN WHITE...                                                

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
MARK A 40FT RADIUS OF ALL THE PL LOCATIONS MARKED IN WHITE....UPDATE/REMARK   
PER STEVE LINK  TKT 123550721                                                 

--------------------------------------------------------------------------------
Grids:   [58C  ] [58D  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
CW              Consolidated Util Dist                            No                  
ME              Murfreesboro Electric (utiliquest)                No                  
MRU             Murfreesboro Water & Sewer                        No                  
MTEMMU          Middle Tenn Electric Membership Coop - Murfrees...No                  
U01             Atmos Energy (United Cities Gas) - Murfreesboro...No                  
INTRU           Comcast- Rutherford - INTRU                       No                  


