
                  ========//  TNOCS LOCATE REQUEST  //========

TICKET NUMBER: 130085254                   OLD TICKET NUM: 123490933         

Message Type:  Normal                      For Code:       NES               
Hours Notice:  72                          Seq Num:        69                
Prepared By:   Tammy.potts                 Taken Date:     01/08/13 08:08    

                             Company Information
--------------------------------------------------------------------------------
Excavator:     SITE TECH                   Excavator Phone:(615) 207-5375    
Address:       PO BOX 261                  Caller:         RICK JEWELL       
City, St, Zip: CHARLOTTE, TN 37036         Caller Phone:   (615) 207-5375    
Contact Fax:   (615) 763-2811              Contact:        RICK JEWELL       
Contact Email:                             Contact Phone:  (615) 207-5375    
Call Back:     615 207-5375                                          

                               Work Information
--------------------------------------------------------------------------------
State:         TN                          Work To Begin:  01/11/13 AT 08:15 
County:        DAVIDSON                    Update Date:    01/23/13 AT 00:00 
Place:         NASHVILLE                   Expire Date:    01/26/13 AT 00:00 

Address:       2618 OLD LEBANON RD                                   
Intersection:  LEBANON PIKE                                          

Latitude:      36.169808                   Longitude:      -86.672976        
Secondary Lat: 36.169808                   Secondary Long: -86.672976        

Work Type:     STORM DRAINAGE, INSTL       Explosives: No       WhitePaint: No       
Done For:      HANNAH CONSTRUCTION         Directional Boring: No                
Extent:                                    ADD'L ADDR IN REMARKS: No                

                       Location Information(DIRECTIONS)
--------------------------------------------------------------------------------
PROP AT WESTERN MOST INTER.                                                   

                        Location Information(REMARKS)
--------------------------------------------------------------------------------
ALSO SITE GRADING.....MARK ENTIRE PROP..        ...UPDATE / REMARK PER RICK   
JEWELL TKT 123490933                                                          

--------------------------------------------------------------------------------
Grids:   [84M  ] [95D  ] 

                              Utilities Notified
--------------------------------------------------------------------------------
Code            Name                                         Manually Added      
--------------- ----------------------------------------------------------------
NES             Nashville Electric Svc (utiliquest) - NES         No                  
MPW             Metro Public Works                                No                  
B01             ATT/D-Nash (270)846-3191 - B01                    No                  
NYG             Piedmont Natural Gas (Nashville Gas)              No                  
NEX             XO Communications - Nashville - NEX               No                  
MWS             Metro Water & Sewer                               No                  
INTMC           Comcast - Metro Center (Davidson) - INTMC         No                  


