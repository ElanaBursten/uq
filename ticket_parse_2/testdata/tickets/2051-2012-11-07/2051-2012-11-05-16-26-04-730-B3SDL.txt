
------=_Part_5687_17936971.1352150705082
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1531 SC811 Remote 11/05/2012 04:22:00 PM 1211052269 Update 

Notice Number:    1211052269        Old Notice:                        
Sequence:         1531              Created By:      R-TRS             

Created:          11/05/12 04:23 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        GIBBS SHOALS RD                                       
Intersection:  DILLARD RD                                            
Subdivision:                                                         

Lat/Long: 34.886682,-82.241653
Second:  34.886682,-82.241653

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

MARK THE ENTIRE INTERSECTION AND BOTH     
SIDES OF ROAD FOR 50 FT IN ALL DIRECTIONS//APPROX TOTAL FT 12500 ENDING AT W  
PHILLIPS RD                                                                   

Directions:
W PHILLIPS RD AND MEDFORD DR ARE OTHER ROADS IN AREA                          

Remarks:


**WORK IS IN PROGRESS**
Updated from V2 notice.                             

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 CCMZ41 



------=_Part_5687_17936971.1352150705082--