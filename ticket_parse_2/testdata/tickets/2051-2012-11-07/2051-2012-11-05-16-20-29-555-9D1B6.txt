
------=_Part_5194_30411263.1352150379490
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1513 SC811 Remote 11/05/2012 04:17:00 PM 1211052240 Update 

Notice Number:    1211052240        Old Notice:                        
Sequence:         1513              Created By:      R-TRS             

Created:          11/05/12 04:18 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        GIBBS SHOALS RD                                       
Intersection:  S HWY 14                                              
Subdivision:                                                         

Lat/Long: 34.877231,-82.241747
Second:  34.88062,-82.240351

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

STARTING AT A POINT OF 10560 FT PAST THE  
INTERSECTION MARK THE LEFT SIDE OF GIBBS SHOALS RD FOR 1320 FT HEADING SOUTH  
TOWARD PHILLIPS RD//S HWY 14 MAY ALSO BE KNOWN AS S MAIN ST//APPROX TOTAL FT  
12500 ENDING AT PHILLIPS DR                                                   

Directions:
MEDFORD DR AND W PHILLIPS RD ARE OTHER ROADS IN AREA                          

Remarks:


**WORK IS IN PROGRESS**
Updated from V2 notice.                             

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 PNGZ81 CCMZ41 



------=_Part_5194_30411263.1352150379490--