
------=_Part_3870_4785696.1352149307879
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1470 SC811 Remote 11/05/2012 03:59:00 PM 1211052163 Update 

Notice Number:    1211052163        Old Notice:                        
Sequence:         1470              Created By:      R-TRS             

Created:          11/05/12 03:59 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        GIBBS SHOALS RD                                       
Intersection:  S HWY 14                                              
Subdivision:                                                         

Lat/Long: 34.901695,-82.245908
Second:  34.904736,-82.243952

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

STARTING AT A POINT OF 1320 FT PAST THE   
INTERSECTION MARK THE LEFT SIDE OF GIBBS SHOALS RD FOR 1320 FT HEADING SW     
TOWARD SUBER RD//S HWY 14 MAY ALSO BE KNOWN AS S MAIN ST//APPROX TOTAL FT     
12500 ENDING AT PHILLIPS DR                                                   

Directions:
SUBER RD AND DILLARD DR ARE OTHER ROADS IN AREA
                              

Remarks:

**WORK IS IN PROGRESS**
Updated from V2 notice.                              

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 CCMZ41 



------=_Part_3870_4785696.1352149307879--