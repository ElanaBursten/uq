
------=_Part_3676_15900622.1352149137613
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 1463 SC811 Remote 11/05/2012 03:56:00 PM 1211052152 Update 

Notice Number:    1211052152        Old Notice:                        
Sequence:         1463              Created By:      R-TRS             

Created:          11/05/12 03:57 PM                                     
Work Date:        11/08/12 11:59 PM                                     
Update on:        11/29/2012        Good Through:    12/04/2012        

Caller Information:
ANSCO & ASSOCIATES
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Business          
Caller:           TAMMY STEGALL       Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  864-295-0235

Excavation Information:
SC    County:  GREENVILLE           Place:  GREER             
Street:        S HWY 14 HWY                                          
Intersection:  GIBBS SHOALS RD                                       
Subdivision:                                                         

Lat/Long: 34.90515,-82.24019
Second:  34.90649,-82.239909

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC CABLE, BURY                                   
Work Done By:  ANSCO & ASSOCIATES, LLCDuration:        6 WEEKS           

Instructions:
WORK IS FOR ATT//REF JOB 39F21108N

STARTING AT INTERSECTION MARK BOTH SIDES  
OF S HWY 14 FOR 500 FT HEADING SE TOWARD SUBER RD//S HWY 14 MAY ALSO BE KNOWN 
AS S MAIN ST                                                                  

Directions:
SUBER RD AND DILLARD DR ARE OTHER ROADS IN AREA
                              

Remarks:

**WORK IS IN PROGRESS**
Updated from V2 notice.                              

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 CCMZ41 



------=_Part_3676_15900622.1352149137613--