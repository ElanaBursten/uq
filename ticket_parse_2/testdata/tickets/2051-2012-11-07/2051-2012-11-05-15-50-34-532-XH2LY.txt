
------=_Part_2519_20656869.1352148334581
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 29 SC811 Remote 11/04/2012 10:19:00 PM 1211040046 Normal 

Notice Number:    1211040046        Old Notice:                        
Sequence:         29                Created By:      R-TRS             

Created:          11/04/12 10:21 PM                                     
Work Date:        11/07/12 11:59 PM                                     
Update on:        11/28/2012        Good Through:    12/03/2012        

Caller Information:
ANSCO & ASSOCIATES 
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Contractor        
Caller:            TAMMY STEGALL      Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  SPARTANBURG          Place:  GREER             
Street:        OLD WOODRUFF RD                                       
Intersection:  MCELRATH RD                                           
Subdivision:                                                         

Lat/Long: 34.919647,-82.212758
Second:  34.921351,-82.212518

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC, RELOCATE CABLE                                   
Work Done By:  ANSCO                Duration:        1 WEEK            

Instructions:
WORK IS FOR ATT//REF JOB 29F22288N

STARTING AT INTERSECTION MARK BOTH SIDES  
OF OLD WOODRUFF RD HEADING SOUTH TO NEW WOODRUFF RD  FOR APPROX 625 FT        

Directions:
OTHER ROADS IN AREA: NEW WOODRUFF RD AND MAPLE DR                             

Remarks:

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 



------=_Part_2519_20656869.1352148334581--