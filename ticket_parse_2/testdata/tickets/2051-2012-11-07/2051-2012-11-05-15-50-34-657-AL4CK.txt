
------=_Part_2520_25846336.1352148334812
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 31 SC811 Remote 11/04/2012 10:24:00 PM 1211040048 Normal 

Notice Number:    1211040048        Old Notice:                        
Sequence:         31                Created By:      R-TRS             

Created:          11/04/12 10:27 PM                                     
Work Date:        11/07/12 11:59 PM                                     
Update on:        11/28/2012        Good Through:    12/03/2012        

Caller Information:
ANSCO & ASSOCIATES 
43 SENTELL RD
GREENVILLE, SC 29611
Company Fax:                        Type:            Contractor        
Caller:            TAMMY STEGALL      Phone:         (864) 295-0235 Ext:
Caller Email:     TAMMY.STEGALL@ANSCOLLC.COM                            

Site Contact Information:
TAMMY STEGALL                              Phone:(864) 295-0235 Ext:
Site Contact Email:                                                        
CallBack:  

Excavation Information:
SC    County:  SPARTANBURG          Place:  GREER             
Street:        OLD WOODRUFF RD                                       
Intersection:  NEW WOODRUFF RD                                       
Subdivision:                                                         

Lat/Long: 34.919181,-82.212514
Second:  34.919571,-82.212346

Explosives:  N Premark:  N Drilling/Boring:  Y Near Railroad:  N

Work Type:     TELEPHONE FIBER OPTIC, RELOCATE CABLE                                   
Work Done By:  ANSCO                Duration:        1 WEEK            

Instructions:
WORK IS FOR ATT//REF JOB 29F22288N

STARTING AT INTERSECTION MARK BOTH SIDES  
OF OLD WOODRUFF RD HEADING SOUTH TO MAPLE DR FOR APPROX 150 FT                

Directions:
OTHER ROADS IN AREA: MCELRATH RD AND MAPLE DR                                 

Remarks:

Member Utilities Notified:
BSZT29 GRR01 DPCZ08 



------=_Part_2520_25846336.1352148334812--