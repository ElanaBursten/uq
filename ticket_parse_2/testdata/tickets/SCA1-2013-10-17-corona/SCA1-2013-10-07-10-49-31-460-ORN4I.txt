
UQSTSO 00129A USAS 10/07/13 07:48:44 A32800114-00A NORM NEW GRID

Ticket : A32800114  Date: 10/07/13 Time: 07:45 Oper: TEL Chan: 100
Old Tkt: A32800114  Date: 10/07/13 Time: 07:48 Oper: TEL Revision: 00A

Company: VALVERDE CONSTRUCTION          Caller: BUEN
Co Addr: 10936 SHOEMAKER AVE
City&St: SANTA FE SPRINGS, CA           Zip: 90670      Fax: 562-906-1918
Phone: 562-906-1826 Ext:      Call back: 7AM-5PM
Formn: DANNY ORTIZ          Phone: 310-629-1105
Email: BUENHATA@YAHOO.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:E ONTARIO AVE
X/ST 1 : CALIFORNIA AVE
MPM 1:             MPM 2:
Locat: BOTH SIDES/OF E ONTARIO AVE AT APPROXIMATELY 250FT S/E OF CALIFORNIA AVE

Excav Enters Into St/Sidewalk: Y

Grids: 0773G024
Lat/Long  : 33.846704/-117.538633 33.846244/-117.537844
          : 33.846024/-117.539030 33.845564/-117.538241
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR WTR LINE
Wkend: N  Night: N
Work date: 10/09/13 Time: 07:46 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF CORONA

Tkt Exp: 11/04/13

Mbrs : ATTDSOUTH     UCOR19  EVW37  LVL3CM SAW01  SCG1CO SUNESYSLLC    TWTLH
USCE34 USCE83RIV     UTWCWRIV      UVZMENIF      UVZPERS
