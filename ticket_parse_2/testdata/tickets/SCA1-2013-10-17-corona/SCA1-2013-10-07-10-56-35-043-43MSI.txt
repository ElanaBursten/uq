
UQSTSO 00140A USAS 10/07/13 07:52:23 A32800127-00A NORM NEW GRID

Ticket : A32800127  Date: 10/07/13 Time: 07:47 Oper: TEL Chan: 100
Old Tkt: A32800127  Date: 10/07/13 Time: 07:51 Oper: TEL Revision: 00A

Company: VALVERDE CONSTRUCTION          Caller: BUEN
Co Addr: 10936 SHOEMAKER AVE
City&St: SANTA FE SPRINGS, CA           Zip: 90670      Fax: 562-906-1918
Phone: 562-906-1826 Ext:      Call back: 7AM-5PM
Formn: DANNY ORTIZ          Phone: 310-629-1105
Email: BUENHATA@YAHOO.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:BORDER AVE
X/ST 1 : COPLEN CIR
MPM 1:             MPM 2:
Locat: BOTH SIDES/OF BORDER AVE AT APPROX 130FT N/E OF COPLIN CIR

Excav Enters Into St/Sidewalk: Y

Grids: 0772J011
Lat/Long  : 33.859467/-117.603987 33.859738/-117.603146
          : 33.858693/-117.603738 33.858964/-117.602897
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REPAIR WTR LINE
Wkend: N  Night: N
Work date: 10/09/13 Time: 07:48 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF CORONA

Tkt Exp: 11/04/13

Mbrs : ATTDSOUTH     COR19  MWD06  SCG1CO SUNESYSLLC    USCE34 UTWCWRIV
