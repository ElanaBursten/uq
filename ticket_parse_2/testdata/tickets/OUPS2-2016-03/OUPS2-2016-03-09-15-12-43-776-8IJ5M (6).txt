

CGR    00021 OUPSa 03/04/16 13:30:49 A606402033-00A ROUT NEW POLY LREQ

Ticket : A606402033 Rev: 00A Taken: 03/04/16 01:27 PM Channel: OPR

State: OH  Cnty: CLARK       Place: BETHEL TWP

Address : 1560  Street: SNIDER RD
Cross 1 : LOWER VALLEY PIKE                         Intersection: Y
Rail/Hwy:      Milemarker(s):
Where   : FRONT & REAR OF PROPERTY
        : FARTHEST POINT OFF ROAD: 200FT
        : DISTANCE FROM CROSS STREET: CORNER LOT

WorkType: ELECTRIC LINE INSTALLATION
Done for:
Done by :
Whitelined: N  Blasting: N
Means of Excavation: HAND TOOLS

Work date: 03/08/16 01:45 PM  Meet: N
Start by : 03/18/16 01:42 PM  Response Due: 03/08/16 01:42 PM

Best Fit: 39.915501/-83.970378 39.916075/-83.967843
        : 39.897841/-83.966379 39.898416/-83.963843

Comments:

Caller  : MELVIN CLINE                    Phone: 240-427-7950
Company : MELVIN CLINE                              Type: HOME
Co addr : 1560 SNIDER RD
City    : NEW CARLISLE                    St: OH  Zip: 45344

Members:
BTH    =BETHEL TOWNSHIP (CLARK COUNTY)  CGR    =COLUMBIA GAS OF OHIO -  SPRING
IFNP   =INDEPENDENTS FIBER NETWORK / U  OBF    =AT&T - OHIO
OEI    =OHIO EDISON   - SPRINGFIELD (U  VSDP   =DONNELSVILLE - VILLAGE OF
WSOP   =TIME WARNER CABLE - SE OHIO /U
