

CGE    00107 OUPSb 02/29/16 12:24:56 B606000316-00B ROUT NEW POLY LREQ

Ticket : B606000316 Rev: 00B Taken: 02/29/16 12:21 PM Channel: WEB

State: OH  Cnty: LICKING     Place: HARRISON TWP

Address : 161  Street: MANNASEH DRIVE EAST SW
Cross 1 : ELLINGTON BLVD SW                         Intersection: N
Cross 2 : JOHNATHAN DR                                   Between: N
Rail/Hwy:      Milemarker(s):
Where   : ENTIRE PROPERTY CALLER REQUESTING THAT ALL UTILITIES USE FLAGS WHEN
        : MARKING LINES

WorkType: POLE & ANCHOR INSTALLATION & BURY CABLE FOR SATELITE TV
Done for:
Done by :
Whitelined: N  Blasting: N
Means of Excavation: HAND TOOLS

Work date: 03/02/16 12:36 PM  Meet: N
Start by : 03/14/16 12:36 PM  Response Due: 03/02/16 12:36 PM

Best Fit: 40.029881/-82.612461 40.031678/-82.610343
        : 40.027003/-82.610019 40.028799/-82.607901

Comments: DD REF# 909081698281

Caller  : DENTON TROYER                   Phone: 330-231-2221
Company : DIGITAL DISH                              Type: CONT
Co addr : 5555 COUNTY RD 203
City    : MILLERSBURG                     St: OH  Zip: 44654
Alt Tel#: 330-231-2221
Email: oupstickets@safe7.com

Members:
CGE    =COLUMBIA GAS OF OHIO - COLUMBU  CSP    =AEP COLUMBUS SOUTHERN POWER  (
NGOP   =NATIONAL GAS & OIL (ENERGY COO  OBF    =AT&T - OHIO
SWC    =SOUTHWEST LICKING WAT & SEW  (  UTBP   =CTLCL - CENTURYLINK   (USIC)
WCOP   =TIME WARNER CABLE - CENTRAL OH
