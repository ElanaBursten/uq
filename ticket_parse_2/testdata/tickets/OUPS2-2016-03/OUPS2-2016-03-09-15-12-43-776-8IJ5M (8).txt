

CGR    00023 OUPSa 03/04/16 14:40:12 A606402319-00A ROUT NEW POLY LREQ

Ticket : A606402319 Rev: 00A Taken: 03/04/16 02:35 PM Channel: OPR

State: OH  Cnty: CHAMPAIGN   Place: URBANA

Address : 953  Street: N MAIN ST
Cross 1 : HARMON AVE                                Intersection: N
Cross 2 : TAFT AVE                                       Between: Y
Rail/Hwy:      Milemarker(s):
Where   : FRONT AND REAR OF PROPERTY - MILLER'S AUTO SHOP
        : FARTHEST POINT OFF ROAD: 300 FT EAST
        : DISTANCE FROM CROSS STREET: BETWEEN

WorkType: FENCE INSTALLATION AND SIGN INSTALLATION
Done for:
Done by :
Whitelined: N  Blasting: N
Means of Excavation: BOBCAT W/POST HOLE DIGGER

Work date: 03/08/16 02:50 PM  Meet: N
Start by : 03/18/16 02:50 PM  Response Due: 03/08/16 02:50 PM

Best Fit: 40.121582/-83.750851 40.121297/-83.748149
        : 40.119275/-83.751094 40.118990/-83.748393

Comments:

Caller  : DUSTIN MILLER                   Phone: 937-653-6897
Company : DUSTIN MILLER                             Type: HOME
Co addr : 953 N MAIN ST
City    : URBANA                          St: OH  Zip: 43078

Members:
CCM    =C T COMMUNICATIONS              CGR    =COLUMBIA GAS OF OHIO -  SPRING
DPL    =DAYTON POWER & LIGHT            URBS   =URBANA - CITY OF (SEWER)
URBTR  =URBANA - CITY OF (TRAFFIC)      URBW   =URBANA - CITY OF (WATER)
WSOP   =TIME WARNER CABLE - SE OHIO /U
