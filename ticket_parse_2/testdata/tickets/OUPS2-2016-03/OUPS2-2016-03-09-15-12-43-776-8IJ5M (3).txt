

CGR    00018 OUPSa 03/04/16 12:40:52 A606401836-00A ROUT NEW POLY LREQ

Ticket : A606401836 Rev: 00A Taken: 03/04/16 12:32 PM Channel: OPR

State: OH  Cnty: CLARK       Place: SPRINGFIELD

Address :   Street: EAGLE CITY RD
Cross 1 : N BECHTLE AVE                             Intersection: N
Rail/Hwy:      Milemarker(s):
Where   : SOUTH SIDE OF THE ROAD STARTING 500 FT WEST OF N BECHTLE AVE AND GOING
        : 750 FT WEST - ENTIRE DIG AREA IS ACROSS THE ROAD FROM THE SOCCER
        : FIELDS - 5 LOCATIONS ARE STAKED AND PAINTED WHITE ON THE ROAD
        : FARTHEST POINT OFF ROAD:  40 FT

WorkType: SETTING 5 POLES
Done for:
Done by :
Whitelined: N  Blasting: N
Means of Excavation: AUGER

Work date: 03/08/16 12:47 PM  Meet: N
Start by : 03/18/16 12:47 PM  Response Due: 03/08/16 12:47 PM

Best Fit: 39.975404/-83.818608 39.974814/-83.810713
        : 39.974278/-83.818692 39.973688/-83.810797

Comments:

Caller  : RICH LAPSO                      Phone: 937-327-1257
Company : OHIO EDISON                               Type: MEMB
Co addr : 420 S YORK ST
City    : SPRINGFIELD                     St: OH  Zip: 45505
Alt Tel#: 937-206-2579

Members:
ATBP   =AT&T - ATT/T (AT&T TRANSMISSIO  CGR    =COLUMBIA GAS OF OHIO -  SPRING
OBF    =AT&T - OHIO                     OEI    =OHIO EDISON   - SPRINGFIELD (U
SPRS   =SPRINGFIELD - CITY OF (SEWER)   SPRTR  =SPRINGFIELD - CITY OF (TRAFFIC
SPRW   =SPRINGFIELD - CITY OF (WATER)
