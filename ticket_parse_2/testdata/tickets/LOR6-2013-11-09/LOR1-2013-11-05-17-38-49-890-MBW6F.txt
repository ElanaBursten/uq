

From: IRTHNet  At: 11/05/13 05:37 PM  Seq No: 5

Ticket No: 13242975               48 HOUR NOTICE 
Send To: PGE01      Seq No:  225  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:38 PM    Op: webusr 
Original Call Date: 11/05/13   Time:  2:35 PM    Op: webusr 
Work to Begin Date: 11/18/13   Time:  7:45 AM 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address:   4509      Street: SE 128TH AVE 
Nearest Intersecting Street: SE HOLGATE BLVD 

Twp: 1S    Rng: 2E    Sect-Qtr: 11-SW,14-NW 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.489634 Lon: -122.5332444SE Lat: 45.487953 Lon: -122.532221 

Type of Work: TREE PLANTING 
Location of Work: PLEASE MARK ALL RIGHT-OF-WAY FRONTAGES FOR THE ABOVE
: REFERENCED HOUSE #.  THANK YOU! 

Remarks:  
:  

Company     : CASCADIAN LANDSCAPERS INC        Best Time:  8AM TO 4PM 
Contact Name: CORRISA GRAU                     Phone: (503)647-9933 
Email Address:  ART@CASCADIANLANDSCAPERS.COM 
Alt. Contact: ART MEISNER                      Phone: (503)647-9933 
Contact Fax : (503)647-9922 
Work Being Done For: CITY OF PORTLAND 
Additional Members:  
CMCST01    NWN01      PTLD03     QLNOR17
