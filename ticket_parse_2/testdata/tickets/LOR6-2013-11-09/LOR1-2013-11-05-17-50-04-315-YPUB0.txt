

From: IRTHNet  At: 11/05/13 05:48 PM  Seq No: 14

Ticket No: 13242991               48 HOUR NOTICE 
Send To: PGE01      Seq No:  229  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:48 PM    Op: webusr 
Original Call Date: 11/05/13   Time:  2:43 PM    Op: webusr 
Work to Begin Date: 11/07/13   Time:  2:45 PM 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address:   2222      Street: SE 48TH AVE 
Nearest Intersecting Street: SE SHERMAN ST 

Twp: 1S    Rng: 2E    Sect-Qtr: 6-SW 
Twp: 1S    Rng: 2E    Sect-Qtr: 6-SW   
Ex. Coord NW Lat: 45.508013 Lon: -122.614233 SE Lat: 45.506653 Lon: -122.612930 

Type of Work: INSTALL WATER SERVICE 
Location of Work: MARK ENTIRE ROW IN FRONT OF ABOVE ADDRESS. AREA MARKED IN
: WHITE PAINT. 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION AND 1/4 SCT INFO 
:  

Company     : PORTLAND WATER BUREAU            Best Time:   
Contact Name: DONNIE LEDFORD                   Phone: (503)823-4874 
Email Address:  DONNIE.LEDFORD@PORTLANDOREGON.GOV 
Alt. Contact: JOHN DILG                        Phone: (503)823-8337 
Contact Fax : (503)823-4117 
Work Being Done For: PORTLAND WATER BUREAU 
Additional Members:  
CMCST01    NWN01      PTLD03     QLNOR17
