

From: IRTHNet  At: 11/05/13 05:45 PM  Seq No: 10

Ticket No: 13242986               48 HOUR NOTICE 
Send To: PGE01      Seq No:  227  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:46 PM    Op: webusr 
Original Call Date: 11/05/13   Time:  2:43 PM    Op: webusr 
Work to Begin Date: 11/18/13   Time:  7:45 AM 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address:   3928      Street: SE 134TH AVE 
Nearest Intersecting Street: SE 133RD AVE 

Twp: 1S    Rng: 2E    Sect-Qtr: 11-SW-SE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.4950165Lon: -122.525914 SE Lat: 45.492552 Lon: -122.5244675 

Type of Work: TREE PLANTING 
Location of Work: PLEASE MARK ALL RIGHT-OF-WAY FRONTAGES FOR THE ABOVE
: REFERENCED HOUSE #.  THANK YOU! 

Remarks:  
:  

Company     : CASCADIAN LANDSCAPERS INC        Best Time:  8AM TO 4PM 
Contact Name: CORRISA GRAU                     Phone: (503)647-9933 
Email Address:  ART@CASCADIANLANDSCAPERS.COM 
Alt. Contact: ART MEISNER                      Phone: (503)647-9933 
Contact Fax : (503)647-9922 
Work Being Done For: CITY OF PORTLAND 
Additional Members:  
CMCST01    NWN01      PTLD03     QLNOR17
