

From: IRTHNet  At: 11/05/13 05:33 PM  Seq No: 1

Ticket No: 13242963               48 HOUR NOTICE 
Send To: PGE01      Seq No:  222  Map Ref:  

Transmit      Date: 11/05/13   Time:  2:33 PM    Op: orandr 
Original Call Date: 11/05/13   Time:  2:27 PM    Op: XMLUSE 
Work to Begin Date: 11/07/13   Time:  2:30 PM 

State: OR            County: MULTNOMAH               Place: PORTLAND 
Address:   8910      Street: SE CLAYBOURNE STREET 

Twp: 1S    Rng: 2E    Sect-Qtr: 16-SW-SE,21-NW-NE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 45.475186 Lon: -122.5720091SE Lat: 45.4743265Lon: -122.5704305 

Type of Work: SIGN INSTALL 
Location of Work: LOCATE ONLY WITHIN 3 FEET OF WHITE MARKER FLAG OR COMPANY
: STAKE SIGN, OR IF NO FLAG, LOCATE ONLY A 5 FOOT SQUARE WHERE DRIVEWAY MEETS
: SIDEWALK.
: DO NOT PAINT ON PAVED OR PERMANENT SURFACES.
: DO NOT MARK FROM SOURCE.
: USE AS LITTLE PAINT AS POSSIBLE.
: NO REPLY NEEDED UNLESS CONFLICT FOUND. 

Remarks:  
:  

Company     : NW REALTY SIGN                   Best Time:   
Contact Name: CAROL LAFATA                     Phone: (503)735-0445 
Email Address:  orders@nwrealtysign.com 
Alt. Contact: SAME                             Phone:  
Contact Fax :  
Work Being Done For: NW REALTY SIGN 
Additional Members:  
CMCST01    NWN01      PTLD03     QLNOR17
