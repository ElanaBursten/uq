
New Jersey One Call System        SEQUENCE NUMBER 0068    CDC = CC3 

Transmit:  Date: 06/30/11   At: 15:03 

*** R O U T I N E         *** Request No.: 111811574 

Operators Notified: 
BAN     = VERIZON                       CC3     = COMCAST CABLEVISION OF CE      
DTN     = G4S TECHNOLOGY                EBW     = EAST BRUNSWICK WATER  UTI      
GPS     = JERSEY CENTRAL POWER & LI     LKT     = LEVEL 3 COMMUNICATIONS         
NJT     = NEW JERSEY TURNPIKE AUTHO     P29     = PUBLIC SERVICE ELECTRIC &      
SUN     = RUTGERS, THE STATE UNIVER     TGP     = TRANSCONTINENTAL GAS PIPE      
TMF     = G4S TECHNOLOGY LLC             

Start Date/Time:    07/07/11   At 07:00  Expiration Date: 09/02/11 

Location Information: 
County: MIDDLESEX              Municipality: EAST BRUNSWICK 
Subdivision/Community: NEW JERSEY TURNPIKE RITE OF WAY 
Street:               0 STATE RTE 18 
Nearest Intersection: NARICON PL 
Other Intersection:    
Lat/Lon: 
Type of Work: SOIL BORINGS 
Block:                Lot:                Depth: 100FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 800FT N.  CURB
  TO 200FT BEHIND BOTH CURBS.  CURB TO CURB. 
Remarks:  
  Working For Contact:  CHRIS ELLIS 

Working For: JACOBS 
Address:     450 MADISON AVE 
City:        MORRIS, NJ  07962 
Phone:       973-267-0555   Ext:  

Excavator Information: 
Caller:      SCOTT CRAIG 
Phone:       609-625-4862   Ext:  

Excavator:   CRAIG TEST BORING CO 
Address:     5435 HARDING HWY PO BOX 427 
City:        MAYS LANDING, NJ  08330 
Phone:       609-625-4862   Ext:          Fax:  609-625-4306 
Cellular:    908-910-0246 
Email:       scraig@craigtest.com 
End Request 
