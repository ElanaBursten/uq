
New Jersey One Call System        SEQUENCE NUMBER 0125    CDC = GSC 

Transmit:  Date: 06/30/11   At: 15:02 

*** R O U T I N E         *** Request No.: 111811531 

Operators Notified: 
BAN     = VERIZON                       GSC     = COMCAST CABLEVISION OF GA      
MLT     = MOUNT LAUREL TOWNSHIP MUA     P34     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    07/07/11   At 00:15  Expiration Date: 09/02/11 

Location Information: 
County: BURLINGTON             Municipality: MOUNT LAUREL 
Subdivision/Community:  
Street:               201 CEMETERY ROAD 
Nearest Intersection: HAINESPORT MT LAUREL ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL PHONE SERVICE 
Block:                Lot:                Depth: 4FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks:  
  Working For Contact:  KEN FREYER 

Working For: VERIZON WEST 
Address:     10 TANSBORO RD 
City:        BERLIN, NJ  08009 
Phone:       609-390-9940   Ext:  

Excavator Information: 
Caller:      JOSHUA LEFEVRE 
Phone:       856-728-7773   Ext:  

Excavator:   KLINE CONSTRUCTION 
Address:     822 GLASSBORO ROAD 
City:        WILLIAMSTOWN, NJ  08094 
Phone:       856-728-7773   Ext:          Fax:  856-728-7564 
Cellular:     
Email:       josh@klineconstruction.net 
End Request 
