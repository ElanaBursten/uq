
New Jersey One Call System        SEQUENCE NUMBER 0018    CDC = CAR 

Transmit:  Date: 06/30/11   At: 15:04 

*** E M E R G E N C Y     *** Request No.: 111811580 

Operators Notified: 
BAN     = VERIZON                       CAR     = CABLEVISION OF NJ              
PS7     = PUBLIC SERVICE ELECTRIC &     TGP     = TRANSCONTINENTAL GAS PIPE      
UW4     = /UWNJ-NE                       

Start Date/Time:    06/30/11   At 15:15  Expiration Date:  

Location Information: 
County: HUDSON                 Municipality: NORTH BERGEN 
Subdivision/Community:  
Street:               0 71ST ST 
Nearest Intersection: TONNELLE AVE 
Other Intersection:    
Lat/Lon: 
Type of Work: EMERGENCY - SEARCH AND REPAIR GAS LEAK 
Block:                Lot:                Depth: 4FT 
Extent of Work: M/O BEGINS AT C/L OF INTERSECTION AND EXTENDS 100FT IN ALL
  DIRECTIONS.  CURB TO CURB. 
Remarks:  
  Working For Contact:  ELLEN WATSON 

Working For: PUBLIC SERVICE ELECTRIC & GAS 
Address:     444 ST PAUL'S AVE 
City:        JERSEY CITY, NJ  07306 
Phone:       201-420-3946   Ext:  

Excavator Information: 
Caller:      ELLEN WATSON 
Phone:       201-420-3946   Ext:  

Excavator:   PUBLIC SERVICE ELECTRIC & GAS 
Address:     444 ST PAUL'S AVE 
City:        JERSEY CITY, NJ  07306 
Phone:       201-420-3946   Ext:          Fax:  201-653-1503 
Cellular:    201-420-3946 
Email:       ELLEN.WATSON@PSEG.COM 
End Request 
