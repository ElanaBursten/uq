
GSUPLS101472123-00	JC	2010/05/27 21:10:21	00741

New Jersey One Call System        SEQUENCE NUMBER 0135    CDC = GPC 

Transmit:  Date: 05/27/10   At: 21:09 

*** R O U T I N E         *** Request No.: 101472123 

Operators Notified: 
BAN     = VERIZON                       DVM     = TOMS RIVER MUNICIPAL UTIL 
GPC     = JERSEY CENTRAL POWER & LI     NJN     = NEW JERSEY NATURAL GAS CO 
UW7     = /UWTR-TOMS RIVER 

Start Date/Time:    06/03/10   At 08:00  Expiration Date: 08/02/10 

Location Information: 
County: OCEAN                  Municipality: TOMS RIVER 
Subdivision/Community: 
Street:               27 CHANNEL RD 
Nearest Intersection: GREEN ISLAND ROAD 
Other Intersection: 
Lat/Lon: 
Type of Work: DIGGING 
Block:                Lot:                Depth: 3FT 
Extent of Work: CURB TO ENTIRE PROPERTY. 
Remarks: 

Working For: MRS DEFARIO 
Address:     27 CHANNEL RD 
City:        TOMS RIVER, NJ  08753 
Phone:       732-279-0484   Ext: 

Excavator Information: 
Caller:      GEORGE MORANO 
Phone:       732-920-1962   Ext: 

Excavator:   MORANO LANDSCAPING 
Address:     PO BOX 901 
City:        BRICK, NJ  08723 
Phone:       732-920-1962   Ext:          Fax:  732-920-0160 
Cellular:    732-267-3035 
Email: 
End Request

--------------------------------------------------------------------------------

Summary of tickets sent to Jersey Central Light & Power at 2010/05/27 09:10PM

Ticket ID        Seq.  Notice ID            Priority         Work Start
---------------  ----  -------------------  ---------------  ------------------
101472123        0741  GSUPLS101472123-00   Regular Notice   2010/06/03 08:00AM
