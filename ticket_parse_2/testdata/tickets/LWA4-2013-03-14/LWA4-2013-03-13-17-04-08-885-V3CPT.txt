

From: IRTHNet  At: 03/13/13 04:03 PM  Seq No: 314

UULC 
Ticket No: 13056422               2 FULL BUSINESS 
Send To: QLNWA30    Seq No:    8  Map Ref:  

Transmit      Date:  3/13/13   Time:  1:40 pm    Op: orshan 
Original Call Date:  3/13/13   Time:  1:34 pm    Op: orshan 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: WHATCOM                 Place: BELLINGHAM 
Address: 4280        Street: MERIDIAN ST 
Nearest Intersecting Street: KELLOGG 

Twp: 38N   Rng: 3E    Sect-Qtr: 7-SW-NW 
Twp: 38N   Rng: 2E    Sect-Qtr: 12-SE-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 48.7965700 Lon:-122.4861339 SE Lat: 48.7950351 Lon:-122.4838404 

Type of Work: REMOVE CONCTRETE/ INSTALL BALLARDS 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: SITE IS AT THE NE CORNER OF THE ABV INTER. MARK THE AREA MARKED IN WHITE AT
: THE NE CORNER OF THE PROP BEHIND BUILDING BY AIR AND GAS CYLINDERS AT
: CONCRETE PAD IN FRONT OF FENCE 

Remarks: BEST INFORMATION AVAILABLE 
:  

Company     : HUIZENGA ENTERPRISES LLC 
Contact Name: RICHARD HUIZENGA                 Phone: (360)303-8061 
Cont. Email : richard@huizengaenterprises.com 
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: ST JOES HOSPITAL 
Additional Members:  
BELLNH01   BLKRC01    CC7770     CNG16      PUGE11     WSDOT08
