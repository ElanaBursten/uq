

From: IRTHNet  At: 03/13/13 04:04 PM  Seq No: 320

IEUCC 
Ticket No: 13056227               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   78  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:23 pm    Op: orsusa 
Original Call Date:  3/13/13   Time: 11:36 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 9406        Street: EVENING CT 
Nearest Intersecting Street: DAWN 

Twp: 26N   Rng: 42E   Sect-Qtr: 14-SW-SE,23-NW-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7439970 Lon:-117.4656247 SE Lat: 47.7429522 Lon:-117.4640393 

Type of Work: SETTING A TEMPORARY POWER POLE 
Location of Work: LOCATE A 5FT RADIUS AROUND THE TRANSFORMER  TAKE EXIT 289
: OFF I-90  GO NORTH ON DIVISION  GO WEST ON FRANCIS  GO NORTH ON ELGIN GO
: WEST ON FIVE MILE RD GO WEST ON STRONG  GO NORTH ON DORSET  GO WEST ON DAWN
: GO NORTH ON EVENING COURT 

Remarks: LOT 2  BLOCK 3 
:  

Company     : PROVIDENT ELECTRIC 
Contact Name: JASON SCOTT                      Phone: (509)926-4779 
Cont. Email : SPOKANE@PROVIDENTELECTRIC.NET 
Alt. Contact:                                  Phone: (509)926-4779 
Contact Fax : (509)926-4863 
Work Being Done For: COPPER BASIN CONSTRUCTION 
Additional Members:  
AVISTA08   CC7730     SPENG01    SPOKAN02
