

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 339

UULC 
Ticket No: 13056122               2 FULL BUSINESS 
Send To: QLNWA16    Seq No:   72  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:51 am    Op: webusr 
Original Call Date:  3/13/13   Time:  9:06 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: KING                    Place: SEATTLE 
Address: 1538        Street: NE 107TH ST 
Nearest Intersecting Street: 15TH AVE NE 

Twp: 26N   Rng: 4E    Sect-Qtr: 33-NW,32-NE,29-SE,28-SW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.7080670 Lon:-122.3124089 SE Lat: 47.7054210 Lon:-122.3097540 

Type of Work: WATER SERVICE INSTALLATION 
Location of Work: MARK OUT ENTIRE AREA MARKED OUT IN WHITE PAINT IN NE 107TH
: ST, 200 FEET EAST OF 15TH AVE NE. 

Remarks: THOMAS GUIDE MAP PAGE 505-C6 
:  

Company     : SEATTLE PUBLIC UTILITIES 
Contact Name: JASON HOVER                      Phone: (206)233-2003 
Cont. Email : JASON.HOVER@SEATTLE.GOV 
Alt. Contact: MARK PECKHAM                     Phone: (206)786-7537 
Contact Fax :  
Work Being Done For: SPU WATER 
Additional Members:  
360NET01   ATT08      CC7700     KCMTRO01   LEVL301    PUGG03     SEACL01 
 SEAH2001   SEASIG01   SEAWW01    XO02
