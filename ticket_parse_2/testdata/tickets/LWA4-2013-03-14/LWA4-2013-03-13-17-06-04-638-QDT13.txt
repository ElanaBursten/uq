

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 351

IEUCC 
Ticket No: 13056104               2 FULL BUSINESS 
Send To: QLNWA32    Seq No:   54  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:43 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:40 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 4118        Street: S PITTSBURG 
Nearest Intersecting Street: 42ND 

Twp: 25N   Rng: 43E   Sect-Qtr: 33-SW 
Twp: *MORE Rng: 43E   Sect-Qtr: 4-NW 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6177928 Lon:-117.3867035 SE Lat: 47.6148650 Lon:-117.3827853 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 200FT NORTH OF 42ND ON THE WEST SIDE OF PITTSBURG.
: MARK 5FT RADIUS OF GAS METER LOCATED NORTH OUT.  METER IS FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #393323470 
Additional Members:  
AVISTA07   CC7730     QLNWA34    SPOKAN01   SPOKAN02   SPOKAN03
