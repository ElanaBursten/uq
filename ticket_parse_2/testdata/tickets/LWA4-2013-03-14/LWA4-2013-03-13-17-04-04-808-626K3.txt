

From: IRTHNet  At: 03/13/13 04:03 PM  Seq No: 300

NUNC 
Ticket No: 13056294               2 FULL BUSINESS 
Send To: QLNWA06    Seq No:   52  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:34 pm    Op: orruth 
Original Call Date:  3/13/13   Time: 12:29 pm    Op: orruth 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: CLARK                   Place: RIDGEFIELD 
Address: 23501       Street: NE 10TH AVE 
Nearest Intersecting Street: NE 234TH ST 

Twp: 4N    Rng: 1E    Sect-Qtr: 34-NE,35-NW 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad: 83    Lat: 45 47.456  Lon:-122 39.687    Zone:  
ExCoord NW Lat: 45.7915876 Lon:-122.6620370 SE Lat: 45.7904917 Lon:-122.6595536 

Type of Work: PLACE ANCHOR 
Location of Work: EXCAVATION SITE IS ON THE E SIDE OF THE ROAD. 
: ADD APX 150FT N OF ABV INTER.  MARK APX 30FT RADIUS OF POLE IN FRONT OF ABV
: ADD AT END OF DRIVEWAY. 

Remarks: AREA IS MARKED IN WHITE 
: CALLER GAVE LATITUDE AND LONGITUDE 

Company     : OREGON AERIAL CONSTRUCTION 
Contact Name: BILL SMITH                       Phone: (503)816-5112 
Cont. Email : THEDUDE322@GMAIL.COM 
Alt. Contact: MIKE SOUTH - CALL 1S             Phone: (360)433-7887 
Contact Fax :  
Work Being Done For: CENTURY LINK 
Additional Members:  
CMCST03    CPU01      CPU03      L3C02
