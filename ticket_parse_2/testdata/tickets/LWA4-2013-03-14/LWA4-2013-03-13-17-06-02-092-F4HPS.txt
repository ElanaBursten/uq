

From: IRTHNet  At: 03/13/13 04:05 PM  Seq No: 346

IEUCC 
Ticket No: 13056119               2 FULL BUSINESS 
Send To: QLNWA34    Seq No:   11  Map Ref:  

Transmit      Date:  3/13/13   Time: 10:49 am    Op: webusr 
Original Call Date:  3/13/13   Time: 10:46 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: SPOKANE                 Place: SPOKANE 
Address: 4119        Street: S MARTIN 
Nearest Intersecting Street: 42ND 

Twp: 25N   Rng: 43E   Sect-Qtr: 33-SW 
Twp: *MORE Rng: 43E   Sect-Qtr: 4-NW-NE 
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 47.6179896 Lon:-117.3826426 SE Lat: 47.6148375 Lon:-117.3787983 

Type of Work: CORE DRILL AROUND GAS RISER 
Location of Work: SITE IS 118FT NORTH OF 42ND ON THE EAST SIDE OF MARTIN.
: MARK 5FT RADIUS OF GAS METER LOCATED SOUTH OUT.  METER IS FLAGGED.  CONTACT
: BRIT 495-4963 

Remarks: CALLER GAVE TOWNSHIP, RANGE, SECTION INFO 
:  

Company     : AVISTA UTILITIES 
Contact Name: AMY COLE                         Phone: (509)495-2849 
Cont. Email : ROBBIN.KOHLSTEDT@AVISTACORP.COM 
Alt. Contact: KATY                             Phone: (509)495-4152 
Contact Fax : (509)777-9511 
Work Being Done For: AVISTA #148323474 
Additional Members:  
AVISTA07   CC7730     FBRLNK01   QLNWA32    SPENG01    SPOKAN01   SPOKAN02 
 SPOKAN03
