

From: IRTHNet  At: 03/13/13 04:03 PM  Seq No: 298

NUNC 
Ticket No: 13056225               2 FULL BUSINESS 
Send To: QLNWA06    Seq No:   49  Map Ref:  

Transmit      Date:  3/13/13   Time: 12:24 pm    Op: ormary 
Original Call Date:  3/13/13   Time: 11:42 am    Op: webusr 
Work to Begin Date:  3/16/13   Time: 12:00 am 

State: WA            County: CLARK                   Place: VANCOUVER 
Address: 9401        Street: NE 144TH CT 
Nearest Intersecting Street: NE 94TH ST 

Twp: 2N    Rng: 2E    Sect-Qtr: 2-NW-NE 
Twp:       Rng:       Sect-Qtr:  
Legal Given:  
Nad:       Lat:            Lon:               Zone:  
ExCoord NW Lat: 45.6909308 Lon:-122.5243298 SE Lat: 45.6901563 Lon:-122.5227377 

Type of Work: INSTALL CATV SERVICE 
Location of Work: ADDRESS IS ON THE WEST SIDE OF NE 144TH CT. MARK FROM THE
: FLUSH MOUNT, NORTH OF DRIVEWAY, FOLLOW WHITE LINE MARKING TO THE SOUTHEAST
: CORNER SIDE OF THE HOUSE AT THE ABOVE LOCATION. 

Remarks: CALLER GAVE THOMAS GUIDE PAGE 508 C3 
:  

Company     : FISK COMMUNICATIONS 
Contact Name: JOHN HALL                        Phone: (360)772-3815 
Cont. Email : JOHN@DROPBURY.COM 
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: COMCAST 
Additional Members:  
CMCST03    CPS01      CPU01      NWN02      VAN01      VAN02
