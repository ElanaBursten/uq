
ATTDSOUTH 00621A USAS 10/05/12 14:01:52 A22790766-00A NORM NEW GRID

Ticket : A22790766  Date: 10/05/12 Time: 14:00 Oper: NHR Chan: 100
Old Tkt: A22790766  Date: 10/05/12 Time: 14:01 Oper: NHR Revision: 00A

Company: C/OF SAN DIEGO PUBLIC WORKS    Caller: JOHN SCAMPONE
Co Addr: 2781 CAMINITO CHOLLAS
City&St: SAN DIEGO, CA                  Zip: 92105      Fax: 619-527-3499
Phone: 619-527-3453 Ext:      Call back: ANYTIME
Formn: JOHN                 Phone: 619-838-5525

State: CA County: SAN DIEGO       Place: OTAY MESA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 5431        Street:VISTA SAN BENITO
X/ST 1 : VISTA SAN RAFAEL
MPM 1:             MPM 2:
Locat:

Excav Enters Into St/Sidewalk: Y

Grids: 1331A072
Lat/Long  : 32.582190/-117.013720 32.582860/-117.012964
          : 32.581263/-117.012899 32.581933/-117.012143
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : REMOVE AND REPLACE ASPHALT
Wkend: N  Night: N
Work date: 10/11/12 Time: 07:00 Hrs notc: 136 Work hrs: 064 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : C/OF SAN DIEGO PUBLIC WORKS

Tkt Exp: 11/02/12

Mbrs : ATTDSOUTH     CHU01  COX01  SDG01  SDG09  SND01


At&t Ticket Id: 25656371   clli code: OTMSCA11   OCC Tkt No: A22790766-00A
ATTDSOUTH: CONDUIT