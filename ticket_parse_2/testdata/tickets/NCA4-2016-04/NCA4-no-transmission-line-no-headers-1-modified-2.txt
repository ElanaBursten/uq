Message Number: X612000440 Rev: 00X Received by USAN at 15:46 on 04/29/16

Work Begins:    05/03/16 at 16:00   Notice: 020 hrs      Priority: 1
Night Work: N   Weekend Work: N

Expires: 05/27/16 at 23:59   Update By:    05/25/16 at 00:00

Caller:         MICHELLE JORDON
Company:        MG PAVING
Address:        4585 N HAYS AVE
City:           FRESNO                        State: CA Zip: 93778
Business Tel:   559-275-2575                  Fax:
Email Address:  MICHELLEMGPAVING@SBCGLOBAL.NET

Nature of Work: EXCAVATE AND REPLACE ASPHALT
Done for:       31221703                      Explosives: N
Foreman:        MIKE
Cell Tel:       559-246-3961
Area Premarked: Y   Premark Method: WHITE PAINT
Permit Type:    CITY                          Number: 2016-192
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location:
Street Address: 2236 N BLACKSTONE AVE
  Cross Street: E VASSAR AVE

IN FRONT OF PROPERTY ON E/S/O OF N BLACKSTONE

Place: FRESNO                       County: FRESNO               State: CA

Long/Lat Long: -119.789724 Lat:  36.770541 Long: -119.791176 Lat:  36.771783

Comments:
#1 TEST EMER

Sent to:
SCETUL = TEST1                      

