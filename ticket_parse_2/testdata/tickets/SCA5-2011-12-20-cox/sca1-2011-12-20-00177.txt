

COXRSM 00003A USAS 12/20/11 07:46:38 A13540064-00A NORM NEW GRID

Ticket : A13540064  Date: 12/20/11 Time: 07:38 Oper: CLA Chan: 100
Old Tkt: A13540064  Date: 12/20/11 Time: 07:45 Oper: CLA Revision: 00A

Company: ARIZONA PIPELINE CO            Caller: MIKE DUNN
Co Addr: 1955 SAMPSON ST
City&St: CORONA, CA                     Zip: 92879      Fax: 951-270-3101
Phone: 951-270-3100 Ext:      Call back: 6AM-5PM
Formn: MIKE DUNN            Phone: 909-208-3908 Ext: CELL

State: CA County: ORANGE          Place: IRVINE
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:JAMBOREE RD
X/ST 1 : 5
MPM 1:             MPM 2:
Locat: S/E/SIDE JAMBOREE RD APPROXIMATELY 60FT S/W/OF S/BOUND I-5 OVERCROSSING,
     : BEHIND K-RAIL, MARK A 20X20 AREA OF DIRT BEHIND CURB,
     :    PARKING IS ON N/SIDE OF I-5 SOUTH ONRAMP

Excav Enters Into St/Sidewalk: N

Grids: 0830E064
Lat/Long  : 33.720035/-117.795578 33.719601/-117.794801
          : 33.719327/-117.795974 33.718892/-117.795197
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : TRENCH INSTALL ELECT CONDUIT & BOX
Wkend: N  Night: N
Work date: 12/22/11 Time: 08:00 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : SC EDISON

Tkt Exp: 01/17/12

Mbrs : ATTD124OR     COTTRFSIG     COXRSM IRVTS  LVL3CM QWESTCA       SCG2XQ
TCAFO  TWCCORG       USCE02 USCE04
