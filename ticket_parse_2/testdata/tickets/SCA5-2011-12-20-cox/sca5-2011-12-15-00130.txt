

COX01  00087A USAS 12/15/11 14:19:07 A13490783-00A RUSH NEW GRID

Ticket : A13490783  Date: 12/15/11 Time: 14:14 Oper: CLA Chan: 100
Old Tkt: A13490783  Date: 12/15/11 Time: 14:19 Oper: CLA Revision: 00A

Company: RESCUE ROOTER                  Caller: SCOTT PIERCE
Co Addr: 9895 OLSON DR
City&St: SAN DIEGO, CA                  Zip: 92020      Fax: 858-457-6541
Phone: 800-869-6972 Ext:      Call back: ANYTIME
Formn: SCOTT PIERCE         Phone: 858-518-3132
Email: SPIERCE@ARS.COM

State: CA County: SAN DIEGO       Place: LA MESA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 6945        Street:WYOMING AVE
X/ST 1 : OREGON AVE
MPM 1:             MPM 2:
Locat: CREW ON SITE

Excav Enters Into St/Sidewalk: N

Grids: 1270E012     1270F011
Lat/Long  : 32.779652/-117.046852 32.779829/-117.045697
          : 32.778422/-117.046663 32.778600/-117.045508
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : EMERGENCY SEWER LINE REPAIR
Wkend: N  Night: N
Work date: 12/15/11 Time: 14:15 Hrs notc: 000 Work hrs: 000 Priority: 0
Instruct : NOW                            Permit: PENDING
Done for : H/O-MR WELCH

Tkt Exp: 01/12/12

Mbrs : ATTD28SD      COX01  HWD01  LAM01  SDC07  SDG01  SND01  UATLSD
