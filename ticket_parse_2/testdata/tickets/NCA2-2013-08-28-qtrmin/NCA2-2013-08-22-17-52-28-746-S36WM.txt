
PACBEL 01029 USAN 08/22/13 10:17:27 0206714 NORMAL NOTICE EXTENSION

Message Number: 0206714 Received by USAN at 10:17 on 08/22/13 by VLERMA

Work Begins:    06/06/13 at 08:00   Notice: 030 hrs      Priority: 2
Night Work: N    Weekend Work: Y

Expires: 09/23/13 at 23:59   Update By: 09/19/13 at 16:59

Caller:         EDWARD MEDELLIN          
Company:        BRAUN ELECTRIC                     
Address:        3000 E. BELLE TER                       
City:           BAKERSFIELD                   State: CA Zip: 93307
Business Tel:   661-978-1170                  Fax:                             
Email Address:  ENJQ@CHEVRON.COM                                            

Nature of Work: HAND DIG TO INST SWITCH STAND           
Done for:       CHEVRON                       Explosives: N
Foreman:        RICK OBEIRNE             
Field Tel:                                    Cell Tel: 661-978-1170           
Area Premarked: Y   Premark Method: STAKES                                     
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    AT A POINT 2.7MI N AND 0.1MI W/O INT CHINA GRADE LOOP AND MANOR ST
    (SEC 25, WELL#122, INCL 300' RAD)

Place: BAKERSFIELD, CO AREA         County: KERN                 State: CA

Long/Lat Long: -119.018986 Lat:  35.46257  Long: -119.010928 Lat:  35.469162 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 07/29/2013 RE-MARK NO ORIG DATE 06/03/2013-KAR 06/27/2013
PER MICHAEL
#2 EXTEND TO 08/26/2013 RE-MARK NO ORIG DATE 06/03/2013-JWH 07/25/2013
PER MICHAEL
#3 EXTEND TO 09/23/2013 RE-MARK NO ORIG DATE 06/03/2013-VLERMA
08/22/2013 PER MICHAEL

Sent to:
PACBEL = PACIFIC BELL                 PGEBFD = PGE DISTR BAKERSFIELD        
SCGBFD = SO CAL GAS BAKERSFIELD       





At&t Ticket Id: 26470180   clli code: OLDLCA11   OCC Tkt No: 0206714-003