
PACBEL 01027 USAN 08/22/13 10:16:46 0288777 NORMAL NOTICE EXTENSION

Message Number: 0288777 Received by USAN at 10:16 on 08/22/13 by VLERMA

Work Begins:    07/31/13 at 13:45   Notice: 020 hrs      Priority: 2
Night Work: N    Weekend Work: Y

Expires: 09/23/13 at 23:59   Update By: 09/19/13 at 16:59

Caller:         MICHAEL MAGEE            
Company:        BRAUN ELECTRIC                     
Address:        3000 E. BELLE TER                       
City:           BAKERSFIELD                   State: CA Zip: 93307
Business Tel:   661-448-7146                  Fax:                             
Email Address:  NYRU@CHEVRON.COM                                            

Nature of Work: AUGER TO INST POLE,ANC & DR GROUND ROD  
Done for:       CHEVRON                       Explosives: N
Foreman:        JASON                    
Field Tel:                                    Cell Tel: 661-448-7146           
Area Premarked: Y   Premark Method: STAKES                                     
Permit Type:    NO                            
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location: 

    AT A POINT 3.1MI N AND 0.8MI E/O INT CHINA GRADE LOOP AND MANOR ST
    300' RAD AT SEC 19RAM, WELL#48R

Place: BAKERSFIELD, CO AREA         County: KERN                 State: CA

Long/Lat Long: -119.003961 Lat:  35.467994 Long: -118.994332 Lat:  35.475871 

Excavator Requests Operator(s) To Re-mark Their Facilities: N
Comments:
#1 EXTEND TO 09/23/2013 RE-MARK NO ORIG DATE 07/29/2013-VLERMA
08/22/2013

Sent to:
CHEVSJ = CHEVRON PIPE-L SAN JOAQU     PACBEL = PACIFIC BELL                 
PGEBFD = PGE DISTR BAKERSFIELD        P66TFT = PHILLIPS 66 -TOSCO           
SCGBFD = SO CAL GAS BAKERSFIELD       





At&t Ticket Id: 26470177   clli code: OLDLCA11   OCC Tkt No: 0288777-001