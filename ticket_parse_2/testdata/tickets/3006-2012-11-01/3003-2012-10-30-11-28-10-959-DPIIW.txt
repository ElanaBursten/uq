
------=_Part_11473_19937479.1351262038191
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 184 Transmitted: Fri Oct 26 09:33:58 CDT 2012

BGAWS  00192 GAUPC 10/26/12 10:30:22 10262-323-007-000 NORMAL
Underground Notification             
Notice : 10262-323-007 Date: 10/26/12  Time: 10:30  Revision: 000 

State : GA County: CARROLL       Place: WHITESBURG                              
Addr  : From: 301    To:        Name:    BANNING                        RD      
Near  : Name:    CLARA                          DR  

Subdivision:                                         
Locate: LOCATE THE FRONT RIGHT OF WAY OF 301 BANNING RD.                       

Grids       : 3331C8455C 3331D8455B 3331D8455C 3331C8455B 
Work type   : INSTALLING PHONE MAIN                                               

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 3 WEEKS    Priority: 3
Done for  : ATT ANSCO                               
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks : RUDESEAL @ 770-231-1857 JOB # BANNING                               

Company : ANSCO AND ASSOCIATES                      Type: CONT                
Co addr : 29 SHORTER INDUSTRIAL BLVD NW            
City    : ROME                            State   : GA Zip: 30165              
Caller  : JENNIE MEADOR                   Phone   :  706-232-5665              
Fax     :                                 Alt. Ph.:                            
Email   : JMEADOR@ANSCOINC.COM                                                
Contact : BILLY RUDESEAL                  770-231-1857              

Submitted date: 10/26/12  Time: 10:30  Oper: 166
Mbrs : AGL106 BGAWS CCW01 COMNOR CRL70 GAUPC PPL02 WHT50

-------------------------------------------------------------------------------



------=_Part_11473_19937479.1351262038191--