
------=_Part_11712_18671505.1351262143880
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 186 Transmitted: Fri Oct 26 09:35:43 CDT 2012

BGAWS  00196 GAUPC 10/26/12 10:32:42 10262-300-245-000 NORMAL
Underground Notification             
Notice : 10262-300-245 Date: 10/26/12  Time: 10:32  Revision: 000 

State : GA County: FLOYD         Place: ROME                                    
Addr  : From: 12     To:        Name:    SHORTER                        AVE  NW 
Near  : Name: N  2ND                            AVE 

Subdivision:                                         
Locate: LOCATE 10' RADIUS AROUND POLE FROM SIDEWALK TO SIDEWALK IN DIRT AREA.  

Grids       : 3415B8511D 
Work type   : INSTALL ANCHORS                                                     

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 2 HOURS    Priority: 3
Done for  : GEORGIA POWER CO.                       
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : NO GPC                                                              
        : *** DrivingInstructions : POLE @ ROAD & SIDEWALK MARKED W/WHITE RIBBON & NUMBERED L135 AT NEW 
        : BP/DUNKIN DOUGHNUTS BLDG. UNDER CONSTRUCTION ACROSS FROM AMERICAN LE
        : GION. GPC REPLACING DOWN GUY, REMOVED FOR CONSTRUCTION WITH A SIDEWA
        : LK DOWN GUY & ANCHOR. THIS POLE IS NEXT TO CIGARETTE SIGN.          

Company : GEORGIA POWER COMPANY                     Type: MEMB                
Co addr : 224 HOLMES ROAD                          
City    : ROME                            State   : GA Zip: 30161              
Caller  : JOSEPH FOPPIANO                 Phone   :  706-236-1472              
Fax     :  706-236-1498                   Alt. Ph.:                            
Email   : JEFOPPIA@SOUTHERNCO.COM                                             
Contact :                                                           

Submitted date: 10/26/12  Time: 10:32  Oper: 207
Mbrs : AGL122 BGAWS CCAST1 GAUPC GP600 PARKF ROM50 ROM51 AVFN

-------------------------------------------------------------------------------



------=_Part_11712_18671505.1351262143880--