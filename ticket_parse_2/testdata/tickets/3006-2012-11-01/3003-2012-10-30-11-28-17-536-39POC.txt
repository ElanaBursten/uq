
------=_Part_12096_7591926.1351262432410
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 192 Transmitted: Fri Oct 26 09:40:32 CDT 2012

BGAWS  00187 GAUPC 10/26/12 10:25:55 10262-320-011-000 NORMAL
Underground Notification             
Notice : 10262-320-011 Date: 10/26/12  Time: 10:25  Revision: 000 

State : GA County: COLQUITT      Place: MOULTRIE                                
Addr  : From:        To:        Name:    9TH                            AVE  NW 
Near  : Name: N  MAIN                           AVE 

Subdivision:                                         
Locate: FROM THE INTERSECTION GO WEST FOR 300FT LOCATE A 10FT RADIUS AROUND THE
      :  WHITE FLAG                                                             

Grids       : 3111C8347C 
Work type   : INSTALLING SIGNS                                                    

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : SCRUGGS                                 
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : MIDDLE GEORGIA SIGNS                      Type: CONT                
Co addr : 246 N 2ND ST                             
City    : COCHRAN                         State   : GA Zip: 31014              
Caller  : JESSICA DANIELS                 Phone   :  478-934-1094              
Fax     :                                 Alt. Ph.:                            
Email   : JNORIS@MIDDLEGEORGIA.COM                                            
Contact :                                                           

Submitted date: 10/26/12  Time: 10:25  Oper: 229
Mbrs : BGAWS CNS04 CNS04F GAUPC MOU90 MOU91 MOU92 MOU93 TCI11 WSMOUL

-------------------------------------------------------------------------------



------=_Part_12096_7591926.1351262432410--