
------=_Part_12069_19476472.1351262402436
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

ATTGA Seq: 190 Transmitted: Fri Oct 26 09:40:02 CDT 2012

BGAWS  00179 GAUPC 10/26/12 10:19:47 10262-235-006-000 NORMAL
Underground Notification             
Notice : 10262-235-006 Date: 10/26/12  Time: 10:19  Revision: 000 

State : GA County: SPALDING      Place: GRIFFIN                                 
Addr  : From: 134    To:        Name:    MEEKS                          ST      
Near  : Name:    TEAMON                         CIR 

Subdivision:                                         
Locate: LOC THE R/O/W FOR THE ENTIRE CUL DE SAC ON BOTH SIDES OF THE RD.       

Grids       : 3319A8414D 3319A8413A 3319A8413B 
Work type   : CLEANING DITCHES                                                    

Start date: 10/31/12 Time: 07:00 Hrs notc : 000
Legal day : 10/31/12 Time: 07:00 Good thru: 11/16/12 Restake by: 11/13/12
RespondBy : 10/30/12 Time: 23:59 Duration : 3 DYS      Priority: 3
Done for  : SPALDING CO                             
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks : AREA IS WHITE LINED.                                                

Company : SPALDING COUNTY PUBLIC WORKS              Type: CONT                
Co addr : 1515-A  WILLIAMSON RD                    
City    : GRIFFIN                         State   : GA Zip: 30224              
Caller  : DEANA PLYMEL                    Phone   :  770-467-4774              
Fax     :                                 Alt. Ph.:  770-467-4774              
Email   : DPLYMEL@SPALDINGCOUNTY.COM                                          
Contact :                                                           

Submitted date: 10/26/12  Time: 10:19  Oper: 198
Mbrs : AGL113 BGAWS CNT70 COMPER GAUPC GRI50 GRI51 GRI52

-------------------------------------------------------------------------------



------=_Part_12069_19476472.1351262402436--