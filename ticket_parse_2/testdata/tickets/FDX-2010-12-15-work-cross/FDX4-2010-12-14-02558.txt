

DIG-TESS Locate Request For AMP
----------------------------------------------------------------------------
Ticket Number:    103483021         Old Ticket:                         
Priority:         Normal            By:               RC_Carolyn Allen  
Source:           Remote            Hours Notice:     48                
Type:             Normal            Date:             12/14/2010 11:39:36 
Sequence:         825                                                   
Map Reference:                                                          

Company Information
----------------------------------------------------------------------------
VERIZON                             Type:             Contractor        
7979 BELT LINE RD                   Contact:          CAROLYN           
IRVING, TX 75063                    Caller:           CAROLYN           
Phone:            (866) 977-6053    Caller Phone:     (800) 609-1603    
Fax:                                Callback:         0800 - 1600       
Alt Contact:                                                            
Caller Email:     nwsadmin@verizon.com                                  

Work Information
----------------------------------------------------------------------------
State:            TX                Work Date:        12/16/10 at 1145  
County:           COLLIN            Type:             EXCAVATION        
Place:            PLANO             Done For:         VERIZON           
Street:           3609 SALFORD DR                                       
Intersection:                                                           
Nature of Work:   FIBER DROP                                            
Explosives:       No                Deeper Than 16":  No                
White Lined:      No                Duration:         1 DAY             
Mapsco:           MAPSCO 557,L                                          

Remarks
----------------------------------------------------------------------------
JOB I.D. E3093130                                                           
                                                                            
LOCATE ALL PROPERTIES FROM 3609 SALFORD DR                                  
LOCATE ENTIRE PROPERTY WITHIN 48 HRS.                                       
LOCATE BOTH SIDES OF THE STREET                                             
                                                                            
DIRECTIONAL BORING                                                          
NO LOCATES FOR VERIZON FACILITIES                                           
INSTALLATION OF 8 FT. GROUND ROD AND GROUND WIRE                            
BACK MGN DIRECTIONAL BORING                                                 
FOR PLANO ONLY ?LOCATE BOTH SIDES OF ALLEY?                                 
                                                                            
FOR ALL INQUIRIES PLEASE CALL:                                              
    (800) 609-1605 (OPT. #2)                                                
                                                                            
*EMAIL CLEAR OR CONFLICTS TO NWSADMIN@VERIZON.COM                           
                                                                            


Members
----------------------------------------------------------------------------
Code  Name                                              Added Manually      
----------------------------------------------------------------------------
1TN   TIME WARNER CABLE                                 No                  
AMP   ATMOS-MIDTX-UTILIQUEST (METROPLEX)                No                  
PIS   PLANO INDEPENDENT SCHOOL DISTRICT                 No                  
PL4   ONCOR ELECTRIC DISTRIBUTION-SMP (PLANO)           No                  
TXNE  TXNE - ATT/D                                      No                  


Location
----------------------------------------------------------------------------
Latitude:         33.0929195947805  Longitude:        -96.7615585271933 
Second Latitude:  33.0925683067241  Second Longitude: -96.7609355763733
