
AGL104  01628 GAUPC 01/26/11 16:32:12 01261-301-388-000 NORMAL RESTAK
Underground Notification             
Notice : 01261-301-388 Date: 01/26/11  Time: 16:31  Revision: 000 
Old Tkt: 01071-301-311 Date: 01/07/11  Time: 16:53     

State : GA County: RICHMOND      Place: AUGUSTA                                 
Addr  : From:        To:        Name:    PEBBLE CREEK                   DR      
Near  : Name:    MORGAN                         RD  

Subdivision:                                         
Locate: STARTING FROM MORGAN RD, LOCATE THE ENTIRE R\O\W ON BOTH SIDES OF THE R
      :  OAD GOING W FOR 200FT                                                  

Grids       : 3323A8205A 3323A8206D 3323B8205A 3323B8206D 
Work type   : HIGHWAY CONSTRUCTION                                                

Start date: 02/01/11 Time: 07:00 Hrs notc : 000
Legal day : 02/01/11 Time: 07:00 Good thru: 02/17/11 Restake by: 02/14/11
RespondBy : 01/31/11 Time: 23:59 Duration : 2 MONTHS   Priority: 3
Done for  : RICHMOND COUNTY                         
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : PREV TKT 01210-300-305  PREVIOUS TICKET NUMBER:02100-257-006  PREVIO
        : US TICKET NUMBER:02260-214-017  PREVIOUS TICKET NUMBER:03160-300-284
        : PREVIOUS TICKET NUMBER:03310-300-327  PREVIOUS TICKET NUMBER:04160-3
        : 00-986  PREVIOUS TICKET NUMBER:05040-300-447  PREVIOUS TICKET NUMBER
        : :05200-300-284  PREVIOUS TICKET NUMBER:06070-301-369  PREVIOUS TICKE
        : T NUMBER:06230-300-785  PREVIOUS TICKET NUMBER:07090-301-194  PREVIO
        : US TICKET NUMBER:08130-300-475  PREVIOUS TICKET NUMBER:08310-301-576
        : PREVIOUS TICKET NUMBER:09160-301-448  PREVIOUS TICKET NUMBER:10040-3
        : 01-570  PREVIOUS TICKET NUMBER:10200-301-633  PREVIOUS TICKET NUMBER
        : :11050-300-880  PREVIOUS TICKET NUMBER:11220-302-359  PREVIOUS TICKE
        : T NUMBER:12080-301-551  PREVIOUS TICKET NUMBER:12220-301-366  PREVIO
        : US TICKET NUMBER:01071-301-311                                      

Company : MABUS BROTHERS CONSTRUCTION               Type: CONT                
Co addr : 920 MOLLY POND RD                        
City    : AUGUSTA                         State   : GA Zip: 30901              
Caller  : KEVIN BALDWIN                   Phone   :  706-722-8941              
Fax     :                                 Alt. Ph.:  706-722-8941              
Email   :                                                                     
Contact : IVAN FRANZ                      706-220-8422              

Submitted date: 01/26/11  Time: 16:31  Oper: 206
Mbrs : AGL104 AUG50 AUG52 BSEG GAUPC GP380 JEF70 JIC01 KNOL2 
-------------------------------------------------------------------------------
AGL104-HP : Schools	MORGAN ROAD MIDDLE SCHOOL
