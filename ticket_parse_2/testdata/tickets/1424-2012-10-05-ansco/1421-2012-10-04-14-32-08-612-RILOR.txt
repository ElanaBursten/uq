
------=_Part_33_17852572.1349375485190
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

BSZT29 361 PUPS Remote 10/04/2012 12:10:00 PM 1210041176 Normal 

Ticket Number: 1210041176
Old Ticket Number: 
Created By: R-RLW
Seq Number: 361

Created Date: 10/04/2012 12:11:17 PM
Work Date/Time: 10/09/2012 11:59:59 PM
Update: 10/25/2012 Good Through: 10/30/2012

Excavation Information:
State: SC     County: PICKENS
Place: CENTRAL
Address Number: 
Street: MAW BRIDGE ROAD
Inters St: WINCHESTER DRIVE
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: ANSCO & ASSOCIATES

Remarks/Instructions: WORK IS FOR ATT // JOB NUMBER SA1203535                 
                                                                              
STARTING AT 2386 MAW BRIDGE ROAD, MARK 200 FEET IN EACH DIRECTION // MARK BOTH
SIDES OF THE ROAD // TERMINAL ADDRESS IS 3867 CLEMSON HWY // DROP WILL BE     
BURIED FROM PED TO ONI // MAIN STREET AND CAMP CREEK ROAD ARE ADDITIONAL ROADS
IN THE AREA                                                                   


Caller Information: 
Name: REBECCA WAMPLER                       ANSCO & ASSOCIATES                    
Address: 43 SENTELL ROAD
City: GREENVILLE State: SC Zip: 29611
Phone: (864) 295-0235 Ext:  Type: Business
Fax:  Caller Email: 

Contact Information:
Contact:REBECCA WAMPLER Email: REBECCALEEWAMPLER@GMAIL.COM
Call Back:8642950235 Fax: 

Grids: 
Lat/Long: 34.7918999789726, -82.8127210869016
Secondary: 34.7903531039717, -82.8107106702338
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ25 FTHZ63                                 


Map Link: (NEEDS DEVELOPMENT)




------=_Part_33_17852572.1349375485190--