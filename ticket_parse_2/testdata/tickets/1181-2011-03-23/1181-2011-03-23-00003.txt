
Miss Utility of West Virginia Locate Request
----------------------------------------------------------------------------
Ticket Number:    103280009         Old Ticket:                         
By:               HAILEY S          Source:           FAX               
Type:             NORMAL            Date:             11/24/2010 7:11:12 AM
Send To:          ANDREW            Sequence:         3                 

Company Information
----------------------------------------------------------------------------
APPALACHIAN POWER CO.               Type:             CONTRACTOR        
1122 SEVENTH AVE                                                        
HUNTINGTON, WV 25701                                                    
Caller:           TERRY ADKINS      Caller Phone:     (304) 696-1203    
Contact:          TERRY ADKINS / NICContact Phone:    (304) 710-9042    
Company Phone:    (304) 696-1203    Company Fax:      (304) 696-1241    
Company Email:    CALLER DID NOT PROVIDE EMAIL                          

Work Information
----------------------------------------------------------------------------
State:            WV                Work Date:        11/29/2010 8:15:22 AM
County:           CABELL            Work Done For:    APPALACHIAN POWER 
Place:            BARBOURSVILLE                                         
Street:           8 CARRIAGE LN                                         
Intersection:     NICHOLS DR                                            
Nature of Work:   NEED PRIMARY LOCATED TO INSTALL SWITCH BOX            
Excavation Depth: 04 FT             Excavation Length:960 FEET          
White Markings:   No                Explosives:       No                

Remarks
----------------------------------------------------------------------------
LOCATE TO BEGIN 0.29MI NE OF THE INTER AND CONT 960FT SE. NEED PRIMARY      
ELECTICITY FACILITES LOCATED ONLY CUSTOMER IS BUILDING HOUSE NEED LOCATED IN
ORDER TO DESIGN JOB TO SERVICE CUSTOMER, FROM MALL RD JUST OFF EXIT 20 OF   
I-64 TURN RIGHT ONTO NICHOLS DR TOWARD WALMART PAST WALMART TURN LEFT INTO  
MELODY FARMS SUBD (GATE CODE IS 3247) NICHOLS DR CONTINUES IN SUBD MARK 1ST 
RIGHT TO CARRIAGE LN GO TO BOTTOM OF HILL KEEP RIGHT HALF WAY DOWN HILL NEAR
STABLES. LOCATE: STREET, FRONT. START GPS N 38.427241 W 82.253402 END GPD N 
38.425862 W 82.250928. AEP NEAR POLE NUMBER 38820078C00016. APCO LOCATE:    
YES.                                                                        


Members
----------------------------------------------------------------------------
Code        Name                                          Phone Number      
----------------------------------------------------------------------------
AW          VERIZON WEST VIRGINIA INC.                                        
CGA         COLUMBIA GAS TRANSMISSION LLC                                     
CH          MOUNTAINEER GAS COMPANY                                           
PC          AMERICAN ELECTRIC POWER COMPANY                                   
SOG         SIMCON OIL & GAS CORP.                                            
SRW         WEST VIRGINIA-AMERICAN WATER COMPANY                              
VBV         VILLAGE OF BARBOURSVILLE                                          
WVH         WEST VIRGINIA AMERICAN WATER COMPANY                              


Location
----------------------------------------------------------------------------
Latitude:         38.4308953649765  Longitude:        -82.2564992960132 
Second Latitude:  38.421821738479   Second Longitude: -82.2474256695156 

Grids
----------------------------------------------------------------------------
382520821480   382520821500   382520821520   382540821460   382540821480
382540821500   382540821520   382540821540   382560821460   382560821480
382560821500   382560821520   382560821540   382580821480   382580821500
382580821520


