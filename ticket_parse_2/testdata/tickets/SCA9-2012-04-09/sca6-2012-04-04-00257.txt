
ATTD28SD 00222A USAS 04/03/12 14:47:59 A20941008-00A NORM NEW GRID

Ticket : A20941008  Date: 04/03/12 Time: 14:42 Oper: CLA Chan: 100
Old Tkt: A20941008  Date: 04/03/12 Time: 14:47 Oper: CLA Revision: 00A

Company: HAZARD CONSTRUCTION            Caller: LANCE GIBSON
Co Addr: 6465 MAR INDUSTRY PL
City&St: SAN DIEGO, CA                  Zip: 92192
Phone: 858-864-6756 Ext:      Call back: ANYTIME
Formn: LANCE GIBSON
Email: LGIBSON@HAZARDCONSTRUCTION.COM

State: CA County: SAN DIEGO       Place: VISTA
Delineated: Y
Delineated Method: WHITEPAINT
Address:             Street:W VISTA WAY
X/ST 1 : HUFF ST
MPM 1:             MPM 2:
Locat: E/BOUND SHOULDER W VISTA WAY FRM HUFF ST S/W/FOR 300FT

Excav Enters Into St/Sidewalk: N

Grids: 1107D0113
Lat/Long  : 33.189642/-117.278554 33.190040/-117.276689
          : 33.188884/-117.278393 33.189282/-117.276527
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : WALL FOOTING EXCAVATION
Wkend: N  Night: N
Work date: 04/06/12 Time: 07:00 Hrs notc: 064 Work hrs: 064 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : CALTRANS

Tkt Exp: 05/01/12

Mbrs : ATTD28SD      COX04  FREESD OCE01  OCE03  REDFLEXTRF    SDG01  SDG02
VID01  VIS01

At&t Ticket Id: 25146086   clli code: VISTCA12