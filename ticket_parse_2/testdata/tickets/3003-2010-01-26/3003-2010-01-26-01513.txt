
BSNW  01524 GAUPC 01/26/10 16:00:03 01260-301-244-000 NORMAL
Underground Notification             
Notice : 01260-301-244 Date: 01/26/10  Time: 15:56  Revision: 000 

State : GA County: COBB          Place: MARIETTA                                
Addr  : From: 2300   To:        Name:    HIGHLAND PARC                  PL   SE 
Near  : Name:    POWERS FERRY                   PL  

Subdivision: JASMINE @ MARIETTA CROSSING APTS        
Locate: LOCATE THE EXISTING AT&T XBOX WITH A 150FT RADIUS                      

Grids       : 3355B8428A 3355B8429D 
Work type   : INSTALLING NEW CABINET AND DUCT                                     

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 DAYS     Priority: 3
Done for  : ANSCO BROADBAND                         
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : JOB# 0FW52513N                                                      
        : *** DrivingInstructions : TURN INTO APT COMPLEX FROM POWERS FERRY PL, XBOX IS NEXT TO BUILDING
        : 2200                                                                

Company : ANSCO & ASSOCIATES                        Type: CONT                
Co addr : 1841A WEST OAK PARKWAY                   
City    : MARIETTA                        State   : GA Zip: 30062              
Caller  : STEPHEN SNOW                    Phone   :  770-218-0369              
Fax     :                                 Alt. Ph.:                            
Email   : SSNOW@ANSCOINC.COM                                                  
Contact :                                                           

Submitted date: 01/26/10  Time: 15:56  Oper: 213
Mbrs : AGL119 AGLN01 BSNW COB70 COMNOR GAUPC GP171 MBL90 MBL92 MBL93 
     : MTS50 
-------------------------------------------------------------------------------


