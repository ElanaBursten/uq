
NDPTST 00935 USANW 03/30/16 19:29:28 W607790001-02W RUSH FLUP GRID LREQ RSEN

Message Number: W607790001 Rev: 02W Received by USAN at 11:24 on 03/17/16

Work Begins:    03/21/16 at 11:45   Notice: 048 hrs      Priority: 0
Night Work: N   Weekend Work: N

Expires: 04/14/16 at 23:59   Update By:    04/12/16 at 00:00

Caller:         CHRIS BRASSART
Company:        USA NORTH 811
Address:        4005 PORT CHICAGO HWY
City:           CONCORD                       State: CA Zip: 94520
Business Tel:   925-798-9504  Ext: 1234567890 Fax: 925-798-9506
Email Address:  CBRASSART@USAN.ORG

Nature of Work: DIG TO INST PIPE LINE
Done for:       TEST                          Explosives: N
Foreman:        CALLER
Cell Tel:       925-798-9504 Ext: 1234567890
Area Premarked: Y   Premark Method: CHALK,FLAGS,STAKES,WHISKERS,WHITE PAINT
Work Order:     123456789012345
Permit Type:    CITY                          Number: 12345678901234567890
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location:
Street Address: 4352 CHELSEA WAY
  Cross Street: GRAMMERCY LN

WRK LOC FRT/O ADDR

Place: CONCORD                      County: CONTRA COSTA         State: CA

Long/Lat Long: -121.996391 Lat:  37.952704 Long: -121.997160 Lat:  37.953299

Comments:
TESTING
#1 DAMAGED/EXPOSED INFORMATION:  EXPOSED LINE
#2 FOLLOW-UP MESSAGE:   TEST MESSAGE

Sent to:
ASTBRO = ASTOUND BROADBAND            CCOWTR = CONTRA COSTA WATER
COMCND = COMCAST CONCORD              CTYCND = CITY CONCORD
NDPTST = NDP - TEST ONLY              PACBEL = PACIFIC BELL
PGECND = PGE DISTR CONCORD

[Ticket (re)sent at your request]
