
SCERID 00938 USANW 03/30/16 19:29:29 W607790001-05W SHRT EXTN GRID RMRK RSEN

Message Number: W607790001 Rev: 05W Received by USAN at 11:26 on 03/17/16

Work Begins:    03/21/16 at 11:45   Notice: 048 hrs      Priority: 1
Night Work: N   Weekend Work: N

Expires: 06/09/16 at 23:59   Update By:    06/07/16 at 00:00

Caller:         CHRIS BRASSART
Company:        USA NORTH 811
Address:        4005 PORT CHICAGO HWY
City:           CONCORD                       State: CA Zip: 94520
Business Tel:   925-798-9504  Ext: 1234567890 Fax: 925-798-9506
Email Address:  CBRASSART@USAN.ORG

Nature of Work: DIG TO INST PIPE LINE
Done for:       TEST                          Explosives: N
Foreman:        CALLER
Cell Tel:       925-798-9504 Ext: 1234567890
Area Premarked: Y   Premark Method: CHALK,FLAGS,STAKES,WHISKERS,WHITE PAINT
Work Order:     123456789012345
Permit Type:    CITY                          Number: 12345678901234567890
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location:
Street Address: 4352 CHELSEA WAY
  Cross Street: GRAMMERCY LN

WRK LOC FRT/O ADDR

Place: CONCORD                      County: CONTRA COSTA         State: CA

Long/Lat Long: -121.996391 Lat:  37.952704 Long: -121.997160 Lat:  37.953299

Excavator Requests Operator(s) To Re-mark Their Facilities: 1
Operator(s) To Re-mark Their Facilities:
ASTBRO = ASTOUND BROADBAND            CCOWTR = CONTRA COSTA WATER
PGECND = PGE DISTR CONCORD

Comments:
TESTING - TEST MEET FOR TICKET TYPE - TEST EMER FOR KIND
#1 DAMAGED/EXPOSED INFORMATION:  EXPOSED LINE
#2 FOLLOW-UP MESSAGE:   TEST MESSAGE
#3 FOLLOW-UP MESSAGE:   RE-MARK YES ORIG DATE 03/17/2016-TEST 03/17/2016 ASTOUND
BROADBAND, COMCAST CONCORD, NDP - TEST ONLY RE-MARKING TO BE COMPLETED BY
03/21/2016 08:45 AM
#4 EXTEND TO 05/12/2016 RE-MARK NO ORIG DATE 03/17/2016-TEST 03/17/2016 PER
CHRIS
#5 EXTEND TO 06/09/2016 RE-MARK YES ORIG DATE 03/17/2016-TEST 03/17/2016 COMCAST
CONCORD, CITY CONCORD, NDP - TEST ONLY, PACIFIC BELL RE-MARKING TO BE COMPLETED
BY 03/21/2016 08:45 AM CHRIS BRASSART

Sent to:
ASTBRO = ASTOUND BROADBAND            CCOWTR = CONTRA COSTA WATER
COMCND = COMCAST CONCORD              CTYCND = CITY CONCORD
NDPTST = NDP - TEST ONLY              PACBEL = PACIFIC BELL
SCETUL = TEST1                        SCEVAL = TEST2
PGECND = PGE DISTR CONCORD

[Ticket (re)sent at your request]
