
NDPTST 00951 USANW 03/30/16 19:29:33 W607890090-01W DAMG FLUP GRID RMRK RSEN

Message Number: W607890090 Rev: 01W Received by USAN at 18:19 on 03/18/16

Work Begins:    03/22/16 at 18:30   Notice: 020 hrs      Priority: 9
Night Work: N   Weekend Work: N

Expires: 04/15/16 at 23:59   Update By:    04/13/16 at 00:00

Caller:         LEAH KEYES
Company:        USA NORTH
Address:        4005 PORT CHICAGO HWY
City:           CONCORD                       State: CA Zip: 94520
Business Tel:   925-798-9504                  Fax: 925-798-0927
Email Address:  LKEYES@USAN.ORG

Nature of Work: DIG TO REP A WTR L
Done for:       PG&E                          Explosives: N
Foreman:        RICK SHAW
Cell Tel:       925-798-9504
Area Premarked: Y   Premark Method: WHISKERS,WHITE PAINT
Permit Type:    NO
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: N

Location:
Street Address: 1200 J ST
  Cross Street: 9TH ST

WRK FRT/O PROP

Place: SUNNYVALE                    County: SANTA CLARA          State: CA

Long/Lat Long: -122.003168 Lat:  37.404894 Long: -122.004571 Lat:  37.407634

Excavator Requests Operator(s) To Re-mark Their Facilities: 1
Operator(s) To Re-mark Their Facilities:
CTYSJO = CITY SAN JOSE                CTYSUN = CITY SUNNYVALE
LEVCAL = LEVEL 3 COMM - CALIF         MCIWSA = MCI WORLDCOM
NDPTST = NDP - TEST ONLY              PACBEL = PACIFIC BELL
PAXINC = PAXIO INC.                   PGECUP = PGE DISTR CUPERTINO
TCOSFO = TELEPORT COMM SFO            VLYTRN = VALLEY TRANSPORTATION AUTHOR
XOCOMM = XO COMM SVCS DBA XO COMM     ZAYOCA = ZAYO - CA

Comments:
DAMAGED/EXPOSED INFORMATION:  CUST DAMAGED AN UNMARKED FACILITY, PLS RESPOND
#1 FOLLOW-UP MESSAGE:   RE-MARK YES ORIG DATE 03/18/2016-TEST 03/18/2016
COMCAST-SAN JOSE, CITY SANTA CLARA RE-MARKING TO BE COMPLETED BY 03/22/2016
03:45 PM

Sent to:
COMSJO = COMCAST-SAN JOSE             CTYSCL = CITY SANTA CLARA
CTYSJO = CITY SAN JOSE                CTYSUN = CITY SUNNYVALE
LEVCAL = LEVEL 3 COMM - CALIF         MCIWSA = MCI WORLDCOM
NDPTST = NDP - TEST ONLY              PACBEL = PACIFIC BELL
PAXINC = PAXIO INC.                   PGECUP = PGE DISTR CUPERTINO
TCOSFO = TELEPORT COMM SFO            VLYTRN = VALLEY TRANSPORTATION AUTHOR
XOCOMM = XO COMM SVCS DBA XO COMM     ZAYOCA = ZAYO - CA

[Ticket (re)sent at your request]
