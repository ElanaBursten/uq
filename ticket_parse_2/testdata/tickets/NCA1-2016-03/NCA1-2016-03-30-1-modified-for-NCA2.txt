
XXXTST 00944 USANW 03/30/16 19:29:31 W607790008-00W SHRT NEW GRID LREQ RSEN

Message Number: W607790008 Rev: 00W Received by USAN at 11:35 on 03/17/16

Work Begins:    03/21/16 at 12:00   Notice: 048 hrs      Priority: 1
Night Work: N   Weekend Work: N

Expires: 04/14/16 at 23:59   Update By:    04/12/16 at 00:00

Field Meet Requested - Please Call and Confirm
Call Requested

Caller:         CHRIS BRASSART
Company:        USA NORTH 811
Address:        4005 PORT CHICAGO HWY
City:           CONCORD                       State: CA Zip: 94520
Business Tel:   925-798-9504                  Fax: 925-798-9506
Email Address:  CBRASSART@USAN.ORG

Nature of Work: TEST
Done for:       TEST                          Explosives: N
Foreman:        UNKNOWN
Cell Tel:
Area Premarked: Y   Premark Method: WHITE PAINT
Permit Type:    NO
Vac / Pwr Equip Use In The Approx Location Of Member Facilities Requested: N
Excavation Enters Into Street Or Sidewalk Area: Y

Location:
Street Address: BATES AVE
  Cross Street: NELSON AVE                                            Corner: SW

SW/COR/O BATES AVE & NELSON AVE

Place: CONCORD                      County: CONTRA COSTA         State: CA

Long/Lat Long: -122.043407 Lat:  38.010791 Long: -122.044793 Lat:  38.011889

Sent to:
ASTBRO = ASTOUND BROADBAND            CCOWTR = CONTRA COSTA WATER
CENTRL = CENTRAL SANITARY             COMCND = COMCAST CONCORD
CTYCND = CITY CONCORD                 NDPTST = NDP - TEST ONLY
PACBEL = PACIFIC BELL                 PGECND = PGE DISTR CONCORD

[Ticket (re)sent at your request]


At&t Ticket Id: 25652606   clli code: SNJSCA21   OCC Tkt No: W607790008-00W
PACBEL: CONDUIT,FIBER,UNDERGROUND,FTTN,FIBER LANDMARK

