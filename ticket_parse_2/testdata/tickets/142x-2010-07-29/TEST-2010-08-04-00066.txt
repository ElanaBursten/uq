
ZZQ35 36 PUPS U.S. Mail 08/04/2010 09:51:00 AM 2208040103 Meet 

Ticket Number: 2208040103
Old Ticket Number: 
Created By: JAC
Seq Number: 36

Created Date: 08/04/2010 09:52:44 AM
Work Date/Time: 08/09/2010 10:00:28 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: ANDERSON
Place: CENTERVILLE
Address Number: 902
Street: PARKWOOD DR
Inters St: PARKWOOD CT
Subd: 

Type of Work: FOUNDATION, REPAIR
Duration: APPROX 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.5363662598484, -82.7238246619353
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ04 ECA47 PNAZ80 WAW10                     


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


