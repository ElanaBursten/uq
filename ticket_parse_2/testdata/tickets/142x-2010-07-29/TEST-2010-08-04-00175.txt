
ZZQ06 107 PUPS Voice 08/04/2010 02:33:00 PM 2208040289 Normal 

Ticket Number: 2208040289
Old Ticket Number: 
Created By: JDB
Seq Number: 107

Created Date: 08/04/2010 02:33:30 PM
Work Date/Time: 08/09/2010 02:45:13 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 204
Street: RAINSBOROUGH WAY
Inters St: CLEMSON RD 
Subd: 

Type of Work: FENCE, INSTALL
Duration: 3 DAY 

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1377653756023, -80.8926593094074
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ82 SCGZ02 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


