
ZZQ35 63 PUPS Web 08/04/2010 10:34:00 AM 2208040196 Emergency 

Ticket Number: 2208040196
Old Ticket Number: 
Created By: JDB
Seq Number: 63

Created Date: 08/04/2010 10:34:41 AM
Work Date/Time: 08/04/2010 10:45:17 AM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: CHEROKEE
Place: GAFFNEY
Address Number: 176
Street: BOILING SPRINGS HWY
Inters St: I 85
Subd: 

Type of Work: SEE REMARKS
Duration: APPROX 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.1012039484308, -81.6573961310227
Secondary: 35.0966019417391, -81.6558710474098
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60 GPW48 PNCZ57 SCTDZ04A                  


Map Link: (NEEDS DEVELOPMENT)


