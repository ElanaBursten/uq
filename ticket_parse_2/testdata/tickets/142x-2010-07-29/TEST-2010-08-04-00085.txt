
ZZQ06 64 PUPS Walk-In 08/04/2010 10:11:00 AM 2208040141 Emergency 

Ticket Number: 2208040141
Old Ticket Number: 
Created By: JAC
Seq Number: 64

Created Date: 08/04/2010 10:12:12 AM
Work Date/Time: 08/04/2010 10:15:00 AM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: ORANGEBURG
Place: CORDOVA
Address Number: 
Street: DRY SWAMP RD
Inters St: CARVER SCHOOL RD
Subd: 

Type of Work: STUMP REMOVAL & GRADING
Duration: COUPLE OF DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.3752671026194, -80.9024643615348
Secondary: 33.3709326544563, -80.9009258999954
Lat/Long Caller Supplied: N

Members Involved: BSZC46 ODPU53 TWOZ73                                        


Map Link: (NEEDS DEVELOPMENT)


