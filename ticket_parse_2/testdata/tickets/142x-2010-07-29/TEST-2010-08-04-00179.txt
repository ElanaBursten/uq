
ZZQ35 89 PUPS Voice 08/04/2010 02:35:00 PM 2208040298 No Show 

Ticket Number: 2208040298
Old Ticket Number: 
Created By: JAC
Seq Number: 89

Created Date: 08/04/2010 02:36:38 PM
Work Date/Time: 08/04/2010 02:45:14 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SPARTANBURG
Place: CAMPOBELLO
Address Number: 122
Street: HORTON RD
Inters St: HWY 176
Subd: 

Type of Work: SEE REMARKS
Duration: APPROX 2 WEEK

Boring/Drilling: N Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.1448274785064, -82.1807887876576
Secondary: 35.1283061383192, -82.1681482991922
Lat/Long Caller Supplied: N

Members Involved: DPCZ60 GRR01 SWS58 WIIZ28 WTGC09                            


Map Link: (NEEDS DEVELOPMENT)


