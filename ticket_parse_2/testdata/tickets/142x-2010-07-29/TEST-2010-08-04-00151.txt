
ZZQ35 70 PUPS Voice 08/04/2010 02:19:00 PM 2208040255 Survey 

Ticket Number: 2208040255
Old Ticket Number: 
Created By: JDB
Seq Number: 70

Created Date: 08/04/2010 02:20:03 PM
Work Date/Time: 08/09/2010 02:30:51 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: SPARTANBURG
Place: MOORE
Address Number: 
Street: HOBBYSVILLE RD
Inters St: HWY 221
Subd: 

Type of Work: SEE REMARKS
Duration: APPROX 60 DAYS

Boring/Drilling: Y Blasting: Y White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.831015494904, -81.9899526506089
Secondary: 34.801155963114, -81.9794643562885
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 CGT62 DPCZ60 LAU27 PNSZ82 WRW47               


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


