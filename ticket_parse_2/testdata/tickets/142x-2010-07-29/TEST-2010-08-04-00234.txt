
ZZQ35 117 PUPS Tck Project 08/04/2010 03:43:00 PM 2208040403 Survey 

Ticket Number: 2208040403
Old Ticket Number: 
Created By: JAC
Seq Number: 117

Created Date: 08/04/2010 03:44:57 PM
Work Date/Time: 08/09/2010 03:45:49 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: GREENVILLE
Place: SIMPSONVILLE
Address Number: 67
Street: MCRAE PL
Inters St: MATHIS FERRY CT
Subd: MCRAE PARK

Type of Work: GAS, INSTALL SERVICE
Duration: 1 DAY 

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8221506110808, -82.2414281262217
Secondary: 34.7887861510319, -82.2011859046715
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ02 LAU27 MCI18 PNGZ81 VERZ06              


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


