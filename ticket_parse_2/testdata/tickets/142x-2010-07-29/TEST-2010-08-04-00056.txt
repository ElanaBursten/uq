
ZZQ06 48 PUPS Voice 08/04/2010 09:32:00 AM 2208040074 Normal 

Ticket Number: 2208040074
Old Ticket Number: 
Created By: JDB
Seq Number: 48

Created Date: 08/04/2010 09:33:06 AM
Work Date/Time: 08/09/2010 09:45:56 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: IRMO
Address Number: 
Street: JAMIL RD
Inters St: PINEY GROVE RD
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN LINE
Duration: 2 DAYS

Boring/Drilling: Y Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0681994771466, -81.149545609942
Secondary: 34.0581625271685, -81.1357255018952
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 DPT77 MID55 SCA88 SCEKZ42 SCGT05 SCGZ05 SCTDZ01
STG10 TWCZ40                                                                  


Map Link: (NEEDS DEVELOPMENT)


