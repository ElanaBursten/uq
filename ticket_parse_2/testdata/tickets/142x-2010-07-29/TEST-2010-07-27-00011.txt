
ZZQ06 5 PUPS Email 07/27/2010 08:36:00 1007270006 Update 

Ticket Number: 1007270006
Old Ticket Number: 1007220016
Created By: RMD
Seq Number: 5

Created Date: 07/27/2010 08:36:37
Work Date/Time: 07/30/2010 08:45:36
Update: 08/18/2010 08:45:36 Good Through: 08/23/2010 08:45:36

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 
Street: WINDY HOLLOW DR
Inters St: EAGLEVIEW DR
Subd: MANCHESTER PARK

Type of Work: DRAINAGE WORK
Duration: 1 DAY

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: MCEC &/OR CONTRACTOR

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9358060413837, -81.1963505315545
Secondary: 33.9279215898316, -81.1891310337478
Lat/Long Caller Supplied: N

Members Involved: JMW83 MID55 SCGZ05 TWCZ40 WINZ08                            


Map Link: (NEEDS DEVELOPMENT)


