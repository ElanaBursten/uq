
ZZQ06 57 PUPS Walk-In 07/27/2010 13:32:00 1007270080 Design 

Ticket Number: 1007270080
Old Ticket Number: 
Created By: RMD
Seq Number: 57

Created Date: 07/27/2010 13:33:23
Work Date/Time: 08/10/2010 13:45:45
Update: 08/27/2010 13:45:45 Good Through: 09/01/2010 13:45:45

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 1924
Street: PONTIAC AVE
Inters St: PENNFIELD DR
Subd: DREXEL LAKES

Type of Work: GRADING
Duration: APPROX 1 DAY

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0662842790083, -80.9144715301824
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ82 SCGZ02 TWCZ40 TWTZ26                   


Map Link: (NEEDS DEVELOPMENT)

Design tickets are courtesy tickets.  The utilities will mark the
requested area on a voluntary basis.  Members are to locate within
10 working days, please call requestor if you are unable to locate
within the 10 working days.


