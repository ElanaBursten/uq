
ZZQ06 27 PUPS AH-Emerg 07/27/2010 08:30:00 1007270001 Emergency 

Ticket Number: 1007270001
Old Ticket Number: 
Created By: RMD
Seq Number: 27

Created Date: 07/27/2010 08:32:36
Work Date/Time: 07/27/2010 08:45:00
Update: 08/13/2010 08:45:00 Good Through: 08/18/2010 08:45:00

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 1500
Street: MARION ST
Inters St: TAYLOR ST
Subd: 

Type of Work: WATER, REPLACE METER
Duration: APPROX 5 HOURS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY // IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME //THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1       


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0249832910105, -81.0412873316091
Secondary: 33.9994383908052, -81.0290386728356
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 COC82 ITC73 LC393 SCA88 SCEJZ40 SCEKZ42 SCG02  
SCGZ02 STG10 TWCZ40 TWTZ26 USC42                                              


Map Link: (NEEDS DEVELOPMENT)


