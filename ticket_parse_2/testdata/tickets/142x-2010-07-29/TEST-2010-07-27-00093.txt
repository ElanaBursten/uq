
ZZQ06 76 PUPS AH-Emerg 07/27/2010 14:55:00 1007270113 Update 

Ticket Number: 1007270113
Old Ticket Number: 
Created By: DBL
Seq Number: 76

Created Date: 07/27/2010 14:56:21
Work Date/Time: 07/30/2010 15:00:40
Update: 08/18/2010 15:00:40 Good Through: 08/23/2010 15:00:40

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 
Street: BARTRAM ST
Inters St: BARTRAM WAY
Subd: WELLESLEY 

Type of Work: FENCE, CLEAN ROW
Duration: 15 DAYS

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: PUPS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0106159940134, -81.1802796479352
Secondary: 33.9981078513693, -81.1666704363963
Lat/Long Caller Supplied: N

Members Involved: MID55 PBT43 SCEDZ05 SCEJZ40 SCGZ05 SCTDZ01 TLX24 TWCZ40     
WINZ08                                                                        


Map Link: (NEEDS DEVELOPMENT)


