
ZZQ06 119 PUPS Voice 08/04/2010 03:23:00 PM 2208040369 No Show 

Ticket Number: 2208040369
Old Ticket Number: 
Created By: JDB
Seq Number: 119

Created Date: 08/04/2010 03:24:25 PM
Work Date/Time: 08/04/2010 03:30:54 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: CALHOUN
Place: ST MATTHEWS
Address Number: 
Street: ELLIS AVE
Inters St: SAINTS AVE
Subd: 

Type of Work: FIBER OPTIC, INSTALL CABLE
Duration: 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.685742985808, -80.8129307071372
Secondary: 33.6372182364193, -80.7583121520114
Lat/Long Caller Supplied: N

Members Involved: SCG20 SCGT20 TCE12 TWOZ73 WISZ52                            


Map Link: (NEEDS DEVELOPMENT)


