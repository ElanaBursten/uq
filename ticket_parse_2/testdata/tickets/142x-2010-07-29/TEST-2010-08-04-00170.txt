
ZZQ35 81 PUPS Tck Project 08/04/2010 02:29:00 PM 2208040279 Normal 

Ticket Number: 2208040279
Old Ticket Number: 
Created By: JDB
Seq Number: 81

Created Date: 08/04/2010 02:30:03 PM
Work Date/Time: 08/09/2010 02:30:51 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CHEROKEE
Place: GAFFNEY
Address Number: 
Street: BAILEY BRIDGE ROAD
Inters St: OLD METAL ROAD & BONNER ROAD
Subd: 

Type of Work: BRIDGE, CONSTRUCTION
Duration: 108 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.1183067505374, -81.8062721184355
Secondary: 35.1067221619135, -81.8039189988713
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60                                        


Map Link: (NEEDS DEVELOPMENT)


