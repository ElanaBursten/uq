
ZZQ06 11 PUPS Voice 07/22/2010 10:48:00 1007220018 Emergency 

Ticket Number: 1007220018
Old Ticket Number: 
Created By: DBL
Seq Number: 11

Created Date: 07/22/2010 10:49:34
Work Date/Time: 07/22/2010 11:00:37
Update: 08/10/2010 11:00:37 Good Through: 08/13/2010 11:00:37

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 
Street: GOVERNORS GRANT BLVD
Inters St: POINDEXTER LN
Subd: GOVERNORS GRANT

Type of Work: GRADING & ASPHALT PAVING
Duration: A COUPLE HOURS

Boring/Drilling: N
Blasting: N
White Lined: Y
Near Railroad: N


Work Done By: SCE&G GAS DEPT

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0224830113, -81.2786172838609
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: JMW83 SCEDZ05 SCGZ05 TWCZ40 WINZ08                          


Map Link: (NEEDS DEVELOPMENT)


