
ZZQ06 52 PUPS Voice 08/04/2010 09:41:00 AM 2208040086 Normal 

Ticket Number: 2208040086
Old Ticket Number: 
Created By: JAC
Seq Number: 52

Created Date: 08/04/2010 09:41:59 AM
Work Date/Time: 08/09/2010 09:45:38 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: IRMO
Address Number: 7300
Street: COLLEGE ST
Inters St: LAKE MURRAY BLVD
Subd: 

Type of Work: WATER, REPAIR MAIN
Duration: APPROX:5HRS

Boring/Drilling: N Blasting: N White Lined: Y Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.085101035325, -81.1731568888214
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ42 SCGZ05 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


