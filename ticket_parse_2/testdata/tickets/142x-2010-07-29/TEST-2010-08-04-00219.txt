
ZZQ35 113 PUPS Voice 08/04/2010 03:30:00 PM 2208040380 Update 

Ticket Number: 2208040380
Old Ticket Number: 
Created By: JDB
Seq Number: 113

Created Date: 08/04/2010 03:30:49 PM
Work Date/Time: 08/09/2010 03:30:22 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CHESTER
Place: CHESTER
Address Number: 
Street: J A COCHRAN BYPASS
Inters St: OLD YORK RD
Subd: 

Type of Work: TRAFFIC SIGNALS
Duration: 4 WEEKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7472571370579, -81.2278732013503
Secondary: 34.7079775161817, -81.179949030706
Lat/Long Caller Supplied: N

Members Involved: CGT62 CHG31 CHT11 CHW51 DPCZ06 QWC42                        


Map Link: (NEEDS DEVELOPMENT)


