
ZZQ35 76 PUPS AH-Emerg 08/04/2010 02:26:00 PM 2208040271 Emergency 

Ticket Number: 2208040271
Old Ticket Number: 
Created By: JDB
Seq Number: 76

Created Date: 08/04/2010 02:26:26 PM
Work Date/Time: 08/04/2010 02:30:05 PM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: SPARTANBURG
Place: DUNCAN
Address Number: 112
Street: HIDDEN LAKE CIR
Inters St: SPARTANGREEN BLVD
Subd: 

Type of Work: ELECTRIC, REPAIR SECONDARY
Duration: APPROX 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9164771201837, -82.0996778058436
Secondary: 34.9133612652637, -82.0956733991403
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZT29 CCMZ41 DPCZ08 PNSZ82 SJW43                     


Map Link: (NEEDS DEVELOPMENT)


