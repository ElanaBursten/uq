
ZZQ06 56 PUPS Tck Project 08/04/2010 09:53:00 AM 2208040105 Cancel 

Ticket Number: 2208040105
Old Ticket Number: 
Created By: JAC
Seq Number: 56

Created Date: 08/04/2010 09:53:33 AM
Work Date/Time: 08/04/2010 10:00:20 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SUMTER
Place: SUMTER
Address Number: 
Street: WOODLAND CT
Inters St: CURTISWOOD AVE
Subd: 

Type of Work: SEE REMARKS
Duration: APPROX 30 MINS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9106133881251, -80.3771082314937
Secondary: 33.9099645586933, -80.3762085659413
Lat/Long Caller Supplied: N

Members Involved: ATT09 CPLZ05 FTCZ81 MCI18 SCG76 TWUZ46 VERZ06               


Map Link: (NEEDS DEVELOPMENT)


