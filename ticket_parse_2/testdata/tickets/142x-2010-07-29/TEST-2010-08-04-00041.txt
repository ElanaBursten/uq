
ZZQ06 39 PUPS Web 08/04/2010 09:15:00 AM 2208040047 Resend 

Ticket Number: 2208040047
Old Ticket Number: 
Created By: JAC
Seq Number: 39

Created Date: 08/04/2010 09:16:40 AM
Work Date/Time: 08/04/2010 09:30:22 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: COLUMBIA
Address Number: 440
Street: SULGRAVE DR
Inters St: LETON DR
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 15 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: DRODRIGUEZ@TECHONEINC.NET

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0553560395475, -81.140882488615
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCGT05 SCGZ05 TWCZ40                           


Map Link: (NEEDS DEVELOPMENT)


