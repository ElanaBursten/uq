
ZZQ06 97 PUPS AH-Emerg 08/04/2010 02:15:00 PM 2208040243 Design 

Ticket Number: 2208040243
Old Ticket Number: 
Created By: JDB
Seq Number: 97

Created Date: 08/04/2010 02:15:40 PM
Work Date/Time: 08/18/2010 02:15:28 PM
Update: 09/07/2010 Good Through: 09/10/2010

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 
Street: LORICK CIR
Inters St: LORICK AVENUE
Subd: LATIMER MANOR 

Type of Work: SEE REMARKS
Duration: 6 MONTHS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0373362046847, -81.0381338032405
Secondary: 34.0338117291582, -81.0338634069676
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 MCI18 SCEKZ42 SCGZ02 STG10 TWCZ40              


Map Link: (NEEDS DEVELOPMENT)

Design tickets are courtesy tickets.  The utilities will mark the
requested area on a voluntary basis.  Members are to locate within
10 working days, please call requestor if you are unable to locate
within the 10 working days.


