
ZZQ06 34 PUPS Email 08/04/2010 08:35:00 AM 2208040028 Meet 

Ticket Number: 2208040028
Old Ticket Number: 
Created By: JAC
Seq Number: 34

Created Date: 08/04/2010 08:36:45 AM
Work Date/Time: 08/09/2010 08:45:14 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: CHAPIN
Address Number: 124
Street: SUMMIT POINT CT
Inters St: EDGEWOOD DR
Subd: TIMBERLAKE @ PENINSULA SECTION

Type of Work: DRAINAGE WORK
Duration: APPROX 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0899285698058, -81.3646137893557
Secondary: 34.0894952448968, -81.3628923616351
Lat/Long Caller Supplied: N

Members Involved: BSZB45 SCEDZ08 SCGZ05 TWCZ40                                


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


