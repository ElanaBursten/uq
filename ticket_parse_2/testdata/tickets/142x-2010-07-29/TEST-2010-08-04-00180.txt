
ZZQ06 108 PUPS Voice 08/04/2010 02:37:00 PM 2208040300 Resend 

Ticket Number: 2208040300
Old Ticket Number: 
Created By: JDB
Seq Number: 108

Created Date: 08/04/2010 02:38:12 PM
Work Date/Time: 08/04/2010 02:45:48 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: SEVEN OAKS
Address Number: 2426
Street: BUSH RIVER RD
Inters St: WESTOVER RD 
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: UNKNOWN

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0340754956076, -81.1260469881343
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 LC393 MID55 SCGZ05 TWCZ40                      


Map Link: (NEEDS DEVELOPMENT)


