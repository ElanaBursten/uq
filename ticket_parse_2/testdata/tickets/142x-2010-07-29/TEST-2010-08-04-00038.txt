
ZZQ06 36 PUPS AH-Emerg 08/04/2010 09:11:00 AM 2208040043 Cancel 

Ticket Number: 2208040043
Old Ticket Number: 
Created By: JAC
Seq Number: 36

Created Date: 08/04/2010 09:12:58 AM
Work Date/Time: 08/04/2010 09:15:20 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SUMTER
Place: SUMTER
Address Number: 40
Street: PAD ME WAY
Inters St: EBENEEZER
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 1 HR

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: WALKINGTHUNDER@CAROLINA.RR.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9993581509803, -80.4017812434365
Secondary: 33.9991914640155, -80.4010618575886
Lat/Long Caller Supplied: N

Members Involved: FTCZ81 TWUZ46                                               


Map Link: (NEEDS DEVELOPMENT)


