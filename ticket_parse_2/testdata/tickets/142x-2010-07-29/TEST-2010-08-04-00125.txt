
ZZQ06 85 PUPS Tck Project 08/04/2010 10:42:00 AM 2208040218 Resend 

Ticket Number: 2208040218
Old Ticket Number: 
Created By: JAC
Seq Number: 85

Created Date: 08/04/2010 10:43:54 AM
Work Date/Time: 08/04/2010 10:45:15 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: WEST COLUMBIA
Address Number: 
Street: CONTINENTAL DR
Inters St: DOGWOOD LN
Subd: N/A

Type of Work: SEE REMARKS
Duration: 1 WEEK

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: ABODINE@LEX-CO.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9918095015136, -81.1262550268771
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CWC18 SCEJZ40 SCGZ05 TLX24 TWCZ40                    


Map Link: (NEEDS DEVELOPMENT)


