
ZZQ35 62 PUPS U.S. Mail 08/04/2010 10:31:00 AM 2208040191 Survey 

Ticket Number: 2208040191
Old Ticket Number: 
Created By: JAC
Seq Number: 62

Created Date: 08/04/2010 10:32:48 AM
Work Date/Time: 08/09/2010 10:45:07 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LANCASTER
Place: FORT MILL
Address Number: 
Street: CHARLOTTE HWY
Inters St: CORPORATE CENTER DRIVE
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN CABLE
Duration: 2 WEEKS

Boring/Drilling: Y Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: KAREN.BLACKWOOD@STAR-LLC.NET

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.0131085989684, -80.8527690752131
Secondary: 35.0105723558857, -80.8518179840571
Lat/Long Caller Supplied: N

Members Involved: DPCZ72F FMT20 LAN22 LNWS52 TWJZ54                           


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


