
ZZQ35 94 PUPS Web 08/04/2010 02:41:00 PM 2208040308 Cancel 

Ticket Number: 2208040308
Old Ticket Number: 
Created By: JAC
Seq Number: 94

Created Date: 08/04/2010 02:42:31 PM
Work Date/Time: 08/04/2010 02:45:12 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 130
Street: OLD GROVE RD
Inters St: DIXIE CIR 
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: UNKNOWN

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7822369976754, -82.4132490131538
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ02 PNGZ81                                 


Map Link: (NEEDS DEVELOPMENT)


