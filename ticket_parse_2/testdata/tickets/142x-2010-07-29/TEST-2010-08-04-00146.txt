
ZZQ06 98 PUPS Email 08/04/2010 02:15:00 PM 2208040244 Resend 

Ticket Number: 2208040244
Old Ticket Number: 
Created By: JAC
Seq Number: 98

Created Date: 08/04/2010 02:16:03 PM
Work Date/Time: 08/04/2010 02:30:00 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SUMTER
Place: SUMTER
Address Number: 
Street: LEGETTE ST
Inters St: ALICE DR
Subd: 

Type of Work: CATV, INSTALL MAIN
Duration: TWO WEEKS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         
                                                                              


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9543095911457, -80.3876094492656
Secondary: 33.9543035531636, -80.3874826516406
Lat/Long Caller Supplied: N

Members Involved: CPLZ05 FTCZ81 SCG76 TWUZ46 VERZ06                           


Map Link: (NEEDS DEVELOPMENT)


