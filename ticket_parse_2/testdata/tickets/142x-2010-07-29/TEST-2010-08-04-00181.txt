
ZZQ35 90 PUPS Web 08/04/2010 02:38:00 PM 2208040302 Resend 

Ticket Number: 2208040302
Old Ticket Number: 
Created By: JDB
Seq Number: 90

Created Date: 08/04/2010 02:39:08 PM
Work Date/Time: 08/04/2010 02:45:56 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SPARTANBURG
Place: SPARTANBURG
Address Number: 208
Street: SWANSEA RD
Inters St: DUNBARTON
Subd: 

Type of Work: IRRIGATION, INSTALL PIPE(S)
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: MPOSTTOASTY1@GMAIL.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9558213675199, -81.8469849299744
Secondary: 34.9519540030096, -81.8461021619884
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60 PNSZ82 SWS58                           


Map Link: (NEEDS DEVELOPMENT)


