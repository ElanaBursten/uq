
ZZQ06 117 PUPS U.S. Mail 08/04/2010 02:50:00 PM 2208040327 Survey 

Ticket Number: 2208040327
Old Ticket Number: 
Created By: JAC
Seq Number: 117

Created Date: 08/04/2010 02:51:50 PM
Work Date/Time: 08/09/2010 03:00:11 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 126 
Street: EAST MAIN STREET
Inters St: NORTH CHURCH STREET
Subd: 

Type of Work: SIGN, OTHER
Duration: 2 HOURS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: RALIVINGSTON@GREGORYELECTRIC.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0059272067932, -81.27143456685
Secondary: 33.9512382435506, -81.2001569748352
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CGT63 CWC18 DIX54 JMW83 MID55 PBT43 QWC42 SCA88      
SCEDZ05 SCGZ05 SCTDZ01 TLX24 TWCZ40 TWTZ26 WINZ08                             


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


