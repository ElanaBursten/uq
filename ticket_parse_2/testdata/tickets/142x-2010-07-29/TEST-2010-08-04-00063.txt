
ZZQ06 54 PUPS AH-Emerg 08/04/2010 09:45:00 AM 2208040095 Cancel 

Ticket Number: 2208040095
Old Ticket Number: 
Created By: JAC
Seq Number: 54

Created Date: 08/04/2010 09:47:02 AM
Work Date/Time: 08/04/2010 10:00:13 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: RICHLAND
Place: IRMO
Address Number: 233
Street: CAEDMONS CREEK DR
Inters St: BROOKSONG CT
Subd: CAEDMONS CREEK

Type of Work: GAS, INSTALL SERVICE
Duration: 15 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1268098479041, -81.2091363292538
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 MID55 SCGZ05 TWCZ40                            


Map Link: (NEEDS DEVELOPMENT)


