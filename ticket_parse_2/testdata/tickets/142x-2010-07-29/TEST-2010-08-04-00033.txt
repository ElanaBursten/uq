
ZZQ35 24 PUPS Email 08/04/2010 09:06:00 AM 2208040037 Update 

Ticket Number: 2208040037
Old Ticket Number: 
Created By: JAC
Seq Number: 24

Created Date: 08/04/2010 09:07:31 AM
Work Date/Time: 08/09/2010 09:15:55 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: GREENVILLE
Place: GREENVILLE
Address Number: 1511
Street: S BATESVILLE ROAD
Inters St: HIGH TEC COURT
Subd: 

Type of Work: ELECTRIC, REPLACE POLE(S) & ANCHOR(S)
Duration: 4 HOURS

Boring/Drilling: Y Blasting: N White Lined: Y Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8622998255334, -82.237843831264
Secondary: 34.8527733000405, -82.2321973144983
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZT29 CCMZ41 DPCZ08 DPT77 PNGZ81 SXGZ30              


Map Link: (NEEDS DEVELOPMENT)


