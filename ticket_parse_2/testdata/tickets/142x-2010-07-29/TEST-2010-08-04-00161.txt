
ZZQ06 105 PUPS Voice 08/04/2010 02:24:00 PM 2208040267 Update 

Ticket Number: 2208040267
Old Ticket Number: 
Created By: JDB
Seq Number: 105

Created Date: 08/04/2010 02:24:48 PM
Work Date/Time: 08/09/2010 02:30:24 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: RICHLAND
Place: IRMO
Address Number: 124
Street: RUSHING WIND DR
Inters St: CHARING CROSS RD & DONCASTER DR
Subd: FRIARSGATE

Type of Work: GAS, REPLACE SERVICE
Duration: 15 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0991624141832, -81.1818840986203
Secondary: 34.0978007789623, -81.180368375349
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 LC393 MID55 RCU73 SCGZ05 TWCZ40                


Map Link: (NEEDS DEVELOPMENT)


