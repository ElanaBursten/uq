
ZZQ06 73 PUPS Voice 08/04/2010 10:26:00 AM 2208040176 Update 

Ticket Number: 2208040176
Old Ticket Number: 
Created By: JDB
Seq Number: 73

Created Date: 08/04/2010 10:26:31 AM
Work Date/Time: 08/09/2010 10:30:13 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: IRMO
Address Number: 
Street: SHADOWOOD DR
Inters St: NURSERY HILL RD
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN LINE
Duration: APPROX 2 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0689273789269, -81.1867841563942
Secondary: 34.0680711916355, -81.1863493737853
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ42 SCGZ05 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


