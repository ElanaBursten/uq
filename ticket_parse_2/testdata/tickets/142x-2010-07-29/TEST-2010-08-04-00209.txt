
ZZQ35 106 PUPS Web 08/04/2010 03:14:00 PM 2208040358 Update 

Ticket Number: 2208040358
Old Ticket Number: 
Created By: JDB
Seq Number: 106

Created Date: 08/04/2010 03:14:43 PM
Work Date/Time: 08/09/2010 03:15:15 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: ANDERSON
Place: IVA
Address Number: 
Street: PARK DR
Inters St: CRAFT ST
Subd: 

Type of Work: WATER, REPLACE METER
Duration: 6 MONTHS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: SOWENS@JMCONI.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.31897750059, -82.7000950218137
Secondary: 34.289456110508, -82.6473577812788
Lat/Long Caller Supplied: N

Members Involved: CCMZ41 DPCZ04 PNAZ80 WCR44                                  


Map Link: (NEEDS DEVELOPMENT)


