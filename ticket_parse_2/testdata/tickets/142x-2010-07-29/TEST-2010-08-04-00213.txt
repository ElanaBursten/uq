
ZZQ35 108 PUPS Tck Project 08/04/2010 03:25:00 PM 2208040373 Normal 

Ticket Number: 2208040373
Old Ticket Number: 
Created By: JDB
Seq Number: 108

Created Date: 08/04/2010 03:26:34 PM
Work Date/Time: 08/09/2010 03:30:39 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CHEROKEE
Place: GAFFNEY
Address Number: 131
Street: GREENFIELD RD
Inters St: HAMRICK ST
Subd: 

Type of Work: CATV, INSTALL DROP(S)
Duration: 1 DAY

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 35.0925619044601, -81.7221992781476
Secondary: 35.0129537389371, -81.6302574531774
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 COL23 DPCZ60 GPW48 LC393 MCI18 PNCZ57 PPL86   
QWC42 SCTDZ04A SPR80 WTGC09                                                   


Map Link: (NEEDS DEVELOPMENT)


