
ZZQ06 124 PUPS Tck Project 08/04/2010 03:34:00 PM 2208040388 Meet 

Ticket Number: 2208040388
Old Ticket Number: 
Created By: JDB
Seq Number: 124

Created Date: 08/04/2010 03:35:15 PM
Work Date/Time: 08/09/2010 03:45:00 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CHESTERFIELD
Place: CHERAW
Address Number: 623
Street: 2ND ST
Inters St: PARK ST
Subd: 

Type of Work: GAS, RETIRE SERVICE
Duration: 15 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7279464870594, -79.9166019323375
Secondary: 34.6771407998257, -79.874440794872
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 DXP53 MCI18 SCG84 SCGT84 TWYZ52               


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


