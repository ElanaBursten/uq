
ZZQ06 14 PUPS Tck Project 07/22/2010 10:53:00 1007220022 Survey 

Ticket Number: 1007220022
Old Ticket Number: 
Created By: DBL
Seq Number: 14

Created Date: 07/22/2010 10:54:49
Work Date/Time: 07/27/2010 11:00:14
Update: 08/13/2010 11:00:14 Good Through: 08/18/2010 11:00:14

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 1100
Street: SUMTER STREET
Inters St: SENATE STREET
Subd: 

Type of Work: CATCH BASIN, COLLASPED
Duration: 2 WEEKS

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: LIFEGUARD FIRE PROTECTION

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: JONATHAN@LIFEGUARDSC.COM

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0005033297239, -81.031350186754
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 COC82 ITC73 LC393 SCA88 SCEJZ40 SCG02 STG10    
TWCZ40 TWTZ26 USC42                                                           


Map Link: (NEEDS DEVELOPMENT)

Members are to contact the caller within 72 hours to make arrangements for the survey locate.


