
ZZQ06 125 PUPS Voice 08/04/2010 03:35:00 PM 2208040390 Resend 

Ticket Number: 2208040390
Old Ticket Number: 
Created By: JDB
Seq Number: 125

Created Date: 08/04/2010 03:37:04 PM
Work Date/Time: 08/04/2010 03:45:24 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: CHESTERFIELD
Place: CHERAW
Address Number: 312
Street: SUMMER LN
Inters St: RED HILL RD
Subd: DEERFIELD ESTATES

Type of Work: SEPTIC, INSTALL FIELD LINES
Duration: 1 1/2 TO 2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: Y

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7074899059377, -79.9170277955431
Secondary: 34.6365136025784, -79.8733500703989
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 DXP53 LRE34 MCI18 SCG84 SCGT84 TWYZ52         


Map Link: (NEEDS DEVELOPMENT)


