
ZZQ35 48 PUPS Web 08/04/2010 10:13:00 AM 2208040146 Cancel 

Ticket Number: 2208040146
Old Ticket Number: 
Created By: JDB
Seq Number: 48

Created Date: 08/04/2010 10:14:21 AM
Work Date/Time: 08/04/2010 10:15:08 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: GREENVILLE
Place: SIMPSONVILLE
Address Number: 105, 107, 109
Street: CLEAR LAKE DRIVE
Inters St: POND COURT, 
Subd: STANDING SPRINGS ESTATES

Type of Work: LANDSCAPE, OTHER
Duration: 1-2 WEEKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: HIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY THAT
YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS START   
WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //                   
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: PLINGEL@AOL.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.738187442208, -82.306640066268
Secondary: 34.7275012854121, -82.2950227505813
Lat/Long Caller Supplied: N

Members Involved: CCMZ41 DPCZ02 PNGZ81 VERZ06                                 


Map Link: (NEEDS DEVELOPMENT)


