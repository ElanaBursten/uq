
ZZQ06 49 PUPS Web 07/27/2010 08:53:00 1007270045 Meet 

Ticket Number: 1007270045
Old Ticket Number: 
Created By: RMD
Seq Number: 49

Created Date: 07/27/2010 08:53:37
Work Date/Time: 07/30/2010 09:00:21
Update: 08/18/2010 09:00:21 Good Through: 08/23/2010 09:00:21

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 163
Street: SIR EDWARDS LN
Inters St: CORLEY MILL RD 
Subd: 

Type of Work: CONDUIT AND FOUNDATION
Duration: 15 DAYS 

Boring/Drilling: Y
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: TECH ONE 

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0368343506336, -81.1818213763033
Secondary: 34.0355528729712, -81.1796143869959
Lat/Long Caller Supplied: N

Members Involved: SCEDZ05 SCGZ05 TLX24 TWCZ40 WINZ08                          


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


