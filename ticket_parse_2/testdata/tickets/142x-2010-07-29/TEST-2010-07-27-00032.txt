
ZZQ06 25 PUPS Web 07/27/2010 09:02:00 1007270060 Update 

Ticket Number: 1007270060
Old Ticket Number: 1007220033
Created By: RMD
Seq Number: 25

Created Date: 07/27/2010 09:02:26
Work Date/Time: 07/30/2010 09:15:24
Update: 08/18/2010 09:15:24 Good Through: 08/23/2010 09:15:24

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 306 
Street: WATEREE AVE
Inters St: CATAWBA ST 
Subd: 

Type of Work: SEE REMARKS
Duration: 2 HOURS 

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: SEE REMARKS

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY // IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME //THANKS RHONDA  ** rdotman@sc1pups.org (800) 290-2783 press 1       


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Mobile
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9934347361365, -81.0185818367744
Secondary: 33.9904269983576, -81.0172505430034
Lat/Long Caller Supplied: N

Members Involved: ATT09 BSZB45 COC82 QWC42 SCEJZ40 SCG02 TWCZ40               


Map Link: (NEEDS DEVELOPMENT)


