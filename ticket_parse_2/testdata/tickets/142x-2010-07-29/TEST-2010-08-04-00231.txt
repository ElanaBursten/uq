
ZZQ06 130 PUPS U.S. Mail 08/04/2010 03:39:00 PM 2208040396 No Show 

Ticket Number: 2208040396
Old Ticket Number: 
Created By: JAC
Seq Number: 130

Created Date: 08/04/2010 03:40:23 PM
Work Date/Time: 08/04/2010 03:45:39 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: CHESTERFIELD
Place: CHERAW
Address Number: 707
Street: ESKRIDGE DR
Inters St: CHRISTIAN ST 
Subd: 

Type of Work: SEE REMARKS
Duration: UNKNOWN

Boring/Drilling: Y Blasting: Y White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: shaqd32@yahoo.com

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.7055965713926, -79.9122052599942
Secondary: 34.6830681432859, -79.8809009121498
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CPLZ05 MCI18 SCG84 SCGT84 TWYZ52                     


Map Link: (NEEDS DEVELOPMENT)


