
ZZQ06 41 PUPS Voice 08/04/2010 09:20:00 AM 2208040053 Update 

Ticket Number: 2208040053
Old Ticket Number: 
Created By: JDB
Seq Number: 41

Created Date: 08/04/2010 09:21:11 AM
Work Date/Time: 08/09/2010 09:30:52 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: SEVEN OAKS
Address Number: 
Street: SAINT ANDREWS RD
Inters St: BUSH RIVER RD
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN LINE
Duration: 2 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0576855646845, -81.1615985303714
Secondary: 34.0545343926525, -81.1604417710179
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 MID55 SCEKZ42 SCGT05 SCGZ05 TWCZ40             


Map Link: (NEEDS DEVELOPMENT)


