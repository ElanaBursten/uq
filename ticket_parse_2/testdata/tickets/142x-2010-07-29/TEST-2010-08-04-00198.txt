
ZZQ06 116 PUPS Walk-In 08/04/2010 02:47:00 PM 2208040324 Emergency 

Ticket Number: 2208040324
Old Ticket Number: 
Created By: JAC
Seq Number: 116

Created Date: 08/04/2010 02:50:01 PM
Work Date/Time: 08/04/2010 03:00:50 PM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: MARLBORO
Place: BENNETTSVILLE
Address Number: 401 A
Street: WINSTON AVE
Inters St: NORMAN ST
Subd: 

Type of Work: WATER, LINE REPAIR
Duration: APPROX 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.6547097686381, -79.7189736238352
Secondary: 34.6120074274761, -79.6582913495523
Lat/Long Caller Supplied: N

Members Involved: BSZU45 CBN90 CGT61 CPLZ05                                   


Map Link: (NEEDS DEVELOPMENT)


