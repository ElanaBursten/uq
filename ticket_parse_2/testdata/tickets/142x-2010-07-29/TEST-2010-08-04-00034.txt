
ZZQ35 25 PUPS Email 08/04/2010 09:08:00 AM 2208040038 Resend 

Ticket Number: 2208040038
Old Ticket Number: 
Created By: JAC
Seq Number: 25

Created Date: 08/04/2010 09:09:16 AM
Work Date/Time: 08/04/2010 09:15:45 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: GREENVILLE
Place: TRAVELERS REST
Address Number: 14
Street: TEX MCCLURE LANE
Inters St: KEELER MILL ROAD
Subd: 

Type of Work: ELECTRIC, RELOCATE SERVICE
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9566520803371, -82.5005344175363
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ02 PNGZ81                                 


Map Link: (NEEDS DEVELOPMENT)


