
ZZQ35 93 PUPS Voice 08/04/2010 02:40:00 PM 2208040307 Resend 

Ticket Number: 2208040307
Old Ticket Number: 
Created By: JDB
Seq Number: 93

Created Date: 08/04/2010 02:42:16 PM
Work Date/Time: 08/04/2010 02:45:42 PM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SPARTANBURG
Place: SPARTANBURG
Address Number: 749
Street: GLENRIDGE RD
Inters St: BRANDERMILL RD
Subd: WOODRIDGE

Type of Work: DRAINAGE WORK
Duration: 1/2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9279379311269, -82.0143095691102
Secondary: 34.9227533225012, -82.0108590018829
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ60 PNSZ82 SWS58                           


Map Link: (NEEDS DEVELOPMENT)


