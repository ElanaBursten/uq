
ZZQ06 78 PUPS Voice 08/04/2010 10:34:00 AM 2208040197 Normal 

Ticket Number: 2208040197
Old Ticket Number: 
Created By: JDB
Seq Number: 78

Created Date: 08/04/2010 10:35:12 AM
Work Date/Time: 08/09/2010 10:45:48 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: LEXINGTON
Address Number: 143
Street: SIXTEEN MILE CT
Inters St: PILGRIM CHURCH RD
Subd: 

Type of Work: SEWER, INSTALL TAP
Duration: APPROX 3 DAYS

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0177743875116, -81.2321101732483
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: CWC18 SCEDZ05 SCGZ05 TLX24 TWCZ40 WINZ08                    


Map Link: (NEEDS DEVELOPMENT)


