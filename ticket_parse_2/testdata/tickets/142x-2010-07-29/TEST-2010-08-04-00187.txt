
ZZQ06 109 PUPS Voice 08/04/2010 02:43:00 PM 2208040311 Emergency 

Ticket Number: 2208040311
Old Ticket Number: 
Created By: JDB
Seq Number: 109

Created Date: 08/04/2010 02:43:59 PM
Work Date/Time: 08/04/2010 02:45:38 PM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: COLUMBIA
Address Number: 104
Street: WINDSONG ISLAND LN
Inters St: SETTING SUN LN 
Subd: WEXFORD 

Type of Work: SEWER, REPAIR LINE
Duration: 1 DAY 

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0892822199949, -81.2395312646283
Secondary: 34.0886477468771, -81.2387054424749
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ42 SCGZ05 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


