
ZZQ06 42 PUPS Voice 08/04/2010 09:22:00 AM 2208040056 Normal 

Ticket Number: 2208040056
Old Ticket Number: 
Created By: JDB
Seq Number: 42

Created Date: 08/04/2010 09:22:54 AM
Work Date/Time: 08/09/2010 09:30:29 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: LEXINGTON
Place: WEST COLUMBIA
Address Number: 1644
Street: GOLDFINCH LN
Inters St: WHIPPOORWILL DR
Subd: WESTOVER ACRES

Type of Work: LANDSCAPING
Duration: APPROX 2 DAYS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.0100414427347, -81.1049671107687
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 CWC18 SCGZ05 TWCZ40                                  


Map Link: (NEEDS DEVELOPMENT)


