
ZZQ06 128 PUPS Voice 08/04/2010 03:38:00 PM 2208040394 Meet 

Ticket Number: 2208040394
Old Ticket Number: 
Created By: JDB
Seq Number: 128

Created Date: 08/04/2010 03:39:16 PM
Work Date/Time: 08/09/2010 03:45:42 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CLARENDON
Place: SUMMERTON
Address Number: 1318
Street: SAILING WAY
Inters St: WASH DAVIS RD
Subd: 

Type of Work: GARDENING
Duration: UNSURE

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.5647000921971, -80.3655622440385
Secondary: 33.4798030466363, -80.2791175960846
Lat/Long Caller Supplied: N

Members Involved: FTCZ80 SECZ05 TWUZ46                                        


Map Link: (NEEDS DEVELOPMENT)

Members are to contact caller to schedule day and time to meet and locate lines.


