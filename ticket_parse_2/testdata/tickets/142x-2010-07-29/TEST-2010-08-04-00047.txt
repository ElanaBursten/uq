
ZZQ35 30 PUPS AH-Emerg 08/04/2010 09:23:00 AM 2208040058 Emergency 

Ticket Number: 2208040058
Old Ticket Number: 
Created By: JDB
Seq Number: 30

Created Date: 08/04/2010 09:23:56 AM
Work Date/Time: 08/04/2010 09:30:00 AM
Update: 08/23/2010 Good Through: 08/26/2010

Excavation Information:
State: SC     County: GREENVILLE
Place: TRAVELERS REST
Address Number: 1700
Street: KEELER MILL RD
Inters St: FORESTVILLE DR 
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: UNKNOWN

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.9550536428038, -82.4984712889872
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 DPCZ02                                               


Map Link: (NEEDS DEVELOPMENT)


