
ZZQ35 52 PUPS Web 08/04/2010 10:19:00 AM 2208040157 Resend 

Ticket Number: 2208040157
Old Ticket Number: 
Created By: JDB
Seq Number: 52

Created Date: 08/04/2010 10:19:40 AM
Work Date/Time: 08/04/2010 10:30:20 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: SPARTANBURG
Place: MOORE
Address Number: 528
Street: SCENIC OAK DRIVE RESIDENCE
Inters St: S. SWEETWATER HILLS
Subd: SWEETWATER HILLS 

Type of Work: SIDEWALK(S), INSTALL
Duration: 2 WEEKS

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: CULLETTOM@BELLSOUTH.NET

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.8753913464057, -82.0758786657597
Secondary: 34.8674452184549, -82.0724577903368
Lat/Long Caller Supplied: N

Members Involved: BSZT29 CCMZ41 DPCZ08 PNSZ82 SJW43                           


Map Link: (NEEDS DEVELOPMENT)


