
ZZQ06 53 PUPS Email 08/04/2010 09:42:00 AM 2208040087 Resend 

Ticket Number: 2208040087
Old Ticket Number: 
Created By: JAC
Seq Number: 53

Created Date: 08/04/2010 09:43:16 AM
Work Date/Time: 08/04/2010 09:45:49 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: LEXINGTON
Place: CAYCE
Address Number: 207
Street: ARDEN LN
Inters St: ANN LN
Subd: EDENWOOD

Type of Work: SEE REMARKS
Duration: UNSURE

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: **THIS IS TESTING ONLY// DO NOT MARK// PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS// TEST TICKETS     
START WITH 22// PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS//                 
RDOTMAN@SC1PUPS.ORG**                                                         


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.9556972110388, -81.0620170619907
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 SCEJZ40 SCGZ05 TWCZ40                                


Map Link: (NEEDS DEVELOPMENT)


