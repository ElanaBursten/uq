
ZZQ35 41 PUPS Voice 08/04/2010 09:55:00 AM 2208040112 Resend 

Ticket Number: 2208040112
Old Ticket Number: 
Created By: JDB
Seq Number: 41

Created Date: 08/04/2010 09:55:54 AM
Work Date/Time: 08/04/2010 10:00:44 AM
Update:                     Good Through:                    

Excavation Information:
State: SC     County: ANDERSON
Place: BELTON
Address Number: 710
Street: BLAKE DAIRY RD
Inters St: BELTON FARM RD
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: 1 DAY

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.5207366660838, -82.4613776929409
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZT29 DPCZ04                                               


Map Link: (NEEDS DEVELOPMENT)


