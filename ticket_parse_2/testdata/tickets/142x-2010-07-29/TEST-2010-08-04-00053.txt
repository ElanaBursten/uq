
ZZQ06 46 PUPS Voice 08/04/2010 09:30:00 AM 2208040070 Normal 

Ticket Number: 2208040070
Old Ticket Number: 
Created By: JDB
Seq Number: 46

Created Date: 08/04/2010 09:30:50 AM
Work Date/Time: 08/09/2010 09:30:31 AM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: RICHLAND
Place: COLUMBIA
Address Number: 747
Street: N SHADOWBROOK DR
Inters St: ROCK HAVEN DR 
Subd: 

Type of Work: TELEPHONE, INSTALL DROP(S)
Duration: UNKNOWN

Boring/Drilling: Y Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1102858212982, -80.9394253600432
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 COC82 SCEKZ82 SCGZ02 TWCZ40                          


Map Link: (NEEDS DEVELOPMENT)


