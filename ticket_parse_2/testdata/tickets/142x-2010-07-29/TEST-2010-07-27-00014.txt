
ZZQ06 8 PUPS Walk-In 07/27/2010 08:38:00 1007270012 Update 

Ticket Number: 1007270012
Old Ticket Number: 1007220005
Created By: RMD
Seq Number: 8

Created Date: 07/27/2010 08:39:31
Work Date/Time: 07/30/2010 08:45:31
Update: 08/18/2010 08:45:31 Good Through: 08/23/2010 08:45:31

Excavation Information:
State: SC     County: KERSHAW
Place: ELGIN
Address Number: 1013
Street: LOWER BRANCH LN
Inters St: RYAN LN
Subd: 

Type of Work: TELEPHONE, INSTALL MAIN AND DROP (S)
Duration: UNKNOWN

Boring/Drilling: N
Blasting: N
White Lined: N
Near Railroad: N


Work Done By: AT&T

Remarks/Instructions: TESTING FOR V3 THIS IS ONLY A TEST DO NOT MARK TESTING  
ONLY//  IF YOU CAN NOT READ ANY PART OF THIS TICKETE PLEASE GIVE ME A CALL OR 
EMAIL ME // THANKS RHONDA  ** RDOTMAN@SC1PUPS.ORG (800) 290-2783 PRESS 1      


Caller Information: 
Name: RHONDA DOTMAN                         PUPS                                  
Address: 810 DUTCH SQUARE BLVD SUITE 320
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 2 Type: Business
Fax:  Caller Email: 

Contact Information:
Contact: RHONDA DOTMAN Email: rdotman@hotmail.com
Call Back:  Fax: 

Grids: 
Lat/Long: 34.1931501462475, -80.8158162656271
Secondary: 0, 0
Lat/Long Caller Supplied: N

Members Involved: BSZB45 TWCZ40                                               


Map Link: (NEEDS DEVELOPMENT)


