
ZZQ06 111 PUPS Web 08/04/2010 02:44:00 PM 2208040313 Normal 

Ticket Number: 2208040313
Old Ticket Number: 
Created By: JDB
Seq Number: 111

Created Date: 08/04/2010 02:44:26 PM
Work Date/Time: 08/09/2010 02:45:09 PM
Update: 08/26/2010 Good Through: 08/31/2010

Excavation Information:
State: SC     County: CLARENDON
Place: MANNING
Address Number: 1067
Street: JAMIE CT
Inters St: GIBBONS ST
Subd: DOLPHIN COVE

Type of Work: CATV, INSTALL DROP(S)
Duration: 1 HR

Boring/Drilling: N Blasting: N White Lined: N Near Railroad: N

Work Done By: PALMETTO UTILITY PROTECTION SERVICE

Remarks/Instructions: THIS IS TESTING ONLY // DO NOT MARK // PLEASE VERIFY    
THAT YOU ARE ABLE TO PARSE, READ AND PROCESS THESE TICKETS // TEST TICKETS    
START WITH "22" // PLEASE EMAIL RHONDA DOTMAN IF ANY QUESTIONS //             
RDOTMAN@SC1PUPS.ORG                                                           


Caller Information: 
Name: RHONDA DOTMAN                         PALMETTO UTILITY PROTECTION SERVICE   
Address: 810 DUTCH SQUARE BLVD
City: COLUMBIA State: SC Zip: 29210
Phone: (803) 939-1117 Ext: 126 Type: Business
Fax:  Caller Email: WALKINGTHUNDER@CAROLINA.RR.COM

Contact Information:
Contact: Rhonda Dotman Email: rdotman@sc1pups.org
Call Back:  Fax: 

Grids: 
Lat/Long: 33.6866869037551, -80.2236178963363
Secondary: 33.6753886638856, -80.2099595796942
Lat/Long Caller Supplied: N

Members Involved: CPLZ05 FTCZ80 SECZ05 TWUZ46 VERZ06                          


Map Link: (NEEDS DEVELOPMENT)


