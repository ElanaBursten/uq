
BYER04     00200 ALOC 04/27/11 16:16:28 111171080 NORMAL NOGRID

=========//  ALOC LOCATE REQUEST  //=========

TICKET NUMBER--[111171080] 
OLD TICKET NUM-[]

MESSAGE TYPE---[NORMAL] LEAD TIME--[48]
PREPARED-------[04/27/11]  AT  [1611]  BY  [KBERRY]

CONTRACTOR--[THE ZONE]   CALLER--[WILLIAM COLLINS]
ADDRESS-----[23480 CORD 55]
CITY--------[SILVERHILL]   STATE--[AL]   ZIP--[36576]
CALL BACK---[(251) 978-8330]   PHONE--[(251) 945-1543]
CONTACT-----[WILLIAM COLLINS]   PHONE--[(251) 945-1543]
CONTACT FAX-[]

WORK TO BEGIN--[04/29/11] AT [1615]      STATE--[AL]
COUNTY---[BALDWIN]  PLACE--[SILVERHILL]

ADDRESS--[23480]   STREET--[][CORD 55][][]
NEAREST INTERSECTION--------[CORD 54]
LATITUDE--[30.5671934082843]    LONGITUDE--[-87.7507878353319]
SECONDARY LATITUDE--[0]    SECONDARY LONGITUDE--[0]

ADDITIONAL ADDRESSES IN LOCATION--[N]

LOCATION INFORMATION--
[WORK TYPE: POWER LINE EXTENSION -- PER WILLIAM THERE IS A RACE TRACK BESIDE   ]
[THE STORE -- LOCATE ENTIRE AREA BETWEEN THE RACE TRACK & THE ASPHALT AT THE   ]
[ZONE COMPANY ITSELF - NEED TO LOCATE AS FAR BACK AS THE POWER POLES & THE     ]
[TRANSFORMER WHEN LOCATING - IF ANY QUESTIONS PLEASE CONTACT WILLIAM AT        ]
[251-978-8330                                                                  ]


WORK TYPE--[ELEC. U/G,  INSTL]  DONE FOR--[THE ZONE]
EXTENT-----[]

EXPLOSIVES--[N]    WHITE PAINT--[N]    AT INTERSECTION--[N]

SUBDIVISION--[]
GRIDS-----
          [                 ]

UTILITIES NOTIFIED--
  CODE       NAME                         CODE       NAME                      
  ---------- --------------------------   ---------- --------------------------
  BCCM01     BALDWIN COUNTY COMMISSI...   BCSS01     BALDWIN COUNTY SEWER SE...
  BEMC01     BALDWIN COUNTY EMC           CAOP01     CABLE OPTIONS             
  DPHN01     DAPHNE UTILITIES BOARD       GULF01     GULF TELEPHONE            
  HBCM01     HARBOR COMMUNICATIONS, ...   LVL301     LEVEL 3 COMMUNICATIONS    
  RBDL01     ROBERTSDALE, CITY OF         SLLC01     SOUTHERN LIGHT, LLC       

