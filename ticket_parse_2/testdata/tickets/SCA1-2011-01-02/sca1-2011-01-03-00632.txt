
UQSTSO 00547A USAS 01/03/11 10:15:43 A10030368-00A NORM NEW GRID

Ticket : A10030368  Date: 01/03/11 Time: 10:11 Oper: ADS Chan: 100
Old Tkt: A10030368  Date: 01/03/11 Time: 10:15 Oper: ADS Revision: 00A

Company: GSI                            Caller: RYAN WHITE-VERIZON
Co Addr: UNK
City&St:                                Zip:
Phone: 951-235-0592 Ext:      Call back: 7-330 M-F
Formn: ADAM HOLGUIN         Phone: 951-533-1011

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 13212       Street:PRAIRIESTONE DR
X/ST 1 : COLT DR
Locat:

Excav Enters Into St/Sidewalk: N

Grids: 0835B042
Lat/Long  : 33.732568/-117.430840 33.732486/-117.427904
          : 33.731755/-117.430862 33.731673/-117.427927
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : INSTALL FIBER OPTICS
Wkend: N  Night: N
Work date: 01/05/11 Time: 10:11 Hrs notc: 048 Work hrs: 048 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : VERIZON OC3267060

Tkt Exp: 01/31/11

Mbrs : EVW37  SCG1CO TWCWRIV       USCE77 UVZMENIF
