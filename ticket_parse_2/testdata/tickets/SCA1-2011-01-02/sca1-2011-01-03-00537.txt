UQSTSO 00455A USAS 01/03/11 09:42:11 A10030300-01A NORM RXMT GRID

Ticket : A10030300  Date: 01/03/11 Time: 09:41 Oper: CJD Chan: 100
Old Tkt: A10030300  Date: 01/03/11 Time: 09:40 Oper: CJD Revision: 01A

Company: C/OF SAN DIEGO-WASTEWATER      Caller: ANTONIO ANDRADE
Co Addr: 9150 TOPAZ WAY
City&St: SAN DIEGO, CA                  Zip: 92112
Phone: 619-274-1447 Ext:      Call back: ANYTIME
Formn: ANTONIO

State: CA County: SAN DIEGO       Place: SAN DIEGO
Delineated: Y
Delineated Method: WHITEPAINT
Address: 3730        Street:WARNER ST
X/ST 1 : CATALINA BLVD
Locat: LOC IN THE ALLEY REAR OF PROP ......CREW ON SITE.......(PLEASE CALL
     : ANTONIO AT 619/274-1447 IF ANY CONFLICT)

Excav Enters Into St/Sidewalk: N

Grids: 1287J034     1288A033
Lat/Long  : 32.716974/-117.247541 32.716236/-117.243627
          : 32.716165/-117.247694 32.715426/-117.243779
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : EMERGENCY REPAIR SEWER LATERAL
Wkend: N  Night: N
Work date: 01/03/11 Time: 09:41 Hrs notc: 000 Work hrs: 000 Priority: 2
Instruct :                                Permit: NOT REQUIRED
Done for : C/OF SAN DIEGO WASTEWATER

Tkt Exp: 01/31/11

                                  COMMENTS

**RESEND**ADDING TO PLEASE CALL ANTONIO AT 619/274-1447 IF ANY CONFLICT PER
ANTONIO--[CJD 01/03/11 09:42]

Mbrs : ATTD28SD      COX01  NEXTG  SDG01  SDGET  SND01  SND02  UATLSD
UNIFPORTSD
