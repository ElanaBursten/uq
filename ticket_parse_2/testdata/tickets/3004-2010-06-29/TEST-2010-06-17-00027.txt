
AGL118  01583 GAUPC 06/17/10 14:08:23 06170-269-026-000 NORMAL
Underground Notification             
Notice : 06170-269-026 Date: 06/17/10  Time: 14:05  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 124    To:        Name: N  WELLINGTON                             
Near  : Name:    FORSYTH                        RD  

Subdivision:                                         
Locate: LOC THE PERIMETER OF HOME WITH A 10FT OFFSET                           

Grids       : 3253C8344B 
Work type   : INSTALL SENTRICON                                                   

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 1 HOURS    Priority: 3
Done for  : COPELAND/RED                            
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : ARROW EXTERMINATORS                       Type: CONT                
Co addr : 4230 CAVALIER DR                         
City    : MACON                           State   : GA Zip: 31220              
Caller  : ADAM SMITH                      Phone   :  478-471-8571              
Fax     :                                 Alt. Ph.:  478-471-8571              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/17/10  Time: 14:05  Oper: 215
Mbrs : AGL118 BGAWM CCM01 GAUPC GP500 JONG01 MWA01 MWA02 
-------------------------------------------------------------------------------
