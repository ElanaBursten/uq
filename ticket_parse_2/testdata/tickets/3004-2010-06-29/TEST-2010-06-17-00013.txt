
AGL106  01128 GAUPC 06/17/10 12:04:27 06170-258-024-001 INSUF  CANCEL
Underground Notification             
Notice : 06170-258-024 Date: 06/17/10  Time: 12:03  Revision: 001 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 615    To:        Name:    BEULAH CHURCH                  RD      
Near  : Name:    COLUMBIA                       DR  

Subdivision:                                         
Locate:  LEFT SIDE OF PROPERTY                                                 

Grids       : 3336C8506B 3336C8506C 3336C8506D 3336D8506B 3336D8506C 
Grids       : 3336D8506D 
Work type   : INSTALL WATER SVC                                                   

Start date: 06/18/10 Time: 00:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 1 DAY      Priority: 2
Done for  :                                         
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : CANCELED, PLEASE DISREGARD WRONG ADDRESS - GW - 6-17-10             
        : *** DrivingInstructions : THIS IS WITHIN 1 MILE OF MT ZION RD ON THE LEFT                     

Company :                                           Type: HOME                
Co addr :                                          
City    :                                 State   : GA Zip:                    
Caller  : RONALD EDWARDS                  Phone   :  256-310-1196              
Fax     :                                 Alt. Ph.:  256-310-1196              
Email   :                                                                     
Contact : BRADLEY EDWARDS                 205-492-7725              

Submitted date: 06/17/10  Time: 12:03  Oper: 198
Mbrs : AGL106 BGAWR CAR50 CAR51 COMNOR CRL70 GAUPC GP620 
-------------------------------------------------------------------------------
