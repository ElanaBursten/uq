
AGL103  01125 GAUPC 06/18/10 12:48:05 06180-300-679-000 NORMAL
Underground Notification             
Notice : 06180-300-679 Date: 06/18/10  Time: 12:44  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 942    To:        Name:    VIRGINIA                       CIR  NE 
Near  : Name:    TODD                           RD  

Subdivision:                                         
Locate: C. ROBERTSON 404-228-0615 PLEASE LOCATE ALL UTILITIES FRONT LEFT AND RI
      :  GHT QUADRANTS TO METER AND TO INCLUDE THE RIGHT OF WAY.  PLEASE MARK AL
      :  L UTILITIES AND NOTE ANY CONFLICTS.  FRONT AND BOTH SIDES OF PROPERTY  

Grids       : 3346A8421B 3346A8421C 
Work type   : BURYING CATV SERVICE DROPS                                          

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : COMCAST                                 
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Driveway & Sidewalk           

Company : SPECTRUM TECHNOLOGY SOLUTIONS             Type: CONT                
Co addr : 20 GILL LN                               
City    : STOCKBRIDGE                     State   : GA Zip: 30281              
Caller  : ERIN KORESKI                    Phone   :  404-558-8704              
Fax     :  770-898-8901                   Alt. Ph.:                            
Email   : ERIN.SPECTRUM@MINDSPRING.COM                                        
Contact :                                                           

Submitted date: 06/18/10  Time: 12:44  Oper: 201
Mbrs : AGL103 ATL01 ATL02 BSCA GAUPC GP104 
-------------------------------------------------------------------------------
