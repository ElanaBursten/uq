
AGL118  01136 GAUPC 06/18/10 12:56:18 06180-249-016-000 DAMAGE
Underground Notification             
Notice : 06180-249-016 Date: 06/18/10  Time: 12:53  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 426    To:        Name:    APPLE BLOSSOM                  WAY     
Near  : Name:    CRABAPPLE                          

Subdivision:                                         
Locate:  ENTIRE PROPERTY AND WITHIN RIGHT OF WAY                               

Grids       : 3248A8332C 3248A8332D 3249D8332C 3249D8332D 
Work type   : INSTALL IRRIGATION SYSTEM & SOD                                     

Start date: 06/18/10 Time: 00:00 Hrs notc : 000
Legal day : 06/18/10 Time: 00:00 Good thru: 06/18/10 Restake by: 06/18/10
RespondBy : 06/18/10 Time: 00:00 Duration : 3 WEEKS    Priority: 6
Done for  : RES/EDMONDS                             
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks :  ORG TICKET - 06100-258-061 DAMAGE IS IN THE FRONT OF PROPERTY - CAL
        : LER STATES IT WAS NOT LOCATED  SERVICE/DROP LINE DAMAGE TO BGAWM,ATT
        : / D **PRIVATE LAND OWNER** **EXTENT: CUT IN HALF **                 
        : DAMAGE TO SERVICE/DROP BGAWM EXTENT: CUT IN HALF  **SERVICE OUT

Company : CUTTING EDGE LANDSCAPING                  Type: CONT                
Co addr : 40 OLD POPES FERRY RD                    
City    : JUILETTE                        State   : GA Zip: 31046              
Caller  : WADE HALL                       Phone   :  478-475-1910              
Fax     :                                 Alt. Ph.:  478-972-3032              
Email   :                                                                     
Contact : WADE HALL                       478-972-3032              

Submitted date: 06/18/10  Time: 12:53  Oper: 198
Mbrs : AGL118 BGAWM CCM01 GAUPC GP500 MWA01 MWA02 
-------------------------------------------------------------------------------
