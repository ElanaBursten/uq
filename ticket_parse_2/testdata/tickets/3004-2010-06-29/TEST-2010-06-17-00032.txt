
AGL118  01695 GAUPC 06/17/10 14:36:33 06170-301-235-000 NORMAL
Underground Notification             
Notice : 06170-301-235 Date: 06/17/10  Time: 14:35  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 6707   To:        Name:    RIVERSIDE                      DR      
Near  : Name:    WESLEYAN                       DR  

Subdivision:                                         
Locate: SETTING NEW POLE IN BACK YARD    FLAGGED REAR OF PROPERTY              

Grids       : 3256A8343C 3256B8343C 
Work type   : SETTING POLES AND ANCHORS                                           

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 6 HOURS    Priority: 3
Done for  : GPC                                     
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : GEORGIA POWER                             Type: MEMB                
Co addr : 960 KEY STREET                           
City    : MACON                           State   : GA Zip: 31204              
Caller  : DENNIS PRITCHETT                Phone   :  478-784-5006              
Fax     :                                 Alt. Ph.:  478-784-5006              
Email   : DKPRITCH@SOUTHERNCO.COM                                             
Contact :                                                           

Submitted date: 06/17/10  Time: 14:35  Oper: 138
Mbrs : AGL118 BGAWM CCM01 CNT70 GAUPC GP500 MWA01 MWA02 SNG85 
-------------------------------------------------------------------------------
