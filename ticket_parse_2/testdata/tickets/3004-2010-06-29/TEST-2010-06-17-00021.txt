
AGL103  01420 GAUPC 06/17/10 13:36:50 06170-220-030-000 EMERG 
Underground Notification             
Notice : 06170-220-030 Date: 06/17/10  Time: 13:34  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 355    To:        Name:    CAMERON                        ST   SE 
Near  : Name:    MEMORIAL                       DR  

Subdivision:                                         
Locate:  FRONT OF PROPERTY                                                     

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3344B8421A 
Work type   : RPLC WTR SVC                                                        

Start date: 06/17/10 Time: 00:00 Hrs notc : 000
Legal day : 06/17/10 Time: 13:33 Good thru: 06/22/10 Restake by: 06/17/10
RespondBy : 06/17/10 Time: 23:59 Duration : UNKNOWN    Priority: 1
Done for  : KATHLEEN LEVY                           
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks :  EMERGENCY: SVC IS OUT                                              

Company : CODY'S PLUMBING                           Type: CONT                
Co addr : 2295 PINEWOOD DRIVE                      
City    : DECATUR                         State   : GA Zip: 30032              
Caller  : NORMAN CODY                     Phone   :  404-933-4030              
Fax     :                                 Alt. Ph.:  404-933-4030              
Email   :                                                                     
Contact : NORMAN CODY                     404-933-4030              

Submitted date: 06/17/10  Time: 13:34  Oper: 198
Mbrs : AGL103 ATL01 ATL02 BSCA GAUPC GP104 GP115T 
-------------------------------------------------------------------------------
