
AGL118  01767 GAUPC 06/17/10 14:56:12 06170-274-034-000 EMERG 
Underground Notification             
Notice : 06170-274-034 Date: 06/17/10  Time: 14:52  Revision: 000 

State : GA County: BIBB          Place: MACON                                   
Addr  : From: 1226   To:        Name:    EDNA                           PL      
Near  : Name:    SHARON                         DR  

Subdivision:                                         
Locate: LOCATE THE FRONT OF THE PROPERTY INCLUDING R/O/W B/S                   

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3250D8341C 3250D8341D 
Work type   : REPAIRING WATER SERVICE                                             

Start date: 06/17/10 Time: 00:00 Hrs notc : 000
Legal day : 06/17/10 Time: 14:49 Good thru: 06/22/10 Restake by: 06/17/10
RespondBy : 06/17/10 Time: 14:49 Duration : UNKNOWN    Priority: 1
Done for  : MACON WATER AUTHORITY                   
Crew on Site: N White-lined: Y Blasting: N  Boring: Y

Remarks : AREA WILL BE MARKED WITH WHITE PAINT EMERGENCY: WATER ESCAPING... CR
        : EW EN ROUTE... ETA 30 MINUTES                                       
        : *** WILL BORE Road                          

Company : MACON WATER AUTHORITY                     Type: MEMB                
Co addr : 790 SECOND ST.                           
City    : MACON                           State   : GA Zip: 31202              
Caller  : FELECIA BUFFINGTON              Phone   :  478-464-5671              
Fax     :                                 Alt. Ph.:  478-464-5671              
Email   : FBUFFINGTON@MACONWATER.ORG                                          
Contact : CASEY CONNELL                   478-338-0411              

Submitted date: 06/17/10  Time: 14:52  Oper: 224
Mbrs : AGL118 BGAWM CCM01 GAUPC GP501 MAC01 MWA01 MWA02 
-------------------------------------------------------------------------------
