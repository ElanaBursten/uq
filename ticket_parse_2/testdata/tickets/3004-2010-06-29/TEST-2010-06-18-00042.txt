
AGL103  00886 GAUPC 06/18/10 11:24:58 06180-251-010-000 NORMAL
Underground Notification             
Notice : 06180-251-010 Date: 06/18/10  Time: 11:22  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From:        To:        Name:    BAKER                          ST   NW 
Cross1: Name:    MARIETTA                       ST   NW 
Near  : Name:                                       

State : GA County: FULTON        Place: ATLANTA                                 
Cross2: Name:    COURTLAND                      ST      

Subdivision:                                         
Locate:  RIGHT OF WAY ON BOTH SIDES OF ROAD FROM INTER TO INTER  INCLUDING ALL 
      :  INTERSECTIONS FOR 200FT                                                

Grids       : 3345A8423A 3345A8423B 3345A8423C 3345A8423D 3345B8423A 
Grids       : 3345B8423B 3345B8423C 3345B8423D 
Work type   : INSTL DRIVEWAY, SIDEWALK, CURBS AND RAMPS                           

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1-2 MOS    Priority: 3
Done for  : CITY OF ATLANTA                         
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : KEMI CONSTRUCTION                         Type: CONT                
Co addr : 2920 CAMPBELLTON RD                      
City    : ATLANTA                         State   : GA Zip: 30311              
Caller  : SAMIR PATEL                     Phone   :  404-349-8228              
Fax     :                                 Alt. Ph.:  404-456-4623              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/18/10  Time: 11:22  Oper: 198
Mbrs : AGL103 AGLN01 ATL01 ATL02 ATT02 BSCA COMCEN FBRLTE GAUPC GP103 
     : GP104 LEV3 MCI02 MESXX MFN02 NU103 NU104 QWEST8 ST005 SYNCH 
     : TWT90 XOC90 GP115T 
-------------------------------------------------------------------------------
