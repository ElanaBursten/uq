
AGL106  01755 GAUPC 06/17/10 14:52:45 06170-240-034-000 NORMAL
Underground Notification             
Notice : 06170-240-034 Date: 06/17/10  Time: 14:50  Revision: 000 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From: 120    To:        Name: W  CENTER                         ST      
Near  : Name:    BRADLEY                        ST  

Subdivision:                                         
Locate:  RIGHT OF WAY ON BOTH SIDES OF ROAD                                    

Grids       : 3334B8504B 3334B8504C 
Work type   : INSTL WTR SVC LINE                                                  

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 8 HRS      Priority: 3
Done for  : CITY OF CARROLLTON                      
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Road                          

Company : CITY OF CARROLLTON                        Type: MEMB                
Co addr : PO BOX 1949                              
City    : CARROLLTON                      State   : GA Zip: 30117              
Caller  : MARK PERKINS                    Phone   :  770-830-2023              
Fax     :                                 Alt. Ph.:  404-535-3659              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/17/10  Time: 14:50  Oper: 198
Mbrs : AGL106 BGAWR CAR50 CAR51 CHC06 COMNOR GAUPC GP620 SYNCH TMC02 
     : 
-------------------------------------------------------------------------------
