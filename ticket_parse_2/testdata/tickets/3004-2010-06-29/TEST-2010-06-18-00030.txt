
AGL103  00469 GAUPC 06/18/10 09:44:13 06180-300-211-000 NORMAL
Underground Notification             
Notice : 06180-300-211 Date: 06/18/10  Time: 09:41  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From:        To:        Name:    SPRING                         ST   NW 
Cross1: Name:    PEACHTREE                      PLAC NW 
Near  : Name:                                       

Subdivision:                                         
Locate: LOCATE A 40 FOOT APPROXIMATELY 100 FEET SOUTH OF THE INTERSECTION OF SP
      :  RING ST AND PEACHTREE PL.                                              

Grids       : 3346A8423C 
Work type   : REPAIR WATER SERVICE                                                

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1 HOUR     Priority: 3
Done for  : CITY OF ATLANTA                         
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : WACH WATER SERVICES                       Type: CONT                
Co addr : 2750 PEYTON ROAD                         
City    : ATLANTA                         State   : GA Zip: 30318              
Caller  : MOUNICA KONDAMURI               Phone   :  770-826-8305              
Fax     :                                 Alt. Ph.:                            
Email   : FCAGLE@WACHSWS.COM                                                  
Contact :                                                           

Submitted date: 06/18/10  Time: 09:41  Oper: 221
Mbrs : AGL103 AGLN01 ATL01 ATL02 BSCA GAUPC GP103 LEV3 MBL91 MCI02 
     : MESXX MFN02 NU103 ST005 XOC90 
-------------------------------------------------------------------------------
