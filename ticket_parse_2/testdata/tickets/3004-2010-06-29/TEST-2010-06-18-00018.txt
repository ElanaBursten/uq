
AGL106  00216 GAUPC 06/18/10 08:24:17 06180-237-012-000 EMERG 
Underground Notification             
Notice : 06180-237-012 Date: 06/18/10  Time: 08:20  Revision: 000 

State : GA County: CARROLL       Place: VILLA RICA                              
Addr  : From: 614    To:        Name:    CARROLLTON VILLA RICA          HWY     
Near  : Name: S  CARROLL                        RD  

Subdivision:                                         
Locate:  REAR OF PROPERTY OF THE SMILE CENTER                                  

EMERGENCY *** URGENT WORK *** GAAS MAIN & SERVICE LINES
Grids       : 3342A8456C 3342A8456D 3342B8456C 3342B8456D 
Work type   : RPR GAS MAIN                                                        

Start date: 06/18/10 Time: 00:00 Hrs notc : 000
Legal day : 06/18/10 Time: 08:14 Good thru: 06/23/10 Restake by: 06/18/10
RespondBy : 06/18/10 Time: 08:14 Duration : UNKNOWN    Priority: 1
Done for  : AGL                                     
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : THIS IS IN THE WALMART SHOPPING CENTER  EMERGENCY: GAS IS ESCAPING  

Company : ATLANTA GAS LIGHT                         Type: MEMB                
Co addr : 42 EDGE RD                               
City    : VILLA RICA                      State   : GA Zip: 30180              
Caller  : BRADLEY CANNON                  Phone   :  770-294-3593              
Fax     :                                 Alt. Ph.:  770-294-3593              
Email   :                                                                     
Contact : BRADLEY CANNON                  770-294-3593              

Submitted date: 06/18/10  Time: 08:20  Oper: 198
Mbrs : AGL106 BGAWR CHC06 COMNOR CRL70 GAUPC GP176 PPL02 SYNCH VILL50 
     : VILL51 
-------------------------------------------------------------------------------
