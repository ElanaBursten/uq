
AGL106  02271 GAUPC 06/17/10 17:00:20 06170-214-038-000 NORMAL
Underground Notification             
Notice : 06170-214-038 Date: 06/17/10  Time: 16:57  Revision: 000 

State : GA County: HARALSON      Place: BREMEN                                  
Addr  : From: 2278   To:        Name:    OLD BUSH MILL                  RD      
Near  : Name:    OLD RIDGEWAY                   RD  

Subdivision:                                         
Locate: LOC THE AREA MARKED WITH WHITE PAINT FROM BARN TO BARN                 

Grids       : 3345A8510B 3345B8510A 3345B8510B 3345B8510C 3345C8510A 
Grids       : 3345C8510B 
Work type   : INSTALL ELECTRIC CONDUIT                                            

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1 1/2 HOUR Priority: 3
Done for  : SELF                                    
Crew on Site: Y White-lined: Y Blasting: N  Boring: N

Remarks : PLS CALL WINFORD SWAFFORD IF ANY QUESTIONS                          

Company :                                           Type: HOME                
Co addr :                                          
City    :                                 State   : GA Zip:                    
Caller  : DAVID ROSS                      Phone   :  770-313-9516              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact : WINFORD SWAFFORD                770-367-0450              

Submitted date: 06/17/10  Time: 16:57  Oper: 198
Mbrs : AGL106 BGAWR COMNOR CRL70 GAUPC HAR01 
-------------------------------------------------------------------------------
