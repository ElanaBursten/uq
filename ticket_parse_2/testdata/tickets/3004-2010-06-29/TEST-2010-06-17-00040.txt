
AGL103  01806 GAUPC 06/17/10 15:04:45 06170-301-321-000 NORMAL RESTAK
Underground Notification             
Notice : 06170-301-321 Date: 06/17/10  Time: 15:02  Revision: 000 
Old Tkt: 06020-218-017 Date: 06/02/10  Time: 09:21     

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From:        To:        Name:    4TH                            ST   NW 
Near  : Name:    ATLANTIC                       DR  

Subdivision:                                         
Locate: LOCATE ENTIRE PROPERTY AND WITHIN THE R/O/W AT 245, 247, & 258 4TH ST N
      :  W   PLEASE NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILIT
      :  IES, PLEASE RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1
      :  -866-461-7271. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA T
      :  HE SAME METHODS. ***                                                   

Grids       : 3346B8423A 3346B8423B 3346B8423C 3346B8423D 3346C8423A 
Grids       : 3346C8423B 3346C8423C 3346C8423D 
Work type   : INSTL SANITARY SWR SVC LINES, STORM DRAINS, WTR SVC LINES , GRADING 

Start date: 06/22/10 Time: 07:00 Hrs notc : 000
Legal day : 06/22/10 Time: 07:00 Good thru: 07/08/10 Restake by: 07/02/10
RespondBy : 06/21/10 Time: 23:59 Duration : 80 DAYS    Priority: 3
Done for  : TURNER CONSTRUCTION                     
Crew on Site: Y White-lined: N Blasting: N  Boring: N

Remarks : AWARE OF SP----RESTAKE OF IRTH TKT #12219-042-035   -- IF ANY QUESTI
        : ONS, PLS CALL FIELD CONTACT FOR DETAILS & HE WILL MEET W/ YOU AND NA
        : RROW IT DOWN  PREVIOUS TICKET NUMBER:01060-244-021  PREVIOUS TICKET 
        : NUMBER:01220-238-012  PREVIOUS TICKET NUMBER:02090-258-018  PREVIOUS
        : TICKET NUMBER:02250-268-019  PREVIOUS TICKET NUMBER:03150-243-034  P
        : REVIOUS TICKET NUMBER:03310-247-029  PREVIOUS TICKET NUMBER:04160-25
        : 3-021  PREVIOUS TICKET NUMBER:04300-238-013  PREVIOUS TICKET NUMBER:
        : 05170-231-032  PREVIOUS TICKET NUMBER:06020-218-017                 

Company : RICHARD R HARP EXCAVATION                 Type: CONT                
Co addr : 240 INDUSTRIAL WAY                       
City    : FAYETTEVILLE                    State   : GA Zip: 30214              
Caller  : DIANNE PAGE                     Phone   :  770-460-7747              
Fax     :  770-460-9571                   Alt. Ph.:  404-427-7713              
Email   : DPAGE@HARPEX.COM                                                    
Contact : DAVE STUDSTILL                  678-618-0775              

Submitted date: 06/17/10  Time: 15:02  Oper: 226
Mbrs : AGL103 AGLN01 ATL01 ATL02 BSCA COMCEN FBRLTE GAUPC GP103 GP104 
     : IFN01 LEV3 MBL91 MCI02 MEA70 MESXX MFN02 NU103 NU104 ST005 
     : TWT90 XOC90 
-------------------------------------------------------------------------------
