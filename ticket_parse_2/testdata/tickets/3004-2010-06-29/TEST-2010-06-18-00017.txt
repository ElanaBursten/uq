
AGL103  00210 GAUPC 06/18/10 08:20:36 06180-236-002-000 NORMAL
Underground Notification             
Notice : 06180-236-002 Date: 06/18/10  Time: 08:18  Revision: 000 

State : GA County: FULTON        Place: ATLANTA                                 
Addr  : From: 165    To:        Name:    16TH                           ST   NW 
Near  : Name:    FOWLER                         ST  

Subdivision:                                         
Locate: LOC ANY AND ALL UTILITIES ON THE NORTH END OF THE BLDG                 

Grids       : 3347C8423B 
Work type   : INSTL GAS SVC                                                       

Start date: 06/23/10 Time: 07:00 Hrs notc : 000
Legal day : 06/23/10 Time: 07:00 Good thru: 07/09/10 Restake by: 07/06/10
RespondBy : 06/22/10 Time: 23:59 Duration : 1 DAY      Priority: 3
Done for  : BRASSFIELD AND GORRIE                   
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks :                                                                     

Company : MILLER MECHANICAL & ENGINEERING           Type: CONT                
Co addr : AIRPORT INDUSTRIAL BLVD                  
City    : MARIETTA                        State   : GA Zip: 30060              
Caller  : LARRY HUGHES                    Phone   :  770-952-3864              
Fax     :                                 Alt. Ph.:  770-952-3864              
Email   :                                                                     
Contact :                                                           

Submitted date: 06/18/10  Time: 08:18  Oper: 198
Mbrs : AGL103 ATL01 ATL02 BSCA COMCEN GAUPC GP103 MCI02 MESXX NU103 
     : TWT90 XOC90 
-------------------------------------------------------------------------------
