Please locate and mark per the attached file(s).

If you have any questions or concerns, please contact rsnyder@sunesys.com.

SUNESYSLLC 00023A USAS 04/04/14 12:15:16 A40940600-00A NORM NEW GRID

Ticket : A40940600  Date: 04/04/14 Time: 12:10 Oper: CAU Chan: 100
Old Tkt: A40940600  Date: 04/04/14 Time: 12:15 Oper: CAU Revision: 00A

Company: H/O-RAUL CASTRO                Caller: H/O-RAUL CASTRO
Co Addr: 2264 SUNNYSANDS DR
City&St: PERRIS, CA                     Zip: 92570
Phone: 951-570-4347 Ext: CELL Call back: ANYTIME ON CELL
Formn: RAUL CASTRO          Phone: 951-943-0626 Ext: HOME
Email: CASTRO_66@LIVE.COM

State: CA County: RIVERSIDE       Place: RUBIDOUX
Delineated: N
Address: 4410        Street:FAIRBANKS AVE
X/ST 1 : JURUPA RD
MPM 1:             MPM 2:
Locat: **CALLER STATES CITY IS JURUPA VALLEY**

Excav Enters Into St/Sidewalk: N

Grids: 0684J012
Lat/Long  : 34.008982/-117.437493 34.008035/-117.435208
          : 34.007925/-117.437931 34.006978/-117.435646
Caller GPS:

Boring: Y  Explosives: N  Vacuum: N
Re-Mark: N

Work : GRADING FOR FOOTINGS
Wkend: N  Night: N
Work date: 04/14/14 Time: 08:00 Hrs notc: 235 Work hrs: 139 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : H/O-RAUL CASTRO

Tkt Exp: 05/02/14

Mbrs : ATTDSOUTH     JCS01  MCISOCAL      RCS01  SAW01  SCG1OK SUNESYSLLC
UCHARCM       USCE06


