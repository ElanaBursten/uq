Please locate and mark per the attached file(s).

If you have any questions or concerns, please contact rsnyder@sunesys.com.

SUNESYSLLC 00015A USAS 04/04/14 09:47:38 A40940316-00A NORM NEW GRID

Ticket : A40940316  Date: 04/04/14 Time: 09:46 Oper: WCA1 Chan: WEB
Old Tkt: A40940316  Date: 04/04/14 Time: 09:47 Oper: WCA1 Revision: 00A

Company: WEST COAST ARBORISTS           Caller: JUSTIN
Co Addr: 2200 E VIA BURTON
City&St: ANAHEIM, CA                    Zip: 92806      Fax: 714-991-7844
Phone: 714-956-4793 Ext: 130
Formn: JUSTIN               Phone: 714-956-4793 Ext: 124
Email: AMANOS@WCAINC.COM

State: CA County: RIVERSIDE       Place: CORONA
Delineated: Y
Delineated Method: WHITEPAINT
Address: 3292        Street:MOUNTAIN PASS DR
X/ST 1 : BAINBRIDGE CIR
MPM 1:             MPM 2:
Locat: (1) TREE BEING REMOVED IN FRONT OF THIS ADDRESS.

Excav Enters Into St/Sidewalk: N

Grids: 0773B0313
Lat/Long  : 33.842642/-117.587520 33.843165/-117.586538
          : 33.840415/-117.586332 33.840939/-117.585351
Caller GPS:

Boring: N  Explosives: N  Vacuum: N
Re-Mark: N

Work : TREE AND STUMP REMOVAL
Wkend: N  Night: N
Work date: 04/11/14 Time: 08:00 Hrs notc: 166 Work hrs: 118 Priority: 2
Instruct : MARK BY                        Permit: NOT REQUIRED
Done for : CITY OF CORONA

Tkt Exp: 05/02/14

Mbrs : ATTATL ATTDSOUTH     COR19  MWD06  NEXTGLAVEN    SCG1CO SUNESYSLLC
UCOR19 USCE34 USCE83RIV     UTWCWRIV      UVZMENIF      UVZPERS


