
KORTERRA JOB UTILIQUEST_MD-SMTP-1 MD11200403 Seq: 226 04/21/2011 16:56:27
NOTICE OF INTENT TO EXCAVATE
Ticket No: 11200403
Transmit      Date: 04/21/11      Time: 04:55 PM           Op: mdsuza
Original Call Date: 04/21/11      Time: 04:52 PM           Op: mdsuza
Work to Begin Date: 04/27/11      Time: 12:01 AM
Expiration    Date: 05/10/11      Time: 11:59 PM

Place: OAKLAND
Address: 2470        Street: SWALLOW FALLS ROAD
Nearest Intersecting Street: WOTRINGS LANE

Type of Work: TRENCHING, REPLACE SEPTIC TANK
Extent of Work: LOC 100 FT RADIUS AROUND THE HOME.
Remarks: ALL WORK IN THIS GRID

Company      : BYCO ENTERPRISES, INC
Contact Name : KEVIN YODER                    Fax          : (301)245-4364
Contact Phone: (301)245-4322                  Ext          :
Caller Address: 11746 BITTINGER RD
                GRANTSVILLE, MD 21536
Email Address:
Alt. Contact : ANOTHER OFFICE #               Alt. Phone   : (301)895-4407
Work Being Done For: DONALD DEWITT
State: MD              County: GARRETT
MPG:  Y
Caller    Provided:  Map Name: GART  Map#: 1    Grid Cells: F19
Computer Generated:  Map Name: GART  Map#: 1    Grid Cells: F19

Lat:               Lon:                        Zone:
Ex. Coord NW Lat: 39.5120393Lon: -79.4155027 SE Lat: 39.5002348Lon: -79.4002130
Explosives: N
CBLC01     FRTV01     VGT
Send To: PTE09     Seq No: 0051   Map Ref:



-----------------------------------------
The information contained in this message is intended only for the
personal and confidential use of the recipient(s) named above. If
the reader of this message is not the intended recipient or an
agent responsible for delivering it to the intended recipient, you
are hereby notified that you have received this document in error
and that any review, dissemination, distribution, or copying of
this message is strictly prohibited. If you have received this
communication in error, please notify us immediately, and delete
the original message.