
PCND00 00080 UNCCa 11/09/15 08:24 AM A531300318-00A NORM NEW STRT LREQ

Ticket Nbr: A531300318-00A
Original Call Date: 11/09/15  Time: 08:24 AM  Op: DMS
Locate By Date    : 11/12/15  Time: 11:59 PM  Meet: N  Extended job: N
State: CO     County: ADAMS            City: BRIGHTON
Addr:   4737  Street: MT SHAVANO ST
Grids: 01S066W10N*               :              :               Legal: Y
Lat/Long: 39.984706/-104.764210 39.984706/-104.753516
        : 39.983178/-104.764210 39.983178/-104.753516
Type of Work: FOUNDATION/H20/SEW/GAS/ELEC SERV NEW/FIN   Exp.: N  Boring: N
Location: LOC ENTIRE LOT TO INCLUD ALL EASEMENTS AND STREET  *ACCESS OPEN*  *
        : PAINT/FLAG/SKETCH* ADDRESS PAINTED ON CURB
        : AG 11/9
Company : ST PETER EXCAVATING                       Type: NONR
Caller  : MARK ST PETER              Phone: (303)870-0754
Alt Cont: MARK                       Phone: (303)960-8922
Fax:   Email: UNCC@STPETERINC.COM
Done for: MERITAGE HOMES
Remarks:

Members :BRTN01 :CIGS02 :CMSNOCO        :DCPMDS1B       :DPPORTAL       :KMAG
Members :KMAO   :NDP01  :PCBR02 :PCND00 :PSNG04 :QLNCNC00       :TESTGS :UNPR06
Members :UWSN3