
DIG REQUEST from UFPO for: CBLVSN W NYACK             Taken: 04/16/2010 23:05
To: UTILIQUEST / PRIMARY             Transmitted: 04/16/2010 23:08 00105

Ticket: 04160-500-182-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: NEW HEMPSTEAD  /V
Addr:  From: 34     To: 36     Name:    SHERRI                         LN   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: FRONT OF PROPERTY OF 34 THROUGH 36 SHERRI LN. ALSO LOCATE THE ROAD
      : CROSSING FROM 34 TO 35, LOCATING 35 AS WELL. AREA WHITELINED. MARK
      : USING PAINT AND FLAGS 
NearSt: LACEY RD
Means of Excavation: BORING                                  Blasting: N

Work Type: INST PHONE CONDUIT VERIZON WORK ORDER 8ABRKL PRINT FOOTAGE 295'
Start Date and Time: 04/21/2010 07:00
------------------------------------------------------------------------------
Contact Name: TAMARA HACK
Company: SUTALLEE DEVELOPMENT
Addr1: 935 FIELDS CHAPEL RD            Addr2:                                 
City: CANTON                           State: GA    Zip: 30114
Phone: 678-618-3317                    Fax: 703-542-5765                    
Email: SDTAMMY@VERIZON.NET                                                    
Field Contact: MACK LYNN
Cell Phone: 703-499-7733               Fax: 770-720-2512
Working for: VERIZON FTTP
------------------------------------------------------------------------------
Comments: DIG SITE ALSO AFFECTS: WESLEY HILLS /V
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : RCKLND CTY SWR 1                 TWN RAMAPO HWY                   
       : UNI WTR NEW YRK                  BELL-VALHALLA / HDSN VLY         



