
DIG REQUEST from UFPO for: UNI WTR NEW YRK            Taken: 02/12/2010 19:51
To:                                  Transmitted: 02/17/2010 11:53 00000

Ticket: 02120-154-009-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: CONGERS  /P
Addr:  From: 69     To:        Name:    LAKE                           RD   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: PSL LOCATE THE ENTIRE FRONT OF THE STRIP MALL IN THE SIDEWALK AREA
NearSt: FRIEND ST AND SHERMAN AVE
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: REMOVE SIDEWALK
Start Date and Time: 02/18/2010 07:00
------------------------------------------------------------------------------
Contact Name: MIKE RICKLI
Company: M R J EXCAVATING
Addr1: 7 BEAVER CT                     Addr2:                                 
City: NEW CITY                         State: NY    Zip: 10956
Phone: 845-639-1015                    Fax: 845-639-1301                    
Email:                                                                        
Field Contact: MIKE RICKLI
Cell Phone: 845-629-3402               
Working for: MASON CONTRACTING
------------------------------------------------------------------------------
Comments: DIG SITE ALSO AFFECTS: CLARKSTOWN /T
        : Lookup Type: MANUAL
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : RCKLND CTY SWR 1                 T CLARKSTWN HWY                  
       : UNI WTR NEW YRK                  BELL-VALHALLA / HDSN VLY         



