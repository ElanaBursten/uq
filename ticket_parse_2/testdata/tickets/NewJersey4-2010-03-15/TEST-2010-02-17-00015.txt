
DIG REQUEST from UFPO for: UNI WTR NEW YRK            Taken: 02/17/2010 08:19
To:                                  Transmitted: 02/17/2010 11:54 00000

Ticket: 02110-145-024-01 Type: Priority      Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ROCKLAND             Place: SPRING VALLEY  /V
Addr:  From:        To:        Name:    SECORA                         RD   
Cross: From:        To:        Name:    LIBERTY                        PKWY 
Offset:

State: NY  County: ROCKLAND             Place: SPRING VALLEY  /V
Addr:  From:        To:        Name:    SECORA                         RD   
Cross: From:        To:        Name:    KENNEDY                        DR   
Offset:
------------------------------------------------------------------------------
Locate: LOCATE BOTH SIDES OF STREET ENTIRE LENGTH BTWN CROSS ROADS
NearSt: NY RT 59 AND LIBERTY PKWY
Means of Excavation: BACKHOE                                 Blasting: N

Work Type: INSTALLING NEW GAS AND ELECTRIC MAINS
Start Date and Time: 02/17/2010 07:00
------------------------------------------------------------------------------
Contact Name: PATTY SMITH
Company: ENVIRONMENTAL CONSTRUCTION
Addr1: PO BOX 563                      Addr2:                                 
City: STONY POINT                      State: NY    Zip: 10980
Phone: 845-429-0497                    Fax: 845-429-0483                    
Email:                                                                        
Field Contact: BRUCE
Cell Phone: 845-494-6715               
Working for: O&R UTILS
------------------------------------------------------------------------------
Comments: THE FOLLOWING UTILITIES NEED TO RESPOND IN ACCORDANCE WITH THE 2 DAY
        : LAW:VERIZON PLEASE RESPOND ASAP CREW IS WAITING FOR
        : RESPONSES.
        : REPEAT MESSAGE: 2010/02/17 08:19:03 KRISTIN WHITNEY
        : Lookup Type: BETWEEN INTERSECTION
------------------------------------------------------------------------------

Members: CBLVSN W NYACK                   O&R UTILS / RCK-ORNG             
       : RCKLND CTY SWR 1                 UNI WTR NEW YRK                  
       : BELL-VALHALLA / HDSN VLY         VIL SPRG VALLEY                  



