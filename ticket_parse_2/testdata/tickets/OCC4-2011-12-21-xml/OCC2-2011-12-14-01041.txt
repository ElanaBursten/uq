
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2011-12-14T16:04:24</transmitted>
    <seq_num>527</seq_num>
    <ticket>A134801649</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A134801649</ticket>
    <revision>00A</revision>
    <started>2011-12-14T15:57:35</started>
    <account>WLPILGRIM</account>
    <original_ticket>A134801649</original_ticket>
    <original_date>2011-12-14T15:57:35</original_date>
    <original_account>WLPILGRIM</original_account>
    <replace_by_date>2012-01-04T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2011-12-19T07:00:00</response_due>
    <expires>2012-01-09T07:00:00</expires>
    <state>VA</state>
    <county>PRINCE WILLIAM</county>
    <st_from_address>12304</st_from_address>
    <st_to_address>12304</st_to_address>
    <street>INDIGO SPRINGS CT</street>
    <work_type>FTTP</work_type>
    <done_for>VERIZION</done_for>
    <reference>7PFA0CQ\8754\242340</reference>
    <white_paint>Y</white_paint>
    <blasting>N</blasting>
    <boring>Y</boring>
    <name>PILGRIM COMMUNICATIONS</name>
    <address1>137 CROSS CENTER RD</address1>
    <city>DENVER</city>
    <cstate>NC</cstate>
    <zip>28037</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7048277751</phone>
    <caller>LORETTA PILGRIM</caller>
    <caller_phone>7048277751</caller_phone>
    <email>pilgrimcomm@yahoo.com</email>
    <contact>PILGRIM COMM</contact>
    <contact_phone>7048277751</contact_phone>
    <map_url>http://newtina.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&amp;TRG=A13480164900A&amp;OPR=ONH9uoiUHynZ7sdFt</map_url>
    <map_reference>5756A9</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.760311</latitude>
        <longitude>-77.561051</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.760673</latitude>
        <longitude>-77.561240</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.760544</latitude>
        <longitude>-77.560715</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.759949</latitude>
        <longitude>-77.560861</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.760078</latitude>
        <longitude>-77.561386</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >MARK ALL UNDERGROUND SERVICES MARK ENTIRE FRONT PROPERY OF 12304</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.760574</latitude>
        <longitude>-77.561264</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.760078</latitude>
        <longitude>-77.561386</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.759995</latitude>
        <longitude>-77.560966</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.760544</latitude>
        <longitude>-77.560715</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.760574</latitude>
        <longitude>-77.561180</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3845B7733B-21</grid>
      <grid>3845B7733B-22</grid>
      <grid>3845B7733B-31</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CMC703</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>NVE901</member>
        <group_code>NVE</group_code>
        <description>NORTHERN VIRGINIA ELECTRIC COO</description>
      </memberitem>
      <memberitem>
        <member>PWS901</member>
        <group_code>PWS</group_code>
        <description>PRINCE WILLIAM COUNTY SERVICE </description>
      </memberitem>
      <memberitem>
        <member>VZN213</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
