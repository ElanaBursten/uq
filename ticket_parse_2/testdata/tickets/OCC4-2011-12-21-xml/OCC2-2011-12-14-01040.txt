
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2011-12-14T16:00:17</transmitted>
    <seq_num>512</seq_num>
    <ticket>B134801346</ticket>
    <revision>00B</revision>
  </delivery>

  <tickets>
    <ticket>B134801346</ticket>
    <revision>00B</revision>
    <started>2011-12-14T15:56:05</started>
    <account>1LAB</account>
    <original_ticket>B134801346</original_ticket>
    <original_date>2011-12-14T15:56:05</original_date>
    <original_account>1LAB</original_account>
    <replace_by_date>2012-01-04T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2011-12-19T07:00:00</response_due>
    <expires>2012-01-09T07:00:00</expires>
    <state>VA</state>
    <county>FAIRFAX</county>
    <st_from_address>7816</st_from_address>
    <st_to_address>7816</st_to_address>
    <street>WOLF RUN SHOALS RD</street>
    <cross1>HENDERSON RD</cross1>
    <work_type>CONSTRUCTION - REPAIR FOUNDATION / WATERPROOFING</work_type>
    <done_for>HO ERIC</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>HOMELAND EXCAVATIONS</name>
    <address1>8501 MCGRATH RD</address1>
    <city>MANASSAS</city>
    <cstate>VA</cstate>
    <zip>20112</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>5719217683</phone>
    <caller>DAN SMITH</caller>
    <caller_phone>5719217683</caller_phone>
    <email>homelandexcavations@msn.com</email>
    <map_url>http://newtinb.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&amp;TRG=B13480134600B&amp;OPR=21vlWQKAxfSGuhQ8o</map_url>
    <map_reference>5875F1</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.743620</latitude>
        <longitude>-77.341645</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.743927</latitude>
        <longitude>-77.342684</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.744688</latitude>
        <longitude>-77.341451</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.743314</latitude>
        <longitude>-77.340604</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.742554</latitude>
        <longitude>-77.341837</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >COME UP THE LONG DRIVEWAY AND WHEN APPROACHING THE HOUSE CHECK  ENTIRE LEFT
SIDE OF THE HOUSE</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.743555</latitude>
        <longitude>-77.340908</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.743084</latitude>
        <longitude>-77.340977</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.742994</latitude>
        <longitude>-77.341194</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.743805</latitude>
        <longitude>-77.342609</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.743931</latitude>
        <longitude>-77.342678</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.744585</latitude>
        <longitude>-77.341617</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.744607</latitude>
        <longitude>-77.341471</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.744534</latitude>
        <longitude>-77.341356</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.743563</latitude>
        <longitude>-77.340911</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3844B7720B-14</grid>
      <grid>3844B7720B-23</grid>
      <grid>3844B7720B-24</grid>
      <grid>3844B7720B-34</grid>
      <grid>3844B7720C-10</grid>
      <grid>3844B7720C-20</grid>
      <grid>3844B7720C-30</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>COX609</member>
        <group_code>COX</group_code>
        <description>COX COMMUNICATIONS</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>NVE901</member>
        <group_code>NVE</group_code>
        <description>NORTHERN VIRGINIA ELECTRIC COO</description>
      </memberitem>
      <memberitem>
        <member>VZN104</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
