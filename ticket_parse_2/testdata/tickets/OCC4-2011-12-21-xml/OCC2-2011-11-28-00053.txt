
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2011-11-28T07:22:45</transmitted>
    <seq_num>34</seq_num>
    <ticket>A133200065</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A133200065</ticket>
    <revision>00A</revision>
    <started>2011-11-28T07:22:37</started>
    <account>WLLONG</account>
    <original_ticket>B130800917</original_ticket>
    <original_date>2011-11-04T12:44:06</original_date>
    <original_account>WLLONG</original_account>
    <replace_by_date>2011-12-15T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>UPDT</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>UPDATE</derived_type>
    <response_required>Y</response_required>
    <response_due>2011-12-01T07:00:00</response_due>
    <expires>2011-12-20T07:00:00</expires>
    <state>VA</state>
    <county>AUGUSTA</county>
    <street>BRADLEY  LANE</street>
    <cross1>EAST SIDE HIGHWAY</cross1>
    <cross2>LANDES LN</cross2>
    <work_type>OTHER - SEE EXCAVATION AREA FIELD FOR DETAILS</work_type>
    <done_for>DOMINION VIRGINIA POWER</done_for>
    <white_paint>Y</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>BRUCE HOWARD CONTRACTING INC</name>
    <address1>PO BOX 492</address1>
    <city>PROVIDENCE FORGE</city>
    <cstate>VA</cstate>
    <zip>23140</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>8049665825</phone>
    <caller>LANCE LONG II</caller>
    <caller_phone>8049665825</caller_phone>
    <email>lancelong@brucehowardcontracting.com</email>
    <contact>JOE WALL</contact>
    <contact_phone>8043392380</contact_phone>
    <map_url>http://newtina.vups.org/newtinweb/map_tkt.nap?Operation=MAPTKT&amp;TRG=A13320006500A&amp;OPR=53xqjXKCxiXFmdIwg</map_url>
    <centroid>
      <coordinate>
        <latitude>38.106593</latitude>
        <longitude>-78.852077</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.106940</latitude>
        <longitude>-78.852293</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106780</latitude>
        <longitude>-78.851713</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106247</latitude>
        <longitude>-78.851860</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106407</latitude>
        <longitude>-78.852440</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >INSTALLIN ACCESS FOR NEW STRUCTURES.
FROM THE INTERSECTION OF BRADLEY LANE AND EAST SIDE HIGHWAY PROCEED 1750  FEET
TOWARD THE EAST. AT THAT LOCATION YOU WILL BE ON A DOMINION VIRGINIA POWER RIGHT
OF WAY. PROCEED TOWARD THE SOUTH 240 FEET. AT THAT LOCATION BEGIN MARKING ALL
UTILITIES WITHIN THE RIGHT OF WAY FROM THAT POINT PROCEEDING SOUTH UNTIL YOU
REACH THE SUBSTATION.</location>
    <remarks xml:space="preserve" ></remarks>
    <polygon>
      <coordinate>
        <latitude>38.106892</latitude>
        <longitude>-78.852257</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106781</latitude>
        <longitude>-78.851715</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106247</latitude>
        <longitude>-78.851860</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106407</latitude>
        <longitude>-78.852440</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.106869</latitude>
        <longitude>-78.852295</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3806C7851D-12</grid>
      <grid>3806C7851D-22</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CGV347</member>
        <group_code>CGV</group_code>
        <description>COLUMBIA GAS OF VIRGINIA</description>
      </memberitem>
      <memberitem>
        <member>DOM082</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>LMS477</member>
        <group_code>LMS</group_code>
        <description>LUMOS NETWORKS INC</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
