
BEC03  00001 NCOCa 06/22/10 10:02:04 A101730763-00A NORM NEW GRID LR

North Carolina 811

Ticket : A101730763 Date: 06/22/10 Time: 10:01 Oper: LS2891 Chan:WEB
Old Tkt: A101730763 Date: 06/22/10 Time: 10:01 Oper: LS2891 Rev :00A

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address : 70
Street  : ANNIE JONES RD   Intersection: N
Cross 1 : SHOAL CREEK RD
Location: LOCATE BY 06/25/10 12:01 AM
LOCATE FROM TERMINAL ADDRESS TO AND INCLUDE ENTIRE PROPERTY//TA1001104
:
Grids   : 3504C8415B    3504D8415B    3504D8415C

Work type:BURY PHONE DROP
Work date:06/25/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: ONE DAY  Done for: AT&T

Company : AT&T  Type: UTIL
Co addr : 4500 PINNACLE LN
City    : CHATTANOOGA State: TN Zip: 37415
Caller  : LEATTRICE STRICKLAND Phone: 423-875-9514
Contact : MIKE BROWN Phone: 615-848-0843
BestTime: 615-848-0843

Submitted date: 06/22/10 Time: 10:01
Members: BEC03  SCB01* TSE01

View map at:
http://newtina.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=A10173076300A&OPR=3A4xlcVG1odT5kO7
