
BEC03  00001 NCOCc 06/21/10 09:46:25 C101720273-00C NORM NEW GRID LR

North Carolina 811

Ticket : C101720273 Date: 06/21/10 Time: 09:36 Oper: LS2891 Chan:WEB
Old Tkt: C101720273 Date: 06/21/10 Time: 09:46 Oper: LS2891 Rev :00C

State: NC Cnty: CHEROKEE Place: MURPHY In/Out: B
Subdivision:

Address : 14887
Street  : US HWY 64 W   Intersection: N
Cross 1 : MARRESTOP RD
Location: LOCATE BY 06/24/10 12:01 AM
LOCATE FROM TERMINAL ADDRESS TO AND INCLUDE ENTIRE PROPERTY//TA1001094
:
Grids   : 3501D8414B    3501D8414C

Work type:BURY PHONE DROP
Work date:06/24/10  Time: 00:01  Hours notice: 48/48  Priority: NORM
Ug/Oh/Both: U  Blasting: N  Boring: N  Railroad: N     Emergency: N
Duration: ONE DAY  Done for: AT&T

Company : AT&T  Type: UTIL
Co addr : 4500 PINNACLE LN
City    : CHATTANOOGA State: TN Zip: 37415
Caller  : LEATTRICE STRICKLAND Phone: 423-875-9514
Contact : MIKE BROWN Phone: 615-848-0843
BestTime: 615-848-0843

Submitted date: 06/21/10 Time: 09:46
Members: BEC03  BRM01  BWF01  SCB01*

View map at:
http://newtinc.ncocc.org/newtinweb/map_tkt.nap?Operation=MAPTKT&TRG=C10172027300C&OPR=daXSME5pZMAyZECz
