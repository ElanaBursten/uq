
BGAWC  00622 GAUPC 01/26/10 16:04:04 01260-236-054-000 NORMAL
Underground Notification             
Notice : 01260-236-054 Date: 01/26/10  Time: 16:01  Revision: 000 

State : GA County: MUSCOGEE      Place: COLUMBUS CITY (BALANCE)                 
Addr  : From: 555    To:        Name:    HONOLULU                       DR      
Near  : Name: N  OAKLEY                         DR  

Subdivision:                                         
Locate: LOCATE THE ENTIRE PROPERTY  *** PLEASE NOTE: THIS TICKET WAS GENERATED 
      :  FROM THE EDEN SYSTEM. UTILITIES, PLEASE RESPOND TO POSITIVE RESPONSE VI
      :  A HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE CHECK THE
      :  STATUS OF YOUR TICKET VIA THE SAME METHODS. ***                        

Grids       : 3227C8455A 3227D8455A 
Work type   : BURYING CATV SVC LINES                                              

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 1 HOUR     Priority: 3
Done for  : MEDIACOM                                
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     

Company : ATREX                                     Type: CONT                
Co addr : 7000 STORAGE COURT  SUITE 14             
City    : COLUMBUS                        State   : GA Zip: 31907              
Caller  : DOROTHY BOLTON                  Phone   :  706-568-1137              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 16:01  Oper: 198
Mbrs : BGAWC COL90 CWW01 CWW02 GAUPC KNOL1 TCC01 GP403 
-------------------------------------------------------------------------------


