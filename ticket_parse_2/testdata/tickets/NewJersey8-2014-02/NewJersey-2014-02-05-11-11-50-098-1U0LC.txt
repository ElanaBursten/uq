
KORTERRA JOB PARKWAY-SMTP-1 NJ140351231 Seq: 52 02/05/2014 11:06:26
New Jersey One Call System        SEQUENCE NUMBER 0033    CDC = NJH

Transmit:  Date: 02/04/14   At: 15:23

*** R O U T I N E         *** Request No.: 140351231

Operators Notified:
BAN     = VERIZON                       CAN     = CABLEVISION OF NJ
GPMF1   = G4S TECHNOLOGY                JOM     = JOINT MEETING OF ESSEX &
NJH     = GARDEN STATE PARKWAY          NW2     = NEWARK, CITY OF
PSHR    = PUBLIC SERVICE ELECTRIC &     XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/08/14   At 07:00  Expiration Date: 04/09/14

Location Information:
County: ESSEX                  Municipality: NEWARK
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G146.0
Other Intersection:   G146.1
Lat/Lon:
Type of Work: INSTALL GUARD RAIL
Block:                Lot:                Depth: 6FT
Extent of Work: M/O FROM MILE MARKER 146.0 TO MILE MARKER 146.1.  CURB TO
  15FT BEHIND W CURB. CURB TO 75FT BEHIND THE E CURB.   INCLUDING RAMPS
  WORKING NORTH AND SOUTHBOUND LANES
Remarks:
  Working For Contact:  JOHN HEFTY

Working For: PKF MARK III
Address:     170 PHEASANT RUN
City:        NEWTOWN, NJ  18940
Phone:       215-416-7734   Ext:

Excavator Information:
Caller:      JACK BLAZER
Phone:       609-567-2122   Ext: 25

Excavator:   HIGHWAY SAFETY SYSTEMS INC.
Address:     716 WHITE HORSE PIKE
City:        HAMMONTON, NJ  08037
Phone:       609-567-2122   Ext: 25       Fax:  609-567-4420
Cellular:    609-839-2008
Email:       JBLAZER@HIGHWAYSAFETYSYSTEMSINC.COM
End Request


