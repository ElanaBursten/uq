
KORTERRA JOB TURNPIKE-SMTP-1 NJ140350299 Seq: 22 02/05/2014 11:06:44
New Jersey One Call System        SEQUENCE NUMBER 0004    CDC = NJT

Transmit:  Date: 02/04/14   At: 09:34

*** R O U T I N E         *** Request No.: 140350299

Operators Notified:
BAN     = VERIZON                       CPC     = COLONIAL PIPELINE COMPANY
GTMF2   = G4S TECHNOLOGY LLC            NJT     = NEW JERSEY TURNPIKE AUTHO
PSBU    = PUBLIC SERVICE ELECTRIC &     SU2     = SUNOCO LOGISTICS
TGP     = TRANSCONTINENTAL GAS PIPE

Start Date/Time:    02/08/14   At 00:01  Expiration Date: 04/09/14

Location Information:
County: BURLINGTON             Municipality: MANSFIELD
Subdivision/Community:
Street:               0 NEW JERSEY TPKE
Nearest Intersection: T48.3
Other Intersection:   T48.5
Lat/Lon:
Type of Work: ROAD RECONSTRUCTION
Block:                Lot:                Depth: 50FT
Extent of Work: M/O FROM MILE MARKER 48.3 TO MILE MARKER 48.5.  CURB TO
  100FT BEHIND E CURB.  CURB TO CURB.  WORKING IN NORTHBOUND LANE
Remarks:
  Working For Contact:  FRANK CORSO

Working For: NJ TURNPIKE AUTHORITY
Address:     PO BOX 504
City:        WOODBRIDGE, NJ  07095
Phone:       732-750-5300   Ext:

Excavator Information:
Caller:      JACKIE PARKER
Phone:       732-938-4004   Ext:

Excavator:   GEORGE HARMS CONSTRUCTION
Address:     PO BOX 817
City:        FARMINGDALE, NJ  07727
Phone:       732-938-4004   Ext:          Fax:  732-375-6114
Cellular:    908-618-6353
Email:       jadamiec@ghcci.com
End Request


