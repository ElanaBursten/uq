
KORTERRA JOB TURNPIKE-SMTP-1 NJ140360212 Seq: 10 02/05/2014 11:06:31
New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = NJT

Transmit:  Date: 02/05/14   At: 09:50

*** R O U T I N E         *** Request No.: 140360212

Operators Notified:
BAN     = VERIZON                       CJJ     = AQUA NEW JERSEY, INC
CP2     = COLONIAL PIPELINE COMPANY     GTMF2   = G4S TECHNOLOGY LLC
HTW     = HAMILTON TOWNSHIP WATER P     MCI     = MCI
NJT     = NEW JERSEY TURNPIKE AUTHO     PSTN    = PUBLIC SERVICE ELECTRIC &
SU2     = SUNOCO LOGISTICS              TGP     = TRANSCONTINENTAL GAS PIPE
TWW     = TRENTON WATER WORKS

Start Date/Time:    02/11/14   At 00:01  Expiration Date: 04/10/14

Location Information:
County: MERCER                 Municipality: HAMILTON TWP
Subdivision/Community: ROBBINSVILLE
Street:               0 S BROAD ST
Nearest Intersection: CROSSWICKS HAMILTON SQ RD
Other Intersection:   NEW JERSEY TPKE
Lat/Lon:
Type of Work: INSTALL CONDUIT
Block:                Lot:                Depth: 10FT
Extent of Work: M/O BEGINS 750FT W OF C/L OF INTERSECTION AND EXTENDS 1000FT
  W.  CURB TO 50FT BEHIND W CURB.  CURB TO CURB.
Remarks:
  Working For Contact:  JEFF VARADAY

Working For: NEW JERSEY TPKE AUTHORITY
Address:     581 MAIN ST
City:        WOODBRIDGE, NJ  07095
Phone:       732-750-5300   Ext:

Excavator Information:
Caller:      CHARLES BERENATO
Phone:       609-561-0722   Ext:

Excavator:   BERENATO CONTRACTORS
Address:     PO BOX 496
City:        HAMMONTON, NJ  08037
Phone:       609-561-0722   Ext:          Fax:  609-561-8027
Cellular:    856-229-1525
Email:       berenato@comcast.net
End Request


