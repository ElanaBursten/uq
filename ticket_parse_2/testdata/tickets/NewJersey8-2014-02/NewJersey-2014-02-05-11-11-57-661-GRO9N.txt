
KORTERRA JOB TURNPIKE-SMTP-1 NJ140301003 Seq: 17 02/05/2014 11:06:39
New Jersey One Call System        SEQUENCE NUMBER 0007    CDC = NJT

Transmit:  Date: 01/30/14   At: 14:28

*** R O U T I N E         *** Request No.: 140301003

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
BUK     = BUCKEYE PARTNERS              CGD     = NEON TRANSCOM
CP2     = COLONIAL PIPELINE COMPANY     EG1     = ELIZABETHTOWN GAS COMPANY
ELC     = ELIZABETH CITY                GTMF1   = G4S TECHNOLOGY LLC
NEITS01 = NORTHEASTERN ITS, LLC         NJT     = NEW JERSEY TURNPIKE AUTHO
NW2     = NEWARK, CITY OF               POR     = PORT AUTHORITY OF NY & NJ
PSHR    = PUBLIC SERVICE ELECTRIC &     SCLWC   = NJ AMERICAN WATER
TGP     = TRANSCONTINENTAL GAS PIPE     TK7     = CABLEVISION OF NJ
USS     = SPRINT

Start Date/Time:    02/05/14   At 00:01  Expiration Date: 04/04/14

Location Information:
County: UNION                  Municipality: ELIZABETH
Subdivision/Community: NEWARK LIBERTY INTERNATIONAL AIRPORT
Street:               0 NEW JERSEY TPKE
Nearest Intersection: NORTH AVE E
Other Intersection:
Lat/Lon:
Type of Work: INSTALL ELECTRIC SECONDARY
Block:                Lot:                Depth: 5FT
Extent of Work: M/O BEGINS 1500FT N OF C/L OF INTERSECTION AND EXTENDS
  7100FT N.  M/O BEGINS 1000FT BEHIND W CURB AND EXTENDS 1000FT W.
Remarks:
  Working For Contact:  KARL MARTINSEN

Working For: PORT AUTHORITY OF NY/NJ
Address:     80 BREWSTER WAY
City:        NEWARK, NJ  07114
Phone:       646-879-2113   Ext:

Excavator Information:
Caller:      GARY GROSSI
Phone:       973-923-5400   Ext:

Excavator:   CON-EL ELECTRIC INC
Address:     1416 N BROAD ST
City:        HILLSIDE, NJ  07205
Phone:       973-923-5400   Ext:          Fax:  973-923-0001
Cellular:    732-616-8950
Email:       ggrossiconel@gmail.com
End Request


