
KORTERRA JOB PARKWAY-SMTP-1 NJ140301093 Seq: 47 02/05/2014 11:06:20
New Jersey One Call System        SEQUENCE NUMBER 0020    CDC = NJH

Transmit:  Date: 01/30/14   At: 15:03

*** R O U T I N E         *** Request No.: 140301093

Operators Notified:
BAN     = VERIZON                       CCO     = CLIFTON CITY, CITY OF
DWS     = NORTH JERSEY DISTRICT WAT     FTN     = FIBER TECHNOLOGY NETWORKS
GPMF1   = G4S TECHNOLOGY                NJH     = GARDEN STATE PARKWAY
NW1     = NEWARK, CITY OF               PSCF    = PUBLIC SERVICE ELECTRIC &
PVW     = PASSAIC VALLEY WATER COMM     TCI     = CABLEVISION OF OAKLAND, L
TGP     = TRANSCONTINENTAL GAS PIPE     UW5     = UNITED WATER JERSEY CITY

Start Date/Time:    02/05/14   At 01:00  Expiration Date: 04/04/14

Location Information:
County: PASSAIC                Municipality: CLIFTON
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G154
Other Intersection:
Lat/Lon:
Type of Work: REPAIR GUIDE RAIL
Block:                Lot:                Depth: 6FT
Extent of Work: M/O AT MILE MARKER 154 EXTENDING 500FT N AND 500FT S.  CURB
  TO 30FT BEHIND W CURB.  NORTHBOUND
Remarks:
  Working For Contact:  CARLO SOCIO

Working For: NJ TURNPIKE AUTHORITY
Address:     5050 GARDEN STATE PKWY
City:        WOODBRIDGE, NJ  07095
Phone:       908-413-0644   Ext:

Excavator Information:
Caller:      BRANDY BLAZER
Phone:       609-481-3312   Ext:

Excavator:   J FLETCHER CREAMER & SON
Address:     PO BOX 617
City:        HAMMONTON, NJ  08037
Phone:       609-481-3312   Ext:          Fax:  609-561-6507
Cellular:
Email:       bblazer@jfcson.com
End Request


