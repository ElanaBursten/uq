
KORTERRA JOB PARKWAY-SMTP-1 NJ140351036 Seq: 44 02/05/2014 11:06:16
New Jersey One Call System        SEQUENCE NUMBER 0028    CDC = NJH

Transmit:  Date: 02/04/14   At: 14:29

*** R O U T I N E         *** Request No.: 140351036

Operators Notified:
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH
FRC     = 4 CONNECTIONS, L.L.C.         GPE     = JERSEY CENTRAL POWER & LI
GPMF3   = G4S TECHNOLOGY                NJH     = GARDEN STATE PARKWAY
NJN     = NEW JERSEY NATURAL GAS CO     OCE     = OCEAN COUNTY ENGINEERING
OCU     = OCEAN COUNTY UTILITIES AU     USS     = SPRINT

Start Date/Time:    02/08/14   At 07:00  Expiration Date: 04/09/14

Location Information:
County: OCEAN                  Municipality: LAKEWOOD
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G91.23
Other Intersection:
Lat/Lon:
Type of Work: ROAD RECONSTRUCTION
Block:                Lot:                Depth: 20FT
Extent of Work: M/O AT MILE MARKER 91.23 EXTENDING 1905FT N.  CURB TO 20FT
  BEHIND BOTH CURBS.  CURB TO CURB.  WORKING SOUTHBOUND PARKWAY
Remarks:
  Working For Contact:  RICHARD RACZYINSKI

Working For: NJTA
Address:     581 MAIN ST
City:        WOODBRIDGE, NJ  07095
Phone:       732-750-5300   Ext:

Excavator Information:
Caller:      JEANETTE CHOJNACKI
Phone:       732-223-9393   Ext:

Excavator:   MIDLANTIC CONSTRUCTION LLC
Address:     371 MAIN ST
City:        BARNEGAT, NJ  08005
Phone:       732-223-9393   Ext:          Fax:  732-223-2460
Cellular:
Email:       janaya@midlanticconstllc.com
End Request


