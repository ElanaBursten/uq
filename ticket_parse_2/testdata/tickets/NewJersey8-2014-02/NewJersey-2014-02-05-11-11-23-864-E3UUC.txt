
KORTERRA JOB PARKWAY-SMTP-1 NJ140351029 Seq: 42 02/05/2014 11:06:14
New Jersey One Call System        SEQUENCE NUMBER 0029    CDC = NJH

Transmit:  Date: 02/04/14   At: 14:30

*** R O U T I N E         *** Request No.: 140351029

Operators Notified:
BAN     = VERIZON                       BIT     = BRICK TOWNSHIP MUNICIPAL
CAM     = CABLEVISION OF MONMOUTH       CC4     = COMCAST CABLEVISION OF NE
FRC     = 4 CONNECTIONS, L.L.C.         GPMF3   = G4S TECHNOLOGY
GPP     = JERSEY CENTRAL POWER & LI     NJH     = GARDEN STATE PARKWAY
NJN     = NEW JERSEY NATURAL GAS CO     OCE     = OCEAN COUNTY ENGINEERING
USS     = SPRINT

Start Date/Time:    02/08/14   At 07:00  Expiration Date: 04/09/14

Location Information:
County: OCEAN                  Municipality: BRICK
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G91.55
Other Intersection:   G93.5
Lat/Lon:
Type of Work: ROAD RECONSTRUCTION
Block:                Lot:                Depth: 20FT
Extent of Work: M/O FROM MILE MARKER 91.55 TO MILE MARKER 93.5.  CURB TO
  20FT BEHIND BOTH CURBS.  CURB TO CURB.   NORTHBOUND LANE.
Remarks:
  Working For Contact:  RICHARD RACZYINSKI

Working For: NJTA
Address:     581 MAIN ST
City:        WOODBRIDGE, NJ  07095
Phone:       732-750-5300   Ext:

Excavator Information:
Caller:      JEANETTE CHOJNACKI
Phone:       732-223-9393   Ext:

Excavator:   MIDLANTIC CONSTRUCTION LLC
Address:     371 MAIN ST
City:        BARNEGAT, NJ  08005
Phone:       732-223-9393   Ext:          Fax:  732-223-2460
Cellular:
Email:       janaya@midlanticconstllc.com
End Request


