
KORTERRA JOB PARKWAY-SMTP-1 NJ140381049 Seq: 3 02/07/2014 13:55:35New Jersey One Call System        SEQUENCE NUMBER 0005    CDC = NJH

Transmit:  Date: 02/07/14   At: 13:41

*** R O U T I N E         *** Request No.: 140381049

Operators Notified:
ACT     = ATLANTIC COUNTY DPW           ACV     = ATLANTIC COUNTY UTILITIES
AE1     = ATLANTIC CITY ELECTRIC        AM2     = AT&T CORP
BAN     = VERIZON                       FTN     = FIBER TECHNOLOGY NETWORKS
GAL     = TOWNSHIP OF GALLOWAY          GPMF4   = G4S TECHNOLOGY
MCI     = MCI                           NJH     = GARDEN STATE PARKWAY
SCNJ3   = NJ AMERICAN WATER             SCV     = COMCAST CABLE (VINELAND)
SJG     = SOUTH JERSEY GAS COMPANY      USS     = SPRINT

Start Date/Time:    02/13/14   At 00:01  Expiration Date: 04/14/14

Location Information:
County: ATLANTIC               Municipality: GALLOWAY
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: G40.7
Other Intersection:   G42.5
Lat/Lon:
Type of Work: ROAD RECONSTRUCTION
Block:                Lot:                Depth: 100FT
Extent of Work: M/O FROM MILE MARKER 40.7 TO MILE MARKER 42.5.  CURB TO
  350FT BEHIND BOTH CURBS.  CURB TO CURB.  INCLUDING ALL RAMPS AND
  INTERSECTIONS. WORKING IN NORTH AND SOUTHBOUND LANES.
Remarks:
  Working For Contact:  BOB HARTMAN

Working For: NEW JERSEY TURNPIKE AUTHORITY
Address:     160 S PITNEY RD STE 1
City:        GALLOWAY, NJ  08205
Phone:       609-377-5131   Ext:

Excavator Information:
Caller:      SAM PAGANO
Phone:       856-451-5300   Ext:

Excavator:   SOUTH STATE INC.
Address:     PO BOX 68
City:        BRIDGETON, NJ  08302
Phone:       856-451-5300   Ext:          Fax:  609-481-2418
Cellular:    856-498-5055
Email:       spagano@southstateinc.com
End Request


