
KORTERRA JOB PARKWAY-SMTP-1 NJ140351387 Seq: 53 02/05/2014 11:06:27
New Jersey One Call System        SEQUENCE NUMBER 0034    CDC = NJH

Transmit:  Date: 02/04/14   At: 17:21

*** R O U T I N E         *** Request No.: 140351387

Operators Notified:
AM2     = AT&T CORP                     BAN     = VERIZON
BIT     = BRICK TOWNSHIP MUNICIPAL      CAM     = CABLEVISION OF MONMOUTH
GPMF3   = G4S TECHNOLOGY                GPP     = JERSEY CENTRAL POWER & LI
LKT     = LEVEL 3 COMMUNICATIONS        MCI     = MCI
MFR     = ZAYO GROUP                    NJH     = GARDEN STATE PARKWAY
NJN     = NEW JERSEY NATURAL GAS CO     NWS     = NEW JERSEY WATER SUPPLY A
SCNJ7   = NJ AMERICAN WATER             USS     = SPRINT
WAT     = WALL TOWNSHIP

Start Date/Time:    02/08/14   At 07:00  Expiration Date: 04/09/14

Location Information:
County: MONMOUTH               Municipality: WALL
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: CO RD 524
Other Intersection:   HERBERTSVILLE RD
Lat/Lon:
Type of Work: INSTALL FENCE
Block:                Lot:                Depth: 4FT
Extent of Work: M/O ENTIRE LENGTH OF GARDEN STATE PKWY FROM C/L OF CO RD 524
  TO C/L OF HERBERTSVILLE RD.  CURB TO 100FT BEHIND W CURB.  WORKING ON
  THE SOUTHBOUND LANES.
Remarks:
  Working For Contact:  ANTHONY MERLINO

Working For: NORTHEAST REMSCO CONST. INC
Address:     1433 NEW JERSEY RTE 34
City:        WALL, NJ
Phone:       732-557-6100   Ext:

Excavator Information:
Caller:      ROBERT ESAU
Phone:       609-513-6145   Ext:

Excavator:   A & R FENCE AND GUIDERAIL LLC
Address:     1711 BETULA DR
City:        WILLIAMSTOWN, NJ  08094
Phone:       609-513-6145   Ext:          Fax:  609-270-7348
Cellular:    609-513-6145
Email:       PESAU564@COMCAST.NET
End Request


