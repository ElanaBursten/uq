
KORTERRA JOB TURNPIKE-SMTP-1 NJ140360094 Seq: 19 02/05/2014 11:06:41
New Jersey One Call System        SEQUENCE NUMBER 0002    CDC = NJT

Transmit:  Date: 02/05/14   At: 08:32

*** R O U T I N E         *** Request No.: 140360094

Operators Notified:
AGT     = ALGON/TEXAS EASTERN GAS T     AM2     = AT&T CORP
BAN     = VERIZON                       C10     = COMCAST CABLEVISION OF NE
CEC     = RCN BUSINESS SOLUTIONS        JCE     = JERSEY CITY ECNOMIC DEVEL
JCS     = JERSEY CITY MUNICIPAL UTI     LKT     = LEVEL 3 COMMUNICATIONS
MCI     = MCI                           MFR     = ZAYO GROUP
NJT     = NEW JERSEY TURNPIKE AUTHO     POR     = PORT AUTHORITY OF NY & NJ
PSET    = PUBLIC SERVICE ELECTRIC &     PSJC    = PUBLIC SERVICE ELECTRIC &
UW5     = UNITED WATER JERSEY CITY      XOC1    = XO NEW JERSEY, INC.

Start Date/Time:    02/11/14   At 07:00  Expiration Date: 04/10/14

Location Information:
County: HUDSON                 Municipality: JERSEY CITY
Subdivision/Community:
Street:               0 STATE RTE 139
Nearest Intersection: PALISADE AVE
Other Intersection:   JOHN F KENNEDY BLVD
Lat/Lon:
Type of Work: INSTALL FOOTINGS
Block:                Lot:                Depth: 10FT
Extent of Work: M/O ENTIRE LENGTH OF STATE RTE 139 FROM C/L OF PALISADE AVE
  TO C/L OF JOHN F KENNEDY BLVD.  CURB TO 15FT BEHIND ALL CURBS.  WORK
  TAKING PLACE IN THE WEST BOUND LANES OF STATE ROUTE 139
Remarks:
  Working For Contact:  JASON GIENER

Working For: SCHIAVONE CONSTRUCTIONS
Address:     150 MEADOWLANDS PARKWAY
City:        SECAUCUS, NJ  07094
Phone:       201-876-5070   Ext:

Excavator Information:
Caller:      CHRIS NEAL
Phone:       732-738-7700   Ext:

Excavator:   SOLAR-MITE ELECTRIC
Address:     922 KING GEORGE POST ROAD
City:        FORDS, NJ  08863
Phone:       732-738-7700   Ext: 108      Fax:  732-738-7703
Cellular:    732-496-1725
Email:       CHRIS@SOLARMITE.COM
End Request


