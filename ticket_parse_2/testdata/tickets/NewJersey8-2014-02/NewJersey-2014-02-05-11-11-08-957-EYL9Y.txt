
KORTERRA JOB PARKWAY-SMTP-1 NJ140350641 Seq: 36 02/05/2014 11:06:08
New Jersey One Call System        SEQUENCE NUMBER 0016    CDC = NJH

Transmit:  Date: 02/04/14   At: 11:34

*** R O U T I N E         *** Request No.: 140350641

Operators Notified:
BAN     = VERIZON                       CC7     = COMCAST CABLEVISION OF NE
EG1     = ELIZABETHTOWN GAS COMPANY     GPMF1   = G4S TECHNOLOGY
NJH     = GARDEN STATE PARKWAY          PSCE    = PUBLIC SERVICE ELECTRIC &
SCEW1   = NJ AMERICAN WATER

Start Date/Time:    02/08/14   At 08:00  Expiration Date: 04/09/14

Location Information:
County: UNION                  Municipality: KENILWORTH
Subdivision/Community:
Street:               0 GARDEN STATE PKWY
Nearest Intersection: MARKET ST
Other Intersection:
Lat/Lon:
Type of Work: SOIL BORINGS
Block:                Lot:                Depth: 100FT
Extent of Work: M/O BEGINS 800FT N OF C/L OF INTERSECTION AND EXTENDS 1000FT
  N.  CURB TO 600FT BEHIND E CURB.  CURB TO CURB.
Remarks:
  Working For Contact:  MIKE ROSSI

Working For: STONE ENVIRONMENTAL
Address:     535 STONE CUTTERS WAY
City:        MONTPELIER, VT  05602
Phone:       802-229-2194   Ext:

Excavator Information:
Caller:      SAM MIGLIACCIO
Phone:       973-580-5432   Ext:

Excavator:   ALL STAR DRILLING AND PROBING
Address:     31 DELRAY PL
City:        LAURENCE HARBOR, NJ  08879
Phone:       973-580-5432   Ext:          Fax:  732-946-7210
Cellular:    973-580-5432
Email:       ONECALL@allstardrillingandprobing.com
End Request


