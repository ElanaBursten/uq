

Ticket Identifier: 110139174
Ticket No: 16044516               2 FULL BUSINESS DAYS      DUPLICATION 
Send To: CC7722     Seq No:    2  Map Ref:  

Transmit      Date:  2/22/16   Time:  1:16 PM    Op: webusr 
Original Call Date:  2/22/16   Time: 11:05 AM    Op: webusr 
Work to Begin Date:  2/25/16   Time: 12:00 AM 

State: WA            County: KING                    Place: BELLEVUE 
Address:  13515      Street: SE 50TH PL 
Nearest Intersecting Street: 134TH PL SE 

Twp: 24N   Rng: 5E    Sect-Qtr: 22-NW 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 47.5576732Lon: -122.1601244SE Lat: 47.5567522Lon: -122.1591159 

Type of Work: INSTALL GAS SVC 
Location of Work: ADD IS APRX 188 FT EAST OF INTER ON SOUTH SIDE OF 50TH.
: MARK ENTIRE LOT OUT TO BOTH SIDES OF 50TH. MARKED IN WHITE. INCLUDE ALL ROWS
: AND SIDE SEWER INFORMATION RELATED TO THIS REQUEST.++FLAGS IF TOO WET TO
: PAINT++ 

Remarks: JOB START 2/25/16 
: 106297491 

Company     : NOK INFRASOURCE                  Best Time:   
Contact Name: MARY OUM                         Phone: (425)516-4916 
Email Address:  mary.oum@pse.com 
Alt. Contact:                                  Phone:  
Contact Fax :  
Work Being Done For: PSE 
Additional Members:  
BELLVE01   OLYPE01    PUGE03     PUGG03     QLNWA16
