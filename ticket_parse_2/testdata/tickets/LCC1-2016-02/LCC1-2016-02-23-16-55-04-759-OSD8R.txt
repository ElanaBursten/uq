

Ticket Identifier: 110138909
Ticket No: 16043744               2 FULL BUSINESS DAYS      DUPLICATION 
Send To: CC7712     Seq No:    3  Map Ref:  

Transmit      Date:  2/22/16   Time:  1:12 PM    Op: orheid 
Original Call Date:  2/21/16   Time:  5:32 AM    Op: webusr 
Work to Begin Date:  3/04/16   Time: 12:00 PM 

State: WA            County: PACIFIC                 Place: SOUTH BEND 
Address:             Street: US 101 
Nearest Intersecting Street: FERRY ST. 

Twp: 14N   Rng: 9W    Sect-Qtr: 33-NW-NE,28-SW-SE 
Twp:       Rng:       Sect-Qtr:        
Ex. Coord NW Lat: 46.6649819Lon: -123.8072377SE Lat: 46.6636749Lon: -123.8044327 

Type of Work: PUTTING IN A CHAIN-LINK FENCE 
Location of Work: APPROXIMATELY 200 FEET ON RIVER SIDE ALONG SIDEWALK NORTH
: OF FERRY ST. 

Remarks:  
:  

Company     : DEPARTMENT OF TRANSPORTION       Best Time:   
Contact Name: SCOTT STEPHENS                   Phone: (360)942-2092 
Email Address:  STEPHESC@WSDOT.WA.GOV 
Alt. Contact: GARY AUST                        Phone: (360)749-1622 
Contact Fax :  
Work Being Done For: WSDOT 
Additional Members:  
CSB01      PTI38      PUD2PC01
