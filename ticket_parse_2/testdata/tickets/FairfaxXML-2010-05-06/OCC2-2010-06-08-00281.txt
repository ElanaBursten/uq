
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-06-08T09:54:52</transmitted>
    <seq_num>190</seq_num>
    <ticket>A015900716</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A015900716</ticket>
    <revision>00A</revision>
    <started>2010-06-08T09:49:34</started>
    <account>1SMM</account>
    <original_ticket>A015900716</original_ticket>
    <original_date>2010-06-08T09:49:34</original_date>
    <original_account>1SMM</original_account>
    <replace_by_date>2010-06-25T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>NORMAL</derived_type>
    <response_required>Y</response_required>
    <response_due>2010-06-11T07:00:00</response_due>
    <expires>2010-06-30T07:00:00</expires>
    <state>VA</state>
    <county>FREDERICK</county>
    <subdivision>LAKE HOIDAY</subdivision>
    <st_from_address>506</st_from_address>
    <st_to_address>513</st_to_address>
    <street>NORTHWOOD CIR</street>
    <cross1>VISTA CT</cross1>
    <work_type>SEWER LINE - REPAIR</work_type>
    <done_for>AQUA OF VIRGINIA</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>CORBIN CONSTRUCTION AND EXCAVATING</name>
    <address1>5189A MAIN ST</address1>
    <city>STEPHENS CITY</city>
    <cstate>VA</cstate>
    <zip>22655</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>5408690050</phone>
    <caller>NICK HALL</caller>
    <caller_phone>5403369684</caller_phone>
    <email>corbinconstruction@verizon.net</email>
    <contact>CHARLIE CORBIN</contact>
    <contact_phone>5403369684</contact_phone>
    <map_reference>6A7</map_reference>
    <centroid>
      <coordinate>
        <latitude>39.310533</latitude>
        <longitude>-78.331253</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>39.311039</latitude>
        <longitude>-78.330589</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310604</latitude>
        <longitude>-78.330415</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310014</latitude>
        <longitude>-78.331892</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310449</latitude>
        <longitude>-78.332065</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >IN FRONT OF THE ABOVE ADDRESSES THE ROAD AND BOTH SIDES OF THE ROAD</location>
    <remarks xml:space="preserve" >CALLER MAP REF: 6A7</remarks>
    <polygon>
      <coordinate>
        <latitude>39.311008</latitude>
        <longitude>-78.330666</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310848</latitude>
        <longitude>-78.330513</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310196</latitude>
        <longitude>-78.331436</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310268</latitude>
        <longitude>-78.331993</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310524</latitude>
        <longitude>-78.331879</longitude>
      </coordinate>
      <coordinate>
        <latitude>39.310406</latitude>
        <longitude>-78.331558</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3918B7819A-12</grid>
      <grid>3918B7819A-13</grid>
      <grid>3918B7819A-21</grid>
      <grid>3918B7819A-22</grid>
      <grid>3918B7819A-23</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>AQV321</member>
        <group_code>AQV</group_code>
        <description>AQUA VIRGINIA INC</description>
      </memberitem>
      <memberitem>
        <member>CMC209</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>SVE905</member>
        <group_code>SVE</group_code>
        <description>SHENANDOAH VALLEY ELECTRIC COO</description>
      </memberitem>
      <memberitem>
        <member>VZN210</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
