
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSb</center>
    <recipient>TEST05</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-05-11T09:34:33</transmitted>
    <seq_num>1</seq_num>
    <ticket>A013000321</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A013000321</ticket>
    <revision>00A</revision>
    <started>2010-05-10T08:25:26</started>
    <account>WTCORNELL</account>
    <original_ticket>A013000321</original_ticket>
    <original_date>2010-05-10T08:25:26</original_date>
    <original_account>WTCORNELL</original_account>
    <replace_by_date>2010-05-27T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>RETRANSMIT</derived_type>
    <response_required>Y</response_required>
    <response_due>2010-05-13T07:00:00</response_due>
    <expires>2010-06-02T07:00:00</expires>
    <state>VA</state>
    <county>FAIRFAX</county>
    <place>ANNANDALE</place>
    <st_from_address>3612</st_from_address>
    <st_to_address>3612</st_to_address>
    <street>TERRACE DR</street>
    <work_type>PLANT TREES OR SHRUBS</work_type>
    <done_for>HART 7033549528</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>MEADOWS FARMS GROUNDS MAINTENANCE</name>
    <address1>43054 JOHN MOSBY HWY</address1>
    <city>CHANTILLY</city>
    <cstate>VA</cstate>
    <zip>20152</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7033273940</phone>
    <caller>TAMATHA CORNELL</caller>
    <caller_phone>7033275050</caller_phone>
    <caller_phone_ext>254</caller_phone_ext>
    <email>mfgm@meadowsfarms.com</email>
    <map_reference>5646A5</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.844815</latitude>
        <longitude>-77.185615</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.845014</latitude>
        <longitude>-77.186187</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.845182</latitude>
        <longitude>-77.185146</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844623</latitude>
        <longitude>-77.185056</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844455</latitude>
        <longitude>-77.186097</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >MARK ENTIRE PROPERTY OF HOME DO NOT MARK HARD SURFACES</location>
    <remarks xml:space="preserve" >CALLER MAP REF: NONE</remarks>
    <polygon>
      <coordinate>
        <latitude>38.844758</latitude>
        <longitude>-77.185078</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844603</latitude>
        <longitude>-77.185184</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844497</latitude>
        <longitude>-77.185841</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844543</latitude>
        <longitude>-77.185977</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844768</latitude>
        <longitude>-77.186147</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.845050</latitude>
        <longitude>-77.185960</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.845134</latitude>
        <longitude>-77.185320</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.845063</latitude>
        <longitude>-77.185170</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.844759</latitude>
        <longitude>-77.185078</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3850B7711D-01</grid>
      <grid>3850B7711D-02</grid>
      <grid>3850B7711D-11</grid>
      <grid>3850B7711D-12</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>COX609</member>
        <group_code>COX</group_code>
        <description>COX COMMUNICATIONS</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>FCU901</member>
        <group_code>FCU</group_code>
        <description>FAIRFAX COUNTY</description>
      </memberitem>
      <memberitem>
        <member>FCW902</member>
        <group_code>FCW</group_code>
        <description>FAIRFAX WATER</description>
      </memberitem>
      <memberitem>
        <member>VZN102</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
