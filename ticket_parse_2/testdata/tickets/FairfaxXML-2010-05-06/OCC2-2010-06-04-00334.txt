
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2010-06-04T10:26:43</transmitted>
    <seq_num>191</seq_num>
    <ticket>A015500610</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A015500610</ticket>
    <revision>00A</revision>
    <started>2010-06-04T10:21:37</started>
    <account>1IAP</account>
    <original_ticket>A015500610</original_ticket>
    <original_date>2010-06-04T10:21:37</original_date>
    <original_account>1IAP</original_account>
    <priority>EMER</priority>
    <type>NEW</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>EMERGENCY</derived_type>
    <response_required>N</response_required>
    <response_due>2010-06-04T13:21:38</response_due>
    <state>VA</state>
    <county>FAIRFAX</county>
    <place>RESTON</place>
    <st_from_address>12310</st_from_address>
    <st_to_address>12310</st_to_address>
    <street>SUNRISE VALLEY DR</street>
    <cross1>GLADE DR</cross1>
    <work_type>WATER MAIN BREAK</work_type>
    <done_for>SAME</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>N</boring>
    <name>FAIRFAX COUNTY WATER AUTHORITY</name>
    <address1>8560 ARLINGTON BLVD</address1>
    <city>MERRIFIELD</city>
    <cstate>VA</cstate>
    <zip>22116</zip>
    <caller_type>UTIL</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>7036985613</phone>
    <caller>LUCY GUILLEN</caller>
    <caller_phone>7036985613</caller_phone>
    <email>dfelton@fairfaxwater.org</email>
    <contact>RICHARD HAYNES</contact>
    <contact_phone>5717227700</contact_phone>
    <map_reference>5402K8</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.949341</latitude>
        <longitude>-77.374793</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.951258</latitude>
        <longitude>-77.375109</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950033</latitude>
        <longitude>-77.372977</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.947423</latitude>
        <longitude>-77.374476</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.948648</latitude>
        <longitude>-77.376608</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >CREW IS ON SITE

FRONT OF PROPERTY INCLUDING THE ST.</location>
    <remarks xml:space="preserve" >CALLER MAP REF: 5403A9</remarks>
    <polygon>
      <coordinate>
        <latitude>38.948460</latitude>
        <longitude>-77.375719</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.948044</latitude>
        <longitude>-77.375557</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.948513</latitude>
        <longitude>-77.374268</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.949181</latitude>
        <longitude>-77.373466</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.949909</latitude>
        <longitude>-77.373177</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950363</latitude>
        <longitude>-77.373550</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950230</latitude>
        <longitude>-77.373708</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950632</latitude>
        <longitude>-77.374617</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950664</latitude>
        <longitude>-77.374687</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950870</latitude>
        <longitude>-77.375161</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.950828</latitude>
        <longitude>-77.375356</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.949063</latitude>
        <longitude>-77.376370</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.948945</latitude>
        <longitude>-77.376337</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.948571</latitude>
        <longitude>-77.375806</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3856A7722B-03</grid>
      <grid>3856A7722B-04</grid>
      <grid>3856A7722B-13</grid>
      <grid>3856A7722B-14</grid>
      <grid>3856A7722B-24</grid>
      <grid>3856A7722C-00</grid>
      <grid>3856A7722C-01</grid>
      <grid>3856A7722C-02</grid>
      <grid>3856A7722C-10</grid>
      <grid>3856A7722C-11</grid>
      <grid>3856A7722C-20</grid>
      <grid>3857D7722B-34</grid>
      <grid>3857D7722B-44</grid>
      <grid>3857D7722C-40</grid>
      <grid>3857D7722C-41</grid>
      <grid>3857D7722C-42</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CGT908</member>
        <group_code>CGT</group_code>
        <description>COLUMBIA GAS TRANSMISSION CORP</description>
      </memberitem>
      <memberitem>
        <member>CGV309</member>
        <group_code>CGV</group_code>
        <description>COLUMBIA GAS OF VIRGINIA</description>
      </memberitem>
      <memberitem>
        <member>CMC091</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>COX609</member>
        <group_code>COX</group_code>
        <description>COX COMMUNICATIONS</description>
      </memberitem>
      <memberitem>
        <member>CPL901</member>
        <group_code>CPL</group_code>
        <description>COLONIAL PIPELINE COMPANY</description>
      </memberitem>
      <memberitem>
        <member>DOM400</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>FBL410</member>
        <group_code>FBL</group_code>
        <description>FIBERLIGHT LLC</description>
      </memberitem>
      <memberitem>
        <member>FCU901</member>
        <group_code>FCU</group_code>
        <description>FAIRFAX COUNTY</description>
      </memberitem>
      <memberitem>
        <member>FCW902</member>
        <group_code>FCW</group_code>
        <description>FAIRFAX WATER</description>
      </memberitem>
      <memberitem>
        <member>LTC903</member>
        <group_code>LTC</group_code>
        <description>LEVEL 3 COMMUNICATIONS</description>
      </memberitem>
      <memberitem>
        <member>MCII81</member>
        <group_code>MCI</group_code>
        <description>MCI</description>
      </memberitem>
      <memberitem>
        <member>MFN902</member>
        <group_code>MFN</group_code>
        <description>ABOVENET COMMUNICATIONS INC</description>
      </memberitem>
      <memberitem>
        <member>QGS901</member>
        <group_code>QGS</group_code>
        <description>QWEST GOVERNMENT SERVICES</description>
      </memberitem>
      <memberitem>
        <member>TMT903</member>
        <group_code>TMT</group_code>
        <description>TW TELECOM</description>
      </memberitem>
      <memberitem>
        <member>VZN102</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WGL904</member>
        <group_code>WGL</group_code>
        <description>WASHINGTON GAS</description>
      </memberitem>
      <memberitem>
        <member>XOC903</member>
        <group_code>XOC</group_code>
        <description>XO COMMUNICATIONS INC</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
