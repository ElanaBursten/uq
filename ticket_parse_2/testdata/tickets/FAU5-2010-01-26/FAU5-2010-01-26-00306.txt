
AGL122  01937 GAUPC 01/26/10 15:56:09 01260-234-042-000 NORMAL
Underground Notification             
Notice : 01260-234-042 Date: 01/26/10  Time: 15:54  Revision: 000 

State : GA County: FLOYD         Place: ROME                                    
Addr  : From: 24     To:        Name:    VETERANS MEMORIAL              HWY  NE 
Near  : Name:    MARTHA BERRY                   HWY 

Subdivision:                                         
Locate: LOCATE THE PROPERTY IN THE AREA MARKED WITH YELLOW TAPE APPROX 100FT ON
      :  THE RIGHT BEFORE THE EXIT AND FOR 10FT ACROSS THE DRIVEWAY  *** PLEASE 
      :  NOTE: THIS TICKET WAS GENERATED FROM THE EDEN SYSTEM. UTILITIES, PLEASE
      :  RESPOND TO POSITIVE RESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-461-727
      :  1. EXCAVATORS, PLEASE CHECK THE STATUS OF YOUR TICKET VIA THE SAME METH
      :  ODS. ***                                                               

Grids       : 3416A8510A 3416A8510B 3416A8511C 3416A8511D 3416B8510A 
Grids       : 3416B8510B 3416B8511C 3416B8511D 
Work type   : RPR WATER SERVICE LINE                                              

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 7 DAYS     Priority: 3
Done for  : BERRY COLLEGE                           
Crew on Site: N White-lined: N Blasting: N  Boring: Y

Remarks :                                                                     
        : *** WILL BORE Driveway                      

Company : BERRY COLLEGE                             Type: MEMB                
Co addr : 2277 MARTHA BERRY HWY                    
City    : MOUNT BERRY                     State   : GA Zip: 30149              
Caller  : TAMMY ERRIGO                    Phone   :  706-236-2231              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:54  Oper: 198
Mbrs : AGL122 BGAWR CCAST1 GAUPC GP600 PARKF ROM50 ROM51 
-------------------------------------------------------------------------------
