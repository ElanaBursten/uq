
AGL106  01928 GAUPC 01/26/10 15:56:02 01260-260-045-000 NORMAL
Underground Notification             
Notice : 01260-260-045 Date: 01/26/10  Time: 15:52  Revision: 000 

State : GA County: CARROLL       Place: CARROLLTON                              
Addr  : From:        To:        Name:    TYUS CARROLLTON                RD      
Near  : Name:    WILLIE WALKER                  RD  

Subdivision:                                         
Locate: LOC FOR A 50FT RADIUS AROUND THE PHONE POLE/PEDESTAL #P14 AT ADDRESS 10
      :  62 AND AT ADDRESS 1063 FOR #P14.5.    *** PLEASE NOTE: THIS TICKET WAS 
      :  GENERATED FROM THE EDEN SYSTEM. UTILITIES, PLEASE RESPOND TO POSITIVE R
      :  ESPONSE VIA HTTP://EDEN.GAUPC.COM OR 1-866-461-7271. EXCAVATORS, PLEASE
      :  CHECK THE STATUS OF YOUR TICKET VIA THE SAME METHODS. ***              

Grids       : 3332A8508C 3332A8508D 3332B8508C 3332B8508D 3332C8508C 
Grids       : 
Work type   : DIGGING SPLICE PITS                                                 

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 WEEKS    Priority: 3
Done for  : AT&T                                    
Crew on Site: N White-lined: N Blasting: N  Boring: N

Remarks : AREA IS NOT MARKED NOW BUT MAY BE MARKED IN WHITE PAINT BEFORE LOCAT
        : ORS ARRIVE ON SITE.                                                 

Company : CLARK PREMIER CONSTRUCTION                Type: CONT                
Co addr : 7080 OLD BEULAH RD                       
City    : LITHIA SPRINGS                  State   : GA Zip: 30122              
Caller  : GAIL SWEATMAN                   Phone   :  678-265-7032              
Fax     :                                 Alt. Ph.:                            
Email   :                                                                     
Contact :                                                           

Submitted date: 01/26/10  Time: 15:52  Oper: 198
Mbrs : AGL106 BGAWR CAR50 CAR51 CCW01 COMNOR COMNR2 CRL70 GAUPC 
-------------------------------------------------------------------------------
