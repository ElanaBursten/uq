
AGL113  01952 GAUPC 01/26/10 16:00:18 01260-301-249-000 NORMAL
Underground Notification             
Notice : 01260-301-249 Date: 01/26/10  Time: 15:58  Revision: 000 

State : GA County: MONROE        Place: JACKSON                                 
Addr  : From: 47     To:        Name:    LAKEVIEW                       RD      
Near  : Name:    LAKESHORE                      DR  

Subdivision:                                         
Locate:  FRONT OF PROPERTY ... WHITE FLAG STAKED NEAR STREET AND CUSTOMER FENCE
      :  FOR NEW POLE TO BE SET.                                                

Grids       : 
Work type   : SETTING POLES AND ANCHORS                                           

Start date: 01/29/10 Time: 07:00 Hrs notc : 000
Legal day : 01/29/10 Time: 07:00 Good thru: 02/16/10 Restake by: 02/11/10
RespondBy : 01/28/10 Time: 23:59 Duration : 2 HOURS    Priority: 3
Done for  : GA POWER CO.                            
Crew on Site: N White-lined: Y Blasting: N  Boring: N

Remarks :                                                                     
        : *** DrivingInstructions : OFF LAKESHORE DR ONTO LAKEWIEW RD AND HOUSE AT DEAD END ON RT.      

Company : GEORGIA POWER                             Type: MEMB                
Co addr : 524 ATLANTIC ST                          
City    : MANCHESTER                      State   : GA Zip: 31816              
Caller  : SCOTT ROLISON                   Phone   :  706-846-4114              
Fax     :  706-846-4159                   Alt. Ph.:  706-846-4114              
Email   : WSROLISO@SOUTHERNCO.COM                                             
Contact :                                                           

Submitted date: 01/26/10  Time: 15:58  Oper: 147
Mbrs : AGL113 BGAWM CHC08 GAUPC GP750 MON90 
-------------------------------------------------------------------------------
