
UtiliQ-NY  08233-134-002-00  10/23/2013 10:15:57  00009

Assigned to ORUUNY for 08/23/2013


DIG REQUEST from DSNY for: O&R UTILS / ORNG           Taken: 08/23/2013 07:06
To: O&R UTILS / DAY                  Transmitted: 08/23/2013 07:09 00002


Ticket: 08233-134-002-00 Type: Regular       Previous Ticket: 
------------------------------------------------------------------------------
State: NY  County: ORANGE               Place: HIGHLANDS  /T
Addr:  From: 44     To:        Name:    OLD STATE                      RD   
Cross: From:        To:        Name:
Offset:
------------------------------------------------------------------------------
Locate: MARK DRIVEWAY AREA
NearSt: US RT 9W
Means of Excavation: MINI EXCAVATOR                          Blasting: N
Site marked with white: N
Boring/Directional Drilling: N
Within 25ft of Edge of Road: Y


Work Type: REMOVE/REPLACE DRIVEWAY
Duration: 1 DAYS
Depth of excavation: 1.5 FEET
Site dimensions: Length 70 FEET Width 10 FEET
Start Date and Time: 08/29/2013 07:00
Must Start By: 09/13/2013
------------------------------------------------------------------------------
Contact Name: PETER DESIMONE
Company: ALPHA PAVING CORP
Addr1: 17 COLLABERG RD                 Addr2:                                 
City: STONY POINT                      State: NY    Zip: 10980
Phone: 845-786-2930                    Fax: 845-268-7257                    
Email:                                                                        
Field Contact: PETER
Cell Phone: 914-447-4740               
Working for: MURPHY-HOMEOWNER
------------------------------------------------------------------------------
Comments: DIG SITE ALSO AFFECTS: WEST POINT /P, FORT MONTGOMERY /P,
        : HIGHLAND FALLS /V
        : Lookup Type: MANUAL
------------------------------------------------------------------------------
Boundary: n 41.358734    s 41.340419    w -73.977050    e -73.967837
------------------------------------------------------------------------------


Members: CHG / SOUTH / GAS                O&R UTILS / ORNG                 
       : TWCBL-HUDSON VALLEY              TWN HIGHLANDS                    
       : VERIZON / HUDSON VLY             BELL-VALHALLA / HDSN VLY         
       : VIL HIGHLND FLS                  
