
New Jersey One Call System        SEQUENCE NUMBER 0455    CDC = NJN 

Transmit:  Date: 01/16/14   At: 16:49 

*** R O U T I N E         *** Request No.: 140161663 

Operators Notified: 
AE2     = ATLANTIC CITY ELECTRIC CO     BAN     = VERIZON                        
LEH     = LITTLE EGG HARBOR MUNIC.      NJN     = NEW JERSEY NATURAL GAS CO      

Start Date/Time:    01/23/14   At 00:15  Expiration Date: 03/24/14 

Location Information: 
County:                        Municipality:  
Subdivision/Community:  
Street:                
Nearest Intersection:  
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL CATV MAIN 
Block:                Lot:                Depth: 4FT 
Extent of Work: CURB TO 12FT BEHIND CURB. 
Remarks:  
  CONSECUTIVE EVEN 
  Working For Contact:  MIKE MCKEON 

Working For: COMCAST 
Address:     50 RANDOLPH RD 
City:        SOMERSET, NJ  08002 
Phone:       609-597-3421   Ext:  

Excavator Information: 
Caller:      BRIAN SHORTINO 
Phone:       732-581-0133   Ext:  

Excavator:   JV INSTALLATIONS 
Address:     54 11TH ST 
City:        TOMS RIVER, NJ  08753 
Phone:       848-992-7318   Ext:          Fax:  
Cellular:    732-581-0133 
Email:       studio7spa@aol.com 
End Request 
