
XZ     00001 POCS 02/07/12 09:36:26 20120385001-000 NEW  XCAV RTN

============PENNSYLVANIA UNDERGROUND UTILITY LINE PROTECTION REQUEST============
Serial Number--[20120385001]-[000] Channel#--[0927900][0567]
Message Type--[NEW][EXCAVATION][ROUTINE]

County--[BUCKS]           Municipality--[BENSALEM TWP]
Work Site--[539 DUNKSFERRY RD]
     Nearest Intersection--[NESHAMINY STATE PARK]
     Second Intersection--[MARSHALL LN]
     Subdivision--[]                              Site Marked in White--[N]
Location Information--
     [NEW ABOVE GROUND STORAGE TANKS HAVE BEEN INSTALLED. WORKING AROUND THE NEW
      TANKS BTWN STATE RD AND MARSHALL LN]
     Caller Lat/Lon--[]
Mapped Type--[P] Mapped Lat/Lon--
     [40.082384/-74.927447,40.079270/-74.924377,40.081365/-74.919789,
      40.085357/-74.921935]
     Map Graphic--[http://www.pa1call.org/ViewMap/view.aspx?sn=20120385001]
Type of Work--[INSTL FENCE]                                  Depth--[3FT]
Extent of Excavation--[10 IN DIA]       Method of Excavation--[AUGERING]
Street--[ ] Sidewalk--[ ] Pub Prop--[ ] Pvt Prop--[X] Other--[]

Lawful Start Dates--[10-Feb-12] thru [22-Feb-12] Response Due Date--[09-Feb-12]
   Scheduled Excavation Date--[10-Feb-12] Dig Time--[0800] Duration--[2 DAYS]

Caller--[JAMES HARRIS]
Caller Phone--[609-387-4050]              Caller Ext--[]
Excavator--[HARRIS FENCE CORP]
Address--[4492  US HWY130]
City--[BERLINGTON ]                                State--[NJ] Zip--[08016]
FAX--[609-387-0277]              Caller Type--[B]
Email--[]
Work For--[NRAC]

Person to Contact--[JAMES HARRIS ]
Contact Phone--[609-387-4050]              Contact Ext--[]
Best Time to Call--[0800-1700]

Prepared--[07-Feb-12] at [0936] by [DONNA WILLIAMS]
Remarks--
     []

ATM0  ATM=AT&T ATLANTA     FP 0  FP =BUCKS CNTY W&SA  HR 0  HR =AQUA PA INC    
HR10  HR1=BENSALEM TWP     KD 0  KD =PECO WRTR        XZ 0  XZ =COMCAST CABLE B
YI 0  YI =VERIZON EAST 1

Serial Number--[20120385001]-[000]
========== Copyright (c) 2012 by Pennsylvania One Call System, Inc. ==========