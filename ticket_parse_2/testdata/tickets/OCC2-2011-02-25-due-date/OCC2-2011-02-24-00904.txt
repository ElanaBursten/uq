
<?xml version="1.0"?>
<NewDataSet>
  <delivery>
    <center>VUPSa</center>
    <recipient>UTIL13</recipient>
    <transmission_type>TKT</transmission_type>
    <transmitted>2011-02-24T15:38:26</transmitted>
    <seq_num>390</seq_num>
    <ticket>A105501421</ticket>
    <revision>00A</revision>
  </delivery>

  <tickets>
    <ticket>A105501421</ticket>
    <revision>00A</revision>
    <started>2011-02-24T15:38:06</started>
    <account>WSORRIS</account>
    <original_ticket>A103401178</original_ticket>
    <original_date>2011-02-03T14:01:15</original_date>
    <original_account>WSORRIS</original_account>
    <replace_by_date>2011-03-15T23:59:59</replace_by_date>
    <priority>NORM</priority>
    <type>RMRK</type>
    <lookup>GRID</lookup>
    <category>LREQ</category>
    <meet>N</meet>
    <derived_type>REMARK</derived_type>
    <response_required>Y</response_required>
    <response_due>2011-03-01T07:00:00</response_due>
    <expires>2011-03-18T07:00:00</expires>
    <state>VA</state>
    <county>FAUQUIER</county>
    <st_from_address>9203</st_from_address>
    <st_to_address>9203</st_to_address>
    <street>OLD DUMFRIES RD</street>
    <cross1>CATLETT SCHOOL RD</cross1>
    <work_type>CABLE TV SERVICE DROP- INSTALL</work_type>
    <done_for>COMCAST</done_for>
    <white_paint>N</white_paint>
    <blasting>N</blasting>
    <boring>Y</boring>
    <name>FIBER TECHNOLOGIES INC</name>
    <address1>11208 SINGLE OAK RD</address1>
    <city>FREDERICKSBURG</city>
    <cstate>VA</cstate>
    <zip>22407</zip>
    <caller_type>CONT</caller_type>
    <first_time_caller>N</first_time_caller>
    <phone>5407858044</phone>
    <caller>SANDRA ORRIS</caller>
    <caller_phone>5407858044</caller_phone>
    <email>sorris@fibertechnologiesinc.net</email>
    <contact>GERALD ABEL</contact>
    <contact_phone>5409033721</contact_phone>
    <map_reference>5986G5</map_reference>
    <centroid>
      <coordinate>
        <latitude>38.660177</latitude>
        <longitude>-77.643815</longitude>
      </coordinate>
    </centroid>
    <best_fit_rect>
      <coordinate>
        <latitude>38.660366</latitude>
        <longitude>-77.644531</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.660727</latitude>
        <longitude>-77.643317</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.659988</latitude>
        <longitude>-77.643097</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.659627</latitude>
        <longitude>-77.644311</longitude>
      </coordinate>
    </best_fit_rect>
    <location xml:space="preserve" >PLEASE LOCATE THE ENTIRE PROPERTY


IF SNOW ON GRD PLEASE FLAG</location>
    <remarks xml:space="preserve" >REMARK DUE TO: WEATHER</remarks>
    <polygon>
      <coordinate>
        <latitude>38.660259</latitude>
        <longitude>-77.643188</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.660686</latitude>
        <longitude>-77.643456</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.660366</latitude>
        <longitude>-77.644531</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.659763</latitude>
        <longitude>-77.644241</longitude>
      </coordinate>
      <coordinate>
        <latitude>38.659988</latitude>
        <longitude>-77.643097</longitude>
      </coordinate>
    </polygon>
    <gridlist>
      <grid>3839B7738B-21</grid>
      <grid>3839B7738B-22</grid>
      <grid>3839B7738B-23</grid>
      <grid>3839B7738B-31</grid>
      <grid>3839B7738B-32</grid>
      <grid>3839B7738B-33</grid>
    </gridlist>
    <memberlist>
      <memberitem>
        <member>CMC540</member>
        <group_code>CMC</group_code>
        <description>COMCAST</description>
      </memberitem>
      <memberitem>
        <member>CPL901</member>
        <group_code>CPL</group_code>
        <description>COLONIAL PIPELINE COMPANY</description>
      </memberitem>
      <memberitem>
        <member>DOM470</member>
        <group_code>DOM</group_code>
        <description>DOMINION VIRGINIA POWER</description>
      </memberitem>
      <memberitem>
        <member>FAS901</member>
        <group_code>FAS</group_code>
        <description>FAUQUIER COUNTY WATER &amp; SEWER</description>
      </memberitem>
      <memberitem>
        <member>VZN212</member>
        <group_code>VZN</group_code>
        <description>VERIZON</description>
      </memberitem>
      <memberitem>
        <member>WMG902</member>
        <group_code>WMG</group_code>
        <description>WILLIAMS COMPANY</description>
      </memberitem>
    </memberlist>
  </tickets>
</NewDataSet>
