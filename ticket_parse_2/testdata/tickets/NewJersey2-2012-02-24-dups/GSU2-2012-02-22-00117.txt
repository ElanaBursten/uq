
New Jersey One Call System        SEQUENCE NUMBER 0007    CDC = FPC3 

Transmit:  Date: 02/22/12   At: 09:14 

*** U P D A T E           *** Request No.: 120530332  Of Request No.: 120411369 

Operators Notified: 
BAN     = VERIZON                       CAM     = CABLEVISION OF MONMOUTH        
FPC     = JCP&L (FIRST ENERGY)          P31     = PUBLIC SERVICE ELECTRIC &      

Start Date/Time:    02/16/12   At 07:00  Expiration Date: 04/17/12 

Location Information: 
County: MONMOUTH               Municipality: MILLSTONE 
Subdivision/Community:  
Street:               17 - 21 WHITE BIRCH DR 
Nearest Intersection: BACK BONE HILL ROAD 
Other Intersection:    
Lat/Lon: 
Type of Work: INSTALL CATV MAIN 
Block:                Lot:                Depth: 15FT 
Extent of Work: CURB TO CURB.  CURB TO ENTIRE PROPERTY. 
Remarks:  
  CONSECUTIVE ALL 
  CALLER STATRE ELECTRIC & PHONE MISMARK LINE. 
  Working For Contact:  PETE MANN 

Working For: CABLEVISION 
Address:     40 PINE ST 
City:        TINTON FALLS, NJ  07752 
Phone:       732-681-8222   Ext:  

Excavator Information: 
Caller:      DUANE CARR 
Phone:       732-938-2441   Ext:  

Excavator:   DANELLA LINE SERVICE 
Address:     385 CRANBERRY ROAD 
City:        FARMINGDALE, NJ  07727 
Phone:       732-938-2441   Ext:          Fax:  732-938-7354 
Cellular:    732-938-2441 
Email:        
End Request 
