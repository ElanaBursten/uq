
------=_Part_3862_14775930.1453382556405
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

North Carolina 811

CANCELLATION NOTICE

Ticket : A160210244 Date: 01/21/16 Time: 08:21 Oper: SHIRLT76 Chan:WEB
Old Tkt:    Date: 01/21/16 Time: 08:22 Oper:   Rev :01A

State: NC Cnty: WAKE Place: RALEIGH In/Out: B
Subdivision:

Address :
Street  : BIRCHLEAF DR   Intersection: N
Cross 1 : PENNY RD
Location: LOCATE BY 11:59 PM ON 01/26/2016
LOCATE THE ENTIRE INTERSECTION OF BIRCHLEAF DR./PENNY RD. TO INCLUDE BOTH SIDES
AND IN THE ROAD FOR A ROAD BORE ON BIRCHLEAF DR.
EXCAVATION AREA WILL BE MARKED WITH WHITE PAINT, FLAGS, OR STAKES.
:
Grids   : 3542C7841A   3542B7842C   3542B7842D   3542C7842D

Work type:INSTALL UNDG CABLE
Work date:01/27/16  Time: 00:00  Hours notice: 72/72  Priority: NORM
Ug/Oh/Both: U  Blasting: Y  Boring: Y  Railroad: N     Emergency: N
Duration: 14 DAYS  Done for: AT&T/ANSCO

Company : MB CONCRETE LLC  Type: CONT
Co addr : 1346 CROSS LINK  RD
City    : RALEIGH State: NC Zip: 27610
Caller  : SHIRLENE NIXON Phone: 919-320-7424
Contact : JOSE Phone: 703-656-5927
BestTime:
Cellular: 919-344-3960
Email   : snixon@mb-concrete-llc.com

Remarks : JOB # 62D03142N DWG 5 OF 65
REASON FOR CANCEL: LOCATE INSTRUCTIONS CHANGE
01/21/2016 08:21:45 AM CANCELLED BY SHIRLT76
:
Submitted date: 01/21/16 Time: 08:22
Members: ATT322*       CLS01  CPL10* CVI04* DET01*
------=_Part_3862_14775930.1453382556405--